using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TipoRegimenTributario
    /// </summary>
    public class TipoRegimenTributario : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoRegimenTributario
        /// </summary>
        public int IdTipoRegimenTributario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTipoRegimenTributario
        /// </summary>
        public String CodigoTipoRegimenTributario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoRegimenTributario
        /// </summary>
        public String NombreTipoRegimenTributario
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        ///Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TipoRegimenTributario
        /// </summary>
        public TipoRegimenTributario()
        {
        }
    }
}
