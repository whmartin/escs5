using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase IdoneidadOferente
    /// </summary>
    public class IdoneidadOferente : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad Idtercero
        /// </summary>
        public int Idtercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdIdentidad
        /// </summary>
        public int IdIdentidad
        {
            get; 
            set; 
        }
        /// <summary>
        /// propiedad IdRankingOferente
        /// </summary>
        public int IdRankingOferente
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad TipoIdentificacion
        /// </summary>
        public string TipoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad TipoPersona
        /// </summary>
        public string TipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NoIdentificacion
        /// </summary>
        public long NoIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Oferente
        /// </summary>
        public string Oferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedd ValidacionDocumental
        /// </summary>
        public string ValidacionDocumental
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ValidacionFinanciera
        /// </summary>
        public string ValidacionFinanciera
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedd ValidacionExperiencia
        /// </summary>
        public string ValidacionExperiencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Ranking
        /// </summary>
        public int Ranking
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Liquidez
        /// </summary>
        public decimal Liquidez
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NivelEndeudamiento
        /// </summary>
        public decimal NivelEndeudamiento
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad Experiencia
        /// </summary>
        public int Experiencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EndeudamientoFinanciero
        /// </summary>
        public int EndeudamientoFinanciero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead Idoneo
        /// </summary>
        public string Idoneo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead Obervacion
        /// </summary>
        public string Obervacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propeidad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead UsuarioModifica
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase IdoneidadOferente
        /// </summary>
        public IdoneidadOferente()
        {
        }
    }
}
