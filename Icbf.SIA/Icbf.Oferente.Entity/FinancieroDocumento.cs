using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase  FinancieroDocumento
    /// </summary>
    public class FinancieroDocumento : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdFinancieroDocumento
        /// </summary>
        public int IdFinancieroDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdFinanciero
        /// </summary>
        public int IdFinanciero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdParametroDocumentoFinanciero
        /// </summary>
        public int IdParametroDocumentoFinanciero
        {
            get;
            set;
        }
       /// <summary>
        /// Propiedad IDArchivo
       /// </summary>
        public int IDArchivo
        {
            get;
            set;
        }
       /// <summary>
        /// Propiedad UsuarioCrea
       /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase FinancieroDocumento
        /// </summary>
        public FinancieroDocumento()
        {
        }
    }
}
