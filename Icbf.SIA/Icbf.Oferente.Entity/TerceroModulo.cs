using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// efinición de clase TerceroModulo
    /// </summary>
    public class TerceroModulo
    {
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public int IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdModulo
        /// </summary>
        public int IdModulo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public int Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propieda ordern
        /// </summary>
        public int ordern
        {
            get;
            set;
        }   
        /// <summary>
        /// Constructor de clase TerceroModulo
        /// </summary>
        public TerceroModulo()
        {
        }
    }
}
