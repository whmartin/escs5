﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Clase DatosGeneralesCarga
    /// </summary>
    public class DatosGeneralesCarga
    {
        /// <summary>
        /// Campo IdAnalisis
        /// </summary>
        /// <value>Valor del campo</value>
        public int IdAnalisis { get; set; }

        /// <summary>
        /// Campo Vigencia
        /// </summary>
        /// <value>Valor del campo</value>
        public int Vigencia { get; set; }

        /// <summary>
        /// Campo Mes
        /// </summary>
        /// <value>Valor del campo</value>
        public string Mes { get; set; }

        /// <summary>
        /// Campo IdTipoArchivo
        /// </summary>
        /// <value>Valor del campo</value>
        public int IdTipoArchivo { get; set; }

        /// <summary>
        /// Campo NombreArchivo
        /// </summary>
        /// <value>Valor del campo</value>
        public string NombreArchivo { get; set; }

        /// <summary>
        /// Campo IdUsuario
        /// </summary>
        /// <value>Valor del campo</value>
        public int IdUsuario { get; set; }

        /// <summary>
        /// Campo FechaCarga
        /// </summary>
        /// <value>Valor del campo</value>
        public DateTime FechaCarga { get; set; }

        /// <summary>
        /// Campo IdEstadoAnalisis
        /// </summary>
        /// <value>Valor del campo</value>
        public int IdEstadoAnalisis { get; set; }

        /// <summary>
        /// Campo Observaciones
        /// </summary>
        /// <value>Valor del campo</value>
        public string Observaciones { get; set; }

        /// <summary>
        /// Campo Version
        /// </summary>
        /// <value>Valor del campo</value>
        public int Version { get; set; }

        /// <summary>
        /// Campo CantidadRegistrosExcel
        /// </summary>
        /// <value>Valor del campo</value>
        public int CantidadRegistrosExcel { get; set; }

        /// <summary>
        /// Campo CantidadRegistrosDB
        /// </summary>
        /// <value>Valor del campo</value>
        public int CantidadRegistrosDB { get; set; }

        /// <summary>
        /// Campo UsuarioCrea
        /// </summary>
        /// <value>Valor del campo</value>
        public string UsuarioCrea { get; set; }

        /// <summary>
        /// Campo FechaCrea
        /// </summary>
        /// <value>Valor del campo</value>
        public DateTime FechaCrea { get; set; }

        /// <summary>
        /// Campo UsuarioModifica
        /// </summary>
        /// <value>Valor del campo</value>
        public string UsuarioModifica { get; set; }

        /// <summary>
        /// Campo FechaModifica
        /// </summary>
        /// <value>Valor del campo</value>
        public DateTime FechaModifica { get; set; }
    }
}
