using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase ModalidadAtender
    /// </summary>
    public class ModalidadAtender : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdModalidadAtender
        /// </summary>
        public int IdModalidadAtender
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoModalidadAtender
        /// </summary>
        public String CodigoModalidadAtender
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreModalidadAtender
        /// </summary>
        public String NombreModalidadAtender
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ModalidadAtender
        /// </summary>
        public ModalidadAtender()
        {
        }
    }
}
