using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase NaturalezaJuridica
    /// </summary>
    public class NaturalezaJuridica : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdNaturalezaJuridica
        /// </summary>
        public int IdNaturalezaJuridica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoNaturalezaJuridica
        /// </summary>
        public String CodigoNaturalezaJuridica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreNaturalezaJuridica
        /// </summary>
        public String NombreNaturalezaJuridica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public string DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase NaturalezaJuridica
        /// </summary>
        public NaturalezaJuridica()
        {
        }
    }
}
