﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase ExperienciaDepartamento
    /// </summary>
    public class ExperienciaDepartamento : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDepartamento
        /// </summary>
        public String NombreDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ExperienciaDepartamento
        /// </summary>
        public ExperienciaDepartamento()
        {
        }
    }
}
