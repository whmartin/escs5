﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TipoIdentificacion
    /// </summary>
    public class TipoIdentificacion : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTipoDocumento
        /// </summary>
        public int IdTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodDocumento
        /// </summary>
        public String CodDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoDocumento
        /// </summary>
        public String NombreTipoDocumento
        {
            get;
            set;
        }
    }
}
