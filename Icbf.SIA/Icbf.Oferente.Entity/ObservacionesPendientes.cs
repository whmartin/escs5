﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class ObservacionesPendientes : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdObservacionConvocatoria { get; set; }
        public int IdTipoObservacion { get; set; }
        public string NombreSolicitante { get; set; }
        public string CorreoSolicitante { get; set; }
        public int IdConvocatoria { get; set; }
        public string Observacion { get; set; }
        public int IdGestionObservacionConvocatoria { get; set; }
        public bool EsDevolucion { get; set; }
        public string Descripcion { get; set; }
        public string EsDevolucionMensaje { get; set; }
        public int? IdUsuario { get; set; }
        public string UsuarioCrea { get; set; }
        public int IdEstadoObservacion { get; set; }
        public bool EsCoordinador { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime? FechaModifica { get; set; }
        public string NombreEvaluador { get; set; }
        public string NombreCoordinador { get; set; }
        public int IdEvaluador { get; set; }
        public int IdCoordinador { get; set; }
        public string DocumentoEvaluador { get; set; }
        public DateTime? FechaPublicacionConvocatoria { get; set; }
        public string FechaPublicacionConvocatoriaGrilla { get; set; }
        public string EstadoConvocatoria { get; set; }
        public string NombreConvocatoria { get; set; }
        public int NumeroRadicado { get; set; }
        public DateTime FechaObservacion { get; set; }
        public string FechaObservacionGrilla { get; set; }
        public string TipoObservacion { get; set; }
        public DateTime? FechaRespuesta { get; set; }
        public string FechaRespuestaGrilla { get; set; }
        public string EstadoObservacion { get; set; }
        public DateTime FechaUltimoEstado { get; set; }
        public string FechaUltimoEstadoGrilla { get; set; }
        public string NumeroDocumento { get; set; }
        public string MotivoDevolucionDelContratista { get; set; }
        public int IdEquipoEvaluador { get; set; }
    }
}
