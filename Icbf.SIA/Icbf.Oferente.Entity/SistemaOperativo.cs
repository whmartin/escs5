using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase SistemaOperativo
    /// </summary>
    public class SistemaOperativo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propieda IdSistemaOperativo
        /// </summary>
        public int IdSistemaOperativo
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad CodigoSistemaOperativo
        /// </summary>
        public String CodigoSistemaOperativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase SistemaOperativo
        /// </summary>
        public SistemaOperativo()
        {
        }
    }
}
