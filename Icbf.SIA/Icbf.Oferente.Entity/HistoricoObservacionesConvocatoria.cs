﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class HistoricoObservacionesConvocatoria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdHistoricoObservacionConvocatoria { get; set; }
        public int IdObservacionConvocatoria { get; set; }
        public int IdUsuarioAnterior { get; set; }
        public int? IdUsuarioNuevo { get; set; }
        public int IdGestionObservacionAnterior { get; set; }
        public int IdGestionObservacionNueva { get; set; }
        public int IdEstadoObservacionAnterior { get; set; }
        public int IdEstadoObservacionNueva { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime? FechaModifica { get; set; }
    }
}
