using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase ZonaResidencia
    /// </summary>
    public class ZonaResidencia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdZonaResidencia
        /// </summary>
        public int IdZonaResidencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoZonaResidencia
        /// </summary>
        public String CodigoZonaResidencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreZonaResidencia
        /// </summary>
        public String NombreZonaResidencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ZonaResidencia
        /// </summary>
        public ZonaResidencia()
        {
        }
    }
}
