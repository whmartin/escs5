using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase Estado
    /// </summary>
    public class Estado : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdEstado
        /// </summary>
        public int IdEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoEstado
        /// </summary>
        public String CodigoEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEstado
        /// </summary>
        public String NombreEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado2
        /// </summary>
        public Boolean Estado2
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        ///¨Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Estado
        /// </summary>
        public Estado()
        {
        }
    }
}
