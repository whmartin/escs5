﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class EstadoObservaciones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdEstadoObservacion { get; set; }
        public string CodigoEstado { get; set; }
        public string DescripcionEstado { get; set; }
        public bool Estado { get; set; }
        public int PasoAnterior { get; set; }
        public int PasoSiguiente { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime? FechaModifica { get; set; }
    }
}
