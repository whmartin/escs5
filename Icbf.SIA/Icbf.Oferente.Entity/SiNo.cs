﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase SiNo
    /// </summary>
    public class SiNo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdSiNo
        /// </summary>
        public int IdSiNo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoSiNo
        /// </summary>
        public String CodigoSiNo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreSiNo
        /// </summary>
        public String NombreSiNo
        {
            get;
            set;
        }
        /// <summary>
        ///Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public string DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Usuariocrea
        /// </summary>
        public String Usuariocrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}
