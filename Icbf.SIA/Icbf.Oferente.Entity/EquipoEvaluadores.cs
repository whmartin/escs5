﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class EquipoEvaluadores : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdEquipoEvaluador { get; set; }
        public string NombreEquipoEvaluador { get; set; }
        public bool Activo { get; set; }
        public int IdUsuarioCoordinador { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public int IdUsuario { get; set; }
        public string NumeroDocumento { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NombreArea { get; set; }
        public string NombreCompleto { get; set; }
    }
}