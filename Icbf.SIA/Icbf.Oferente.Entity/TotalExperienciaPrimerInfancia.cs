﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TotalExperienciaPrimerInfancia
    /// </summary>
    public class TotalExperienciaPrimerInfancia : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTotalExperienciaPrimerInfancia
        /// </summary>
        public int IdTotalExperienciaPrimerInfancia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTotalExperienciaPrimerInfancia
        /// </summary>
        public String CodigoTotalExperienciaPrimerInfancia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTotalExperienciaPrimerInfancia
        /// </summary>
        public String NombreTotalExperienciaPrimerInfancia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public string DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Usuariocrea
        /// </summary>
        public String Usuariocrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
    }
}