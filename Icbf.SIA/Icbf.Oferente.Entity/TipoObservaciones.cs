﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class TipoObservaciones : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdTipoObservacion { get; set; }
        public string CodigoTipo { get; set; }
        public string DescripcionTipo { get; set; }
        public bool Estado { get; set; }

        public TipoObservaciones() { }
    }
}
