using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase RUPBalance
    /// </summary>
    public class RUPBalance : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdRUPBalance
        /// </summary>
        public int IdRUPBalance
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoRUPBalance
        /// </summary>
        public String CodigoRUPBalance
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreRUPBalance
        /// </summary>
        public String NombreRUPBalance
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public string DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase RUPBalance
        /// </summary>
        public RUPBalance()
        {
        }
    }
}
