using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase Tercero
    /// </summary>
    public class Referente : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdReferente
        /// </summary>
        public int IdReferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public int IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdMunicipio
        /// </summary>
        public int IdMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Nombre
        /// </summary>
        public String Nombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Correo
        /// </summary>
        public String Correo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Indicativo
        /// </summary>
        public String Indicativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Telefono
        /// </summary>
        public String Telefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Extension
        /// </summary>
        public String Extension
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Celular
        /// </summary>
        public String Celular
        {
            get;
            set;
        }
        

        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        
        /// <summary>
        ///  Constructor de clase Referente
        /// </summary>
        public Referente()
        {
        }
    }
}
