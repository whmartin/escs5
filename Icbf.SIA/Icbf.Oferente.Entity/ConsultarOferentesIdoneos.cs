using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    ///  Clase ConsultarOferentesIdoneos
    /// </summary>
    public class ConsultarOferentesIdoneos : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad Identificacion
        /// </summary>
        public string Identificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Sigla
        /// </summary>
        public string Sigla
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEntidadOferente
        /// </summary>
        public string NombreEntidadOferente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DepartamentoUbicacionSede
        /// </summary>
        public string DepartamentoUbicacionSede
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MunicipioUbicacionSede
        /// </summary>
        public string MunicipioUbicacionSede
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DepartamentoUbicacionCaracterizacion
        /// </summary>
        public string DepartamentoUbicacionCaracterizacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad MunicipioUbicacionCaracterizacion
        /// </summary>
        public string MunicipioUbicacionCaracterizacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Ranking
        /// </summary>
        public string Ranking { get; set; }

        /// <summary>
        /// Propiedad FormaVisualizarReporte
        /// </summary>
        public string FormaVisualizarReporte
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ConsultarOferentesIdoneos
        /// </summary>
        public ConsultarOferentesIdoneos()
        {
        }
    }
}
