using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definici�n de clase Tercero
    /// </summary>
    public class Tercero : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTercero
        /// </summary>
        public Int32 IdTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IDENTIDAD
        /// </summary>
        public int IDENTIDAD
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DigitoVerificacion
        /// </summary>
        public Nullable<int> DigitoVerificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDigitoVerificacion
        /// </summary>
        public String NombreDigitoVerificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoPersona
        /// </summary>
        public Nullable<int> IdTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTipoPersona
        /// </summary>
        public String NombreTipoPersona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDListaTipoDocumento
        /// </summary>
        public int IdDListaTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreListaTipoDocumento
        /// </summary>
        public String NombreListaTipoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEstadoTercero
        /// </summary>
        public Nullable<int> IdEstadoTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreEstadoTercero
        /// </summary>
        public String NombreEstadoTercero
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad RazonSocial
        /// </summary>
        public String RazonSocial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NumeroIdentificacion
        /// </summary>
        public String NumeroIdentificacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerNombre
        /// </summary>
        public String PrimerNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoNombre
        /// </summary>
        public String SegundoNombre
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrimerApellido
        /// </summary>
        public String PrimerApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SegundoApellido
        /// </summary>
        public String SegundoApellido
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Email
        /// </summary>
        public String Email
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad ProviderUserKey
        /// </summary>
        public String ProviderUserKey
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Sexo
        /// </summary>
        public String Sexo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Nombre_Razonsocial
        /// </summary>
        public String Nombre_Razonsocial
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaExpedicionId
        /// </summary>
        public Nullable<DateTime> FechaExpedicionId
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaNacimiento
        /// </summary>
        public Nullable<DateTime> FechaNacimiento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Consecutivo interno
        /// </summary>
        public string ConsecutivoInterno
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad Indicativo
        /// </summary>
        public string Indicativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Tel�fono
        /// </summary>
        public string Telefono
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Extensi�n
        /// </summary>
        public string Extension
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Celular
        /// </summary>
        public string Celular
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDepartamento
        /// </summary>
        public int IdDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdMunicipio
        /// </summary>
        public int IdMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdZona
        /// </summary>
        public int? IdZona
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Direcci�n
        /// </summary>
        public string Direccion
        {
            get;
            set;
        }








        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EsFundacion
        /// </summary>
        public Nullable<Boolean> EsFundacion
        {
            get;
            set;
        }

        /// <summary>
        /// Propiedad CreadoPorInterno
        /// </summary>
        public Nullable<Boolean> CreadoPorInterno
        {
            get;
            set;
        }
        
        /// <summary>
        ///  Constructor de clase Tercero
        /// </summary>
        public Tercero()
        {
        }
        public int claseActividad 
        { 
            get; 
            set; 
        }
    }
}
