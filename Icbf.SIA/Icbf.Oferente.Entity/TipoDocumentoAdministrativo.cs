using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TipoDocumentoAdministrativo
    /// </summary>
    public class TipoDocumentoAdministrativo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdAdministrativoDocumento
        /// </summary>
        public int IdAdministrativoDocumento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdTipoDocAdministrativo
        /// </summary>
        public int IdTipoDocAdministrativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdFormatoArchivo
        /// </summary>
        public int IdFormatoArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Personal
        /// </summary>
        public int Personal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Instituciones
        /// </summary>
        public int Instituciones
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Seleccion
        /// </summary>
        public int Seleccion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public int Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        ///¨Propiedad NombreArchivoOri
        /// </summary>
        public string NombreArchivoOri
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreArchivo
        /// </summary>
        public string NombreArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdArchivo
        /// </summary>
        public int IdArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Extension
        /// </summary>
        public string Extension
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase TipoDocumentoAdministrativo
        /// </summary>
        public TipoDocumentoAdministrativo()
        {
        }
    }
}
