using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase FechaConvocatoria
    /// </summary>
    public class FechaConvocatoria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdFechaConvocatoria
        /// </summary>
        public int IdFechaConvocatoria
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad IdDListaDepartamento
        /// </summary>
        public int IdDListaDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaIncio
        /// </summary>
        public DateTime FechaIncio
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaTerminacion
        /// </summary>
        public DateTime FechaTerminacion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedd UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase FechaConvocatoria
        /// </summary>
        public FechaConvocatoria()
        {
        }
    }
}
