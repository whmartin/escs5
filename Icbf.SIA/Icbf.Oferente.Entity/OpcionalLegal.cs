using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase OpcionalLegal
    /// </summary>
    public class OpcionalLegal : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdOpcionalLegal
        /// </summary>
        public int IdOpcionalLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoOpcionalLegal
        /// </summary>
        public String CodigoOpcionalLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase OpcionalLegal
        /// </summary>
        public OpcionalLegal()
        {
        }
    }
}
