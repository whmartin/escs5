using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase TablaParametrica
    /// </summary>
    public class TablaParametrica : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdTablaParametrica
        /// </summary>
        public int IdTablaParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoTablaParametrica
        /// </summary>
        public String CodigoTablaParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreTablaParametrica
        /// </summary>
        public String NombreTablaParametrica
        {
            get;
            set;
        }
        /// <summary>
        /// propieda Url
        /// </summary>
        public String Url
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public Boolean Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad DescripcionEstado
        /// </summary>
        public String DescripcionEstado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propieda FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor TablaParametrica
        /// </summary>
        public TablaParametrica()
        {
        }
    }
}
