using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// definición de clase ObligatorioLegal
    /// </summary>
    public class ObligatorioLegal : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// propiedad IdObligatorioLegal
        /// </summary>
        public int IdObligatorioLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad CodigoObligatorioLegal
        /// </summary>
        public String CodigoObligatorioLegal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Descripcion
        /// </summary>
        public String Descripcion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Estado
        /// </summary>
        public String Estado
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase ObligatorioLegal
        /// </summary>
        public ObligatorioLegal()
        {
        }
    }
}
