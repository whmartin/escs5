using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase  Infraestructura
    /// </summary>
    public class Infraestructura : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdInfraestructura
        /// </summary>
        public int IdInfraestructura
        {
            get;
            set;
        }
        /// <summary>
        /// Prpiedad IdVigencia
        /// </summary>
        public int IdVigencia
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdEntidad
        /// </summary>
        public Decimal IdEntidad
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdContacto
        /// </summary>
        public int IdContacto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad IdDireccion
        /// </summary>
        public int IdDireccion
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Personal
        /// </summary>
        public Decimal Personal
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad VincLaboral
        /// </summary>
        public Decimal VincLaboral
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad PrestServicios
        /// </summary>
        public Decimal PrestServicios
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Voluntariado
        /// </summary>
        public Decimal Voluntariado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad VoluntPermanente
        /// </summary>
        public Decimal VoluntPermanente
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Asociados
        /// </summary>
        public Decimal Asociados
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Mision 
        /// </summary>
        public Decimal Mision
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EspacioArchivo
        /// </summary>
        public Boolean EspacioArchivo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EspacioMatOficina
        /// </summary>
        public Boolean EspacioMatOficina
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad EquipoComputo
        /// </summary>
        public Boolean EquipoComputo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SistOperativo
        /// </summary>
        public int? SistOperativo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConLineaFija
        /// </summary>
        public Boolean ConLineaFija
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConLineaCelular
        /// </summary>
        public Boolean ConLineaCelular
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConEmail
        /// </summary>
        public Boolean ConEmail
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad ConAccesoInternet
        /// </summary>
        public Boolean ConAccesoInternet
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad OtrasSedes
        /// </summary>
        public Boolean OtrasSedes
        {
            get;
            set;
        }
        /// <summary>
        ///¨Propiedad CantidadOtrasSedes
        /// </summary>
        public int CantidadOtrasSedes
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad SedesPropias
        /// </summary>
        public Boolean SedesPropias
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiead FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreContacto
        /// </summary>
        public String NombreContacto
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreDepartamento
        /// </summary>
        public String NombreDepartamento
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad NombreMunicipio
        /// </summary>
        public String NombreMunicipio
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase Infraestructura
        /// </summary>
        public Infraestructura()
        {
        }
    }
}
