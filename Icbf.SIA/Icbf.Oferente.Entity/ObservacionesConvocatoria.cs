﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    public class ObservacionesConvocatoria : Icbf.Seguridad.Entity.EntityAuditoria
    {
        public int IdObservacionConvocatoria { get; set; }
        public int IdTipoObservacion { get; set; }
        public int IdConvocatoria { get; set; }
        public string NombreSolicitante { get; set; }
        public string CorreoSolicitante { get; set; }
        public string Observacion { get; set; }
        public int? IdUsuario { get; set; }
        public bool? Coordinador { get; set; }
        public int IdEstadoObservacion { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
    }
}
