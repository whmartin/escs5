﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Oferente.Entity
{
    /// <summary>
    /// Definición de clase SalarioMinimo
    /// </summary>
    public class SalarioMinimo : Icbf.Seguridad.Entity.EntityAuditoria
    {
        /// <summary>
        /// Propiedad IdSalarioMinimo
        /// </summary>
        public int IdSalarioMinimo
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Ano
        /// </summary>
        public int Ano
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad Valor
        /// </summary>
        public int Valor
        {
            get;
            set;
        }
        /// <summary>
        /// propiedad Estado
        /// </summary>
        public int Estado
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioCrea
        /// </summary>
        public String UsuarioCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad UsuarioModifica
        /// </summary>
        public String UsuarioModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaCrea
        /// </summary>
        public DateTime FechaCrea
        {
            get;
            set;
        }
        /// <summary>
        /// Propiedad FechaModifica
        /// </summary>
        public DateTime FechaModifica
        {
            get;
            set;
        }
        /// <summary>
        /// Constructor de clase SalarioMinimo
        /// </summary>
        public SalarioMinimo()
        {
        }
    }
}