﻿//-----------------------------------------------------------------------
// <copyright file="PagosBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase PagosBLL.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>15/12/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class PagosBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo vOrdenesPagoDAL
        /// </summary>
        private PagosDAL OrdenesPagoDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="HistoricoEstadosDenunciaBienBLL"/>.
        /// </summary>
        public PagosBLL()
        {
            this.OrdenesPagoDAL = new PagosDAL();
        }

        /// <summary>
        /// consulta todos los históricos de los estados 
        /// </summary>
        /// <returns>lista de tipo OrdenesPago por DenunciaBien con la información consultada</returns>
        public List<OrdenesPago> ConsultarOrdenesPagoDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.OrdenesPagoDAL.ConsultarOrdenesPagoParticipacionById(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de pagos por IdDenuncia y IdOrdenPago 
        /// </summary>
        /// <param name="IdOrdenPago">id de la Orden de pago</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionPorIdOrdenPago(int IdOrdenPago)
        {
            try
            {
                return this.OrdenesPagoDAL.ConsultarOrdenesPagoParticipacionPorIdOrdenPago(IdOrdenPago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los registros de la tabla Ordenes Pago por IdDenuncia
        /// </summary>
        /// <param name="IdDenuncia">id de la Orden de la denuncia</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarOrdenesPagoParticipacionPorIdDenuncia(int IdDenuncia)
        {
            try
            {
                return this.OrdenesPagoDAL.ConsultarOrdenesPagoParticipacionPorIdDenuncia(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de pagos por IdDenuncia y número de comprobante de pago 
        /// </summary>
        /// <param name="IdDenunciaBien">id de la denuncia</param>
        /// /// <param name="NoComprobantePago">Número de comprobante de pago</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarExistenciaOrdenDePagoXComprobantePago(int IdDenunciaBien, decimal NoComprobantePago)
        {
            try
            {
                return this.OrdenesPagoDAL.ConsultarExistenciaOrdenDePagoXComprobantePago(IdDenunciaBien, NoComprobantePago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de pagos por IdDenuncia y número de resolución
        /// </summary>
        /// <param name="IdDenunciaBien">id de la denuncia</param>
        /// /// <param name="NumeroDeResolucion">Número de resolución de pago</param>
        /// <returns>Retorna la lista con el objeto de la denuncia consultada </returns>
        public List<OrdenesPago> ConsultarExistenciaOrdenDePagoXNumeroDeResolucion(int IdDenunciaBien, decimal NumeroDeResolucion)
        {
            try
            {
                return this.OrdenesPagoDAL.ConsultarExistenciaOrdenDePagoXNumeroDeResolucion(IdDenunciaBien, NumeroDeResolucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro en la tabla OrdenesPago
        /// </summary>
        /// <param name="pOrdenDePago"> variable de tipo OrdenesPago con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarOrdenesDePago(OrdenesPago pOrdenDePago)
        {
            try
            {
                return this.OrdenesPagoDAL.InsertarOrdenesDePago(pOrdenDePago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza un nuevo registro en la tabla OrdenesPago
        /// </summary>
        /// <param name="pOrdenDePago"> variable de tipo OrdenesPago con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int ActualizarOrdenesDePago(OrdenesPago pOrdenDePago)
        {
            try
            {
                return this.OrdenesPagoDAL.ActualizarOrdenesDePago(pOrdenDePago);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
