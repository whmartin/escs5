﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo GN_VDIPBLL.
    /// </summary>
    public class GN_VDIPBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private GN_VDIPDAL vGN_VDIPDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public GN_VDIPBLL()
        {
            this.vGN_VDIPDAL = new GN_VDIPDAL();
        }

        /// <summary>
        /// Consulta los departamentos de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        public List<GN_VDIP> ConsultarDepartamentos()
        {
            try
            {
                return this.vGN_VDIPDAL.ConsultarDepartamentos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los municipios de un departamento de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        /// <param name="pCodMunicipio">id del departamento</param>
        public List<GN_VDIP> ConsultarMunicipios(string pCodDepto)
        {
            try
            {
                return this.vGN_VDIPDAL.ConsultarMunicipios(pCodDepto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los departamentos de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        public List<GN_VDIP> ConsultarDepartamentosSeven()
        {
            try
            {
                return this.vGN_VDIPDAL.ConsultarDepartamentosSeven();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los municipios de un departamento de Colombia
        /// </summary>
        /// <returns>lista de municipios</returns>
        /// <param name="pCodMunicipio">id del departamento</param>
        public List<GN_VDIP> ConsultarMunicipiosSeven(string pCodDepto)
        {
            try
            {
                return this.vGN_VDIPDAL.ConsultarMunicipiosSeven(pCodDepto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        
    }
}
