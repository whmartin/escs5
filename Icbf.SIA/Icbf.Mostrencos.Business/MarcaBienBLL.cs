﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class MarcaBienBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private MarcaBienDAL vMarcaBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public MarcaBienBLL()
        {
            this.vMarcaBienDAL = new MarcaBienDAL();
        }

        /// <summary>
        /// consulta los MarcaBien
        /// </summary>
        /// <returns>LISTA DE MarcaBien</returns>
        public List<MarcaBien> ConsultarMarcaBien()
        {
            try
            {
                return this.vMarcaBienDAL.ConsultarMarcaBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
