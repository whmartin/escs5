//-----------------------------------------------------------------------
// <copyright file="ConfiguracionTiemposEjecucionProcesoBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ConfiguracionTiemposEjecucionProcesoBLL.</summary>
// <author>Ingenian Software</author>
// <date>09/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class ConfiguracionTiemposEjecucionProcesoBLL
    {
        private ConfiguracionTiemposEjecucionProcesoDAL vConfiguracionTiemposEjecucionProcesoDAL;

        public ConfiguracionTiemposEjecucionProcesoBLL()
        {
            vConfiguracionTiemposEjecucionProcesoDAL = new ConfiguracionTiemposEjecucionProcesoDAL();
        }

        /// <summary>
        /// M�todo que inserta ConfiguracionTiemposEjecucionProceso
        /// </summary>
        /// <param name="pConfiguracionTiemposEjecucionProceso">Entidad ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int InsertarConfiguracionTiemposEjecucionProceso(ConfiguracionTiemposEjecucionProceso pConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoDAL.InsertarConfiguracionTiemposEjecucionProceso(pConfiguracionTiemposEjecucionProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que actualiza ConfiguracionTiemposEjecucionProceso
        /// </summary>
        /// <param name="pConfiguracionTiemposEjecucionProceso">Entidad ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int ModificarConfiguracionTiemposEjecucionProceso(ConfiguracionTiemposEjecucionProceso pConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoDAL.ModificarConfiguracionTiemposEjecucionProceso(pConfiguracionTiemposEjecucionProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta la ConfiguracionTiemposEjecucionProceso x Id
        /// </summary>
        /// <param name="pIdConfiguracionTiemposEjecucionProceso">Id de ConfiguracionTiemposEjecucionProceso</param>
        /// <returns>Entidad ConfiguracionTiemposEjecucionProceso</returns>
        public ConfiguracionTiemposEjecucionProceso ConsultarConfiguracionTiemposEjecucionProceso(int pIdConfiguracionTiemposEjecucionProceso)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoDAL.ConsultarConfiguracionTiemposEjecucionProceso(pIdConfiguracionTiemposEjecucionProceso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta  los registros de ConfiguracionTiemposEjecucionProceso x Filtros
        /// </summary>
        /// <param name="pIdFase">Id de la Fase</param>
        /// <param name="pIdActuacion">Id de la actuaci�n</param>
        /// <param name="pIdAccion">Id de la Acci�n</param>
        /// <param name="pEstado">Id del estado</param>
        /// <returns>Lista de Resultados</returns>
        public List<ConfiguracionTiemposEjecucionProceso> ConsultarListaConfiguracionTiemposEjecucionProceso(string pFase, string pActuacion, string pAccion, byte? pEstado)
        {
            try
            {
                return vConfiguracionTiemposEjecucionProcesoDAL.ConsultarListaConfiguracionTiemposEjecucionProceso(pFase, pActuacion, pAccion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
