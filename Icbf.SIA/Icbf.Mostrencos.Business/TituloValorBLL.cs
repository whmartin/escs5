﻿//-----------------------------------------------------------------------
// <copyright file="MuebleInmuebleBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase MuebleInmuebleBLL.</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TituloValorBLL.
    /// </summary>
    public class TituloValorBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private TituloValorDAL vTituloValorDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public TituloValorBLL()
        {
            this.vTituloValorDAL = new TituloValorDAL();
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public List<TituloValor> ConsultarTituloValorPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vTituloValorDAL.ConsultarTituloValorPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public TituloValor ConsultarTituloValorPorIdTituloValor(int pIdTituloValor)
        {
            try
            {
                return this.vTituloValorDAL.ConsultarTituloValorPorIdTituloValor(pIdTituloValor);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Inserta la información de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Variable de tipo TituloValor que contiene la información que se va a insertar</param>
        /// <returns>Id del TituloValor insertado</returns>
        public int InsertarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                return this.vTituloValorDAL.InsertarTituloValor(pTituloValor);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// EdIta la información de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Variable de tipo TituloValor que contiene la información que se va a insertar</param>
        /// <returns>Numero de filas afectadas</returns>
        public int EditarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                return this.vTituloValorDAL.EditarTituloValor(pTituloValor);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// consulta los TituloValor por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de TituloValor</returns>
        public List<TituloValor> ConsultarTituloValor(int pIdDenunciaBien)
        {
            try
            {
                return this.vTituloValorDAL.ConsultarTituloValor(pIdDenunciaBien);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        #region CU 154

        /// <summary>
        /// Edita los campos UsoBien y IdInformacionVenta de un TituloValor
        /// </summary>
        /// <param name="pTituloValor">Entidad a modificar</param>
        /// <returns>Resultado de la Operación</returns>
        public int ModificarTituloValor(TituloValor pTituloValor)
        {
            try
            {
                return this.vTituloValorDAL.ModificarTituloValor(pTituloValor);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        #endregion
    }
}
