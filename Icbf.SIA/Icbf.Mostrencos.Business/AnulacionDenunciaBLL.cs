﻿using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo Anulacion denuncia.
    /// </summary>
    public class AnulacionDenunciaBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo AnulacionDenunciaDAL
        /// </summary>
        private AnulacionDenunciaDAL vAnulacionDenunciaDal;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AnulacionDenunciaDAL"/>.
        /// </summary>
        public AnulacionDenunciaBLL()
        {
            this.vAnulacionDenunciaDal = new AnulacionDenunciaDAL();
        }

        /// <summary>
        /// Consulta el Histórico Anulación Denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien"></param>
        /// <returns></returns>
        public List<AnulacionDenuncia> ConsultarHistoricoAnulacionDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vAnulacionDenunciaDal.ConsultarHistoricoAnulacionDenunciaPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un registro de una solicitud de anulacion
        /// </summary>
        /// <param name="pAnulacionDenuncia"></param>
        /// <returns></returns>
        public int InsertarSolicitudAnulacion(AnulacionDenuncia pAnulacionDenuncia)
        {
            try
            {
                return this.vAnulacionDenunciaDal.InsertarSolicitudAnulacion(pAnulacionDenuncia);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta un registro de una aprobacion de anulacion
        /// </summary>
        /// <param name="pAnulacionDenuncia"></param>
        /// <returns></returns>
        public int InsertarAprobacionAnulacion(AnulacionDenuncia pAnulacionDenuncia)
        {
            try
            {
                return this.vAnulacionDenunciaDal.InsertarAprobacionAnulacion(pAnulacionDenuncia);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Consultar el nombre y correo del abogado solicitante
        /// </summary>
        /// <param name="vIdDenunciaBien"></param>
        /// <returns></returns>
        public AnulacionDenuncia ConsultarAbogadoSolicitudAnulacion(int vIdDenunciaBien)
        {
            try
            {
                return this.vAnulacionDenunciaDal.ConsultarAbogadoSolicitudAnulacion(vIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AnulacionDenuncia ConsultarCoordinadorJuridicoSolicitudAnulacion(int vIdDenunciaBien, string vkeys)
        {
            try
            {
                return this.vAnulacionDenunciaDal.ConsultarCoordinadorJuridicoSolicitudAnulacion(vIdDenunciaBien, vkeys);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
