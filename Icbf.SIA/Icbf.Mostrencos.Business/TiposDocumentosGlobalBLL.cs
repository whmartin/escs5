﻿//-----------------------------------------------------------------------
// <copyright file="TiposDocumentosGlobalBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TiposDocumentosGlobalBLL.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltrán]</author>
// <date>18/03/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase TiposDocumentosGlobalBLL
    /// </summary>
    public class TiposDocumentosGlobalBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TiposDocumentosGlobalDAL
        /// </summary>
        private TiposDocumentosGlobalDAL vTiposDocumentosGlobalDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TiposDocumentosGlobalBLL"/>.
        /// </summary>
        public TiposDocumentosGlobalBLL()
        {
            this.vTiposDocumentosGlobalDAL = new TiposDocumentosGlobalDAL();
        }

        /// <summary>
        /// Consultar Tipo Identificación.
        /// </summary>
        /// <param name="pTiposDocumentos">Id del tipo de identificación a consultar</param>
        /// <returns>Lista resultado de la operación</returns>
        public List<TiposDocumentosGlobal> ConsultarTiposIdentificacionGlobal(string pTiposDocumentos)
        {
            try
            {
                return this.vTiposDocumentosGlobalDAL.ConsultarTiposIdentificacionGlobal(pTiposDocumentos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Identificación por el id.
        /// </summary>
        /// <param name="pIdTiposDocumentos">Id del tipo de identificación a consultar</param>
        /// <returns>resultado de la operación</returns>
        public TiposDocumentosGlobal ConsultarTiposIdentificacionGlobalPorId(int pIdTiposDocumentos)
        {
            try
            {
                return this.vTiposDocumentosGlobalDAL.ConsultarTiposIdentificacionGlobalPorId(pIdTiposDocumentos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
