﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class ClaseEntradaBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private ClaseEntradaDAL vClaseEntradaDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public ClaseEntradaBLL()
        {
            this.vClaseEntradaDAL = new ClaseEntradaDAL();
        }

        /// <summary>
        /// consulta los ClaseEntrada
        /// </summary>
        /// <returns>LISTA DE ClaseEntrada</returns>
        public List<ClaseEntrada> ConsultarClaseEntrada()
        {
            try
            {
                return this.vClaseEntradaDAL.ConsultarClaseEntrada();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
