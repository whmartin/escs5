using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class ReporteDenunciasBLL
    {
        private ReporteDenunciasDAL vReporteDenunciasDAL;
        public ReporteDenunciasBLL()
        {
            vReporteDenunciasDAL = new ReporteDenunciasDAL();
        }
        public int InsertarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                return vReporteDenunciasDAL.InsertarReporteDenuncias(pReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                return vReporteDenunciasDAL.ModificarReporteDenuncias(pReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarReporteDenuncias(ReporteDenuncias pReporteDenuncias)
        {
            try
            {
                return vReporteDenunciasDAL.EliminarReporteDenuncias(pReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ReporteDenuncias ConsultarReporteDenuncias(int pIdReporteDenuncias)
        {
            try
            {
                return vReporteDenunciasDAL.ConsultarReporteDenuncias(pIdReporteDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ConsultarReporteDenunciass(ReporteDenunciaFiltros pFiltros)
        {
            try
            {
                return vReporteDenunciasDAL.ConsultarReporteDenunciass(pFiltros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarTipoPersonaAsociacion()
        {
            try
            {
                return vReporteDenunciasDAL.ConsultarTipoPersonaAsociacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarTipoDeIdentificacion()
        {
            try
            {
                return vReporteDenunciasDAL.ConsultarTipoDeIdentificacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarEstadoDenuncia()
        {
            try
            {
                return vReporteDenunciasDAL.ConsultarEstadoDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ReporteDenuncias> ConsultarDepartamentoUbicacion()
        {
            try
            {
                return vReporteDenunciasDAL.ConsultarDepartamentoUbicacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
