﻿
namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo ContinuarOficioBLL.
    /// </summary>
    public class ContinuarOficioBLL
    {
        /// <summary>
        /// Variable de tipo vContinuarOficioDAL
        /// </summary>
        private ContinuarOficioDAL vContinuarOficioDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="ContinuarOficioBLL"/>.
        /// </summary>
        public ContinuarOficioBLL()
        {
            this.vContinuarOficioDAL = new ContinuarOficioDAL();
        }

        //public List<ContinuarOficio> ConsultarContinuarOficiosXIdOficio(int pIdContinuarOficio)
        //{
        //    try
        //    {
        //        return vContinuarOficioDAL.ConsultarContinuarOficiosXIdOficio(pIdContinuarOficio);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        //public List<ContinuarOficio> ConsultarContinuarOficiosXIdDenunciaBien(int pIdDenunciaBien)
        //{
        //    try
        //    {
        //        return vContinuarOficioDAL.ConsultarContinuarOficiosXIdDenunciaBien(pIdDenunciaBien);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        public List<ContinuarOficio> ConsultarContinuarOficios(int? pIdDenunciaBien, int? pIdContinuarOficio)
        {
            try
            {
                return vContinuarOficioDAL.ConsultarContinuarOficios(pIdDenunciaBien, pIdContinuarOficio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarContinuarOficio(ContinuarOficio pContinuarOficio)
        {
            try
            {
                return vContinuarOficioDAL.InsertarContinuarOficio(pContinuarOficio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarContinuarOficio(ContinuarOficio pContinuarOficio)
        {
            try
            {
                return vContinuarOficioDAL.ModificarContinuarOficio(pContinuarOficio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



    }
}
