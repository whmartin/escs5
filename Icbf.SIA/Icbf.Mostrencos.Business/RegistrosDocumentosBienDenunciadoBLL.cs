﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class RegistrosDocumentosBienDenunciadoBLL
    {
        private RegistrosDocumentosBienDenunciadoDAL vRegistrosDocumentosBienDenunciadoDAL;
        public RegistrosDocumentosBienDenunciadoBLL()
        {
            vRegistrosDocumentosBienDenunciadoDAL = new RegistrosDocumentosBienDenunciadoDAL();
        }

        public List<RegistroDenuncia> ConsultarRegistroDocumentosBienDenunciado(int pIdRegional, int pEstado, string pRegistradoDesde, string pRegistradoHasta, string pRadicadoDenuncia, int pIdUsuario)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ConsultarRegistroDocumentosBienDenunciado(pIdRegional, pEstado, pRegistradoDesde, pRegistradoHasta, pRadicadoDenuncia, pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Regional ConsultarRegionalDelAbogado(int pIdRegional)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ConsultarRegionalDelAbogado(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroDenuncia ConsultarDenunciaBienXId(int pIdDenuncia)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ConsultarDenunciaxId(pIdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosXIdDenuncia(int pIdDenuncia, int pIdEstado)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ConsultarDocumentosXIdDenuncia(pIdDenuncia, pIdEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBienObtieneId(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien, ref int IdDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.InsertarDocumentosSolicitadosDenunciaBienObtieneId(pDocumentosSolicitadosDenunciaBien, ref IdDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.InsertarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBienOtros(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.InsertarDocumentosSolicitadosDenunciaBienOtros(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ModificarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ActualizarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ActualizarEstadoDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDocumentosSolicitadosDenunciaBienRecibidos(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ModificarDocumentosSolicitadosDenunciaBienRecibidos(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarDenunciaDocumentosSolicitadosDenunciaBien(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return vRegistrosDocumentosBienDenunciadoDAL.ModificarDenunciaDocumentosSolicitadosDenunciaBien(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina los tipos de documentos Otros
        /// </summary>
        /// <param name="pRegistroDenuncia">Parametro que envía el IDDenunica y TipoDocumento</param>
        /// <returns>correos</returns>
        public int EliminarDenunciaDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pRegistroDenuncia)
        {
            try
            {
                return this.vRegistrosDocumentosBienDenunciadoDAL.EliminarDenunciaDocumentosSolicitadosDenunciaBien(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
