﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo DocumentosSolicitadosDenunciaBienBLL.
    /// </summary>
    public class DocumentosSolicitadosDenunciaBienBLL
    {
        /// <summary>
        /// Variable de tipo DocumentosSolicitadosDenunciaBienDAL
        /// </summary>
        private DocumentosSolicitadosDenunciaBienDAL vDocumentosSolicitadosDenunciaBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DocumentosSolicitadosDenunciaBienBLL"/>.
        /// </summary>
        public DocumentosSolicitadosDenunciaBienBLL()
        {
            this.vDocumentosSolicitadosDenunciaBienDAL = new DocumentosSolicitadosDenunciaBienDAL();
        }

        /// <summary>
        /// Inserta un registro en la tabla DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.InsertarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un registro de la tabla  DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EditarDocumentosSolicitadosDenunciaBien(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un registro de la tabla  DocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EditarDocumentosSolicitadosYRecibidosDenunciaBien(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.EditarDocumentosSolicitadosYRecibidosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pIdDocumentosSolicitadosDenunciaBien">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public DocumentosSolicitadosDenunciaBien ConsultarDocumentosSolicitadosDenunciaBienPorId(int pIdDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.ConsultarDocumentosSolicitadosDenunciaBienPorId(pIdDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdCausante
        /// </summary>
        /// <param name="pIdCausante">id del registro a consulta</param>
        /// <returns>lista de tipo DocumentosSolicitadosDenunciaBien con la información consultada</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(int pIdCausante)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.ConsultarDocumentosSolicitadosDenunciaBienPorIdCausante(pIdCausante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdApoderado
        /// </summary>
        /// <param name="pIdCausante">id del registro a consulta</param>
        /// <returns>lista de tipo DocumentosSolicitadosDenunciaBien con la información consultada</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(int pIdApoderado)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.ConsultarDocumentosSolicitadosDenunciaBienPorIdApoderado(pIdApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.ConsultarDocumentosSolicitadosDenunciaBienPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de la tabla DocumentosSolicitadosDenunciaBien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdCalidadDenunciante">id del registro a consultar</param>
        /// <returns>variable de tipo DocumentosSolicitadosDenunciaBien con la información consultada </returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(int pIdCalidadDenunciante)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.ConsultarDocumentosSolicitadosDenunciaBienPorIdCalidadDenunciante(pIdCalidadDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta los datos de la documentación recibida del causante
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int InsertarDocumentoRecibidoCausante(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosDenunciaBienDAL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina la información de documentación recibida del causante
        /// </summary>
        /// <param name="pDocumentosSolicitadosDenunciaBien">Variable de tipo DocumentosSolicitadosDenunciaBien con la información que se va a modificar</param>
        /// <returns>1 si la modificación es exitosa</returns>
        public int EliminarDocumentoRecibidoCausante(DocumentosSolicitadosDenunciaBien pDocumentosSolicitadosDenunciaBien)
        {
            try
            {
                pDocumentosSolicitadosDenunciaBien.FechaRecibido = null;
                pDocumentosSolicitadosDenunciaBien.ObservacionesDocumentacionRecibida = null;
                pDocumentosSolicitadosDenunciaBien.NombreArchivo = null;
                pDocumentosSolicitadosDenunciaBien.RutaArchivo = null;
                return this.vDocumentosSolicitadosDenunciaBienDAL.EditarDocumentosSolicitadosDenunciaBien(pDocumentosSolicitadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
