﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using System.Collections.Generic;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TerceroBLL.
    /// </summary>
    public class TerceroBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TerceroDAL
        /// </summary>
        private TerceroDAL vTerceroDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TerceroBLL"/>.
        /// </summary>
        public TerceroBLL()
        {
            this.vTerceroDAL = new TerceroDAL();
        }

        /// <summary>
        /// Consulta un tercero por el id tercero
        /// </summary>
        /// <param name="pIdTercero">id del tercero que se va a consultar</param>
        /// <returns>Variable de tipo Tercero con la información consultada</returns>
        public Tercero ConsultarTerceroXId(int pIdTercero)
        {
            try
            {
                return this.vTerceroDAL.ConsultarTercerolXId(pIdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un tercero por el tipo de identificación , el número de identificación y el nombre del tipo de persona
        /// </summary>
        /// <param name="pIdTipoIdentificacion"> id del tipo de identificación</param>
        /// <param name="pNumeroIdentificacion">Número de identificación</param>
        /// <param name="pNombreTipoPersona">Nombre del tipo de persona</param>
        /// <returns>Variable de tipo Tercero con la información resultante de la consulta</returns>
        public Tercero ConsultarTercero(int pIdTipoIdentificacion, string pNumeroIdentificacion, string pNombreTipoPersona)
        {
            try
            {
                return this.vTerceroDAL.ConsultarTercero(pIdTipoIdentificacion, pNumeroIdentificacion, pNombreTipoPersona);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta un tercero por el id tercero
        /// </summary>
        /// <param name="pIdTercero">id del tercero que se va a consultar</param>
        /// <returns>Variable de tipo Tercero con la información consultada</returns>
        public List<Tercero> ConsultarTerceroTodos()
        {
            try
            {
                return this.vTerceroDAL.ConsultarTercerolTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta del tercero por Tipo Juridico y Razon Social
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public List<Tercero> ConsultarJuridicoTercerosPorNombre(string nombre)
        {
            try
            {
                return this.vTerceroDAL.ConsultarJuridicoTercerosPorNombre(nombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Tipo de Persona por IdTercero
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public Tercero ConsultarTipoEntidadPorIdTercero(int IdTercero)
        {
            try
            {
                return this.vTerceroDAL.ConsultarTipoEntidadPorIdTercero(IdTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
