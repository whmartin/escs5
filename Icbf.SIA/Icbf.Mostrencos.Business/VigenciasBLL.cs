﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class VigenciasBLL
    {
        private VigenciasDAL vVigenciasDAL;
        public VigenciasBLL()
        {
            vVigenciasDAL = new VigenciasDAL();
        }

        
        public List<Vigencias> ConsultarVigenciasAll()
        {
            try
            {
                return this.vVigenciasDAL.ConsultarVigenciasAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

