﻿//-----------------------------------------------------------------------
// <copyright file="filtrosMostrencosBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase filtrosMostrencosBLL.</summary>
// <author>INGENIAN</author>
// <date>01/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TipoBienBLL.
    /// </summary>
    public class FiltrosMostrencosBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TipoBienDAL
        /// </summary>
        private FiltrosMostrencosDAL vfiltrosMostrencosDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TipoBienBLL"/>.
        /// </summary>
        public FiltrosMostrencosBLL()
        {
            this.vfiltrosMostrencosDAL = new FiltrosMostrencosDAL();
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<ResultFiltroMueble> filtroMueble(FiltroMueble filtroMueble)
        {
            try
            {
                return this.vfiltrosMostrencosDAL.filtroMueble(filtroMueble);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public void Sino(bool ValorSiNo, int vIdDenunciaBien)
        {
            try
            {
                this.vfiltrosMostrencosDAL.Sino(ValorSiNo, vIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<ResultFiltroMueble> BusquedaMueble(string vInStringRadicado)
        {
            try
            {
                return this.vfiltrosMostrencosDAL.BusquedaMuebleInmueble(vInStringRadicado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<ResultFiltroTitulo> BusquedaTitulo(string vInStringRadicado)
        {
            try
            {
                return this.vfiltrosMostrencosDAL.BusquedaTitulo(vInStringRadicado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<ResultFiltroTitulo> filtroTitulo(FiltroTitulo vfiltroTitulo)
        {
            try
            {
                return this.vfiltrosMostrencosDAL.filtroTitulo(vfiltroTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<ResultFiltroDenunciante> filtroDenunciante(FiltroDenunciante vfiltroDenunciante)
        {
            try
            {
                return this.vfiltrosMostrencosDAL.filtroDenunciante(vfiltroDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<ResultFiltroDenunciante> filtroCausante(FiltroDenunciante vfiltroDenunciante)
        {
            try
            {
                return this.vfiltrosMostrencosDAL.filtroCausante(vfiltroDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}