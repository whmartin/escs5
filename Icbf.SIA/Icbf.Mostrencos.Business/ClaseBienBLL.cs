﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class ClaseBienBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private ClaseBienDAL vClaseBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public ClaseBienBLL()
        {
            this.vClaseBienDAL = new ClaseBienDAL();
        }

        /// <summary>
        /// consulta los ClaseBien
        /// </summary>
        /// <returns>LISTA DE ClaseBien</returns>
        public List<ClaseBien> ConsultarClaseBien(string pIdSubTipoBien)
        {
            try
            {
                return this.vClaseBienDAL.ConsultarClaseBien(pIdSubTipoBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
