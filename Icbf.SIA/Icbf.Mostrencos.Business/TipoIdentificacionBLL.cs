//-----------------------------------------------------------------------
// <copyright file="TipoIdentificacionBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoIdentificacionBLL.</summary>
// <author>INGENIAN SOFTWARE[Dar�o Beltr�n]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase TipoIdentificacionBLL
    /// </summary>
    public class TipoIdentificacionBLL
    {
        /// <summary>
        /// The vTipoIdentificacionDAL
        /// </summary>
        private TipoIdentificacionDAL vTipoIdentificacionDAL;

        /// <summary>
        /// Initializes a new instance of the <see cref="TipoIdentificacionBLL"/> class.
        /// </summary>
        public TipoIdentificacionBLL()
        {
            this.vTipoIdentificacionDAL = new TipoIdentificacionDAL();
        }

        /// <summary>
        /// Consultar Tipo Identificacion.
        /// </summary>
        /// <returns>Lista resultado de la operaci�n</returns>
        public List<TipoIdentificacion> ConsultarTipoIdentificacion()
        {
            try
            {
                return this.vTipoIdentificacionDAL.ConsultarTipoIdentificacion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
