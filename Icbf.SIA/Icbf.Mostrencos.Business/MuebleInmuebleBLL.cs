﻿//-----------------------------------------------------------------------
// <copyright file="MuebleInmuebleBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase MuebleInmuebleBLL.</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo MuebleInmuebleBLL.
    /// </summary>
    public class MuebleInmuebleBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private MuebleInmuebleDAL vMuebleInmuebleDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public MuebleInmuebleBLL()
        {
            this.vMuebleInmuebleDAL = new MuebleInmuebleDAL();
        }

        /// <summary>
        /// consulta los MuebleInmueble por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de MuebleInmueble</returns>
        public List<MuebleInmueble> ConsultarMuebleInmueblePorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ConsultarMuebleInmueblePorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// consulta los MuebleInmueble por el Id
        /// </summary>
        /// <param name="pIdMuebleInmueble">id que se va a consultar</param>
        /// <returns>MuebleInmueble</returns>
        public MuebleInmueble ConsultarMuebleInmueblePorId(int pIdMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ConsultarMuebleInmueblePorId(pIdMuebleInmueble);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// consulta la informacion que se va a cargar en los campos ocultos
        /// </summary>
        /// <param name="pIdSubTipoBien">Valos que se va a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public string ConsultarCamposOcultos(string pIdSubTipoBien)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ConsultarCamposOcultos(pIdSubTipoBien);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// valida si el bien denunciado ya existe
        /// </summary>
        /// <param name="pIddenunciaBien">id de la denuncia</param>
        /// <param name="pBienDenunciado">tipo del bien denunciado</param>
        /// <param name="pDato">identificador del bien denunciado</param>
        /// <param name="pIdBienDenunciado">id del bien denunciado</param>
        /// <returns>thue si existe o false en caso contrario</returns>
        public bool ValidarExistenciaBienDenunciado(int pIddenunciaBien, string pBienDenunciado, string pDato, int pIdBienDenunciado)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ValidarExistenciaBienDenunciado(pIddenunciaBien, pBienDenunciado, pDato, pIdBienDenunciado);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// valida si el bien denunciado ya existe en otras regionales
        /// </summary>
        /// <param name="pIddenunciaBien">id de la denuncia</param>
        /// <param name="pBienDenunciado">tipo del bien denunciado</param>
        /// <param name="pDato">identificador del bien denunciado</param>
        /// <returns>thue si existe o false en caso contrario</returns>
        public string ValidarExistenciaBienDenunciadoOtrasRegionales(int pIddenunciaBien, string pBienDenunciado, string pDato)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ValidarExistenciaBienDenunciadoOtrasRegionales(pIddenunciaBien, pBienDenunciado, pDato);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Inserta la información de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Variable de tipo MuebleInmueble que contiene la información que se va a insertar</param>
        /// <returns>Id del MuebleInmueble insertado</returns>
        public int InsertarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleDAL.InsertarMuebleInmueble(pMuebleInmueble);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Edita la información de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Variable de tipo MuebleInmueble que contiene la información que se va a insertar</param>
        /// <returns>numero de registros modificados</returns>
        public int EditarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleDAL.EditarMuebleInmueble(pMuebleInmueble);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// consulta los MuebleInmueble por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>lista de MuebleInmueble</returns>
        public List<MuebleInmueble> ConsultarMuebleInmueble(int pIdDenunciaBien)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ConsultarMuebleInmueble(pIdDenunciaBien);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        #region CU 154

        /// <summary>
        /// Edita los campos UsoBien y IdInformacionVenta de un MuebleInmueble
        /// </summary>
        /// <param name="pMuebleInmueble">Entidad a modificar</param>
        /// <returns>Resultado de la Operación</returns>
        public int ModificarMuebleInmueble(MuebleInmueble pMuebleInmueble)
        {
            try
            {
                return this.vMuebleInmuebleDAL.ModificarMuebleInmueble(pMuebleInmueble);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        #endregion
    }
}
