﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class RegistroCalidadDenuncianteBLL
    {
        private RegistroCalidadDenuncianteDAL vRegistroCalidadDenuncianteDAL;
        public RegistroCalidadDenuncianteBLL()
        {
            vRegistroCalidadDenuncianteDAL = new RegistroCalidadDenuncianteDAL();
        }

        public int InsertarCalidadDenunciante(RegistroCalidadDenunciante vregistroCalidadDenunciante)
        {
            try
            {
                return vRegistroCalidadDenuncianteDAL.InsertarCalidadDenunciante(vregistroCalidadDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarCalidadDenunciante(RegistroCalidadDenunciante vregistroCalidadDenunciante)
        {
            try
            {
                return vRegistroCalidadDenuncianteDAL.ModificarCalidadDenunciante(vregistroCalidadDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroCalidadDenunciante> ConsultarCalidadDenunciante(int pIdDenunciaBien)
        {
            try
            {
                return vRegistroCalidadDenuncianteDAL.ConsultarCalidadDenunciante(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DenunciaDatosEntidad> BuscarDatosEntidadXNombre(string pNombreRazonSocial, int pIdDenunciaBien)
        {
            try
            {
                return vRegistroCalidadDenuncianteDAL.BuscarDatosEntidadXNombre(pNombreRazonSocial, pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
