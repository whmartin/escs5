﻿//-----------------------------------------------------------------------
// <copyright file="NotariasBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase NotariasBLL.</summary>
// <author>INGENIAN</author>
// <date>28/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo RegionalBLL.
    /// </summary>
    public class NotariasBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo vNotariasDAL
        /// </summary>
        private NotariasDAL vNotariasDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="NotariasBLL"/>.
        /// </summary>
        public NotariasBLL()
        {
            this.vNotariasDAL = new NotariasDAL();
        }

        /// <summary>
        /// Consulta Todas las Notarias
        /// </summary>
        /// <returns> List<Notarias> con la informacion solicitada</returns>
        public List<Notarias> ConsultarNotarias()
        {
            try
            {
                return this.vNotariasDAL.ConsultarNotarias();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
