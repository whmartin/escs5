﻿//-----------------------------------------------------------------------
// <copyright file="TramiteNotarialBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteNotarialBLL.</summary>
// <author>INGENIAN</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using System.Collections.Generic;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TipoBienBLL.
    /// </summary>
    public class TramiteNotarialBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TipoBienDAL
        /// </summary>
        private TramiteNotarialDAL vTramiteNotarialDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TramiteNotarialBLL"/>.
        /// </summary>
        public TramiteNotarialBLL()
        {
            this.vTramiteNotarialDAL = new TramiteNotarialDAL();
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<TramiteNotarial> ObtenerNotarias()
        {
            try
            {
                return this.vTramiteNotarialDAL.ObtenerNotarias();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Notarial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public int InsertarTramiteNotarial(TramiteNotarial vTramiteNotarial)
        {
            try
            {
                return this.vTramiteNotarialDAL.InsertarTramiteNotarial(vTramiteNotarial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Notarial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public int InsertarTramiteNotarialAceptado(TramiteNotarialAceptado vTramiteNotarialAceptado)
        {
            try
            {
                return this.vTramiteNotarialDAL.InsertarTramiteNotarialAceptado(vTramiteNotarialAceptado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Notarial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public int UpdateTramiteNotarial(TramiteNotarial vTramiteNotarial)
        {
            try
            {
                return this.vTramiteNotarialDAL.UpdateTramiteNotarial(vTramiteNotarial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Notarial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public bool validarTramiteNotarial(int IdDenuncia, string Notaria)
        {
            try
            {
                return this.vTramiteNotarialDAL.validarTramiteNotarial(IdDenuncia, Notaria);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Notarial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public TramiteNotarial ConsultarTramiteNotarial(int IdDenuncia)
        {
            try
            {
                return this.vTramiteNotarialDAL.ConsultarTramiteNotarial(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Notarial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public TramiteNotarialAceptado ConsultarTramiteNotarialAceptado(int IdDenuncia)
        {
            try
            {
                return this.vTramiteNotarialDAL.ConsultarTramiteNotarialAceptado(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}