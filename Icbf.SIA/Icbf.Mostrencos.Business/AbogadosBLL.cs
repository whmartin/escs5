﻿
namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AbogadosBLL.
    /// </summary>
    public class AbogadosBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private AbogadosDAL vAbogadosDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public AbogadosBLL()
        {
            this.vAbogadosDAL = new AbogadosDAL();
        }

        /// <summary>
        /// consulta los abogados por el IdAbogado
        /// </summary>
        /// <param name="pIdAbogado">Id del abogado a consultar</param>
        /// <returns>Lista de tipo Abogados con el resultado de la consulta</returns>
        public Abogados ConsultarAbogadoXId(int pIdAbogado)
        {
            try
            {
                return this.vAbogadosDAL.ConsultarAbogadoXId(pIdAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información del abogado por el id de la regional a la que esta asignado
        /// </summary>
        /// <param name="pIdRegional">id de la regional que se va a consultar</param>
        /// <returns>Lista de tipo Abogados con la información consultada</returns>
        public List<Abogados> ConsultarAbogadoXIdRegional(int pIdRegional)
        {
            try
            {
                return this.vAbogadosDAL.ConsultarAbogadoXIdRegional(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información del abogado por el IdUsuario
        /// </summary>
        /// <param name="pIdUsuario">Id usuario que se va a consultar</param>
        /// <returns>Variable de tipo Abogados con la información consultada</returns>
        public Abogados ConsultarAbogadosPorIdUsuario(int pIdUsuario)
        {
            try
            {
                return this.vAbogadosDAL.ConsultarAbogadosPorIdUsuario(pIdUsuario);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta la información de un nuevo abogado
        /// </summary>
        /// <param name="pAbogados">Variable de tipoAbogados que contiene la información que se va a insertar</param>
        /// <returns> id del abogado insertado</returns>
        public int InsetrarAbogados(Abogados pAbogados)
        {
            try
            {
                return this.vAbogadosDAL.InsetrarAbogados(pAbogados);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los abogados de la base de datos SIA
        /// </summary>
        /// <param name="pConsulta">variable con la información a consultar</param>
        /// <returns>resultado de la consulta</returns>
        public List<AsignacionAbogado> ConsultarAbogadosSoloEnSIA(AsignacionAbogado pConsulta)
        {
            try
            {
                return this.vAbogadosDAL.ConsultarAbogadosSoloEnSIA(pConsulta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
