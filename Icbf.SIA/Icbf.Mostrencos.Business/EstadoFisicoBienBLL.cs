﻿//-----------------------------------------------------------------------
// <copyright file="EstadoFisicoBienBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase EstadoFisicoBienBLL.</summary>
// <author>INDESAP[Jesus Eduardo Cortes]</author>
// <date>06/01/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class EstadoFisicoBienBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private EstadoFisicoBienDAL vEstadoFisicoBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public EstadoFisicoBienBLL()
        {
            this.vEstadoFisicoBienDAL = new EstadoFisicoBienDAL();
        }

        /// <summary>
        /// consulta los EstadoFisicoBien
        /// </summary>
        /// <returns>LISTA DE EstadoFisicoBien</returns>
        public List<EstadoFisicoBien> ConsultarEstadoFisicoBien()
        {
            try
            {
                return this.vEstadoFisicoBienDAL.ConsultarEstadoFisicoBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
