﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo MedioComunicacionBLL.
    /// </summary>
    public class MedioComunicacionBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo MedioComunicacionDAL
        /// </summary>
        private MedioComunicacionDAL vMedioComunicacionDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="MedioComunicacionBLL"/>.
        /// </summary>
        public MedioComunicacionBLL()
        {
            vMedioComunicacionDAL = new MedioComunicacionDAL();
        }

        /// <summary>
        /// Consultar Medios de Comunicación.
        /// </summary>
        /// <returns>Lista de medios de comunicación filtrados por nombre o estado</returns>
        public List<MedioComunicacion> ConsultarMedioComunicacion(string pNombre, bool? pEstado)
        {
            try
            {
                return vMedioComunicacionDAL.ConsultarMedioComunicacion(string.IsNullOrEmpty(pNombre) ? null : pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Ingresar Medios de Comunicación.
        /// </summary>
        /// <returns>0 si el nombre se encuentra duplicado y 1 si se agregó correctamente</returns>
        public int IngresarMedioComunicacion(MedioComunicacion pMedioComunicacion)
        {
            try
            {
                return vMedioComunicacionDAL.IngresarMedioComunicacion(pMedioComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar medios de comunicación por id.
        /// </summary>
        /// <returns>Medio de comunicación consultado en detalle</returns>
        public MedioComunicacion ConsultarMedioComunicacionPorId(int pIdMedioComunicacion)
        {
            try
            {
                return vMedioComunicacionDAL.ConsultarMedioComunicacionPorId(pIdMedioComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza medio de comunicación editado.
        /// </summary>
        /// <returns>Retorna 1 si el nombre se encuentra duplicado y 0 si se actualizó correctamente</returns>
        public int ActualizarMedioComunicacion(MedioComunicacion pMedioComunicacion)
        {
            try
            {
                return vMedioComunicacionDAL.ActualizarMedioComunicacion(pMedioComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
