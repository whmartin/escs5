﻿//-----------------------------------------------------------------------
// <copyright file="TipoBienBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoBienBLL.</summary>
// <author>INGENIAN</author>
// <date>31/01/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using System.Collections.Generic;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TipoBienBLL.
    /// </summary>
    public class TipoBienBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TipoBienDAL
        /// </summary>
        private TipoBienDAL vTipoBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TipoBienBLL"/>.
        /// </summary>
        public TipoBienBLL()
        {
            this.vTipoBienDAL = new TipoBienDAL();
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<TipoBien> ConsultarTipoBienTodos()
        {
            try
            {
                return this.vTipoBienDAL.ConsultarSeven();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<string> ConsultarSubTipoBien(int tipoBien)
        {
            try
            {
                return this.vTipoBienDAL.ConsultarSubTipoBien(tipoBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<string> ConsultarMarcaBien()
        {
            try
            {
                return this.vTipoBienDAL.ConsultarMarcaBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los ClaseBien
        /// </summary>
        /// <returns>List de tipo string con la información consultada</returns>
        public List<string> ConsultarClaseBien()
        {
            try
            {
                return this.vTipoBienDAL.ConsultarClaseBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


    }
}
