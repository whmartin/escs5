﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class EntidadEmisoraBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private EntidadEmisoraDAL vEntidadEmisoraDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public EntidadEmisoraBLL()
        {
            this.vEntidadEmisoraDAL = new EntidadEmisoraDAL();
        }

        /// <summary>
        /// consulta los EntidadEmisora
        /// </summary>
        /// <returns>LISTA DE EntidadEmisora</returns>
        public List<EntidadEmisora> ConsultarEntidadEmisora()
        {
            try
            {
                return this.vEntidadEmisoraDAL.ConsultarEntidadEmisora();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
