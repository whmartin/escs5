﻿//-----------------------------------------------------------------------
// <copyright file="InformacionVentaBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase InformacionVentaBLL.</summary>
// <author>Ingenian Software</author>
// <date>02/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo InformacionVenta.
    /// </summary>
    public class InformacionVentaBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private InformacionVentaDAL vInformacionVentaDAL;

        /// <summary>
        /// Initializes a new instance of the <see cref="InformacionVentaBLL"/> class.
        /// </summary>
        public InformacionVentaBLL()
        {
            this.vInformacionVentaDAL = new InformacionVentaDAL();
        }

        /// <summary>
        /// Método para consultar la Información de Venta por Id
        /// </summary>
        /// <param name="pIdInformacionVenta">Id a consultar</param>
        /// <returns>Resultado de la consulta</returns>
        public InformacionVenta ConsultarInformacionVentaId(int pIdInformacionVenta)
        {
            try
            {
                return this.vInformacionVentaDAL.ConsultarInformacionVentaId(pIdInformacionVenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Insertar la información de la venta
        /// </summary>
        /// <param name="pInformacionVenta">Entidad a Insertar</param>
        /// <returns>Resultado de la operación</returns>
        public int InsertarInformacionVenta(InformacionVenta pInformacionVenta)
        {
            try
            {
                return this.vInformacionVentaDAL.InsertarInformacionVenta(pInformacionVenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar la información de la venta
        /// </summary>
        /// <param name="pInformacionVenta">Entidad a Modificar</param>
        /// <returns>Resultado de la operación</returns>
        public int ModificarInformacionVenta(InformacionVenta pInformacionVenta)
        {
            try
            {
                return this.vInformacionVentaDAL.ModificarInformacionVenta(pInformacionVenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
