﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class EntidadBancariaBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private EntidadBancariaDAL vEntidadBancariaDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="EntidadBancariaBLL"/>.
        /// </summary>
        public EntidadBancariaBLL()
        {
            this.vEntidadBancariaDAL = new EntidadBancariaDAL();
        }

        /// <summary>
        /// consulta los EntidadBancaria
        /// </summary>
        /// <returns>LISTA DE EntidadBancaria</returns>
        public List<EntidadBancaria> ConsultarEntidadBancaria()
        {
            try
            {
                return this.vEntidadBancariaDAL.ConsultarEntidadBancaria();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los tipos de cuentas
        /// </summary>
        /// <returns>LISTA DE TipoCuenta</returns>
        public List<TipoCuenta> ConsultarTipoCuenta()
        {
            try
            {
                return this.vEntidadBancariaDAL.ConsultarTipoCuenta();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
