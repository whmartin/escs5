﻿//-----------------------------------------------------------------------
// <copyright file="TipoTituloBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoTituloBLL.</summary>
// <author>INGENIAN</author>
// <date>02/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TipoTituloBLL.
    /// </summary>
    public class TipoTituloBLL
    {
        /// <summary>
        /// Variable de tipo TipoTituloDAL
        /// </summary>
        private TipoTituloDAL vTipoTituloDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TipoTituloBLL"/>.
        /// </summary>
        public TipoTituloBLL()
        {
            this.vTipoTituloDAL = new TipoTituloDAL();
        }

        // Todo: Se comenta porque no cumple con la estructura de la tabla en BD
        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        //public List<TipoTitulo> ConsultarTipoTituloTodos()
        //{
        //    try
        //    {
        //        return this.vTipoTituloDAL.ConsultarTipoTituloTodos();
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new GenericException(ex);
        //    }
        //}

        /// <summary>
        /// consulta los TipoTitulo por el IdTipoTitulo
        /// </summary>
        /// <param name="pIdDenunciaBien">id que se va a consultar</param>
        /// <returns>TipoTitulo</returns>
        public TipoTitulo ConsultarTipoTituloPorIdTipoTitulo(int pIdTipoTitulo)
        {
            try
            {
                return this.vTipoTituloDAL.ConsultarTipoTituloPorIdTipoTitulo(pIdTipoTitulo);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Consultar Titulos de Valor.
        /// </summary>
        /// <returns>Lista de títulos de valor filtrada por nombre o estado</returns>
        public List<TipoTitulo> ConsultarTipoTitulo(string pNombre, bool? pEstado)
        {
            try
            {
                return vTipoTituloDAL.ConsultarTipoTitulo(string.IsNullOrEmpty(pNombre) ? null : pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los TipoTitulo 
        /// </summary>
        /// <returns>lISTA DE TipoTitulo</returns>
        public List<TipoTitulo> ConsultarTipoTitulo()
        {
            try
            {
                return this.vTipoTituloDAL.ConsultarTipoTitulo();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Ingresar Titulos de valor.
        /// </summary>
        /// <returns>0 si el nombre se encuentra duplicado y 1 si se agregó correctamente</returns>
        public int IngresarTipoTitulo(TipoTitulo pTipoTitulo)
        {
            try
            {
                return vTipoTituloDAL.IngresarTipoTitulo(pTipoTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Titulos de valor por id.
        /// </summary>
        /// <returns>Tipo titulo consultado en detalle</returns>
        public TipoTitulo ConsultarTipoTituloPorId(int pIdTipoTitulo)
        {
            try
            {
                return vTipoTituloDAL.ConsultarTipoTituloPorId(pIdTipoTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza Titulos de valor editado.
        /// </summary>
        /// <returns>Retorna 1 si el nombre se encuentra duplicado y 0 si se actualizó correctamente</returns>
        public int ActualizarTipoTitulo(TipoTitulo pTipoTitulo)
        {
            try
            {
                return vTipoTituloDAL.ActualizarTipoTitulo(pTipoTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<TipoTitulo> ConsultarTipoTituloTodos()
        {
            try
            {
                return this.vTipoTituloDAL.ConsultarTipoTituloTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
