﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TrasladoDenunciaBLL.
    /// </summary>
    public class TrasladoDenunciaBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TrasladoDenunciaDAL
        /// </summary>
        private TrasladoDenunciaDAL vTrasladoDenunciaDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TrasladoDenunciaBLL"/>.
        /// </summary>
        public TrasladoDenunciaBLL()
        {
            this.vTrasladoDenunciaDAL = new TrasladoDenunciaDAL();
        }

        /// <summary>
        /// consulta los traslados denuncia por varios parámetros
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia que contiene la información a consultar </param>
        /// <returns>lista de tipo TrasladoDenuncia con la información resultante de la consulta</returns>
        public List<TrasladoDenuncia> ConsultarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia)
        {
            try
            {
                return this.vTrasladoDenunciaDAL.ConsultarTrasladoDenuncia(pTrasladoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo traslado de la denuncia
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información a insertar</param>
        /// <returns>id del TrasladoDenuncia que se inserto</returns>
        public int InsertarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia, HistoricoEstadosDenunciaBien phistorico)
        {
            try
            {
                return this.vTrasladoDenunciaDAL.InsertarTrasladoDenuncia(pTrasladoDenuncia, phistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los traslados de las denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo TrasladoDenuncia con la información consultada </returns>
        public List<TrasladoDenuncia> ConsultarTrasladoDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vTrasladoDenunciaDAL.ConsultarTrasladoDenunciaPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta el ultimo numero consecutivo de los traslados
        /// </summary>
        /// <returns>ultimo numero consecutivo de los traslados</returns>
        public int ConsultarUltimoNumerotraslado()
        {
            try
            {
                return this.vTrasladoDenunciaDAL.ConsultarUltimoNumerotraslado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta el TrasladoDenuncia por el idTrasladoDenuncia
        /// </summary>
        /// <param name="pIdTrasladoDenuncia">idTrasladoDenuncia a consultar</param>
        /// <returns>variable de tipo TrasladoDenuncia con la información consultada</returns>
        public TrasladoDenuncia ConsultarTrasladoDenunciaPorId(int pIdTrasladoDenuncia)
        {
            try
            {
                return this.vTrasladoDenunciaDAL.ConsultarTrasladoDenunciaPorId(pIdTrasladoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un TrasladoDenuncia
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo v con la información que se va aguardar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia)
        {
            try
            {
                return this.vTrasladoDenunciaDAL.EditarTrasladoDenuncia(pTrasladoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }    
  
        /// <summary>
        /// edita un TrasladoDenuncia, cambia el estado de la denuncia  e inserta en el historial de estados de la denuncia 
        /// </summary>
        /// <param name="pTrasladoDenuncia">variable de tipo TrasladoDenuncia con la información que se va aguardar</param>
        /// <param name="pDenunciabien">variable de tipo DenunciaBien con la información que se va aguardar</param>
        /// <param name="pHistorico">variable de tipo HistóricoEstadosDenunciaBien con la información que se va aguardar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarTrasladoDenuncia(TrasladoDenuncia pTrasladoDenuncia, DenunciaBien pDenunciabien, HistoricoEstadosDenunciaBien pHistorico)
        {
            try
            {
                return this.vTrasladoDenunciaDAL.EditarTrasladoDenuncia(pTrasladoDenuncia, pDenunciabien, pHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
