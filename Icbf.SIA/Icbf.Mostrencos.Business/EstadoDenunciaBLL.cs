﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo EstadoDenunciaBLL.
    /// </summary>
    public class EstadoDenunciaBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo EstadoDenunciaDAL
        /// </summary>
        private EstadoDenunciaDAL vEstadoDenunciaDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="EstadoDenunciaBLL"/>.
        /// </summary>
        public EstadoDenunciaBLL()
        {
            this.vEstadoDenunciaDAL = new EstadoDenunciaDAL();
        }

        /// <summary>
        /// Consulta todos los estados denuncia que estén activos
        /// </summary>
        /// <returns>Lista de estados denuncia</returns>
        public List<EstadoDenuncia> ConsultarEstadoDenuncia()
        {
            try
            {
                return this.vEstadoDenunciaDAL.ConsultarEstadoDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los estados denuncia por el IdEstadoDenuncia
        /// </summary>
        /// <param name="pIdEstadoDenuncia">Id del estado denuncia</param>
        /// <returns>Estado denuncia consultado</returns>
        public EstadoDenuncia ConsultarEstadoDenunciaPorId(int pIdEstadoDenuncia)
        {
            try
            {
                return this.vEstadoDenunciaDAL.ConsultarEstadoDenunciaPorId(pIdEstadoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los Estados de la Denuncia para Repoorte
        /// </summary>
        /// <returns>Lista Filtrada de Estados</returns>
        public List<EstadoDenuncia> ConsultarEstadosDenunciasReportes()
        {
            try
            {
                return vEstadoDenunciaDAL.ConsultarEstadosDenunciasReportes();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
