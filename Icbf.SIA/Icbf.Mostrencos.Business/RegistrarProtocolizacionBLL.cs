﻿using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Business
{
    public class RegistrarProtocolizacionBLL
    {
        RegistrarProtocolizacionDAL vRegistrarProtocolizacionDAL = new RegistrarProtocolizacionDAL();

        /// <summary>
        /// Consultar Info Escrituración
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Informacion detallada de la escrituracion relacionada con el Bien denunciado</returns>
        public InfoEscrituracion ConsultarDatosInfoEscrituracion(int IdDenunciaBien)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ConsultarDatosInfoEscrituracion(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Info Adjudicación
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Informacion detallada de los datos de la adjudicacion relacionados con la denuncia del bien</returns>
        public InfoAdjudicacion ConsultarDatosInfoAdjudicacion(int IdDenunciaBien)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ConsultarDatosInfoAdjudicacion(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Tipo Trámite
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Tipo de tramite generado 1: Juridico - 2: Notarial</returns>
        public int ConsultarTipoTramite(int IdDenunciaBien)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ConsultarTipoTramite(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar la informacion de los muebles e inmuebles a partir del Id de la denuncia del bien
        /// </summary>
        /// <param name="IdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns>Lista de todos los muebles e inmuebles relacionados con el Id de la denuncia</returns>
        public List<InfoMuebleProtocolizacion> ConsultarInfoMuebleXIdBienDenuncia(int IdDenunciaBien)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ConsultarInfoMuebleXIdBienDenuncia(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar la informacion del titulo de valor a partir del Id de la Denuncia
        /// </summary>
        /// <param name="vInIntIdDenunciaBien"></param>
        /// <returns>Lista de todos los titulos relacionados con el Id de la denuncia</returns>
        public List<ResultFiltroTitulo> ConsultarTituloXIdDenuncia(int vInIntIdDenunciaBien)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ConsultarTituloXIdDenuncia(vInIntIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del tramite judicial relacionado con el Id de la denuncia
        /// </summary>
        /// <param name="vDatoTramiteJudicial"></param>
        /// <returns>Numero de filas afectadas en la base de datos tras la actualizacion</returns>
        public int ActualizarTramiteJudicialxIdDenunciaBien(TramiteJudicial vDatoTramiteJudicial)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ActualizarTramiteJudicialxIdDenunciaBien(vDatoTramiteJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del bien relacionado con el ID de la denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien que se va a actualizar</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia relacionada con el Id del bien</param>
        /// <returns></returns>
        public int ActualizarEstadoBienxIdDenuncia(string vEstadoBien, int vIdDenunciaBien, DateTime? vFechaAdjudica, int vIdMuebleInmueble)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ActualizarEstadoBienxIdDenuncia(vEstadoBien, vIdDenunciaBien, vFechaAdjudica, vIdMuebleInmueble);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del titulo del bien relacionado con el ID de la Denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien a actualizar para el titulo del valor</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns></returns>
        public int ActualizarTituloBienxIdDenuncia(string vEstadoBien, int vIdDenunciaBien, DateTime? vFechaAdjudica, int pIdTitulo)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ActualizarTituloBienxIdDenuncia(vEstadoBien, vIdDenunciaBien, vFechaAdjudica, pIdTitulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del titulo del bien relacionado con el ID de la Denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien a actualizar para el titulo del valor</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns></returns>
        public int ActualizarFechaComunicacionTitulo(int vIdDenunciaBien, DateTime vFechaAComunicacion)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ActualizarFechaComunicacionTitulo(vIdDenunciaBien, vFechaAComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza la informacion del estado del titulo del bien relacionado con el ID de la Denuncia
        /// </summary>
        /// <param name="vEstadoBien">Estado del bien a actualizar para el titulo del valor</param>
        /// <param name="vIdDenunciaBien">Id de la denuncia del bien</param>
        /// <returns></returns>
        public int ActualizarFechaComunicacionNotarial(int vIdDenunciaBien, DateTime vFechaAComunicacion)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ActualizarFechaComunicacionNotarial(vIdDenunciaBien, vFechaAComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Actualiza la informacion del tramite notarial relacionado con el Id de la denuncia
        /// </summary>
        /// <param name="vIdDenunciaBien">ID de la Denuncia relacionada con el Bien</param>
        /// <param name="vFechaEscritura">Fecha actualizada de la escritura</param>
        /// <param name="vNumeroEscritura">Numero actualizado de la escritura</param>
        /// <param name="vNotariaRegistra">Nombre actualizado de la notaria donde se registra la denuncia</param>
        /// <param name="vNumeroRegistro">Numero actualizado del registro relacionado con el Bien</param>
        /// <param name="vFechaIngresoICBF">Fecha actualizada del ingreso del registro al ICBF</param>
        /// <param name="vFechaInstrumentosP">Fecha actualizada de los instrumentos relacionados con el Id de la denuncia</param>
        /// <returns></returns>
        public int ActualizarTramiteNotarialxIdDenuncia(int vIdDenunciaBien, DateTime vFechaEscritura, decimal vNumeroEscritura,
            decimal vNumeroRegistro, DateTime vFechaIngresoICBF, DateTime vFechaInstrumentosP, DateTime vFechaComunicacion)
        {
            try
            {
                return this.vRegistrarProtocolizacionDAL.ActualizarTramiteNotarialxIdDenuncia(vIdDenunciaBien, vFechaEscritura, vNumeroEscritura,
                    vNumeroRegistro, vFechaIngresoICBF, vFechaInstrumentosP, vFechaComunicacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
