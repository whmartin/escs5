﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class RegistroCalidadDenuncianteCitacionBLL
    {
        private RegistroCalidadDenuncianteCitacionDAL vRegistroCalidadDenuncianteCitacionDAL;
        public RegistroCalidadDenuncianteCitacionBLL()
        {
            vRegistroCalidadDenuncianteCitacionDAL = new RegistroCalidadDenuncianteCitacionDAL();
        }

        public RegistroCalidadDenuncianteCitacion ConsultarCalidadDenunciante(int IdReconocimientoCalidadDenunciante)
        {
            try
            {
                return vRegistroCalidadDenuncianteCitacionDAL.ConsultarCalidadDenuncianteCitacion(IdReconocimientoCalidadDenunciante);
            }
            catch (UserInterfaceException exc)
            {
                throw new UserInterfaceException(exc.Message);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int SetCalidadDenuncianteCitacion(RegistroCalidadDenuncianteCitacion vRegistroCalidad)
        {
            try
            {
                return vRegistroCalidadDenuncianteCitacionDAL.SetCalidadDenuncianteCitacion(vRegistroCalidad);
            }
            catch (UserInterfaceException exc)
            {
                throw new UserInterfaceException(exc.Message);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
