﻿//-----------------------------------------------------------------------
// <copyright file="AccionBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase AccionBLL.</summary>
// <author>Ingenian Software</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo Accion.
    /// </summary>
    public class AccionBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private AccionDAL vAccionDAL;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccionBLL"/> class.
        /// </summary>
        public AccionBLL()
        {
            this.vAccionDAL = new AccionDAL();
        }

        /// <summary>
        /// Evento para Cargar la Lista de opciones de Acciones x IdActuacion
        /// </summary>
        /// <param name="sender">The DropDownList</param>
        /// <param name="e">The SelectedIndexChanged</param>
        public List<Accion> ConsultarAccionesActivasPorIdFase(int pIdActuacion)
        {
            return this.vAccionDAL.ConsultarAccionesActivasPorIdFase(pIdActuacion);
        }
    }
}
