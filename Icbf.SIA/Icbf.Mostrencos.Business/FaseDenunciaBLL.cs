﻿using System;
using System.Collections.Generic;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class FaseDenunciaBLL
    {
        private FaseDenunciaDAL vFaseDenunciaDAL;

        public FaseDenunciaBLL()
        {
            this.vFaseDenunciaDAL = new FaseDenunciaDAL();
        }

        public List<FasesDenuncia> ConsultarFasesDenuncia()
        {
            return this.vFaseDenunciaDAL.ConsultarFasesDenuncia();
        }

        /// <summary>
        /// Método que consulta las fases en estado Activo
        /// </summary>
        /// <returns>Lista de Fases</returns>
        public List<FasesDenuncia> ConsultarFasesActivas()
        {
            return this.vFaseDenunciaDAL.ConsultarFasesActivas();
        }
    }
}
