//-----------------------------------------------------------------------
// <copyright file="RegistroDenunciaBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase RegistroDenunciaBLL.</summary>
// <author>INGENIAN SOFTWARE[Dar�o Beltr�n]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la l�gica de negocio del modulo RegistroDenuncia.
    /// </summary>
    public class RegistroDenunciaBLL
    {
        /// <summary>
        /// Inicializaci�n de un objeto tipo DAL
        /// </summary>
        private RegistroDenunciaDAL vRegistroDenunciaDAL;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistroDenunciaBLL"/> class.
        /// </summary>
        public RegistroDenunciaBLL()
        {
            this.vRegistroDenunciaDAL = new RegistroDenunciaDAL();
        }

        /// <summary>
        /// M�todo para Insertar RegistroDenuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a Insertar</param>
        /// <returns>resultado de la operaci�n</returns>
        public int InsertarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return this.vRegistroDenunciaDAL.InsertarRegistroDenuncia(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para Modificar un Registro de denuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a modificar</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int ModificarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ModificarRegistroDenuncia(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para eliminar un Registro de denuncia.
        /// </summary>
        /// <param name="pRegistroDenuncia">Entidad a eliminar</param>
        /// <returns>Resultado de la operaci�n</returns>
        public int EliminarRegistroDenuncia(RegistroDenuncia pRegistroDenuncia)
        {
            try
            {
                return this.vRegistroDenunciaDAL.EliminarRegistroDenuncia(pRegistroDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una denuncia por Id
        /// </summary>
        /// <param name="pIdDenunciaBien">Id a consultar</param>
        /// <returns>Resultadod e la consulta</returns>
        public RegistroDenuncia ConsultarRegistroDenuncia(int pIdDenunciaBien)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarRegistroDenuncia(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar una denuncia por Tipo y n�mero de identificaci�n
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Id Tipo de documento</param>
        /// <param name="pNumeroIdentificacion">Numero de identificaci�n</param>
        /// <returns>Resultado de la operaci�n</returns>
        public RegistroDenuncia ConsultarRegistroDenunciante(int pIdTipoDocIdentifica, string pNumeroIdentificacion)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarRegistroDenunciante(pIdTipoDocIdentifica, pNumeroIdentificacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta una denuncia por parametros
        /// </summary>
        /// <param name="pIdTipoDocIdentifica">Id tipo documento identificaci�n</param>
        /// <param name="pNumeroIdentificacion">N�mero de identificaci�n</param>
        /// <param name="pDenunciante">Nombre del denunciante</param>
        /// <param name="pRadicadoDenuncia">N�mero de radicado de la denuncia</param>
        /// <returns>Lista del resultado de la operaci�n</returns>
        public List<RegistroDenuncia> ConsultarRegistroDenuncias(int? pIdTipoDocIdentifica, string pNumeroIdentificacion, string pDenunciante, string pRadicadoDenuncia)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarRegistroDenuncias(pIdTipoDocIdentifica, pNumeroIdentificacion, pDenunciante, pRadicadoDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta una denuncia por id de la denuncia y tipo de documento
        /// </summary>
        /// <param name="pIdDenuncia">Id de la denuncia</param>
        /// <param name="pTipoDocumento">Tipo de documento</param>
        /// <returns>Resultado de la operaci�n</returns>
        public RegistroDenuncia ConsultarDocumentosBasicos(int pIdDenuncia, int pTipoDocumento)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarDocumentosBasicos(pIdDenuncia, pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta los documentos de la secci�n Otros 
        /// </summary>
        /// <param name="pIdDenuncia">Id de la denuncia</param>
        /// <param name="pTipoDocumento">Tipo de documento</param>
        /// <returns>Lista del resultado de la operaci�n</returns>
        public List<DocumentosSolicitadosDenunciaBien> ConsultarOtrosDocumentos(int pIdDenuncia, int pTipoDocumento)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarOtrosDocumentos(pIdDenuncia, pTipoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta el ultimo id de la denuncia
        /// </summary>
        /// <returns>�ltimo Id Insertado</returns>
        public int ConsultarConsecutivoDenuncia()
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarConsecutivoDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo que consulta el ultimo id de la denuncia extrayendo el a�o
        /// </summary>
        /// <returns>�ltimo Id Insertado</returns>
        public int ConsultarAnioUltimoRegistro()
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarAnioUltimoRegistro();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del coordinador juridico.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Parametro de consulta por id de ProviderKey</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoCoordinadorJuridico(int? pIdRegional, string pProviderKey)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarCorreoCoordinadorJuridico(pIdRegional, pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del coordinador juridico.
        /// </summary>
        /// <param name="pIdRegional">Parametro de consulta por id de regional</param>
        /// <param name="pProviderKey">Parametro de consulta por id de ProviderKey</param>
        /// <returns>correos</returns>
        public string ConsultarNombreCoordinadorJuridico(string pCorreoCoordinadorJuridico)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarNombreCoordinadorJuridico(pCorreoCoordinadorJuridico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// M�todo para consultar el correo del coordinador juridico.
        /// </summary>
        /// <param name="pProviderKey">Parametro de consulta por id de ProviderKey</param>
        /// <returns>correos</returns>
        public string ConsultarCorreoAdministrador(string pProviderKey)
        {
            try
            {
                return this.vRegistroDenunciaDAL.ConsultarCorreoAdministrador(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
