﻿
namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class SubTipoBienBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private SubTipoBienDAL vSubTipoBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="AbogadosBLL"/>.
        /// </summary>
        public SubTipoBienBLL()
        {
            this.vSubTipoBienDAL = new SubTipoBienDAL();
        }

        /// <summary>
        /// consulta los SubTipoBien
        /// </summary>
        /// <returns>LISTA DE SubTipoBien</returns>
        public List<SubTipoBien> ConsultarSubTipoBien(string pIdTipoBien)
        {
            try
            {
                return this.vSubTipoBienDAL.ConsultarSubTipoBien(pIdTipoBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
