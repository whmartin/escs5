﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo DespachoJudicialBLL.
    /// </summary>
    public class DespachoJudicialBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DespachoJudicialDAL
        /// </summary>
        private DespachoJudicialDAL vDespachoJudicialDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DespachoJudicialDAL"/>.
        /// </summary>
        public DespachoJudicialBLL()
        {
            vDespachoJudicialDAL = new DespachoJudicialDAL();
        }

        /// <summary>
        /// Consultar Despachos.
        /// </summary>
        /// <returns>Lista de medios de comunicación filtrada por nombre o estado</returns>
        public List<DespachoJudicial> ConsultarDespachoJudicial(string pNombre, int pDepartamento, int pMunicipio, bool? pEstado)
        {
            try
            {
                return vDespachoJudicialDAL.ConsultarDespachoJudicial(string.IsNullOrEmpty(pNombre) ? null : pNombre, pDepartamento, pMunicipio, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DespachoJudicial> ConsultarDespachoJudicialTodos()
        {
            try
            {
                return vDespachoJudicialDAL.ConsultarDespachoJudicialTodos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Ingresar Despachos.
        /// </summary>
        /// <returns>0 si el nombre se encuentra duplicado y 1 si se agregó correctamente</returns>
        public int IngresarDespachoJudicial(DespachoJudicial pDespachoJudicial)
        {
            try
            {
                return vDespachoJudicialDAL.IngresarDespachoJudicial(pDespachoJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar despacho judicial por id.
        /// </summary>
        /// <returns>despacho judicial consultado en detalle</returns>
        public DespachoJudicial ConsultarDespachoJudicialPorId(int pIdDespacho)
        {
            try
            {
                return vDespachoJudicialDAL.ConsultarDespachoJudicialPorId(pIdDespacho);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Actualiza despacho judicial editado.
        /// </summary>
        /// <returns>Retorna 1 si el nombre se encuentra duplicado y 0 si se actualizó correctamente</returns>
        public int ActualizarDespachoJudicial(DespachoJudicial pDespachoJudicial)
        {
            try
            {
                return vDespachoJudicialDAL.ActualizarDespachoJudicial(pDespachoJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
