﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo DenunciaBienBLL.
    /// </summary>
    public class DenunciaBienBLL
    {
        /// <summary>
        /// Variable de tipo DenunciaBienDAL
        /// </summary>
        private DenunciaBienDAL vDenunciaBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DenunciaBienBLL"/>.
        /// </summary>
        public DenunciaBienBLL()
        {
            this.vDenunciaBienDAL = new DenunciaBienDAL();
        }

        /// <summary>
        /// Consulta una denuncia bien por el IdDenunciaBien
        /// </summary>
        /// <param name="pIdDenunciaBien">IdDenunciaBien que se va a consultar</param>
        /// <returns>Una variable de tipo DenunciaBien con la información encontrada</returns>
        public DenunciaBien ConsultarDenunciaBienXIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vDenunciaBienDAL.ConsultarDenunciaBienXIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de una denuncia
        /// </summary>
        /// <param name="pDenunciaBien">variable de tipo DenunciaBien que contiene la información que se va a almacenar </param>
        /// <returns>1 si se actualizó correctamente la denuncia </returns>
        public int EditarDenunciaBien(DenunciaBien pDenunciaBien)
        {
            try
            {
                return this.vDenunciaBienDAL.EditarDenunciaBien(pDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EditarDenunciaBienFase(int vDenunciaBien)
        {
            try
            {
                return this.vDenunciaBienDAL.EditarDenunciaBienFase(vDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EditarDocumentacionCompleta(int vDenunciaBien, bool vDocumentacion)
        {
            try
            {
                return this.vDenunciaBienDAL.EditarDocumentacionCompleta(vDenunciaBien, vDocumentacion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary> 
        /// Actualizar el Estado de la Denuncia Bien a Terminada
        /// </summary>
        /// <param name="vDenunciaBien">Id de la denuncia</param>
        /// <returns>Resultado de la operación</returns>
        public int EditarDenunciaBienEstado(int vDenunciaBien)
        {
            try
            {
                return this.vDenunciaBienDAL.EditarDenunciaBienEstado(vDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta las denuncias bien que coincidan con los parámetros de búsqueda
        /// </summary>
        /// <param name="pIdRegional">Id de la regional de la denuncia</param>
        /// <param name="pEstado">Estado de la denuncia</param>
        /// <param name="pRegistradoDesde">Fecha desde del registro de la denuncia</param>
        /// <param name="pRegistradoHasta">Fecha hasta del registro de la denuncia</param>
        /// <param name="pRadicadoDenuncia">radicado de la denuncia</param>
        /// <param name="pIdAbogado">id del abogado que esta asignado a la denuncia</param>
        /// <returns>Lista de denuncias que cumplen con los criterios de la búsqueda</returns>
        public List<DenunciaBien> ConsultarDenunciaBien(int pIdRegional, int pEstado, string pRegistradoDesde, string pRegistradoHasta, string pRadicadoDenuncia, int pIdAbogado)
        {
            try
            {
                return this.vDenunciaBienDAL.ConsultarDenunciaBien(pIdRegional, pEstado, pRegistradoDesde, pRegistradoHasta, pRadicadoDenuncia, pIdAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
