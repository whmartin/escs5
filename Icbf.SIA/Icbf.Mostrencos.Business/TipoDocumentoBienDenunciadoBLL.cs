﻿//-----------------------------------------------------------------------
// <copyright file="TipoDocumentoBienDenunciadoBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TipoDocumentoBienDenunciadoBLL.</summary>
// <author>INGENIAN SOFTWARE[Darío Beltrán]</author>
// <date>06/03/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase Tipo Documento bien denunciado
    /// </summary>
    public class TipoDocumentoBienDenunciadoBLL
    {
        /// <summary>
        /// The v tipo documento bien denunciado dal
        /// </summary>
        private TipoDocumentoBienDenunciadoDAL vTipoDocumentoBienDenunciadoDAL;

        /// <summary>
        /// Initializes a new instance of the <see cref="TipoDocumentoBienDenunciadoBLL"/> class.
        /// </summary>
        public TipoDocumentoBienDenunciadoBLL()
        {
            this.vTipoDocumentoBienDenunciadoDAL = new TipoDocumentoBienDenunciadoDAL();
        }

        /// <summary>
        /// Consultars the tipo documento bien denunciados.
        /// </summary>
        /// <returns>Lista resultado de la operación</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciados()
        {
            try
            {
                List<TipoDocumentoBienDenunciado> vLstDocumentos = this.vTipoDocumentoBienDenunciadoDAL.ConsultarTipoDocumentoBienDenunciados();
                return vLstDocumentos;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Variable de tipo TipoDocumentoBienDenunciado con los datos a insertar</param>
        /// <returns>IdTipoDocumentoBienDenunciado del registro insertado</returns>
        public int InsertarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoDAL.InsertarTipoDocumentoBienDenunciado(pTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// edita un registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Variable de tipo TipoDocumentoBienDenunciado con los datos a insertar</param>
        /// <returns>el numero de registros editados</returns>
        public int EditarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoDAL.EditarTipoDocumentoBienDenunciado(pTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado por el id
        /// </summary>
        /// <param name="pIdTipoDocumentoBienDenunciado">id del TipoDocumentoBienDenunciado</param>
        /// <returns>Variable de tipo TipoDocumentoBienDenunciado con los datos consultados</returns>
        public TipoDocumentoBienDenunciado ConsultarTipoDocumentoBienDenunciadoPorId(int pIdTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoDAL.ConsultarTipoDocumentoBienDenunciadoPorId(pIdTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoBienDenunciado que coincidan con los parámetros de entrada
        /// </summary>
        /// <param name="pNombre">Nombre del documento</param>
        /// <param name="pEstado">Estado del documento</param>
        /// <returns>Lista de resultados de la consulta</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoBienDenunciado(string pNombre, string pEstado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoDAL.ConsultarTipoDocumentoBienDenunciado(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla TipoDocumentoBienDenunciado
        /// </summary>
        /// <param name="pTipoDocumentoBienDenunciado">Id del registro que se va a eliminar</param>
        /// <returns>1 si se ejecuta correctamente</returns>
        public int EliminarTipoDocumentoBienDenunciado(TipoDocumentoBienDenunciado pTipoDocumentoBienDenunciado)
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoDAL.EliminarTipoDocumentoBienDenunciado(pTipoDocumentoBienDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que consulta los tipos de documento Activos
        /// </summary>
        /// <returns>Lista de Tipos de Documento</returns>
        public List<TipoDocumentoBienDenunciado> ConsultarTipoDocumentoSoporte()
        {
            try
            {
                return this.vTipoDocumentoBienDenunciadoDAL.ConsultarTipoDocumentoSoporte();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}