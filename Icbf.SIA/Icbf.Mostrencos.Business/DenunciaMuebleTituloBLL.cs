﻿//-----------------------------------------------------------------------
// <copyright file="DenunciaMuebleTituloBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase DenunciaMuebleTituloBLL.</summary>
// <author>INGENIAN</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class DenunciaMuebleTituloBLL
    {
        private DenunciaMuebleTituloDAL vDenunciaMuebleTituloDAL;
        public DenunciaMuebleTituloBLL()
        {
            vDenunciaMuebleTituloDAL = new DenunciaMuebleTituloDAL();
        }

        public List<DenunciaMuebleTitulo> ConsultarValorSumaDenuncia(int IdDenunciaBien)
        {
            try
            {
                return vDenunciaMuebleTituloDAL.ConsultarValorSumaDenuncia(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DenunciaMuebleTitulo ConsultarValorSumaDenunciaPorIdDenuncia(int IdDenunciaBien)
        {
            try
            {
                return vDenunciaMuebleTituloDAL.ConsultarValorSumaDenunciaPorIdDenuncia(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
