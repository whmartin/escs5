﻿//-----------------------------------------------------------------------
// <copyright file="ActuacionBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ActuacionBLL.</summary>
// <author>Ingenian Software</author>
// <date>10/10/2018</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo Actuacion.
    /// </summary>
    public class ActuacionBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo DAL
        /// </summary>
        private ActuacionDAL vActuacionDAL;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActuacionBLL"/> class.
        /// </summary>
        public ActuacionBLL()
        {
            this.vActuacionDAL = new ActuacionDAL();
        }

        /// <summary>
        /// Evento para Cargar la Lista de opciones de Actuaciones x IdFase
        /// </summary>
        /// <param name="sender">The DropDownList</param>
        /// <param name="e">The SelectedIndexChanged</param>
        public List<Actuacion> ConsultarActuacionesActivasPorIdFase(int pIdFase)
        {
            return this.vActuacionDAL.ConsultarActuacionesActivasPorIdFase(pIdFase); 
        }
    }
}
