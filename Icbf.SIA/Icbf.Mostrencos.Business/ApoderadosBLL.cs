﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo ApoderadosBLL.
    /// </summary>
    public class ApoderadosBLL
    {
        /// <summary>
        /// Variable de tipo vApoderadosDAL
        /// </summary>
        private ApoderadosDAL vApoderadosDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="ApoderadosBLL"/>.
        /// </summary>
        public ApoderadosBLL()
        {
            this.vApoderadosDAL = new ApoderadosDAL();
        }

        /// <summary>
        /// Inserta la información de un Apoderado
        /// </summary>
        /// <param name="pApoderado">Variable de tipo Apoderado que contiene la información que se va a insertar</param>
        /// <returns>Id del Apoderado insertado</returns>
        public int InsertarApoderado(Apoderados pApoderado)
        {
            try
            {
                return this.vApoderadosDAL.InsertarApoderado(pApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de un Apoderado por el id Apoderado
        /// </summary>
        /// <param name="pIdApoderado">Id Apoderado que se va a consultar</param>
        /// <returns>Variable de tipo Apoderado que contiene los datos de la consulta</returns>
        public Apoderados ConsultarApoderadosPorIdApoderado(int pIdApoderado)
        {
            try
            {
                return this.vApoderadosDAL.ConsultarApoderadosPorIdApoderado(pIdApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de un Apoderado por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo Apoderados con la información resultante de la consulta</returns>
        public List<Apoderados> ConsultarApoderadosPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vApoderadosDAL.ConsultarApoderadosPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un Apoderado
        /// </summary>
        /// <param name="pApoderado">Variable de tipo Apoderado que contiene la información que se va a insertar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarApoderado(Apoderados pApoderado)
        {
            try
            {
                return this.vApoderadosDAL.EditarApoderado(pApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida si ya existe un Apoderado con el mismo id tercero y id denuncia bien
        /// </summary>
        /// <param name="pIdTercero">Id del tercero</param>
        /// <param name="pIdDenunciaBien"> id de la denuncia bien</param>
        /// <returns>true si existe o false si no existe</returns>
        public bool ValidarDuplicidad(int pIdTercero, int pIdDenunciaBien, int pIdApoderado)
        {
            try
            {
                return this.vApoderadosDAL.ValidarDuplicidad(pIdTercero, pIdDenunciaBien, pIdApoderado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
