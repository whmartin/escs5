﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo RegionalBLL.
    /// </summary>
    public class RegionalBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo RegionalDAL
        /// </summary>
        private RegionalDAL vRegionalDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="RegionalBLL"/>.
        /// </summary>
        public RegionalBLL()
        {
            this.vRegionalDAL = new RegionalDAL();
        }

        /// <summary>
        /// Consulta la regional por el id regional
        /// </summary>
        /// <param name="pIdRegional">id de la regional que se va a consultar</param>
        /// <returns>variable de tipo Regional con la información encontrada</returns>
        public Regional ConsultarRegionalXId(int pIdRegional)
        {
            try
            {
                return this.vRegionalDAL.ConsultarRegionalXId(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }        
    }
}
