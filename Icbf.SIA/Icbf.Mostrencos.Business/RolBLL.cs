﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Seguridad.Entity;
    using Icbf.Utilities.Exceptions;    

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo RolBLL.
    /// </summary>
    public class RolBLL
    {        
        /// <summary>
        /// Variable de tipo vRolDAL
        /// </summary>
        private RolDAL vRolDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="RolBLL"/>.
        /// </summary>
        public RolBLL()
        {
            this.vRolDAL = new RolDAL();
        }

        /// <summary>
        /// Consulta los roles por el nombre de programa y el nombre de la función
        /// </summary>
        /// <param name="pNombrePrograma">Nombre del programa</param>
        /// <param name="pNombreFuncion">Nombre de la función</param>
        /// <returns>lista de roles</returns>
        public List<Rol> ConsultarRolesPorProgramaFuncion(string pNombrePrograma, string pNombreFuncion)
        {
            try
            {
                return this.vRolDAL.ConsultarRolesPorProgramaFuncion(pNombrePrograma, pNombreFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
