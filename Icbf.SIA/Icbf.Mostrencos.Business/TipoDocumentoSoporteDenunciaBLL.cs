﻿using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Business
{
    public class TipoDocumentoSoporteDenunciaBLL
    {
        TipoDocumentoSoporteDenunciaDAL vTipoDocumentoSoporteDenunciaDAL = new TipoDocumentoSoporteDenunciaDAL();

        //Consultar Datos Informe Denunciante
        public List<TipoDocumentoSoporteDenuncia> ListarTipoDocumentoSporteDenuncia()
        {
            try
            {
                return this.vTipoDocumentoSoporteDenunciaDAL.ListarTipoDocumentoSporteDenuncia();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta los TipoDocumentoSoporteDenunciado por el id
        /// </summary>
        /// <param name="pIdTipoDocumentoSoporteDenunciado">id del TipoDocumentoSoporteDenunciado</param>
        /// <returns>Variable de tipo TipoDocumentoBienDenunciado con los datos consultados</returns>
        public TipoDocumentoSoporteDenuncia ConsultarTipoDocumentoSoporteDenunciadoPorId(int pIdTipoDocumentoSoporteDenunciado)
        {
            try
            {
                return this.vTipoDocumentoSoporteDenunciaDAL.ConsultarTipoDocumentoSoporteDenunciadoPorId(pIdTipoDocumentoSoporteDenunciado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
