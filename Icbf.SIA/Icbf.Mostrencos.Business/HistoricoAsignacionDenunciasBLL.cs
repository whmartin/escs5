﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo HistóricoAsignaciónDenunciasBLL.
    /// </summary>
    public class HistoricoAsignacionDenunciasBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo HistóricoAsignaciónDenunciasDAL
        /// </summary>
        private HistoricoAsignacionDenunciasDAL vHistoricoAsignacionDenunciasDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="HistoricoAsignacionDenunciasBLL"/>.
        /// </summary>
        public HistoricoAsignacionDenunciasBLL()
        {
            this.vHistoricoAsignacionDenunciasDAL = new HistoricoAsignacionDenunciasDAL();
        }

        /// <summary>
        /// inserta un nuevo histórico asignación denuncia
        /// </summary>
        /// <param name="pHistoricoAsignacionDenuncias">variable de tipo Histórico Asignación Denuncias que contiene la información a insertar</param>
        /// <returns>id del Histórico Asignación Denuncias insertado</returns>
        public int InsertarHistoricoAsignacionDenuncias(HistoricoAsignacionDenuncias pHistoricoAsignacionDenuncias)
        {
            try
            {
                return this.vHistoricoAsignacionDenunciasDAL.InsertarHistoricoAsignacionDenuncias(pHistoricoAsignacionDenuncias);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el Histórico Asignación Denuncias por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de asignación de abogados</returns>
        public List<HistoricoAsignacionDenuncias> ConsultarHistoricoAsignacionDenunciasPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoAsignacionDenunciasDAL.ConsultarHistoricoAsignacionDenunciasPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}