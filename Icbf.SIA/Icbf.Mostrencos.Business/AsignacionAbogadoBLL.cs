using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class AsignacionAbogadoBLL
    {
        private AsignacionAbogadoDAL vAsignacionAbogadoDAL;
        public AsignacionAbogadoBLL()
        {
            vAsignacionAbogadoDAL = new AsignacionAbogadoDAL();
        }

        public string CosultarRegionalUsuario(int pIdRegional)
        {
            try
            {
                return vAsignacionAbogadoDAL.CosultarRegionalUsuario(pIdRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        {
            try
            {
                return vAsignacionAbogadoDAL.InsertarAsignacionAbogado(pAsignacionAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                 throw new GenericException(ex);
            }
        }

        public List<AsignacionAbogado> ConsultarAbogados(AsignacionAbogado pAbogado)
        {
            try
            {
                return vAsignacionAbogadoDAL.ConsultarAbogados(pAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ConsultaAbogadoXDenuncia> ConsultarAsignacionAbogadosXDenuncia(ConsultaAbogadoXDenuncia pCosnultaAbogado)
        {
            try
            {
                return vAsignacionAbogadoDAL.ConsultarAsignacionAbogadosXDenuncia(pCosnultaAbogado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //public int ModificarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        //{
        //    try
        //    {
        //        return vAsignacionAbogadoDAL.ModificarAsignacionAbogado(pAsignacionAbogado);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}
        //public int EliminarAsignacionAbogado(AsignacionAbogado pAsignacionAbogado)
        //{
        //    try
        //    {
        //        return vAsignacionAbogadoDAL.EliminarAsignacionAbogado(pAsignacionAbogado);
        //    }
        //    catch (UserInterfaceException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //         throw new GenericException(ex);
        //    }
        //}

    }
}
