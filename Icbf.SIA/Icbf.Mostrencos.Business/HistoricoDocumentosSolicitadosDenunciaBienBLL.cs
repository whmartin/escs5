﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo HistóricoDocumentosSolicitadosDenunciaBienBLL.
    /// </summary>
    public class HistoricoDocumentosSolicitadosDenunciaBienBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private HistoricoDocumentosSolicitadosDenunciaBienDAL vHistoricoDocumentosSolicitadosDenunciaBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="HistoricoDocumentosSolicitadosDenunciaBienBLL"/>.
        /// </summary>
        public HistoricoDocumentosSolicitadosDenunciaBienBLL()
        {
            this.vHistoricoDocumentosSolicitadosDenunciaBienDAL = new HistoricoDocumentosSolicitadosDenunciaBienDAL();
        }

        /// <summary>
        /// consulta la información de los históricos de documentos solicitados filtrados por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>lista de tipo  HistóricoDocumentosSolicitadosDenunciaBien con la información resultante de la consulta</returns>
        public List<HistoricoDocumentosSolicitadosDenunciaBien> ConsultarHistoricoDocumentosDenunciaPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoDocumentosSolicitadosDenunciaBienDAL.ConsultarHistoricoDocumentosDenunciaPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Inserta un nuevo registro en la tabla HistóricoDocumentosSolicitadosDenunciaBien
        /// </summary>
        /// <param name="pHistorico"> variable de tipo HistóricoDocumentosSolicitadosDenunciaBien con la información a insertar</param>
        /// <returns>id del registro insertado</returns>
        public int InsertarHistoricoDocumentosSolicitadosDenunciaBien(HistoricoDocumentosSolicitadosDenunciaBien pHistorico)
        {
            try
            {
                return this.vHistoricoDocumentosSolicitadosDenunciaBienDAL.InsertarHistoricoDocumentosSolicitadosDenunciaBien(pHistorico);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
