

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo HistóricoEstadosDenunciaBienBLL.
    /// </summary>
    public class HistoricoEstadosDenunciaBienBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo HistóricoEstadosDenunciaBienDAL
        /// </summary>
        private HistoricoEstadosDenunciaBienDAL vHistoricoEstadosDenunciaBienDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="HistoricoEstadosDenunciaBienBLL"/>.
        /// </summary>
        public HistoricoEstadosDenunciaBienBLL()
        {
            this.vHistoricoEstadosDenunciaBienDAL = new HistoricoEstadosDenunciaBienDAL();
        }

        /// <summary>
        /// inserta un nuevo Histórico Estados DenunciaBien
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Variable de tipo Histórico Estados DenunciaBien que contiene la información del histórico</param>
        /// <returns>id del histórico insertado</returns>
        public int InsertarHistoricoEstadosDenunciaBien(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.InsertarHistoricoEstadosDenunciaBien(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaXContratoParticipacion(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.InsertarHistoricoEstadosDenunciaXContratoParticipacion(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Hisotrico de la denuncia
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien"></param>
        /// <returns></returns>
        public int InsertarHistoricoEstadosDenunciaBienGeneral(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.InsertarHistoricoEstadosDenunciaBienGeneral(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarHistoricoEstadosDenunciaXContratoParticipacionDocumentacionRecibida(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.InsertarHistoricoEstadosDenunciaXContratoParticipacionDocumentacionRecibida(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el histórico de estados de la denuncia por el id del histórico
        /// </summary>
        /// <param name="pIdHistoricoEstadoDenunciaBien">id del histórico a consultar</param>
        /// <returns>variable de tipo Histórico Estados DenunciaBien con la información consultada</returns>
        public HistoricoEstadosDenunciaBien ConsultarHistoricoEstadosDenunciaBien(int pIdHistoricoEstadoDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.ConsultarHistoricoEstadosDenunciaBien(pIdHistoricoEstadoDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta todos los históricos de los estados 
        /// </summary>
        /// <returns>lista de tipo Histórico Estados DenunciaBien con la información consultada</returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBien()
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.ConsultarHistoricoEstadosDenunciaBien();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el historico de denuncias por ID Denuncia Bien
        /// </summary>
        /// <returns>lista de tipo Histórico Estados DenunciaBien con la información consultada</returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBienByID(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.ConsultarHistoricoEstadosDenunciaBienByID(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los históricos de los estados de una denuncia por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de la denuncia consultada </returns>
        public List<HistoricoEstadosDenunciaBien> ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.ConsultarHistoricoEstadosDenunciaBienPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta los históricos de los estados de una denuncia por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia</param>
        /// <returns>Lista de históricos de la denuncia consultada </returns>
        public List<HistoricoEstadosDenunciaBien> HistoricoEstadosDenunciaBienConsultar(int pIdDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.HistoricoEstadosDenunciaBienConsultar(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// inserta un nuevo Histórico Estados DenunciaBien
        /// </summary>
        /// <param name="pHistoricoEstadosDenunciaBien">Variable de tipo Histórico Estados DenunciaBien que contiene la información del histórico</param>
        /// <returns>id del histórico insertado</returns>
        public int InsertarHistoricoEstadosDenuncia(HistoricoEstadosDenunciaBien pHistoricoEstadosDenunciaBien)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.InsertarHistoricoEstadosDenuncia(pHistoricoEstadosDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método que valida si la denuncia ya ejecuto la Fse de "Protocolización" y la acción Comunicación Administrativa y Financiera
        /// </summary>
        /// <param name="pIdDenunciaBien">Id de la Denuncia</param>
        /// <returns>Resultadod e la operación (Bool)</returns>
        public List<HistoricoEstadosDenunciaBien> ValidaFaseAccion(int pIdDenunciaBien, int pFase, int? pAccion)
        {
            try
            {
                return this.vHistoricoEstadosDenunciaBienDAL.ValidaFaseAccion(pIdDenunciaBien, pFase, pAccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
