﻿//-----------------------------------------------------------------------
// <copyright file="TramiteJudicialBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramiteJudicialBLL.</summary>
// <author>INGENIAN</author>
// <date>19/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using System.Collections.Generic;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TramiteJudicialBLL.
    /// </summary>
    public class TramiteJudicialBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo vTramiteJudicialDAL
        /// </summary>
        private TramiteJudicialDAL vTramiteJudicialDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TramiteJudicialDAL"/>.
        /// </summary>
        public TramiteJudicialBLL()
        {
            this.vTramiteJudicialDAL = new TramiteJudicialDAL();
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Int con el ID del Tramite Registrado</returns>
        public int InsertarTramiteJudicial(TramiteJudicial vTramiteJudicial)
        {
            try
            {
                return this.vTramiteJudicialDAL.InsertarTramiteJudicial(vTramiteJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Int con el ID del Tramite Registrado</returns>
        public int InsertarDesicionAuto(int IdDenunciaBien, int idDesicionAuto)
        {
            try
            {
                return this.vTramiteJudicialDAL.InsertarDesicionAuto(IdDenunciaBien, idDesicionAuto);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Int con el ID del Tramite Registrado</returns>
        public int InsertarTipoTramite(int IdDenunciaBien, int TipoTramite)
        {
            try
            {
                return this.vTramiteJudicialDAL.InsertarTipoTramite(IdDenunciaBien, TipoTramite);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Int con el ID del Tramite Registrado</returns>
        public int InsertarDesicionAutoInadmitida(int IdDenunciaBien, int idInadmitida)
        {
            try
            {
                return this.vTramiteJudicialDAL.InsertarDesicionAutoInadmitida(IdDenunciaBien, idInadmitida);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Int con el ID del Tramite Registrado</returns>
        public int InsertarDesicionRecurso(int IdDenunciaBien, int idRecurso, bool presenta)
        {
            try
            {
                return this.vTramiteJudicialDAL.InsertarDesicionRecurso(IdDenunciaBien, idRecurso, presenta);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int UpdateTramiteJudicial(TramiteJudicial vTramiteJudicial)
        {
            try
            {
                return this.vTramiteJudicialDAL.UpdateTramiteJudicial(vTramiteJudicial);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public bool validarTramiteJudicial(int IdDenuncia)
        {
            try
            {
                return this.vTramiteJudicialDAL.validarTramiteJudicial(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Tramite Judicial
        /// </summary>
        /// <returns>Boolean, true si es insertado correctamente</returns>
        public TramiteJudicial ConsultarTramiteJudicial(int IdDenuncia)
        {
            try
            {
                return this.vTramiteJudicialDAL.ConsultarTramiteJudicial(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //consultarDesicionAutoId

        public DesicionAuto consultarDesicionAutoId(int IdDenuncia)
        {
            try
            {
                return this.vTramiteJudicialDAL.consultarDesicionAutoId(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DecisionAutoInadmitida consultarDecisionAutoInadmitidaId(int IdDenuncia)
        {
            try
            {
                return this.vTramiteJudicialDAL.consultarDecisionAutoInadmitidaId(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DecisionRecurso consultarDecisionRecursoId(int IdDenuncia)
        {
            try
            {
                return this.vTramiteJudicialDAL.consultarDecisionRecursoId(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
