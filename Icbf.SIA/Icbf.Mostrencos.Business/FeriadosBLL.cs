﻿//-----------------------------------------------------------------------
// <copyright file="FeriadosBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase FeriadosBLL.</summary>
// <author>INGENIAN</author>
// <date>16/02/2018</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;
    using System.Collections.Generic;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo TipoBienBLL.
    /// </summary>
    public class FeriadosBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo TipoBienDAL
        /// </summary>
        private FeriadosDAL vFeriadosDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="TramiteNotarialBLL"/>.
        /// </summary>
        public FeriadosBLL()
        {
            this.vFeriadosDAL = new FeriadosDAL();
        }

        /// <summary>
        /// Consulta de todos los TipoBien
        /// </summary>
        /// <returns>List de tipo TipoBien con la información consultada</returns>
        public List<Feriados> ObtenerFeriados()
        {
            try
            {
                return this.vFeriadosDAL.ObtenerFeriados();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}