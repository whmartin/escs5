﻿
namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo EstadoDocumentoBLL.
    /// </summary>
    public class EstadoDocumentoBLL
    {
        /// <summary>
        /// Variable de tipo EstadoDocumentoDAL
        /// </summary>
        private EstadoDocumentoDAL vEstadoDocumentoDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="EstadoDocumentoBLL"/>.
        /// </summary>
        public EstadoDocumentoBLL()
        {
            this.vEstadoDocumentoDAL = new EstadoDocumentoDAL();
        }

        /// <summary>
        /// Consulta el estado documento por el nombre del estado
        /// </summary>
        /// <param name="pNombreEstadoDocumento">nombre a consultar</param>
        /// <returns>variable de tipo EstadoDocumento con la información resultante de la consulta</returns>
        public EstadoDocumento ConsultarEstadoDocumentoPorNombre(string pNombreEstadoDocumento)
        {
            try
            {
                return this.vEstadoDocumentoDAL.ConsultarEstadoDocumentoPorNombre(pNombreEstadoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta el EstadoDocumento por el id del estado
        /// </summary>
        /// <param name="pIdEstadoDocumento">Id del estado que se va a consultar</param>
        /// <returns>variable de tipo EstadoDocumento con la información resultante de la consulta</returns>
        public EstadoDocumento ConsultarEstadoDocumentoPorId(int pIdEstadoDocumento)
        {
            try
            {
                return this.vEstadoDocumentoDAL.ConsultarEstadoDocumentoPorId(pIdEstadoDocumento);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
