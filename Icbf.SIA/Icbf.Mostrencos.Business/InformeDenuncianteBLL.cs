﻿using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Business
{
    public class InformeDenuncianteBLL
    {
        InformeDenuncianteDAL vInformeDenuncianteDAL = new InformeDenuncianteDAL();

        //Consultar Datos Informe Denunciante
        public List<InformeDenunciante> ConsultarDatosInformeDenunciante(int IdDenunciaBien, string FechaInicio, string FechaFinal)
        {
            try
            {
                return this.vInformeDenuncianteDAL.ConsultarDatosInformeDenunciante(IdDenunciaBien, FechaInicio, FechaFinal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Datos Informe Denunciante Por ID
        public List<InformeDenunciante> ConsultarDatosInformeDenuncianteXID(int IdInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteDAL.ConsultarDatosInformeDenuncianteXID(IdInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Datos Informe Denunciante Por ID
        public List<InformeDenunciante> ConsultarDatosInformeDenuncianteIdDenuncia(int IdDenuncia)
        {
            try
            {
                return this.vInformeDenuncianteDAL.ConsultarDatosInformeDenuncianteIdDenuncia(IdDenuncia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Validar Existencia Informe Denunciante por Fecha Informe y ID
        public List<InformeDenunciante> ValidarExistenciaDatosInformeDenunciante(int IdDenunciaBien, int IdDocumentoInforme, string FechaInforme)
        {
            try
            {
                return this.vInformeDenuncianteDAL.ValidarExistenciaDatosInformeDenunciante(IdDenunciaBien, IdDocumentoInforme, FechaInforme);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //  Insertar Informe Denunciante
        public int InsertarInformeDenunciante(InformeDenunciante pInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteDAL.InsertarInformeDenunciante(pInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Actualizar Informe Denunciante
        public int ActualizarInformeDenunciante(InformeDenunciante pInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteDAL.ActualizarInformeDenunciante(pInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Eliminar Informe Denunciante
        public int EliminarInformeDenunciante(int IdInformeDenunciante)
        {
            try
            {
                return this.vInformeDenuncianteDAL.EliminarInformeDenunciante(IdInformeDenunciante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
