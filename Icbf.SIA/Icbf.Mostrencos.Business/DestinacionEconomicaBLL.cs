﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo AnulacionDenunciaBLL.
    /// </summary>
    public class DestinacionEconomicaBLL
    {
        /// <summary>
        /// Variable de tipo vAbogadosDAL
        /// </summary>
        private DestinacionEconomicaDAL vDestinacionEconomicaDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="DestinacionEconomicaBLL"/>.
        /// </summary>
        public DestinacionEconomicaBLL()
        {
            this.vDestinacionEconomicaDAL = new DestinacionEconomicaDAL();
        }

        /// <summary>
        /// consulta los DestinacionEconomica
        /// </summary>
        /// <returns>LISTA DE DestinacionEconomica</returns>
        public List<DestinacionEconomica> ConsultarDestinacionEconomica()
        {
            try
            {
                return this.vDestinacionEconomicaDAL.ConsultarDestinacionEconomica();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
