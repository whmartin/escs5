﻿using Icbf.Mostrencos.Entity;
using Icbf.Mostrencos.DataAccess;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icbf.Mostrencos.Business
{

    public class DocumentosSolicitadosYRecibidosBLL
    {
        private DocumentosSolicitadosYRecibidosDAL vDocumentosSolicitadosYRecibidosDAL;
        public DocumentosSolicitadosYRecibidosBLL()
        {
            vDocumentosSolicitadosYRecibidosDAL = new DocumentosSolicitadosYRecibidosDAL();
        }

        //Consultar Documentos Solicitados y Recibidos
        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(int IdDenunciaBien)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.ConsultarDocumentosSolicitadosYRecibidosDenunciaBienPorId(IdDenunciaBien);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Documentos Solicitados y Recibidos
        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(int IdContratoParticipacionEconomica)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.ConsultarDocumentosSolicitadosYRecibidosContratoParticipacionEconomicaPorId(IdContratoParticipacionEconomica);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consultar Documentación Solicitada y Recibida por IdDenuncia y IdOrdenPago
        /// </summary>
        /// <param name="IdDenuncia">Parámetro IdDenuncia</param>
        /// /// <param name="IdOrdenPago">Parámetro IdOrdenPago</param>
        /// <returns>Retorna Lista de documentación </returns>
        public List<DocumentosSolicitadosYRecibidos> ConsultarDocumentosSolicitadosYRecibidosPorIdDenuncia(int IdDenuncia)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.ConsultarDocumentosSolicitadosYRecibidosPorIdDenuncia(IdDenuncia);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarDocumentosSolicitadosDenunciaBienContratos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.InsertarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosYRecibidos);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Insertar Documentos solicitados con relación de Orden de Pago
        /// </summary>
        /// <param name="pDocumentosSolicitadosYRecibidos">Objeto documento</param>
        /// <returns>Retorna el resultado de la operación</returns>
        public int InsertarDocumentosSolicitadosDenunciaBienRelacionDePagos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.InsertarDocumentosSolicitadosDenunciaBienRelacionDePagos(pDocumentosSolicitadosYRecibidos);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarDocumentosSolicitadosDenunciaBienContratos(DocumentosSolicitadosYRecibidos pDocumentosSolicitadosYRecibidos)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.ActualizarDocumentosSolicitadosDenunciaBienContratos(pDocumentosSolicitadosYRecibidos);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarDocumentosSolicitadosDenunciaBienContratos(int vIdDocumento)
        {
            try
            {
                return this.vDocumentosSolicitadosYRecibidosDAL.EliminarDocumentosSolicitadosDenunciaBienContratos(vIdDocumento);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
