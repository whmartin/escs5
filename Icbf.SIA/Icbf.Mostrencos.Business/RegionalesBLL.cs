using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{
    public class RegionalesBLL
    {
        private RegionalesDAL vRegionalesDAL;
        public RegionalesBLL()
        {
            vRegionalesDAL = new RegionalesDAL();
        }

        public List<Regionales> ConsultarRegionales()
        {
            try
            {
                return vRegionalesDAL.ConsultarRegionales();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Regionales ConsultarRegionalesPorID(String idRegional)
        {
            try
            {
                return vRegionalesDAL.ConsultarRegionalesPorId(idRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Regionales> ConsultarRegionalAll()
        {
            try
            {
                return this.vRegionalesDAL.ConsultarRegionalesAll();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
