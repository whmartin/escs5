﻿using System;
using System.Collections.Generic;
using Icbf.Mostrencos.DataAccess;
using Icbf.Mostrencos.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.Mostrencos.Business
{

    public class ContratoParticipacionEconomicaBLL
    {
        ContratoParticipacionEconomicaDAL vContratoDenunciaBienDAL = new ContratoParticipacionEconomicaDAL();

        public List<ContratoParticipacionEconomica> ConsultarContratoXDenuncia(int IdDenunciaBien)
        {
            try
            {
                return this.vContratoDenunciaBienDAL.ConsultarContratosXDenuncia(IdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoParticipacionEconomica> ConsultarExistenciaContratoXDenuncia(int IdDenunciaBien, int IdContrato)
        {
            try
            {
                return this.vContratoDenunciaBienDAL.ConsultarExistenciaContratosXDenuncia(IdDenunciaBien, IdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoParticipacionEconomica> ConsultarContratoXDenunciaMultiplesParametros(int IdVigencia, int idRegional, string NumeroContrato)
        {
            try
            {
                return this.vContratoDenunciaBienDAL.ConsultarContratoXDenunciaMultiplesParametros(IdVigencia, idRegional, NumeroContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        //Consultar Información de Contrato
        public List<ContratoParticipacionEconomica> ConsultarInformacionContrato(int IdDenuncia, int IdContrato)
        {
            try
            {
                List<ContratoParticipacionEconomica> vListaContratoDenunciaBien = vContratoDenunciaBienDAL.ConsultarInformacionContrato(IdDenuncia, IdContrato);

                return vListaContratoDenunciaBien;
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarContratoParticipacionEconomica(ContratoParticipacionEconomica pContratoParticipacionEconomica)
        {
            try
            {
                return this.vContratoDenunciaBienDAL.InsertarContratoParticipacionEconomica(pContratoParticipacionEconomica);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}

