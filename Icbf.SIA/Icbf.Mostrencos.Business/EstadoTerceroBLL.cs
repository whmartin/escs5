﻿//-----------------------------------------------------------------------
// <copyright file="EstadoTerceroBLL.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase EstadoTerceroBLL.</summary>
// <author>INDESAP[Jesus Eduardo Cortes]</author>
// <date>26/09/2017</date>
//-----------------------------------------------------------------------

namespace Icbf.Mostrencos.Business
{
    using System;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo EstadoTerceroBLL.
    /// </summary>
    public class EstadoTerceroBLL
    {
        /// <summary>
        /// Inicialización de un objeto tipo vEstadoTerceroDAL
        /// </summary>
        private EstadoTerceroDAL vEstadoTerceroDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="EstadoTerceroBLL"/>.
        /// </summary>
        public EstadoTerceroBLL()
        {
            this.vEstadoTerceroDAL = new EstadoTerceroDAL();
        }

        /// <summary>
        /// Consulta el estado de un tercero por el id estado
        /// </summary>
        /// <param name="pIdEstadoTercero">Id del estado del tercero</param>
        /// <returns>Variable de tipo Estado tercero con la información encontrada</returns>
        public EstadoTercero ConsultarEstadoTerceroXId(int pIdEstadoTercero)
        {
            try
            {
                return this.vEstadoTerceroDAL.ConsultarEstadoTerceroXId(pIdEstadoTercero);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
