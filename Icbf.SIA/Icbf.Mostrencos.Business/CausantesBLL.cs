﻿

namespace Icbf.Mostrencos.Business
{
    using System;
    using System.Collections.Generic;
    using Icbf.Mostrencos.DataAccess;
    using Icbf.Mostrencos.Entity;
    using Icbf.Utilities.Exceptions;

    /// <summary>
    /// Clase para manejar la lógica de negocio del modulo CausantesBLL.
    /// </summary>
    public class CausantesBLL
    {
        /// <summary>
        /// Variable de tipo vCausantesDAL
        /// </summary>
        private CausantesDAL vCausantesDAL;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="CausantesBLL"/>.
        /// </summary>
        public CausantesBLL()
        {
            this.vCausantesDAL = new CausantesDAL();
        }

        /// <summary>
        /// Inserta la información de un causante
        /// </summary>
        /// <param name="pCausante">Variable de tipo causante que contiene la información que se va a insertar</param>
        /// <returns>Id del causante insertado</returns>
        public int InsertarCausante(Causantes pCausante)
        {
            try
            {
                return this.vCausantesDAL.InsertarCausante(pCausante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// consulta la información de un causante por el id causante
        /// </summary>
        /// <param name="pIdCaudante">Id causante que se va a consultar</param>
        /// <returns>Variable de tipo causante que contiene los datos de la consulta</returns>
        public Causantes ConsultarCausantesPorIdCaudante(int pIdCaudante)
        {
            try
            {
                return this.vCausantesDAL.ConsultarCausantesPorIdCaudante(pIdCaudante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Consulta la información de un causante por el id de la denuncia
        /// </summary>
        /// <param name="pIdDenunciaBien">id de la denuncia a consultar</param>
        /// <returns>lista de tipo Causantes con la información resultante de la consulta</returns>
        public List<Causantes> ConsultarCausantesPorIdDenunciaBien(int pIdDenunciaBien)
        {
            try
            {
                return this.vCausantesDAL.ConsultarCausantesPorIdDenunciaBien(pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Edita la información de un causante
        /// </summary>
        /// <param name="pCausante">Variable de tipo causante que contiene la información que se va a insertar</param>
        /// <returns>1 si la modificación fue exitosa</returns>
        public int EditarCausante(Causantes pCausante)
        {
            try
            {
                return this.vCausantesDAL.EditarCausante(pCausante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Valida si ya existe un Causante con el mismo id tercero y id denuncia bien
        /// </summary>
        /// <param name="pIdTercero">Id del tercero</param>
        /// <param name="pIdDenunciaBien"> id de la denuncia bien</param>
        /// <returns>true si existe o false si no existe</returns>
        public bool ValidarDuplicidad(int pIdTercero, int pIdDenunciaBien)
        {
            try
            {
                return this.vCausantesDAL.ValidarDuplicidad(pIdTercero, pIdDenunciaBien);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
