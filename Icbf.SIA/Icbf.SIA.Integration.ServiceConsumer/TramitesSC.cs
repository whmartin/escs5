﻿//-----------------------------------------------------------------------
// <copyright file="TramitesSC.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase TramitesSC.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Icbf.SIA.Entity;
using Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer;
using ICBF.Parametricas.ClientObjectModel40;

namespace Icbf.SIA.Integration.ServiceConsumer
{
    /// <summary>
    /// Clase que se enlaza con la capa del Client Object Model
    /// </summary>
    public class TramitesSC: ITramiteSC
    {
        /// <summary>
        /// Variable de tipo Client Object Model
        /// </summary>
        private TramiteCOM vTramiteCOM;

        /// <summary>
        /// Clase que inicializa la variable de tipo Client Object Model
        /// </summary>
        public TramitesSC()
        {
            vTramiteCOM = new TramiteCOM();
        }

        /// <summary>
        /// Obtener una lista de tramites
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>Lista de tramites</returns>
        public List<Tramite> GetTramite(int pId)
        {
            List<Tramite> vTramite = vTramiteCOM.GetTramite<Tramite>(pId);
            return vTramite;
        }
    }
}
