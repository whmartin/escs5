﻿//-----------------------------------------------------------------------
// <copyright file="SistemaInformacionSC.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase SistemaInformacionSC.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------
using System.Collections.Generic;
using Icbf.SIA.Entity;
using Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer;
using ICBF.Parametricas.ClientObjectModel40;

namespace Icbf.SIA.Integration.ServiceConsumer
{
    /// <summary>
    /// Clase que Enlaza el service consumer al  Client Object Model
    /// </summary>
    public class SistemaInformacionSC:ISistemaInformacionSC
    {
        /// <summary>
        /// Variable de tipo  Client Object Model
        /// </summary>
        private SistemaInformacionCOM vSistemaInformacionCOM;

        /// <summary>
        /// Clase que inicializa la variable de tipo  Client Object Model
        /// </summary>
        public SistemaInformacionSC()
        {
            this.vSistemaInformacionCOM = new SistemaInformacionCOM();
        }

        /// <summary>
        /// Metodo para obtener una lista de Sistemas de información.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns>Lista de sistemas de información</returns>
        public List<SistemaInformacion> GetSistemaInformacion(int pId)
        {
            List<SistemaInformacion> vSistemaInformacion = this.vSistemaInformacionCOM.GetSistemaInformacion<SistemaInformacion>(pId);
            return vSistemaInformacion;
        }
    }
}
