﻿using System.Collections.Generic;
using Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer;
using ICBF.Parametricas.ClientObjectModel40;
using System.Configuration;
//using ICBF.Parametricas.ClientObjectModel;
using Icbf.SIA.Entity;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System;
//using ICBF.Parametricas.ClientObjectModel;

namespace Icbf.SIA.Integration.ServiceConsumer
{
    /// <summary>
    /// Clase que implementa los métodos para la consulta de paramétricas
    /// </summary>
    public class ConsultaNotariasSC : IConsultaNotariasSC
    {
        /// <summary>
        /// Instancia de objeto client.
        /// </summary>
        private HttpClient client;

        /// <summary>
        /// Url del WebApi
        /// </summary>
        private string vURL = ConfigurationManager.AppSettings["URLWebApi"];

        /// <summary>
        /// Variable de tipo  Client Object Model
        /// </summary>
        //private ICBF.Parametricas.ClientObjectModel.ParametricasCOM vParametros;

        /// <summary>
        /// Url del WebApi de Paramétricas
        /// </summary>
        //private string vURLParametricas = ConfigurationManager.AppSettings["URLWebParametricas"];

        /// <summary>
        ///  Inicializa una nueva instancia de la clase <see cref="ConsultaNotariasSC" />.
        /// </summary>

        public ConsultaNotariasSC()
        {
            //this.vParametros = new ParametricasCOM("http://172.17.133.132/PublicacionTFSDesarrolloParametricas/");
            this.client = new HttpClient();
            this.client.BaseAddress = new Uri(this.vURL);
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public List<Notaria> GetNotarias(string pCodigo)
        {
            RegistroParametrica objRegistroParametrica = new RegistroParametrica();
            List<RegistroParametrica> ListRegistroParametrica = new List<RegistroParametrica>();
            Notaria objNotaria = new Notaria();
            List<Notaria> listNotarias = new List<Notaria>();
            RespuestaParametrica objRespuestaParametrica = new RespuestaParametrica();
            //ICBF.Parametricas.ClientObjectModel.ParametricasCOM parametros = new ParametricasCOM(this.vURLParametricas);
            //ICBF.Parametricas.ClientObjectModel.ParametricasCOM parametros = new ParametricasCOM("http://172.17.133.132/PublicacionTFSDesarrolloParametricas/");
            //List<RegistroParametrica> lstParametricas = vParametros.ConsultarParametrica<RegistroParametrica>(pCodigo, "_");
            //List<Notarias> listNotarias = this.vParametricasCOM.GetTramite(2);
            //string jsonContent = JsonConvert.SerializeObject(request);

            //byte[] buffer = System.Text.Encoding.UTF8.GetBytes(jsonContent);

            //ByteArrayContent byteContent = new ByteArrayContent(buffer);
            //byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = this.client.GetAsync("NOTARIAS/_").Result;
            if (response.IsSuccessStatusCode)
            {
                string prueba = response.Content.ReadAsStringAsync().Result;
                objRespuestaParametrica = JsonConvert.DeserializeObject<RespuestaParametrica>(prueba);

                foreach (ContenidoRespuesta itemUno in objRespuestaParametrica.ContenidoRespuesta)
                {
                    foreach (RegistroParametrica item in itemUno.ListRegistroParametrica)
                    {
                        objRegistroParametrica.IdRegistro = item.IdRegistro;
                        objRegistroParametrica.IdParametrica = item.IdParametrica;
                        objRegistroParametrica.CodigoRegistro = item.CodigoRegistro;
                        objRegistroParametrica.NombreRegistro = item.NombreRegistro;
                        objRegistroParametrica.DescripcionRegistro = item.DescripcionRegistro;
                        objRegistroParametrica.EstadoRegistro = item.EstadoRegistro;
                        objRegistroParametrica.Campo0 = item.Campo0;
                        objRegistroParametrica.Campo1 = item.Campo1;
                        objRegistroParametrica.Campo2 = item.Campo2;
                        objRegistroParametrica.Campo3 = item.Campo3;
                        objRegistroParametrica.Campo4 = item.Campo4;
                        objRegistroParametrica.Campo5 = item.Campo5;
                        objRegistroParametrica.Campo6 = item.Campo6;
                        objRegistroParametrica.Campo7 = item.Campo7;
                        objRegistroParametrica.Campo8 = item.Campo8;
                        objRegistroParametrica.Campo9 = item.Campo9;
                        ListRegistroParametrica.Add(objRegistroParametrica);
                    }
                }
            }

            foreach (var item in ListRegistroParametrica)
            {
                objNotaria.DepartamentoNotaria = item.Campo0;
                objNotaria.CiudadNotaria = item.Campo1;
                objNotaria.CodNotaria = item.CodigoRegistro;
                objNotaria.NombreNotaria = item.NombreRegistro;
                listNotarias.Add(objNotaria);

            }

            return listNotarias;
        }

    }
}
