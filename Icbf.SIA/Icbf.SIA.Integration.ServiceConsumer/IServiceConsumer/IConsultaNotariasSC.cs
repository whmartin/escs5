﻿using System.Collections.Generic;
using Icbf.SIA.Entity;

namespace Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer
{
    /// <summary>
    /// Interface que tiene una lista de las Notarias
    /// </summary>
    public interface IConsultaNotariasSC
    {
        List<Notaria> GetNotarias(string pCodigo);
    }
}
