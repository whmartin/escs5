﻿//-----------------------------------------------------------------------
// <copyright file="ISistemaInformacionSC.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ISistemaInformacionSC.</summary>
// <author>INGENIAN SOFTWARE SAS.</author>
// <date>20/12/2017</date>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Icbf.SIA.Entity;

namespace Icbf.SIA.Integration.ServiceConsumer.IServiceConsumer
{
    /// <summary>
    /// Interface que tiene una lista de tramites
    /// </summary>
    public interface ISistemaInformacionSC
    {
        List<SistemaInformacion> GetSistemaInformacion(int pId);
    }
}
