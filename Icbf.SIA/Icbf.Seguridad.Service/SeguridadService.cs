﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Business;
using Icbf.Seguridad.Entity;
using Icbf.SEG.Business;
using System.Data;
using Icbf.Mostrencos.Entity;
using Icbf.Mostrencos.Business;
using Icbf.SIA.Business;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

namespace Icbf.Seguridad.Service
{
    public class SeguridadService
    {
        private PermisoBLL vPermisoBLL;
        
        private Icbf.Seguridad.Business.RolBLL vRolBLL;
        private MembershipRolesBLL vMembershipRolesBLL;
        private ProgramaBLL vProgramaBLL;
        private ModuloBLL vModuloBll;
        private AuditoriaBLL vAuditoriaBLL;
        private ParametroBLL vParametroBLL;


        /// <summary>
        /// Inicialización de un objeto tipo vAuditoriaBLL
        /// </summary>
        private ProgramaFuncionBLL vProgramaFuncionBLL;

        /// <summary>
        /// Inicialización de un objeto tipo vAuditoriaBLL
        /// </summary>
        private ProgramaFuncionRolBLL vProgramaFuncionRolBLL;


        public SeguridadService()
        {
            vPermisoBLL = new PermisoBLL();
            vRolBLL = new Icbf.Seguridad.Business.RolBLL();
            vMembershipRolesBLL = new MembershipRolesBLL();
            vProgramaBLL = new ProgramaBLL();
            vModuloBll = new ModuloBLL();
            vAuditoriaBLL = new AuditoriaBLL();
            vParametroBLL = new ParametroBLL();

            vProgramaFuncionBLL = new ProgramaFuncionBLL();
            vProgramaFuncionRolBLL = new ProgramaFuncionRolBLL();
        }
        public int InsertarPermiso(Permiso pPermiso)
        {
            try
            {
                return vPermisoBLL.InsertarPermiso(pPermiso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPermiso(Permiso pPermiso)
        {
            try
            {
                return vPermisoBLL.ModificarPermiso(pPermiso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPermiso(int pIdPermiso)
        {
            try
            {
                return vPermisoBLL.EliminarPermiso(pIdPermiso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarRol(Rol pRol)
        {
            try
            {
                return vRolBLL.InsertarRol(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRol(Rol pRol)
        {
            try
            {
                return vRolBLL.ModificarRol(pRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol ConsultarRol(string pProviderKey)
        {
            try
            {
                return vRolBLL.ConsultarRol(pProviderKey);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Rol ConsultarRol(int pIdRol)
        {
            try
            {
                return vRolBLL.ConsultarRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Rol> ConsultarRoles(string pNombreRol, Boolean? pEstadoRol)
        {
            try
            {
                return vRolBLL.ConsultarRoles(pNombreRol, pEstadoRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public bool ConsultarRolesNombreEsAdmin(string pNombreRol, Boolean? pEstadoRol)
        {
            try
            {
                return vRolBLL.ConsultarRolesNombreEsAdmin(pNombreRol, pEstadoRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Programa> ConsultarProgramasRol(int pIdRol)
        {
            try
            {
                return vRolBLL.ConsultarProgramasAsignados(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasSinAsignarRol(int pIdRol)
        {
            try
            {
                return vRolBLL.ConsultarProgramasSinAsignar(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Permiso> ConsultarPermisosRol(int pIdRol)
        {
            try
            {
                return vRolBLL.ConsultarPermisosRol(pIdRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasConPermiso(String pNombreRol)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramasConPermiso(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Programa> ConsultarProgramasConPermiso(String[] pNombreRol)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramasConPermiso(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarModulo(Modulo pIdModulo)
        {
            try
            {
                return vModuloBll.InsertarModulo(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModulo(Modulo pIdModulo)
        {
            try
            {
                return vModuloBll.ModificarModulo(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModulo(Modulo pModulo)
        {
            try
            {
                return vModuloBll.EliminarModulo(pModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarModulo(int pIdModulo)
        {
            try
            {
                return vModuloBll.ConsultarModulo(pIdModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModuloPorNombre(string pNombreModulo)
        {
            try
            {
                return vModuloBll.ConsultarModuloPorNombre(pNombreModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModulosRol(string pNombreRol)
        {
            try
            {
                return vModuloBll.ConsultarModulosRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarPermisosRol(string pNombreRol)
        {
            try
            {
                return vModuloBll.ConsultarPermisosRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarPermisosRol(string[] pNombreRol)
        {
            try
            {
                return vModuloBll.ConsultarPermisosRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Modulo> ConsultarModulos(string pNombreModulo, Boolean? pEstado)
        {
            try
            {
                return vModuloBll.ConsultarModulos(pNombreModulo, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Modulo ConsultarTodosModulos()
        {
            try
            {
                return vModuloBll.ConsultarTodosModulos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int InsertarPrograma(Programa pPrograma)
        {
            try
            {
                return vProgramaBLL.InsertarPrograma(pPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarPrograma(Programa pPrograma)
        {
            try
            {
                return vProgramaBLL.ModificarPrograma(pPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarPrograma(Programa pPrograma)
        {
            try
            {
                return vProgramaBLL.EliminarPrograma(pPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Programa ConsultarPrograma(int pIdPrograma)
        {
            try
            {
                return vProgramaBLL.ConsultarPrograma(pIdPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramas(int pIdModulo, String pNombreModulo)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramas(pIdModulo, pNombreModulo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasPermitidos(int pIdModulo, String pNombreRol)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramasPermitidos(pIdModulo, pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasHijosPermitidosPermiso(int pIdModulo, String pNombreRol, int? idPadre)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramasHijosPermitidosPermiso(pIdModulo, pNombreRol, idPadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Programa> ConsultarProgramasHijosPermitidosPermiso(int pIdModulo, String[] pNombreRol, int? idPadre)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramasHijosPermitidosPermiso(pIdModulo, pNombreRol, idPadre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Programa> ConsultarProgramasPermitidosPermiso(int pIdModulo, String pNombreRol)
        {
            try
            {
                return vProgramaBLL.ConsultarProgramasPermitidosPermiso(pIdModulo, pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Perfil> ConsultarTodosPerfiles()
        {
            try
            {
                PerfilBLL vPerfilBLL = new PerfilBLL();
                return vPerfilBLL.ConsultarTodosPerfil();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<Auditoria> ConsultarAuditoria(String pNombrePrograma, Double pIdRegistro)
        {
            try
            {
                return vAuditoriaBLL.ConsultarAuditoria(pNombrePrograma, pIdRegistro);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public int InsertarParametro(Parametro pParametro)
        {
            try
            {
                return vParametroBLL.InsertarParametro(pParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ModificarParametro(Parametro pParametro)
        {
            try
            {
                return vParametroBLL.ModificarParametro(pParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarParametro(Parametro pParametro)
        {
            try
            {
                return vParametroBLL.EliminarParametro(pParametro);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Parametro ConsultarParametro(int pIdParametro)
        {
            try
            {
                return vParametroBLL.ConsultarParametro(pIdParametro);
            }
           
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Parametro> ConsultarParametros(String pNombreParametro, String pValorParametro, Boolean? pEstado, string pFuncionalidad)
        {
            try
            {
                return vParametroBLL.ConsultarParametros(pNombreParametro, pValorParametro, pEstado, pFuncionalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region"FuncionRol"

        /// <summary>
        /// Método para Consultar Programa Funcion por Id
        /// </summary>
        /// <param name="pIdProgramaFuncion">Id del Programa Funcion a filtrar</param>
        /// <returns>Programa Funcion consultado</returns>public ProgramaFuncion
        public ProgramaFuncion ConsultarProgramaFuncion(int pIdProgramaFuncion)
        {
            try
            {
                return vProgramaFuncionBLL.ConsultarProgramaFuncion(pIdProgramaFuncion);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas de Funcion
        /// </summary>
        /// <param name="pIdPrograma">Id Programa a filtrar</param>
        /// <param name="pNombreFuncion">Nombre Funcion a filtrar</param>
        /// <param name="pEstado">Estado a filtrar</param>
        /// <returns>Lista de Programa Funcion</returns>public List
        public List<ProgramaFuncion> ConsultarProgramaFuncions(int? pIdPrograma, string pNombreFuncion, int? pEstado)
        {
            try
            {
                return vProgramaFuncionBLL.ConsultarProgramaFuncions(pIdPrograma, pNombreFuncion, pEstado);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Eliminar Programa Funcion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int 
        public int EliminarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                return vProgramaFuncionBLL.EliminarProgramaFuncion(pProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar un Programa Funcion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int 
        public int ModificarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                return vProgramaFuncionBLL.ModificarProgramaFuncion(pProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Insertar un ProgramaFuncion
        /// </summary>
        /// <param name="pProgramaFuncion">Entidad ProgramaFuncion a filtrar</param>
        /// <returns>Id del ProgramaFuncion insertado</returns>public int 
        public int InsertarProgramaFuncion(ProgramaFuncion pProgramaFuncion)
        {
            try
            {
                return vProgramaFuncionBLL.InsertarProgramaFuncion(pProgramaFuncion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas
        /// </summary>
        /// <returns>Lista de Programas</returns>public List
        public List<Programa> ConsultarProgramas()
        {
            try
            {
                return vProgramaFuncionBLL.ConsultarProgramas();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas del Modulo Denuncias
        /// </summary>
        /// <returns>Lista de Programas consultados</returns>public List
        public List<Programa> ConsultarProgramasDenuncias()
        {
            try
            {
                return vProgramaFuncionBLL.ConsultarProgramasDenuncias();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programa Funcion Rol
        /// </summary>
        /// <param name="pIdProgramaFuncionRol">Id ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public ProgramaFuncionRol
        public ProgramaFuncionRol ConsultarProgramaFuncionRol(int pIdProgramaFuncionRol)
        {
            try
            {
                return vProgramaFuncionRolBLL.ConsultarProgramaFuncionRol(pIdProgramaFuncionRol);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas Funcion Roles
        /// </summary>
        /// <param name="pIdPrograma">Id Programa a filtrar</param>
        /// <param name="pIdProgramaFuncion">Id Programa Funcion a filtrar</param>
        /// <param name="pIdRol">Id Rol a filtrar</param>
        /// <param name="pEstado">Estado a filtrar</param>
        /// <returns>Lista de Programas Funcion Roles consultado</returns>public List
        public List<ProgramaFuncionRol> ConsultarProgramaFuncionRols(int? pIdPrograma, int? pIdProgramaFuncion, int? pIdRol, int? pEstado)
        {
            try
            {
                return vProgramaFuncionRolBLL.ConsultarProgramaFuncionRols(pIdPrograma, pIdProgramaFuncion, pIdRol, pEstado);
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Eliminar Programa Funcion Rol por Id
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int
        public int EliminarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                return vProgramaFuncionRolBLL.EliminarProgramaFuncionRol(pProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Modificar Programa Funcion Rol
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Resultado de la cantidad de filas afectadas</returns>public int
        public int ModificarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                return vProgramaFuncionRolBLL.ModificarProgramaFuncionRol(pProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Insertar Programa Funcion Rol
        /// </summary>
        /// <param name="pProgramaFuncionRol">Entidad ProgramaFuncionRol a filtrar</param>
        /// <returns>Id del Programa Funcion Rol insertado</returns>public int
        public int InsertarProgramaFuncionRol(ProgramaFuncionRol pProgramaFuncionRol)
        {
            try
            {
                return vProgramaFuncionRolBLL.InsertarProgramaFuncionRol(pProgramaFuncionRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Roles
        /// </summary>
        /// <returns>Lista Rol consultada</returns>public List
        public List<Rol> ConsultarRoles()
        {
            try
            {
                return vProgramaFuncionRolBLL.ConsultarRoles();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para Consultar Programas Funcion por Programa
        /// </summary>
        /// <param name="pIdPrograma">Id del Programa a filtrar</param>
        /// <returns>Lista de ProgramaFuncion consultada</returns>public List
        public List<ProgramaFuncion> ConsultarProgramasFuncionPorPrograma(int pIdPrograma)
        {
            try
            {
                return vProgramaFuncionRolBLL.ConsultarProgramasFuncionPorPrograma(pIdPrograma);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Consulta de las funciones de una página a las que tiene acceso un rol
        /// </summary>
        /// <param name="pRolNombre">Nombre del Rol</param>
        /// <param name="pCodigoPrograma">Nombre en el code behind de la página</param>
        /// <returns>Lista de ProgramaFuncion</returns>public List
        public List<ProgramaFuncion> ConsultarUsuarioProgramaFuncionRol(string pRolNombre, string pCodigoPrograma)
        {
            try
            {
                try
                {
                    return new ProgramaFuncionBLL().ConsultarUsuarioProgramaFuncionRol(pRolNombre, pCodigoPrograma);
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para la Consulta de las funciones de una página a las que tiene acceso un rol
        /// </summary>
        /// <param name="pRolNombre">Nombre del Rol</param>
        /// <param name="pCodigoPrograma">Nombre en el code behind de la página</param>
        /// <returns>Lista de ProgramaFuncion</returns>public List
        public List<ProgramaFuncion> ConsultarUsuarioProgramaFuncionRolComisiones(string pRolNombre, string pCodigoPrograma)
        {
            try
            {
                try
                {
                    return new ProgramaFuncionBLL().ConsultarUsuarioProgramaFuncionRolComisiones(pRolNombre, pCodigoPrograma);
                }
                catch (UserInterfaceException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new GenericException(ex);
                }
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion"FuncionRol"

        public string ConsultarUsuarioPorRol(string pNombreRol)
        {
            try
            {
                return vRolBLL.ConsultarUsuarioPorRol(pNombreRol);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
    }
}
