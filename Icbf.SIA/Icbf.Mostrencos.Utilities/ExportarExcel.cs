﻿//-----------------------------------------------------------------------
// <copyright file="BooleanExtensions" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase BooleanExtensions.</summary>
// <author>ICBF</author>
// <date>18/02/2016 0355 PM</date>
//-----------------------------------------------------------------------

using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using System.Web;


/// <summary>
/// Classs and description for Boolean Extensions
/// </summary>
public static class BooleanExtensions
{
    /// <summary>
    /// Método ToString
    /// </summary>
    /// <param name="value">Value a filtrar</param>
    /// <param name="format">Format a filtrar</param>
    /// <returns>Cadena generada</returns> public static string
    public static string ToString(this bool value, string format)
    {
        format = format ?? string.Empty;
        return string.Format(new BoolFormatProvider(), format.StartsWith("{0:") ? format : "{0:" + format + "}", value);
    }
}

/// <summary>
/// Clase BoolFormatProvider
/// </summary>
public class BoolFormatProvider : IFormatProvider, ICustomFormatter
{
    #region ICustomFormatter Members

    /// <summary>
    /// Método Format
    /// </summary>
    /// <param name="format">Format a filtrar</param>
    /// <param name="arg">The arg a filtrar</param>
    /// <param name="formatProvider">The format Provider a filtrar</param>
    /// <returns>Cadena generada</returns>Format
    public string Format(string format, object arg, IFormatProvider formatProvider)
    {
        bool value = (bool)arg;
        format = (format == null ? null : format.Trim().ToLower());
        System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
        bool es = ci.Name.Split('-')[0].Equals("es");

        switch (format)
        {
            case "yn":
                return value ? es ? "Si" : "Yes" : "No";
            case "ai":
                return value ? es ? "Activo" : "Active" : es ? "Inactivo" : "Inactive";
            default:
                if (arg is IFormattable)
                    return ((IFormattable)arg).ToString(format, formatProvider);
                else
                    return arg.ToString();
        }
    }

    #endregion

    #region IFormatProvider Members

    /// <summary>
    /// Método GetFormat
    /// </summary>
    /// <param name="formatType">The format Type a filtrar</param>
    /// <returns>Valor obtenido</returns>public object
    public object GetFormat(Type formatType)
    {
        if (formatType == typeof(ICustomFormatter))
            return this;
        else
            return null;
    }

    #endregion
}

/// <summary>
/// Class and description for Asistente Excel
/// </summary>
public class AsistenteExcel
{
    /// <summary>
    /// Clase struct InformacionCelda
    /// </summary>
    public struct InformacionCelda
    {
        /// <summary>
        /// La Columna
        /// </summary>
        public string Columna;

        /// <summary>
        /// El Titulo
        /// </summary>
        public string Titulo;

        /// <summary>
        /// El Formato
        /// </summary>
        public string Formato;

        /// <summary>
        /// El Estilo
        /// </summary>
        public string Estilo;

        /// <summary>
        /// Valor Verdadero
        /// </summary>
        public string ValorVerdadero;

        /// <summary>
        /// Valor Falso
        /// </summary>
        public string ValorFalso;
    }

    /// <summary>
    /// Método ConvertirListaEnTextoHtml.
    /// </summary>
    /// <typeparam name="T">T parametro a filtrar</typeparam>
    /// <param name="lista">Lista a filtrar</param>
    /// <param name="formatos">Formatos a filtrar</param>
    /// <returns>Cadena operada</returns>public static string
    public static string ConvertirListaEnTextoHtml<T>(List<T> lista, params InformacionCelda[] formatos) where T : Icbf.Seguridad.Entity.EntityAuditoria, new()
    {
        StringBuilder sb = new StringBuilder();
        if (lista != null && formatos != null && formatos.Length > 0)
        {
            PropertyInfo[] propiedades = typeof(T).GetProperties(BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);

            for (int i = 0; i < lista.Count; i++)
            {
                if (i == 0)
                {
                    sb.Append("<html><head></head><body><table><thead><tr>");
                    for (int j = 0; j < formatos.Length; j++)
                    {
                        if (!string.IsNullOrEmpty(formatos[j].Columna))
                        {
                            PropertyInfo propiedad = Array.Find(
                                propiedades,
                                p => p.Name.Equals(formatos[j].Columna, StringComparison.InvariantCultureIgnoreCase));
                            if (propiedad != null)
                                sb.AppendFormat("<th>{0}</th>", !string.IsNullOrEmpty(formatos[j].Titulo) ? formatos[j].Titulo : formatos[j].Columna);
                        }
                    }

                    sb.Append("</tr></thead><tbody>");
                }

                sb.Append("<tr>");
                for (int j = 0; j < formatos.Length; j++)
                {
                    PropertyInfo propiedad = Array.Find(propiedades, p => p.Name.Equals(formatos[j].Columna, StringComparison.InvariantCultureIgnoreCase));

                    if (propiedad != null)
                    {
                        object valor = propiedad.GetValue(lista[i], new object[] { });
                        sb.AppendFormat(
                            "<td{0}>{1}</td>",
                            !string.IsNullOrEmpty(
                                formatos[j].Estilo)
                                ? string.Format(
                                " style=\"{0}\"",
                                HttpUtility.HtmlAttributeEncode(
                                formatos[j].Estilo)) : string.Empty,
                            HttpUtility.HtmlEncode(valor is bool
                                ? ((bool)valor).ToString(!string.IsNullOrEmpty(formatos[j].Formato) ? formatos[j].Formato : "ai")
                                : valor != null
                                    ? valor is IFormattable && !string.IsNullOrEmpty(formatos[j].Formato)
                                        ? (valor as IFormattable).ToString(formatos[j].Formato, null) : valor.ToString()
                                    : string.Empty));
                    }
                }

                sb.Append("</tr>");
                if (i == lista.Count - 1)
                    sb.Append("</tbody></table></body></html>");
            }
        }

        return sb.ToString();
    }

    /// <summary>
    /// Método ConvertirListaEnTextoHtml.
    /// </summary>
    /// <typeparam name="T">T parametro a filtrar</typeparam>
    /// <param name="lista">Lista a filtrar</param>
    /// <param name="formatos">Formatos a filtrar</param>
    /// <returns>Cadena operada</returns>public static string
    public static DataTable ConvertirListaEnDataTable<T>(List<T> lista, params InformacionCelda[] formatos) where T : Icbf.Seguridad.Entity.EntityAuditoria, new()
    {
        DataTable dtExcel = new DataTable();

        if (lista != null && formatos != null && formatos.Length > 0)
        {
            PropertyInfo[] propiedades = typeof(T).GetProperties(BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);

            for (int j = 0; j < formatos.Length; j++)
            {
                if (!string.IsNullOrEmpty(formatos[j].Columna))
                {
                    PropertyInfo propiedad = Array.Find(propiedades, p => p.Name.Equals(formatos[j].Columna, StringComparison.InvariantCultureIgnoreCase));

                    if (propiedad != null)
                    {
                        Type vType = Type.GetType("System.String");

                        if (string.IsNullOrEmpty(formatos[j].ValorFalso))
                        {
                            if (propiedad.PropertyType.FullName.ToUpper() == "System.String".ToUpper())
                            {
                                vType = Type.GetType("System.String");
                            }
                            else if (propiedad.PropertyType.FullName.ToUpper() == "System.Int32".ToUpper() && string.IsNullOrEmpty(formatos[j].Formato))
                            {
                                vType = Type.GetType("System.Int32");
                            }
                            else if (propiedad.PropertyType.FullName.ToUpper() == "System.Boolean".ToUpper() && string.IsNullOrEmpty(formatos[j].Formato))
                            {
                                vType = Type.GetType("System.Boolean");
                            }
                            else if (propiedad.PropertyType.FullName.ToUpper() == "System.Double".ToUpper() && string.IsNullOrEmpty(formatos[j].Formato))
                            {
                                vType = Type.GetType("System.Double");
                            }
                            else if (propiedad.PropertyType.FullName.ToUpper() == "System.Single".ToUpper() && string.IsNullOrEmpty(formatos[j].Formato))
                            {
                                vType = Type.GetType("System.Single");
                            }
                            else if (propiedad.PropertyType.FullName.ToUpper() == "System.DateTime".ToUpper() && string.IsNullOrEmpty(formatos[j].Formato))
                            {
                                vType = Type.GetType("System.DateTime");
                            }
                            else if (propiedad.PropertyType.FullName.ToUpper() == "System.Decimal".ToUpper() && string.IsNullOrEmpty(formatos[j].Formato))
                            {
                                vType = Type.GetType("System.Decimal");
                            }
                        }

                        dtExcel.Columns.Add(formatos[j].Columna, vType);
                    }
                }
            }

            for (int i = 0; i < lista.Count; i++)
            {
                if (i == 0)
                {
                    DataRow rowHeader = dtExcel.NewRow();

                    for (int j = 0; j < formatos.Length; j++)
                    {
                        if (!string.IsNullOrEmpty(formatos[j].Columna))
                        {
                            StringBuilder sbHeader = new StringBuilder();
                            PropertyInfo propiedad = Array.Find(propiedades, p => p.Name.Equals(formatos[j].Columna, StringComparison.InvariantCultureIgnoreCase));

                            if (propiedad != null)
                            {
                                object valor = !string.IsNullOrEmpty(formatos[j].Titulo) ? formatos[j].Titulo : formatos[j].Columna;

                                sbHeader.AppendFormat(
                               "{1}",
                               !string.IsNullOrEmpty(formatos[j].Estilo) ?
                               formatos[j].Estilo : string.Empty, valor is bool ? ((bool)valor).ToString(!string.IsNullOrEmpty(formatos[j].Formato) ? formatos[j].Formato : "ai")
                                   : valor != null ? valor is IFormattable && !string.IsNullOrEmpty(formatos[j].Formato)
                                           ? (valor as IFormattable).ToString(formatos[j].Formato, null) : valor.ToString() : string.Empty);
                                rowHeader[propiedad.Name.Trim()] = sbHeader.ToString();
                            }
                        }
                    }

                    dtExcel.Rows.Add(rowHeader);
                }

                DataRow rowInicial = dtExcel.NewRow();

                for (int j = 0; j < formatos.Length; j++)
                {
                    StringBuilder sb = new StringBuilder();
                    PropertyInfo propiedad = Array.Find(propiedades, p => p.Name.Equals(formatos[j].Columna, StringComparison.InvariantCultureIgnoreCase));

                    if (propiedad != null)
                    {
                        object valor = propiedad.GetValue(lista[i], new object[] { });

                        sb.AppendFormat(
                               "{1}",
                               !string.IsNullOrEmpty(formatos[j].Estilo) ?
                               formatos[j].Estilo : string.Empty, valor is bool ? ((bool)valor).ToString(!string.IsNullOrEmpty(formatos[j].Formato) ? formatos[j].Formato : "ai")
                                   : valor != null ? valor is IFormattable && !string.IsNullOrEmpty(formatos[j].Formato)
                                           ? (valor as IFormattable).ToString(formatos[j].Formato, null) : valor.ToString() : string.Empty);
                        rowInicial[propiedad.Name.Trim()] = sb.ToString();

                    }
                }

                dtExcel.Rows.Add(rowInicial);
            }
        }

        return dtExcel;
    }

    /// <summary>
    /// Método GenerarXLS
    /// </summary>
    /// <param name="response">El response a filtrar</param>
    /// <param name="textoHtml">El texto Html a filtrar</param>
    /// <param name="nombreArchivo">Nombre Archiva a filtrar</param>
    public static void GenerarXLS(HttpResponse response, string textoHtml, string nombreArchivo)
    {
        if (response != null && !string.IsNullOrEmpty(textoHtml))
        {
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Charset = UTF8Encoding.Default.WebName;
            response.ContentEncoding = UTF8Encoding.Default;
            response.Buffer = true;
            response.BufferOutput = true;
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.SetExpires(DateTime.Now);
            response.ContentType = "application/vnd.ms-excel";
            response.AppendHeader(
            "Content-Disposition",
            string.Format(
            "{0}; filename=\"{1}\"; size={2}; creation-date=\"{3}\"; modification-date=\"{3}\"; read-date=\"{3}\"",
                !string.IsNullOrEmpty(nombreArchivo) && nombreArchivo.EndsWith(
                 ".xls",
                 StringComparison.InvariantCultureIgnoreCase) ? "attachment" : "inline",
                !string.IsNullOrEmpty(nombreArchivo) ? nombreArchivo : "reporteExcel",
                textoHtml.Length,
                DateTime.Now.ToString("R")));
            response.AppendHeader(
            "Content-Length",
            textoHtml.Length.ToString());
            response.Write(textoHtml);
            response.End();
        }
    }

    /// <summary>
    /// Método GenerarXLSX
    /// </summary>
    /// <param name="response">El response a filtrar</param>
    /// <param name="data">Datatable de los datos del reporte</param>
    /// <param name="nombreArchivo">Nombre Archiva a filtrar</param>
    public static void GenerarXLSX(HttpResponse response, DataTable data, string nombreArchivo)
    {
        if (data.Rows.Count > 0)
        {
            byte[] bytes = { };
            response.Clear();
            response.Charset = "";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;";
            response.AddHeader("content-disposition", "attachment;filename=" + nombreArchivo + ".xlsx");


            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet wsDt = pck.Workbook.Worksheets.Add("Sheet1");
                wsDt.Cells["A1"].LoadFromDataTable(data, false);
                response.BinaryWrite(pck.GetAsByteArray());
            }

            response.Flush();
            response.End();
        }
    }
}