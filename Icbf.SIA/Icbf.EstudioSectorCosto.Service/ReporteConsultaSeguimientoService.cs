﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;

namespace Icbf.EstudioSectorCosto.Service
{
    public class ReporteConsultaSeguimientoService
    {
        /// <summary>
        /// Método para consultar cierres por vigencia por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de CierreEstudiosPorVigencia</returns>
        public List<CierreEstudiosPorVigencia> ConsultarCierresEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarCierresEstudio(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar información proceso selección por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de InformacionProcesoSeleccion</returns>
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccion(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarProcesoSeleccion(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de BitacoraAcciones</returns>
        public List<BitacoraAcciones> ConsultarBitacoraAccion(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return  new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarBitacoraAccion(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de TiempoEntreActividadesConsulta</returns>
        public List<TiempoEntreActividadesConsulta> ConsultarTiempos(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarTiempos(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        /// <summary>
        /// Método para consultar bitacora por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de BitacoraAcciones</returns>
        public List<ResultadoEstudioSector> ConsultarResultado(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarResultado(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar gestiones por id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de GestionesEstudioSectorCostos</returns>
        public List<GestionesEstudioSectorCostos> ConsultarGestionEstudio(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarGestionEstudio(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar registro inicial id consecutivo de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNumeroRegistros">Número de registros solicitados</param>
        /// <returns>Lista de RegistroSolicitudEstudioSectoryCaso</returns>
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroInicial(decimal pIdConsecutivoEstudio, int pNumeroRegistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarRegistroInicial(pIdConsecutivoEstudio, pNumeroRegistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<GestionesEstudioSectorCostos> ConsultarGestion_By_IdConsecutivoEstudio(decimal pIdConsecutivoEstudio, int Nregistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarGestion_By_IdConsecutivoEstudio(pIdConsecutivoEstudio, Nregistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroSolicitudEstudioSectoryCaso ConsultarRegistroInicial_By_IdConsecutivoEstudio(decimal pIdConsecutivoEstudio, int Nregistros)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarRegistroInicial_By_IdConsecutivoEstudio(pIdConsecutivoEstudio, Nregistros);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(Int32? pConsecutivoEstudio, String pNombreAbreviado, String pObjeto, int? pIdModalidadSeleccion, int? pIdResponsableES, int? pIdResponsableEC, int? pIdEstadoSolicitud, int? pIdDireccionSolicitante)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ReporteConsultaSeguimientoBLL().ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(pConsecutivoEstudio, pNombreAbreviado,  pObjeto, pIdModalidadSeleccion, pIdResponsableES, pIdResponsableEC, pIdEstadoSolicitud, pIdDireccionSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }



    }
}
