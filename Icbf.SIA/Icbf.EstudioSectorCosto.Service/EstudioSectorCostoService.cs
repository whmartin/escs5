﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Business;

namespace Icbf.EstudioSectorCosto.Service
{
    public partial class EstudioSectorCostoService
    {
        #region Consulta Dependencias
        /// <summary>
        /// Método para listar dependencias
        /// </summary>
        /// <param name="pDireccionSolicitante">Dependencia solicitante</param>
        /// <param name="pVigencia">Año de vigencia</param>
        public List<ConsultaDependencias> ConsultarConsultaDependenciass(string pDireccionSolicitante, int? pVigencia)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ConsultaDependenciasBLL().ConsultarConsultaDependenciass(pDireccionSolicitante, pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region Actividades
        public int InsertarActividades(Actividades pActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActividadesBLL().InsertarActividades(pActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarActividades(Actividades pActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActividadesBLL().ModificarActividades(pActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarActividades(Actividades pActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActividadesBLL().EliminarActividades(pActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Actividades ConsultarActividades(int pIdActividad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActividadesBLL().ConsultarActividades(pIdActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Actividades> ConsultarActividadess(String pNombre, String pDescripcion, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActividadesBLL().ConsultarActividadess(pNombre, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarActividad(String pNombreActividad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActividadesBLL().ValidarActividad(pNombreActividad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region BitacoraAcciones

        /// <summary>
        /// Método para modificar una bitácora de acción
        /// </summary>
        /// <param name="pBitacoraAcciones">La bitácora de acción</param>
        public int ModificarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ModificarBitacoraAcciones(pBitacoraAcciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar una bitácora de acción por su id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El id de la bitácora de acción</param>
        public List<BitacoraAcciones> ConsultarBitacoraAcciones(decimal pIdBitacoraEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ConsultarBitacoraAcciones(pIdBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar una bitácora de acción
        /// </summary>
        /// <param name="pBitacoraAcciones">La bitácora de acción</param>
        public int InsertarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().InsertarBitacoraAcciones(pBitacoraAcciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método eliminar una bitácora de acción
        /// </summary>
        /// <param name="pBitacoraAcciones">La bitácora de acción</param>
        public int EliminarBitacoraAcciones(BitacoraAcciones pBitacoraAcciones)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().EliminarBitacoraAcciones(pBitacoraAcciones);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para listar la bitácora de acciones
        /// </summary>
        /// <param name="pIdEstudio">El id del estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado</param>
        /// <param name="pObjeto">El objeto</param>
        /// <param name="pIdResponsableES">El id del responsable del estudio de sector</param>
        /// <param name="pIdResponsableEC">El id del responsable del estúdio de costos</param>
        /// <param name="pIdModalidadSeleccion">El id de la Modalidad de contratación</param>
        /// <param name="pDireccionSolicitante">Dependencia solicitante</param>
        /// <param name="pEstado">El estado</param>
        public List<BitacoraAcciones> ConsultarBitacoraAccioness(long? pIdEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ConsultarBitacoraAccioness(pIdEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar una bitácora de estúdio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bitácora de estúdio</param>
        public decimal InsertarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().InsertarBitacoraEstudio( pBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para modificar una bitácora de estúdio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bitácora de estúdio</param>
        public decimal ModificarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ModificarBitacoraEstudio(pBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para eliminar una bitácora de estúdio
        /// </summary>
        /// <param name="pBitacoraEstudio">La bitácora de estúdio</param>
        public int EliminarBitacoraEstudio(BitacoraEstudio pBitacoraEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().EliminarBitacoraEstudio(pBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar una bitácora de estúdio por su Id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El Id de la bitácora de estúdio</param>
        public BitacoraEstudio ConsultarBitacoraEstudio(decimal pIdBitacoraEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ConsultarBitacoraEstudio(pIdBitacoraEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar si una bitácora de estúdio existe por su Id
        /// </summary>
        /// <param name="pIdBitacoraEstudio">El Id de la bitácora de estúdio</param>
        public bool ConsultarBitacoraEstudioExiste(decimal pIdconsecutivoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ConsultarBitacoraEstudioExiste(pIdconsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar la traza de una bitácora de acción
        /// </summary>
        /// <param name="pIdBitacoraAccion">El Id de la bitácora de acción</param>
        public List<BitacoraAcciones> ConsultarBitacoraAccionTraza(decimal pIdBitacoraAccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.BitacoraAccionesBLL().ConsultarBitacoraAccionTraza(pIdBitacoraAccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region Complejidad
        public int InsertarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ComplejidadesBLL().InsertarComplejidades(pComplejidades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ComplejidadesBLL().ModificarComplejidades(pComplejidades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarComplejidades(Complejidades pComplejidades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ComplejidadesBLL().EliminarComplejidades(pComplejidades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Complejidades ConsultarComplejidades(int pIdComplejidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ComplejidadesBLL().ConsultarComplejidades(pIdComplejidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Complejidades> ConsultarComplejidadess(String pNombre, int? pTipoComplejidad, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ComplejidadesBLL().ConsultarComplejidadess(pNombre, pTipoComplejidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarComplejidades(String pNombre, int pTipoComplejidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ComplejidadesBLL().ValidarComplejidades(pNombre, pTipoComplejidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region DiasEstimadosRealesPorModalidad
        public int InsertarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DiasEstimadosRealesPorModalidadBLL().InsertarDiasEstimadosRealesPorModalidad(pDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DiasEstimadosRealesPorModalidadBLL().ModificarDiasEstimadosRealesPorModalidad(pDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDiasEstimadosRealesPorModalidad(DiasEstimadosRealesPorModalidad pDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DiasEstimadosRealesPorModalidadBLL().EliminarDiasEstimadosRealesPorModalidad(pDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DiasEstimadosRealesPorModalidad ConsultarDiasEstimadosRealesPorModalidad(int pIdDiasEstimadosRealesPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DiasEstimadosRealesPorModalidadBLL().ConsultarDiasEstimadosRealesPorModalidad(pIdDiasEstimadosRealesPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<DiasEstimadosRealesPorModalidad> ConsultarDiasEstimadosRealesPorModalidads(int? pIdModalidadSeleccion, int? pIdComplejidadIndicador, String pOperador, int? pLimite, int? pDiasHabilesEntrePSeInicioEjecucion, int? pDiasHabilesEntreComitePS, int? pDiasHabilesEntreRadicacionContratoYComite, int? pDiasHabilesEntreESyRadicacionContratos, int? pDiasHabilesEntreFCTDefinitivaYES, int? pDiasHabilesEntreRadicacionFCTyFCTDefinitiva, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DiasEstimadosRealesPorModalidadBLL().ConsultarDiasEstimadosRealesPorModalidads(pIdModalidadSeleccion, pIdComplejidadIndicador, pOperador, pLimite, pDiasHabilesEntrePSeInicioEjecucion, pDiasHabilesEntreComitePS, pDiasHabilesEntreRadicacionContratoYComite, pDiasHabilesEntreESyRadicacionContratos, pDiasHabilesEntreFCTDefinitivaYES, pDiasHabilesEntreRadicacionFCTyFCTDefinitiva, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarDiasEstimadosRealesPorModalidad(int pIdModalidadSeleccion
                                                            , int? pIdComplejidadIndicador
                                                            , String pOperador
                                                            , int? pLimite
                                                            , int? pDiasHabilesEntrePSeInicioEjecucion
                                                            , int? pDiasHabilesEntreComitePS
                                                            , int? pDiasHabilesEntreRadicacionContratoYComite
                                                            , int? pDiasHabilesEntreESyRadicacionContratos
                                                            , int? pDiasHabilesEntreFCTDefinitivaYES
                                                            , int? pDiasHabilesEntreRadicacionFCTyFCTDefinitiva)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DiasEstimadosRealesPorModalidadBLL().ValidarDiasEstimadosRealesPorModalidad(pIdModalidadSeleccion
                                                                                                , pIdComplejidadIndicador
                                                                                                , pOperador
                                                                                                , pLimite
                                                                                                , pDiasHabilesEntrePSeInicioEjecucion
                                                                                                , pDiasHabilesEntreComitePS
                                                                                                , pDiasHabilesEntreRadicacionContratoYComite
                                                                                                , pDiasHabilesEntreESyRadicacionContratos
                                                                                                , pDiasHabilesEntreFCTDefinitivaYES
                                                                                                , pDiasHabilesEntreRadicacionFCTyFCTDefinitiva);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region EstadoEstudio
        public int InsertarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.EstadoEstudioBLL().InsertarEstadoEstudio(pEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.EstadoEstudioBLL().ModificarEstadoEstudio(pEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarEstadoEstudio(EstadoEstudio pEstadoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.EstadoEstudioBLL().EliminarEstadoEstudio(pEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public EstadoEstudio ConsultarEstadoEstudio(int pIdEstadoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.EstadoEstudioBLL().ConsultarEstadoEstudio(pIdEstadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<EstadoEstudio> ConsultarEstadoEstudios(String pNombre, String pMotivo, String pDescripcion, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.EstadoEstudioBLL().ConsultarEstadoEstudios(pNombre, pMotivo, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarEstadoEstudio(String pNombre, String pMotivo)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.EstadoEstudioBLL().ValidarEstadoEstudio(pNombre, pMotivo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region RangoDiasComplejidadPorModalidad
        public int InsertarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RangoDiasComplejidadPorModalidadBLL().InsertarRangoDiasComplejidadPorModalidad(pRangoDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RangoDiasComplejidadPorModalidadBLL().ModificarRangoDiasComplejidadPorModalidad(pRangoDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRangoDiasComplejidadPorModalidad(RangoDiasComplejidadPorModalidad pRangoDiasComplejidadPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RangoDiasComplejidadPorModalidadBLL().EliminarRangoDiasComplejidadPorModalidad(pRangoDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RangoDiasComplejidadPorModalidad ConsultarRangoDiasComplejidadPorModalidad(int pIdRangosDiasComplejidadPorModalidad)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RangoDiasComplejidadPorModalidadBLL().ConsultarRangoDiasComplejidadPorModalidad(pIdRangosDiasComplejidadPorModalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RangoDiasComplejidadPorModalidad> ConsultarRangoDiasComplejidadPorModalidads(int? pIdModalidadSeleccion, String pOperador, int? pLimite, int? pIdComplejidadInterna, int? pIdComplejidadIndicador, int? pDiasHabilesInternos, int? pDiasHabilesCumplimientoIndicador, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RangoDiasComplejidadPorModalidadBLL().ConsultarRangoDiasComplejidadPorModalidads(pIdModalidadSeleccion, pOperador, pLimite, pIdComplejidadInterna, pIdComplejidadIndicador, pDiasHabilesInternos, pDiasHabilesCumplimientoIndicador, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Boolean ValidarRangoDiasComplejidadPorModalidad(int pIdModalidadSeleccion
                                                               , String pOperador
                                                               , int? pLimite
                                                               , int? pIdComplejidadInterna
                                                               , int? pIdComplejidadIndicador
                                                               , int? pDiasHabilesInternos
                                                               , int? pDiasHabilesCumplimientoIndicador)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RangoDiasComplejidadPorModalidadBLL().ValidarRangoDiasComplejidadPorModalidad(pIdModalidadSeleccion
                                                                                                                                        , pOperador
                                                                                                                                        , pLimite
                                                                                                                                        , pIdComplejidadInterna
                                                                                                                                        , pIdComplejidadIndicador
                                                                                                                                        , pDiasHabilesInternos
                                                                                                                                        , pDiasHabilesCumplimientoIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region TiposEstudio
        public int InsertarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.TiposEstudioBLL().InsertarTiposEstudio(pTiposEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.TiposEstudioBLL().ModificarTiposEstudio(pTiposEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarTiposEstudio(TiposEstudio pTiposEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.TiposEstudioBLL().EliminarTiposEstudio(pTiposEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiposEstudio ConsultarTiposEstudio(int pIdTipoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.TiposEstudioBLL().ConsultarTiposEstudio(pIdTipoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<TiposEstudio> ConsultarTiposEstudios(String pNombre, String pDescripcion, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.TiposEstudioBLL().ConsultarTiposEstudios(pNombre, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarTiposEstudio(String pNombre)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.TiposEstudioBLL().ValidarTiposEstudio(pNombre);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region InformacionProcesoSeleccion

        public int InsertarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.InformacionProcesoSeleccionBLL().InsertarInformacionProcesoSeleccion(pInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.InformacionProcesoSeleccionBLL().ModificarInformacionProcesoSeleccion(pInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarInformacionProcesoSeleccion(InformacionProcesoSeleccion pInformacionProcesoSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.InformacionProcesoSeleccionBLL().EliminarInformacionProcesoSeleccion(pInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public InformacionProcesoSeleccion ConsultarInformacionProcesoSeleccion(Int32 pIdInformacionProcesoSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.InformacionProcesoSeleccionBLL().ConsultarInformacionProcesoSeleccion(pIdInformacionProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<InformacionProcesoSeleccion> ConsultarProcesoSeleccions(Int32? pConsecutivoEstudio, String pNombreAbreviado, String pObjeto, int? pIdModalidadSeleccion, int? pIdResponsableES, int? pIdResponsableEC, int? pIdEstadoSolicitud, int? pIdDireccionSolicitante)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.InformacionProcesoSeleccionBLL().ConsultarProcesoSeleccions(pConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdModalidadSeleccion, pIdResponsableES, pIdResponsableEC, pIdEstadoSolicitud, pIdDireccionSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<InformacionProcesoSeleccion> ConsultarContratoESC(int? pConsecutivo, string pNumeroProceso, int? pVigenciaProceso, string pObjetoContrato, int? pProcesoSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.InformacionProcesoSeleccionBLL().ConsultarContratoESC(pConsecutivo, pNumeroProceso, pVigenciaProceso, pObjetoContrato, pProcesoSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region CierreEstudiosPorVigencia

        public int InsertarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().InsertarCierreEstudiosPorVigencia(pCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().ModificarCierreEstudiosPorVigencia(pCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarCierreEstudiosPorVigencia(CierreEstudiosPorVigencia pCierreEstudiosPorVigencia)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().EliminarCierreEstudiosPorVigencia(pCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public CierreEstudiosPorVigencia ConsultarCierreEstudiosPorVigencia(Int32 pIdCierreEstudiosPorVigencia)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().ConsultarCierreEstudiosPorVigencia(pIdCierreEstudiosPorVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<CierreEstudiosPorVigencia> ConsultarCierreEstudiosPorVigencias(int? pAnioCierre, DateTime? pFechaEjecucion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().ConsultarCierreEstudiosPorVigencias(pAnioCierre, pFechaEjecucion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<int> ConsultarAnioCierre()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().ConsultarAnioCierre();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<DateTime> ConsultarFechaEjecucion()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.CierreEstudiosPorVigenciaBLL().ConsultarFechaEjecucion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region RegistroSolicitudEstudioSectoryCaso

        public int InsertarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().InsertarRegistroSolicitudEstudioSectoryCaso(pRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().ModificarRegistroSolicitudEstudioSectoryCaso(pRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRegistroSolicitudEstudioSectoryCaso(RegistroSolicitudEstudioSectoryCaso pRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().EliminarRegistroSolicitudEstudioSectoryCaso(pRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public RegistroSolicitudEstudioSectoryCaso ConsultarRegistroSolicitudEstudioSectoryCaso(Int32 pIdRegistroSolicitudEstudioSectoryCaso)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().ConsultarRegistroSolicitudEstudioSectoryCaso(pIdRegistroSolicitudEstudioSectoryCaso);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos(Int32? pConsecutivoEstudio, int? pAplicaPACCO, int? pConsecutivoPACCO, int? pDireccionsolicitantePACCO, String pObjetoPACCO, int? pModalidadPACCO, Decimal? pValorPresupuestalPACCO, String pVigenciaPACCO, int? pConsecutivoEstudioRelacionado, DateTime pFechaSolicitudInicial, String pActaCorreoNoRadicado, String pNombreAbreviado, String pNumeroReproceso, String pObjeto, String pCuentaVigenciasFuturasPACCO, String pAplicaProcesoSeleccion, int? pIdModalidadSeleccion, int? pIdTipoEstudio, int? pIdComplejidadInterna, int? pIdComplejidadIndicador, int? pIdResponsableES, int? pIdResponsableEC, String pOrdenadorGasto, int? pIdEstadoSolicitud, int? pIdMotivoSolicitud, int? pIdDireccionSolicitante, int? pIdAreaSolicitante, String pTipoValor, Decimal? pValorPresupuestoEstimadoSolicitante)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().ConsultarRegistroSolicitudEstudioSectoryCasos(pConsecutivoEstudio, pAplicaPACCO, pConsecutivoPACCO, pDireccionsolicitantePACCO, pObjetoPACCO, pModalidadPACCO, pValorPresupuestalPACCO, pVigenciaPACCO, pConsecutivoEstudioRelacionado, pFechaSolicitudInicial, pActaCorreoNoRadicado, pNombreAbreviado, pNumeroReproceso, pObjeto, pCuentaVigenciasFuturasPACCO, pAplicaProcesoSeleccion, pIdModalidadSeleccion, pIdTipoEstudio, pIdComplejidadInterna, pIdComplejidadIndicador, pIdResponsableES, pIdResponsableEC, pOrdenadorGasto, pIdEstadoSolicitud, pIdMotivoSolicitud, pIdDireccionSolicitante, pIdAreaSolicitante, pTipoValor, pValorPresupuestoEstimadoSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<RegistroSolicitudEstudioSectoryCaso> ConsultaGeneralSeguimiento(long? pConsecutivoEstudio, string pObjeto, string pNombreAbreviado, long? pConsecutivoPACCO, int? pModalidadSeleccion, string pIsEstado, int? pAnioCierre, int? pVigencia)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().ConsultaGeneralSeguimiento(pConsecutivoEstudio, pObjeto, pNombreAbreviado, pConsecutivoPACCO, pModalidadSeleccion, pIsEstado, pAnioCierre, pVigencia);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<int> ConsultarFechaSolicitudInicial()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.RegistroSolicitudEstudioSectoryCasoBLL().ConsultarFechaSolicitudInicial();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region AreasSolicitante
        public int InsertarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.AreasSolicitantesESCBLL().InsertarAreasSolicitantesESC(pAreasSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.AreasSolicitantesESCBLL().ModificarAreasSolicitantesESC(pAreasSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarAreasSolicitantesESC(AreasSolicitantesESC pAreasSolicitantesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.AreasSolicitantesESCBLL().EliminarAreasSolicitantesESC(pAreasSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public AreasSolicitantesESC ConsultarAreasSolicitantesESC(int pIdAreasSolicitantes)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.AreasSolicitantesESCBLL().ConsultarAreasSolicitantesESC(pIdAreasSolicitantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<AreasSolicitantesESC> ConsultarAreasSolicitantesESCs(String pAreaSolicitante, int? pIdDireccionSolicitanteESC, String pDescripcion, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.AreasSolicitantesESCBLL().ConsultarAreasSolicitantesESCs(pAreaSolicitante, pIdDireccionSolicitanteESC, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarAreasSolicitantesESC(String pAreaSolicitante, int pIdDireccionSolicitanteESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.AreasSolicitantesESCBLL().ValidarAreasSolicitantesESC(pAreaSolicitante, pIdDireccionSolicitanteESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region direccionSolicitante
        public int InsertarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DireccionesSolicitantesESCBLL().InsertarDireccionesSolicitantesESC(pDireccionesSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DireccionesSolicitantesESCBLL().ModificarDireccionesSolicitantesESC(pDireccionesSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarDireccionesSolicitantesESC(DireccionesSolicitantesESC pDireccionesSolicitantesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DireccionesSolicitantesESCBLL().EliminarDireccionesSolicitantesESC(pDireccionesSolicitantesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public DireccionesSolicitantesESC ConsultarDireccionesSolicitantesESC(int pIdDireccionesSolicitantes)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DireccionesSolicitantesESCBLL().ConsultarDireccionesSolicitantesESC(pIdDireccionesSolicitantes);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<DireccionesSolicitantesESC> ConsultarDireccionesSolicitantesESCs(String pDireccionSolicitante, String pDescripcion, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DireccionesSolicitantesESCBLL().ConsultarDireccionesSolicitantesESCs(pDireccionSolicitante, pDescripcion, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarDireccionesSolicitantesESC(String pDireccionSolicitante)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.DireccionesSolicitantesESCBLL().ValidarDireccionesSolicitantesESC(pDireccionSolicitante);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region ModalidadSeleccion
        public int InsertarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ModalidadesSeleccionESCBLL().InsertarModalidadesSeleccionESC(pModalidadesSeleccionESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ModalidadesSeleccionESCBLL().ModificarModalidadesSeleccionESC(pModalidadesSeleccionESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarModalidadesSeleccionESC(ModalidadesSeleccionESC pModalidadesSeleccionESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ModalidadesSeleccionESCBLL().EliminarModalidadesSeleccionESC(pModalidadesSeleccionESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ModalidadesSeleccionESC ConsultarModalidadesSeleccionESC(int pIdModalidadSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ModalidadesSeleccionESCBLL().ConsultarModalidadesSeleccionESC(pIdModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ModalidadesSeleccionESC> ConsultarModalidadesSeleccionESCs(String pModalidadSeleccion, String pDescripcion, int? pEstado,string Tipomodalidad=null)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ModalidadesSeleccionESCBLL().ConsultarModalidadesSeleccionESCs(pModalidadSeleccion, pDescripcion, pEstado, Tipomodalidad);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarModalidadesSeleccionESC(String pModalidadSeleccion)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ModalidadesSeleccionESCBLL().ValidarModalidadesSeleccionESC(pModalidadSeleccion);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region Responsables
        public int InsertarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResponsablesESCBLL().InsertarResponsablesESC(pResponsablesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResponsablesESCBLL().ModificarResponsablesESC(pResponsablesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarResponsablesESC(ResponsablesESC pResponsablesESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResponsablesESCBLL().EliminarResponsablesESC(pResponsablesESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ResponsablesESC ConsultarResponsablesESC(int pIdResponsable)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResponsablesESCBLL().ConsultarResponsablesESC(pIdResponsable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ResponsablesESC> ConsultarResponsablesESCs(String pResponsable, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResponsablesESCBLL().ConsultarResponsablesESCs(pResponsable, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public Boolean ValidarResponsablesESC(String pResponsable)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResponsablesESCBLL().ValidarResponsablesESC(pResponsable);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region Reporte de consulta seguimiento

        public string GenerarReporteSeguimiento(List<RegistroSolicitudEstudioSectoryCaso> ModalidadyCierresAnio, string Usuario,List<CierreEstudiosPorVigencia> vCierres,List<BitacoraAcciones> vBitacora,List<IndicadorAdicionalResultado> vIndicadores,List<GestionesActividadesAdicionales> vGAdicionales,List<InformacionProcesoSeleccion> ProcesoSeleccion,ResultadoEstudioSector Estudios,List<GestionesEstudioSectorCostos> Gestiones,RegistroSolicitudEstudioSectoryCaso vRegistro, TiempoEntreActividadesEstudioSyC vTiemposReales, TiempoEntreActividadesEstudioSyC vTiemposESC, TiemposPACCO vTiempoPACCO, TiempoEntreActividadesEstudioSyC vTiemposESyC, int IdConsecutivoEstudio, string pathPlantilla)
        {
            try
            {
                return new ReporteConsultaSeguimientoBLL().GenerarReporteSeguimiento(ModalidadyCierresAnio, Usuario, vCierres, vBitacora, vIndicadores, vGAdicionales, ProcesoSeleccion,Estudios,Gestiones,vRegistro, vTiemposReales, vTiemposESC, vTiempoPACCO, vTiemposESyC,IdConsecutivoEstudio, pathPlantilla);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
