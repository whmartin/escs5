﻿//-----------------------------------------------------------------------
// <copyright file="ResultadoEstudioSectorService.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  ResultadoEstudioSectorService</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icbf.EstudioSectorCosto.Entity;

namespace Icbf.EstudioSectorCosto.Service
{
    /// <summary>
    /// Clase de la capa service que permite la comunicación de la presentación con el modelo de negocios del sistema
    /// </summary>
    public class ResultadoEstudioSectorService
    {
        /// <summary>
        /// Método para listar ResultadoEstudioSectorConsulta
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado del estudio</param>
        /// <param name="pObjeto">Objeto del estudio</param>
        /// <param name="pIdResponsableES">Responsable del estudio ES</param>
        /// <param name="pIdResponsableEC">Responsable del estudio EC</param>
        /// <param name="pIdModalidadDeSeleccion">Id modalidad selección</param>
        /// <param name="pDireccionSolicitante">Dirección del solicitante</param>
        /// <param name="pEstado">Estado del estudiio</param>
        /// <returns>Retorna una lista tipo ResultadoEstudioSectorConsulta</returns>
        public List<ResultadoEstudioSectorConsulta> ConsultarResultadoEstudioSectores(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarResultadoEstudioSectors(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para obtener lista responsables
        /// </summary>
        /// <returns>Retorna lista de reposnsables</returns>
        public List<BaseDTO> ConsultarResponsable()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarResponsable();


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar días festivos
        /// </summary>
        /// <param name="pAnnio">Año</param>
        /// <returns>Listas de días festivos</returns>
        public List<BaseDTO> ConsultarDiasFestivos(int pAnnio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarDiasFestivos(pAnnio);


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar vigencia
        /// </summary>
        /// <returns>Retorna una lista de vigencias</returns>
        public List<BaseDTO> ConsultarVigencia()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarVigencia();


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar año cierre
        /// </summary>
        /// <returns>Lista de años cierre</returns>
        public List<BaseDTO> ConsultarAnioCierre()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarAnioCierre();


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar modalidad selección
        /// </summary>
        /// <returns>Lista de modalidad selección</returns>
        public List<BaseDTO> ConsultarModalidadSeleccion()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarModalidadSeleccion();


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar gestiones de estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
        /// <param name="pNombreAbreviado">Nombre abreviado del estudio</param>
        /// <param name="pObjeto">Objeto del estudio</param>
        /// <param name="pIdResponsableES">Responsable del estudio ES</param>
        /// <param name="pIdResponsableEC">Responsable del estudio EC</param>
        /// <param name="pIdModalidadDeSeleccion">Id modalidad selección</param>
        /// <param name="pDireccionSolicitante">Dirección del solicitante</param>
        /// <param name="pEstado">Estado del estudiio</param>
        /// <returns>Retorna una lista tipo GestionesEstudioSectorCostos</returns>
        public List<GestionesEstudioSectorCostos> ConsultarGestionEstudioSectores(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, int? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarGestionEstudioSectores(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);

            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para modificar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesEstudioSectorCostos">Id del registro gestion estudio</param>
        /// <returns>Retorna número de filas afectadas</returns>
        public decimal ModificarGestionesEstudioSectorCostos(GestionesEstudioSectorCostos pGestionesEstudioSectorCostos)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarGestionesEstudioSectorCostos(pGestionesEstudioSectorCostos);


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesActividadesAdicionales">Objeto GestionesActividadesAdicionales</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public int InsertarActividadesEstudioSector(GestionesActividadesAdicionales pGestionesActividadesAdicionales)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarActividadesEstudioSector(pGestionesActividadesAdicionales);


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método modificar actividades gestión estudio
        /// </summary>
        /// <param name="pGestionesActividadesAdicionales">Objeto GestionesActividadesAdicionales</param>
        /// <returns>retorna el número de filas afectadas</returns>
        public int ModificarActividadesEstudioSector(GestionesActividadesAdicionales pGestionesActividadesAdicionales)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarActividadesEstudioSector(pGestionesActividadesAdicionales);


            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar Diracción del solicitante
        /// </summary>
        /// <returns>Lista de direcciones</returns>
        public List<BaseDTO> ConsultarDireccion()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarDireccion();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para insertar gestiones de estudio
        /// </summary>
        /// <param name="pGestionesEstudioSectorCostos">Objeto GestionesEstudioSectorCostos</param>
        /// <returns>Id del registro de gestión insertado</returns>
        public decimal InsertarGestionesEstudioSectorCostos(GestionesEstudioSectorCostos pGestionesEstudioSectorCostos)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarGestionesEstudioSectorCostos(pGestionesEstudioSectorCostos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para eliminar gestiones de estudio
        /// </summary>
        /// <param name="pGestionEstudioSector">Objeto GestionesEstudioSectorCostos</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public int EliminarGestionEstudioSector(GestionesEstudioSectorCostos pGestionEstudioSector)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().EliminarGestionEstudioSector(pGestionEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consutar fechas definitivas de gestiones
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id del cosecutivo de estudio</param>
        /// <returns>Retorna objeto GestionesEstudioSectorCostos</returns>
        public GestionesEstudioSectorCostos ConsultarGestionDefinitivas(long pIdConsecutivoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarGestionDefinitivas(pIdConsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar gestión de estudio
        /// </summary>
        /// <param name="pIdGestionEstudio">Id de la gestión de estudio</param>
        /// <returns>Retorna objeto GestionesEstudioSectorCostos</returns>
        public GestionesEstudioSectorCostos ConsultarGestionEstudioSector(decimal pIdGestionEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarGestionEstudioSector(pIdGestionEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar actividades adicionales de gestiones
        /// </summary>
        /// <param name="pIdGestionEstudio">Id de la gestion de estudio</param>
        /// <returns>Retorna lista de GestionesActividadesAdicionales</returns>
        public List<GestionesActividadesAdicionales> ConsultarGestionesActividadesAdicionales(decimal pIdGestionEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar estados 
        /// </summary>
        /// <returns>Retorna lista de estados</returns>
        public List<BaseDTO> ConsultarEstado()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarEstado();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para consultar actividades
        /// </summary>
        /// <returns>Retorna lista de actividades</returns>
        public List<BaseDTO> ConsultarActividades()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarActividades();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para modificar resultados de estudio
        /// </summary>
        /// <param name="vResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna número de filas afectadas</returns>
        public long ModificarResultadoEstudioSector(ResultadoEstudioSector vResultadoEstudioSector)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarResultadoEstudioSector(vResultadoEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar resultado de estudio
        /// </summary>
        /// <param name="pResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna el id del resultado insertado</returns>
        public long InsertarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarResultadoEstudioSector(pResultadoEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para eliminar resultado estudio
        /// </summary>
        /// <param name="pResultadoEstudioSector">Objeto ResultadoEstudioSector</param>
        /// <returns>Retorna el número de filas insertadas</returns>
        public int EliminarResultadoEstudioSector(ResultadoEstudioSector pResultadoEstudioSector)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().EliminarResultadoEstudioSector(pResultadoEstudioSector);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar resultado estudio
        /// </summary>
        /// <param name="pIdConsecutivoEstudio">Id registro resultado estudio</param>
        /// <returns>Objeto ResultadoEstudioSector</returns>
        public ResultadoEstudioSector ConsultarResultadoEstudioSector(long pIdConsecutivoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarResultadoEstudioSector(pIdConsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #region INDICADORES ADICIONALES

        /// <summary>
        /// Método para modificar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public long ModificarIndicadoresResultado(IndicadorAdicionalResultado pIndicador)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarIndicadorResultado(pIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar indicadores resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>Retorna el id insertado</returns>
        public long InsertarIndicadorResultado(IndicadorAdicionalResultado pIndicador)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarIndicadorResultado(pIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para eliminar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIndicador">Objeto IndicadorAdicionalResultado</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public long EliminarIndicadorResultado(IndicadorAdicionalResultado pIndicador)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarIndicadorResultado(pIndicador);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar indicadores adcionales de resultado estudio
        /// </summary>
        /// <param name="pIdResultadoEstudio">Id del registro resultado</param>
        /// <returns>Retorna lista de indicadores</returns>
        public List<IndicadorAdicionalResultado> ConsultarIndicadorResultado(long pIdResultadoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarIndicadorResultado(pIdResultadoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion

        #region TIEMPO ENTRE ACTIVIDADES

        /// <summary>
        /// Método para validar parámetros para calcular tiempos
        /// </summary>
        /// <param name="IdConsecutivoEstudio">Id del cosecutivo de estudio</param>
        /// <returns>Retorna "OK" cuando se cumplen las condiciones</returns>
        public string ValidarCalculoTiempos(decimal IdConsecutivoEstudio)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ValidarCalculoTiempos(IdConsecutivoEstudio);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar Tiempos entre actividades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id tiempo entre actividades insertado</returns>
        public decimal InsertarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarTiempoEntreActividades(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para modificar tiempos entre actividades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public int ModificarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarTiempoEntreActividades(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        /// <summary>
        /// Método para eliminar tiempos entre activiades
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public int EliminarTiempoEntreActividades(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().EliminarTiempoEntreActividades(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }

        #endregion

        #region TIEMPOS

        /// <summary>
        /// Método para insertar tiempos SyC
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el id insertado</returns>
        public decimal InsertarTiempoEntreActividadesEstudioSyC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarTiempoEntreActividadesEstudioSyC(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para modificar tiempos SyC
        /// </summary>
        /// <param name="pTiempoEntreActividadesEstudioSyC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna el número de filas afectadas</returns>
        public int ModificarTiempoEntreActividadesEstudioSyC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesEstudioSyC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarTiempoEntreActividadesEstudioSyC(pTiempoEntreActividadesEstudioSyC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        

        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesEstudioSyC(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesEstudioSyC(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para insertar tiempos ESC
        /// </summary>
        /// <param name="pTiempoEntreActividadesFechasESC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id insertado</returns>
        public decimal InsertarTiempoEntreActividadesFechasESC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesFechasESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarTiempoEntreActividadesFechasESC(pTiempoEntreActividadesFechasESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para modificar tiempos ESC
        /// </summary>
        /// <param name="pTiempoEntreActividadesFechasESC">Objeto TiempoEntreActividadesEstudioSyC</param>
        /// <returns>Retorna id insertado</returns>
        public decimal ModificarTiempoEntreActividadesFechasESC(TiempoEntreActividadesEstudioSyC pTiempoEntreActividadesFechasESC)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarTiempoEntreActividadesFechasESC(pTiempoEntreActividadesFechasESC);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        /// <summary>
        /// Método para consultar tiempos ESC
        /// </summary>
        /// <param name="pIdTiempoEntreActividades">Id Tiempo entre activiades</param>
        /// <returns>Retorna teimpos ESC</returns>
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesFechasESC(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesFechasESC(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesFechasESCs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesFechasESCs(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public int InsertarTiempoEntreActividadesFechasReales(TiempoEntreActividadesEstudioSyC pTiempoActividadesFechasReales)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarTiempoEntreActividadesFechasReales(pTiempoActividadesFechasReales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarTiempoEntreActividadesFechasReales(TiempoEntreActividadesEstudioSyC pTiempoActividadesFechasReales)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarTiempoEntreActividadesFechasReales(pTiempoActividadesFechasReales);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public TiempoEntreActividadesEstudioSyC ConsultarTiempoEntreActividadesFechasReales(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesFechasReales(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiempoEntreActividadesEstudioSyC> ConsultarTiempoEntreActividadesFechasRealess(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesFechasRealess(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }


        public decimal InsertarTiempoEntreActividadesPACCO(TiemposPACCO InsertarTiempoEntreActividadesPACCO)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().InsertarTiempoEntreActividadesPACCO(InsertarTiempoEntreActividadesPACCO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        public int ModificarTiempoEntreActividadesPACCO(TiemposPACCO pTiempoEntreActividadesPACCO)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ModificarTiempoEntreActividadesPACCO(pTiempoEntreActividadesPACCO);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }

        }
        public TiemposPACCO ConsultarTiempoEntreActividadesPACCO(decimal pIdTiempoEntreActividades)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesPACCO(pIdTiempoEntreActividades);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<TiemposPACCO> ConsultarTiempoEntreActividadesPACCOs(long? pIdConsecutivoEstudio, string pNombreAbreviado, string pObjeto, int? pIdResponsableES, int? pIdResponsableEC, long? pIdModalidadDeSeleccion, int? pDireccionSolicitante, int? pEstado)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiempoEntreActividadesPACCOs(pIdConsecutivoEstudio, pNombreAbreviado, pObjeto, pIdResponsableES, pIdResponsableEC, pIdModalidadDeSeleccion, pDireccionSolicitante, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public TiempoEntreActividadesEstudioSyC ConsultarTiemposESC(decimal pIdConsecutivoEstudio, string pFEstimadaFCTPreliminar)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ResultadoEstudioSectorBLL().ConsultarTiemposESC(pIdConsecutivoEstudio,  pFEstimadaFCTPreliminar);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion


    }
}
