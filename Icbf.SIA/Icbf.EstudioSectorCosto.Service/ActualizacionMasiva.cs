﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icbf.EstudioSectorCosto.Service
{
    public class ActualizacionMasivaService
    {
        public List<RegistroSolicitudEstudioSectoryCaso> ConsultarRegistroSolicitudEstudioSectoryCasos()
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActualizacionMasivaBLL().ConsultarRegistroSolicitudEstudioSectoryCasos();
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public List<LogActualizacionMasiva> ConsultarRegistroActualizaciones(string Usuario,string FI,string FF)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActualizacionMasivaBLL().ConsultarRegistroActualizaciones(Usuario, FI, FF);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int CreaLogActualizacion(string Userp)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActualizacionMasivaBLL().CreaLogActualizacion(Userp);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int ActualizarRegistroSolicitudEstudioSectoryCasos(string Userp,int vigenciapacco,decimal consecutivo,string area, string codigoarea,string estado,string FechaInicioProceso,string Id_modalidad,decimal? Id_tipo_contrato, string modalidad,string objeto_contractual,string tipo_contrato,string FechaInicioEjecucion,decimal valor_contrato)
        {
            try
            {
                return new Icbf.EstudioSectorCosto.Business.ActualizacionMasivaBLL().ActualizarRegistroSolicitudEstudioSectoryCasos(Userp, vigenciapacco,consecutivo, area, codigoarea, estado, FechaInicioProceso, Id_modalidad, Id_tipo_contrato, modalidad, objeto_contractual, tipo_contrato, FechaInicioEjecucion, valor_contrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

    }
}
