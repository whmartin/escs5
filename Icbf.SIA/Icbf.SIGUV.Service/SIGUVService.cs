﻿using System;
using System.Collections.Generic;
using Icbf.SIGUV.Entity;
using Icbf.SIGUV.Business;
using Icbf.Utilities.Exceptions;
using System.Linq;
using System.Text;

namespace Icbf.SIGUV.Service
{
    public class SIGUVService
    {
        #region MacroRegional
        public int InsertarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalBLL().InsertarMacroRegional(pMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalBLL().ModificarMacroRegional(pMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarMacroRegional(MacroRegional pMacroRegional)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalBLL().EliminarMacroRegional(pMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public MacroRegional ConsultarMacroRegional(int pIdMacroRegional)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalBLL().ConsultarMacroRegional(pIdMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<MacroRegional> ConsultarMacroRegionals(String pNombre, int? pEstado)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalBLL().ConsultarMacroRegionals(pNombre, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion Macroregional
        #region MacroRegionalRegionalPCI
        public List<MacroRegional> ConsultarMacroRegionalMacroRegionalPCI(int pIdMacroRegional)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalBLL().ConsultarMacroRegionalMacroRegionalPCI(pIdMacroRegional);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int EliminarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalRegionalPCIBLL().EliminarMacroRegionalRegionalPCI(pMacroRegionalRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public int InsertarMacroRegionalRegionalPCI(MacroRegionalRegionalPCI pMacroRegionalRegionalPCI)
        {
            try
            {
                return new Icbf.SIGUV.Business.MacroRegionalRegionalPCIBLL().InsertarMacroRegionalRegionalPCI(pMacroRegionalRegionalPCI);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region ContratoSiguv
        public int InsertarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvBLL().InsertarContratoSiguv(pContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvBLL().ModificarContratoSiguv(pContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarContratoSiguv(ContratoSiguv pContratoSiguv)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvBLL().EliminarContratoSiguv(pContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ContratoSiguv ConsultarContratoSiguv(int pIdContratoSiguv)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvBLL().ConsultarContratoSiguv(pIdContratoSiguv);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoSiguv> ConsultarContratoSiguvs(int? pIdRegionalPCI, int? pIdMacroRegional, int? pIdContrato)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvBLL().ConsultarContratoSiguvs(pIdRegionalPCI, pIdMacroRegional, pIdContrato);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region ContratoSiguvVehiculos
        public int InsertarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvVehiculosBLL().InsertarContratoSiguvVehiculos(pContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvVehiculosBLL().ModificarContratoSiguvVehiculos(pContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarContratoSiguvVehiculos(ContratoSiguvVehiculos pContratoSiguvVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvVehiculosBLL().EliminarContratoSiguvVehiculos(pContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public ContratoSiguvVehiculos ConsultarContratoSiguvVehiculos(int pIdContratoSiguvVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvVehiculosBLL().ConsultarContratoSiguvVehiculos(pIdContratoSiguvVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<ContratoSiguvVehiculos> ConsultarContratoSiguvVehiculoss(int? pIdContratoSiguv, int? pIdVehiculo, int? pIdRegionalPCI, int? pIdCentroZonal)
        {
            try
            {
                return new Icbf.SIGUV.Business.ContratoSiguvVehiculosBLL().ConsultarContratoSiguvVehiculoss(pIdContratoSiguv, pIdVehiculo, pIdRegionalPCI, pIdCentroZonal);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        #endregion
        #region Relevo
        public int InsertarRelevo(Relevo pRelevo)
        {
            try
            {
                return new Icbf.SIGUV.Business.RelevoBLL().InsertarRelevo(pRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarRelevo(Relevo pRelevo)
        {
            try
            {
                return new Icbf.SIGUV.Business.RelevoBLL().ModificarRelevo(pRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarRelevo(Relevo pRelevo)
        {
            try
            {
                return new Icbf.SIGUV.Business.RelevoBLL().EliminarRelevo(pRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Relevo ConsultarRelevo(int pIdRelevo)
        {
            try
            {
                return new Icbf.SIGUV.Business.RelevoBLL().ConsultarRelevo(pIdRelevo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Relevo> ConsultarRelevos(int? pIdContratoSiguv, int? pIdVehiculoPrincipal, int? pIdVehiculoReemplazo, DateTime? pFechaInicio, DateTime? pFechaFin)
        {
            try
            {
                return new Icbf.SIGUV.Business.RelevoBLL().ConsultarRelevos(pIdContratoSiguv, pIdVehiculoPrincipal, pIdVehiculoReemplazo, pFechaInicio, pFechaFin);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
        #region Vehiculo
        public int InsertarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.VehiculosBLL().InsertarVehiculos(pVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int ModificarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.VehiculosBLL().ModificarVehiculos(pVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }
        public int EliminarVehiculos(Vehiculos pVehiculos)
        {
            try
            {
                return new Icbf.SIGUV.Business.VehiculosBLL().EliminarVehiculos(pVehiculos);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public Vehiculos ConsultarVehiculos(int pIdVehiculo)
        {
            try
            {
                return new Icbf.SIGUV.Business.VehiculosBLL().ConsultarVehiculos(pIdVehiculo);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        public List<Vehiculos> ConsultarVehiculoss(int? pPropio, int? pRelevo, int? pIdClase, String pPlaca, int? pCilindraje, int? pModelo, int? pCapacidad, int? pEstado)
        {
            try
            {
                return new Icbf.SIGUV.Business.VehiculosBLL().ConsultarVehiculoss(pPropio, pRelevo, pIdClase, pPlaca, pCilindraje, pModelo, pCapacidad, pEstado);
            }
            catch (UserInterfaceException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GenericException(ex);
            }
        }

        #endregion
    }
}
