﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;

public partial class vM : GeneralWeb
{
    /// <summary>
    /// Manejador del evento Cargar página
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        ValidateSession();
        String vMi = Request.QueryString["vMi"].ToString();
        Session["MenuId"] = vMi;
    }
}