﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Net.Configuration;
using System.Web.Configuration;
using Icbf.SIA.Entity;


/// <summary>
/// Página de recuperación de contraseña para internos, maneja los eventos de envío de correo y consulta de usuario.
/// </summary>
public partial class Page_RecoverPassword : GeneralWeb
{

    //La declaramos para salvar aqui el nombre de usuario consultado hacia la BD
    //se consulta en el evento VerifyingUser y lo guardamos para que cuando se tenga que 
    //enviar el correo electrónico no volver a consultar.
    private string nombreUsuario = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["correo"] != null)
            {
                TextBox txtNombreusuario = ((TextBox)prGeneraciones.UserNameTemplateContainer.FindControl("UserName"));
                TextBox txtEmail = ((TextBox)prGeneraciones.UserNameTemplateContainer.FindControl("txtEmail"));

                txtEmail.Text = Request.QueryString["correo"].ToString();
                txtNombreusuario.Text = Request.QueryString["correo"].ToString();
            }
        }

    }

    /// <summary>
    /// Manejador del evento click para el botón Cancelar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        string Pagina;
        Pagina = "Default.aspx";
        Response.Redirect(Pagina, false);
    }

    /// <summary>
    /// Recuperar password de una cuenta existente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PasswordRecovery_VerifyingUser(object sender, LoginCancelEventArgs e)
    {

        TextBox txtUserName = ((TextBox)prGeneraciones.UserNameTemplateContainer.FindControl("UserName"));
        Literal FailureText = ((Literal)prGeneraciones.UserNameTemplateContainer.FindControl("FailureText"));
        Usuario oUsuario = ConsultarUsuarioRecordarPass(txtUserName.Text);
        if (oUsuario != null)
        {
            if (oUsuario.IdTipoPersona == 1)
                nombreUsuario = oUsuario.PrimerNombre + " " + oUsuario.SegundoNombre + " " + oUsuario.PrimerApellido + " " + oUsuario.SegundoApellido;
            else if (oUsuario.IdTipoPersona == 2)
                nombreUsuario = oUsuario.RazonSocial;
            MembershipUser usuario = Membership.GetUser(txtUserName.Text);
            string nuevaClave = usuario.ResetPassword();
            this.EnviarNuevaClave(ConfigurationManager.AppSettings["MailFrom"], txtUserName.Text, "ICBF Recuperación de clave de acceso al sistema", "http://www.icbf.gov.co/", nombreUsuario, nuevaClave);

        }
        else
        {
            e.Cancel = true;
            FailureText.Text = "Esta cuenta de correo no existe, si desea crear un nuevo usuario ingresa por la opción Regístra tu cuenta de usuario";
        }
    }

    protected void PasswordRecovery_SendingMail(object sender, MailMessageEventArgs e)
    {
        e.Cancel = true;
    }

    /// <summary>
    /// Enviar nueva clave de acceso
    /// </summary>
    /// <param name="pDe"></param>
    /// <param name="pPara"></param>
    /// <param name="pAsunto"></param>
    /// <param name="pLinkUrl"></param>
    /// <param name="pNombre"></param>
    /// <param name="pContrasena"></param>
    private void EnviarNuevaClave(string pDe, string pPara, string pAsunto, string pLinkUrl, string pNombre, string pContrasena)
    {

        Literal FailureText = ((Literal)prGeneraciones.UserNameTemplateContainer.FindControl("FailureText"));

        MailMessage MyMailMessage = new MailMessage();

        MyMailMessage.From = new MailAddress(pDe);
        MyMailMessage.To.Add(new MailAddress(pPara));
        MyMailMessage.Subject = pAsunto;

        MyMailMessage.IsBodyHtml = true;

        StringBuilder strbulMnj = new StringBuilder();

        //MyMailMessage.Body =
        strbulMnj.AppendLine("<html><body>");
        strbulMnj.AppendLine("  <table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'>");
        strbulMnj.AppendLine("      <tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("      <tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("      <tr><td width='10%' bgcolor='#81BA3D'>&nbsp;</td><td>");
        strbulMnj.AppendLine("          <table width='100%'border='0'align='center'>");
        strbulMnj.AppendLine("              <tr><td width='5%'>&nbsp;</td><td align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td align='left'><strong>Cambio de contraseña</strong></td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td><strong>Apreciado(a): </strong>&nbsp;" + pNombre + "</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>Su nueva contraseña temporal de acceso al sistema es:</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>");
        strbulMnj.AppendLine("                  <table width='100%'border='0'>");
        strbulMnj.AppendLine("                      <tr><td width='50%' align='right'><strong>Contraseña: </strong></td><td width='50%'>" + pContrasena + "</td></tr>");
        strbulMnj.AppendLine("                  </table></td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>Recuerde modificar esta contraseña, una vez inicie la sesión.</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        strbulMnj.AppendLine("          </table></td><td width='10%' bgcolor='#81BA3D'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("      <tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("      <tr><td colspan='3' align='center' bgcolor='#81BA3D'>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo.</td></tr>");
        strbulMnj.AppendLine("      <tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr>");
        strbulMnj.AppendLine("  </table>");
        strbulMnj.AppendLine("</body></html>");

        MyMailMessage.Body = strbulMnj.ToString();

        SmtpClient MySmtpClient = new SmtpClient();

        object userState = MyMailMessage;

        MySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

        try
        {
            MySmtpClient.SendAsync(MyMailMessage, userState);
            //MySmtpClient.Send(MyMailMessage);
        }
        catch (UserInterfaceException ex)
        {
            FailureText.Text = ex.Message;
        }
        catch (Exception ex)
        {
            FailureText.Text = ex.Message;
        }
    }

    /// <summary>
    /// Manejador del evento Completar envìo de correo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        MailMessage MyMailMessage;
        MyMailMessage = (MailMessage)e.UserState;

        Literal FailureText = ((Literal)prGeneraciones.UserNameTemplateContainer.FindControl("FailureText"));
        if (e.Cancelled)
        {
            FailureText.Text = "El envío del correo de activación fue cancelado";
        }
        if (e.Error != null)
        {
            FailureText.Text = "Error: " + e.Error.Message.ToString();
        }
        else
        {
            FailureText.Text = "Su nueva contraseña ha sido enviada a su correo";
        }
    }



}