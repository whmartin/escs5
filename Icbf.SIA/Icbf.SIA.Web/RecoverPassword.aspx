﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecoverPassword.aspx.cs"
    Inherits="Page_RecoverPassword" Async="true" %>

<%@ Import Namespace="System.ComponentModel" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript" language="javascript" src="Scripts/sP.js"></script>
    <link type="text/css" href="Styles/log.css" rel="Stylesheet" />
    <script src="Scripts/jq.min.js" type="text/javascript"></script>
    <script src="Scripts/WS.js" type="text/javascript"></script>
    <script src="Scripts/coin-slider.min.js" type="text/javascript"></script>
    <link href="Styles/coin-slider-styles.css" rel="stylesheet" type="text/css" />
    <title>.: ICBF - SIA :.</title>
    <script type="text/javascript">
        var service = new WS("DefaultWeb.aspx", WSDataType.json);
        $(document).ready(function() {
            $('#coin-slider').coinslider({
                width: 225,
                height: 210
            });
        });
    </script>
</head>
<body style="font-family: Verdana; font-size: smaller; color: White" onload="">
    <form id="form1" runat="server">
    <div class='centrar'>
        <table cellpadding="0" cellspacing="0" class="Loggin">
            <tr>
                <td colspan="2" class="header">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="Logo">
                    <%--<img alt="logo" src="Image/loggin/LogoGCB.jpg" style="height: 263px; width: 177px" />--%>
                    <div class="content">
                        <div id='coin-slider'>
                            <img alt="logo" src="Image/loggin/Img1.jpg" />
                            <img alt="logo" src="Image/loggin/Img2.jpg" />
                            <img alt="logo" src="Image/loggin/Img3.jpg" /> 
                            <img alt="logo" src="Image/loggin/Img4.jpg" />
                            <img alt="logo" src="Image/loggin/Img5.jpg" /> 
                        </div>
                    </div>
                </td>
                <td class="user">
                    <table cellpadding="5" cellspacing="0" class="labels" width="100%">
                        <tr>
                            <td>
                                <asp:PasswordRecovery ID="prGeneraciones" runat="server" SubmitButtonText="Enviar clave"
                                    SuccessText="Tu contraseña ha sido enviada a tu correo electronico." UserNameInstructionText="Ingresa tu usuario y recibiras tu contraseña en tu correo electronico."
                                    UserNameLabelText="Usuario:" UserNameRequiredErrorMessage="El usuario es requerido."
                                    UserNameTitleText="Olvidaste tu contraseña?" OnSendingMail="PasswordRecovery_SendingMail"
                                    OnVerifyingUser="PasswordRecovery_VerifyingUser">
                                    <SuccessTemplate>
                                        <table cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                Tu contraseña ha sido enviada a tu correo electr&oacute;nico.
                                                                <br />
                                                                <br />
                                                                <asp:ImageButton ID="btnRegresar" runat="server" ToolTip="Regresar" PostBackUrl="Default.aspx" ImageUrl="~/Image/btn/logout.png"
                                                                    Height="29px" Width="29px"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </SuccessTemplate>
                                    <UserNameTemplate>
                                        <table cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="2">
                                                        <tr>
                                                            <td align="center" class="sT">
                                                                Olvidaste tu contraseña?
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Usuario:</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowA">
                                                            <td>
                                                                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                    ErrorMessage="El usuario es requerido." ToolTip="El usuario es requerido." ValidationGroup="PasswordRecovery"
                                                                    ForeColor="Red">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="UserName">Correo electr&oacute;nico:</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowA">
                                                            <td>
                                                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                                    ErrorMessage="Ingrese el email que utilizó para el registro" ToolTip="Ingrese el email de registro"
                                                                    ValidationGroup="PasswordRecovery" ForeColor="Red">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr class="rowB">
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr align="right">
                                                            <td>
                                                                <asp:ImageButton ID="btnCancelar" runat="server" OnClick="btnCancelar_Click" Text="Cancelar"
                                                                    ImageUrl="~/Image/btn/logout.png" Width="29px" Height="29px" ToolTip="Regresar"></asp:ImageButton>
                                                                &nbsp;
                                                                <asp:ImageButton ID="btnEnviar" runat="server" CommandName="Submit" Text="Enviar"
                                                                    ValidationGroup="PasswordRecovery" ImageUrl="~/Image/btn/apply.png" Width="29px"
                                                                    Height="29px" ToolTip="Enviar"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </UserNameTemplate>
                                </asp:PasswordRecovery>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="5" cellspacing="0" class="IconosT">
                                    <tr>
                                        <td>
                                            <a href="http://www.icbf.gov.co" target="_blank">
                                                <img alt="icbf" style="text-decoration: none" src="Image/loggin/LogoIcbf.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://wsp.presidencia.gov.co/Portal/Paginas/Default.aspx" target="_blank">
                                                <img alt="prosperidad" src="Image/loggin/ProspTodos.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.deceroasiempre.gov.co/Paginas/deCeroaSiempre.aspx" target="_blank">
                                                <img alt="ceroSiempre" src="Image/loggin/CeroSiempre.png" /></a>
                                        </td>
                                        <td>
                                            <a href="http://www.icbf.gov.co" target="_blank">
                                                <img alt="icbfDigital" src="Image/loggin/IcbfDigital.png" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Footer">
                    Instituto Colombiano de Bienestar Familiar
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
