﻿$(document).ready(function () {
       $.fn.reajustar = function () {
        $(this[0]).css({
            position: 'absolute',
            left: ($(window).width() - $(this[0]).outerWidth()) / 2,
            top: (($(window).height() - 180) - $(this[0]).outerHeight()) / 2
        });
    };

});


function mensajeError(mensaje) {

    $("#lblErrorJs").html(mensaje).show().addClass("lbEJs"); ;

}

function mensajeErrorModal(mensaje) {

    $("#lblErrorJsModal").html(mensaje).show().addClass("lbEJs"); ;

}

function cerrarModal() {

    $('#dvVentanaModal').hide();
    $('#dvContendedorDatosModal').html('');
    $("#lblErrorJsModal").hide();
    $("#dvBloqueo").hide();

}


///***************************Funciones Ajax***************************/

function cargarCombos(url, contenedorDestino, parametros, mensajeCargando, funcionFin, divError) {

    $("#" + contenedorDestino).html("<option value='-1'>" + mensajeCargando + "</option>").attr("disabled", "disabled");

    var funcionFinal = function (msg) {
        $("#" + contenedorDestino).html("<option value='-1'>Seleccione</option>" + msg.d).removeAttr("disabled");
        if (funcionFin != undefined && funcionFin != "")
        { funcionFin(); }

    };

    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        contentType: "application/json",
        dataType: "json",
        success: funcionFinal,
        error: function (jqXHR, textStatus, errorThrown) {

            $("#" + divError).html(jqXHR.responseJSON.d[0]).show();
        }
    });

}

function cargarPaginaAjax(url, contenedorDestino, parametros, funcionSuccess) {
    $("#" + contenedorDestino).html("<image src='../../../Image/main/loading.gif'/>");

    var request = $.ajax({
        url: url,
        method: "POST",
        data: parametros,
        dataType: "html",
        success: function (msg) {
            $("#" + contenedorDestino).html(msg);
            if (funcionSuccess != "")
            { funcionSuccess(); }
        },
        complete: function () {
            //  $("#dvMuestraPregunta").reajustar();

        }
    });

    request.fail(function (jqXHR, textStatus) {
        $("#" + divError).html(jqXHR.responseJSON.Message).show();
    });

}

function ejecutarMetodo(url, contenedorDestino, parametros, funcionFin, divError) {

    var funcionFinal = function (msg) {
        if (funcionFin != undefined && funcionFin != "")
        { funcionFin(msg); }

    };

    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        contentType: "application/json",
        dataType: "json",
        success: funcionFinal,
        error: function (jqXHR, textStatus, errorThrown) {

            $("#" + divError).html(jqXHR.responseJSON.Message).show();
        }
    });

}

///***************************Funciones Ajax***************************/