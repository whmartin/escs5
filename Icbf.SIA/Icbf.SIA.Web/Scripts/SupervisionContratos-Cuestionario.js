﻿
//Remueve un elemento de la lista de origen y lo envia a la lista de destino
function moverOptions(Origen, Destino, ListaGuadrar) {

    $("#" + Origen + " option:selected").each(function (i, o) {
        $("#" + Destino + "").append('<option value=' + $(o).val() + '>' + $(o).text() + '</option>');
        $(o).remove();
    });

    $("#cphCont_listPre").val($('#' + ListaGuadrar + ' option').map(function () { return $(this).val(); }).get().join("-"));
    title();

}

//Agrega el titulo al elemento de la lista
function title() {
    $("#cphCont_lstMain option").each(function (i) {
        $(this).attr("title", this.text);
    });

    $("#cphCont_lstSelect option").each(function (i) {
        $(this).attr("title", this.text);
    });
}

(function ($) {
    $.QueryString = (function (a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i) {
            var p = a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);


$(document).ready(function () {
    postback = false;
    title();

    //Centra en la mitad de la pantalla un objeto
    $.fn.reajustar = function () {
        $(this[0]).css({
            position: 'absolute',
            left: ($(window).width() - $(this[0]).outerWidth()) / 2,
            top: ($(window).height() - $(this[0]).outerHeight()) / 2
        });
    };

    $("#btnGuardar").removeAttr("onclick");
    $("#btnGuardar").click(function (event) {

        if (postback != true) {
            guardar(event);
        }

        postback = false;
    });
});


function guardar(event) {

    if ($("#lstSelect option").length == 0) {
        $("#lblErrorJs").html("Debe agregar una pregunta al listado").show();
        event.preventDefault();
    }
    else {

        if ($("#lblErrorJs:visible").length > 0) {
            $("#lblErrorJs").hide();
        }



    }

}


