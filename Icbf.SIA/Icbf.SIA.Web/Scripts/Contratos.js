﻿function GetContratoClase(camposValor, posicionList, claseContrato) {
    var rutaContratos = '../../../Page/Contratos/Lupas/LupaContratos.aspx?cc=' + claseContrato;
    window__showModalDialog(rutaContratos, setReturnGetContratoClase, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
}
function setReturnGetContratoClase(dialog) {
    var pObj = window_returnModalDialog(dialog);
    if (pObj != undefined && pObj != null) {

        var retLupa = pObj.split(",");
        if (retLupa.length >= camposValors.length) {
            var camposValors = camposValor.split(',');
            var posicionList = posicionList.split(',');
            if (camposValors.length == posicionList.length) {
                for (var i = 0; i < camposValors.length; i++) {
                    $("#" + camposValors[i] + "").val(retLupa[posicionList[i]]);
                    /* posicionList: 0 IdContrato, 1 Fecha Registro, 2 Numero Proceso,
                    3 Numero Contrato, 4 Modalidad, 5 Categoria Contrato, 6 Tipo Contrato */
                }
            }
        }
    }
}

function GetContrato(camposValor, posicionList) {
    window__showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
}

function setReturnGetContrato(dialog) {
    var pObj = window_returnModalDialog(dialog);
    if (pObj != undefined && pObj != null) {

        var retLupa = pObj.split(",");
        if (retLupa.length >= camposValors.length) {
            var camposValors = camposValor.split(',');
            var posicionList = posicionList.split(',');
            if (camposValors.length == posicionList.length) {
                for (var i = 0; i < camposValors.length; i++) {
                    $("#" + camposValors[i] + "").val(retLupa[posicionList[i]]);
                    /* posicionList: 0 IdContrato, 1 Fecha Registro, 2 Numero Proceso,
                    3 Numero Contrato, 4 Modalidad, 5 Categoria Contrato, 6 Tipo Contrato
                    */
                }
            }
        }
    }
}
function GetTercero(camposValor, posicionList) {
    window__showModalDialog('../../../Page/Contratos/Lupas/LupaTercero.aspx', setReturnGetTercero, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
}
function setReturnGetTercero(dialog) {
    var pObj = window_returnModalDialog(dialog);
    if (pObj != undefined && pObj != null) {

        var retLupa = pObj.split(",");
        if (retLupa.length >= camposValors.length) {
            var camposValors = camposValor.split(',');
            var posicionList = posicionList.split(',');
            if (camposValors.length == posicionList.length) {
                for (var i = 0; i < camposValors.length; i++) {
                    $("#" + camposValors[i] + "").val(retLupa[posicionList[i]]);
                    /* posicionList: 0 IdTercero, 1 Tipo Persona, 2 Tipo Identificacion,
                    3 Numero Identificacion, 4 Tercero, 5 Correo Electronico, 6 Validacion */
                }
            }
        }
    }
}
function GetGarantia(camposValor, posicionList) {
    window__showModalDialog('../../../Page/Contratos/Lupas/LupaGarantias.aspx', setGetGarantia, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
}

function setGetGarantia(dialog) {
    var pObj = window_returnModalDialog(dialog);
    if (pObj != undefined && pObj != null) {

        var retLupa = pObj.split(",");
        if (retLupa.length >= camposValors.length) {
            var camposValors = camposValor.split(',');
            var posicionList = posicionList.split(',');
            if (camposValors.length == posicionList.length) {
                for (var i = 0; i < camposValors.length; i++) {
                    $("#" + camposValors[i] + "").val(retLupa[posicionList[i]]);
                    /* posicionList: 0 IdTercero, 1 Tipo Persona, 2 Tipo Identificacion,
                    3 Numero Identificacion, 4 Tercero, 5 Correo Electronico, 6 Validacion */
                }
            }
        }
    }
}
