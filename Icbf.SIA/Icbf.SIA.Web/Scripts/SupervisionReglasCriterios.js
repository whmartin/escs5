﻿$(document).ready(function () {
    postback = false;

    $("#ddlDireccion").change(function () {
        cargueSubCompPregunta(this);
    });


    //cargarPaginaAjax("CargueCriterios", parametros);

    $("#ddlPregunta").change(function () {
        cargueRespuestas(this);
    });


    if ($("#hfIdConfCriterio").val() == '')//Si no hay un idCriterio es un insertar
    { carguePaginaDetalle(1, ''); }
    else
    { carguePaginaDetalle($("input[name*='rblConfig']:checked").val(), $("#hfIdConfCriterio").val()); }



    $("input[name*='rblConfig']").click(function () {

        //alert($(this).val());
        carguePaginaDetalle($(this).val(), $("#hfIdConfCriterio").val());

    });

    //Centra en la mitad de la pantalla un objeto 
    $.fn.reajustar = function () {
        $(this[0]).css({
            position: 'absolute',
            left: ($(window).width() - $(this[0]).outerWidth()) / 2,
            top: ($(window).height() - $(this[0]).outerHeight()) / 2
        });
    };


    $("#btnGuardar").removeAttr("onclick");
    $("#btnGuardar").click(function (event) {
        if (postback != true) {
            guardar(event);
        }
        postback = false;
    });


    function guardar(event) {

        $("#lblErrorJs").hide();

        var valorMaximo = 0;
        var valorMinimo = 0;
        var tipoConfiguracion = $("input[name*='rblConfig']:checked").val();

        if ($("#ddlDireccion").val() == "-1") {
            mensajeError("Debe seleccionar una dirección")
        }
        else if ($("input[name*='rdblAplicableA']").length == 0) {
            mensajeError("Debe seleccionar una entidad aplicable")
        }
        else if ($("#ddlPregunta").val() == "-1") {
            mensajeError("Debe seleccionar una pregunta")
        }
        else if ($("#ddlRespuesta").val() == "-1") {
            mensajeError("Debe seleccionar una respuesta")
        }
        else if ($("input[name*='rblConfig']").length == 0) {
            mensajeError("Debe seleccionar un tipo de configuración")
        }
        else if ($("input[name*='rblEstado']").length == 0) {
            mensajeError("Debe seleccionar un estado")
        }
        else {

            if (tipoConfiguracion == "1") {

                if ($("input[name*='chklCriterios']:checked").length == 0) {
                    mensajeError("Debe seleccionar por lo menos un criterio de la lista")
                }
                else {
                    var criterios = $("input[name*='chklCriterios']:checked").map(function () { return $(this).val(); }).get().join("-");
                    btnProcesoGuardar(tipoConfiguracion, valorMaximo, valorMinimo, criterios);
                }
            }

            else if (tipoConfiguracion == "2") {
                valorMaximo = $("#txtMaximo").val();
                valorMinimo = $("#txtMinimo").val();
                btnProcesoGuardar(tipoConfiguracion, valorMaximo, valorMinimo, "");
            }

            else {

                mensajeError("Debe seleccionar un tipo de configuración")
                //alert("se procesa el guardar");
            }
        }

        event.preventDefault();

    }

});


///***************************Funciones Ajax***************************/

function cargarCombos(url, contenedorDestino, parametros, mensajeCargando, funcionFin, divError) {

    $("#" + contenedorDestino).html("<option value='-1'>" + mensajeCargando + "</option>").attr("disabled", "disabled");

    var funcionFinal = function (msg) {
        $("#" + contenedorDestino).html("<option value='-1'>Seleccione</option>" + msg.d).removeAttr("disabled");
        if (funcionFin != undefined && funcionFin != "")
        { funcionFin(); }

    };

    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        contentType: "application/json",
        dataType: "json",
        success: funcionFinal,
        error: function (jqXHR, textStatus, errorThrown) {

            $("#" + divError).html(jqXHR.responseJSON.d[0]).show();
        }
    });

}

function cargarPaginaAjax(url, contenedorDestino, parametros, funcionSuccess) {
    $("#" + contenedorDestino).html("<image src='../../../Image/main/loading.gif'/>");

    var request = $.ajax({
        url: url,
        method: "POST",
        data: parametros,
        dataType: "html",
        success: function (msg) {
            $("#" + contenedorDestino).html(msg);
            if (funcionSuccess != "")
            { funcionSuccess(); }
        },
        complete: function () {
            //  $("#dvMuestraPregunta").reajustar();

        }
    });

    request.fail(function (jqXHR, textStatus) {
        $("#" + divError).html(jqXHR.responseJSON.Message).show();
    });

}

function ejecutarMetodo(url, contenedorDestino, parametros, funcionFin, divError) {

    var funcionFinal = function (msg) {
        if (funcionFin != undefined && funcionFin != "")
        { funcionFin(msg); }

    };

    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        contentType: "application/json",
        dataType: "json",
        success: funcionFinal,
        error: function (jqXHR, textStatus, errorThrown) {

            $("#" + divError).html(jqXHR.responseJSON.Message).show();
        }
    });

}

///***************************Funciones Ajax***************************/

function cargueSubCompPregunta(obj) {

    if ($(obj).val() == '-1') {

        $("#ddlPregunta").html("<option value='-1'>Seleccione</option>");
    }
    else {

        var parametrosP = "{pDireccion:'" + $(obj).val() + "',pAplicable:'" + $("input[name*='rdblAplicableA']:checked").val() + "'}";
        cargarCombos("Add.aspx/ConsultaPreguntas", "ddlPregunta", parametrosP, "Consulando Preguntas", "", "lblErrorJs");

    }
}

function cargueRespuestas(obj) {

    if ($(obj).val() == '-1') {

        $("#ddlRespuesta").html("<option value='-1'>Seleccione</option>");
    }
    else {

        var parametros = "{pIdPregunta:'" + $(obj).val() + "'}";

        cargarCombos("Add.aspx/ConsultaRespuestas", "ddlRespuesta", parametros, "Consulando Respuestas", "", "lblErrorJs");

    }
}


function carguePaginaDetalle(tipoConf, idCriterio) {
    var parametros = { "tipoConf": tipoConf, "IdPregunta": $("#ddlPregunta").val(), "IdCriterio": idCriterio };
    cargarPaginaAjax("AddDetalle.aspx", "CargueCriterios", parametros, "");

}

function mensajeError(mensaje) {

    $("#lblErrorJs").html(mensaje).show().addClass("lbEJs"); ;

}

function ocultarDiv() {

    $("#dvBloqueo,#dvConfirmacion").hide();
}


function btnProcesoGuardar(tipoConf, valMaximo, valMinimo, criterios) {
    $("#dvBloqueo").show();
    $("#dvConfirmacion").show().reajustar();
    $("#btnAceptar").unbind("click");
    $("#btnAceptar").click(function () { guardarConfCriterios(tipoConf, valMaximo, valMinimo, criterios) });
}

function guardarConfCriterios(tipoConf, valMaximo, valMinimo, criterios) {

    var criterios = "''";
    if (tipoConf == "1")
    { criterios = "'" + $("input[name*='chklCriterios']:checked").map(function () { return $(this).val(); }).get().join("-") + "'"; }

    var parametrosGuardar = "{'tipoConf':" + tipoConf + ",'IdRespuesta':" + $("#ddlRespuesta").val() + ",'Criterios':" + criterios + ",'valMaximo':" + valMaximo + ",'valMinimo':" + valMinimo + "}";

    ejecutarMetodo("Add.aspx/GuardarConfiguracion", "lblErrorJs", parametrosGuardar, function (msg) {

        if (msg.d == "@OK") {
            $("#lblErrorJs").html("Configuración guardada exitosamente").addClass("lbEM");
        }
        else {
            $("#lblErrorJs").html(msg.d);
        }

        $("#dvBloqueo,#dvConfirmacion").hide();
        $("#lblErrorJs").show();

    }, "lblErrorJs");

}
