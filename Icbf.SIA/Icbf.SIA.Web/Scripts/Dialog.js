﻿function window_showModalDialog(url, functionReturn, parameters) {
    //busca el div donde se crean los hidden con la información retornada por los dialog

    var nivel = 1;
    var window_parent = window
    var obj = jQuery('#DialogoMaster');
    while (obj.length == 0 && nivel < 10) {
        nivel++;
        obj = jQuery('#DialogoMaster', window_parent.document);
        window_parent = window_parent.parent
    }

    var widthframe = $(window.parent.document).width();
    var heightframe = $(window.parent.document).height();
    var resizeframe = true;

    //crea el nombre con el que se va a identificar cada uno de los objetos
    var _pageName = url.substring(url.lastIndexOf("../") + 3).replace(/\//g, '_');
    _pageName = _pageName.substring(0, _pageName.indexOf("."));
    if (parameters != null && parameters.length > 0) {
        var _parameters = parameters.split(';');
        if (_parameters.count > 0) {
            for (var i = 0; i < _parameters.count; i++) {
                var tuple = _parameters[i].split('=');
                if (tuple.length >= 2) {
                    if (tuple[0] == 'dialogWidth') {
                        widthframe = tuple[1].replace('px', '');
                    }
                    if (tuple[0] == 'dialogHeight') {
                        heightframe = tuple[1].replace('px', '');
                    }
                    if (tuple[0] == 'resizable') {
                        resizeframe = tuple[1] == 'yes' ? true : false;
                    }
                }
            }
        }
    }

    //si ya existió se elimina
    jQuery('#hdLupa' + _pageName, window_parent.document).remove();
    jQuery('#dialog' + _pageName, window_parent.document).remove();

    //se crea el hidden y se crea el div que va actuar como dialog con el iframe interno
    obj.html('<input type="Hidden" id ="hdLupa' + _pageName + '"/>' +
                '<div id="dialog' + _pageName + '" >  <iframe id="iframe_' + _pageName + '" name="iframe_' + _pageName + '"style="border: 0px; " src="' + url + '" width="' + ((widthframe * 0.86 < 1000 ? 1000 : widthframe * 0.86)) + 'px" height="1000px"></iframe></div>');

    var obj2 = obj.find('#dialog' + _pageName);
    obj2.dialog({
        autoOpen: false,
        show: {
            effect: "scale",
            duration: 500
        },
        hide: {
            effect: "scale",
            duration: 500
        },
        resizable: resizeframe,
        height: heightframe * 0.9,
        width: widthframe * 0.9,
        modal: true,
        position: { my: "center center",
            at: "center center",
            of: window_parent.document,
            collision: "none none"
        },
        close: function () {
            if ($.isFunction(functionReturn)) {
                functionReturn(_pageName);
            }
            obj2.html("");
            obj2.dialog("destroy");
        }
    });
    obj2.dialog('open');
}

function window_closeModalDialog(dialog) {
    var nivel = 1;
    var window_parent = window.parent;
    var obj = jQuery('body #' + dialog);
    while (obj.length == 0 && nivel < 10) {
        nivel++;
        obj = jQuery("body #" + dialog, window_parent.document);
        window_parent = window_parent.parent
    }

    //como se pierde la referencia al dialog, se dispara el botón cerrar.
    obj.prev().find("button").trigger("click");
}

function window_returnModalDialog(dialog) {
    var nivel = 1;
    var window_parent = window.parent;
    var obj = jQuery("#hdLupa" + dialog);
    while (obj.length == 0 && nivel < 10) {
        nivel++;
        obj = jQuery("#hdLupa" + dialog, window_parent.document);
        window_parent = window_parent.parent
    }
    //se retorna la información almacenada en el hidden y se elimina
    var value = obj.val();
    obj.remove();
    return value;
}