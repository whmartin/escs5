﻿function colapsar() {
    var objeto = document.getElementById('MenuDiv');
    if (objeto.style.display == 'block') {
        objeto.style.display = 'none';
        setCookie("ebGCB", "0");
    }
    else {
        objeto.style.display = 'block';
        setCookie("ebGCB", "1");
    }
}
function getState() {


    var Estado = getCookie("ebGCB");
    var divM = document.getElementById("m");
    if (divM != null) {
        if (Estado == "0") {
            if (divM.style.display != "none") {
                divM.style.display = "none";
            }
            else {
                divM.style.display = "";
                setCookie("ebGCB", "1");
            }

            AjustarHorizontal();
        }
    }
}
function getCookie(name) {
    var cname = name + "=";
    var dc = document.cookie;
    if (dc.length > 0) {
        begin = dc.indexOf(cname);
        if (begin != -1) {
            begin += cname.length;
            end = dc.indexOf(";", begin);
            if (end == -1) end = dc.length;
            return unescape(dc.substring(begin, end));
        }
    }
    return null;
}
function setCookie(name, value) {
    document.cookie = name + "=" + escape(value);
}



function AjustarHorizontal() {
    var divM = document.getElementById("m");
    var divT = document.getElementById("t");
    var divF = document.getElementById("f");

    if (divF != null) {
        divF.style.width = "425px";
        divF.style.width = document.documentElement.clientWidth - (divM.clientWidth + divT.clientWidth) + "px";
    }

}

function AjustarVertical() {
    //document.documentElement.style.overflow = "hidden";
    var divH = document.getElementById("h");
    var divP = document.getElementById("p");

    var divM = document.getElementById("m");
    var divT = document.getElementById("t");
    var divF = document.getElementById("f");
    var divFT = document.getElementById("dvft");
    var divErr = document.getElementById("lblError");
    var taFT = document.getElementById("fT");




    //    if (divM != null && divT != null && divF != null) {
    //        if (document.documentElement.clientHeight - (divH.clientHeight + divP.clientHeight) < divFT.clientHeight + divErr.clientHeight + taFT.clientHeight) {
    //        
    //            divM.style.height = divF.clientHeight + "px";
    //            divT.style.height = divF.clientHeight + "px";
    //            document.documentElement.style.overflow = "auto";
    //        }
    //        else {
    //            var hVert = document.documentElement.clientHeight - (divH.clientHeight + divP.clientHeight) + "px";
    //            divM.style.height = hVert;
    //            divT.style.height = hVert;
    //            divF.style.height = hVert;
    //            document.documentElement.style.overflow = "hidden";
    //        }
    //    }

    if (divM != null && divT != null && divF != null) {
        if (document.documentElement.clientHeight > 400) {

            if (divErr == null) {
                divM.style.height = (document.documentElement.clientHeight - divH.clientHeight - divP.clientHeight) + "px";
                divT.style.height = (document.documentElement.clientHeight - divH.clientHeight - divP.clientHeight) + "px";
                divFT.style.height = (document.documentElement.clientHeight - divH.clientHeight - divP.clientHeight - taFT.clientHeight) + "px";

                document.documentElement.style.overflow = "hidden";
            }
            else {
                divM.style.height = (document.documentElement.clientHeight - divH.clientHeight - divP.clientHeight - divErr.clientHeight) + "px";
                divT.style.height = (document.documentElement.clientHeight - divH.clientHeight - divP.clientHeight - divErr.clientHeight) + "px";
                divFT.style.height = (document.documentElement.clientHeight - divH.clientHeight - divP.clientHeight - taFT.clientHeight - divErr.clientHeight - 24) + "px";

                document.documentElement.style.overflow = "hidden";
            }
        }
        else {
            divM.style.height = (400 - divH.clientHeight - divP.clientHeight) + "px";
            divT.style.height = (400 - divH.clientHeight - divP.clientHeight) + "px";
            divFT.style.height = (400 - divH.clientHeight - divP.clientHeight - taFT.clientHeight) + "px";
            document.documentElement.style.overflow = "auto";
        }
    }
}

function AjustarTodo() {
    AjustarHorizontal();
    AjustarVertical();
}

function ocultarMenu() {
    var divM = document.getElementById("m");
    if (divM != null) {
        if (divM.style.display != "none")
            divM.style.display = "none";
        else
            divM.style.display = "";

        AjustarHorizontal();
    }
}
function SetFocus(nombre) {
    var objeto = document.getElementById(nombre);
    if (objeto) {
        objeto.focus();
    }
}
function ReadOnly(evt) {
    if (!evt)
        evt = window.event;

    code = null;

    if (evt.keyCode)
        code = evt.keyCode;
    else if (evt.which)
        code = eVT.which;

    if (code == 9)
        return;

    evt.cancelBubble = true;
    if (evt.preventDefault)
        evt.preventDefault();
    if (evt.stopPropagation)
        evt.stopPropagation();
    return false;
}
window.onresize = AjustarTodo;
/// Visualiza la imagen de carga para registro o edicion de informacion
function Loading() {
    if (Page_ClientValidate()) {
//        var imgLoading = document.getElementById("imgLoading");
        //        imgLoading.style.visibility = "visible";
        if (typeof $.blockUI != "undefined") {
            $.blockUI(
       {
           message: '<h1><img src="../../../Image/main/loading.gif"/></h1>',
           centerX: true,
           centerY: true,
           css: { width: '70px', border: 'none' },
           overlayCSS: { backgroundColor: '#FFFFFF', opacity: 0, border: '1px solid #000000' }
       });
        }
       
        return true;
    }
}

/// Visualiza la imagen de carga para consulta de informacion
function LoadingList() {
    var imgLoading = document.getElementById("imgLoading");
    imgLoading.style.visibility = "visible";
}

function BuscarInputs() {
    Loading();
    var inputs, i,l;
    try {
        for (i = 0; current = document.getElementsByTagName('input')[i]; i++) {
            if (current.type == "text") {
                if (current.id != 'cphCont_txtNombreUsuario' && current.id != 'cphCont_txtCuentaCorreo' && current.id != 'cphCont_TxtSitioWeb' && current.id != 'cphCont_TxtCorreoElectronicoRepr' && current.id != 'cphCont_TxtCorreoElectronico' && current.id != 'cphCont_txtCuentaCorreo' && current.id != 'cphCont_txtCuentaUsuario' && current.id != 'cphCont_txtEmail' && current.id != 'cphCont_txtCorreo') {
                    current.value = current.value.toUpperCase();
                }
            }
        }
        for (l = 0; current = document.getElementsByTagName('textarea')[l]; l++) {
            current.value = current.value.toUpperCase();
        }
        return true;
    }
    catch (err) {
        return false;
    }
}

function MiFuncion(miMenu) {
    // This AJAX call will save the Navigator's state to session.
    // We don't need a callback function because nothing happens
    // once said state is saved.
    var url = "../../../vM.aspx?vMi=" + miMenu;

    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("POST", url, true);
    xmlhttp.send();
}

function checkTextAreaMaxLength(textBox, e, length) {

    var mLen = textBox["MaxLength"];
    if (null == mLen)
        mLen = length;

    var maxLength = parseInt(mLen);
    if (!checkSpecialKeys(e)) {
        if (textBox.value.length > maxLength - 1) {
            if (window.event)//IE
                e.returnValue = false;
            else//Firefox
                e.preventDefault();
        }
    }
}