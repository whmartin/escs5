﻿$(document).ready(function () {
    postback = false;

    $("#btnGuardar").removeAttr("onclick");
    $("#btnGuardar").click(function (event) {

        if (postback != true) {
            guardar(event);
        }

        postback = false;
    });
});

function guardar(event) {
    var texto = $(":input[id$='txtNombre']").val();
    var combo = $("select[id$='ddlIdTipoRespuesta'] option:selected").val();
    if (texto != "" && combo != "-1") {

        if ($(":input[name$='rblTipoLogicaCerrada']:enabled").length > 0) {

            if ($(":input[name$='rblTipoLogicaCerrada']:checked").length > 0) {
                mostrarDiv();
                event.preventDefault();
            }
        }
        else {
            mostrarDiv();
            event.preventDefault();
        }
    }
}

function mostrarDiv() {
    $("#dvBloqueo").show("slow", function () {

        $("#dvConfirmacion").show("slow", function () {
            $("#dvConfirmacion").reajustar();
        });
    });
}

function ocultarDiv() {
    $("#dvConfirmacion,#dvBloqueo").hide("slow");
}


function cargarPaginaAjax(url, contenedorDestino, parametros, funcionSuccess) {
    $("#" + contenedorDestino).html("<image src='../../../Image/main/loading.gif'/>");

    var request = $.ajax({
        url: url,
        method: "POST",
        data: parametros,
        dataType: "html",
        success: function (msg) {
            $("#" + contenedorDestino).html(msg);
            if (funcionSuccess != "")
            { funcionSuccess(); }
        },
        complete: function () {
            //  $("#dvMuestraPregunta").reajustar();

        }
    });

    request.fail(function (jqXHR, textStatus) {
        $("#" + divError).html(jqXHR.responseJSON.Message).show();
    });

}

//Centra en la mitad de la pantalla un objeto
//$.fn.reajustar = function () {
//    $(this[0]).css({
//        position: 'absolute',
//        left: ($(window).width() - $(this[0]).outerWidth()) / 2,
//        top: ($(window).height() - $(this[0]).outerHeight()) / 2
//    });
//    return this;
//};

function consultaTerceros() {

    $("#dvVentanaModal").css("border", "1px solid grey").css("width", "550px").css("height", "auto").css("position", "absolute").show().reajustar();
    var parametros = { mostrar: '0' };
    cargarPaginaAjax('../Lupas/LupaIntegranteGrupoApoyo.aspx', 'dvContendedorDatosModal', parametros, "");

}


function cargaGridTerceros(indice) {

     $("#dvVentanaModal").reajustar();
    var TipoDocumento = $("#ddlTipoDocTercero").val();
    //Se valida que por lo menos una de las opciones esté seleccionada
    if (TipoDocumento == "-1" && $.trim($("#txtIdentificacionTercero").val()) == "" && $.trim($("#txtNombre1Tercero").val()) == "" && $.trim($("#txtNombre2Tercero").val()) == "" && $.trim($("#txtApellido1Tercero").val()) == "" && $.trim($("#txtApellido2Tercero").val()) == "")
    { mensajeErrorModal("Debe suministrar criterios de búsqueda"); }
    //Se valida que si se seleccionó un tipo de documento se digite un número de documento
    else if (TipoDocumento != "-1" && $.trim($("#txtIdentificacionTercero").val()) == "")
    { mensajeErrorModal("Debe suministrar un número de documento para la búsqueda"); }
    else {

        if (TipoDocumento == "-1") {
            TipoDocumento = "";
        }

        var parametros = { mostrar: '1', tipoDoc: TipoDocumento, numDoc: $.trim($("#txtIdentificacionTercero").val()),
            nombre1: $.trim($("#txtNombre1Tercero").val()),
            nombre2: $.trim($("#txtNombre2Tercero").val()),
            apellido1: $.trim($("#txtApellido1Tercero").val()),
            apellido2: $.trim($("#txtApellido2Tercero").val()),
            indice: indice
        };


        cargarPaginaAjax('../Lupas/LupaIntegranteGrupoApoyo.aspx', 'dvGridIntegrantes', parametros, "");
        $("#dvVentanaModal").css("height", "auto").reajustar();
    }

}


function asignarNombres(numeroDocumento, nombres, apellidos) {

    $("#txtIdentificacion").val(numeroDocumento).attr("disabled", "true");
    $("#txtNombres").val(nombres).attr("disabled", "true"); ;
    $("#txtApellidos").val(apellidos).attr("disabled", "true"); ;
    cerrarModal();
}

function guardarIntegrante() {
    var ventanaConfirmacion = $("#dvConfirmacion").html();
    var parametros = "{ pNombreGrupo:'" + $("#txtGrupo").val() + "',  pIdgrupo:'" + $("#hfIdGrupo").val() + "',  pIdDireccion:'" + $("#ddlIdDireccion").val() + "',  pNdocIntegrante:'" + $("#txtIdentificacion").val() + "',  pNombIntegrante:'" + $("#txtNombres").val() + "',  pApeIntegrante:'" + $("#txtApellidos").val() + "',  pEstado:'" + $("input[name*='rblEstado']:checked").val() + "'}";
    ejecutarMetodo("Add.aspx/Guardar", "dvConfirmacion", parametros,

    function (msg) {

        if (msg.d == "@OK") {
            $("#lblErrorJs").html("Configuración guardada exitosamente").addClass("lbEM");
        }
        else {
            $("#lblErrorJs").html(msg.d).removeClass("lbEM").addClass("lbEJs");
        }

        $("#dvConfirmacion").html(ventanaConfirmacion);
        ocultarDiv();
        $("#lblErrorJs").show();

    }

    , "lblErrorJs");

}