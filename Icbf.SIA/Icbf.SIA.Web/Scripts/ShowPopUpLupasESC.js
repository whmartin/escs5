﻿var vIdConsecutiEstudio;
function GetListRSE(vIdConsecutiEstudio) {
    this.vIdConsecutiEstudio = vIdConsecutiEstudio
    muestraImagenLoading();
    document.getElementById(vIdConsecutiEstudio).disabled = false;
    window_showModalDialog('../../../Page/EstudioSectorCosto/Lupas/LupaRegistroInicial.aspx', setReturnGetListRSE, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    ocultaImagenLoading();
}
function setReturnGetListRSE(dialog) {
    var pObj = window_returnModalDialog(dialog);

    if (pObj != undefined && pObj != null && pObj != '') {
        var retLupa = pObj.split(",");        
        document.getElementById(vIdConsecutiEstudio).value = retLupa[0];

        __doPostBack( vIdConsecutiEstudio , "TextChanged");
    }
    else {
        document.getElementById( vIdConsecutiEstudio ).disabled = true;
    }
}

function muestraImagenLoading() {
    var imgLoading = document.getElementById("imgLoading");
    imgLoading.style.visibility = "visible";
}

function ocultaImagenLoading() {
    var imgLoading = document.getElementById("imgLoading");
    imgLoading.style.display = 'none';
}