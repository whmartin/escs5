﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultDenuncias.aspx.cs" Inherits="DefaultDenuncias" %>

<%@ Import Namespace="Subkismet" %>
<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    /// <summary>
    /// Evento para cerrar el slider
    /// </summary>
    /// <param name="sender">The Button</param>
    /// <param name="e">The Click</param>
    protected void lbCerrar_Click(object sender, EventArgs e)
    {
        this.mpeMessage.Hide();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script type="text/javascript" language="javascript" src="Scripts/pI.js"></script>
    <link type="text/css" href="Styles/log.css" rel="Stylesheet" />
    <script src="Scripts/jq.min.js" type="text/javascript"></script>
    <script src="Scripts/WS.js" type="text/javascript"></script>
    <script src="Scripts/coin-slider.min.js" type="text/javascript"></script>
    <link href="Styles/coin-slider-styles.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Proveedores.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="Scripts/slider/sliderman.1.3.8.js"></script>
    <link rel="stylesheet" type="text/css" href="Styles/slider/sliderman.css" />
    <title>.: ICBF - SIA - Denuncias:.</title>


    <script type="text/javascript">
        var service = new WS("DefaultDenuncias.aspx", WSDataType.json);
        $(document).ready(function () {

            $('#coin-slider').coinslider({
                width: 225,
                height: 210
            });


            // APLICA WATERMARK SI EL TEXTBOX NO TIENE VALOR
            $("#loGeneraciones_UserName").each(function () {

                if ($(this).text() == "aa@example.com")
                    $(this).val('').addClass('WaterMarkOn');
                else
                    if ($.trim($(this).val()) == '')
                        $(this).addClass('WaterMarkOn').val("aa@example.com");
            });

            $("#loGeneraciones_Password").each(function () {

                if ($(this).text() == "Contraseña")
                    $(this).val('').addClass('WaterMarkOn');
                else
                    if ($.trim($(this).val()) == '')
                        $(this).addClass('WaterMarkOn').val("Contraseña");
            });

            // AL OBTENER EL FOCO LIMPIA TITLE Y QUITA CLASE DE MARCA
            $("#loGeneraciones_UserName").focus(function () {
                if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
            });

            $("#loGeneraciones_Password").focus(function () {
                if ($(this).hasClass("WaterMarkOn")) $(this).removeClass('WaterMarkOn').val('');
            });

            // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
            $("#loGeneraciones_Password").blur(function () {
                if ($(this).val() == '')
                    $(this).addClass('WaterMarkOn').val("Contraseña");
            });

            // AL PERDER FOCO SI EL INPUT ESTÁ VACIO VUELVE A PONER MARCA
            $("#loGeneraciones_UserName").blur(function () {
                if ($(this).val() == '')
                    $(this).addClass('WaterMarkOn').val("aa@example.com");
            });

            $.ajax({
                type: "POST",
                url: "DefaultDenuncias.aspx/RetornarContador",
                data: "{}",
                contentType: "application/json; chartset:utf-8",
                dataType: "json",
                success:
                          function (data) {
                              if (data.d == "1") {
                                  $("#CaptchaDiv").show();
                              }
                          },
                async: true,
                cache: false
            });
        });
    </script>

    <style type="text/css">
        .modalBackground {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .ModalWindow {
            border: solid1px#c0c0c0;
            background: #89C566;
            padding: 0px10px10px10px;
            position: absolute;
        }

        .divModalDialog {
            margin: 5px;
            text-align: center;
        }

        .rfvalidatorCodigo {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: Red;
        }

        .botonLog {
            color: White;
            background-color: #66C42F;
            border-color: #66C42F;
            height: 29px;
            width: 200px;
            font-size: 12px;
            padding: 0px 10px 0px 10px;
            margin: 5px 0px 5px 0px;
        }

        .lblMensage {
            font-family: Verdana;
            font-size: small;
            color: Black;
            font-weight: bold;
        }

        .cssDivMsg {
            text-align: justify;
            width: 225px;
        }

        .CustomValidatorCalloutStyle div, .CustomValidatorCalloutStyle td {
            border: solid 1px white;
            background-color: #66C42F;
            color: white;
            font-size: 11px;
        }

        .style1 {
            width: 143px;
        }

        .style2 {
            width: 93px;
        }

        .modalPopup {
            width: 1000px;
            height: 450px;
            border: 3px solid #FFF;
            border-radius: 12px;
            padding: 0;
            margin-top: 35px;
        }

            .modalPopup .header {
                background-color: #81BA3D;
                height: 450px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
                border-top-left-radius: 6px;
                border-top-right-radius: 6px;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }
    </style>
</head>
<body style="font-family: Verdana; font-size: smaller; color: White" onload="SetFocus('loGeneraciones_UserName');">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:LinkButton runat="server" ID="lbAyuda" OnClick="lbAyuda_Click" Text="Ayuda para denuncias"
            CausesValidation="false" Visible="true" CssClass="btnAbrir"></asp:LinkButton>
        <div class='centrar'>
            <table cellpadding="0" cellspacing="0" class="Loggin">
                <tr>
                    <td class="Logo">
                        <div class="content">
                            <div id='coin-slider'>
                                <img alt="logo" src="Image/loggin/Img6.jpg" />
                                <img alt="logo" src="Image/loggin/Img7.jpg" />
                                <img alt="logo" src="Image/loggin/Img8.jpg" />
                            </div>
                        </div>
                    </td>
                    <td class="user">
                        <table cellpadding="5" cellspacing="0" class="labels" width="100%">
                            <tr>
                                <td>
                                    <img alt="logo" src="Image/loggin/LogoDenuncias.png" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td style="color: #669933">Bienes, vacantes, mostrencos y vocaciones hereditarias
                                </td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <asp:Login ID="loGeneraciones" runat="server" Width="100%" LabelStyle-Width="30%"
                                        CssClass="labels input_byclass" DisplayRememberMe="False"
                                        UserNameLabelText="" OnLoggedIn="OnLoggedIn"
                                        OnLoginError="OnLoginError" ForeColor="Black" Height="120px"
                                        LoginButtonImageUrl="~/Image/btn/apply.png" LoginButtonText="Iniciar Sesión"
                                        UserNameRequiredErrorMessage="Ingrese usuario"
                                        LoginButtonType="Button" OnLoggingIn="OnLoggingIn"
                                        PasswordLabelText=""
                                        TitleText="" FailureText="Datos de usuario invalidos. verifique por favor."
                                        PasswordRequiredErrorMessage="Ingrese la contraseña"
                                        InstructionText=" "
                                        PasswordRecoveryUrl="RecoverPassword.aspx" CreateUserUrl="~/Page/General/RegistroUsuario/Add.aspx"
                                        PasswordRecoveryText="Olvidaste tu clave?" UserName="">
                                        <LayoutTemplate>
                                            <table cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" style="height: 120px; width: 100%;">
                                                            <tr>
                                                                <td align="center" colspan="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <%-- <asp:Label ID="lblUser" runat="server" Text="Usuario *" />--%>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="UserName" runat="server" CssClass="caja" Width="300px" Height="25px" MaxLength="50"></asp:TextBox>
                                                                    <asp:CustomValidator ID="customUserNameValidator" runat="server" ControlToValidate="UserName"
                                                                        Text="*" ValidationGroup="loGeneraciones" ErrorMessage="Ingrese usuario" CssClass="rfvalidatorCodigo"
                                                                        ClientValidationFunction="ClientValidate" Display="Dynamic" />
                                                                    <asp:RegularExpressionValidator runat="server" ID="UserNameRequired" ErrorMessage="El usuario debe corresponder a la estructura del correo electrónico que usted registro."
                                                                        ControlToValidate="UserName" SetFocusOnError="true" ValidationGroup="loGeneraciones"
                                                                        CssClass="rfvalidatorCodigo" ValidationExpression="^[0-9a-zA-Z_\-\.]+@[0-9a-zA-Z\-\.]+\.[a-zA-Z]{2,4}$"
                                                                        Display="Dynamic">*</asp:RegularExpressionValidator>
                                                                    <asp:CompareValidator runat="server" ID="CVNameUserMark" ControlToValidate="UserName" Text="*"
                                                                        SetFocusOnError="true" ErrorMessage="Ingrese usuario" ValidationGroup="loGeneraciones" CssClass="rfvalidatorCodigo"
                                                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="aa@example.com" Display="Dynamic"></asp:CompareValidator>

                                                                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="server" TargetControlID="UserNameRequired"
                                                                        WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                        PopupPosition="TopLeft" />
                                                                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="CVNameUserMark"
                                                                        WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                        PopupPosition="TopLeft" />
                                                                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="customUserNameValidator"
                                                                        WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                        PopupPosition="TopLeft" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <%-- <asp:Label ID="lblPassword" runat="server" Text="Contraseña *" />--%>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="Password" runat="server" CssClass="caja" TextMode="Password" Width="300px" MaxLength="50"
                                                                        Height="25px" onkeypress="return Validatespacebar(event);"></asp:TextBox>
                                                                    <asp:CustomValidator ID="PasswordRequired" runat="server" ClientValidationFunction="ClientValidatePassword"
                                                                        ControlToValidate="Password" CssClass="rfvalidatorCodigo" ErrorMessage="Ingrese la contraseña"
                                                                        ForeColor="Red" ValidationGroup="loGeneraciones">*</asp:CustomValidator>
                                                                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtenderPwd" runat="server" TargetControlID="PasswordRequired"
                                                                        WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                        PopupPosition="TopLeft" />

                                                                    <%--<asp:CompareValidator runat="server" ID="CVPassword" ControlToValidate="Password"
                                                                        SetFocusOnError="true" ErrorMessage="Ingrese la contraseña" ValidationGroup="loGeneraciones" Text="*"
                                                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="Contraseña" Display="Dynamic"></asp:CompareValidator>
                                                                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="PasswordRequired"
                                                                        WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                        PopupPosition="TopLeft" />--%>
                                                                    <asp:CompareValidator runat="server" ID="CvPassWater" ControlToValidate="Password" Text="*"
                                                                        SetFocusOnError="true" ErrorMessage="Ingrese la contraseña" ValidationGroup="loGeneraciones" CssClass="rfvalidatorCodigo"
                                                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="Contraseña"></asp:CompareValidator>
                                                                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="CvPassWater"
                                                                        WarningIconImageUrl="~/Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle"
                                                                        PopupPosition="TopLeft" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="rfvalidatorCodigo" colspan="2" style="color: Red;">
                                                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" colspan="2">
                                                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btnLog"
                                                                        Text="Iniciar Sesión" ValidationGroup="loGeneraciones" />
                                                                    <br />
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <br />
                                                                    <br />
                                                                    <asp:LinkButton ID="lnkRegister" runat="server" CausesValidation="false" Text="Registrarme"
                                                                        OnClick="lnkRegister_Click" />
                                                                    <br />
                                                                    <asp:LinkButton ID="PasswordRecoveryLinkButton" runat="server" CausesValidation="false"
                                                                        OnClick="PasswordRecoveryLinkButton_Click" Text="¿Olvidaste tu contraseña?" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <FailureTextStyle CssClass="error" />
                                        <LoginButtonStyle CssClass="btnLog" />
                                        <TextBoxStyle CssClass="caja" />
                                        <ValidatorTextStyle CssClass="error" />
                                    </asp:Login>
                                    <asp:LinkButton ID="GetActivationCodeLink" runat="server">Obtén tu código de activación</asp:LinkButton>
                                </td>
                            </tr>
                            <%-- <tr>
                            <td>                                
                                <div id="CaptchaDiv" style="display: none;" >                                 
                                  <sbk:CaptchaControl id="captcha" 
                                    runat="server" 
                                    ErrorMessage=" :: Codigo invalido" 
                                    CaptchaLength="5"                                    
                                    LayoutStyle="Vertical"                                          
                                    />
                                </div>
                            </td>
                        </tr>--%>
                            <tr>
                                <td>
                                    <asp:Label ID="lblError" runat="server" Font-Bold="False" ForeColor="Red" Width="100%"
                                        Font-Italic="False" Font-Overline="False" Font-Strikeout="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LblVersion" runat="server" Text=""></asp:Label><br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="5" cellspacing="0" class="IconosT">
                                        <tr>
                                            <td>
                                                <a href="http://www.dps.gov.co" target="_blank">
                                                    <img alt="DPS" src="Image/loggin/DPS.jpg" /></a>
                                            </td>
                                            <td>
                                                <a href="http://www.presidencia.gov.co" target="_blank">
                                                    <img alt="prosperidad" src="Image/loggin/Prosperidad.png" /></a>
                                            </td>
                                            <td>
                                                <a href="http://www.icbf.gov.co" target="_blank">
                                                    <img alt="icbfDigital" src="Image/loggin/IcbfDigital.png" /></a>
                                            </td>
                                            <td>
                                                <a href="http://www.icbf.gov.co" target="_blank">
                                                    <img alt="icbf" style="text-decoration: none" src="Image/loggin/LogoIcbf.png" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="FooterL">Instituto Colombiano de Bienestar Familiar
                    </td>
                    <td class="FooterR">Todos los Derechos Reservados - &copy; <%: DateTime.Now.Year %> 
                    </td>
                </tr>
            </table>

            <asp:Panel ID="codigoActivacion" runat="server" Height="200px" Width="400px" CssClass="ModalWindow" Style="display: none;">
                <div class="divModalDialog">
                    <h3>Obtén tu código de activación
                    </h3>
                    <p>
                        Ingrese su correo y contraseña
                    </p>
                    <div align="center">
                        <table>
                            <tr>
                                <td>Correo
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCorreo" runat="server" Width="148px" CssClass="input"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvtxtCorreo" runat="server" CssClass="rfvalidatorCodigo" ErrorMessage="*"
                                        ControlToValidate="txtCorreo"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Contraseña
                                </td>
                                <td>
                                    <asp:TextBox ID="txtContrasena" runat="server" Width="148px" CssClass="input" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvtxtContrasena" runat="server" CssClass="rfvalidatorCodigo" ErrorMessage="*"
                                        ControlToValidate="txtContrasena"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:Label ID="lbCodigo" runat="server"
                        Visible="False" />
                    <br />
                    <table align="center">
                        <tr>
                            <td>
                                <asp:Button ID="btnOkCodigo" runat="server" Text="Obtener" CssClass="botonLog"
                                    Width="100px" OnClick="btnOkCodigo_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" CausesValidation="false" Text="Salir"
                                    CssClass="botonLog" Width="100px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <Ajax:ModalPopupExtender ID="mdpCodActivacion" runat="server" TargetControlID="GetActivationCodeLink"
                PopupControlID="codigoActivacion" BackgroundCssClass="modalBackground">
            </Ajax:ModalPopupExtender>
        </div>
        <asp:Button runat="server" ID="btnInvisible" Style="display: none" />
        <Ajax:ModalPopupExtender ID="mpeMessage" runat="server" PopupControlID="pnlMessage"
            TargetControlID="btnInvisible" BackgroundCssClass="modalBackground">
        </Ajax:ModalPopupExtender>
        <asp:Panel ID="pnlMessage" runat="server" CssClass="modalPopup" align="center" Style="display: none">
            <div style="height: 60px">
                <asp:UpdatePanel ID="upMessage" runat="server">
                    <ContentTemplate>
                        <div id="slider_container_1">
                            <div id="SliderName">
                                <asp:LinkButton runat="server" ID="btnCerrar" Text="Cerrar" CausesValidation="false"
                                    OnClick="lbCerrar_Click" CssClass="btnCerrar" Visible="true"></asp:LinkButton>
                                <img src="Image/slider/img1.png" alt="" />
                                <img src="Image/slider/img2.png" alt="" />
                                <img src="Image/slider/img3.png" alt="" />
                                <img src="Image/slider/img4.png" alt="" />
                                <img src="Image/slider/img5.png" alt="" />
                            </div>
                            <div class="c"></div>
                            <div id="SliderNameNavigation"></div>
                            <div class="c"></div>
                            <script type="text/javascript">
                                //// Creamos un nuevo efecto y lo llamamos 'sliderDenuncias'. Usamos este nombre más tarde.
                                Sliderman.effect({ name: 'sliderDenuncias', cols: 10, rows: 5, delay: 10, fade: true, order: 'straight_stairs' });
                                var demoSlider = Sliderman.slider({
                                    container: 'SliderName', width: 1000, height: 425, effects: 'sliderDenuncias',
                                    display: {
                                        pause: false, //// el control deslizante se detiene en mouseover
                                        autoplay: 20000, //// 20 segundos x diapositiva
                                        always_show_loading: 200, //// modo de carga de prueba
                                        description: { background: '#ffffff', opacity: 0.5, height: 50, position: 'bottom' }, //// configuración de cuadro de descripción de imagen
                                        loading: { background: '#000000', opacity: 0.2, image: 'Image/slider/loading.gif' }, //// cargando ajustes de caja
                                        buttons: { opacity: 1, prev: { className: 'SliderNamePrev', label: '' }, next: { className: 'SliderNameNext', label: '' } }, //// Configuración de los botones Next/Prev
                                        navigation: { container: 'SliderNameNavigation', label: '&nbsp;' } //// configuración de navegación (páginas)
                                    }
                                });
                            </script>
                            <div class="c"></div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
    </form>
</body>
</html>

