using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.Linq.Expressions;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using System.Collections.Generic;

public partial class Page_BancoOferentes_Convocatoria_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "BancoOferentes/Convocatoria";

    SIAService vRuboService = new SIAService();

    BancoOferentesService vBancoOferentesService = new BancoOferentesService();


    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void gvConvocatorias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConvocatorias.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvConvocatorias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConvocatorias.SelectedRow);
    }

    protected void gvConvocatorias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void Buscar()
    {
        try
        {
            CargarGrilla(gvConvocatorias, GridViewSortExpression, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += Nuevo;
            gvConvocatorias.PageSize = PageSize();
            gvConvocatorias.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registro de Convocatorias", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            DateTime? vFechaPublicacionD = null;
            DateTime? vFechaPublicacionP = null;
            int? vNumeroConvocatoria = null;
            int? vIDRegional = null;
            int? vIDEstadoConvocatoria = null;

            if (txtFechaPublicacionPreliminar.Date.Year.ToString() != "1900")
                vFechaPublicacionP = Convert.ToDateTime(txtFechaPublicacionPreliminar.Date);
            
            if (txtFechaPublicacionDefinitiva.Date.Year.ToString() != "1900")
                vFechaPublicacionD = Convert.ToDateTime(txtFechaPublicacionDefinitiva.Date);

            if (txtNumeroConvocatoria.Text != "")
                vNumeroConvocatoria = Convert.ToInt32(txtNumeroConvocatoria.Text);
           
            
            if (ddlIDRegional.SelectedValue != "-1")
                vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            
            if (ddlEstadoSolicitud.SelectedValue != "-1")
                vIDEstadoConvocatoria = Convert.ToInt32(ddlEstadoSolicitud.SelectedValue);

            string usuarioArea = null;

            if (GetSessionUser().IdTipoUsuario != 1)
                usuarioArea = GetSessionUser().NombreUsuario;

            var myGridResults = vBancoOferentesService.ConsultarConvocatorias(vNumeroConvocatoria,vFechaPublicacionD,vFechaPublicacionP,vIDEstadoConvocatoria,vIDRegional);

                int nRegistros = myGridResults.Count;
                int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

                if (nRegistros < NumRegConsultaGrilla)
                {
                    if (expresionOrdenamiento != null)
                    {
                        if (string.IsNullOrEmpty(GridViewSortExpression))
                            GridViewSortDirection = SortDirection.Ascending;
                        else if (GridViewSortExpression != expresionOrdenamiento)
                            GridViewSortDirection = SortDirection.Descending;

                        if (myGridResults != null)
                        {
                            var param = Expression.Parameter(typeof(Convocatoria), expresionOrdenamiento);

                            var prop = Expression.Property(param, expresionOrdenamiento);

                            var sortExpression = Expression.Lambda<Func<Convocatoria, object>>(Expression.Convert(prop, typeof(object)), param);

                            if (GridViewSortDirection == SortDirection.Ascending)
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Descending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Ascending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }

                            GridViewSortExpression = expresionOrdenamiento;
                        }
                    }
                    else
                        gridViewsender.DataSource = myGridResults;
                    
                    gridViewsender.DataBind();
                }
                else
                    toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaRegional();
            CargarEstados();

            if (! string.IsNullOrEmpty(GetSessionParameter("BancoOferentes.CambioEstado").ToString()))
            {
                toolBar.MostrarMensajeGuardado(GetSessionParameter("BancoOferentes.CambioEstado").ToString());
                RemoveSessionParameter("BancoOferentes.CambioEstado");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {
            if (usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        //ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        //ddlIDRegional.Enabled = false;
                    }
                }
            }
            else
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
        }
    }

    private void CargarEstados()
    {
        ddlEstadoSolicitud.Items.Clear();
       Dictionary<int,string> vLEstados = vBancoOferentesService.ConsultarEstados();
        foreach(var tD in vLEstados)
            ddlEstadoSolicitud.Items.Add(new ListItem(tD.Value, tD.Key.ToString()));

        ddlEstadoSolicitud.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvConvocatorias.DataKeys[rowIndex].Value.ToString();
            string url = string.Format("Detail.aspx?Id={0}", strValue);
            NavigateTo(url);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
