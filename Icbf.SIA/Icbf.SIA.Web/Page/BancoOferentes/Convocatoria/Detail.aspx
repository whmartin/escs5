﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_BancoOferentes_Convocatoria_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">


            function ConfirmarAprobar(Tipo) {
                        return confirm('Esta seguro de que desea cambiar el estado de la convocatoría al estado: ' + Tipo);
                }

            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }

            function ReturnCallBack() {
                ocultaImagenLoading();
            }


        </script>

        <table width="90%" id="pnInfoSolicitud" runat="server" align="center" >
        <tr class="rowB">
            <td>
                N&uacute;mero
            </td>
            <td>
                Regional                                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtNumeroConvocatoria" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtRegional" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
       <tr class="rowB">
            <td>
                Nombre
            </td>
            <td>
                Estado                                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtNombre" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtEstado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
               Objeto
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                    <asp:TextBox ID="txtObjeto" TextMode="MultiLine"  Height="80px" runat="server" Enabled="false" Width="90%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Solicitante
            </td>
            <td>
               Dependenc&iacute;a Solicitante                                          
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Cargo Solicitante
            </td>
            <td>
                Identificaci&oacute;n Solicitante                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtIdSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
                 <tr class="rowB">
            <td>
              Fecha publicaci&oacute;n Preliminar
            </td>
            <td>                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaPublicacionP" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaPublicacionP" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">

                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha Observaciones Proyecto de Pliegos Inicial
            </td>
            <td>
              Fecha Observaciones Proyecto de Pliegos Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaObsPreliminarI" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsPreliminarI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaObsPreliminarF" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsPreliminarF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha Respuesta Observaciones Proyecto de Pliegos Inicial
            </td>
            <td>
              Fecha Respuesta Observaciones Proyecto de Pliegos Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaRpsPreliminarI" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaRpsPreliminarI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaRpsPreliminarF" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaRpsPreliminarF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
            </tr>
        <tr class="rowB">
            <td>
                Fecha publicaci&oacute;n Definitiva 
            </td>
            <td>                 
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaPublicacionD" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                    <asp:Image ID="imgFechaPublicacionD" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">

                </td>
            </tr>

        <tr class="rowB">
            <td>
              Fecha Observaciones Pliego Definitivo Inicial
            </td>
            <td>
              Fecha Observaciones Pliego Definitivo Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaObsDefinitivoI" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsDefinitivoI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaObsDefinitivoF" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsDefinitivoF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha Respuesta Observaciones Pliego Definitivo Inicial
            </td>
            <td>
              Fecha Respuesta Observaciones Pliego Definitivo Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaResDefinitivoI" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaResDefinitivoI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaResDefinitivoF" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaResDefinitivoF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>

                </td>
          </tr>
          <tr class="rowB">
            <td>
              Fecha de Inscrpci&oacute;n Inicial Oferentes
            </td>
            <td>
               Fecha de Inscrpci&oacute;n Final Oferentes                                
            </td>
        </tr>
             <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaInsOferenteI" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaInsOferenteI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaInsOferenteF" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaInsOferenteF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                </td>
          </tr>


        </table>

        <asp:HiddenField ID="hfIdConvocatoria" runat="server" />

</asp:Content>
