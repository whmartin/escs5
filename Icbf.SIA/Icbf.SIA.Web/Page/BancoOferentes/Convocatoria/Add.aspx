﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_BancoOferentes_Convocatoria_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">

            function GetEmpleado() {
                muestraImagenLoading();
                var IdConvocatoria = document.getElementById('<%= hfIdConvocatoria.ClientID %>').value;
                window_showModalDialog('../../../Page/BancoOferentes/Lupas/LupaEmpleado.aspx?idConvocatoria=' + IdConvocatoria, setReturnGetEmpleado, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetEmpleado(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 12) {
                        document.getElementById('<%= txtSolicitante.ClientID  %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                        document.getElementById('<%= txtDependenciaSolicitante.ClientID %>').value = retLupa[8];
                        document.getElementById('<%= txtCargoSolicitante.ClientID %>').value = retLupa[9];
                        document.getElementById('<%= txtIdSolicitante.ClientID %>').value = retLupa[1];
                        prePostbck(false);
                    }
                    else
                        ocultaImagenLoading();
                }
                else 
                    ocultaImagenLoading();
            }

            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }

            function ReturnCallBack() {
                ocultaImagenLoading();
            }


        </script>

        <table width="90%" id="pnInfoSolicitud" runat="server" align="center" >
        <tr class="rowB">
            <td>
                Número&nbsp;
              <asp:RequiredFieldValidator ID="rfvNumero" runat="server" ControlToValidate="txtNumeroConvocatoria" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td>
                Regional                                            
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtNumeroConvocatoria" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtRegional" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
       <tr class="rowB">
            <td>
                Nombre&nbsp;
               <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtNombre" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado                                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtNombre" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtEstado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
                Objeto&nbsp;
              <asp:RequiredFieldValidator ID="rfvObjeto" runat="server" ControlToValidate="txtObjeto" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                    <asp:TextBox ID="txtObjeto" TextMode="MultiLine" MaxLength="4000"  Height="80px" runat="server" Enabled="true" Width="90%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Solicitante
            </td>
            <td>
               Identificaci&oacute;n Solicitante   
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                    <asp:ImageButton ID="imgNombreSolicitante" Visible="false" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClientClick="GetEmpleado(); return false;" Style="cursor: hand" ToolTip="Buscar"
                                                 />
                </td>
                <td class="Cell">
                   <asp:TextBox ID="txtIdSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Cargo Solicitante
            </td>
            <td>
                Dependenc&iacute;a Solicitante                                          
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                   <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha publicaci&oacute;n Preliminar
            </td>
            <td>                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaPublicacionP" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaPublicacionP" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaPublicacionP" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaPublicacionP" TargetControlID="txtFechaPublicacionP"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaPublicacionP"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaPublicacionP">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">

                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha Observaciones Proyecto de Pliegos Inicial
            </td>
            <td>
              Fecha Observaciones Proyecto de Pliegos Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaObsPreliminarI" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsPreliminarI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="CetFechaObsPreliminarI" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaObsPreliminarI" TargetControlID="txtFechaObsPreliminarI"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="MeeFechaObsPreliminarI"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaObsPreliminarI">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaObsPreliminarF" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsPreliminarF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="CetFechaObsPreliminarF" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaObsPreliminarF" TargetControlID="txtFechaObsPreliminarF"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="MeeFechaObsPreliminarF"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaObsPreliminarF">
                    </Ajax:MaskedEditExtender>
                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha Respuesta Observaciones Proyecto de Pliegos Inicial
            </td>
            <td>
              Fecha Respuesta Observaciones Proyecto de Pliegos Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaRpsPreliminarI" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaRpsPreliminarI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="CetFechaRpsPreliminarI" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaRpsPreliminarI" TargetControlID="txtFechaRpsPreliminarI"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="MeeFechaRpsPreliminarI"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaRpsPreliminarI">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaRpsPreliminarF" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaRpsPreliminarF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="CetFechaRpsPreliminarF" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaRpsPreliminarF" TargetControlID="txtFechaRpsPreliminarF"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="MeeFechaRpsPreliminarF"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaRpsPreliminarF">
                    </Ajax:MaskedEditExtender>
                </td>
            </tr>
        <tr class="rowB">
            <td>
                Fecha publicaci&oacute;n Definitiva 
            </td>
            <td>                 
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaPublicacionD" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                    <asp:Image ID="imgFechaPublicacionD" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaPublicacionD" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaPublicacionD" TargetControlID="txtFechaPublicacionD"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaPublicacionD" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaPublicacionD">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">

                </td>
            </tr>

        <tr class="rowB">
            <td>
              Fecha Observaciones Pliego Definitivo Inicial
            </td>
            <td>
              Fecha Observaciones Pliego Definitivo Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaObsDefinitivoI" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsDefinitivoI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaObsDefinitivoI" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaObsDefinitivoI" TargetControlID="txtFechaObsDefinitivoI"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaObsDefinitivoI"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaObsDefinitivoI">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaObsDefinitivoF" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaObsDefinitivoF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaObsDefinitivoF" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaObsDefinitivoF" TargetControlID="txtFechaObsDefinitivoF"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaObsDefinitivoF"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaObsDefinitivoF">
                    </Ajax:MaskedEditExtender>
                </td>
            </tr>
        <tr class="rowB">
            <td>
              Fecha Respuesta Observaciones Pliego Definitivo Inicial
            </td>
            <td>
              Fecha Respuesta Observaciones Pliego Definitivo Final                                    
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaResDefinitivoI" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaResDefinitivoI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaResDefinitivoI" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaResDefinitivoI" TargetControlID="txtFechaResDefinitivoI"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaResDefinitivoI"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaResDefinitivoI">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaResDefinitivoF" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaResDefinitivoF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaResDefinitivoF" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaResDefinitivoF" TargetControlID="txtFechaResDefinitivoF"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaResDefinitivoF"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaResDefinitivoF">
                    </Ajax:MaskedEditExtender>
                </td>
          </tr>
          <tr class="rowB">
            <td>
              Fecha de Inscrpci&oacute;n Inicial Oferentes
            </td>
            <td>
               Fecha de Inscrpci&oacute;n Final Oferentes                                
            </td>
        </tr>
             <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtFechaInsOferenteI" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaInsOferenteI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaInsOferenteI" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaInsOferenteI" TargetControlID="txtFechaInsOferenteI"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaInsOferenteI"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInsOferenteI">
                    </Ajax:MaskedEditExtender>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtFechaInsOferenteF" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaInsOferenteF" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaInsOferenteF" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaInsOferenteF" TargetControlID="txtFechaInsOferenteF"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaInsOferenteF"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInsOferenteF">
                    </Ajax:MaskedEditExtender>
                </td>
          </tr>
        </table>

        <asp:HiddenField ID="hfIdConvocatoria" runat="server" />
        <asp:HiddenField ID="hfCodigoEstado" runat="server" />

</asp:Content>
