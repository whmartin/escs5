﻿using System;
using System.Web.UI;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_BancoOferentes_Convocatoria_Detail : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "BancoOferentes/Convocatoria";

    string PermisoActualizarFechas = "BancoOferentes/ActualizarFechas";

    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(hfIdConvocatoria.Value))
            NavigateTo("Detail.aspx?Id=" + hfIdConvocatoria.Value);
        else
        NavigateTo(SolutionPage.List);
    }

    private void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            if(ValidarInformacion())
            {
                int result = 0;
                Convocatoria item = new Convocatoria();
                item.Objeto = txtObjeto.Text.ToUpper();
                item.Nombre = txtObjeto.Text.ToUpper();
                item.Numero = int.Parse(txtNumeroConvocatoria.Text);

                item.FechaPublicacionD = DateTime.Parse(txtFechaPublicacionD.Text);

                // Fechas Preliminar
                item.FechaPublicacionP = DateTime.Parse(txtFechaPublicacionP.Text);
                item.FechaObservacionesInicioP = DateTime.Parse(txtFechaObsPreliminarI.Text);
                item.FechaObservacionesFinP = DateTime.Parse(txtFechaObsPreliminarF.Text);
                item.FechaRespuestasInicioP = DateTime.Parse(txtFechaRpsPreliminarI.Text);
                item.FechaRespuestasFinP = DateTime.Parse(txtFechaRpsPreliminarF.Text);

                // Fechas Definitiva.         
                item.FechaPublicacionD = DateTime.Parse(txtFechaPublicacionD.Text);
                item.FechaObservacionesInicioD = DateTime.Parse(txtFechaObsDefinitivoI.Text);
                item.FechaObservacionesFinD = DateTime.Parse(txtFechaObsDefinitivoF.Text);
                item.FechaRespuestasInicioD = DateTime.Parse(txtFechaResDefinitivoI.Text);
                item.FechaRespuestasFinD = DateTime.Parse(txtFechaResDefinitivoF.Text);

                item.FechaInicioInscripcionOferente = DateTime.Parse(txtFechaInsOferenteI.Text);
                item.FechaFinInscripcionOferente = DateTime.Parse(txtFechaInsOferenteF.Text);

                item.UsuarioCrea = GetSessionUser().NombreUsuario;

                if(!string.IsNullOrEmpty(hfIdConvocatoria.Value))
                {
                    item.IdConvocatoria = int.Parse(hfIdConvocatoria.Value);
                    result = vBancoOferentesService.ActualizarConvocatoria(item);
                }
                else
                   result = vBancoOferentesService.CrearConvocatoria(item);

                if(result > 0)
                {
                    string url = string.Format("Detail.aspx?Id={0}&Actualizo=1", item.IdConvocatoria);
                    NavigateTo(url);
                }
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool ValidarInformacion()
    {
        StringBuilder validador = new StringBuilder();

        // Fecha Publicación
        DateTime  fechaPrelimianr = DateTime.MinValue;
        if(txtFechaPublicacionP.Text != String.Empty)
        {
            int anioanterior = DateTime.Now.Year - 1;
            if(!DateTime.TryParse(txtFechaPublicacionP.Text, out fechaPrelimianr))
                validador.AppendLine("La fecha de publicaci&oacute;n preliminar invalida.");
            else if(fechaPrelimianr < new DateTime(anioanterior, 1, 1) || fechaPrelimianr.Date > DateTime.Now.Date)
                validador.AppendLine("La fecha de publicaci&oacute;n preliminar debe ser mayor o igual al 1 de enero del año" + anioanterior + " y menor a la fecha act&uacute;al");
        }
        else
            validador.AppendLine("Debe Ingresar el valor de la fecha preliminar.");

        // Fecha Inicio Observaciones 
        DateTime fechaPrelimianrObservacionesI = DateTime.MinValue;
        if(txtFechaObsPreliminarI.Text != String.Empty && fechaPrelimianr != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaObsPreliminarI.Text, out fechaPrelimianrObservacionesI))
                validador.AppendLine("La fecha de inicio de las observaciones preliminares es invalida.");
            else if(fechaPrelimianrObservacionesI < fechaPrelimianr)
                validador.AppendLine("La fecha de inicio de las observaciones preliminares debe ser mayor o igual a la fecha publicaci&oacute;n preliminar");
        }
        else
            validador.AppendLine("El valor de la fecha inicial de observaciones preliminar es invalido.");

        // Fecha Fin Observaciones 
        DateTime fechaPrelimianrObservacionesF = DateTime.MinValue;
        if(txtFechaObsPreliminarF.Text != String.Empty && fechaPrelimianrObservacionesI != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaObsPreliminarF.Text, out fechaPrelimianrObservacionesF))
                validador.AppendLine("La fecha de fin de las observaciones preliminares es invalida.");
            else if(fechaPrelimianrObservacionesF < fechaPrelimianrObservacionesI)
                validador.AppendLine("La fecha de fin de las observaciones preliminares debe ser mayor o igual a la fecha inicio de las observaciones preliminares");
        }
        else
            validador.AppendLine("El valor de la fecha final de observaciones preliminar es invalido.");

        // Fecha Inicio Respuestas Preliminar 
        DateTime fechaPreliminarRespuestasI = DateTime.MinValue;
        if(txtFechaRpsPreliminarI.Text != String.Empty && fechaPrelimianrObservacionesF != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaRpsPreliminarI.Text, out fechaPreliminarRespuestasI))
                validador.AppendLine("La fecha de inicio de las respuestas a observaciones preliminares es invalida.");
            else if(fechaPreliminarRespuestasI < fechaPrelimianrObservacionesF)
                validador.AppendLine("La fecha de inicio de las respuestas a observaciones preliminar debe ser mayor o igual a la fecha de fin de las observaciones preliminares.");
        }
        else
            validador.AppendLine("El valor de la fecha inicial de respuestas a observaciones preliminar es invalido.");

        // Fecha Fin Respuestas Preliminar 
        DateTime fechaPreliminarRespuestasF = DateTime.MinValue;
        if(txtFechaRpsPreliminarF.Text != String.Empty && fechaPreliminarRespuestasI != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaRpsPreliminarF.Text, out fechaPreliminarRespuestasF))
                validador.AppendLine("La fecha de fin de respuestas a observaciones preliminar es invalida.");
            else if(fechaPreliminarRespuestasF < fechaPreliminarRespuestasI)
                validador.AppendLine("La fecha de fin de las respuestas a observaciones preliminares debe ser mayor o igual a la fecha de inicio de las respuestas a observaciones preliminares.");
        }
        else
            validador.AppendLine("El valor de la fecha inicial de respuestas a observaciones preliminar es invalido.");


        // Fecha Publicación Definitiva 
        DateTime fechaPublicacionD = DateTime.MinValue;
        if(txtFechaPublicacionD.Text != String.Empty && fechaPreliminarRespuestasF != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaPublicacionD.Text, out fechaPublicacionD))
                validador.AppendLine("La fecha de publicaci&oacute;n definitiva es invalida.");
            else if(fechaPublicacionD < fechaPreliminarRespuestasF)
                validador.AppendLine("La fecha de publicaci&oacute;n definitiva debe ser mayor o igual a la fecha de fin de las respuestas a las observaciones preliminares.");
        }
        else
            validador.AppendLine("El valor de la fecha de publicaci&oacute;n definitiva es invalido.");


        // Fecha Inicial Observaciones Definitiva 
        DateTime fechaIniciObservacionesD = DateTime.MinValue;
        if(txtFechaObsDefinitivoI.Text != String.Empty && fechaPublicacionD != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaObsDefinitivoI.Text, out fechaIniciObservacionesD))
                validador.AppendLine("La fecha de inicio de las observaciones definitivas es invalida.");
            else if(fechaIniciObservacionesD < fechaPublicacionD)
                validador.AppendLine("La fecha de inicio a las observaciones definitivas debe ser mayor o igual a la fecha publicaci&oacute;n definitiva.");
        }
        else
            validador.AppendLine("El valor de la fecha inicial de las observaciones definitivas es invalido.");

        // Fecha Final Observaciones Definitiva 
        DateTime fechaFinObservacionesD = DateTime.MinValue;
        if(txtFechaObsDefinitivoF.Text != String.Empty && fechaIniciObservacionesD != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaObsDefinitivoF.Text, out fechaFinObservacionesD))
                validador.AppendLine("La fecha de fin de las observaciones definitivas es invalida.");
            else if(fechaFinObservacionesD < fechaIniciObservacionesD)
                validador.AppendLine("La fecha de fin a las observaciones definitivas debe ser mayor o igual a la fecha de inicio de las observaciones definitivas.");
        }
        else
            validador.AppendLine("El valor de la fecha inicial de las observaciones definitivas es invalido.");

        // Fecha Inicio Respuestas a las Observaciones Definitivas 
        DateTime fechaInicioRespuestasD = DateTime.MinValue;
        if(txtFechaResDefinitivoI.Text != String.Empty && fechaFinObservacionesD != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaResDefinitivoI.Text, out fechaInicioRespuestasD))
                validador.AppendLine("La fecha de inicio de las respuestas a las observaciones definitivas es invalida.");
            else if(fechaInicioRespuestasD < fechaFinObservacionesD)
                validador.AppendLine("La fecha de inicio de las respustas a las observaciones definitivas debe ser mayor o igual a la fecha de fin de las observaciones definitivas.");
        }
        else
            validador.AppendLine("El valor de la fecha inicial de las respuestas a las observaciones definitivas es invalido.");

        // Fecha Final de las Respuestas a las Observaciones Definitivas 
        DateTime fechaFinRespuestasD = DateTime.MinValue;
        if(txtFechaResDefinitivoF.Text != String.Empty && fechaInicioRespuestasD != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaResDefinitivoF.Text, out fechaFinRespuestasD))
                validador.AppendLine("La fecha de fin de las respuestas a las observaciones definitivas es invalida.");
            else if(fechaFinRespuestasD < fechaInicioRespuestasD)
                validador.AppendLine("La fecha de fin de las respustas a las observaciones definitivas debe ser mayor o igual a la fecha de inicio de las respuestas a las observaciones definitivas.");
        }
        else
            validador.AppendLine("El valor de la fecha final de las respuestas a las observaciones definitivas es invalido.");


        // Fecha Inicio Inscripción Oferentes. 
        DateTime FechaInicioInscripcion = DateTime.MinValue;
        if(txtFechaInsOferenteI.Text != String.Empty && fechaFinRespuestasD != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaInsOferenteI.Text, out FechaInicioInscripcion))
                validador.AppendLine("La fecha de inicio para la inscripci&oacute;n de oferentes es invalida.");
            else if(FechaInicioInscripcion < fechaFinRespuestasD)
                validador.AppendLine("La fecha de inicio para la inscripci&oacute;n de oferentes debe ser mayor o igual a la fecha de fin de las respuestas a las observaciones definitivas.");
        }
        else
            validador.AppendLine("El valor de la fecha de inicio para la inscripci&oacute;n es invalido.");


        // Fecha Inicio Inscripción Oferentes. 
        DateTime FechaFinInscripcion = DateTime.MinValue;
        if(txtFechaInsOferenteF.Text != String.Empty && FechaInicioInscripcion != DateTime.MinValue)
        {
            if(!DateTime.TryParse(txtFechaInsOferenteF.Text, out FechaFinInscripcion))
                validador.AppendLine("La fecha de fin para la inscripci&oacute;n de oferentes es invalida.");
            else if(FechaFinInscripcion < FechaInicioInscripcion)
                validador.AppendLine("La fecha de fin para la inscripci&oacute;n de oferentes debe ser mayor o igual a la fecha de inicio para la inscripci&oacute;n de oferentes ");
        }
        else
            validador.AppendLine("El valor de la fecha de fin para la inscripci&oacute;n es invalido.");


        if(validador.Length > 0)
            toolBar.MostrarMensajeError(validador.ToString().Replace("\n", "<br/>"));

        return validador.Length > 0 ? false: true;
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Registro de Convocatoria P&uacute;blica", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ValidarPermisos(string codigoEstado)
    {
        if(ValidateAccessEdit(toolBar, PermisoActualizarFechas, SolutionPage.Edit))
            GestionarFechas(codigoEstado);
        else if(codigoEstado == "RE")
            HabilitarCalendario(true);
        else
            HabilitarCalendario(false);
    }

    private void HabilitarCalendario(bool enable)
    {
        // Publicación Preliminar
        cetFechaPublicacionP.Enabled = enable;
        meeFechaPublicacionP.Enabled = enable;
        txtFechaPublicacionP.Enabled = enable;

        txtFechaObsPreliminarI.Enabled = enable;
        CetFechaObsPreliminarI.Enabled = enable;
        MeeFechaObsPreliminarI.Enabled = enable;

        txtFechaObsPreliminarF.Enabled = enable;
        CetFechaObsPreliminarF.Enabled = enable;
        MeeFechaObsPreliminarF.Enabled = enable;

        txtFechaRpsPreliminarI.Enabled = enable;
        CetFechaRpsPreliminarI.Enabled = enable;
        MeeFechaRpsPreliminarI.Enabled = enable;

        txtFechaRpsPreliminarF.Enabled = enable;
        CetFechaRpsPreliminarF.Enabled = enable;
        MeeFechaRpsPreliminarF.Enabled = enable;

        // Publicación Definitiva
        cetFechaPublicacionD.Enabled = enable;
        meeFechaPublicacionD.Enabled = enable;
        txtFechaPublicacionD.Enabled = enable;

        txtFechaObsDefinitivoI.Enabled = enable;
        cetFechaObsDefinitivoI.Enabled = enable;
        meeFechaObsDefinitivoI.Enabled = enable;

        txtFechaObsDefinitivoF.Enabled = enable;
        cetFechaObsDefinitivoF.Enabled = enable;
        meeFechaObsDefinitivoF.Enabled = enable;

        txtFechaResDefinitivoI.Enabled = enable;
        cetFechaResDefinitivoI.Enabled = enable;
        meeFechaResDefinitivoI.Enabled = enable;

        txtFechaResDefinitivoF.Enabled = enable;
        cetFechaResDefinitivoF.Enabled = enable;
        meeFechaResDefinitivoF.Enabled = enable;

        // Inscripción Ofeerentes
        txtFechaInsOferenteI.Enabled = enable;
        cetFechaInsOferenteI.Enabled = enable;
        meeFechaInsOferenteI.Enabled = enable;

        txtFechaInsOferenteF.Enabled = enable;
        cetFechaInsOferenteF.Enabled = enable;
        meeFechaInsOferenteF.Enabled = enable;
    }

    private void GestionarFechas(string codigoEstado)
    {
        if(codigoEstado == "RE")
            HabilitarCalendario(true);
        else 
        {
            if(codigoEstado == "PRE" || codigoEstado == "PREOBS" || codigoEstado == "DEF" || codigoEstado == "DEFOBS" || codigoEstado == "INSOFE")
            {
                cetFechaPublicacionP.Enabled = false;
                meeFechaPublicacionP.Enabled = false;
                txtFechaPublicacionP.Enabled = false;
            }

            if(codigoEstado == "PREOBS" || codigoEstado == "DEF" || codigoEstado == "DEFOBS" || codigoEstado == "INSOFE")
            {
                txtFechaObsPreliminarI.Enabled = false;
                CetFechaObsPreliminarI.Enabled = false;
                MeeFechaObsPreliminarI.Enabled = false;
                txtFechaObsPreliminarF.Enabled = false;
                CetFechaObsPreliminarF.Enabled = false;
                MeeFechaObsPreliminarF.Enabled = false;
                txtFechaRpsPreliminarI.Enabled = false;
                CetFechaRpsPreliminarI.Enabled = false;
                MeeFechaRpsPreliminarI.Enabled = false;
            }

            if(codigoEstado == "DEF" || codigoEstado == "DEFOBS" || codigoEstado == "INSOFE")
            {
                cetFechaPublicacionD.Enabled = false;
                meeFechaPublicacionD.Enabled = false;
                txtFechaPublicacionD.Enabled = false;
                txtFechaRpsPreliminarF.Enabled = false;
                CetFechaRpsPreliminarF.Enabled = false;
                MeeFechaRpsPreliminarF.Enabled = false;
            }

            if(codigoEstado == "DEFOBS" || codigoEstado == "INSOFE")
            {
                txtFechaObsDefinitivoI.Enabled = false;
                cetFechaObsDefinitivoI.Enabled = false;
                meeFechaObsDefinitivoI.Enabled = false;
                txtFechaObsDefinitivoF.Enabled = false;
                cetFechaObsDefinitivoF.Enabled = false;
                meeFechaObsDefinitivoF.Enabled = false;
                txtFechaResDefinitivoI.Enabled = false;
                cetFechaResDefinitivoI.Enabled = false;
                meeFechaResDefinitivoI.Enabled = false;
            }

             if(codigoEstado == "INSOFE")
            {
                txtFechaResDefinitivoF.Enabled = false;
                cetFechaResDefinitivoF.Enabled = false;
                meeFechaResDefinitivoF.Enabled = false;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            Convocatoria itemDetalle = null;

            if(VerificarQueryStrings())
            {
                int id = int.Parse(hfIdConvocatoria.Value);
                itemDetalle = vBancoOferentesService.ConsultarConvocatoriaPorId(id);
                txtNombre.Text = itemDetalle.Nombre;
                txtEstado.Text = itemDetalle.Estado.ToUpper();
                txtNumeroConvocatoria.Text = itemDetalle.Numero.ToString();
                txtRegional.Text = itemDetalle.DescripcionRegional.ToUpper();
                txtObjeto.Text = itemDetalle.Objeto;
                txtSolicitante.Text = itemDetalle.EmpleadoSolicitante;
                txtCargoSolicitante.Text = itemDetalle.CargoSolicitante;
                txtDependenciaSolicitante.Text = itemDetalle.Dependencia;
                txtIdSolicitante.Text = itemDetalle.IdEmpleadoSolicitante == 0 ? string.Empty : itemDetalle.IdEmpleadoSolicitante.ToString();
                hfCodigoEstado.Value = itemDetalle.CodigoEstado;
                imgNombreSolicitante.Visible = true;

                // Fechas Preliminar
                txtFechaPublicacionP.Text = itemDetalle.FechaPublicacionP.Value.ToShortDateString();
                txtFechaObsPreliminarI.Text = itemDetalle.FechaObservacionesInicioP.Value.ToShortDateString();
                txtFechaObsPreliminarF.Text = itemDetalle.FechaObservacionesFinP.Value.ToShortDateString();
                txtFechaRpsPreliminarI.Text = itemDetalle.FechaRespuestasInicioP.Value.ToShortDateString();
                txtFechaRpsPreliminarF.Text = itemDetalle.FechaRespuestasFinP.Value.ToShortDateString();

                // Fechas Definitiva.
                txtFechaPublicacionD.Text = itemDetalle.FechaPublicacionD.Value.ToShortDateString();
                txtFechaObsDefinitivoI.Text = itemDetalle.FechaObservacionesInicioD.Value.ToShortDateString();
                txtFechaObsDefinitivoF.Text = itemDetalle.FechaObservacionesFinD.Value.ToShortDateString();
                txtFechaResDefinitivoI.Text = itemDetalle.FechaRespuestasInicioD.Value.ToShortDateString();
                txtFechaResDefinitivoF.Text = itemDetalle.FechaRespuestasFinD.Value.ToShortDateString();

                // Fechas Inscripción
                txtFechaInsOferenteI.Text = itemDetalle.FechaInicioInscripcionOferente.Value.ToShortDateString();
                txtFechaInsOferenteF.Text = itemDetalle.FechaFinInscripcionOferente.Value.ToShortDateString();

                ValidarPermisos(itemDetalle.CodigoEstado);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
            hfIdConvocatoria.Value = Request.QueryString["Id"];
            isvalid = true;
        }

        return isvalid;
    }
}

