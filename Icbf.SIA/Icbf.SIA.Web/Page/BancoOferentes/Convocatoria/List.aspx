<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_BancoOferentes_Convocatoria_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
 <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
                <tr class="rowB">
            <td class="Cell">
                Fecha Publicaci&oacute;n Definitiva
            </td>
            <td class="Cell">
                Fecha Publicaci&oacute;n Preliminar
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaPublicacionDefinitiva" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFechaPublicacionPreliminar" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Convotaroria</td>
            <td class="Cell">
                Estado de la Convocatoria</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroConvocatoria" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNoConvocatoria" runat="server" TargetControlID="txtNumeroConvocatoria" FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                                <asp:DropDownList ID="ddlEstadoSolicitud" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Convocatoria</td>
            <td class="Cell">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                &nbsp;</td>
        </tr> 

    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConvocatorias" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdConvocatoria" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvConvocatorias_PageIndexChanging" OnSorting="gvConvocatorias_Sorting" OnSelectedIndexChanged="gvConvocatorias_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                                <asp:BoundField HeaderText="N&uacute;mero" DataField="Numero"  SortExpression="Numero"/>
                                <asp:BoundField HeaderText="Nombre" DataField="Nombre"  SortExpression="Nombre"/>
                                <asp:BoundField HeaderText="Regional" DataField="DescripcionRegional"  SortExpression="DescripcionRegional"/>
                                <asp:BoundField HeaderText="Estado" DataField="Estado"  SortExpression="Estado"/>
                                <asp:BoundField HeaderText="Fecha Publicaci&oacute;n Preliminar" DataField="FechaPublicacionP"  SortExpression="FechaPublicacionP" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Fecha Publicaci&oacute;n Definitiva" DataField="FechaPublicacionD"  SortExpression="FechaPublicacionD" DataFormatString="{0:dd/MM/yyyy}"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
