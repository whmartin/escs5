﻿using System;
using System.Web.UI;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_BancoOferentes_Convocatoria_Detail : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "BancoOferentes/Convocatoria";

    string PermiosoAprobar = "BancoOferentes/ConvocatoriaCambioE";

    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
            if(ValidarCambioEstado())
            {
                int idConvocatoria = int.Parse(hfIdConvocatoria.Value);
                var result = vBancoOferentesService.CambiarEstadoConvocatoria(idConvocatoria);

                if(result > 0)
                    NavigateTo("Detail.aspx?Id=" + hfIdConvocatoria.Value);
                else
                    toolBar.MostrarMensajeError("Ocurrio un error al intentar guardar la información. Intenete nuevamente");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool ValidarCambioEstado()
    {
        bool isValid = true;

        if(string.IsNullOrEmpty(txtSolicitante.Text) || string.IsNullOrEmpty(txtRegional.Text))
        {
            toolBar.MostrarMensajeError("Debe seleccionar la infromación del solicitante para cambiar de estado.");
            isValid = false;
        }

        return isValid;
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo("Add.aspx?Id=" + hfIdConvocatoria.Value);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.MostrarBotonAprobar(false);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Registro de Convocatoria P&uacute;blica", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ValidarPermisos(Convocatoria item)
    {
        if(ValidateAccessEdit(toolBar, PermiosoAprobar, SolutionPage.Edit))
        {
            string nombre = string.Empty;
            bool isValid = true;

            if(item.CodigoEstado == "RE")
                nombre = "Preliminar";
            else if(item.CodigoEstado == "PRE")
                nombre = "Respuesta Observaciones Preliminar";
            else if(item.CodigoEstado == "PREOBS")
                nombre = "Definitiva";
            else if(item.CodigoEstado == "DEF")
                nombre = "Respuesta Observaciones Definitiva";
            else if(item.CodigoEstado == "DEFOBS")
                nombre = "Inscripción de Oferentes";
            else
                isValid = false;

            if(isValid)
            {
                toolBar.MostrarBotonAprobar(true);
                toolBar.SetAprobarConfirmation("return ConfirmarAprobar('" + nombre + "');");
            }          
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if(VerificarQueryStrings())
            {
                int id = int.Parse(hfIdConvocatoria.Value);
                Convocatoria itemDetalle = vBancoOferentesService.ConsultarConvocatoriaPorId(id);
                txtNombre.Text = itemDetalle.Nombre;
                txtEstado.Text = itemDetalle.Estado.ToUpper();
                txtNumeroConvocatoria.Text = itemDetalle.Numero.ToString();
                txtRegional.Text = itemDetalle.DescripcionRegional.ToUpper();
                txtObjeto.Text = itemDetalle.Objeto;
                txtSolicitante.Text = itemDetalle.EmpleadoSolicitante;
                txtCargoSolicitante.Text = itemDetalle.CargoSolicitante;
                txtDependenciaSolicitante.Text = itemDetalle.Dependencia;
                txtIdSolicitante.Text = itemDetalle.IdEmpleadoSolicitante == 0 ? string.Empty : itemDetalle.IdEmpleadoSolicitante.ToString();
                // Fechas Preliminar
                txtFechaPublicacionP.Text = itemDetalle.FechaPublicacionP.Value.ToShortDateString();
                txtFechaObsPreliminarI.Text = itemDetalle.FechaObservacionesInicioP.Value.ToShortDateString();
                txtFechaObsPreliminarF.Text = itemDetalle.FechaObservacionesFinP.Value.ToShortDateString();
                txtFechaRpsPreliminarI.Text = itemDetalle.FechaRespuestasInicioP.Value.ToShortDateString();
                txtFechaRpsPreliminarF.Text = itemDetalle.FechaRespuestasFinP.Value.ToShortDateString();

                // Fechas Definitiva.
                txtFechaPublicacionD.Text = itemDetalle.FechaPublicacionD.Value.ToShortDateString();
                txtFechaObsDefinitivoI.Text = itemDetalle.FechaObservacionesInicioD.Value.ToShortDateString();
                txtFechaObsDefinitivoF.Text = itemDetalle.FechaObservacionesFinD.Value.ToShortDateString();
                txtFechaResDefinitivoI.Text = itemDetalle.FechaRespuestasInicioD.Value.ToShortDateString();
                txtFechaResDefinitivoF.Text = itemDetalle.FechaRespuestasFinD.Value.ToShortDateString();

                // Fechas Inscripción
                txtFechaInsOferenteI.Text = itemDetalle.FechaInicioInscripcionOferente.Value.ToShortDateString();
                txtFechaInsOferenteF.Text = itemDetalle.FechaFinInscripcionOferente.Value.ToShortDateString();
                ValidarPermisos(itemDetalle);
            }
            else
                NavigateTo(SolutionPage.List);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
            hfIdConvocatoria.Value = Request.QueryString["Id"];
            isvalid = true;
        }

        if(!string.IsNullOrEmpty(Request.QueryString["Actualizo"]))
            toolBar.MostrarMensajeGuardado("Se guardo la informaci&oacute;n de la convocatoria correctamente.");

        return isvalid;
    }
}

