﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaCoordinador.aspx.cs" Inherits="Page_BancoOferentes_LupaCoordinador_LupaCoordinador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="Panel1">
        <asp:Panel runat="server" ID="pnlConsulta">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        N&uacute;mero de identificaci&oacute;n
                    </td>
                    <td>
                        Nombre
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroIdentificacion"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNombre"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvCoordinadores" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" DataKeyNames="IdUsuario,NumeroDocumento" CellPadding="0" Height="16px"
                            OnSorting="gvCoordinadores_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvCoordinadores_PageIndexChanging" OnSelectedIndexChanged="gvCoordinadores_SelectedIndexChanged">
                            <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                             </asp:TemplateField>
                                <asp:BoundField HeaderText="N&uacute;mero documento" DataField="NumeroDocumento" SortExpression="NumeroDocumento" />
                                <asp:BoundField HeaderText="Nombre" DataField="NombreCompleto" SortExpression="NombreCompleto" />
                                <asp:BoundField HeaderText="&aacute;rea" DataField="NombreArea" SortExpression="NombreArea" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

