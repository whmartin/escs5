﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2Publica.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_BancoOferentes_ConvocatoriaPublica_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">


            function ConfirmarAprobar(Tipo) {
                        return confirm('Esta seguro de que desea cambiar el estado de la convocatoría al estado: ' + Tipo);
                }

            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }

            function ReturnCallBack() {
                ocultaImagenLoading();
            }

            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            function ClientValidateCorreo(source, arguments) {
                if (arguments.Value == '123456789') {
                    arguments.IsValid = false;
                }
                else if (!validateEmail(arguments.Value)) {
                    arguments.IsValid = false;
                }
                else {
                    arguments.IsValid = true;

                }
            }



            function ValidaEliminacion(tipo) {

                if (tipo == 'Observacion')
                    return confirm('Esta seguro de que desea eliminar la observaci&oacute;n?');
            }

        </script>

        <table width="90%" id="pnInfoSolicitud" runat="server" align="center" >
        <tr class="rowB">
            <td>
                N&uacute;mero
            </td>
            <td>
                Regional                                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtNumeroConvocatoria" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtRegional" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
       <tr class="rowB">
            <td>
                Nombre
            </td>
            <td>
                Estado                                                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtNombre" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtEstado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
       <tr class="rowB">
            <td>
                Nombres 
               <asp:RequiredFieldValidator ID="rfvNombreObservador" runat="server" ControlToValidate="txtNombreObservador" Display="Dynamic" Enabled="true" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td>
                Correo Electronico 
               <asp:RequiredFieldValidator ID="rfvCorreoE" runat="server" ControlToValidate="txtCorreoObservador" Display="Dynamic" Enabled="true" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="customUserNameValidator" runat="server" ControlToValidate="txtCorreoObservador"  ForeColor="Red" 
                 ValidationGroup="btnGuardar" ErrorMessage="Ingrese un correo valido" 
                ClientValidationFunction="ClientValidateCorreo" Display="Dynamic" />
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                  <asp:TextBox ID="txtNombreObservador" runat="server" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                 <asp:TextBox ID="txtCorreoObservador" runat="server" Width="80%"></asp:TextBox>

               </td>
        </tr>
        <tr class="rowB">
            <td>
             Tipo
                <asp:CompareValidator runat="server" ID="cvTipoObservacion" ControlToValidate="ddlTipoObservación"
                SetFocusOnError="true" ErrorMessage="*" ValidationGroup="AdicionarObservacion"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
               <asp:DropDownList ID="ddlTipoObservación" runat="server"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:ImageButton ID="ImgAgregarObservacion" runat="server" CommandName="Select" ImageUrl="~/Image/btn/add.gif"
                                        Height="16px" Width="16px" ToolTip="Agregar"  ValidationGroup="AdicionarObservacion" OnClick="ImgAgregarObservacion_Click" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Observación  
               <asp:RequiredFieldValidator ID="rfvObservacion" runat="server" ControlToValidate="txtObservacion" Display="Dynamic" Enabled="true" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="AdicionarObservacion"></asp:RequiredFieldValidator>
             </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                    <asp:TextBox ID="txtObservacion"  TextMode="MultiLine" Height="80px" runat="server" Width="100%"></asp:TextBox>
                </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                        <asp:GridView runat="server" ID="gvObservaciones" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Id" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvObservaciones_PageIndexChanging" >
                        <Columns>  
                                <asp:BoundField HeaderText="TipoObservacion" DataField="TipoObservacion"/>
                                <asp:BoundField HeaderText="Observaci&oacute;n" DataField="Observacion"/>
                                <asp:TemplateField>
                                <ItemTemplate>
                                        <asp:LinkButton ID="btnEliminarObservacion" runat="server" OnClick="btnEliminarObservacion_Click"
                                        OnClientClick="return ValidaEliminacion('Observacion');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                        <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                        </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>

        </table>

        <asp:HiddenField ID="hfIdConvocatoria" runat="server" />

        </table>

        </table>

</asp:Content>
