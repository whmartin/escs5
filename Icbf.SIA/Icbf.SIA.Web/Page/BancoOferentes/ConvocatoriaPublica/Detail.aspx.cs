﻿using System;
using System.Web.UI;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_BancoOferentes_ConvocatoriaPublica_Detail : GeneralWeb
{
    masterPrincipalPublica toolBar;

    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
            if (!Page.IsPostBack)
                CargarDatos();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo("Add.aspx?Id=" + hfIdConvocatoria.Value);
    }
    
    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipalPublica)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegatePublic(btnRetornar_Click);
            toolBar.eventoNuevo += new ToolBarDelegatePublic(btnNuevo_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Detalle de Convocatoria P&uacute;blica", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if(VerificarQueryStrings())
            {
                int id = int.Parse(hfIdConvocatoria.Value);
                Convocatoria itemDetalle = vBancoOferentesService.ConsultarConvocatoriaPorId(id);
                txtNombre.Text = itemDetalle.Nombre;
                txtEstado.Text = itemDetalle.Estado;
                txtNumeroConvocatoria.Text = itemDetalle.Numero.ToString();
                txtRegional.Text = itemDetalle.DescripcionRegional;
                txtObjeto.Text = itemDetalle.Objeto;
                // Fechas Preliminar
                txtFechaPublicacionP.Text = itemDetalle.FechaPublicacionP.Value.ToShortDateString();
                txtFechaObsPreliminarI.Text = itemDetalle.FechaObservacionesInicioP.Value.ToShortDateString();
                txtFechaObsPreliminarF.Text = itemDetalle.FechaObservacionesFinP.Value.ToShortDateString();
                txtFechaRpsPreliminarI.Text = itemDetalle.FechaRespuestasInicioP.Value.ToShortDateString();
                txtFechaRpsPreliminarF.Text = itemDetalle.FechaRespuestasFinP.Value.ToShortDateString();
                // Fechas Definitiva.
                txtFechaPublicacionD.Text = itemDetalle.FechaPublicacionD.Value.ToShortDateString();
                txtFechaObsDefinitivoI.Text = itemDetalle.FechaObservacionesInicioD.Value.ToShortDateString();
                txtFechaObsDefinitivoF.Text = itemDetalle.FechaObservacionesFinD.Value.ToShortDateString();
                txtFechaResDefinitivoI.Text = itemDetalle.FechaRespuestasInicioD.Value.ToShortDateString();
                txtFechaResDefinitivoF.Text = itemDetalle.FechaRespuestasFinD.Value.ToShortDateString();
                // Fechas Inscripción
                txtFechaInsOferenteI.Text = itemDetalle.FechaInicioInscripcionOferente.Value.ToShortDateString();
                txtFechaInsOferenteF.Text = itemDetalle.FechaFinInscripcionOferente.Value.ToShortDateString();
            }
            else
                NavigateTo(SolutionPage.List);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
            hfIdConvocatoria.Value = Request.QueryString["Id"];
            isvalid = true;
        }

        return isvalid;
    }
}

