using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.Linq.Expressions;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using System.Collections.Generic;

public partial class Page_BancoOferentes_ConvocatoriaPublica_List : GeneralWeb
{
    masterPrincipalPublica toolBar;
     
    SIAService vRuboService = new SIAService();

    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
            if (!Page.IsPostBack)
            CargarDatosIniciales();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void gvConvocatorias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConvocatorias.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvConvocatorias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConvocatorias.SelectedRow);
    }

    protected void gvConvocatorias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void Buscar()
    {
        try
        {
            CargarGrilla(gvConvocatorias, GridViewSortExpression, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipalPublica)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegatePublic(btnBuscar_Click);
            gvConvocatorias.PageSize = PageSize();
            gvConvocatorias.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Convocatorias P&uacute;blicas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            DateTime? vFechaPublicacionD = null;
            DateTime? vFechaPublicacionP = null;
            int? vNumeroConvocatoria = null;
            int? vIDRegional = null;
            int? vIDEstadoConvocatoria = 0;

            if (txtFechaPublicacionPreliminar.Date.Year.ToString() != "1900")
                vFechaPublicacionP = Convert.ToDateTime(txtFechaPublicacionPreliminar.Date);
            
            if (txtFechaPublicacionDefinitiva.Date.Year.ToString() != "1900")
                vFechaPublicacionD = Convert.ToDateTime(txtFechaPublicacionDefinitiva.Date);

            if (txtNumeroConvocatoria.Text != "")
                vNumeroConvocatoria = Convert.ToInt32(txtNumeroConvocatoria.Text);
           
            
            if (ddlIDRegional.SelectedValue != "-1")
                vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            
            string usuarioArea = null;

            var myGridResults = vBancoOferentesService.ConsultarConvocatorias(vNumeroConvocatoria,vFechaPublicacionD,vFechaPublicacionP,vIDEstadoConvocatoria,vIDRegional);

                int nRegistros = myGridResults.Count;
                int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

                if (nRegistros < NumRegConsultaGrilla)
                {
                    if (expresionOrdenamiento != null)
                    {
                        if (string.IsNullOrEmpty(GridViewSortExpression))
                            GridViewSortDirection = SortDirection.Ascending;
                        else if (GridViewSortExpression != expresionOrdenamiento)
                            GridViewSortDirection = SortDirection.Descending;

                        if (myGridResults != null)
                        {
                            var param = Expression.Parameter(typeof(Convocatoria), expresionOrdenamiento);

                            var prop = Expression.Property(param, expresionOrdenamiento);

                            var sortExpression = Expression.Lambda<Func<Convocatoria, object>>(Expression.Convert(prop, typeof(object)), param);

                            if (GridViewSortDirection == SortDirection.Ascending)
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Descending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Ascending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }

                            GridViewSortExpression = expresionOrdenamiento;
                        }
                    }
                    else
                        gridViewsender.DataSource = myGridResults;
                    
                    gridViewsender.DataBind();
                }
                else
                    toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvConvocatorias.DataKeys[rowIndex].Value.ToString();
            string url = string.Format("Detail.aspx?Id={0}", strValue);
            NavigateTo(url);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
