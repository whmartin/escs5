﻿using System;
using System.Web.UI;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_BancoOferentes_ConvocatoriaPublica_Detail : GeneralWeb
{
    masterPrincipalPublica toolBar;

    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    public List<ConvotatoriaObservacion> Observaciones
    {
        get
        {
            List<ConvotatoriaObservacion> list = new List<ConvotatoriaObservacion>();
            if(ViewState["Observaciones"] != null)
                list = ViewState["Observaciones"] as List<ConvotatoriaObservacion>;
            return list;
        }
        set
        {
            ViewState["Observaciones"] = value;
        }
    }

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
            if (!Page.IsPostBack)
                CargarDatos();
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("Detail.aspx?Id="+hfIdConvocatoria.Value);
    }

    protected void gvObservaciones_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            gvObservaciones.PageIndex = e.NewPageIndex;
            gvObservaciones.DataSource = Observaciones;
            gvObservaciones.DataBind();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ImgAgregarObservacion_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ConvotatoriaObservacion item = new ConvotatoriaObservacion();
            item.IdTipoObservacion = int.Parse(ddlTipoObservación.SelectedValue);
            item.TipoObservacion = ddlTipoObservación.SelectedItem.Text;
            item.Observacion = txtObservacion.Text;
            item.Id = Guid.NewGuid().ToString();
            var miLista = Observaciones;
            miLista.Add(item);
            gvObservaciones.DataSource = miLista;
            gvObservaciones.DataBind();
            Observaciones = miLista;
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void btnGuardar_Click(object sender, EventArgs e)
    {
    }

    protected void btnEliminarObservacion_Click(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            string strValue = gvObservaciones.DataKeys[rowIndex].Value.ToString();
            var items = Observaciones.Where(ie => ie.Id != strValue).ToList();
            Observaciones = items;
            gvObservaciones.DataSource = items;
            gvObservaciones.DataBind();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipalPublica)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegatePublic(btnRetornar_Click);
            toolBar.eventoGuardar += new ToolBarDelegatePublic(btnGuardar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Registro de Observaciones de Convocatoria P&uacute;blica", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if(VerificarQueryStrings())
            {
                ManejoControlesBancoOferentes.LlenarComboTipoObservacion(ddlTipoObservación, true);
                int id = int.Parse(hfIdConvocatoria.Value);
                Convocatoria itemDetalle = vBancoOferentesService.ConsultarConvocatoriaPorId(id);
                txtNombre.Text = itemDetalle.Nombre;
                txtEstado.Text = itemDetalle.Estado;
                txtNumeroConvocatoria.Text = itemDetalle.Numero.ToString();
                txtRegional.Text = itemDetalle.DescripcionRegional;
                gvObservaciones.PageSize = PageSize();
                gvObservaciones.EmptyDataText = EmptyDataText();
            }
            else
                NavigateTo(SolutionPage.List);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
            hfIdConvocatoria.Value = Request.QueryString["Id"];
            isvalid = true;
        }

        return isvalid;
    }


}

