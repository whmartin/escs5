<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ObservacionesPendientes_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdGestionObservacionConvocatoria" runat="server" />
    <asp:HiddenField ID="hfIdObservacionConvocatoria" runat="server" />
    <asp:HiddenField ID="hfIdEvaluador" runat="server" />
    <asp:HiddenField ID="hfIdCoordinador" runat="server" />
    <asp:HiddenField ID="hfRolUsuario" runat="server" />
    <asp:HiddenField ID="hfObservacion" runat="server" />
    <asp:HiddenField ID="hfEsDevolucionDelContratista" runat="server" />
    <asp:HiddenField ID="hfMotivoDevolucionDelContratista" runat="server" />
    <asp:HiddenField ID="hfIdEquipoEvaluador" runat="server" />
    <asp:HiddenField ID="hfIdTipoObservacion" runat="server" />
    
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Observaci&oacute;n
            </td>
            <td>Respuesta
                <asp:RequiredFieldValidator runat="server" ID="rfvRespuesta" ControlToValidate="txtRespuesta"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObservacion" TextMode="multiline" Columns="50" Rows="8" Enabled="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRespuesta" TextMode="multiline" Columns="50" Rows="8" />
            </td>
        </tr>


         <tr>
            <td colspan="2">
                <div id="divMensajeDevolucionDelContratista" runat="server" visible="false">
                    <table width="90%" align="center" runat="server" id="tbMensajeDevolucionDelContratista">
                        <tr class="rowB">
                            <td>
                                Motivo devoluci&oacute;n de contrataci&oacute;n
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtMensajeDevolucionDelContratista" TextMode="multiline" Columns="80" Rows="8" Enabled="false"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
         </tr>   


        <tr>
            <td colspan="2">
                <div id="divtbAprobacionObservacion" runat="server" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 20px;">
                    <table width="90%" align="center" runat="server" id="tbAprobacionObservacion">
                         <tr class="rowA" >
                            <td style="text-align: center;">
                                Aprobar
                            </td>
                            <td style="text-align: center;">
                                Rechazar
                            </td>
                        </tr>
                        <tr class="rowB" style="text-align: center;">
                            <td style="text-align: center;">
                                <asp:ImageButton ID="AprobarImageButton" runat="server" AlternateText="Aprobar" OnClick="AprobarObservacion"
                                    Height="29px" ImageUrl="../../../Image/btn/apply.png" Width="29px" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="RechazarImageButton" runat="server" AlternateText="Rechazar" OnClick="RechazarObservacion"
                                    Height="29px" ImageUrl="../../../Image/btn/Cancel.png" Width="29px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
