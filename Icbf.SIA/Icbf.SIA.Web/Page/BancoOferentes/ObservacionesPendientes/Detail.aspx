<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ObservacionesPendientes_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Observaci&oacute;n
            </td>
            <td>
                Respuesta
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObservacion" TextMode="multiline" Columns="50" Rows="8" Enabled="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRespuesta" TextMode="multiline" Columns="50" Rows="8" Enabled="false"/>
            </td>
        </tr>
    </table>
</asp:Content>

