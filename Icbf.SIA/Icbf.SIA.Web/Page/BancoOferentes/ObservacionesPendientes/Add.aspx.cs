using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Oferente.Entity;
using Icbf.Contrato.Service;
using Icbf.Oferente.Service;
using System.Web.Script.Serialization;
using System.Linq.Expressions;

/// <summary>
/// Página de registro y edición para la entidad ObservacionesPendientes
/// </summary>
public partial class Page_ObservacionesPendientes_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    public List<Evaluador> vListaEvaluadores;

    string PageName = "BancoOferentes/ObservacionesPendientes";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                hfIdGestionObservacionConvocatoria.Value = GetSessionParameter("ObservacionesPendientes.IdGestionObservacionConvocatoria").ToString();
                RemoveSessionParameter("ObservacionesPendientes.IdGestionObservacionConvocatoria");

                hfIdObservacionConvocatoria.Value = GetSessionParameter("ObservacionesPendientes.IdObservacionConvocatoria").ToString();
                RemoveSessionParameter("ObservacionesPendientes.IdObservacionConvocatoria");

                hfIdCoordinador.Value = GetSessionParameter("ObservacionesPendientes.IdCoordinador").ToString();
                RemoveSessionParameter("ObservacionesPendientes.IdCoordinador");

                hfIdEvaluador.Value = GetSessionParameter("ObservacionesPendientes.IdEvaluador").ToString();
                RemoveSessionParameter("ObservacionesPendientes.IdEvaluador");

                hfRolUsuario.Value = GetSessionParameter("ObservacionesPendientes.RolUsuario").ToString();
                RemoveSessionParameter("ObservacionesPendientes.RolUsuario");

                hfObservacion.Value = GetSessionParameter("ObservacionesPendientes.Observacion").ToString();
                RemoveSessionParameter("ObservacionesPendientes.Observacion");

                hfEsDevolucionDelContratista.Value = GetSessionParameter("ObservacionesPendientes.EsDevolucionDelContratista").ToString();
                RemoveSessionParameter("ObservacionesPendientes.EsDevolucionDelContratista");

                hfMotivoDevolucionDelContratista.Value = GetSessionParameter("ObservacionesPendientes.MotivoDevolucionDelContratista").ToString();
                RemoveSessionParameter("ObservacionesPendientes.MotivoDevolucionDelContratista");

                hfIdEquipoEvaluador.Value = GetSessionParameter("ObservacionesPendientes.IdEquipoEvaluador").ToString();
                RemoveSessionParameter("ObservacionesPendientes.IdEquipoEvaluador");

                hfIdTipoObservacion.Value = GetSessionParameter("ObservacionesPendientes.IdTipoObservacion").ToString();
                RemoveSessionParameter("ObservacionesPendientes.IdTipoObservacion");
                
                CargarDatosIniciales();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado para la entidad ObservacionesPendientes
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;

            if (string.IsNullOrWhiteSpace(txtObservacion.Text))
                throw new Exception("El campo de observación no puede estar vacío");

            if (hfRolUsuario.Value == "Evaluador" || hfRolUsuario.Value == "Administrador-Evaluador")
            {

                int vIdEstadoObservacionAsignada = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("ASI").IdEstadoObservacion;
                int vIdEstadoObservacionEnSupervision = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("SUP").IdEstadoObservacion;

                ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

                vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                vObservacionesPendientes.Descripcion = txtRespuesta.Text;
                vObservacionesPendientes.EsDevolucion = false;
                vObservacionesPendientes.IdUsuario = Convert.ToInt32(hfIdEvaluador.Value);
                vObservacionesPendientes.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarGestionObservacionConvocatoria(vObservacionesPendientes);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    var vEquipoEvaluador = vBancoOferentesService.ConsultarEquipoEvaluador(vBancoOferentesService.ConsultarEvaluador(Convert.ToInt32(hfIdEvaluador.Value)).IdEquipoEvaluador);

                    vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                    vObservacionesPendientes.EsCoordinador = true;
                    vObservacionesPendientes.IdEstadoObservacion = vIdEstadoObservacionEnSupervision;
                    vObservacionesPendientes.IdUsuario = vEquipoEvaluador.IdUsuarioCoordinador;
                    vObservacionesPendientes.UsuarioModifica = GetSessionUser().NombreUsuario;

                    InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                    vResultado = vBancoOferentesService.ModificarObservacionConvocatoria(vObservacionesPendientes);

                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        HistoricoObservacionesConvocatoria vHistoricoObservacionesConvocatoria = new HistoricoObservacionesConvocatoria();

                        vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                        vHistoricoObservacionesConvocatoria.IdUsuarioAnterior = Convert.ToInt32(hfIdEvaluador.Value);
                        vHistoricoObservacionesConvocatoria.IdUsuarioNuevo = vEquipoEvaluador.IdUsuarioCoordinador;
                        vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior = Convert.ToInt32(hfIdGestionObservacionConvocatoria.Value);
                        vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva = vObservacionesPendientes.IdGestionObservacionConvocatoria;
                        vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior = vIdEstadoObservacionAsignada; 
                        vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva = vIdEstadoObservacionEnSupervision; 
                        vHistoricoObservacionesConvocatoria.UsuarioCrea = GetSessionUser().NombreUsuario;

                        InformacionAudioria(vHistoricoObservacionesConvocatoria, this.PageName, vSolutionPage);
                        vResultado = vBancoOferentesService.InsertarHistoricoObservacionesConvocatoria(vHistoricoObservacionesConvocatoria);

                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                        }
                        else if (vResultado == 1)
                        {
                            SetSessionParameter("ObservacionesPendientes.Guardado", "1");
                            SetSessionParameter("ObservacionesPendientes.Observacion", txtObservacion.Text);
                            SetSessionParameter("ObservacionesPendientes.Respuesta", txtRespuesta.Text);
                            NavigateTo(SolutionPage.Detail);
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La observación debe ser aprobada o rechazada");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);

            toolBar.EstablecerTitulos("Observaciones pendientes", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if (hfRolUsuario.Value == "Evaluador" || hfRolUsuario.Value == "Administrador-Evaluador")
                divtbAprobacionObservacion.Visible = false;

            if (hfRolUsuario.Value == "Coordinador" || hfRolUsuario.Value == "Administrador-Coordinador" )
            {
                txtObservacion.Enabled = true;

                if (hfEsDevolucionDelContratista.Value == "True")
                {
                    divMensajeDevolucionDelContratista.Visible = true;
                    txtMensajeDevolucionDelContratista.Text = hfMotivoDevolucionDelContratista.Value;
                }
            }

            txtObservacion.Text = hfObservacion.Value;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void AprobarObservacion(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(txtRespuesta.Text))
                throw new Exception("El campo de respuesta no puede estar vacío");

            if (string.IsNullOrWhiteSpace(txtObservacion.Text))
                throw new Exception("El campo de observación no puede estar vacío");

            if (hfRolUsuario.Value == "Coordinador" || hfRolUsuario.Value == "Administrador-Coordinador")
            {
                AprobarObservacionCoordinador();
            }
            else if (hfRolUsuario.Value == "RevisionBancoOferente")
            {
                AprobarObservacionContratista();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void AprobarObservacionCoordinador()
    {

        if ((hfRolUsuario.Value == "Coordinador" || hfRolUsuario.Value == "Administrador-Coordinador" ) && hfEsDevolucionDelContratista.Value == "True")
        {
            if (String.Compare(hfObservacion.Value.Trim(), txtObservacion.Text.Trim(), StringComparison.OrdinalIgnoreCase) == 0)
                throw new Exception("Debe modificar la observación");
        }

        int vResultado;

        int vIdEstadoObservacionEnSupervision = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("SUP").IdEstadoObservacion;
        int vIdEstadoObservacionEnContratacion = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("CON").IdEstadoObservacion;

        ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

        vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
        vObservacionesPendientes.Descripcion = txtRespuesta.Text;
        vObservacionesPendientes.EsDevolucion = false;
        vObservacionesPendientes.IdUsuario = Convert.ToInt32(hfIdCoordinador.Value);
        vObservacionesPendientes.UsuarioCrea = GetSessionUser().NombreUsuario;

        InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
        vResultado = vBancoOferentesService.InsertarGestionObservacionConvocatoria(vObservacionesPendientes);

        if (vResultado == 0)
        {
            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
        }
        else if (vResultado == 1)
        {
            vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
            vObservacionesPendientes.EsCoordinador = false;
            vObservacionesPendientes.IdEstadoObservacion = vIdEstadoObservacionEnContratacion; 
            vObservacionesPendientes.IdUsuario = null;
            vObservacionesPendientes.UsuarioModifica = GetSessionUser().NombreUsuario;

            InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
            vResultado = vBancoOferentesService.ModificarObservacionConvocatoria(vObservacionesPendientes);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                HistoricoObservacionesConvocatoria vHistoricoObservacionesConvocatoria = new HistoricoObservacionesConvocatoria();

                vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                vHistoricoObservacionesConvocatoria.IdUsuarioAnterior = Convert.ToInt32(hfIdCoordinador.Value);
                vHistoricoObservacionesConvocatoria.IdUsuarioNuevo = null;
                vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior = Convert.ToInt32(hfIdGestionObservacionConvocatoria.Value);
                vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva = vObservacionesPendientes.IdGestionObservacionConvocatoria;
                vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior = vIdEstadoObservacionEnSupervision;
                vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva = vIdEstadoObservacionEnContratacion;   
                vHistoricoObservacionesConvocatoria.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vHistoricoObservacionesConvocatoria, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarHistoricoObservacionesConvocatoria(vHistoricoObservacionesConvocatoria);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    vObservacionesPendientes.IdGestionObservacionConvocatoria = Convert.ToInt32(hfIdGestionObservacionConvocatoria.Value);
                    vObservacionesPendientes.Descripcion = txtObservacion.Text;
                    vObservacionesPendientes.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                    vResultado = vBancoOferentesService.ModificarDescripcionGestionObservacionConvocatoria(vObservacionesPendientes);

                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        SetSessionParameter("ObservacionesPendientes.Guardado", "2");
                        SetSessionParameter("ObservacionesPendientes.Observacion", txtObservacion.Text);
                        SetSessionParameter("ObservacionesPendientes.Respuesta", txtRespuesta.Text);
                        NavigateTo(SolutionPage.Detail);
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
        }
    }

    private void AprobarObservacionContratista()
    {
        int vResultado;

        int vIdEstadoObservacionEnContratacion = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("CON").IdEstadoObservacion;
        int vIdEstadoObservacionPublicada = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("PUB").IdEstadoObservacion;

        ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

        vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
        vObservacionesPendientes.Descripcion = txtRespuesta.Text;
        vObservacionesPendientes.EsDevolucion = false;
        vObservacionesPendientes.IdUsuario = GetSessionUser().IdUsuario;
        vObservacionesPendientes.UsuarioCrea = GetSessionUser().NombreUsuario;

        InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
        vResultado = vBancoOferentesService.InsertarGestionObservacionConvocatoria(vObservacionesPendientes);

        if (vResultado == 0)
        {
            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
        }
        else if (vResultado == 1)
        {
            vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
            vObservacionesPendientes.EsCoordinador = false;
            vObservacionesPendientes.IdEstadoObservacion = vIdEstadoObservacionPublicada; 
            vObservacionesPendientes.IdUsuario = null;
            vObservacionesPendientes.UsuarioModifica = GetSessionUser().NombreUsuario;

            InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
            vResultado = vBancoOferentesService.ModificarObservacionConvocatoria(vObservacionesPendientes);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                HistoricoObservacionesConvocatoria vHistoricoObservacionesConvocatoria = new HistoricoObservacionesConvocatoria();

                vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                vHistoricoObservacionesConvocatoria.IdUsuarioAnterior = GetSessionUser().IdUsuario;
                vHistoricoObservacionesConvocatoria.IdUsuarioNuevo = null;
                vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior = Convert.ToInt32(hfIdGestionObservacionConvocatoria.Value);
                vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva = vObservacionesPendientes.IdGestionObservacionConvocatoria;
                vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior = vIdEstadoObservacionEnContratacion; 
                vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva = vIdEstadoObservacionPublicada;  
                vHistoricoObservacionesConvocatoria.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vHistoricoObservacionesConvocatoria, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarHistoricoObservacionesConvocatoria(vHistoricoObservacionesConvocatoria);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("ObservacionesPendientes.Guardado", "2");
                    SetSessionParameter("ObservacionesPendientes.Observacion", txtObservacion.Text);
                    SetSessionParameter("ObservacionesPendientes.Respuesta", txtRespuesta.Text);
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
        }
    }

    protected void RechazarObservacion(object sender, EventArgs e)
    {
        try
        {
            int vResultado;

            if (string.IsNullOrWhiteSpace(txtRespuesta.Text))
                throw new Exception("El campo de respuesta no puede estar vacío");

            if (string.IsNullOrWhiteSpace(txtObservacion.Text))
                throw new Exception("El campo de observación no puede estar vacío");

            int vIdEstadoObservacionAsignada = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("ASI").IdEstadoObservacion;
            int vIdEstadoObservacionEnSupervision = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("SUP").IdEstadoObservacion;
            int vIdEstadoObservacionEnContratacion = vBancoOferentesService.ConsultarEstadoObservacionPorCodigoEstado("CON").IdEstadoObservacion;

            if (hfRolUsuario.Value == "Coordinador" || hfRolUsuario.Value == "Administrador-Coordinador")
            {
                var vEvaluador = vBancoOferentesService.ConsultarEvaluador(Convert.ToInt32(hfIdEvaluador.Value));
                if ((vEvaluador.IdEvaluador == 0 || vEvaluador.Activo == false) && vEvaluador.IdEquipoEvaluador == Convert.ToInt32(hfIdEquipoEvaluador.Value))
                {
                    var vIdEvaluadorCandidato = vBancoOferentesService.ConsultarEvaluadorDisponibleParaAsignarObservacion(Convert.ToInt32(hfIdEquipoEvaluador.Value), Convert.ToInt32(hfIdTipoObservacion.Value)).IdEvaluador;
                    if (vIdEvaluadorCandidato != 0)
                        hfIdEvaluador.Value = vIdEvaluadorCandidato.ToString();
                    else
                        throw new Exception("No hay ningún evaluador disponible en el momento para realizar la devolución, agrege uno nuevo o intente más tarde");
                }
                    
                ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();
                
                vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                vObservacionesPendientes.Descripcion = txtRespuesta.Text;
                vObservacionesPendientes.EsDevolucion = true;
                vObservacionesPendientes.IdUsuario = Convert.ToInt32(hfIdCoordinador.Value);
                vObservacionesPendientes.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarGestionObservacionConvocatoria(vObservacionesPendientes);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                    vObservacionesPendientes.EsCoordinador = false;
                    vObservacionesPendientes.IdEstadoObservacion = vIdEstadoObservacionAsignada;
                    vObservacionesPendientes.IdUsuario = Convert.ToInt32(hfIdEvaluador.Value); // validación si todavía existe ese evaluador en ese equipo
                    vObservacionesPendientes.UsuarioModifica = GetSessionUser().NombreUsuario;

                    InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                    vResultado = vBancoOferentesService.ModificarObservacionConvocatoria(vObservacionesPendientes);

                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        HistoricoObservacionesConvocatoria vHistoricoObservacionesConvocatoria = new HistoricoObservacionesConvocatoria();

                        vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                        vHistoricoObservacionesConvocatoria.IdUsuarioAnterior = Convert.ToInt32(hfIdCoordinador.Value);
                        vHistoricoObservacionesConvocatoria.IdUsuarioNuevo = Convert.ToInt32(hfIdEvaluador.Value); // validación si todavía existe ese evaluador en ese equipo
                        vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior = Convert.ToInt32(hfIdGestionObservacionConvocatoria.Value);
                        vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva = vObservacionesPendientes.IdGestionObservacionConvocatoria;
                        vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior = vIdEstadoObservacionEnSupervision; 
                        vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva = vIdEstadoObservacionAsignada; 
                        vHistoricoObservacionesConvocatoria.UsuarioCrea = GetSessionUser().NombreUsuario;

                        InformacionAudioria(vHistoricoObservacionesConvocatoria, this.PageName, vSolutionPage);
                        vResultado = vBancoOferentesService.InsertarHistoricoObservacionesConvocatoria(vHistoricoObservacionesConvocatoria);

                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                        }
                        else if (vResultado == 1)
                        {
                            SetSessionParameter("ObservacionesPendientes.Guardado", "3");
                            SetSessionParameter("ObservacionesPendientes.Observacion", txtObservacion.Text);
                            SetSessionParameter("ObservacionesPendientes.Respuesta", txtRespuesta.Text);
                            NavigateTo(SolutionPage.Detail);
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
            else if (hfRolUsuario.Value == "RevisionBancoOferente")
            {
                ObservacionesPendientes vObservacionesPendientes = new ObservacionesPendientes();

                vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                vObservacionesPendientes.Descripcion = txtRespuesta.Text;
                vObservacionesPendientes.EsDevolucion = true;
                vObservacionesPendientes.IdUsuario = GetSessionUser().IdUsuario;
                vObservacionesPendientes.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarGestionObservacionConvocatoria(vObservacionesPendientes);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    vObservacionesPendientes.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                    vObservacionesPendientes.EsCoordinador = true;
                    vObservacionesPendientes.IdEstadoObservacion = vIdEstadoObservacionEnSupervision; 
                    vObservacionesPendientes.IdUsuario = Convert.ToInt32(hfIdCoordinador.Value);
                    vObservacionesPendientes.UsuarioModifica = GetSessionUser().NombreUsuario;

                    InformacionAudioria(vObservacionesPendientes, this.PageName, vSolutionPage);
                    vResultado = vBancoOferentesService.ModificarObservacionConvocatoria(vObservacionesPendientes);

                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        HistoricoObservacionesConvocatoria vHistoricoObservacionesConvocatoria = new HistoricoObservacionesConvocatoria();

                        vHistoricoObservacionesConvocatoria.IdObservacionConvocatoria = Convert.ToInt32(hfIdObservacionConvocatoria.Value);
                        vHistoricoObservacionesConvocatoria.IdUsuarioAnterior = GetSessionUser().IdUsuario;
                        vHistoricoObservacionesConvocatoria.IdUsuarioNuevo = Convert.ToInt32(hfIdCoordinador.Value);
                        vHistoricoObservacionesConvocatoria.IdGestionObservacionAnterior = Convert.ToInt32(hfIdGestionObservacionConvocatoria.Value);
                        vHistoricoObservacionesConvocatoria.IdGestionObservacionNueva = vObservacionesPendientes.IdGestionObservacionConvocatoria;
                        vHistoricoObservacionesConvocatoria.IdEstadoObservacionAnterior = vIdEstadoObservacionEnContratacion;
                        vHistoricoObservacionesConvocatoria.IdEstadoObservacionNueva = vIdEstadoObservacionEnSupervision; 
                        vHistoricoObservacionesConvocatoria.UsuarioCrea = GetSessionUser().NombreUsuario;

                        InformacionAudioria(vHistoricoObservacionesConvocatoria, this.PageName, vSolutionPage);
                        vResultado = vBancoOferentesService.InsertarHistoricoObservacionesConvocatoria(vHistoricoObservacionesConvocatoria);

                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                        }
                        else if (vResultado == 1)
                        {
                            SetSessionParameter("ObservacionesPendientes.Guardado", "3");
                            SetSessionParameter("ObservacionesPendientes.Observacion", txtObservacion.Text);
                            SetSessionParameter("ObservacionesPendientes.Respuesta", txtRespuesta.Text);
                            NavigateTo(SolutionPage.Detail);
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}