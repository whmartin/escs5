<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ObservacionesPendientes_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfRolUsuario" runat="server" />
    <asp:HiddenField ID="hfEsDevolucionDelCoordinador" runat="server" />

    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObservacionesPendientes" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdGestionObservacionConvocatoria,IdObservacionConvocatoria,IdEvaluador,IdCoordinador,Observacion,Descripcion,EsDevolucion,MotivoDevolucionDelContratista,IdTipoObservacion,IdEquipoEvaluador" CellPadding="0" Height="16px"
                        OnSorting="gvObservacionesPendientes_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvObservacionesPendientes_PageIndexChanging" OnSelectedIndexChanged="gvObservacionesPendientes_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Gestionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnGestionar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha publicaci&oacute;n" DataField="FechaPublicacionConvocatoriaGrilla" SortExpression="FechaPublicacionConvocatoriaGrilla" />
                            <asp:BoundField HeaderText="Estado convocatoria" DataField="EstadoConvocatoria" SortExpression="EstadoConvocatoria" />
                            <asp:BoundField HeaderText="Nombre convocatoria" DataField="NombreConvocatoria" SortExpression="NombreConvocatoria" />
                            <asp:BoundField HeaderText="N&uacute;mero radicado" DataField="NumeroRadicado" SortExpression="NumeroRadicado" />
                            <asp:BoundField HeaderText="Fecha observaci&oacute;n" DataField="FechaObservacionGrilla" SortExpression="FechaObservacionGrilla" />
                            <asp:BoundField HeaderText="Observaci&oacute;n" DataField="Observacion" SortExpression="Observacion" />
                            <asp:BoundField HeaderText="Tipo observaci&oacute;n" DataField="TipoObservacion" SortExpression="TipoObservacion" />
                            <asp:BoundField HeaderText="Respuesta" DataField="Descripcion" SortExpression="Descripcion" />
                            <asp:BoundField HeaderText="Fecha respuesta" DataField="FechaRespuestaGrilla" SortExpression="FechaRespuestaGrilla" />
                            <asp:BoundField HeaderText="Nombre evaluador" DataField="NombreEvaluador" SortExpression="NombreEvaluador" />
                            <asp:BoundField HeaderText="Nombre coordinador" DataField="NombreCoordinador" SortExpression="NombreCoordinador" />
                            <asp:BoundField HeaderText="Estado observaci&oacute;n" DataField="EstadoObservacion" SortExpression="EstadoObservacion" />
                            <asp:BoundField HeaderText="Fecha &uacute;ltimo estado" DataField="FechaUltimoEstadoGrilla" SortExpression="FechaUltimoEstadoGrilla" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
