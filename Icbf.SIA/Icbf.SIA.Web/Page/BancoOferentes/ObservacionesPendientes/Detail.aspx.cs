using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Oferente.Entity;
using Icbf.Contrato.Service;
using Icbf.Oferente.Service;
using System.Web.Script.Serialization;
using System.Linq.Expressions;

/// <summary>
/// Página de detalles para la entidad ObservacionesPendientes
/// </summary>
public partial class Page_ObservacionesPendientes_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    
    string PageName = "BancoOferentes/ObservacionesPendientes";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatos();
            }
        }
    }
    
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Observaciones Pendientes", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("ObservacionesPendientes.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La observación ha sido respondida exitosamente");

            if (GetSessionParameter("ObservacionesPendientes.Guardado").ToString() == "2")
                toolBar.MostrarMensajeGuardado("La observación ha sido aprobada exitosamente");

            if (GetSessionParameter("ObservacionesPendientes.Guardado").ToString() == "3")
                toolBar.MostrarMensajeGuardado("La observación ha sido rechazada exitosamente");

            RemoveSessionParameter("ObservacionesPendientes.Guardado");

            txtObservacion.Text = GetSessionParameter("ObservacionesPendientes.Observacion").ToString();
            txtRespuesta.Text = GetSessionParameter("ObservacionesPendientes.Respuesta").ToString();

            RemoveSessionParameter("ObservacionesPendientes.Observacion");
            RemoveSessionParameter("ObservacionesPendientes.Respuesta");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}