using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using System.Collections.Generic;
using Icbf.SIA.Service;

/// <summary>
/// Página de consulta para la entidad ObservacionesPendientes
/// </summary>
public partial class Page_ObservacionesPendientes_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "BancoOferentes/ObservacionesPendientes";
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();

            Buscar();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvObservacionesPendientes, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvObservacionesPendientes.PageSize = PageSize();
            gvObservacionesPendientes.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Observaciones pendientes", SolutionPage.List.ToString());

            if ((GetSessionUser().Rol.ToLower()).Contains("administrador"))
            {
                hfRolUsuario.Value = "Administrador";
            }
            else if ((String.Compare(GetSessionUser().Rol, "RevisionBancoOferente", StringComparison.OrdinalIgnoreCase)) == 0)
            {
                hfRolUsuario.Value = "RevisionBancoOferente";
            }
            else
            {
                var vCoordinador = vBancoOferentesService.ConsultarEquiposEvaluadoresss(GetSessionUser().NumeroDocumento, null, null);

                if (vCoordinador.Count > 0)
                    hfRolUsuario.Value = "Coordinador";
                else
                    hfRolUsuario.Value = "Evaluador";
            }

            Buscar();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string dknIdGestionObservacionConvocatoria = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[0].ToString();
            string dknIdObservacionConvocatoria = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[1].ToString();
            string dknIdEvaluador = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[2].ToString();
            string dknIdCoordinador = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[3].ToString();
            string dknObservacion = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[4].ToString();
            string dknRespuesta = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[5].ToString();
            string dknEsDevolucion = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[6].ToString();
            string dknMotivoDevolucionDelContratista = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[7].ToString();
            string dknIdTipoObservacion = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[8].ToString();
            string dknIdEquipoEvaluador = gvObservacionesPendientes.DataKeys[pRow.RowIndex].Values[9].ToString();

            SetSessionParameter("ObservacionesPendientes.IdGestionObservacionConvocatoria", dknIdGestionObservacionConvocatoria);
            SetSessionParameter("ObservacionesPendientes.IdObservacionConvocatoria", dknIdObservacionConvocatoria);
            SetSessionParameter("ObservacionesPendientes.IdEvaluador", dknIdEvaluador);
            SetSessionParameter("ObservacionesPendientes.IdCoordinador", dknIdCoordinador);
            SetSessionParameter("ObservacionesPendientes.EsDevolucionDelContratista", dknEsDevolucion);
            SetSessionParameter("ObservacionesPendientes.IdEquipoEvaluador", dknIdEquipoEvaluador);
            SetSessionParameter("ObservacionesPendientes.IdTipoObservacion", dknIdTipoObservacion);

            if (string.IsNullOrEmpty(dknRespuesta))
            {
                SetSessionParameter("ObservacionesPendientes.Observacion", dknObservacion);
                if (hfRolUsuario.Value == "Administrador")
                    hfRolUsuario.Value = "Administrador-Evaluador";
            }
            else
            {
                SetSessionParameter("ObservacionesPendientes.Observacion", dknRespuesta);
                if (hfRolUsuario.Value == "Administrador")
                {
                    if (hfEsDevolucionDelCoordinador.Value == "true")
                        hfRolUsuario.Value = "Administrador-Evaluador";
                    else
                        hfRolUsuario.Value = "Administrador-Coordinador";
                }
            }

            if (string.IsNullOrEmpty(dknMotivoDevolucionDelContratista))
                SetSessionParameter("ObservacionesPendientes.MotivoDevolucionDelContratista", "");
            else
                SetSessionParameter("ObservacionesPendientes.MotivoDevolucionDelContratista", dknMotivoDevolucionDelContratista);

            SetSessionParameter("ObservacionesPendientes.RolUsuario", hfRolUsuario.Value);

            NavigateTo(SolutionPage.Add);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvObservacionesPendientes_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvObservacionesPendientes.SelectedRow);
    }

    protected void gvObservacionesPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvObservacionesPendientes.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvObservacionesPendientes_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            var vListaObservacionesPendientesGrilla = new List<ObservacionesPendientes>();

            if (hfRolUsuario.Value == "Coordinador")
                vListaObservacionesPendientesGrilla = ConsultarObservacionesPendientesCoordinador(GetSessionUser().NumeroDocumento);

            if (hfRolUsuario.Value == "Evaluador")
                vListaObservacionesPendientesGrilla = ConsultarObservacionesPendientesEvaluador(GetSessionUser().NumeroDocumento);

            if (hfRolUsuario.Value == "RevisionBancoOferente")
                vListaObservacionesPendientesGrilla = ConsultarObservacionesPendientesContratista();

            if (hfRolUsuario.Value == "Administrador")
            {
                var vListaObservacionesPendientes = vBancoOferentesService.ConsultarObservacionesPendientess();

                var vListaCoordinadoresNoRepetidos =
                            (
                                from coordinador
                                in vListaObservacionesPendientes
                                where coordinador.EsCoordinador == true
                                select coordinador.NumeroDocumento
                            ).Distinct();

                var vListaEvaluadoresNoRepetidos =
                            (
                                from evaluador
                                in vListaObservacionesPendientes
                                where evaluador.EsCoordinador == false
                                select evaluador.NumeroDocumento
                            ).Distinct();

                if (vListaCoordinadoresNoRepetidos.Count() > 0)
                {
                    foreach (var numeroDocumento in vListaCoordinadoresNoRepetidos)
                    {
                        vListaObservacionesPendientesGrilla = ConsultarObservacionesPendientesCoordinador(numeroDocumento).Concat(vListaObservacionesPendientesGrilla).ToList();
                    }
                }

                if (vListaEvaluadoresNoRepetidos.Count() > 0)
                {
                    foreach (var numeroDocumento in vListaEvaluadoresNoRepetidos)
                    {
                        vListaObservacionesPendientesGrilla = ConsultarObservacionesPendientesEvaluador(numeroDocumento).Concat(vListaObservacionesPendientesGrilla).ToList();
                    }
                }
            }

            if (vListaObservacionesPendientesGrilla.Count == 0)
                throw new Exception("Por el momento no tiene observaciones pendientes por responder");

            var myGridResults = vListaObservacionesPendientesGrilla;

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }

            if (expresionOrdenamiento != null)
            {
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Oferente.Entity.ObservacionesPendientes), expresionOrdenamiento);

                    var prop = Expression.Property(param, expresionOrdenamiento);

                    var sortExpression = Expression.Lambda<Func<Icbf.Oferente.Entity.ObservacionesPendientes, object>>(Expression.Convert(prop, typeof(object)), param);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private List<ObservacionesPendientes> ConsultarObservacionesPendientesCoordinador(string pNumeroDocumento)
    {
        var vObservacionesPendientes = new ObservacionesPendientes();
        var vConvocatoria = new Convocatoria();
        var vListaObservacionesPendientes = new List<ObservacionesPendientes>();

        vListaObservacionesPendientes = vBancoOferentesService.ConsultarObservacionesPendientessCoordinador(pNumeroDocumento);

        foreach (var observacionPendiente in vListaObservacionesPendientes)
        {
            vConvocatoria = vBancoOferentesService.ConsultarConvocatoriaPorId(observacionPendiente.IdConvocatoria);

            observacionPendiente.FechaObservacionGrilla = (observacionPendiente.FechaObservacion != null) ? observacionPendiente.FechaObservacion.ToShortDateString() : "";
            observacionPendiente.FechaUltimoEstadoGrilla = (observacionPendiente.FechaUltimoEstado != null) ? observacionPendiente.FechaUltimoEstado.ToShortDateString() : "";
            observacionPendiente.FechaPublicacionConvocatoriaGrilla = (vConvocatoria.FechaPublicacionD != null) ? vConvocatoria.FechaPublicacionD.Value.ToShortDateString() : "";

            observacionPendiente.EstadoConvocatoria = vConvocatoria.Estado;
            observacionPendiente.NombreConvocatoria = vConvocatoria.Nombre;
            observacionPendiente.NumeroRadicado = vConvocatoria.Numero;

            vObservacionesPendientes = vBancoOferentesService.ConsultarGestionMasRecienteAUnaObservacion(observacionPendiente.IdObservacionConvocatoria);

            observacionPendiente.FechaRespuestaGrilla = (vObservacionesPendientes.FechaRespuesta != null) ? vObservacionesPendientes.FechaRespuesta.Value.ToShortDateString() : "";
            observacionPendiente.EsDevolucion = vObservacionesPendientes.EsDevolucion;

            if (observacionPendiente.EsDevolucion == true)
            {
                observacionPendiente.MotivoDevolucionDelContratista = string.IsNullOrEmpty(vObservacionesPendientes.Descripcion) ? "" : vObservacionesPendientes.Descripcion;

                var vObservacionesPendientess = vBancoOferentesService.ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(observacionPendiente.IdObservacionConvocatoria);

                observacionPendiente.FechaRespuestaGrilla = (vObservacionesPendientess.FechaRespuesta != null) ? vObservacionesPendientess.FechaRespuesta.Value.ToShortDateString() : "";
                observacionPendiente.IdGestionObservacionConvocatoria = vObservacionesPendientess.IdGestionObservacionConvocatoria;
                observacionPendiente.Descripcion = string.IsNullOrEmpty(vObservacionesPendientess.Descripcion) ? "" : vObservacionesPendientess.Descripcion;

                var vDatosEvaluador = vBancoOferentesService.ConsultarEvaluador(vObservacionesPendientess.IdUsuario ?? 0);
                observacionPendiente.IdEvaluador = vDatosEvaluador.IdUsuario;
                observacionPendiente.NombreEvaluador = vDatosEvaluador.NombreEvaluador;

                observacionPendiente.IdEquipoEvaluador = vDatosEvaluador.IdEquipoEvaluador;

                var vDatosCoordinador = vBancoOferentesService.ConsultarEquipoEvaluador(vDatosEvaluador.IdEquipoEvaluador);
                observacionPendiente.IdCoordinador = vDatosCoordinador.IdUsuarioCoordinador;
                observacionPendiente.NombreCoordinador = vDatosCoordinador.NombreCompleto;
            }
            else
            {
                observacionPendiente.MotivoDevolucionDelContratista = "";
                observacionPendiente.IdGestionObservacionConvocatoria = vObservacionesPendientes.IdGestionObservacionConvocatoria;
                observacionPendiente.Descripcion = string.IsNullOrEmpty(vObservacionesPendientes.Descripcion) ? "" : vObservacionesPendientes.Descripcion;

                var datosUsuarioEvaluador = vSIAService.ConsultarUsuarioPorId(vBancoOferentesService.ConsultarHistoricoObservacionesConvocatoriaPorGestionMasReciente(vObservacionesPendientes.IdGestionObservacionConvocatoria).IdUsuarioAnterior);
                observacionPendiente.IdEvaluador = datosUsuarioEvaluador.IdUsuario;
                observacionPendiente.NombreEvaluador = datosUsuarioEvaluador.NombreCompleto;

                observacionPendiente.IdCoordinador = observacionPendiente.IdUsuario ?? 0;
                observacionPendiente.NombreCoordinador = vSIAService.ConsultarUsuarioPorId(observacionPendiente.IdCoordinador).NombreCompleto;

                observacionPendiente.IdEquipoEvaluador = vBancoOferentesService.ConsultarEvaluador(datosUsuarioEvaluador.IdUsuario).IdEquipoEvaluador;
            }
        }
        return vListaObservacionesPendientes;
    }

    private List<ObservacionesPendientes> ConsultarObservacionesPendientesEvaluador(string pNumeroDocumento)
    {
        var vObservacionesPendientes = new ObservacionesPendientes();
        var vConvocatoria = new Convocatoria();
        var vListaObservacionesPendientes = new List<ObservacionesPendientes>();

        vListaObservacionesPendientes = vBancoOferentesService.ConsultarObservacionesPendientessEvaluador(pNumeroDocumento);

        foreach (var observacionPendiente in vListaObservacionesPendientes)
        {
            vConvocatoria = vBancoOferentesService.ConsultarConvocatoriaPorId(observacionPendiente.IdConvocatoria);

            observacionPendiente.FechaObservacionGrilla = (observacionPendiente.FechaObservacion != null) ? observacionPendiente.FechaObservacion.ToShortDateString() : "";
            observacionPendiente.FechaUltimoEstadoGrilla = (observacionPendiente.FechaUltimoEstado != null) ? observacionPendiente.FechaUltimoEstado.ToShortDateString() : "";
            observacionPendiente.FechaPublicacionConvocatoriaGrilla = (vConvocatoria.FechaPublicacionD != null) ? vConvocatoria.FechaPublicacionD.Value.ToShortDateString() : "";

            observacionPendiente.EstadoConvocatoria = vConvocatoria.Estado;
            observacionPendiente.NombreConvocatoria = vConvocatoria.Nombre;
            observacionPendiente.NumeroRadicado = vConvocatoria.Numero;

            vObservacionesPendientes = vBancoOferentesService.ConsultarGestionMasRecienteAUnaObservacion(observacionPendiente.IdObservacionConvocatoria);

            if (vObservacionesPendientes.EsDevolucion == true)
                hfEsDevolucionDelCoordinador.Value = "true";

            observacionPendiente.FechaRespuestaGrilla = (vObservacionesPendientes.FechaRespuesta != null) ? vObservacionesPendientes.FechaRespuesta.Value.ToShortDateString() : "";
            observacionPendiente.IdGestionObservacionConvocatoria = vObservacionesPendientes.IdGestionObservacionConvocatoria;

            observacionPendiente.Descripcion = string.IsNullOrEmpty(vObservacionesPendientes.Descripcion) ? "" : vObservacionesPendientes.Descripcion;
            observacionPendiente.MotivoDevolucionDelContratista = "";

            var datosCoordinador = vBancoOferentesService.ConsultarEquipoEvaluador(vBancoOferentesService.ConsultarEvaluador(observacionPendiente.IdUsuario ?? 0).IdEquipoEvaluador);
            observacionPendiente.IdCoordinador = datosCoordinador.IdUsuarioCoordinador;
            observacionPendiente.NombreCoordinador = datosCoordinador.NombreCompleto;

            observacionPendiente.IdEquipoEvaluador = datosCoordinador.IdEquipoEvaluador;

            observacionPendiente.IdEvaluador = observacionPendiente.IdUsuario ?? 0;
            observacionPendiente.NombreEvaluador = vSIAService.ConsultarUsuarioPorId(observacionPendiente.IdEvaluador).NombreCompleto;
        }
        return vListaObservacionesPendientes;
    }


    private List<ObservacionesPendientes> ConsultarObservacionesPendientesContratista()
    {
        var vObservacionesPendientes = new ObservacionesPendientes();
        var vConvocatoria = new Convocatoria();
        var vListaObservacionesPendientes = new List<ObservacionesPendientes>();

        vListaObservacionesPendientes = vBancoOferentesService.ConsultarObservacionesPendientessContratista();

        foreach (var observacionPendiente in vListaObservacionesPendientes)
        {
            vConvocatoria = vBancoOferentesService.ConsultarConvocatoriaPorId(observacionPendiente.IdConvocatoria);

            observacionPendiente.FechaObservacionGrilla = (observacionPendiente.FechaObservacion != null) ? observacionPendiente.FechaObservacion.ToShortDateString() : "";
            observacionPendiente.FechaUltimoEstadoGrilla = (observacionPendiente.FechaUltimoEstado != null) ? observacionPendiente.FechaUltimoEstado.ToShortDateString() : "";
            observacionPendiente.FechaPublicacionConvocatoriaGrilla = (vConvocatoria.FechaPublicacionD != null) ? vConvocatoria.FechaPublicacionD.Value.ToShortDateString() : "";

            observacionPendiente.EstadoConvocatoria = vConvocatoria.Estado;
            observacionPendiente.NombreConvocatoria = vConvocatoria.Nombre;
            observacionPendiente.NumeroRadicado = vConvocatoria.Numero;

            vObservacionesPendientes = vBancoOferentesService.ConsultarGestionMasRecienteAUnaObservacionDelEvaluadorParaContratista(observacionPendiente.IdObservacionConvocatoria);

            observacionPendiente.FechaRespuestaGrilla = (vObservacionesPendientes.FechaRespuesta != null) ? vObservacionesPendientes.FechaRespuesta.Value.ToShortDateString() : "";
            observacionPendiente.IdGestionObservacionConvocatoria = vObservacionesPendientes.IdGestionObservacionConvocatoria;

            observacionPendiente.Descripcion = string.IsNullOrEmpty(vObservacionesPendientes.Descripcion) ? "" : vObservacionesPendientes.Descripcion;
            observacionPendiente.MotivoDevolucionDelContratista = "";

            var vDatosEvaluador = vBancoOferentesService.ConsultarEvaluador(vObservacionesPendientes.IdUsuario ?? 0);
            observacionPendiente.IdEvaluador = vDatosEvaluador.IdUsuario;
            observacionPendiente.NombreEvaluador = vDatosEvaluador.NombreEvaluador;

            observacionPendiente.IdEquipoEvaluador = vDatosEvaluador.IdEquipoEvaluador;

            var vDatosCoordinador = vBancoOferentesService.ConsultarEquipoEvaluador(vDatosEvaluador.IdEquipoEvaluador);
            observacionPendiente.IdCoordinador = vDatosCoordinador.IdUsuarioCoordinador;
            observacionPendiente.NombreCoordinador = vDatosCoordinador.NombreCompleto;
        }
        return vListaObservacionesPendientes;
    }
}