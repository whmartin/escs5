<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_EquiposEvaluadores_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>N&uacute;mero de identificaci&oacute;n
            </td>
            <td>Nombre Coordinador
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreCoordinador"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Nombre Equipo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreEquipo"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvEquiposEvaluadores" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" DataKeyNames="IdEquipoEvaluador" CellPadding="0" Height="16px"
                            OnSorting="gvEquiposEvaluadores_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvEquiposEvaluadores_PageIndexChanging" OnSelectedIndexChanged="gvEquiposEvaluadores_SelectedIndexChanged">
                            <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                             </asp:TemplateField>
                                <asp:BoundField HeaderText="N&uacute;mero documento" DataField="NumeroDocumento" SortExpression="NumeroDocumento" />
                                <asp:BoundField HeaderText="Nombre coordinador" DataField="NombreCompleto" SortExpression="NombreCompleto" />
                                <asp:BoundField HeaderText="Nombre equipo" DataField="NombreEquipoEvaluador" SortExpression="NombreEquipoEvaluador" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
</asp:Content>
