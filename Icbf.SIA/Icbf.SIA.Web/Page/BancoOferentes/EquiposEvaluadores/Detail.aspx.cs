using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Oferente.Entity;
using Icbf.Contrato.Service;
using Icbf.Oferente.Service;
using System.Web.Script.Serialization;
using System.Linq.Expressions;

/// <summary>
/// Página de registro y edición para la entidad EquiposEvaluadores
/// </summary>
public partial class Page_EquiposEvaluadores_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    string PageName = "BancoOferentes/EquiposEvaluadores";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatos();
            }
        }

        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
    }


    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("EquiposEvaluadores.IdEquipoEvaluador", hfIdEquipoEvaluador.Value);
        SetSessionParameter("EquiposEvaluadores.IdUsuarioCoordinador", hfIdUsuarioCoordinador.Value);
        SetSessionParameter("EquiposEvaluadores.NumDocCoordinador", hfNumDocCoordinador.Value);
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }


    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            gvEvaluadores.PageSize = PageSize();
            gvEvaluadores.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Equipos evaluadores", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("EquiposEvaluadores.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();

            if (GetSessionParameter("EquiposEvaluadores.Guardado").ToString() == "2")
                toolBar.MostrarMensajeModificado();

            RemoveSessionParameter("EquiposEvaluadores.Guardado");

            hfIdEquipoEvaluador.Value = GetSessionParameter("EquiposEvaluadores.IdEquipoEvaluador").ToString();
            RemoveSessionParameter("EquiposEvaluadores.IdEquipoEvaluador");

            EquipoEvaluadores vEquipoEvaluadores = vBancoOferentesService.ConsultarEquipoEvaluador(Convert.ToInt32(hfIdEquipoEvaluador.Value));

            txtNombreCoordinador.Text = vEquipoEvaluadores.NombreCompleto;
            txtNombreEquipo.Text = vEquipoEvaluadores.NombreEquipoEvaluador;

            hfIdUsuarioCoordinador.Value = Convert.ToString(vEquipoEvaluadores.IdUsuarioCoordinador);
            hfNumDocCoordinador.Value = vEquipoEvaluadores.NumeroDocumento;

            CargarGrilla(gvEvaluadores, GridViewSortExpression, true);
            CargarGrillaConvocatorias(gvConvocatorias, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvEvaluadores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEvaluadores.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvEvaluadores_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
           var myGridResults = vBancoOferentesService.ConsultarEvaluadoress(Convert.ToInt32(hfIdEquipoEvaluador.Value), true);

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }

            if (expresionOrdenamiento != null)
            {
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Oferente.Entity.Evaluador), expresionOrdenamiento);
                    var prop = Expression.Property(param, expresionOrdenamiento);
                    var sortExpression = Expression.Lambda<Func<Icbf.Oferente.Entity.Evaluador, object>>(Expression.Convert(prop, typeof(object)), param);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaConvocatorias(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            var vListaConvocatorias = new List<Convocatoria>();
            var vListaConvocatoriasEquipos = vBancoOferentesService.ConsultarConvocatoriassEquiposEvaluadores(Convert.ToInt32(hfIdEquipoEvaluador.Value));
            
            if (vListaConvocatoriasEquipos.Count > 0)
            {
                foreach (var convocatoriaEquipo in vListaConvocatoriasEquipos)
                {
                    var vConvocatoria = new Convocatoria();
                    vConvocatoria = vBancoOferentesService.ConsultarConvocatoriaPorId(convocatoriaEquipo.IdConvocatoria);
                    if(vConvocatoria != null)
                        vListaConvocatorias.Add(vConvocatoria);
                }
            }
            
            var myGridResults = vListaConvocatorias;

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }

            if (expresionOrdenamiento != null)
            {
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Oferente.Entity.Convocatoria), expresionOrdenamiento);
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    var sortExpression = Expression.Lambda<Func<Icbf.Oferente.Entity.Convocatoria, object>>(Expression.Convert(prop, typeof(object)), param);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }
                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }
            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvConvocatorias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaConvocatorias((GridView)sender, e.SortExpression, false);
    }
    protected void gvConvocatorias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConvocatorias.PageIndex = e.NewPageIndex;
        CargarGrillaConvocatorias((GridView)sender, GridViewSortExpression, true);
    }
}
