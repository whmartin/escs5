<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_EquiposEvaluadores_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdUsuarioCoordinador" runat="server" />
    <asp:HiddenField ID="hfIdUsuarioCoordinadorVerif" runat="server" />
    <asp:HiddenField ID="hfNumDocCoordinador" runat="server" />
    <asp:HiddenField ID="hfNumDocCoordinadorVerif" runat="server" />

    <asp:HiddenField ID="hfIdUsuarioEvaluador" runat="server" />
    <asp:HiddenField ID="hfIdUsuarioEvaluadorVerif" runat="server" />
    <asp:HiddenField ID="hfNumDocEvaluador" runat="server" />
    <asp:HiddenField ID="hfNumDocEvaluadorVerif" runat="server" />

    <asp:HiddenField ID="hfPostback" runat="server" />
    <asp:HiddenField ID="hfListaEvaluadores" runat="server" />
    <asp:HiddenField ID="hfIdEquipoEvaluador" runat="server" />

    <asp:HiddenField ID="hfIdConvocatoria" runat="server" />
    <asp:HiddenField ID="hfIdConvocatoriaVerif" runat="server" />
    <asp:HiddenField ID="hfNombreConvocatoria" runat="server" />
    <asp:HiddenField ID="hfNombreConvocatoriaVerif" runat="server" />
    <asp:HiddenField ID="hfNumeroConvocatoria" runat="server" />
    <asp:HiddenField ID="hfNumeroConvocatoriaVerif" runat="server" />
    <asp:HiddenField ID="hfNombreRegional" runat="server" />
    <asp:HiddenField ID="hfNombreRegionalVerif" runat="server" />
    <asp:HiddenField ID="hfListaConvocatorias" runat="server" />
    <asp:HiddenField ID="hfListaConvocatoriasInicial" runat="server" />
    <asp:HiddenField ID="hfCoordinadorYaTieneObservaciones" runat="server" />
    
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Nombre Equipo
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreEquipo" ControlToValidate="txtNombreEquipo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Nombre Coordinador
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreCoordinador" ControlToValidate="txtNombreCoordinador"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreEquipo" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreCoordinador" Enabled="false" Width="90%"></asp:TextBox>
                <asp:ImageButton ID="imgNombreCoordinador" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClientClick="GetCoordinador(); return false;" Style="cursor: hand" ToolTip="Buscar" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="tabsConvocatorias" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 20px;">
                    <table width="90%" align="center">
                        <tr class="rowA">
                            <td>
                                <span style="color:#666666;font-size:16px;">Lista Convocatorias</span>
                            </td>
                            <td>
                                <asp:ImageButton ID="AddConvocatoriaImageButton" runat="server" AlternateText="Nuevo" 
                                    OnClick="AddConvocatoria"
                                    Height="29px" ImageUrl="../../../Image/btn/add.gif" Width="29px" Style="float: right;" />
                            </td>
                        </tr>

                        <tr class="rowB">
                            <td>
                                Nombre convocatoria
                            </td>
                            <td>
                                N&uacute;mero convocatoria
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtNombreConvocatoria" Enabled="false" Width="90%"></asp:TextBox>
                                <asp:ImageButton ID="imgConvocatoria" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                    OnClientClick="GetConvocatoria(); return false;" Style="cursor: hand" ToolTip="Buscar" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtNumeroConvocatoria" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>

                    <asp:Panel runat="server" ID="Panel1">
                        <table width="90%" align="center">
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="gvConvocatorias" AutoGenerateColumns="False" AllowPaging="True"
                                        GridLines="None" Width="100%" DataKeyNames="IdConvocatoria,Nombre" CellPadding="0" Height="16px"
                                        OnSorting="gvConvocatorias_Sorting" AllowSorting="True"
                                        OnPageIndexChanging="gvConvocatorias_PageIndexChanging" OnSelectedIndexChanged="gvConvocatorias_SelectedIndexChanged">
                                        <Columns>
                                            <asp:BoundField HeaderText="Nombre" DataField="Nombre" SortExpression="Nombre" />
                                            <asp:BoundField HeaderText="N&uacute;mero" DataField="Numero" SortExpression="Numero" />
                                            <asp:BoundField HeaderText="Nombre Regional" DataField="DescripcionRegional" SortExpression="DescripcionRegional" />
                                            <asp:TemplateField HeaderText="Eliminar">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="Select" ImageUrl="~/Image/btn/delete.gif" Height="16px" Width="16px" ToolTip="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </div>
            </td>

        </tr>

        <tr>
            <td colspan="2">
                <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 20px;">
                    <table width="90%" align="center">
                        <tr class="rowA">
                            <td>
                                <span style="color:#666666;font-size:16px;">Lista Evaluadores</span>
                            </td>
                            <td>
                                <asp:ImageButton ID="AddImageButton" runat="server" AlternateText="Nuevo" OnClick="AddEvaluador"
                                    Height="29px" ImageUrl="../../../Image/btn/add.gif" Width="29px" Style="float: right;" />
                            </td>
                        </tr>

                        <tr class="rowB">
                            <td>Evaluador
                            </td>
                            <td>Tipo Evaluador
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtNombreEvaluador" Enabled="false" Width="90%"></asp:TextBox>
                                <asp:ImageButton ID="imgEvaluador" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                    OnClientClick="GetEvaluador(); return false;" Style="cursor: hand" ToolTip="Buscar" />
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlTipoEvaluador"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>

                    <asp:Panel runat="server" ID="pnlLista">
                        <table width="90%" align="center">
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="gvEvaluadoresAdicionados" AutoGenerateColumns="False" AllowPaging="True"
                                        GridLines="None" Width="100%" DataKeyNames="IdUsuario" CellPadding="0" Height="16px"
                                        OnSorting="gvEvaluadoresAdicionados_Sorting" AllowSorting="True"
                                        OnPageIndexChanging="gvEvaluadoresAdicionados_PageIndexChanging" OnSelectedIndexChanged="gvEvaluadoresAdicionados_SelectedIndexChanged">
                                        <Columns>
                                            <asp:BoundField HeaderText="Nombre Evaluador" DataField="NombreEvaluador" SortExpression="NombreEvaluador" />
                                            <asp:BoundField HeaderText="Tipo Evaluador" DataField="DescTipoObservacion" SortExpression="DescTipoObservacion" />
                                            <asp:TemplateField HeaderText="Eliminar">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="Select" ImageUrl="~/Image/btn/delete.gif" Height="16px" Width="16px" ToolTip="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </div>
            </td>
        </tr>
    </table>

    <script type="text/javascript" language="javascript">

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function GetCoordinador() {
            muestraImagenLoading();
            window_showModalDialog('../../../Page/BancoOferentes/LupaCoordinador/LupaCoordinador.aspx', setReturnGetCoordinador, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetCoordinador(dialog) {

            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");

                if (retLupa.length > 0) {
                    document.getElementById('<%= hfIdUsuarioCoordinador.ClientID %>').value = retLupa[0];
                    document.getElementById('<%= hfNumDocCoordinador.ClientID %>').value = retLupa[1];
                    prePostbck(false);
                    __doPostBack('<%= hfIdUsuarioCoordinador.ClientID %>', '');
                    __doPostBack('<%= hfNumDocCoordinador.ClientID %>', '');
                }
                else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }

            <%--  prePostbck(false);
                __doPostBack('<%= txtIdProveedor.ClientID %>', '');--%>
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostback.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }



        function GetEvaluador() {
            muestraImagenLoading();
            window_showModalDialog('../../../Page/BancoOferentes/LupaEvaluador/LupaEvaluador.aspx', setReturnGetEvaluador, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetEvaluador(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 0) {
                    document.getElementById('<%= hfIdUsuarioEvaluador.ClientID %>').value = retLupa[0];
                    document.getElementById('<%= hfNumDocEvaluador.ClientID %>').value = retLupa[1];
                    prePostbck(false);
                    __doPostBack('<%= hfIdUsuarioEvaluador.ClientID %>', '');
                    __doPostBack('<%= hfNumDocEvaluador.ClientID %>', '');
                } else {
                    ocultaImagenLoading();
                }
            }
            else {
                ocultaImagenLoading();
            }

          <%--  prePostbck(false);
            __doPostBack('<%= txtIdProveedor.ClientID %>', '');--%>
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostback.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }


         function GetConvocatoria() {
            muestraImagenLoading();
            window_showModalDialog('../../../Page/BancoOferentes/LupaConvocatoria/LupaConvocatoria.aspx', setReturnGetConvocatoria, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetConvocatoria(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 0) {
                    document.getElementById('<%= hfIdConvocatoria.ClientID %>').value = retLupa[0];
                    document.getElementById('<%= hfNombreConvocatoria.ClientID %>').value = retLupa[1];
                    document.getElementById('<%= hfNumeroConvocatoria.ClientID %>').value = retLupa[2];
                    document.getElementById('<%= hfNombreRegional.ClientID %>').value = retLupa[3];
                    prePostbck(false);
                    __doPostBack('<%= hfIdConvocatoria.ClientID %>', '');
                    __doPostBack('<%= hfNombreConvocatoria.ClientID %>', '');
                    __doPostBack('<%= hfNumeroConvocatoria.ClientID %>', '');
                    __doPostBack('<%= hfNombreRegional.ClientID %>', '');
                } else {
                    ocultaImagenLoading();
                }
            }
            else {
                ocultaImagenLoading();
            }

          <%--  prePostbck(false);
            __doPostBack('<%= txtIdProveedor.ClientID %>', '');--%>
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostback.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }

        function Confirmacion() {
            return confirm("Ya tiene convocatorias agregadas. Esta seguro de agregar una nueva?");
        }

       
    </script>

</asp:Content>
