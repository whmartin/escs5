<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_EquiposEvaluadores_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdUsuarioCoordinador" runat="server" />
    <asp:HiddenField ID="hfNumDocCoordinador" runat="server" />
    <asp:HiddenField ID="hfIdEquipoEvaluador" runat="server" />

    <table width="90%" align="center">
        <tr class="rowB">
            <td>Nombre Equipo
            </td>
            <td>Nombre Coordinador
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreEquipo" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreCoordinador" Enabled="false" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="tabsConvocatorias" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 20px;">
                    <table width="90%" align="center">
                        <tr class="rowA">
                            <td>
                                <span style="color:#666666;font-size:16px;">Lista Convocatorias</span>
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    <asp:Panel runat="server" ID="Panel1">
                        <table width="90%" align="center">
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="gvConvocatorias" AutoGenerateColumns="False" AllowPaging="True"
                                        GridLines="None" Width="100%" DataKeyNames="IdConvocatoria,Nombre" CellPadding="0" Height="16px"
                                        OnSorting="gvConvocatorias_Sorting" AllowSorting="True" OnPageIndexChanging="gvConvocatorias_PageIndexChanging" >
                                        <Columns>
                                            <asp:BoundField HeaderText="Nombre" DataField="Nombre" SortExpression="Nombre" />
                                            <asp:BoundField HeaderText="N&uacute;mero" DataField="Numero" SortExpression="Numero" />
                                            <asp:BoundField HeaderText="Nombre Regional" DataField="DescripcionRegional" SortExpression="DescripcionRegional" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </div>
            </td>

        </tr>
        <tr>
            <td colspan="2">
                <div id="tabsEvaluadores" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="margin-top: 20px;">
                    <table width="90%" align="center">
                        <tr class="rowA">
                            <td>
                                <span style="color:#666666;font-size:16px;">Lista Evaluadores</span>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                        <asp:Panel runat="server" ID="pnlLista">
                            <table width="90%" align="center">
                                <tr class="rowAG">
                                    <td>
                                        <asp:GridView runat="server" ID="gvEvaluadores" AutoGenerateColumns="False" AllowPaging="True"
                                            GridLines="None" Width="100%" DataKeyNames="IdEquipoEvaluador" CellPadding="0" Height="16px"
                                            OnSorting="gvEvaluadores_Sorting" AllowSorting="True" OnPageIndexChanging="gvEvaluadores_PageIndexChanging" >
                                            <Columns>
                                                <asp:BoundField HeaderText="N&uacute;mero documento" DataField="NumeroDocumento" SortExpression="NumeroDocumento" />
                                                <asp:BoundField HeaderText="Nombre evaluador" DataField="NombreEvaluador" SortExpression="NombreEvaluador" />
                                                <asp:BoundField HeaderText="&aacute;rea" DataField="NombreArea" SortExpression="NombreArea" />
                                                <asp:BoundField HeaderText="Tipo evaluaci&oacute;n" DataField="DescTipoObservacion" SortExpression="DescTipoObservacion" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="rowBG" />
                                            <EmptyDataRowStyle CssClass="headerForm" />
                                            <HeaderStyle CssClass="headerForm" />
                                            <RowStyle CssClass="rowAG" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                </div>
            </td>
        </tr>
</asp:Content>

