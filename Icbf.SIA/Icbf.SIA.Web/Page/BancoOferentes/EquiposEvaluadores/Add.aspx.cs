using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Oferente.Entity;
using Icbf.Contrato.Service;
using Icbf.Oferente.Service;
using System.Web.Script.Serialization;
using System.Linq.Expressions;

/// <summary>
/// Página de registro y edición para la entidad EquiposEvaluadores
/// </summary>
public partial class Page_EquiposEvaluadores_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();

    public List<Evaluador> vListaEvaluadores;
    public List<Convocatoria> vListaConvocatoria;

    string PageName = "BancoOferentes/EquiposEvaluadores";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        toolBar.LipiarMensajeError();
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oP"] == "E")
                {
                    hfIdEquipoEvaluador.Value = GetSessionParameter("EquiposEvaluadores.IdEquipoEvaluador").ToString();
                    RemoveSessionParameter("EquiposEvaluadores.IdEquipoEvaluador");

                    hfIdUsuarioCoordinador.Value = GetSessionParameter("EquiposEvaluadores.IdUsuarioCoordinador").ToString();
                    RemoveSessionParameter("EquiposEvaluadores.IdUsuarioCoordinador");

                    hfIdUsuarioCoordinadorVerif.Value = hfIdUsuarioCoordinador.Value;

                    hfNumDocCoordinador.Value = GetSessionParameter("EquiposEvaluadores.NumDocCoordinador").ToString();
                    RemoveSessionParameter("EquiposEvaluadores.NumDocCoordinador");

                    CargarDatosEdicionEquipo();
                    CargarDatosEdicionConvocatorias();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(hfIdUsuarioCoordinador.Value))
                {
                    if (Request.QueryString["oP"] == "E")
                    {
                        if (hfIdUsuarioCoordinador.Value != hfIdUsuarioCoordinadorVerif.Value)
                        {
                            var vListaObservacionesCoordinador = vBancoOferentesService.ConsultarObservacionessConvocatoria(Convert.ToInt32(hfIdUsuarioCoordinadorVerif.Value));

                            if (vListaObservacionesCoordinador.Count() > 0)
                            {
                                foreach (var observacionCoordinador in vListaObservacionesCoordinador)
                                {
                                    if (observacionCoordinador.IdEstadoObservacion == 2 || observacionCoordinador.IdEstadoObservacion == 3)
                                    {
                                        hfCoordinadorYaTieneObservaciones.Value = "Si";
                                        break;
                                    }
                                }
                            }
                            if (hfCoordinadorYaTieneObservaciones.Value == "Si")
                            {
                                hfIdUsuarioCoordinador.Value = hfIdUsuarioCoordinadorVerif.Value;
                                hfNumDocCoordinador.Value = hfNumDocCoordinadorVerif.Value;
                            }
                            else
                            {
                                hfIdUsuarioCoordinadorVerif.Value = hfIdUsuarioCoordinador.Value;
                                hfNumDocCoordinadorVerif.Value = hfNumDocCoordinador.Value;
                            }
                        }
                        else
                        {
                            hfIdUsuarioCoordinadorVerif.Value = hfIdUsuarioCoordinador.Value;
                            hfNumDocCoordinadorVerif.Value = hfNumDocCoordinador.Value;
                        }
                    }
                    else
                    {
                        hfIdUsuarioCoordinadorVerif.Value = hfIdUsuarioCoordinador.Value;
                        hfNumDocCoordinadorVerif.Value = hfNumDocCoordinador.Value;
                    }
                    CargarRegistroCoordinador();
                }
                else
                {
                    hfIdUsuarioCoordinador.Value = hfIdUsuarioCoordinadorVerif.Value;
                    hfNumDocCoordinador.Value = hfNumDocCoordinadorVerif.Value;
                }

                if (!string.IsNullOrEmpty(hfIdUsuarioEvaluador.Value))
                {
                    hfIdUsuarioEvaluadorVerif.Value = hfIdUsuarioEvaluador.Value;
                    hfNumDocEvaluadorVerif.Value = hfNumDocEvaluador.Value;
                    CargarRegistroEvaluador();
                }
                else
                {
                    hfIdUsuarioEvaluador.Value = hfIdUsuarioEvaluadorVerif.Value;
                    hfNumDocEvaluador.Value = hfNumDocEvaluadorVerif.Value;
                }

                if (!string.IsNullOrEmpty(hfIdConvocatoria.Value))
                {
                    hfIdConvocatoriaVerif.Value = hfIdConvocatoria.Value;
                    hfNombreConvocatoriaVerif.Value = hfNombreConvocatoria.Value;
                    hfNumeroConvocatoriaVerif.Value = hfNumeroConvocatoria.Value;
                    hfNombreRegionalVerif.Value = hfNombreRegional.Value;
                    CargarRegistroConvocatoria();
                }
                else
                {
                    hfIdConvocatoria.Value = hfIdConvocatoriaVerif.Value;
                    hfNombreConvocatoria.Value = hfNombreConvocatoriaVerif.Value;
                    hfNumeroConvocatoria.Value = hfNumeroConvocatoriaVerif.Value;
                    hfNombreRegional.Value = hfNombreRegionalVerif.Value;
                }
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void AddEvaluador(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtNombreEvaluador.Text))
                throw new Exception("Debe seleccionar un evaluador");

            if (ddlTipoEvaluador.SelectedItem.Value == "-1")
                throw new Exception("Debe seleccionar un tipo evaluador");

            if (hfIdUsuarioEvaluador.Value == hfIdUsuarioCoordinador.Value || hfNumDocEvaluador.Value == hfNumDocCoordinador.Value)
                throw new Exception("El coordinador no puede ser un evaluador");
            
            var vjss = new JavaScriptSerializer();

            vListaEvaluadores = new List<Evaluador>();
            if (!string.IsNullOrEmpty(hfListaEvaluadores.Value))
            {
                vListaEvaluadores = vjss.Deserialize<List<Evaluador>>(hfListaEvaluadores.Value);
                for (int i = 0; i < vListaEvaluadores.Count; i++)
                {
                    if (vListaEvaluadores[i].IdUsuario == Convert.ToInt32(hfIdUsuarioEvaluador.Value) || vListaEvaluadores[i].NumeroDocumento == hfNumDocEvaluador.Value)
                        throw new Exception("El usuario ya está agregado");
                }
            }

            Evaluador vEvaluador = new Evaluador();

            vEvaluador.IdUsuario = Convert.ToInt32(hfIdUsuarioEvaluador.Value);
            vEvaluador.NombreEvaluador = txtNombreEvaluador.Text;
            vEvaluador.IdTipoObservacion = Convert.ToInt32(ddlTipoEvaluador.SelectedItem.Value);
            vEvaluador.DescTipoObservacion = ddlTipoEvaluador.SelectedItem.Text;
            vEvaluador.NumeroDocumento = hfNumDocEvaluador.Value;

            vListaEvaluadores.Add(vEvaluador);
            hfListaEvaluadores.Value = vjss.Serialize(vListaEvaluadores);


            txtNombreEvaluador.Text = "";
            ddlTipoEvaluador.SelectedValue = "-1";

            CargarGrilla(gvEvaluadoresAdicionados, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void AddConvocatoria(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(txtNombreConvocatoria.Text))
                throw new Exception("Debe seleccionar una convocatoria");

            var vjss = new JavaScriptSerializer();

            vListaConvocatoria = new List<Convocatoria>();
            if (!string.IsNullOrEmpty(hfListaConvocatorias.Value))
            {
                vListaConvocatoria = vjss.Deserialize<List<Convocatoria>>(hfListaConvocatorias.Value);
                for (int i = 0; i < vListaConvocatoria.Count; i++)
                {
                    if (vListaConvocatoria[i].Numero == Convert.ToInt32(hfNumeroConvocatoria.Value))
                    {
                        throw new Exception("La convocatoria ya se agregó antes");
                    }
                }
            }

            Convocatoria vConvocatoria = new Convocatoria();

            vConvocatoria.IdConvocatoria = Convert.ToInt32(hfIdConvocatoria.Value);
            vConvocatoria.Nombre = hfNombreConvocatoria.Value;
            vConvocatoria.Numero = Convert.ToInt32(hfNumeroConvocatoria.Value);
            vConvocatoria.DescripcionRegional = hfNombreRegional.Value;

            vListaConvocatoria.Add(vConvocatoria);
            hfListaConvocatorias.Value = vjss.Serialize(vListaConvocatoria);

            if (!string.IsNullOrEmpty(hfListaConvocatorias.Value))
            {
                if (vListaConvocatoria.Count == 0)
                    AddConvocatoriaImageButton.Attributes["onclick"] = "";
                if (vListaConvocatoria.Count > 0)
                    AddConvocatoriaImageButton.Attributes["onclick"] = "javascript:return Confirmacion();";
            }

            CargarGrillaConvocatorias(gvConvocatorias, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad ProyeccionPresupuestos
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (string.IsNullOrEmpty(hfListaEvaluadores.Value))
                throw new Exception("No ha agregado ningún evaluador");

            if (!string.IsNullOrEmpty(hfListaConvocatorias.Value))
            {
                vListaConvocatoria = new JavaScriptSerializer().Deserialize<List<Convocatoria>>(hfListaConvocatorias.Value);
                if(vListaConvocatoria.Count == 0)
                    throw new Exception("No ha agregado ninguna convocatoria");
            }
            else
            {
                throw new Exception("No ha agregado ninguna convocatoria");
            }
                
            vListaEvaluadores = new JavaScriptSerializer().Deserialize<List<Evaluador>>(hfListaEvaluadores.Value);

            var vListaTiposObservaciones = vBancoOferentesService.ConsultarTiposObservacioness(true);

            foreach (var tipoObservacion in vListaTiposObservaciones)
            {
                var tipoObservacionQuery = from evaluador in vListaEvaluadores
                                           where tipoObservacion.DescripcionTipo == evaluador.DescTipoObservacion
                                           select evaluador;

                if (!tipoObservacionQuery.Any())
                    throw new Exception("Debe agregar mínimo un evaluador de tipo " + tipoObservacion.DescripcionTipo);
            }

            var vListaUsuarioCoorRepetido = vSIAService.ConsultarUsuarioss(hfNumDocCoordinador.Value, txtNombreCoordinador.Text);

            foreach (var coorRepetido in vListaUsuarioCoorRepetido)
            {
                Evaluador vEvaluadorPorIdUsuarioCoor = vBancoOferentesService.ConsultarEvaluador(coorRepetido.IdUsuario);
                if (vEvaluadorPorIdUsuarioCoor.IdUsuario != 0 && vEvaluadorPorIdUsuarioCoor.Activo == true)
                    throw new Exception("El usuario " + coorRepetido.NombreCompleto + " ya se encuentra registrado como evaluador en otro equipo");
            }
            int vResultado;

            if (Request.QueryString["oP"] == "E")
            {
                foreach (var evaluador in vListaEvaluadores)
                {
                    var vListaUsuarioRepetido = vSIAService.ConsultarUsuarioss(evaluador.NumeroDocumento, evaluador.NombreEvaluador);

                    foreach (var usuarioRepetido in vListaUsuarioRepetido)
                    {
                        Evaluador vEvaluadorPorIdUsuario = vBancoOferentesService.ConsultarEvaluador(usuarioRepetido.IdUsuario);

                        if (vEvaluadorPorIdUsuario.IdUsuario != 0 && vEvaluadorPorIdUsuario.IdEquipoEvaluador != evaluador.IdEquipoEvaluador && vEvaluadorPorIdUsuario.Activo == true)
                            throw new Exception("El usuario " + usuarioRepetido.NombreCompleto + " ya se encuentra registrado como evaluador en otro equipo");

                        var vListaCoordPorIdUsuario = vBancoOferentesService.ConsultarEquiposEvaluadoress(usuarioRepetido.IdUsuario);
                        if (vListaCoordPorIdUsuario.Exists(x => x.IdUsuarioCoordinador == usuarioRepetido.IdUsuario))
                            throw new Exception("El usuario " + usuarioRepetido.NombreCompleto + " ya se encuentra registrado como coordinador en otro equipo");
                    }
                }

                EquipoEvaluadores vEquipoEvaluadores = new EquipoEvaluadores();
                vEquipoEvaluadores.IdEquipoEvaluador = Convert.ToInt32(hfIdEquipoEvaluador.Value);
                vEquipoEvaluadores.IdUsuarioCoordinador = Convert.ToInt32(hfIdUsuarioCoordinador.Value);
                vEquipoEvaluadores.NombreEquipoEvaluador = txtNombreEquipo.Text;
                vEquipoEvaluadores.UsuarioModifica = GetSessionUser().NombreUsuario;

                InformacionAudioria(vEquipoEvaluadores, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.ModificarEquipoEvaluador(vEquipoEvaluadores);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    var vListaEvaluadoresActivos = vBancoOferentesService.ConsultarEvaluadoress(Convert.ToInt32(vEquipoEvaluadores.IdEquipoEvaluador), true);

                    foreach (var evaluadorActivo in vListaEvaluadoresActivos)
                    {
                        var evaluadorQuery = from v in vListaEvaluadores
                                             where evaluadorActivo.NumeroDocumento == v.NumeroDocumento
                                             select v;

                        if (!evaluadorQuery.Any())
                        {
                            Evaluador vEvaluadorMod = new Evaluador();
                            vEvaluadorMod.IdEvaluador = evaluadorActivo.IdEvaluador;
                            vEvaluadorMod.IdUsuario = evaluadorActivo.IdUsuario;
                            vEvaluadorMod.IdEquipoEvaluador = vEquipoEvaluadores.IdEquipoEvaluador;
                            vEvaluadorMod.IdTipoObservacion = evaluadorActivo.IdTipoObservacion;
                            vEvaluadorMod.Activo = false;
                            vEvaluadorMod.UsuarioModifica = GetSessionUser().NombreUsuario;
                            InformacionAudioria(vEvaluadorMod, this.PageName, vSolutionPage);
                            vResultado = vBancoOferentesService.ModificarEvaluador(vEvaluadorMod);
                        }
                    }

                    foreach (var evaluador in vListaEvaluadores)
                    {
                        var vListaUsuarioRepetido = vSIAService.ConsultarUsuarioss(evaluador.NumeroDocumento, evaluador.NombreEvaluador);

                        foreach (var usuarioRepetido in vListaUsuarioRepetido)
                        {
                            Evaluador vEvaluadorPorIdUsuario = vBancoOferentesService.ConsultarEvaluador(usuarioRepetido.IdUsuario);

                            Evaluador vEvaluadorMod = new Evaluador();

                            vEvaluadorMod.IdUsuario = evaluador.IdUsuario;
                            vEvaluadorMod.IdEquipoEvaluador = vEquipoEvaluadores.IdEquipoEvaluador;
                            vEvaluadorMod.IdTipoObservacion = evaluador.IdTipoObservacion;
                            vEvaluadorMod.Activo = true;

                            if (vEvaluadorPorIdUsuario.IdUsuario == 0)
                            {
                                if (!ExisteUsuarioEnEvaluadores(evaluador.NumeroDocumento))
                                {
                                    vEvaluadorMod.UsuarioCrea = GetSessionUser().NombreUsuario;
                                    InformacionAudioria(vEvaluadorMod, this.PageName, vSolutionPage);
                                    vResultado = vBancoOferentesService.InsertarEvaluador(vEvaluadorMod);
                                }
                            }
                            else
                            {
                                vEvaluadorMod.IdEvaluador = vEvaluadorPorIdUsuario.IdEvaluador;
                                vEvaluadorMod.UsuarioModifica = GetSessionUser().NombreUsuario;
                                InformacionAudioria(vEvaluadorMod, this.PageName, vSolutionPage);
                                vResultado = vBancoOferentesService.ModificarEvaluador(vEvaluadorMod);
                            }
                        }
                    }
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        vListaConvocatoria = new JavaScriptSerializer().Deserialize<List<Convocatoria>>(hfListaConvocatorias.Value);
                        var vListaConvocatoriaInicial = new JavaScriptSerializer().Deserialize<List<Convocatoria>>(hfListaConvocatoriasInicial.Value);

                        var convParaEliminar = (
                                                    from p in vListaConvocatoriaInicial
                                                    where !(
                                                                from ex in vListaConvocatoria
                                                                select ex.IdConvocatoria
                                                            ).Contains(p.IdConvocatoria)
                                                    select p
                                                ).ToList();

                        if (convParaEliminar.Count > 0)
                        {
                            ConvocatoriasEquiposEvaluadores vConvocatoriasEquiposEvaluadores = new ConvocatoriasEquiposEvaluadores();

                            vConvocatoriasEquiposEvaluadores.IdEquipoEvaluador = vEquipoEvaluadores.IdEquipoEvaluador;
                            foreach (var convocatoria in convParaEliminar)
                            {
                                vConvocatoriasEquiposEvaluadores.IdConvocatoria = convocatoria.IdConvocatoria;
                                InformacionAudioria(vConvocatoriasEquiposEvaluadores, this.PageName, vSolutionPage);
                                vResultado = vBancoOferentesService.EliminarConvocatoriasEquiposEvaluadores(vConvocatoriasEquiposEvaluadores);
                            }
                            if (vResultado == 0)
                            {
                                toolBar.MostrarMensajeError("Las convocatorias no pudieron ser insertadas, verifique por favor.");
                            }
                        }

                        var vListaConvParaInsertar = (
                                                    from p in vListaConvocatoria
                                                    where !(
                                                                from ex in vListaConvocatoriaInicial
                                                                select ex.IdConvocatoria
                                                            ).Contains(p.IdConvocatoria)
                                                    select p
                                                ).ToList();
                        if (vListaConvParaInsertar.Count > 0)
                        {
                            ConvocatoriasEquiposEvaluadores vConvocatoriasEquiposEvaluadores = new ConvocatoriasEquiposEvaluadores();

                            vConvocatoriasEquiposEvaluadores.IdEquipoEvaluador = vEquipoEvaluadores.IdEquipoEvaluador;
                            vConvocatoriasEquiposEvaluadores.UsuarioCrea = GetSessionUser().NombreUsuario;

                            foreach (var convocatoria in vListaConvParaInsertar)
                            {
                                vConvocatoriasEquiposEvaluadores.IdConvocatoria = convocatoria.IdConvocatoria;
                                InformacionAudioria(vConvocatoriasEquiposEvaluadores, this.PageName, vSolutionPage);
                                vResultado = vBancoOferentesService.InsertarConvocatoriasEquiposEvaluadores(vConvocatoriasEquiposEvaluadores);
                            }
                            if (vResultado == 0)
                            {
                                toolBar.MostrarMensajeError("Las convocatorias no pudieron ser insertadas, verifique por favor.");
                            }
                        }
                        if (vResultado == 1)
                        {
                            SetSessionParameter("EquiposEvaluadores.Guardado", "2");
                            SetSessionParameter("EquiposEvaluadores.IdEquipoEvaluador", vEquipoEvaluadores.IdEquipoEvaluador);
                            NavigateTo(SolutionPage.Detail);
                        }
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                foreach (var evaluador in vListaEvaluadores)
                {
                    var vListaUsuarioRepetido = vSIAService.ConsultarUsuarioss(evaluador.NumeroDocumento, evaluador.NombreEvaluador);

                    foreach (var usuarioRepetido in vListaUsuarioRepetido)
                    {
                        Evaluador vEvaluadorPorIdUsuario = vBancoOferentesService.ConsultarEvaluador(usuarioRepetido.IdUsuario);

                        if (vEvaluadorPorIdUsuario.IdUsuario != 0 && vEvaluadorPorIdUsuario.Activo == true)
                            throw new Exception("El usuario " + usuarioRepetido.NombreCompleto + " ya se encuentra registrado como evaluador en otro equipo");

                        var vListaCoordPorIdUsuario = vBancoOferentesService.ConsultarEquiposEvaluadoress(usuarioRepetido.IdUsuario);
                        if (vListaCoordPorIdUsuario.Exists(x => x.IdUsuarioCoordinador == usuarioRepetido.IdUsuario))
                            throw new Exception("El usuario " + usuarioRepetido.NombreCompleto + " ya se encuentra registrado como coordinador en otro equipo");
                    }
                }

                EquipoEvaluadores vEquipoEvaluadores = new EquipoEvaluadores();

                vEquipoEvaluadores.NombreEquipoEvaluador = txtNombreEquipo.Text;
                vEquipoEvaluadores.IdUsuarioCoordinador = Convert.ToInt32(hfIdUsuarioCoordinador.Value);
                vEquipoEvaluadores.Activo = true;
                vEquipoEvaluadores.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vEquipoEvaluadores, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarEquipoEvaluadores(vEquipoEvaluadores);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("El equipo evaluador no se pudo insertar satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    foreach (var evaluador in vListaEvaluadores)
                    {
                        Evaluador vEvaluadorPorIdUsuario = vBancoOferentesService.ConsultarEvaluador(evaluador.IdUsuario);

                        Evaluador vEvaluador = new Evaluador();
                        vEvaluador.IdEvaluador = vEvaluadorPorIdUsuario.IdEvaluador;
                        vEvaluador.IdUsuario = evaluador.IdUsuario;
                        vEvaluador.IdEquipoEvaluador = vEquipoEvaluadores.IdEquipoEvaluador;
                        vEvaluador.IdTipoObservacion = evaluador.IdTipoObservacion;
                        vEvaluador.Activo = true;

                        if (vEvaluadorPorIdUsuario.IdUsuario != 0 && vEvaluadorPorIdUsuario.Activo == false)
                        {
                            vEvaluador.UsuarioModifica = GetSessionUser().NombreUsuario;
                            InformacionAudioria(vEvaluador, this.PageName, vSolutionPage);
                            vResultado = vBancoOferentesService.ModificarEvaluador(vEvaluador);
                        }
                        else
                        {
                            vEvaluador.UsuarioCrea = GetSessionUser().NombreUsuario;
                            InformacionAudioria(vEvaluador, this.PageName, vSolutionPage);
                            vResultado = vBancoOferentesService.InsertarEvaluador(vEvaluador);
                        }

                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La lista de evaluadores no se pudo insertar satisfactoriamente, verifique por favor.");
                        }
                    }
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La lista de evaluadores no se pudo insertar satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        vListaConvocatoria = new JavaScriptSerializer().Deserialize<List<Convocatoria>>(hfListaConvocatorias.Value);
                        ConvocatoriasEquiposEvaluadores vConvocatoriasEquiposEvaluadores = new ConvocatoriasEquiposEvaluadores();

                        vConvocatoriasEquiposEvaluadores.IdEquipoEvaluador = vEquipoEvaluadores.IdEquipoEvaluador;
                        vConvocatoriasEquiposEvaluadores.UsuarioCrea = GetSessionUser().NombreUsuario;

                        foreach (var convocatoria in vListaConvocatoria)
                        {
                            vConvocatoriasEquiposEvaluadores.IdConvocatoria = convocatoria.IdConvocatoria;
                            InformacionAudioria(vConvocatoriasEquiposEvaluadores, this.PageName, vSolutionPage);
                            vResultado = vBancoOferentesService.InsertarConvocatoriasEquiposEvaluadores(vConvocatoriasEquiposEvaluadores);
                        }
                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("Las convocatorias no pudieron ser insertadas, verifique por favor.");
                        }
                        else if (vResultado == 1)
                        {
                            SetSessionParameter("EquiposEvaluadores.Guardado", "1");
                            SetSessionParameter("EquiposEvaluadores.IdEquipoEvaluador", vEquipoEvaluadores.IdEquipoEvaluador);
                            NavigateTo(SolutionPage.Detail);
                        }
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afecto más registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            if (ex.Message.Contains("UNIQUE"))
            {
                toolBar.MostrarMensajeError("Ya existe un equipo con ese nombre, seleccione otro nombre");
            }
            else
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private bool ExisteUsuarioEnEvaluadores(string pNumeroDocumento)
    {
        var vListaUsuarioRepetido = vSIAService.ConsultarUsuarioss(pNumeroDocumento, null);
        foreach (var usuarioRepetido in vListaUsuarioRepetido)
        {
            Evaluador vEvaluadorPorIdUsuario = vBancoOferentesService.ConsultarEvaluador(usuarioRepetido.IdUsuario);
            if (vEvaluadorPorIdUsuario.IdUsuario != 0)
                return true;
        }
        return false;
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.EstablecerTitulos("Equipos evaluadores", SolutionPage.Add.ToString());

            CargarDatosIniciales();
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro para el coordinador
    /// </summary>
    private void CargarRegistroCoordinador()
    {
        try
        {
            if(hfCoordinadorYaTieneObservaciones.Value == "Si")
                throw new Exception("No se puede cambiar el coordinador ya que tiene observaciones pendientes");

            if (!string.IsNullOrEmpty(hfListaEvaluadores.Value))
            {
                vListaEvaluadores = new JavaScriptSerializer().Deserialize<List<Evaluador>>(hfListaEvaluadores.Value);

                if (vListaEvaluadores.Count > 0)
                {
                    for (int i = 0; i < vListaEvaluadores.Count; i++)
                    {
                        if (vListaEvaluadores[i].IdUsuario == Convert.ToInt32(hfIdUsuarioCoordinador.Value) || vListaEvaluadores[i].NumeroDocumento == hfNumDocCoordinador.Value)
                            throw new Exception("El usuario ya está agregado como evaluador");
                    }
                }
            }

            Usuario vUsuario = new Usuario();
            vUsuario = vSIAService.ConsultarDatosUsuario(Convert.ToInt32(hfIdUsuarioCoordinador.Value));
            txtNombreCoordinador.Text = vUsuario.PrimerNombre + ' ' + vUsuario.SegundoNombre + ' ' + vUsuario.PrimerApellido + ' ' + vUsuario.SegundoApellido;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Método que carga los datos del registro para el evaluador
    /// </summary>
    private void CargarRegistroEvaluador()
    {
        try
        {
            Usuario vUsuario = new Usuario();

            vUsuario = vSIAService.ConsultarDatosUsuario(Convert.ToInt32(hfIdUsuarioEvaluador.Value));
            txtNombreEvaluador.Text = vUsuario.PrimerNombre + ' ' + vUsuario.SegundoNombre + ' ' + vUsuario.PrimerApellido + ' ' + vUsuario.SegundoApellido;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos de la convocatoria
    /// </summary>
    private void CargarRegistroConvocatoria()
    {
        try
        {
            txtNombreConvocatoria.Text = hfNombreConvocatoria.Value;
            txtNumeroConvocatoria.Text = hfNumeroConvocatoria.Value;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlTipoEvaluador.DataSource = vBancoOferentesService.ConsultarTiposObservacioness(true);
            ddlTipoEvaluador.DataTextField = "DescripcionTipo";
            ddlTipoEvaluador.DataValueField = "IdTipoObservacion";
            ddlTipoEvaluador.DataBind();
            ddlTipoEvaluador.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosEdicionEquipo()
    {
        try
        {
            EquipoEvaluadores vEquipoEvaluadores = vBancoOferentesService.ConsultarEquipoEvaluador(Convert.ToInt32(hfIdEquipoEvaluador.Value));
            txtNombreCoordinador.Text = vEquipoEvaluadores.NombreCompleto;
            txtNombreEquipo.Text = vEquipoEvaluadores.NombreEquipoEvaluador;

            vListaEvaluadores = vBancoOferentesService.ConsultarEvaluadoress(Convert.ToInt32(hfIdEquipoEvaluador.Value), true);
            hfListaEvaluadores.Value = new JavaScriptSerializer().Serialize(vListaEvaluadores);
            txtNombreEvaluador.Text = "";
            ddlTipoEvaluador.SelectedValue = "-1";

            CargarGrilla(gvEvaluadoresAdicionados, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosEdicionConvocatorias()
    {
        try
        {
            var vListaConvocatorias = new List<Convocatoria>();
            var vListaConvocatoriasEquipos = vBancoOferentesService.ConsultarConvocatoriassEquiposEvaluadores(Convert.ToInt32(hfIdEquipoEvaluador.Value));

            if (vListaConvocatoriasEquipos.Count > 0)
            {
                foreach (var convocatoriaEquipo in vListaConvocatoriasEquipos)
                {
                    var vConvocatoria = new Convocatoria();
                    vConvocatoria = vBancoOferentesService.ConsultarConvocatoriaPorId(convocatoriaEquipo.IdConvocatoria);
                    if (vConvocatoria != null)
                        vListaConvocatorias.Add(vConvocatoria);
                }
            }

            hfListaConvocatorias.Value = new JavaScriptSerializer().Serialize(vListaConvocatorias);
            hfListaConvocatoriasInicial.Value = new JavaScriptSerializer().Serialize(vListaConvocatorias);

            if (vListaConvocatorias.Count == 0)
                AddConvocatoriaImageButton.Attributes["onclick"] = "";
            if (vListaConvocatorias.Count > 0)
                AddConvocatoriaImageButton.Attributes["onclick"] = "javascript:return Confirmacion();";
            
            CargarGrillaConvocatorias(gvConvocatorias, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            var vjss = new JavaScriptSerializer();

            vListaEvaluadores = new List<Evaluador>();
            vListaEvaluadores = vjss.Deserialize<List<Evaluador>>(hfListaEvaluadores.Value);

            var myGridResults = vListaEvaluadores;

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }

            if (expresionOrdenamiento != null)
            {
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Oferente.Entity.Evaluador), expresionOrdenamiento);
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    var sortExpression = Expression.Lambda<Func<Icbf.Oferente.Entity.Evaluador, object>>(Expression.Convert(prop, typeof(object)), param);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }
                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }
            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gvEvaluadoresAdicionados_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarEvaluador(gvEvaluadoresAdicionados.SelectedRow);
    }
    protected void gvEvaluadoresAdicionados_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvEvaluadoresAdicionados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEvaluadoresAdicionados.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaConvocatorias(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            vListaConvocatoria = new List<Convocatoria>();

            var myGridResults = new JavaScriptSerializer().Deserialize<List<Convocatoria>>(hfListaConvocatorias.Value);

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }

            if (expresionOrdenamiento != null)
            {
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Oferente.Entity.Convocatoria), expresionOrdenamiento);
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    var sortExpression = Expression.Lambda<Func<Icbf.Oferente.Entity.Convocatoria, object>>(Expression.Convert(prop, typeof(object)), param);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }
                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }
            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvConvocatorias_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarConvocatoria(gvConvocatorias.SelectedRow);
    }
    protected void gvConvocatorias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaConvocatorias((GridView)sender, e.SortExpression, false);
    }
    protected void gvConvocatorias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConvocatorias.PageIndex = e.NewPageIndex;
        CargarGrillaConvocatorias((GridView)sender, GridViewSortExpression, true);
    }
    
    private void EliminarEvaluador(GridViewRow pRow)
    {
        try
        {
            toolBar.LipiarMensajeError();

            string pIdUsuario = gvEvaluadoresAdicionados.DataKeys[pRow.RowIndex].Values[0].ToString();

            if (Request.QueryString["oP"] == "E")
            {
                /// hacer esta consulta solo cuando se vaya a editar
                var observacionesEvaluador = vBancoOferentesService.ConsultarObservacionessConvocatoria(Convert.ToInt32(pIdUsuario));

                if (observacionesEvaluador.Count() > 0)
                    throw new Exception("No se puede eliminar el evaluador dado que tiene observaciones asignadas");
            }
                
            var vjss = new JavaScriptSerializer();
            vListaEvaluadores = vjss.Deserialize<List<Evaluador>>(hfListaEvaluadores.Value);

            for (int i = 0; i < vListaEvaluadores.Count; i++)
            {
                if (vListaEvaluadores[i].IdUsuario == Convert.ToInt32(pIdUsuario))
                {
                    vListaEvaluadores.RemoveAt(i);
                    hfListaEvaluadores.Value = vjss.Serialize(vListaEvaluadores);
                    CargarGrilla(gvEvaluadoresAdicionados, GridViewSortExpression, true);
                    return;
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarConvocatoria(GridViewRow pRow)
    {
        try
        {
            toolBar.LipiarMensajeError();

            string pIdConvocatoria = gvConvocatorias.DataKeys[pRow.RowIndex].Values[0].ToString();
            string pNombreConvocatoria = gvConvocatorias.DataKeys[pRow.RowIndex].Values[1].ToString();

            if (Request.QueryString["oP"] == "E")
            {
                var vCoordinador = vBancoOferentesService.ConsultarEquipoEvaluador(Convert.ToInt32(hfIdEquipoEvaluador.Value));
                var vListaObservacionesCoordinador = vBancoOferentesService.ConsultarObservacionessConvocatoria(vCoordinador.IdUsuarioCoordinador);

                if (vListaObservacionesCoordinador.Count() > 0)
                {
                    foreach (var observacionCoordinador in vListaObservacionesCoordinador)
                    {
                        if (observacionCoordinador.IdConvocatoria == Convert.ToInt32(pIdConvocatoria))
                        {
                            if (observacionCoordinador.IdEstadoObservacion == 2 || observacionCoordinador.IdEstadoObservacion == 3)
                                throw new Exception("No se puede eliminar la convocatoria " + pNombreConvocatoria + " dado que el usuario " + vCoordinador.NombreCompleto + " tiene observaciones pendientes");
                        }
                    }
                }

                var vListaEvaluadores = vBancoOferentesService.ConsultarEvaluadoress(Convert.ToInt32(hfIdEquipoEvaluador.Value), true);

                foreach (var evaluador in vListaEvaluadores)
                {
                    var vListaObservacionesEvaluador = vBancoOferentesService.ConsultarObservacionessConvocatoria(evaluador.IdUsuario);
                    if (vListaObservacionesEvaluador.Count > 0)
                    {
                        foreach (var observacionEvaluador in vListaObservacionesEvaluador)
                        {
                            if (observacionEvaluador.IdConvocatoria == Convert.ToInt32(pIdConvocatoria))
                            {
                                if (observacionEvaluador.IdEstadoObservacion == 2 || observacionEvaluador.IdEstadoObservacion == 3)
                                    throw new Exception("No se puede eliminar la convocatoria " + pNombreConvocatoria + " dado que el usuario " + evaluador.NombreEvaluador + " tiene observaciones pendientes");
                            }
                        }
                    }
                }
            }

            var vjss = new JavaScriptSerializer();
            vListaConvocatoria = vjss.Deserialize<List<Convocatoria>>(hfListaConvocatorias.Value);

            for (int i = 0; i < vListaConvocatoria.Count; i++)
            {
                if (vListaConvocatoria[i].IdConvocatoria == Convert.ToInt32(pIdConvocatoria))
                {
                    vListaConvocatoria.RemoveAt(i);

                    if (vListaConvocatoria.Count == 0)
                        AddConvocatoriaImageButton.Attributes["onclick"] = "";
                    if (vListaConvocatoria.Count > 0)
                        AddConvocatoriaImageButton.Attributes["onclick"] = "javascript:return Confirmacion();";

                    hfListaConvocatorias.Value = vjss.Serialize(vListaConvocatoria);
                    CargarGrillaConvocatorias(gvConvocatorias, GridViewSortExpression, true);
                    
                    return;
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
