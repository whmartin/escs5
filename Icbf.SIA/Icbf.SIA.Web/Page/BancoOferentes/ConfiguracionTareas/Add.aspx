﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ConfiguracionTareas_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfExiste" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Cupo m&aacute;ximo
            </td>
            <td>
                Porcentaje
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCupoMaximo"/>
                <Ajax:FilteredTextBoxExtender ID="ftCupoMaximo" runat="server" TargetControlID="txtCupoMaximo" FilterType="Numbers"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPorcentaje"/>
                <Ajax:FilteredTextBoxExtender ID="ftPorcentaje" runat="server" TargetControlID="txtPorcentaje" FilterType="Numbers, Custom" ValidChars="," />
            </td>
        </tr>
    </table>
    
</asp:Content>
