﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;


/// <summary>
/// Página de registro y edición para la entidad ConfiguracionTareas
/// </summary>
public partial class Page_ConfiguracionTareas_Add : GeneralWeb
{
    #region Variables

    masterPrincipal toolBar;
    string PageName = "BancoOferentes/ConfiguracionTareas";
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;

        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }
    
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    #endregion

    #region Métodos
    /// <summary>
    /// Método de edición para la entidad ConfiguracionTareas
    /// </summary>
    private void Guardar()
    {
        toolBar.LipiarMensajeError();
        try
        {
            if (string.IsNullOrEmpty(txtCupoMaximo.Text))
                throw new Exception("El campo cupo máximo no puede estar vacío");
            if (string.IsNullOrEmpty(txtPorcentaje.Text))
                throw new Exception("El campo porcentaje no puede estar vacío");
            if (Convert.ToDecimal(txtPorcentaje.Text) > 100)
                throw new Exception("El porcentaje no puede ser mayor a 100%");
            if (Convert.ToDecimal(txtPorcentaje.Text) < 1)
                throw new Exception("El porcentaje no puede ser 0%");
            if (Convert.ToDecimal(txtCupoMaximo.Text) < 1)
                throw new Exception("El cupo máximo no puede ser 0");

            int vResultado = 0;
            ConfiguracionTareas vConfiguracionTareas = new ConfiguracionTareas();

            vConfiguracionTareas.CupoMaximoPorPersona = Convert.ToInt32(txtCupoMaximo.Text);
            vConfiguracionTareas.PorcentajeMaximoAlerta = Convert.ToDecimal(txtPorcentaje.Text);

            if (hfExiste.Value != "false")
            {
                vConfiguracionTareas.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vConfiguracionTareas, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.ModificarConfiguracionTareas(vConfiguracionTareas);
            }
            else
            {
                vConfiguracionTareas.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vConfiguracionTareas, this.PageName, vSolutionPage);
                vResultado = vBancoOferentesService.InsertarConfiguracionTareas(vConfiguracionTareas);
            }
            
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ConfiguracionTareas.Guardado", "1");
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.EstablecerTitulos("Configuración de tareas", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            hfExiste.Value = GetSessionParameter("ConfiguracionTareas.Existe").ToString();
            RemoveSessionParameter("ConfiguracionTareas.Existe");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ConfiguracionTareas vConfiguracionTareas = new ConfiguracionTareas();
            vConfiguracionTareas = vBancoOferentesService.ConsultarConfiguracionTareas();

            txtCupoMaximo.Text = vConfiguracionTareas.CupoMaximoPorPersona.ToString();
            txtPorcentaje.Text = Convert.ToDouble(vConfiguracionTareas.PorcentajeMaximoAlerta).ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
}
