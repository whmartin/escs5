<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConfiguracionTareas_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfExiste" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Cupo m&aacute;ximo
            </td>
            <td>
                Porcentaje
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCupoMaximo" Enabled="false"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPorcentaje" Enabled="false"/>
            </td>
        </tr>
    </table>
</asp:Content>
