using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;
using System.Linq.Expressions;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;

public partial class Page_ConfiguracionTareas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "BancoOferentes/ConfiguracionTareas";
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Configuracion de tareas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ConfiguracionTareas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ConfiguracionTareas.Guardado");

            ConfiguracionTareas vConfiguracionTareas = new ConfiguracionTareas();
            vConfiguracionTareas = vBancoOferentesService.ConsultarConfiguracionTareas();

            txtCupoMaximo.Text = vConfiguracionTareas.CupoMaximoPorPersona.ToString();
            txtPorcentaje.Text = Convert.ToDouble(vConfiguracionTareas.PorcentajeMaximoAlerta).ToString();

            if (txtCupoMaximo.Text == "0" && txtPorcentaje.Text == "0")
            {
                hfExiste.Value = "false";
                SetSessionParameter("ConfiguracionTareas.Existe", hfExiste.Value);
                NavigateTo(SolutionPage.Edit);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
