﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaConvocatoria.aspx.cs" Inherits="Page_BancoOferentes_LupaConvocatoria_LupaConvocatoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="Panel1">
        <asp:Panel runat="server" ID="pnlConsulta">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        Nombre convocatoria
                    </td>
                    <td>
                        N&uacute;mero convocatoria
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtNombre"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroConvocatoria"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvConvocatorias" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" DataKeyNames="IdConvocatoria,Nombre,Numero,NombreRegional" CellPadding="0" Height="16px"
                            OnSorting="gvConvocatorias_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvConvocatorias_PageIndexChanging" OnSelectedIndexChanged="gvConvocatorias_SelectedIndexChanged">
                            <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                             </asp:TemplateField>
                                <asp:BoundField HeaderText="Nombre" DataField="Nombre" SortExpression="Nombre" />
                                <asp:BoundField HeaderText="N&uacute;mero" DataField="Numero" SortExpression="Numero" />
                                <asp:BoundField HeaderText="Nombre Regional" DataField="NombreRegional" SortExpression="NombreRegional" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

