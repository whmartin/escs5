﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using AjaxControlToolkit;
using System.Linq.Expressions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;


public partial class Page_BancoOferentes_LupaConvocatoria_LupaConvocatoria : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    BancoOferentesService vBancoOferentesService = new BancoOferentesService();
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text) && string.IsNullOrWhiteSpace(txtNumeroConvocatoria.Text))
                throw new Exception("Debe diligenciar al menos un campo para realizar la búsqueda");

            CargarGrilla(gvConvocatorias, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.EstablecerTitulos("Lista Convocatorias");
            gvConvocatorias.EmptyDataText = "No se encontraron datos, verifique por favor";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

   
    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int vNumeroConvocatoria = string.IsNullOrWhiteSpace(txtNumeroConvocatoria.Text) ? 0 : Convert.ToInt32(txtNumeroConvocatoria.Text);

            var myGridResults = vBancoOferentesService.ConsultarConvocatoriass(txtNombre.Text, vNumeroConvocatoria);

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }
            if (expresionOrdenamiento != null)
            {
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Oferente.Entity.Convocatoria), expresionOrdenamiento);
                    var prop = Expression.Property(param, expresionOrdenamiento);
                    var sortExpression = Expression.Lambda<Func<Icbf.Oferente.Entity.Convocatoria, object>>(Expression.Convert(prop, typeof(object)), param);

                    if (GridViewSortDirection == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {

            int rowIndex = pRow.RowIndex;
            string pIdConvocatoria = gvConvocatorias.DataKeys[rowIndex].Values[0].ToString();
            string pNombreConvocatoria = gvConvocatorias.DataKeys[rowIndex].Values[1].ToString();
            string pNumeroConvocatoria = gvConvocatorias.DataKeys[rowIndex].Values[2].ToString();
            string pNombreRegional = gvConvocatorias.DataKeys[rowIndex].Values[3].ToString();

            string returnValues =
                "<script language='javascript'> " +
                "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + pIdConvocatoria + "';" + "pObj[" + (1) + "] = '" + pNombreConvocatoria + "';" + "pObj[" + (2) + "] = '" + pNumeroConvocatoria + "';" + "pObj[" + (3) + "] = '" + pNombreRegional + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "Closed", returnValues);

            return;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gvConvocatorias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConvocatorias.SelectedRow);
    }
    protected void gvConvocatorias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvConvocatorias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConvocatorias.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }


    private void RetornarVentana()
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";

        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
        return;
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }
}
