﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Seguridad_Programa_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%--<asp:ScriptManager runat="server" ID="smPrincipal"></asp:ScriptManager>--%>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblModulo" runat="server" Text="Modulo:"></asp:Label>
                *
                <asp:RequiredFieldValidator runat="server" ID="rfvModulo" ControlToValidate="ddlModulo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvModulo" ControlToValidate="ddlModulo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                <asp:Label ID="lblNombrePrograma" runat="server" Text="Nombre Programa:"></asp:Label>
                *
                <asp:RequiredFieldValidator runat="server" ID="cvNombrePrograma" ControlToValidate="txtNombrePrograma"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlModulo">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombrePrograma"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombrePrograma" runat="server" TargetControlID="txtNombrePrograma"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblCodigoPrograma" runat="server" Text="C&oacute;digo Programa:"></asp:Label>
                *
                <asp:RequiredFieldValidator runat="server" ID="rfCodigoPrograma" ControlToValidate="txtCodigoPrograma"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblPosicion" runat="server" Text="Posicion:"></asp:Label>
                *
                <asp:RequiredFieldValidator runat="server" ID="rfvPosicion" ControlToValidate="txtPosicion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvPosicion" ControlToValidate="txtPosicion"
                    SetFocusOnError="true" ErrorMessage="Dato numerico" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="DataTypeCheck" Type="Integer" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoPrograma"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoPrograma" runat="server" TargetControlID="txtCodigoPrograma"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPosicion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPosicion" runat="server" TargetControlID="txtPosicion"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:HiddenField runat="server" ID="hfIdPrograma" />
            </td>
            <td>
                <asp:Label ID="lblVisibleMenu" runat="server" Text="Visible Menu:"></asp:Label>
                *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="cbVisibleMenu" />
            </td>
        </tr>
        <tr class="rowB">
           <td>Es Reporte</td>
            <td>Reporte</td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:RadioButtonList ID="rblEsReporte" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Si</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                 <asp:DropDownList ID="ddlIdReporte" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:Label ID="lblGeneraLog" runat="server" Text="Genera Log Auditoria:"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBox runat="server" ID="cbGeneraLog" />
            </td>
        </tr>
    </table>
</asp:Content>
