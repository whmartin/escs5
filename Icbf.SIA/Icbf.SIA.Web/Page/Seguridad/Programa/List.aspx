﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Seguridad_Programa_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    <asp:Label ID="lblModulo" runat="server" Text="Módulo:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblPrograma" runat="server" Text="Programa:"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlModulo">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtPrograma" runat="server"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftPrograma" runat="server" TargetControlID="txtPrograma"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowBG">
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProgramas" AutoGenerateColumns="false" AllowPaging="True"
                        Width="100%" DataKeyNames="IdPrograma" CellPadding="0" GridLines="None" OnPageIndexChanging="gvProgramas_PageIndexChanging"
                        OnSelectedIndexChanged="gvProgramas_SelectedIndexChanged"  AllowSorting="true"
                        onsorting="gvProgramas_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Modulo" DataField="NombreModulo" SortExpression="NombreModulo" />
                            <asp:BoundField HeaderText="Programa" DataField="NombrePrograma" SortExpression="NombrePrograma"/>
                            <asp:BoundField HeaderText="CodigoPrograma" DataField="CodigoPrograma" SortExpression="CodigoPrograma"/>
                            <asp:CheckBoxField HeaderText="Estado" DataField="Estado" />
                            <asp:CheckBoxField HeaderText="Visible Menu" DataField="VisibleMenu" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
