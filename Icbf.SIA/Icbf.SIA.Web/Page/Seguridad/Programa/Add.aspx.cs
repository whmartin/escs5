﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;

/// <summary>
/// Página de registro y edición de programas de la aplicación
/// </summary>
public partial class Page_Seguridad_Programa_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();
    SIAService vSIAService = new SIAService();

    string PageName = "Seguridad/Programa";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            //CrearFormulario();
            int vResultado;
            Programa vPrograma = new Programa();


            vPrograma.IdModulo = Convert.ToInt32(ddlModulo.SelectedValue);
            vPrograma.NombrePrograma = txtNombrePrograma.Text;
            vPrograma.CodigoPrograma = txtCodigoPrograma.Text;
            vPrograma.Posicion = Convert.ToInt32(txtPosicion.Text);
            vPrograma.Estado = rblEstado.SelectedValue == "1" ? true : false;
            vPrograma.VisibleMenu = cbVisibleMenu.Checked;
            vPrograma.GeneraLog = cbGeneraLog.Checked;

            //reportes dinamicos
            vPrograma.EsReporte = Convert.ToInt32(rblEsReporte.SelectedValue);
            vPrograma.IdReporte = Convert.ToInt32(ddlIdReporte.SelectedValue);
            //reportes dinamicos


            InformacionAudioria(vPrograma, this.PageName, vSolutionPage);

            if (Request.QueryString["oP"] == "E")
            {
                vPrograma.IdPrograma = Convert.ToInt32(hfIdPrograma.Value);
                vPrograma.UsuarioModificacion = GetSessionUser().NombreUsuario;
                vResultado = vSeguridadFAC.ModificarPrograma(vPrograma);
            }
            else
            {
                vPrograma.UsuarioCreacion = GetSessionUser().NombreUsuario;
                vResultado = vSeguridadFAC.InsertarPrograma(vPrograma);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Programa.Id", vPrograma.IdPrograma);
                SetSessionParameter("Programa.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Programa", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdPrograma = Convert.ToInt32(GetSessionParameter("Programa.Id"));
            //RemoveSessionParameter("Programa.Id");


            Programa vPrograma = new Programa();
            vPrograma = vSeguridadFAC.ConsultarPrograma(vIdPrograma);

            ddlModulo.SelectedValue = vPrograma.IdModulo.ToString();
            hfIdPrograma.Value = vPrograma.IdPrograma.ToString();
            txtNombrePrograma.Text = vPrograma.NombrePrograma;
            txtCodigoPrograma.Text = vPrograma.CodigoPrograma;
            txtPosicion.Text = vPrograma.Posicion.ToString();
            cbVisibleMenu.Checked = vPrograma.VisibleMenu;
            rblEstado.SelectedValue = vPrograma.Estado == true ? "1" : "0";
            cbGeneraLog.Checked = vPrograma.GeneraLog;

            //reportes dinamicos
            if (vPrograma.EsReporte == null || vPrograma.EsReporte == 0) rblEsReporte.SelectedValue = "0"; else rblEsReporte.SelectedValue = "1";
            if (vPrograma.IdReporte != null) ddlIdReporte.SelectedValue = vPrograma.IdReporte.ToString(); else ddlIdReporte.SelectedValue = "-1";
            //reportes dinamicos

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vPrograma.UsuarioCreacion, vPrograma.FechaCreacion, vPrograma.UsuarioModificacion, vPrograma.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlModulo.DataSource = vSeguridadFAC.ConsultarModulos(null, null);
            ddlModulo.DataTextField = "NombreModulo";
            ddlModulo.DataValueField = "IdModulo";
            ddlModulo.DataBind();
            ddlModulo.Items.Insert(0, new ListItem("Seleccione", "-1"));

            ddlIdReporte.DataSource = vSIAService.ConsultarReportes(null, null);
            ddlIdReporte.DataTextField = "NombreReporte";
            ddlIdReporte.DataValueField = "IdReporte";
            ddlIdReporte.DataBind();
            ddlIdReporte.Items.Insert(0, new ListItem("Seleccione", "-1"));


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}