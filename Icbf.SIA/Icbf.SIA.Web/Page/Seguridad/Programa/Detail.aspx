﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Seguridad_Programa_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblModulo" runat="server" Text="Modulo:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblNombrePrograma" runat="server" Text="Nombre Programa:"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlModulo" Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombrePrograma" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblCodigoPrograma" runat="server" Text="C&oacute;digo Programa:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPosicion" runat="server" Text="Posicion:"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoPrograma" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPosicion" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                <asp:HiddenField ID="hfIdPrograma" runat="server" />
            </td>
            <td>
                <asp:Label ID="lblVisibleMenu" runat="server" Text="Visible Menu:"></asp:Label>
                *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="cbVisibleMenu" Enabled="false" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:Label ID="lblGeneraLog" runat="server" Text="Genera Log Auditoria:"></asp:Label>
                *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBox runat="server" ID="cbGeneraLog" Enabled="false" />
            </td>
        </tr>
    </table>
</asp:Content>
