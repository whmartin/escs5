﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;

/// <summary>
/// Página que despliega el detalle del registro de programa
/// </summary>
public partial class Page_Seguridad_Programa_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();
    string PageName = "Seguridad/Programa";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Programa.Id", hfIdPrograma.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }


    /// <summary>
    /// Manejador de evento click para el botòn Auditoria
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAuditoria_Click(object sender, EventArgs e)
    {
        
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdPrograma = Convert.ToInt32(GetSessionParameter("Programa.Id"));
            //RemoveSessionParameter("Programa.Id");

            if (GetSessionParameter("Programa.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Programa.Guardado");

            Programa vPrograma = new Programa();
            vPrograma = vSeguridadFAC.ConsultarPrograma(vIdPrograma);

            ddlModulo.SelectedValue = vPrograma.IdModulo.ToString();
            hfIdPrograma.Value = vPrograma.IdPrograma.ToString();
            txtNombrePrograma.Text = vPrograma.NombrePrograma;
            txtCodigoPrograma.Text = vPrograma.CodigoPrograma;
            txtPosicion.Text = vPrograma.Posicion.ToString();
            cbVisibleMenu.Checked = vPrograma.VisibleMenu;
            rblEstado.SelectedValue = vPrograma.Estado == true ? "1" : "0";
            cbGeneraLog.Checked = vPrograma.GeneraLog;

            ObtenerAuditoria(PageName, hfIdPrograma.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vPrograma.UsuarioCreacion, vPrograma.FechaCreacion, vPrograma.UsuarioModificacion, vPrograma.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Eliminar registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdPrograma = Convert.ToInt32(hfIdPrograma.Value);
            Programa vPrograma = new Programa();
            vPrograma = vSeguridadFAC.ConsultarPrograma(vIdPrograma);

            InformacionAudioria(vPrograma, this.PageName, vSolutionPage);

            int vResultado = vSeguridadFAC.EliminarPrograma(vPrograma);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Programa.Eliminado", "1");
                NavigateTo(SolutionPage.List);

            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            //Para ir a ver la auditoria
            

            toolBar.EstablecerTitulos("Programa", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlModulo.DataSource = vSeguridadFAC.ConsultarModulos(null, null);
            ddlModulo.DataTextField = "NombreModulo";
            ddlModulo.DataValueField = "IdModulo";
            ddlModulo.DataBind();

            ddlModulo.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}