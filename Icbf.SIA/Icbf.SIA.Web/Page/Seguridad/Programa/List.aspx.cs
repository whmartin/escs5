﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Página que despliega la consulta basada en filtros de los programas de la aplicación
/// </summary>
public partial class Page_Seguridad_Programa_List : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();


    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, "Seguridad/Programa", SolutionPage.List))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    private List<Programa> EstablecerConsulta()
    {
        int vIdModulo = Convert.ToInt32(ddlModulo.SelectedValue);
        string vNombrePrograma = txtPrograma.Text;
        return vSeguridadFAC.ConsultarProgramas(vIdModulo, vNombrePrograma);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            RemoveSessionParameter("Programa.NombreModulo.Orden");
            RemoveSessionParameter("Programa.Coleccion");

            int vIdModulo;
            String vNombrePrograma;

            vIdModulo = Convert.ToInt32(ddlModulo.SelectedValue);
            vNombrePrograma = txtPrograma.Text;

            gvProgramas.DataSource = vSeguridadFAC.ConsultarProgramas(vIdModulo, vNombrePrograma);
            gvProgramas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvProgramas.PageSize = PageSize();
            gvProgramas.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Programa", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            if (GetSessionParameter("Programa.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Programa.Eliminado");

            ddlModulo.DataSource = vSeguridadFAC.ConsultarModulos(null, null);
            ddlModulo.DataTextField = "NombreModulo";
            ddlModulo.DataValueField = "IdModulo";
            ddlModulo.DataBind();

            ddlModulo.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProgramas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Programa.Id", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProgramas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProgramas.SelectedRow);
    }

    protected void gvProgramas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProgramas.PageIndex = e.NewPageIndex;
        Buscar();
    }

    private List<Programa> SeleccionarColeccion()
    {
        if (GetSessionParameter("Programa.Coleccion").ToString() == "")
        {
            return EstablecerConsulta();
        }
        else
        {
            try
            {
                return (List<Programa>)GetSessionParameter("Programa.Coleccion");
            }
            catch
            {
                return EstablecerConsulta();
            }
        }
    }

    private void GuardarColeccion(Object pColeccion)
    {
        SetSessionParameter("Programa.Coleccion", pColeccion);
    }

    protected void gvProgramas_Sorting(object sender, GridViewSortEventArgs e)
    {
         List<Programa> vProgramas = SeleccionarColeccion();

         switch (e.SortExpression)
         {
             case "NombreModulo":
                 if (GetSessionParameter("Programa.NombreModulo.Orden").ToString() == "" ||
                     GetSessionParameter("Programa.NombreModulo.Orden").ToString() == "A")
                 {
                     vProgramas = vProgramas.OrderBy(x => x.NombreModulo).ToList();
                     SetSessionParameter("Programa.NombreModulo.Orden", "D");

                 }
                 else
                 {
                     vProgramas = vProgramas.OrderByDescending(x => x.NombreModulo).ToList();
                     SetSessionParameter("Programa.NombreModulo.Orden", "A");
                 }
                 break;
             case "NombrePrograma":
                 if (GetSessionParameter("Programa.NombrePrograma.Orden").ToString() == "" ||
                     GetSessionParameter("Programa.NombrePrograma.Orden").ToString() == "A")
                 {
                     vProgramas = vProgramas.OrderBy(x => x.NombrePrograma).ToList();
                     SetSessionParameter("Programa.NombrePrograma.Orden", "D");
                 }
                 else
                 {
                     vProgramas = vProgramas.OrderByDescending(x => x.NombrePrograma).ToList();
                     SetSessionParameter("Programa.NombrePrograma.Orden", "A");
                 }
                 break;
             case "CodigoPrograma":
                 if (GetSessionParameter("Programa.CodigoPrograma.Orden").ToString() == "" ||
                     GetSessionParameter("Programa.CodigoPrograma.Orden").ToString() == "A")
                 {
                     vProgramas = vProgramas.OrderBy(x => x.CodigoPrograma).ToList();
                     SetSessionParameter("Programa.CodigoPrograma.Orden", "D");
                 }
                 else
                 {
                     vProgramas = vProgramas.OrderByDescending(x => x.CodigoPrograma).ToList();
                     SetSessionParameter("Programa.CodigoPrograma.Orden", "A");
                 }
                 break;
             case "Estado":
                 if (GetSessionParameter("Programa.Estado.Orden").ToString() == "" ||
                     GetSessionParameter("Programa.Estado.Orden").ToString() == "A")
                 {
                     vProgramas = vProgramas.OrderBy(x => x.Estado).ToList();
                     SetSessionParameter("Programa.Estado.Orden", "D");
                 }
                 else
                 {
                     vProgramas = vProgramas.OrderByDescending(x => x.Estado).ToList();
                     SetSessionParameter("Programa.Estado.Orden", "A");
                 }
                 break;
             case "VisibleMenu":
                 if (GetSessionParameter("Programa.VisibleMenu.Orden").ToString() == "" ||
                     GetSessionParameter("Programa.VisibleMenu.Orden").ToString() == "A")
                 {
                     vProgramas = vProgramas.OrderBy(x => x.VisibleMenu).ToList();
                     SetSessionParameter("Programa.VisibleMenu.Orden", "D");
                 }
                 else
                 {
                     vProgramas = vProgramas.OrderByDescending(x => x.VisibleMenu).ToList();
                     SetSessionParameter("Programa.VisibleMenu.Orden", "A");
                 }
                 break;
         }
         GuardarColeccion(vProgramas);
         gvProgramas.DataSource = vProgramas;
         gvProgramas.DataBind();
    }
}