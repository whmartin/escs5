﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Seguridad_rol_list" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">
    <%--<asp:ScriptManager ID="smPagina" runat="server"></asp:ScriptManager>--%>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Nombre del rol:
                </td>
                <td>
                    Estado:
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombreRol"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftbeNombreRol" runat="server" TargetControlID="txtNombreRol"
                        FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" " />
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRoles" AutoGenerateColumns="false" AllowPaging="True"
                        Width="100%" OnPageIndexChanging="gvRoles_PageIndexChanging" OnSelectedIndexChanged="gvRoles_SelectedIndexChanged"
                        DataKeyNames="IdRol" CellPadding="0" GridLines="None">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        ToolTip="Detalle" Width="16px" Height="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre" DataField="NombreRol" />
                            <asp:BoundField HeaderText="Descripcion" DataField="DescripcionRol" />
                            <asp:CheckBoxField HeaderText="Activo" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
