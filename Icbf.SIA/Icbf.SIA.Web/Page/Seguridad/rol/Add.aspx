﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Seguridad_rol_Add" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">
    <%--<asp:ScriptManager ID="smPagina" runat="server"></asp:ScriptManager>--%>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblNombreRol" runat="server" Text="Nombre del rol * "></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="cvNombreRol" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtNombreRol" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblDescripcionRol" runat="server" Text="Descripcion del rol * "></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="cpDescripcion" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtDescripcionRol" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreRol"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreRol" runat="server" TargetControlID="txtNombreRol"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <asp:HiddenField runat="server" ID="hfIdRol" />
            </td>
                 <td>
                <asp:TextBox runat="server" ID="txtDescripcionRol"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionRol" runat="server" TargetControlID="txtDescripcionRol"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblEstadoRol" runat="server" Text="Estado del rol * "></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="cvEstado" ErrorMessage="Campo Requerido"
                    ControlToValidate="rblEstadoRol" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
              <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo de Rol"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstadoRol" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Text="Activo" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Inactivo"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="ChkAdministrador" Text="Rol Administrador"/>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upPermisos">
        <ContentTemplate>
            <table width="90%" align="center">
                <tr class="rowBG">
                    <td colspan="2">
                        Permisos
                    </td>
                </tr>
                <tr class="rowBG">
                    <td colspan="2">
                        <asp:GridView runat="server" ID="gvPermisos" AutoGenerateColumns="false"
                            Width="100%" DataKeyNames="IdPrograma,Insertar,Modificar,Eliminar,Consultar"
                            CellPadding="0" GridLines="None" OnRowDataBound="gvPermisos_RowDataBound">
                            <Columns>
                                <asp:BoundField HeaderText="Programa" DataField="NombrePrograma" />
                                <asp:TemplateField HeaderText="Insertar">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chbInsertar" AutoPostBack="true" OnCheckedChanged="Operacion_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Modificar">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chbModificar" AutoPostBack="true" OnCheckedChanged="Operacion_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eliminar">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chbEliminar" AutoPostBack="true" OnCheckedChanged="Operacion_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Consultar">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chbConsultar" AutoPostBack="true" OnCheckedChanged="Consultar_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

