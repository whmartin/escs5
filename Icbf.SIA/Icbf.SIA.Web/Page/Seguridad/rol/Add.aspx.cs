﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;

/// <summary>
/// Página de registro y edición de roles de la aplicación incluyendo las pantallas a las que tiene acceso el rol
/// </summary>
public partial class Page_Seguridad_rol_Add : GeneralWeb
{
    masterPrincipal toolBar;

    SeguridadService vSeguridadFAC = new SeguridadService();
    string PageName = "Seguridad/rol";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarPermisos(0);
                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                }
                else
                {
                    gvPermisos.DataSource = vSeguridadFAC.ConsultarPermisosRol(-1);
                    gvPermisos.DataBind();
                }
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Roles", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Rol vRol = new Rol();
            vRol.NombreRol = txtNombreRol.Text;
            vRol.DescripcionRol = txtDescripcionRol.Text;
            vRol.EsAdministrador = ChkAdministrador.Checked;
            if (rblEstadoRol.SelectedValue == "1")
                vRol.Estado = true;
            if (rblEstadoRol.SelectedValue == "0")
                vRol.Estado = false;

            foreach (GridViewRow vGridViewRow in gvPermisos.Rows)
            {
                Permiso vPermiso = new Permiso();
                if (((CheckBox)vGridViewRow.Cells[1].Controls[1]).Checked)
                    vPermiso.Insertar = true;
                if (((CheckBox)vGridViewRow.Cells[2].Controls[1]).Checked)
                    vPermiso.Modificar = true;
                if (((CheckBox)vGridViewRow.Cells[3].Controls[1]).Checked)
                    vPermiso.Eliminar = true;
                if (((CheckBox)vGridViewRow.Cells[4].Controls[1]).Checked)
                    vPermiso.Consultar = true;

                InformacionAudioria(vPermiso, this.PageName, vSolutionPage);

                vPermiso.IdPrograma = Convert.ToInt32(gvPermisos.DataKeys[vGridViewRow.RowIndex].Value.ToString());

                vRol.Permisos.Add(vPermiso);
            }

            InformacionAudioria(vRol, this.PageName, vSolutionPage);

            if (Request.QueryString["oP"] == "E")
            {
                vRol.IdRol = Convert.ToInt32(hfIdRol.Value);
                vRol.UsuarioModificacion = GetSessionUser().NombreUsuario.ToString();
                vResultado = vSeguridadFAC.ModificarRol(vRol);
            }
            else
            {
                vRol.UsuarioCreacion = GetSessionUser().NombreUsuario.ToString();
                vResultado = vSeguridadFAC.InsertarRol(vRol);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Rol.Id", vRol.IdRol.ToString());
                SetSessionParameter("Rol.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado == -1)
            {
                toolBar.MostrarMensajeError("Ya existe un rol con el nombre ingresado, verifique por favor.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            string vIdRol = Convert.ToString(GetSessionParameter("Rol.Id"));
            int iIdRol = Convert.ToInt32(vIdRol);
            //RemoveSessionParameter("Rol.Id");

            Rol vRol = new Rol();
            vRol = vSeguridadFAC.ConsultarRol(iIdRol);

            hfIdRol.Value = vIdRol;
            txtNombreRol.Text = vRol.NombreRol;
            txtDescripcionRol.Text = vRol.DescripcionRol;
            ChkAdministrador.Checked = vRol.EsAdministrador;
            if (vRol.Estado)
                rblEstadoRol.SelectedValue = "1";
            else
                rblEstadoRol.SelectedValue = "0";

            CargarPermisos(iIdRol);

            gvPermisos.DataSource = vSeguridadFAC.ConsultarPermisosRol(iIdRol);
            gvPermisos.DataBind();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRol.UsuarioCreacion, vRol.FechaCreacion, vRol.UsuarioModificacion, vRol.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnAsignar_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnRevocar_Click(object sender, EventArgs e)
    {
        
    }

    private void CargarPermisos(int pIdRol)
    {
        
    }

    protected void gvPermisos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Permiso vPermiso = (Permiso)e.Row.DataItem;
            if (vPermiso.Insertar)
                ((CheckBox)e.Row.Cells[1].Controls[1]).Checked = true;
            if (vPermiso.Modificar)
                ((CheckBox)e.Row.Cells[2].Controls[1]).Checked = true;
            if (vPermiso.Eliminar)
                ((CheckBox)e.Row.Cells[3].Controls[1]).Checked = true;
            if (vPermiso.Consultar)
                ((CheckBox)e.Row.Cells[4].Controls[1]).Checked = true;

        }
    }

    protected void Operacion_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow vGridViewRow in gvPermisos.Rows)
        {
            if (((CheckBox)vGridViewRow.Cells[3].Controls[1]).Checked || ((CheckBox)vGridViewRow.Cells[2].Controls[1]).Checked || ((CheckBox)vGridViewRow.Cells[1].Controls[1]).Checked)
            {
                ((CheckBox)vGridViewRow.Cells[4].Controls[1]).Checked = true;
            }
        }
    }

    protected void Consultar_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow vGridViewRow in gvPermisos.Rows)
        {
            if (!((CheckBox)vGridViewRow.Cells[4].Controls[1]).Checked)
            {
                ((CheckBox)vGridViewRow.Cells[3].Controls[1]).Checked = false;
                ((CheckBox)vGridViewRow.Cells[2].Controls[1]).Checked = false;
                ((CheckBox)vGridViewRow.Cells[1].Controls[1]).Checked = false;
            }
        }
    }


}
