﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;

/// <summary>
/// Página que despliega el detalle del registro de rol
/// </summary>
public partial class Page_Seguridad_rol_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();
    string PageName = "Seguridad/rol";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Rol.Id", hfId.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            
            gvPermisos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Roles", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("Rol.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();

            //RemoveSessionParameter("Rol.Guardado");
            String vIdRol = GetSessionParameter("Rol.Id").ToString();
            int iIdRol = Convert.ToInt32(vIdRol);
            RemoveSessionParameter("Rol.Id");

            Rol vRol = new Rol();
            vRol = vSeguridadFAC.ConsultarRol(iIdRol);

            hfId.Value = vIdRol;
            lblNombreRol.Text = vRol.NombreRol;
            lblDescripcionRol.Text = vRol.DescripcionRol;
            if (vRol.Estado)
                lblEstadoRol.Text = "Activo";
            else
                lblEstadoRol.Text = "Inactivo";

            gvPermisos.DataSource = vSeguridadFAC.ConsultarPermisosRol(iIdRol);
            gvPermisos.DataBind();

            ObtenerAuditoria(PageName, hfId.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRol.UsuarioCreacion, vRol.FechaCreacion, vRol.UsuarioModificacion, vRol.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarRegistro()
    {
        try
        {
            String vIdRol = Convert.ToString(hfId.Value);
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvPermisos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int vIdRol;
        if (Int32.TryParse(hfId.Value, out vIdRol))
        {
            gvPermisos.PageIndex = e.NewPageIndex;
            gvPermisos.DataSource = vSeguridadFAC.ConsultarPermisosRol(vIdRol);
            gvPermisos.DataBind();
        }
    }
}
