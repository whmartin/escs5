﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Seguridad_rol_Detail" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre del rol:
                <asp:HiddenField ID="hfId" runat="server"></asp:HiddenField>
            </td>
            <td>
                Descripcion del rol:
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="lblNombreRol" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="lblDescripcionRol" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado del rol:
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox ID="lblEstadoRol" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Permisos
            </td>
        </tr>
        <tr class="rowBG">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvPermisos" AutoGenerateColumns="false"
                    Width="100%" DataKeyNames="IdPrograma" CellPadding="0" GridLines="None" 
                    OnPageIndexChanging="gvPermisos_PageIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="Programa" DataField="NombrePrograma" />
                        <asp:CheckBoxField HeaderText="Insertar" DataField="Insertar" ReadOnly="true" />
                        <asp:CheckBoxField HeaderText="Modificar" DataField="Modificar" ReadOnly="true" />
                        <asp:CheckBoxField HeaderText="Eliminar" DataField="Eliminar" ReadOnly="true" />
                        <asp:CheckBoxField HeaderText="Consultar" DataField="Consultar" ReadOnly="true" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
