﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Business;
using System.Data;
using Icbf.SIA.Service;
using System.Configuration;
using System.Net.Mail;

/// <summary>
/// Página que despliega el detalle del registro de usuario
/// </summary>
public partial class Page_Seguridad_usuario_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vRUBOService = new SIAService();
    SeguridadService vSeguridadService = new SeguridadService();
    string PageName = "Seguridad/usuario";
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
 

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vSolutionPage = SolutionPage.Detail;
        if (!Page.IsPostBack)
            CargarDatos();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Usuario.Id", hfId.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            vRUBOService = new SIAService();
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Usuario", SolutionPage.Detail.ToString());

            LLenarTipoDocumento();
            LLenarPerfil();
            LLenarRol();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdUsuario = Convert.ToInt32(GetSessionParameter("Usuario.Id"));
            //RemoveSessionParameter("Usuario.Id");

            if (GetSessionParameter("Usuario.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Usuario.Guardado");

            Usuario vUsuario = new Usuario();
            vUsuario = vRUBOService.ConsultarUsuario(vIdUsuario);
            hfId.Value = vUsuario.IdUsuario.ToString();
            txtNumeroDoc.Text = vUsuario.NumeroDocumento;
            txtPrimerNombre.Text = vUsuario.PrimerNombre;
            txtSegundoNombre.Text = vUsuario.SegundoNombre;
            txtPrimerApellido.Text = vUsuario.PrimerApellido;
            txtSegundoApellido.Text = vUsuario.SegundoApellido;
            txtCorreoElectronico.Text = vUsuario.CorreoElectronico;
            txtNombreUsuario.Text = vUsuario.NombreUsuario;

            ddlTipoDoc.SelectedValue = vUsuario.IdTipoDocumento.ToString();

            //ddlRol.SelectedValue = vUsuario.Rol.ToString().ToUpper();
            rblEstado.SelectedValue = vUsuario.Estado == true ? "1" : "0";
            List<Rol> RolesUsuario = new List<Rol>();

            foreach(string itemRolUsuario in System.Web.Security.Roles.GetRolesForUser(vUsuario.NombreUsuario))
            {
                Rol rolUsuario = new Rol();
                rolUsuario.NombreRol = itemRolUsuario;
                RolesUsuario.Add(rolUsuario);
            }
            gvRolesAsignadoUsuario.DataSource = RolesUsuario;
            gvRolesAsignadoUsuario.DataBind();

            ObtenerAuditoria(PageName, hfId.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vUsuario.UsuarioCreacion, vUsuario.FechaCreacion, vUsuario.UsuarioModificacion, vUsuario.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void LLenarPerfil()
    {

    }

    /// <summary>
    /// Llenar rol
    /// </summary>
    private void LLenarRol()
    {
        List<Rol> vLRol = vSeguridadService.ConsultarRoles("", true);

        foreach (Rol vRol in vLRol)
        {
            vRol.NombreRol = vRol.NombreRol.ToUpper();
        }
        vLRol.Insert(0, new Rol() { NombreRol = "Seleccione" });
        //ddlRol.DataSource = vLRol;
        //ddlRol.DataTextField = "NombreRol";
        //ddlRol.DataValueField = "NombreRol";
        //ddlRol.DataBind();
    }

    /// <summary>
    /// Llenar tipo de documento
    /// </summary>
    private void LLenarTipoDocumento()
    {
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "CC") || (tD.Codigo == "CE") || (tD.Codigo == "PA") || (tD.Codigo == "RC") || (tD.Codigo == "NIT") || (tD.Codigo == "RUT"))
            {
                ddlTipoDoc.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString()));
            }
        }
    }

    protected void reenviarCorreo(object sender, EventArgs e)
    {
        string emailAdministrador = new GeneralWeb().GetSessionUser().CorreoElectronico;
        string vIdUsuario = Convert.ToString(GetSessionParameter("Usuario.Id"));
        Usuario vUsuario = new Usuario();
        vUsuario = vRUBOService.ConsultarUsuario(Convert.ToInt32(vIdUsuario));
        string emailFuncionario = vUsuario.CorreoElectronico;
        string codigo = String.Empty;
        codigo = vUsuario.CodigoValidacion;
        if (!string.IsNullOrEmpty(codigo))
        {
            System.Net.Mail.MailMessage MyMailMessage = new System.Net.Mail.MailMessage();
            MyMailMessage.To.Add(emailFuncionario);
            MyMailMessage.To.Add(emailAdministrador);
            MyMailMessage.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);
            MyMailMessage.Subject = "Reenvío código de Activación";
            String cuerpo = String.Empty;
            cuerpo += @"<table><tr><td><b>" + DateTime.Now.ToLongDateString() + @"</b></td></tr><tr><td></td></tr>";
            cuerpo += @"<tr><td></td></tr>";
            cuerpo += @"<tr><td>Señor(es) :" + vUsuario.NombreUsuario.ToString() + vUsuario.PrimerApellido.ToString() + vUsuario.SegundoApellido.ToString() + "</td></tr>";
            cuerpo += @"<tr><td>Cordialmente remitimos su código de Activación</td></tr>";
            cuerpo += @"<tr><td><b>" + codigo.ToString() + "</b></td></tr>";
            cuerpo += @"<tr><td></td></tr>";
            cuerpo += @"<tr><td><b>Instituto Colombiano de Bienestar Familiar</b></td></tr>";
            cuerpo += @"<tr><td>Sede de la Dirección General</td></tr>";
            cuerpo += @"<tr><td>Av. 68 No. 64C - 75, Bogotá, Colombia</td></tr>";
            cuerpo += @"<tr><td></td></tr>";
            cuerpo += @"<tr><td></td></tr></table>";
            MyMailMessage.Body = cuerpo;   //CrearMensajeSolicitud(pProgVisitasTramite);
            MyMailMessage.IsBodyHtml = true;
            SmtpClient MySmtpClient = new SmtpClient();
            MySmtpClient.Host = ConfigurationManager.AppSettings["MailHost"].ToString();
            MySmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MailPort"]);
            MySmtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MailEnableSsl"]);

            object userState = MyMailMessage;
            MySmtpClient.SendCompleted += new SendCompletedEventHandler(MySmtpClient_SendCompleted);

            try
            {
                MySmtpClient.SendAsync(MyMailMessage, userState);
                //MySmtpClient.Send(MyMailMessage);
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        else
        {
            toolBar.MostrarMensajeError("El usuario no posee código de activación.");
        }
    }

    public void MySmtpClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo de justificación fue cancelado");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
        else
        {
            toolBar.MostrarMensajeGuardado("Enviado exitosamente");
        }
    }

}