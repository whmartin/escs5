﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Seguridad_usuario_Add" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">


    
        <script type="text/javascript">
        
        $(function () {
            $('[id*=LstRolesUsuario]').multiselect({
                includeSelectAllOption: true
            });
        });

    </script>


    <asp:HiddenField ID="hfIdRolesUruario" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de documento *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Campo Requerido"
                    ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvTipoDocumento" ErrorMessage="Campo Requerido"
                    ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    Operator="NotEqual" ValueToCompare="-1" ForeColor="Red" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                <asp:Label ID="lblNumeroDoc" runat="server" Text="Numero de documento *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroDoc" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="revNumDocCero" ErrorMessage="El documento no debe empezar con cero (0)"
                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" ValidationExpression="^[A-Za-z1-9]+.*$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="revCC" ErrorMessage="*" ControlToValidate="txtNumeroDoc"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^[0-9]{10,11}|[0-9]{3,8}$"
                    Enabled="true"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="revCE" ErrorMessage="*" ControlToValidate="txtNumeroDoc"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^[0-9]{1,6}$"
                    Enabled="false"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="revPA" ErrorMessage="*" ControlToValidate="txtNumeroDoc"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^[A-Za-z0-9]{1,16}$"
                    Enabled="false"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoDoc" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:HiddenField runat="server" ID="hfIdUsuario" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroDoc" Height="25px" Width="235px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroDoc" runat="server" TargetControlID="txtNumeroDoc"
                    FilterType="Numbers,Custom,LowercaseLetters,UppercaseLetters" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblPrimerNombre" runat="server" Text="Primer nombre *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvPrimerNombre" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtPrimerNombre" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblSegundoNombre" runat="server" Text="Segundo nombre"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" Height="22px" Width="224px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" Height="23px" Width="239px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblPrimerApellido" runat="server" Text="Primer apellido *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvPrimerApellido" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtPrimerApellido" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblSegundoApellido" runat="server" Text="Segundo apellido"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" Height="23px" Width="219px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" Height="23px" Width="237px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        --
        <tr class="rowB">
            <td>
                    <asp:Label ID="lblRazonSocial" runat="server" Text="Raz&oacute;n Social"></asp:Label>
            </td>
            <td>
             </td>
        </tr>
        <tr class="rowA">
            <td>
                     <asp:TextBox runat="server" ID="txtRazonSocial" Height="24px" Width="321px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftRazonSocial" runat="server" TargetControlID="txtRazonSocial"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
           </td>
        </tr>
        --
        <tr class="rowB">
            <td>
                <asp:Label ID="lblRol" runat="server" Text="Rol *"></asp:Label>
               <%-- <asp:RequiredFieldValidator runat="server" ID="rfvRol" ErrorMessage="Campo Requerido"
                    ControlToValidate="ddlRol" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvRol" ErrorMessage="Campo Requerido" ControlToValidate="ddlRol"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" Operator="NotEqual" ValueToCompare="Seleccione"
                    ForeColor="Red" Display="Dynamic"></asp:CompareValidator>--%>
            </td>
            <td>
                <asp:Label ID="lblCorreoElectronico" runat="server" Text="Correo electronico *"></asp:Label>
                <asp:RegularExpressionValidator runat="server" ID="revCorreoElectronico" ErrorMessage="*"
                    ControlToValidate="txtCorreoElectronico" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" ValidationExpression="^[0-9a-zA-Z_\-\.]+@[0-9a-zA-Z\-\.]+\.[a-zA-Z]{2,4}$"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator runat="server" ID="rfvCorreoElectronico" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtCorreoElectronico" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <%-- <asp:DropDownList runat="server" ID="ddlRol">
                </asp:DropDownList>--%>

                <asp:ListBox ID="LstRolesUsuario" runat="server"  Width="300px" SelectionMode="Multiple" ></asp:ListBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCorreoElectronico" Width="335px"></asp:TextBox>
                <asp:HiddenField ID="hfCorreoElectronico" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Roles Agregados al usuario
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <%-- <asp:GridView runat="server" ID="gvRolesUsuario" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRole" CellPadding="0" Height="16px"                        
                        OnPageIndexChanging="gvRolesUsuario_PageIndexChanging" >
                        <Columns>                            
                            <asp:BoundField HeaderText="Rol" DataField="Rol"/>                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblNombreUsuario" runat="server" Text="Usuario *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreUsuario" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtNombreUsuario" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblContrasena" runat="server" Text="Contraseña *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvContrasena" ErrorMessage="Campo Requerido"
                    ControlToValidate="txtContrasena" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvContrasena" runat="server" ControlToValidate="txtContrasena"
                    ForeColor="Red" Display="Dynamic" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ErrorMessage="La contraseña debe tener mínimo 8 caracteres de longitud" ClientValidationFunction="ValidaContrasena"></asp:CustomValidator>
                <asp:RegularExpressionValidator runat="server" ID="revContrasena1" ErrorMessage="Requiere por lo menos una letra mayuscula"
                    ToolTip="Contraseña insegura." ControlToValidate="txtContrasena" SetFocusOnError="true"
                    ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^.*[A-Z]+.*$"
                    Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="revContrasena2" ErrorMessage="Requiere por lo menos una letra minuscula"
                    ToolTip="Requiere por lo menos una letra minuscula" ControlToValidate="txtContrasena"
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^.*[a-z]+.*$"
                    Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="revContrasena3" ErrorMessage="Requiere por lo menos un numero"
                    ToolTip="Requiere por lo menos un numero" ControlToValidate="txtContrasena" SetFocusOnError="true"
                    ValidationGroup="btnGuardar" ForeColor="Red" ValidationExpression="^.*[0-9]+.*$"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreUsuario" Height="24px" Width="292px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreUsuario" runat="server" TargetControlID="txtNombreUsuario"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" /> 
                <asp:HiddenField  ID="hfNombreUsuario" runat="server" />              
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtContrasena" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:Label ID="LbTipoUsuario" runat="server" Text="Tipo de usuario *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoUsuario" ErrorMessage="Campo Requerido"
                    ControlToValidate="rblTipoUsuario" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblTipoUsuario" RepeatDirection="Horizontal"
                    AutoPostBack="True" Width="90%" OnSelectedIndexChanged="rblTipoUsuario_SelectedIndexChanged">
                </asp:RadioButtonList>
            </td>
        </tr>
        <asp:Panel ID="Regional" runat="server" Visible="false">
            <tr class="rowB">
                <td colspan="2">
                    Regional *
                    <asp:RequiredFieldValidator runat="server" ID="rfvRegional" ControlToValidate="ddlIdRegional"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvRegional" ControlToValidate="ddlIdRegional"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="false"></asp:CompareValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdRegional">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </asp:Panel>
        <tr class="rowB">
            <td colspan="2">
                <asp:Label ID="lblEstado" runat="server" Text="Estado *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ErrorMessage="Campo Requerido"
                    ControlToValidate="rblEstado" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>

    
    <script type="text/javascript" language="javascript">
        function ValidaContrasena(sender, args) {
            if (args.Value.length > 7)
                args.IsValid = true;
            else
                args.IsValid = false;
        }
    </script>
</asp:Content>
