﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" Async="true"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Seguridad_usuario_Detail" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo de documento
            </td>
            <td>
                Numero de documento
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoDoc" Enabled="false">
                </asp:DropDownList>
                <asp:HiddenField ID="hfId" runat="server"></asp:HiddenField>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroDoc" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer nombre
            </td>
            <td>
                Segundo nombre
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer apellido
            </td>
            <td>
                Segundo apellido
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Correo Electronico
                <%--Rol--%>
            </td>
            <td>
                Usuario
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCorreoElectronico" Enabled="false"></asp:TextBox>
                <%--<asp:DropDownList runat="server" ID="ddlRol" Enabled="false">
                </asp:DropDownList>--%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreUsuario" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                
            </td>
            <td>
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:Button ID="btnReenviarCorreo" runat="server" onclick="reenviarCorreo" 
                    Text="Reenviar Correo" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
         <tr class="rowA">
            <td colspan="2">
                
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Roles Usuario
            </td>

        </tr>
        <tr Class="rowA">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvRolesAsignadoUsuario" AutoGenerateColumns="False" AllowPaging="True"
                        Width="100%" CellPadding="0" GridLines="None">
                        <Columns>
                            
                            <asp:BoundField HeaderText="Roles Asignados" DataField="NombreRol" />
                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
