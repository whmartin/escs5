﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Seguridad_usuario_list" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">
    <%--<asp:ScriptManager ID="smPagina" runat="server"></asp:ScriptManager>--%>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Numero documento:
                </td>
                <td>
                    Rol:
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroDocumento"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftbeNumeroDocumento" runat="server" TargetControlID="txtNumeroDocumento"
                        FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" " />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlRol">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer nombre:
                </td>
                <td>
                    Primer apellido:
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftbePrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                        FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" " />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftbePrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                        FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" " />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado:
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Todos" Value="-1" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowBG">
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvUsuarios" AutoGenerateColumns="False" AllowPaging="True"
                        Width="100%" OnPageIndexChanging="gvUsuarios_PageIndexChanging" OnSelectedIndexChanged="gvUsuarios_SelectedIndexChanged"
                        DataKeyNames="IdUsuario" CellPadding="0" GridLines="None">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        ToolTip="Detalle" Width="16px" Height="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumento" />
                            <asp:BoundField HeaderText="Numero documento" DataField="NumeroDocumento" />
                            <asp:BoundField HeaderText="Primer nombre" DataField="PrimerNombre" />
                            <asp:BoundField HeaderText="Primer apellido" DataField="PrimerApellido" />
                            <asp:BoundField HeaderText="Rol" DataField="Rol" />
                            <asp:BoundField HeaderText="Fecha creacion" DataField="FechaCreacion" />
                            <asp:BoundField HeaderText="Usuario" DataField="UsuarioCreacion" />
                            <asp:CheckBoxField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
