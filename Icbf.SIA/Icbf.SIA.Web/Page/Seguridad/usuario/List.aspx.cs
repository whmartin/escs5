﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Business;
using System.Web.Security;
using Icbf.SIA.Service;

/// <summary>
/// Página que despliega la consulta basada en filtros de usuarios que se loguean en la aplicación
/// </summary>
public partial class Page_Seguridad_usuario_list : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vRUBOService = new SIAService();
    SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, "Seguridad/usuario", SolutionPage.List))
        {
            if (!Page.IsPostBack)
            {
                if (GetSessionParameter("usuario.Eliminado").ToString() == "1")
                    toolBar.MostrarMensajeEliminado();
                RemoveSessionParameter("usuario.Eliminado");

                LLenarPerfil();
                LLenarRol();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }
    protected void gvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsuarios.PageIndex = e.NewPageIndex;
        Buscar();
    }
    protected void gvUsuarios_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvUsuarios.SelectedRow);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Usuario", SolutionPage.List.ToString());

            gvUsuarios.PageSize = PageSize();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNumeroDocumento = null;
            String vPrimerNombre = null;
            String vPrimerApellido = null;
            String vPerfil = null;
            //String vRol = null;
            Boolean? vEstado = null;

            vNumeroDocumento = txtNumeroDocumento.Text.Trim();
            
            if (rblEstado.SelectedValue == "1")
                vEstado = true;
            if (rblEstado.SelectedValue == "0")
                vEstado = false;
            vPrimerNombre = txtPrimerNombre.Text.Trim();
            vPrimerApellido = txtPrimerApellido.Text.Trim();

            List<Usuario> vListaUsuarios = vRUBOService.ConsultarUsuarios(vNumeroDocumento, vPrimerNombre, vPrimerApellido, Convert.ToInt32(vPerfil), vEstado);
            List<Usuario> vListaUsuariosFinal = new List<Usuario>();

            if (ddlRol.SelectedValue != "Seleccione")
            {
                var vUsuarioRol = Roles.GetUsersInRole(ddlRol.SelectedValue);

                List<String> UsuarioPK = (from item in vUsuarioRol select Membership.GetUser(item) into membershipUser where membershipUser != null select membershipUser.ProviderUserKey.ToString()).ToList();


                //vListaUsuarios = vListaUsuarios.Where(x => ((x.NombreUsuario != null) && (x.NombreUsuario != string.Empty))).ToList();
                                
                vListaUsuariosFinal = vListaUsuarios.Where(x => UsuarioPK.Contains(x.Providerkey)).ToList();

                //foreach (Usuario vUsuario in vListaUsuarios)
                //{
                //    string[] vRoles = Roles.FindUsersInRole(ddlRol.SelectedValue, vUsuario.NombreUsuario);
                //    if (vRoles.Length != 0)
                //    {
                //        vListaUsuariosFinal.Add(vUsuario);

                //    }
                //}

            }
            else
            {
                vListaUsuariosFinal = vListaUsuarios;
            }

            gvUsuarios.DataSource = vListaUsuariosFinal;
            gvUsuarios.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvUsuarios.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Usuario.Id", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Llena perfil
    /// </summary>
    private void LLenarPerfil()
    {
        
    }

    /// <summary>
    /// Llena rol
    /// </summary>
    private void LLenarRol()
    {
        List<Rol> vLRol = vSeguridadService.ConsultarRoles("", true);
        vLRol.Insert(0, new Rol() { NombreRol = "Seleccione" });
        ddlRol.DataSource = vLRol;
        ddlRol.DataTextField = "NombreRol";
        ddlRol.DataValueField = "NombreRol";
        ddlRol.DataBind();
    }
}