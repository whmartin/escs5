﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Business;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición de usuarios de la aplicación
/// </summary>
public partial class Page_Seguridad_usuario_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadService = new SeguridadService();
    SIAService vRUBOService = new SIAService();
    string PageName = "Seguridad/usuario";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                    rfvContrasena.Visible = false;
                }
                else
                    rblEstado.Enabled = false;
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            vSeguridadService = new SeguridadService();

            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);


            toolBar.EstablecerTitulos("Usuario", SolutionPage.Add.ToString());

            LLenarTipoDocumento();
            LLenarPerfil();
            LLenarRol();
            LLenarTipoUsuario();
            //LLenarCombo(vDominio, ddlOperador);
            ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vRUBOService.ConsultarRegionals(null, null),
                                                 "IdRegional", "NombreRegional");
            //Controles.LlenarRegionales(ddlIdRegional, null, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Usuario vUsuario = new Usuario();

            vUsuario.NumeroDocumento = txtNumeroDoc.Text.Trim();
            vUsuario.PrimerNombre = txtPrimerNombre.Text.Trim();
            vUsuario.SegundoNombre = txtSegundoNombre.Text.Trim();
            vUsuario.PrimerApellido = txtPrimerApellido.Text.Trim();
            vUsuario.SegundoApellido = txtSegundoApellido.Text.Trim();
            vUsuario.CorreoElectronico = txtCorreoElectronico.Text.Trim();
            vUsuario.NombreUsuario = txtNombreUsuario.Text.Trim();
            vUsuario.Contrasena = txtContrasena.Text;            
            vUsuario.IdTipoDocumento = Convert.ToInt32(ddlTipoDoc.SelectedValue);
            vUsuario.RazonSocial = txtRazonSocial.Text.Trim();
            string vRolesUsuario = LstRolesUsuario.Items.Cast<ListItem>().Where(item => item.Selected)
                                                .Aggregate<ListItem, string>(null, (current, item) => current + (item.Value + ";"));

            if (vRolesUsuario == null)
                throw new Exception("Debe seleccionar un rol para continuar");
            //if (indicadorRecurso != null)
            //{
            //    indicarPresupuestal = indicarPresupuestal.Substring(0, indicarPresupuestal.Length);
            //}
            vUsuario.Rol = vRolesUsuario;

            if (rblEstado.SelectedValue == "1")
                vUsuario.Estado = true;
            if (rblEstado.SelectedValue == "0")
                vUsuario.Estado = false;

            vUsuario.IdTipoUsuario = Convert.ToInt32(rblTipoUsuario.SelectedValue);

            //Si es EC
            //if (rblTipoUsuario.SelectedValue == "3")
            //{
            //    vUsuario.IdEntidadContratista = Convert.ToInt32(ddlEntidadContratista.Value);
            //    vUsuario.IdRegional = null;
            //}

            //Si es Regional
            if (!ddlIdRegional.SelectedValue.Equals("-1") && rblTipoUsuario.SelectedValue == "2")
            {
                vUsuario.IdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
                vUsuario.IdEntidadContratista = null;
            }

            if (rblTipoUsuario.SelectedValue == "1")
            {
                vUsuario.IdEntidadContratista = null;
                vUsuario.IdRegional = null;
            }

            InformacionAudioria(vUsuario, this.PageName, vSolutionPage);

            if (Request.QueryString["oP"] == "E")
            {
                vUsuario.IdUsuario = Convert.ToInt32(hfIdUsuario.Value);
                vUsuario.UsuarioModificacion = GetSessionUser().NombreUsuario.ToString();
                vUsuario.IdTipoPersona = Convert.ToInt32(GetSessionParameter("Usuario.IdTipoPersona"));
                vUsuario.IdTipoDocumento = Convert.ToInt32(GetSessionParameter("Usuario.IdTipoDocumento"));
                vUsuario.RazonSocial = txtRazonSocial.Text.Trim();
                vUsuario.TelefonoContacto = GetSessionParameter("Usuario.TelefonoContacto").ToString();
                vUsuario.DV = GetSessionParameter("Usuario.DV").ToString();
                
                bool existeCorreoElectronico = false;
                MembershipUserCollection oMuColection = Membership.FindUsersByEmail(txtCorreoElectronico.Text.Trim());
                if (oMuColection.Count > 0) { existeCorreoElectronico = true; }             

                //sumary
                //ajuste para permitir cambio de correo de usuario comprobando si existe en el sistema
                //Néstor Amaya
                if (vRolesUsuario.Split(new char[] { ';' }).Contains("PROVEEDORES") && vUsuario.NombreUsuario.ToLower().Equals(hfNombreUsuario.Value.ToLower().ToString()))
                {
                    if (txtCorreoElectronico.Text.ToLower() != hfNombreUsuario.Value.ToString().ToLower())
                    {                       
                        //NavigateTo(SolutionPage.Detail);
                        if (existeCorreoElectronico == true)
                        {
                            toolBar.MostrarMensajeError("El correo electrónico  " + txtCorreoElectronico.Text.Trim() + "  ya existe en el sistema");
                            return;
                        }

                        vResultado = vRUBOService.modificarCorreoUsuario(vUsuario);
                        vUsuario.NombreUsuario = vUsuario.CorreoElectronico;
                    }

                }
                
                vResultado = vRUBOService.ModificarUsuario(vUsuario);
            }
            else
            {
                vUsuario.UsuarioCreacion = GetSessionUser().NombreUsuario.ToString();
                vResultado = vRUBOService.InsertarUsuario(vUsuario);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Usuario.Id", vUsuario.IdUsuario);
                SetSessionParameter("Usuario.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
                RemoveSessionParameter("Usuario.IdTipoPersona");
                RemoveSessionParameter("Usuario.IdTipoDocumento");
                RemoveSessionParameter("Usuario.TelefonoContacto");
                RemoveSessionParameter("Usuario.DV");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            string vIdUsuario = Convert.ToString(GetSessionParameter("Usuario.Id"));
            
            //RemoveSessionParameter("Usuario.Id");

            Usuario vUsuario = new Usuario();
            vUsuario = vRUBOService.ConsultarUsuario(Convert.ToInt32(vIdUsuario));
            
            int TipoPersona = vUsuario.IdTipoPersona;
            SetSessionParameter("Usuario.IdTipoPersona", vUsuario.IdTipoPersona);
            int TipoDoc = vUsuario.IdTipoDocumento;
            SetSessionParameter("Usuario.IdTipoDocumento", vUsuario.IdTipoDocumento);
            string TelefonoContacto = vUsuario.TelefonoContacto;
            SetSessionParameter("Usuario.TelefonoContacto", vUsuario.TelefonoContacto);
            string DV = vUsuario.DV;
            SetSessionParameter("Usuario.DV", vUsuario.DV);
            
            //sumary
            //ajuste para permitir edicion de un usuario cuando es persona juridica
            //Zulma Barrantes

            if (vUsuario.IdTipoPersona == 2)
            {
                hfIdUsuario.Value = vIdUsuario;
                txtNumeroDoc.Text = vUsuario.NumeroDocumento;
                txtPrimerNombre.Text = "";
                txtSegundoNombre.Text = "";
                txtPrimerApellido.Text = "";
                txtSegundoApellido.Text = "";
                txtCorreoElectronico.Text = vUsuario.CorreoElectronico;
                txtNombreUsuario.Text = vUsuario.NombreUsuario;
                txtRazonSocial.Text = vUsuario.RazonSocial;
                ddlTipoDoc.SelectedValue = vUsuario.IdTipoDocumento.ToString(); 
                
                lblPrimerNombre.Visible = false;
                txtPrimerNombre.Visible = false;
                lblSegundoNombre.Visible = false;
                txtSegundoNombre.Visible = false;
                lblPrimerApellido.Visible = false;
                txtPrimerApellido.Visible = false;
                lblSegundoApellido.Visible = false;
                txtSegundoApellido.Visible = false;
                ddlTipoDoc.Enabled = false;
            }

            else
            {
                hfIdUsuario.Value = vIdUsuario;
                txtNumeroDoc.Text = vUsuario.NumeroDocumento;
                txtPrimerNombre.Text = vUsuario.PrimerNombre;
                txtSegundoNombre.Text = vUsuario.SegundoNombre;
                txtPrimerApellido.Text = vUsuario.PrimerApellido;
                txtSegundoApellido.Text = vUsuario.SegundoApellido;
                txtCorreoElectronico.Text = vUsuario.CorreoElectronico;
                txtNombreUsuario.Text = vUsuario.NombreUsuario;
                ddlTipoDoc.SelectedValue = vUsuario.IdTipoDocumento.ToString();
                //ddlRol.SelectedValue = vUsuario.Rol.ToString().ToUpper();
                lblRazonSocial.Visible = false;
                txtRazonSocial.Visible = false;
            }

            string[] rolesUsuario = Roles.GetRolesForUser(vUsuario.NombreUsuario);

            int i = 0;
            foreach (string itemRol in rolesUsuario)
            {
                rolesUsuario[i] = itemRol.ToUpper();
                i++;
            }


            foreach (ListItem item in LstRolesUsuario.Items)
            {                
                if (rolesUsuario.Contains(item.Value.ToUpper()))
                    item.Selected = true;               
            }

            hfNombreUsuario.Value = vUsuario.CorreoElectronico.ToString();
    
            rblTipoUsuario.SelectedValue = vUsuario.IdTipoUsuario.ToString();

            if (vUsuario.IdTipoUsuario.ToString() == "2")
            {
                Regional.Visible = true;
                if (vUsuario.IdRegional != null)
                    ddlIdRegional.SelectedValue = vUsuario.IdRegional.ToString();
            }
            else if (vUsuario.IdTipoUsuario.ToString() == "3")
            {
                //EntidadContratista.Visible = true;
                //ddlEntidadContratista.Value = vUsuario.IdEntidadContratista.ToString();
                //EntidadContratista vEntidadContratista = new EntidadContratista();
                //if (vUsuario.IdEntidadContratista != null)
                //{
                //    vEntidadContratista = vRUBOService.ConsultarEntidadContratista(Convert.ToInt32(vUsuario.IdEntidadContratista.ToString()));
                //    txtNumeroDocumento.Text = vEntidadContratista.Identificacion.ToString();
                //    txtNombreEntidadContratista.Text = vEntidadContratista.NombreEntidadContratista.ToString();
                //}
            }

            CambiarValidadorDocumentoUsuario();

            if (vUsuario.Estado)
                rblEstado.SelectedValue = "1";
            else
                rblEstado.SelectedValue = "0";

            txtNombreUsuario.Enabled = false;

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vUsuario.UsuarioCreacion, vUsuario.FechaCreacion, vUsuario.UsuarioModificacion, vUsuario.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void LLenarPerfil()
    {

    }

    /// <summary>
    /// Llena rol
    /// </summary>
    private void LLenarRol()
    {
        List<Rol> vLRol = vSeguridadService.ConsultarRoles("", true);
        foreach (Rol vRol in vLRol)
        {
            vRol.NombreRol = vRol.NombreRol.ToUpper();
        }
        //vLRol.Insert(0, new Rol() { NombreRol = "Seleccione" });
        //ddlRol.DataSource = vLRol;
        //ddlRol.DataTextField = "NombreRol";
        //ddlRol.DataValueField = "NombreRol";
        //ddlRol.DataBind();

        LstRolesUsuario.DataSource = vLRol;
        LstRolesUsuario.DataTextField = "NombreRol";
        LstRolesUsuario.DataValueField = "NombreRol";
        LstRolesUsuario.DataBind();
    }

    /// <summary>
    /// Llena tipo documento
    /// </summary>
    private void LLenarTipoDocumento()
    {
        List<TipoDocumento> vLTipoDoc = vRUBOService.ConsultarTodosTipoDocumento();
        foreach (TipoDocumento tD in vLTipoDoc)
        {
            if ((tD.Codigo == "CC") || (tD.Codigo == "CE") || (tD.Codigo == "PA") || (tD.Codigo == "RC") || (tD.Codigo == "NIT") || (tD.Codigo == "RUT"))
            {
                ddlTipoDoc.Items.Add(new ListItem(tD.Codigo, tD.IdTipoDocumento.ToString()));
            }
        }
        ddlTipoDoc.Items.Insert(0, new ListItem("Seleccione", "-1"));
    }

    /// <summary>
    /// Cambia validadores de acuerdo al tipo de documento
    /// </summary>
    private void CambiarValidadorDocumentoUsuario()
    {
        if (ddlTipoDoc.SelectedItem.Text == "CC")
        {
            revCC.Enabled = true;
            revCE.Enabled = false;
            revPA.Enabled = false;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "CE")
        {
            revCE.Enabled = true;
            revCC.Enabled = false;
            revPA.Enabled = false;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "PA")
        {
            revPA.Enabled = true;
            revCE.Enabled = false;
            revCC.Enabled = false;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "RC")
        {
            revPA.Enabled = true;
            revCE.Enabled = false;
            revCC.Enabled = false;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "NIT")
        {
            revPA.Enabled = true;
            revCE.Enabled = false;
            revCC.Enabled = false;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "RUT")
        {
            revPA.Enabled = true;
            revCE.Enabled = false;
            revCC.Enabled = false;
        }
    }

    private void LLenarTipoUsuario()
    {
        List<TipoUsuario> vLTipoUsuario = vRUBOService.ConsultarTipoUsuarios(null, null, "A");
        //foreach (TipoUsuario vTipoUsuario in vLTipoUsuario)
        //{
        //    vTipoUsuario.NombreTipoUsuario = vTipoUsuario.NombreTipoUsuario.ToUpper();
        //}
        rblTipoUsuario.DataSource = vLTipoUsuario;
        rblTipoUsuario.DataTextField = "NombreTipoUsuario";
        rblTipoUsuario.DataValueField = "IdTipoUsuario";
        rblTipoUsuario.DataBind();
    }

    protected void rblTipoUsuario_SelectedIndexChanged(object sender, EventArgs e)
    {
        String value = rblTipoUsuario.SelectedValue.ToString();
        if (value == "2")
        {
            ddlIdRegional.SelectedValue = "-1";
            Regional.Visible = true;
            //EntidadContratista.Visible = false;
            cvRegional.Enabled = true;
            rfvRegional.Enabled = true;
        }
        else if (value == "3")
        {
            //    ddlEntidadContratista.Value = "";
            //    txtNumeroDocumento.Text = "";
            //    txtNombreEntidadContratista.Text = "";
            //    Regional.Visible = false;
            //    EntidadContratista.Visible = true;
            //    rfvEntidadContratista.Enabled = true;
            //    rfvEntidadContratistaDos.Enabled = true;
        }
        else
        {
            Regional.Visible = false;
            //EntidadContratista.Visible = false;
        }
    }
}