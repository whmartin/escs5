﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Business;

/// <summary>
/// Página de cambio de clave
/// </summary>
public partial class Page_Seguridad_CambioClave_List : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

       

        if (ValidateAccess(toolBar, "Seguridad/CambioClave", vSolutionPage))
        {
            if (!Page.IsPostBack)
            {

            }
        }
    }
    protected void ChangePassword1_ContinueButtonClick(object sender, EventArgs e)
    {
        string Pagina = "~/Page/Seguridad/CambioClave/List.aspx";
        NavigateTo(Pagina);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            vSeguridadFAC = new SeguridadService(); 

            toolBar.EstablecerTitulos("Cambio de Clave", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}