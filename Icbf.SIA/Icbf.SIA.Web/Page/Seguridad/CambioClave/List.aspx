﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Seguridad_CambioClave_List" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="server">
    <asp:ChangePassword ID="ChangePassword1" runat="server" Width="100%" CancelButtonText="Cancelar"
        ChangePasswordButtonText="Cambiar" ChangePasswordTitleText="" ConfirmNewPasswordLabelText="Confirmar Nueva contraseña"
        NewPasswordLabelText="Nueva contraseña" PasswordLabelText="Contraseña" ContinueButtonText="Continuar"
        ConfirmPasswordCompareErrorMessage="La nueva confirmación de la nueva contraseña no coincide"
        NewPasswordRegularExpressionErrorMessage="Ingrese una contraseña diferente" NewPasswordRequiredErrorMessage="Nueva contraseña es requerida"
        PasswordRequiredErrorMessage="Contraseña actual es requerida." SuccessText="La contraseña ha sido cambiada!"
        SuccessTitleText="" ChangePasswordFailureText="Contraeña incorrecta o Nueva contraseña invalida"
        ConfirmPasswordRequiredErrorMessage="Confirmar nueva contraseña es requerido"
        OnContinueButtonClick="ChangePassword1_ContinueButtonClick" CancelButtonType="Image"
        ChangePasswordButtonType="Image" ContinueButtonType="Image" CancelButtonImageUrl="../../../Image/btn/delete.gif"
        ChangePasswordButtonImageUrl="../../../Image/btn/apply.png" ContinueButtonImageUrl="../../../Image/btn/apply.png"
        CancelButtonStyle-Height="29px" CancelButtonStyle-Width="29px" ChangePasswordButtonStyle-Height="29px"
        ChangePasswordButtonStyle-Width="29px" ContinueButtonStyle-Height="29px" ContinueButtonStyle-Width="29px"
        TitleTextStyle-CssClass="sT" FailureTextStyle-ForeColor="Red">
        <CancelButtonStyle Height="29px" Width="29px"></CancelButtonStyle>
        <ChangePasswordButtonStyle Height="29px" Width="29px"></ChangePasswordButtonStyle>
        <ChangePasswordTemplate>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td colspan="2">
                        <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Contraseña</asp:Label>
                        <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                            ErrorMessage="Campo Requerido" ForeColor="Red" ToolTip="Contraseña actual es requerida."
                            ValidationGroup="ChangePassword1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">Nueva contraseña</asp:Label>
                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                            ErrorMessage="Campo Requerido" ToolTip="Nueva contraseña es requerida" ValidationGroup="ChangePassword1"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirmar Nueva contraseña</asp:Label>
                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                            ErrorMessage="Campo Requerido" ToolTip="Confirmar nueva contraseña es requerido"
                            ValidationGroup="ChangePassword1" ForeColor="Red"></asp:RequiredFieldValidator>

                                                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                            ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="La nueva confirmación de la nueva contraseña no coincide"
                            ValidationGroup="ChangePassword1" ForeColor="Red"></asp:CompareValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td colspan="2">
                        <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                 <tr class="rowB">
                    <td colspan="2">
                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:ImageButton ID="ChangePasswordImageButton" runat="server" AlternateText="Cambiar"
                            CommandName="ChangePassword" Height="29px" ImageUrl="../../../Image/btn/apply.png"
                            ValidationGroup="ChangePassword1" Width="29px" />
                    </td>
                    <td>
                        <asp:ImageButton ID="CancelImageButton" runat="server" AlternateText="Cancelar" CausesValidation="False"
                            CommandName="Cancel" Height="29px" ImageUrl="../../../Image/btn/delete.gif" Width="29px" />
                    </td>
                </tr>
            </table>
        </ChangePasswordTemplate>
        <ContinueButtonStyle Height="29px" Width="29px"></ContinueButtonStyle>
        <TitleTextStyle CssClass="sT"></TitleTextStyle>
    </asp:ChangePassword>
</asp:Content>
