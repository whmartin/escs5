<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ProgramaFuncion_Detail" %>

<%@ Register Src="~/General/General/Control/confirmMessage.ascx" TagName="confirm" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProgramaFuncion" runat="server" />
    <uc2:confirm ID="ctrConfirmMessage" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblPrograma" runat="server" Text="Programa:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblNombreFuncion" runat="server" Text="Funci&oacuten:"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlProgramas" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtNombreFuncion" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Estado
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
