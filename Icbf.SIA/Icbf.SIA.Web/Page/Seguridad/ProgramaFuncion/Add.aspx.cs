//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncion_Add.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>27/01/2016 0200 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase parcial para gestionar el Add del Page de ProgramaFuncion
/// </summary>
public partial class Page_ProgramaFuncion_Add : GeneralWeb
{
    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Referencia a SeguridadService
    /// </summary>
    private SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Variable global del path de la página
    /// </summary>
    private string vPageName = "Seguridad/ProgramaFuncion";

    #region Eventos

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if ( Request.QueryString["oP"] == "E" )
            vSolutionPage = SolutionPage.Edit;

        if ( ValidateAccess(toolBar, this.vPageName, vSolutionPage) )
        {
            if ( !Page.IsPostBack )
            {
                CargarDatosIniciales();
                if ( Request.QueryString["oP"] == "E" )
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Evento asociado al Click del Botón Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Evento asociado al Click del Botón Nuevo
    /// </summary>
    /// <param name="sender">The btnNuevo</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento asociado al Click del Botón Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    #endregion "Eventos"

    #region Metodos

    /// <summary>
    /// Método para la Funcionalidad de búsqueda
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();

            vProgramaFuncion.IdPrograma = Convert.ToInt32(ddlProgramas.SelectedValue);
            vProgramaFuncion.NombreFuncion = txtNombreFuncion.Text;
            vProgramaFuncion.Estado = rblEstado.SelectedValue == "1" ? true : false;
            InformacionAudioria(vProgramaFuncion, this.vPageName, vSolutionPage);

            if ( Request.QueryString["oP"] == "E" )
            {
                vProgramaFuncion.IdProgramaFuncion = Convert.ToInt32(hfIdProgramaFuncion.Value);
                vProgramaFuncion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProgramaFuncion, this.vPageName, vSolutionPage);
                vResultado = vSeguridadService.ModificarProgramaFuncion(vProgramaFuncion);
            }
            else
            {
                vProgramaFuncion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProgramaFuncion, this.vPageName, vSolutionPage);
                vResultado = vSeguridadService.InsertarProgramaFuncion(vProgramaFuncion);
            }

            if ( vResultado == 0 )
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if ( vResultado == 1 )
            {
                SetSessionParameter("ProgramaFuncion.IdProgramaFuncion", vProgramaFuncion.IdProgramaFuncion);
                SetSessionParameter("ProgramaFuncion.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if ( vResultado == -1 )
            {
                this.toolBar.MostrarMensajeError("Registro ya Existente para este Programa.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los controles y eventos iniciales
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.SetSaveConfirmation(this.ctrConfirmMessage.GetConfirmationFunction());

            toolBar.EstablecerTitulos("Programa Funci&oacuten", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos de un Programa Funcion por el id
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdProgramaFuncion = Convert.ToInt32(GetSessionParameter("ProgramaFuncion.IdProgramaFuncion"));
            RemoveSessionParameter("ProgramaFuncion.IdProgramaFuncion");

            if ( GetSessionParameter("ProgramaFuncion.Guardado").ToString() == "1" )
                toolBar.MostrarMensajeGuardado();

            RemoveSessionParameter("ProgramaFuncion");
            RemoveSessionParameter("ProgramaFuncion.Guardado");

            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
            vProgramaFuncion = vSeguridadService.ConsultarProgramaFuncion(vIdProgramaFuncion);
            hfIdProgramaFuncion.Value = vProgramaFuncion.IdProgramaFuncion.ToString();
            ddlProgramas.SelectedValue = vProgramaFuncion.IdPrograma.ToString();
            txtNombreFuncion.Text = vProgramaFuncion.NombreFuncion;
            rblEstado.SelectedValue = vProgramaFuncion.Estado ? "1" : "0";
            ObtenerAuditoria(vPageName, hfIdProgramaFuncion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProgramaFuncion.UsuarioCrea, vProgramaFuncion.FechaCrea, vProgramaFuncion.UsuarioModifica, vProgramaFuncion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if ( GetSessionParameter("ProgramaFuncion.Eliminado").ToString() == "1" )
                toolBar.MostrarMensajeEliminado();

            RemoveSessionParameter("ProgramaFuncion.Eliminado");

            ddlProgramas.DataSource = vSeguridadService.ConsultarProgramasDenuncias();
            ddlProgramas.DataTextField = "NombrePrograma";
            ddlProgramas.DataValueField = "IdPrograma";
            ddlProgramas.DataBind();

            ddlProgramas.Items.Insert(0, new ListItem("Seleccione", "-1"));

            this.rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            this.rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            this.rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion "Metodos"
}
