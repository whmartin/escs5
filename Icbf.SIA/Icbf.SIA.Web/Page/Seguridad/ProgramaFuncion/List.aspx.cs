//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncion_List.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>27/01/2016 0200 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase parcial para gestionar el List del Page de ProgramaFuncion
/// </summary>
public partial class Page_ProgramaFuncion_List : GeneralWeb
{
    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Variable global del path de la p�gina
    /// </summary>
    private string vPageName = "Seguridad/ProgramaFuncion";

    /// <summary>
    /// Referencia a SeguridadService
    /// </summary>
    private SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if ( ValidateAccess(toolBar, vPageName, vSolutionPage) )
        {
            if ( !Page.IsPostBack )
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento asociado al Click del Bot�n Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Evento asociado al Click del Bot�n Nuevo
    /// </summary>
    /// <param name="sender">The btnNuevo</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// M�todo para la Funcionalidad de b�squeda
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdPrograma = null;
            string vNombreFuncion = null;
            int? vEstado = null;
            if ( ddlProgramas.SelectedValue!= "-1" )
            {
                vIdPrograma = Convert.ToInt32(ddlProgramas.SelectedValue);
            }
            else
            {
                vIdPrograma = -1;
            }

            if ( txtNombreFuncion.Text!= string.Empty )
            {
                vNombreFuncion = (txtNombreFuncion.Text.Trim());
            }
            else
            {
                vNombreFuncion = "-1";
            }

            if ( rblEstado.SelectedValue!= "-1" )
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            else
            {
                vEstado = -1;
            }

            gvProgramasFuncion.DataSource = vSeguridadService.ConsultarProgramaFuncions(vIdPrograma, vNombreFuncion, vEstado);
            gvProgramasFuncion.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los controles iniciales
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvProgramasFuncion.PageSize = PageSize();
            gvProgramasFuncion.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Programa Funci&oacuten", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para la Selecc�n de Registro
    /// </summary>
    /// <param name="pRow">Fila de la grilla</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProgramasFuncion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ProgramaFuncion.IdProgramaFuncion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento para la selecci�n de fila grilla de Programa Funcion
    /// </summary>
    /// <param name="sender">The gvProgramaFuncion</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvProgramaFuncion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProgramasFuncion.SelectedRow);
    }

    /// <summary>
    /// Evento para la paginaci�n de la grilla Programa Funcion
    /// </summary>
    /// <param name="sender">The gvProgramaFuncion</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvProgramaFuncion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProgramasFuncion.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// M�todo para cargar los datos en los controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if ( GetSessionParameter("ProgramaFuncion.Eliminado").ToString() == "1" )
                toolBar.MostrarMensajeEliminado();

            RemoveSessionParameter("ProgramaFuncion.Eliminado");

            ddlProgramas.DataSource = vSeguridadService.ConsultarProgramasDenuncias();
            ddlProgramas.DataTextField = "NombrePrograma";
            ddlProgramas.DataValueField = "IdPrograma";
            ddlProgramas.DataBind();

            ddlProgramas.Items.Insert(0, new ListItem("Seleccione", "-1"));

            this.rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            this.rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            this.rblEstado.Items.Insert(2, new ListItem("Todos", "-1"));
            this.rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
