//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncion_Detail.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>27/01/2016 0200 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase parcial para gestionar el Detail del Page de ProgramaFuncion
/// </summary>
public partial class Page_ProgramaFuncion_Detail : GeneralWeb
{
    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Variable global del path de la página
    /// </summary>
    private string vPageName = "Seguridad/ProgramaFuncion";

    /// <summary>
    /// Referencia a SeguridadService
    /// </summary>
    private SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if ( ValidateAccess(toolBar, this.vPageName, vSolutionPage) )
        {
            if ( !Page.IsPostBack )
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Evento asociado al Click del Botón Nuevo
    /// </summary>
    /// <param name="sender">The btnNuevo</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento asociado al Click del Botón Editar
    /// </summary>
    /// <param name="sender">The btnEditar</param>
    /// <param name="e">The Click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ProgramaFuncion.IdProgramaFuncion", hfIdProgramaFuncion.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento asociado al Click del Botón Eliminar
    /// </summary>
    /// <param name="sender">The btnElimina</param>
    /// <param name="e">The Click</param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Evento asociado al Click del Botón Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método para cargar los datos de un ProgramaFuncion por el id
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdProgramaFuncion = Convert.ToInt32(GetSessionParameter("ProgramaFuncion.IdProgramaFuncion"));
            RemoveSessionParameter("ProgramaFuncion.IdProgramaFuncion");
            
            if ( GetSessionParameter("ProgramaFuncion.Guardado").ToString() == "1" )
                toolBar.MostrarMensajeGuardado();

            RemoveSessionParameter("ProgramaFuncion");
            RemoveSessionParameter("ProgramaFuncion.Guardado");

            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
            vProgramaFuncion = vSeguridadService.ConsultarProgramaFuncion(vIdProgramaFuncion);
            hfIdProgramaFuncion.Value = vProgramaFuncion.IdProgramaFuncion.ToString();
            ddlProgramas.SelectedValue = vProgramaFuncion.IdPrograma.ToString();
            txtNombreFuncion.Text = vProgramaFuncion.NombreFuncion;
            rblEstado.SelectedValue = vProgramaFuncion.Estado ? "1" : "0";
            ObtenerAuditoria(vPageName, hfIdProgramaFuncion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProgramaFuncion.UsuarioCrea, vProgramaFuncion.FechaCrea, vProgramaFuncion.UsuarioModifica, vProgramaFuncion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para eliminar los datos de un ProgramaFuncion por el id
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdProgramaFuncion = Convert.ToInt32(hfIdProgramaFuncion.Value);

            ProgramaFuncion vProgramaFuncion = new ProgramaFuncion();
            vProgramaFuncion = vSeguridadService.ConsultarProgramaFuncion(vIdProgramaFuncion);
            InformacionAudioria(vProgramaFuncion, this.vPageName, vSolutionPage);
            int vResultado = vSeguridadService.EliminarProgramaFuncion(vProgramaFuncion);
            if ( vResultado == 0 )
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if ( vResultado == 1 )
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ProgramaFuncion.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los controles y eventos iniciales
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Programa Funci&oacuten", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos en los controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if ( GetSessionParameter("ProgramaFuncion.Eliminado").ToString() == "1" )
                toolBar.MostrarMensajeEliminado();

            RemoveSessionParameter("ProgramaFuncion.Eliminado");

            ddlProgramas.DataSource = vSeguridadService.ConsultarProgramasDenuncias();
            ddlProgramas.DataTextField = "NombrePrograma";
            ddlProgramas.DataValueField = "IdPrograma";
            ddlProgramas.DataBind();

            ddlProgramas.Items.Insert(0, new ListItem("Seleccione", "-1"));

            this.rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            this.rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            this.rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
