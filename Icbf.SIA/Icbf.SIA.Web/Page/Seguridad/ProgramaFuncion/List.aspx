<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ProgramaFuncion_List" %>




<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" src="../../../Scripts/Utilidades.js"></script>
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    <asp:Label ID="lblPrograma" runat="server" Text="Programa:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblNombreFuncion" runat="server" Text="Funci&oacuten:"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlProgramas"></asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtNombreFuncion" runat="server" MaxLength="250" onkeypress="return CheckLength(250,this,event);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftProgramaFuncion" runat="server" TargetControlID="txtNombreFuncion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="������������ .,@_():;" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Estado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowBG">
                <td>&nbsp;
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProgramasFuncion" AutoGenerateColumns="false" AllowPaging="True"
                        Width="100%" DataKeyNames="IdProgramaFuncion" CellPadding="0" GridLines="None" OnPageIndexChanging="gvProgramaFuncion_PageIndexChanging"
                        OnSelectedIndexChanged="gvProgramaFuncion_SelectedIndexChanged" AllowSorting="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Programa" DataField="NombrePrograma" SortExpression="NombrePrograma" />
                            <asp:BoundField HeaderText="Funcion" DataField="NombreFuncion" SortExpression="NombreFuncion" />
                            <asp:CheckBoxField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
