<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ProgramaFuncion_Add" %>

<%@ Register Src="~/General/General/Control/confirmMessage.ascx" TagName="confirm" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <script type="text/javascript" src="../../../Scripts/Utilidades.js"></script>
    <asp:HiddenField ID="hfIdProgramaFuncion" runat="server" />
    <uc2:confirm ID="ctrConfirmMessage" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblPrograma" runat="server" Text="Programa: *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvPrograma" ControlToValidate="ddlProgramas"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvPrograma" ControlToValidate="ddlProgramas"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                <asp:Label ID="lblNombreFuncion" runat="server" Text="Funci&oacuten: *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvnombreFuncion" ControlToValidate="txtNombreFuncion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr> 
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlProgramas"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtNombreFuncion" runat="server" MaxLength="250"  onkeypress="return CheckLength(250,this,event);"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Estado
                    <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                        SetFocusOnError="true" ErrorMessage="Campo Obligatorio" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
