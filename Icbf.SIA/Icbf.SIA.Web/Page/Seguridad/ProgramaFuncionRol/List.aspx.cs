//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncionRol_List.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>27/01/2016 0200 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Clase parcial para gestionar el List del Page de ProgramaFuncionRol
/// </summary>
public partial class Page_ProgramaFuncionRol_List : GeneralWeb
{
    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Variable global del path de la p�gina
    /// </summary>
    private string vPageName = "Seguridad/ProgramaFuncionRol";

    /// <summary>
    /// Referencia a SeguridadService
    /// </summary>
    private SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if ( ValidateAccess(toolBar, vPageName, vSolutionPage) )
        {
            if ( !Page.IsPostBack )
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Evento asociado al Click del Bot�n Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Evento asociado al Click del Bot�n Nuevo
    /// </summary>
    /// <param name="sender">The btnNuevo</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// M�todo para la Funcionalidad de b�squeda
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdPrograma = null;
            int? vIdProgramaFuncion = null;
            int? vIdRol = null;
            int? vEstado = null;

            if ( ddlProgramas.SelectedValue!= "-1" )
            {
                vIdPrograma = Convert.ToInt32(ddlProgramas.SelectedValue);
            }
            else
            {
                vIdPrograma = -1;
            }

            if ( ddlFuncion.SelectedValue != "-1" )
            {
                vIdProgramaFuncion = Convert.ToInt32(ddlFuncion.SelectedValue);
            }
            else
            {
                vIdProgramaFuncion = -1;
            }

            if ( ddlRol.SelectedValue != "-1" )
            {
                vIdRol = Convert.ToInt32(ddlRol.SelectedValue);
            }
            else
            {
                vIdRol = -1;
            }

            if ( rblEstado.SelectedValue!= "-1" )
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            else
            {
                vEstado = -1;
            }

            gvProgramasFuncionRol.DataSource = vSeguridadService.ConsultarProgramaFuncionRols(vIdPrograma, vIdProgramaFuncion, vIdRol, vEstado);
            gvProgramasFuncionRol.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los controles iniciales
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvProgramasFuncionRol.PageSize = PageSize();
            gvProgramasFuncionRol.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Programa Funci&oacuten Rol", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para la Selecc�n de Registro
    /// </summary>
    /// <param name="pRow">Fila de la grilla</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProgramasFuncionRol.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ProgramaFuncionRol.IdProgramaFuncionRol", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento para la selecci�n de fila grilla de Programa Funcion Rol
    /// </summary>
    /// <param name="sender">The gvProgramaFuncionRol</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void gvProgramaFuncionRol_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProgramasFuncionRol.SelectedRow);
    }

    /// <summary>
    /// Evento para la paginaci�n de la grilla Programa Funcion Rol
    /// </summary>
    /// <param name="sender">The gvProgramaFuncionRol</param>
    /// <param name="e">The PageIndexChanging</param>
    protected void gvProgramaFuncionRol_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProgramasFuncionRol.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Evento para la selecci�n del dropdownlist Programas
    /// </summary>
    /// <param name="sender">The ddlProgramas</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void ddlProgramas_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlFuncion.Enabled = true;
        int vIdPrograma = int.Parse(ddlProgramas.SelectedValue);

        ddlFuncion.DataSource = vSeguridadService.ConsultarProgramasFuncionPorPrograma(vIdPrograma);
        ddlFuncion.DataTextField = "NombreFuncion";
        ddlFuncion.DataValueField = "IdProgramaFuncion";
        ddlFuncion.DataBind();

        ddlFuncion.Items.Insert(0, new ListItem("Seleccione", "-1"));
    }

    /// <summary>
    /// M�todo para cargar los datos en los controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if ( GetSessionParameter("ProgramaFuncionRol.Eliminado").ToString() == "1" )
                toolBar.MostrarMensajeEliminado();

            RemoveSessionParameter("ProgramaFuncionRol.Eliminado");

            ddlProgramas.DataSource = vSeguridadService.ConsultarProgramasDenuncias();
            ddlProgramas.DataTextField = "NombrePrograma";
            ddlProgramas.DataValueField = "IdPrograma";
            ddlProgramas.DataBind();

            ddlProgramas.Items.Insert(0, new ListItem("Seleccione", "-1"));

            ddlFuncion.Enabled = false;
            ddlFuncion.Items.Insert(0, new ListItem("Seleccione", "-1"));
            
            ddlRol.DataSource = vSeguridadService.ConsultarRoles();
            ddlRol.DataTextField = "NombreRol";
            ddlRol.DataValueField = "IdRol";
            ddlRol.DataBind();

            ddlRol.Items.Insert(0, new ListItem("Seleccione", "-1"));
            
            this.rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            this.rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            this.rblEstado.Items.Insert(2, new ListItem("Todos", "-1"));
            this.rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
