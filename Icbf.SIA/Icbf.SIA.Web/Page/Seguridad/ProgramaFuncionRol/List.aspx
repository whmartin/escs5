<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ProgramaFuncionRol_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    <asp:Label ID="lblPrograma" runat="server" Text="Programa:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblNombreFuncion" runat="server" Text="Funci&oacuten:"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlProgramas" OnSelectedIndexChanged="ddlProgramas_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlFuncion"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:Label ID="lblRol" runat="server" Text="Rol:"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlRol"></asp:DropDownList>
                </td>
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowBG">
                <td>&nbsp;
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProgramasFuncionRol" AutoGenerateColumns="false" AllowPaging="True"
                        Width="100%" DataKeyNames="IdProgramaFuncionRol" CellPadding="0" GridLines="None" OnPageIndexChanging="gvProgramaFuncionRol_PageIndexChanging"
                        OnSelectedIndexChanged="gvProgramaFuncionRol_SelectedIndexChanged" AllowSorting="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Programa" DataField="NombrePrograma" SortExpression="NombrePrograma" />
                            <asp:BoundField HeaderText="Funcion" DataField="NombreFuncion" SortExpression="NombreFuncion" />
                            <asp:BoundField HeaderText="Rol" DataField="NombreRol" SortExpression="NombreRol" />
                            <asp:CheckBoxField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
