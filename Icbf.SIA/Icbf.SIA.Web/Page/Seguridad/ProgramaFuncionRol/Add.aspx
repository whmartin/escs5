<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ProgramaFuncionRol_Add" %>

<%@ Register Src="~/General/General/Control/confirmMessage.ascx" TagName="confirm" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProgramaFuncionRol" runat="server" />
    <uc2:confirm ID="ctrConfirmMessage" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblPrograma" runat="server" Text="Programa: *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvPrograma" ControlToValidate="ddlProgramas"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvPrograma" ControlToValidate="ddlProgramas"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                <asp:Label ID="lblNombreFuncion" runat="server" Text="Funci&oacuten: *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvFuncion" ControlToValidate="ddlFuncion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvFuncion" ControlToValidate="ddlFuncion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlProgramas" OnSelectedIndexChanged="ddlProgramas_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlFuncion"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblRol" runat="server" Text="Rol: *"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTRol" ControlToValidate="ddlRol"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvRol" ControlToValidate="ddlRol"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                        SetFocusOnError="true" ErrorMessage="Campo Obligatorio" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlRol"></asp:DropDownList>
            </td>
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
