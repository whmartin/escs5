//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2016 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase ProgramaFuncionRol_Add.</summary>
// <author>Alvaro Mauricio Guerrero</author>
// <date>27/01/2016 0200 PM</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Entity;

/// <summary>
/// Clase parcial para gestionar el Add del Page de ProgramaFuncionRol
/// </summary>
public partial class Page_ProgramaFuncionRol_Add : GeneralWeb
{
    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    /// <summary>
    /// Referencia a SeguridadService
    /// </summary>
    private SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Variable global del path de la página
    /// </summary>
    private string vPageName = "Seguridad/ProgramaFuncionRol";

    #region Eventos

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if ( Request.QueryString["oP"] == "E" )
            vSolutionPage = SolutionPage.Edit;

        if ( ValidateAccess(toolBar, this.vPageName, vSolutionPage) )
        {
            if ( !Page.IsPostBack )
            {
                CargarDatosIniciales();
                if ( Request.QueryString["oP"] == "E" )
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Evento asociado al Click del Botón Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Evento asociado al Click del Botón Nuevo
    /// </summary>
    /// <param name="sender">The btnNuevo</param>
    /// <param name="e">The Click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento asociado al Click del Botón Buscar
    /// </summary>
    /// <param name="sender">The btnBuscar</param>
    /// <param name="e">The Click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento para la selección del dropdownlist Programas
    /// </summary>
    /// <param name="sender">The ddlProgramas</param>
    /// <param name="e">The SelectedIndexChanged</param>
    protected void ddlProgramas_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlFuncion.Enabled = true;
        int vIdPrograma = int.Parse(ddlProgramas.SelectedValue);

        ddlFuncion.DataSource = vSeguridadService.ConsultarProgramasFuncionPorPrograma(vIdPrograma);
        ddlFuncion.DataTextField = "NombreFuncion";
        ddlFuncion.DataValueField = "IdProgramaFuncion";
        ddlFuncion.DataBind();

        ddlFuncion.Items.Insert(0, new ListItem("Seleccione", "-1"));
    }

    #endregion "Eventos"

    #region Metodos

    /// <summary>
    /// Método para la Funcionalidad de búsqueda
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ProgramaFuncionRol vProgramaFuncionRol = new ProgramaFuncionRol();

            vProgramaFuncionRol.IdProgramaFuncion = Convert.ToInt32(ddlFuncion.SelectedValue);
            vProgramaFuncionRol.IdRol = Convert.ToInt32(ddlRol.SelectedValue);
            vProgramaFuncionRol.Estado = rblEstado.SelectedValue == "1" ? true : false;

            InformacionAudioria(vProgramaFuncionRol, this.vPageName, vSolutionPage);

            if ( Request.QueryString["oP"] == "E" )
            {
                vProgramaFuncionRol.IdProgramaFuncionRol = Convert.ToInt32(hfIdProgramaFuncionRol.Value);
                vProgramaFuncionRol.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProgramaFuncionRol, this.vPageName, vSolutionPage);
                vResultado = vSeguridadService.ModificarProgramaFuncionRol(vProgramaFuncionRol);
            }
            else
            {
                vProgramaFuncionRol.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProgramaFuncionRol, this.vPageName, vSolutionPage);
                vResultado = vSeguridadService.InsertarProgramaFuncionRol(vProgramaFuncionRol);
            }

            if ( vResultado == 0 )
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if ( vResultado == 1 )
            {
                SetSessionParameter("ProgramaFuncionRol.IdProgramaFuncionRol", vProgramaFuncionRol.IdProgramaFuncionRol);
                SetSessionParameter("ProgramaFuncionRol.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if ( vResultado == -1 )
            {
                this.toolBar.MostrarMensajeError("Registro ya Existente para este Rol.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los controles y eventos iniciales
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.SetSaveConfirmation(this.ctrConfirmMessage.GetConfirmationFunction());

            toolBar.EstablecerTitulos("Programa Funci&oacuten Rol", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos de un Programa Funcion Rol por el id
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdProgramaFuncionRol = Convert.ToInt32(GetSessionParameter("ProgramaFuncionRol.IdProgramaFuncionRol"));
            RemoveSessionParameter("ProgramaFuncionRol.IdProgramaFuncionRol");

            if ( GetSessionParameter("ProgramaFuncionRol.Guardado").ToString() == "1" )
                toolBar.MostrarMensajeGuardado();

            RemoveSessionParameter("ProgramaFuncionRol");
            RemoveSessionParameter("ProgramaFuncionRol.Guardado");

            ProgramaFuncionRol vProgramaFuncionRol = new ProgramaFuncionRol();
            vProgramaFuncionRol = vSeguridadService.ConsultarProgramaFuncionRol(vIdProgramaFuncionRol);

            hfIdProgramaFuncionRol.Value = vProgramaFuncionRol.IdProgramaFuncionRol.ToString();
            ddlProgramas.SelectedValue = vProgramaFuncionRol.IdPrograma.ToString();

            ddlFuncion.DataSource = vSeguridadService.ConsultarProgramasFuncionPorPrograma(vProgramaFuncionRol.IdPrograma);
            ddlFuncion.DataTextField = "NombreFuncion";
            ddlFuncion.DataValueField = "IdProgramaFuncion";
            ddlFuncion.DataBind();
            ddlFuncion.SelectedValue = vProgramaFuncionRol.IdProgramaFuncion.ToString();

            ddlRol.SelectedValue = vProgramaFuncionRol.IdRol.ToString();

            rblEstado.SelectedValue = vProgramaFuncionRol.Estado ? "1" : "0";
            ObtenerAuditoria(vPageName, hfIdProgramaFuncionRol.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProgramaFuncionRol.UsuarioCrea, vProgramaFuncionRol.FechaCrea, vProgramaFuncionRol.UsuarioModifica, vProgramaFuncionRol.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para Cargar Datos Iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if ( GetSessionParameter("ProgramaFuncionRol.Eliminado").ToString() == "1" )
                toolBar.MostrarMensajeEliminado();

            RemoveSessionParameter("ProgramaFuncionRol.Eliminado");

            ddlProgramas.DataSource = vSeguridadService.ConsultarProgramasDenuncias();
            ddlProgramas.DataTextField = "NombrePrograma";
            ddlProgramas.DataValueField = "IdPrograma";
            ddlProgramas.DataBind();

            ddlProgramas.Items.Insert(0, new ListItem("Seleccione", "-1"));

            ddlFuncion.Items.Insert(0, new ListItem("Seleccione", "-1"));

            ddlRol.DataSource = vSeguridadService.ConsultarRoles();
            ddlRol.DataTextField = "NombreRol";
            ddlRol.DataValueField = "IdRol";
            ddlRol.DataBind();

            ddlRol.Items.Insert(0, new ListItem("Seleccione", "-1"));

            this.rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            this.rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            this.rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion "Metodos"
}
