<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ProgramaFuncionRol_Detail" %>

<%@ Register Src="~/General/General/Control/confirmMessage.ascx" TagName="confirm" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProgramaFuncionRol" runat="server" />
    <uc2:confirm ID="ctrConfirmMessage" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblPrograma" runat="server" Text="Programa:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblNombreFuncion" runat="server" Text="Funci&oacuten:"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlProgramas" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlFuncion" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblRol" runat="server" Text="Rol:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlRol" Enabled="false"></asp:DropDownList>
            </td>
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" Enabled="false" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
