﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Seguridad_DesbloquearUsuario_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                <asp:Label ID="lblUsuarios" runat="server" Text="Usuarios Bloqueados: * "></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBoxList runat="server" ID="rblUsuarios">
                </asp:CheckBoxList>
            </td>
        </tr>
    </table>
</asp:Content>
