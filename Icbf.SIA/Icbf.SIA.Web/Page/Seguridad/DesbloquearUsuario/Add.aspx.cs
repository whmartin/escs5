﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Seguridad.Service;
using Icbf.RUBO.Service;
using Icbf.Utilities.Exceptions;
using System.Web.Security;

public partial class Page_Seguridad_DesbloquearUsuario_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadService = new SeguridadService();
    RUBOService vRUBOService = new RUBOService();
    string PageName = "Seguridad/DesbloquearUsuario";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                Actualizar(false);
            }
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            vSeguridadService = new SeguridadService();
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            ((LinkButton)toolBar.FindControl("btnAprobar")).ToolTip = "Desbloquear";
            toolBar.EstablecerTitulos("Desbloquear Usuario", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        Boolean vActualizador = false;
        foreach (ListItem vListItem in rblUsuarios.Items)
        {
            if (vListItem.Selected)
            {
                MembershipUserCollection vMemberShipUser = Membership.FindUsersByName(vListItem.Text);
                foreach (MembershipUser vMembership in vMemberShipUser)
                {
                    vMembership.UnlockUser();
                    vActualizador = true;
                }
            }
        }

        Actualizar(vActualizador);
    }

    private void Actualizar(Boolean vMostrarMensaje)
    {
        rblUsuarios.Items.Clear();
        MembershipUserCollection vUsuarios = Membership.GetAllUsers();
        foreach (MembershipUser vUsuario in vUsuarios)
        {
            if (vUsuario.IsLockedOut)
            {
                ListItem vListItem = new ListItem(vUsuario.UserName, vUsuario.ProviderUserKey.ToString());
                rblUsuarios.Items.Add(vListItem);
            }
        }
        if (vMostrarMensaje)
            toolBar.MostrarMensajeGuardado("Usuarios desbloqueados.");
    }
}