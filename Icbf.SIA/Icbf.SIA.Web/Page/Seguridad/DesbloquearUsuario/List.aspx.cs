﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.RUBO.Business;
using Icbf.RUBO.Entity;
using Icbf.RUBO.Service;

public partial class Page_Seguridad_DesbloquearUsuario_List : GeneralWeb
{
    masterPrincipal toolBar;
    RUBOService vRUBOService = new RUBOService();
    string PageName = "Seguridad/DesbloquearUsuario";

    protected void Page_PreInit(object sender, EventArgs e) { Iniciar(); }
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, PageName, SolutionPage.List))
        {
            NavigateTo(SolutionPage.Add);
        }
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    { Buscar(); }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Desbloquear Usuario", SolutionPage.List.ToString());

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    //Funcionalidad de búsqueda
    private void Buscar()
    {
        try
        {
            if (GetSessionParameter("DesbloquearUsuario.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("DesbloquearUsuario.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}