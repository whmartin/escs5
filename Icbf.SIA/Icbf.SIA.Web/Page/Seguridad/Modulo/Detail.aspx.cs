﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;

/// <summary>
/// Página que despliega el detalle del registro de módulo
/// </summary>
public partial class Page_Seguridad_Modulo_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();
    string PageName = "Seguridad/Modulo";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, SolutionPage.Detail))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }


    /// <summary>
    /// Manejador de evento click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Modulo.Id", hfIdModulo.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdModulo = Convert.ToInt32(GetSessionParameter("Modulo.Id"));
            //RemoveSessionParameter("Modulo.Id");

            if (GetSessionParameter("Modulo.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Modulo.Guardado");

            Modulo vModulo = new Modulo();
            vModulo = vSeguridadFAC.ConsultarModulo(vIdModulo);

            txtNombreModulo.Text = vModulo.NombreModulo.ToString();
            txtPosicion.Text = vModulo.Posicion.ToString();
            hfIdModulo.Value = vModulo.IdModulo.ToString();
            rblEstado.SelectedValue = vModulo.Estado == true ? "1" : "0";

            ObtenerAuditoria(PageName, hfIdModulo.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModulo.UsuarioCreacion, vModulo.FechaCreacion, vModulo.UsuarioModificacion, vModulo.FechaModificacion);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Eliminar registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdModulo = Convert.ToInt32(hfIdModulo.Value);
            Modulo vModulo = new Modulo();
            vModulo = vSeguridadFAC.ConsultarModulo(vIdModulo);

            InformacionAudioria(vModulo, this.PageName, vSolutionPage);

            int vResultado = vSeguridadFAC.EliminarModulo(vModulo);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Modulo.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Módulo", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}