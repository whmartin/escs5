﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using System.Web.Security;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Drawing;
using System.IO;
using System.Collections;

/// <summary>
/// Página que despliega la consulta basada en filtros de los módulos de la aplicación
/// </summary>
public partial class Page_Seguridad_Modulo_List : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, "Seguridad/Modulo", SolutionPage.List))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreModulo = null;
            Boolean? vEstado = null;

            if (ddlNombreModulo.Text != "")
            {
                vNombreModulo = ddlNombreModulo.Text;
            }

            switch (rblEstado.SelectedValue)
            {
                case "0":
                    vEstado = false;
                    break;
                case "1":
                    vEstado = true;
                    break;
                default:
                    vEstado = null;
                    break;
            }
            gvModulo.DataSource = vSeguridadFAC.ConsultarModulos(vNombreModulo, vEstado);
            gvModulo.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Modulo", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Modulo.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Modulo.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvModulo.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Modulo.Id", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvModulo.SelectedRow);
    }

    protected void gvModulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModulo.PageIndex = e.NewPageIndex;
        Buscar();
    }
}