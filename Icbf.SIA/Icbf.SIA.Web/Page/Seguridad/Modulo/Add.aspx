﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Seguridad_Modulo_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%--<asp:ScriptManager runat="server" ID="smPrincipal"></asp:ScriptManager>--%>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre:*
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreModulo" ControlToValidate="txtNombreModulo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:HiddenField ID="hfIdModulo" runat="server" />
            </td>
            <td>
                Posicion*
                <asp:RequiredFieldValidator runat="server" ID="cvPosicion" ControlToValidate="txtPosicion"
                    Display="Dynamic" SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td style="height: 31px">
                <asp:TextBox ID="txtNombreModulo" runat="server"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreModulo" runat="server" TargetControlID="txtNombreModulo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox ID="txtPosicion" runat="server"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPosicion" runat="server" TargetControlID="txtPosicion"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado*
                <asp:RequiredFieldValidator runat="server" ID="cvEstado" ControlToValidate="rblEstado"
                    Display="Dynamic" SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Text="Activo" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Inactivo"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
