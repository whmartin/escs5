﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Seguridad_Modulo_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre:
            </td>
            <td>
                Posicion:
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtNombreModulo" runat="server" Enabled="false"></asp:TextBox>
                <asp:HiddenField ID="hfIdModulo" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="txtPosicion" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado:
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="1" Text="Activo" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Inactivo"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
