﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Seguridad_Modulo_List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" /><asp:Panel runat="server" ID="pnlConsulta">--%>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Nombre:
                </td>
                <td>
                    Estado:
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="ddlNombreModulo" runat="server"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreModulo" runat="server" TargetControlID="ddlNombreModulo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td>
                    <asp:RadioButtonList ID="rblEstado" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Text="Activo" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                        <asp:ListItem Value="-1">Todos</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvModulo" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdModulo" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvModulo_PageIndexChanging" OnSelectedIndexChanged="gvModulo_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="NombreModulo" DataField="NombreModulo" />
                            <asp:BoundField HeaderText="Posicion" DataField="Posicion" />
                            <asp:CheckBoxField DataField="Estado" HeaderText="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
