﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Session;
using Icbf.Utilities.Presentation;
using System.Web.Security;
using Icbf.Seguridad.Service;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Entity;
using System.Data;
using Icbf.Utilities.Exceptions;
using System.Drawing;
using System.IO;
using System.Collections;

/// <summary>
/// Página de registro y edición de módulos de la aplicación
/// </summary>
public partial class Page_Seguridad_Modulo_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SeguridadService vSeguridadFAC = new SeguridadService();
    string PageName = "Seguridad/Modulo";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    
    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Modulo", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;

            Modulo vModulo = new Modulo();

            vModulo.IdModulo = 1;
            vModulo.NombreModulo = txtNombreModulo.Text;
            vModulo.Posicion = Convert.ToInt32(txtPosicion.Text);
            vModulo.Estado = rblEstado.SelectedValue == "1" ? true : false;

            InformacionAudioria(vModulo, this.PageName, vSolutionPage);

            if (Request.QueryString["oP"] == "E")
            {
                vModulo.IdModulo = Convert.ToInt32(hfIdModulo.Value);
                vModulo.UsuarioModificacion = GetSessionUser().NombreUsuario;
                vResultado = vSeguridadFAC.ModificarModulo(vModulo);
            }
            else
            {
                vModulo.UsuarioCreacion = GetSessionUser().NombreUsuario;
                vResultado = vSeguridadFAC.InsertarModulo(vModulo);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Modulo.Id", vModulo.IdModulo);
                SetSessionParameter("Modulo.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdModulo = Convert.ToInt32(GetSessionParameter("Modulo.Id"));
            //RemoveSessionParameter("Modulo.Id");

            Modulo vModulo = new Modulo();
            vModulo = vSeguridadFAC.ConsultarModulo(vIdModulo);

            txtNombreModulo.Text = vModulo.NombreModulo.ToString();
            txtPosicion.Text = vModulo.Posicion.ToString();
            hfIdModulo.Value = vModulo.IdModulo.ToString();
            rblEstado.SelectedValue = vModulo.Estado == true ? "1" : "0";

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModulo.UsuarioCreacion, vModulo.FechaCreacion, vModulo.UsuarioModificacion, vModulo.FechaModificacion);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


}