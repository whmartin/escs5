﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_EstudioSectorCosto_ReporteGeneraldeEstudios_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ReporteGeneraldeEstudios";
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        ddlVigencia.Focus();
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                ViewState["Tipoestudio"] = 0;
                ViewState["Tipoestado"] = 0;
                ViewState["Tipomodalidad"] = 0;
                CargarDatosIniciales();
            }
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        LimpiarCampos();
    }

    private void LimpiarCampos()
    {
        toolBar = (masterPrincipal)Master;
        toolBar.LipiarMensajeError();
        LimpiarControles(pnlConsulta.Controls);
    }
    /// <summary>
    /// Evento para generar consulta generica
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void btnReporte_Click(object sender, EventArgs e)
    {
        toolBar = (masterPrincipal)Master;

        try
        {
            string titulo = "Reporte General de estudios";
            string nombreReporte = "ReporteGeneral";
            //Crea un objeto de tipo reporte

            string vigencia = ddlVigencia.SelectedValue;
            string tipoEstudio = CargarSeleccion(ddlTiposEstudio);
            string tipoEstado = CargarSeleccion(ddlEstado);
            string modalidad = CargarSeleccion(ddlmodalidad);
            if (tipoEstado.Trim() == "") tipoEstado = "-1";
            if (modalidad.Trim() == "") modalidad = "-1";
            var showExportControls = true;

            var objReport = new Report(nombreReporte, showExportControls, PageName, titulo);

            objReport.AddParameter("Vigencia", vigencia);
            objReport.AddParameter("DireccionSolicitante", tipoEstudio);
            objReport.AddParameter("Objeto", modalidad);
            objReport.AddParameter("Estado", tipoEstado);
            objReport.AddParameter("Usuario", GetSessionUser().NombreUsuario);

            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para cargar los datos iniciales de la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)Master;
            toolBar.eventoReporte += btnReporte_Click;

            toolBar.EstablecerTitulos(@"Reporte General de Estudios", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para obtener la sesión e inicializar controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Informe.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Informe.Eliminado");

            var lstRegional = vResultadoEstudioSectorService.ConsultarVigencia();

            Utilidades.LlenarDropDownList(ddlVigencia, lstRegional);

            llenarTiposEstudio();
            llenarEstado();
            llenarmodalidad();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void llenarmodalidad()
    {
        EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

        List<ModalidadesSeleccionESC> vTiposEstudios = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESCs(null, null, 1);

        ListItem ls = new ListItem();
        ls = new ListItem();
        ls.Text = "TODAS";
        ls.Value = "0";

        ddlmodalidad.Items.Clear();
        if (vTiposEstudios.Count() > 1)
        {
            LoadCheckTiposmodalidad(vTiposEstudios);
        }

        ddlmodalidad.Items.Insert(0, ls);
    }
    public void llenarEstado()
    {
        EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

        List<EstadoEstudio> vTiposEstudios = vEstudioSectorCostoService.ConsultarEstadoEstudios(null, null, null, 1);

        ListItem ls = new ListItem();
        ls = new ListItem();
        ls.Text = "TODAS";
        ls.Value = "0";

        ddlEstado.Items.Clear();
        if (vTiposEstudios.Count() > 1)
        {
            LoadCheckTiposEstados(vTiposEstudios);
        }

        ddlEstado.Items.Insert(0, ls);
    }

    /// <summary>
    /// Método para agregar cargar ListBox de Tipos de Estudios
    /// </summary>
    public void llenarTiposEstudio()
    {
        EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

        List<DireccionesSolicitantesESC> vTiposEstudios = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESCs(null, null, 1);

        ListItem ls = new ListItem();
        ls = new ListItem();
        ls.Text = "TODAS";
        ls.Value = "0";

        ddlTiposEstudio.Items.Clear();
        if (vTiposEstudios.Count() > 1)
        {
            LoadCheckTiposEstudios(vTiposEstudios);
        }

        ddlTiposEstudio.Items.Insert(0, ls);
    }

    /// <summary>
    /// Método para agregar Tipos de modalidades
    /// </summary>
    /// <param name="lista">lista de  Tipos de estados</param>
    public void LoadCheckTiposmodalidad(List<ModalidadesSeleccionESC> lista)
    {
        int var;
        ListItem ls = new ListItem();
        //ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_ddlClaseActividad');");

        foreach (ModalidadesSeleccionESC pci in lista.OrderBy(t => t.ModalidadSeleccion))
        {
            ls = new ListItem();
            ls.Text = pci.ModalidadSeleccion;
            var = pci.IdModalidadSeleccion;
            ls.Value = var.ToString();
            ddlmodalidad.Items.Add(ls);
        }
    }

    /// <summary>
    /// Método para agregar Tipos de estados
    /// </summary>
    /// <param name="lista">lista de  Tipos de estados</param>
    public void LoadCheckTiposEstados(List<EstadoEstudio> lista)
    {
        int var;
        ListItem ls = new ListItem();
        //ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_ddlClaseActividad');");

        foreach (EstadoEstudio pci in lista.OrderBy(t => t.Nombre))
        {
            ls = new ListItem();
            ls.Text = pci.Nombre;
            var = pci.IdEstadoEstudio;
            ls.Value = var.ToString();
            ddlEstado.Items.Add(ls);
        }
    }

    /// <summary>
    /// Método para agregar Tipos de Estudios
    /// </summary>
    /// <param name="lista">lista de  Tipos de Estudios</param>
    public void LoadCheckTiposEstudios(List<DireccionesSolicitantesESC> lista)
    {
        int var;
        ListItem ls = new ListItem();
        //ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_ddlClaseActividad');");

        foreach (DireccionesSolicitantesESC pci in lista.OrderBy(t => t.DireccionSolicitante))
        {
            ls = new ListItem();
            ls.Text = pci.DireccionSolicitante;
            var = pci.IdDireccionesSolicitantes;
            ls.Value = var.ToString();
            ddlTiposEstudio.Items.Add(ls);
        }
    }

    /// <summary>
    /// Evento para seleccionar todos los tipos de estudio de interés
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void CheckBoxTodosTiposEstudio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTiposEstudio.Items[0].Selected)
        {
            SelecionarTodos(this.ddlTiposEstudio);
            ViewState["Tipoestudio"] = 1;
        }
        else
        {
            if (ViewState["Tipoestudio"].ToString() == "1")
            {
                SelecionarNinguno(this.ddlTiposEstudio);
                ViewState["Tipoestudio"] = 0;
            }
        }
    }

    protected void CheckBoxTodosTiposEstado_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEstado.Items[0].Selected)
        {
            SelecionarTodos(this.ddlEstado);
            ViewState["Tipoestado"] = 1;
        }
        else
        {
            if (ViewState["Tipoestado"].ToString() == "1")
            {
                SelecionarNinguno(this.ddlEstado);
                ViewState["Tipoestado"] = 0;
            }
        }
    }

    protected void CheckBoxTodosTiposmodalidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlmodalidad.Items[0].Selected)
        {
            SelecionarTodos(this.ddlmodalidad);
            ViewState["Tipomodalidad"] = 1;
        }
        else
        {
            if (ViewState["Tipomodalidad"].ToString() == "1")
            {
                SelecionarNinguno(this.ddlmodalidad);
                ViewState["Tipomodalidad"] = 0;
            }
        }
    }
    /// <summary>
    /// Método que selecciona todas el checkBoxList recibido
    /// </summary>
    public void SelecionarTodos(CheckBoxList ddlList)
    {
        for (int i = 1; ddlList.Items.Count > i; i++)
        {
            ddlList.Items[i].Selected = true;
        }
    }

    // Método que limpiar el checkBoxList seleccionadas
    /// </summary>
    public void SelecionarNinguno(CheckBoxList ddlList)
    {
        for (int i = 1; ddlList.Items.Count > i; i++)
        {
            ddlList.Items[i].Selected = false;
        }
    }

    /// <summary>
    /// Método que carga las regionales seleccionadas para consulta
    /// </summary>
    public string CargarSeleccion(CheckBoxList list)
    {
        string vCadena = "";

        for (int i = 0; list.Items.Count > i; i++)
        {
            if (list.Items[i].Selected)
            {
                if (vCadena.Equals(""))
                {
                    vCadena = list.Items[i].Value.ToString();
                }
                else
                {
                    vCadena = vCadena + "," + list.Items[i].Value.ToString();
                }
            }
        }
        return vCadena;
    }

}