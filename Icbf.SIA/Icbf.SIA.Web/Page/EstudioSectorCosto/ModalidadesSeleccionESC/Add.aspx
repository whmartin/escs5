<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ModalidadesSeleccionESC_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }
    </script>
    <asp:HiddenField ID="hfIdModalidadSeleccion" runat="server" />
    <asp:HiddenField ID="HfValidacion" runat="server" />
     <asp:HiddenField ID="hfNombreModalidadSeleccion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Modalidad de contratación *
                <asp:RequiredFieldValidator runat="server" ID="rfvModalidadSeleccion" ControlToValidate="txtModalidadSeleccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">Descripción
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtModalidadSeleccion" Width="99%" MaxLength="100" OnTextChanged="txtModalidadSeleccion_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftModalidadSeleccion" runat="server" TargetControlID="txtModalidadSeleccion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine"
                    onKeyDown="limitText(this,255);" onKeyUp="limitText(this,255);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Tipo modalidad *
                
            </td>
        
        
            <td colspan="2">Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            </tr>
         <tr class="rowB">
            <td colspan="2">
                <asp:CheckBox ID="chk_tipo1" Text="Tipo 1" name="chk_tipo1"  AutoPostBack="True" runat="server" RepeatDirection="Horizontal"  Checked="true" OnCheckedChanged="chk_tipo1_CheckedChanged" ></asp:CheckBox>     
                <asp:CheckBox ID="chk_tipo2" Text="Tipo 2" name="chk_tipo2"  AutoPostBack="True" runat="server" RepeatDirection="Horizontal" OnCheckedChanged="chk_tipo2_CheckedChanged" ></asp:CheckBox>     
                
            </td>
       
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
