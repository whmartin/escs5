using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_ModalidadesSeleccionESC_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ModalidadesSeleccionESC";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtModalidadSeleccion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<ModalidadesSeleccionESC> Buscar()
    {
        try
        {
            String vModalidadSeleccion = null;
            String vDescripcion = null;
            int? vEstado = null;
            if (txtModalidadSeleccion.Text != "")
            {
                vModalidadSeleccion = Convert.ToString(txtModalidadSeleccion.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }
            string tips = null;
            if (chk_tipo1.Checked) tips = "Tipo 1";
            if (chk_tipo2.Checked) tips = "Tipo 2";

            List<ModalidadesSeleccionESC> ListaModalidadesSeleccionESC = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESCs(vModalidadSeleccion, vDescripcion, vEstado, tips);
            return ListaModalidadesSeleccionESC;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvModalidadesSeleccionESC.PageSize = PageSize();
            gvModalidadesSeleccionESC.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Modalidades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvModalidadesSeleccionESC.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ModalidadesSeleccionESC.IdModalidadSeleccion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModalidadesSeleccionESC_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvModalidadesSeleccionESC.SelectedRow);
    }
    protected void gvModalidadesSeleccionESC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModalidadesSeleccionESC.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ModalidadesSeleccionESC.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ModalidadesSeleccionESC.Eliminado");

            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";

            //
            //chk_tipomodalidad.Items.Add(new ListItem("Tipo 1"));
            //chk_tipomodalidad.Items.Add(new ListItem("Tipo 2"));
            //ddl_tipomodalidad.SelectedValue = "0";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvModalidadesSeleccionESC.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvModalidadesSeleccionESC.AllowPaging;
        Boolean vPaginadorError = gvModalidadesSeleccionESC.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvModalidadesSeleccionESC.AllowPaging = false;

        List<ModalidadesSeleccionESC> vListaModalidadesSeleccionESCs = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvModalidadesSeleccionESC, vListaModalidadesSeleccionESCs);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvModalidadesSeleccionESC.Columns.Count; i++)
                {
                    dt.Columns.Add(gvModalidadesSeleccionESC.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvModalidadesSeleccionESC.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvModalidadesSeleccionESC.Columns.Count; j++)
                    {
                        dr[gvModalidadesSeleccionESC.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaModalidadesSeleccionESC");
                gvModalidadesSeleccionESC.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }


    protected List<ModalidadesSeleccionESC> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<ModalidadesSeleccionESC> listaModalidadesSeleccionESCs = Buscar();
        gvModalidadesSeleccionESC.DataSource = listaModalidadesSeleccionESCs;
        gvModalidadesSeleccionESC.DataBind();


        return listaModalidadesSeleccionESCs;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<ModalidadesSeleccionESC> listaModalidadesSeleccionESCs = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaModalidadesSeleccionESCs != null)
                {
                    var param = Expression.Parameter(typeof(ModalidadesSeleccionESC), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ModalidadesSeleccionESC, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaModalidadesSeleccionESCs.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaModalidadesSeleccionESCs.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaModalidadesSeleccionESCs.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaModalidadesSeleccionESCs.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaModalidadesSeleccionESCs;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvModalidadesSeleccionESC_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void chk_tipo1_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_tipo1.Checked) { chk_tipo2.Checked = false; }
        else
        {
            chk_tipo2.Checked = true;
        }
    }

    protected void chk_tipo2_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_tipo2.Checked) { chk_tipo1.Checked = false; }
        else { chk_tipo1.Checked = true; }
    }
}
