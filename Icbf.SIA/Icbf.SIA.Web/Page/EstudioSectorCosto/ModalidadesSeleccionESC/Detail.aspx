<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ModalidadesSeleccionESC_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdModalidadSeleccion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Modalidad de contratación 
            </td>
            <td colspan="2">Descripción
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtModalidadSeleccion" Width="99%" Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Tipo modalidad
            </td>
            <td>Estado </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:CheckBox ID="chk_tipo1" Text="Tipo 1" name="chk_tipo1"  AutoPostBack="True" runat="server" RepeatDirection="Horizontal" Enabled="false"></asp:CheckBox>     
                    <asp:CheckBox ID="chk_tipo2" Text="Tipo 2" name="chk_tipo2"  AutoPostBack="True" runat="server" RepeatDirection="Horizontal" Enabled="false"></asp:CheckBox>     
               
                 </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
           
            </td>
        </tr>
    </table>
</asp:Content>
