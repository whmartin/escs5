using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_ModalidadesSeleccionESC_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ModalidadesSeleccionESC";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ModalidadesSeleccionESC.IdModalidadSeleccion", hfIdModalidadSeleccion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdModalidadSeleccion = Convert.ToInt32(GetSessionParameter("ModalidadesSeleccionESC.IdModalidadSeleccion"));
            RemoveSessionParameter("ModalidadesSeleccionESC.IdModalidadSeleccion");

            if (GetSessionParameter("ModalidadesSeleccionESC.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ModalidadesSeleccionESC.Guardado");


            ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();
            vModalidadesSeleccionESC = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESC(vIdModalidadSeleccion);
            hfIdModalidadSeleccion.Value = vModalidadesSeleccionESC.IdModalidadSeleccion.ToString();
            txtModalidadSeleccion.Text = vModalidadesSeleccionESC.ModalidadSeleccion;
            txtDescripcion.Text = vModalidadesSeleccionESC.Descripcion;
            if (vModalidadesSeleccionESC.TipoModalidad.Trim() == "Tipo 1")
            {
                chk_tipo1.Checked = true;
            }
            else
            {
                chk_tipo2.Checked = true;
            }
            rblEstado.SelectedValue = Convert.ToBoolean(vModalidadesSeleccionESC.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdModalidadSeleccion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModalidadesSeleccionESC.UsuarioCrea, vModalidadesSeleccionESC.FechaCrea, vModalidadesSeleccionESC.UsuarioModifica, vModalidadesSeleccionESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdModalidadSeleccion = Convert.ToInt32(hfIdModalidadSeleccion.Value);

            ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();
            vModalidadesSeleccionESC = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESC(vIdModalidadSeleccion);
            InformacionAudioria(vModalidadesSeleccionESC, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarModalidadesSeleccionESC(vModalidadesSeleccionESC);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ModalidadesSeleccionESC.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Modalidades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
