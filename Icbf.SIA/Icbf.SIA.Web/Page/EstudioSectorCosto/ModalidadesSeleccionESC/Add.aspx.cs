using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_ModalidadesSeleccionESC_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/ModalidadesSeleccionESC";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtModalidadSeleccion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();

            vModalidadesSeleccionESC.ModalidadSeleccion = Convert.ToString(txtModalidadSeleccion.Text).ToUpper();
            vModalidadesSeleccionESC.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();
            vModalidadesSeleccionESC.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            string tips = "Tipo 1";
            if (!chk_tipo1.Checked) tips = "Tipo 2";

            vModalidadesSeleccionESC.TipoModalidad = tips;
            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vModalidadesSeleccionESC.IdModalidadSeleccion = Convert.ToInt32(hfIdModalidadSeleccion.Value);
                    vModalidadesSeleccionESC.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vModalidadesSeleccionESC, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarModalidadesSeleccionESC(vModalidadesSeleccionESC);
                }
                else
                {
                    vModalidadesSeleccionESC.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vModalidadesSeleccionESC, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarModalidadesSeleccionESC(vModalidadesSeleccionESC);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("ModalidadesSeleccionESC.IdModalidadSeleccion", vModalidadesSeleccionESC.IdModalidadSeleccion);
                    SetSessionParameter("ModalidadesSeleccionESC.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Modalidades", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdModalidadSeleccion = Convert.ToInt32(GetSessionParameter("ModalidadesSeleccionESC.IdModalidadSeleccion"));
            String NombreModalidadSeleccion = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESC(vIdModalidadSeleccion).ModalidadSeleccion;
            hfNombreModalidadSeleccion.Value = NombreModalidadSeleccion;
            RemoveSessionParameter("ModalidadesSeleccionESC.Id");

            ModalidadesSeleccionESC vModalidadesSeleccionESC = new ModalidadesSeleccionESC();
            vModalidadesSeleccionESC = vEstudioSectorCostoService.ConsultarModalidadesSeleccionESC(vIdModalidadSeleccion);
            hfIdModalidadSeleccion.Value = vModalidadesSeleccionESC.IdModalidadSeleccion.ToString();
            txtModalidadSeleccion.Text = vModalidadesSeleccionESC.ModalidadSeleccion;
            txtDescripcion.Text = vModalidadesSeleccionESC.Descripcion;
            if(vModalidadesSeleccionESC.TipoModalidad.Trim()=="Tipo 1")
            {
                chk_tipo1.Checked = true;
                chk_tipo2.Checked = false;
            }
            else
            {
                chk_tipo1.Checked = false;
                chk_tipo2.Checked = true;
            }
            rblEstado.SelectedValue = Convert.ToBoolean(vModalidadesSeleccionESC.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModalidadesSeleccionESC.UsuarioCrea, vModalidadesSeleccionESC.FechaCrea, vModalidadesSeleccionESC.UsuarioModifica, vModalidadesSeleccionESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtModalidadSeleccion.Text.ToUpper().Equals(hfNombreModalidadSeleccion.Value))
        {
            if (vEstudioSectorCostoService.ValidarModalidadesSeleccionESC(txtModalidadSeleccion.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
    protected void txtModalidadSeleccion_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }

    protected void chk_tipo1_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_tipo1.Checked) { chk_tipo2.Checked = false; }
        else
        {
            chk_tipo2.Checked = true;
        }
    }

    protected void chk_tipo2_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_tipo2.Checked) { chk_tipo1.Checked = false; }
        else
        {
            chk_tipo1.Checked = true;
        }

    }
}
