<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ModalidadesSeleccionESC_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script>
        function CheckCheckboxes() {

            if (document.getElementById("chk_tipo1").checked == true) {
                document.getElementById("chk_tipo2").checked = false;
            }
            else if (document.getElementById("chk_tipo2").checked == true) {
                document.getElementById("chk_tipo1").checked = false;
            }

        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">

        <table width="90%" align="center">
            <tr class="rowB">
                <td>Modalidad de contratación
                </td>
                <td>Tipo modalidad
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtModalidadSeleccion" MaxLength="100" Width="99%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtModalidadSeleccion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddl_tipomodalidad" runat="server"  ></asp:DropDownList>--%>    
                    <asp:CheckBox ID="chk_tipo1" Text="Tipo 1" name="chk_tipo1"  AutoPostBack="True" runat="server" RepeatDirection="Horizontal"  Checked="true" OnCheckedChanged="chk_tipo1_CheckedChanged"></asp:CheckBox>     
                    <asp:CheckBox ID="chk_tipo2" Text="Tipo 2" name="chk_tipo2"  AutoPostBack="True" runat="server" RepeatDirection="Horizontal" OnCheckedChanged="chk_tipo2_CheckedChanged" ></asp:CheckBox>     
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td>
                           <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>   
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2"></td>
            </tr>
            <tr class="rowA">
                <td colspan="2"></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvModalidadesSeleccionESC" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdModalidadSeleccion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvModalidadesSeleccionESC_PageIndexChanging" OnSelectedIndexChanged="gvModalidadesSeleccionESC_SelectedIndexChanged" OnSorting="gvModalidadesSeleccionESC_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Modalidad de contratación" DataField="ModalidadSeleccion" SortExpression="ModalidadSeleccion" />
                            <asp:BoundField HeaderText="Tipo de Modalidad" DataField="TipoModalidad" SortExpression="TipoModalidad" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
