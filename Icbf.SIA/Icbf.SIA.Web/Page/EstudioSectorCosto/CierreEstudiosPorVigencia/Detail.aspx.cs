﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_EstudioSectorCosto_CierreEstudiosPorVigencia_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/CierreEstudiosPorVigencia";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdCierreEstudiosPorVigencia = Convert.ToInt32(GetSessionParameter("CierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia"));
            RemoveSessionParameter("CierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia");

            if (GetSessionParameter("CierreEstudiosPorVigencia.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("CierreEstudiosPorVigencia.Guardado");

            CierreEstudiosPorVigencia vCierreEstudiosPorVigencia = new CierreEstudiosPorVigencia();
            vCierreEstudiosPorVigencia = vEstudioSectorCostoService.ConsultarCierreEstudiosPorVigencia(vIdCierreEstudiosPorVigencia);
            hfIdCierreEstudiosPorVigencia.Value = vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia.ToString();
            txtAnioCierre.Text = vCierreEstudiosPorVigencia.AnioCierre.ToString();
            txtFechaEjecucion.Text = vCierreEstudiosPorVigencia.FechaEjecucion.ToString("dd/MM/yyyy");
            txtDescripcion.Text = vCierreEstudiosPorVigencia.Descripcion;
            ObtenerAuditoria(PageName, hfIdCierreEstudiosPorVigencia.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCierreEstudiosPorVigencia.UsuarioCrea, vCierreEstudiosPorVigencia.FechaCrea, vCierreEstudiosPorVigencia.UsuarioModifica, vCierreEstudiosPorVigencia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Cierre de estudios por vigencia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}