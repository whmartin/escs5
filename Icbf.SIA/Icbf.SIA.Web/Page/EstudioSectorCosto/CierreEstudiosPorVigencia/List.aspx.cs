﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_EstudioSectorCosto_CierreEstudiosPorVigencia_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/CierreEstudiosPorVigencia";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            ddlAnioCierre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<CierreEstudiosPorVigencia> Buscar()
    {
        try
        {
            int? vAnioCierre = null;
            DateTime? vFechaEjecucion = null;

            if (ddlAnioCierre.SelectedValue != "-1")
            {
                vAnioCierre = Convert.ToInt32(ddlAnioCierre.SelectedValue);
            }
            if (ddlFechaEjecucion.SelectedValue != "-1")
            {
                vFechaEjecucion = Convert.ToDateTime(ddlFechaEjecucion.SelectedValue);
            }
            List<CierreEstudiosPorVigencia> ListaCierreEstudiosPorVigencia = vEstudioSectorCostoService.ConsultarCierreEstudiosPorVigencias(vAnioCierre, vFechaEjecucion);
            return ListaCierreEstudiosPorVigencia;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvCierreEstudiosPorVigencia.PageSize = PageSize();
            gvCierreEstudiosPorVigencia.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Cierre de estudios por vigencia", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvCierreEstudiosPorVigencia.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("CierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCierreEstudiosPorVigencia_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvCierreEstudiosPorVigencia.SelectedRow);
    }
    protected void gvCierreEstudiosPorVigencia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCierreEstudiosPorVigencia.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("CierreEstudiosPorVigencia.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("CierreEstudiosPorVigencia.Guardado");

            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.ConsultarAnioCierre(ddlAnioCierre, null, true);
            vManejoControlesContratos.ConsultarFechaEjecucion(ddlFechaEjecucion, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvCierreEstudiosPorVigencia.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            gvCierreEstudiosPorVigencia.DataSource = null;
            gvCierreEstudiosPorVigencia.DataBind();
            //toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvCierreEstudiosPorVigencia.AllowPaging;
        Boolean vPaginadorError = gvCierreEstudiosPorVigencia.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvCierreEstudiosPorVigencia.AllowPaging = false;

        List<CierreEstudiosPorVigencia> vListaCierreEstudiosPorVigencias = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvCierreEstudiosPorVigencia, vListaCierreEstudiosPorVigencias);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvCierreEstudiosPorVigencia.Columns.Count; i++)
                {
                    dt.Columns.Add(gvCierreEstudiosPorVigencia.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvCierreEstudiosPorVigencia.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvCierreEstudiosPorVigencia.Columns.Count; j++)
                    {
                        dr[gvCierreEstudiosPorVigencia.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "CierreEstudios_" + DateTime.Now.ToString("ddMMyyyy"));
                gvCierreEstudiosPorVigencia.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }


    protected List<CierreEstudiosPorVigencia> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<CierreEstudiosPorVigencia> listaCierreEstudiosPorVigencias = Buscar();
        gvCierreEstudiosPorVigencia.DataSource = listaCierreEstudiosPorVigencias;
        gvCierreEstudiosPorVigencia.DataBind();

        return listaCierreEstudiosPorVigencias;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<CierreEstudiosPorVigencia> listaCierreEstudiosPorVigencias = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaCierreEstudiosPorVigencias != null)
                {
                    var param = Expression.Parameter(typeof(CierreEstudiosPorVigencia), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<CierreEstudiosPorVigencia, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaCierreEstudiosPorVigencias.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaCierreEstudiosPorVigencias.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaCierreEstudiosPorVigencias.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaCierreEstudiosPorVigencias.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaCierreEstudiosPorVigencias;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvCierreEstudiosPorVigencia_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
}