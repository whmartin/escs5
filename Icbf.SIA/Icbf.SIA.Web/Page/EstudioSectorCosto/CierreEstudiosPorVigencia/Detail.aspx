﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_EstudioSectorCosto_CierreEstudiosPorVigencia_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdCierreEstudiosPorVigencia" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Año Cierre
            </td>
            <td>Fecha Ejecución              
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAnioCierre" Enabled="false" MaxLength="4" Width="80%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaEjecucion" Enabled="false" MaxLength="10" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Descripción
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" Enabled="false" Height="80px" TextMode="MultiLine" CssClass="TextBoxGrande"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

