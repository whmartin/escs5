﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_EstudioSectorCosto_CierreEstudiosPorVigencia_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Año Cierre 
                </td>
                <td>Fecha Ejecución
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlAnioCierre" Width="98%"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlFechaEjecucion" Width="98%"></asp:DropDownList>
                </td>
            </tr>            
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvCierreEstudiosPorVigencia" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdCierreEstudiosPorVigencia" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvCierreEstudiosPorVigencia_PageIndexChanging" OnSelectedIndexChanged="gvCierreEstudiosPorVigencia_SelectedIndexChanged" OnSorting="gvCierreEstudiosPorVigencia_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Año Cierre" DataField="AnioCierre" SortExpression="AnioCierre" />
                            <asp:BoundField HeaderText="Fecha Ejecución" DataField="FechaEjecucion" SortExpression="FechaEjecucion" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

