﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_EstudioSectorCosto_CierreEstudiosPorVigencia_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/CierreEstudiosPorVigencia";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtDescripcion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            CierreEstudiosPorVigencia vCierreEstudiosPorVigencia = new CierreEstudiosPorVigencia();

            vCierreEstudiosPorVigencia.AnioCierre = Convert.ToInt32(txtAnioCierre.Text);
            vCierreEstudiosPorVigencia.FechaEjecucion = Convert.ToDateTime(txtFechaEjecucion.Text);
            vCierreEstudiosPorVigencia.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();           
            
            if (Request.QueryString["oP"] == "E")
            {
                vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia = Convert.ToInt32(hfIdCierreEstudiosPorVigencia.Value);
                vCierreEstudiosPorVigencia.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCierreEstudiosPorVigencia, this.PageName, SolutionPage.Edit);
                vResultado = vEstudioSectorCostoService.ModificarCierreEstudiosPorVigencia(vCierreEstudiosPorVigencia);
            }
            else
            {
                int? vAnioCierre = Convert.ToInt32(txtAnioCierre.Text);
                List<CierreEstudiosPorVigencia> List = vEstudioSectorCostoService.ConsultarCierreEstudiosPorVigencias(vAnioCierre, null);

                if (List.Count == 0)
                {
                    vCierreEstudiosPorVigencia.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vCierreEstudiosPorVigencia, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarCierreEstudiosPorVigencia(vCierreEstudiosPorVigencia);
                }
                else
                {
                    toolBar.MostrarMensajeError("El cierre para este año ya existe");
                    return;
                }
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("El cierre no pudo ejecutarse exitosamente. Inténtelo nuevamente.");
            }
            else if (vResultado > 0)
            {
                SetSessionParameter("CierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia", vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia);
                SetSessionParameter("CierreEstudiosPorVigencia.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.ConfirmarAccion("btnGuardar", "Se realizar\u00e1 el cierre de la informaci\u00F3n de estudios y pasar\u00e1 a hist\u00F3ricos");
            
            toolBar.EstablecerTitulos("Cierre de estudios por vigencia", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdCierreEstudiosPorVigencia = Convert.ToInt32(GetSessionParameter("CierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia"));
            RemoveSessionParameter("CierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia");

            CierreEstudiosPorVigencia vCierreEstudiosPorVigencia = new CierreEstudiosPorVigencia();
            vCierreEstudiosPorVigencia = vEstudioSectorCostoService.ConsultarCierreEstudiosPorVigencia(vIdCierreEstudiosPorVigencia);
            hfIdCierreEstudiosPorVigencia.Value = vCierreEstudiosPorVigencia.IdCierreEstudiosPorVigencia.ToString();
            txtAnioCierre.Text = vCierreEstudiosPorVigencia.AnioCierre.ToString();
            txtFechaEjecucion.Text = vCierreEstudiosPorVigencia.FechaEjecucion.ToString("dd/MM/yyyy");
            txtDescripcion.Text = vCierreEstudiosPorVigencia.Descripcion;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCierreEstudiosPorVigencia.UsuarioCrea, vCierreEstudiosPorVigencia.FechaCrea, vCierreEstudiosPorVigencia.UsuarioModifica, vCierreEstudiosPorVigencia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            txtAnioCierre.Text = DateTime.Now.AddYears(-1).ToString("yyyy");
            txtFechaEjecucion.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }   
}