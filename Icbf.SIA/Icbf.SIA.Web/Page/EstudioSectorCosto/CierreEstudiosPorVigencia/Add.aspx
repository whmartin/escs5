﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"  AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_EstudioSectorCosto_CierreEstudiosPorVigencia_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }
        </script>
    <asp:HiddenField ID="hfIdCierreEstudiosPorVigencia" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Año Cierre *
            </td>
            <td>Fecha Ejecución *                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAnioCierre" Enabled="false" MaxLength="4" Width="80%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaEjecucion" Enabled="false" MaxLength="10" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Descripción
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="250" Height="80px" TextMode="MultiLine"
                    onKeyDown="limitText(this,250);" onKeyUp="limitText(this,250);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;-" />
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
