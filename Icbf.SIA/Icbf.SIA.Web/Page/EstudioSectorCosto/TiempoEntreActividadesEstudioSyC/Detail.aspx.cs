//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_TiempoEntreActividadesEstudioSyC_Detail</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;

/// <summary>
/// Clase detalles tiempo entre actividades
/// </summary>
public partial class Page_TiempoEntreActividadesEstudioSyC_Detail : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/TiempoEntreActividadesEstudioSyC";

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vEstudioSectorCostoService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Método para cargar datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            pnlContenedor.Enabled = false;
            Decimal vIdTiempoEntreActividades = Convert.ToDecimal(GetSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades"));
            RemoveSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades");

            if (GetSessionParameter("TiempoEntreActividadesEstudioSyC.Guardado").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado();
            }
                
            RemoveSessionParameter("TiempoEntreActividadesEstudioSyC.Guardado");
            var a = txtIdConsecutivoEstudio.BackColor.Name;
            TiempoEntreActividadesEstudioSyC vTiemposESyC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesEstudioSyC(vIdTiempoEntreActividades);
            TiemposPACCO vTiempoPACCO = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesPACCO(vIdTiempoEntreActividades);
            TiempoEntreActividadesEstudioSyC vTiemposESC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasESC(vIdTiempoEntreActividades);
            TiempoEntreActividadesEstudioSyC vTiemposReales = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasReales(vIdTiempoEntreActividades);

            hfIdTiempoEntreActividades.Value = vIdTiempoEntreActividades.ToString();
            txtIdConsecutivoEstudio.Text = vTiemposESC.IdConsecutivoEstudio.ToString();
            ////Fechas ESyC
            vTiemposESyC.FentregaES_ECTiemposEquipo = vTiemposESyC.FentregaES_ECTiemposEquipo != null ? vTiemposESyC.FentregaES_ECTiemposEquipo.Substring(0, 10) : string.Empty;
            vTiemposESyC.FEentregaES_ECTiemposIndicador = vTiemposESyC.FEentregaES_ECTiemposIndicador != null ? vTiemposESyC.FEentregaES_ECTiemposIndicador.Substring(0, 10) : string.Empty;

            txtFentregaES_ECTiemposEquipo.Text = vTiemposESyC.FentregaES_ECTiemposEquipo;
            txtFEentregaES_ECTiemposIndicador.Text = vTiemposESyC.FEentregaES_ECTiemposIndicador;
            txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Text = vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
            lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Visible = vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR == null ? false : vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR.Value;
            txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.BackColor = GetColor(vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor);
            txtDiasHabilesEntreFCTFinalYSDC.Text = vTiemposESyC.DiasHabilesEntreFCTFinalYSDC;
            lblDiasHabilesEntreFCTFinalYSDC.Visible = vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR == null ? false : vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR.Value;
            txtDiasHabilesEntreFCTFinalYSDC.BackColor = GetColor(vTiemposESyC.DiasHabilesEntreFCTFinalYSDCColor);
            txtDiasHabilesEnESOCOSTEO.Text = vTiemposESyC.DiasHabilesEnESOCOSTEO;
            lblDiasHabilesEnESOCOSTEO.Visible = vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR == null ? false : vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR.Value;
            lblDiasHabilesEnESOCOSTEODevuelto.Visible = vTiemposESyC.DevueltoDiasHabilesEnESOCOSTEOR == null ? false : vTiemposESyC.DevueltoDiasHabilesEnESOCOSTEOR.Value;
            txtDiasHabilesEnESOCOSTEO.BackColor = GetColor(vTiemposESyC.DiasHabilesEnESOCOSTEOColor);
            txtDiashabilesParaDevolucion.Text = vTiemposESyC.DiashabilesParaDevolucion;
            lblDiashabilesParaDevolucion.Visible = vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR == null ? false : vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR.Value;
            txtDiashabilesParaDevolucion.BackColor = GetColor(vTiemposESyC.DiashabilesParaDevolucionColor);

            ////Tiempos PACCO
            vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL != null ? vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaFCTPreliminar = vTiempoPACCO.FEstimadaFCTPreliminar != null ? vTiempoPACCO.FEstimadaFCTPreliminar.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado != null ? vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEestimadaES_EC = vTiempoPACCO.FEestimadaES_EC != null ? vTiempoPACCO.FEestimadaES_EC.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaDocsRadicadosEnContratos = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos != null ? vTiempoPACCO.FEstimadaDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaComiteContratacion = vTiempoPACCO.FEstimadaComiteContratacion != null ? vTiempoPACCO.FEstimadaComiteContratacion.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaInicioDelPS = vTiempoPACCO.FEstimadaInicioDelPS != null ? vTiempoPACCO.FEstimadaInicioDelPS.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaPresentacionPropuestas = vTiempoPACCO.FEstimadaPresentacionPropuestas != null ? vTiempoPACCO.FEstimadaPresentacionPropuestas.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaDeInicioDeEjecucion = vTiempoPACCO.FEstimadaDeInicioDeEjecucion != null ? vTiempoPACCO.FEstimadaDeInicioDeEjecucion.Substring(0, 10) : string.Empty;

            txtFEstimadaFCTPreliminarREGINICIAL.Text = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL;
            lblFEstimadaFCTPreliminarREGINICIAL.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL.Value; ;
            txtFEstimadaFCTPreliminar.Text = vTiempoPACCO.FEstimadaFCTPreliminar;
            lblFEstimadaFCTPreliminar.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar.Value;
            txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Text = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
            lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Value;
            txtFEestimadaES_EC.Text = vTiempoPACCO.FEestimadaES_EC;
            lblFEestimadaES_EC.Visible = vTiempoPACCO.NoAplicaFEestimadaES_EC == null ? false : vTiempoPACCO.NoAplicaFEestimadaES_EC.Value;
            txtFEstimadaDocsRadicadosEnContratos.Text = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos;
            lblFEstimadaDocsRadicadosEnContratos.Visible = vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos == null ? false : vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos.Value;
            txtFEstimadaComiteContratacion.Text = vTiempoPACCO.FEstimadaComiteContratacion;
            lblFEstimadaComiteContratacion.Visible = vTiempoPACCO.NoAplicaFEstimadaComiteContratacion == null ? false : vTiempoPACCO.NoAplicaFEstimadaComiteContratacion.Value; ;
            txtFEstimadaInicioDelPS.Text = vTiempoPACCO.FEstimadaInicioDelPS;
            lblFEstimadaInicioDelPS.Visible = vTiempoPACCO.NoAplicaFEstimadaInicioDelPS == null ? false : vTiempoPACCO.NoAplicaFEstimadaInicioDelPS.Value;
            txtFEstimadaPresentacionPropuestas.Text = vTiempoPACCO.FEstimadaPresentacionPropuestas;
            lblFEstimadaPresentacionPropuestas.Visible = vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas == null ? false : vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas.Value; ;
            txtFEstimadaDeInicioDeEjecucion.Text = vTiempoPACCO.FEstimadaDeInicioDeEjecucion;

            ////Fechas ESC

            vTiemposESC.FRealFCTPreliminarESC = vTiemposESC.FRealFCTPreliminarESC != null ? vTiemposESC.FRealFCTPreliminarESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC != null ? vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaES_EC_ESC = vTiemposESC.FEstimadaES_EC_ESC != null ? vTiemposESC.FEstimadaES_EC_ESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaDocsRadicadosEnContratosESC = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC != null ? vTiemposESC.FEstimadaDocsRadicadosEnContratosESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaComitecontratacionESC = vTiemposESC.FEstimadaComitecontratacionESC != null ? vTiemposESC.FEstimadaComitecontratacionESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaInicioDelPS_ESC = vTiemposESC.FEstimadaInicioDelPS_ESC != null ? vTiemposESC.FEstimadaInicioDelPS_ESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaPresentacionPropuestasESC = vTiemposESC.FEstimadaPresentacionPropuestasESC != null ? vTiemposESC.FEstimadaPresentacionPropuestasESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaDeInicioDeEjecucionESC = vTiemposESC.FEstimadaDeInicioDeEjecucionESC != null ? vTiemposESC.FEstimadaDeInicioDeEjecucionESC.Substring(0, 10) : string.Empty;

            txtFRealFCTPreliminarESC.Text = vTiemposESC.FRealFCTPreliminarESC;
            lblFRealFCTPreliminarESC.Visible = vTiemposESC.NoAplicaFRealFCTPreliminarESC == null ? false : vTiemposESC.NoAplicaFRealFCTPreliminarESC.Value;
            txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Text = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
            lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Visible = vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC == null ? false : vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Value;
            txtFEstimadaES_EC_ESC.Text = vTiemposESC.FEstimadaES_EC_ESC;
            lblFEstimadaES_EC_ESC.Visible = vTiemposESC.NoAplicaFEstimadaES_EC_ESC == null ? false : vTiemposESC.NoAplicaFEstimadaES_EC_ESC.Value;
            txtFEstimadaDocsRadicadosEnContratosESC.Text = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC;
            lblFEstimadaDocsRadicadosEnContratosESC.Visible = vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC == null ? false : vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC.Value;
            txtFEstimadaComitecontratacionESC.Text = vTiemposESC.FEstimadaComitecontratacionESC;
            lblFEstimadaComitecontratacionESC.Visible = vTiemposESC.NoAplicaFEstimadaComitecontratacionESC == null ? false : vTiemposESC.NoAplicaFEstimadaComitecontratacionESC.Value;
            txtFEstimadaInicioDelPS_ESC.Text = vTiemposESC.FEstimadaInicioDelPS_ESC;
            lblFEstimadaInicioDelPS_ESC.Visible = vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC == null ? false : vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC.Value;
            txtFEstimadaPresentacionPropuestasESC.Text = vTiemposESC.FEstimadaPresentacionPropuestasESC;
            lblFEstimadaPresentacionPropuestasESC.Visible = vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC == null ? false : vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC.Value;
            txtFEstimadaDeInicioDeEjecucionESC.Text = vTiemposESC.FEstimadaDeInicioDeEjecucionESC;

            ////Tiempos reales
            vTiemposReales.FRealFCTPreliminar = vTiemposReales.FRealFCTPreliminar != null ? vTiemposReales.FRealFCTPreliminar.Substring(0, 10) : string.Empty;
            vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado != null ? vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Substring(0, 10) : string.Empty;
            vTiemposReales.FRealES_EC = vTiemposReales.FRealES_EC != null ? vTiemposReales.FRealES_EC.Substring(0, 10) : string.Empty;


            txtFRealFCTPreliminar.Text = vTiemposReales.FRealFCTPreliminar;
            txtFRealDocsRadicadosEnContratos.SetDate(vTiemposReales.FRealDocsRadicadosEnContratos == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealDocsRadicadosEnContratos));
            txtFRealFCTDefinitivaParaMCFRealES_MCRevisado.Text = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
            txtFRealComiteContratacion.SetDate(vTiemposReales.FRealComiteContratacion == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealComiteContratacion));
            txtFRealES_EC.Text = vTiemposReales.FRealES_EC;
            txtFRealInicioPS.SetDate(vTiemposReales.FRealInicioPS == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealInicioPS));
            txtFRealPresentacionPropuestas.SetDate(vTiemposReales.FRealPresentacionPropuestas == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealPresentacionPropuestas));
            txtFRealDocsRadicadosEnContratos.SetReadOnly(true);
            txtFRealComiteContratacion.SetReadOnly(true);
            txtFRealInicioPS.SetReadOnly(true);
            txtFRealPresentacionPropuestas.SetReadOnly(true);

            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTiempoEntreActividadesEstudioSyC.UsuarioCrea, vTiempoEntreActividadesEstudioSyC.FechaCrea, vTiempoEntreActividadesEstudioSyC.UsuarioModifica, vTiempoEntreActividadesEstudioSyC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para eliminar registro
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            ////No aplica
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para configurar la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);            
            toolBar.EstablecerTitulos("Tiempo entre actividades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para obtener colores
    /// </summary>
    /// <param name="pColor"></param>
    /// <returns></returns>
    public System.Drawing.Color GetColor(string pColor)
    {

        System.Drawing.Color vColor = System.Drawing.ColorTranslator.FromHtml(pColor);
        if (pColor.Equals("0"))
        {
            return System.Drawing.Color.Empty;
        }
        return vColor;
    }
    #endregion

    /// <summary>
    /// Evento click del botón nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click del botón editar
    /// </summary>
    /// <param name="sender">Boton btnEditar</param>
    /// <param name="e">Evento click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {        
        SetSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades", hfIdTiempoEntreActividades.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento click del botón editar
    /// </summary>
    /// <param name="sender">Boton btnEditar</param>
    /// <param name="e">Evento click</param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Evento click del botón eliminar
    /// </summary>
    /// <param name="sender">Boton btnEliminar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
   
}
