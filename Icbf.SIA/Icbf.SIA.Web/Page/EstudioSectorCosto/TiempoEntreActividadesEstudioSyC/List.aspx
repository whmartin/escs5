<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TiempoEntreActividadesEstudioSyC_List" %>
<%@ Register TagPrefix="uc1" TagName="consulta" Src="~/General/General/EstudioSectores/FormularioConsulta.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <uc1:consulta runat="server" ID="ucConsulta" />
     
    
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr runat="server" >
                <td runat="server">
                    <h3 runat="server" id="lblPACCO">PACCO</h3>
                    <asp:GridView runat="server" ID="gvTiempoEntreActividadesEstudioPACCO" AutoGenerateColumns="False" AllowPaging="True" OnSorting="gvTiempoEntreActividadesEstudioPACCO_Sorting"
                        GridLines="None" Width="100%" DataKeyNames="IdTiempoEntreActividades" CellPadding="0" Height="16px" AllowSorting="true"
                        OnPageIndexChanging="gvTiempoEntreActividadesEstudioPACCO_PageIndexChanging"
                         OnSelectedIndexChanged="gvTiempoEntreActividadesEstudioPACCO_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdConsecutivoEstudio"  SortExpression="IdConsecutivoEstudio" />
                            <asp:BoundField HeaderText="F. estimada DT preliminar REG. INICIAL" DataField="FEstimadaFCTPreliminarREGINICIAL" SortExpression="FEstimadaFCTPreliminarREGINICIAL" />
                            <asp:BoundField HeaderText="F. estimada DT preliminar" DataField="FEstimadaFCTPreliminar" SortExpression="FEstimadaFCTPreliminar" />
                            <asp:BoundField HeaderText="F. Estimada DT definitivos/para MC F. Estimada ES_MC revisado" DataField="FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"  SortExpression="FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"/>
                            <asp:BoundField HeaderText="F. estimada  ES-EC" DataField="FEestimadaES_EC"  SortExpression="FEestimadaES_EC"/>
                            <asp:BoundField HeaderText="F. estimada docs radicados en contratos" DataField="FEstimadaDocsRadicadosEnContratos"  SortExpression="FEstimadaDocsRadicadosEnContratos"/>
                            <asp:BoundField HeaderText="F. estimada comit&eacute; contrataci&oacute;n" DataField="FEstimadaComiteContratacion"  SortExpression="FEstimadaComiteContratacion"/>
                            <asp:BoundField HeaderText="F. estimada inicio del PS" DataField="FEstimadaInicioDelPS"  SortExpression="FEstimadaInicioDelPS"/>
                            <asp:BoundField HeaderText="F. estimada presentaci&oacute;n propuestas" DataField="FEstimadaPresentacionPropuestas"  SortExpression="FEstimadaPresentacionPropuestas"/>
                            <asp:BoundField HeaderText="F. estimada de inicio de ejecuci&oacute;n" DataField="FEstimadaDeInicioDeEjecucion"  SortExpression="FEstimadaDeInicioDeEjecucion"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <br />
                </td>
            </tr>

            <tr runat="server">
                <td runat="server">
                    <h3  runat="server" id="lblParametrica">C&aacute;lculos param&eacute;trica </h3>
                      <asp:GridView runat="server" ID="gvTiempoEntreActividadesCalculosParametrica" AutoGenerateColumns="False" AllowPaging="True" OnSorting="gvTiempoEntreActividadesCalculosParametrica_Sorting"
                        GridLines="None" Width="100%" DataKeyNames="IdTiempoEntreActividades" CellPadding="0" Height="16px" AllowSorting="true"

                        OnPageIndexChanging="gvTiempoEntreActividadesCalculosParametrica_PageIndexChanging"
                         >
                        <Columns>
                            
                            <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdConsecutivoEstudio"  SortExpression="IdConsecutivoEstudio"/>
                            <asp:TemplateField HeaderText="F. estimada DT preliminar REG. INICIAL ESC" >
                                <ItemTemplate>
                                    No Aplica
                                </ItemTemplate>
                            </asp:TemplateField>                               
                            <asp:BoundField HeaderText="F. real DT preliminar ESC" DataField="FRealFCTPreliminarESC" SortExpression="FRealFCTPreliminarESC" />
                            <asp:BoundField HeaderText="F. Estimada DT definitivos/para MC F. Estimada ES_MC revisado" DataField="FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"  SortExpression="FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"/>
                            <asp:BoundField HeaderText="F. estimada  ES-EC. ESC" DataField="FEstimadaES_EC_ESC"  SortExpression="FEstimadaES_EC_ESC"/>
                            <asp:BoundField HeaderText="F. estimada docs radicados en contratos ESC" DataField="FEstimadaDocsRadicadosEnContratosESC"  SortExpression="FEstimadaDocsRadicadosEnContratosESC"/>
                            <asp:BoundField HeaderText="F. estimada comit&eacute; contrataci&oacute;n ESC" DataField="FEstimadaComitecontratacionESC"  SortExpression="FEstimadaComitecontratacionESC"/>
                            <asp:BoundField HeaderText="F. estimada inicio del PS. ESC" DataField="FEstimadaInicioDelPS_ESC"  SortExpression="FEstimadaInicioDelPS_ESC"/>
                            <asp:BoundField HeaderText="F. estimada presentaci&oacute;n propuestas ESC" DataField="FEstimadaPresentacionPropuestasESC"  SortExpression="FEstimadaPresentacionPropuestasESC"/>
                            <asp:BoundField HeaderText="F. estimada de inicio de ejecuci&oacute;n ESC" DataField="FEstimadaDeInicioDeEjecucionESC"  SortExpression="FEstimadaDeInicioDeEjecucionESC"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <br />

                </td>
            </tr>
            <tr runat="server">
                <td runat="server">
                    <h3  runat="server" id="lblReal">REAL</h3>
                    <asp:GridView runat="server" ID="gvTiempoEntreActividadesEstudioREAL" AutoGenerateColumns="False" AllowPaging="True" OnSorting="gvTiempoEntreActividadesEstudioREAL_Sorting"
                        GridLines="None" Width="100%" DataKeyNames="IdTiempoEntreActividades" CellPadding="0" Height="16px" AllowSorting="true"
                        OnPageIndexChanging="gvTiempoEntreActividadesEstudioREAL_PageIndexChanging"
                         >
                        <Columns>                            
                            <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdConsecutivoEstudio"  SortExpression="IdConsecutivoEstudio"/>
                            <asp:TemplateField HeaderText="F. real DT preliminar REG. INICIAL">
                                <ItemTemplate>
                                    No Aplica
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:BoundField HeaderText="F. real DT preliminar" DataField="FRealFCTPreliminar" SortExpression="FRealFCTPreliminar" />
                            <asp:BoundField HeaderText="F. real DT avalados" DataField="FRealFCTDefinitivaParaMCFRealES_MCRevisado"  SortExpression="FRealFCTDefinitivaParaMCFRealES_MCRevisado"/>
                            <asp:BoundField HeaderText="F. real ES-EC" DataField="FRealES_EC"  SortExpression="FRealES_EC"/>
                            <asp:BoundField HeaderText="F. real docs radicados en contratos" DataField="FRealDocsRadicadosEnContratos"  SortExpression="FRealDocsRadicadosEnContratos"/>
                            <asp:BoundField HeaderText="F. real comit&eacute; contrataci&oacute;n" DataField="FRealComiteContratacion"  SortExpression="FRealComiteContratacion"/>
                            <asp:BoundField HeaderText="F. real inicio PS" DataField="FRealInicioPS"  SortExpression="FRealInicioPS"/>
                            <asp:BoundField HeaderText="F. real presentaci&oacute;n propuestas" DataField="FRealPresentacionPropuestas"  SortExpression="FRealPresentacionPropuestas"/>
                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
         <table width="50%" align="center" border="1" >
             <tr>
                 <td> F</td>
                 <td> Fecha</td>
                 <td> REG</td>
                 <td> Registro</td>
                 <td> Docs</td>
                 <td> Documentos</td>
             </tr>
              <tr>
                 <td> PS</td>
                 <td> Proceso de selecci&oacute;n</td>
                 <td> ES</td>
                 <td> Estudios de sector</td>
                 <td> SDC</td>
                 <td> Solicitud de cotizaci&oacute;n</td>
             </tr>
             <tr>
                 <td> DT</td>
                 <td> Documentos t&eacute;cnicos</td>
                 <td> EC</td>
                 <td> Estudio de costos</td>
                 <td> MC</td>
                 <td> M&iacute;nima cuant&iacute;a</td>
             </tr>
         </table>
    </asp:Panel>
</asp:Content>
