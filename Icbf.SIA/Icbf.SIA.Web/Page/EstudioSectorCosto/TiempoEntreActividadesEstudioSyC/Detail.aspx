<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TiempoEntreActividadesEstudioSyC_Detail" %>
<%@ Register TagPrefix="uc1" TagName="Fechas" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfIdTiempoEntreActividades" runat="server" />

    <asp:Panel ID="pnlContenedor" runat="server">
        <table width="70%" align="center">
            <tr class="rowB">
                <td>Consecutivo  solicitud *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdConsecutivoEstudio" ControlToValidate="txtIdConsecutivoEstudio"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" Enabled="false" ID="txtIdConsecutivoEstudio" MaxLength="5"  ClientIDMode="Static"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtIdConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                    <asp:ImageButton runat="server" ID="btnLupa" ImageUrl="~/Image/btn/list.png" class="SetFocus" OnClientClick="MostrarPopUp();return false;" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlTiemposFechasESyC" runat="server" GroupingText="Tiempos y Fechas ESyC" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>F. Entrega ES-EC tiempos equipo
                                </td>
                                <td>F. Entrega ES-EC tiempos indicador
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFentregaES_ECTiemposEquipo"></asp:TextBox>
                                    <asp:Label ID="lblFentregaES_ECTiemposEquipo" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEentregaES_ECTiemposIndicador"></asp:TextBox>
                                    <asp:Label ID="lblFEentregaES_ECTiemposIndicador" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Días hábiles entre DT iniciales y finales / para ES-EC entre Solicitud inicial y cambio en los DT aprobados 
                                </td>
                                <td>Días hábiles entre Solicitud de ES y SDC
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"></asp:TextBox>
                                    <asp:Label ID="lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiasHabilesEntreFCTFinalYSDC"></asp:TextBox>
                                    <asp:Label ID="lblDiasHabilesEntreFCTFinalYSDC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Días hábiles en ES O COSTEO
                                </td>
                                <td>Días hábiles para devolución
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiasHabilesEnESOCOSTEO"></asp:TextBox>
                                    <asp:Label ID="lblDiasHabilesEnESOCOSTEO" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                     <asp:Label ID="lblDiasHabilesEnESOCOSTEODevuelto" runat="server" Text="Devuelto" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiashabilesParaDevolucion"></asp:TextBox>
                                    <asp:Label ID="lblDiashabilesParaDevolucion" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlPACCO" runat="server" GroupingText="PACCO" BorderColor="1" Style="border-width: 1px; border-style: solid;">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>F. Estimada DT preliminar REG. INICIAL
                                </td>
                                <td>F. Estimada DT preliminar
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTPreliminarREGINICIAL"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaFCTPreliminarREGINICIAL" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTPreliminar"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaFCTPreliminar" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada DT definitivos/para MC F. Estimada ES_MC revisado
                                </td>
                                <td>F. Estimada  ES-EC
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEestimadaES_EC"></asp:TextBox>
                                    <asp:Label ID="lblFEestimadaES_EC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada docs radicados en contratos
                                </td>
                                <td>F. Estimada comité contratación
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDocsRadicadosEnContratos"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaDocsRadicadosEnContratos" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaComiteContratacion"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaComiteContratacion" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada inicio del PS
                                </td>
                                <td>F. Estimada presentación propuestas
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaInicioDelPS"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaInicioDelPS" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaPresentacionPropuestas"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaPresentacionPropuestas" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada de inicio de ejecución
                                </td>

                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDeInicioDeEjecucion"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaDeInicioDeEjecucion" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlechasSistemaESC" runat="server" GroupingText="Fechas Sistema ESC" BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>F. Real DT preliminar ESC
                                </td>
                                <td>F. Estimada DT definitivos / para MC F. Estimada ES_MC revisado ESC
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealFCTPreliminarESC"></asp:TextBox>
                                    <asp:Label ID="lblFRealFCTPreliminarESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada  ES-EC. ESC
                                </td>
                                <td>F. Estimada docs radicados en contratos ESC
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaES_EC_ESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaES_EC_ESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDocsRadicadosEnContratosESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaDocsRadicadosEnContratosESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada comité contratación ESC
                                </td>
                                <td>F. Estimada inicio del PS. ESC
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaComitecontratacionESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaComitecontratacionESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaInicioDelPS_ESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaInicioDelPS_ESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Estimada presentación propuestas ESC
                                </td>
                                <td>F. Estimada de inicio de ejecución ESC
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaPresentacionPropuestasESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaPresentacionPropuestasESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDeInicioDeEjecucionESC"></asp:TextBox>
                                    <asp:Label ID="lblFEstimadaDeInicioDeEjecucionESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Panel ID="pnlFechasReales" runat="server" GroupingText="Fechas Reales" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>F. Real DT preliminar
                                </td>
                                <td>F. Real docs radicados en contratos
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealFCTPreliminar"></asp:TextBox>
                                    <asp:Label ID="lblFRealFCTPreliminar" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <uc1:fechas runat="server" id="txtFRealDocsRadicadosEnContratos" />

                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. real DT avalados
                                </td>
                                <td>F. Real comité contratación
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealFCTDefinitivaParaMCFRealES_MCRevisado"></asp:TextBox>
                                    <asp:Label ID="lblFRealFCTDefinitivaParaMCFRealES_MCRevisado" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <uc1:fechas id="txtFRealComiteContratacion" runat="server" />

                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>F. Real ES-EC
                                </td>
                                <td>F. Real inicio PS
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealES_EC"></asp:TextBox>
                                    <asp:Label ID="lblFRealES_EC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                </td>
                                <td>
                                    <uc1:fechas runat="server" id="txtFRealInicioPS" />
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td colspan="2">F. Real presentación propuestas
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td colspan="2">
                                    <uc1:fechas runat="server" id="txtFRealPresentacionPropuestas" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
        </table>
        <br />
        <table width="50%" align="center" border="1">
            <tr>
                <td>F</td>
                <td>Fecha</td>
                <td>REG</td>
                <td>Registro</td>
                <td>Docs</td>
                <td>Documentos</td>
            </tr>
            <tr>
                <td>PS</td>
                <td>Proceso de selección</td>
                <td>ES</td>
                <td>Estudios de sector</td>
                <td>SDC</td>
                <td>Solicitud de cotización</td>
            </tr>
            <tr>
                <td>DT</td>
                <td>Documentos técnicos</td>
                <td>EC</td>
                <td>Estudio de costos</td>
                <td>MC</td>
                <td>Mínima cuantía</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
