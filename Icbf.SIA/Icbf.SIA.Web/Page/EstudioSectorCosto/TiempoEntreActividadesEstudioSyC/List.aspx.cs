//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_TiempoEntreActividadesEstudioSyC_List</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Service;
using System.Linq.Expressions;
using System.Data;
using System.IO;
using System.Text;

/// <summary>
/// Clase para tiempos entre actividades
/// </summary>
public partial class Page_TiempoEntreActividadesEstudioSyC_List : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/TiempoEntreActividadesEstudioSyC";

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

   

    #region  METODOS

    /// <summary>
    /// M�todo para buscar registros de tiempo entre actividades
    /// </summary>
    private void Buscar()
    {
        try
        {
            LLenarGrillaPACCO(BuscarTiemposPACCO(), gvTiempoEntreActividadesEstudioPACCO);
            LLenarGrilla(BuscarTiemposESC(), gvTiempoEntreActividadesCalculosParametrica,false);
            LLenarGrilla(BuscarTiemposReales(), gvTiempoEntreActividadesEstudioREAL,true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para consultar tiempos reales
    /// </summary>
    /// <returns>Lista de tiempos reales</returns>
    private List<TiempoEntreActividadesEstudioSyC> BuscarTiemposReales()
    {
        try
        {
            ucConsulta.GetValues();
            long? vIdConsecutivoEstudio = ucConsulta.vIdConsecutivoEstudio;
            string vNombreAbreviado = ucConsulta.vNombreAbreviado;
            string vObjeto = ucConsulta.vObjeto;
            int? vIdResponsableES = ucConsulta.vIdResponsableES;
            int? vIdResponsableEC = ucConsulta.vIdResponsableEC;
            int? vIdModalidadDeSeleccion = ucConsulta.vIdModalidadDeSeleccion;
            int? vDireccionSolicitante = ucConsulta.vDireccionSolicitante;
            int? vEstado = ucConsulta.vEstado;
            List<TiempoEntreActividadesEstudioSyC> vLstFechasReales = vResultadoEstudioSectorService.ConsultarTiempoEntreActividadesFechasRealess(vIdConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdResponsableES, vIdResponsableEC, vIdModalidadDeSeleccion, vDireccionSolicitante, vEstado);
            foreach (TiempoEntreActividadesEstudioSyC item in vLstFechasReales)
            {
                item.FRealFCTPreliminar = item.FRealFCTPreliminar != null ? item.FRealFCTPreliminar.Substring(0, 10) : string.Empty;
                item.FRealFCTDefinitivaParaMCFRealES_MCRevisado = item.FRealFCTDefinitivaParaMCFRealES_MCRevisado != null ? item.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Substring(0, 10) : string.Empty;
                item.FRealES_EC = item.FRealES_EC != null ? item.FRealES_EC.Substring(0, 10) : string.Empty;
                item.FRealDocsRadicadosEnContratos = item.FRealDocsRadicadosEnContratos != null ? item.FRealDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
                item.FRealComiteContratacion = item.FRealComiteContratacion != null ? item.FRealComiteContratacion.Substring(0, 10) : string.Empty;
                item.FRealInicioPS = item.FRealInicioPS != null ? item.FRealInicioPS.Substring(0, 10) : string.Empty;
                item.FRealPresentacionPropuestas = item.FRealPresentacionPropuestas != null ? item.FRealPresentacionPropuestas.Substring(0, 10) : string.Empty;
            }
            return vLstFechasReales;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return null;
    }

    /// <summary>
    /// M�todo para consultar tiempos ESC
    /// </summary>
    /// <returns>Lista de tiempos ESC</returns>
    private List<TiempoEntreActividadesEstudioSyC> BuscarTiemposESC()
    {
        try
        {
            ucConsulta.GetValues();
            long? vIdConsecutivoEstudio = ucConsulta.vIdConsecutivoEstudio;
            string vNombreAbreviado = ucConsulta.vNombreAbreviado;
            string vObjeto = ucConsulta.vObjeto;
            int? vIdResponsableES = ucConsulta.vIdResponsableES;
            int? vIdResponsableEC = ucConsulta.vIdResponsableEC;
            int? vIdModalidadDeSeleccion = ucConsulta.vIdModalidadDeSeleccion;
            int? vDireccionSolicitante = ucConsulta.vDireccionSolicitante;
            int? vEstado = ucConsulta.vEstado;
            List<TiempoEntreActividadesEstudioSyC> vLstFechasESCs = vResultadoEstudioSectorService.ConsultarTiempoEntreActividadesFechasESCs(vIdConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdResponsableES, vIdResponsableEC, vIdModalidadDeSeleccion, vDireccionSolicitante, vEstado);
            foreach (TiempoEntreActividadesEstudioSyC item in vLstFechasESCs)
            {
                item.FRealFCTPreliminarESC = item.FRealFCTPreliminarESC != null ? item.FRealFCTPreliminarESC.Substring(0, 10) : string.Empty;
                item.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = item.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC != null ? item.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Substring(0, 10) : string.Empty;
                item.FEstimadaES_EC_ESC = item.FEstimadaES_EC_ESC != null ? item.FEstimadaES_EC_ESC.Substring(0, 10) : string.Empty;
                item.FEstimadaDocsRadicadosEnContratosESC = item.FEstimadaDocsRadicadosEnContratosESC != null ? item.FEstimadaDocsRadicadosEnContratosESC.Substring(0, 10) : string.Empty;
                item.FEstimadaComitecontratacionESC = item.FEstimadaComitecontratacionESC != null ? item.FEstimadaComitecontratacionESC.Substring(0, 10) : string.Empty;
                item.FEstimadaInicioDelPS_ESC = item.FEstimadaInicioDelPS_ESC != null ? item.FEstimadaInicioDelPS_ESC.Substring(0, 10) : string.Empty;
                item.FEstimadaPresentacionPropuestasESC = item.FEstimadaPresentacionPropuestasESC != null ? item.FEstimadaPresentacionPropuestasESC.Substring(0, 10) : string.Empty;
                item.FEstimadaDeInicioDeEjecucionESC = item.FEstimadaDeInicioDeEjecucionESC != null ? item.FEstimadaDeInicioDeEjecucionESC.Substring(0, 10) : string.Empty;
            }
            return vLstFechasESCs;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return null;
    }

    /// <summary>
    /// M�todo para uscar tiempos PACCO
    /// </summary>
    /// <returns>Lista PACCO</returns>
    private List<TiemposPACCO> BuscarTiemposPACCO()
    {
        try
        {
            ucConsulta.GetValues();
            long? vIdConsecutivoEstudio = ucConsulta.vIdConsecutivoEstudio;
            string vNombreAbreviado = ucConsulta.vNombreAbreviado;
            string vObjeto = ucConsulta.vObjeto;
            int? vIdResponsableES = ucConsulta.vIdResponsableES;
            int? vIdResponsableEC = ucConsulta.vIdResponsableEC;
            int? vIdModalidadDeSeleccion = ucConsulta.vIdModalidadDeSeleccion;
            int? vDireccionSolicitante = ucConsulta.vDireccionSolicitante;
            int? vEstado = ucConsulta.vEstado;
            List<TiemposPACCO> vLstPACCO = vResultadoEstudioSectorService.ConsultarTiempoEntreActividadesPACCOs(vIdConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdResponsableES, vIdResponsableEC, vIdModalidadDeSeleccion, vDireccionSolicitante, vEstado);
            foreach (TiemposPACCO item in vLstPACCO)
            {
                item.FEstimadaFCTPreliminarREGINICIAL = item.FEstimadaFCTPreliminarREGINICIAL != null ? item.FEstimadaFCTPreliminarREGINICIAL.Substring(0, 10) : string.Empty;
                item.FEstimadaFCTPreliminar = item.FEstimadaFCTPreliminar != null ? item.FEstimadaFCTPreliminar.Substring(0, 10) : string.Empty;
                item.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = item.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado != null ? item.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Substring(0, 10) : string.Empty;
                item.FEestimadaES_EC = item.FEestimadaES_EC != null ? item.FEestimadaES_EC.Substring(0, 10) : string.Empty;
                item.FEstimadaDocsRadicadosEnContratos = item.FEstimadaDocsRadicadosEnContratos != null ? item.FEstimadaDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
                item.FEstimadaComiteContratacion = item.FEstimadaComiteContratacion != null ? item.FEstimadaComiteContratacion.Substring(0, 10) : string.Empty;
                item.FEstimadaInicioDelPS = item.FEstimadaInicioDelPS != null ? item.FEstimadaInicioDelPS.Substring(0, 10) : string.Empty;
                item.FEstimadaPresentacionPropuestas = item.FEstimadaPresentacionPropuestas != null ? item.FEstimadaPresentacionPropuestas.Substring(0, 10) : string.Empty;
                item.FEstimadaDeInicioDeEjecucion = item.FEstimadaDeInicioDeEjecucion != null ? item.FEstimadaDeInicioDeEjecucion.Substring(0, 10) : string.Empty;
            }
            MostrarOcultarTitulos(true);
            return vLstPACCO;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return null;

    }

    /// <summary>
    /// M�todo para llenar grilla PACCO
    /// </summary>
    /// <param name="pLstTiempos">Lista tiempos PACCO</param>
    /// <param name="gvTiempos">Grilla PACCO</param>
    protected void LLenarGrillaPACCO(List<TiemposPACCO> pLstTiempos, GridView gvTiempos)
    {
        if (pLstTiempos == null || pLstTiempos.Count == 0)
        {
            gvTiempos.DataSource = null;
            gvTiempos.DataBind();
            MostrarOcultarTitulos(false);
            return;
        }
        toolBar.LipiarMensajeError();
        string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
        if (pSortExpresion.Equals(string.Empty))
        {
            OrdenarGrillaPACCO(Constantes.SinOrden, pLstTiempos, gvTiempos);
        }
        else
        {
            OrdenarGrillaPACCO(pSortExpresion, pLstTiempos, gvTiempos);
        }
        MostrarOcultarTitulos(true);

    }

    /// <summary>
    /// M�todo para ordenar la grilla PACCO
    /// </summary>
    /// <param name="pSortExpresion">Expresio�n de ordenamiento</param>
    /// <param name="vLstTiempos">Liste PACCO</param>
    /// <param name="gvTiempos">Grilla PACCO</param>
    protected void OrdenarGrillaPACCO(string pSortExpresion, List<TiemposPACCO> vLstTiempos, GridView gvTiempos)
    {
        if (vLstTiempos != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvTiempos.DataSource = vLstTiempos;
                gvTiempos.DataBind();
                MostrarOcultarTitulos(false);
                return;

            }

            var param = Expression.Parameter(typeof(TiemposPACCO), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<TiemposPACCO, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);

            if (GridViewSortDirectionPACCO == SortDirection.Ascending)
            {
                gvTiempos.DataSource = vLstTiempos.AsQueryable<TiemposPACCO>().OrderBy(sortExpression).ToList<TiemposPACCO>();
            }
            else
            {
                gvTiempos.DataSource = vLstTiempos.AsQueryable<TiemposPACCO>().OrderByDescending(sortExpression).ToList<TiemposPACCO>();
            };

            gvTiempos.DataBind();
        }
    }

    /// <summary>
    /// M�todo para llenar grilla tiempos
    /// </summary>
    /// <param name="pLstTiempos">Lista tiempos</param>
    /// <param name="gvTiempos">Grilla tiempos</param>
    protected void LLenarGrilla(List<TiempoEntreActividadesEstudioSyC> pLstTiempos, GridView gvTiempos,bool pEsReal)
    {
        if (pLstTiempos == null || pLstTiempos.Count == 0)
        {
            gvTiempos.DataSource = null;
            gvTiempos.DataBind();
            MostrarOcultarTitulos(false);
            return;
        }
        toolBar.LipiarMensajeError();
        string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
        if (pSortExpresion.Equals(string.Empty))
        {
            OrdenarGrilla(Constantes.SinOrden, pLstTiempos, gvTiempos,pEsReal);
        }
        else
        {
            OrdenarGrilla(pSortExpresion, pLstTiempos, gvTiempos,pEsReal);
        }

    }

    /// <summary>
    /// M�todo para ordenar grilla
    /// </summary>
    /// <param name="pSortExpresion">Expresi�n de ordenamiento</param>
    /// <param name="vLstTiempos">Lista tiempos</param>
    /// <param name="gvTiempos">Grilla tiempos</param>
    protected void OrdenarGrilla(string pSortExpresion, List<TiempoEntreActividadesEstudioSyC> vLstTiempos, GridView gvTiempos,bool pEsReal)
    {
        if (vLstTiempos != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvTiempos.DataSource = vLstTiempos;
                gvTiempos.DataBind();
                return;

            }

            var param = Expression.Parameter(typeof(TiempoEntreActividadesEstudioSyC), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<TiempoEntreActividadesEstudioSyC, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);
            if (pEsReal)
            {
                if (GridViewSortDirectionREAL == SortDirection.Ascending)
                {
                    gvTiempos.DataSource = vLstTiempos.AsQueryable<TiempoEntreActividadesEstudioSyC>().OrderBy(sortExpression).ToList<TiempoEntreActividadesEstudioSyC>();
                }
                else
                {
                    gvTiempos.DataSource = vLstTiempos.AsQueryable<TiempoEntreActividadesEstudioSyC>().OrderByDescending(sortExpression).ToList<TiempoEntreActividadesEstudioSyC>();
                };
            }
            else
            {

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    gvTiempos.DataSource = vLstTiempos.AsQueryable<TiempoEntreActividadesEstudioSyC>().OrderBy(sortExpression).ToList<TiempoEntreActividadesEstudioSyC>();
                }
                else
                {
                    gvTiempos.DataSource = vLstTiempos.AsQueryable<TiempoEntreActividadesEstudioSyC>().OrderByDescending(sortExpression).ToList<TiempoEntreActividadesEstudioSyC>();
                };
            }                       

            gvTiempos.DataBind();
        }
    }

    /// <summary>
    /// M�todo para configurar p�gina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvTiempoEntreActividadesEstudioPACCO.PageSize = PageSize();
            gvTiempoEntreActividadesEstudioPACCO.EmptyDataText = EmptyDataText();

            gvTiempoEntreActividadesCalculosParametrica.PageSize = PageSize();         

            gvTiempoEntreActividadesEstudioREAL.PageSize = PageSize();           

            toolBar.EstablecerTitulos("Tiempo entre actividades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Mpetodo para seleccionar registro de la grilla PACCO
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;

            string strValue = gvTiempoEntreActividadesEstudioPACCO.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TiempoEntreActividadesEstudioSyC.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TiempoEntreActividadesEstudioSyC.Eliminado");
            MostrarOcultarTitulos(false);            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Mpetodo para mostrar u ocultar t�tulos
    /// </summary>
    /// <param name="pVisible"></param>
    private void MostrarOcultarTitulos(bool pVisible)
    {
        lblPACCO.Visible = pVisible;
        lblParametrica.Visible = pVisible;
        lblReal.Visible = pVisible;
    }

    /// <summary>
    /// M�todo para exportar la grillar
    /// </summary>
    private void Exportar()
    {
        toolBar.LipiarMensajeError();


        Response.Clear();
        Response.Buffer = true;

        Response.AddHeader("content-disposition",
         "attachment;filename=TiempoActividades_" + DateTime.Now.ToString("ddMMyyyy") + ".xls");
        Response.Charset = "UTF-8";
        Response.ContentType = "application/octet-stream";
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(sw);


        DataTable dt = new DataTable();
        gvTiempoEntreActividadesEstudioPACCO.AllowPaging = false;
        gvTiempoEntreActividadesEstudioPACCO.DataSource = BuscarTiemposPACCO();
        gvTiempoEntreActividadesEstudioPACCO.DataBind();
        for (int i = 1; i < gvTiempoEntreActividadesEstudioPACCO.Columns.Count; i++)
        {
            dt.Columns.Add(gvTiempoEntreActividadesEstudioPACCO.Columns[i].HeaderText);
        }
        foreach (GridViewRow row in gvTiempoEntreActividadesEstudioPACCO.Rows)
        {
            DataRow dr = dt.NewRow();
            for (int j = 1; j < gvTiempoEntreActividadesEstudioPACCO.Columns.Count; j++)
            {
                string cad = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                dr[gvTiempoEntreActividadesEstudioPACCO.Columns[j].HeaderText] = cad.Equals("&nbsp;") ? string.Empty : cad;

            }

            dt.Rows.Add(dr);
        }

        GridView datosexportar = new GridView();
        datosexportar.DataSource = dt;
        datosexportar.DataBind();


        DataTable dt2 = new DataTable();
        gvTiempoEntreActividadesCalculosParametrica.AllowPaging = false;
        gvTiempoEntreActividadesCalculosParametrica.DataSource = BuscarTiemposESC();
        gvTiempoEntreActividadesCalculosParametrica.DataBind();
        for (int i = 0; i < gvTiempoEntreActividadesCalculosParametrica.Columns.Count; i++)
        {
            dt2.Columns.Add(gvTiempoEntreActividadesCalculosParametrica.Columns[i].HeaderText);
        }
        foreach (GridViewRow row in gvTiempoEntreActividadesCalculosParametrica.Rows)
        {
            DataRow dr = dt2.NewRow();
            for (int j = 0; j < gvTiempoEntreActividadesCalculosParametrica.Columns.Count; j++)
            {
                string cad = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                dr[gvTiempoEntreActividadesCalculosParametrica.Columns[j].HeaderText] = cad.Equals("&nbsp;") ? string.Empty : cad;
            }

            dt2.Rows.Add(dr);
        }

        GridView datosexportar2 = new GridView();
        datosexportar2.DataSource = dt2;
        datosexportar2.DataBind();

        DataTable dt3 = new DataTable();
        gvTiempoEntreActividadesEstudioREAL.AllowPaging = false;
        gvTiempoEntreActividadesEstudioREAL.DataSource = BuscarTiemposReales();
        gvTiempoEntreActividadesEstudioREAL.DataBind();
        for (int i = 0; i < gvTiempoEntreActividadesEstudioREAL.Columns.Count; i++)
        {
            dt3.Columns.Add(gvTiempoEntreActividadesEstudioREAL.Columns[i].HeaderText);
        }
        foreach (GridViewRow row in gvTiempoEntreActividadesEstudioREAL.Rows)
        {
            DataRow dr = dt3.NewRow();
            for (int j = 0; j < gvTiempoEntreActividadesEstudioREAL.Columns.Count; j++)
            {
                string cad = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                dr[gvTiempoEntreActividadesEstudioREAL.Columns[j].HeaderText] = cad.Equals("&nbsp;") ? string.Empty : cad;

            }

            dt3.Rows.Add(dr);
        }

        GridView datosexportar3 = new GridView();
        datosexportar3.DataSource = dt3;
        datosexportar3.DataBind();


        Table tb = new Table();
        TableRow tr1t = new TableRow();
        TableCell cell1t = new TableCell();
        Label lbl1t = new Label();
        lbl1t.Text = "PACCO";
        lbl1t.Font.Bold = true;
        lbl1t.Font.Size = 15;
        cell1t.Controls.Add(lbl1t);
        tr1t.Cells.Add(cell1t);

        TableRow tr1 = new TableRow();
        TableCell cell1 = new TableCell();
        cell1.Controls.Add(datosexportar);
        tr1.Cells.Add(cell1);

        TableRow tre = new TableRow();
        TableCell celle = new TableCell();
        celle.Text = string.Empty;
        tre.Cells.Add(celle);
        TableRow tre2 = new TableRow();
        TableCell celle2 = new TableCell();
        celle2.Text = string.Empty;
        tre2.Cells.Add(celle2);

        TableRow tr2T = new TableRow();
        TableCell cell2T = new TableCell();
        Label lbl2t = new Label();
        lbl2t.Text = "C&aacute;lculos param&eacute;trica";
        lbl2t.Font.Bold = true;
        lbl2t.Font.Size = 15;
        cell2T.Controls.Add(lbl2t);
        tr2T.Cells.Add(cell2T);

        TableRow tr2 = new TableRow();
        TableCell cell2 = new TableCell();
        cell2.Controls.Add(datosexportar2);
        tr2.Cells.Add(cell2);


        TableRow tr3t = new TableRow();
        TableCell cell3t = new TableCell();
        Label lbl3t = new Label();
        lbl3t.Text = "REAL";
        lbl3t.Font.Bold = true;
        lbl3t.Font.Size = 15;
        cell3t.Controls.Add(lbl3t);
        tr3t.Cells.Add(cell3t);

        TableRow tr3 = new TableRow();
        TableCell cell3 = new TableCell();
        cell3.Controls.Add(datosexportar3);
        tr3.Cells.Add(cell3);


        tb.Rows.Add(tr1t);
        tb.Rows.Add(tr1);
        tb.Rows.Add(tre);
        tb.Rows.Add(tr2T);
        tb.Rows.Add(tr2);
        tb.Rows.Add(tre2);
        tb.Rows.Add(tr3t);
        tb.Rows.Add(tr3);


        tb.RenderControl(hw);

        //style to format numbers to string

        Response.Write(sb.ToString());
        Response.Flush();
        Response.End();

    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento click bot�n buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {

        Buscar();
    }

    /// <summary>
    /// Evento click bot�n nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento paginar grilla PACCO
    /// </summary>
    /// <param name="sender">Grilla gvTiempoEntreActividadesEstudioPACCO</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesEstudioPACCO_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTiempoEntreActividadesEstudioPACCO.PageIndex = e.NewPageIndex;
        LLenarGrillaPACCO(BuscarTiemposPACCO(), gvTiempoEntreActividadesEstudioPACCO);
    }


    /// <summary>
    /// Evento seleccionar registro grilla PACCO
    /// </summary>
    /// <param name="sender">Grilla gvTiempoEntreActividadesEstudioPACCO</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesEstudioPACCO_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTiempoEntreActividadesEstudioPACCO.SelectedRow);
    }


    /// <summary>
    /// Evento ordenar grilla PACCO
    /// </summary>
    /// <param name="sender">Grilla gvTiempoEntreActividadesEstudioPACCO</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesEstudioPACCO_Sorting(object sender, GridViewSortEventArgs e)
    {

        GridViewSortDirectionPACCO = GridViewSortDirectionPACCO == SortDirection.Descending ? SortDirection.Ascending : SortDirection.Descending;
        OrdenarGrillaPACCO(e.SortExpression, BuscarTiemposPACCO(), gvTiempoEntreActividadesEstudioPACCO);
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;
    }

    /// <summary>
    /// Evento ordenar grilla c�lculos parametrica
    /// </summary>
    /// <param name="sender">Grilla gvTiempoEntreActividadesCalculosParametrica</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesCalculosParametrica_Sorting(object sender, GridViewSortEventArgs e)
    {
       
        GridViewSortDirection = GridViewSortDirection == SortDirection.Descending ? SortDirection.Ascending : SortDirection.Descending;
        OrdenarGrilla(e.SortExpression, BuscarTiemposESC(), gvTiempoEntreActividadesCalculosParametrica,false);
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;
    }

    /// <summary>
    /// Evento paginar grilla c�lculos parametrica
    /// </summary>
    /// <param name="sender">Grilla gvTiempoEntreActividadesCalculosParametrica</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesCalculosParametrica_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTiempoEntreActividadesCalculosParametrica.PageIndex = e.NewPageIndex;
        LLenarGrilla(BuscarTiemposESC(), gvTiempoEntreActividadesCalculosParametrica,false);
    }

    /// <summary>
    /// Evento ordenar grilla real
    /// </summary>
    /// <param name="sender">Grilla vTiempoEntreActividadesEstudioREAL</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesEstudioREAL_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortDirectionREAL = GridViewSortDirectionREAL == SortDirection.Descending ? SortDirection.Ascending : SortDirection.Descending;
        OrdenarGrilla(e.SortExpression, BuscarTiemposReales(), gvTiempoEntreActividadesEstudioREAL,true);       

    }

    /// <summary>
    /// Evento paginar grilla real
    /// </summary>
    /// <param name="sender">Grilla vTiempoEntreActividadesEstudioREAL</param>
    /// <param name="e">Evento click</param>
    protected void gvTiempoEntreActividadesEstudioREAL_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTiempoEntreActividadesEstudioREAL.PageIndex = e.NewPageIndex;
        LLenarGrilla(BuscarTiemposReales(), gvTiempoEntreActividadesEstudioREAL,true);
    }

    /// <summary>
    /// Evento click bot�n exportar
    /// </summary>
    /// <param name="sender">Boton btnExportar</param>
    /// <param name="e">Evento click</param>
    protected void btnExportar_Click(object sender, EventArgs e)
    {
           
        if (gvTiempoEntreActividadesEstudioPACCO.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            gvTiempoEntreActividadesEstudioPACCO.DataSource = null;
            gvTiempoEntreActividadesEstudioPACCO.DataBind();
            gvTiempoEntreActividadesCalculosParametrica.DataSource = null;
            gvTiempoEntreActividadesCalculosParametrica.DataBind();
            gvTiempoEntreActividadesEstudioREAL.DataSource = null;
            gvTiempoEntreActividadesEstudioREAL.DataBind();
        }
    }
    #endregion

    /// <summary>
    /// Atributo que describe la direcci�n del ordenamiento
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState[Constantes.ViewStateSortDirection] == null)
            {
                ViewState[Constantes.ViewStateSortDirection] = SortDirection.Ascending;
            }


            return (SortDirection)ViewState[Constantes.ViewStateSortDirection];
        }
        set { ViewState[Constantes.ViewStateSortDirection] = value; }
    }

    /// <summary>
    /// Atributo que describe la direcci�n del ordenamiento
    /// </summary>
    public SortDirection GridViewSortDirectionREAL
    {
        get
        {
            if (ViewState["SortDirectionREAL"] == null)
            {
                ViewState["SortDirectionREAL"] = SortDirection.Ascending;
            }


            return (SortDirection)ViewState["SortDirectionREAL"];
        }
        set { ViewState["SortDirectionREAL"] = value; }
    }
    /// <summary>
    /// Atributo que describe la direcci�n del ordenamiento
    /// </summary>
    public SortDirection GridViewSortDirectionPACCO
    {
        get
        {
            if (ViewState["SortDirectionPACCO"] == null)
            {
                ViewState["SortDirectionPACCO"] = SortDirection.Ascending;
            }


            return (SortDirection)ViewState["SortDirectionPACCO"];
        }
        set { ViewState["SortDirectionPACCO"] = value; }
    }
}
