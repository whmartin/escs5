//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_GestionesEstudioSectorCostos_Add</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;

using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;


/// <summary>
/// Clase para agregar tiempos entre actividades
/// </summary>
public partial class Page_TiempoEntreActividadesEstudioSyC_Add : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;

    string PageName = "EstudioSectorCosto/TiempoEntreActividadesEstudioSyC";

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vEstudioSectorCostoService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Instancia a capa de servicio EstudioSectorCostoService
    /// </summary>
    EstudioSectorCostoService vEstudioService = new EstudioSectorCostoService();

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Método guardar tiempos entre activiades
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (!ValidarFestivos())
            {
                return;
            }
            decimal vResultado;
            TiempoEntreActividadesEstudioSyC vTiempoEntreActividades = new TiempoEntreActividadesEstudioSyC();
            TiempoEntreActividadesEstudioSyC vTiemposESyC = new TiempoEntreActividadesEstudioSyC();
            TiemposPACCO vTiempoPACCO = new TiemposPACCO();
            TiempoEntreActividadesEstudioSyC vTiemposESC = new TiempoEntreActividadesEstudioSyC();
            TiempoEntreActividadesEstudioSyC vTiemposReales = new TiempoEntreActividadesEstudioSyC();

            vTiempoEntreActividades.IdConsecutivoEstudio = decimal.Parse(txtIdConsecutivoEstudio.Text);


            vTiemposESyC.FentregaES_ECTiemposEquipo = txtFentregaES_ECTiemposEquipo.Text;
            vTiemposESyC.FEentregaES_ECTiemposIndicador = txtFEentregaES_ECTiemposIndicador.Text;
            vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado = txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Text;
            vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR = lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Visible; ;
            vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor = txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.BackColor.Name;
            vTiemposESyC.DiasHabilesEntreFCTFinalYSDC = txtDiasHabilesEntreFCTFinalYSDC.Text;
            vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR = lblDiasHabilesEntreFCTFinalYSDC.Visible;
            vTiemposESyC.DiasHabilesEntreFCTFinalYSDCColor = txtDiasHabilesEntreFCTFinalYSDC.BackColor.Name;
            vTiemposESyC.DiasHabilesEnESOCOSTEO = txtDiasHabilesEnESOCOSTEO.Text;
            vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR = lblDiasHabilesEnESOCOSTEO.Visible;
            vTiemposESyC.DevueltoDiasHabilesEnESOCOSTEOR = lblDiasHabilesEnESOCOSTEODevuelto.Visible;
            vTiemposESyC.DiasHabilesEnESOCOSTEOColor = txtDiasHabilesEnESOCOSTEO.BackColor.Name;
            vTiemposESyC.DiashabilesParaDevolucion = txtDiashabilesParaDevolucion.Text;
            vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR = lblDiashabilesParaDevolucion.Visible;
            vTiemposESyC.DiashabilesParaDevolucionColor = txtDiashabilesParaDevolucion.BackColor.Name;


            vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL = txtFEstimadaFCTPreliminarREGINICIAL.Text;
            vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL = lblFEstimadaFCTPreliminarREGINICIAL.Visible;
            vTiempoPACCO.FEstimadaFCTPreliminar = txtFEstimadaFCTPreliminar.Text;
            vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar = lblFEstimadaFCTPreliminar.Visible;
            vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Text;
            vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Visible;
            vTiempoPACCO.FEestimadaES_EC = txtFEestimadaES_EC.Text;
            vTiempoPACCO.NoAplicaFEestimadaES_EC = lblFEestimadaES_EC.Visible;
            vTiempoPACCO.FEstimadaDocsRadicadosEnContratos = txtFEstimadaDocsRadicadosEnContratos.Text;
            vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos = lblFEstimadaDocsRadicadosEnContratos.Visible;
            vTiempoPACCO.FEstimadaComiteContratacion = txtFEstimadaComiteContratacion.Text;
            vTiempoPACCO.NoAplicaFEstimadaComiteContratacion = lblFEstimadaComiteContratacion.Visible;
            vTiempoPACCO.FEstimadaInicioDelPS = txtFEstimadaInicioDelPS.Text;
            vTiempoPACCO.NoAplicaFEstimadaInicioDelPS = lblFEstimadaInicioDelPS.Visible;
            vTiempoPACCO.FEstimadaPresentacionPropuestas = txtFEstimadaPresentacionPropuestas.Text;
            vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas = lblFEstimadaPresentacionPropuestas.Visible;
            vTiempoPACCO.FEstimadaDeInicioDeEjecucion = txtFEstimadaDeInicioDeEjecucion.Text;


            vTiemposESC.FRealFCTPreliminarESC = txtFRealFCTPreliminarESC.Text;
            vTiemposESC.NoAplicaFRealFCTPreliminarESC = lblFRealFCTPreliminarESC.Visible;
            vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Text;
            vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Visible;
            vTiemposESC.FEstimadaES_EC_ESC = txtFEstimadaES_EC_ESC.Text;
            vTiemposESC.NoAplicaFEstimadaES_EC_ESC = lblFEstimadaES_EC_ESC.Visible;
            vTiemposESC.FEstimadaDocsRadicadosEnContratosESC = txtFEstimadaDocsRadicadosEnContratosESC.Text;
            vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC = lblFEstimadaDocsRadicadosEnContratosESC.Visible;
            vTiemposESC.FEstimadaComitecontratacionESC = txtFEstimadaComitecontratacionESC.Text;
            vTiemposESC.NoAplicaFEstimadaComitecontratacionESC = lblFEstimadaComitecontratacionESC.Visible;
            vTiemposESC.FEstimadaInicioDelPS_ESC = txtFEstimadaInicioDelPS_ESC.Text;
            vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC = lblFEstimadaInicioDelPS_ESC.Visible;
            vTiemposESC.FEstimadaPresentacionPropuestasESC = txtFEstimadaPresentacionPropuestasESC.Text;
            vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC = lblFEstimadaPresentacionPropuestasESC.Visible;
            vTiemposESC.FEstimadaDeInicioDeEjecucionESC = txtFEstimadaDeInicioDeEjecucionESC.Text;

            vTiemposReales.FRealFCTPreliminar = txtFRealFCTPreliminar.Text;
            vTiemposReales.FRealDocsRadicadosEnContratos = txtFRealDocsRadicadosEnContratos.GetDate() == null ? string.Empty : txtFRealDocsRadicadosEnContratos.GetDate().Value.ToString("dd/MM/yyyy");
            vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado = txtFRealFCTDefinitivaParaMCFRealES_MCRevisado.Text;
            vTiemposReales.FRealComiteContratacion = txtFRealComiteContratacion.GetDate() == null ? string.Empty : txtFRealComiteContratacion.GetDate().Value.ToString("dd/MM/yyyy");
            vTiemposReales.FRealES_EC = txtFRealES_EC.Text;
            vTiemposReales.FRealInicioPS = txtFRealInicioPS.GetDate() == null ? string.Empty : txtFRealInicioPS.GetDate().Value.ToString("dd/MM/yyyy");
            vTiemposReales.FRealPresentacionPropuestas = txtFRealPresentacionPropuestas.GetDate() == null ? string.Empty : txtFRealPresentacionPropuestas.GetDate().Value.ToString("dd/MM/yyyy");

            if (Request.QueryString["oP"] == "E")
            {

                vTiempoEntreActividades.IdTiempoEntreActividades = Convert.ToDecimal(hfIdTiempoEntreActividades.Value);
                vTiempoEntreActividades.UsuarioModifica = GetSessionUser().NombreUsuario;
                //InformacionAudioria(vTiempoEntreActividades, this.PageName, vSolutionPage);
                InformacionAudioria(vTiempoEntreActividades, this.PageName, SolutionPage.Edit);
                vEstudioSectorCostoService.ModificarTiempoEntreActividades(vTiempoEntreActividades);
                vResultado = vTiempoEntreActividades.IdTiempoEntreActividades;

                vTiemposESyC.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiemposESyC.UsuarioModifica = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.ModificarTiempoEntreActividadesEstudioSyC(vTiemposESyC);

                vTiempoPACCO.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiempoPACCO.UsuarioModifica = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.ModificarTiempoEntreActividadesPACCO(vTiempoPACCO);

                vTiemposESC.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiemposESC.UsuarioModifica = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.ModificarTiempoEntreActividadesFechasESC(vTiemposESC);

                vTiemposReales.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiemposReales.UsuarioModifica = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.ModificarTiempoEntreActividadesFechasReales(vTiemposReales);
            }
            else
            {

                vTiempoEntreActividades.UsuarioCrea = GetSessionUser().NombreUsuario;
                //InformacionAudioria(vTiempoEntreActividades, this.PageName, vSolutionPage);
                InformacionAudioria(vTiempoEntreActividades, this.PageName, SolutionPage.Add);
                vResultado = vEstudioSectorCostoService.InsertarTiempoEntreActividades(vTiempoEntreActividades);
                vTiempoEntreActividades.IdTiempoEntreActividades = vResultado;

                vTiemposESyC.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiemposESyC.UsuarioCrea = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.InsertarTiempoEntreActividadesEstudioSyC(vTiemposESyC);

                vTiempoPACCO.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiempoPACCO.UsuarioCrea = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.InsertarTiempoEntreActividadesPACCO(vTiempoPACCO);

                vTiemposESC.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiemposESC.UsuarioCrea = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.InsertarTiempoEntreActividadesFechasESC(vTiemposESC);

                vTiemposReales.IdTiempoEntreActividades = vTiempoEntreActividades.IdTiempoEntreActividades;
                vTiemposReales.UsuarioCrea = GetSessionUser().NombreUsuario;
                vEstudioSectorCostoService.InsertarTiempoEntreActividadesFechasReales(vTiemposReales);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 0)
            {
                SetSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades", vTiempoEntreActividades.IdTiempoEntreActividades);
                SetSessionParameter("TiempoEntreActividadesEstudioSyC.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tiempo entre actividades", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar registros
    /// </summary>
    private void CargarRegistro()
    {
        try
        {

            Decimal vIdTiempoEntreActividades = Convert.ToDecimal(GetSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades"));
            RemoveSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades");

            TiempoEntreActividadesEstudioSyC vTiemposESyC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesEstudioSyC(vIdTiempoEntreActividades);
            TiemposPACCO vTiempoPACCO = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesPACCO(vIdTiempoEntreActividades);
            TiempoEntreActividadesEstudioSyC vTiemposESC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasESC(vIdTiempoEntreActividades);
            TiempoEntreActividadesEstudioSyC vTiemposReales = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasReales(vIdTiempoEntreActividades);

            hfIdTiempoEntreActividades.Value = vIdTiempoEntreActividades.ToString();
            txtIdConsecutivoEstudio.Text = vTiemposESC.IdConsecutivoEstudio.ToString();
            ////Fechas ESyC
            vTiemposESyC.FentregaES_ECTiemposEquipo = vTiemposESyC.FentregaES_ECTiemposEquipo != null ? vTiemposESyC.FentregaES_ECTiemposEquipo.Substring(0, 10) : string.Empty;
            vTiemposESyC.FEentregaES_ECTiemposIndicador = vTiemposESyC.FEentregaES_ECTiemposIndicador != null ? vTiemposESyC.FEentregaES_ECTiemposIndicador.Substring(0, 10) : string.Empty;

            txtFentregaES_ECTiemposEquipo.Text = vTiemposESyC.FentregaES_ECTiemposEquipo;
            txtFEentregaES_ECTiemposIndicador.Text = vTiemposESyC.FEentregaES_ECTiemposIndicador;
            txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Text = vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
            lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Visible = vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR == null ? false : vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR.Value;
            txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.BackColor = GetColor(vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor);
            txtDiasHabilesEntreFCTFinalYSDC.Text = vTiemposESyC.DiasHabilesEntreFCTFinalYSDC;
            lblDiasHabilesEntreFCTFinalYSDC.Visible = vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR == null ? false : vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR.Value;
            txtDiasHabilesEntreFCTFinalYSDC.BackColor = GetColor(vTiemposESyC.DiasHabilesEntreFCTFinalYSDCColor);
            txtDiasHabilesEnESOCOSTEO.Text = vTiemposESyC.DiasHabilesEnESOCOSTEO;
            lblDiasHabilesEnESOCOSTEO.Visible = vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR == null ? false : vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR.Value;
            lblDiasHabilesEnESOCOSTEODevuelto.Visible = vTiemposESyC.DevueltoDiasHabilesEnESOCOSTEOR == null ? false : vTiemposESyC.DevueltoDiasHabilesEnESOCOSTEOR.Value;
            txtDiasHabilesEnESOCOSTEO.BackColor = GetColor(vTiemposESyC.DiasHabilesEnESOCOSTEOColor);
            txtDiashabilesParaDevolucion.Text = vTiemposESyC.DiashabilesParaDevolucion;
            lblDiashabilesParaDevolucion.Visible = vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR == null ? false : vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR.Value;
            txtDiashabilesParaDevolucion.BackColor = GetColor(vTiemposESyC.DiashabilesParaDevolucionColor);

            ////Tiempos PACCO
            vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL != null ? vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaFCTPreliminar = vTiempoPACCO.FEstimadaFCTPreliminar != null ? vTiempoPACCO.FEstimadaFCTPreliminar.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado != null ? vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEestimadaES_EC = vTiempoPACCO.FEestimadaES_EC != null ? vTiempoPACCO.FEestimadaES_EC.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaDocsRadicadosEnContratos = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos != null ? vTiempoPACCO.FEstimadaDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaComiteContratacion = vTiempoPACCO.FEstimadaComiteContratacion != null ? vTiempoPACCO.FEstimadaComiteContratacion.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaInicioDelPS = vTiempoPACCO.FEstimadaInicioDelPS != null ? vTiempoPACCO.FEstimadaInicioDelPS.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaPresentacionPropuestas = vTiempoPACCO.FEstimadaPresentacionPropuestas != null ? vTiempoPACCO.FEstimadaPresentacionPropuestas.Substring(0, 10) : string.Empty;
            vTiempoPACCO.FEstimadaDeInicioDeEjecucion = vTiempoPACCO.FEstimadaDeInicioDeEjecucion != null ? vTiempoPACCO.FEstimadaDeInicioDeEjecucion.Substring(0, 10) : string.Empty;

            txtFEstimadaFCTPreliminarREGINICIAL.Text = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL;
            lblFEstimadaFCTPreliminarREGINICIAL.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL.Value; ;
            txtFEstimadaFCTPreliminar.Text = vTiempoPACCO.FEstimadaFCTPreliminar;
            lblFEstimadaFCTPreliminar.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar.Value;
            txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Text = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
            lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Value;
            txtFEestimadaES_EC.Text = vTiempoPACCO.FEestimadaES_EC;
            lblFEestimadaES_EC.Visible = vTiempoPACCO.NoAplicaFEestimadaES_EC == null ? false : vTiempoPACCO.NoAplicaFEestimadaES_EC.Value;
            txtFEstimadaDocsRadicadosEnContratos.Text = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos;
            lblFEstimadaDocsRadicadosEnContratos.Visible = vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos == null ? false : vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos.Value;
            txtFEstimadaComiteContratacion.Text = vTiempoPACCO.FEstimadaComiteContratacion;
            lblFEstimadaComiteContratacion.Visible = vTiempoPACCO.NoAplicaFEstimadaComiteContratacion == null ? false : vTiempoPACCO.NoAplicaFEstimadaComiteContratacion.Value; ;
            txtFEstimadaInicioDelPS.Text = vTiempoPACCO.FEstimadaInicioDelPS;
            lblFEstimadaInicioDelPS.Visible = vTiempoPACCO.NoAplicaFEstimadaInicioDelPS == null ? false : vTiempoPACCO.NoAplicaFEstimadaInicioDelPS.Value;
            txtFEstimadaPresentacionPropuestas.Text = vTiempoPACCO.FEstimadaPresentacionPropuestas;
            lblFEstimadaPresentacionPropuestas.Visible = vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas == null ? false : vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas.Value; ;
            txtFEstimadaDeInicioDeEjecucion.Text = vTiempoPACCO.FEstimadaDeInicioDeEjecucion;

            ////Fechas ESC

            vTiemposESC.FRealFCTPreliminarESC = vTiemposESC.FRealFCTPreliminarESC != null ? vTiemposESC.FRealFCTPreliminarESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC != null ? vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaES_EC_ESC = vTiemposESC.FEstimadaES_EC_ESC != null ? vTiemposESC.FEstimadaES_EC_ESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaDocsRadicadosEnContratosESC = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC != null ? vTiemposESC.FEstimadaDocsRadicadosEnContratosESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaComitecontratacionESC = vTiemposESC.FEstimadaComitecontratacionESC != null ? vTiemposESC.FEstimadaComitecontratacionESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaInicioDelPS_ESC = vTiemposESC.FEstimadaInicioDelPS_ESC != null ? vTiemposESC.FEstimadaInicioDelPS_ESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaPresentacionPropuestasESC = vTiemposESC.FEstimadaPresentacionPropuestasESC != null ? vTiemposESC.FEstimadaPresentacionPropuestasESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaDeInicioDeEjecucionESC = vTiemposESC.FEstimadaDeInicioDeEjecucionESC != null ? vTiemposESC.FEstimadaDeInicioDeEjecucionESC.Substring(0, 10) : string.Empty;

            txtFRealFCTPreliminarESC.Text = vTiemposESC.FRealFCTPreliminarESC;
            lblFRealFCTPreliminarESC.Visible = vTiemposESC.NoAplicaFRealFCTPreliminarESC == null ? false : vTiemposESC.NoAplicaFRealFCTPreliminarESC.Value;
            txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Text = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
            lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Visible = vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC == null ? false : vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Value;
            txtFEstimadaES_EC_ESC.Text = vTiemposESC.FEstimadaES_EC_ESC;
            lblFEstimadaES_EC_ESC.Visible = vTiemposESC.NoAplicaFEstimadaES_EC_ESC == null ? false : vTiemposESC.NoAplicaFEstimadaES_EC_ESC.Value;
            txtFEstimadaDocsRadicadosEnContratosESC.Text = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC;
            lblFEstimadaDocsRadicadosEnContratosESC.Visible = vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC == null ? false : vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC.Value;
            txtFEstimadaComitecontratacionESC.Text = vTiemposESC.FEstimadaComitecontratacionESC;
            lblFEstimadaComitecontratacionESC.Visible = vTiemposESC.NoAplicaFEstimadaComitecontratacionESC == null ? false : vTiemposESC.NoAplicaFEstimadaComitecontratacionESC.Value;
            txtFEstimadaInicioDelPS_ESC.Text = vTiemposESC.FEstimadaInicioDelPS_ESC;
            lblFEstimadaInicioDelPS_ESC.Visible = vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC == null ? false : vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC.Value;
            txtFEstimadaPresentacionPropuestasESC.Text = vTiemposESC.FEstimadaPresentacionPropuestasESC;
            lblFEstimadaPresentacionPropuestasESC.Visible = vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC == null ? false : vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC.Value;
            txtFEstimadaDeInicioDeEjecucionESC.Text = vTiemposESC.FEstimadaDeInicioDeEjecucionESC;

            ////Tiempos reales
            vTiemposReales.FRealFCTPreliminar = vTiemposReales.FRealFCTPreliminar != null ? vTiemposReales.FRealFCTPreliminar.Substring(0, 10) : string.Empty;
            vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado != null ? vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Substring(0, 10) : string.Empty;
            vTiemposReales.FRealES_EC = vTiemposReales.FRealES_EC != null ? vTiemposReales.FRealES_EC.Substring(0, 10) : string.Empty;


            txtFRealFCTPreliminar.Text = vTiemposReales.FRealFCTPreliminar;
            txtFRealDocsRadicadosEnContratos.SetDate(vTiemposReales.FRealDocsRadicadosEnContratos == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealDocsRadicadosEnContratos));
            txtFRealFCTDefinitivaParaMCFRealES_MCRevisado.Text = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
            txtFRealComiteContratacion.SetDate(vTiemposReales.FRealComiteContratacion == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealComiteContratacion));
            txtFRealES_EC.Text = vTiemposReales.FRealES_EC;
            txtFRealInicioPS.SetDate(vTiemposReales.FRealInicioPS == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealInicioPS));
            txtFRealPresentacionPropuestas.SetDate(vTiemposReales.FRealPresentacionPropuestas == null ? (DateTime?)null : Convert.ToDateTime(vTiemposReales.FRealPresentacionPropuestas));

            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTiempoEntreActividadesEstudioSyC.UsuarioCrea, vTiempoEntreActividadesEstudioSyC.FechaCrea, vTiempoEntreActividadesEstudioSyC.UsuarioModifica, vTiempoEntreActividadesEstudioSyC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para retornar colores
    /// </summary>
    /// <param name="pColor"></param>
    /// <returns></returns>
    public System.Drawing.Color GetColor(string pColor)
    {

        System.Drawing.Color vColor = System.Drawing.ColorTranslator.FromHtml(pColor);
        if (pColor.Equals("0"))
        {
            return System.Drawing.Color.Empty;
        }
        return vColor;
    }

    /// <summary>
    /// Método para validar si aplica PACCO
    /// </summary>
    /// <param name="pIdConsecutivoEstudio">Id consecutivo estudio</param>
    /// <returns>Retorna true si aplica PACCO</returns>
    protected bool AplicaPACCO(int pIdConsecutivoEstudio)
    {

        RegistroSolicitudEstudioSectoryCaso EstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
        EstudioSectoryCaso = vEstudioService.ConsultarRegistroSolicitudEstudioSectoryCaso(pIdConsecutivoEstudio);
        return EstudioSectoryCaso.AplicaPACCO == 0;

    }

    /// <summary>
    /// Método para reiniciar los controles
    /// </summary>
    protected void ReiniciarControles()
    {
        Utilidades.SetControls(pnlechasSistemaESC, false);
        Utilidades.SetControls(pnlFechasReales, false);
        Utilidades.SetControls(pnlPACCO, false);
        Utilidades.SetControls(pnlTiemposFechasESyC, false);
        OcultarEtiquetas(pnlContenedor);
    }

    /// <summary>
    /// Método para ocultar las etiquetas
    /// </summary>
    /// <param name="pPanel">Panel contenedor</param>
    protected void OcultarEtiquetas(Panel pPanel)
    {
        foreach (Control vControl in pPanel.Controls)
        {
            if (vControl is Label)
            {
                ((Label)vControl).Visible = false;
            }
            if (vControl is Panel)
            {
                OcultarEtiquetas(((Panel)vControl));
            }
            if (vControl is TextBox)
            {
                ((TextBox)vControl).BackColor = System.Drawing.Color.Empty;
            }
        }
    }


    /// <summary>
    /// Método para cargar tiempos y fechas activiades
    /// </summary>
    /// <param name="pIdconsecutivoEstudio">Id consecutivo estudio</param>
    protected void CargarTiemposYFechas(decimal pIdconsecutivoEstudio)
    {
        try
        {
            ReiniciarControles();
            RegistroSolicitudEstudioSectoryCaso EstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
            EstudioSectoryCaso = vEstudioService.ConsultarRegistroSolicitudEstudioSectoryCaso(Convert.ToInt32(pIdconsecutivoEstudio));

            ////Fechas PACCO
            if (EstudioSectoryCaso.AplicaPACCO == 0)
            {
                WsContratosPacco.WSContratosPACCOSoapClient ws = new WsContratosPacco.WSContratosPACCOSoapClient();
                //WsContratosPacco.GetObjetosContractualesServicioXModalidad_Result[] res = ws.GetListaConsecutivosEncabezadosPACCOXModalidad(int.Parse(EstudioSectoryCaso.VigenciaPACCO), null, 0, 0, EstudioSectoryCaso.ConsecutivoPACCO, null, 0, 0, null, 0);
                WsContratosPacco.GetObjetosContractualesServicioXModalidad_Result[] res = ws.GetListaConsecutivosEncabezadosPACCOXModalidad(Convert.ToInt32(EstudioSectoryCaso.VigenciaPACCO), null, 0, 0, EstudioSectoryCaso.ConsecutivoPACCO, null, 0, 0, null, 0);
                if (res != null && res.ToList().Count > 0)
                {

                    string pFechaEstimadaInicioEjecucion = res[0].FechaInicioEjecucion.Value.ToString("dd/MM/yyyy");
                    string pIdModalidadSeleccionPACCO = res[0].Id_modalidad == null ? "0" : res[0].Id_modalidad.Value.ToString().Trim();
                    string pModalidadSeleccionPACCO = res[0].modalidad.Trim();
                    decimal pValorContrato = res[0].valor_contrato == null ? 0 : res[0].valor_contrato.Value;

                    var vRes = new Icbf.SIA.Integrations.ClientObjectModel.EstudioSectoresCOM().ConsultaFechasPACCO<TiemposPACCO>(pIdconsecutivoEstudio.ToString(), pFechaEstimadaInicioEjecucion, pIdModalidadSeleccionPACCO, pModalidadSeleccionPACCO, pValorContrato);
                    if (vRes != null)
                    {
                        SetValues(txtFEstimadaFCTPreliminarREGINICIAL, lblFEstimadaFCTPreliminarREGINICIAL, vRes.FEstimadaFCTPreliminarREGINICIAL);
                        SetValues(txtFEstimadaFCTPreliminar, lblFEstimadaFCTPreliminar, vRes.FEstimadaFCTPreliminar);
                        SetValues(txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado, lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado, vRes.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado);
                        SetValues(txtFEestimadaES_EC, lblFEestimadaES_EC, vRes.FEestimadaES_EC);
                        SetValues(txtFEstimadaDocsRadicadosEnContratos, lblFEstimadaDocsRadicadosEnContratos, vRes.FEstimadaDocsRadicadosEnContratos);
                        SetValues(txtFEstimadaComiteContratacion, lblFEstimadaComiteContratacion, vRes.FEstimadaComiteContratacion);
                        SetValues(txtFEstimadaInicioDelPS, lblFEstimadaInicioDelPS, vRes.FEstimadaInicioDelPS);
                        SetValues(txtFEstimadaPresentacionPropuestas, lblFEstimadaPresentacionPropuestas, vRes.FEstimadaPresentacionPropuestas);
                    }

                    txtFEstimadaDeInicioDeEjecucion.Text = pFechaEstimadaInicioEjecucion;

                }


            }
            else
            {
                txtFEstimadaFCTPreliminarREGINICIAL.Text = string.Empty;
                txtFEstimadaFCTPreliminar.Text = string.Empty;
                txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Text = string.Empty;
                txtFEestimadaES_EC.Text = string.Empty;
                txtFEstimadaDocsRadicadosEnContratos.Text = string.Empty;
                txtFEstimadaComiteContratacion.Text = string.Empty;
                txtFEstimadaInicioDelPS.Text = string.Empty;
                txtFEstimadaPresentacionPropuestas.Text = string.Empty;
                txtFEstimadaDeInicioDeEjecucion.Text = string.Empty;

            }

            if (!ValidarParametros(pIdconsecutivoEstudio))
            {
                return;
            }
            TiempoEntreActividadesEstudioSyC vTiemposESyC = vEstudioSectorCostoService.ConsultarTiemposESC(pIdconsecutivoEstudio, lblFEstimadaFCTPreliminar.Visible ? "NO APLICA" : txtFEstimadaFCTPreliminar.Text);
            if (vTiemposESyC == null)
            {
                return;
            }
            SetValues(txtFentregaES_ECTiemposEquipo, lblFentregaES_ECTiemposEquipo, vTiemposESyC.FentregaES_ECTiemposEquipo);
            SetValues(txtFentregaES_ECTiemposEquipo, lblFentregaES_ECTiemposEquipo, "NO APLICA");
            SetValues(txtFEentregaES_ECTiemposIndicador, lblFEentregaES_ECTiemposIndicador, vTiemposESyC.FEentregaES_ECTiemposIndicador);
            SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor);
            SetValues(txtDiasHabilesEntreFCTFinalYSDC, lblDiasHabilesEntreFCTFinalYSDC, vTiemposESyC.DiasHabilesEntreFCTFinalYSDC, vTiemposESyC.DiasHabilesEntreFCTFinalYSDCColor);
            SetValues(txtDiasHabilesEnESOCOSTEO, lblDiasHabilesEnESOCOSTEO, vTiemposESyC.DiasHabilesEnESOCOSTEO, vTiemposESyC.DiasHabilesEnESOCOSTEOColor);
            SetValues(txtDiashabilesParaDevolucion, lblDiashabilesParaDevolucion, vTiemposESyC.DiashabilesParaDevolucion, vTiemposESyC.DiashabilesParaDevolucionColor);



            SetValues(txtFRealFCTPreliminarESC, lblFRealFCTPreliminarESC, vTiemposESyC.FEstimadaFCTPreliminarESC);
            SetValues(txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC, lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC, vTiemposESyC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC);
            SetValues(txtFEstimadaES_EC_ESC, lblFEstimadaES_EC_ESC, vTiemposESyC.FEstimadaES_EC_ESC);
            SetValues(txtFEstimadaDocsRadicadosEnContratosESC, lblFEstimadaDocsRadicadosEnContratosESC, vTiemposESyC.FEstimadaDocsRadicadosEnContratosESC);
            SetValues(txtFEstimadaComitecontratacionESC, lblFEstimadaComitecontratacionESC, vTiemposESyC.FEstimadaComitecontratacionESC);
            SetValues(txtFEstimadaInicioDelPS_ESC, lblFEstimadaInicioDelPS_ESC, vTiemposESyC.FEstimadaInicioDelPS_ESC);
            SetValues(txtFEstimadaPresentacionPropuestasESC, lblFEstimadaPresentacionPropuestasESC, vTiemposESyC.FEstimadaPresentacionPropuestasESC);
            SetValues(txtFEstimadaDeInicioDeEjecucionESC, lblFEstimadaDeInicioDeEjecucionESC, vTiemposESyC.FEstimadaDeInicioDeEjecucionESC);

            SetValues(txtFRealFCTPreliminar, lblFRealFCTPreliminar, vTiemposESyC.FRealFCTPreliminar);
            SetValues(txtFRealFCTDefinitivaParaMCFRealES_MCRevisado, lblFRealFCTDefinitivaParaMCFRealES_MCRevisado, vTiemposESyC.FRealFCTDefinitivaParaMCFRealES_MCRevisado);
            SetValues(txtFRealES_EC, lblFRealES_EC, vTiemposESyC.FRealES_EC);

            //INICIO CC.2018.INGENIAN.SIA.ESC.026_V11

            //Campo F. entrega ES-EC tiempos equipo
            GestionesEstudioSectorCostos vGestion = vResultadoEstudioSectorService.ConsultarGestionDefinitivas(EstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso);
            GestionesEstudioSectorCostos vGestionEstudio = vResultadoEstudioSectorService.ConsultarGestionEstudioSector(vGestion.IdGestionEstudio);
            List<ResultadoEstudioSectorConsulta> lstResultadoEstudioSector = new List<ResultadoEstudioSectorConsulta>();
            lstResultadoEstudioSector = vResultadoEstudioSectorService.ConsultarResultadoEstudioSectores(EstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso, string.Empty, string.Empty, null, null, null, null, null);

            if (EstudioSectoryCaso.NumeroReproceso.Contains("E"))
            {
                if (EstudioSectoryCaso.ModalidadSeleccion.ToUpper() != "MINIMA CUANTIA")
                {
                    if (vGestion != null)
                    {

                        List<RangoDiasComplejidadPorModalidad> lstDiasPorModalidad = new List<RangoDiasComplejidadPorModalidad>();
                        lstDiasPorModalidad = vEstudioService.ConsultarRangoDiasComplejidadPorModalidads
                            (EstudioSectoryCaso.IdModalidadSeleccion, null, null, EstudioSectoryCaso.IdComplejidadInterna, EstudioSectoryCaso.IdComplejidadIndicador,
                            null, null, null);

                        if (lstDiasPorModalidad != null)
                        {

                            RangoDiasComplejidadPorModalidad vDiasInternos = lstDiasPorModalidad.Find(
                                rangosDias => rangosDias.IdModalidadSeleccion.Equals(EstudioSectoryCaso.IdModalidadSeleccion));


                            if (vGestion.FechaEntregaFCTORequerimientoDef == null)
                            {
                                if (vDiasInternos != null)
                                {
                                    DateTime valor = EstudioSectoryCaso.FechaSolicitudInicial.AddDays(Convert.ToDouble(vDiasInternos.DiasHabilesInternos));
                                    SetValues(txtFentregaES_ECTiemposEquipo, null, valor.ToShortDateString());
                                }
                            }

                            else
                            {
                                if (vDiasInternos != null)
                                {
                                    DateTime valor = Convert.ToDateTime(vGestion.FechaEntregaFCTORequerimientoDef).AddDays(Convert.ToDouble(vDiasInternos.DiasHabilesInternos));
                                    SetValues(txtFentregaES_ECTiemposEquipo, null, valor.ToShortDateString());
                                }

                            }
                        }
                    }
                }
            }

            //Campo Días hábiles entre DT iniciales y finales / para ES-EC entre Solicitud inicial y cambio en los DT aprobados
            if (EstudioSectoryCaso.NumeroReproceso.Contains("D"))
            {

                if (lstResultadoEstudioSector != null)
                {

                    ResultadoEstudioSectorConsulta vResultadoEstudioSector = lstResultadoEstudioSector.Find
                        (fecha => fecha.IdConsecutivoEstudio.Equals(EstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso));

                    if (vResultadoEstudioSector != null)
                    {
                        if (vResultadoEstudioSector.FechaDeEntregaESyC != null)
                        {
                            TimeSpan diferencia = Convert.ToDateTime(vResultadoEstudioSector.FechaDeEntregaESyC).Subtract(EstudioSectoryCaso.FechaSolicitudInicial);
                            DateTime valor = Convert.ToDateTime(vResultadoEstudioSector.FechaDeEntregaESyC).Subtract(diferencia);
                            //double valor = diferencia.TotalDays;
                            SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, null, valor.ToShortDateString());
                        }
                        else
                        {
                            TimeSpan diferencia = Convert.ToDateTime(DateTime.Now).Subtract(EstudioSectoryCaso.FechaSolicitudInicial);
                            DateTime valor = Convert.ToDateTime(DateTime.Now).Subtract(diferencia);
                            //double valor = diferencia.TotalDays;
                            SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, null, valor.ToShortDateString(), "AMARILLO");
                        }

                    }
                    else
                    {
                        TimeSpan diferencia = Convert.ToDateTime(DateTime.Now).Subtract(EstudioSectoryCaso.FechaSolicitudInicial);
                        DateTime valor = Convert.ToDateTime(DateTime.Now).Subtract(diferencia);
                        //double valor = diferencia.TotalDays;
                        SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, null, valor.ToShortDateString(), "AMARILLO");
                    }


                }
            }
            else if (EstudioSectoryCaso.NumeroReproceso.Contains("E"))
            {
                if (vGestionEstudio != null)
                {
                    List<RangoDiasComplejidadPorModalidad> lstDiasPorModalidad = new List<RangoDiasComplejidadPorModalidad>();
                    lstDiasPorModalidad = vEstudioService.ConsultarRangoDiasComplejidadPorModalidads
                        (EstudioSectoryCaso.IdModalidadSeleccion, null, null, EstudioSectoryCaso.IdComplejidadInterna, EstudioSectoryCaso.IdComplejidadIndicador,
                        null, null, null);

                    if (lstDiasPorModalidad != null)
                    {

                        RangoDiasComplejidadPorModalidad vDiasInternos = lstDiasPorModalidad.Find(
                            rangosDias => rangosDias.IdModalidadSeleccion.Equals(EstudioSectoryCaso.IdModalidadSeleccion));

                        if (vGestionEstudio.FechaEntregaFCTORequerimientoDef != null)
                        {
                            if (vDiasInternos != null)
                            {

                                TimeSpan diferencia = Convert.ToDateTime(vGestionEstudio.FechaEntregaFCTORequerimientoDef).Subtract(EstudioSectoryCaso.FechaSolicitudInicial);
                                DateTime valor = Convert.ToDateTime(vGestionEstudio.FechaEntregaFCTORequerimientoDef).Subtract(diferencia);
                                //double valor = diferencia.TotalDays;
                                SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, null, valor.ToShortDateString());

                            }
                            else
                            {
                                SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, "NO APLICA");
                            }
                        }
                        else
                        {
                            SetValues(txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado, "NO APLICA");
                        }
                    }
                }
            }

            //Campo Días hábiles entre Solicitud de ES y SDC
            if (EstudioSectoryCaso.NumeroReproceso.Contains("D") || vGestionEstudio.NoAplicaFechaEnvioSDC)
            {
                SetValues(txtDiasHabilesEntreFCTFinalYSDC, lblDiasHabilesEntreFCTFinalYSDC, "NO APLICA");
            }
            else if (EstudioSectoryCaso.NumeroReproceso.Contains("E") && EstudioSectoryCaso.ModalidadSeleccion.ToUpper() == "MINIMA CUANTIA")
            {
                SetValues(txtDiasHabilesEntreFCTFinalYSDC, lblDiasHabilesEntreFCTFinalYSDC, "NO APLICA");
            }
            else if (EstudioSectoryCaso.NumeroReproceso.Contains("E") && EstudioSectoryCaso.ModalidadSeleccion.ToUpper() != "MINIMA CUANTIA")
            {
                if (vGestionEstudio != null)
                {
                    if (vGestionEstudio.FechaEntregaFCTORequerimientoDef == null)
                    {
                        if (vGestionEstudio.FechaEnvioSDCDef != null)
                        {
                            TimeSpan diferencia = Convert.ToDateTime(vGestionEstudio.FechaEnvioSDCDef).Subtract(EstudioSectoryCaso.FechaSolicitudInicial);
                            DateTime valor = Convert.ToDateTime(vGestionEstudio.FechaEnvioSDCDef).Subtract(diferencia);
                            //double valor = diferencia.TotalDays;
                            SetValues(txtDiasHabilesEntreFCTFinalYSDC, null, valor.ToShortDateString());
                        }
                        else
                        {
                            TimeSpan diferencia = Convert.ToDateTime(DateTime.Now).Subtract(EstudioSectoryCaso.FechaSolicitudInicial);
                            DateTime valor = Convert.ToDateTime(DateTime.Now).Subtract(diferencia);
                            //double valor = diferencia.TotalDays;
                            SetValues(txtDiasHabilesEntreFCTFinalYSDC, null, valor.ToShortDateString());
                        }
                    }
                    else
                    {
                        if (vGestionEstudio.FechaEnvioSDCDef != null)
                        {
                            TimeSpan diferencia = Convert.ToDateTime(vGestionEstudio.FechaEnvioSDCDef).Subtract(Convert.ToDateTime(vGestionEstudio.FechaEntregaFCTORequerimientoDef));
                            DateTime valor = Convert.ToDateTime(vGestionEstudio.FechaEnvioSDCDef).Subtract(diferencia);
                            //double valor = diferencia.TotalDays;
                            SetValues(txtDiasHabilesEntreFCTFinalYSDC, null, valor.ToShortDateString());
                        }
                        else
                        {
                            TimeSpan diferencia = Convert.ToDateTime(DateTime.Now).Subtract(Convert.ToDateTime(vGestionEstudio.FechaEntregaFCTORequerimientoDef));
                            DateTime valor = Convert.ToDateTime(DateTime.Now).Subtract(diferencia);
                            //double valor = diferencia.TotalDays;
                            SetValues(txtDiasHabilesEntreFCTFinalYSDC, null, valor.ToShortDateString());
                        }
                    }
                }
            }

            //Campo Días hábiles en ES o COSTEO
            //txtDiasHabilesEnESOCOSTEO
            if (EstudioSectoryCaso.NumeroReproceso.Contains("D"))
            {

                if (EstudioSectoryCaso.ModalidadSeleccion.ToUpper() == "MINIMA CUANTIA" && (vGestionEstudio.NoAplicaFechaEntregaES_MC))
                {
                    SetValues(txtDiasHabilesEnESOCOSTEO, lblDiasHabilesEnESOCOSTEO, "NO APLICA");
                }
                else
                {
                    SetValues(txtDiasHabilesEnESOCOSTEO, lblDiasHabilesEnESOCOSTEO, "NO APLICA");
                }

            }
            else if (EstudioSectoryCaso.NumeroReproceso.Contains("E") && (EstudioSectoryCaso.EstadoSolicitud.ToUpper() == "DESCARTADO")
                                                                          || EstudioSectoryCaso.EstadoSolicitud.ToUpper() == "DEVUELTO")
            {
                SetValues(txtDiasHabilesEnESOCOSTEO, lblDiasHabilesEnESOCOSTEODevuelto, EstudioSectoryCaso.EstadoSolicitud.ToUpper());
            }
            else
            {
                if (EstudioSectoryCaso.ModalidadSeleccion.ToUpper() == "MINIMA CUANTIA" && (vGestionEstudio.NoAplicaFechaEntregaES_MC))
                {
                    SetValues(txtDiasHabilesEnESOCOSTEO, lblDiasHabilesEnESOCOSTEO, "NO APLICA");
                }
            }
            //FIN CC.2018.INGENIAN.SIA.ESC.026_V11
        }

        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    /// Método para validar condiciones para realizar los cálculos
    /// </summary>
    /// <param name="pIdconsecutivoEstudio">Id consecutivo estudio</param>
    /// <returns>retorna true si las consdiciones son correctas</returns>
    protected bool ValidarParametros(decimal pIdconsecutivoEstudio)
    {
        toolBar.LipiarMensajeError();
        bool esValido = false;
        string vRes = vEstudioSectorCostoService.ValidarCalculoTiempos(pIdconsecutivoEstudio);
        if (vRes.Equals("OK"))
        {
            esValido = true;
        }
        else
        {
            toolBar.MostrarMensajeError(vRes);
        }
        return esValido;
    }

    /// <summary>
    /// Método para establecer valores a los controles
    /// </summary>
    /// <param name="pTxtFecha">Caja de texto</param>
    /// <param name="pLblNoAplica">Etiqueta no aplica</param>
    /// <param name="pValor">Valor del campo</param>
    /// <param name="pColor">color del campo</param>
    protected void SetValues(TextBox pTxtFecha, Label pLblNoAplica, string pValor, string pColor = "NA")
    {

        if (pValor.Equals("NO APLICA"))
        {
            pTxtFecha.Text = string.Empty;
            pLblNoAplica.Visible = true;
        }

        if (pValor.Contains("D"))
        {
            pTxtFecha.Text = string.Empty;
            pLblNoAplica.Visible = true;
        }

        if (pValor.Equals("DEVUELTO") || pValor.Equals("DESCARTADO"))
        {
            pTxtFecha.Text = string.Empty;
            lblDiasHabilesEnESOCOSTEODevuelto.Visible = true;
            lblDiasHabilesEnESOCOSTEODevuelto.Text = pValor;
            //pLblNoAplica.Text = "Devuelto";
        }

        if (!pValor.Equals(string.Empty) && pValor != null && !pValor.Equals("NO APLICA") && !pValor.Equals("DEVUELTO"))
        {
            pTxtFecha.Text = pValor;
        }


        if (pColor.Equals("AMARILLO"))
        {
            pTxtFecha.BackColor = System.Drawing.Color.Yellow;
        }
        if (pColor.Equals("ROSADO"))
        {
            pTxtFecha.BackColor = System.Drawing.Color.Pink;
        }
    }

    /// <summary>
    /// Método para validar que las fechas no sean festivos
    /// </summary>
    /// <returns>Retorna true si no existen fechas festivo selecciondas</returns>
    public bool ValidarFestivos()
    {
        bool vEsValido = true;
        bool vValida1 = txtFRealDocsRadicadosEnContratos.EsDiaFestivo();
        bool vValida2 = txtFRealComiteContratacion.EsDiaFestivo();
        bool vValida3 = txtFRealInicioPS.EsDiaFestivo();
        bool vValida4 = txtFRealPresentacionPropuestas.EsDiaFestivo();

        if (vValida1 || vValida2 || vValida3 || vValida4)
        {
            vEsValido = false;
        }
        return vEsValido;

    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Método para guardar registro
    /// </summary>
    /// <param name="sender">Boton btnGuardar</param>
    /// <param name="e">Evento click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Método para agregar un registro
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método para buscar registro
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento al seleccionar consecutivo estudio
    /// </summary>
    /// <param name="sender">caja de texto txtIdConsecutivoEstudio</param>
    /// <param name="e">Evento change</param>
    protected void txtIdConsecutivoEstudio_TextChanged(object sender, EventArgs e)
    {
        hdnIdConsecutivo2.Value = txtIdConsecutivoEstudio.Text;
        CargarTiemposYFechas(decimal.Parse(txtIdConsecutivoEstudio.Text));
    }
    #endregion


    protected void hdnIdConsecutivo_ValueChanged(object sender, EventArgs e)
    {
        CargarTiemposYFechas(decimal.Parse(txtIdConsecutivoEstudio.Text));
        hdnIdConsecutivo2.Value = txtIdConsecutivoEstudio.Text;
    }
}
