﻿//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_ConsultaDependencias_List</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Service;
using Icbf.EstudioSectorCosto.Entity;
using System.Linq.Expressions;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data.Odbc;

/// <summary>
/// Clase para consultar dependencias
/// </summary>
public partial class Page_ConsultaDependencias_List : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ConsultaDependencias";

    /// <summary>
    /// Instancia a capa de servicio EstudioSectorCostoService
    /// </summary>
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Método para buscar registro
    /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            string vIdDireccionSolicitante = null;
            ListItemCollection items = lstbDireccionSolicitante.items();
            foreach (ListItem item in items)
            {
                if(item.Selected)
                {
                    vIdDireccionSolicitante += item.Value + ",";
                }                
            }

            if (vIdDireccionSolicitante != null)
            {
                vIdDireccionSolicitante = vIdDireccionSolicitante.Remove(vIdDireccionSolicitante.Length - 1, 1);
            }
            int? vVigencia = ddlVigencia.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlVigencia.SelectedValue);

            List<ConsultaDependencias> vLstCosulta = vEstudioSectorCostoService.ConsultarConsultaDependenciass(vIdDireccionSolicitante, vVigencia);
            ViewState[Constantes.ViewStateConsultaDependencias] = vLstCosulta;
            LlenarGrilla(vLstCosulta);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para llenar la grilla de registros
    /// </summary>
    /// <param name="pLstDireccionesSolicitantes">Lista de registros a llenar en la grilla</param>
    protected void LlenarGrilla(List<ConsultaDependencias> pLstDireccionesSolicitantes)
    {
        if (pLstDireccionesSolicitantes == null || pLstDireccionesSolicitantes.Count == 0)
        {
            gvConsultaDependencias.DataSource = null;
            gvConsultaDependencias.DataBind();
            return;
        }
        toolBar.LipiarMensajeError();
        string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
        if (pSortExpresion.Equals(string.Empty))
        {
            OrdenarGrilla(Constantes.SinOrden, pLstDireccionesSolicitantes);
        }
        else
        {
            OrdenarGrilla(pSortExpresion, pLstDireccionesSolicitantes);
        }

    }

    /// <summary>
    /// Método para ordenar la grilla
    /// </summary>
    /// /// <param name="pSortExpresion">La forma de ordenamiento de la grilla</param>
    /// <param name="vLstConsulta">Lista de registros a ordenar en la grilla</param>
    protected void OrdenarGrilla(string pSortExpresion, List<ConsultaDependencias> vLstConsulta)
    {
        if (vLstConsulta != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvConsultaDependencias.DataSource = vLstConsulta.OrderByDescending(x => x.Vigencia).ToList(); ;
                gvConsultaDependencias.DataBind();
                return;

            }

            var param = Expression.Parameter(typeof(ConsultaDependencias), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<ConsultaDependencias, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvConsultaDependencias.DataSource = vLstConsulta.AsQueryable<ConsultaDependencias>().OrderBy(sortExpression).ToList<ConsultaDependencias>();
            }
            else
            {
                gvConsultaDependencias.DataSource = vLstConsulta.AsQueryable<ConsultaDependencias>().OrderByDescending(sortExpression).ToList<ConsultaDependencias>();
            };

            gvConsultaDependencias.DataBind();
        }
    }

    /// <summary>
    /// Método para configurar página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvConsultaDependencias.PageSize = PageSize();
            gvConsultaDependencias.EmptyDataText = EmptyDataText();
            gvConsultaDependencias.Columns[0].Visible = false;

            toolBar.EstablecerTitulos("Registro consulta por Dependencias", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvConsultaDependencias.DataKeys[rowIndex].Value.ToString();
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para exportar registro
    /// </summary>
    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvConsultaDependencias.AllowPaging;
        Boolean vPaginadorError = gvConsultaDependencias.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvConsultaDependencias.AllowPaging = false;

        List<ConsultaDependencias> vListaDependencias = ViewState[Constantes.ViewStateConsultaDependencias] as List<ConsultaDependencias>;

        datos = Utilidades.GenerarDataTable(this.gvConsultaDependencias, vListaDependencias);

        GridView datosexportar = new GridView();
        datosexportar.DataSource = datos;
        datosexportar.DataBind();
        vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ConsultarDependencias_" + DateTime.Now.ToString("ddMMyyyy"));
        gvConsultaDependencias.AllowPaging = vPaginador;
    }

    /// <summary>
    /// Método para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ConsultaDependencias.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ConsultaDependencias.Eliminado");
            var direcciones = vResultadoEstudioSectorService.ConsultarDireccion();         
            lstbDireccionSolicitante.AddItems(direcciones);
            lstbDireccionSolicitante.DataBind();
            lstbDireccionSolicitante.SetFocus();


            Utilidades.LlenarDropDownList(ddlVigencia, vResultadoEstudioSectorService.ConsultarVigencia());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento click botón del buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Evento click del botón exportar
    /// </summary>
    /// <param name="sender">Boton btnExportar</param>
    /// <param name="e">Evento click</param>
    private void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvConsultaDependencias.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            gvConsultaDependencias.DataSource = null;
            gvConsultaDependencias.DataBind();
        }
    }

    /// <summary>
    /// Evento al seleccionar registro 
    /// </summary>
    /// <param name="sender">Grilla gvConsultaDependencias</param>
    /// <param name="e">Evento click</param>
    protected void gvConsultaDependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConsultaDependencias.SelectedRow);
    }

    /// <summary>
    /// Evento de paginación 
    /// </summary>
    /// <param name="sender">Grilla gvConsultaDependencias</param>
    /// <param name="e">Evento click</param>
    protected void gvConsultaDependencias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConsultaDependencias.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Evento de ordenación 
    /// </summary>
    /// <param name="sender">Grilla gvConsultaDependencias</param>
    /// <param name="e">Evento click</param>
    protected void gvConsultaDependencias_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState[Constantes.ViewStateSortDirection] != null && GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
        }

        OrdenarGrilla(e.SortExpression, ViewState[Constantes.ViewStateConsultaDependencias] as List<ConsultaDependencias>);
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;
        
    }
    #endregion

    /// <summary>
    /// Atributo que describe la dirección del ordenamiento
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState[Constantes.ViewStateSortDirection] == null)
            {
                ViewState[Constantes.ViewStateSortDirection] = SortDirection.Ascending;
            }


            return (SortDirection)ViewState[Constantes.ViewStateSortDirection];
        }
        set { ViewState[Constantes.ViewStateSortDirection] = value; }
    }
}