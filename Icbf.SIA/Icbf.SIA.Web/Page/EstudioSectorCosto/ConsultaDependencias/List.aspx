﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConsultaDependencias_List" %>
<%@ Register TagPrefix="uccCheckBoxList" TagName="listaCheck" Src="~/General/General/EstudioSectores/CheckBoxList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <script type="text/javascript" src="../../../Scripts/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../../../Scripts/jquery-1.10.2.js"></script>
    
</script>

    <asp:Panel runat="server" ID="pnlConsulta">
        <asp:Label ID="lblMsgError" runat="server" Text="Label" Visible="false"></asp:Label>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Dependencia solicitante
                </td>
                <td>Vigencia
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <%--<asp:ListBox runat="server" ID="lstbDireccionSolicitante" SelectionMode="Multiple"></asp:ListBox>--%>
                    <uccCheckBoxList:listaCheck runat="server" ID="lstbDireccionSolicitante" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlVigencia"></asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConsultaDependencias" AutoGenerateColumns="False" AllowPaging="True" PageIndex="0" AllowSorting="true"
                        GridLines="None" Width="100%" DataKeyNames="IdConsultaDependencia" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvConsultaDependencias_PageIndexChanging" OnSelectedIndexChanged="gvConsultaDependencias_SelectedIndexChanged" OnSorting="gvConsultaDependencias_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" SortExpression="Vigencia" />                           
                            <asp:TemplateField HeaderText="Dependencia solicitante" SortExpression="DireccionSolicitante" HeaderStyle-CssClass="centerHeaderText">
                                <ItemTemplate>
                                    <asp:Label ID="lblDireccion" runat="server" Text='<%# ((string)Eval("DireccionSolicitante")).Length < 60? Eval("DireccionSolicitante") :((string)Eval("DireccionSolicitante")).Substring(0,60) + "..."%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Objeto" SortExpression="Objeto" HeaderStyle-CssClass="centerHeaderText">
                                <ItemTemplate>
                                    <asp:Label ID="lblObjeto" runat="server" Text='<%# ((string)Eval("Objeto")).Length < 60? Eval("Objeto") :((string)Eval("Objeto")).Substring(0,60) + "..."%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha estimada solicitud inicial en PACCO" SortExpression="FEstimadaFCTPreliminarREGINICIAL">
                                <ItemTemplate>
                                    <asp:Label ID="lblFEstimadaFCTPreliminarREGINICIAL" runat="server" Text=' <%#Eval("FEstimadaFCTPreliminarREGINICIAL")==null?string.Empty: Convert.ToDateTime(Eval("FEstimadaFCTPreliminarREGINICIAL").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F. estimada DT definitivos / para MC F. estimada ES_MC revisado" SortExpression="FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado">
                                <ItemTemplate>
                                    <asp:Label ID="lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado" runat="server" Text=' <%#Eval("FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado")==null?string.Empty: Convert.ToDateTime(Eval("FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F. estimada entrega ES-EC" SortExpression="FEestimadaES_EC">
                                <ItemTemplate>
                                    <asp:Label ID="lblFEestimadaES_EC" runat="server" Text=' <%#Eval("FEestimadaES_EC")==null?string.Empty: Convert.ToDateTime(Eval("FEestimadaES_EC").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FECHA DE SOLICITUD INICIAL" SortExpression="FechaSolicitudInicial">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaSolicitudInicial" runat="server" Text=' <%#Eval("FechaSolicitudInicial")==null?string.Empty: Convert.ToDateTime(Eval("FechaSolicitudInicial").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F. real DT avalados" SortExpression="FRealFCTDefinitivaParaMCFRealES_MCRevisado">
                                <ItemTemplate>
                                    <asp:Label ID="lblFRealFCTDefinitivaParaMCFRealES_MCRevisado" runat="server" Text=' <%#Eval("FRealFCTDefinitivaParaMCFRealES_MCRevisado")==null?string.Empty: Convert.ToDateTime(Eval("FRealFCTDefinitivaParaMCFRealES_MCRevisado").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F. real entrega ES-EC" SortExpression="FechaDeEntregaESyC">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaDeEntregaESyC" runat="server" Text=' <%#Eval("FechaDeEntregaESyC")==null?string.Empty: Convert.ToDateTime(Eval("FechaDeEntregaESyC").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table width="50%" align="center" border="1">
            <tr>
                <td>F</td>
                <td>Fecha</td>
                <td>REG</td>
                <td>Registro</td>
                <td>Docs</td>
                <td>Documentos</td>
            </tr>
            <tr>
                <td>PS</td>
                <td>Proceso de selecci&oacute;n</td>
                <td>ES</td>
                <td>Estudios de sector</td>
                <td>SDC</td>
                <td>Solicitud de cotizaci&oacute;n</td>
            </tr>
            <tr>
                <td>DT</td>
                <td>Documentos técnicos</td>
                <td>EC</td>
                <td>Estudio de costos</td>
                <td>MC</td>
                <td>M&iacute;nima cuant&iacute;a</td>
            </tr>
        </table>

    </asp:Panel>
    <style>
        /*.lista {
            margin-top: 0px;
            background-color: white;
            border-radius: 0px;
            text-align: left;
            width: 100%;
            border-width: 1px;
            border-top: 1px;
        }

        .caret {
            position: static;
            margin-left: 180px;
        }*/

        .centerHeaderText {
            text-align: center;
        }        
    </style>

    <script type="text/javascript" src="../../../Scripts/bootstrap/bootstrap-multiselect.js"></script>
    <script type="text/javascript">

        //$(document).ready(function () {

        //    $('[id*=lstbDireccionSolicitante]').multiselect({
        //        allSelectedText: "TODOS",
        //        nonSelectedText: "SELECCIONE",
        //        nSelectedText: "SELECCIONADOS",
        //        includeSelectAllOption: true,
        //        selectAllText: "TODOS",
        //        buttonClass: "lista",
        //        buttonWidth: '284px',
        //        numberDisplayed: 1,
        //        maxHeight: 200
        //    });
        //})
    </script>


</asp:Content>
