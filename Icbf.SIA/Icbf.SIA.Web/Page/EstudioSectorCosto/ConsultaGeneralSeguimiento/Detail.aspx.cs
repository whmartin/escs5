using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using System.IO;
using System.Data;
using WsContratosPacco;

public partial class Page_ConsultaGeneralSeguimiento_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/TiempoEntreActividadesEstudioSyC";
    ResultadoEstudioSectorService vEstudioSectorCostoService = new ResultadoEstudioSectorService();
    EstudioSectorCostoService vRegistroEstudio = new EstudioSectorCostoService();
    ReporteConsultaSeguimientoService vSeguimientos = new ReporteConsultaSeguimientoService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;        
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TiempoEntreActividadesEstudioSyC.IdTiempoEntreActividades", hfIdTiempoEntreActividades.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        //var file = Server.MapPath("/Seguimientos/");      

        //System.IO.DirectoryInfo di = new DirectoryInfo(Server.MapPath("/Seguimientos/"));
        //foreach (FileInfo file in di.GetFiles())
        //{
        //    file.Delete();
        //}
        LimpiarCarpetaArchivos();
        GenerarReporte(true);

    }

    private void LimpiarCarpetaArchivos()
    {
        DirectoryInfo dir = new DirectoryInfo(Server.MapPath(@"~\Seguimientos\"));
        try
        {
            if (!Directory.Exists(Server.MapPath(@"~\Seguimientos")))
            {
                DirectoryInfo di = Directory.CreateDirectory(Server.MapPath(@"~\Seguimientos"));
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError("Error 3:" + ex.Message);
        }
        List<string> lstArchivos = new List<string>();
        foreach (var file in dir.GetFiles())
        {
            if (File.Exists(file.FullName))
            {
                try
                {
                    File.Delete(file.FullName);
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError("Error al borrar archivos " + ex.Message);
                }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            pnlContenedor.Enabled = false;
            Decimal vIdConsecutivoEstudio = Convert.ToDecimal(GetSessionParameter("ConsultaGeneralSeguimiento.IdConsecutivoEstudio"));
          
            ViewState["idConsecutivo"] = vIdConsecutivoEstudio;
            ViewState["NombreModalidad"] = "";
            ViewState["NombreDireccionPACCO"] = "";
            ViewState["Revisadopor"] = "";
            decimal  idContrato  = Convert.ToDecimal(GetSessionParameter("ConsultaGeneralSeguimiento.IdConsecutivoEstudio"));

            RemoveSessionParameter("ConsultaGeneralSeguimiento.IdConsecutivoEstudio");
            ///gestion
            List<GestionesEstudioSectorCostos> vGestionesEstudioSC = new List<GestionesEstudioSectorCostos>();
            vGestionesEstudioSC = vSeguimientos.ConsultarGestion_By_IdConsecutivoEstudio(vIdConsecutivoEstudio, 1);
            decimal IdGestionEstudio = 0;
            foreach (var item in vGestionesEstudioSC)
            {
                IdGestionEstudio = item.IdGestionEstudio;               
            }
            //
            //grilla bitacora
            List<BitacoraAcciones> vBitacora = vSeguimientos.ConsultarBitacoraAccion(vIdConsecutivoEstudio, 1);
            decimal IdBitacora = 0;
            foreach(var item in vBitacora)
            {
                IdBitacora = item.IdBitacoraEstudio;
            }
            List<BitacoraAcciones> vLstBitacoraAccion = vRegistroEstudio.ConsultarBitacoraAcciones(Convert.ToInt32(IdBitacora));
            gvBitacoraAcciones.DataSource = vLstBitacoraAccion;
            gvBitacoraAcciones.DataBind();
            //
            var a = txtIdConsecutivoEstudio.BackColor.Name;
            RegistroSolicitudEstudioSectoryCaso vTiemposESyC_idConsecutivo = vSeguimientos.ConsultarRegistroInicial_By_IdConsecutivoEstudio(vIdConsecutivoEstudio, 1);

            List<TiempoEntreActividadesConsulta> vTiempos =
            vSeguimientos.ConsultarTiempos(vIdConsecutivoEstudio, 1);
            
            TiempoEntreActividadesEstudioSyC vTiemposESyC = null;
            TiemposPACCO vTiempoPACCO = null;
            TiempoEntreActividadesEstudioSyC vTiemposESC = null;
            TiempoEntreActividadesEstudioSyC vTiemposReales = null;
            foreach (var Tiempo in vTiempos)
            {
                vTiemposESyC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesEstudioSyC(Tiempo.IdTiempoEntreActividades);

                vTiempoPACCO = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesPACCO(Tiempo.IdTiempoEntreActividades);

                vTiemposESC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasESC(Tiempo.IdTiempoEntreActividades);

                vTiemposReales = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasReales(Tiempo.IdTiempoEntreActividades);
                break;
            }

            hfIdTiempoEntreActividades.Value = vIdConsecutivoEstudio.ToString();
            txtIdConsecutivoEstudio.Text = vIdConsecutivoEstudio.ToString();
            ////Fechas ESyC
            if (vTiemposESC != null)
            {
                vTiemposESyC.FentregaES_ECTiemposEquipo = vTiemposESyC.FentregaES_ECTiemposEquipo != null ? vTiemposESyC.FentregaES_ECTiemposEquipo.Substring(0, 10) : string.Empty;
                vTiemposESyC.FEentregaES_ECTiemposIndicador = vTiemposESyC.FEentregaES_ECTiemposIndicador != null ? vTiemposESyC.FEentregaES_ECTiemposIndicador.Substring(0, 10) : string.Empty;

                 ////Fechas ESC

            vTiemposESC.FRealFCTPreliminarESC = vTiemposESC.FRealFCTPreliminarESC != null ? vTiemposESC.FRealFCTPreliminarESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC != null ? vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaES_EC_ESC = vTiemposESC.FEstimadaES_EC_ESC != null ? vTiemposESC.FEstimadaES_EC_ESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaDocsRadicadosEnContratosESC = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC != null ? vTiemposESC.FEstimadaDocsRadicadosEnContratosESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaComitecontratacionESC = vTiemposESC.FEstimadaComitecontratacionESC != null ? vTiemposESC.FEstimadaComitecontratacionESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaInicioDelPS_ESC = vTiemposESC.FEstimadaInicioDelPS_ESC != null ? vTiemposESC.FEstimadaInicioDelPS_ESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaPresentacionPropuestasESC = vTiemposESC.FEstimadaPresentacionPropuestasESC != null ? vTiemposESC.FEstimadaPresentacionPropuestasESC.Substring(0, 10) : string.Empty;
            vTiemposESC.FEstimadaDeInicioDeEjecucionESC = vTiemposESC.FEstimadaDeInicioDeEjecucionESC != null ? vTiemposESC.FEstimadaDeInicioDeEjecucionESC.Substring(0, 10) : string.Empty;

            txtFRealFCTPreliminarESC.Text = vTiemposESC.FRealFCTPreliminarESC;
            lblFRealFCTPreliminarESC.Visible = vTiemposESC.NoAplicaFRealFCTPreliminarESC == null ? false : vTiemposESC.NoAplicaFRealFCTPreliminarESC.Value;
            txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Text = vTiemposESC.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC;
            lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Visible = vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC == null ? false : vTiemposESC.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Value;
            txtFEstimadaES_EC_ESC.Text = vTiemposESC.FEstimadaES_EC_ESC;
            lblFEstimadaES_EC_ESC.Visible = vTiemposESC.NoAplicaFEstimadaES_EC_ESC == null ? false : vTiemposESC.NoAplicaFEstimadaES_EC_ESC.Value;
            txtFEstimadaDocsRadicadosEnContratosESC.Text = vTiemposESC.FEstimadaDocsRadicadosEnContratosESC;
            lblFEstimadaDocsRadicadosEnContratosESC.Visible = vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC == null ? false : vTiemposESC.NoAplicaFEstimadaDocsRadicadosEnContratosESC.Value;
            txtFEstimadaComitecontratacionESC.Text = vTiemposESC.FEstimadaComitecontratacionESC;
            lblFEstimadaComitecontratacionESC.Visible = vTiemposESC.NoAplicaFEstimadaComitecontratacionESC == null ? false : vTiemposESC.NoAplicaFEstimadaComitecontratacionESC.Value;
            txtFEstimadaInicioDelPS_ESC.Text = vTiemposESC.FEstimadaInicioDelPS_ESC;
            lblFEstimadaInicioDelPS_ESC.Visible = vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC == null ? false : vTiemposESC.NoAplicaFEstimadaInicioDelPS_ESC.Value;
            txtFEstimadaPresentacionPropuestasESC.Text = vTiemposESC.FEstimadaPresentacionPropuestasESC;
            lblFEstimadaPresentacionPropuestasESC.Visible = vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC == null ? false : vTiemposESC.NoAplicaFEstimadaPresentacionPropuestasESC.Value;
            txtFEstimadaDeInicioDeEjecucionESC.Text = vTiemposESC.FEstimadaDeInicioDeEjecucionESC;
            }

            if (vTiemposESyC != null)
            {
                txtFentregaES_ECTiemposEquipo.Text = vTiemposESyC.FentregaES_ECTiemposEquipo;
                txtFEentregaES_ECTiemposIndicador.Text = vTiemposESyC.FEentregaES_ECTiemposIndicador;
                txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Text = vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado;
                lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.Visible = vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR == null ? false : vTiemposESyC.NoAplicaDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoR.Value;
                txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado.BackColor = GetColor(vTiemposESyC.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisadoColor);
                txtDiasHabilesEntreFCTFinalYSDC.Text = vTiemposESyC.DiasHabilesEntreFCTFinalYSDC;
                lblDiasHabilesEntreFCTFinalYSDC.Visible = vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR == null ? false : vTiemposESyC.NoAplicaDiasHabilesEntreFCTFinalYSDCR.Value;
                txtDiasHabilesEntreFCTFinalYSDC.BackColor = GetColor(vTiemposESyC.DiasHabilesEntreFCTFinalYSDCColor);
                txtDiasHabilesEnESOCOSTEO.Text = vTiemposESyC.DiasHabilesEnESOCOSTEO;
                lblDiasHabilesEnESOCOSTEO.Visible = vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR == null ? false : vTiemposESyC.NoAplicaDiasHabilesEnESOCOSTEOR.Value;
                txtDiasHabilesEnESOCOSTEO.BackColor = GetColor(vTiemposESyC.DiasHabilesEnESOCOSTEOColor);
                txtDiashabilesParaDevolucion.Text = vTiemposESyC.DiashabilesParaDevolucion;
                lblDiashabilesParaDevolucion.Visible = vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR == null ? false : vTiemposESyC.NoAplicaDiasHabilesParaDevolucionR.Value;
                txtDiashabilesParaDevolucion.BackColor = GetColor(vTiemposESyC.DiashabilesParaDevolucionColor);
                
            }
            ////Tiempos PACCO
            if (vTiempoPACCO != null)
            {
                vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL != null ? vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaFCTPreliminar = vTiempoPACCO.FEstimadaFCTPreliminar != null ? vTiempoPACCO.FEstimadaFCTPreliminar.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado != null ? vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEestimadaES_EC = vTiempoPACCO.FEestimadaES_EC != null ? vTiempoPACCO.FEestimadaES_EC.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaDocsRadicadosEnContratos = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos != null ? vTiempoPACCO.FEstimadaDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaComiteContratacion = vTiempoPACCO.FEstimadaComiteContratacion != null ? vTiempoPACCO.FEstimadaComiteContratacion.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaInicioDelPS = vTiempoPACCO.FEstimadaInicioDelPS != null ? vTiempoPACCO.FEstimadaInicioDelPS.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaPresentacionPropuestas = vTiempoPACCO.FEstimadaPresentacionPropuestas != null ? vTiempoPACCO.FEstimadaPresentacionPropuestas.Substring(0, 10) : string.Empty;
                vTiempoPACCO.FEstimadaDeInicioDeEjecucion = vTiempoPACCO.FEstimadaDeInicioDeEjecucion != null ? vTiempoPACCO.FEstimadaDeInicioDeEjecucion.Substring(0, 10) : string.Empty;

                txtFEstimadaFCTPreliminarREGINICIAL.Text = vTiempoPACCO.FEstimadaFCTPreliminarREGINICIAL;
                lblFEstimadaFCTPreliminarREGINICIAL.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTPreliminarREGINICIAL.Value; ;
                txtFEstimadaFCTPreliminar.Text = vTiempoPACCO.FEstimadaFCTPreliminar;
                lblFEstimadaFCTPreliminar.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTPreliminar.Value;
                txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Text = vTiempoPACCO.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado;
                lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Visible = vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado == null ? false : vTiempoPACCO.NoAplicaFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Value;
                txtFEestimadaES_EC.Text = vTiempoPACCO.FEestimadaES_EC;
                lblFEestimadaES_EC.Visible = vTiempoPACCO.NoAplicaFEestimadaES_EC == null ? false : vTiempoPACCO.NoAplicaFEestimadaES_EC.Value;
                txtFEstimadaDocsRadicadosEnContratos.Text = vTiempoPACCO.FEstimadaDocsRadicadosEnContratos;
                lblFEstimadaDocsRadicadosEnContratos.Visible = vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos == null ? false : vTiempoPACCO.NoAplicaFEstimadaDocsRadicadosEnContratos.Value;
                txtFEstimadaComiteContratacion.Text = vTiempoPACCO.FEstimadaComiteContratacion;
                lblFEstimadaComiteContratacion.Visible = vTiempoPACCO.NoAplicaFEstimadaComiteContratacion == null ? false : vTiempoPACCO.NoAplicaFEstimadaComiteContratacion.Value; ;
                txtFEstimadaInicioDelPS.Text = vTiempoPACCO.FEstimadaInicioDelPS;
                lblFEstimadaInicioDelPS.Visible = vTiempoPACCO.NoAplicaFEstimadaInicioDelPS == null ? false : vTiempoPACCO.NoAplicaFEstimadaInicioDelPS.Value;
                txtFEstimadaPresentacionPropuestas.Text = vTiempoPACCO.FEstimadaPresentacionPropuestas;
                lblFEstimadaPresentacionPropuestas.Visible = vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas == null ? false : vTiempoPACCO.NoAplicaFEstimadaPresentacionPropuestas.Value; ;
                txtFEstimadaDeInicioDeEjecucion.Text = vTiempoPACCO.FEstimadaDeInicioDeEjecucion;
            }


            ////Tiempos reales
            if (vTiemposReales != null)
            {
                vTiemposReales.FRealFCTPreliminar = vTiemposReales.FRealFCTPreliminar != null ? vTiemposReales.FRealFCTPreliminar.Substring(0, 10) : string.Empty;
                vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado != null ? vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Substring(0, 10) : string.Empty;
                vTiemposReales.FRealES_EC = vTiemposReales.FRealES_EC != null ? vTiemposReales.FRealES_EC.Substring(0, 10) : string.Empty;


                txtFRealFCTPreliminar.Text = vTiemposReales.FRealFCTPreliminar;
                txtFRealDocsRadicadosEnContratos.Text = (vTiemposReales.FRealDocsRadicadosEnContratos == null ? string.Empty : (vTiemposReales.FRealDocsRadicadosEnContratos.Substring(0, 10)));
                txtFRealFCTDefinitivaParaMCFRealES_MCRevisado.Text = vTiemposReales.FRealFCTDefinitivaParaMCFRealES_MCRevisado;
                txtFRealComiteContratacion.Text = (vTiemposReales.FRealComiteContratacion == null ? string.Empty : (vTiemposReales.FRealComiteContratacion.Substring(0, 10)));
                txtFRealES_EC.Text = vTiemposReales.FRealES_EC;
                txtFRealInicioPS.Text = (vTiemposReales.FRealInicioPS == null ? string.Empty : (vTiemposReales.FRealInicioPS.Substring(0, 10)));
                txtFRealPresentacionPropuestas.Text = (vTiemposReales.FRealPresentacionPropuestas == null ? string.Empty : (vTiemposReales.FRealPresentacionPropuestas.Substring(0, 10)));
            }
            //Información Proceso de Selección

            List<InformacionProcesoSeleccion> ProcesoSeleccion = vSeguimientos.ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(Convert.ToInt32(vIdConsecutivoEstudio), null, null, null, null, null, null, null);

            
            if (ProcesoSeleccion != null)
            {
                foreach (var item in ProcesoSeleccion)
                {
                    txtNumeroDelProceso.Text = item.NumeroDelProceso;
                    txtObjetoDelContrato.Text = item.ObjetoContrato;
                    txtEstadoDelProceso.Text = item.EstadoDelProceso;
                    txtFechaAdjudicacion.Text = item.FechaAdjudicacionDelProceso == null ? String.Empty : item.FechaAdjudicacionDelProceso.Value.ToString("dd/MM/yyyy");
                    txtContratista.Text = item.Contratista;
                    txtValorPresupuestadoInicial.Text = item.ValorPresupuestadoInicial.ToString();
                    txtValorAdjudicado.Text = item.ValorAdjudicado.ToString();
                    txtPlazoDeEjecucion.Text = item.PlazoDeEjecucion;
                    txtObservacionInformacionProceso.Text = item.Observacion;
                    txtURLDelProceso.Text = item.URLDelProceso;
                    //idRegistroSolicitud = ProcesoSeleccion.IdRegistroSolicitudEstudioSectoryCaso;
                    break;
                }               
            }
            /////////////
            RegistroSolicitudEstudioSectoryCaso vRegistro = vSeguimientos.ConsultarRegistroInicial(vIdConsecutivoEstudio, 1).FirstOrDefault();
            if (vRegistro != null)
            {
                ManejoControlesContratos vManejoControlesContratos = new ManejoControlesContratos();
                vManejoControlesContratos.LlenarModalidadPACCO(ddlModalidadPACCO, null, true, vRegistro.VigenciaPACCO.Equals(string.Empty)?0:int.Parse(vRegistro.VigenciaPACCO));
                
                //txtIdConsecutivoEstudio.Text = vRegistro.id IdRegistroSolicitudEstudioSectoryCaso.ToString();
                txtConsecutivoPACCO.Text = vRegistro.ConsecutivoPACCO.ToString();
                txtDireccionSolicitante.Text = vRegistro.DireccionSolicitante;
                txtObjetoPACCO.Text = vRegistro.ObjetoPACCO;
                txtValorPresupuestalPACCO.Text = vRegistro.ValorPresupuestalPACCO.ToString();
                txtVigenciaPACCO.Text = vRegistro.VigenciaPACCO;

                foreach (ListItem item in ddlModalidadPACCO.Items)
                {
                    if (item.Value.Equals(vRegistro.ModalidadPACCO.ToString()))
                    {
                        ViewState["NombreModalidad"] = item.Text;
                        txtModalidadDeSeleccionPACCO.Text = item.Text;
                        break;
                    }
                }

                //
                if (vRegistro.VigenciaPACCO.Trim() != "")
                {
                    if (Convert.ToInt32(vRegistro.VigenciaPACCO) != 0)
                    {
                        WsContratosPacco.WSContratosPACCOSoap _wsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
                        GetAreasYCodigos_Result[] vDirecciones = _wsContratosPacco.GetAreas(Convert.ToInt32(vRegistro.VigenciaPACCO));

                        foreach (GetAreasYCodigos_Result vDireccion in vDirecciones.Where(t => t.CodigoArea == vRegistro.DireccionsolicitantePACCO))
                        {
                            ViewState["NombreDireccionPACCO"] = vDireccion.area;
                            DireccionSolicitantePACCO.Text = vDireccion.area;
                        }
                    }
                }
                //
                txtConsecutivoEstudioRelacionado.Text = vRegistro.ConsecutivoEstudioRelacionado.ToString();
                txtFechaSolicitudInicial.Text = vRegistro.FechaSolicitudInicial.ToString("dd/MM/yyyy");
                txtActaCorreoONroDeRadicado.Text = vRegistro.ActaCorreoNoRadicado;
                txtNombreAbreviado.Text = vRegistro.NombreAbreviado;
                txtNumeroDeReproceso.Text = vRegistro.NumeroReproceso;
                txtObjeto.Text = vRegistro.Objeto;
                txtCuentaConVigenciasFuturas.Text = vRegistro.CuentaVigenciasFuturasPACCO;
                txtAplicaProcesoSeleccion.Text = vRegistro.AplicaProcesoSeleccion;
                txtModalidadDeSeleccion.Text = vRegistro.ModalidadSeleccion;
                txtTipoDeEstudio.Text = vRegistro.TipoEstudio;
                txtComplejidadInterna.Text = vRegistro.ComplejidadInterna;
                txtComplejidadIndicador.Text = vRegistro.ComplejidadIndicador;
                txtResponsableES.Text = vRegistro.ResponsableES;
                txtResponsableEC.Text = vRegistro.ResponsableEC;
                txtEstado.Text = vRegistro.EstadoSolicitud;
                txtMotivo.Text = vRegistro.MotivoSolicitud;
                txtDireccionSolicitante.Text = vRegistro.DireccionSolicitante; 
                txtAreaSolicitante.Text = vRegistro.AreaSolicitante;
                txtTipoDeValor.Text = vRegistro.TipoValor.Equals("-1") ? string.Empty : vRegistro.TipoValor;
                txtValorPresupuestoEstimadoSolicitante.Text = vRegistro.ValorPresupuestoEstimadoSolicitante.ToString();
                txtOrdenadorDelGasto.Text = vRegistro.OrdenadorGasto;
                
            }
            //registro de Gestion
            List<GestionesEstudioSectorCostos> vGestionesEstudioSectorCostos = new List<GestionesEstudioSectorCostos>();
            vGestionesEstudioSectorCostos = vSeguimientos.ConsultarGestion_By_IdConsecutivoEstudio(vIdConsecutivoEstudio,100);
            decimal IdGestionEs = 0;
            foreach (var item in vGestionesEstudioSectorCostos) {
                //txtFechaPreguntaOComentario.Text= item.FechaPreguntaOComentario == null ? string.Empty : item.FechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");
                //txtFechaRespuesta.Text = item.FechaRespuesta == null ? string.Empty : item.FechaRespuesta.Value.ToString("dd/MM/yyyy");
                txtUltimaPreguntaOComentario.Text = item.UltimaFechaPreguntaOComentario != null ? Convert.ToString(item.UltimaFechaPreguntaOComentario).Substring(0, 10) : string.Empty;
                txtUltimaRespuesta.Text = item.UltimaFechaRespuesta != null ? Convert.ToString(item.UltimaFechaRespuesta).Substring(0, 10) : string.Empty;

                txtFechaEntregaES_MC.SetDate(item.FechaEntregaES_MC);
                txtFechaEntregaES_MCDefinitiva.Text = item.FechaEntregaES_MCDef == null ? string.Empty : item.FechaEntregaES_MCDef.Value.ToString("dd/MM/yyyy");
                chkNoAplicaFechaEntregaES_MC.Checked = item.NoAplicaFechaEntregaES_MC;
                txtFechaEntregaFCTORequerimiento.SetDate(item.FechaEntregaFCTORequerimiento);
                txtFechaEntregaFCTORequerimientoDefinitiva.Text = item.FechaEntregaFCTORequerimientoDef == null ? string.Empty : item.FechaEntregaFCTORequerimientoDef.Value.ToString("dd/MM/yyyy"); ;
                chkNoAplicaFechaEntregaFCTORequerimiento.Checked = (item.NoAplicaFechaentregaFCTOrequerimiento);
                txtFechaEnvioSDC.SetDate(item.FechaEnvioSDC);
                txtFechaEnvioSDCDefinitiva.Text = item.FechaEnvioSDCDef == null ? string.Empty : item.FechaEnvioSDCDef.Value.ToString("dd/MM/yyyy");
                chkNoAplicaFechaEnvioSDC.Checked = item.NoAplicaFechaEnvioSDC;
                txtPlazoEntregaCotizaciones.SetDate(item.PlazoEntregaCotizaciones);
                txtPlazoEntregaCotizacionesDefinitiva.Text = item.PlazoEntregaCotizacionesDef == null ? string.Empty : item.PlazoEntregaCotizacionesDef.Value.ToString("dd/MM/yyyy"); ;
                chkNoAplicaPlazoEntregaCotizaciones.Checked = (item.NoAplicaPlazoEntregaCotizaciones);
                                
                txtFechaEntregaES_MC.Enabled(false);
                txtFechaEntregaFCTORequerimiento.Enabled(false);
                txtFechaEnvioSDC.Enabled(false);
                txtPlazoEntregaCotizaciones.Enabled(false);
                

                IdGestionEs = item.IdGestionEstudio;
                break;
            }
            ///registro de resultados
            //ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            //vResultadoEstudioSector = vEstudioSectorCostoService.ConsultarResultadoEstudioSector(Convert.ToInt32(vIdConsecutivoEstudio));
            //List<ResultadoEstudioSector> vResultadoEstudioSector =
            //vSeguimientos.ConsultarResultado(vIdConsecutivoEstudio, 30);

            List<ResultadoEstudioSectorConsulta> LstConsulta = vEstudioSectorCostoService.ConsultarResultadoEstudioSectores((long)vIdConsecutivoEstudio, "", "", null, null, null, null, null);
            long IdConsecutivoResultadoEstudio = 0;
            foreach (var item in LstConsulta)
            {

                ResultadoEstudioSector vResultado = new ResultadoEstudioSector();
            vResultado = vEstudioSectorCostoService.ConsultarResultadoEstudioSector((long)item.IdResultadoEstudio);

            
                txtConsultados.Text = vResultado.Consultados.ToString();
                txtParticipantes.Text = vResultado.Participantes.ToString();
                txtCotizacionesRecibidas.Text = vResultado.CotizacionesRecibidas.ToString();
                txtCotizacionesParaPresupuesto.Text = vResultado.CotizacionesParaPresupuesto.ToString();
                txtRadicadoOficioEntregaESyC.Text = vResultado.RadicadoOficioEntregaESyC;
                txtFechaDeEntregaESyC.Text = vResultado.FechaDeEntregaESyC != null ?vResultado.FechaDeEntregaESyC.ToString().Substring(0,10) : string.Empty ;
                txtTipoDeValorRegistroResultado.Text = vResultado.TipoDeValor;
                txtValorPresupuestoESyC.Text = vResultado.ValorPresupuestoESyC.ToString();
                txtKResidual.Text = vResultado.KResidual;
                txtDocumentosRevisadosNAS.Text = vResultado.DocumentosRevisadosNAS;
                txtFecha.Text = vResultado.Fecha != null ? vResultado.Fecha.ToString().Substring(0,10) : string.Empty;

                List<BaseDTO> Usuarios=vEstudioSectorCostoService.ConsultarResponsable();
                string idUser =  vResultado.IdRevisadoPor.ToString();
                var nom=(from U in Usuarios
                                      where U.Id==idUser select new { U.Name });
                foreach (var item1 in nom)
                {
                    txtRevisadoPor.Text = item1.Name;
                    ViewState["Revisadopor"]= item1.Name;
                }
                txtObservacion.Text = vResultado.Observacion;

                ///Grillas 
                /////indicadores
                //gvIndicadores.DataSource = vEstudioSectorCostoService.ConsultarIndicadorResultado(item.IdConsecutivoResultadoEstudio);
                //gvIndicadores.DataBind();
                //gvIndicadores.DataSource = vEstudioSectorCostoService.ConsultarIndicadorResultado(vResultado.IdConsecutivoResultadoEstudio);
                //gvIndicadores.DataBind();
                IdConsecutivoResultadoEstudio = vResultado.IdConsecutivoResultadoEstudio;
                break;
            }
            //grillas

            CargarGrillaIndicadores(IdConsecutivoResultadoEstudio, vIdConsecutivoEstudio);
            //actividades
            CargarGrilla(IdGestionEs,vIdConsecutivoEstudio);
            //gestiones
            gvFechaenvioPreguntaOComentario.DataSource = vGestionesEstudioSectorCostos;
            gvFechaenvioPreguntaOComentario.DataBind();

            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTiempoEntreActividadesEstudioSyC.UsuarioCrea, vTiempoEntreActividadesEstudioSyC.FechaCrea, vTiempoEntreActividadesEstudioSyC.UsuarioModifica, vTiempoEntreActividadesEstudioSyC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void btnReporte_Click(object sender, EventArgs e)
    {
        //toolBar.LipiarMensajeError(); 
        GenerarReporte(true);
    }
    private void GenerarReporte(bool pPrevisualizar = false)
    {
        try
        {

            var reportePath = Server.MapPath(@"~\General\General\Report\ConsultaSeguimiento.xlsx");
            Decimal vIdTiempoEntreActividades = Convert.ToDecimal(ViewState["idConsecutivo"].ToString());
            //datos
            RegistroSolicitudEstudioSectoryCaso vTiemposESyC_idConsecutivo = vSeguimientos.ConsultarRegistroInicial_By_IdConsecutivoEstudio(vIdTiempoEntreActividades, 1);
            
            List<TiempoEntreActividadesConsulta> vTiempos =
            vSeguimientos.ConsultarTiempos(vIdTiempoEntreActividades, 1);

            List<GestionesEstudioSectorCostos> vGestionesEstudioSectorCostos = new List<GestionesEstudioSectorCostos>();
            vGestionesEstudioSectorCostos = vSeguimientos.ConsultarGestion_By_IdConsecutivoEstudio(vIdTiempoEntreActividades, 6);

            ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            List<InformacionProcesoSeleccion> ProcesoSeleccion = null;
            ProcesoSeleccion = vSeguimientos.ConsultarProcesoSeleccion_By_IdConsecutivoEstudio(Convert.ToInt32(vIdTiempoEntreActividades), null, null, null, null, null, null, null);

            List<BitacoraAcciones> vBitacora = vSeguimientos.ConsultarBitacoraAccion(vIdTiempoEntreActividades, 32);

            List<CierreEstudiosPorVigencia> vCierres = vSeguimientos.ConsultarCierresEstudio(vIdTiempoEntreActividades, 1);


            TiempoEntreActividadesEstudioSyC vTiemposESyC = null;
            TiemposPACCO vTiempoPACCO = null;
            TiempoEntreActividadesEstudioSyC vTiemposESC = null;
            TiempoEntreActividadesEstudioSyC vTiemposReales = null;
            RegistroSolicitudEstudioSectoryCaso vRegistro = null;

            List<ResultadoEstudioSectorConsulta> LstConsulta = vEstudioSectorCostoService.ConsultarResultadoEstudioSectores((long)vIdTiempoEntreActividades, "", "", null, null, null, null, null);
            if (LstConsulta.Count >0)
            {
                vResultadoEstudioSector = vEstudioSectorCostoService.ConsultarResultadoEstudioSector(Convert.ToInt32(LstConsulta[0].IdResultadoEstudio));

                vResultadoEstudioSector.RevisadoPor = ViewState["Revisadopor"].ToString();
            }           

            //
            vRegistro = vSeguimientos.ConsultarRegistroInicial(vIdTiempoEntreActividades, 1).FirstOrDefault();
            /////////
            vRegistro.NombreModalidadPACCO= ViewState["NombreModalidad"].ToString();
            vRegistro.NombreDireccionSolicitantePACCO= ViewState["NombreDireccionPACCO"].ToString();
            //
            foreach (var Tiempo in vTiempos)
            {               

                vTiemposESyC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesEstudioSyC(Tiempo.IdTiempoEntreActividades);

                vTiempoPACCO = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesPACCO(Tiempo.IdTiempoEntreActividades);

                vTiemposESC = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasESC(Tiempo.IdTiempoEntreActividades);

                vTiemposReales = vEstudioSectorCostoService.ConsultarTiempoEntreActividadesFechasReales(Tiempo.IdTiempoEntreActividades);

                break;

            }
            //gestiones adicionales
            List<GestionesActividadesAdicionales> vGAdicionales = null;

            foreach (var item in vGestionesEstudioSectorCostos)
            {
                vGAdicionales = vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(item.IdGestionEstudio);
                break;
            }

            //Indicadores
            List<IndicadorAdicionalResultado> vIndicadores = null;
            vIndicadores = vEstudioSectorCostoService.ConsultarIndicadorResultado(vResultadoEstudioSector.IdConsecutivoResultadoEstudio);
            //
            string Usuario= GetSessionUser().NombreUsuario;
            //
            EstudioSectorCostoService a = new EstudioSectorCostoService(); 
            List<RegistroSolicitudEstudioSectoryCaso> ModalidadyCierresAnio = a.ConsultaGeneralSeguimiento((long)vIdTiempoEntreActividades, null, null, null, null, null, null, null);

            ///
            var result = vRegistroEstudio.GenerarReporteSeguimiento(ModalidadyCierresAnio, Usuario, vCierres,vBitacora, vIndicadores, vGAdicionales, ProcesoSeleccion, vResultadoEstudioSector, vGestionesEstudioSectorCostos, vRegistro, vTiemposReales, vTiemposESC, vTiempoPACCO, vTiemposESyC, Convert.ToInt32(vIdTiempoEntreActividades), reportePath);

            if (result != string.Empty)
            {
                string archivoLocal = Path.Combine(Server.MapPath(@"~\Seguimientos"), result);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(archivoLocal);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=ConsultaSeguimiento_" + DateTime.Now.ToString("ddMMyyyy") + ".xls");
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(archivoLocal);
                Response.End();
                toolBar.MostrarMensajeGuardado("Se genero el reporte correctamente");

            }
            else
            {
                toolBar.MostrarMensajeError("Los parametros seleccionados no generarón información, Por favor verifique.");
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError("Error 1:" + ex.Message);
        }

    }

    private void CargarGrillaIndicadores(long IdConsecutivoResultadoEstudio,decimal vIdConsecutivoEstudio)
    {       

        if (vEstudioSectorCostoService.ConsultarIndicadorResultado(IdConsecutivoResultadoEstudio).Count == 0 || vEstudioSectorCostoService.ConsultarIndicadorResultado(IdConsecutivoResultadoEstudio) == null)
        {
            pnlActividades.Visible = true;
        }
        else
        {
            //gvActividades.DataSource = vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            //gvActividades.DataBind();
            //pnlActividades.Visible = true;

            ///
            List<IndicadorAdicionalResultado> vIndicadores = new List<IndicadorAdicionalResultado>();

            vIndicadores = vEstudioSectorCostoService.ConsultarIndicadorResultado(IdConsecutivoResultadoEstudio);
            //gvIndicadores.DataBind();
            
            DataTable a = new DataTable("lista");
            // Declare DataColumn and DataRow variables.
            DataColumn column;
            DataRow row;
            DataView view;

            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "IdIndicadoresAdicionalesResultado";
            a.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Indicador";
            a.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Condicion";
            a.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Valor";
            a.Columns.Add(column);


            // Create new DataRow objects and add to DataTable.    
            //List<GestionesActividadesAdicionales> Gestiones =
            //vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            foreach (var item in vIndicadores)
            {
                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = item.IdIndicadoresAdicionalesResultado;
                row["Indicador"] = item.Indicador;
                row["Condicion"] = item.Condicion;
                row["Valor"] = item.Valor;
                a.Rows.Add(row);
            }
            //
            ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            List<ResultadoEstudioSectorConsulta> LstConsulta = vEstudioSectorCostoService.ConsultarResultadoEstudioSectores((long)vIdConsecutivoEstudio, "", "", null, null, null, null, null);
            if (LstConsulta.Count > 0)
            {
                vResultadoEstudioSector = vEstudioSectorCostoService.ConsultarResultadoEstudioSector(Convert.ToInt32(LstConsulta[0].IdResultadoEstudio));
            }
            if (vResultadoEstudioSector !=null)
            {
                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1001";
                row["Indicador"] = "Indice de Liquidez (veces)";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.IndiceLiquidez;
                
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1002";
                row["Indicador"] = "Nivel de Endeudamiento (%)";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.NivelDeEndeudamiento;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1003";
                row["Indicador"] = "Razon de Cobertura de Intereses (veces)";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.RazonDeCoberturaDeIntereses;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1004";
                row["Indicador"] = "Capital de Trabajo (% del P.O.)";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.CapitalDeTrabajo;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1005";
                row["Indicador"] = "Rentabilidad del Patrimonio (%)";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.RentabilidadDelPatrimonio;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1006";
                row["Indicador"] = "Rentabilidad del Activo (%)";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.RentabilidadDelActivo;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdIndicadoresAdicionalesResultado"] = "1007";
                row["Indicador"] = "Total patrimonio ($) ";
                row["Condicion"] = "Mayor O Igual";
                row["Valor"] = vResultadoEstudioSector.TotalPatrimonio;
                a.Rows.Add(row);
            }

            // Create a DataView using the DataTable.
            view = new DataView(a);

            // Set a DataGrid control's DataSource to the DataView.
            gvIndicadores.DataSource = view;
            gvIndicadores.DataBind();
            //foreach (GridViewRow row in gvActividades.Rows)
            //{
            //    ImageButton btn = row.FindControl("btnEliminar") as ImageButton;
            //    btn.Enabled = false;

            //}
        }
    }

    private void CargarGrilla(decimal pIdGestionEstudio,decimal vIdConsecutivoEstudio)
    {
        //if (vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio).Count == 0 || vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio) == null)
        //{
        //    pnlActividades.Visible = true;
        //}
        //else
        //{
            //gvActividades.DataSource = vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            //gvActividades.DataBind();
            pnlActividades.Visible = true;

            ///
            List<GestionesEstudioSectorCostos> vGestionesEstudioSectorCostos = new List<GestionesEstudioSectorCostos>();
            vGestionesEstudioSectorCostos = vSeguimientos.ConsultarGestion_By_IdConsecutivoEstudio(vIdConsecutivoEstudio, 6);
            decimal IdGestionEs = 0;
            foreach (var item in vGestionesEstudioSectorCostos)
            { }
                DataTable a = new DataTable("lista");
            // Declare DataColumn and DataRow variables.
            DataColumn column;
            DataRow row;
            DataView view;

            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "IdGestionActividadAdicional";
            a.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Actividad";
            a.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Detalle";
            a.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "FechaActividadAdicional";
            a.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.Boolean");
            column.ColumnName = "NoAplicaActividadAdicional";
            a.Columns.Add(column);

            // Create new DataRow objects and add to DataTable.    
            List<GestionesActividadesAdicionales> Gestiones=
            vEstudioSectorCostoService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            foreach(var item in Gestiones)
            {
                row = a.NewRow();
                row["IdGestionActividadAdicional"] = item.IdGestionActividadAdicional;
                row["Actividad"] = item.Actividad;
                row["Detalle"] = item.Detalle;
                row["FechaActividadAdicional"] = item.FechaActividadAdicional != null ? Convert.ToString(item.FechaActividadAdicional) : string.Empty ;
                row["NoAplicaActividadAdicional"] = item.NoAplicaActividadAdicional;
                a.Rows.Add(row);
            }
            //
            foreach (var item in vGestionesEstudioSectorCostos)
            {
                row = a.NewRow();
                row["IdGestionActividadAdicional"] = item.IdGestionEstudio;
                row["Actividad"] = "Fecha Entrega ES_MC Por Parte Del Area Solicitante";
                row["Detalle"] = "";
                row["FechaActividadAdicional"] = item.FechaEntregaES_MC;
                row["NoAplicaActividadAdicional"] = item.NoAplicaFechaEntregaES_MC;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdGestionActividadAdicional"] = item.IdGestionEstudio;
                row["Actividad"] = "Fecha Entrega DT O Requerimiento";
                row["Detalle"] = "";
                row["FechaActividadAdicional"] = item.FechaEntregaFCTORequerimiento;
                row["NoAplicaActividadAdicional"] = item.NoAplicaFechaentregaFCTOrequerimiento;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdGestionActividadAdicional"] = item.IdGestionEstudio;
                row["Actividad"] = "Fecha Envio SDC";
                row["Detalle"] = "";
                row["FechaActividadAdicional"] = item.FechaEnvioSDC;
                row["NoAplicaActividadAdicional"] = item.NoAplicaFechaEnvioSDC;
                a.Rows.Add(row);

                row = a.NewRow();
                row["IdGestionActividadAdicional"] = item.IdGestionEstudio;
                row["Actividad"] = "PLazo Establecido Entre Cotizaciones";
                row["Detalle"] = "";
                row["FechaActividadAdicional"] = item.PlazoEntregaCotizaciones;
                row["NoAplicaActividadAdicional"] = item.NoAplicaPlazoEntregaCotizaciones;
                a.Rows.Add(row);
            }

            // Create a DataView using the DataTable.
            view = new DataView(a);

            // Set a DataGrid control's DataSource to the DataView.
            gvActividades.DataSource = view;
            gvActividades.DataBind();
            //foreach (GridViewRow row in gvActividades.Rows)
            //{
            //    ImageButton btn = row.FindControl("btnEliminar") as ImageButton;
            //    btn.Enabled = false;

            //}
        //}
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Consulta general para seguimiento", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public System.Drawing.Color GetColor(string pColor)
    {
        if (pColor == null) pColor = "0";
        System.Drawing.Color vColor = System.Drawing.ColorTranslator.FromHtml(pColor);
        if (pColor.Equals("0"))
        {
            return System.Drawing.Color.Empty;
        }
        return vColor;
    }

    public void CargarDatosRegistroInicial(decimal IdconsecutivoEstudio)
    {

        RegistroSolicitudEstudioSectoryCaso vRegistro = vRegistroEstudio.ConsultarRegistroSolicitudEstudioSectoryCaso(Convert.ToInt32(IdconsecutivoEstudio));
        if (vRegistro == null)
        {
            return;
        }
        ManejoControlesContratos vManejoControlesContratos = new ManejoControlesContratos();
        vManejoControlesContratos.LlenarModalidadPACCO(ddlModalidadPACCO, null, true, 0);
        vManejoControlesContratos.LlenarTiposEstudio(ddlTipoDeEstudio, null, true);

        txtIdConsecutivoEstudio.Text = vRegistro.IdRegistroSolicitudEstudioSectoryCaso.ToString();
        txtConsecutivoPACCO.Text = vRegistro.ConsecutivoPACCO.ToString();
        txtDireccionSolicitante.Text = vRegistro.DireccionSolicitante;
        txtObjetoPACCO.Text = vRegistro.ObjetoPACCO;
        txtValorPresupuestalPACCO.Text = vRegistro.ValorPresupuestalPACCO.ToString();
        txtVigenciaPACCO.Text = vRegistro.VigenciaPACCO.ToString();
        foreach (ListItem item in ddlModalidadPACCO.Items)
        {
            if (item.Value.Equals(vRegistro.ModalidadPACCO.ToString()))
            {
                txtModalidadDeSeleccionPACCO.Text = item.Text;
                break;
            }
        }

        txtConsecutivoEstudioRelacionado.Text = vRegistro.ConsecutivoEstudioRelacionado.ToString();
        txtFechaSolicitudInicial.Text = vRegistro.FechaSolicitudInicial.ToString("dd/MM/yyyy");
        txtActaCorreoONroDeRadicado.Text = vRegistro.ActaCorreoNoRadicado;
        txtNombreAbreviado.Text = vRegistro.NombreAbreviado;
        txtNumeroDeReproceso.Text = vRegistro.NumeroReproceso.ToString();
        txtObjeto.Text = vRegistro.Objeto;
        txtCuentaConVigenciasFuturas.Text = vRegistro.CuentaVigenciasFuturasPACCO.ToString();
        txtAplicaProcesoSeleccion.Text = vRegistro.AplicaProcesoSeleccion.ToString();
        txtModalidadDeSeleccion.Text = vRegistro.ModalidadSeleccion;
        txtTipoDeEstudio.Text = vRegistro.IdTipoEstudio.ToString();//id
        txtComplejidadInterna.Text = vRegistro.IdComplejidadInterna.ToString();
        txtComplejidadIndicador.Text = vRegistro.IdComplejidadIndicador.ToString();
        txtResponsableES.Text = vRegistro.IdResponsableES.ToString();
        txtResponsableEC.Text = vRegistro.IdResponsableEC.ToString();
        txtEstado.Text = vRegistro.EstadoSolicitud;
        txtMotivo.Text = vRegistro.IdMotivoSolicitud.ToString();
        txtDireccionSolicitante.Text = vRegistro.DireccionSolicitante;
        txtAreaSolicitante.Text = vRegistro.IdAreaSolicitante.ToString();
        txtTipoDeValor.Text = vRegistro.TipoValor;
        txtValorPresupuestoEstimadoSolicitante.Text = vRegistro.ValorPresupuestoEstimadoSolicitante.ToString();

    }

    public void CargarDatosRegistroGestiones(decimal pIdconsecutivoEstudio)
    {
        GestionesEstudioSectorCostos vGestion = vEstudioSectorCostoService.ConsultarGestionEstudioSector(pIdconsecutivoEstudio);
    }

}
