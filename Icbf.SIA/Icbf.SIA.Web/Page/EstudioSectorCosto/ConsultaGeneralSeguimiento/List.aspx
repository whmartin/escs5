<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConsultaGeneralSeguimiento_List" %>
<%@ Register TagPrefix="uccCheckBoxList" TagName="listaCheck" Src="~/General/General/EstudioSectores/CheckBoxList.ascx" %>
<%@ Register TagPrefix="uccConsulta" TagName="Consulta" Src="~/General/General/EstudioSectores/FormularioConsulta.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <style>
        .lista {
            -webkit-appearance: menulist;
            background-color: white;
            border-radius: 0px;
            text-align: left;
            width: 100%;
            border-width: 1px;
            border-top: 1px;
        }

        .caret {
            margin-left: 40px;
        }
    </style>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Consecutivo  solicitud
                </td>

            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server"  ID="txtConsecutivoEstudio" MaxLength="5"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                </td>
                <td></td>
            </tr>
            <tr class="rowB">

                <td>Objeto
                </td>
                <td>Nombre Abreviado
                </td>
            </tr>
            <tr class="rowA">
                <td rowspan="3">
                    <asp:TextBox runat="server" ID="txtObjeto"  TextMode="MultiLine" Rows="4" Columns="30" onChange="limitText(this,400);" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" MaxLength="400"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteObjeto" TargetControlID="txtObjeto" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                </td>
                <td rowspan="1">
                    <asp:TextBox runat="server" ID="txtNombreAbreviado"  MaxLength="255"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteNombreAbreviado" TargetControlID="txtNombreAbreviado" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
                <td></td>
            </tr>
            <tr class="rowB">
                <td>Consecutivo PACCO
                </td>
                <td>Modalidad de contratación
                </td>
            </tr>
            <tr class="rowA">
                <td>                   
                
                    <asp:TextBox runat="server" ID="txtConsecutivoPACCO"  MaxLength="9"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoPACCO" TargetControlID="txtConsecutivoPACCO" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                </td>
                <td>
                    <asp:DropDownList  runat="server" ID="ddlModalidadDeSeleccion"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Estado
                </td>
                <td>Año cierre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <%--<asp:ListBox ID="lstbEstado" TabIndex="6" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                    <uccCheckBoxList:listaCheck runat="server" ID="lstbEstado" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlAnioCierre"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td></td>
                <td>Vigencia
                </td>
            </tr>
            <tr class="rowA">
                <td></td>
                <td>
                    <asp:DropDownList runat="server"  ID="ddlVigencia"></asp:DropDownList>
                </td>

            </tr>
        </table>
    </asp:Panel>
    <asp:DropDownList ID="ddlModalidadPACCO" runat="server" Visible="false"></asp:DropDownList>
    <asp:DropDownList ID="ddlDireccionPACCO" runat="server" Visible="false"></asp:DropDownList>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvEstudiosSector" AutoGenerateColumns="False" AllowPaging="True" PageIndex="0" AllowSorting="true"
                        GridLines="None" Width="100%" DataKeyNames="IdRegistroSolicitudEstudioSectoryCaso" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvEstudiosSector_PageIndexChanging" OnSelectedIndexChanged="gvEstudiosSector_SelectedIndexChanged" OnSorting="gvEstudiosSector_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo  solicitud" DataField="IdRegistroSolicitudEstudioSectoryCaso" SortExpression="IdRegistroSolicitudEstudioSectoryCaso" />
                            <asp:BoundField HeaderText="Objeto PACCO" DataField="ObjetoPACCO" SortExpression="ObjetoPACCO" />
                            <asp:BoundField HeaderText="Nombre Abreviado" DataField="NombreAbreviado" SortExpression="NombreAbreviado" />                            
                            <asp:TemplateField HeaderText="Consecutivo PACCO" SortExpression="ConsecutivoPACCO">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblConsecutivoPACCO" Text='<%# Eval("ConsecutivoPACCO").ToString().Equals("0")?string.Empty: Eval("ConsecutivoPACCO").ToString() %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Modalidad contratación" DataField="ModalidadSeleccion" SortExpression="ModalidadSeleccion" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoSolicitud" SortExpression="EstadoSolicitud" />
                            <asp:TemplateField HeaderText="Año Cierre" SortExpression="AnioCierre">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblAnioCierre" Text='<%# Eval("AnioCierre").ToString().Equals("0")?string.Empty: Eval("AnioCierre").ToString() %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Vigencia" DataField="AnioVigencia" SortExpression="AnioVigencia" />


                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" src="../../../Scripts/bootstrap/bootstrap-multiselect.js"></script>
    <script type="text/javascript">

        /*$(document).ready(function () {

            $('[id*=lstbEstado]').multiselect({
                allSelectedText: "TODOS",
                nonSelectedText: "SELECCIONE",
                nSelectedText: "SELECCIONADOS",
                includeSelectAllOption: true,
                selectAllText: "TODOS",
                buttonClass: "lista"

            });

            $('[id*=lstbEstado]').removeClass('btn btn-default');
        })*/





    </script>
</asp:Content>
