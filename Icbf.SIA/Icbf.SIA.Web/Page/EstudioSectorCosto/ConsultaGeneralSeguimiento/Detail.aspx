<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ConsultaGeneralSeguimiento_Detail" %>

<%@ Register TagPrefix="uc1" TagName="Fechas" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfIdTiempoEntreActividades" runat="server" />

    <asp:Panel ID="pnlContenedor" runat="server">

        <table width="90%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlRegistroInicial" runat="server" GroupingText="Registro Inicial" BorderWidth="1">
                        <table width="70%" align="center">
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlRegistroInicialPACCO" runat="server" GroupingText="PACCO" BorderWidth="1">
                                        <table width="70%" align="center">
                                            <tr class="rowB">
                                                <td>Consecutivo  solicitud *</td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtIdConsecutivoEstudio" MaxLength="5" ClientIDMode="Static"></asp:TextBox>
                                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtIdConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>

                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Consecutivo PACCO
                                                </td>
                                                <td>Dependencia solicitante PACCO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtConsecutivoPACCO"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="DireccionSolicitantePACCO"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Objeto PACCO
                                                </td>
                                                <td>Valor Presupuestal PACCO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtObjetoPACCO"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtValorPresupuestalPACCO"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Vigencia PACCO
                                                </td>
                                                <td>Modalidad de contratación PACCO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtVigenciaPACCO"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtModalidadDeSeleccionPACCO"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlModalidadPACCO" runat="server" Visible="false"></asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>

                        <br />
                        <table width="70%" align="center">
                            <tr class="rowB">
                                <td>Consecutivo solicitud relacionado</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtConsecutivoEstudioRelacionado"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Fecha de solicitud inicial *
                                </td>
                                <td>Acta, correo  o Nro. de radicado *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFechaSolicitudInicial"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtActaCorreoONroDeRadicado"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Nombre abreviado *
                                </td>
                                <td>Número de reproceso * 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtNombreAbreviado"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtNumeroDeReproceso"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Objeto * 
                                </td>
                                <td>¿Cuenta con Vigencias Futuras?
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtObjeto"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtCuentaConVigenciasFuturas"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>¿Se busca suscribir un contrato? *
                                </td>
                                <td>Modalidad de contratación *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtAplicaProcesoSeleccion"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtModalidadDeSeleccion"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Tipo de Solicitud *
                                </td>
                                <td>Complejidad Interna *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtTipoDeEstudio"></asp:TextBox>
                                    <asp:DropDownList ID="ddlTipoDeEstudio" runat="server" Visible="false"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtComplejidadInterna"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Complejidad Indicador *
                                </td>
                                <td>Responsable DT o ES *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtComplejidadIndicador"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtResponsableES"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Responsable EC *
                                </td>
                                <td>Ordenador del gasto *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtResponsableEC"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtOrdenadorDelGasto"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Estado *
                                </td>
                                <td>Motivo *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtEstado"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtMotivo"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Dependencia solicitante *
                                </td>
                                <td>Área solicitante *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtDireccionSolicitante"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtAreaSolicitante"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Tipo de Valor *
                                </td>
                                <td>Valor presupuesto estimado solicitante *
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtTipoDeValor"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtValorPresupuestoEstimadoSolicitante"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
        </table>


        <br />
        <table width="90%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlRegistroDeGestiones" runat="server" GroupingText="Registro de Gestiones" BorderWidth="1">
                        <table width="70%" align="center">
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlFechaPreguntaOComentario" runat="server" GroupingText="Fechas envió preguntas y recepción respuestas a los DT" BorderWidth="1">

                                        <table width="90%" align="center">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvFechaenvioPreguntaOComentario" runat="server" AutoGenerateColumns="False">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Fecha Pregunta">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblFechaPreguntaOComentario" Text='<%# Eval("FechaPreguntaOComentario")==null?string.Empty:Convert.ToDateTime( Eval("FechaPreguntaOComentario")).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Fecha Respuesta">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblFechaRespuesta" Text='<%# Eval("FechaRespuesta")==null?string.Empty:Convert.ToDateTime( Eval("FechaRespuesta")).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                    <br />
                                                </td>
                                            </tr>
                                            <%--<tr class="rowB">
                                                <td>Fecha Pregunta o Comentario</td>
                                                <td>Fecha Respuesta</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFechaPreguntaOComentario"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFechaRespuesta"></asp:TextBox>
                                                </td>
                                            </tr>--%>

                                            <tr class="rowB">
                                                <td>Última Pregunta</td>
                                                <td>Última Respuesta</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtUltimaPreguntaOComentario"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtUltimaRespuesta"></asp:TextBox>
                                                </td>
                                            </tr>

                                        </table>

                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel Visible="false" ID="pnlFechaEntregaESMC" runat="server" GroupingText="Fecha entrega ES_MC por parte del área solicitante" BorderWidth="1">
                                        <table width="90%" align="center">
                                            <tr class="rowB">
                                                <td>Fecha
                                                </td>
                                                <td>Definitiva
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <uc1:Fechas runat="server" ID="txtFechaEntregaES_MC" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaEntregaES_MCDefinitiva" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkNoAplicaFechaEntregaES_MC" runat="server" Checked="false" Text="No Aplica" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <%--<br />--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel Visible="false" ID="pnlFechaEntregaFCT" runat="server" GroupingText="Fecha entrega DT o requerimiento" BorderWidth="1">
                                        <table width="90%" align="center">
                                            <tr class="rowB">
                                                <td>Fecha
                                                </td>
                                                <td>Definitiva
                                                </td>
                                            </tr>
                                            <tr class="rowA">

                                                <td>
                                                    <uc1:Fechas runat="server" ID="txtFechaEntregaFCTORequerimiento" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaEntregaFCTORequerimientoDefinitiva" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkNoAplicaFechaEntregaFCTORequerimiento" runat="server" Checked="false" Text="No Aplica" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <%--<br />--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel Visible="false" ID="pnlFechaEnvioSDC" runat="server" GroupingText="Fecha envío SDC" BorderWidth="1">
                                        <table width="90%" align="center">

                                            <tr class="rowB">
                                                <td>Fecha 
                                                </td>
                                            </tr>
                                            <tr class="rowA">

                                                <td>
                                                    <uc1:Fechas runat="server" ID="txtFechaEnvioSDC" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaEnvioSDCDefinitiva" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkNoAplicaFechaEnvioSDC" runat="server" Checked="false" Text="No Aplica" />
                                                </td>
                                            </tr>

                                        </table>
                                    </asp:Panel>
                                    <%--<br />--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel Visible="false" ID="pnlPlazoEstablecidoCotizaciones" runat="server" GroupingText="Plazo establecido entrega cotizaciones" BorderWidth="1">
                                        <table width="90%" align="center">
                                            <tr class="rowB">

                                                <td>Fecha
                                                </td>
                                                <td>Definitiva
                                                </td>
                                            </tr>
                                            <tr class="rowA">

                                                <td>
                                                    <uc1:Fechas runat="server" ID="txtPlazoEntregaCotizaciones" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPlazoEntregaCotizacionesDefinitiva" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkNoAplicaPlazoEntregaCotizaciones" runat="server" Checked="false" Text="No Aplica" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <%--<br />--%>
                                </td>
                            </tr>

                            <tr>
                                <td>

                                    <asp:Panel ID="pnlActividades" runat="server" GroupingText="Actividades" BorderWidth="1">
                                        <table width="70%" align="center">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvActividades" align="center" runat="server" AutoGenerateColumns="false" Width="100%" GridLines="None" ShowHeaderWhenEmpty="true">
                                                        <Columns>
                                                            <asp:BoundField HeaderText="IdGestionActividadAdicional" DataField="IdGestionActividadAdicional" Visible="false" />
                                                            <asp:BoundField HeaderText="Actividad" DataField="Actividad" />
                                                            <asp:BoundField HeaderText="Detalle" DataField="Detalle" />
                                                            <asp:TemplateField HeaderText="Fecha" SortExpression="FechaActividadAdicional">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecha" runat="server" Text=' <%#Eval("FechaActividadAdicional")==null || Convert.ToString(Eval("FechaActividadAdicional"))== ""  ?string.Empty: Convert.ToDateTime(Eval("FechaActividadAdicional").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No Aplica">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkNoAplicaActividadAdicional" Checked='<%#Eval("NoAplicaActividadAdicional") %>' Enabled="false" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>

                        <br />

                    </asp:Panel>
                </td>
            </tr>
        </table>

        <br />

        <table width="90%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlRegistroDeResultados" runat="server" GroupingText="Registro de Resultados" BorderWidth="1">
                        <table width="70%" align="center">
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlProveedores" runat="server" GroupingText="Proveedores" BorderWidth="1">
                                        <table width="70%" align="center">
                                            <tr class="rowB">
                                                <td>Consultados
                                                </td>
                                                <td>Participantes
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtConsultados"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtParticipantes"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Cotizaciones recibidas
                                                </td>
                                                <td>Cotizaciones para presupuesto
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtCotizacionesRecibidas"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtCotizacionesParaPresupuesto"></asp:TextBox>

                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>
                        </table>
                        <table width="70%" align="center">
                            <tr class="rowB">
                                <td>Correo electrónico aval DT o No. Radicado oficio entrega ESyC
                                </td>
                                <td>Fecha aval DT o entrega ESyC
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtRadicadoOficioEntregaESyC"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFechaDeEntregaESyC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Tipo de valor
                                </td>
                                <td>Valor presupuesto ESyC
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtTipoDeValorRegistroResultado"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtValorPresupuestoESyC"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>K Residual *
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtKResidual"></asp:TextBox>
                                    <br />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <table width="70%" align="center">
                            <tr>
                                <td>
                                    <br />
                                    <asp:Panel ID="pnlIndicadores" runat="server" GroupingText="Indicadores" BorderWidth="1">
                                        <table width="70%" align="center">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvIndicadores" align="center" runat="server" AutoGenerateColumns="false" Width="90%" BorderWidth="2px" BorderColor="Black" HorizontalAlign="Center" HeaderStyle-BackColor="LightGray" GridLines="Both" EmptyDataText="">
                                                        <Columns>
                                                            <asp:BoundField HeaderText="IdIndicadoresAdicionalesResultado" DataField="IdIndicadoresAdicionalesResultado" Visible="false" />
                                                            <asp:BoundField HeaderText="Indicador" DataField="Indicador" />
                                                            <asp:BoundField HeaderText="Condición" DataField="condicion" />
                                                            <asp:BoundField HeaderText="Valor" DataField="Valor" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                    <br />
                                                </td>

                                            </tr>
                                            <tr class="rowB">
                                                <td>Documentos Revisados NAS *</td>
                                                <td>Fecha</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtDocumentosRevisadosNAS"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFecha"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Revisado por</td>
                                                <td>Observación</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtRevisadoPor"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtObservacion"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table width="90%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlTiempoentreActividades" runat="server" GroupingText="Fecha entre actividades" BorderWidth="1">
                        <table width="70%" align="center">
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlTiemposFechasESyC" runat="server" GroupingText="Tiempos y Fechas ESyC" BorderWidth="1">
                                        <table width="90%" align="center">
                                            <tr class="rowB">
                                                <td>F. Entrega ES-EC tiempos equipo *
                                                </td>
                                                <td>F. Entrega ES-EC tiempos indicador *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFentregaES_ECTiemposEquipo"></asp:TextBox>
                                                    <asp:Label ID="lblFentregaES_ECTiemposEquipo" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEentregaES_ECTiemposIndicador"></asp:TextBox>
                                                    <asp:Label ID="lblFEentregaES_ECTiemposIndicador" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Días hábiles entre DT Inicial y final/para MC entre DT inicial y ES_MC revisado *
                                                </td>
                                                <td>Días hábiles entre DT final y SDC *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado"></asp:TextBox>
                                                    <asp:Label ID="lblDiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiasHabilesEntreFCTFinalYSDC"></asp:TextBox>
                                                    <asp:Label ID="lblDiasHabilesEntreFCTFinalYSDC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>Días hábiles en ES O COSTEO *
                                                </td>
                                                <td>Días hábiles para devolución *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiasHabilesEnESOCOSTEO"></asp:TextBox>
                                                    <asp:Label ID="lblDiasHabilesEnESOCOSTEO" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtDiashabilesParaDevolucion"></asp:TextBox>
                                                    <asp:Label ID="lblDiashabilesParaDevolucion" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlPACCO" runat="server" GroupingText="PACCO" BorderColor="1" Style="border-width: 1px; border-style: solid;">
                                        <table width="90%" align="center">
                                            <tr class="rowB">
                                                <td>F. Estimada DT preliminar REG. INICIAL *
                                                </td>
                                                <td>F. Estimada DT preliminar *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTPreliminarREGINICIAL"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaFCTPreliminarREGINICIAL" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTPreliminar"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaFCTPreliminar" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada DT definitiva/para  MC F. Estimada ES_MC revisado *
                                                </td>
                                                <td>F. Estimada  ES-EC *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEestimadaES_EC"></asp:TextBox>
                                                    <asp:Label ID="lblFEestimadaES_EC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada docs radicados en contratos *
                                                </td>
                                                <td>F. Estimada comité contratación *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDocsRadicadosEnContratos"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaDocsRadicadosEnContratos" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaComiteContratacion"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaComiteContratacion" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada inicio del PS *
                                                </td>
                                                <td>F. Estimada presentación propuesta *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaInicioDelPS"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaInicioDelPS" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaPresentacionPropuestas"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaPresentacionPropuestas" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada de inicio de ejecución *
                                                </td>

                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDeInicioDeEjecucion"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaDeInicioDeEjecucion" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlechasSistemaESC" runat="server" GroupingText="Fechas Sistema ESC" BorderWidth="1">

                                        <table width="90%" align="center">
                                            <tr class="rowB">
                                                <td>F. Real DT preliminar ESC *
                                                </td>
                                                <td>F. Estimada DT definitiva / para MC F. Estimada ES_MC  revisado ESC *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealFCTPreliminarESC"></asp:TextBox>
                                                    <asp:Label ID="lblFRealFCTPreliminarESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada  ES-EC. ESC *
                                                </td>
                                                <td>F. Estimada docs radicados en contratos ESC *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaES_EC_ESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaES_EC_ESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDocsRadicadosEnContratosESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaDocsRadicadosEnContratosESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada comité contratación ESC *
                                                </td>
                                                <td>F. Estimada inicio del PS. ESC *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaComitecontratacionESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaComitecontratacionESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaInicioDelPS_ESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaInicioDelPS_ESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Estimada presentación propuestas ESC *
                                                </td>
                                                <td>F. Estimada de inicio de ejecución ESC *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaPresentacionPropuestasESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaPresentacionPropuestasESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFEstimadaDeInicioDeEjecucionESC"></asp:TextBox>
                                                    <asp:Label ID="lblFEstimadaDeInicioDeEjecucionESC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Panel ID="pnlFechasReales" runat="server" GroupingText="Fechas Reales" BorderWidth="1">
                                        <table width="90%" align="center">
                                            <tr class="rowB">
                                                <td>F. Real DT preliminar *
                                                </td>
                                                <td>F. Real docs radicados en contratos *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealFCTPreliminar"></asp:TextBox>
                                                    <asp:Label ID="lblFRealFCTPreliminar" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealDocsRadicadosEnContratos"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Real DT definitiva/para MC F. Real ES_MC revisado *
                                                </td>
                                                <td>F. Real comité contratación *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealFCTDefinitivaParaMCFRealES_MCRevisado"></asp:TextBox>
                                                    <asp:Label ID="lblFRealFCTDefinitivaParaMCFRealES_MCRevisado" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealComiteContratacion"></asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td>F. Real ES-EC *
                                                </td>
                                                <td>F. Real inicio PS *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealES_EC"></asp:TextBox>
                                                    <asp:Label ID="lblFRealES_EC" runat="server" Text="No Aplica" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealInicioPS"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="rowB">
                                                <td colspan="2">F. Real presentación propuestas *
                                                </td>
                                            </tr>
                                            <tr class="rowA">
                                                <td colspan="2">
                                                    <asp:TextBox runat="server" Enabled="false" ID="txtFRealPresentacionPropuestas"></asp:TextBox>

                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table width="90%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlBitacoraAcciones" runat="server" GroupingText="Bitácora de Acciones" BorderWidth="1">
                        <table width="70%" align="center">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvBitacoraAcciones" align="center" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="none" DataKeyNames="IdBitacoraAcciones">
                                        <Columns>
                                            <asp:BoundField HeaderText="IdBitacoraAcciones" DataField="IdBitacoraAcciones" Visible="false" />
                                            <asp:TemplateField HeaderText="Fecha Registro" SortExpression="FechaRegistro">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFechaRegistro" runat="server" Text=' <%#Eval("FechaRegistro")==null?string.Empty:Convert.ToDateTime(Eval("FechaRegistro").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha Modificación" SortExpression="FechaModifica">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFechaModifica" runat="server" Text=' <%#Eval("FechaModifica")==null?string.Empty:Convert.ToDateTime(Eval("FechaModifica").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Usuario" DataField="UsuarioBitacora" />
                                            <asp:BoundField HeaderText="Última Acción" DataField="UltimaAccion" />
                                            <asp:BoundField HeaderText="Próxima Acción" DataField="ProximaAccion" />

                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </asp:Panel>
                </td>
            </tr>

        </table>
        <br />
        <table width="90%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlInformacionProcesoSeleccion" runat="server" GroupingText="Información Proceso de Selección" BorderWidth="1">
                        <table width="70%" align="center">
                            <tr class="rowB">
                                <td>Número del Proceso
                                </td>
                                <td>Objeto del contrato
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtNumeroDelProceso"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtObjetoDelContrato"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Estado del proceso
                                </td>
                                <td>Fecha adjudicación
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtEstadoDelProceso"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtFechaAdjudicacion"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Contratista
                                </td>
                                <td>Valor presupuestado inicial
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtContratista"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtValorPresupuestadoInicial"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Valor adjudicado
                                </td>
                                <td>Plazo de ejecución
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtValorAdjudicado"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtPlazoDeEjecucion"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Observación
                                </td>
                                <td>URL del proceso
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtObservacionInformacionProceso"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Enabled="false" ID="txtURLDelProceso"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
        </table>

    </asp:Panel>
</asp:Content>
