using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Linq.Expressions;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class Page_ConsultaGeneralSeguimiento_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ConsultaGeneralSeguimiento";
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();
    ReporteConsultaSeguimientoService vReporte = new ReporteConsultaSeguimientoService();
    EstudioSectorCostoService VEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos = new ManejoControlesContratos();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        txtConsecutivoEstudio.Focus();
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }

        HtmlMeta meta = new HtmlMeta();
        meta.HttpEquiv = "X-UA-Compatible";
        meta.Content = "IE=edge";
        HtmlHead head = Page.Header;
        head.Controls.Add(meta);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        LLenarGrilla(Buscar());
    }

    //protected void btnNuevo_Click(object sender, EventArgs e)
    //{
    //    NavigateTo(SolutionPage.Add);
    //}

    private List<RegistroSolicitudEstudioSectoryCaso> Buscar()
    {
        List<RegistroSolicitudEstudioSectoryCaso> vLstCosulta = new List<RegistroSolicitudEstudioSectoryCaso>();
        try
        {

            long? vIdConsecutivoEstudio = txtConsecutivoEstudio.Text.Equals(string.Empty) ? (long?)null : long.Parse(txtConsecutivoEstudio.Text);
            string vNombreAbreviado = txtNombreAbreviado.Text.Equals(string.Empty) ? null : txtNombreAbreviado.Text;
            string vObjeto = txtObjeto.Text.Equals(string.Empty) ? null : txtObjeto.Text;
            long? vIdConsecutivoPACCO = txtConsecutivoPACCO.Text.Equals(string.Empty) ? (long?)null : long.Parse(txtConsecutivoPACCO.Text);
            int? vIdModalidadDeSeleccion = ddlModalidadDeSeleccion.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlModalidadDeSeleccion.SelectedValue);
            string vIdEstado = null;
            foreach (ListItem item in lstbEstado.items())
            {
                if (item.Selected)
                {
                    vIdEstado += item.Value + ",";
                }
            }

            if (vIdEstado != null)
            {
                vIdEstado = vIdEstado.Remove(vIdEstado.Length - 1, 1);
            }
            int? vAnioCierre = ddlAnioCierre.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlAnioCierre.SelectedValue);
            int? vVigencia = ddlVigencia.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : int.Parse(ddlVigencia.SelectedValue);

            vLstCosulta = VEstudioSectorCostoService.ConsultaGeneralSeguimiento(vIdConsecutivoEstudio, vObjeto, vNombreAbreviado, vIdConsecutivoPACCO, vIdModalidadDeSeleccion, vIdEstado, vAnioCierre, vVigencia);
            return vLstCosulta;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return vLstCosulta;
    }


    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);
            gvEstudiosSector.PageSize = PageSize();
            gvEstudiosSector.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Consulta general para seguimiento", SolutionPage.List.ToString());

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvEstudiosSector.Rows.Count > 0)
        {
            toolBar.LipiarMensajeError();
            Exportar();
        }
        else
        {            
            //toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
            gvEstudiosSector.DataSource = null;
            gvEstudiosSector.DataBind();           
            return;
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvEstudiosSector.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ConsultaGeneralSeguimiento.IdConsecutivoEstudio", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvEstudiosSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvEstudiosSector.SelectedRow);
    }
    protected void gvEstudiosSector_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEstudiosSector.PageIndex = e.NewPageIndex;
        LLenarGrilla(Buscar());
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ResultadoEstudioSector.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ResultadoEstudioSector.Eliminado");

            Utilidades.LlenarDropDownList(ddlModalidadDeSeleccion, vResultadoEstudioSectorService.ConsultarModalidadSeleccion());
            Utilidades.LlenarDropDownList(ddlVigencia, vResultadoEstudioSectorService.ConsultarVigencia(),false);
            lstbEstado.AddItems(vResultadoEstudioSectorService.ConsultarEstado());
            lstbEstado.DataBind();         
            Utilidades.LlenarDropDownList(ddlAnioCierre, vResultadoEstudioSectorService.ConsultarAnioCierre(),false);



        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvEstudiosSector_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortDirection = GridViewSortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
        OrdenarGrilla(e.SortExpression, Buscar());
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;

    }
    protected void OrdenarGrilla(string pSortExpresion, List<RegistroSolicitudEstudioSectoryCaso> vLstConsulta)
    {
        if (vLstConsulta != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvEstudiosSector.DataSource = vLstConsulta;
                gvEstudiosSector.DataBind();
                return;

            }

            var param = Expression.Parameter(typeof(RegistroSolicitudEstudioSectoryCaso), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<RegistroSolicitudEstudioSectoryCaso, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvEstudiosSector.DataSource = vLstConsulta.AsQueryable<RegistroSolicitudEstudioSectoryCaso>().OrderBy(sortExpression).ToList<RegistroSolicitudEstudioSectoryCaso>();
            }
            else
            {
                gvEstudiosSector.DataSource = vLstConsulta.AsQueryable<RegistroSolicitudEstudioSectoryCaso>().OrderByDescending(sortExpression).ToList<RegistroSolicitudEstudioSectoryCaso>();
            };

            gvEstudiosSector.DataBind();
        }
    }

    protected void LLenarGrilla(List<RegistroSolicitudEstudioSectoryCaso> pLstGestion)
    {
        if (pLstGestion == null || pLstGestion.Count == 0)
        {
            gvEstudiosSector.DataSource = null;
            gvEstudiosSector.DataBind();
            return;
        }
        toolBar.LipiarMensajeError();
        string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
        if (pSortExpresion.Equals(string.Empty))
        {
            OrdenarGrilla(Constantes.SinOrden, pLstGestion);
        }
        else
        {
            OrdenarGrilla(pSortExpresion, pLstGestion);
        }

    }

    private void Exportar()
    {
        List<RegistroSolicitudEstudioSectoryCaso> vLstRes = Buscar();
        List<RegistroSolicitudEstudioSectoryCaso> vLstRegistroinicial = new List<RegistroSolicitudEstudioSectoryCaso>();
        List<GestionesEstudioSectorCostos> vLstGestion = new List<GestionesEstudioSectorCostos>();
        foreach (RegistroSolicitudEstudioSectoryCaso item in vLstRes)
        {
            RegistroSolicitudEstudioSectoryCaso vRegistroSolcictud = vReporte.ConsultarRegistroInicial(item.IdRegistroSolicitudEstudioSectoryCaso, 1).FirstOrDefault();

            vLstRegistroinicial.Add(vRegistroSolcictud);


        }


        toolBar.LipiarMensajeError();
        GenerarReporteSeguimiento(vLstRegistroinicial);

    }


    public string GenerarReporteSeguimiento(List<RegistroSolicitudEstudioSectoryCaso> pRegistroinicial)
    {
        string reportePath = Server.MapPath(@"~\General\General\Report\ConsultaSeguimientoLista.xlsx");

        string result = string.Empty;
        var fileResult = new XLWorkbook();
        var fileTemplate = new XLWorkbook(reportePath);

        IXLWorksheet pestanaInit = null;
        pestanaInit = fileTemplate.Worksheets.First();
        var pestanaBase = fileTemplate.Worksheets.First();

        string xy = string.Empty;
        string xyBase = string.Empty;
        string vFila = "10";
        string vColumna = "A";

        /////Fecha y usuario
        pestanaInit.Cell("C2").Value = GetSessionUser().NombreUsuario;
        pestanaInit.Cell("C4").Value = DateTime.Now.ToShortDateString();



        ////Cargar registro inicial
        int vVigenciaPACCO = 0;

        foreach (RegistroSolicitudEstudioSectoryCaso item in pRegistroinicial)
        {
            string vPosXY = vColumna + vFila.ToString();

            pestanaInit.Cell("A" + vFila).Value = item.IdRegistroSolicitudEstudioSectoryCaso;
            pestanaInit.Cell("B" + vFila).Value = item.ConsecutivoPACCO;

            ////Direcci�n PACCO
            if (!item.VigenciaPACCO.Equals(string.Empty))
            {
                if (vVigenciaPACCO != int.Parse(item.VigenciaPACCO))
                {
                    vVigenciaPACCO = int.Parse(item.VigenciaPACCO);
                    vManejoControlesContratos.LlenarDireccionPACCO(ddlDireccionPACCO, null, false, vVigenciaPACCO);
                }

                foreach (ListItem lItem in ddlDireccionPACCO.Items)
                {
                    if (item.DireccionsolicitantePACCO.ToString().Equals(lItem.Value))
                    {
                        pestanaInit.Cell("C" + vFila).Value = lItem.Text;
                        break;
                    }
                }
            }

           // pestanaInit.Cell("C" + vFila).Value = item.DireccionsolicitantePACCO;
            pestanaInit.Cell("D" + vFila).Value = item.ObjetoPACCO;
            pestanaInit.Cell("E" + vFila).Value = item.ValorPresupuestalPACCO.ToString();
            pestanaInit.Cell("F" + vFila).Value = item.VigenciaPACCO;
            
            ////Modalidades PACCO            
            if (!item.VigenciaPACCO.Equals(string.Empty))
            {
                //if (vVigenciaPACCO != int.Parse(item.VigenciaPACCO))
                //{
                    vVigenciaPACCO = int.Parse(item.VigenciaPACCO);
                    vManejoControlesContratos.LlenarModalidadPACCO(ddlModalidadPACCO, null, true, vVigenciaPACCO);
                //}

                foreach (ListItem lItem in ddlModalidadPACCO.Items)
                {
                    if (item.ModalidadPACCO.ToString().Equals(lItem.Value))
                    {
                        pestanaInit.Cell("G" + vFila).Value = lItem.Text;
                        break;
                    }
                }
            }

            pestanaInit.Cell("H" + vFila).Value = item.ConsecutivoEstudioRelacionado == 0 ? string.Empty : item.ConsecutivoEstudioRelacionado.ToString(); ;
            pestanaInit.Cell("I" + vFila).Value = item.FechaSolicitudInicial.ToString("dd/MM/yyyy");
            pestanaInit.Cell("J" + vFila).Value = item.ActaCorreoNoRadicado;
            pestanaInit.Cell("K" + vFila).Value = item.NombreAbreviado;
            pestanaInit.Cell("L" + vFila).Value = item.NumeroReproceso;
            pestanaInit.Cell("M" + vFila).Value = item.Objeto;
            pestanaInit.Cell("N" + vFila).Value = item.CuentaVigenciasFuturasPACCO;
            pestanaInit.Cell("O" + vFila).Value = item.AplicaProcesoSeleccion;
            pestanaInit.Cell("P" + vFila).Value = item.ModalidadSeleccion;
            pestanaInit.Cell("Q" + vFila).Value = item.TipoEstudio;
            pestanaInit.Cell("R" + vFila).Value = item.ComplejidadInterna;
            pestanaInit.Cell("S" + vFila).Value = item.ComplejidadIndicador;
            pestanaInit.Cell("T" + vFila).Value = item.ResponsableES;
            pestanaInit.Cell("U" + vFila).Value = item.ResponsableEC;
            pestanaInit.Cell("V" + vFila).Value = item.OrdenadorGasto.Equals("-1")?string.Empty: item.OrdenadorGasto;
            pestanaInit.Cell("W" + vFila).Value = item.EstadoSolicitud;
            pestanaInit.Cell("X" + vFila).Value = item.MotivoSolicitud;
            pestanaInit.Cell("Y" + vFila).Value = item.DireccionSolicitante;
            pestanaInit.Cell("Z" + vFila).Value = item.AreaSolicitante;

            pestanaInit.Cell("AA" + vFila).Value = item.TipoValor.Equals("-1") ? string.Empty : item.TipoValor;
            pestanaInit.Cell("AB" + vFila).Value = item.ValorPresupuestoEstimadoSolicitante.ToString();


            ////Cargar gestion

            List<GestionesEstudioSectorCostos> vLstGestiones = vReporte.ConsultarGestionEstudio(item.IdRegistroSolicitudEstudioSectoryCaso, 1);
            foreach (GestionesEstudioSectorCostos res in vLstGestiones)
            {
                pestanaInit.Cell("AC" + vFila).Value = res.FechaPreguntaOComentario == null ? string.Empty : res.FechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AD" + vFila).Value = res.FechaRespuesta == null ? string.Empty : res.FechaRespuesta.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AE" + vFila).Value = res.UltimaFechaPreguntaOComentario == null ? string.Empty : res.UltimaFechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AF" + vFila).Value = res.UltimaFechaRespuesta == null ? string.Empty : res.UltimaFechaRespuesta.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AG" + vFila).Value = res.FechaEntregaES_MC == null ? string.Empty : res.FechaEntregaES_MC.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AH" + vFila).Value = res.FechaEntregaES_MCDef == null ? string.Empty : res.FechaEntregaES_MCDef.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AI" + vFila).Value = res.FechaEntregaFCTORequerimiento == null ? string.Empty : res.FechaEntregaFCTORequerimiento.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AJ" + vFila).Value = res.FechaEntregaFCTORequerimientoDef == null ? string.Empty : res.FechaEntregaFCTORequerimientoDef.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AK" + vFila).Value = res.FechaEnvioSDC == null ? string.Empty : res.FechaEnvioSDC.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AL" + vFila).Value = res.FechaEnvioSDCDef == null ? string.Empty : res.FechaEnvioSDCDef.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AM" + vFila).Value = res.PlazoEntregaCotizaciones == null ? string.Empty : res.PlazoEntregaCotizaciones.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("AN" + vFila).Value = res.PlazoEntregaCotizacionesDef == null ? string.Empty : res.PlazoEntregaCotizacionesDef.Value.ToString("dd/MM/yyyy");

                ////Gestiones
                List<GestionesActividadesAdicionales> vLstActiviades = vResultadoEstudioSectorService.ConsultarGestionesActividadesAdicionales(res.IdGestionEstudio);

                string pColActividad1 = "A";
                string pColActividad2 = "O";
                string vColActual = pColActividad1 + pColActividad2;
                int count = 0;
                foreach (GestionesActividadesAdicionales actividad in vLstActiviades)
                {
                    pestanaInit.Cell(vColActual + vFila).Value = actividad.Actividad;

                    vColActual = NextPos(vColActual);
                    pestanaInit.Cell(vColActual + vFila).Value = actividad.Detalle;

                    vColActual = NextPos(vColActual);
                    pestanaInit.Cell(vColActual + vFila).Value = actividad.FechaActividadAdicional == null ? string.Empty : actividad.FechaActividadAdicional.Value.ToString("dd/MM/yyyy"); ;
                    vColActual = NextPos(vColActual);
                    count++;
                    if (count > 5)
                    {
                        break;
                    }
                }

            }

            ////Resultados

            List<ResultadoEstudioSector> vLstResultado = vReporte.ConsultarResultado(item.IdRegistroSolicitudEstudioSectoryCaso, 1);
            foreach (ResultadoEstudioSector res in vLstResultado)
            {
                pestanaInit.Cell("BG" + vFila).Value = res.Consultados;
                pestanaInit.Cell("BH" + vFila).Value = res.Participantes;
                pestanaInit.Cell("BI" + vFila).Value = res.CotizacionesRecibidas;
                pestanaInit.Cell("BJ" + vFila).Value = res.CotizacionesParaPresupuesto;
                pestanaInit.Cell("BK" + vFila).Value = res.RadicadoOficioEntregaESyC;
                pestanaInit.Cell("BL" + vFila).Value = res.FechaDeEntregaESyC == null ? string.Empty : res.FechaDeEntregaESyC.Value.ToString("dd/MM/yyyy");
                pestanaInit.Cell("BM" + vFila).Value = res.TipoDeValor;
                pestanaInit.Cell("BN" + vFila).Value = res.ValorPresupuestoESyC.ToString();

                pestanaInit.Cell("BO" + vFila).Value = res.IndiceLiquidez.Equals(string.Empty) ? string.Empty : Constantes.MayorOIgual;
                pestanaInit.Cell("BP" + vFila).Value = res.IndiceLiquidez;
                pestanaInit.Cell("BQ" + vFila).Value = res.NivelDeEndeudamiento.Equals(string.Empty) ? string.Empty : Constantes.MenorOIgual;
                pestanaInit.Cell("BR" + vFila).Value = res.NivelDeEndeudamiento;
                pestanaInit.Cell("BS" + vFila).Value = res.RazonDeCoberturaDeIntereses.Equals(string.Empty) ? string.Empty : Constantes.MayorOIgual;
                pestanaInit.Cell("BT" + vFila).Value = res.RazonDeCoberturaDeIntereses;
                pestanaInit.Cell("BU" + vFila).Value = res.CapitalDeTrabajo.Equals(string.Empty) ? string.Empty : Constantes.MayorOIgual;
                pestanaInit.Cell("BV" + vFila).Value = res.CapitalDeTrabajo;
                pestanaInit.Cell("BW" + vFila).Value = res.RentabilidadDelPatrimonio.Equals(string.Empty) ? string.Empty : Constantes.MayorOIgual;
                pestanaInit.Cell("BX" + vFila).Value = res.RentabilidadDelPatrimonio;
                pestanaInit.Cell("BY" + vFila).Value = res.RentabilidadDelActivo.Equals(string.Empty) ? string.Empty : Constantes.MayorOIgual;
                pestanaInit.Cell("BZ" + vFila).Value = res.RentabilidadDelActivo;
                pestanaInit.Cell("CA" + vFila).Value = res.TotalPatrimonio.Equals(string.Empty) ? string.Empty : Constantes.MayorOIgual;
                pestanaInit.Cell("CB" + vFila).Value = res.TotalPatrimonio;
                pestanaInit.Cell("CC" + vFila).Value = res.KResidual;

                pestanaInit.Cell("CM" + vFila).Value = res.DocumentosRevisadosNAS;
                pestanaInit.Cell("CN" + vFila).Value = res.Fecha == null ? string.Empty : res.Fecha.Value.ToString("dd/MM/yyyy"); ;
                pestanaInit.Cell("CO" + vFila).Value = res.RevisadoPor;
                pestanaInit.Cell("CP" + vFila).Value = res.Observacion;

                ////Indicadores adicionales

                List<IndicadorAdicionalResultado> vLstIndicadores = vResultadoEstudioSectorService.ConsultarIndicadorResultado(res.IdConsecutivoResultadoEstudio);
                string pColActividad1 = "C";
                string pColActividad2 = "D";
                string vColActual = pColActividad1 + pColActividad2;
                int count = 0;
                foreach (IndicadorAdicionalResultado ind in vLstIndicadores)
                {
                    pestanaInit.Cell(vColActual + vFila).Value = ind.Indicador;
                    vColActual = NextPos(vColActual);
                    pestanaInit.Cell(vColActual + vFila).Value = ind.Condicion;
                    vColActual = NextPos(vColActual);
                    pestanaInit.Cell(vColActual + vFila).Value = ind.Valor;
                    vColActual = NextPos(vColActual);
                    count++;
                    if (count > 2)
                    {
                        break;
                    }
                }
            }

            ////Tiempos

            List<TiempoEntreActividadesConsulta> vLstTiempos = vReporte.ConsultarTiempos(item.IdRegistroSolicitudEstudioSectoryCaso, 1);
            foreach (TiempoEntreActividadesConsulta t in vLstTiempos)
            {
                pestanaInit.Cell("CQ" + vFila).Value = t.FentregaES_ECTiemposEquipo != null ? t.FentregaES_ECTiemposEquipo.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("CR" + vFila).Value = t.FEentregaES_ECTiemposIndicador != null ? t.FEentregaES_ECTiemposIndicador.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("CS" + vFila).Value = t.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado != null ? t.DiasHabilesFCTInicialYFinalParaFCTYES_MCRevisado : string.Empty;
                pestanaInit.Cell("CT" + vFila).Value = t.DiasHabilesEntreFCTFinalYSDC != null ? t.DiasHabilesEntreFCTFinalYSDC : string.Empty;
                pestanaInit.Cell("CU" + vFila).Value = t.DiasHabilesEnESOCOSTEO != null ? t.DiasHabilesEnESOCOSTEO : string.Empty;
                pestanaInit.Cell("CV" + vFila).Value = t.DiashabilesParaDevolucion != null ? t.DiashabilesParaDevolucion : string.Empty;
                pestanaInit.Cell("CW" + vFila).Value = t.FEstimadaFCTPreliminarREGINICIAL != null ? t.FEstimadaFCTPreliminarREGINICIAL.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("CX" + vFila).Value = t.FEstimadaFCTPreliminar != null ? t.FEstimadaFCTPreliminar.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("CY" + vFila).Value = t.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado != null ? t.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCrevisado.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("CZ" + vFila).Value = t.FEestimadaES_EC != null ? t.FEestimadaES_EC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DA" + vFila).Value = t.FEstimadaDocsRadicadosEnContratos != null ? t.FEstimadaDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DB" + vFila).Value = t.FEstimadaComiteContratacion != null ? t.FEstimadaComiteContratacion.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DC" + vFila).Value = t.FEstimadaInicioDelPS != null ? t.FEstimadaInicioDelPS.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DD" + vFila).Value = t.FEstimadaPresentacionPropuestas != null ? t.FEstimadaPresentacionPropuestas.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DE" + vFila).Value = t.FEstimadaDeInicioDeEjecucion != null ? t.FEstimadaDeInicioDeEjecucion.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DF" + vFila).Value = t.FRealFCTPreliminarESC != null ? t.FRealFCTPreliminarESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DG" + vFila).Value = t.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC != null ? t.FEstimadaFCTDefinitivaParaMCFEstimadaES_MCRevisadoESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DH" + vFila).Value = t.FEstimadaES_EC_ESC != null ? t.FEstimadaES_EC_ESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DI" + vFila).Value = t.FEstimadaDocsRadicadosEnContratosESC != null ? t.FEstimadaDocsRadicadosEnContratosESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DJ" + vFila).Value = t.FEstimadaComitecontratacionESC != null ? t.FEstimadaComitecontratacionESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DK" + vFila).Value = t.FEstimadaInicioDelPS_ESC != null ? t.FEstimadaInicioDelPS_ESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DL" + vFila).Value = t.FEstimadaPresentacionPropuestasESC != null ? t.FEstimadaPresentacionPropuestasESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DM" + vFila).Value = t.FEstimadaDeInicioDeEjecucionESC != null ? t.FEstimadaDeInicioDeEjecucionESC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DN" + vFila).Value = t.FRealFCTPreliminar != null ? t.FRealFCTPreliminar.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DO" + vFila).Value = t.FRealDocsRadicadosEnContratos != null ? t.FRealDocsRadicadosEnContratos.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DP" + vFila).Value = t.FRealFCTDefinitivaParaMCFRealES_MCRevisado != null ? t.FRealFCTDefinitivaParaMCFRealES_MCRevisado.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DQ" + vFila).Value = t.FRealComiteContratacion != null ? t.FRealComiteContratacion.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DR" + vFila).Value = t.FRealES_EC != null ? t.FRealES_EC.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DS" + vFila).Value = t.FRealInicioPS != null ? t.FRealInicioPS.Substring(0, 10) : string.Empty;
                pestanaInit.Cell("DT" + vFila).Value = t.FRealPresentacionPropuestas != null ? t.FRealPresentacionPropuestas.Substring(0, 10) : string.Empty;
            }

            ////B�tacora acciones

            string pColBitacora1 = "D";
            string pColBitacora2 = "U";
            string vColActualB = pColBitacora1 + pColBitacora2;

            List<BitacoraAcciones> vLstBitacora = vReporte.ConsultarBitacoraAccion(item.IdRegistroSolicitudEstudioSectoryCaso, 25);
            foreach (BitacoraAcciones bitacora in vLstBitacora)
            {
                pestanaInit.Cell(vColActualB + vFila).Value = bitacora.UltimaAccion;
                vColActualB = NextPos(vColActualB);
                pestanaInit.Cell(vColActualB + vFila).Value = bitacora.ProximaAccion;
                vColActualB = NextPos(vColActualB);

            }

            ////Proceso selecci�n

            List<InformacionProcesoSeleccion> vLstInformacion = vReporte.ConsultarProcesoSeleccion(item.IdRegistroSolicitudEstudioSectoryCaso, 1);
            foreach (InformacionProcesoSeleccion info in vLstInformacion)
            {
                pestanaInit.Cell("FS" + vFila).Value = info.NumeroDelProceso;
                pestanaInit.Cell("FT" + vFila).Value = info.ObjetoContrato;
                pestanaInit.Cell("FU" + vFila).Value = info.EstadoDelProceso;
                pestanaInit.Cell("FV" + vFila).Value = info.FechaAdjudicacionDelProceso == null ? string.Empty : info.FechaAdjudicacionDelProceso.Value.ToString("dd/MM/yyyy"); ;
                pestanaInit.Cell("FW" + vFila).Value = info.Contratista;
                pestanaInit.Cell("FX" + vFila).Value = info.ValorPresupuestadoInicial.ToString();
                pestanaInit.Cell("FY" + vFila).Value = info.ValorAdjudicado.ToString();
                pestanaInit.Cell("FZ" + vFila).Value = info.PlazoDeEjecucion;
                pestanaInit.Cell("GA" + vFila).Value = info.Observacion;
                pestanaInit.Cell("GB" + vFila).Value = info.URLDelProceso;
            }

            ////Cierres

            List<CierreEstudiosPorVigencia> vLstCierres = vReporte.ConsultarCierresEstudio(item.IdRegistroSolicitudEstudioSectoryCaso, 1);
            foreach (CierreEstudiosPorVigencia cierre in vLstCierres)
            {
                pestanaInit.Cell("GC" + vFila).Value = cierre.AnioCierre;
                pestanaInit.Cell("GD" + vFila).Value = cierre.FechaEjecucion == null ? string.Empty : cierre.FechaEjecucion.ToString("dd/MM/yyyy"); ;
                pestanaInit.Cell("GE" + vFila).Value = cierre.Descripcion;
            }

            vFila = (int.Parse(vFila) + 1).ToString();
        }





        if (pestanaInit != null)
        {
            fileResult.AddWorksheet(pestanaInit);
        }


        string mes = DateTime.Now.Month.ToString();
        if (DateTime.Now.Month < 10)
        {
            mes = "0" + mes;
        }
        var id = "ConsultaSeguimiento_" + DateTime.Now.Day + "" + mes + "" + DateTime.Now.Year + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ".xlsx";
        var pathResult = HttpContext.Current.Server.MapPath(@"~\Seguimientos");// System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"];



        fileResult.SaveAs(Path.Combine(pathResult, id));
        result = id;

        if (result != string.Empty)
        {
            string archivoLocal = Path.Combine(Server.MapPath(@"~\Seguimientos"), result);
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(archivoLocal);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=ConsultaSeguimiento_" + DateTime.Now.ToString("ddMMyyyy") + ".xls");
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(archivoLocal);
            Response.Flush();
            LimpiarCarpetaArchivos(id, pathResult);
            Response.Close();

        }


        return result;
    }

    public string NextPos(string pPosCol)
    {
        char[] vCol = pPosCol.ToCharArray();

        char vCol1 = vCol.Length > 0 ? vCol[0] : ' ';
        char vCol2 = vCol.Length > 1 ? vCol[1] : ' ';

        int vAscii1 = System.Text.Encoding.ASCII.GetBytes(vCol1.ToString())[0];
        int vAscii2 = System.Text.Encoding.ASCII.GetBytes(vCol2.ToString())[0];

        if (vCol2.Equals('Z'))
        {
            vAscii1 = (System.Text.Encoding.ASCII.GetBytes(vCol1.ToString())[0]) + 1;
            vAscii2 = System.Text.Encoding.ASCII.GetBytes("A")[0];
        }
        else
        {
            vAscii2 = vAscii2 + 1;
        }

        string vNextPos = Char.ToString(((char)vAscii1)) + Char.ToString(((char)vAscii2));

        return vNextPos;
    }

    public void LimpiarCarpetaArchivos(string pNombreArchivo, string pRuta)
    {
        if (File.Exists(Path.Combine(pRuta, pNombreArchivo)))
        {
            try
            {
                File.Delete(Path.Combine(pRuta, pNombreArchivo));
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError("Error eliminando el archivo " + ex.Message);
            }
        }

    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState[Constantes.ViewStateSortDirection] == null)
            {
                ViewState[Constantes.ViewStateSortDirection] = SortDirection.Ascending;
            }


            return (SortDirection)ViewState[Constantes.ViewStateSortDirection];
        }
        set { ViewState[Constantes.ViewStateSortDirection] = value; }
    }

    protected void btnLupa_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();
        LLenarGrilla(Buscar());
    }
}
