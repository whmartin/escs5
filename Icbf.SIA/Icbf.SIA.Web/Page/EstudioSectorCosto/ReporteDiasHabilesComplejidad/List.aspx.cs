using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;


public partial class Page_Informe_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ReporteDiasHabilesComplejidad";
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">the page</param>
    /// <param name="e">thePreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        ddlVigencia.Focus();
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                ViewState["Tipoestudio"] = 0;
                CargarDatosIniciales();
            }
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        LimpiarCampos();
    }

    private void LimpiarCampos()
    {
        toolBar = (masterPrincipal)Master;
        toolBar.LipiarMensajeError();
        LimpiarControles(pnlConsulta.Controls);
    }

    /// <summary>
    /// Evento para generar consulta generica
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void btnReporte_Click(object sender, EventArgs e)
    {
        toolBar = (masterPrincipal)Master;

        try
        {
            string titulo = "Reporte de D&iacute;as h&aacute;biles por complejidad";
            string nombreReporte = "ReporteDiasHabComplejidad";
            //Crea un objeto de tipo reporte

            string vigencia = ddlVigencia.SelectedValue;
            string tipoEstudio = CargarSeleccion(ddlTiposEstudio);
            var showExportControls = true;

            var objReport = new Report(nombreReporte, showExportControls, PageName, titulo);

            objReport.AddParameter("Vigencia", vigencia);
            objReport.AddParameter("tipoestudio", tipoEstudio);
            objReport.AddParameter("Usuario", GetSessionUser().NombreUsuario);

            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los datos iniciales de la p�gina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)Master;
            toolBar.eventoReporte += btnReporte_Click;

            toolBar.EstablecerTitulos(@"Reporte de D&iacute;as h&aacute;biles por complejidad", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para obtener la sesi�n e inicializar controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Informe.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Informe.Eliminado");

            var lstRegional = vResultadoEstudioSectorService.ConsultarVigencia();

            Utilidades.LlenarDropDownList(ddlVigencia, lstRegional);

            llenarTiposEstudio();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para agregar cargar ListBox de Tipos de Estudios
    /// </summary>
    public void llenarTiposEstudio()
    {
        EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

        List<TiposEstudio> vTiposEstudios = vEstudioSectorCostoService.ConsultarTiposEstudios(null, null, 1);

        ListItem ls = new ListItem();
        ls = new ListItem();
        ls.Text = "TODAS";
        ls.Value = "0";

        ddlTiposEstudio.Items.Clear();
        if (vTiposEstudios.Count() > 1)
        {
            LoadCheckTiposEstudios(vTiposEstudios);
        }

        ddlTiposEstudio.Items.Insert(0, ls);
    }

    /// <summary>
    /// M�todo para agregar Tipos de Estudios
    /// </summary>
    /// <param name="lista">lista de  Tipos de Estudios</param>
    public void LoadCheckTiposEstudios(List<TiposEstudio> lista)
    {
        int var;
        ListItem ls = new ListItem();
        //ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_ddlClaseActividad');");

        foreach (TiposEstudio pci in lista.OrderBy(t => t.Descripcion))
        {
            ls = new ListItem();
            ls.Text = pci.Nombre;
            var = pci.IdTipoEstudio;
            ls.Value = var.ToString();
            ddlTiposEstudio.Items.Add(ls);
        }
    }

    /// <summary>
    /// Evento para seleccionar todos los tipos de estudio de inter�s
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void CheckBoxTodosTiposEstudio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTiposEstudio.Items[0].Selected)
        {
            SelecionarTodos(this.ddlTiposEstudio);
            ViewState["Tipoestudio"] = 1;
        }
        else
        {
            if (ViewState["Tipoestudio"].ToString() == "1")
            {
                SelecionarNinguno(this.ddlTiposEstudio);
                ViewState["Tipoestudio"] = 0;
            }
        }
    }

    /// <summary>
    /// M�todo que selecciona todas el checkBoxList recibido
    /// </summary>
    public void SelecionarTodos(CheckBoxList ddlList)
    {
        for (int i = 1; ddlList.Items.Count > i; i++)
        {
            ddlList.Items[i].Selected = true;
        }
    }

    // M�todo que limpiar el checkBoxList seleccionadas
    /// </summary>
    public void SelecionarNinguno(CheckBoxList ddlList)
    {
        for (int i = 1; ddlList.Items.Count > i; i++)
        {
            ddlList.Items[i].Selected = false;
        }
    }

    /// <summary>
    /// M�todo que carga las regionales seleccionadas para consulta
    /// </summary>
    public string CargarSeleccion(CheckBoxList list)
    {
        string vCadena = "";

        for (int i = 0; list.Items.Count > i; i++)
        {
            if (list.Items[i].Selected)
            {
                if (vCadena.Equals(""))
                {
                    vCadena = list.Items[i].Value.ToString();
                }
                else
                {
                    vCadena = vCadena + "," + list.Items[i].Value.ToString();
                }
            }
        }
        return vCadena;
    }
}