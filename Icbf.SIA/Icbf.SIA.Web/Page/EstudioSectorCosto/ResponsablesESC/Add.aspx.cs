using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_ResponsablesESC_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/ResponsablesESC";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtResponsable.Focus();
            if (!Page.IsPostBack)
            {
                HfValidacion.Value = validarDuplicidad();
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            ResponsablesESC vResponsablesESC = new ResponsablesESC();

            vResponsablesESC.Responsable = Convert.ToString(txtResponsable.Text).ToUpper();
            vResponsablesESC.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));

            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vResponsablesESC.IdResponsable = Convert.ToInt32(hfIdResponsable.Value);
                    vResponsablesESC.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vResponsablesESC, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarResponsablesESC(vResponsablesESC);
                }
                else
                {
                    vResponsablesESC.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vResponsablesESC, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarResponsablesESC(vResponsablesESC);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("ResponsablesESC.IdResponsable", vResponsablesESC.IdResponsable);
                    SetSessionParameter("ResponsablesESC.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Responsables", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdResponsable = Convert.ToInt32(GetSessionParameter("ResponsablesESC.IdResponsable"));
            String NombreResponsable = vEstudioSectorCostoService.ConsultarResponsablesESC(vIdResponsable).Responsable;
            hfNombreResponsable.Value = NombreResponsable;
            RemoveSessionParameter("ResponsablesESC.Id");

            ResponsablesESC vResponsablesESC = new ResponsablesESC();
            vResponsablesESC = vEstudioSectorCostoService.ConsultarResponsablesESC(vIdResponsable);
            hfIdResponsable.Value = vResponsablesESC.IdResponsable.ToString();
            txtResponsable.Text = vResponsablesESC.Responsable;
            rblEstado.SelectedValue = Convert.ToBoolean(vResponsablesESC.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vResponsablesESC.UsuarioCrea, vResponsablesESC.FechaCrea, vResponsablesESC.UsuarioModifica, vResponsablesESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void txtResponsable_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtResponsable.Text.ToUpper().Equals(hfNombreResponsable.Value))
        {
            if (vEstudioSectorCostoService.ValidarResponsablesESC(txtResponsable.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
}
