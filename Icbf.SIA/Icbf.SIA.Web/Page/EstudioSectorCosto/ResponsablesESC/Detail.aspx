<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ResponsablesESC_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdResponsable" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Responsable *
            </td>
            <td>Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtResponsable" Enabled="false" Width="90%" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
