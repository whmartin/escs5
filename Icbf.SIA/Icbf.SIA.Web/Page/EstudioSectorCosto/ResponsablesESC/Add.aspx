<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ResponsablesESC_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdResponsable" runat="server" />
    <asp:HiddenField ID="HfValidacion" runat="server" />
    <asp:HiddenField ID="hfNombreResponsable" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Responsable *
                <asp:RequiredFieldValidator runat="server" ID="rfvResponsable" ControlToValidate="txtResponsable"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtResponsable" Width="90%" MaxLength="50" AutoPostBack="true" OnTextChanged="txtResponsable_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftResponsable" runat="server" TargetControlID="txtResponsable"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
