using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_RangoDiasComplejidadPorModalidad_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/RangoDiasComplejidadPorModalidad";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("RangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad", hfIdRangosDiasComplejidadPorModalidad.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdRangosDiasComplejidadPorModalidad = Convert.ToInt32(GetSessionParameter("RangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad"));
            RemoveSessionParameter("RangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad");

            if (GetSessionParameter("RangoDiasComplejidadPorModalidad.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("RangoDiasComplejidadPorModalidad.Guardado");
            String limiteAMostar = string.Empty;

            RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();
            vRangoDiasComplejidadPorModalidad = vEstudioSectorCostoService.ConsultarRangoDiasComplejidadPorModalidad(vIdRangosDiasComplejidadPorModalidad);
            hfIdRangosDiasComplejidadPorModalidad.Value = vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad.ToString();
            ddlIdModalidadSeleccion.SelectedValue = vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion.ToString();
            ddlOperador.SelectedValue = vRangoDiasComplejidadPorModalidad.Operador;
            txtLimite.Text = vRangoDiasComplejidadPorModalidad.Limite == null ? string.Empty : vRangoDiasComplejidadPorModalidad.Limite.ToString();
            ddlIdComplejidadInterna.SelectedValue = vRangoDiasComplejidadPorModalidad.IdComplejidadInterna.ToString();
            ddlIdComplejidadIndicador.SelectedValue = vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador.ToString();
            txtDiasHabilesInternos.Text = vRangoDiasComplejidadPorModalidad.DiasHabilesInternos.ToString();
            txtDiasHabilesCumplimientoIndicador.Text = vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador.ToString();
            rblEstado.SelectedValue = Convert.ToBoolean(vRangoDiasComplejidadPorModalidad.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdRangosDiasComplejidadPorModalidad.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRangoDiasComplejidadPorModalidad.UsuarioCrea, vRangoDiasComplejidadPorModalidad.FechaCrea, vRangoDiasComplejidadPorModalidad.UsuarioModifica, vRangoDiasComplejidadPorModalidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdRangosDiasComplejidadPorModalidad = Convert.ToInt32(hfIdRangosDiasComplejidadPorModalidad.Value);

            RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();
            vRangoDiasComplejidadPorModalidad = vEstudioSectorCostoService.ConsultarRangoDiasComplejidadPorModalidad(vIdRangosDiasComplejidadPorModalidad);
            InformacionAudioria(vRangoDiasComplejidadPorModalidad, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarRangoDiasComplejidadPorModalidad(vRangoDiasComplejidadPorModalidad);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("RangoDiasComplejidadPorModalidad.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de l&iacute;mites y d&iacute;as por modalidad", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            /*llenado de combos*/
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadInterna(ddlIdComplejidadInterna, null, false);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, false);

            ddlIdComplejidadIndicador.Items.Insert(0, new ListItem("No especificado", "0"));
            ddlIdComplejidadIndicador.SelectedIndex = 0;
            ddlIdComplejidadInterna.Items.Insert(0, new ListItem("No especificado", "0"));
            ddlIdComplejidadInterna.SelectedIndex = 0;

            ddlOperador.Items.Insert(0, new ListItem("No especificado", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
