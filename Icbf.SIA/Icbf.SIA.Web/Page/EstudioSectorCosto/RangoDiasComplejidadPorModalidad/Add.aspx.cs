using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_RangoDiasComplejidadPorModalidad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/RangoDiasComplejidadPorModalidad";
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            ddlIdModalidadSeleccion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();

            vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            vRangoDiasComplejidadPorModalidad.Operador = Convert.ToString(ddlOperador.SelectedValue);
            vRangoDiasComplejidadPorModalidad.Limite = txtLimite.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtLimite.Text);
            vRangoDiasComplejidadPorModalidad.IdComplejidadInterna = Convert.ToInt32(ddlIdComplejidadInterna.SelectedValue);
            vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador = Convert.ToInt32(ddlIdComplejidadIndicador.SelectedValue);
            vRangoDiasComplejidadPorModalidad.DiasHabilesInternos = txtDiasHabilesInternos.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesInternos.Text);
            vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador = txtDiasHabilesCumplimientoIndicador.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesCumplimientoIndicador.Text);
            vRangoDiasComplejidadPorModalidad.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            if (vEstudioSectorCostoService.ValidarRangoDiasComplejidadPorModalidad(vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion
                                                                                        , vRangoDiasComplejidadPorModalidad.Operador
                                                                                        , vRangoDiasComplejidadPorModalidad.Limite
                                                                                        , vRangoDiasComplejidadPorModalidad.IdComplejidadInterna
                                                                                        , vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador
                                                                                        , vRangoDiasComplejidadPorModalidad.DiasHabilesInternos
                                                                                        , vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador))
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad = Convert.ToInt32(hfIdRangosDiasComplejidadPorModalidad.Value);
                    vRangoDiasComplejidadPorModalidad.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vRangoDiasComplejidadPorModalidad, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarRangoDiasComplejidadPorModalidad(vRangoDiasComplejidadPorModalidad);
                }
                else
                {
                    vRangoDiasComplejidadPorModalidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vRangoDiasComplejidadPorModalidad, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarRangoDiasComplejidadPorModalidad(vRangoDiasComplejidadPorModalidad);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("RangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad", vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad);
                    SetSessionParameter("RangoDiasComplejidadPorModalidad.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe.");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de l&iacute;mites y d&iacute;as por modalidad", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdRangosDiasComplejidadPorModalidad = Convert.ToInt32(GetSessionParameter("RangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad"));
            RemoveSessionParameter("RangoDiasComplejidadPorModalidad.Id");

            RangoDiasComplejidadPorModalidad vRangoDiasComplejidadPorModalidad = new RangoDiasComplejidadPorModalidad();
            vRangoDiasComplejidadPorModalidad = vEstudioSectorCostoService.ConsultarRangoDiasComplejidadPorModalidad(vIdRangosDiasComplejidadPorModalidad);
            hfIdRangosDiasComplejidadPorModalidad.Value = vRangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad.ToString();
            ddlIdModalidadSeleccion.SelectedValue = vRangoDiasComplejidadPorModalidad.IdModalidadSeleccion.ToString();
            ddlOperador.SelectedValue = vRangoDiasComplejidadPorModalidad.Operador;
            txtLimite.Text = vRangoDiasComplejidadPorModalidad.Limite.ToString();
            ddlIdComplejidadInterna.SelectedValue = vRangoDiasComplejidadPorModalidad.IdComplejidadInterna.ToString();
            ddlIdComplejidadIndicador.SelectedValue = vRangoDiasComplejidadPorModalidad.IdComplejidadIndicador.ToString();
            txtDiasHabilesInternos.Text = vRangoDiasComplejidadPorModalidad.DiasHabilesInternos.ToString();
            txtDiasHabilesCumplimientoIndicador.Text = vRangoDiasComplejidadPorModalidad.DiasHabilesCumplimientoIndicador.ToString();
            rblEstado.SelectedValue = Convert.ToBoolean(vRangoDiasComplejidadPorModalidad.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRangoDiasComplejidadPorModalidad.UsuarioCrea, vRangoDiasComplejidadPorModalidad.FechaCrea, vRangoDiasComplejidadPorModalidad.UsuarioModifica, vRangoDiasComplejidadPorModalidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;

            /*llenado de combos*/
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadInterna(ddlIdComplejidadInterna, null, false);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, false);

            ddlIdComplejidadIndicador.Items.Insert(0, new ListItem("Seleccione", "0"));
            ddlIdComplejidadIndicador.SelectedIndex = 0;
            ddlIdComplejidadInterna.Items.Insert(0, new ListItem("Seleccione", "0"));
            ddlIdComplejidadInterna.SelectedIndex = 0;

            ddlOperador.Items.Insert(0, new ListItem("Seleccione", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
