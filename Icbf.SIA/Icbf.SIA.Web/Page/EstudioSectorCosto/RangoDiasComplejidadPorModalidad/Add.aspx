<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RangoDiasComplejidadPorModalidad_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdRangosDiasComplejidadPorModalidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Modalidad de contrataci&oacute;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdModalidadSeleccion" ControlToValidate="ddlIdModalidadSeleccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdModalidadSeleccion" ControlToValidate="ddlIdModalidadSeleccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>Operador
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlOperador">
                    <asp:ListItem Value="<"><</asp:ListItem>
                    <asp:ListItem Value=">">></asp:ListItem>
                    <asp:ListItem Value="=">=</asp:ListItem>
                    <asp:ListItem Value="<="><=</asp:ListItem>
                    <asp:ListItem Value=">=">>=</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>L&iacute;mite en cantidad de SMMLV 
            </td>
            <td>Complejidad Interna
                <asp:RequiredFieldValidator runat="server" ID="rvIdComplejidadInterna" ControlToValidate="ddlIdComplejidadInterna"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" InitialValue="-1"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdComplejidadInterna" ControlToValidate="ddlIdComplejidadInterna"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtLimite" MaxLength="6"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftLimite" runat="server" TargetControlID="txtLimite"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadInterna"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Complejidad Indicador
                 <asp:RequiredFieldValidator runat="server" ID="rvIdComplejidadIndicador" ControlToValidate="ddlIdComplejidadIndicador"
                     SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" InitialValue="-1"
                     ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdComplejidadIndicador" ControlToValidate="ddlIdComplejidadIndicador"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>D&iacute;as H&aacute;biles Internos
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadIndicador"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesInternos" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesInternos" runat="server" TargetControlID="txtDiasHabilesInternos"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>D&iacute;as H&aacute;biles Cumplimiento Indicador
            </td>
            <td>Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesCumplimientoIndicador" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesCumplimientoIndicador" runat="server" TargetControlID="txtDiasHabilesCumplimientoIndicador"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
