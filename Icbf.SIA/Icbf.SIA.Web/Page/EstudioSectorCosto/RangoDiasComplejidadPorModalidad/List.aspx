<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RangoDiasComplejidadPorModalidad_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Modalidad de contrataci&oacute;n 
                </td>
                <td>Operador 
                 <asp:CustomValidator runat="server" ID="CustomValidator" ControlToValidate="txtLimite" SetFocusOnError="true"
                     ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar" ForeColor="Red" OnServerValidate="CustomValidator_ServerValidate" />

                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList  runat="server" ID="ddlIdModalidadSeleccion"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList  runat="server" ID="ddlOperador" ValidationGroup="btnBuscar" >
                        <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                        <asp:ListItem Value="<"><</asp:ListItem>
                        <asp:ListItem Value=">">></asp:ListItem>
                        <asp:ListItem Value="=">=</asp:ListItem>
                        <asp:ListItem Value="<="><=</asp:ListItem>
                        <asp:ListItem Value=">=">>=</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>L&iacute;mite en cantidad de SMMLV
                 <asp:CustomValidator runat="server" ID="CustomValidator1" ControlToValidate="ddlOperador" SetFocusOnError="true"
                     ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar" ForeColor="Red" OnServerValidate="CustomValidator1_ServerValidate" />

                </td>
                <td>Complejidad interna 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox  runat="server" ID="txtLimite" MaxLength="6" ValidationGroup="btnBuscar"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftLimite" runat="server" TargetControlID="txtLimite"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td>
                    <asp:DropDownList   runat="server" ID="ddlIdComplejidadInterna" ></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Complejidad indicador 
                </td>
                <td>D&iacute;as h&aacute;biles internos 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList  runat="server" ID="ddlIdComplejidadIndicador"></asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesInternos" MaxLength="3" ></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDiasHabilesInternos"
                        FilterType="Numbers" ValidChars="" />
                </td>
            </tr>
            <tr class="rowB">
                <td>D&iacute;as h&aacute;biles cumplimiento del indicador 
                </td>
                <td>Estado 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server"  ID="txtDiasHabilesCumplimientoIndicador" MaxLength="3" ></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtDiasHabilesCumplimientoIndicador"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td align="left">
                    <asp:RadioButtonList runat="server"  ID="rblEstado" RepeatDirection="Horizontal" ></asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRangoDiasComplejidadPorModalidad" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRangosDiasComplejidadPorModalidad" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRangoDiasComplejidadPorModalidad_PageIndexChanging" OnSelectedIndexChanged="gvRangoDiasComplejidadPorModalidad_SelectedIndexChanged" EmptyDataText="No se encontraron resultados, verifique por favor" OnSorting="gvRangoDiasComplejidadPorModalidad_Sorting">

                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Modalidad de contrataci&oacute;n" DataField="Modalidad" SortExpression="Modalidad" />
                            <asp:BoundField HeaderText="L&iacute;mite" DataField="LimiteGrilla" SortExpression="LimiteGrilla" />
                            <asp:BoundField HeaderText="Complejidad interna" DataField="NombreComplejidadInterna" SortExpression="NombreComplejidadInterna" />
                            <asp:BoundField HeaderText="Complejidad indicador" DataField="NombreComplejidadIndicador" SortExpression="NombreComplejidadIndicador" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles internos" DataField="DiasHabilesInternos" SortExpression="DiasHabilesInternos" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles cumplimiento del indicador" DataField="DiasHabilesCumplimientoIndicador" SortExpression="DiasHabilesCumplimientoIndicador" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />
                            <asp:BoundField HeaderText="Operador" DataField="Operador" Visible="false" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
