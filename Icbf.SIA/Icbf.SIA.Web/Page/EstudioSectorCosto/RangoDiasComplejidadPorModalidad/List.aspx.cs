using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_RangoDiasComplejidadPorModalidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/RangoDiasComplejidadPorModalidad";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;
    int vColumnaOperador = 8;
    Boolean SwLimiteOperador = true;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            ddlIdModalidadSeleccion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {

        if (txtLimite.Text == "" && ddlOperador.SelectedValue == "-1")
        {
            args.IsValid = true;
        }
        else if (txtLimite.Text == "")
        {
            gvRangoDiasComplejidadPorModalidad.Visible = false;
            args.IsValid = false;
            //Validaci�n para la b�squea
            SwLimiteOperador = false;
        }
    }
    protected void CustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtLimite.Text == "" && ddlOperador.SelectedValue == "-1")
        {
            args.IsValid = true;
        }
        else if (ddlOperador.SelectedValue == "-1")
        {
            gvRangoDiasComplejidadPorModalidad.Visible = false;
            args.IsValid = false;
            //Validaci�n para la b�squea
            SwLimiteOperador = false;
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (SwLimiteOperador)
        {
            LlenarGrilla();
            MostrarColumnasExportacionExcel(false);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<RangoDiasComplejidadPorModalidad> Buscar()
    {
        try
        {
            int? vIdModalidadSeleccion = null;
            String vOperador = null;
            int? vLimite = null;
            int? vIdComplejidadInterna = null;
            int? vIdComplejidadIndicador = null;
            int? vDiasHabilesInternos = null;
            int? vDiasHabilesCumplimientoIndicador = null;
            int? vEstado = null;
            if (ddlIdModalidadSeleccion.SelectedValue != "-1")
            {
                vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            }
            if (ddlOperador.SelectedValue != "-1")
            {
                vOperador = Convert.ToString(ddlOperador.SelectedValue);
            }
            if (txtLimite.Text != "")
            {
                vLimite = Convert.ToInt32(txtLimite.Text);
            }
            if (ddlIdComplejidadInterna.SelectedValue != "-1")
            {
                vIdComplejidadInterna = Convert.ToInt32(ddlIdComplejidadInterna.SelectedValue);
            }
            if (ddlIdComplejidadIndicador.SelectedValue != "-1")
            {
                vIdComplejidadIndicador = Convert.ToInt32(ddlIdComplejidadIndicador.SelectedValue);
            }
            if (txtDiasHabilesInternos.Text != "")
            {
                vDiasHabilesInternos = Convert.ToInt32(txtDiasHabilesInternos.Text);
            }
            if (txtDiasHabilesCumplimientoIndicador.Text != "")
            {
                vDiasHabilesCumplimientoIndicador = Convert.ToInt32(txtDiasHabilesCumplimientoIndicador.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }
            List<RangoDiasComplejidadPorModalidad> ListaRangoDiasComplejidadPorModalidad = vEstudioSectorCostoService.ConsultarRangoDiasComplejidadPorModalidads(vIdModalidadSeleccion, vOperador, vLimite, vIdComplejidadInterna, vIdComplejidadIndicador, vDiasHabilesInternos, vDiasHabilesCumplimientoIndicador, vEstado);
            return ListaRangoDiasComplejidadPorModalidad;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvRangoDiasComplejidadPorModalidad.PageSize = PageSize();
            gvRangoDiasComplejidadPorModalidad.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de l&iacute;mites y d&iacute;as por modalidad", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRangoDiasComplejidadPorModalidad.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("RangoDiasComplejidadPorModalidad.IdRangosDiasComplejidadPorModalidad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRangoDiasComplejidadPorModalidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRangoDiasComplejidadPorModalidad.SelectedRow);
    }
    protected void gvRangoDiasComplejidadPorModalidad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRangoDiasComplejidadPorModalidad.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("RangoDiasComplejidadPorModalidad.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("RangoDiasComplejidadPorModalidad.Eliminado");

            //Llenado de ListRadioButton

            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";

            /*llenado de combos*/
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadInterna(ddlIdComplejidadInterna, null, true);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvRangoDiasComplejidadPorModalidad.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvRangoDiasComplejidadPorModalidad.AllowPaging;
        Boolean vPaginadorError = gvRangoDiasComplejidadPorModalidad.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvRangoDiasComplejidadPorModalidad.AllowPaging = false;

        List<RangoDiasComplejidadPorModalidad> vListaRangoDiasComplejidadPorModalidads = LlenarGrilla();
        MostrarColumnasExportacionExcel(true);

        datos = ManejoControlesContratos.GenerarDataTable(this.gvRangoDiasComplejidadPorModalidad, vListaRangoDiasComplejidadPorModalidads);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvRangoDiasComplejidadPorModalidad.Columns.Count; i++)
                {
                    dt.Columns.Add(gvRangoDiasComplejidadPorModalidad.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvRangoDiasComplejidadPorModalidad.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvRangoDiasComplejidadPorModalidad.Columns.Count; j++)
                    {
                        dr[gvRangoDiasComplejidadPorModalidad.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaDiasCompejidadModalidad");
                gvRangoDiasComplejidadPorModalidad.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }

    protected List<RangoDiasComplejidadPorModalidad> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<RangoDiasComplejidadPorModalidad> listaRangoDiasComplejidadPorModalidads = Buscar();
        gvRangoDiasComplejidadPorModalidad.Visible = true;
        gvRangoDiasComplejidadPorModalidad.DataSource = listaRangoDiasComplejidadPorModalidads;
        gvRangoDiasComplejidadPorModalidad.DataBind();

        return listaRangoDiasComplejidadPorModalidads;

    }

    protected void MostrarColumnasExportacionExcel(Boolean pMostrarColumnas)
    {
        this.gvRangoDiasComplejidadPorModalidad.Columns[vColumnaOperador].Visible = pMostrarColumnas;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvRangoDiasComplejidadPorModalidad_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el c�digo de llenado de datos para la grilla 
            List<RangoDiasComplejidadPorModalidad> listaRangoDiasComplejidadPorModalidads = Buscar();

            //Fin del c�digo de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaRangoDiasComplejidadPorModalidads != null)
                {
                    var param = Expression.Parameter(typeof(RangoDiasComplejidadPorModalidad), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<RangoDiasComplejidadPorModalidad, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaRangoDiasComplejidadPorModalidads.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaRangoDiasComplejidadPorModalidads.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaRangoDiasComplejidadPorModalidads.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaRangoDiasComplejidadPorModalidads.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaRangoDiasComplejidadPorModalidads;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
