<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_RangoDiasComplejidadPorModalidad_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdRangosDiasComplejidadPorModalidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Modalidad de contrataci&oacute;n *
            </td>
            <td>Operador 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlOperador" Enabled="false">
                    <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                    <asp:ListItem Value="<"><</asp:ListItem>
                    <asp:ListItem Value=">">></asp:ListItem>
                    <asp:ListItem Value="=">=</asp:ListItem>
                    <asp:ListItem Value="<="><=</asp:ListItem>
                    <asp:ListItem Value=">=">>=</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>L&iacute;mite en cantidad de SMMLV 
            </td>
            <td>Complejidad Interna 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtLimite" Enabled="false" MaxLength="6"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadInterna" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Complejidad Indicador 
            </td>
            <td>D&iacute;as H&aacute;biles Internos 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadIndicador" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesInternos" Enabled="false" MaxLength="3"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>D&iacute;as H&aacute;biles Cumplimiento Indicador 
            </td>
            <td>Estado 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesCumplimientoIndicador" Enabled="false" MaxLength="3"></asp:TextBox>
            </td>
            <td align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
