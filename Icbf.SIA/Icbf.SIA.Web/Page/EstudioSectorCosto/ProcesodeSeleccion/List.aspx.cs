﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_ProcesodeSeleccion_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ProcesodeSeleccion";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtConsecutivoEstudio.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<InformacionProcesoSeleccion> Buscar()
    {
        try
        {
            Int32? vConsecutivoEstudio = null;
            String vNombreAbreviado = null;
            String vObjeto = null;
            int? vIdModalidadSeleccion = null;
            int? vIdResponsableES = null;
            int? vIdResponsableEC = null;
            int? vIdEstadoSolicitud = null;
            int? vIdDireccionSolicitante = null;

            if (txtConsecutivoEstudio.Text != "")
            {
                vConsecutivoEstudio = Convert.ToInt32(txtConsecutivoEstudio.Text);
            }
            if (txtNombreAbreviado.Text != "")
            {
                vNombreAbreviado = Convert.ToString(txtNombreAbreviado.Text);
            }
            if (txtObjeto.Text != "")
            {
                vObjeto = Convert.ToString(txtObjeto.Text);
            }
            if (ddlIdModalidadSeleccion.SelectedValue != "-1")
            {
                vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            }
            if (ddlIdResponsableES.SelectedValue != "-1")
            {
                vIdResponsableES = Convert.ToInt32(ddlIdResponsableES.SelectedValue);
            }
            if (ddlIdResponsableEC.SelectedValue != "-1")
            {
                vIdResponsableEC = Convert.ToInt32(ddlIdResponsableEC.SelectedValue);
            }
            if (ddlIdEstadoSolicitud.SelectedValue != "-1")
            {
                vIdEstadoSolicitud = Convert.ToInt32(ddlIdEstadoSolicitud.SelectedValue);
            }
            if (ddlIdDireccionSolicitante.SelectedValue != "-1")
            {
                vIdDireccionSolicitante = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);
            }
            List<InformacionProcesoSeleccion> ListaProcesoSeleccionPorContrato = vEstudioSectorCostoService.ConsultarProcesoSeleccions(vConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdModalidadSeleccion, vIdResponsableES, vIdResponsableEC, vIdEstadoSolicitud, vIdDireccionSolicitante);
            return ListaProcesoSeleccionPorContrato;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvInformacionProcesoSeleccion.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            gvInformacionProcesoSeleccion.DataSource = null;
            gvInformacionProcesoSeleccion.DataBind();
            //toolBar.MostrarMensajeError("No se encontraron datos, verifique por favor");
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvInformacionProcesoSeleccion.PageSize = PageSize();
            gvInformacionProcesoSeleccion.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Informaci&oacute;n Proceso de Selecci&oacute;n", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvInformacionProcesoSeleccion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvInformacionProcesoSeleccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvInformacionProcesoSeleccion.SelectedRow);
    }
    protected void gvInformacionProcesoSeleccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInformacionProcesoSeleccion.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("InformacionProcesoSeleccion.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("InformacionProcesoSeleccion.Eliminado");

            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableES, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableEC, null, true);
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitante, null, true);
            vManejoControlesContratos.LlenarEstadoEstudio(ddlIdEstadoSolicitud, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvInformacionProcesoSeleccion.AllowPaging;
        Boolean vPaginadorError = gvInformacionProcesoSeleccion.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvInformacionProcesoSeleccion.AllowPaging = false;

        List<InformacionProcesoSeleccion> vListaSolicitudes = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvInformacionProcesoSeleccion, vListaSolicitudes);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvInformacionProcesoSeleccion.Columns.Count; i++)
                {
                    dt.Columns.Add(gvInformacionProcesoSeleccion.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvInformacionProcesoSeleccion.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvInformacionProcesoSeleccion.Columns.Count; j++)
                    {
                        if (row.Cells[j].Text.Equals("&nbsp;"))
                        {
                            row.Cells[j].Text = "";
                        }
                        dr[gvInformacionProcesoSeleccion.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ProcesoSeleccion_"+DateTime.Now.ToString("ddMMyyyy"));
                gvInformacionProcesoSeleccion.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }

    protected List<InformacionProcesoSeleccion> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<InformacionProcesoSeleccion> listaProcesoSeleccionPorContratos = Buscar();
        gvInformacionProcesoSeleccion.DataSource = listaProcesoSeleccionPorContratos;
        gvInformacionProcesoSeleccion.DataBind();

        return listaProcesoSeleccionPorContratos;
    }
    protected void gvInformacionProcesoSeleccion_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }


    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<InformacionProcesoSeleccion> vListaProcesoSeleccionPorContratos = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (vListaProcesoSeleccionPorContratos != null)
                {
                    var param = Expression.Parameter(typeof(InformacionProcesoSeleccion), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<InformacionProcesoSeleccion, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = vListaProcesoSeleccionPorContratos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaProcesoSeleccionPorContratos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = vListaProcesoSeleccionPorContratos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaProcesoSeleccionPorContratos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = vListaProcesoSeleccionPorContratos;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}