﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_ProcesodeSeleccion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ProcesodeSeleccion";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            imgListRSE.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo(SolutionPage.List);
    }
    protected void btnConBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }
    protected void btnLimpiarPantalla_Click(object sender, EventArgs e)
    {
        txtCVigenciaProceso.Text = "";
        txtCNumeroProceso.Text = "";
        txtCObjetoContrato.Text = "";

        gvContrato.DataSource = null;
        gvContrato.DataBind();
    }

    protected List<InformacionProcesoSeleccion> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<InformacionProcesoSeleccion> ListaContratos = Buscar();
        if (ListaContratos.Count > 0)
        {
            pnlContrato.Visible = true;
        }
        else
        {
            pnlContrato.Visible = false;
        }
        gvContrato.DataSource = ListaContratos;
        gvContrato.DataBind();

        return ListaContratos;
    }

    protected void txtConsecutivoEstudio_TextChanged(object sender, EventArgs e)
    {
        txtConsecutivoEstudio.Enabled = false;
    }

    protected void gvContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContrato.SelectedRow);
    }

    protected void gvContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContrato.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            pnlContrato.Visible = gvContrato.DataKeys[rowIndex].Values[1] == null ? false : true;
            if (gvContrato.DataKeys[rowIndex].Values[1] == null)
            {
                return;
            }
            if (gvContrato.DataKeys[rowIndex].Values[0] != null)
            {
                hfIdContrato.Value = gvContrato.DataKeys[rowIndex].Values[0].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[1] != null)
            {
                txtNumeroProceso.Text = gvContrato.DataKeys[rowIndex].Values[1].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[2] != null)
            {
                txtObjetoContrato.Text = gvContrato.DataKeys[rowIndex].Values[2].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[3] != null)
            {
                txtFechaAdjudicacion.Text = Convert.ToDateTime(gvContrato.DataKeys[rowIndex].Values[3].ToString()).Date.ToShortDateString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[4] != null)
            {
                txtContratista.Text = gvContrato.DataKeys[rowIndex].Values[4].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[5] != null)
            {
                txtPlazoEjecucion.Text = gvContrato.DataKeys[rowIndex].Values[5].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[6] != null)
            {
                txtValorPresupuestado.Text = gvContrato.DataKeys[rowIndex].Values[6].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[7] != null)
            {
                txtValorAdjudicado.Text = gvContrato.DataKeys[rowIndex].Values[7].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[8] != null)
            {
                txtObservacion.Text = gvContrato.DataKeys[rowIndex].Values[8].ToString();
            }
            if (gvContrato.DataKeys[rowIndex].Values[9] != null)
            {
                txtUrlProceso.Text = gvContrato.DataKeys[rowIndex].Values[9].ToString();
            }

           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private List<InformacionProcesoSeleccion> Buscar()
    {
        try
        {
            int? vConsecutivo = null;
            String vNumeroProceso = null;
            int? vVigenciaProceso = null;
            String vObjetoContrato = null;

            if (txtConsecutivoEstudio.Text != "")
            {
                vConsecutivo = Convert.ToInt32(txtConsecutivoEstudio.Text);
            }
            if (txtCNumeroProceso.Text != "")
            {
                vNumeroProceso = txtCNumeroProceso.Text;
            }
            if (txtCVigenciaProceso.Text != "")
            {
                vVigenciaProceso = Convert.ToInt32(txtCVigenciaProceso.Text);
            }
            if (txtCObjetoContrato.Text != "")
            {
                vObjetoContrato = txtCObjetoContrato.Text;
            }

            List<InformacionProcesoSeleccion> ListaContratos = vEstudioSectorCostoService.ConsultarContratoESC(vConsecutivo, vNumeroProceso, vVigenciaProceso, vObjetoContrato, null);
            return ListaContratos;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            txtConsecutivoEstudio.Enabled = false;

            toolBar.EstablecerTitulos("Informaci&oacute;n Proceso de Selecci&oacute;n", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<InformacionProcesoSeleccion> vListaContratos = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (vListaContratos != null)
                {
                    var param = Expression.Parameter(typeof(InformacionProcesoSeleccion), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<InformacionProcesoSeleccion, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = vListaContratos;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdInformacionProcesoSeleccion = Convert.ToInt32(GetSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion"));
            RemoveSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion");

            if (GetSessionParameter("InformacionProcesoSeleccion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("InformacionProcesoSeleccion.Guardado");

            InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();
            vInformacionProcesoSeleccion = vEstudioSectorCostoService.ConsultarInformacionProcesoSeleccion(vIdInformacionProcesoSeleccion);
            hfIdInformacionProcesoSeleccion.Value = vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion.ToString();
            hfIdContrato.Value = vInformacionProcesoSeleccion.IdContrato.ToString();

            txtConsecutivoEstudio.Text = vInformacionProcesoSeleccion.IdConsecutivoEstudio.ToString();
            txtNumeroProceso.Text = vInformacionProcesoSeleccion.NumeroDelProceso;
            txtObjetoContrato.Text = vInformacionProcesoSeleccion.ObjetoContrato;
            txtEstadoProceso.Text = vInformacionProcesoSeleccion.EstadoDelProceso;
            txtFechaAdjudicacion.Text = vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso == null ? "" : vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso.Value.Date.ToShortDateString();
            txtContratista.Text = vInformacionProcesoSeleccion.Contratista;
            txtValorPresupuestado.Text = Convert.ToDecimal(vInformacionProcesoSeleccion.ValorPresupuestadoInicial) == 0 ? string.Empty : string.Format("${0:#,##0.00}", vInformacionProcesoSeleccion.ValorPresupuestadoInicial);
            txtValorAdjudicado.Text = Convert.ToDecimal(vInformacionProcesoSeleccion.ValorAdjudicado) == 0 ? string.Empty : string.Format("${0:#,##0.00}", vInformacionProcesoSeleccion.ValorAdjudicado);
            txtPlazoEjecucion.Text = vInformacionProcesoSeleccion.PlazoDeEjecucion;
            txtObservacion.Text = vInformacionProcesoSeleccion.Observacion;
            txtUrlProceso.Text = vInformacionProcesoSeleccion.URLDelProceso;
            pnlContrato.Visible = !string.IsNullOrWhiteSpace(vInformacionProcesoSeleccion.NumeroDelProceso);
            ObtenerAuditoria(PageName, hfIdInformacionProcesoSeleccion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInformacionProcesoSeleccion.UsuarioCrea, vInformacionProcesoSeleccion.FechaCrea, vInformacionProcesoSeleccion.UsuarioModifica, vInformacionProcesoSeleccion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();
            
            vInformacionProcesoSeleccion.IdConsecutivoEstudio = txtConsecutivoEstudio.Text == "" ? 0 : Convert.ToInt32(txtConsecutivoEstudio.Text);
            vInformacionProcesoSeleccion.IdContrato = hfIdContrato.Value == "" ? null : (int?)Convert.ToInt32(hfIdContrato.Value);
            if (txtNumeroProceso.Text != "") vInformacionProcesoSeleccion.NumeroDelProceso = txtNumeroProceso.Text.ToUpper();
            if (txtObjetoContrato.Text != "") vInformacionProcesoSeleccion.ObjetoContrato = txtObjetoContrato.Text.ToUpper();
            if (txtEstadoProceso.Text != "") vInformacionProcesoSeleccion.EstadoDelProceso = txtEstadoProceso.Text.ToUpper();
            vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso = txtFechaAdjudicacion.Text == "" ? null : (DateTime?)Convert.ToDateTime(txtFechaAdjudicacion.Text);
            if (txtContratista.Text != "") vInformacionProcesoSeleccion.Contratista = txtContratista.Text.ToUpper();
            Char vDelimitador = '$';
            string[] vValorPresupuestado = txtValorPresupuestado.Text.ToString().Split(vDelimitador);
            //vInformacionProcesoSeleccion.ValorPresupuestadoInicial = txtValorPresupuestado.Text.Equals(string.Empty) ? 0 : decimal.Parse(vValorPresupuestado[1]);
            vInformacionProcesoSeleccion.ValorPresupuestadoInicial = txtValorPresupuestado.Text.Equals(string.Empty) ? 0 : decimal.Parse(txtValorPresupuestado.Text.Replace('$', ' ').Trim());
            string[] vValorAdjudicado = txtValorAdjudicado.Text.ToString().Split(vDelimitador);
            //vInformacionProcesoSeleccion.ValorAdjudicado = txtValorAdjudicado.Text.Equals(string.Empty) ? 0 : decimal.Parse(vValorAdjudicado[1]);
            vInformacionProcesoSeleccion.ValorAdjudicado = txtValorAdjudicado.Text.Equals(string.Empty) ? 0 : decimal.Parse(txtValorAdjudicado.Text.Replace('$', ' ').Trim());
            if (txtPlazoEjecucion.Text != "") vInformacionProcesoSeleccion.PlazoDeEjecucion = txtPlazoEjecucion.Text.ToUpper();
            if (txtObservacion.Text != "") vInformacionProcesoSeleccion.Observacion = txtObservacion.Text.ToUpper();
            if (txtUrlProceso.Text != "") vInformacionProcesoSeleccion.URLDelProceso = txtUrlProceso.Text.ToUpper();             
            
            if (Request.QueryString["oP"] == "E")
            {
                vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion = Convert.ToInt32(hfIdInformacionProcesoSeleccion.Value);
                vInformacionProcesoSeleccion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vInformacionProcesoSeleccion, this.PageName, SolutionPage.Edit);
                vResultado = vEstudioSectorCostoService.ModificarInformacionProcesoSeleccion(vInformacionProcesoSeleccion);
            }
            else
            {
                //List<InformacionProcesoSeleccion> ListaInformacionProcesoSeleccion = vEstudioSectorCostoService.ConsultarProcesoSeleccions();

                //if (ListaInformacionProcesoSeleccion.Count == 0)
                //{
                    vInformacionProcesoSeleccion.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vInformacionProcesoSeleccion, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarInformacionProcesoSeleccion(vInformacionProcesoSeleccion);
                //}
                //else
                //{
                //    vResultado = -1;
                //}
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion", vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion);
                SetSessionParameter("InformacionProcesoSeleccion.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado == -1)
            {
                toolBar.MostrarMensajeError("El registro ya existe.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }   
}