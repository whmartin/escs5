﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Service;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

public partial class Page_ProcesodeSeleccion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ProcesodeSeleccion";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ContratoService vContratoService = new ContratoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();
            }
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            vManejoControlesContratos = new ManejoControlesContratos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion", hfIdInformacionProcesoSeleccion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Informaci&oacute;n Proceso de Selecci&oacute;n", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdInformacionProcesoSeleccion = Convert.ToInt32(GetSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion"));
            RemoveSessionParameter("InformacionProcesoSeleccion.IdInformacionProcesoSeleccion");

            if (GetSessionParameter("InformacionProcesoSeleccion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("InformacionProcesoSeleccion.Guardado");

            InformacionProcesoSeleccion vInformacionProcesoSeleccion = new InformacionProcesoSeleccion();
            vInformacionProcesoSeleccion = vEstudioSectorCostoService.ConsultarInformacionProcesoSeleccion(vIdInformacionProcesoSeleccion);
            hfIdInformacionProcesoSeleccion.Value = vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion.ToString();

            txtConsecutivoEstudio.Text = vInformacionProcesoSeleccion.IdConsecutivoEstudio.ToString();
            txtNumeroProceso.Text = vInformacionProcesoSeleccion.NumeroDelProceso;
            txtObjetoContrato.Text = vInformacionProcesoSeleccion.ObjetoContrato;
            txtEstadoProceso.Text = vInformacionProcesoSeleccion.EstadoDelProceso;
            txtFechaAdjudicacion.Text = vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso == null ? "" : vInformacionProcesoSeleccion.FechaAdjudicacionDelProceso.Value.Date.ToShortDateString();
            txtContratista.Text = vInformacionProcesoSeleccion.Contratista;
            txtValorPresupuestado.Text = Convert.ToDecimal(vInformacionProcesoSeleccion.ValorPresupuestadoInicial) == 0 ? string.Empty : string.Format("${0:#,##0.00}", vInformacionProcesoSeleccion.ValorPresupuestadoInicial);
            txtValorAdjudicado.Text = Convert.ToDecimal(vInformacionProcesoSeleccion.ValorAdjudicado) == 0 ? string.Empty : string.Format("${0:#,##0.00}", vInformacionProcesoSeleccion.ValorAdjudicado);
            txtPlazoEjecucion.Text = vInformacionProcesoSeleccion.PlazoDeEjecucion;
            txtObservacion.Text = vInformacionProcesoSeleccion.Observacion;
            txtUrlProceso.Text = vInformacionProcesoSeleccion.URLDelProceso;
            pnlContrato.Visible = !string.IsNullOrWhiteSpace(vInformacionProcesoSeleccion.NumeroDelProceso);
            hdnIdConsecutivoEstudio.Value = vInformacionProcesoSeleccion.IdConsecutivoEstudio.ToString();
            hdnNumeroDelProceso.Value = vInformacionProcesoSeleccion.NumeroDelProceso;
            hdnObjetoContrato.Value = vInformacionProcesoSeleccion.ObjetoContrato;
            
            List<InformacionProcesoSeleccion> ListaContratos = vEstudioSectorCostoService.ConsultarContratoESC(vInformacionProcesoSeleccion.IdConsecutivoEstudio, vInformacionProcesoSeleccion.NumeroDelProceso, null, vInformacionProcesoSeleccion.ObjetoContrato, vInformacionProcesoSeleccion.IdInformacionProcesoSeleccion);
            gvContrato.DataSource = ListaContratos;
            gvContrato.DataBind();

            ObtenerAuditoria(PageName, hfIdInformacionProcesoSeleccion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInformacionProcesoSeleccion.UsuarioCrea, vInformacionProcesoSeleccion.FechaCrea, vInformacionProcesoSeleccion.UsuarioModifica, vInformacionProcesoSeleccion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected List<InformacionProcesoSeleccion> Buscar()
    {
        List<InformacionProcesoSeleccion> ListaContratos = vEstudioSectorCostoService.ConsultarContratoESC(int.Parse(hdnIdConsecutivoEstudio.Value), hdnNumeroDelProceso.Value.Equals(string.Empty)?null: hdnNumeroDelProceso.Value, null, hdnObjetoContrato.Value.Equals(string.Empty) ? null : hdnObjetoContrato.Value, null);
        return ListaContratos;
    }
    protected void gvContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContrato.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }

    
    protected List<InformacionProcesoSeleccion> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<InformacionProcesoSeleccion> ListaContratos = Buscar();       
        gvContrato.DataSource = ListaContratos;
        gvContrato.DataBind();

        return ListaContratos;
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<InformacionProcesoSeleccion> vListaContratos = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (vListaContratos != null)
                {
                    var param = Expression.Parameter(typeof(InformacionProcesoSeleccion), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<InformacionProcesoSeleccion, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaContratos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = vListaContratos;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }
}