﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ProcesodeSeleccion_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Consecutivo solicitud
                </td>
                <td>Nombre abreviado 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" Width="90%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftConsecutivoEstudio" runat="server" TargetControlID="txtConsecutivoEstudio"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" Width="90%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreAbreviado" runat="server" TargetControlID="txtNombreAbreviado"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Objeto 
                </td>
                <td>Responsable DT o ES 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtObjeto" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" Width="90%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObjeto" runat="server" TargetControlID="txtObjeto"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdResponsableES" Width="90%"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Responsable EC 
                </td>
                <td>Modalidad de contratación
                </td>
            </tr>
            <tr class="rowA">
                <td>

                    <asp:DropDownList runat="server" ID="ddlIdResponsableEC" Width="90%"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Width="90%"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Dependencia solicitante
                </td>
                <td> 
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Width="90%"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" Width="90%"></asp:DropDownList>
                </td>
            </tr>            
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvInformacionProcesoSeleccion" AutoGenerateColumns="False" AllowPaging="True" PageIndex="0" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdInformacionProcesoSeleccion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvInformacionProcesoSeleccion_PageIndexChanging" OnSelectedIndexChanged="gvInformacionProcesoSeleccion_SelectedIndexChanged" OnSorting="gvInformacionProcesoSeleccion_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdConsecutivoEstudio" SortExpression="IdConsecutivoEstudio" />
                            <asp:BoundField HeaderText="Número del Proceso" DataField="NumeroDelProceso" SortExpression="NumeroDelProceso" />
                            <asp:BoundField HeaderText="Objeto" DataField="ObjetoContrato" SortExpression="ObjetoContrato" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoDelProceso" SortExpression="EstadoDelProceso" />
                            <asp:BoundField HeaderText="Fecha Adjudicación" DataField="FechaAdjudicacionDelProceso" SortExpression="FechaAdjudicacionDelProceso" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Contratista" DataField="Contratista" SortExpression="Contratista" />
                            <asp:BoundField HeaderText="Valor presupuestado inicial" DataField="ValorPresupuestadoInicial" SortExpression="ValorPresupuestadoInicial" DataFormatString="{0:c0}"/>
                            <asp:BoundField HeaderText="Valor Adjudicado" DataField="ValorAdjudicado" SortExpression="ValorAdjudicado" DataFormatString="{0:c0}"/>
                            <asp:BoundField HeaderText="Plazo de ejecución" DataField="PlazoDeEjecucion" SortExpression="PlazoDeEjecucion" />
                            <asp:BoundField HeaderText="Observación" DataField="Observacion" SortExpression="Observacion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <asp:HiddenField ID="hfNombreSolicitanteES" runat="server" />
            <asp:HiddenField ID="hfNombreSolicitanteEC" runat="server" />
        </table>
    </asp:Panel>
</asp:Content>

