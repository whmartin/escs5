﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ProcesodeSeleccion_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdInformacionProcesoSeleccion" runat="server" />
    <asp:HiddenField runat="server" id="hdnIdConsecutivoEstudio"/>
    <asp:HiddenField runat="server" id="hdnNumeroDelProceso"/>
    <asp:HiddenField runat="server" id="hdnObjetoContrato"/>
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">Consecutivo solicitud *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" Width="34%" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">&nbsp
            </td>
        </tr>
    </table>
    <fieldset>
        <legend>Búsqueda de Contrato </legend>
        <asp:Panel runat="server" ID="pnlConsulta">
            <table width="90%" align="center">
                <tr class="rowB">
                    <td class="Cell">Vigencia del Proceso
                    </td>
                    <td class="Cell">Número del Proceso
                    </td>
                </tr>
                <tr class="rowA">
                    <td class="Cell">
                        <asp:TextBox runat="server" ID="txtCVigenciaProceso" Width="80%" Enabled="False"></asp:TextBox>
                    </td>
                    <td class="Cell">
                        <asp:TextBox runat="server" ID="txtCNumeroProceso" Width="80%" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td class="Cell">Objeto del Contrato
                    </td>
                    <td class="Cell"></td>
                </tr>
                <tr class="rowA">
                    <td class="Cell">
                        <asp:TextBox runat="server" ID="txtCObjetoContrato" Width="80%" Enabled="false"></asp:TextBox>
                    </td>
                    <td class="Cell"></td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvContrato" AutoGenerateColumns="False" AllowPaging="True"  OnPageIndexChanging="gvContrato_PageIndexChanging" OnSorting="gvContrato_Sorting"
                            GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px"
                            AllowSorting="True">
                            <Columns>
                                <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdConsecutivoEstudio" SortExpression="IdConsecutivoEstudio" />
                                <asp:BoundField HeaderText="Número del Proceso" DataField="NumeroDelProceso" SortExpression="NumeroDelProceso" />
                                <asp:BoundField HeaderText="Objeto del contrato" DataField="ObjetoContrato" SortExpression="ObjetoContrato" />
                                <asp:BoundField HeaderText="Fecha adjudicación" DataField="FechaAdjudicacionDelProceso" SortExpression="FechaAdjudicacionDelProceso" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:BoundField HeaderText="Contratista" DataField="Contratista" SortExpression="Contratista" />
                                <asp:BoundField HeaderText="Valor presupuestado inicial" DataField="ValorPresupuestadoInicial" SortExpression="ValorPresupuestadoInicial" DataFormatString="{0:c0}" />
                                <asp:BoundField HeaderText="Valor adjudicado" DataField="ValorAdjudicado" SortExpression="ValorAdjudicado" DataFormatString="{0:c0}" />
                                <asp:BoundField HeaderText="Plazo de ejecución" DataField="PlazoDeEjecucion" SortExpression="PlazoDeEjecucion" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <legend>&nbsp </legend>
    </fieldset>
    <asp:Panel runat="server" ID="pnlContrato">
        <fieldset>
            <legend>Información Contrato</legend>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>Número del Proceso
                    </td>
                    <td>Objeto del Contrato 
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroProceso" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                    <td style="width: 50%">
                        <asp:TextBox runat="server" ID="txtObjetoContrato" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Estado del Proceso
                    </td>
                    <td>Fecha Adjudicación
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtEstadoProceso" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                    <td style="width: 50%">
                        <asp:TextBox runat="server" ID="txtFechaAdjudicacion" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Contratista
                    </td>
                    <td>Valor Presupuestado Inicial 
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtContratista" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                    <td style="width: 50%">
                        <asp:TextBox runat="server" ID="txtValorPresupuestado" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>Valor adjudicado
                    </td>
                    <td>Plazo de ejecución
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtValorAdjudicado" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                    <td style="width: 50%">
                        <asp:TextBox runat="server" ID="txtPlazoEjecucion" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <legend>&nbsp </legend>
        </fieldset>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Observación 
                </td>
                <td>Url del Proceso
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtObservacion" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td style="width: 50%">
                    <asp:TextBox runat="server" ID="txtUrlProceso" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">&nbsp
                </td>
            </tr>
        </table>
    </asp:Panel>


</asp:Content>
