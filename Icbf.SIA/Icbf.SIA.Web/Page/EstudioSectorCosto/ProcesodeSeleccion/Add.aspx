﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ProcesodeSeleccion_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function limitNum(limitField, limitNum) {
            var n = limitField.value.indexOf(".");
            if (n == -1) {
                if (limitField.value.length > limitNum - 3) {
                    limitField.value = limitField.value.substring(0, limitNum - 3);
                }
            }
            else
            {
                if (limitField.value.length > limitNum) {
                    limitField.value = limitField.value.substring(0, limitNum);
                }
            }
        }

        /*****************************************************************************
        Código para colocar los indicadores de miles  y decimales mientras se escribe
        Script creado por Tunait!
        Si quieres usar este script en tu sitio eres libre de hacerlo con la condición de que permanezcan intactas estas líneas, osea, los créditos.
        ******************************************************************************/
        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

        function onBlurValorPresupuestado() {
            prePostbck(true)
            __doPostBack('<%= txtValorPresupuestado.ClientID %>', '');
        }
    </script>
    <asp:HiddenField ID="hfIdInformacionProcesoSeleccion" runat="server"/>
    <asp:HiddenField ID="hfIdContrato" runat="server"/>
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan= "2">
               Consecutivo solicitud *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan = "2">
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" ClientIDMode="Static" Width="34%" AutoPostBack="true" OnTextChanged="txtConsecutivoEstudio_TextChanged"></asp:TextBox>
                <asp:ImageButton ID="imgListRSE" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClientClick="GetListRSE(); return false;" Style="cursor: hand"
                    ToolTip="Buscar" />
                <asp:RequiredFieldValidator runat="server" ID="rfvConsecutivoEstudio" ControlToValidate="txtConsecutivoEstudio"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan= "2">
               &nbsp
            </td>
        </tr>
    </table>
<fieldset>
    <legend> Búsqueda de Contrato </legend>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
	        <td class="Cell">
                Vigencia del Proceso
	        </td>
	        <td class="Cell">
	            Número del Proceso
	        </td>
        </tr>
        <tr class="rowA">
	        <td class="Cell">
	            <asp:TextBox runat="server" ID="txtCVigenciaProceso" MaxLength="4" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCVigenciaProceso" runat="server" TargetControlID="txtCVigenciaProceso"
                    FilterType="Custom,Numbers" ValidChars="" />
	        </td>
	        <td class="Cell">
                <asp:TextBox runat="server" ID="txtCNumeroProceso" MaxLength="30" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCNumeroProceso" runat="server" TargetControlID="txtCNumeroProceso"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
	        </td>
        </tr>
        <tr class="rowB">
	        <td class="Cell">
                Objeto del Contrato
	        </td>
	        <td class="Cell">
	        </td>
        </tr>
        <tr class="rowA">
	        <td class="Cell">
	            <asp:TextBox runat="server" ID="txtCObjetoContrato" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,800);" onKeyUp="limitText(this,800);" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCObjetoContrato" runat="server" TargetControlID="txtCObjetoContrato"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
	        </td>
	        <td class="Cell">                
	        </td>
        </tr>
        <tr class="rowB">
            <td colspan= "2" style="text-align:right">
                   <asp:LinkButton runat="server" ID="btnLimpiarPantalla" OnClick="btnLimpiarPantalla_Click"><img src="../../../Image/btn/clean.png" alt="Limpiar Campos" title="Limpiar Campos" /></asp:LinkButton>
                   <asp:LinkButton ID="btnConBuscar" runat="server" OnClick="btnConBuscar_Click" ValidationGroup="btnConBuscar"><img alt="Consultar Contrato" src="../../../Image/btn/list.png" title="Consultar Contrato" /></asp:LinkButton>  
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContrato" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,NumeroDelProceso,ObjetoContrato,FechaAdjudicacionDelProceso,Contratista,PlazoDeEjecucion,valorpresupuestadoinicial,valoradjudicado,observacion,urldelproceso" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContrato_PageIndexChanging" OnSelectedIndexChanged="gvContrato_SelectedIndexChanged" OnSorting="gvContrato_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdConsecutivoEstudio" SortExpression="IdConsecutivoEstudio"/>
                            <asp:BoundField HeaderText="Número del Proceso" DataField="NumeroDelProceso"  SortExpression="NumeroDelProceso"/>
                            <asp:BoundField HeaderText="Objeto del contrato" DataField="ObjetoContrato"  SortExpression="ObjetoContrato"/>
                            <asp:BoundField HeaderText="Fecha adjudicación" DataField="FechaAdjudicacionDelProceso"  SortExpression="FechaAdjudicacionDelProceso" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Contratista" DataField="Contratista"  SortExpression="Contratista"/>
                            <asp:BoundField HeaderText="Valor presupuestado inicial" DataField="ValorPresupuestadoInicial"  SortExpression="ValorPresupuestadoInicial" DataFormatString="{0:c0}"/>                            
                            <asp:BoundField HeaderText="Valor adjudicado" DataField="ValorAdjudicado"  SortExpression="ValorAdjudicado" DataFormatString="{0:c0}"/>                            
                            <asp:BoundField HeaderText="Plazo de ejecución" DataField="PlazoDeEjecucion"  SortExpression="PlazoDeEjecucion"/>
                            <asp:BoundField HeaderText="URL del proceso" DataField="URLdelproceso"  SortExpression="URLdelproceso" Visible="false"/>
                            <asp:BoundField HeaderText="Observacion" DataField="Observacion"  SortExpression="Observacion" Visible="false"/>                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <legend> &nbsp </legend>
</fieldset>
<asp:Panel runat="server" ID="pnlContrato" Visible="false">
<fieldset>
    <legend>Información Contrato</legend>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
		        Número del Proceso
		    </td>
		    <td>
		        Objeto del Contrato 
		    </td>
        </tr>
        <tr class="rowA">
            <td>
		        <asp:TextBox runat="server" ID="txtNumeroProceso" Enabled="false" Width="80%"></asp:TextBox>
		    </td>
		    <td style="width: 50%">
			    <asp:TextBox runat="server" ID="txtObjetoContrato" Enabled="false" Width="80%"></asp:TextBox>
		    </td>
        </tr>
        <tr class="rowB">
		    <td>
		        Estado del Proceso
		    </td>
		    <td>
		        Fecha Adjudicación
		    </td>
        </tr>
        <tr class="rowA">
		    <td>
		        <asp:TextBox runat="server" ID="txtEstadoProceso" MaxLength="50" Width="80%"></asp:TextBox>
		    </td>
		    <td style="width: 50%">
			    <asp:TextBox runat="server" ID="txtFechaAdjudicacion" Enabled="false" Width="80%"></asp:TextBox>
		    </td>
        </tr>
        <tr class="rowB">
            <td>
		        Contratista
		    </td>
		    <td>
		        Valor Presupuestado Inicial 
		    </td>
        </tr>
        <tr class="rowA">
            <td>
		        <asp:TextBox runat="server" ID="txtContratista" Enabled="false" Width="80%"></asp:TextBox>
		    </td>
		    <td style="width: 50%">
                <asp:TextBox runat="server" ID="txtValorPresupuestado" MaxLength="17" Width="80%"></asp:TextBox>
		    </td>
        </tr>
        <tr class="rowB">
		    <td>
		        Valor adjudicado
		    </td>
		    <td>
		        Plazo de ejecución
		    </td>
        </tr>
        <tr class="rowA">
		    <td>
		        <asp:TextBox runat="server" ID="txtValorAdjudicado" MaxLength="17" Width="80%"></asp:TextBox>
		    </td>
		    <td style="width: 50%">
			    <asp:TextBox runat="server" ID="txtPlazoEjecucion" Enabled="false" Width="80%"></asp:TextBox>
		    </td>
        </tr>
    </table>
    <legend> &nbsp </legend>
</fieldset>
     <table width="90%" align="center">
        <tr class="rowB">
		    <td>
		        Observación 
		    </td>
		    <td>
		        Url del Proceso
		    </td>
        </tr>
        <tr class="rowA">
		    <td>
		        <asp:TextBox runat="server" ID="txtObservacion" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObservacion" runat="server" TargetControlID="txtObservacion"
                FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
		    </td>
		    <td style="width: 50%">
			    <asp:TextBox runat="server" ID="txtUrlProceso" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftUrlProceso" runat="server" TargetControlID="txtUrlProceso"
                FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
		    </td>
        </tr>
        <tr class="rowB">
            <td colspan= "2">
               &nbsp
            </td>
        </tr>
    </table>
</asp:Panel>
   
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {

            $("#cphCont_txtValorPresupuestado").maskMoney({ prefix: '$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: true });
            $("#cphCont_txtValorAdjudicado").maskMoney({ prefix: '$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: true });
        });

        function GetListRSE() {
            muestraImagenLoading();
            document.getElementById("txtConsecutivoEstudio").disabled = false;
            window_showModalDialog('../../../Page/EstudioSectorCosto/Lupas/LupaRegistroInicial.aspx', setReturnGetListRSE, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            ocultaImagenLoading();
        }

        function setReturnGetListRSE(dialog) {
            var pObj = window_returnModalDialog(dialog);

            if (pObj != undefined && pObj != null && pObj != '') {
                var retLupa = pObj.split(",");
                $('#txtConsecutivoEstudio').val(retLupa[0]);

                __doPostBack("txtConsecutivoEstudio", "TextChanged");
            }
            else {
                document.getElementById("txtConsecutivoEstudio").disabled = true;
            }
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.display = 'none';
        }
    </script>
</asp:Content>
