<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TiposEstudio_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdTipoEstudio" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Tipo de solicitud *
            </td>
            <td>Descripci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre" Enabled="false" Width="90%" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
