using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_TiposEstudio_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/TiposEstudio";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TiposEstudio.IdTipoEstudio", hfIdTipoEstudio.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdTipoEstudio = Convert.ToInt32(GetSessionParameter("TiposEstudio.IdTipoEstudio"));
            RemoveSessionParameter("TiposEstudio.IdTipoEstudio");

            if (GetSessionParameter("TiposEstudio.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TiposEstudio.Guardado");


            TiposEstudio vTiposEstudio = new TiposEstudio();
            vTiposEstudio = vEstudioSectorCostoService.ConsultarTiposEstudio(vIdTipoEstudio);
            hfIdTipoEstudio.Value = vTiposEstudio.IdTipoEstudio.ToString();
            txtNombre.Text = vTiposEstudio.Nombre;
            txtDescripcion.Text = vTiposEstudio.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vTiposEstudio.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdTipoEstudio.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTiposEstudio.UsuarioCrea, vTiposEstudio.FechaCrea, vTiposEstudio.UsuarioModifica, vTiposEstudio.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoEstudio = Convert.ToInt32(hfIdTipoEstudio.Value);

            TiposEstudio vTiposEstudio = new TiposEstudio();
            vTiposEstudio = vEstudioSectorCostoService.ConsultarTiposEstudio(vIdTipoEstudio);
            InformacionAudioria(vTiposEstudio, this.PageName, vSolutionPage);
            int vResultado = vEstudioSectorCostoService.EliminarTiposEstudio(vTiposEstudio);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TiposEstudio.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de tipo de Solicitud", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
