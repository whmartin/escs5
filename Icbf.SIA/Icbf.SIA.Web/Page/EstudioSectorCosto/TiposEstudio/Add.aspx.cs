using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_TiposEstudio_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/TiposEstudio";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtNombre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            TiposEstudio vTiposEstudio = new TiposEstudio();

            vTiposEstudio.Nombre = Convert.ToString(txtNombre.Text).ToUpper();
            vTiposEstudio.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();
            vTiposEstudio.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vTiposEstudio.IdTipoEstudio = Convert.ToInt32(hfIdTipoEstudio.Value);
                    vTiposEstudio.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vTiposEstudio, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarTiposEstudio(vTiposEstudio);
                }
                else
                {
                    vTiposEstudio.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vTiposEstudio, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarTiposEstudio(vTiposEstudio);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("TiposEstudio.IdTipoEstudio", vTiposEstudio.IdTipoEstudio);
                    SetSessionParameter("TiposEstudio.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe.");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de tipo de Solicitud", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdTipoEstudio = Convert.ToInt32(GetSessionParameter("TiposEstudio.IdTipoEstudio"));
            String NombreTipoEstudio = vEstudioSectorCostoService.ConsultarTiposEstudio(vIdTipoEstudio).Nombre;
            hfNombreTipoEstudio.Value = NombreTipoEstudio;
            RemoveSessionParameter("TiposEstudio.Id");

            TiposEstudio vTiposEstudio = new TiposEstudio();
            vTiposEstudio = vEstudioSectorCostoService.ConsultarTiposEstudio(vIdTipoEstudio);
            hfIdTipoEstudio.Value = vTiposEstudio.IdTipoEstudio.ToString();
            txtNombre.Text = vTiposEstudio.Nombre;
            txtDescripcion.Text = vTiposEstudio.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vTiposEstudio.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTiposEstudio.UsuarioCrea, vTiposEstudio.FechaCrea, vTiposEstudio.UsuarioModifica, vTiposEstudio.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtNombre.Text.ToUpper().Equals(hfNombreTipoEstudio.Value))
        {
            if (vEstudioSectorCostoService.ValidarTiposEstudio(txtNombre.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
    protected void txtNombre_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
}
