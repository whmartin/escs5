﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaInfoPACCO.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoPACCO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <asp:HiddenField ID="hfEsAdicion" runat="server" />
        <br />
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Consecutivo PACCO
            </td>
            <td>
                Vigencia *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoPACCO" MaxLength="9" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftConsecutivoPACCO" runat="server" TargetControlID="txtConsecutivoPACCO"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>                
                <asp:TextBox runat="server" ID="txtVigenciaPACCO" MaxLength="4" AutoPostBack="true" OnTextChanged="txtVigenciaPACCO_TextChanged" Width="90%"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvVigenciaPACCO" ErrorMessage="Debe ingresar una Vigencia para realizar la consulta"
                        ControlToValidate="txtVigenciaPACCO" SetFocusOnError="true" ValidationGroup="btnBuscar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                <Ajax:FilteredTextBoxExtender ID="ftVigenciaPACCO" runat="server" TargetControlID="txtVigenciaPACCO"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto PACCO
            </td>
            <td>                
                Dependencia solicitante PACCO                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoPACCO" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjetoPACCO" runat="server" TargetControlID="txtObjetoPACCO"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Width="90%"></asp:DropDownList>
            </td>
        </tr>        
        <tr class="rowB">
            <td>
                Valor Presupuestal mínimo PACCO
            </td>
            <td>
                Valor Presupuestal máximo PACCO
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorPresupuestalInicialPACCO" MaxLength="12" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorPresupuestalInicialPACCO" runat="server" TargetControlID="txtValorPresupuestalInicialPACCO"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorPresupuestalFinalPACCO" MaxLength="12" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorPresupuestalFinalPACCO" runat="server" TargetControlID="txtValorPresupuestalFinalPACCO"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Modalidad de contratación PACCO
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidad" Width="90%"></asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvLupaRegistroSolicitudEstudioSectoryCaso" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="consecutivo,anop_vigencia,alcance,codigoarea,valor_contrato,Id_modalidad" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging" OnSelectedIndexChanged="gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField headertext="Acción">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo PACCO" DataField="consecutivo" />                            
                            <asp:BoundField HeaderText="Vigencia PACCO" DataField="anop_vigencia" />
                            <asp:BoundField HeaderText="Objeto PACCO" DataField="alcance" />
                            <asp:BoundField HeaderText="Dependencia solicitante PACCO" DataField="area"/>                            
                            <asp:BoundField HeaderText="Valor Presupuestal PACCO" DataField="valor_contrato" DataFormatString="{0:###}" />
                            <asp:BoundField HeaderText="Modalidad de contratación PACCO" DataField="modalidad"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>