﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaListRSE.aspx.cs" Inherits="Page_Contratos_Lupas_LupaListRSE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <asp:HiddenField ID="hfEsAdicion" runat="server" />
    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td style="height: 24px">
                Consecutivo Estudio
            </td>
            <td style="height: 24px">
                Nombre abreviado 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftConsecutivoEstudio" runat="server" TargetControlID="txtConsecutivoEstudio"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreAbreviado" runat="server" TargetControlID="txtNombreAbreviado"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto 
            </td>
            <td>
                Responsable DT o ES 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjeto" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);"  Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjeto" runat="server" TargetControlID="txtObjeto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdResponsableES" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Responsable EC 
            </td>
            <td>
                Modalidad de contratación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                  
                <asp:DropDownList runat="server" ID="ddlIdResponsableEC" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Año Fecha de solicitud inicial 
            </td>
            <td>
                Dependencia solicitante 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:DropDownList runat="server" ID="ddlIdFechaSolicitudInicial" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Width="90%"></asp:DropDownList>
            </td>
        </tr>
       <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" Width="90%"></asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvLupaRegistroSolicitudEstudioSectoryCaso" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRegistroSolicitudEstudioSectoryCaso" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging" OnSelectedIndexChanged="gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField headertext="Acción">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo  Estudio" DataField="IdRegistroSolicitudEstudioSectoryCaso" />
                            <asp:BoundField HeaderText="Nombre abreviado" DataField="NombreAbreviado" />
                            <asp:BoundField HeaderText="Número de reproceso" DataField="NumeroReproceso" />
                            <asp:BoundField HeaderText="Consecutivo estudio relacionado" DataField="ConsecutivoEstudioRelacionado" />
                            <asp:BoundField HeaderText="Fecha de solicitud inicial" DataField="FechaSolicitudInicial" />
                            <asp:BoundField HeaderText="Acta, correo o Nro. Radicado" DataField="ActaCorreoNoRadicado" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoSolicitud" />
                            <asp:BoundField HeaderText="Modalidad de contratación" DataField="ModalidadSeleccion" />
                            <asp:BoundField HeaderText="Dependencia solicitante" DataField="DireccionSolicitante" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
