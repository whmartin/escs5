﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

using System.Data;

public partial class Page_Contratos_Lupas_LupaListRSE : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "EstudioSectorCosto/RegistroSolicitudEstudioSectoryCaso";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }

    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    private void Buscar()
    {
        try
        {
            Int32? vConsecutivoEstudio = null;
            int? vAplicaPACCO = null;
            int? vConsecutivoPACCO = null;
            int? vDireccionsolicitantePACCO = null;
            String vObjetoPACCO = null;
            int? vModalidadPACCO = null;
            int? vValorPresupuestalPACCO = null;
            String vVigenciaPACCO = null;
            int? vConsecutivoEstudioRelacionado = null;
            DateTime vFechaSolicitudInicial = Convert.ToDateTime("0009-01-01");
            String vActaCorreoNoRadicado = null;
            String vNombreAbreviado = null;
            String vNumeroReproceso = null;
            String vObjeto = null;
            String vCuentaVigenciasFuturasPACCO = null;
            String vAplicaProcesoSeleccion = null;
            int? vIdModalidadSeleccion = null;
            int? vIdTipoEstudio = null;
            int? vIdComplejidadInterna = null;
            int? vIdComplejidadIndicador = null;
            int? vIdResponsableES = null;
            int? vIdResponsableEC = null;
            String vOrdenadorGasto = null;
            int? vIdEstadoSolicitud = null;
            int? vIdMotivoSolicitud = null;
            int? vIdDireccionSolicitante = null;
            int? vIdAreaSolicitante = null;
            String vTipoValor = null;
            int? vValorPresupuestoEstimadoSolicitante = null;
            if (txtConsecutivoEstudio.Text != "")
            {
                vConsecutivoEstudio = Convert.ToInt32(txtConsecutivoEstudio.Text);
            }
            if (ddlIdFechaSolicitudInicial.SelectedValue != "-1")
            {
                vFechaSolicitudInicial = Convert.ToDateTime(ddlIdFechaSolicitudInicial.SelectedValue + "-01-01");
            }
            if (txtNombreAbreviado.Text != "")
            {
                vNombreAbreviado = Convert.ToString(txtNombreAbreviado.Text);
            }
            if (txtObjeto.Text != "")
            {
                vObjeto = Convert.ToString(txtObjeto.Text);
            }
            if (ddlIdModalidadSeleccion.SelectedValue != "-1")
            {
                vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            }
            if (ddlIdResponsableES.SelectedValue != "-1")
            {
                vIdResponsableES = Convert.ToInt32(ddlIdResponsableES.SelectedValue);
            }
            if (ddlIdResponsableEC.SelectedValue != "-1")
            {
                vIdResponsableEC = Convert.ToInt32(ddlIdResponsableEC.SelectedValue);
            }
            if (ddlIdEstadoSolicitud.SelectedValue != "-1")
            {
                vIdEstadoSolicitud = Convert.ToInt32(ddlIdEstadoSolicitud.SelectedValue);
            }
            if (ddlIdDireccionSolicitante.SelectedValue != "-1")
            {
                vIdDireccionSolicitante = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);
            }
            gvLupaRegistroSolicitudEstudioSectoryCaso.DataSource = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCasos(vConsecutivoEstudio, vAplicaPACCO, vConsecutivoPACCO, vDireccionsolicitantePACCO, vObjetoPACCO, vModalidadPACCO, vValorPresupuestalPACCO, vVigenciaPACCO, vConsecutivoEstudioRelacionado, vFechaSolicitudInicial, vActaCorreoNoRadicado, vNombreAbreviado, vNumeroReproceso, vObjeto, vCuentaVigenciasFuturasPACCO, vAplicaProcesoSeleccion, vIdModalidadSeleccion, vIdTipoEstudio, vIdComplejidadInterna, vIdComplejidadIndicador, vIdResponsableES, vIdResponsableEC, vOrdenadorGasto, vIdEstadoSolicitud, vIdMotivoSolicitud, vIdDireccionSolicitante, vIdAreaSolicitante, vTipoValor, vValorPresupuestoEstimadoSolicitante);
            gvLupaRegistroSolicitudEstudioSectoryCaso.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Asociar el Consecutivo de Estudio a un Registro");

            gvLupaRegistroSolicitudEstudioSectoryCaso.PageSize = PageSize();
            gvLupaRegistroSolicitudEstudioSectoryCaso.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvLupaRegistroSolicitudEstudioSectoryCaso.Rows[gvLupaRegistroSolicitudEstudioSectoryCaso.SelectedIndex].Cells[1].Text) + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvLupaRegistroSolicitudEstudioSectoryCaso.SelectedRow);
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvLupaRegistroSolicitudEstudioSectoryCaso.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableES, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableEC, null, true);
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarFechaSolicitudInicial(ddlIdFechaSolicitudInicial, null, true);
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitante, null, true);
            vManejoControlesContratos.LlenarEstadoEstudio(ddlIdEstadoSolicitud, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}