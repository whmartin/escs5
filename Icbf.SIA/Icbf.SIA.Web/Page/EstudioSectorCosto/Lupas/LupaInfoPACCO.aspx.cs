﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

using System.Data;

public partial class Page_Contratos_Lupas_LupaInfoPACCO : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "EstudioSectorCosto/RegistroSolicitudEstudioSectoryCaso";
    WsContratosPacco.WSContratosPACCOSoap _wsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    private void Buscar()
    {
        try
        {
            int vVigenciaPACCO = 0;

            if (txtVigenciaPACCO.Text != "")
            {
                vVigenciaPACCO = Convert.ToInt32(txtVigenciaPACCO.Text);
                int vConsecutivoPACCO = 0;
                int vDireccionsolicitantePACCO = 0;
                String vObjetoPACCO = null;
                decimal vValorPresupuestalInicialPACCO = 0;
                decimal vValorPresupuestalFinalPACCO = 0;
                decimal vModalidad = 0;

                if (txtConsecutivoPACCO.Text != "")
                {
                    vConsecutivoPACCO = Convert.ToInt32(txtConsecutivoPACCO.Text);
                }
                if (ddlIdDireccionSolicitante.SelectedValue != "-1")
                {
                    vDireccionsolicitantePACCO = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);
                }
                if (txtObjetoPACCO.Text != "")
                {
                    vObjetoPACCO = txtObjetoPACCO.Text.ToUpper();
                }
                if (txtValorPresupuestalInicialPACCO.Text != "")
                {
                    vValorPresupuestalInicialPACCO = Convert.ToDecimal(txtValorPresupuestalInicialPACCO.Text);
                }
                if (txtValorPresupuestalFinalPACCO.Text != "")
                {
                    vValorPresupuestalFinalPACCO = Convert.ToDecimal(txtValorPresupuestalFinalPACCO.Text);
                }
                if (ddlIdModalidad.SelectedValue != "-1")
                {
                    vModalidad = Convert.ToInt32(ddlIdModalidad.SelectedValue);
                }

                var myGridResults = _wsContratosPacco.GetListaConsecutivosEncabezadosPACCOXModalidad(vVigenciaPACCO, //2014, //Convert.ToInt32(vVigencia),
                                                                               null, //vIdRegional,
                                                                               0, //vIdEstadoPlanCompras,
                                                                               vDireccionsolicitantePACCO, //codarea,
                                                                               vConsecutivoPACCO, //15450, //vNumeroConsecutivoPlanCompras,
                                                                               null, //vUsuarioDuenoPlanCompras,
                                                                               vValorPresupuestalInicialPACCO, //vValorDesde,
                                                                               vValorPresupuestalFinalPACCO, //vValorHasta,
                                                                               vObjetoPACCO,//vIdAlcance
                                                                               vModalidad); //id_modalidad

                gvLupaRegistroSolicitudEstudioSectoryCaso.DataSource = myGridResults;
                gvLupaRegistroSolicitudEstudioSectoryCaso.DataBind();
            }
            else
            {
                toolBar.MostrarMensajeError("Debe ingresar una Vigencia para realizar la consulta.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            //toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Consulta PACCO");

            gvLupaRegistroSolicitudEstudioSectoryCaso.PageSize = PageSize();
            gvLupaRegistroSolicitudEstudioSectoryCaso.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            for (int c = 0; c < 6; c++)
            {
                if (gvLupaRegistroSolicitudEstudioSectoryCaso.DataKeys[pRow.RowIndex].Values[c] != null && gvLupaRegistroSolicitudEstudioSectoryCaso.DataKeys[pRow.RowIndex].Values[c].ToString() != "&nbsp;")
                {
                    if(c != 4)
                        returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvLupaRegistroSolicitudEstudioSectoryCaso.DataKeys[pRow.RowIndex].Values[c].ToString()).Replace(',', ' ').Replace('\n', ' ') + "';";
                    else
                    {
                        decimal ValorContrato = Convert.ToDecimal(gvLupaRegistroSolicitudEstudioSectoryCaso.DataKeys[pRow.RowIndex].Values[c]);
                        string [] ValorSTR = ValorContrato.ToString().Split(',');
                        returnValues += "pObj[" + (c) + "] = '" + ValorSTR[0] + "';";
                    }
                }
                else
                {
                    returnValues += "pObj[" + (c) + "] = '';";
                }
                    
            }

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvLupaRegistroSolicitudEstudioSectoryCaso.SelectedRow);
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvLupaRegistroSolicitudEstudioSectoryCaso.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarDireccionPACCO(ddlIdDireccionSolicitante, null, true, 0);
            vManejoControlesContratos.LlenarModalidadPACCO(ddlIdModalidad, null, true, 0);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void txtVigenciaPACCO_TextChanged(object sender, EventArgs e)
    {
        vManejoControlesContratos = new ManejoControlesContratos();
        int pVigencia = Convert.ToInt32(txtVigenciaPACCO.Text);
        vManejoControlesContratos.LlenarDireccionPACCO(ddlIdDireccionSolicitante, null, true, pVigencia);
        vManejoControlesContratos.LlenarModalidadPACCO(ddlIdModalidad, null, true, pVigencia);
    }
}