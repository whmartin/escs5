using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_AreasSolicitantesESC_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/AreasSolicitantesESC";
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtAreaSolicitante.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();

            vAreasSolicitantesESC.AreaSolicitante = Convert.ToString(txtAreaSolicitante.Text).ToUpper();
            vAreasSolicitantesESC.IdDireccionSolicitanteESC = Convert.ToInt32(ddlIdDireccionSolicitanteESC.SelectedValue);
            vAreasSolicitantesESC.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();
            vAreasSolicitantesESC.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));

            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vAreasSolicitantesESC.IdAreasSolicitantes = Convert.ToInt32(hfIdAreasSolicitantes.Value);
                    vAreasSolicitantesESC.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vAreasSolicitantesESC, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarAreasSolicitantesESC(vAreasSolicitantesESC);
                }
                else
                {
                    vAreasSolicitantesESC.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vAreasSolicitantesESC, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarAreasSolicitantesESC(vAreasSolicitantesESC);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("AreasSolicitantesESC.IdAreasSolicitantes", vAreasSolicitantesESC.IdAreasSolicitantes);
                    SetSessionParameter("AreasSolicitantesESC.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de &Aacute;reas Solicitantes", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdAreasSolicitantes = Convert.ToInt32(GetSessionParameter("AreasSolicitantesESC.IdAreasSolicitantes"));
            String NombreAreasSolicitante = vEstudioSectorCostoService.ConsultarAreasSolicitantesESC(vIdAreasSolicitantes).AreaSolicitante;
            HfNombreAreasSolicitantes.Value = NombreAreasSolicitante;
            RemoveSessionParameter("AreasSolicitantesESC.Id");

            AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();
            vAreasSolicitantesESC = vEstudioSectorCostoService.ConsultarAreasSolicitantesESC(vIdAreasSolicitantes);
            hfIdAreasSolicitantes.Value = vAreasSolicitantesESC.IdAreasSolicitantes.ToString();
            txtAreaSolicitante.Text = vAreasSolicitantesESC.AreaSolicitante;
            ddlIdDireccionSolicitanteESC.SelectedValue = vAreasSolicitantesESC.IdDireccionSolicitanteESC.ToString();
            HfIdDireccion.Value = vAreasSolicitantesESC.IdDireccionSolicitanteESC.ToString();
            txtDescripcion.Text = vAreasSolicitantesESC.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vAreasSolicitantesESC.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vAreasSolicitantesESC.UsuarioCrea, vAreasSolicitantesESC.FechaCrea, vAreasSolicitantesESC.UsuarioModifica, vAreasSolicitantesESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitanteESC, null, true);
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtAreaSolicitante.Text.ToUpper().Equals(HfNombreAreasSolicitantes.Value))
        {
            if (vEstudioSectorCostoService.ValidarAreasSolicitantesESC(txtAreaSolicitante.Text.ToUpper(), Convert.ToInt16(ddlIdDireccionSolicitanteESC.SelectedValue)))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        else if (!ddlIdDireccionSolicitanteESC.SelectedValue.Equals(HfIdDireccion.Value))
        {
            if (vEstudioSectorCostoService.ValidarAreasSolicitantesESC(txtAreaSolicitante.Text.ToUpper(), Convert.ToInt16(ddlIdDireccionSolicitanteESC.SelectedValue)))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
    protected void txtAreaSolicitante_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
    protected void ddlIdDireccionSolicitanteESC_SelectedIndexChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
}
