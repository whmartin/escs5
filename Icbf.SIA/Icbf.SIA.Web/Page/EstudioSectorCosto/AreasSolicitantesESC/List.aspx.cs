using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_AreasSolicitantesESC_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/AreasSolicitantesESC";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtAreaSolicitante.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<AreasSolicitantesESC> Buscar()
    {
        try
        {
            String vAreaSolicitante = null;
            int? vIdDireccionSolicitanteESC = null;
            String vDescripcion = null;
            int? vEstado = null;
            if (txtAreaSolicitante.Text != "")
            {
                vAreaSolicitante = Convert.ToString(txtAreaSolicitante.Text);
            }
            if (ddlIdDireccionSolicitanteESC.SelectedValue != "-1")
            {
                vIdDireccionSolicitanteESC = Convert.ToInt32(ddlIdDireccionSolicitanteESC.SelectedValue);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }
            List<AreasSolicitantesESC> ListaAreasSolicitantesESC = vEstudioSectorCostoService.ConsultarAreasSolicitantesESCs(vAreaSolicitante, vIdDireccionSolicitanteESC, vDescripcion, vEstado);
            return ListaAreasSolicitantesESC;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvAreasSolicitantesESC.PageSize = PageSize();
            gvAreasSolicitantesESC.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de &Aacute;reas Solicitantes", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvAreasSolicitantesESC.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("AreasSolicitantesESC.IdAreasSolicitantes", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAreasSolicitantesESC_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvAreasSolicitantesESC.SelectedRow);
    }
    protected void gvAreasSolicitantesESC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAreasSolicitantesESC.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("AreasSolicitantesESC.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("AreasSolicitantesESC.Eliminado");

            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitanteESC, null, true);

            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvAreasSolicitantesESC.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvAreasSolicitantesESC.AllowPaging;
        Boolean vPaginadorError = gvAreasSolicitantesESC.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvAreasSolicitantesESC.AllowPaging = false;

        List<AreasSolicitantesESC> vListaAreasSolicitantesESCs = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvAreasSolicitantesESC, vListaAreasSolicitantesESCs);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvAreasSolicitantesESC.Columns.Count; i++)
                {
                    dt.Columns.Add(gvAreasSolicitantesESC.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvAreasSolicitantesESC.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvAreasSolicitantesESC.Columns.Count; j++)
                    {
                        dr[gvAreasSolicitantesESC.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaAreasSolicitantes");
                gvAreasSolicitantesESC.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }


    protected List<AreasSolicitantesESC> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<AreasSolicitantesESC> listaAreasSolicitantesESCs = Buscar();
        gvAreasSolicitantesESC.DataSource = listaAreasSolicitantesESCs;
        gvAreasSolicitantesESC.DataBind();

        return listaAreasSolicitantesESCs;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el c�digo de llenado de datos para la grilla 
            List<AreasSolicitantesESC> listaAreasSolicitantesESCs = Buscar();

            //Fin del c�digo de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaAreasSolicitantesESCs != null)
                {
                    var param = Expression.Parameter(typeof(AreasSolicitantesESC), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<AreasSolicitantesESC, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaAreasSolicitantesESCs.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaAreasSolicitantesESCs.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaAreasSolicitantesESCs.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaAreasSolicitantesESCs.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaAreasSolicitantesESCs;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvAreasSolicitantesESC_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
}
