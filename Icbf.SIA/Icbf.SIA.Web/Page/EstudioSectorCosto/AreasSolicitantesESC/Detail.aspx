<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_AreasSolicitantesESC_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdAreasSolicitantes" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Área solicitante *
            </td>
            <td>Dirección *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAreaSolicitante" Enabled="false" MaxLength="100" Width="98%"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitanteESC" Enabled="false" Width="98%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Descripción
            </td>
            <td>Estado Actividad *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
