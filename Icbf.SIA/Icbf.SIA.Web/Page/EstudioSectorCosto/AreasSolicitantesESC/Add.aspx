<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_AreasSolicitantesESC_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }
    </script>
    <asp:HiddenField ID="hfIdAreasSolicitantes" runat="server" />
    <asp:HiddenField ID="HfValidacion" runat="server" />
    <asp:HiddenField ID="HfNombreAreasSolicitantes" runat="server" />
    <asp:HiddenField ID="HfIdDireccion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Área solicitante *
                <asp:RequiredFieldValidator runat="server" ID="rfvAreaSolicitante" ControlToValidate="txtAreaSolicitante"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Dirección *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccionSolicitanteESC" ControlToValidate="ddlIdDireccionSolicitanteESC"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdDireccionSolicitanteESC" ControlToValidate="ddlIdDireccionSolicitanteESC"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAreaSolicitante" MaxLength="100" Width="98%" OnTextChanged="txtAreaSolicitante_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftAreaSolicitante" runat="server" TargetControlID="txtAreaSolicitante"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitanteESC" Width="98%" OnSelectedIndexChanged="ddlIdDireccionSolicitanteESC_SelectedIndexChanged"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Descripción
            </td>
            <td>Estado Actividad *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine"
                    onKeyDown="limitText(this,255);" onKeyUp="limitText(this,255);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
