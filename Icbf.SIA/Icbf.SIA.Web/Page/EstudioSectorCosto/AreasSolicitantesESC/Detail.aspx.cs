using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_AreasSolicitantesESC_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/AreasSolicitantesESC";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("AreasSolicitantesESC.IdAreasSolicitantes", hfIdAreasSolicitantes.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdAreasSolicitantes = Convert.ToInt32(GetSessionParameter("AreasSolicitantesESC.IdAreasSolicitantes"));
            RemoveSessionParameter("AreasSolicitantesESC.IdAreasSolicitantes");

            if (GetSessionParameter("AreasSolicitantesESC.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("AreasSolicitantesESC.Guardado");


            AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();
            vAreasSolicitantesESC = vEstudioSectorCostoService.ConsultarAreasSolicitantesESC(vIdAreasSolicitantes);
            hfIdAreasSolicitantes.Value = vAreasSolicitantesESC.IdAreasSolicitantes.ToString();
            txtAreaSolicitante.Text = vAreasSolicitantesESC.AreaSolicitante;
            ddlIdDireccionSolicitanteESC.SelectedValue = vAreasSolicitantesESC.IdDireccionSolicitanteESC.ToString();
            txtDescripcion.Text = vAreasSolicitantesESC.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vAreasSolicitantesESC.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdAreasSolicitantes.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vAreasSolicitantesESC.UsuarioCrea, vAreasSolicitantesESC.FechaCrea, vAreasSolicitantesESC.UsuarioModifica, vAreasSolicitantesESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdAreasSolicitantes = Convert.ToInt32(hfIdAreasSolicitantes.Value);

            AreasSolicitantesESC vAreasSolicitantesESC = new AreasSolicitantesESC();
            vAreasSolicitantesESC = vEstudioSectorCostoService.ConsultarAreasSolicitantesESC(vIdAreasSolicitantes);
            InformacionAudioria(vAreasSolicitantesESC, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarAreasSolicitantesESC(vAreasSolicitantesESC);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("AreasSolicitantesESC.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de &Aacute;reas Solicitantes", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitanteESC, null, true);
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
