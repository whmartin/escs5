<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RegistroSolicitudEstudioSectoryCaso_Add" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdRegistroSolicitudEstudioSectoryCaso" runat="server" />
    <asp:HiddenField ID="hfIdDireccion" runat="server" />    
    <asp:HiddenField ID="hfIdModalidad" runat="server" />    
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:CheckBox ID="chkAplicaPacco" runat="server" AutoPostBack="True" OnCheckedChanged="chkAplicaPacco_CheckedChanged" />
                No Aplica PACCO
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA" style="background-color:lightblue">            
            <td colspan="2">
                <asp:Label runat="server" ID="Label1" Text="Información PACCO" style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Consecutivo PACCO
            </td>
            <td>
                Dependencia solicitante PACCO
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoPACCO" ClientIDMode="Static" MaxLength="9" Width="90%" OnTextChanged="txtConsecutivoPACCO_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftConsecutivoPACCO" runat="server" TargetControlID="txtConsecutivoPACCO"
                    FilterType="Numbers" ValidChars="" />
                <asp:ImageButton ID="imgGetInfoPACCO" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClientClick="GetInfoPACCO(); return false;" Style="cursor: hand"
                    ToolTip="Buscar"/>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitantePacco" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto PACCO
            </td>
            <td>
                Valor Presupuestal PACCO
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoPACCO" TextMode="MultiLine" Rows="4" ClientIDMode="Static" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjetoPACCO" runat="server" TargetControlID="txtObjetoPACCO"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorPresupuestalPACCO" ClientIDMode="Static" MaxLength="12" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorPresupuestalPACCO" runat="server" TargetControlID="txtValorPresupuestalPACCO"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia PACCO *
                <asp:RequiredFieldValidator runat="server" ID="rfvVigenciaPACCO" ControlToValidate="txtVigenciaPACCO"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Modalidad de contratación PACCO
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtVigenciaPACCO" ClientIDMode="Static" MaxLength="4" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftVigenciaPACCO" runat="server" TargetControlID="txtVigenciaPACCO"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>      
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccionPACCO" Width="90%"></asp:DropDownList>          
            </td>
        </tr>
        <tr class="rowB">
            <td>
                
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA" style="background-color:lightblue">
            <td colspan="2">
                <asp:Label runat="server" ID="lblTitulo2" class="success" Text="Registro Inicial" style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Consecutivo solicitud *
            </td>
            <td>
                Consecutivo estudio relacionado
                <asp:RequiredFieldValidator runat="server" ID="rfvConsecutivoEstudioRelacionado" ControlToValidate="txtConsecutivoEstudioRelacionado" Enabled="false"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudioRelacionado" ClientIDMode="Static" MaxLength="5" Width="90%" AutoPostBack="true" OnTextChanged="txtConsecutivoEstudioRelacionado_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftConsecutivoEstudioRelacionado" runat="server" TargetControlID="txtConsecutivoEstudioRelacionado"
                    FilterType="Numbers" ValidChars="" />
                <asp:ImageButton ID="imgListRSE" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClientClick="GetListRSE(); return false;" Style="cursor: hand"
                    ToolTip="Buscar" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de solicitud inicial *
            </td>
            <td>
                Acta, correo o Nro. Radicado *
                <asp:RequiredFieldValidator runat="server" ID="rfvActaCorreoNoRadicado" ControlToValidate="txtActaCorreoNoRadicado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="txtFechaSolicitudInicial" runat="server"  Requerid="True" Width="90%"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtActaCorreoNoRadicado" MaxLength="100" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftActaCorreoNoRadicado" runat="server" TargetControlID="txtActaCorreoNoRadicado"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nombre abreviado *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreAbreviado" ControlToValidate="txtNombreAbreviado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Vigencia(s) de ejecución *
                <asp:RequiredFieldValidator runat="server" ID="rfvVigenciasEjecucion" ControlToValidate="txtVigenciasEjecucion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>            
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreAbreviado" runat="server" TargetControlID="txtNombreAbreviado"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVigenciasEjecucion" MaxLength="15" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftVigenciasEjecucion" runat="server" TargetControlID="txtVigenciasEjecucion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>            
        </tr>
        <tr class="rowB">
            <td>
                Número de reproceso *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroReproceso" ControlToValidate="ddlNumeroReproceso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvNumeroReproceso" ControlToValidate="ddlNumeroReproceso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Objeto *
                <asp:RequiredFieldValidator runat="server" ID="rfvObjeto" ControlToValidate="txtObjeto"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>            
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlNumeroReproceso" AutoPostBack="true" OnSelectedIndexChanged="ddlNumeroReproceso_SelectedIndexChanged" Width="90%">
                  <asp:ListItem Selected="True" Value="-1"> SELECCIONE </asp:ListItem>
                  <asp:ListItem Value="D1"> D1 </asp:ListItem>
                  <asp:ListItem Value="D2"> D2 </asp:ListItem>
                  <asp:ListItem Value="D3"> D3 </asp:ListItem>
                  <asp:ListItem Value="D4"> D4 </asp:ListItem>
                  <asp:ListItem Value="D5"> D5 </asp:ListItem>
                  <asp:ListItem Value="D6"> D6 </asp:ListItem>
                  <asp:ListItem Value="D7"> D7 </asp:ListItem>
                  <asp:ListItem Value="D8"> D8 </asp:ListItem>
                  <asp:ListItem Value="D9"> D9 </asp:ListItem>
                  <asp:ListItem Value="E1"> E1 </asp:ListItem>
                  <asp:ListItem Value="E2"> E2 </asp:ListItem>
                  <asp:ListItem Value="E3"> E3 </asp:ListItem>
                  <asp:ListItem Value="E4"> E4 </asp:ListItem>
                  <asp:ListItem Value="E5"> E5 </asp:ListItem>
                  <asp:ListItem Value="E6"> E6 </asp:ListItem>
                  <asp:ListItem Value="E7"> E7 </asp:ListItem>
                  <asp:ListItem Value="E8"> E8 </asp:ListItem>
                  <asp:ListItem Value="E9"> E9 </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtObjeto"  TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjeto" runat="server" TargetControlID="txtObjeto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>            
        </tr>
        <tr class="rowB">
            <td>
                Reproceso de DT al que se vincula la solicitud de estudio *
                <asp:RequiredFieldValidator runat="server" ID="rfvReprocesoVincula" ControlToValidate="txtReprocesoVincula"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                ¿Cuenta con Vigencias Futuras?
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtReprocesoVincula" MaxLength="15" Width="90%"></asp:TextBox>
            </td> 
            <td>
                <asp:DropDownList runat="server" ID="ddlCuentaVigenciasFuturasPACCO" Width="90%">
                  <asp:ListItem Selected="True" Value="-1"> SELECCIONE </asp:ListItem>
                  <asp:ListItem Value="Si"> NO </asp:ListItem>
                  <asp:ListItem Value="No"> SI </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                ¿Se busca suscribir un contrato? *
                <asp:RequiredFieldValidator runat="server" ID="rfvAplicaProcesoSeleccion" ControlToValidate="ddlAplicaProcesoSeleccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvAplicaProcesoSeleccion" ControlToValidate="ddlAplicaProcesoSeleccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Modalidad de contratación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlAplicaProcesoSeleccion" Width="90%">
                  <asp:ListItem Selected="True" Value="-1"> SELECCIONE </asp:ListItem>
                  <asp:ListItem Value="Si"> NO </asp:ListItem>
                  <asp:ListItem Value="No"> SI </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo de Solicitud *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoEstudio" ControlToValidate="ddlIdTipoEstudio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoEstudio" ControlToValidate="ddlIdTipoEstudio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Complejidad Interna *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdComplejidadInterna" ControlToValidate="ddlIdComplejidadInterna"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdComplejidadInterna" ControlToValidate="ddlIdComplejidadInterna"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoEstudio" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadInterna" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Complejidad Indicador *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdComplejidadIndicador" ControlToValidate="ddlIdComplejidadIndicador"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdComplejidadIndicador" ControlToValidate="ddlIdComplejidadIndicador"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Responsable DT o ES *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdResponsableES" ControlToValidate="ddlIdResponsableES"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdResponsableES" ControlToValidate="ddlIdResponsableES"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadIndicador" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdResponsableES" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Responsable EC *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdResponsableEC" ControlToValidate="ddlIdResponsableEC"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdResponsableEC" ControlToValidate="ddlIdResponsableEC"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Ordenador del gasto
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdResponsableEC" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlOrdenadorGasto" Width="90%">
                  <asp:ListItem Selected="True" Value="-1"> SELECCIONE </asp:ListItem>
                  <asp:ListItem Value="Delegación Especial"> DELEGACI&Oacute;N ESPECIAL </asp:ListItem>
                  <asp:ListItem Value="Director(a) General"> DIRECTOR(A) GENERAL </asp:ListItem>                  
                  <asp:ListItem Value="Director(a) Regional"> DIRECTOR(A) REGIONAL </asp:ListItem>                  
                  <asp:ListItem Value="No aplica"> NO APLICA </asp:ListItem>
                  <asp:ListItem Value="Secretario(a) General"> SECRETARIO(A) GENERAL </asp:ListItem>
                  <asp:ListItem Value="Subdirector(a) General"> SUBDIRECTOR(A) GENERAL </asp:ListItem>                  
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdEstadoSolicitud" ControlToValidate="ddlIdEstadoSolicitud"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdEstadoSolicitud" ControlToValidate="ddlIdEstadoSolicitud"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Motivo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" AutoPostBack="true" OnSelectedIndexChanged="ddlIdEstadoSolicitud_SelectedIndexChanged" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdMotivoSolicitud" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Dependencia solicitante *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccionSolicitante" ControlToValidate="ddlIdDireccionSolicitante"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdDireccionSolicitante" ControlToValidate="ddlIdDireccionSolicitante"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Área solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Width="90%" AutoPostBack="true" OnSelectedIndexChanged="ddlIdDireccionSolicitante_SelectedIndexChanged"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdAreaSolicitante" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo de Valor
            </td>
            <td>
                Valor presupuesto estimado solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoValor" Width="90%">
                  <asp:ListItem Selected="True" Value="-1"> SELECCIONE </asp:ListItem>
                  <asp:ListItem Value="No aplica"> NO APLICA </asp:ListItem>
                  <asp:ListItem Value="Vr total"> VR TOTAL </asp:ListItem>   
                  <asp:ListItem Value="Vr unitario"> VR UNITARIO </asp:ListItem>                                 
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorPresupuestoEstimadoSolicitante" MaxLength="29" Width="90%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorPresupuestoEstimadoSolicitante" runat="server" TargetControlID="txtValorPresupuestoEstimadoSolicitante"
                    FilterType="Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
    </table>
    
    <script type="text/javascript" language="javascript">
        function GetListRSE() {
            muestraImagenLoading();
            document.getElementById("txtConsecutivoEstudioRelacionado").disabled = false;
            window_showModalDialog('../../../Page/EstudioSectorCosto/Lupas/LupaListRSE.aspx', setReturnGetListRSE, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            ocultaImagenLoading();
        }

        function setReturnGetListRSE(dialog) {
            var pObj = window_returnModalDialog(dialog);

            if (pObj != undefined && pObj != null && pObj != '') {
                var retLupa = pObj.split(",");
                $('#txtConsecutivoEstudioRelacionado').val(retLupa[0]);

                __doPostBack("txtConsecutivoEstudioRelacionado", "TextChanged");
            }
            else
            {
                document.getElementById("txtConsecutivoEstudioRelacionado").disabled = true;
            }
        }

        function GetInfoPACCO() {

            muestraImagenLoading();

            document.getElementById("txtConsecutivoPACCO").disabled = false;
            document.getElementById("txtVigenciaPACCO").disabled = false;
            document.getElementById("txtObjetoPACCO").disabled = false;
            $('#ddlIdDireccionSolicitantePacco').disabled = false;
            $('#ddlIdModalidadSeleccionPACCO').disabled = false;
            $('#chkAplicaPacco').disabled = false;
            document.getElementById("txtValorPresupuestalPACCO").disabled = false;
            window_showModalDialog('../../../Page/EstudioSectorCosto/Lupas/LupaInfoPACCO.aspx', setReturnGetInfoPACCO, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            ocultaImagenLoading();
        }

        function setReturnGetInfoPACCO(dialog) {
            
            var pObj = window_returnModalDialog(dialog);

            if (pObj != undefined && pObj != null && pObj != '') {
                var retLupa = pObj.split(",");
                console.log(retLupa[3]);
                $('#txtConsecutivoPACCO').val(retLupa[0]);                
                $('#txtVigenciaPACCO').val(retLupa[1]);                
                $('#txtObjetoPACCO').val(retLupa[2]);
                document.getElementById('<%= hfIdDireccion.ClientID %>').value = retLupa[3];
                $('#txtValorPresupuestalPACCO').val(retLupa[4]);
                document.getElementById('<%= hfIdModalidad.ClientID %>').value = retLupa[5];

                __doPostBack("txtConsecutivoPACCO", "TextChanged");
            }
            else
            {
                document.getElementById("txtConsecutivoPACCO").disabled = true;
                document.getElementById("txtVigenciaPACCO").disabled = true;
                document.getElementById("txtObjetoPACCO").disabled = true;
                $('#ddlIdDireccionSolicitantePacco').disabled = true;
                $('#ddlIdModalidadSeleccionPACCO').disabled = true;
                $('#chkAplicaPacco').disabled = true;
                document.getElementById("txtValorPresupuestalPACCO").disabled = true;
            }
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.display = 'none';
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
</asp:Content>
