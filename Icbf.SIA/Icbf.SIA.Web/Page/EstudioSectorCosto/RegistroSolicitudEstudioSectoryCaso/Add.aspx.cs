using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using WsContratosPacco;

public partial class Page_RegistroSolicitudEstudioSectoryCaso_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/RegistroSolicitudEstudioSectoryCaso";
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();

            String vActaCorreo = txtActaCorreoNoRadicado.Text.ToUpper();
            String vObjeto = txtObjeto.Text.ToUpper();
            String vNombreAbreviado = txtNombreAbreviado.Text.ToUpper();
            String vNumeroReproceso = "";
            String vVigenciasEjecucion = txtVigenciasEjecucion.Text.ToUpper();
            String vReprocesoVincula = txtReprocesoVincula.Text.ToUpper();
            Int32 vConsecutivoPACCO = 0;
            if (txtConsecutivoPACCO.Text != "")
            {
                vConsecutivoPACCO = Convert.ToInt32(txtConsecutivoPACCO.Text);
            }
            if (ddlNumeroReproceso.SelectedValue != "-1")
            {
                vNumeroReproceso = Convert.ToString(ddlNumeroReproceso.SelectedValue);
            }

            //Información PACCO
            int AplicaPACCO = 0;
            if (chkAplicaPacco.Checked) AplicaPACCO = 1;
            vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO = AplicaPACCO;
            vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO = vConsecutivoPACCO;
            vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO = txtObjetoPACCO.Text;

            if (txtValorPresupuestalPACCO.Text != "")
            {
                vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO = Convert.ToDecimal(txtValorPresupuestalPACCO.Text);
            }

            vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO = txtVigenciaPACCO.Text;

            if (ddlIdDireccionSolicitantePacco.SelectedValue != "-1")
            {
                vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO = Convert.ToInt32(ddlIdDireccionSolicitantePacco.SelectedValue);
            }

            if (ddlIdModalidadSeleccionPACCO.SelectedValue != "-1")
            {
                vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO = Convert.ToInt32(ddlIdModalidadSeleccionPACCO.SelectedValue);
            }

            //Registro Inicial
            if (txtConsecutivoEstudioRelacionado.Text != "") vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado = Convert.ToInt32(txtConsecutivoEstudioRelacionado.Text);
            vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial = txtFechaSolicitudInicial.Date;
            vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado = vActaCorreo;
            vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado = vNombreAbreviado;
            vRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion = vVigenciasEjecucion;
            vRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula = vReprocesoVincula;
            vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso = vNumeroReproceso;
            vRegistroSolicitudEstudioSectoryCaso.Objeto = vObjeto;
            if (ddlCuentaVigenciasFuturasPACCO.SelectedValue != "-1") vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO = Convert.ToString(ddlCuentaVigenciasFuturasPACCO.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion = Convert.ToString(ddlAplicaProcesoSeleccion.SelectedValue);
            if (ddlIdModalidadSeleccion.SelectedValue != "-1") vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio = Convert.ToInt32(ddlIdTipoEstudio.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna = Convert.ToInt32(ddlIdComplejidadInterna.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador = Convert.ToInt32(ddlIdComplejidadIndicador.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdResponsableES = Convert.ToInt32(ddlIdResponsableES.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC = Convert.ToInt32(ddlIdResponsableEC.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto = Convert.ToString(ddlOrdenadorGasto.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud = Convert.ToInt32(ddlIdEstadoSolicitud.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud = Convert.ToInt32(ddlIdMotivoSolicitud.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante = Convert.ToInt32(ddlIdAreaSolicitante.SelectedValue);
            vRegistroSolicitudEstudioSectoryCaso.TipoValor = Convert.ToString(ddlTipoValor.SelectedValue);
            Decimal vValorPresupuesto = 0;
            if (txtValorPresupuestoEstimadoSolicitante.Text != "") vValorPresupuesto = Convert.ToDecimal(txtValorPresupuestoEstimadoSolicitante.Text);
            vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante = vValorPresupuesto;

            if (Request.QueryString["oP"] == "E")
            {
                if (txtConsecutivoEstudio.Text != txtConsecutivoEstudioRelacionado.Text)
                {
                    vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso = Convert.ToInt32(hfIdRegistroSolicitudEstudioSectoryCaso.Value);
                    vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vRegistroSolicitudEstudioSectoryCaso, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarRegistroSolicitudEstudioSectoryCaso(vRegistroSolicitudEstudioSectoryCaso);
                }
                else
                {
                    vResultado = -2;
                }
            }
            else
            {
                List<RegistroSolicitudEstudioSectoryCaso> ListaSolicitudes = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCasos(null, null, null, null, null, null, null, null, null, txtFechaSolicitudInicial.Date, null, vNombreAbreviado, vNumeroReproceso, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                if (ListaSolicitudes.Count == 0)
                {
                    vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vRegistroSolicitudEstudioSectoryCaso, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarRegistroSolicitudEstudioSectoryCaso(vRegistroSolicitudEstudioSectoryCaso);
                }
                else
                {
                    vResultado = -1;
                }
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso", vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso);
                SetSessionParameter("RegistroSolicitudEstudioSectoryCaso.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado == -1)
            {
                toolBar.MostrarMensajeError("El registro ya existe.");
            }
            else if (vResultado == -2)
            {
                toolBar.MostrarMensajeError("Un estudio no puede relacionarse consigo mismo.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            txtConsecutivoPACCO.Enabled = false;
            ddlIdDireccionSolicitantePacco.Enabled = false;
            txtObjetoPACCO.Enabled = false;
            txtValorPresupuestalPACCO.Enabled = false;
            txtVigenciaPACCO.Enabled = false;
            txtConsecutivoEstudioRelacionado.Enabled = false;
            ddlIdModalidadSeleccionPACCO.Enabled = false;

            if (Request.QueryString["oP"] == "E")
            {
                chkAplicaPacco.Enabled = false;
                toolBar.ConfirmarAccion("btnGuardar", "Cualquier modificaci\u00F3n en los campos Modalidad, Complejidad Interna, Complejidad Indicador, Valor presupuesto estimado solicitante, afectan las fechas de entrega. ¿Seguro que desea continuar?");
            }

            toolBar.EstablecerTitulos("Registro Inicial", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdRegistroSolicitudEstudioSectoryCaso = Convert.ToInt32(GetSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso"));
            RemoveSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso");

            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
            vRegistroSolicitudEstudioSectoryCaso = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCaso(vIdRegistroSolicitudEstudioSectoryCaso);
            hfIdRegistroSolicitudEstudioSectoryCaso.Value = vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso.ToString();

            bool AplicaPacco = false;
            if (vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO == 1) AplicaPacco = true;

            if (AplicaPacco)
            {
                rfvVigenciaPACCO.Enabled = false;
                imgGetInfoPACCO.Enabled = false;
            }
            else
            {
                rfvVigenciaPACCO.Enabled = true;
                imgGetInfoPACCO.Enabled = true;
            }

            //Información PACCO
            chkAplicaPacco.Checked = AplicaPacco;
            txtConsecutivoPACCO.Text = vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO.ToString();
            String VigenciaPACCO = vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;
            txtVigenciaPACCO.Text = VigenciaPACCO;

            if (!string.IsNullOrEmpty(VigenciaPACCO))
            {
                vManejoControlesContratos.LlenarDireccionPACCO(ddlIdDireccionSolicitantePacco, null, true, Convert.ToInt32(VigenciaPACCO));
                ddlIdDireccionSolicitantePacco.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO.ToString();

                vManejoControlesContratos.LlenarModalidadPACCO(ddlIdModalidadSeleccionPACCO, null, true, Convert.ToInt32(VigenciaPACCO));
                ddlIdModalidadSeleccionPACCO.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO.ToString();
            }
            txtObjetoPACCO.Text = vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO;
            txtValorPresupuestalPACCO.Text = vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO.ToString();

            //Registro Inicial
            txtConsecutivoEstudio.Text = vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso.ToString();
            txtConsecutivoEstudioRelacionado.Text = vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado.ToString();
            txtFechaSolicitudInicial.Date = Convert.ToDateTime(vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial);
            txtActaCorreoNoRadicado.Text = vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado;
            txtNombreAbreviado.Text = vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado;
            txtReprocesoVincula.Text = vRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula;
            txtVigenciasEjecucion.Text = vRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion;
            ddlNumeroReproceso.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso;
            txtObjeto.Text = vRegistroSolicitudEstudioSectoryCaso.Objeto;
            ddlCuentaVigenciasFuturasPACCO.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO;
            ddlAplicaProcesoSeleccion.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion;
            ddlIdModalidadSeleccion.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion.ToString();
            ddlIdTipoEstudio.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio.ToString();
            ddlIdComplejidadInterna.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna.ToString();
            ddlIdComplejidadIndicador.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador.ToString();
            ddlIdResponsableES.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdResponsableES.ToString();
            ddlIdResponsableEC.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC.ToString();
            ddlOrdenadorGasto.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto;
            ddlIdEstadoSolicitud.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud.ToString();

            vManejoControlesContratos.LlenarMotivoSolicitud(ddlIdMotivoSolicitud, null, true, ddlIdEstadoSolicitud.SelectedItem.Text);
            ddlIdMotivoSolicitud.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud.ToString();

            ddlIdDireccionSolicitante.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante.ToString();

            vManejoControlesContratos.LlenarAreas(ddlIdAreaSolicitante, null, true, Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue));
            ddlIdAreaSolicitante.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante.ToString();

            ddlTipoValor.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.TipoValor;
            txtValorPresupuestoEstimadoSolicitante.Text = vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea, vRegistroSolicitudEstudioSectoryCaso.FechaCrea, vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica, vRegistroSolicitudEstudioSectoryCaso.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            vManejoControlesContratos = new ManejoControlesContratos();

            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadInterna(ddlIdComplejidadInterna, null, true);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableES, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableEC, null, true);
            vManejoControlesContratos.LlenarEstadoEstudio(ddlIdEstadoSolicitud, null, true);
            vManejoControlesContratos.LlenarMotivoSolicitud(ddlIdMotivoSolicitud, null, true, null);
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitante, null, true);
            vManejoControlesContratos.LlenarAreas(ddlIdAreaSolicitante, null, true, 0);
            vManejoControlesContratos.LlenarTiposEstudio(ddlIdTipoEstudio, null, true);

            vManejoControlesContratos.LlenarDireccionPACCO(ddlIdDireccionSolicitantePacco, null, true, 0);
            vManejoControlesContratos.LlenarModalidadPACCO(ddlIdModalidadSeleccionPACCO, null, true, 0);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void chkAplicaPacco_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAplicaPacco.Checked == true)
        {
            imgGetInfoPACCO.Enabled = false;
            txtConsecutivoPACCO.Enabled = false;
            ddlIdDireccionSolicitantePacco.Enabled = false;
            ddlIdModalidadSeleccionPACCO.Enabled = false;
            txtObjetoPACCO.Enabled = false;
            txtValorPresupuestalPACCO.Enabled = false;
            txtVigenciaPACCO.Enabled = false;
            rfvVigenciaPACCO.Enabled = false;

            txtConsecutivoPACCO.Text = string.Empty;
            ddlIdDireccionSolicitantePacco.SelectedValue = "-1";
            ddlIdModalidadSeleccionPACCO.SelectedValue = "-1";
            txtObjetoPACCO.Text = string.Empty;
            txtValorPresupuestalPACCO.Text = string.Empty;
            txtVigenciaPACCO.Text = string.Empty;
        }
        else
        {
            imgGetInfoPACCO.Enabled = true;
            rfvVigenciaPACCO.Enabled = true;
        }
    }
    protected void ddlIdEstadoSolicitud_SelectedIndexChanged(object sender, EventArgs e)
    {
        String Estado = ddlIdEstadoSolicitud.SelectedItem.ToString();

        vManejoControlesContratos = new ManejoControlesContratos();
        vManejoControlesContratos.LlenarMotivoSolicitud(ddlIdMotivoSolicitud, null, true, Estado);
    }
    protected void ddlNumeroReproceso_SelectedIndexChanged(object sender, EventArgs e)
    {
        String Numero = ddlNumeroReproceso.SelectedItem.ToString();

        if (Numero==" E1 ")
        {
            rfvConsecutivoEstudioRelacionado.Enabled = false;
            ddlAplicaProcesoSeleccion.SelectedIndex = -1;
            //ddlAplicaProcesoSeleccion.Enabled = true;
            txtReprocesoVincula.Enabled = true;
            rfvReprocesoVincula.Enabled = true;

        }
        else if (Numero==" D1 "|| Numero==" D2 " || Numero==" D3 " || Numero==" D4 " || Numero==" D5 " || Numero==" D6 " || Numero==" D7 " || Numero==" D8 " || Numero==" D9 " )
        {
            ddlAplicaProcesoSeleccion.SelectedIndex = 1;
            //ddlAplicaProcesoSeleccion.Enabled = false;       
            txtReprocesoVincula.Text = "No aplica";
            txtReprocesoVincula.Enabled = false;
            rfvReprocesoVincula.Enabled = false;
        }
        else
        {
            txtReprocesoVincula.Text = "";
            rfvConsecutivoEstudioRelacionado.Enabled = true;
            ddlAplicaProcesoSeleccion.SelectedIndex = -1;
            //ddlAplicaProcesoSeleccion.Enabled = true;      
            txtReprocesoVincula.Enabled = true;
            rfvReprocesoVincula.Enabled = true;
        }


    }
    protected void txtConsecutivoPACCO_TextChanged(object sender, EventArgs e)
    {
        if (txtConsecutivoPACCO.Text != "")
        {
            vManejoControlesContratos = new ManejoControlesContratos();
            int pVigencia = Convert.ToInt32(txtVigenciaPACCO.Text);
            vManejoControlesContratos.LlenarDireccionPACCO(ddlIdDireccionSolicitantePacco, null, true, pVigencia);
            vManejoControlesContratos.LlenarModalidadPACCO(ddlIdModalidadSeleccionPACCO, null, true, pVigencia);


            ListItem vValue = ddlIdDireccionSolicitantePacco.Items.FindByValue(hfIdDireccion.Value);
            if (vValue != null)
            {
                ddlIdDireccionSolicitantePacco.SelectedValue = hfIdDireccion.Value;
            }
            else
            {
                ddlIdDireccionSolicitantePacco.SelectedValue = "-1";
            }

            ListItem vValueModalidad = ddlIdModalidadSeleccionPACCO.Items.FindByValue(hfIdModalidad.Value);
            if (vValueModalidad != null)
            {
                ddlIdModalidadSeleccionPACCO.SelectedValue = hfIdModalidad.Value;
            }
            else
            {
                ddlIdModalidadSeleccionPACCO.SelectedValue = "-1";
            }
            

            chkAplicaPacco.Enabled = false;
            txtConsecutivoPACCO.Enabled = false;
            txtVigenciaPACCO.Enabled = false;
            txtObjetoPACCO.Enabled = false;
            txtValorPresupuestalPACCO.Enabled = false;
            ddlIdDireccionSolicitantePacco.Enabled = false;
            ddlIdModalidadSeleccionPACCO.Enabled = false;
        }
        else
        {
            ddlIdDireccionSolicitantePacco.SelectedValue = "-1";
            ddlIdModalidadSeleccionPACCO.SelectedValue = "-1";
            chkAplicaPacco.Enabled = true;
            txtConsecutivoPACCO.Enabled = true;
            txtVigenciaPACCO.Enabled = true;
            txtObjetoPACCO.Enabled = true;
            txtValorPresupuestalPACCO.Enabled = true;
            ddlIdDireccionSolicitantePacco.Enabled = true;
            ddlIdModalidadSeleccionPACCO.Enabled = true;
        }
    }

    protected void txtConsecutivoEstudioRelacionado_TextChanged(object sender, EventArgs e)
    {
        txtConsecutivoEstudioRelacionado.Enabled = false;
    }

    protected void ddlIdDireccionSolicitante_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 Direccion = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);

        vManejoControlesContratos = new ManejoControlesContratos();
        vManejoControlesContratos.LlenarAreas(ddlIdAreaSolicitante, null, true, Direccion);
    }
}
