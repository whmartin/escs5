using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;
using System.Data;

public partial class Page_RegistroSolicitudEstudioSectoryCaso_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/RegistroSolicitudEstudioSectoryCaso";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtConsecutivoEstudio.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<RegistroSolicitudEstudioSectoryCaso> Buscar()
    {
        try
        {
            Int32? vConsecutivoEstudio = null;
            int? vAplicaPACCO = null;
            int? vConsecutivoPACCO = null;
            int? vDireccionsolicitantePACCO = null;
            String vObjetoPACCO = null;
            int? vModalidadPACCO = null;
            Decimal? vValorPresupuestalPACCO = null;
            String vVigenciaPACCO = null;
            int? vConsecutivoEstudioRelacionado = null;
            DateTime vFechaSolicitudInicial = Convert.ToDateTime("0009-01-01");
            String vActaCorreoNoRadicado = null;
            String vNombreAbreviado = null;
            String vNumeroReproceso = null;
            String vObjeto = null;
            String vCuentaVigenciasFuturasPACCO = null;
            String vAplicaProcesoSeleccion = null;
            int? vIdModalidadSeleccion = null;
            int? vIdTipoEstudio = null;
            int? vIdComplejidadInterna = null;
            int? vIdComplejidadIndicador = null;
            int? vIdResponsableES = null;
            int? vIdResponsableEC = null;
            String vOrdenadorGasto = null;
            int? vIdEstadoSolicitud = null;
            int? vIdMotivoSolicitud = null;
            int? vIdDireccionSolicitante = null;
            int? vIdAreaSolicitante = null;
            String vTipoValor = null;
            int? vValorPresupuestoEstimadoSolicitante = null;
            if (txtConsecutivoEstudio.Text != "")
            {
                vConsecutivoEstudio = Convert.ToInt32(txtConsecutivoEstudio.Text);
            }
            if (ddlIdFechaSolicitudInicial.SelectedValue != "-1")
            {
                vFechaSolicitudInicial = Convert.ToDateTime(ddlIdFechaSolicitudInicial.SelectedValue + "-01-01");
            }
            if (txtNombreAbreviado.Text != "")
            {
                vNombreAbreviado = Convert.ToString(txtNombreAbreviado.Text);
            }
            if (txtObjeto.Text != "")
            {
                vObjeto = Convert.ToString(txtObjeto.Text);
            }
            if (ddlIdModalidadSeleccion.SelectedValue != "-1")
            {
                vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            }
            if (ddlIdResponsableES.SelectedValue != "-1")
            {
                vIdResponsableES = Convert.ToInt32(ddlIdResponsableES.SelectedValue);
            }
            if (ddlIdResponsableEC.SelectedValue != "-1")
            {
                vIdResponsableEC = Convert.ToInt32(ddlIdResponsableEC.SelectedValue);
            }
            if (ddlIdEstadoSolicitud.SelectedValue != "-1")
            {
                vIdEstadoSolicitud = Convert.ToInt32(ddlIdEstadoSolicitud.SelectedValue);
            }
            if (ddlIdDireccionSolicitante.SelectedValue != "-1")
            {
                vIdDireccionSolicitante = Convert.ToInt32(ddlIdDireccionSolicitante.SelectedValue);
            }
            List<RegistroSolicitudEstudioSectoryCaso> ListaRegistroSolicitudEstudioSectoryCaso = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCasos(vConsecutivoEstudio, vAplicaPACCO, vConsecutivoPACCO, vDireccionsolicitantePACCO, vObjetoPACCO, vModalidadPACCO, vValorPresupuestalPACCO, vVigenciaPACCO, vConsecutivoEstudioRelacionado, vFechaSolicitudInicial, vActaCorreoNoRadicado, vNombreAbreviado, vNumeroReproceso, vObjeto, vCuentaVigenciasFuturasPACCO, vAplicaProcesoSeleccion, vIdModalidadSeleccion, vIdTipoEstudio, vIdComplejidadInterna, vIdComplejidadIndicador, vIdResponsableES, vIdResponsableEC, vOrdenadorGasto, vIdEstadoSolicitud, vIdMotivoSolicitud, vIdDireccionSolicitante, vIdAreaSolicitante, vTipoValor, vValorPresupuestoEstimadoSolicitante);
            return ListaRegistroSolicitudEstudioSectoryCaso;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvRegistroSolicitudEstudioSectoryCaso.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvRegistroSolicitudEstudioSectoryCaso.PageSize = PageSize();
            gvRegistroSolicitudEstudioSectoryCaso.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registro Inicial", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRegistroSolicitudEstudioSectoryCaso.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRegistroSolicitudEstudioSectoryCaso.SelectedRow);
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroSolicitudEstudioSectoryCaso.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("RegistroSolicitudEstudioSectoryCaso.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("RegistroSolicitudEstudioSectoryCaso.Eliminado");

            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableES, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableEC, null, true);
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarFechaSolicitudInicial(ddlIdFechaSolicitudInicial, null, true);
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitante, null, true);
            vManejoControlesContratos.LlenarEstadoEstudio(ddlIdEstadoSolicitud, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvRegistroSolicitudEstudioSectoryCaso.AllowPaging;
        Boolean vPaginadorError = gvRegistroSolicitudEstudioSectoryCaso.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvRegistroSolicitudEstudioSectoryCaso.AllowPaging = false;

        List<RegistroSolicitudEstudioSectoryCaso> vListaSolicitudes = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvRegistroSolicitudEstudioSectoryCaso, vListaSolicitudes);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvRegistroSolicitudEstudioSectoryCaso.Columns.Count; i++)
                {
                    dt.Columns.Add(gvRegistroSolicitudEstudioSectoryCaso.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvRegistroSolicitudEstudioSectoryCaso.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvRegistroSolicitudEstudioSectoryCaso.Columns.Count; j++)
                    {
                        dr[gvRegistroSolicitudEstudioSectoryCaso.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "RegistroInicial");
                gvRegistroSolicitudEstudioSectoryCaso.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }

    protected List<RegistroSolicitudEstudioSectoryCaso> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<RegistroSolicitudEstudioSectoryCaso> listaSolicitudes = Buscar();
        gvRegistroSolicitudEstudioSectoryCaso.DataSource = listaSolicitudes;
        gvRegistroSolicitudEstudioSectoryCaso.DataBind();

        return listaSolicitudes;
    }
    protected void gvRegistroSolicitudEstudioSectoryCaso_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }


    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el c�digo de llenado de datos para la grilla 
            List<RegistroSolicitudEstudioSectoryCaso> vListaRegistroSolicitudEstudioSectoryCasos = Buscar();

            //Fin del c�digo de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (vListaRegistroSolicitudEstudioSectoryCasos != null)
                {
                    var param = Expression.Parameter(typeof(RegistroSolicitudEstudioSectoryCaso), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<RegistroSolicitudEstudioSectoryCaso, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = vListaRegistroSolicitudEstudioSectoryCasos;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
