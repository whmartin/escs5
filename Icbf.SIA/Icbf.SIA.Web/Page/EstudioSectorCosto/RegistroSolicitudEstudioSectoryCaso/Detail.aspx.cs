using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_RegistroSolicitudEstudioSectoryCaso_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/RegistroSolicitudEstudioSectoryCaso";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso", hfIdRegistroSolicitudEstudioSectoryCaso.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdRegistroSolicitudEstudioSectoryCaso = Convert.ToInt32(GetSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso"));
            RemoveSessionParameter("RegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso");

            if (GetSessionParameter("RegistroSolicitudEstudioSectoryCaso.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("RegistroSolicitudEstudioSectoryCaso.Guardado");

            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
            vRegistroSolicitudEstudioSectoryCaso = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCaso(vIdRegistroSolicitudEstudioSectoryCaso);
            hfIdRegistroSolicitudEstudioSectoryCaso.Value = vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso.ToString();

            bool AplicaPacco = false;
            if (vRegistroSolicitudEstudioSectoryCaso.AplicaPACCO == 1) AplicaPacco = true;

            //Información PACCO
            chkAplicaPacco.Checked = AplicaPacco;
            txtConsecutivoPACCO.Text = vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO == 0 ? string.Empty : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoPACCO.ToString();
            String VigenciaPACCO = vRegistroSolicitudEstudioSectoryCaso.VigenciaPACCO;
            txtVigenciaPACCO.Text = VigenciaPACCO;

            if (!string.IsNullOrEmpty(VigenciaPACCO))
            {
                vManejoControlesContratos.LlenarDireccionPACCO(ddlIdDireccionSolicitantePacco, null, true, Convert.ToInt32(VigenciaPACCO));
                ddlIdDireccionSolicitantePacco.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.DireccionsolicitantePACCO.ToString();

                vManejoControlesContratos.LlenarModalidadPACCO(ddlIdModalidadSeleccionPACCO, null, true, Convert.ToInt32(VigenciaPACCO));
                ddlIdModalidadSeleccionPACCO.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.ModalidadPACCO.ToString();
            }

            txtObjetoPACCO.Text = vRegistroSolicitudEstudioSectoryCaso.ObjetoPACCO;
            txtValorPresupuestalPACCO.Text = Convert.ToDecimal(vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO) == 0 ? string.Empty : string.Format("${0:#,##0}", vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestalPACCO);

            //Registro Inicial
            txtConsecutivoEstudioRelacionado.Text = vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado == 0 ? string.Empty : vRegistroSolicitudEstudioSectoryCaso.ConsecutivoEstudioRelacionado.ToString();
            txtConsecutivoEstudio.Text = vRegistroSolicitudEstudioSectoryCaso.IdRegistroSolicitudEstudioSectoryCaso.ToString();
            txtFechaSolicitudInicial.Text = vRegistroSolicitudEstudioSectoryCaso.FechaSolicitudInicial.ToString();
            txtActaCorreoNoRadicado.Text = vRegistroSolicitudEstudioSectoryCaso.ActaCorreoNoRadicado;
            txtNombreAbreviado.Text = vRegistroSolicitudEstudioSectoryCaso.NombreAbreviado;
            txtReprocesoVincula.Text = vRegistroSolicitudEstudioSectoryCaso.ReprocesoVincula;
            txtVigenciasEjecucion.Text = vRegistroSolicitudEstudioSectoryCaso.VigenciasEjecucion;
            ddlNumeroReproceso.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.NumeroReproceso;
            txtObjeto.Text = vRegistroSolicitudEstudioSectoryCaso.Objeto;
            ddlCuentaVigenciasFuturasPACCO.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.CuentaVigenciasFuturasPACCO;
            ddlAplicaProcesoSeleccion.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.AplicaProcesoSeleccion;
            ddlIdModalidadSeleccion.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdModalidadSeleccion.ToString();
            ddlIdTipoEstudio.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdTipoEstudio.ToString();
            ddlIdComplejidadInterna.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdComplejidadInterna.ToString();
            ddlIdComplejidadIndicador.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdComplejidadIndicador.ToString();
            ddlIdResponsableES.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdResponsableES.ToString();
            ddlIdResponsableEC.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdResponsableEC.ToString();
            ddlOrdenadorGasto.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.OrdenadorGasto;
            ddlIdEstadoSolicitud.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdEstadoSolicitud.ToString();

            vManejoControlesContratos.LlenarMotivoSolicitud(ddlIdMotivoSolicitud, null, true, ddlIdEstadoSolicitud.SelectedItem.Text);

            ddlIdMotivoSolicitud.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdMotivoSolicitud.ToString();
            ddlIdDireccionSolicitante.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdDireccionSolicitante.ToString();
            ddlIdAreaSolicitante.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.IdAreaSolicitante.ToString();
            ddlTipoValor.SelectedValue = vRegistroSolicitudEstudioSectoryCaso.TipoValor;
            txtValorPresupuestoEstimadoSolicitante.Text = Convert.ToDecimal(vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante) == 0 ? string.Empty : string.Format("${0:#,##0}", vRegistroSolicitudEstudioSectoryCaso.ValorPresupuestoEstimadoSolicitante);
            ObtenerAuditoria(PageName, hfIdRegistroSolicitudEstudioSectoryCaso.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRegistroSolicitudEstudioSectoryCaso.UsuarioCrea, vRegistroSolicitudEstudioSectoryCaso.FechaCrea, vRegistroSolicitudEstudioSectoryCaso.UsuarioModifica, vRegistroSolicitudEstudioSectoryCaso.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdRegistroSolicitudEstudioSectoryCaso = Convert.ToInt32(hfIdRegistroSolicitudEstudioSectoryCaso.Value);

            RegistroSolicitudEstudioSectoryCaso vRegistroSolicitudEstudioSectoryCaso = new RegistroSolicitudEstudioSectoryCaso();
            vRegistroSolicitudEstudioSectoryCaso = vEstudioSectorCostoService.ConsultarRegistroSolicitudEstudioSectoryCaso(vIdRegistroSolicitudEstudioSectoryCaso);
            InformacionAudioria(vRegistroSolicitudEstudioSectoryCaso, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarRegistroSolicitudEstudioSectoryCaso(vRegistroSolicitudEstudioSectoryCaso);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("RegistroSolicitudEstudioSectoryCaso.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Registro Inicial", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            vManejoControlesContratos = new ManejoControlesContratos();

            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadInterna(ddlIdComplejidadInterna, null, true);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableES, null, true);
            vManejoControlesContratos.LlenarResponsablesESCs(ddlIdResponsableEC, null, true);
            vManejoControlesContratos.LlenarEstadoEstudio(ddlIdEstadoSolicitud, null, true);
            vManejoControlesContratos.LlenarDireccion(ddlIdDireccionSolicitante, null, true);
            vManejoControlesContratos.LlenarAreas(ddlIdAreaSolicitante, null, true, null);
            vManejoControlesContratos.LlenarTiposEstudio(ddlIdTipoEstudio, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
