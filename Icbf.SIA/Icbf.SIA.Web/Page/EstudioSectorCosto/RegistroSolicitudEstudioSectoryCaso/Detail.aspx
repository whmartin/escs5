<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_RegistroSolicitudEstudioSectoryCaso_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdRegistroSolicitudEstudioSectoryCaso" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:CheckBox ID="chkAplicaPacco" runat="server" Enabled="false" />
                No Aplica PACCO
            </td>
            <td></td>
        </tr>
        <tr class="rowA" style="background-color: lightblue">
            <td colspan="2">
                <asp:Label runat="server" ID="Label1" Text="Información PACCO" Style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td>Consecutivo PACCO
            </td>
            <td>Dependencia solicitante PACCO
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoPACCO" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitantePacco" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Objeto PACCO
            </td>
            <td>Valor Presupuestal PACCO
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoPACCO" Enabled="false" TextMode="MultiLine" Rows="4" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorPresupuestalPACCO" Enabled="false" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Vigencia PACCO
            </td>
            <td>Modalidad de contratación PACCO         
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtVigenciaPACCO" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccionPACCO" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td></td>
            <td></td>
        </tr>
        <tr class="rowA" style="background-color: lightblue">
            <td colspan="2">
                <asp:Label runat="server" ID="lblTitulo2" class="success" Text="Registro Inicial" Style="font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr class="rowB">
            <td>Consecutivo solicitud
                *</td>
            <td>Consecutivo estudio relacionado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudioRelacionado" Enabled="false" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Fecha de solicitud inicial *
            </td>
            <td>Acta, correo o Nro. Radicado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaSolicitudInicial" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtActaCorreoNoRadicado" Enabled="false" Width="90%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Nombre abreviado *
            </td>
            <td>Vigencia(s) de ejecución *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbreviado" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVigenciasEjecucion" Enabled="false" Width="90%"></asp:TextBox>                
            </td>  
        </tr>
        <tr class="rowB">
            <td>Número de reproceso *
            </td>
            <td>Objeto *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlNumeroReproceso" Enabled="false" Width="90%">
                    <asp:ListItem Value="D1"> D1 </asp:ListItem>
                    <asp:ListItem Value="D2"> D2 </asp:ListItem>
                    <asp:ListItem Value="D3"> D3 </asp:ListItem>
                    <asp:ListItem Value="D4"> D4 </asp:ListItem>
                    <asp:ListItem Value="D5"> D5 </asp:ListItem>
                    <asp:ListItem Value="D6"> D6 </asp:ListItem>
                    <asp:ListItem Value="D7"> D7 </asp:ListItem>
                    <asp:ListItem Value="D8"> D8 </asp:ListItem>
                    <asp:ListItem Value="D9"> D9 </asp:ListItem>
                    <asp:ListItem Value="E1"> E1 </asp:ListItem>
                    <asp:ListItem Value="E2"> E2 </asp:ListItem>
                    <asp:ListItem Value="E3"> E3 </asp:ListItem>
                    <asp:ListItem Value="E4"> E4 </asp:ListItem>
                    <asp:ListItem Value="E5"> E5 </asp:ListItem>
                    <asp:ListItem Value="E6"> E6 </asp:ListItem>
                    <asp:ListItem Value="E7"> E7 </asp:ListItem>
                    <asp:ListItem Value="E8"> E8 </asp:ListItem>
                    <asp:ListItem Value="E9"> E9 </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtObjeto" Enabled="false" Width="90%"></asp:TextBox>
            </td>            
        </tr>
        <tr class="rowB">
            <td>
                Reproceso de DT al que se vincula la solicitud de estudio *
            </td>
            <td>
                ¿Cuenta con Vigencias Futuras?
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtReprocesoVincula" Enabled="false" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCuentaVigenciasFuturasPACCO" Enabled="false" Width="90%">
                    <asp:ListItem Selected="True" Value="-1"> Seleccione </asp:ListItem>
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>¿Se busca suscribir un contrato? *
            </td>
            <td>Modalidad de contratación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlAplicaProcesoSeleccion" Enabled="false" Width="90%">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Tipo de Solicitud *
            </td>
            <td>Complejidad Interna *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoEstudio" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadInterna" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Complejidad Indicador *
            </td>
            <td>Responsable DT o ES *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadIndicador" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdResponsableES" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Responsable EC *
            </td>
            <td>Ordenador del gasto
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdResponsableEC" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlOrdenadorGasto" Enabled="false" Width="90%">
                    <asp:ListItem Selected="True" Value="-1"> Seleccione </asp:ListItem>
                    <asp:ListItem Value="Director(a) General"> Director(a) General </asp:ListItem>
                    <asp:ListItem Value="Subdirector(a) General"> Subdirector(a) General </asp:ListItem>
                    <asp:ListItem Value="Secretario(a) General"> Secretario(a) General </asp:ListItem>
                    <asp:ListItem Value="Director(a) Regional"> Director(a) Regional </asp:ListItem>
                    <asp:ListItem Value="Delegación Especial"> Delegación Especial </asp:ListItem>
                    <asp:ListItem Value="No aplica"> No aplica </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Estado *
            </td>
            <td>Motivo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdMotivoSolicitud" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Dependencia solicitante *
            </td>
            <td>Área solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdAreaSolicitante" Enabled="false" Width="90%"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Tipo de Valor
            </td>
            <td>Valor presupuesto estimado solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoValor" Enabled="false" Width="90%">
                    <asp:ListItem Selected="True" Value="-1"> Seleccione </asp:ListItem>
                    <asp:ListItem Value="Vr unitario"> Vr unitario </asp:ListItem>
                    <asp:ListItem Value="Vr total"> Vr total </asp:ListItem>
                    <asp:ListItem Value="No aplica"> No aplica </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorPresupuestoEstimadoSolicitante" Enabled="false" Width="90%"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
