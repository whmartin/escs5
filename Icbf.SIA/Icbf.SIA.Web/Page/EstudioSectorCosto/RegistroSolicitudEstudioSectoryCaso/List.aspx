<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RegistroSolicitudEstudioSectoryCaso_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Consecutivo Solicitud
                </td>
                <td>Nombre abreviado 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" Width="90%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftConsecutivoEstudio" runat="server" TargetControlID="txtConsecutivoEstudio"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" Width="90%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreAbreviado" runat="server" TargetControlID="txtNombreAbreviado"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Objeto 
                </td>
                <td>Responsable DT o ES 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtObjeto" TextMode="MultiLine" Rows="4" onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" Width="90%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObjeto" runat="server" TargetControlID="txtObjeto"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdResponsableES" Width="90%"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Responsable EC 
                </td>
                <td>Modalidad de contratación
                </td>
            </tr>
            <tr class="rowA">
                <td>

                    <asp:DropDownList runat="server" ID="ddlIdResponsableEC" Width="90%"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Width="90%"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Año Fecha de solicitud inicial 
                </td>
                <td>Dependencia solicitante 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdFechaSolicitudInicial" Width="90%"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdDireccionSolicitante" Width="90%"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Estado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" Width="90%"></asp:DropDownList>
                </td>
                <td></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegistroSolicitudEstudioSectoryCaso" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRegistroSolicitudEstudioSectoryCaso" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRegistroSolicitudEstudioSectoryCaso_PageIndexChanging" OnSelectedIndexChanged="gvRegistroSolicitudEstudioSectoryCaso_SelectedIndexChanged" OnSorting="gvRegistroSolicitudEstudioSectoryCaso_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="Acción">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo Solicitud" DataField="IdRegistroSolicitudEstudioSectoryCaso" SortExpression="IdRegistroSolicitudEstudioSectoryCaso" />
                            <asp:BoundField HeaderText="Nombre abreviado" DataField="NombreAbreviado" SortExpression="NombreAbreviado" />
                            <asp:BoundField HeaderText="Número de reproceso" DataField="NumeroReproceso" SortExpression="NumeroReproceso" />
                            <asp:TemplateField HeaderText="Consecutivo estudio relacionado" SortExpression="ConsecutivoEstudioRelacionado">
                                <ItemTemplate>
                                    <%# Eval("ConsecutivoEstudioRelacionado").ToString()=="0"? string .Empty:Eval("ConsecutivoEstudioRelacionado").ToString() %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha de solicitud inicial" DataField="FechaSolicitudInicial" SortExpression="FechaSolicitudInicial" />
                            <asp:BoundField HeaderText="Acta, correo o Nro. Radicado" DataField="ActaCorreoNoRadicado" SortExpression="ActaCorreoNoRadicado" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoSolicitud" SortExpression="EstadoSolicitud" />
                            <asp:BoundField HeaderText="Modalidad de contratación" DataField="ModalidadSeleccion" SortExpression="ModalidadSeleccion" />
                            <asp:BoundField HeaderText="Dependencia solicitante" DataField="DireccionSolicitante" SortExpression="DireccionSolicitante" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <asp:HiddenField ID="hfNombreSolicitanteES" runat="server" />
            <asp:HiddenField ID="hfNombreSolicitanteEC" runat="server" />
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }
    </script>
</asp:Content>
