<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Informe_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblVigencia" runat="server">Vigencia</asp:Label>
                &nbsp;*
                <asp:RequiredFieldValidator ID="rfvVigencia" runat="server" ControlToValidate="ddlVigencia" Display="Dynamic" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
            <td>Direcci&oacute;n solicitante *
                  <asp:CustomValidator runat="server" ID="cvmodulelist"
                  ClientValidationFunction="ValidateModuleList" ForeColor="Red"
                  ErrorMessage="Campo Requerido" ValidationGroup="btnReporte"></asp:CustomValidator>
            </td>  
        </tr>
        <tr class="rowA">
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlVigencia" Width="406px"  ></asp:DropDownList>
            </td>
            <td class="Cell" style="height: 80px">
                    <div style="width: 400px; height: 80px; overflow-y: scroll; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="ddlTiposEstudio" runat="server" Width="400px" AutoPostBack="false" OnSelectedIndexChanged="CheckBoxTodosTiposEstudio_SelectedIndexChanged"/>
                    </div>                    
             </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label ID="lblobjeto" runat="server">Objeto</asp:Label>
                
            </td>
            <td>Estado                  
            </td>  
        </tr>
        <tr class="rowA">
            <td valign="top">
                <asp:textbox runat="server" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);" ID="txtObjeto" Width="406px" TextMode="MultiLine"  ></asp:textbox>
            </td>
            <td class="Cell" style="height: 80px">
                    <div style="width: 400px; height: 80px; overflow-y: scroll; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="ddlEstado" runat="server" Width="400px" AutoPostBack="false" OnSelectedIndexChanged="CheckBoxTodosTiposEstado_SelectedIndexChanged"/>
                    </div>                    
             </td>
        </tr>
    </table>
    </asp:Panel>

    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>

    <script type="text/javascript">
        // javascript to add to your aspx page
        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= ddlTiposEstudio.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>
