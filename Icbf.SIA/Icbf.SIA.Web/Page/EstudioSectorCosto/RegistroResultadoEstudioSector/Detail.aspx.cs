//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_ResultadoEstudioSector_Detail</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Service;

public partial class Page_ResultadoEstudioSector_Detail : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/RegistroResultadoEstudioSector";

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    #region EVENTOS

    /// <summary>
    /// Evento click del botón nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click del botón editar
    /// </summary>
    /// <param name="sender">Boton btnEditar</param>
    /// <param name="e">Evento click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ResultadoEstudioSector.IdConsecutivoEstudio", hfIdConsecutivoResultadoEstudio.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento click del botón editar
    /// </summary>
    /// <param name="sender">Boton btnEditar</param>
    /// <param name="e">Evento click</param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Evento click del botón eliminar
    /// </summary>
    /// <param name="sender">Boton btnEliminar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    #endregion

    #region METODOS

    /// <summary>
    /// Método para cargar los datos de un registro
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            long vIdResultadoConsecutivoEstudio = Convert.ToInt32(GetSessionParameter("ResultadoEstudioSector.IdConsecutivoEstudio"));
            RemoveSessionParameter("ResultadoEstudioSector.IdConsecutivoEstudio");

            if (GetSessionParameter("ResultadoEstudioSector.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ResultadoEstudioSector.Guardado");

            encabezados.Visible = false;
            ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            vResultadoEstudioSector = vResultadoEstudioSectorService.ConsultarResultadoEstudioSector(vIdResultadoConsecutivoEstudio);
            hfIdConsecutivoResultadoEstudio.Value = vResultadoEstudioSector.IdConsecutivoResultadoEstudio.ToString();
            txtConsecutivoEstudio.Text = vResultadoEstudioSector.IdConsecutivoEstudio.ToString();
            rblIdDocumentosRevisadosNAS.SelectedValue = vResultadoEstudioSector.DocumentosRevisadosNAS.ToString();
            txtFecha.SetDate(vResultadoEstudioSector.Fecha);
            txtFecha.SetReadOnly(true);
            ddlIdRevisadoPor.SelectedValue = vResultadoEstudioSector.IdRevisadoPor == null ? Constantes.ValorDefecto : vResultadoEstudioSector.IdRevisadoPor.ToString();
            txtObservacion.Text = vResultadoEstudioSector.Observacion.ToString();
            txtConsultados.Text = vResultadoEstudioSector.Consultados == null ? string.Empty : vResultadoEstudioSector.Consultados.ToString();
            txtParticipantes.Text = vResultadoEstudioSector.Participantes == null ? string.Empty : vResultadoEstudioSector.Participantes.ToString();
            txtCotizacionesRecibidas.Text = vResultadoEstudioSector.CotizacionesRecibidas == null ? string.Empty : vResultadoEstudioSector.CotizacionesRecibidas.ToString();
            txtCotizacionesParaPresupuesto.Text = vResultadoEstudioSector.CotizacionesParaPresupuesto == null ? string.Empty : vResultadoEstudioSector.CotizacionesParaPresupuesto.ToString();
            txtRadicadoOficioEntregaESyC.Text = vResultadoEstudioSector.RadicadoOficioEntregaESyC.ToString();
            txtFechaDeEntregaESyC.SetDate(vResultadoEstudioSector.FechaDeEntregaESyC);
            txtFechaDeEntregaESyC.SetReadOnly(true);
            ddlIdTipoDeValor.SelectedValue = vResultadoEstudioSector.TipoDeValor.Equals(string.Empty) ? Constantes.ValorDefecto : vResultadoEstudioSector.TipoDeValor.ToString();
            txtValorPresupuestoESyC.Text = vResultadoEstudioSector.ValorPresupuestoESyC == null ? string.Empty : vResultadoEstudioSector.ValorPresupuestoESyC.ToString();
            txtIndiceLiquidez.Text = vResultadoEstudioSector.IndiceLiquidez.ToString();
            txtNivelDeEndeudamiento.Text = vResultadoEstudioSector.NivelDeEndeudamiento.ToString();
            txtCoberturaInteres.Text = vResultadoEstudioSector.RazonDeCoberturaDeIntereses.ToString();
            txtCapitalDeTrabajo.Text = vResultadoEstudioSector.CapitalDeTrabajo.ToString();
            txtRentabilidadDelPatrimonio.Text = vResultadoEstudioSector.RentabilidadDelPatrimonio.ToString();
            txtRentabilidadDelActivo.Text = vResultadoEstudioSector.RentabilidadDelActivo.ToString();
            txtTotalPAtrimonio.Text = vResultadoEstudioSector.TotalPatrimonio == null ? string.Empty : vResultadoEstudioSector.TotalPatrimonio.ToString();
            rblKResidual.SelectedValue = vResultadoEstudioSector.KResidual.ToString();
            if (vResultadoEstudioSectorService.ConsultarIndicadorResultado(vResultadoEstudioSector.IdConsecutivoResultadoEstudio).Count == 0 || vResultadoEstudioSectorService.ConsultarIndicadorResultado(vResultadoEstudioSector.IdConsecutivoResultadoEstudio) == null)
            {
                encabezados.Visible = true;
            }
            gvIndicadores.DataSource = vResultadoEstudioSectorService.ConsultarIndicadorResultado(vResultadoEstudioSector.IdConsecutivoResultadoEstudio);
            gvIndicadores.DataBind();
            pnlConsulta.Enabled = false;
            ObtenerAuditoria(PageName, hfIdConsecutivoResultadoEstudio.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vResultadoEstudioSector.UsuarioCrea, vResultadoEstudioSector.FechaCrea, vResultadoEstudioSector.UsuarioModifica, vResultadoEstudioSector.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para eliminar un registro
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            long vIdConsecutivoEstudio = long.Parse(hfIdConsecutivoResultadoEstudio.Value);

            ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            vResultadoEstudioSector = vResultadoEstudioSectorService.ConsultarResultadoEstudioSector(vIdConsecutivoEstudio);
            InformacionAudioria(vResultadoEstudioSector, this.PageName, vSolutionPage);
            int vResultado = vResultadoEstudioSectorService.EliminarResultadoEstudioSector(vResultadoEstudioSector);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }

            toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            SetSessionParameter("ResultadoEstudioSector.Eliminado", "1");
            NavigateTo(SolutionPage.List);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para configurar la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Registros de Resultados", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            txtConsecutivoEstudio.Focus();
            Utilidades.LlenarRadioButtonList(rblIdDocumentosRevisadosNAS, Constantes.LstSINO);
            rblIdDocumentosRevisadosNAS.SelectedValue = Constantes.CodNo;
            Utilidades.LlenarDropDownList(ddlIdRevisadoPor, vResultadoEstudioSectorService.ConsultarResponsable());
            Utilidades.LlenarDropDownList(ddlIdTipoDeValor, Constantes.LstTipoDeValor);
            Utilidades.LlenarRadioButtonList(rblKResidual, Constantes.LstSINO);
            rblKResidual.SelectedValue = Constantes.CodNo;


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

}
