//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_GestionesEstudioSectorCostos_Add</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using System.Linq.Expressions;

/// <summary>
/// Clase para agregar registros de resultado estudio 
/// </summary>
public partial class Page_ResultadoEstudioSector_Add : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Instancia a clase ManejoControlesContratos
    /// </summary>
    ManejoControlesContratos vManejoControlesContratos;

    /// <summary>
    /// Instancia a clase EstudioSectorCostoService
    /// </summary>
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/RegistroResultadoEstudioSector";

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
               
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Métod para validar festivos
    /// </summary>
    /// <returns>Retorna  true: si la fecha no es festivo de lo contrario false </returns>
    protected bool ValidarFestivos()
    {
        bool vEsValido = true;
        if (txtFecha.EsDiaFestivo())
        {
            vEsValido = false;
        }
        if (txtFechaDeEntregaESyC.EsDiaFestivo())
        {
            vEsValido = false;
        }
        return vEsValido;
    }

    /// <summary>
    /// Método para guardar registro
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (!ValidarFestivos())
            {
                return;
            }
            long vResultado;
            string formato = "{0} {1} {2}";
            ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            vResultadoEstudioSector.IdConsecutivoEstudio = int.Parse(txtConsecutivoEstudio.Text);
            vResultadoEstudioSector.DocumentosRevisadosNAS = rblIdDocumentosRevisadosNAS.SelectedValue;
            vResultadoEstudioSector.Fecha = txtFecha.GetDate();
            vResultadoEstudioSector.IdRevisadoPor = ddlIdRevisadoPor.SelectedValue.Equals(Constantes.ValorDefecto) ? (int?)null : Convert.ToInt32(ddlIdRevisadoPor.SelectedValue);
            vResultadoEstudioSector.Observacion = txtObservacion.Text;
            vResultadoEstudioSector.Consultados = txtConsultados.Text.Equals(string.Empty) ? (int?)null : Convert.ToInt32(txtConsultados.Text);
            vResultadoEstudioSector.Participantes = txtParticipantes.Text.Equals(string.Empty) ? (int?)null : Convert.ToInt32(txtParticipantes.Text);
            vResultadoEstudioSector.CotizacionesRecibidas = txtCotizacionesRecibidas.Text.Equals(string.Empty) ? (int?)null : Convert.ToInt32(txtCotizacionesRecibidas.Text);
            vResultadoEstudioSector.CotizacionesParaPresupuesto = txtCotizacionesParaPresupuesto.Text.Equals(string.Empty) ? (int?)null : Convert.ToInt32(txtCotizacionesParaPresupuesto.Text);
            vResultadoEstudioSector.RadicadoOficioEntregaESyC = txtRadicadoOficioEntregaESyC.Text;
            vResultadoEstudioSector.FechaDeEntregaESyC = txtFechaDeEntregaESyC.GetDate();
            vResultadoEstudioSector.TipoDeValor = ddlIdTipoDeValor.SelectedValue.Equals(Constantes.ValorDefecto) ? string.Empty : ddlIdTipoDeValor.SelectedValue;                        
            vResultadoEstudioSector.ValorPresupuestoESyC = txtValorPresupuestoESyC.Text.Equals(string.Empty) ? (decimal?)null : decimal.Parse(txtValorPresupuestoESyC.Text.ToString().Replace("$",string.Empty));
            vResultadoEstudioSector.IndiceLiquidez = txtIndiceLiquidez.Text;
            vResultadoEstudioSector.IndiceLiquidezDesc = txtIndiceLiquidez.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlIndiceLiquidez.GroupingText, Constantes.MayorOIgual, txtIndiceLiquidez.Text);
            vResultadoEstudioSector.NivelDeEndeudamiento = txtNivelDeEndeudamiento.Text;
            vResultadoEstudioSector.NivelDeEndeudamientoDesc = txtNivelDeEndeudamiento.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlNivelEndeudamiento.GroupingText, Constantes.MenorOIgual, txtNivelDeEndeudamiento.Text);
            vResultadoEstudioSector.RazonDeCoberturaDeIntereses = txtCoberturaInteres.Text;
            vResultadoEstudioSector.RazonDeCoberturaDeInteresesDesc = txtCoberturaInteres.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlCobertura.GroupingText, Constantes.MayorOIgual, txtCoberturaInteres.Text);
            vResultadoEstudioSector.CapitalDeTrabajo = txtCapitalDeTrabajo.Text;
            vResultadoEstudioSector.CapitalDeTrabajoDesc = txtCapitalDeTrabajo.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlCapitalTrabajo.GroupingText, Constantes.MayorOIgual, txtCapitalDeTrabajo.Text);
            vResultadoEstudioSector.RentabilidadDelPatrimonio = txtRentabilidadDelPatrimonio.Text;
            vResultadoEstudioSector.RentabilidadDelPatrimonioDesc = txtRentabilidadDelPatrimonio.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlRentabilidadPatrimonio.GroupingText, Constantes.MayorOIgual, txtRentabilidadDelPatrimonio.Text);
            vResultadoEstudioSector.RentabilidadDelActivo = txtRentabilidadDelActivo.Text;
            vResultadoEstudioSector.RentabilidadDelActivoDesc = txtRentabilidadDelActivo.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlRentabilidadActivo.GroupingText, Constantes.MayorOIgual, txtRentabilidadDelActivo.Text);            
            vResultadoEstudioSector.TotalPatrimonio = txtTotalPAtrimonio.Text.Equals(string.Empty) ? (decimal?)null : decimal.Parse(txtTotalPAtrimonio.Text.Replace("$",string.Empty));
            vResultadoEstudioSector.TotalPatrimonioDesc = txtTotalPAtrimonio.Text.Equals(string.Empty) ? string.Empty : string.Format(formato, pnlTotalPatrimonio.GroupingText, Constantes.MayorOIgual, txtTotalPAtrimonio.Text);
            vResultadoEstudioSector.KResidual = rblKResidual.SelectedValue;
            vResultadoEstudioSector.Indicador = txtIndicador.Text;
            vResultadoEstudioSector.Condicion = ddlCondicion.SelectedValue.Equals(Constantes.ValorDefecto) ? string.Empty : ddlCondicion.SelectedValue;
            vResultadoEstudioSector.Valor = txtValor.Text;

            if (Request.QueryString["oP"] == "E")
            {
                vResultadoEstudioSector.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vResultadoEstudioSector, this.PageName, SolutionPage.Edit);
                vResultadoEstudioSector.IdConsecutivoResultadoEstudio = long.Parse(hfIdConsecutivoResultadoEstudio.Value);
                vResultadoEstudioSectorService.ModificarResultadoEstudioSector(vResultadoEstudioSector);
                vResultado = vResultadoEstudioSector.IdConsecutivoResultadoEstudio;
            }
            else
            {
                vResultadoEstudioSector.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vResultadoEstudioSector, this.PageName, SolutionPage.Add);
                vResultado = vResultadoEstudioSectorService.InsertarResultadoEstudioSector(vResultadoEstudioSector);
                vResultadoEstudioSector.IdConsecutivoResultadoEstudio = vResultado;
            }
            GuardarIndicadoresAdicionales(vResultado);

            SetSessionParameter("ResultadoEstudioSector.IdConsecutivoEstudio", vResultadoEstudioSector.IdConsecutivoResultadoEstudio);
            SetSessionParameter("ResultadoEstudioSector.Guardado", "1");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para guardar indicadores adicionales
    /// </summary>
    /// <param name="pIdResultadoEstudio">Id consecutivo registro de resultado</param>
    protected void GuardarIndicadoresAdicionales(long pIdResultadoEstudio)
    {
        List<IndicadorAdicionalResultado> vLstIndicadores = ViewState[Constantes.ViewStateIndicadores] as List<IndicadorAdicionalResultado>;
        List<long> vLstIndicadoresEliminar = ViewState[Constantes.ViewStateEliminarIndicadores] as List<long>;
        if (vLstIndicadores != null)
        {
            foreach (IndicadorAdicionalResultado indicador in vLstIndicadores)
            {
                indicador.IdResultadoEstudio = pIdResultadoEstudio;
                if (indicador.IdIndicadoresAdicionalesResultado == 0)
                {
                    vResultadoEstudioSectorService.InsertarIndicadorResultado(indicador);
                }
                else
                {
                    vResultadoEstudioSectorService.ModificarIndicadoresResultado(indicador);
                }
            }
        }
        if (vLstIndicadoresEliminar != null)
        {

            foreach (long idIndicador in vLstIndicadoresEliminar)
            {
                IndicadorAdicionalResultado aux = new IndicadorAdicionalResultado();
                aux.IdIndicadoresAdicionalesResultado = idIndicador;
                vResultadoEstudioSectorService.EliminarIndicadorResultado(aux);
            }

        }




    }

    /// <summary>
    /// Método para configurar la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Registros de Resultados", SolutionPage.Add.ToString());

            txtConsecutivoEstudio.Enabled = false;


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar registro
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            long vIdConsecutivoResultadoEstudio = Convert.ToInt32(GetSessionParameter("ResultadoEstudioSector.IdConsecutivoEstudio"));
            RemoveSessionParameter("ResultadoEstudioSector.Id");

            ResultadoEstudioSector vResultadoEstudioSector = new ResultadoEstudioSector();
            vResultadoEstudioSector = vResultadoEstudioSectorService.ConsultarResultadoEstudioSector(vIdConsecutivoResultadoEstudio);
            hfIdConsecutivoResultadoEstudio.Value = vIdConsecutivoResultadoEstudio.ToString();
            rblIdDocumentosRevisadosNAS.SelectedValue = vResultadoEstudioSector.DocumentosRevisadosNAS.ToString();
            txtFecha.SetDate(vResultadoEstudioSector.Fecha);
            txtConsecutivoEstudio.Text = vResultadoEstudioSector.IdConsecutivoEstudio.ToString();
            ddlIdRevisadoPor.SelectedValue = vResultadoEstudioSector.IdRevisadoPor.ToString();
            txtObservacion.Text = vResultadoEstudioSector.Observacion.ToString();
            txtConsultados.Text = vResultadoEstudioSector.Consultados.ToString();
            txtParticipantes.Text = vResultadoEstudioSector.Participantes.ToString();
            txtCotizacionesRecibidas.Text = vResultadoEstudioSector.CotizacionesRecibidas.ToString();
            txtCotizacionesParaPresupuesto.Text = vResultadoEstudioSector.CotizacionesParaPresupuesto.ToString();
            txtRadicadoOficioEntregaESyC.Text = vResultadoEstudioSector.RadicadoOficioEntregaESyC.ToString();
            txtFechaDeEntregaESyC.SetDate(vResultadoEstudioSector.FechaDeEntregaESyC);
            ddlIdTipoDeValor.SelectedValue = vResultadoEstudioSector.TipoDeValor.ToString();
            txtValorPresupuestoESyC.Text = vResultadoEstudioSector.ValorPresupuestoESyC.ToString();
            txtIndiceLiquidez.Text = vResultadoEstudioSector.IndiceLiquidez.ToString();
            txtNivelDeEndeudamiento.Text = vResultadoEstudioSector.NivelDeEndeudamiento.ToString();
            txtCoberturaInteres.Text = vResultadoEstudioSector.RazonDeCoberturaDeIntereses.ToString();
            txtCapitalDeTrabajo.Text = vResultadoEstudioSector.CapitalDeTrabajo.ToString();
            txtRentabilidadDelPatrimonio.Text = vResultadoEstudioSector.RentabilidadDelPatrimonio.ToString();
            txtRentabilidadDelActivo.Text = vResultadoEstudioSector.RentabilidadDelActivo.ToString();
            txtTotalPAtrimonio.Text = vResultadoEstudioSector.TotalPatrimonio.ToString();
            rblKResidual.SelectedValue = vResultadoEstudioSector.KResidual.ToString();

            List<IndicadorAdicionalResultado> lstIndicadores = vResultadoEstudioSectorService.ConsultarIndicadorResultado(vResultadoEstudioSector.IdConsecutivoResultadoEstudio);
            ViewState[Constantes.ViewStateIndicadores] = lstIndicadores;
            gvIndicadores.DataSource = lstIndicadores;
            gvIndicadores.DataBind();
            //DEscativar opción eliminar Indicadores Adicionales
            foreach (GridViewRow row in gvIndicadores.Rows)
            {
                ImageButton btn = row.FindControl("btnEliminar") as ImageButton;
                btn.Enabled = false;
            }

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vResultadoEstudioSector.UsuarioCrea, vResultadoEstudioSector.FechaCrea, vResultadoEstudioSector.UsuarioModifica, vResultadoEstudioSector.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.LipiarMensajeError();
            txtConsecutivoEstudio.Focus();
            Utilidades.LlenarRadioButtonList(rblIdDocumentosRevisadosNAS, Constantes.LstSINO);
            rblIdDocumentosRevisadosNAS.SelectedValue = Constantes.CodNo;
            Utilidades.LlenarDropDownList(ddlIdRevisadoPor, vResultadoEstudioSectorService.ConsultarResponsable());
            Utilidades.LlenarDropDownList(ddlIdTipoDeValor, Constantes.LstTipoDeValor);
            Utilidades.LlenarRadioButtonList(rblKResidual, Constantes.LstSINO);
            rblKResidual.SelectedValue = Constantes.CodNo;
            Utilidades.LlenarDropDownList(ddlCondicion, Constantes.LstCondicion);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para agregar indicadores
    /// </summary>
    protected void AgregarIndicador()
    {
        btnAgregarIndicador.Focus();
        btnAgregarIndicador.ImageUrl = "~/Image/btn/add.gif";
        if (ddlCondicion.SelectedValue.Equals(Constantes.ValorDefecto) || txtIndicador.Text.Equals(string.Empty) || txtValor.Text.Equals(string.Empty))
        {
            return;
        }

        List<IndicadorAdicionalResultado> vLstIndicadores = ViewState[Constantes.ViewStateIndicadores] as List<IndicadorAdicionalResultado>;
        if (vLstIndicadores == null)
        {
            vLstIndicadores = new List<IndicadorAdicionalResultado>();
        }

        vLstIndicadores.Add(new IndicadorAdicionalResultado
        {
            IdIndicadoresAdicionalesResultado = int.Parse(hdfIdIndicadorAdicional.Value),
            Indicador = txtIndicador.Text,
            Condicion = ddlCondicion.SelectedValue,
            Valor = txtValor.Text,
            IndicadorAdicionalDesc = string.Format("{0} {1} {2} ", txtIndicador.Text, ddlCondicion.SelectedValue, txtValor.Text),
            FechaCrea = DateTime.Now,
            FechaModifica = DateTime.Now,
            UsuarioCrea = GetSessionUser().NombreUsuario,
            UsuarioModifica = GetSessionUser().NombreUsuario
        });
        gvIndicadores.DataSource = vLstIndicadores;
        gvIndicadores.DataBind();
        ViewState[Constantes.ViewStateIndicadores] = vLstIndicadores;
        hdfIdIndicadorAdicional.Value = "0";
        Utilidades.SetControls(pnlIndicadoresadicionales);
    } 

    /// <summary>
    /// Método para eliminar o editar indicador
    /// </summary>
    /// <param name="pCommandArgument">Argumento eliminar o editar</param>
    /// <param name="pEsEliminar">True para eliminar, False para editar</param>
    protected void EditarEliminarIndicador(string pCommandArgument, bool pEsEliminar)
    {
        List<IndicadorAdicionalResultado> vLstIndicadores = ViewState[Constantes.ViewStateIndicadores] as List<IndicadorAdicionalResultado>;
        if (vLstIndicadores == null)
        {
            return;
        }
        string[] argumentos = pCommandArgument.Split('-');
        long IdIndicador = int.Parse(argumentos[0]);
        int index = int.Parse(argumentos[1]);
        IndicadorAdicionalResultado indicador;
        if (IdIndicador == 0)
        {
            indicador = vLstIndicadores[index];
            vLstIndicadores.RemoveAt(index);
        }
        else
        {
            indicador = vLstIndicadores.Where(x => x.IdIndicadoresAdicionalesResultado == IdIndicador).FirstOrDefault();
            vLstIndicadores.Remove(indicador);
        }

        if (!pEsEliminar)
        {
            txtIndicador.Text = indicador.Indicador;
            ddlCondicion.SelectedValue = indicador.Condicion;
            txtValor.Text = indicador.Valor;
            hdfIdIndicadorAdicional.Value = indicador.IdIndicadoresAdicionalesResultado.ToString();
            btnAgregarIndicador.ImageUrl = "~/Image/btn/apply.png";
            btnAgregarIndicador.ToolTip = "Aplicar";
        }
        else
        {
            if (IdIndicador != 0)
            {
                List<long> vLstEliminarIndice = ViewState[Constantes.ViewStateEliminarIndicadores] as List<long>;
                if (vLstEliminarIndice == null)
                {
                    vLstEliminarIndice = new List<long>();
                }

                vLstEliminarIndice.Add(IdIndicador);
                ViewState[Constantes.ViewStateEliminarIndicadores] = vLstEliminarIndice;
            }
        }

        gvIndicadores.DataSource = vLstIndicadores;
        gvIndicadores.DataBind();
    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// Método para guardar registro
    /// </summary>
    /// <param name="sender">Boton btnGuardar</param>
    /// <param name="e">Evento click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Método para agregar un registro
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método para buscar registro
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento click boton agregar indicador
    /// </summary>
    /// <param name="sender">Botón btnAgregarIndicador</param>
    /// <param name="e">Evento click</param>
    protected void btnAgregarIndicador_Click(object sender, ImageClickEventArgs e)
    {
        AgregarIndicador();
    }

    /// <summary>
    /// Evento click botones de la grilla
    /// </summary>
    /// <param name="sender">Grilla gvIndicadores</param>
    /// <param name="e">Evento click</param>
    protected void gvIndicadores_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Editar":
                EditarEliminarIndicador(e.CommandArgument.ToString(), false);
                break;
            case "Eliminar":
                EditarEliminarIndicador(e.CommandArgument.ToString(), true);
                break;
        }

    }

    /// <summary>
    /// Evento seleccionar consecutivo estudio
    /// </summary>
    /// <param name="sender">caja de texto txtConsecutivoEstudio</param>
    /// <param name="e">Evento change</param>
    protected void txtConsecutivoEstudio_TextChanged(object sender, EventArgs e)
    {
        ////ignore
    }


    #endregion

}
