<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ResultadoEstudioSector_List" %>
<%@ Register  TagPrefix="uccConsulta" TagName="Consulta" Src="~/General/General/EstudioSectores/FormularioConsulta.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>

    <%--<asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Consecutivo  Estudio
            </td>
            <td>
                Nombre Abreviado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="10"  ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtConsecutivoEstudio" FilterType="Numbers"  ></Ajax:FilteredTextBoxExtender>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreAbreviado" MaxLength="255" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteNombreAbreviado" TargetControlID="txtNombreAbreviado" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto
            </td>
            <td>
                Responsable DT o ES
            </td>
        </tr>
        <tr class="rowA">
            <td rowspan="3">
                <asp:TextBox runat="server" ID="txtObjeto"  TextMode="MultiLine" rows="4" Columns="30" onChange="limitText(this,400);"  onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteObjeto" TargetControlID="txtObjeto" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
            </td>
            <td rowspan="1">
                <asp:DropDownList runat="server" ID="ddlResponsableES"  ></asp:DropDownList>
            </td>
        </tr>
        <tr><td><br /></td><td></td></tr>
        <tr><td><br /></td><td></td></tr>
        <tr class="rowB">
            <td>
                Responsable EC
            </td>
            <td>
                Modalidad de contratación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlResponsableEC"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidadDeSeleccion"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Dependencia solicitante
            </td>
            <td>
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlDireccionSolicitante"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlEstado"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>--%>
    <uccConsulta:Consulta runat="server"  ID="ucConsulta"/>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvResultadoEstudioSector" AutoGenerateColumns="False" AllowPaging="True" PageIndex="0" AllowSorting="true"
                        GridLines="None" Width="100%" DataKeyNames="IdResultadoEstudio" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvResultadoEstudioSector_PageIndexChanging" OnSelectedIndexChanged="gvResultadoEstudioSector_SelectedIndexChanged" OnSorting="gvResultadoEstudioSector_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo  Estudio" DataField="IdConsecutivoEstudio" SortExpression="IdConsecutivoEstudio" />
                            <asp:BoundField HeaderText="Nombre Abreviado" DataField="NombreAbreviado" SortExpression="NombreAbreviado" />
                            <asp:BoundField HeaderText="Consultados" DataField="ProveedoresConsultados"  SortExpression="ProveedoresConsultados"/>
                            <asp:BoundField HeaderText="Participantes" DataField="ProveedoresParticipantes" SortExpression="ProveedoresParticipantes" />
                            <asp:BoundField HeaderText="Cotizaciones recibidas" DataField="CotizacionesRecibidas" SortExpression="CotizacionesRecibidas" />
                            <asp:BoundField HeaderText="Observación" DataField="Observacion" SortExpression="Observacion" />
                            

                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
