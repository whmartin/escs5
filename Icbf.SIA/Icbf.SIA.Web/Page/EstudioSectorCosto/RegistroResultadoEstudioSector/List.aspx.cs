//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_ResultadoEstudioSector_List</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Linq.Expressions;
using System.Data;

/// <summary>
/// Clase para consultar resultados de estudio 
/// </summary>
public partial class Page_ResultadoEstudioSector_List : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/RegistroResultadoEstudioSector";

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// M�todo para buscar registro
    /// </summary>
    private void Buscar()
    {
        try
        {
            ucConsulta.GetValues();
            long? vIdConsecutivoEstudio = ucConsulta.vIdConsecutivoEstudio;
            string vNombreAbreviado = ucConsulta.vNombreAbreviado;
            string vObjeto = ucConsulta.vObjeto;
            int? vIdResponsableES = ucConsulta.vIdResponsableES;
            int? vIdResponsableEC = ucConsulta.vIdResponsableEC;
            int? vIdModalidadDeSeleccion = ucConsulta.vIdModalidadDeSeleccion;
            int? vDireccionSolicitante = ucConsulta.vDireccionSolicitante;
            int? vEstado = ucConsulta.vEstado;


            List<ResultadoEstudioSectorConsulta> LstConsulta = vResultadoEstudioSectorService.ConsultarResultadoEstudioSectores(vIdConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdResponsableES, vIdResponsableEC, vIdModalidadDeSeleccion, vDireccionSolicitante, vEstado);
            ViewState[Constantes.ViewStateConsultaEstudio] = LstConsulta;
            string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
            if (pSortExpresion.Equals(string.Empty))
            {
                OrdenarGrilla(Constantes.SinOrden);

            }
            else
            {
                OrdenarGrilla(pSortExpresion);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para configurar p�gina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);
            gvResultadoEstudioSector.PageSize = PageSize();
            gvResultadoEstudioSector.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registros de Resultados", SolutionPage.List.ToString());

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvResultadoEstudioSector.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ResultadoEstudioSector.IdConsecutivoEstudio", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ResultadoEstudioSector.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ResultadoEstudioSector.Eliminado");


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Mpetodo para ordenar grilla
    /// </summary>
    /// <param name="pSortExpresion">Expresi�n de ordenamiento</param>
    protected void OrdenarGrilla(string pSortExpresion)
    {
        List<ResultadoEstudioSectorConsulta> vLstConsulta = ViewState[Constantes.ViewStateConsultaEstudio] as List<ResultadoEstudioSectorConsulta>;

        if (vLstConsulta != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvResultadoEstudioSector.DataSource = vLstConsulta.OrderByDescending(x=>x.IdConsecutivoEstudio).ToList();
                gvResultadoEstudioSector.DataBind();
                return;

            }

            var param = Expression.Parameter(typeof(ResultadoEstudioSectorConsulta), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<ResultadoEstudioSectorConsulta, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvResultadoEstudioSector.DataSource = vLstConsulta.AsQueryable<ResultadoEstudioSectorConsulta>().OrderBy(sortExpression).ToList<ResultadoEstudioSectorConsulta>();
            }
            else
            {
                gvResultadoEstudioSector.DataSource = vLstConsulta.AsQueryable<ResultadoEstudioSectorConsulta>().OrderByDescending(sortExpression).ToList<ResultadoEstudioSectorConsulta>();
            };

            gvResultadoEstudioSector.DataBind();
        }
    }

    /// <summary>
    /// M�todo para exportar registro
    /// </summary>
    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvResultadoEstudioSector.AllowPaging;
        Boolean vPaginadorError = gvResultadoEstudioSector.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvResultadoEstudioSector.AllowPaging = false;

        List<ResultadoEstudioSectorConsulta> vListaResultados = ViewState[Constantes.ViewStateConsultaEstudio] as List<ResultadoEstudioSectorConsulta>;
        if (vListaResultados == null)
        {
            return;
        }


        datos = Utilidades.GenerarDataTable(this.gvResultadoEstudioSector, vListaResultados);

        GridView datosexportar = new GridView();
        datosexportar.DataSource = datos;
        datosexportar.DataBind();
        vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "RegistroResultados_" + DateTime.Now.ToShortDateString());
        gvResultadoEstudioSector.AllowPaging = vPaginador;


    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento click bot�n buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    /// <summary>
    /// Evento click bot�n nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click bot�n exportar
    /// </summary>
    /// <param name="sender">Boton btnExportar</param>
    /// <param name="e">Evento click</param>
    private void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvResultadoEstudioSector.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            gvResultadoEstudioSector.DataSource = null;
            gvResultadoEstudioSector.DataBind();
        }
    }

    /// <summary>
    /// Evento seleccionar registro 
    /// </summary>
    /// <param name="sender">Grilla gvResultadoEstudioSector</param>
    /// <param name="e">Evento click</param>
    protected void gvResultadoEstudioSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvResultadoEstudioSector.SelectedRow);
    }

    /// <summary>
    /// Evento paginaci�n 
    /// </summary>
    /// <param name="sender">Grilla gvResultadoEstudioSector</param>
    /// <param name="e">Evento click</param>
    protected void gvResultadoEstudioSector_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvResultadoEstudioSector.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Evento ordenaci�n 
    /// </summary>
    /// <param name="sender">Grilla gvResultadoEstudioSector</param>
    /// <param name="e">Evento click</param>
    protected void gvResultadoEstudioSector_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState[Constantes.ViewStateSortDirection] != null && GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
        }

        OrdenarGrilla(e.SortExpression);
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;
        
    }

    #endregion

    /// <summary>
    /// Atributo que describe la direcci�n del ordenamiento
    /// </summary>
    public SortDirection GridViewSortDirection
{
    get
    {
        if (ViewState[Constantes.ViewStateSortDirection] == null)
        {
            ViewState[Constantes.ViewStateSortDirection] = SortDirection.Ascending;
        }


        return (SortDirection)ViewState[Constantes.ViewStateSortDirection];
    }
    set { ViewState[Constantes.ViewStateSortDirection] = value; }
}
}
