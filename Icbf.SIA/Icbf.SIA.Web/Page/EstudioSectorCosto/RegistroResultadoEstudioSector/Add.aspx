<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ResultadoEstudioSector_Add" Culture="auto" UICulture="auto"%>

<%@ Register TagPrefix="uc2" TagName="Festivos" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>
<%@ Register TagPrefix="ucPopUp" TagName="PopUpConsulta" Src="~/General/General/EstudioSectores/EstudioConsultaPopUp.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }            
        }
        function borrar(mask) {
            if ((event.keyCode == 8 || event.keyCode == 46)) {
                
            }
        }
        
    </script>
    <%--<asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>--%>
    <asp:HiddenField ID="hfIdConsecutivoResultadoEstudio" runat="server" />
    <asp:Panel ID="pnlContenedor" runat="server" HorizontalAlign="Justify">
        <!--Agregar-->
        <table width="70%" align="center">
            <tr class="rowB">
                <td>Consecutivo Estudio *</td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" ClientIDMode="Static" OnTextChanged="txtConsecutivoEstudio_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                    <asp:ImageButton runat="server" ID="btnLupa" ImageUrl="~/Image/btn/list.png" class="SetFocus" OnClientClick="MostrarPopUp(); return false;"/>
                    <asp:RequiredFieldValidator ID="rfvConsecutivoEstudio" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtConsecutivoEstudio" ValidationGroup="btnGuardar" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Panel ID="pnlProveedores" runat="server" GroupingText="Proveedores" BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">

                                <td>Consultados
                                </td>
                                <td>Participantes
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtConsultados" MaxLength="4"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftConsultados" runat="server" TargetControlID="txtConsultados"
                                        FilterType="Numbers" ValidChars="" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtParticipantes" MaxLength="4"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftParticipantes" runat="server" TargetControlID="txtParticipantes"
                                        FilterType="Numbers" ValidChars="" />
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Cotizaciones recibidas
                                </td>
                                <td>Cotizaciones para Presupuesto</td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtCotizacionesRecibidas" MaxLength="4"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftCotizacionesRecibidas" runat="server" TargetControlID="txtCotizacionesRecibidas"
                                        FilterType="Numbers" ValidChars="" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtCotizacionesParaPresupuesto" MaxLength="4"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteCotizacionesParaPresupuesto" runat="server" TargetControlID="txtCotizacionesParaPresupuesto"
                                        FilterType="Numbers" ValidChars="" />
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>

            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlRegistroResultados" runat="server" GroupingText="Registro de resultados" BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Correo electrónico aval DT o No. Radicado oficio entrega ESyC
                                </td>
                                <td>Fecha aval DT o entrega ESyC
                                </td>
                            </tr>
                            <tr class="rowA">

                                <td>

                                    <asp:TextBox runat="server" ID="txtRadicadoOficioEntregaESyC" MaxLength="100"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteRadicadoOficioEntregaESyC" TargetControlID="txtRadicadoOficioEntregaESyC" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                                <td>

                                    <uc2:Festivos ID="txtFechaDeEntregaESyC" runat="server" />
                                </td>
                            </tr>
                            <tr class="rowB">

                                <td>Tipo de Valor
                                </td>
                                <td>Valor presupuesto ESyC
                                </td>
                            </tr>
                            <tr class="rowA">

                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdTipoDeValor"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtValorPresupuestoESyC" MaxLength="18"></asp:TextBox>
                                    <%--<Ajax:MaskedEditExtender runat="server" ID="mskValorPresupuestoESyC" TargetControlID="txtValorPresupuestoESyC" Mask="999,999,999,999.99" MaskType="Number" DisplayMoney="Left" PromptCharacter=" "></Ajax:MaskedEditExtender>--%>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlIndiceLiquidez" runat="server" GroupingText="Índice de Liquidez (veces)" BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowB">
                                <td>Mayor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtIndiceLiquidez" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteIndiceLiquidez" TargetControlID="txtIndiceLiquidez" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>

                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlNivelEndeudamiento" runat="server" GroupingText="Nivel de Endeudamiento (%)" BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowB">
                                <td>Menor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtNivelDeEndeudamiento" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteNivelDeEndeudamiento" TargetControlID="txtNivelDeEndeudamiento" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlCobertura" runat="server" GroupingText="Razón de Cobertura de Intereses (veces) " BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowB">
                                <td>Mayor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtCoberturaInteres" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteCoberturaInteres" TargetControlID="txtCoberturaInteres" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlCapitalTrabajo" GroupingText="Capital de Trabajo (% del P.O.)" runat="server" BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowB">
                                <td>Mayor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtCapitalDeTrabajo" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteCapitalDeTrabajo" TargetControlID="txtCapitalDeTrabajo" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlRentabilidadPatrimonio" runat="server" GroupingText="Rentabilidad del Patrimonio (%) " BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowB">
                                <td>Mayor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtRentabilidadDelPatrimonio" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteRentabilidadDelPatrimonio" TargetControlID="txtRentabilidadDelPatrimonio" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlRentabilidadActivo" runat="server" GroupingText="Rentabilidad del Activo (%) " BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowA">
                                <td>Mayor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtRentabilidadDelActivo" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteRentabilidadDelActivo" TargetControlID="txtRentabilidadDelActivo" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlTotalPatrimonio" runat="server" GroupingText="Total patrimonio ($)" BorderWidth="1">
                        <br />
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Condición</td>
                                <td>Valor</td>
                            </tr>
                            <tr class="rowB">
                                <td>Mayor o igual</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTotalPAtrimonio" MaxLength="18"></asp:TextBox>
                                    <%--<Ajax:MaskedEditExtender runat="server" ID="mskTotalPAtrimonio" TargetControlID="txtTotalPAtrimonio" Mask="999,999,999,999.99" MaskType="Number" DisplayMoney="Left" PromptCharacter=" " ErrorTooltipEnabled="true"></Ajax:MaskedEditExtender>--%>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlKResidual" runat="server" GroupingText="K Residual " BorderWidth="1">

                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblKResidual" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlIndicadoresadicionales" runat="server" GroupingText="Indicadores adicionales" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Indicador</td>
                                <td>Condición</td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    <asp:TextBox runat="server" ID="txtIndicador" MaxLength="100"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteIndicador" TargetControlID="txtIndicador" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlCondicion"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>Valor</td>
                                <td></td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtValor" MaxLength="50"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteValor" TargetControlID="txtValor" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnAgregarIndicador" runat="server" ImageUrl="~/Image/btn/add.gif" OnClick="btnAgregarIndicador_Click" ToolTip="Agregar"/>
                                    <asp:HiddenField ID="hdfIdIndicadorAdicional" runat="server" Value="0" />
                                </td>
                            </tr>
                        </table>
                        <br />

                        <asp:GridView ID="gvIndicadores" align="center" runat="server" OnRowCommand="gvIndicadores_RowCommand" AutoGenerateColumns="false" Width="100%" GridLines="None" ShowHeaderWhenEmpty="true">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("IdIndicadoresAdicionalesResultado").ToString()+"-"+((GridViewRow) Container).RowIndex %>' ImageUrl="~/Image/btn/edit.gif"
                                            ToolTip="Editar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="IdIndicadoresAdicionalesResultado" DataField="IdIndicadoresAdicionalesResultado" Visible="false" />
                                <asp:BoundField HeaderText="Indicador" DataField="Indicador" />
                                <asp:BoundField HeaderText="Condición" DataField="condicion" />
                                <asp:BoundField HeaderText="Valor" DataField="Valor" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" CommandArgument='<%#Eval("IdIndicadoresAdicionalesResultado").ToString()+"-"+((GridViewRow) Container).RowIndex %>' ImageUrl="~/Image/btn/delete.gif"
                                            ToolTip="Eliminar" OnClientClick="return confirm('¿Seguro que desea eliminar el registro?');"  Enabled='<%#Eval("IdIndicadoresAdicionalesResultado").ToString().Equals("0")?true:false %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table width="70%" align="center">
            <tr class="rowB">
                <td>Documentos Revisados NAS
                </td>
                <td>Fecha
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblIdDocumentosRevisadosNAS" RepeatDirection="Horizontal"></asp:RadioButtonList>
                </td>
                <td>

                    <uc2:Festivos ID="txtFecha" runat="server" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Revisado por
                </td>
                <td>Observación
                </td>
            </tr>
            <tr class="rowA">
                <td rowspan="1">
                    <asp:DropDownList runat="server" ID="ddlIdRevisadoPor"></asp:DropDownList>
                </td>
                <td rowspan="3">
                    <asp:TextBox runat="server" ID="txtObservacion" TextMode="MultiLine" Rows="5" Columns="25" onChange="limitText(this,500);" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteObservacion" TargetControlID="txtObservacion" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="@/áéíóúÁÉÍÓÚñÑ.,_():; "></Ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
                <td></td>
            </tr>
        </table>

        
    </asp:Panel>
    <script type="text/javascript" language="javascript" src="../../../Scripts/ShowPopUpLupasESC.js"></script>
    <script type="text/javascript" src="../../../Scripts/jquery.maskMoney.js"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            
            $("#cphCont_txtValorPresupuestoESyC").maskMoney({ prefix: '$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: true });
            $("#cphCont_txtTotalPAtrimonio").maskMoney({ prefix: '$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: true });
        });
        function MostrarPopUp() {
            GetListRSE("txtConsecutivoEstudio");
        }
    </script>
</asp:Content>


