﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;


public partial class Page_Actividades_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/Actividades";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtNombre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<Actividades> Buscar()
    {
        try
        {
            String vNombre = null;
            String vDescripcion = null;
            int? vEstado = null;
            if (txtNombre.Text != "")
            {
                vNombre = Convert.ToString(txtNombre.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }
            List<Actividades> ListaActividades = vEstudioSectorCostoService.ConsultarActividadess(vNombre, vDescripcion, vEstado);
            return ListaActividades;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvActividades.PageSize = PageSize();
            gvActividades.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de actividades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvActividades.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Actividades.IdActividad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvActividades_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvActividades.SelectedRow);
    }
    protected void gvActividades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvActividades.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Actividades.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Actividades.Eliminado");

            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvActividades.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvActividades.AllowPaging;
        Boolean vPaginadorError = gvActividades.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvActividades.AllowPaging = false;

        List<Actividades> vListaActividadess = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvActividades, vListaActividadess);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvActividades.Columns.Count; i++)
                {
                    dt.Columns.Add(gvActividades.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvActividades.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvActividades.Columns.Count; j++)
                    {
                        dr[gvActividades.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaActividades");
                gvActividades.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }


    protected List<Actividades> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<Actividades> listaActividadess = Buscar();
        gvActividades.DataSource = listaActividadess;
        gvActividades.DataBind();

        return listaActividadess;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<Actividades> listaActividadess = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaActividadess != null)
                {
                    var param = Expression.Parameter(typeof(Actividades), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Actividades, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaActividadess;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvActividades_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
}
