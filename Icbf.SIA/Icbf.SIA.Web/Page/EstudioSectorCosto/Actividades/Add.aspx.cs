using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_Actividades_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/Actividades";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtNombre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            Actividades vActividades = new Actividades();

            vActividades.Nombre = Convert.ToString(txtNombre.Text).ToUpper();
            vActividades.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();
            vActividades.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));

            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vActividades.IdActividad = Convert.ToInt32(hfIdActividad.Value);
                    vActividades.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vActividades, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarActividades(vActividades);
                }
                else
                {
                    vActividades.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vActividades, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarActividades(vActividades);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("Actividades.IdActividad", vActividades.IdActividad);
                    SetSessionParameter("Actividades.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe.");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de actividades", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdActividad = Convert.ToInt32(GetSessionParameter("Actividades.IdActividad"));
            //Cración de HF para validación de duplicidad en el momento de cargar el registro
            String vNombreActividad = vEstudioSectorCostoService.ConsultarActividades(vIdActividad).Nombre;
            HfNombreActividad.Value = vNombreActividad;
            RemoveSessionParameter("Actividades.Id");

            Actividades vActividades = new Actividades();
            vActividades = vEstudioSectorCostoService.ConsultarActividades(vIdActividad);
            hfIdActividad.Value = vActividades.IdActividad.ToString();
            txtNombre.Text = vActividades.Nombre;
            txtDescripcion.Text = vActividades.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vActividades.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vActividades.UsuarioCrea, vActividades.FechaCrea, vActividades.UsuarioModifica, vActividades.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected String validarDuplicidad()
    {
        //Inicializar la validación
        HfValidacion.Value = "0";
        //Validación estado Actual
        if (!txtNombre.Text.ToUpper().Equals(HfNombreActividad.Value))
        {
            //Validación de la existencia en base de datos
            if (vEstudioSectorCostoService.ValidarActividad(txtNombre.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
    protected void txtNombre_TextChanged(object sender, EventArgs e)
    {
        //Validación de duplicidad en el cambio del texto
        HfValidacion.Value = validarDuplicidad();
    }
}
