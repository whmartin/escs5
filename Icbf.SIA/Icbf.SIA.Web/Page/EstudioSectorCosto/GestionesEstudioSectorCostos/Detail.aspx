<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_GestionesEstudioSectorCostos_Detail" %>

<%@ Register TagPrefix="uc1" TagName="Fechas" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hdfIdGestionEstudio" runat="server" />
    <asp:Panel ID="pnlContendor" runat="server">
        <table width="70%" align="center">
            <tr class="rowB">
                <td>Consecutivo  Estudio *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdConsecutivoEstudio" ControlToValidate="txtIdConsecutivoEstudio"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtIdConsecutivoEstudio" MaxLength="5"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtIdConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlFechaEnvioPreguntas" runat="server" GroupingText="Fechas envió preguntas y recepción respuestas a los DT" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Fecha Pregunta
                                </td>
                                <td>Fecha respuesta</td>
                            </tr>
                            <tr class="rowA">

                                <td>
                                    <uc1:Fechas runat="server" ID="txtFechaPreguntaOComentario" />
                                </td>
                                <td>
                                    <uc1:Fechas runat="server" ID="txtFechaRespuesta" />
                                </td>
                            </tr>

                            <tr class="rowB">
                                <td>Última Pregunta
                                </td>
                                <td>Última Respuesta</td>
                            </tr>
                            <tr class="rowA">

                                <td>
                                    <%--<uc1:Fechas runat="server" ID="txtUltimaPregunta" />--%>
                                    <asp:TextBox ID="txtUltimaPregunta" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <%--<uc1:Fechas runat="server" ID="txtUltimaRespuesta" />--%>
                                    <asp:TextBox ID="txtUltimaRespuesta" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Panel ID="pnlFechaEntregaESMC" runat="server" GroupingText="Fecha entrega ES_MC por parte del área solicitante" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Fecha
                                </td>
                                <td>Definitiva
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <uc1:Fechas runat="server" ID="txtFechaEntregaES_MC" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaEntregaES_MCDefinitiva" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkNoAplicaFechaEntregaES_MC" runat="server" Checked="false" Text="No Aplica" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlFechaEntregaFCT" runat="server" GroupingText="Cambio en los DT después de Aprobación" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Fecha
                                </td>
                                <td>Definitiva
                                </td>
                            </tr>
                            <tr class="rowA">

                                <td>
                                    <uc1:Fechas runat="server" ID="txtFechaEntregaFCTORequerimiento" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaEntregaFCTORequerimientoDefinitiva" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkNoAplicaFechaEntregaFCTORequerimiento" runat="server" Checked="false" Text="No Aplica" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlFechaEnvioSDC" runat="server" GroupingText="Fecha envío SDC" BorderWidth="1">
                        <table width="90%" align="center">

                            <tr class="rowB">
                                <td>Fecha 
                                </td>
                                <td>Definitiva 
                                </td>
                            </tr>
                            <tr class="rowA">

                                <td>
                                    <uc1:Fechas runat="server" ID="txtFechaEnvioSDC" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaEnvioSDCDefinitiva" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkNoAplicaFechaEnvioSDC" runat="server" Checked="false" Text="No Aplica" />
                                </td>
                            </tr>

                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlPlazoEstablecidoCotizaciones" runat="server" GroupingText="Plazo establecido entrega cotizaciones" BorderWidth="1">
                        <table width="90%" align="center">
                            <tr class="rowB">

                                <td>Fecha
                                </td>
                                <td>Definitiva
                                </td>
                            </tr>
                            <tr class="rowA">

                                <td>
                                    <uc1:Fechas runat="server" ID="txtPlazoEntregaCotizaciones" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPlazoEntregaCotizacionesDefinitiva" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkNoAplicaPlazoEntregaCotizaciones" runat="server" Checked="false" Text="No Aplica" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Panel ID="pnlActividadesAdicionales" runat="server" BorderWidth="1" GroupingText="Actividades Adicionales">
                        <table width="90%" align="center">
                            <tr class="rowB">
                                <td>Actividad
                                </td>
                                <td>Fecha
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:DropDownList ID="ddlActividadAdicional" runat="server"></asp:DropDownList>
                                </td>
                                <td>
                                    <uc1:Fechas runat="server" ID="txtFechaActividadAdicional" />

                                </td>
                            </tr>
                            <tr>
                                <td>Detalle
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDetalleActividadAdicional" TextMode="MultiLine"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteDetalleActividadAdicional" runat="server" TargetControlID="txtDetalleActividadAdicional"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkNoAplicaActividadAdicional" runat="server" Checked="false" Text="No Aplica" />
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdfIdActividadAdicional" runat="server" Value="0" />
                                    <asp:ImageButton ID="btnAgregarActividad" runat="server" ImageUrl="~/Image/btn/add.gif" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="gvActividades" align="center" runat="server" AutoGenerateColumns="false" Width="100%" GridLines="None" ShowHeaderWhenEmpty="true">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("IdGestionActividadAdicional").ToString()+"-"+((GridViewRow) Container).RowIndex %>' ImageUrl="~/Image/btn/edit.gif"
                                            ToolTip="Editar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="IdGestionActividadAdicional" DataField="IdGestionActividadAdicional" Visible="false" />
                                <asp:BoundField HeaderText="Actividad" DataField="Actividad" />
                                <asp:BoundField HeaderText="Detalle" DataField="Detalle" />
                                <asp:TemplateField HeaderText="Fecha" SortExpression="FechaActividadAdicional">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecha" runat="server" Text=' <%#Eval("FechaActividadAdicional")==null?string.Empty: Convert.ToDateTime(Eval("FechaActividadAdicional").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="No Aplica">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkNoAplicaActividadAdicional" Checked='<%#Eval("NoAplicaActividadAdicional") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" CommandArgument='<%#Eval("IdGestionActividadAdicional").ToString()+"-"+((GridViewRow) Container).RowIndex %>' ImageUrl="~/Image/btn/delete.gif"
                                            ToolTip="Eliminar" OnClientClick="return confirm('¿Seguro que desea eliminar el registro?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>



    </asp:Panel>
</asp:Content>
