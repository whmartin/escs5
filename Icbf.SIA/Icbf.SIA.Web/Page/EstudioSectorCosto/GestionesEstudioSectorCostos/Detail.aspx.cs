//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_GestionesEstudioSectorCostos_Detail</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;

/// <summary>
/// Clase para ver detalle de gestiones de estudio 
/// </summary>
public partial class Page_GestionesEstudioSectorCostos_Detail : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/GestionesEstudioSectorCostos";

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Método para cargar los datos de un registro
    /// </summary>
    private void CargarDatos()
    {
        try
        {

            decimal vIdConsecutivoGestionEstudio = Convert.ToDecimal(GetSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio"));
            RemoveSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio");

            if (GetSessionParameter("GestionesEstudioSectorCostos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("GestionesEstudioSectorCostos.Guardado");
            GestionesEstudioSectorCostos vGestionesEstudioSectorCostos = new GestionesEstudioSectorCostos();
            vGestionesEstudioSectorCostos = vResultadoEstudioSectorService.ConsultarGestionEstudioSector(vIdConsecutivoGestionEstudio);
            hdfIdGestionEstudio.Value = vGestionesEstudioSectorCostos.IdGestionEstudio.ToString();
            txtIdConsecutivoEstudio.Text = vGestionesEstudioSectorCostos.IdRegistroSolicitudEstudioSectoryCaso.ToString();
            txtFechaPreguntaOComentario.SetDate(vGestionesEstudioSectorCostos.FechaPreguntaOComentario);
            txtFechaRespuesta.SetDate(vGestionesEstudioSectorCostos.FechaRespuesta);
            txtUltimaPregunta.Text = vGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario == null ? string.Empty : vGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");
            txtUltimaRespuesta.Text = vGestionesEstudioSectorCostos.UltimaFechaRespuesta == null ? string.Empty : vGestionesEstudioSectorCostos.UltimaFechaRespuesta.Value.ToString("dd/MM/yyyy");
            txtFechaEntregaES_MC.SetDate(vGestionesEstudioSectorCostos.FechaEntregaES_MC);
            txtFechaEntregaES_MCDefinitiva.Text = vGestionesEstudioSectorCostos.FechaEntregaES_MCDef == null ? string.Empty : vGestionesEstudioSectorCostos.FechaEntregaES_MCDef.Value.ToString("dd/MM/yyyy");
            chkNoAplicaFechaEntregaES_MC.Checked = vGestionesEstudioSectorCostos.NoAplicaFechaEntregaES_MC;
            txtFechaEntregaFCTORequerimiento.SetDate(vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimiento);
            txtFechaEntregaFCTORequerimientoDefinitiva.Text = vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef == null ? string.Empty : vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef.Value.ToString("dd/MM/yyyy"); ;
            chkNoAplicaFechaEntregaFCTORequerimiento.Checked = (vGestionesEstudioSectorCostos.NoAplicaFechaentregaFCTOrequerimiento);
            txtFechaEnvioSDC.SetDate(vGestionesEstudioSectorCostos.FechaEnvioSDC);
            txtFechaEnvioSDCDefinitiva.Text = vGestionesEstudioSectorCostos.FechaEnvioSDCDef == null ? string.Empty : vGestionesEstudioSectorCostos.FechaEnvioSDCDef.Value.ToString("dd/MM/yyyy");
            chkNoAplicaFechaEnvioSDC.Checked = vGestionesEstudioSectorCostos.NoAplicaFechaEnvioSDC;
            txtPlazoEntregaCotizaciones.SetDate(vGestionesEstudioSectorCostos.PlazoEntregaCotizaciones);
            txtPlazoEntregaCotizacionesDefinitiva.Text = vGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef == null ? string.Empty : vGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef.Value.ToString("dd/MM/yyyy"); ;
            chkNoAplicaPlazoEntregaCotizaciones.Checked = (vGestionesEstudioSectorCostos.NoAplicaPlazoEntregaCotizaciones);

            txtFechaPreguntaOComentario.Enabled(false);
            txtFechaRespuesta.Enabled(false);
            txtFechaEntregaES_MC.Enabled(false);
            txtFechaEntregaFCTORequerimiento.Enabled(false);
            txtFechaEnvioSDC.Enabled(false);
            txtPlazoEntregaCotizaciones.Enabled(false);
            txtFechaActividadAdicional.Enabled(false);
            CargarGrilla(vIdConsecutivoGestionEstudio);
            pnlContendor.Enabled = false;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGestionesEstudioSectorCostos.UsuarioCrea, vGestionesEstudioSectorCostos.FechaCrea, vGestionesEstudioSectorCostos.UsuarioModifica, vGestionesEstudioSectorCostos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para eliminar un registro
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            decimal vIdConsecutivoGestionEstudio = decimal.Parse(hdfIdGestionEstudio.Value);
            GestionesEstudioSectorCostos vGestionesEstudioSectorCostos = new GestionesEstudioSectorCostos();
            vGestionesEstudioSectorCostos = vResultadoEstudioSectorService.ConsultarGestionEstudioSector(vIdConsecutivoGestionEstudio);
            InformacionAudioria(vGestionesEstudioSectorCostos, this.PageName, vSolutionPage);
            int vResultado = vResultadoEstudioSectorService.EliminarGestionEstudioSector(vGestionesEstudioSectorCostos);

            toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            SetSessionParameter("GestionesEstudioSectorCostos.Eliminado", "1");
            NavigateTo(SolutionPage.List);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar la grilla gestiones
    /// </summary>
    /// <param name="pIdGestionEstudio">Id Consecutivo del registro de gestión </param>
    private void CargarGrilla(decimal pIdGestionEstudio)
    {
        if (vResultadoEstudioSectorService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio).Count == 0 || vResultadoEstudioSectorService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio) == null)
        {
            pnlActividadesAdicionales.Visible = false;
        }
        else
        {
            gvActividades.DataSource = vResultadoEstudioSectorService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
            gvActividades.DataBind();
            pnlActividadesAdicionales.Visible = true;
            foreach (GridViewRow row in gvActividades.Rows)
            {
                ImageButton btn = row.FindControl("btnEliminar") as ImageButton;
                btn.Enabled = false;

            }
        }
    }

    /// <summary>
    /// Método para configurar la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Gestiones Estudio Sector Costos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            Utilidades.LlenarDropDownList(ddlActividadAdicional, vResultadoEstudioSectorService.ConsultarActividades());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento click del botón nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click del botón editar
    /// </summary>
    /// <param name="sender">Boton btnEditar</param>
    /// <param name="e">Evento click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio", hdfIdGestionEstudio.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento click del botón eliminar
    /// </summary>
    /// <param name="sender">Boton btnEliminar</param>
    /// <param name="e">Evento click</param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Evento click del botón buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }


    #endregion



}
