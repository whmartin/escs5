//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_GestionesEstudioSectorCostos_Add</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Entity;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Service;
using System.Threading;
using System.Globalization;

/// <summary>
/// Clase para agregar registros de gestiones de estudio 
/// </summary>
public partial class Page_GestionesEstudioSectorCostos_Add : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();
    string PageName = "EstudioSectorCosto/GestionesEstudioSectorCostos";

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    /// 
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-CO");
        Thread.CurrentThread.CurrentCulture = new CultureInfo("es-CO");
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                    txtFechaMaximaPregunta.Visible = true;
                    lblFechaMaximaPregunta.Visible = true;
                }
                else
                {
                    txtFechaMaximaPregunta.Visible = false;
                    lblFechaMaximaPregunta.Visible = false;
                }

            }
        }
    }

    #region METODOS

    /// <summary>
    /// M�todo para Consultar las fechas definitivas
    /// </summary>
    /// <param name="pIdConsecutivoEstudio">Id consucutivo gestion</param>
    private void ConsultarDefinitivas(long pIdConsecutivoEstudio)
    {
        GestionesEstudioSectorCostos vGestion = vResultadoEstudioSectorService.ConsultarGestionDefinitivas(pIdConsecutivoEstudio);
        if (vGestion != null)
        {
            string vNumeroReproceso = string.Empty;
            txtUltimaPregunta.Text = vGestion.FechaPreguntaOComentarioDef == null ? string.Empty : vGestion.FechaPreguntaOComentarioDef.Value.ToShortDateString();
            txtUltimaRespuesta.Text = vGestion.FechaRespuestaDef == null ? string.Empty : vGestion.FechaRespuestaDef.Value.ToShortDateString();
            txtFechaEntregaES_MCDefinitiva.Text = vGestion.FechaEntregaES_MCDef == null ? string.Empty : vGestion.FechaEntregaES_MCDef.Value.ToShortDateString();
            txtFechaEntregaFCTORequerimientoDefinitiva.Text = vGestion.FechaEntregaFCTORequerimientoDef == null ? string.Empty : vGestion.FechaEntregaFCTORequerimientoDef.Value.ToShortDateString();
            txtFechaEnvioSDCDefinitiva.Text = vGestion.FechaEnvioSDCDef == null ? string.Empty : vGestion.FechaEnvioSDCDef.Value.ToShortDateString();
            txtPlazoEntregaCotizacionesDefinitiva.Text = vGestion.PlazoEntregaCotizacionesDef == null ? string.Empty : vGestion.PlazoEntregaCotizacionesDef.Value.ToShortDateString();
            vNumeroReproceso = vGestion.NumeroReproceso == null ? string.Empty : vGestion.NumeroReproceso.ToString();

            if (vNumeroReproceso.Contains("D"))
            {
                chkNoAplicaFechaEnvioSDC.Checked = true;
                chkNoAplicaFechaEnvioSDC.Enabled = false;
                txtFechaEnvioSDC.Enabled(!chkNoAplicaFechaEnvioSDC.Checked);
            }
            else if (vNumeroReproceso.Contains("E"))
            {
                chkNoAplicaFechaEnvioSDC.Checked = false;
                chkNoAplicaFechaEnvioSDC.Enabled = true;
                txtFechaEnvioSDC.Enabled(!chkNoAplicaFechaEnvioSDC.Checked);
            }

        }
        else
        {
            txtUltimaPregunta.Text = string.Empty;
            txtUltimaRespuesta.Text = string.Empty;
            txtFechaEntregaES_MCDefinitiva.Text = string.Empty;
            txtFechaEntregaFCTORequerimientoDefinitiva.Text = string.Empty;
            txtFechaEnvioSDCDefinitiva.Text = string.Empty;
            txtPlazoEntregaCotizacionesDefinitiva.Text = string.Empty;
        }
    }

    /// <summary>
    /// M�tod para guardar el registro de gesti�n
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (!ValidarFechas())
            {
                return;
            }
            if (!ValidarFestivos())
            {
                return;
            }
            decimal vResultado;
            GestionesEstudioSectorCostos vGestionesEstudioSectorCostos = new GestionesEstudioSectorCostos();

            vGestionesEstudioSectorCostos.IdConsecutivoEstudio = long.Parse(txtIdConsecutivoEstudio.Text);
            vGestionesEstudioSectorCostos.IdRegistroSolicitudEstudioSectoryCaso = long.Parse(txtIdConsecutivoEstudio.Text);
            vGestionesEstudioSectorCostos.FechaPreguntaOComentario = txtFechaPreguntaOComentario.GetDate();
            vGestionesEstudioSectorCostos.FechaRespuesta = txtFechaRespuesta.GetDate();
            vGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario = txtUltimaPregunta.Text.Equals(string.Empty) ? txtFechaPreguntaOComentario.GetDate() : Convert.ToDateTime(txtUltimaPregunta.Text);
            vGestionesEstudioSectorCostos.UltimaFechaRespuesta = txtUltimaRespuesta.Text.Equals(string.Empty) ? txtFechaRespuesta.GetDate() : Convert.ToDateTime(txtUltimaRespuesta.Text);
            vGestionesEstudioSectorCostos.FechaEntregaES_MC = txtFechaEntregaES_MC.GetDate();
            vGestionesEstudioSectorCostos.FechaEntregaES_MCDef = txtFechaEntregaES_MCDefinitiva.Text.Equals(string.Empty) ? txtFechaEntregaES_MC.GetDate() : Convert.ToDateTime(txtFechaEntregaES_MCDefinitiva.Text);
            vGestionesEstudioSectorCostos.NoAplicaFechaEntregaES_MC = chkNoAplicaFechaEntregaES_MC.Checked;
            vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimiento = txtFechaEntregaFCTORequerimiento.GetDate();
            vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef = txtFechaEntregaFCTORequerimientoDefinitiva.Text.Equals(string.Empty) ? txtFechaEntregaFCTORequerimiento.GetDate() : Convert.ToDateTime(txtFechaEntregaFCTORequerimientoDefinitiva.Text);
            vGestionesEstudioSectorCostos.NoAplicaFechaentregaFCTOrequerimiento = chkNoAplicaFechaEntregaFCTORequerimiento.Checked;
            vGestionesEstudioSectorCostos.FechaEnvioSDC = txtFechaEnvioSDC.GetDate();
            vGestionesEstudioSectorCostos.FechaEnvioSDCDef = txtFechaEnvioSDCDefinitiva.Text.Equals(string.Empty) ? txtFechaEnvioSDC.GetDate() : Convert.ToDateTime(txtFechaEnvioSDCDefinitiva.Text);
            vGestionesEstudioSectorCostos.NoAplicaFechaEnvioSDC = chkNoAplicaFechaEnvioSDC.Checked;
            vGestionesEstudioSectorCostos.PlazoEntregaCotizaciones = txtPlazoEntregaCotizaciones.GetDate();
            vGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef = txtPlazoEntregaCotizacionesDefinitiva.Text.Equals(string.Empty) ? txtPlazoEntregaCotizaciones.GetDate() : Convert.ToDateTime(txtPlazoEntregaCotizacionesDefinitiva.Text);
            vGestionesEstudioSectorCostos.NoAplicaPlazoEntregaCotizaciones = chkNoAplicaPlazoEntregaCotizaciones.Checked;


            if (Request.QueryString["oP"] == "E")
            {
                vGestionesEstudioSectorCostos.UsuarioModifica = GetSessionUser().NombreUsuario;
                vGestionesEstudioSectorCostos.IdGestionEstudio = decimal.Parse(hdfIdGestionEstudio.Value);
                InformacionAudioria(vGestionesEstudioSectorCostos, this.PageName, SolutionPage.Edit);
                vResultadoEstudioSectorService.ModificarGestionesEstudioSectorCostos(vGestionesEstudioSectorCostos);
                vResultado = vGestionesEstudioSectorCostos.IdGestionEstudio;
            }
            else
            {
                vGestionesEstudioSectorCostos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vGestionesEstudioSectorCostos, this.PageName, SolutionPage.Add);
                vResultado = vResultadoEstudioSectorService.InsertarGestionesEstudioSectorCostos(vGestionesEstudioSectorCostos);
                vGestionesEstudioSectorCostos.IdGestionEstudio = vResultado;
            }

            GuardarActividadesAdicionales(vResultado);
            SetSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio", vGestionesEstudioSectorCostos.IdGestionEstudio);
            SetSessionParameter("GestionesEstudioSectorCostos.Guardado", "1");
            NavigateTo(SolutionPage.Detail);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para guardar las actividades adicionales
    /// </summary>
    /// <param name="pIdGestionEstudio">Id consecutivo registro de gesti�n</param>
    protected void GuardarActividadesAdicionales(decimal pIdGestionEstudio)
    {
        List<GestionesActividadesAdicionales> vLstGestionActividades = ViewState[Constantes.ViewStateActividadesGestion] as List<GestionesActividadesAdicionales>;

        if (vLstGestionActividades != null)
        {
            foreach (GestionesActividadesAdicionales actividad in vLstGestionActividades)
            {
                actividad.IdGestionEstudio = pIdGestionEstudio;
                if (actividad.IdGestionActividadAdicional == 0)
                {
                    vResultadoEstudioSectorService.InsertarActividadesEstudioSector(actividad);
                }
                else
                {
                    vResultadoEstudioSectorService.ModificarActividadesEstudioSector(actividad);
                }
            }
        }
    }

    /// <summary>
    /// M�todo para configurar la pagina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Gestiones Estudio Sector Costos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los datos de un registro de gesti�n
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            decimal vIdConsecutivoGestionEstudio = Convert.ToDecimal(GetSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio"));
            RemoveSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio");

            GestionesEstudioSectorCostos vGestionesEstudioSectorCostos = new GestionesEstudioSectorCostos();
            vGestionesEstudioSectorCostos = vResultadoEstudioSectorService.ConsultarGestionEstudioSector(vIdConsecutivoGestionEstudio);
            hdfIdGestionEstudio.Value = vGestionesEstudioSectorCostos.IdGestionEstudio.ToString();
            txtIdConsecutivoEstudio.Text = vGestionesEstudioSectorCostos.IdRegistroSolicitudEstudioSectoryCaso.ToString();
            txtFechaPreguntaOComentario.SetDate(vGestionesEstudioSectorCostos.FechaPreguntaOComentario);
            txtFechaRespuesta.SetDate(vGestionesEstudioSectorCostos.FechaRespuesta);
            txtUltimaPregunta.Text = vGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario == null ? string.Empty : vGestionesEstudioSectorCostos.UltimaFechaPreguntaOComentario.Value.ToString("dd/MM/yyyy");
            txtUltimaRespuesta.Text = vGestionesEstudioSectorCostos.UltimaFechaRespuesta == null ? string.Empty : vGestionesEstudioSectorCostos.UltimaFechaRespuesta.Value.ToString("dd/MM/yyyy");
            txtFechaEntregaES_MC.SetDate(vGestionesEstudioSectorCostos.FechaEntregaES_MC);
            txtFechaEntregaES_MCDefinitiva.Text = vGestionesEstudioSectorCostos.FechaEntregaES_MCDef == null ? string.Empty : vGestionesEstudioSectorCostos.FechaEntregaES_MCDef.Value.ToString("dd/MM/yyyy");
            chkNoAplicaFechaEntregaES_MC.Checked = vGestionesEstudioSectorCostos.NoAplicaFechaEntregaES_MC;
            txtFechaEntregaES_MC.Enabled(!chkNoAplicaFechaEntregaES_MC.Checked);
            txtFechaEntregaFCTORequerimiento.SetDate(vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimiento);
            txtFechaEntregaFCTORequerimientoDefinitiva.Text = vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef == null ? string.Empty : vGestionesEstudioSectorCostos.FechaEntregaFCTORequerimientoDef.Value.ToString("dd/MM/yyyy"); ;
            chkNoAplicaFechaEntregaFCTORequerimiento.Checked = (vGestionesEstudioSectorCostos.NoAplicaFechaentregaFCTOrequerimiento);
            txtFechaEntregaFCTORequerimiento.Enabled(!chkNoAplicaFechaEntregaFCTORequerimiento.Checked);
            txtFechaEnvioSDC.SetDate(vGestionesEstudioSectorCostos.FechaEnvioSDC);
            txtFechaEnvioSDCDefinitiva.Text = vGestionesEstudioSectorCostos.FechaEnvioSDCDef == null ? string.Empty : vGestionesEstudioSectorCostos.FechaEnvioSDCDef.Value.ToString("dd/MM/yyyy");
            chkNoAplicaFechaEnvioSDC.Checked = vGestionesEstudioSectorCostos.NoAplicaFechaEnvioSDC;
            txtFechaEnvioSDC.Enabled(!chkNoAplicaFechaEnvioSDC.Checked);
            txtPlazoEntregaCotizaciones.SetDate(vGestionesEstudioSectorCostos.PlazoEntregaCotizaciones);
            txtPlazoEntregaCotizacionesDefinitiva.Text = vGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef == null ? string.Empty : vGestionesEstudioSectorCostos.PlazoEntregaCotizacionesDef.Value.ToString("dd/MM/yyyy"); ;
            chkNoAplicaPlazoEntregaCotizaciones.Checked = (vGestionesEstudioSectorCostos.NoAplicaPlazoEntregaCotizaciones);
            txtPlazoEntregaCotizaciones.Enabled(!chkNoAplicaPlazoEntregaCotizaciones.Checked);
            CargarGrilla(vIdConsecutivoGestionEstudio);

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGestionesEstudioSectorCostos.UsuarioCrea, vGestionesEstudioSectorCostos.FechaCrea, vGestionesEstudioSectorCostos.UsuarioModifica, vGestionesEstudioSectorCostos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Mpetodo para cargar grilla de gesti�n
    /// </summary>
    /// <param name="pIdGestionEstudio">Id consecutivo registro de gesti�n</param>
    private void CargarGrilla(decimal pIdGestionEstudio)
    {
        List<GestionesActividadesAdicionales> vLstGestionActividades = vResultadoEstudioSectorService.ConsultarGestionesActividadesAdicionales(pIdGestionEstudio);
        ViewState[Constantes.ViewStateActividadesGestion] = vLstGestionActividades;
        gvActividades.DataSource = vLstGestionActividades;
        gvActividades.DataBind();
        foreach (GridViewRow row in gvActividades.Rows)
        {
            ImageButton btn = row.FindControl("btnEliminar") as ImageButton;
            btn.Enabled = false;

        }

    }

    /// <summary>
    /// M�todo para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            Utilidades.LlenarDropDownList(ddlActividadAdicional, vResultadoEstudioSectorService.ConsultarActividades());
            txtIdConsecutivoEstudio.Enabled = false;
            txtFechaPreguntaOComentario.ponerFocus();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para agregar actividades
    /// </summary>
    protected void AgregarActividad()
    {
        btnAgregarActividad.Focus();
        toolBar.LipiarMensajeError();
        btnAgregarActividad.ImageUrl = "~/Image/btn/add.gif";
        if (ddlActividadAdicional.SelectedValue.Equals(Constantes.ValorDefecto) || (txtFechaActividadAdicional.GetDate() == null && chkNoAplicaActividadAdicional.Checked == false) || txtDetalleActividadAdicional.Text.Equals(string.Empty))
        {
            toolBar.MostrarMensajeError("No se han ingresado datos para la actividad adicional que desea agregar");
            return;
        }

        List<GestionesActividadesAdicionales> vLstActividades = ViewState[Constantes.ViewStateActividadesGestion] as List<GestionesActividadesAdicionales>;
        if (vLstActividades == null)
        {
            vLstActividades = new List<GestionesActividadesAdicionales>();
        }
        if (txtFechaActividadAdicional.EsDiaFestivo())
        {
            return;
        }
        vLstActividades.Add(new GestionesActividadesAdicionales
        {
            IdGestionActividadAdicional = decimal.Parse(hdfIdActividadAdicional.Value),
            Detalle = txtDetalleActividadAdicional.Text,
            IdActividad = int.Parse(ddlActividadAdicional.SelectedValue),
            FechaActividadAdicional = txtFechaActividadAdicional.GetDate(),
            NoAplicaActividadAdicional = chkNoAplicaActividadAdicional.Checked,
            Actividad = ddlActividadAdicional.SelectedItem.Text,
            FechaCrea = DateTime.Now,
            FechaModifica = DateTime.Now,
            UsuarioCrea = GetSessionUser().NombreUsuario,
            UsuarioModifica = GetSessionUser().NombreUsuario
        });
        gvActividades.DataSource = vLstActividades;
        gvActividades.DataBind();
        ViewState[Constantes.ViewStateActividadesGestion] = vLstActividades;
        hdfIdActividadAdicional.Value = "0";
        Utilidades.SetControls(pnlActividadesAdicionales);

    }

    /// <summary>
    /// M�todo para editar o eliminar actividad
    /// </summary>
    /// <param name="pCommandArgument"></param>
    /// <param name="pEsEliminar"></param>
    protected void EditarEliminarActividad(string pCommandArgument, bool pEsEliminar)
    {
        List<GestionesActividadesAdicionales> vLstActividades = ViewState[Constantes.ViewStateActividadesGestion] as List<GestionesActividadesAdicionales>;
        if (vLstActividades == null)
        {
            return;
        }
        string[] argumentos = pCommandArgument.Split('-');
        decimal IdActividad = decimal.Parse(argumentos[0]);
        int index = int.Parse(argumentos[1]);
        GestionesActividadesAdicionales indicador;
        if (IdActividad == 0)
        {
            indicador = vLstActividades[index];
            vLstActividades.RemoveAt(index);
        }
        else
        {
            indicador = vLstActividades.Where(x => x.IdGestionActividadAdicional == IdActividad).FirstOrDefault();
            vLstActividades.Remove(indicador);
        }

        if (!pEsEliminar)
        {
            ddlActividadAdicional.SelectedValue = indicador.IdActividad.ToString();
            txtFechaActividadAdicional.SetDate(indicador.FechaActividadAdicional);
            txtDetalleActividadAdicional.Text = indicador.Detalle;
            chkNoAplicaActividadAdicional.Checked = indicador.NoAplicaActividadAdicional;
            hdfIdActividadAdicional.Value = indicador.IdGestionActividadAdicional.ToString();
            btnAgregarActividad.ImageUrl = "~/Image/btn/apply.png";
            btnAgregarActividad.ToolTip = "Aplicar";
        }


        gvActividades.DataSource = vLstActividades;
        gvActividades.DataBind();
        ViewState[Constantes.ViewStateActividadesGestion] = vLstActividades;
    }

    /// <summary>
    /// N�todo paa validar las fechas del registro de gesti�n
    /// </summary>
    /// <returns></returns>
    public bool ValidarFechas()
    {
        bool vEsValido = false;
        if (txtFechaPreguntaOComentario.GetDate() != null && txtFechaRespuesta.GetDate() != null)
        {
            if (txtFechaRespuesta.GetDate() >= txtFechaPreguntaOComentario.GetDate())
            {
                vEsValido = true;
            }
        }
        if (txtFechaPreguntaOComentario.GetDate() == null && txtFechaRespuesta.GetDate() == null)
        {
            vEsValido = true;
        }
        if (txtFechaPreguntaOComentario.GetDate() == null && txtFechaRespuesta.GetDate() != null)
        {
            vEsValido = true;
        }
        if (txtFechaPreguntaOComentario.GetDate() != null && txtFechaRespuesta.GetDate() == null)
        {
            vEsValido = true;
        }
        lblValidarFechaRespuesta.Visible = !vEsValido;
        return vEsValido;
    }

    /// <summary>
    /// M�todo para validar festivos
    /// </summary>
    /// <returns></returns>
    public bool ValidarFestivos()
    {
        bool vEsFestivoA = txtFechaPreguntaOComentario.EsDiaFestivo();
        bool vEsFestivoB = txtFechaRespuesta.EsDiaFestivo();
        bool vEsFestivoC = txtFechaEntregaES_MC.EsDiaFestivo();
        bool vEsFestivoD = txtFechaEntregaFCTORequerimiento.EsDiaFestivo();
        bool vEsFestivoE = txtFechaEnvioSDC.EsDiaFestivo();
        bool vEsFestivoF = txtPlazoEntregaCotizaciones.EsDiaFestivo();
        if (vEsFestivoA ||
            vEsFestivoB ||
            vEsFestivoC ||
            vEsFestivoD ||
            vEsFestivoE ||
            vEsFestivoF)
        {

            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// M�todo para guardar registro
    /// </summary>
    /// <param name="sender">Boton btnGuardar</param>
    /// <param name="e">Evento click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// M�todo para agregar un registro
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// M�todo para buscar registro
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }


    /// <summary>
    /// Evento selecci�n no aplica
    /// </summary>
    /// <param name="sender">Checkbox chkNoAplicaFechaEntregaES</param>
    /// <param name="e">Evento click</param>
    protected void chkNoAplicaFechaEntregaES_MC_CheckedChanged(object sender, EventArgs e)
    {
        txtFechaEntregaES_MC.Enabled(!chkNoAplicaFechaEntregaES_MC.Checked);
        txtFechaEntregaES_MC.SetDate((DateTime?)null);
        if (!chkNoAplicaFechaEntregaES_MC.Checked && txtIdConsecutivoEstudio.Text != "")
        {
            ConsultarDefinitivas(long.Parse(txtIdConsecutivoEstudio.Text));
        }
        else
        {
            txtFechaEntregaES_MCDefinitiva.Text = string.Empty;
        }

    }

    /// <summary>
    /// Evento selecci�n no aplica
    /// </summary>
    /// <param name="sender">Checkbox chkNoAplicaFechaEntregaFCTORequerimiento</param>
    /// <param name="e">Evento click</param>
    protected void chkNoAplicaFechaEntregaFCTORequerimiento_CheckedChanged(object sender, EventArgs e)
    {
        txtFechaEntregaFCTORequerimiento.Enabled(!chkNoAplicaFechaEntregaFCTORequerimiento.Checked);
        txtFechaEntregaFCTORequerimiento.SetDate((DateTime?)null);
        if (!chkNoAplicaFechaEntregaFCTORequerimiento.Checked && txtIdConsecutivoEstudio.Text != "")
        {
            ConsultarDefinitivas(long.Parse(txtIdConsecutivoEstudio.Text));
        }
        else
        {
            txtFechaEntregaFCTORequerimientoDefinitiva.Text = string.Empty;
        }
    }

    /// <summary>
    /// Evento selecci�n no aplica
    /// </summary>
    /// <param name="sender">Checkbox chkNoAplicaFechaEnvioSDC</param>
    /// <param name="e">Evento click</param>
    protected void chkNoAplicaFechaEnvioSDC_CheckedChanged(object sender, EventArgs e)
    {
        txtFechaEnvioSDC.Enabled(!chkNoAplicaFechaEnvioSDC.Checked);
        txtFechaEnvioSDC.SetDate((DateTime?)null);
        if (!chkNoAplicaFechaEnvioSDC.Checked && txtIdConsecutivoEstudio.Text != "")
        {
            ConsultarDefinitivas(long.Parse(txtIdConsecutivoEstudio.Text));
        }
        else
        {
            txtFechaEnvioSDCDefinitiva.Text = string.Empty;
        }
    }

    /// <summary>
    /// Evento selecci�n no aplica
    /// </summary>
    /// <param name="sender">Checkbox chkNoAplicaPlazoEntregaCotizaciones</param>
    /// <param name="e">Evento click</param>
    protected void chkNoAplicaPlazoEntregaCotizaciones_CheckedChanged(object sender, EventArgs e)
    {
        txtPlazoEntregaCotizaciones.Enabled(!chkNoAplicaPlazoEntregaCotizaciones.Checked);
        txtPlazoEntregaCotizaciones.SetDate((DateTime?)null);
        if (!chkNoAplicaPlazoEntregaCotizaciones.Checked && txtIdConsecutivoEstudio.Text != "")
        {
            ConsultarDefinitivas(long.Parse(txtIdConsecutivoEstudio.Text));
        }
        else
        {
            txtPlazoEntregaCotizacionesDefinitiva.Text = string.Empty;
        }
    }

    /// <summary>
    /// Evento selecci�n no aplica
    /// </summary>
    /// <param name="sender">Checkbox chkNoAplicaActividadAdicional</param>
    /// <param name="e">Evento click</param>
    protected void chkNoAplicaActividadAdicional_CheckedChanged(object sender, EventArgs e)
    {
        txtFechaActividadAdicional.Enabled(!chkNoAplicaActividadAdicional.Checked);
        txtFechaActividadAdicional.SetDate((DateTime?)null);
    }

    /// <summary>
    /// Evento selecci�n no aplica
    /// </summary>
    /// <param name="sender">Checkbox btnAgregarActividad</param>
    /// <param name="e">Evento click</param>
    protected void btnAgregarActividad_Click(object sender, ImageClickEventArgs e)
    {
        AgregarActividad();
    }

    /// <summary>
    /// Evento click editar eliminar
    /// </summary>
    /// <param name="sender">Grilla gvActividades</param>
    /// <param name="e">Evento click</param>
    protected void gvActividades_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Editar":
                EditarEliminarActividad(e.CommandArgument.ToString(), false);
                break;
            case "Eliminar":
                EditarEliminarActividad(e.CommandArgument.ToString(), true);
                break;
        }
    }

    /// <summary>
    /// Evento cuando se selecciona un consecutiov de estudio
    /// </summary>
    /// <param name="sender">Caja de texto txtIdConsecutivoEstudio</param>
    /// <param name="e">Evento Change</param>
    protected void txtIdConsecutivoEstudio_TextChanged(object sender, EventArgs e)
    {
        long vIdConsecutivoEstudio = long.Parse(txtIdConsecutivoEstudio.Text);
        txtIdConsecutivoEstudio.Enabled = false;
        ConsultarDefinitivas(vIdConsecutivoEstudio);



    }

    #endregion

}
