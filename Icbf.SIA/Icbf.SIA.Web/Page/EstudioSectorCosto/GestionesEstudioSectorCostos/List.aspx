<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_GestionesEstudioSectorCostos_List" %>

<%@ Register TagPrefix="uc1" TagName="consulta" Src="~/General/General/EstudioSectores/FormularioConsulta.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
   <uc1:consulta runat="server" ID="ucConsulta" />
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvGestionesEstudioSectorCostos" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="true"
                        GridLines="None" Width="100%" DataKeyNames="IdGestionEstudio" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvGestionesEstudioSectorCostos_PageIndexChanging" OnSelectedIndexChanged="gvGestionesEstudioSectorCostos_SelectedIndexChanged" OnSorting="gvGestionesEstudioSectorCostos_Sorting" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo  Estudio" DataField="IdRegistroSolicitudEstudioSectoryCaso"  SortExpression="IdRegistroSolicitudEstudioSectoryCaso" />
                            <asp:BoundField HeaderText="Nombre Abreviado" DataField="NombreAbreviado" SortExpression="NombreAbreviado" />
                            <asp:BoundField HeaderText="Actividad" DataField="Actividad" SortExpression="Actividad" />                            
                            <asp:TemplateField HeaderText="Fecha envío pregunta" SortExpression="FechaPreguntaOComentario" >
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaPreguntaOComentario" runat="server" Text=' <%#Eval("FechaPreguntaOComentario")==null?string.Empty:Convert.ToDateTime(Eval("FechaPreguntaOComentario").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="Fecha Respuesta" SortExpression="FechaRespuesta" >
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaRespuesta" runat="server" Text=' <%#Eval("FechaRespuesta")==null?string.Empty:Convert.ToDateTime(Eval("FechaRespuesta").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                                                      
                            <asp:TemplateField HeaderText="No Aplica">
                                <ItemTemplate >
                                    <asp:CheckBox runat="server" ID="chkNoAplica"  Checked='<%#Eval("NoAplica") %>' Enabled="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
