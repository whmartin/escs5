//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_GestionesEstudioSectorCostos_List</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Linq.Expressions;
using System.Data;

/// <summary>
/// Clase para consultar gestiones de estudio 
/// </summary>
public partial class Page_GestionesEstudioSectorCostos_List : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/GestionesEstudioSectorCostos";

    /// <summary>
    /// Instancia de clase GestionesEstudioSectorCostos
    /// </summary>
    GestionesEstudioSectorCostos vGestionEstudioSectorCosto = new GestionesEstudioSectorCostos();

    /// <summary>
    /// Instancia a capa de servicio ResultadoEstudioSectorService
    /// </summary>
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

   

    #region  METODOS

    /// <summary>
    /// M�todo para buscar registros de gesti�n
    /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            ucConsulta.GetValues();
            long? vIdConsecutivoEstudio = ucConsulta.vIdConsecutivoEstudio;
            string vNombreAbreviado = ucConsulta.vNombreAbreviado;
            string vObjeto = ucConsulta.vObjeto;
            int? vIdResponsableES = ucConsulta.vIdResponsableES;
            int? vIdResponsableEC = ucConsulta.vIdResponsableEC;
            int? vIdModalidadDeSeleccion = ucConsulta.vIdModalidadDeSeleccion;
            int? vDireccionSolicitante = ucConsulta.vDireccionSolicitante;
            int? vEstado = ucConsulta.vEstado;

            List<GestionesEstudioSectorCostos> vLstConsulta = vResultadoEstudioSectorService.ConsultarGestionEstudioSectores(vIdConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdResponsableES, vIdResponsableEC, vIdModalidadDeSeleccion, vDireccionSolicitante, vEstado);
            ViewState[Constantes.ViewStateConsultaGestion] = vLstConsulta;
            LLenarGrilla(vLstConsulta);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para configurar p�gina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvGestionesEstudioSectorCostos.PageSize = PageSize();
            gvGestionesEstudioSectorCostos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gestiones Estudio Sector Costos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo que redirecciona al seleccionar un registro
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvGestionesEstudioSectorCostos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("GestionesEstudioSectorCostos.IdConsecutivoEstudio", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar los datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("GestionesEstudioSectorCostos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("GestionesEstudioSectorCostos.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para ordenar la grilla registro de gestiones
    /// </summary>
    /// <param name="pSortExpresion">Expresi�n de ordenamiento</param>
    /// <param name="vLstConsulta">Lista de Gestiones a ordenar</param>
    protected void OrdenarGrilla(string pSortExpresion, List<GestionesEstudioSectorCostos> vLstConsulta)
    {
        if (vLstConsulta != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvGestionesEstudioSectorCostos.DataSource = vLstConsulta;
                gvGestionesEstudioSectorCostos.DataBind();
                return;

            }

            var param = Expression.Parameter(typeof(GestionesEstudioSectorCostos), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<GestionesEstudioSectorCostos, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvGestionesEstudioSectorCostos.DataSource = vLstConsulta.AsQueryable<GestionesEstudioSectorCostos>().OrderBy(sortExpression).ToList<GestionesEstudioSectorCostos>();
               
            }
            else
            {
                gvGestionesEstudioSectorCostos.DataSource = vLstConsulta.AsQueryable<GestionesEstudioSectorCostos>().OrderByDescending(sortExpression).ToList<GestionesEstudioSectorCostos>();
                
            };

            gvGestionesEstudioSectorCostos.DataBind();
        }
    }

    /// <summary>
    /// M�todo para cargar datos a la grilla de gesti�n
    /// </summary>
    /// <param name="pLstGestion"></param>
    protected void LLenarGrilla(List<GestionesEstudioSectorCostos> pLstGestion)
    {
        if (pLstGestion == null || pLstGestion.Count == 0)
        {
            gvGestionesEstudioSectorCostos.DataSource = null;
            gvGestionesEstudioSectorCostos.DataBind();
            return;
        }
        toolBar.LipiarMensajeError();
        string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
        if (pSortExpresion.Equals(string.Empty))
        {
            OrdenarGrilla(Constantes.SinOrden, pLstGestion);
        }
        else
        {
            OrdenarGrilla(pSortExpresion, pLstGestion);
        }

    }

    /// <summary>
    /// M�todo para exporta el resultado de la consulta
    /// </summary>
    private void Exportar()
    {
        toolBar.LipiarMensajeError();
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvGestionesEstudioSectorCostos.AllowPaging;
        Boolean vPaginadorError = gvGestionesEstudioSectorCostos.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvGestionesEstudioSectorCostos.AllowPaging = false;

        List<GestionesEstudioSectorCostos> vListaResultados = ViewState[Constantes.ViewStateConsultaGestion] as List<GestionesEstudioSectorCostos>;
        
        datos = Utilidades.GenerarDataTable(this.gvGestionesEstudioSectorCostos, vListaResultados);

        GridView datosexportar = new GridView();
        datosexportar.DataSource = datos;
        datosexportar.DataBind();
        vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "RegistroGestiones_" + DateTime.Now.ToString("ddMMyyyy"));
        gvGestionesEstudioSectorCostos.AllowPaging = vPaginador;


    }

    #endregion

    #region  EVENTOS

    /// <summary>
    /// Evento click del boton buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Evento click del boton nuevo
    /// </summary>
    /// <param name="sender">Bot�n btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click del boton exportar
    /// </summary>
    /// <param name="sender">Bot�n btnExportar</param>
    /// <param name="e">Evento click</param>
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if(gvGestionesEstudioSectorCostos.Rows.Count>0)
        {
            Exportar();
        }
        else
        {
            gvGestionesEstudioSectorCostos.DataSource = null;
            gvGestionesEstudioSectorCostos.DataBind();
        }
    }

    /// <summary>
    /// Evento click del boton ver detalle
    /// </summary>
    /// <param name="sender">Grilla gvGestionesEstudioSectorCostos</param>
    /// <param name="e">Evento click</param>
    protected void gvGestionesEstudioSectorCostos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvGestionesEstudioSectorCostos.SelectedRow);
    }

    /// <summary>
    /// Evento paginaci�n grilla registro de gestiones
    /// </summary>
    /// <param name="sender">Grilla gvGestionesEstudioSectorCostos</param>
    /// <param name="e">Evento click</param>
    protected void gvGestionesEstudioSectorCostos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGestionesEstudioSectorCostos.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Evento ordenaci�n grilla registro de gestiones
    /// </summary>
    /// <param name="sender">Grilla gvGestionesEstudioSectorCostos</param>
    /// <param name="e">Evento click</param>
    protected void gvGestionesEstudioSectorCostos_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ( ViewState[Constantes.ViewStateSortDirection]!=null && GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;           
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;     
        }

        OrdenarGrilla(e.SortExpression, ViewState[Constantes.ViewStateConsultaGestion] as List<GestionesEstudioSectorCostos>);
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;
      
    }

    #endregion   
    
    /// <summary>
    /// Atributo que describe la direcci�n del ordenamiento
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState[Constantes.ViewStateSortDirection] == null)
            {
                ViewState[Constantes.ViewStateSortDirection] = SortDirection.Ascending;
            }


            return (SortDirection)ViewState[Constantes.ViewStateSortDirection];
        }
        set { ViewState[Constantes.ViewStateSortDirection] = value; }
    }

   
}
