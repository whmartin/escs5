using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_EstadoEstudio_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/EstadoEstudio";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtNombre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            EstadoEstudio vEstadoEstudio = new EstadoEstudio();

            vEstadoEstudio.Nombre = Convert.ToString(txtNombre.Text).ToUpper();
            vEstadoEstudio.Motivo = Convert.ToString(txtMotivo.Text).ToUpper();
            vEstadoEstudio.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();
            vEstadoEstudio.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vEstadoEstudio.IdEstadoEstudio = Convert.ToInt32(hfIdEstadoEstudio.Value);
                    vEstadoEstudio.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vEstadoEstudio, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarEstadoEstudio(vEstadoEstudio);
                }
                else
                {
                    vEstadoEstudio.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vEstadoEstudio, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarEstadoEstudio(vEstadoEstudio);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("EstadoEstudio.IdEstadoEstudio", vEstadoEstudio.IdEstadoEstudio);
                    SetSessionParameter("EstadoEstudio.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe.");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de estados", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdEstadoEstudio = Convert.ToInt32(GetSessionParameter("EstadoEstudio.IdEstadoEstudio"));
            String NombreEstadoEstudio = vEstudioSectorCostoService.ConsultarEstadoEstudio(vIdEstadoEstudio).Nombre;
            hfNombreEstadoEstudio.Value = NombreEstadoEstudio;
            RemoveSessionParameter("EstadoEstudio.Id");

            EstadoEstudio vEstadoEstudio = new EstadoEstudio();
            vEstadoEstudio = vEstudioSectorCostoService.ConsultarEstadoEstudio(vIdEstadoEstudio);
            hfIdEstadoEstudio.Value = vEstadoEstudio.IdEstadoEstudio.ToString();
            txtNombre.Text = vEstadoEstudio.Nombre;
            txtMotivo.Text = vEstadoEstudio.Motivo;
            hfNombreMotivo.Value = vEstadoEstudio.Motivo;
            txtDescripcion.Text = vEstadoEstudio.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vEstadoEstudio.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vEstadoEstudio.UsuarioCrea, vEstadoEstudio.FechaCrea, vEstadoEstudio.UsuarioModifica, vEstadoEstudio.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void txtNombre_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
    protected void txtMotivo_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtNombre.Text.ToUpper().Equals(hfNombreEstadoEstudio.Value))
        {
            if (vEstudioSectorCostoService.ValidarEstadoEstudio(txtNombre.Text.ToUpper(), txtMotivo.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        else if (!txtMotivo.Text.ToUpper().Equals(hfNombreMotivo.Value))
        {
            if (vEstudioSectorCostoService.ValidarEstadoEstudio(txtNombre.Text.ToUpper(), txtMotivo.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
}
