using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_EstadoEstudio_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/EstadoEstudio";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("EstadoEstudio.IdEstadoEstudio", hfIdEstadoEstudio.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdEstadoEstudio = Convert.ToInt32(GetSessionParameter("EstadoEstudio.IdEstadoEstudio"));
            RemoveSessionParameter("EstadoEstudio.IdEstadoEstudio");

            if (GetSessionParameter("EstadoEstudio.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("EstadoEstudio.Guardado");


            EstadoEstudio vEstadoEstudio = new EstadoEstudio();
            vEstadoEstudio = vEstudioSectorCostoService.ConsultarEstadoEstudio(vIdEstadoEstudio);
            hfIdEstadoEstudio.Value = vEstadoEstudio.IdEstadoEstudio.ToString();
            txtNombre.Text = vEstadoEstudio.Nombre;
            txtMotivo.Text = vEstadoEstudio.Motivo;
            txtDescripcion.Text = vEstadoEstudio.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vEstadoEstudio.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdEstadoEstudio.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vEstadoEstudio.UsuarioCrea, vEstadoEstudio.FechaCrea, vEstadoEstudio.UsuarioModifica, vEstadoEstudio.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdEstadoEstudio = Convert.ToInt32(hfIdEstadoEstudio.Value);

            EstadoEstudio vEstadoEstudio = new EstadoEstudio();
            vEstadoEstudio = vEstudioSectorCostoService.ConsultarEstadoEstudio(vIdEstadoEstudio);
            InformacionAudioria(vEstadoEstudio, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarEstadoEstudio(vEstadoEstudio);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("EstadoEstudio.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de estados", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
