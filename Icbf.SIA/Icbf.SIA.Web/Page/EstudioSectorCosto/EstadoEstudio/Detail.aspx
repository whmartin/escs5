<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_EstadoEstudio_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdEstadoEstudio" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Estado de la solicitud *
            </td>
            <td>Motivo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre" Enabled="false" MaxLength="50" Width="90%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMotivo" Enabled="false" MaxLength="100" Width="98%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Descripci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
