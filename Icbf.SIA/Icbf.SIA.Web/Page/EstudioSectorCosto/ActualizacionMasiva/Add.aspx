﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="Add.aspx.cs" Inherits="Page_EstudioSectorCosto_ActualizacionMasiva_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta" GroupingText="Actualizar información PACCO y Contratos" BorderWidth="1">
    <table width="70%" align="center">
        <tr class="rowA">
            <td>
                <asp:Label ID="lblVigencia" runat="server">Recuerde que con esta opción se actualizará toda la información de los registros:<br /> 
                    Registro Inicial<br /> 
                    Cálculos de las fechas retrospectivas<br /> 
                    Tiempo entre actividades<br /> 
                    Informacion proceso de selección
                </asp:Label>
                  </td>
            <td>
                <asp:Button ID="btn_actualiza" runat="server" Text="Actualizar" OnClick="btn_actualiza_Click" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" />
            </td>
           
        </tr>        
        </table>
        </asp:Panel>
    <script type="text/javascript" language="javascript">
    function UserDeleteConfirmation() {
        return confirm("Recuerde que si confirma este mensaje se actualizará la información de PACCO y Contratos en el sistema ¿Desea actualizar la información?");
    }
    </script>
</asp:Content>
