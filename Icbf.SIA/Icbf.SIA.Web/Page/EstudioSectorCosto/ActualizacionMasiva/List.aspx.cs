﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_EstudioSectorCosto_ActualizacionMasiva_List : GeneralWeb
{
    ActualizacionMasivaService vActualizacionMasivaService = new ActualizacionMasivaService();
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ActualizacionMasiva";
    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Método para configurar página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvLista.PageSize = PageSize();
            gvLista.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Actualización masiva de información", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void Page_Load(object sender, EventArgs e)
    {
        txtfechainicial.Focus();

    }
    /// <summary>
    /// Evento click del boton buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        DateTime? FI = txtfechainicial.GetDate();
       
        DateTime? FF = txtfechafinal.GetDate();
       
        string Usuario = txtusuario.Text.Trim();
        if (Usuario == "") Usuario = null;
        if (FI != null)
        {
            if (FF == null)
            {
                toolBar.MostrarMensajeError("No se ha ingresado Fecha Final");
                return;
            }
            else
            {
                if (FI < Convert.ToDateTime("01/01/1753") || FF < Convert.ToDateTime("01/01/1753")) {
                    toolBar.MostrarMensajeError("Fecha invalida");
                    return;
                }

                if (FI > Convert.ToDateTime("31/12/9999") || FF > Convert.ToDateTime("31/12/9999"))
                {
                    toolBar.MostrarMensajeError("Fecha invalida");
                    return;
                }

                if (FI> FF)
                {
                    toolBar.MostrarMensajeError("“La fecha desde debe ser menor a la fecha hasta, verifique por favor.");
                    return;
                }
            }
        }
        if (FF != null)
        {
            if (FI == null)
            {
                toolBar.MostrarMensajeError("No se ha ingresado Fecha Inicial");
                return;
            }
        }            
        Buscar();
    }
    public void Buscar()
    {
        DateTime? FI = txtfechainicial.GetDate();
        
        DateTime? FF = txtfechafinal.GetDate();
        string fechaini = null;
        string fechafin = null;
        if (FI !=null) fechaini = FI.Value.ToString("yyyyMMdd");
        if (FF != null) fechafin = FF.Value.ToString("yyyyMMdd");
        string Usuario = txtusuario.Text.Trim();
        if (Usuario == "") Usuario = null;
        List<LogActualizacionMasiva> ListaRegistro = vActualizacionMasivaService.ConsultarRegistroActualizaciones(Usuario, fechaini, fechafin);

        gvLista.DataSource = ListaRegistro;
        gvLista.DataBind();
    }
    /// <summary>
    /// Evento click del boton nuevo
    /// </summary>
    /// <param name="sender">Botón btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
}