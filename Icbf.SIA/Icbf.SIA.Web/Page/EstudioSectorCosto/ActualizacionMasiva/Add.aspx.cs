﻿using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_EstudioSectorCosto_ActualizacionMasiva_Add : GeneralWeb
{
    ActualizacionMasivaService vActualizacionMasivaService = new ActualizacionMasivaService();
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ActualizacionMasiva";
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Método para configurar página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);
                        
            toolBar.EstablecerTitulos("Actualización masiva de información", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    /// <summary>
    /// Métod para guardar el registro de gestión
    /// </summary>
    private void Guardar()
    { }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btn_actualiza_Click(object sender, EventArgs e)
    {        
        string Userp= GetSessionUser().NombreUsuario;
        List<RegistroSolicitudEstudioSectoryCaso> ListaRegistroSolicitudEstudioSectoryCaso = vActualizacionMasivaService.ConsultarRegistroSolicitudEstudioSectoryCasos();

        int band = 0;
        foreach (var item in ListaRegistroSolicitudEstudioSectoryCaso)//.Where(T => T.FechaCrea.Year == DateTime.Now.Year)
        {
            int vig = 0;
            if (item.VigenciaPACCO.Trim() != "")
            {
                vig = Convert.ToInt32(item.VigenciaPACCO);
            }

            WsContratosPacco.WSContratosPACCOSoapClient ws = new WsContratosPacco.WSContratosPACCOSoapClient();
            WsContratosPacco.GetObjetosContractualesServicioXModalidad_Result[] res = ws.GetListaConsecutivosEncabezadosPACCOXModalidad(vig, null, 0, 0, item.IdRegistroSolicitudEstudioSectoryCaso, null, 0, 0, null, 0);

            //actualiza campos
            
            if (res != null && res.ToList().Count > 0)
            {
                string area = res[0].area;
                string codigoarea = res[0].codigoarea != null ? res[0].codigoarea.ToString() : "";
                string estado = res[0].estado != null ? res[0].estado.ToString() : "";
                string FechaInicioProceso = res[0].FechaInicioProceso.Value.ToString("yyyyMMdd");
                string Id_modalidad = res[0].Id_modalidad == null ? "0" : res[0].Id_modalidad.Value.ToString().Trim();
                decimal? Id_tipo_contrato = res[0].Id_tipo_contrato;
                string modalidad = res[0].modalidad.Trim();
                string objeto_contractual = res[0].objeto_contractual;
                string tipo_contrato = res[0].tipo_contrato;
                int vigenciapacco = res[0].anop_vigencia;

                string FechaInicioEjecucion = res[0].FechaInicioEjecucion.Value.ToString("yyyyMMdd");

                decimal valor_contrato = res[0].valor_contrato == null ? 0 : res[0].valor_contrato.Value;


                vActualizacionMasivaService.ActualizarRegistroSolicitudEstudioSectoryCasos(Userp, vigenciapacco,Convert.ToDecimal(item.IdRegistroSolicitudEstudioSectoryCaso), area, codigoarea, estado, FechaInicioProceso, Id_modalidad, Id_tipo_contrato, modalidad, objeto_contractual, tipo_contrato, FechaInicioEjecucion, valor_contrato);

                band = 1;
            }
        }
        //crea log 
        int retorno = 0;
        if (band == 1)
        {
            retorno = vActualizacionMasivaService.CreaLogActualizacion(Userp);
            toolBar.MostrarMensajeError("La información ha sido guardada exitosamente.");
        }
    }
}