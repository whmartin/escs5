﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_EstudioSectorCosto_ActualizacionMasiva_List" %>
<%@ Register TagPrefix="uc1" TagName="Fechas" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="50%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblfechadesde" runat="server">Fecha Desde</asp:Label>
               <%-- &nbsp;*
                <asp:RequiredFieldValidator ID="rfvfecinicial" runat="server" ControlToValidate="txtfechainicial" Display="Dynamic" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>--%>
            </td>
            <td>Fecha Hasta
                  <%--<asp:RequiredFieldValidator runat="server" ID="rfvfechasta"
                  ControlToValidate="txtfechainicial" Display="Dynamic" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>--%>
            </td>  
        </tr>
        <tr class="rowA">
            <td valign="top">
                <%--<asp:textbox runat="server" ID="txtfechainicial" Width="206px"  ></asp:textbox>3--%>
                <uc1:Fechas runat="server" ID="txtfechainicial" />
            </td>
            <td class="top">
                <uc1:Fechas runat="server" ID="txtfechafinal" />
                    <%--<asp:textbox runat="server" ID="txtfechafinal" Width="206px"  ></asp:textbox>--%>             
             </td>
        </tr><tr class="rowB"><td>
            <asp:Label ID="lblusuario" runat="server">Usuario</asp:Label>
            </td>
            <td></td>
            </tr>
        <tr class="rowA">
             <td valign="top">
                <asp:textbox runat="server" ID="txtusuario" Width="206px"  ></asp:textbox>
            </td>
            <td ></td>
                   
        </tr>
    </table>
    </asp:Panel>
    <br />
    <br />
    <asp:Panel runat="server" ID="pnlLista">
        <table width="50%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvLista" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="true"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px"
                        >
                        <Columns>                            
                            <asp:BoundField HeaderText="Fecha de Actualización" DataField="Fecha" DataFormatString="{0:d}"  SortExpression="Fecha" />
                            <asp:BoundField HeaderText="Hora de Actualización" DataField="Hora" SortExpression="Hora" />
                            <asp:BoundField HeaderText="Usuario" DataField="UsuarioLog" SortExpression="UsuarioLog" />   
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
</asp:Content>
