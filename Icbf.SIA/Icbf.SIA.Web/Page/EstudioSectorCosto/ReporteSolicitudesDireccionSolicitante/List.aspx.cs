using Icbf.EstudioSectorCosto.Entity;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Linq;

public partial class Page_Informe_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/ReporteSolicitudesDireccionSolicitante";
    ResultadoEstudioSectorService vResultadoEstudioSectorService = new ResultadoEstudioSectorService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        ddlVigencia.Focus();
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        LimpiarCampos();
    }

    private void LimpiarCampos()
    {
        toolBar = (masterPrincipal)Master;
        toolBar.LipiarMensajeError();
        LimpiarControles(pnlConsulta.Controls);
    }

    protected void btnReporte_Click(object sender, EventArgs e)
    {
        toolBar = (masterPrincipal)Master;

        try
        {
            string titulo = "Reporte de Solicitudes por direcci&oacute;n solicitante";
            string nombreReporte = "ReporteSolicitudesDirecc";
            //Crea un objeto de tipo reporte

            string vigencia = ddlVigencia.SelectedValue;

            var showExportControls = true;

            var objReport = new Report(nombreReporte, showExportControls, PageName, titulo);

            objReport.AddParameter("Vigencia", vigencia);
            objReport.AddParameter("Usuario", GetSessionUser().NombreUsuario);

            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private string QuitarTildes(string texto)
    {
        string resultado = string.Empty;
        foreach (char item in texto.ToLower())
        {
            if (item.Equals('�') || (int)item == 225)
            {
                resultado += 'a';
            }
            else if (item.Equals('�') || (int)item == 233)
            {
                resultado += 'e';
            }
            else if (item.Equals('�') || (int)item == 237)
            {
                resultado += 'i';
            }
            else if (item.Equals('�') || (int)item == 243)
            {
                resultado += 'o';
            }
            else if (item.Equals('�') || (int)item == 250)
            {
                resultado += 'u';
            }
            else
            {
                resultado += item.ToString();
            }
        }

        return resultado.ToUpper();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)Master;
            toolBar.eventoReporte += btnReporte_Click;

            toolBar.EstablecerTitulos(@"Reporte De Solicitudes Por Direcci&oacute;n Solicitante", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Informe.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Informe.Eliminado");

            var lstRegional = vResultadoEstudioSectorService.ConsultarVigencia();

            Utilidades.LlenarDropDownList(ddlVigencia, lstRegional);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}