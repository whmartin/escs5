using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_DiasEstimadosRealesPorModalidad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/DiasEstimadosRealesPorModalidad";
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            ddlIdModalidadSeleccion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();

            vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador = Convert.ToInt32(ddlIdComplejidadIndicador.SelectedValue);
            vDiasEstimadosRealesPorModalidad.Operador = Convert.ToString(ddlOperador.SelectedValue);
            vDiasEstimadosRealesPorModalidad.Limite = txtLimite.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtLimite.Text);
            vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion = txtDiasHabilesEntrePSeInicioEjecucion.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesEntrePSeInicioEjecucion.Text);
            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS = txtDiasHabilesEntreComitePS.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesEntreComitePS.Text);
            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite = txtDiasHabilesEntreRadicacionContratoYComite.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesEntreRadicacionContratoYComite.Text);
            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos = txtDiasHabilesEntreESyRadicacionContratos.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesEntreESyRadicacionContratos.Text);
            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES = txtDiasHabilesEntreFCTDefinitivaYES.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesEntreFCTDefinitivaYES.Text);
            vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva = txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva.Text.Equals(String.Empty) ? 0 : Convert.ToInt32(txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva.Text);
            vDiasEstimadosRealesPorModalidad.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            if (vEstudioSectorCostoService.ValidarDiasEstimadosRealesPorModalidad(vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion
                    , vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador
                    , vDiasEstimadosRealesPorModalidad.Operador
                    , vDiasEstimadosRealesPorModalidad.Limite
                    , vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion
                    , vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS
                    , vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite
                    , vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos
                    , vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES
                    , vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva))
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad = Convert.ToInt32(hfIdDiasEstimadosRealesPorModalidad.Value);
                    vDiasEstimadosRealesPorModalidad.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vDiasEstimadosRealesPorModalidad, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarDiasEstimadosRealesPorModalidad(vDiasEstimadosRealesPorModalidad);
                }
                else
                {
                    vDiasEstimadosRealesPorModalidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vDiasEstimadosRealesPorModalidad, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarDiasEstimadosRealesPorModalidad(vDiasEstimadosRealesPorModalidad);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("DiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad", vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad);
                    SetSessionParameter("DiasEstimadosRealesPorModalidad.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n d&iacute;as estimados y reales por modalidad", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdDiasEstimadosRealesPorModalidad = Convert.ToInt32(GetSessionParameter("DiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad"));
            RemoveSessionParameter("DiasEstimadosRealesPorModalidad.Id");

            DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();
            vDiasEstimadosRealesPorModalidad = vEstudioSectorCostoService.ConsultarDiasEstimadosRealesPorModalidad(vIdDiasEstimadosRealesPorModalidad);
            hfIdDiasEstimadosRealesPorModalidad.Value = vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad.ToString();
            ddlIdModalidadSeleccion.SelectedValue = vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion.ToString();
            ddlIdComplejidadIndicador.SelectedValue = vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador.ToString();
            ddlOperador.SelectedValue = vDiasEstimadosRealesPorModalidad.Operador;
            txtLimite.Text = vDiasEstimadosRealesPorModalidad.Limite.ToString();
            txtDiasHabilesEntrePSeInicioEjecucion.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion.ToString();
            txtDiasHabilesEntreComitePS.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS.ToString();
            txtDiasHabilesEntreRadicacionContratoYComite.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite.ToString();
            txtDiasHabilesEntreESyRadicacionContratos.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos.ToString();
            txtDiasHabilesEntreFCTDefinitivaYES.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES.ToString();
            txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva.ToString();
            rblEstado.SelectedValue = Convert.ToBoolean(vDiasEstimadosRealesPorModalidad.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDiasEstimadosRealesPorModalidad.UsuarioCrea, vDiasEstimadosRealesPorModalidad.FechaCrea, vDiasEstimadosRealesPorModalidad.UsuarioModifica, vDiasEstimadosRealesPorModalidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            //Llenado de ListRadioButton
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
            /*llenado de combos*/
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, false);

            ddlIdComplejidadIndicador.Items.Insert(0, new ListItem("SELECCIONE", "0"));
            ddlIdComplejidadIndicador.SelectedIndex = 0;

            ddlOperador.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
