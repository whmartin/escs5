<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_DiasEstimadosRealesPorModalidad_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdDiasEstimadosRealesPorModalidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Modalidad de contrataci&oacute;n *
            </td>
            <td>Complejidad indicador *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" Enabled="false"></asp:DropDownList>

            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdComplejidadIndicador" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Operador *
            </td>
            <td>L&iacute;mite
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlOperador" Enabled="false">
                    <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                    <asp:ListItem Value="<"><</asp:ListItem>
                    <asp:ListItem Value=">">></asp:ListItem>
                    <asp:ListItem Value="=">=</asp:ListItem>
                    <asp:ListItem Value="<="><=</asp:ListItem>
                    <asp:ListItem Value=">=">>=</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtLimite" Enabled="false" MaxLength="6"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftLimite" runat="server" TargetControlID="txtLimite"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>Dias h&aacute;biles entre PS e Inicio Ejecuci&oacute;n 
            </td>
            <td>D&iacute;as h&aacute;biles entre Comit&eacute; y PS 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesEntrePSeInicioEjecucion" Enabled="false" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntrePSeInicioEjecucion" runat="server" TargetControlID="txtDiasHabilesEntrePSeInicioEjecucion"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesEntreComitePS" Enabled="false" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreComitePS" runat="server" TargetControlID="txtDiasHabilesEntreComitePS"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>D&iacute;as h&aacute;biles entre Radicaci&oacute;n Contratos y Comit&eacute; 
            </td>
            <td>D&iacute;as h&aacute;biles entre ES y Radicaci&oacute;n Contratos 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesEntreRadicacionContratoYComite" Enabled="false" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreRadicacionContratoYComite" runat="server" TargetControlID="txtDiasHabilesEntreRadicacionContratoYComite"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesEntreESyRadicacionContratos" Enabled="false" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreESyRadicacionContratos" runat="server" TargetControlID="txtDiasHabilesEntreESyRadicacionContratos"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>D&iacute;as h&aacute;biles entre DT definitivos y ES
            </td>
            <td>D&iacute;as h&aacute;biles entre Radicaci&oacute;n DT y DT definitivos
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesEntreFCTDefinitivaYES" Enabled="false" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreFCTES" runat="server" TargetControlID="txtDiasHabilesEntreFCTDefinitivaYES"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva" Enabled="false" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreRadicacionFCTyFCTDefinitiva" runat="server" TargetControlID="txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Estado
            </td>
        </tr>
        <tr class="rowA">
            <td align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <br />
         <table width="50%" align="center" border="1" >
             <tr>
                 <td> F</td>
                 <td> Fecha</td>
                 <td> REG</td>
                 <td> Registro</td>
                 <td> Docs</td>
                 <td> Documentos</td>
             </tr>
              <tr>
                 <td> PS</td>
                 <td> Proceso de selecci&oacute;n</td>
                 <td> ES</td>
                 <td> Estudios del sector</td>
                 <td> SDC</td>
                 <td> Solicitud de cotizaci&oacute;n</td>
             </tr>
             <tr>
                 <td> DT</td>
                 <td> Documentos t&eacute;cnicos</td>
                 <td> EC</td>
                 <td> Estudio de costos</td>
                 <td> MC</td>
                 <td> M&iacute;nima cuant&iacute;a</td>
             </tr>
         </table>
</asp:Content>
