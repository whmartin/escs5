using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Linq.Expressions;

public partial class Page_DiasEstimadosRealesPorModalidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/DiasEstimadosRealesPorModalidad";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;
    int vColumnaOperador = 11;
    Boolean SwLimiteOperador = true;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            ddlIdModalidadSeleccion.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtLimite.Text == "" && ddlOperador.SelectedValue == "-1")
        {
            args.IsValid = true;
        }
        else if (txtLimite.Text == "")
        {
            gvDiasEstimadosRealesPorModalidad.Visible = false;
            args.IsValid = false;
            //Validaci�n para la b�squea
            SwLimiteOperador = false;

        }
    }
    protected void CustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtLimite.Text == "" && ddlOperador.SelectedValue == "-1")
        {
            args.IsValid = true;
        }
        else if (ddlOperador.SelectedValue == "-1")
        {
            gvDiasEstimadosRealesPorModalidad.Visible = false;
            args.IsValid = false;
            //Validaci�n para la b�squea
            SwLimiteOperador = false;

        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (SwLimiteOperador)
        {
            LlenarGrilla();
            MostrarColumnasExportacionExcel(false);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<DiasEstimadosRealesPorModalidad> Buscar()
    {
        try
        {
            int? vIdModalidadSeleccion = null;
            int? vIdComplejidadIndicador = null;
            String vOperador = null;
            int? vLimite = null;
            int? vDiasHabilesEntrePSeInicioEjecucion = null;
            int? vDiasHabilesEntreComitePS = null;
            int? vDiasHabilesEntreRadicacionContratoYComite = null;
            int? vDiasHabilesEntreESyRadicacionContratos = null;
            int? vDiasHabilesEntreFCTDefinitivaYES = null;
            int? vDiasHabilesEntreRadicacionFCTyFCTDefinitiva = null;
            int? vEstado = null;
            if (ddlIdModalidadSeleccion.SelectedValue != "-1")
            {
                vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            }
            if (ddlIdComplejidadIndicador.SelectedValue != "-1")
            {
                vIdComplejidadIndicador = Convert.ToInt32(ddlIdComplejidadIndicador.SelectedValue);
            }
            if (ddlOperador.SelectedValue != "-1")
            {
                vOperador = Convert.ToString(ddlOperador.SelectedValue);
            }
            if (txtLimite.Text != "")
            {
                vLimite = Convert.ToInt32(txtLimite.Text);
            }
            if (txtDiasHabilesEntrePSeInicioEjecucion.Text != "")
            {
                vDiasHabilesEntrePSeInicioEjecucion = Convert.ToInt32(txtDiasHabilesEntrePSeInicioEjecucion.Text);
            }
            if (txtDiasHabilesEntreComitePS.Text != "")
            {
                vDiasHabilesEntreComitePS = Convert.ToInt32(txtDiasHabilesEntreComitePS.Text);
            }
            if (txtDiasHabilesEntreRadicacionContratoYComite.Text != "")
            {
                vDiasHabilesEntreRadicacionContratoYComite = Convert.ToInt32(txtDiasHabilesEntreRadicacionContratoYComite.Text);
            }
            if (txtDiasHabilesEntreESyRadicacionContratos.Text != "")
            {
                vDiasHabilesEntreESyRadicacionContratos = Convert.ToInt32(txtDiasHabilesEntreESyRadicacionContratos.Text);
            }
            if (txtDiasHabilesEntreFCTDefinitivaYES.Text != "")
            {
                vDiasHabilesEntreFCTDefinitivaYES = Convert.ToInt32(txtDiasHabilesEntreFCTDefinitivaYES.Text);
            }
            if (txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva.Text != "")
            {
                vDiasHabilesEntreRadicacionFCTyFCTDefinitiva = Convert.ToInt32(txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }
            List<DiasEstimadosRealesPorModalidad> vListaDiasEstimadosRealesPorModalidad = vEstudioSectorCostoService.ConsultarDiasEstimadosRealesPorModalidads(vIdModalidadSeleccion, vIdComplejidadIndicador, vOperador, vLimite, vDiasHabilesEntrePSeInicioEjecucion, vDiasHabilesEntreComitePS, vDiasHabilesEntreRadicacionContratoYComite, vDiasHabilesEntreESyRadicacionContratos, vDiasHabilesEntreFCTDefinitivaYES, vDiasHabilesEntreRadicacionFCTyFCTDefinitiva, vEstado);
            return vListaDiasEstimadosRealesPorModalidad;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvDiasEstimadosRealesPorModalidad.PageSize = PageSize();
            gvDiasEstimadosRealesPorModalidad.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n d&iacute;as estimados y reales por modalidad", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDiasEstimadosRealesPorModalidad.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDiasEstimadosRealesPorModalidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDiasEstimadosRealesPorModalidad.SelectedRow);
    }

    protected void gvDiasEstimadosRealesPorModalidad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDiasEstimadosRealesPorModalidad.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("DiasEstimadosRealesPorModalidad.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("DiasEstimadosRealesPorModalidad.Eliminado");
            //Llenado de ListRadioButton
            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";
            /*llenado de combos*/
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvDiasEstimadosRealesPorModalidad.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvDiasEstimadosRealesPorModalidad.AllowPaging;
        Boolean vPaginadorError = gvDiasEstimadosRealesPorModalidad.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvDiasEstimadosRealesPorModalidad.AllowPaging = false;

        List<DiasEstimadosRealesPorModalidad> vListaDiasEstimadosRealesPorModalidad = LlenarGrilla();
        MostrarColumnasExportacionExcel(true);

        datos = ManejoControlesContratos.GenerarDataTable(this.gvDiasEstimadosRealesPorModalidad, vListaDiasEstimadosRealesPorModalidad);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvDiasEstimadosRealesPorModalidad.Columns.Count; i++)
                {
                    dt.Columns.Add(gvDiasEstimadosRealesPorModalidad.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvDiasEstimadosRealesPorModalidad.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvDiasEstimadosRealesPorModalidad.Columns.Count; j++)
                    {
                        dr[gvDiasEstimadosRealesPorModalidad.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaDiasEstimadosRealesModalidad");
                gvDiasEstimadosRealesPorModalidad.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }

    protected List<DiasEstimadosRealesPorModalidad> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<DiasEstimadosRealesPorModalidad> listaDiasEstimadosRealesPorModalidad = Buscar();
        gvDiasEstimadosRealesPorModalidad.Visible = true;
        gvDiasEstimadosRealesPorModalidad.DataSource = listaDiasEstimadosRealesPorModalidad;
        gvDiasEstimadosRealesPorModalidad.DataBind();

        toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

        return listaDiasEstimadosRealesPorModalidad;

    }
    protected void MostrarColumnasExportacionExcel(Boolean pMostrarColumnas)
    {
        this.gvDiasEstimadosRealesPorModalidad.Columns[vColumnaOperador].Visible = pMostrarColumnas;
    }

    protected void gvDiasEstimadosRealesPorModalidad_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }
    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el c�digo de llenado de datos para la grilla 
            List<DiasEstimadosRealesPorModalidad> listaDiasEstimadosRealesPorModalidads = Buscar();

            //Fin del c�digo de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaDiasEstimadosRealesPorModalidads != null)
                {
                    var param = Expression.Parameter(typeof(DiasEstimadosRealesPorModalidad), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<DiasEstimadosRealesPorModalidad, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaDiasEstimadosRealesPorModalidads.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaDiasEstimadosRealesPorModalidads.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaDiasEstimadosRealesPorModalidads.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaDiasEstimadosRealesPorModalidads.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaDiasEstimadosRealesPorModalidads;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
