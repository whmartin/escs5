using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_DiasEstimadosRealesPorModalidad_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/DiasEstimadosRealesPorModalidad";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    ManejoControlesContratos vManejoControlesContratos;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad", hfIdDiasEstimadosRealesPorModalidad.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdDiasEstimadosRealesPorModalidad = Convert.ToInt32(GetSessionParameter("DiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad"));
            RemoveSessionParameter("DiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad");

            if (GetSessionParameter("DiasEstimadosRealesPorModalidad.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("DiasEstimadosRealesPorModalidad.Guardado");


            DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();
            vDiasEstimadosRealesPorModalidad = vEstudioSectorCostoService.ConsultarDiasEstimadosRealesPorModalidad(vIdDiasEstimadosRealesPorModalidad);
            hfIdDiasEstimadosRealesPorModalidad.Value = vDiasEstimadosRealesPorModalidad.IdDiasEstimadosRealesPorModalidad.ToString();
            ddlIdModalidadSeleccion.SelectedValue = vDiasEstimadosRealesPorModalidad.IdModalidadSeleccion.ToString();
            ddlIdComplejidadIndicador.SelectedValue = vDiasEstimadosRealesPorModalidad.IdComplejidadIndicador.ToString();
            ddlOperador.SelectedValue = vDiasEstimadosRealesPorModalidad.Operador;
            txtLimite.Text = vDiasEstimadosRealesPorModalidad.Limite == null ? string.Empty : vDiasEstimadosRealesPorModalidad.Limite.ToString();
            txtDiasHabilesEntrePSeInicioEjecucion.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntrePSeInicioEjecucion.ToString();
            txtDiasHabilesEntreComitePS.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreComitePS.ToString();
            txtDiasHabilesEntreRadicacionContratoYComite.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionContratoYComite.ToString();
            txtDiasHabilesEntreESyRadicacionContratos.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreESyRadicacionContratos.ToString();
            txtDiasHabilesEntreFCTDefinitivaYES.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreFCTDefinitivaYES.ToString();
            txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva.Text = vDiasEstimadosRealesPorModalidad.DiasHabilesEntreRadicacionFCTyFCTDefinitiva.ToString();
            rblEstado.SelectedValue = Convert.ToBoolean(vDiasEstimadosRealesPorModalidad.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdDiasEstimadosRealesPorModalidad.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDiasEstimadosRealesPorModalidad.UsuarioCrea, vDiasEstimadosRealesPorModalidad.FechaCrea, vDiasEstimadosRealesPorModalidad.UsuarioModifica, vDiasEstimadosRealesPorModalidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdDiasEstimadosRealesPorModalidad = Convert.ToInt32(hfIdDiasEstimadosRealesPorModalidad.Value);

            DiasEstimadosRealesPorModalidad vDiasEstimadosRealesPorModalidad = new DiasEstimadosRealesPorModalidad();
            vDiasEstimadosRealesPorModalidad = vEstudioSectorCostoService.ConsultarDiasEstimadosRealesPorModalidad(vIdDiasEstimadosRealesPorModalidad);
            InformacionAudioria(vDiasEstimadosRealesPorModalidad, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarDiasEstimadosRealesPorModalidad(vDiasEstimadosRealesPorModalidad);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("DiasEstimadosRealesPorModalidad.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n d&iacute;as estimados y reales por modalidad", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            //Llenado de ListRadioButton
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            /*llenado de combos*/
            vManejoControlesContratos = new ManejoControlesContratos();
            vManejoControlesContratos.LlenarModalidadSeleccionESCs(ddlIdModalidadSeleccion, null, true);
            vManejoControlesContratos.LlenarComplejidadIndicador(ddlIdComplejidadIndicador, null, false);

            ddlIdComplejidadIndicador.Items.Insert(0, new ListItem("No especificado", "0"));
            ddlIdComplejidadIndicador.SelectedIndex = 0;


            ddlOperador.Items.Insert(0, new ListItem("No especificado", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
