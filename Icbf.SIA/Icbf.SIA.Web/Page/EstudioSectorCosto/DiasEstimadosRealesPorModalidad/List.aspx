<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_DiasEstimadosRealesPorModalidad_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Modalidad de contrataci&oacute;n 
                </td>
                <td>Complejidad indicador
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion"></asp:DropDownList>

                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdComplejidadIndicador"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>Operador
                 <asp:CustomValidator runat="server" ID="CustomValidator" ControlToValidate="txtLimite" SetFocusOnError="true"
                     ErrorMessage="Debe asociar un Operador al L&iacute;mite para generar la consulta" Display="Dynamic" ValidationGroup="btnBuscar" ForeColor="Red" OnServerValidate="CustomValidator_ServerValidate" />
                </td>
                <td>L&iacute;mite
                <asp:CustomValidator runat="server" ID="CustomValidator1" ControlToValidate="ddlOperador" SetFocusOnError="true"
                    ErrorMessage="Debe asociar un L&iacute;mite al Operador para generar la consulta" Display="Dynamic" ValidationGroup="btnBuscar" ForeColor="Red" OnServerValidate="CustomValidator1_ServerValidate" />

                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlOperador">
                        <asp:ListItem Value="-1">SELECCIONE</asp:ListItem>
                        <asp:ListItem Value="<"><</asp:ListItem>
                        <asp:ListItem Value=">">></asp:ListItem>
                        <asp:ListItem Value="=">=</asp:ListItem>
                        <asp:ListItem Value="<="><=</asp:ListItem>
                        <asp:ListItem Value=">=">>=</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtLimite" MaxLength="6"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftLimite" runat="server" TargetControlID="txtLimite"
                        FilterType="Numbers" ValidChars="" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Dias h&aacute;biles entre PS e Inicio Ejecuci&oacute;n 
                </td>
                <td>D&iacute;as h&aacute;biles entre Comit&eacute; y PS 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesEntrePSeInicioEjecucion" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntrePSeInicioEjecucion" runat="server" TargetControlID="txtDiasHabilesEntrePSeInicioEjecucion"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesEntreComitePS" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreComitePS" runat="server" TargetControlID="txtDiasHabilesEntreComitePS"
                        FilterType="Numbers" ValidChars="" />
                </td>
            </tr>
            <tr class="rowB">
                <td>D&iacute;as h&aacute;biles entre Radicaci&oacute;n Contratos y Comit&eacute; 
                </td>
                <td>D&iacute;as h&aacute;biles entre ES y Radicaci&oacute;n Contratos 
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesEntreRadicacionContratoYComite" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreRadicacionContratoYComite" runat="server" TargetControlID="txtDiasHabilesEntreRadicacionContratoYComite"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesEntreESyRadicacionContratos" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreESyRadicacionContratos" runat="server" TargetControlID="txtDiasHabilesEntreESyRadicacionContratos"
                        FilterType="Numbers" ValidChars="" />
                </td>
            </tr>
            <tr class="rowB">
                <td>D&iacute;as h&aacute;biles entre DT definitivos y ES
                </td>
                <td>D&iacute;as h&aacute;biles entre Radicaci&oacute;n DT y DT definitivos
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesEntreFCTDefinitivaYES" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreFCTES" runat="server" TargetControlID="txtDiasHabilesEntreFCTDefinitivaYES"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDiasHabilesEntreRadicacionFCTyFCTDefinitiva" runat="server" TargetControlID="txtDiasHabilesEntreRadicacionFCTyFCTDefinitiva"
                        FilterType="Numbers" ValidChars="" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">Estado
                </td>
            </tr>
            <tr class="rowA">
                <td align="left">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDiasEstimadosRealesPorModalidad" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDiasEstimadosRealesPorModalidad" CellPadding="0" Height="16px" OnSorting="gvDiasEstimadosRealesPorModalidad_Sorting"
                        OnPageIndexChanging="gvDiasEstimadosRealesPorModalidad_PageIndexChanging" OnSelectedIndexChanged="gvDiasEstimadosRealesPorModalidad_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Modalidad de contrataci&oacute;n" DataField="Modalidad" SortExpression="Modalidad" />
                            <asp:BoundField HeaderText="Operador" DataField="Operador" Visible="false" />
                            <asp:BoundField HeaderText="L&iacute;mite" DataField="LimiteGrilla" SortExpression="LimiteGrilla" />
                            <asp:BoundField HeaderText="Complejidad indicador" DataField="Complejidad" SortExpression="Complejidad" />
                            <asp:BoundField HeaderText="Dias h&aacute;biles entre PS e Inicio Ejecuci&oacute;n" DataField="DiasHabilesEntrePSeInicioEjecucion" SortExpression="DiasHabilesEntrePSeInicioEjecucion" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles entre Comit&eacute; y PS" DataField="DiasHabilesEntreComitePS" SortExpression="DiasHabilesEntreComitePS" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles entre Radicaci&oacute;n Contratos y Comit&eacute; " DataField="DiasHabilesEntreRadicacionContratoYComite" SortExpression="DiasHabilesEntreRadicacionContratoYComite" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles entre ES y Radicaci&oacute;n Contratos" DataField="DiasHabilesEntreESyRadicacionContratos" SortExpression="DiasHabilesEntreESyRadicacionContratos" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles entre DT definitivos y ES" DataField="DiasHabilesEntreFCTDefinitivaYES" SortExpression="DiasHabilesEntreFCTDefinitivaYES" />
                            <asp:BoundField HeaderText="D&iacute;as h&aacute;biles entre Radicaci&oacute;n DT y DT definitivos" DataField="DiasHabilesEntreRadicacionFCTyFCTDefinitiva" SortExpression="DiasHabilesEntreRadicacionFCTyFCTDefinitiva" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstado" SortExpression="NombreEstado" />

                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
         <table width="50%" align="center" border="1" >
             <tr>
                 <td> F</td>
                 <td> Fecha</td>
                 <td> REG</td>
                 <td> Registro</td>
                 <td> Docs</td>
                 <td> Documentos</td>
             </tr>
              <tr>
                 <td> PS</td>
                 <td> Proceso de selecci&oacute;n</td>
                 <td> ES</td>
                 <td> Estudios del sector</td>
                 <td> SDC</td>
                 <td> Solicitud de cotizaci&oacute;n</td>
             </tr>
             <tr>
                 <td> DT</td>
                 <td> Documentos t&eacute;cnicos</td>
                 <td> EC</td>
                 <td> Estudio de costos</td>
                 <td> MC</td>
                 <td> M&iacute;nima cuant&iacute;a</td>
             </tr>
         </table>
    </asp:Panel>
</asp:Content>
