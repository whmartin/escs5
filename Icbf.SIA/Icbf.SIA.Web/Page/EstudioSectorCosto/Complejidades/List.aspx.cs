﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;


public partial class Page_Complejidades_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/Complejidades";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtNombre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
        if (gvComplejidades.Rows.Count == 0)
        {
            gvComplejidades.DataSource = null;
            gvComplejidades.DataBind();
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<Complejidades> Buscar()
    {
        try
        {
            String vNombre = null;
            int? vEstado = null;
            int? vTipoComplejidad = null;
            if (txtNombre.Text != "")
            {
                vNombre = Convert.ToString(txtNombre.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }

            if (rblTipoComplejidad.SelectedValue != "-1")
            {
                vTipoComplejidad = Convert.ToInt32(Convert.ToBoolean(rblTipoComplejidad.SelectedValue));
            }
           
            List<Complejidades> ListaComplejidades = vEstudioSectorCostoService.ConsultarComplejidadess(vNombre, vTipoComplejidad, vEstado);
            return ListaComplejidades;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvComplejidades.PageSize = PageSize();
            gvComplejidades.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Complejidades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvComplejidades.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Complejidades.IdComplejidad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvComplejidades_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvComplejidades.SelectedRow);
    }
    protected void gvComplejidades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvComplejidades.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Complejidades.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Complejidades.Eliminado");

            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";

            rblTipoComplejidad.Items.Insert(0, new ListItem("Todos", "-1"));
            rblTipoComplejidad.Items.Insert(1, new ListItem("Interna", "True"));
            rblTipoComplejidad.Items.Insert(2, new ListItem("Indicador", "False"));
            rblTipoComplejidad.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvComplejidades.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            gvComplejidades.DataSource = null;
            gvComplejidades.DataBind();
        }
    }

    private void Exportar()
    {

        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvComplejidades.AllowPaging;
        Boolean vPaginadorError = gvComplejidades.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvComplejidades.AllowPaging = false;

        List<Complejidades> vListaComplejidadess = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvComplejidades, vListaComplejidadess);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvComplejidades.Columns.Count; i++)
                {
                    dt.Columns.Add(gvComplejidades.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvComplejidades.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvComplejidades.Columns.Count; j++)
                    {
                        dr[gvComplejidades.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }
                                
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaComplejidades_"+DateTime.Now.ToString("ddMMyyyy"));
                gvComplejidades.AllowPaging = vPaginador;
            }
            else
            {
                gvComplejidades.DataSource = null;
                gvComplejidades.DataBind();
            }
                
        }
        else
        {
            gvComplejidades.DataSource = null;
            gvComplejidades.DataBind();
        }
            
    }


    protected List<Complejidades> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<Complejidades> listaComplejidadess = Buscar();
        gvComplejidades.DataSource = listaComplejidadess;
        gvComplejidades.DataBind();

        return listaComplejidadess;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el código de llenado de datos para la grilla 
            List<Complejidades> listaComplejidadess = Buscar();

            //Fin del código de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaComplejidadess != null)
                {
                    var param = Expression.Parameter(typeof(Complejidades), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Complejidades, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaComplejidadess.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaComplejidadess.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaComplejidadess.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaComplejidadess.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaComplejidadess;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvComplejidades_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
}

