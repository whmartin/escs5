using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

public partial class Page_Complejidades_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/Complejidades";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtNombre.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            Complejidades vComplejidades = new Complejidades();

            vComplejidades.Nombre = Convert.ToString(txtNombre.Text).ToUpper();
            vComplejidades.TipoComplejidad = Convert.ToInt32(Convert.ToBoolean(rblTipoComplejidad.SelectedValue));
            vComplejidades.Descripcion = txtDescripcion.Text.ToUpper();
            vComplejidades.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vComplejidades.IdComplejidad = Convert.ToInt32(hfIdComplejidad.Value);
                    vComplejidades.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vComplejidades, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarComplejidades(vComplejidades);
                }
                else
                {
                    vComplejidades.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vComplejidades, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarComplejidades(vComplejidades);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("Complejidades.IdComplejidad", vComplejidades.IdComplejidad);
                    SetSessionParameter("Complejidades.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);          
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Complejidades", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdComplejidad = Convert.ToInt32(GetSessionParameter("Complejidades.IdComplejidad"));
            //Validación de duplicidad al cargar el registro, se guarda estado Inicial
            String NombreComplejidad = vEstudioSectorCostoService.ConsultarComplejidades(vIdComplejidad).Nombre;
            hfNombreComplejidad.Value = NombreComplejidad;
            RemoveSessionParameter("Complejidades.Id");

            Complejidades vComplejidades = new Complejidades();
            vComplejidades = vEstudioSectorCostoService.ConsultarComplejidades(vIdComplejidad);
            hfIdComplejidad.Value = vComplejidades.IdComplejidad.ToString();
            txtNombre.Text = vComplejidades.Nombre;
            rblTipoComplejidad.SelectedValue = Convert.ToBoolean(vComplejidades.TipoComplejidad).ToString();
            hfIdTipoComplejidad.Value = vComplejidades.TipoComplejidad.ToString();
            txtDescripcion.Text = vComplejidades.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vComplejidades.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vComplejidades.UsuarioCrea, vComplejidades.FechaCrea, vComplejidades.UsuarioModifica, vComplejidades.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;

            rblTipoComplejidad.Items.Insert(0, new ListItem("Interna", "True"));
            rblTipoComplejidad.Items.Insert(1, new ListItem("Indicador", "False"));
            rblTipoComplejidad.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtNombre.Text.ToUpper().Equals(hfNombreComplejidad.Value))
        {
            if (vEstudioSectorCostoService.ValidarComplejidades(txtNombre.Text.ToUpper(), Convert.ToInt16(Convert.ToBoolean(rblTipoComplejidad.SelectedValue))))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        else if (!hfIdTipoComplejidad.Value.Equals(string.Empty))
        {
            if (!rblTipoComplejidad.SelectedValue.Equals(Convert.ToBoolean(Convert.ToInt16(hfIdTipoComplejidad.Value)).ToString()))
            {
                if (vEstudioSectorCostoService.ValidarComplejidades(txtNombre.Text.ToUpper(), Convert.ToInt16(Convert.ToBoolean(rblTipoComplejidad.SelectedValue))))
                {
                    return HfValidacion.Value = "0";
                }
                else
                {
                    return HfValidacion.Value = "1";
                }
            }
        }
        return HfValidacion.Value;
    }
    protected void txtNombre_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
    protected void rblTipoComplejidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
}
