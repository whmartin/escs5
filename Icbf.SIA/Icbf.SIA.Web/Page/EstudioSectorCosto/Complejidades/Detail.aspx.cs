using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_Complejidades_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/Complejidades";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Complejidades.IdComplejidad", hfIdComplejidad.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdComplejidad = Convert.ToInt32(GetSessionParameter("Complejidades.IdComplejidad"));
            RemoveSessionParameter("Complejidades.IdComplejidad");

            if (GetSessionParameter("Complejidades.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Complejidades.Guardado");


            Complejidades vComplejidades = new Complejidades();
            vComplejidades = vEstudioSectorCostoService.ConsultarComplejidades(vIdComplejidad);
            hfIdComplejidad.Value = vComplejidades.IdComplejidad.ToString();
            txtNombre.Text = vComplejidades.Nombre;
            rblTipoComplejidad.SelectedValue = Convert.ToBoolean(vComplejidades.TipoComplejidad).ToString();
            txtDescripcion.Text = vComplejidades.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vComplejidades.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdComplejidad.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vComplejidades.UsuarioCrea, vComplejidades.FechaCrea, vComplejidades.UsuarioModifica, vComplejidades.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdComplejidad = Convert.ToInt32(hfIdComplejidad.Value);

            Complejidades vComplejidades = new Complejidades();
            vComplejidades = vEstudioSectorCostoService.ConsultarComplejidades(vIdComplejidad);
            InformacionAudioria(vComplejidades, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarComplejidades(vComplejidades);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Complejidades.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Complejidades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblTipoComplejidad.Items.Insert(0, new ListItem("Interna", "True"));
            rblTipoComplejidad.Items.Insert(1, new ListItem("Indicador", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
