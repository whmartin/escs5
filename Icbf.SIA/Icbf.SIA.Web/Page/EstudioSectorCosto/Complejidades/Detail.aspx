<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Complejidades_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdComplejidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Complejidad *
            </td>
            <td>Tipo de Complejidad *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre" Enabled="false" Width="98%"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblTipoComplejidad" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Descripci&oacute;n 
            </td>
            <td>Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
