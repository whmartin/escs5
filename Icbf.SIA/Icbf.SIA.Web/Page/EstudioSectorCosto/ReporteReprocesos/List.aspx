<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Informe_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label ID="lblVigencia" runat="server">Vigencia</asp:Label>
                *
                <asp:RequiredFieldValidator ID="rfvVigencia" runat="server" ControlToValidate="ddlVigencia" Display="Dynamic" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlVigencia" Width="406px"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
