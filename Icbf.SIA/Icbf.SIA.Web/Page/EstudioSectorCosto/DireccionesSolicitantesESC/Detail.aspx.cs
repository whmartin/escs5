using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_DireccionesSolicitantesESC_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/DireccionesSolicitantesESC";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DireccionesSolicitantesESC.IdDireccionesSolicitantes", hfIdDireccionesSolicitantes.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdDireccionesSolicitantes = Convert.ToInt32(GetSessionParameter("DireccionesSolicitantesESC.IdDireccionesSolicitantes"));
            RemoveSessionParameter("DireccionesSolicitantesESC.IdDireccionesSolicitantes");

            if (GetSessionParameter("DireccionesSolicitantesESC.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("DireccionesSolicitantesESC.Guardado");


            DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();
            vDireccionesSolicitantesESC = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESC(vIdDireccionesSolicitantes);
            hfIdDireccionesSolicitantes.Value = vDireccionesSolicitantesESC.IdDireccionesSolicitantes.ToString();
            txtDireccionSolicitante.Text = vDireccionesSolicitantesESC.DireccionSolicitante;
            txtDescripcion.Text = vDireccionesSolicitantesESC.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vDireccionesSolicitantesESC.Estado).ToString();
            ObtenerAuditoria(PageName, hfIdDireccionesSolicitantes.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDireccionesSolicitantesESC.UsuarioCrea, vDireccionesSolicitantesESC.FechaCrea, vDireccionesSolicitantesESC.UsuarioModifica, vDireccionesSolicitantesESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdDireccionesSolicitantes = Convert.ToInt32(hfIdDireccionesSolicitantes.Value);

            DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();
            vDireccionesSolicitantesESC = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESC(vIdDireccionesSolicitantes);
            InformacionAudioria(vDireccionesSolicitantesESC, this.PageName, SolutionPage.Detail);
            int vResultado = vEstudioSectorCostoService.EliminarDireccionesSolicitantesESC(vDireccionesSolicitantesESC);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("DireccionesSolicitantesESC.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Dependencias Solicitantes", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
