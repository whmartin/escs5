using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_DireccionesSolicitantesESC_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/DireccionesSolicitantesESC";
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            txtDireccionSolicitante.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<DireccionesSolicitantesESC> Buscar()
    {
        try
        {
            String vDireccionSolicitante = null;
            String vDescripcion = null;
            int? vEstado = null;
            if (txtDireccionSolicitante.Text != "")
            {
                vDireccionSolicitante = Convert.ToString(txtDireccionSolicitante.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            }
            List<DireccionesSolicitantesESC> ListaDireccionesSolicitantesESC = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESCs(vDireccionSolicitante, vDescripcion, vEstado);
            return ListaDireccionesSolicitantesESC;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvDireccionesSolicitantesESC.PageSize = PageSize();
            gvDireccionesSolicitantesESC.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Dependencias Solicitantes", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDireccionesSolicitantesESC.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DireccionesSolicitantesESC.IdDireccionesSolicitantes", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDireccionesSolicitantesESC_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDireccionesSolicitantesESC.SelectedRow);
    }
    protected void gvDireccionesSolicitantesESC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDireccionesSolicitantesESC.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("DireccionesSolicitantesESC.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("DireccionesSolicitantesESC.Eliminado");

            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvDireccionesSolicitantesESC.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvDireccionesSolicitantesESC.AllowPaging;
        Boolean vPaginadorError = gvDireccionesSolicitantesESC.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvDireccionesSolicitantesESC.AllowPaging = false;

        List<DireccionesSolicitantesESC> vListaDireccionesSolicitantesESCs = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvDireccionesSolicitantesESC, vListaDireccionesSolicitantesESCs);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvDireccionesSolicitantesESC.Columns.Count; i++)
                {
                    dt.Columns.Add(gvDireccionesSolicitantesESC.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvDireccionesSolicitantesESC.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvDireccionesSolicitantesESC.Columns.Count; j++)
                    {
                        dr[gvDireccionesSolicitantesESC.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaDireccionesSolicitantes");
                gvDireccionesSolicitantesESC.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }


    protected List<DireccionesSolicitantesESC> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<DireccionesSolicitantesESC> listaDireccionesSolicitantesESCs = Buscar();
        gvDireccionesSolicitantesESC.DataSource = listaDireccionesSolicitantesESCs;
        gvDireccionesSolicitantesESC.DataBind();

        return listaDireccionesSolicitantesESCs;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el c�digo de llenado de datos para la grilla 
            List<DireccionesSolicitantesESC> listaDireccionesSolicitantesESCs = Buscar();

            //Fin del c�digo de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaDireccionesSolicitantesESCs != null)
                {
                    var param = Expression.Parameter(typeof(DireccionesSolicitantesESC), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<DireccionesSolicitantesESC, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaDireccionesSolicitantesESCs.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaDireccionesSolicitantesESCs.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaDireccionesSolicitantesESCs.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaDireccionesSolicitantesESCs.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaDireccionesSolicitantesESCs;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvDireccionesSolicitantesESC_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
}
