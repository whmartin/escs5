using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;


public partial class Page_DireccionesSolicitantesESC_Add : GeneralWeb
{
    masterPrincipal toolBar;
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();
    string PageName = "EstudioSectorCosto/DireccionesSolicitantesESC";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            txtDireccionSolicitante.Focus();
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                HfValidacion.Value = validarDuplicidad();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();

            vDireccionesSolicitantesESC.DireccionSolicitante = Convert.ToString(txtDireccionSolicitante.Text).ToUpper();
            vDireccionesSolicitantesESC.Descripcion = Convert.ToString(txtDescripcion.Text).ToUpper();
            vDireccionesSolicitantesESC.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));
            if (HfValidacion.Value == "0")
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vDireccionesSolicitantesESC.IdDireccionesSolicitantes = Convert.ToInt32(hfIdDireccionesSolicitantes.Value);
                    vDireccionesSolicitantesESC.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vDireccionesSolicitantesESC, this.PageName, SolutionPage.Edit);
                    vResultado = vEstudioSectorCostoService.ModificarDireccionesSolicitantesESC(vDireccionesSolicitantesESC);
                }
                else
                {
                    vDireccionesSolicitantesESC.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vDireccionesSolicitantesESC, this.PageName, SolutionPage.Add);
                    vResultado = vEstudioSectorCostoService.InsertarDireccionesSolicitantesESC(vDireccionesSolicitantesESC);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("DireccionesSolicitantesESC.IdDireccionesSolicitantes", vDireccionesSolicitantesESC.IdDireccionesSolicitantes);
                    SetSessionParameter("DireccionesSolicitantesESC.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El registro ya existe");
                return;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrizaci&oacute;n de Dependencias Solicitantes", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdDireccionesSolicitantes = Convert.ToInt32(GetSessionParameter("DireccionesSolicitantesESC.IdDireccionesSolicitantes"));
            String NombreDireccionesSolicitantes = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESC(vIdDireccionesSolicitantes).DireccionSolicitante;
            hfNombreDireccionesSolicitantes.Value = NombreDireccionesSolicitantes;
            RemoveSessionParameter("DireccionesSolicitantesESC.Id");

            DireccionesSolicitantesESC vDireccionesSolicitantesESC = new DireccionesSolicitantesESC();
            vDireccionesSolicitantesESC = vEstudioSectorCostoService.ConsultarDireccionesSolicitantesESC(vIdDireccionesSolicitantes);
            hfIdDireccionesSolicitantes.Value = vDireccionesSolicitantesESC.IdDireccionesSolicitantes.ToString();
            txtDireccionSolicitante.Text = vDireccionesSolicitantesESC.DireccionSolicitante;
            txtDescripcion.Text = vDireccionesSolicitantesESC.Descripcion;
            rblEstado.SelectedValue = Convert.ToBoolean(vDireccionesSolicitantesESC.Estado).ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vDireccionesSolicitantesESC.UsuarioCrea, vDireccionesSolicitantesESC.FechaCrea, vDireccionesSolicitantesESC.UsuarioModifica, vDireccionesSolicitantesESC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected String validarDuplicidad()
    {
        HfValidacion.Value = "0";
        if (!txtDireccionSolicitante.Text.ToUpper().Equals(hfNombreDireccionesSolicitantes.Value))
        {
            if (vEstudioSectorCostoService.ValidarDireccionesSolicitantesESC(txtDireccionSolicitante.Text.ToUpper()))
            {
                return HfValidacion.Value = "0";
            }
            else
            {
                return HfValidacion.Value = "1";
            }
        }
        return HfValidacion.Value;
    }
    protected void txtDireccionSolicitante_TextChanged(object sender, EventArgs e)
    {
        HfValidacion.Value = validarDuplicidad();
    }
}
