<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_DireccionesSolicitantesESC_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdDireccionesSolicitantes" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Dependencia solicitante *
            </td>
            <td>Descripci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDireccionSolicitante" Width="98%" MaxLength="100" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Width="98%" MaxLength="255" Height="80px" TextMode="MultiLine" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
