<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_BitacoraAcciones_Detail" %>

<%@ Register TagPrefix="uc2" TagName="Festivos" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>
<%@ Register TagPrefix="ucPopUp" TagName="PopUpConsulta" Src="~/General/General/EstudioSectores/EstudioConsultaPopUp.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <link rel="Stylesheet" href="../../../Styles/estudioSectoresPopUp.css" />
    <asp:HiddenField ID="hfIdConsecutivoBitacoraAccion" runat="server" />

    <asp:Panel ID="pnlDetalles" runat="server">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Consecutivo solicitud *                
                </td>
                <td>Usuario *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:HiddenField ID="hdnIdBitacoraEstudio" runat="server" />
                    <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" Width="50%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="rfvConsecutivoEstudio" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtConsecutivoEstudio" ValidationGroup="btnGuardar" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtUsuario"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtUsuario" ValidationGroup="btnGuardar" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha registro *
                </td>
                <td>Fecha modificación                
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc2:Festivos ID="txtFechaRegistro" runat="server" />
                </td>
                <td>
                    <uc2:Festivos ID="txtFechaModificacion" runat="server" />
                </td>
            </tr>
            <tr class="rowB">
                <td>Ultima acción                
                </td>
                <td>Proxima acción                
                 
                 <asp:HiddenField ID="hdfIdBitacoraAcciones" runat="server" Value="0" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtUltimaAccion" Width="98%" MaxLength="400" Height="80px" TextMode="MultiLine"
                        onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteUltimaAccion" runat="server" TargetControlID="txtUltimaAccion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtProximaAccion" Width="98%" MaxLength="400" Height="80px" TextMode="MultiLine"
                        onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteProximaAccion" runat="server" TargetControlID="txtProximaAccion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
        <asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView ID="gvBitacoraAcciones" align="center" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="none" DataKeyNames="IdBitacoraAcciones" OnSelectedIndexChanged="gvBitacoraAcciones_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle traza de acciones" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="IdBitacoraAcciones" DataField="IdBitacoraAcciones" Visible="false" />
                                <asp:TemplateField HeaderText="Fecha Registro" SortExpression="FechaRegistro">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaRegistro" runat="server" Text=' <%#Eval("FechaRegistro")==null?string.Empty:Convert.ToDateTime(Eval("FechaRegistro").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Modificación" SortExpression="FechaModifica">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaModifica" runat="server" Text=' <%#Eval("FechaModifica")==null?string.Empty:Convert.ToDateTime(Eval("FechaModifica").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Usuario" DataField="UsuarioBitacora" />
                                <asp:BoundField HeaderText="Última Acción" DataField="UltimaAccion" />
                                <asp:BoundField HeaderText="Próxima Acción" DataField="ProximaAccion" />

                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
    <!--pop up-->

    <Ajax:ModalPopupExtender ID="mpeTrazaacciones" runat="server" PopupControlID="pnlPopUp" TargetControlID="pnld"
        BackgroundCssClass="modalBackground">
    </Ajax:ModalPopupExtender>
    <asp:Panel ID="pnld" runat="server" CssClass="modalPopup" align="center" Style="display: none" ScrollBars="Vertical"></asp:Panel>
    <asp:Panel ID="pnlPopUp" runat="server" CssClass="modalPopup" align="center" Style="display: none" ScrollBars="Vertical">
        <asp:Label runat="server" Style="line-height: 12px; font-size: 12pt; font-family: tahoma; margin-top: 3px; margin-left: 10px; position: absolute; top: 3; left: 0;">Traza de acciones</asp:Label>
        <asp:ImageButton ID="btnClose" ImageUrl="~/Image/btn/Cancel.png" runat="server" Style="line-height: 12px; width: 18px; font-size: 8pt; font-family: tahoma; margin-top: 1px; margin-right: 2px; position: absolute; top: 0; right: 0;" />
        <br />
        <asp:GridView ID="gvTrazaAcciones" align="center" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="none">
            <Columns>
                <asp:TemplateField HeaderText="Fecha Registro" SortExpression="FechaRegistro">
                    <ItemTemplate>
                        <asp:Label ID="lblFechaRegistro" runat="server" Text=' <%#Eval("FechaRegistro")==null?string.Empty:Convert.ToDateTime(Eval("FechaRegistro").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Usuario Registro" DataField="UsuarioCrea" />
                <asp:TemplateField HeaderText="Fecha Modificación" SortExpression="FechaModifica">
                    <ItemTemplate>
                        <asp:Label ID="lblFechaModifica" runat="server" Text=' <%#Eval("FechaModifica")==null?string.Empty:Convert.ToDateTime(Eval("FechaModifica").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Usuario Modifica" DataField="UsuarioModifica" />
                <asp:BoundField HeaderText="Última Acción" DataField="UltimaAccion" />
                <asp:BoundField HeaderText="Próxima Acción" DataField="ProximaAccion" />

            </Columns>
            <AlternatingRowStyle CssClass="rowBG" />
            <EmptyDataRowStyle CssClass="headerForm" />
            <HeaderStyle CssClass="headerForm" />
            <RowStyle CssClass="rowAG" />
        </asp:GridView>
    </asp:Panel>


</asp:Content>
