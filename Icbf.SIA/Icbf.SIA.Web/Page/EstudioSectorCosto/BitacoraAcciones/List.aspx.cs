//-----------------------------------------------------------------------
// <copyright file="List.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_BitacoraAcciones_List</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Data;
using System.Linq.Expressions;

/// <summary>
/// Clase para consultar las bit�coras de acciones
/// </summary>
public partial class Page_BitacoraAcciones_List : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/BitacoraAcciones";

    /// <summary>
    /// Instancia a capa de servicio EstudioSectorCostoService
    /// </summary>
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    /// Instancia a capa de ManejoControlesContratos
    ManejoControlesContratos vManejoControlesContratos;

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// M�todo para buscar registro
    /// </summary>
    private void Buscar()
    {
        try
        {
            ucConsulta.GetValues();
            long? vIdConsecutivoEstudio = ucConsulta.vIdConsecutivoEstudio;
            string vNombreAbreviado = ucConsulta.vNombreAbreviado;
            string vObjeto = ucConsulta.vObjeto;
            int? vIdResponsableES = ucConsulta.vIdResponsableES;
            int? vIdResponsableEC = ucConsulta.vIdResponsableEC;
            int? vIdModalidadDeSeleccion = ucConsulta.vIdModalidadDeSeleccion;
            int? vDireccionSolicitante = ucConsulta.vDireccionSolicitante;
            int? vEstado = ucConsulta.vEstado;

            List<BitacoraAcciones> ListaBitacoraAcciones = vEstudioSectorCostoService.ConsultarBitacoraAccioness(vIdConsecutivoEstudio, vNombreAbreviado, vObjeto, vIdResponsableES, vIdResponsableEC, vIdModalidadDeSeleccion, vDireccionSolicitante, vEstado);

            ViewState[Constantes.ViewStateBitacoraAcciones] = ListaBitacoraAcciones;
            LLenarGrilla(ListaBitacoraAcciones);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);

        }
    }

    /// <summary>
    /// M�todo para configurar p�gina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoExcel += new ToolBarDelegate(btnExportar_Click);

            gvBitacoraAcciones.PageSize = PageSize();
            gvBitacoraAcciones.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registro Bit&aacute;cora de Acciones", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvBitacoraAcciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("BitacoraAccion.IdBitacoraAcciones", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para exportar registro
    /// </summary>
    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvBitacoraAcciones.AllowPaging;
        Boolean vPaginadorError = gvBitacoraAcciones.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvBitacoraAcciones.AllowPaging = false;

        List<BitacoraAcciones> vListaBitacoraAccioness = ViewState[Constantes.ViewStateBitacoraAcciones] as List<BitacoraAcciones>;
        datos = Utilidades.GenerarDataTable(gvBitacoraAcciones, vListaBitacoraAccioness);
        GridView datosexportar = new GridView();
        datosexportar.DataSource = datos;
        datosexportar.DataBind();
        vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "BitacoraAcciones_" + DateTime.Now.ToString("ddMMyyyy"));
        gvBitacoraAcciones.AllowPaging = vPaginador;

    }

    /// <summary>
    /// M�todo para llenar la grilla de registros
    /// </summary>
    /// <param name="pLstGestion">Lista de registros a llenar en la grilla</param>
    protected void LLenarGrilla(List<BitacoraAcciones> pLstGestion)
    {
        if (pLstGestion == null || pLstGestion.Count == 0)
        {
            gvBitacoraAcciones.DataSource = null;
            gvBitacoraAcciones.DataBind();
            return;            
        }
        toolBar.LipiarMensajeError();

        foreach(BitacoraAcciones bitacora in pLstGestion)
        {
            bitacora.UsuarioBitacora = GetSessionUser().NombreUsuario;
        }

        string pSortExpresion = ViewState[Constantes.ViewStateSortExpresion] == null ? string.Empty : ViewState[Constantes.ViewStateSortExpresion].ToString();
        if (pSortExpresion.Equals(string.Empty))
        {
            OrdenarGrilla(Constantes.SinOrden, pLstGestion);
        }
        else
        {
            OrdenarGrilla(pSortExpresion, pLstGestion);
        }

    }

    /// <summary>
    /// M�todo para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("BitacoraAccion.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("BitacoraAccion.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para ordenar la grilla
    /// </summary>
    /// /// <param name="pSortExpresion">La forma de ordenamiento de la grilla</param>
    /// <param name="vLstConsulta">Lista de registros a ordenar en la grilla</param>
    protected void OrdenarGrilla(string pSortExpresion, List<BitacoraAcciones> vLstConsulta)
    {
        if (vLstConsulta != null)
        {
            if (pSortExpresion.Equals(Constantes.SinOrden))
            {
                gvBitacoraAcciones.DataSource = vLstConsulta.OrderByDescending(x=>x.IdEstudio).ToList();
                gvBitacoraAcciones.DataBind();
                return;

            }

            var param = Expression.Parameter(typeof(BitacoraAcciones), pSortExpresion);
            var sortExpression = Expression.Lambda<Func<BitacoraAcciones, object>>(Expression.Convert(Expression.Property(param, pSortExpresion), typeof(object)), param);

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvBitacoraAcciones.DataSource = vLstConsulta.AsQueryable<BitacoraAcciones>().OrderBy(sortExpression).ToList<BitacoraAcciones>();
            }
            else
            {
                gvBitacoraAcciones.DataSource = vLstConsulta.AsQueryable<BitacoraAcciones>().OrderByDescending(sortExpression).ToList<BitacoraAcciones>();
            };

            gvBitacoraAcciones.DataBind();
        }
    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento click bot�n del buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Evento click del bot�n nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click del bot�n exportar
    /// </summary>
    /// <param name="sender">Boton btnExportar</param>
    /// <param name="e">Evento click</param>
    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvBitacoraAcciones.Rows.Count > 0)
        {
            Exportar();
        }        
        else
        {
            gvBitacoraAcciones.DataSource = null;
            gvBitacoraAcciones.DataBind();            
        }
    }

    /// <summary>
    /// Evento al seleccionar registro 
    /// </summary>
    /// <param name="sender">Grilla gvConsultaDependencias</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvBitacoraAcciones.SelectedRow);
    }

    /// <summary>
    /// Evento de paginaci�n 
    /// </summary>
    /// <param name="sender">Grilla gvConsultaDependencias</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBitacoraAcciones.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Evento de ordenaci�n 
    /// </summary>
    /// <param name="sender">Grilla gvConsultaDependencias</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (ViewState[Constantes.ViewStateSortDirection] != null && GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
        }

        OrdenarGrilla(e.SortExpression, ViewState[Constantes.ViewStateBitacoraAcciones] as List<BitacoraAcciones>);
        ViewState[Constantes.ViewStateSortExpresion] = e.SortExpression;

    }
    #endregion

    /// <summary>
    /// Atributo que describe la direcci�n del ordenamiento
    /// </summary>    
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState[Constantes.ViewStateSortDirection] == null)
            {
                ViewState[Constantes.ViewStateSortDirection] = SortDirection.Ascending;
            }

            return (SortDirection)ViewState[Constantes.ViewStateSortDirection];
        }
        set { ViewState[Constantes.ViewStateSortDirection] = value; }
    }
}
