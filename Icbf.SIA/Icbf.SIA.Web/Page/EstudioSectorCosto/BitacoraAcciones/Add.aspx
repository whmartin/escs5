<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_BitacoraAcciones_Add" Culture="ES-CO" UICulture="ES-CO" %>

<%@ Register TagPrefix="uc2" TagName="Festivos" Src="~/General/General/EstudioSectores/CalendarioFestivos.ascx" %>
<%@ Register TagPrefix="ucPopUp" TagName="PopUpConsulta" Src="~/General/General/EstudioSectores/EstudioConsultaPopUp.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }
    </script>

    <table width="90%" align="center">
        <tr class="rowB">
            <td>Consecutivo solicitud *                
            </td>
            <td>Usuario *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:HiddenField ID="hdnIdBitacoraEstudio" runat="server" />
                <asp:TextBox runat="server" ID="txtConsecutivoEstudio" MaxLength="5" Width="50%" AutoPostBack="true" OnTextChanged="txtConsecutivoEstudio_TextChanged" ClientIDMode="Static"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender runat="server" ID="fteConsecutivoEstudio" TargetControlID="txtConsecutivoEstudio" FilterType="Numbers"></Ajax:FilteredTextBoxExtender>
                <asp:ImageButton runat="server" ID="btnLupa" ImageUrl="~/Image/btn/list.png" class="SetFocus" OnClientClick="MostrarPopUp(); return false;" />
                <asp:RequiredFieldValidator ID="rfvConsecutivoEstudio" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtConsecutivoEstudio" ValidationGroup="btnGuardar" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtUsuario"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtUsuario" ValidationGroup="btnGuardar" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowB">
            <td>Fecha registro *
            </td>
            <td>Fecha modificación                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc2:Festivos ID="txtFechaRegistro" runat="server" />
            </td>
            <td>
                <uc2:Festivos ID="txtFechaModificacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>Ultima acción                
            </td>
            <td>Proxima acción                
                 <asp:ImageButton runat="server" ID="btnAgregarBitacoraAccion" ImageUrl="~/Image/btn/add.gif" OnClick="btnAgregarBitacoraAccion_Click" ToolTip="Agregar" />
                <asp:HiddenField ID="hdfIdBitacoraAcciones" runat="server" Value="0" />
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUltimaAccion" Width="98%" MaxLength="400" Height="80px" TextMode="MultiLine"
                    onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteUltimaAccion" runat="server" TargetControlID="txtUltimaAccion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtProximaAccion" Width="98%" MaxLength="400" Height="80px" TextMode="MultiLine"
                    onKeyDown="limitText(this,400);" onKeyUp="limitText(this,400);" CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteProximaAccion" runat="server" TargetControlID="txtProximaAccion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="gvBitacoraAcciones" align="center" runat="server" OnPageIndexChanging="gvBitacoraAcciones_PageIndexChanging" OnRowCommand="gvBitacoraAcciones_RowCommand" AutoGenerateColumns="False" Width="100%" GridLines="none" AllowPaging="True" DataKeyNames="" CellPadding="0" Height="16px" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" CommandArgument='<%#Eval("IdBitacoraAcciones").ToString()+"-"+((GridViewRow) Container).RowIndex %>' ImageUrl="~/Image/btn/edit.gif"
                                        ToolTip="Editar grilla" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="IdBitacoraAcciones" DataField="IdBitacoraAcciones" Visible="false" />
                            <asp:TemplateField HeaderText="Fecha Registro" SortExpression="FechaRegistro">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaRegistro" runat="server" Text=' <%#Eval("FechaRegistro")==null?string.Empty:Convert.ToDateTime(Eval("FechaRegistro").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Modificación" SortExpression="FechaModifica">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaModifica" runat="server" Text=' <%#Eval("FechaModifica")==null?string.Empty:Convert.ToDateTime(Eval("FechaModifica").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Usuario" DataField="UsuarioBitacora" />
                            <asp:BoundField HeaderText="Última Acción" DataField="UltimaAccion" />
                            <asp:BoundField HeaderText="Próxima Acción" DataField="ProximaAccion" />
                            <%--<asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Eliminar" CommandArgument='<%#Eval("IdBitacoraAcciones").ToString()+"-"+((GridViewRow) Container).RowIndex %>' ImageUrl="~/Image/btn/delete.gif"
                                            ToolTip="Eliminar" OnClientClick="return confirm('¿Seguro que desea eliminar el registro?');"  /> 
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
    <script type="text/javascript" language="javascript" src="../../../Scripts/ShowPopUpLupasESC.js"></script>
    <script type="text/javascript" language="javascript">
        function MostrarPopUp() {
            GetListRSE("txtConsecutivoEstudio")
        }
    </script>
</asp:Content>
