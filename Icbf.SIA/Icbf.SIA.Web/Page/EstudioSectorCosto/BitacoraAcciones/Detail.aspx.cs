//-----------------------------------------------------------------------
// <copyright file="Detail.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_BitacoraAcciones_Detail</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;

/// <summary>
/// Clase para ver el detalle de la bitácora de acción
/// </summary>
public partial class Page_BitacoraAcciones_Detail : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/BitacoraAcciones";

    /// <summary>
    /// Instancia a capa de servicio EstudioSectorCostoService
    /// </summary>
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la página y tipo de transacción.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// Método para cargar datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            decimal vIdBitacoraEstudio = Convert.ToDecimal(GetSessionParameter("BitacoraAccion.IdBitacoraAcciones"));
            RemoveSessionParameter("BitacoraAccion.IdBitacoraAcciones");
            if (GetSessionParameter("BitacoraAccion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("BitacoraAccion.Guardado");

            BitacoraEstudio vBitacoraEstudio = new BitacoraEstudio();
            hdnIdBitacoraEstudio.Value = vIdBitacoraEstudio.ToString();
            vBitacoraEstudio = vEstudioSectorCostoService.ConsultarBitacoraEstudio(vIdBitacoraEstudio);
            txtConsecutivoEstudio.Text = vBitacoraEstudio.IdConsecutivoEstudio.ToString();

            txtFechaRegistro.SetDate(vBitacoraEstudio.FechaCrea);
            txtFechaModificacion.SetDate(vBitacoraEstudio.FechaModifica);
            txtUsuario.Text = GetSessionUser().NombreUsuario;
            CargarGrilla(vIdBitacoraEstudio);

            ObtenerAuditoria(PageName, hfIdConsecutivoBitacoraAccion.Value);
            txtConsecutivoEstudio.Enabled = false;
            txtUsuario.Enabled = false;
            txtFechaRegistro.Enabled(false);
            txtFechaModificacion.Enabled(false);
            txtUltimaAccion.Enabled = false;
            txtProximaAccion.Enabled = false;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar la grilla
    /// </summary>
    /// <param name="pIdBitacoraEstudio">Id de la bitacora</param>
    private void CargarGrilla(decimal pIdBitacoraEstudio)
    {
        List<BitacoraAcciones> vLstBitacoraAccion = vEstudioSectorCostoService.ConsultarBitacoraAcciones(pIdBitacoraEstudio);
        ViewState[Constantes.ViewStateBitacoraAcciones] = vLstBitacoraAccion;
        foreach(BitacoraAcciones vBitacora in vLstBitacoraAccion)
        {
            vBitacora.UsuarioBitacora = GetSessionUser().NombreUsuario;
        }
        gvBitacoraAcciones.DataSource = vLstBitacoraAccion;
        gvBitacoraAcciones.DataBind();
        if(vLstBitacoraAccion!=null && vLstBitacoraAccion.Count > 0)
        {
            BitacoraAcciones vAuxBitacora = vLstBitacoraAccion[0];
            txtFechaModificacion.SetDate(vAuxBitacora.FechaModifica);
            txtFechaRegistro.SetDate(vAuxBitacora.FechaRegistro);
            txtProximaAccion.Text = vAuxBitacora.ProximaAccion;
            txtUltimaAccion.Text = vAuxBitacora.UltimaAccion;
           
        }

    }

    /// <summary>
    /// Método para eliminar un registro
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            decimal vIdBitacoraEstudio = decimal.Parse(hdnIdBitacoraEstudio.Value);

            BitacoraEstudio vBitacoraEstudio = new BitacoraEstudio();
            vBitacoraEstudio.IdBitacoraEstudio = vIdBitacoraEstudio;
            vBitacoraEstudio = vEstudioSectorCostoService.ConsultarBitacoraEstudio(vIdBitacoraEstudio);
            InformacionAudioria(vBitacoraEstudio, this.PageName, vSolutionPage);
            int vResultado = vEstudioSectorCostoService.EliminarBitacoraEstudio(vBitacoraEstudio);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }

            toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            SetSessionParameter("BitacoraAccion.Eliminado", "1");
            NavigateTo(SolutionPage.List);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para configurar página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Registro Bit&aacute;cora de Acciones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            txtConsecutivoEstudio.Focus();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvBitacoraAcciones.DataKeys[rowIndex].Value.ToString();

            mpeTrazaacciones.Show();
            mpeTrazaacciones.CancelControlID = "btnClose";
            gvTrazaAcciones.DataSource = vEstudioSectorCostoService.ConsultarBitacoraAccionTraza(decimal.Parse(strValue));
            gvTrazaAcciones.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// Evento click del botón nuevo
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Evento click del botón editar
    /// </summary>
    /// <param name="sender">Boton btnEditar</param>
    /// <param name="e">Evento click</param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("BitacoraAccion.IdBitacoraAcciones", hdnIdBitacoraEstudio.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Evento click del botón eliminar
    /// </summary>
    /// <param name="sender">Boton btnEliminar</param>
    /// <param name="e">Evento click</param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Evento click del botón Buscar
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento al seleccionar registro 
    /// </summary>
    /// <param name="sender">Grilla gvBitacoraAcciones</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvBitacoraAcciones.SelectedRow);
    }

    /// <summary>
    /// Evento click del botón para cerrar el pop-up de la traza de acciones
    /// </summary>
    /// <param name="sender">Boton btnClose</param>
    /// <param name="e">Evento click</param>
    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        mpeTrazaacciones.Hide();
    }
    #endregion
}
