//-----------------------------------------------------------------------
// <copyright file="Add.aspx.cs" company="ICBF"> 
// Copyright (c) 2017 Todos los derechos reservados.
// </copyright>
// <summary>Esta es la clase  Page_BitacoraAcciones_Add</summary>
// <author>INGENIAN SOFTWARE</author>
// <date>07/07/2017 0800</date>
//-----------------------------------------------------------------------

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.EstudioSectorCosto.Service;
using Icbf.Utilities.Exceptions;
using Icbf.EstudioSectorCosto.Entity;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Clase para agregar una bit�cora de acci�n
/// </summary>
public partial class Page_BitacoraAcciones_Add : GeneralWeb
{
    /// <summary>
    /// Instancia de master principal
    /// </summary>
    masterPrincipal toolBar;
    string PageName = "EstudioSectorCosto/BitacoraAcciones";

    /// <summary>
    /// Instancia a capa de servicio EstudioSectorCostoService
    /// </summary>
    EstudioSectorCostoService vEstudioSectorCostoService = new EstudioSectorCostoService();

    /// <summary>
    /// Evento para cargar los controles y eventos.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The PreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Evento para validar el acceso a la p�gina y tipo de transacci�n.
    /// </summary>
    /// <param name="sender">The Page</param>
    /// <param name="e">The Load</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    #region METODOS

    /// <summary>
    /// M�todo para guardar registro de bit�cora de est�dio
    /// </summary>
    private void Guardar()
    {
        try
        {
            decimal vIdBitacoraEstudio;
            BitacoraEstudio vBitacoraEstudio = new BitacoraEstudio();
            vBitacoraEstudio.IdConsecutivoEstudio = decimal.Parse(txtConsecutivoEstudio.Text);
            if (Request.QueryString["oP"] == "E")
            {
                vBitacoraEstudio.IdBitacoraEstudio = decimal.Parse(hdnIdBitacoraEstudio.Value);
                vBitacoraEstudio.UsuarioModifica = GetSessionUser().NombreUsuario;
                vBitacoraEstudio.FechaModifica = txtFechaModificacion.GetDate();
                InformacionAudioria(vBitacoraEstudio, this.PageName, SolutionPage.Edit);
                vEstudioSectorCostoService.ModificarBitacoraEstudio(vBitacoraEstudio);
                vIdBitacoraEstudio = decimal.Parse(hdnIdBitacoraEstudio.Value);
                GuardarBitacoraAccion(vIdBitacoraEstudio, vBitacoraEstudio.UsuarioModifica);
            }
            else
            {
                vBitacoraEstudio.UsuarioCrea = GetSessionUser().NombreUsuario;
                vBitacoraEstudio.FechaCrea = txtFechaRegistro.GetDate();
                InformacionAudioria(vBitacoraEstudio, this.PageName, SolutionPage.Add);                
                vIdBitacoraEstudio = vEstudioSectorCostoService.InsertarBitacoraEstudio(vBitacoraEstudio);
                GuardarBitacoraAccion(vIdBitacoraEstudio, string.Empty);
            }

            SetSessionParameter("BitacoraAccion.IdBitacoraAcciones", vIdBitacoraEstudio);
            SetSessionParameter("BitacoraAccion.Guardado", "1");
            NavigateTo(SolutionPage.Detail);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para guardar registro de bit�cora de acci�n
    /// </summary>
    /// <param name="pIdBitacoraEstudio">Id de la bit�cora de est�dio</param>
    /// <param name="pUsuarioModifica">Usuario que modifica</param>
    public void GuardarBitacoraAccion(decimal pIdBitacoraEstudio, string pUsuarioModifica)
    {
        List<BitacoraAcciones> vLstBitacoraAcciones = ViewState[Constantes.ViewStateBitacoraAcciones] as List<BitacoraAcciones>;

        if (vLstBitacoraAcciones == null)
        {
            return;
        }

        foreach (BitacoraAcciones vBitacora in vLstBitacoraAcciones)
        {
            vBitacora.IdBitacoraEstudio = pIdBitacoraEstudio;
            vBitacora.UsuarioModifica = pUsuarioModifica.Equals(string.Empty) ? null : pUsuarioModifica;
            vBitacora.FechaModificacion = pUsuarioModifica.Equals(string.Empty) ? null : txtFechaModificacion.GetDate();
            if (vBitacora.IdBitacoraAcciones == 0)
            {
                InformacionAudioria(vBitacora, this.PageName, SolutionPage.Add);
                vEstudioSectorCostoService.InsertarBitacoraAcciones(vBitacora);
            }
            else
            {
                InformacionAudioria(vBitacora, this.PageName, SolutionPage.Edit);
                vEstudioSectorCostoService.ModificarBitacoraAcciones(vBitacora);
            }
        }

    }

    /// <summary>
    /// M�todo para configurar la p�gina
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Registro Bit&aacute;cora de Acciones", SolutionPage.Add.ToString());

            txtConsecutivoEstudio.Enabled = false;
            txtUsuario.Enabled = false;
            txtFechaRegistro.Enabled(false);
            //txtFechaModificacion.Enabled(true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar registro
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            decimal vIdBitacoraEstudio = Convert.ToDecimal(GetSessionParameter("BitacoraAccion.IdBitacoraAcciones"));
            BitacoraEstudio vBitacoraEstudio = new BitacoraEstudio();
            hdnIdBitacoraEstudio.Value = vIdBitacoraEstudio.ToString();
            vBitacoraEstudio = vEstudioSectorCostoService.ConsultarBitacoraEstudio(vIdBitacoraEstudio);
            txtConsecutivoEstudio.Text = vBitacoraEstudio.IdConsecutivoEstudio.ToString();
            CargarGrilla(vIdBitacoraEstudio);
            CargarFechas(txtFechaModificacion);
            txtFechaModificacion.Enabled(false);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar la grilla gvBitacoraAcciones
    /// </summary>
    /// <param name="pIdBitacoraEstudio">Bit�cora de est�dio</param>
    private void CargarGrilla(decimal pIdBitacoraEstudio)
    {
        List<BitacoraAcciones> vLstBitacoraAccion = vEstudioSectorCostoService.ConsultarBitacoraAcciones(pIdBitacoraEstudio);
        ViewState[Constantes.ViewStateBitacoraAcciones] = vLstBitacoraAccion;
        if (vLstBitacoraAccion != null && vLstBitacoraAccion.Count > 0)
        {
            txtFechaRegistro.SetDate(vLstBitacoraAccion.ElementAt(0).FechaRegistro);
        }
        foreach (BitacoraAcciones bitacora in vLstBitacoraAccion)
        {
            bitacora.UsuarioBitacora = GetSessionUser().NombreUsuario;
        }
        gvBitacoraAcciones.DataSource = vLstBitacoraAccion;
        gvBitacoraAcciones.DataBind();
        hdfIdBitacoraAcciones.Value = "0";
    }

    /// <summary>
    /// M�todo para cargar fechas
    /// </summary>
    /// <param name="contrl">UserControl para el manejo de fechas</param>
    public void CargarFechas(General_General_EstudioSectores_CalendarioFestivos contrl)
    {
        try
        {
            DateTime vLaFecha = DateTime.Now;
            if (contrl.EsDiaHabil(vLaFecha))
            {
                contrl.SetDate(vLaFecha);
            }
            else
            {
                contrl.SetDate(contrl.GetDiaHabil(vLaFecha));
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo para cargar datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarFechas(txtFechaRegistro);
            txtUsuario.Text = GetSessionUser().NombreUsuario;
            txtConsecutivoEstudio.Focus();
            txtFechaRegistro.SetReadOnly(true);
            txtFechaModificacion.Enabled(false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow">Fila seleccionada</param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvBitacoraAcciones.DataKeys[rowIndex].Value.ToString();
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// M�todo retornar el valor del consecutivo de estudio elegido en el pop-up de consulta de consecutivos de est�dio en su respectivo campo
    /// </summary>
    /// <param name="IdConsecutivoEstudioConsulta">Consecutivo de est�dio consultado</param>
    public void Retornar(long IdConsecutivoEstudioConsulta)
    {
        txtConsecutivoEstudio.Text = IdConsecutivoEstudioConsulta.ToString();
    }

    /// <summary>
    /// M�todo para editar o eliminar una bit�cora de acci�n
    /// </summary>
    /// <param name="pCommandArgument">Argumento eliminar o editar</param>
    /// <param name="pEsEliminar">True para eliminar, False para editar</param>
    protected void EditarEliminarBitacoraAccion(string pCommandArgument, bool pEsEliminar)
    {
        CargarFechas(txtFechaModificacion);
        List<BitacoraAcciones> vLstBitacoraAcciones = ViewState[Constantes.ViewStateBitacoraAcciones] as List<BitacoraAcciones>;
        if (vLstBitacoraAcciones == null)
        {
            return;
        }
        string[] argumentos = pCommandArgument.Split('-');
        decimal IdBitacoraAccion = decimal.Parse(argumentos[0]);
        int index = int.Parse(argumentos[1]);
        BitacoraAcciones bitacoraAccion;
        if (IdBitacoraAccion == 0)
        {
            bitacoraAccion = vLstBitacoraAcciones[index];
            vLstBitacoraAcciones.RemoveAt(index);
        }
        else
        {
            bitacoraAccion = vLstBitacoraAcciones.Where(x => x.IdBitacoraAcciones == IdBitacoraAccion).FirstOrDefault();
            vLstBitacoraAcciones.Remove(bitacoraAccion);
        }

        if (!pEsEliminar)
        {
            hdfIdBitacoraAcciones.Value = bitacoraAccion.IdBitacoraAcciones.ToString();
            //txtUsuario.Text = bitacoraAccion.UsuarioBitacora;
            txtFechaRegistro.SetDate(bitacoraAccion.FechaRegistro);
            txtUltimaAccion.Text = bitacoraAccion.UltimaAccion;
            txtProximaAccion.Text = bitacoraAccion.ProximaAccion;

            hdfIdBitacoraAcciones.Value = bitacoraAccion.IdBitacoraAcciones.ToString();
            btnAgregarBitacoraAccion.ImageUrl = "~/Image/btn/apply.png";
            btnAgregarBitacoraAccion.ToolTip = "Aplicar";

        }
        ViewState[Constantes.ViewStateBitacoraAcciones] = vLstBitacoraAcciones;        
        gvBitacoraAcciones.DataSource = vLstBitacoraAcciones;
        gvBitacoraAcciones.DataBind();
    }

    /// <summary>
    /// M�todo para agregar una bit�cora de acci�n
    /// </summary>    
    protected void AgregarBitacoraAccion()
    {
        btnAgregarBitacoraAccion.Focus();
        btnAgregarBitacoraAccion.ImageUrl = "~/Image/btn/add.gif";
        if (txtUsuario.Equals(String.Empty) || txtFechaRegistro.Equals(string.Empty) || txtConsecutivoEstudio.Text.Equals(string.Empty))
        {
            return;
        }

        List<BitacoraAcciones> vLstBitacoraAcciones = ViewState[Constantes.ViewStateBitacoraAcciones] as List<BitacoraAcciones>;
        if (vLstBitacoraAcciones == null)
        {
            vLstBitacoraAcciones = new List<BitacoraAcciones>();
        }

        vLstBitacoraAcciones.Add(new BitacoraAcciones
        {
            IdBitacoraAcciones = decimal.Parse(hdfIdBitacoraAcciones.Value),
            UsuarioBitacora = txtUsuario.Text,
            FechaRegistro = txtFechaRegistro.GetDate(),
            FechaModifica= hdfIdBitacoraAcciones.Value.Equals("0")?(DateTime?)null: txtFechaModificacion.GetDate(),
            UltimaAccion = txtUltimaAccion.Text,
            ProximaAccion = txtProximaAccion.Text,
            UsuarioCrea = GetSessionUser().NombreUsuario,
            UsuarioModifica = GetSessionUser().NombreUsuario,

        });
        gvBitacoraAcciones.DataSource = vLstBitacoraAcciones;
        gvBitacoraAcciones.DataBind();
        ViewState[Constantes.ViewStateBitacoraAcciones] = vLstBitacoraAcciones;
        hdfIdBitacoraAcciones.Value = "0";
        txtUltimaAccion.Text = string.Empty;
        txtProximaAccion.Text = string.Empty;
    }
    #endregion

    #region EVENTOS

    /// <summary>
    /// M�todo para guardar registro
    /// </summary>
    /// <param name="sender">Boton btnGuardar</param>
    /// <param name="e">Evento click</param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// M�todo para agregar un registro
    /// </summary>
    /// <param name="sender">Boton btnNuevo</param>
    /// <param name="e">Evento click</param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// M�todo para buscar registro
    /// </summary>
    /// <param name="sender">Boton btnBuscar</param>
    /// <param name="e">Evento click</param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Evento al seleccionar registro 
    /// </summary>
    /// <param name="sender">Grilla gvBitacoraAcciones</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvBitacoraAcciones.SelectedRow);
    }

    /// <summary>
    /// Evento de paginaci�n 
    /// </summary>
    /// <param name="sender">Grilla gvBitacoraAcciones</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBitacoraAcciones.PageIndex = e.NewPageIndex;
        List<BitacoraAcciones> vLstBitacoraAcciones = ViewState[Constantes.ViewStateBitacoraAcciones] as List<BitacoraAcciones>;
        gvBitacoraAcciones.DataSource = vLstBitacoraAcciones;
        gvBitacoraAcciones.DataBind();
    }

    /// <summary>
    /// Evento de botones dentro de la grilla gvBitacoraAcciones
    /// </summary>
    /// <param name="sender">Grilla gvBitacoraAcciones</param>
    /// <param name="e">Evento click</param>
    protected void gvBitacoraAcciones_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Editar":
                EditarEliminarBitacoraAccion(e.CommandArgument.ToString(), false);
                break;
            case "Eliminar":
                EditarEliminarBitacoraAccion(e.CommandArgument.ToString(), true);
                break;
        }

    }

    /// <summary>
    /// Evento click boton agregar bit�cora de acci�n
    /// </summary>
    /// <param name="sender">Bot�n btnAgregarBitacoraAccion</param>
    /// <param name="e">Evento click</param>
    protected void btnAgregarBitacoraAccion_Click(object sender, ImageClickEventArgs e)
    {
        AgregarBitacoraAccion();
    }

    /// <summary>
    /// Evento textChanged para el campo txtConsecutivoEstudio, para verificar si el consecutivo ya esta o no utilizado en la bit�cora
    /// </summary>
    /// <param name="sender">Campo de texto txtConsecutivoEstudio</param>
    /// <param name="e">Evento change</param>
    protected void txtConsecutivoEstudio_TextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (vEstudioSectorCostoService.ConsultarBitacoraEstudioExiste(decimal.Parse(txtConsecutivoEstudio.Text)))
        {
            toolBar.MostrarMensajeError("El consecutivo ya fue utilizado, verifique por favor");
            txtConsecutivoEstudio.Text = string.Empty;
        }
    }
    #endregion
}
