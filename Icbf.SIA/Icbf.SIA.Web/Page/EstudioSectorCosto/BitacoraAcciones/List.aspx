<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_BitacoraAcciones_List" %>

<%@ Register TagPrefix="uc1" TagName="consulta" Src="~/General/General/EstudioSectores/FormularioConsulta.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <uc1:consulta runat="server" ID="ucConsulta" />
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvBitacoraAcciones" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="true"
                        GridLines="None" Width="100%" DataKeyNames="IdBitacoraEstudio" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvBitacoraAcciones_PageIndexChanging" OnSelectedIndexChanged="gvBitacoraAcciones_SelectedIndexChanged" OnSorting="gvBitacoraAcciones_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Consecutivo solicitud" DataField="IdEstudio" SortExpression="IdEstudio" />
                            <asp:BoundField HeaderText="Nombre abreviado" DataField="NombreAbreviado" SortExpression="NombreAbreviado" />
                            <%--<asp:BoundField HeaderText="Fecha Registro" DataField="FechaRegistro" SortExpression="FechaRegistro" />
                            <asp:BoundField HeaderText="Fecha Modificaci&oacute;n" DataField="FechaModificacion" SortExpression="FechaModificacion" />--%>
                            <asp:TemplateField HeaderText="Fecha Registro" SortExpression="FechaRegistro">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaRegistro" runat="server" Text=' <%#Eval("FechaRegistro")==null?string.Empty:Convert.ToDateTime(Eval("FechaRegistro").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Modificaci&oacute;n" SortExpression="FechaModificacion">
                                <ItemTemplate>
                                    <asp:Label ID="lblFechaModificacion" runat="server" Text=' <%#Eval("FechaModificacion")==null?string.Empty: Convert.ToDateTime(Eval("FechaModificacion").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Usuario" DataField="UsuarioBitacora" SortExpression="UsuarioBitacora" />
                            <asp:BoundField HeaderText="&Uacute;ltima Acci&oacute;n" DataField="UltimaAccion" SortExpression="UltimaAccion" />
                            <asp:BoundField HeaderText="Pr&oacute;xima Acci&oacute;n" DataField="ProximaAccion" SortExpression="ProximaAccion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
