using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;

/// <summary>
/// Página de consulta a través de filtros para la entidad ProyeccionPresupuestos
/// </summary>
public partial class Page_ConsultaNecesidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/ConsultaNecesidad";
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnReporte_Click(object sender, EventArgs e)
    {
        GenerarReporte();
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            //CargarGrilla(gvProyeccionPresupuestos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            //toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            //gvProyeccionPresupuestos.PageSize = PageSize();
            //gvProyeccionPresupuestos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Consulta Necesidades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    //private void SeleccionarRegistro(GridViewRow pRow)
    //{
    //    try
    //    {
    //        int rowIndex = pRow.RowIndex;
    //        string strValue = gvProyeccionPresupuestos.DataKeys[rowIndex].Value.ToString();
    //        SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", strValue);
    //        NavigateTo(SolutionPage.Detail);
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    //protected void gvProyeccionPresupuestos_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    SeleccionarRegistro(gvProyeccionPresupuestos.SelectedRow);
    //}
    //protected void gvProyeccionPresupuestos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    gvProyeccionPresupuestos.PageIndex = e.NewPageIndex;
    //    CargarGrilla((GridView)sender, GridViewSortExpression, true);
    //}
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    //public SortDirection GridViewSortDirection
    //{
    //    get
    //    {
    //        if (ViewState["sortDirection"] == null)
    //            ViewState["sortDirection"] = SortDirection.Ascending;

    //        return (SortDirection)ViewState["sortDirection"];
    //    }
    //    set { ViewState["sortDirection"] = value; }
    //}

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    //public string GridViewSortExpression
    //{
    //    get { return (string)ViewState["sortExpression"]; }
    //    set { ViewState["sortExpression"] = value; }
    //}

    //protected void gvProyeccionPresupuestos_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    CargarGrilla((GridView)sender, e.SortExpression, false);
    //}

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    //private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    //{
    //    //////////////////////////////////////////////////////////////////////////////////
    //    //////Aqui va el código de llenado de datos para la grilla 
    //    //////////////////////////////////////////////////////////////////////////////////

    //    //Lleno una lista con los datos que uso para llenar la grilla
    //    try
    //    {
    //        int? vIdVigencia = null;
    //        int? vIdArea = null;
    //        int? vIdRegional = null;
    //        Decimal? vValorCupo = null;
    //        int? vTotalCupos = null;
    //        int? vIdRubro = null;
    //        String vUsuarioAprobo = null;
    //        DateTime? vFechaAprobacion = null;
    //        if (ddlIdVigencia.SelectedValue!= "-1")
    //        {
    //            vIdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
    //        }
    //        if (ddlIdArea.SelectedValue!= "-1")
    //        {
    //            vIdArea = Convert.ToInt32(ddlIdArea.SelectedValue);
    //        }

    //        if (ddlRegional.SelectedValue != "-1")
    //        {
    //            vIdRegional = Convert.ToInt32(ddlRegional.SelectedValue);
    //        }



    //        var myGridResults = vSIAService.ConsultarProyeccionPresupuestoss( vIdVigencia, vIdArea,vUsuarioAprobo,
    //            vFechaAprobacion, vIdRegional, vIdRubro, "CREA;RECH");
    //        //////////////////////////////////////////////////////////////////////////////////
    //        //////Fin del código de llenado de datos para la grilla 
    //        //////////////////////////////////////////////////////////////////////////////////

    //        if (expresionOrdenamiento != null)
    //        {
    //            //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
    //            if (string.IsNullOrEmpty(GridViewSortExpression))
    //            {
    //                GridViewSortDirection = SortDirection.Ascending;
    //            }
    //            else if (GridViewSortExpression != expresionOrdenamiento)
    //            {
    //                GridViewSortDirection = SortDirection.Descending;
    //            }
    //            if (myGridResults != null)
    //            {
    //                var param = Expression.Parameter(typeof(ProyeccionPresupuestos), expresionOrdenamiento);

    //                //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
    //                var prop = Expression.Property(param, expresionOrdenamiento);

    //                //Creo en tiempo de ejecución la expresión lambda
    //                var sortExpression = Expression.Lambda<Func<ProyeccionPresupuestos, object>>(Expression.Convert(prop, typeof(object)), param);

    //                //Dependiendo del modo de ordenamiento . . .
    //                if (GridViewSortDirection == SortDirection.Ascending)
    //                {

    //                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
    //                    if (cambioPaginacion == false)
    //                    {
    //                        GridViewSortDirection = SortDirection.Descending;
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
    //                    }
    //                    else
    //                    {
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList(); 
    //                    }
    //                }
    //                else
    //                {

    //                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
    //                    if (cambioPaginacion == false)
    //                    {
    //                        GridViewSortDirection = SortDirection.Ascending;
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
    //                    }
    //                    else
    //                    {
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
    //                    }
    //                }

    //                GridViewSortExpression = expresionOrdenamiento;
    //            }
    //        }
    //        else
    //        {
    //            gridViewsender.DataSource = myGridResults;
    //        }

    //        gridViewsender.DataBind();
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ProyeccionPresupuestos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ProyeccionPresupuestos.Eliminado");

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlRegional.DataSource = vSIAService.ConsultarRegionals(null, null);
            ddlRegional.DataTextField = "NombreRegional";
            ddlRegional.DataValueField = "IdRegional";
            ddlRegional.DataBind();
            //ddlRegional.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdArea.Items.Insert(0, new ListItem("Seleccione Regional(es)", "-1"));
            ddlIdArea.Enabled = false;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void GenerarReporte()
    {
        String indicadorRecurso = ddlRegional.Items.Cast<ListItem>().Where(item => item.Selected)
                                 .Aggregate<ListItem, string>(null, (current, item) => current + (item.Value + ";"));
        try
        {
            #region Creacion reporte


            //Crea un objeto de tipo reporte
            Report objReport;
            objReport = new Report("ConsCupos", true, PageName, "Generar Consulta Necesidad");

            //Adiciona los parametros al objeto reporte            
            objReport.AddParameter("IdVigencia", ddlIdVigencia.SelectedValue);
            //objReport.AddParameter("IdRegional", ddlRegional.SelectedValue);
            objReport.AddParameter("IdRegional", indicadorRecurso);
            if (ddlIdArea.SelectedValue != "-1")
                objReport.AddParameter("IdArea", ddlIdArea.SelectedValue);
            //objReport.AddParameter("IdDependencia", txtIDContrato.Text);
            //objReport.AddParameter("IdRubro", txtIDContrato.Text);
            if (TxtConsecutivoInterno.Text != string.Empty)
                objReport.AddParameter("IdConsecutivoInterno", TxtConsecutivoInterno.Text);
            if (txtNumeroIDentificacion.Text != string.Empty)
                objReport.AddParameter("IdDatosProveedor", txtNumeroIDentificacion.Text);

            //

            //Crea un session con el objeto Reporte
            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
            #endregion


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
    {

        try
        {
            var itemcom = ddlRegional.Items.Cast<ListItem>().Where(item => item.Selected);

            if (itemcom.Count() == 0)
                return;

            String indicadorRecurso = itemcom.Aggregate<ListItem, string>
                (null, (current, item) => current + (item.Value + ";"));

            int? regSeleccionadas = indicadorRecurso.Count(x => x == ';');

            if (regSeleccionadas != 34)
            {
                ddlIdArea.Enabled = true;
                ddlIdArea.DataSource = vSIAService.ConsultarAreasRegionales(null, indicadorRecurso);
                ddlIdArea.DataTextField = "NombreArea";
                ddlIdArea.DataValueField = "IdArea";
                ddlIdArea.DataBind();
                ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
            }
            else
            {
                ddlIdArea.Enabled = false;
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
