using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;

/// <summary>
/// Página de visualización detallada para la entidad CupoAreas
/// </summary>
public partial class Page_CupoAreas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/CupoNecesidad";
    SIAService vSIAService = new SIAService();    
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", hfidProyeccionPresupuesto.Value);
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", hfidProyeccionPresupuesto.Value);
        SetSessionParameter("CupoAreas.IdcuposAreasNecesidad", hfIdCupoArea.Value);

        if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
            SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);

        NavigateTo(SolutionPage.Edit);
    }   
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        { 
            int vIdCupoArea = Convert.ToInt32(GetSessionParameter("CupoAreas.IdcuposAreasNecesidad"));
            RemoveSessionParameter("CupoAreas.IdcuposAreasNecesidad");

            hfIdCupoArea.Value = vIdCupoArea.ToString();

            if (GetSessionParameter("CupoAreas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("CupoAreas.Guardado");

            ucHonorario.ModoConsulta(false);
            ucDatosContrato.ModoConsulta(false);
            ucDatosTercero.ModoConsulta(false);

            CupoAreas vCupoAreas = new CupoAreas();
            vCupoAreas = vSIAService.ConsultarCupoAreas(vIdCupoArea);
            ucDatosContrato.ConsecutivoInterno = vCupoAreas.ConsecutivoInterno;
            ucDatosContrato.DependenciaKactusValue = vCupoAreas.IdDependenciaKactus;
            ucDatosContrato.ObjetoValue = vCupoAreas.IdObjeto.ToString();
            var vobjeto = vSIAService.ConsultarObjeto(vCupoAreas.IdObjeto);
            ucDatosContrato.ObjetoText = vobjeto.DescripcionObjeto;
            ucDatosTercero.IdEntidad = vCupoAreas.IdProveedor.ToString();
            ucDatosTercero.LlenarDatosTercro();
            ucHonorario.PorIvaHonorario = vCupoAreas.PorcentajeIVA.ToString();
            ucHonorario.FechaInicialProyectado = vCupoAreas.FechaInicialProyectado;
            ucHonorario.FechaFinalProyectado = vCupoAreas.FechaFinalProyectado;
            //ucHonorario.FechaIngresoICBF = vCupoAreas.FechaIngresoICBF;
            //ucHonorario.ddlIdCategoriaValoresValue = vCupoAreas.IdCategoriaValores.ToString();
            ////var vCategoriaValores = vSIAService.ConsultarCategoriaValores(vCupoAreas.IdCategoriaValores);
            ////ucHonorario.Nivel = vCategoriaValores.Nivel.ToString("N");
            //ucHonorario.ddlNivelSource = vSIAService.ConsultarCategoriaValoress(null, null, true, vCupoAreas.IdCategoriaValores);
            var vCategoriaValores = vSIAService.ConsultarCategoriaValores(vCupoAreas.IdCategoriaValores);
            ucHonorario.ddlCategoriaEmpleadoSource = vSIAService.ConsultarCategoriaEmpleados(null, true,null, null);
            ucHonorario.ddlCategoriaEmpleadoValue = vCategoriaValores.IdCategoriaEmpleados.ToString();
            ucHonorario.ddlCategoriaValoresSource = vSIAService.ConsultarCategoriaValoress(null, null, true, vCategoriaValores.IdCategoriaEmpleados);
            ucHonorario.ddlCategoriaValoresValue = vCupoAreas.IdCategoriaValores.ToString();

            ucHonorario.HonorarioBase = vCupoAreas.HonorarioBase.ToString("C");
            ucHonorario.IvaHonorario = vCupoAreas.IvaHonorario.ToString("C");
            ucHonorario.TotalHonorario = (vCupoAreas.HonorarioBase + vCupoAreas.IvaHonorario).ToString("C");
            ucHonorario.TiempoProyectadoMese = vCupoAreas.TiempoProyectadoMeses.ToString("N");
            ucHonorario.TiempoProyectadoDias = vCupoAreas.TiempoProyectadoDias.ToString("N");
            ucHonorario.TiempoProyectadoAnio = vCupoAreas.TiempoProyectadoAnios.ToString();
            ucHonorario.TotalHonorarioTiempo = vCupoAreas.TotalHonorarioTiempoProyectado.ToString("C");

            ObtenerAuditoria(PageName, hfIdCupoArea.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCupoAreas.UsuarioCrea, vCupoAreas.FechaCrea, vCupoAreas.UsuarioModifica, vCupoAreas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
     
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Proyección Necesidades Áreas/Dependencia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            hfidProyeccionPresupuesto.Value = GetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA").ToString();
            RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA");

            hfDesdeModificar.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
            RemoveSessionParameter("PestanaUserControl.Numero");

            ucHonorario.IdProyeccion = hfidProyeccionPresupuesto.Value;

            CupoAreas vCupoAreas = new CupoAreas();
            var vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(Convert.ToInt32(hfidProyeccionPresupuesto.Value));
            var regionalcupo = vSIAService.ConsultarRegional(vProyeccionPresupuestos.IdRegional);
            var vVigencia = vSIAService.ConsultarVigencia(vProyeccionPresupuestos.IdVigencia);
            var vAreas = vSIAService.ConsultarAreas(vProyeccionPresupuestos.IdArea);
            var rubro = vSIAService.ConsultarRubrosCupos(vProyeccionPresupuestos.IdRubro);
            hfiCodgioRegional.Value = regionalcupo.CodigoRegional;
            txtRegional.Text = regionalcupo.NombreRegional;
            txtVigencia.Text = vVigencia.AcnoVigencia.ToString();
            //txtTotalCupo.Text = vProyeccionPresupuestos.TotalCupos.ToString("N");
            //txtValorAprobado.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            txtRubro.Text = rubro.CodigoRubro;
            if (vAreas.Count > 0)
                txtArea.Text = vAreas[0].NombreArea;
            ucDatosContrato.DependenciaKactusSource = vContratoService.ConsultarDependenciaSolicitante(null, null, regionalcupo.CodigoRegional);
            ucDatosContrato.ObjetoSource = vSIAService.ConsultarObjetos(null, null, true);
            //ucHonorario.ddlIdCategoriaValoresSource = vSIAService.ConsultarCategoriaEmpleados(null, true);

            var CuposAreasASignadas = vSIAService.ConsultarCupoAreass(null, null, null
                                , null, Convert.ToInt32(hfidProyeccionPresupuesto.Value), null);

            decimal vTotalValorAsignado = CuposAreasASignadas.Sum(x => x.TotalHonorarioTiempoProyectado);
            int vTotalCuposAsignado = CuposAreasASignadas.Count();
            txtValorAprobado.Text = (vProyeccionPresupuestos.ValorCupo - vTotalValorAsignado).ToString("C");
            txtTotalCupo.Text = (vProyeccionPresupuestos.TotalCupos - vTotalCuposAsignado).ToString("N0");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarRegistro()
    {
        try
        {
            int vIDCupoAreas = Convert.ToInt32(hfIdCupoArea.Value);

            CupoAreas vCupoAreas = new CupoAreas();
            vCupoAreas = vSIAService.ConsultarCupoAreas(vIDCupoAreas);
            vCupoAreas.UsuarioModifica = GetSessionUser().NombreUsuario;
            InformacionAudioria(vCupoAreas, this.PageName, vSolutionPage);
            int vResultado = vSIAService.EliminarCupoAreas(vCupoAreas);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se complet&oacute; satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 1)
            {
                SetSessionParameter("CupoAreas.Eliminado", "1");                

                if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
                {
                    SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", hfidProyeccionPresupuesto.Value);
                    SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);                   
                    Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
                }
                else
                {
                    SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", hfidProyeccionPresupuesto.Value);
                    Response.Redirect("ListNecesidad.aspx");
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se complet&oacute; satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
