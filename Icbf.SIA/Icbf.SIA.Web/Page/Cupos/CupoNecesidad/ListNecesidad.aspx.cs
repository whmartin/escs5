﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de consulta a través de filtros para la entidad CupoAreas
/// </summary>
public partial class Page_ListNecesidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/CupoNecesidad";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //if (GetState(Page.Master, PageName)) {  }
                Buscar();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", hfIfProyeccionPresupuesto.Value);
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvListCupo, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvListCupo.PageSize = PageSize();
            gvListCupo.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Proyección Necesidades Aprobadas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvListCupo.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("CupoAreas.IdcuposAreasNecesidad", strValue);
            SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", hfIfProyeccionPresupuesto.Value);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvListCupo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvListCupo.SelectedRow);
    }
    protected void gvListCupo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvListCupo.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvListCupo_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vConsecutivoInterno = null;
            string vIdProveedor = null;
            DateTime? vFechaIngresoICBF = null;
            int? vIdCategoriaValores = null;
            Decimal? vHonorarioBase = null;
            Decimal? vIvaHonorario = null;
            Decimal? vPorcentajeIVA = null;
            int? vTiempoProyectadoMeses = null;
            int? vTiempoProyectadoDias = null;
            Boolean? vAprobado = null;
            String vUsuarioAprobo = null;
            DateTime? vFechaAprobacion = null;
            if (txtConsecutivoInterno.Text != "")
            {
                vConsecutivoInterno = Convert.ToString(txtConsecutivoInterno.Text);
            }
            if (txtIdentificacion.Text != "")
            {
                vIdProveedor = txtIdentificacion.Text;
            }
            
            int idProyeccionPresu = Convert.ToInt32(txtIdProyeccion.Text);
            var myGridResults = vSIAService.ConsultarCupoAreass(vConsecutivoInterno, null, null, null
                , idProyeccionPresu, null, vIdProveedor);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(CupoAreas), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<CupoAreas, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("CupoAreas.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("CupoAreas.Eliminado");
          
            int vIdProyeccionPresupuestos = Convert.ToInt32(GetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA"));
            RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA");

            hfIfProyeccionPresupuesto.Value = vIdProyeccionPresupuestos.ToString();
            txtNoCuposProyectado.Text = "0";
            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccionPresupuestos);
            txtNoCuposProyectado.Text = vProyeccionPresupuestos.TotalCupos.ToString("N");
            txtIdProyeccion.Text = vProyeccionPresupuestos.IdProyeccionPresupuestos.ToString();
            txtValorTotalCupoProyectado.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");

            var CuposAreasASignadas = vSIAService.ConsultarCupoAreass(null, null, null
                                    , null, vIdProyeccionPresupuestos, null);

            decimal vTotalValorAsignado = CuposAreasASignadas.Sum(x => x.TotalHonorarioTiempoProyectado);
            int vTotalCuposAsignado = CuposAreasASignadas.Count();
            txtValorProyectadoDisponible.Text = (vProyeccionPresupuestos.ValorCupo - vTotalValorAsignado).ToString("C");
            txtCupoProyectadoDisponible.Text = (vProyeccionPresupuestos.TotalCupos - vTotalCuposAsignado).ToString("N0");

            if (txtCupoProyectadoDisponible.Text == "0")
                toolBar.MostrarBotonNuevo(false);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
