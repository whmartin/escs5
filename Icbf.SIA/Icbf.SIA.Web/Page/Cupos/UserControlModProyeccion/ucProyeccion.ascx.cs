﻿using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Cupos_UserControlModProyeecion_ucProyeccion : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (((Parameters)Session["sessionParameters"]).Get("ProyeccionPresupuestos.Guardado") != null)
        {
            if (((Parameters)Session["sessionParameters"]).Get("ProyeccionPresupuestos.Guardado").ToString() == "1")
            {
                lblSuccess.Visible = true;
                lblSuccess.Text = "Registro modificado correctamente";
                ((Parameters)Session["sessionParameters"]).Remove("ProyeccionPresupuestos.Guardado");
            }
            else if (((Parameters)Session["sessionParameters"]).Get("ProyeccionPresupuestos.Guardado").ToString() == "2")
            {
                lblSuccess.Visible = true;
                lblSuccess.Text = "La proyección tiene asignado un tercero, por favor ajuste";
                ((Parameters)Session["sessionParameters"]).Remove("ProyeccionPresupuestos.Guardado");
            }
        }
    }

    public string IdProyeccion
    {
        get
        {
            return hfIdProyeccion.Value;
        }

        set
        {
            hfIdProyeccion.Value = value;
        }
    }

    public object RegionalSource
    {
        set
        {
            ddlIdRegional.DataSource = value;
            ddlIdRegional.DataValueField = "IdRegional";
            ddlIdRegional.DataTextField = "NombreRegional";
            ddlIdRegional.DataBind();
            ddlIdRegional.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string RegionalValue
    {
        get
        {
            return ddlIdRegional.SelectedValue;
        }
        set
        {
            ddlIdRegional.SelectedValue = value;
        }
    }

    public object AreaSource
    {
        set
        {
            ddlIdArea.DataSource = value;
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataBind();
            ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string AreaValue
    {
        get
        {
            return ddlIdArea.SelectedValue;
        }
        set
        {
            ddlIdArea.SelectedValue = value;
        }
    }

    public object VigenciaSource
    {
        set
        {
            ddlIdVigencia.DataSource = value;
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string VigenciaValue
    {
        get
        {
            return ddlIdVigencia.SelectedValue;
        }
        set
        {
            ddlIdVigencia.SelectedValue = value;
        }
    }

    public object RecursoSource
    {
        set
        {
            ddlIdRecurso.DataSource = value;
            ddlIdRecurso.DataValueField = "IdTipoRecursoFinPptal";
            ddlIdRecurso.DataTextField = "DescTipoRecurso";
            ddlIdRecurso.DataBind();
            ddlIdRecurso.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string RecursoValue
    {
        get
        {
            return ddlIdRecurso.SelectedValue;
        }
        set
        {
            ddlIdRecurso.SelectedValue = value;
        }
    }

    public object RubroSource
    {
        set
        {
            ddlIdRubro.DataSource = value;
            ddlIdRubro.DataValueField = "IdRubroCupos";
            ddlIdRubro.DataTextField = "DescripcionRubroCompleto";
            ddlIdRubro.DataBind();
            ddlIdRubro.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string RubroValue
    {
        get
        {
            return ddlIdRubro.SelectedValue;
        }
        set
        {
            ddlIdRubro.SelectedValue = value;
        }
    }

    public string ValorNecesidad
    {
        get
        {
            return txtValorCupo.Text;
        }
        set
        {
            txtValorCupo.Text = value;
        }

    }

    public string TotalNecesidades
    {
        get
        {
            return txtTotalCupos.Text;
        }
        set
        {
            txtTotalCupos.Text = value;
        }

    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        try
        {
            ((Parameters)Session["sessionParameters"]).Set("PestanaUserControl.Numero", "0");

            string ppidValue = hfIdProyeccion.Value;
            ((Parameters)Session["sessionParameters"]).Set("ProyeccionPresupuestos.IdProyeccionPresupuestos", ppidValue);

            Page.Response.Redirect("~/Page/Cupos/ProyeccionPresupuestos/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            // toolBar.MostrarMensajeError(ex.Message);
        }
    }
}