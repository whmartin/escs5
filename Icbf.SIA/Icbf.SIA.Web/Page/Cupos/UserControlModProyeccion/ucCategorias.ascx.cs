﻿using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Cupos_UserControlModProyeecion_ucCategorias : System.Web.UI.UserControl
{
    SIAService vSIAService = new SIAService();
    GeneralWeb gW = new GeneralWeb();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (((Parameters)Session["sessionParameters"]).Get("ProyeccionCategoria.Guardado") != null)
        {
            lblSuccess.Visible = true;
            lblSuccess.Text = "Registro modificado correctamente";
            ((Parameters)Session["sessionParameters"]).Remove("ProyeccionCategoria.Guardado");
        }
        else if (((Parameters)Session["sessionParameters"]).Get("ProyeccionCategoria.Eliminado") != null)
        {
            lblSuccess.Visible = true;
            lblSuccess.Text = "Registro eliminado correctamente";
            ((Parameters)Session["sessionParameters"]).Remove("ProyeccionCategoria.Eliminado");
        }

        CargarGrilla(gvProyeccionCategoria, GridViewSortExpression, true);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Page.EnableEventValidation = false;
    }

    public string IdProyeccion
    {
        get
        {
            return hfIdProyeccion.Value;
        }

        set
        {
            hfIdProyeccion.Value = value;
        }
    }

    protected void btnMostrar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;
            if (dataItem != null)
            {
                SeleccionarRegistro(dataItem);
            }
        }
        catch (UserInterfaceException ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {            
            ((Parameters)Session["sessionParameters"]).Set("PestanaUserControl.Numero", "1");

            int rowIndex = pRow.RowIndex;
            string strValue = gvProyeccionCategoria.DataKeys[rowIndex].Value.ToString();
            ((Parameters)Session["sessionParameters"]).Set("ProyeccionCategoria.IdProyeccionCategoria", strValue);

            string ppidValue = hfIdProyeccion.Value;
            ((Parameters)Session["sessionParameters"]).Set("ProyeccionCategoria.IdProyeccionPresupuestos", ppidValue);            

            Page.Response.Redirect("~/Page/Cupos/ProyeccionCategoria/Detail.aspx");
            //SetSessionParameter("ProyeccionCategoria.NecesidadesDisponibles", necDisponibles);
            //NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void gvProyeccionCategoria_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProyeccionCategoria.SelectedRow);
    }
    protected void gvProyeccionCategoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProyeccionCategoria.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvProyeccionCategoria_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var myGridResults = vSIAService.ConsultarProyeccionCategorias(Convert.ToInt32(IdProyeccion), "CREA;RECH;APROB");

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(ProyeccionCategoria), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ProyeccionCategoria, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }
}