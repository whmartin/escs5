﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCategorias.ascx.cs" Inherits="Page_Cupos_UserControlModProyeecion_ucCategorias" %>

<asp:HiddenField ID="hfIdProyeccion" runat="server" />
<asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
<asp:HiddenField ID="txtCantidad" runat="server"></asp:HiddenField>
<asp:HiddenField ID="TabName" runat="server" />

<table width="90%" align="center">
    <tr>
        <td align="center">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label runat="server" ID="lblSuccess" Visible="false" CssClass="lbEM"></asp:Label>
        </td>
    </tr>
    <tr class="rowAG">
        <td>
            <asp:GridView runat="server" ID="gvProyeccionCategoria" AutoGenerateColumns="False" AllowPaging="True"
                GridLines="None" Width="100%" DataKeyNames="IDProyeccionCategoria, IdProyeccion" CellPadding="0" Height="16px"
                OnSorting="gvProyeccionCategoria_Sorting" AllowSorting="True"
                OnPageIndexChanging="gvProyeccionCategoria_PageIndexChanging" OnSelectedIndexChanged="gvProyeccionCategoria_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="SeleccionarRegistro" ImageUrl="~/Image/btn/info.jpg"
                                Height="16px" Width="16px" ToolTip="Detalle" OnClick="btnMostrar_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Categor&iacute;a" DataField="Descripcion" SortExpression="Descripcion" />
                    <asp:BoundField HeaderText="Nivel" DataField="Nivel" SortExpression="Nivel" />
                    <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" SortExpression="Cantidad" />
                    <asp:BoundField HeaderText="Regional" DataField="NombreRegional" SortExpression="NombreRegional" />
                    <asp:BoundField HeaderText="&Aacute;rea" DataField="NombreArea" SortExpression="NombreArea" />
                </Columns>
                <AlternatingRowStyle CssClass="rowBG" />
                <EmptyDataRowStyle CssClass="headerForm" />
                <HeaderStyle CssClass="headerForm" />
                <RowStyle CssClass="rowAG" />
            </asp:GridView>
        </td>
    </tr>
</table>
