﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucProyeccion.ascx.cs" Inherits="Page_Cupos_UserControlModProyeecion_ucProyeccion" %>

<script type="text/javascript" language="javascript">
    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        }
    }
    function helpOver(idImage) {
        document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
</script>
<asp:HiddenField ID="hfIdProyeccion" runat="server" />
<table width="90%" align="center">
    <tr>
        <td align="center">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label runat="server" ID="lblSuccess" Visible="false" CssClass="lbEM"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <asp:LinkButton ID="btnCargar" runat="server" ValidationGroup="btnCargar"
                OnClientClick="deshabilitar(this.id)" OnClick="btnEditar_Click" Visible="true">
                                        <img alt="Cargar Documento" src="~/Image/btn/edit.gif" title="Cargar" runat="server"/>                                       
            </asp:LinkButton>
            <%--<asp:LinkButton ID="btnGuardar" runat="server" ValidationGroup="btnGuardar"
                OnClientClick="deshabilitar(this.id)" OnClick="btnGuardar_Click" Visible="true">
                                        <img alt="Guardar" src="~/Image/btn/save.gif" title="Guardar" runat="server"/>                                       
            </asp:LinkButton>--%>
        </td>
    </tr>
    <tr class="rowB">
        <td>Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>
        <td>Vigencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="ddlIdVigencia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdVigencia" ControlToValidate="ddlIdVigencia"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:DropDownList runat="server" ID="ddlIdRegional" AutoPostBack="true" Enabled="false"></asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlIdVigencia" Enabled="false"></asp:DropDownList>
        </td>
    </tr>
    <tr class="rowB">
        <td>Área *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdArea" ControlToValidate="ddlIdArea"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdArea" ControlToValidate="ddlIdArea"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>
        <td>Recurso *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRecurso" ControlToValidate="ddlIdRecurso"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdRecurso" ControlToValidate="ddlIdRecurso"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>


    </tr>
    <tr class="rowA">
        <td>
            <asp:DropDownList runat="server" ID="ddlIdArea" Enabled="false"></asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlIdRecurso" Enabled="false"></asp:DropDownList>
        </td>

    </tr>
    <tr class="rowB">
        <td colspan="2">Rubro *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRubro" ControlToValidate="ddlIdRubro"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdRubro" ControlToValidate="ddlIdRubro"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>
    </tr>
    <tr class="rowA">
        <td colspan="2">
            <asp:DropDownList runat="server" ID="ddlIdRubro" Enabled="false"></asp:DropDownList>
        </td>
    </tr>
    <tr class="rowB">
        <td>Valor Necesidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorCupo" ControlToValidate="txtValorCupo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
        <td>Total Necesidades *
                <asp:RequiredFieldValidator runat="server" ID="rfvTotalCupos" ControlToValidate="txtTotalCupos"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox runat="server" ID="txtValorCupo" MaxLength="26" data-thousands="." data-decimal="," Enabled="false"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftValorCupo" runat="server" TargetControlID="txtValorCupo"
                FilterType="Numbers,Custom" ValidChars=",." />
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtTotalCupos" MaxLength="4" Enabled="false"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftTotalCupos" runat="server" TargetControlID="txtTotalCupos"
                FilterType="Numbers" />
        </td>
    </tr>
</table>

<script type="text/javascript">
    $("#<%= txtValorCupo.ClientID %>").maskMoney();
</script>
