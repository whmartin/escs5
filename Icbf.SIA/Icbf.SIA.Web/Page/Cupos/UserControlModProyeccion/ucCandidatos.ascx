﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCandidatos.ascx.cs" Inherits="Page_Cupos_UserControlModProyeecion_ucCandidatos" %>

<asp:HiddenField ID="hfIdProyeccion" runat="server" />
<asp:HiddenField ID="TabName" runat="server" />

<asp:Panel runat="server" ID="pnlLista">
    <table width="90%" align="center">
        <tr>
            <td align="center">
                <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label runat="server" ID="lblSuccess" Visible="false" CssClass="lbEM"></asp:Label>
            </td>
        </tr>
        <tr class="rowAG">
            <td>
                <asp:GridView runat="server" ID="gvListCupo" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" DataKeyNames="IdCupoArea" CellPadding="0" Height="16px"
                    OnSorting="gvListCupo_Sorting" AllowSorting="True"
                    OnPageIndexChanging="gvListCupo_PageIndexChanging" OnSelectedIndexChanged="gvListCupo_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnInfo" runat="server" CommandName="SeleccionarRegistro" ImageUrl="~/Image/btn/info.jpg"
                                    Height="16px" Width="16px" ToolTip="Detalle" OnClick="btnMostrar_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Consecutivo Interno" DataField="ConsecutivoInterno" SortExpression="ConsecutivoInterno" />
                        <asp:BoundField HeaderText="Proveedor" DataField="NombreTercero" SortExpression="NombreTercero" />

                        <asp:TemplateField HeaderText="Fecha Ingreso ICBF" SortExpression="FechaIngresoICBF">
                            <ItemTemplate>
                                <asp:Label ID="lblFechaIngresoICBF" runat="server" Text='<%#  Convert.ToString(Eval("FechaIngresoICBF")) == "1900-01-01 00:00:00.000"   ? Convert.ToString(Eval("FechaIngresoICBF")) : "" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Categoría" DataField="CategoriaEmpleados" SortExpression="CategoriaEmpleados" />
                        <asp:BoundField HeaderText="Nivel" DataField="Nivel" SortExpression="Nivel" />
                        <asp:BoundField HeaderText="Honorario Base" DataField="HonorarioBase" DataFormatString="{0:C2}" SortExpression="HonorarioBase" />
                        <asp:BoundField HeaderText="IVA Honorario" DataField="IvaHonorario" DataFormatString="{0:C2}" SortExpression="IvaHonorario" />
                        <%-- <asp:BoundField HeaderText="Porcentaje IVA" DataField="PorcentajeIVA" SortExpression="PorcentajeIVA" />--%>
                        <asp:BoundField HeaderText="Tiempo Proyectado Meses" DataField="TiempoProyectadoMeses" SortExpression="TiempoProyectadoMeses" />
                        <asp:BoundField HeaderText="Tiempo Proyectado Días" DataField="TiempoProyectadoDias" SortExpression="TiempoProyectadoDias" />
                        <%--<asp:TemplateField HeaderText="Aprobado" SortExpression="Aprobado">
                                <ItemTemplate>
                                    <asp:Label ID="lblAprobado" runat="server" Text='<%# (bool) Eval("Aprobado") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        
                        <asp:BoundField HeaderText="Total Honorarios Proyectado" DataField="TotalHonorarioTiempoProyectado"  DataFormatString="{0:C2}" 
                            SortExpression="TotalHonorarioTiempoProyectado" />
                        <asp:BoundField HeaderText="Usuario Aprobó" DataField="UsuarioAprobo" SortExpression="UsuarioAprobo" />
                        <asp:TemplateField HeaderText="Fecha Aprobación" SortExpression="FechaAprobacion" >
                            <ItemTemplate>
                                <asp:Label ID="lblFechaAprobacion" runat="server" Text='<%#  Convert.ToString(Eval("FechaAprobacion")) == "1900-01-01 00:00:00.000"   ? "" :  string.Format("{0:d}", Convert.ToDateTime(Eval("FechaAprobacion"))) %>' DataFormatString="{0:d}" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
