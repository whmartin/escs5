<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Objeto_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIdObjetoCupos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Titulo Objeto *
                <asp:RequiredFieldValidator runat="server" ID="rfvTitulo" ControlToValidate="txtTitulo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTitulo" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTitulo" runat="server" TargetControlID="txtTitulo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Objeto Prestaci&oacute;n de Servicio *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionObjeto" ControlToValidate="txtDescripcionObjeto"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionObjeto" TextMode="MultiLine" Width="80%" Height="150px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionObjeto" runat="server" TargetControlID="txtDescripcionObjeto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                
            </td>
        </tr>
    </table>
</asp:Content>
