<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Objeto_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdObjetoCupos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Titulo Objeto
            </td>
            <td>
                Estado                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTitulo"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Objeto Prestación de Servicio
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionObjeto" TextMode="MultiLine" Enabled="false" Width="80%" Height="150px"></asp:TextBox>
                
            </td>
        </tr>
    </table>
</asp:Content>
