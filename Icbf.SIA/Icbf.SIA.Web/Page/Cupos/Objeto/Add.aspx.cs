using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad Objeto
/// </summary>
public partial class Page_Objeto_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    string PageName = "Cupos/Objeto";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad Objeto
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Objeto vObjeto = new Objeto();

            vObjeto.Titulo = Convert.ToString(txtTitulo.Text);
            vObjeto.DescripcionObjeto = Convert.ToString(txtDescripcionObjeto.Text);
            vObjeto.Estado = Convert.ToBoolean(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vObjeto.IdObjetoCupos = Convert.ToInt32(hfIdObjetoCupos.Value);
                vObjeto.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vObjeto, this.PageName, vSolutionPage);
                vResultado = vSIAService.ModificarObjeto(vObjeto);
            }
            else
            {
                vObjeto.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vObjeto, this.PageName, vSolutionPage);
                vResultado = vSIAService.InsertarObjeto(vObjeto);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Objeto.IdObjetoCupos", vObjeto.IdObjetoCupos);
                SetSessionParameter("Objeto.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Objeto", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdObjetoCupos = Convert.ToInt32(GetSessionParameter("Objeto.IdObjetoCupos"));
            RemoveSessionParameter("Objeto.Id");

            Objeto vObjeto = new Objeto();
            vObjeto = vSIAService.ConsultarObjeto(vIdObjetoCupos);
            hfIdObjetoCupos.Value = vObjeto.IdObjetoCupos.ToString();
            txtTitulo.Text = vObjeto.Titulo;
            txtDescripcionObjeto.Text = vObjeto.DescripcionObjeto;
            rblEstado.SelectedValue = vObjeto.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vObjeto.UsuarioCrea, vObjeto.FechaCrea, vObjeto.UsuarioModifica, vObjeto.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "true"));
            rblEstado.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblEstado.SelectedValue = "true";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
