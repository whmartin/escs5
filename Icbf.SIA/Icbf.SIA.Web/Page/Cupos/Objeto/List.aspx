<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Objeto_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Titulo Objeto
            </td>
            <td class="Cell">
                Objeto Prestaci&oacute;n de Servicio
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtTitulo" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTitulo" runat="server" TargetControlID="txtTitulo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtDescripcionObjeto" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionObjeto" runat="server" TargetControlID="txtDescripcionObjeto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td  class="Cell" colspan="2">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObjeto" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdObjetoCupos" CellPadding="0" Height="16px"
                        OnSorting="gvObjeto_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvObjeto_PageIndexChanging" OnSelectedIndexChanged="gvObjeto_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Titulo Objeto" DataField="Titulo"  SortExpression="Titulo"/>
                            <asp:BoundField HeaderText="Objeto Prestación de Servicio" DataField="DescripcionObjeto"  SortExpression="DescripcionObjeto"/>
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">  
                                 <ItemTemplate> 
                                     <asp:Label ID="lblEstado"  runat="server" Text='<%# (bool) Eval("Estado") ? "Activo" : "Inactivo" %>'></asp:Label> 
                                 </ItemTemplate> 
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
