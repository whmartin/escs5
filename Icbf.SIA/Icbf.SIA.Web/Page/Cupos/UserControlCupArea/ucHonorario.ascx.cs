﻿using Icbf.SIA.Service;
using Icbf.Utilities.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Cupos_UserControlCupArea_ucHonorario : System.Web.UI.UserControl
{
    #region Enabled Objetos
    SIAService vSIAService = new SIAService();

    public void ModoConsulta(Boolean pEstado)
    {
        txtFechaInicialProyectado.Enabled = pEstado;
        txtFechaFinalProyectado.Enabled = pEstado;
        //txtFechaIngresoICBF.Enabled = pEstado;
        ddlCategoriaValores.Enabled = pEstado;
        ddlCategoriaEmpleado.Enabled = pEstado;
        txtHonorarioBase.Enabled = pEstado;
        btnCalcular.Enabled = pEstado;
    }

    #endregion
    public object ddlCategoriaEmpleadoSource
    {
        set
        {
            ddlCategoriaEmpleado.DataSource = value;
            ddlCategoriaEmpleado.DataValueField = "IdCategoria";
            ddlCategoriaEmpleado.DataTextField = "Descripcion";
            ddlCategoriaEmpleado.DataBind();
            ddlCategoriaEmpleado.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public object ddlCategoriaValoresSource
    {
        set
        {
            ddlCategoriaValores.DataSource = value;
            ddlCategoriaValores.DataTextField = "Nivel";
            ddlCategoriaValores.DataValueField = "IdCategoriaValor";
            ddlCategoriaValores.DataBind();
            ddlCategoriaValores.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string txtVigencia
    {
        set
        {
            HfdidVigencia.Value = value;
        }
        get
        {
            return HfdidVigencia.Value;
        }
    }

    public string ddlCategoriaValoresValue
    {
        get
        {
            return ddlCategoriaValores.SelectedValue;
        }
        set
        {
            ddlCategoriaValores.SelectedValue = value;
        }
    }

    public DateTime FechaInicialProyectado
    {
        get
        {
            return txtFechaInicialProyectado.Date;
        }
        set
        {
            txtFechaInicialProyectado.Date = value;
        }

    }

    public DateTime FechaFinalProyectado
    {
        get
        {
            return txtFechaFinalProyectado.Date;
        }
        set
        {
            txtFechaFinalProyectado.Date = value;
        }

    }

    //private int _IdProyeccion;
    public string IdProyeccion
    {
        get { return HfdidProyeccion.Value; }
        set { HfdidProyeccion.Value = value; }
    }


    public string ddlCategoriaEmpleadoValue
    {
        get
        {
            return ddlCategoriaEmpleado.SelectedValue;
        }
        set
        {
            ddlCategoriaEmpleado.SelectedValue = value;
        }
    }
    //public string Nivel {
    //    get
    //    {
    //        return txtNivel.Text;
    //    }
    //    set
    //    {
    //        txtNivel.Text = value;
    //    }
    //}

    public string HonorarioBase
    {
        get
        {
            return txtHonorarioBase.Text;
        }
        set
        {
            txtHonorarioBase.Text = value;
        }
    }

    public string IvaHonorario
    {
        get
        {
            return txtIvaHonorario.Text;
        }
        set
        {
            txtIvaHonorario.Text = value;
        }
    }

    public string PorIvaHonorario
    {
        get
        {
            return hfPorIvaHonorario.Value;
        }
        set
        {
            hfPorIvaHonorario.Value = value;
        }
    }

    public string TotalHonorario
    {
        get
        {
            return txtTotalProyectado.Text;
        }
        set
        {
            txtTotalProyectado.Text = value;
        }
    }

    public string TiempoProyectadoMese
    {
        get
        {
            return txtTiempoProyectadoMeses.Text;
        }
        set
        {
            txtTiempoProyectadoMeses.Text = value;
        }
    }
    public string TiempoProyectadoDias
    {
        get
        {
            return txtTiempoProyectadoDias.Text;
        }
        set
        {
            txtTiempoProyectadoDias.Text = value;
        }
    }
    public string TiempoProyectadoAnio
    {
        get
        {
            return txtTiempoProyectadoAnio.Text;
        }
        set
        {
            txtTiempoProyectadoAnio.Text = value;
        }
    }

    public string TotalHonorarioTiempo
    {
        get
        {
            return txtTotalHonorarioTiempo.Text;
        }
        set
        {
            txtTotalHonorarioTiempo.Text = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCalcular_Click(object sender, EventArgs e)
    {
        CalcularTotalesHonorario();
    }

    public void CalcularTotalesHonorario()
    {
        try
        {
            //ValidacionesPantalla();

            lblError.Visible = false;
            lblError.Text = "";
            //if (((Parameters)Session["sessionParameters"]).Get("CupoAreas.IvaProveedorSelec") != null)
            //{
            //    if (!string.IsNullOrEmpty(((Parameters)Session["sessionParameters"]).Get("CupoAreas.IvaProveedorSelec").ToString()))
            //    {
            //        PorIvaHonorario = ((Parameters)Session["sessionParameters"]).Get("CupoAreas.IvaProveedorSelec").ToString();
            //        ((Parameters)Session["sessionParameters"]).Remove("CupoAreas.IvaProveedorSelec");
            //    }
            //}
            //else
            //{
            //    if (String.IsNullOrEmpty(PorIvaHonorario))
            //        throw new Exception("Selecione un Tercero Para Continuar");
            //}           

            if (string.IsNullOrEmpty(txtHonorarioBase.Text))
                throw new Exception("Debe digitar Un Honorario Base");

            if (txtFechaFinalProyectado.Date.ToString("yyyyMMdd") == "19000101")
                throw new Exception("Debe digitar Una Fecha Final Proyectado");

            if (txtFechaInicialProyectado.Date.ToString("yyyyMMdd") == "19000101")
                throw new Exception("Debe digitar Una Fecha Inicial Proyectado");

            ValidacionesPantalla();

            TimeSpan ts = Convert.ToDateTime(txtFechaFinalProyectado.Date) - Convert.ToDateTime(txtFechaInicialProyectado.Date);
            int pDias = 0, pMeses = 0, pAnios = 0;
            //txtTiempoProyectadoDias.Text = ts.Days.ToString();
            //txtTiempoProyectadoMeses.Text = Convert.ToString(Math.Round(ts.TotalDays / 30, 0));
            CalcularDiferenciaFEchas(Convert.ToDateTime(txtFechaInicialProyectado.Date), Convert.ToDateTime(txtFechaFinalProyectado.Date), ref pDias, ref pMeses, ref pAnios);
            txtTiempoProyectadoDias.Text = pDias.ToString();
            txtTiempoProyectadoMeses.Text = pMeses.ToString();
            txtTiempoProyectadoAnio.Text = pAnios.ToString();


            //decimal vPorcenIva = Convert.ToDecimal(PorIvaHonorario);
            //decimal vIvaHonorario = Convert.ToDecimal(txtHonorarioBase.Text)* (vPorcenIva / 100);
            txtIvaHonorario.Text = "0"; // vIvaHonorario.ToString("C");
            decimal vTotalPRoyectado = Convert.ToDecimal(txtHonorarioBase.Text);// + vIvaHonorario;
            txtTotalProyectado.Text = vTotalPRoyectado.ToString("C");


            txtTotalHonorarioTiempo.Text = ((vTotalPRoyectado * (pAnios * 12)) + (vTotalPRoyectado * pMeses) + ((vTotalPRoyectado / 30) * pDias)).ToString("C");

            
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    public void CalcularDiferenciaFEchas(DateTime FEchaInicio, DateTime FechaFinal, ref int pDias, ref int pMeses, ref int pAnios)
    {
        DateTime fi = FEchaInicio;
        DateTime ft = FechaFinal;

        //int AntDias = 0, AntMeses = 0, AntAños = 0;

        int DiasFI = fi.Day, MesFI = fi.Month, AñoFI = fi.Year;
        int DiasFT = ft.Day, MesFT = ft.Month, AñoFT = ft.Year;

        if (DiasFI == 31)
            DiasFI = 30;

        if (DiasFT == 31)
            DiasFT = 30;

        if (DiasFT < DiasFI)
        {
            DiasFT += 30;
            MesFT -= 1;
        }

        // calcula dias de antiguedad
        pDias = (DiasFT - DiasFI) + 1;

        if (MesFT < MesFI)
        {
            MesFT += 12;
            AñoFT -= 1;
        }

        // calcula meses de antiguedad
        pMeses = MesFT - MesFI;

        // calcula años antiguedad
        pAnios = AñoFT - AñoFI;

    }
    public void ValidacionesPantalla()
    {
        if (txtFechaInicialProyectado.Date > txtFechaFinalProyectado.Date)
        {
            throw new Exception("La Fecha Inicial de La Proyeccion No Puede Ser Mayor Que La Final");
        }

        //if (txtFechaIngresoICBF.Date > txtFechaInicialProyectado.Date)
        //{
        //    throw new Exception("La Fecha De Ingreso al ICBF No puede Ser Mayor Que La Fecha Inicial Proyectado");
        //}

        if (ddlCategoriaValores.SelectedValue == "-1")
        {
            throw new Exception("Por favor Seleccione un Nivel");
        }

        if (txtVigencia == string.Empty)
        {
            throw new Exception("No Se Ha Seleccionado una vigencia");
        }
        
        int nivelSelc = Convert.ToInt32(ddlCategoriaValores.SelectedValue);

        var categoriaValor = vSIAService.ConsultarCategoriaValores(nivelSelc);

        decimal valorMinimo = categoriaValor.ValorMinimo;
        decimal valorMaximo = categoriaValor.ValorMaximo;
        decimal valorEsrito = 0;

        if (!string.IsNullOrEmpty(txtHonorarioBase.Text))
            valorEsrito = Convert.ToDecimal(txtHonorarioBase.Text);

        if(valorEsrito > valorMaximo)
        {
            throw new Exception("El Valor Del Honorario Base Mensual Digitado Supera El Valor Permitido.");
        }

        if (valorEsrito < valorMinimo)
        {
            throw new Exception("El Valor Del Honorario Base Mensual Digitado Es Inferior Al Valor Permitido.");
        }
        //var vHonorariosTercero = vSIAService.ConsultarCategoriaValoress(Convert.ToInt32(txtNivel.Text),Convert.ToInt32(txtVigencia), true).Take(1).SingleOrDefault();

        //if(vHonorariosTercero == null)
        //{
        //    throw new Exception("No se encunetra el nivel digitado para la vigencia de la proyeccion");
        //}

        //if (txtHonorarioBase.Text == string.Empty)
        //{
        //    throw new Exception("Por favor digite el valor Base del Honorario");
        //}

        //if(Convert.ToDecimal(txtHonorarioBase.Text) > vHonorariosTercero.ValorMaximo)
        //{
        //    throw new Exception("El Valor Del Honorario Supera El Permitido en la Vigencia");
        //}

        //if (Convert.ToDecimal(txtHonorarioBase.Text) < vHonorariosTercero.ValorMinimo)
        //{
        //    throw new Exception("El Valor Del Honorario Es Inferior a Lo Permitido En La Vigencia");
        //}
    }

    protected void ddlCategoriaEmpleado_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlCategoriaValores.DataSource = null;
            ddlCategoriaValores.DataBind();

            lblError.Visible = false;
            lblError.Text = "";

            int vIdCategoriaEmpleados;

            vIdCategoriaEmpleados = Convert.ToInt32(ddlCategoriaEmpleado.SelectedValue);

            int vidproyeccion = Convert.ToInt32(IdProyeccion);

            ddlCategoriaValores.DataSource = vSIAService.ConsultarCategoriaValoresAsignada(vidproyeccion, vIdCategoriaEmpleados);
            ddlCategoriaValores.DataTextField = "Nivel";
            ddlCategoriaValores.DataValueField = "IdCategoriaValor";
            ddlCategoriaValores.DataBind();
            ddlCategoriaValores.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void ddlCategoriaValores_SelectedIndexChanged(object sender, EventArgs e)
    {
        CalcularTotalesHonorario();
    }
}
