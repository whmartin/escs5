﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucHonorario.ascx.cs" Inherits="Page_Cupos_UserControlCupArea_ucHonorario" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
<link type="text/css" href="../../../Styles/direccion.css" rel="Stylesheet" />

<asp:HiddenField ID="hfPorIvaHonorario" runat="server" />
<asp:HiddenField ID="HfdidVigencia" runat="server" />
<asp:HiddenField ID="HfdidProyeccion" runat="server" />
<table width="90%" align="center">
    <tr>
        <td align="center" colspan="2">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>

     <tr class="rowB">
        <td>Fecha Inicial Proyectado *
              <%--  <asp:RequiredFieldValidator runat="server" ID="rfvFechaInicialProyectado" 
                    ControlToValidate="txtFechaInicialProyectado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>--%>
        </td>
        <td>Fecha Final Proyectado *
               <%-- <asp:RequiredFieldValidator runat="server" ID="rfvFechaFinalProyectado" 
                    ControlToValidate="txtFechaFinalProyectado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <%--<asp:TextBox runat="server" ID="txtFechaInicialProyectado"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFechaInicialProyectado"
                FilterType="Numbers" ValidChars="" />--%>
            <uc1:fecha ID="txtFechaInicialProyectado" runat="server" Requerid="true" />
        </td>
        <td>
            <%--<asp:TextBox runat="server" ID="txtFechaFinalProyectado" OnTextChanged="txtFechaFinalProyectado_TextChanged"></asp:TextBox>--%>
            <uc1:fecha ID="txtFechaFinalProyectado" runat="server" Requerid="true" />

        </td>
    </tr>

    <tr class="rowB">

       
        <td>Categoría/Perfil *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaValores" ControlToValidate="ddlCategoriaEmpleado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdCategoriaValores" ControlToValidate="ddlCategoriaEmpleado"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>
         <td>
            Honorario Base *
                <asp:RequiredFieldValidator runat="server" ID="rfvHonorarioBase" ControlToValidate="txtHonorarioBase"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr class="rowA">

        <td>
            <asp:DropDownList runat="server" ID="ddlCategoriaEmpleado" OnSelectedIndexChanged="ddlCategoriaEmpleado_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
        </td>
        
        <td>
             
            <asp:TextBox runat="server" ID="txtHonorarioBase" MaxLength="26" data-thousands="." data-decimal=","></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftHonorarioBase" runat="server" TargetControlID="txtHonorarioBase"
                FilterType="Numbers,Custom" ValidChars=".," />
              <asp:LinkButton ID="btnCalcular" runat="server" ValidationGroup="btnCalcular" OnClick="btnCalcular_Click">
                  <img alt="Calcular" src="../../../Image/btn/calculate.png" title="Calcular" Width="17px" />
            </asp:LinkButton>
           
        </td>
    </tr>

    <tr class="rowB">
        <td>
            Nivel *
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlCategoriaValores"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlCategoriaValores"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>

                   <%--<asp:RequiredFieldValidator runat="server" ID="rfvNivel" ControlToValidate="txtNivel"
                       SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                       ForeColor="Red"></asp:RequiredFieldValidator>--%>
        </td>
        <td>
              IVA Honorario *
                <asp:RequiredFieldValidator runat="server" ID="rfvIvaHonorario" ControlToValidate="txtIvaHonorario"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr class="rowA">
        <td>
           <asp:DropDownList runat="server" ID="ddlCategoriaValores" 
                OnSelectedIndexChanged="ddlCategoriaValores_SelectedIndexChanged"
                AutoPostBack="true"></asp:DropDownList>
             <%-- <asp:TextBox runat="server" ID="txtNivel" MaxLength="0"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="fteValNivel" runat="server" TargetControlID="txtNivel"
                FilterType="Numbers" ValidChars=".," />--%>
         </td>
        
        <td>
           <asp:TextBox runat="server" ID="txtIvaHonorario" MaxLength="0" Enabled="false"></asp:TextBox>
            <%--<Ajax:FilteredTextBoxExtender ID="ftIvaHonorario" runat="server" TargetControlID="txtIvaHonorario"
                FilterType="Numbers" ValidChars=".," />--%>
        </td>
    </tr>
    <tr class="rowB">
        <td>Total Honorario Proyectado *
                <asp:RequiredFieldValidator runat="server" ID="rfvPorcentajeIVA" ControlToValidate="txtTotalProyectado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
        <td>
             Tiempo Proyectado en Años *
        </td>
    </tr>
    <tr class="rowA">
        <td>
              <asp:TextBox runat="server" ID="txtTotalProyectado" MaxLength="0" Enabled="false"></asp:TextBox>
            <%--<Ajax:FilteredTextBoxExtender ID="ftPorcentajeIVA" runat="server" TargetControlID="txtTotalProyectado"
                FilterType="Numbers" ValidChars=".," />--%>
        </td>
        <td>
         <asp:TextBox runat="server" ID="txtTiempoProyectadoAnio" Enabled="false"></asp:TextBox>
        </td>
    </tr>
   
    <tr class="rowB">
        <td>
            Tiempo Proyectado Meses *
        </td>
        <td>
           Tiempo Proyectado Días *      
        </td>
    </tr>
    <tr class="rowA">
        
        <td>
            <asp:TextBox runat="server" ID="txtTiempoProyectadoMeses" Enabled="false"></asp:TextBox>
        </td>
        <td>            
            <asp:TextBox runat="server" ID="txtTiempoProyectadoDias" Enabled="false"></asp:TextBox>
        </td>
    </tr>
     <tr class="rowB">
        <td>
            Total Honorario Por Tiempo Proyectado                
        </td>
        <td>
         
        </td>
    </tr>
    <tr class="rowA">
        <td>
             <asp:TextBox runat="server" ID="txtTotalHonorarioTiempo" Enabled="false"></asp:TextBox>
        </td>
        <td>
          
        </td>
    </tr>
</table>
<script src="../../../Scripts/jquery.maskMoney.js"></script>

<script type="text/javascript">
    $("#<%= txtHonorarioBase.ClientID %>").maskMoney();
</script>
