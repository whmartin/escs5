﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs"
    Inherits="Page_Cupos_AprobacionCupoNecesidad_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">Regional *
                     <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaEmpleados" ControlToValidate="ddlRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdCategoriaEmpleados" ControlToValidate="ddlRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td> 
                <td class="Cell">Vigencia *
                  <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>                
            </tr>
            <tr class="rowA">
                 <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlRegional" OnSelectedIndexChanged="ddlRegional_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
                </td>             
            </tr>
            <tr class="rowB">
                <td class="Cell">Área
                </td>
                <td class="Cell"></td>
            </tr>
            <tr class="rowA">
                  <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdArea"></asp:DropDownList>
                </td>
                <td class="Cell"></td>
            </tr>

        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAprobacionPresupuestos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdProyeccionPresupuestos" CellPadding="0" Height="16px"
                        OnSorting="gvAprobacionPresupuestos_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvAprobacionPresupuestos_PageIndexChanging" OnSelectedIndexChanged="gvAprobacionPresupuestos_SelectedIndexChanged">
                        <columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Vigencia" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Área" DataField="DescrArea"  SortExpression="DescrArea"/>
                            <asp:BoundField HeaderText="Valor Necesidad" DataField="ValorCupo"  DataFormatString="{0:C2}" SortExpression="ValorCupo"/>
                            <asp:BoundField HeaderText="Total Necesidades" DataField="TotalCupos"   DataFormatString="{0:N2}" SortExpression="TotalCupos"/>
                           <%-- <asp:TemplateField HeaderText="Aprobado" SortExpression="Aprobado">  
                                 <ItemTemplate> 
                                     <asp:Label ID="lblAprobado"  runat="server" Text='<%# (bool) Eval("Aprobado") ? "Activo" : "Inactivo" %>'></asp:Label> 
                                 </ItemTemplate> 
                            </asp:TemplateField>--%>
                            <asp:BoundField HeaderText="Usuario Aprobó" DataField="UsuarioAprobo"  SortExpression="UsuarioAprobo"/>
                            <asp:BoundField HeaderText="Fecha Aprobación" DataField="FechaAprobacion"  DataFormatString="{0:d}" SortExpression="FechaAprobacion"/>
                        </columns>
                        <alternatingrowstyle cssclass="rowBG" />
                        <emptydatarowstyle cssclass="headerForm" />
                        <headerstyle cssclass="headerForm" />
                        <rowstyle cssclass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
