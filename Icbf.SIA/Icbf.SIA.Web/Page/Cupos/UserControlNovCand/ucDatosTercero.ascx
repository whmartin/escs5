﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDatosTercero.ascx.cs" Inherits="Page_Cupos_UserControlCupArea_ucDatosTercero" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
<link type="text/css" href="../../../Styles/direccion.css" rel="Stylesheet" />
<asp:HiddenField ID="hfPostbck" runat="server" />
<asp:HiddenField ID="hfPorIva" runat="server" />
<table width="90%" align="center">
    <tr>
        <td align="center">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>

    <tr class="rowB">
        <td>Proveedor *
           <%-- <asp:RequiredFieldValidator runat="server" ID="rfvProveedor" ControlToValidate="txtIdentificacionProveedor"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                ForeColor="Red"></asp:RequiredFieldValidator>--%>
        </td>
        <td>Tipo Documento
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:HiddenField ID="hfIdEntidad" runat="server" />
            <asp:TextBox runat="server" ID="txtIdentificacionProveedor" Enabled="false" Width="90%"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftIdProveedor" runat="server" TargetControlID="txtIdentificacionProveedor"
                FilterType="Numbers" ValidChars="" />
            <asp:ImageButton ID="imgNombreSolicitante" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                OnClientClick="GetContratista(); return false;" Style="cursor: hand" ToolTip="Buscar" />

        </td>
        <td>
            <asp:TextBox ID="txtTipoDocumento" runat="Server" Enabled="false" Width="90%"></asp:TextBox>
        </td>
    </tr>
    <tr class="rowB">
        <td>Nombres
        </td>
        <td>Apellidos
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox ID="txtNombre" runat="Server" Enabled="false" Width="90%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="txtApellidos" runat="Server" Enabled="false" Width="90%"></asp:TextBox>
        </td>
    </tr>

    <tr class="rowB">
        <td>Profesi&oacute;n
        </td>
        <td>Modalidad Educativa
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox ID="txtProfesion" runat="Server" Enabled="false" Width="90%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="txtModalidadEducativa" runat="Server" Enabled="false" Width="90%"></asp:TextBox>
        </td>
    </tr>

    <tr class="rowB">
        <td>Regimen
        </td>
        <td>
            Fecha Ingreso ICBF Primera Vez*
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox ID="txtRegimen" runat="Server" Enabled="false" Width="90%"></asp:TextBox>
        </td>
        <td>
             <uc1:fecha ID="txtFechaIngresoICBF" runat="server" Requerid="false" Enabled="false"/>
        </td>
    </tr>

    <tr class="rowB">
        <td colspan="2">
            Otros Estudios
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:GridView runat="server" ID="gvOtrosEstudios" AutoGenerateColumns="False"
                        GridLines="None" Width="100%" DataKeyNames="IdFormacion" CellPadding="0" 
                        Height="16px">
                        <Columns>                           
                            <asp:BoundField HeaderText="ModalidadAcademica" DataField="ModalidadAcademica"/>
                            <asp:BoundField HeaderText="Profesion" DataField="Profesion"  SortExpression="Profesion"/>                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
        </td>
    </tr>
</table>

<script type="text/javascript" language="javascript">

    function muestraImagenLoading() {
        var imgLoading = document.getElementById("imgLoading");
        imgLoading.style.visibility = "visible";
    }

    function ocultaImagenLoading() {
        var imgLoading = document.getElementById("imgLoading");
        imgLoading.style.visibility = "";
    }

    function GetContratista() {
        muestraImagenLoading();
        window_showModalDialog('../../../Page/Cupos/LupaProveedor/LupaProveedoresCupos.aspx', setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }
    function setReturnGetContratista(dialog) {

        var pObj = window_returnModalDialog(dialog);
        if (pObj != undefined && pObj != null) {

            var retLupa = pObj.split(";");
            if (retLupa.length > 0) {
                document.getElementById('<%= hfIdEntidad.ClientID %>').value = retLupa[0];

                prePostbck(false);
                __doPostBack('<%= hfIdEntidad.ClientID %>', '');
                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }

          <%--  prePostbck(false);
            __doPostBack('<%= txtIdProveedor.ClientID %>', '');--%>
    }

    function prePostbck(imagenLoading) {
        if (!window.Page_IsValid) {
            __doPostBack('<%= hfPostbck.ClientID %>', '');
        }

        if (imagenLoading == true)
            muestraImagenLoading();
    }
</script>
