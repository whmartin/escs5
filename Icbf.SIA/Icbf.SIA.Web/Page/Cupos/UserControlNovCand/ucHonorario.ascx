﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucHonorario.ascx.cs" Inherits="Page_Cupos_UserControlNovCand_ucHonorario" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
<link type="text/css" href="../../../Styles/direccion.css" rel="Stylesheet" />

<asp:HiddenField ID="hfPorIvaHonorario" runat="server" />
<asp:HiddenField ID="HfdidVigencia" runat="server" />
<asp:HiddenField ID="HfdidProyeccion" runat="server" />
<asp:HiddenField ID="HfIdCupo" runat="server" />
<asp:HiddenField ID="hfidProyeccionPresupuesto" runat="server" />
<asp:HiddenField ID="hfUsuarioActual" runat="server" />

<asp:table ID="tbHabilitarEdiciones" runat="server" width="90%" align="center">
    <asp:TableRow>
        <asp:TableCell>
            <asp:CheckBox ID="cbAdiciones" runat="server" Text="Adiciones" AutoPostBack="true"  oncheckedchanged="cbAdiciones_CheckedChanged" />

        </asp:TableCell>
        <asp:TableCell>
            <asp:CheckBox ID="cbProrrogas" runat="server" Text="Prorrogas" AutoPostBack="true" oncheckedchanged="cbProrrogas_CheckedChanged" />
        </asp:TableCell>

    </asp:TableRow>
    </asp:table>

<asp:table ID="tbFechasProrroga" runat="server" width="90%" align="center" Visible="false">
    <asp:TableRow class="rowB">
        <asp:TableCell >
            Fecha Inicial Prorroga *
        </asp:TableCell>
        <asp:TableCell>
            Fecha Final Prorroga *
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow class="rowA">
        <asp:TableCell>
           <uc1:fecha ID="txtFechaInicialProrroga" runat="server" Enable="false" Requerid="true" />
        </asp:TableCell>
        <asp:TableCell>
            <uc1:fecha ID="txtFechaFinalProrroga" runat="server" Enable="false"  Requerid="true" />
        </asp:TableCell>
    </asp:TableRow>
</asp:table>

    <table width="90%" align="center">

    <tr>
        <td align="center" colspan="2">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>
     <tr class="rowB">
        <td>Fecha Inicial Proyectado *
        </td>
        <td>Fecha Final Proyectado *
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <uc1:fecha ID="txtFechaInicialProyectado" runat="server" Enable="false" Requerid="true" />
        </td>
        <td>
            <uc1:fecha ID="txtFechaFinalProyectado" runat="server" Enable="false"  Requerid="true" />

        </td>
    </tr>

    <tr class="rowB">

       
        <td>Categoría/Perfil *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaValores" ControlToValidate="ddlCategoriaEmpleado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdCategoriaValores" ControlToValidate="ddlCategoriaEmpleado"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
        </td>
         <td>
            Honorario Base *
                <asp:RequiredFieldValidator runat="server" ID="rfvHonorarioBase" ControlToValidate="txtHonorarioBase"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr class="rowA">

        <td>
            <asp:DropDownList runat="server" ID="ddlCategoriaEmpleado" Enable="false"  OnSelectedIndexChanged="ddlCategoriaEmpleado_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
        </td>
        
        <td>
             
            <asp:TextBox runat="server" ID="txtHonorarioBase" MaxLength="26" Enable="false"  data-thousands="." data-decimal=","></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftHonorarioBase" runat="server" TargetControlID="txtHonorarioBase"
                FilterType="Numbers,Custom" ValidChars=".," />
              <asp:LinkButton ID="btnCalcular" runat="server" ValidationGroup="btnCalcular" OnClick="btnCalcular_Click">
                  <img alt="Calcular" src="../../../Image/btn/calculate.png" title="Calcular" Width="17px" />
            </asp:LinkButton>
           
        </td>
    </tr>

    <tr class="rowB">
        <td>
            Nivel *
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" Enable="false"  ControlToValidate="ddlCategoriaValores"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlCategoriaValores"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>

                   <%--<asp:RequiredFieldValidator runat="server" ID="rfvNivel" ControlToValidate="txtNivel"
                       SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                       ForeColor="Red"></asp:RequiredFieldValidator>--%>
        </td>
        <td>
              IVA Honorario *
                <asp:RequiredFieldValidator runat="server" ID="rfvIvaHonorario" ControlToValidate="txtIvaHonorario"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr class="rowA">
        <td>
           <asp:DropDownList runat="server" ID="ddlCategoriaValores" 
                OnSelectedIndexChanged="ddlCategoriaValores_SelectedIndexChanged"
                AutoPostBack="true"></asp:DropDownList>
             <%-- <asp:TextBox runat="server" ID="txtNivel" MaxLength="0"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="fteValNivel" runat="server" TargetControlID="txtNivel"
                FilterType="Numbers" ValidChars=".," />--%>
         </td>
        
        <td>
           <asp:TextBox runat="server" ID="txtIvaHonorario" MaxLength="0" Enabled="false"></asp:TextBox>
            <%--<Ajax:FilteredTextBoxExtender ID="ftIvaHonorario" runat="server" TargetControlID="txtIvaHonorario"
                FilterType="Numbers" ValidChars=".," />--%>
        </td>
    </tr>
    <tr class="rowB">
        <td>Total Honorario Proyectado *
                <asp:RequiredFieldValidator runat="server" ID="rfvPorcentajeIVA" ControlToValidate="txtTotalProyectado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
        <td>
             Tiempo Proyectado en Años *
        </td>
    </tr>
    <tr class="rowA">
        <td>
              <asp:TextBox runat="server" ID="txtTotalProyectado" MaxLength="0" Enabled="false"></asp:TextBox>
            <%--<Ajax:FilteredTextBoxExtender ID="ftPorcentajeIVA" runat="server" TargetControlID="txtTotalProyectado"
                FilterType="Numbers" ValidChars=".," />--%>
        </td>
        <td>
         <asp:TextBox runat="server" ID="txtTiempoProyectadoAnio" Enabled="false"></asp:TextBox>
        </td>
    </tr>
   
    <tr class="rowB">
        <td>
            Tiempo Proyectado Meses *
        </td>
        <td>
           Tiempo Proyectado Días *      
        </td>
    </tr>
    <tr class="rowA">
        
        <td>
            <asp:TextBox runat="server" ID="txtTiempoProyectadoMeses" Enabled="false"></asp:TextBox>
        </td>
        <td>            
            <asp:TextBox runat="server" ID="txtTiempoProyectadoDias" Enabled="false"></asp:TextBox>
        </td>
    </tr>
     <tr class="rowB">
        <td>
            Total Honorario Por Tiempo Proyectado                
        </td>
        <td>
         
        </td>
    </tr>
    <tr class="rowA">
        <td>
             <asp:TextBox runat="server" ID="txtTotalHonorarioTiempo" Enabled="false"></asp:TextBox>
        </td>
        <td>
          
        </td>
    </tr>
</table>

<asp:table ID="tbRechazo" runat="server" width="90%" align="center" Visible="false">
    <asp:TableRow class="rowB">
        <asp:TableCell >
            Motivo Rechazo *
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow class="rowA">
        <asp:TableCell heigth="150px">
           <asp:TextBox runat="server" ID="txtRechazo" Enabled="True"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
</asp:table>

 <asp:Table ID="tbAnadirNuevo" runat="server" width="90%" align="center">
        <asp:TableRow class="rowB">
            <asp:TableCell><asp:label Text="Añadir Adicion y/o Prorroga" ID="lblTituloAdicionNuevo" runat="server"> </asp:label>
            </asp:TableCell>
  
        </asp:TableRow>
        <asp:TableRow class="rowA">
            <asp:TableCell class="auto-style1">
                <asp:ImageButton  ID="btnNuevaAdicion" OnClick="btnNuevo_Click" runat="server" CommandName="Select" ImageUrl="~/Image/btn/add.gif"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
            </asp:TableCell>
        </asp:TableRow>

  </asp:Table>

 <asp:Panel runat="server" ID="pnAdiccion">
        <table  align="center">
            <tr class="rowAG">
                <td>
                    <div  style="overflow-x: auto; width: 100%">
                    <asp:GridView runat="server" ID="gvAdicionesProrrogas" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="IdAdicionProrrogaCandidato"
                        >
                        <Columns>
                            <asp:BoundField HeaderText="IdProyeccionPresupuestos" DataField="IdProyeccionPresupuestos" />
                            <asp:BoundField HeaderText="IdAdicionProrrogaCandidato" DataField="IdAdicionProrrogaCandidato" />
                            <asp:BoundField HeaderText="ValorAdicionado" DataField="ValorAdicionado" />
                            <asp:BoundField HeaderText="ProrrogaIndividual" DataField="ProrrogaIndividual" />
                            <asp:BoundField HeaderText="Aprobado" DataField="Aprobado" />
                            <asp:BoundField HeaderText="Rechazado" DataField="Rechazado" />
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:ImageButton  ID="btnEditarAdicion"   runat="server" OnClick="btnEditar_Click"  ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Borrar">
                                <ItemTemplate>
                                    <asp:ImageButton  ID="btnEliminarAdicion"  runat="server" OnClick="btnEliminar_Click" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observación" DataField="RazonRechazo" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                        </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

 <asp:Panel runat="server" ID="Panel1">
        <table  align="center">
            <tr class="rowAG">
                <td>
                    <div  style="overflow-x: auto; width: 100%">
                    <asp:GridView runat="server" ID="gvGrillaAprob" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="IdAdicionProrrogaCandidato"
                        >
                        <Columns>
                            <asp:BoundField HeaderText="IdProyeccionPresupuestos" DataField="IdProyeccionPresupuestos" />
                            <asp:BoundField HeaderText="IdAdicionProrrogaCandidato" DataField="IdAdicionProrrogaCandidato" />
                            <asp:BoundField HeaderText="ValorAdicionado" DataField="ValorAdicionado" />
                            <asp:BoundField HeaderText="ProrrogaIndividual" DataField="ProrrogaIndividual" />
                            <asp:BoundField HeaderText="Aprobado" DataField="Aprobado" />
                            <asp:BoundField HeaderText="Rechazado" DataField="Rechazado" />
                             <asp:TemplateField HeaderText="Aprobar Adición">
                                <ItemTemplate >
                                    <asp:ImageButton OnClick="btnAprobar_Click"  ID="btnAprobarAdicion"  runat="server" CommandName="Select"  ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Rechazar Adición">
                                <ItemTemplate >
                                    <asp:ImageButton OnClick="btnRechazar_Click"  ID="btnRechazarAdicion" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observación" DataField="RazonRechazo" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                        </div>
                </td>
            </tr>
        </table>
    </asp:Panel>


<script src="../../../Scripts/jquery.maskMoney.js"></script>

<script type="text/javascript">
    $("#<%= txtHonorarioBase.ClientID %>").maskMoney();
</script>
