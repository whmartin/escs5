﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatosContrato.ascx.cs"
    Inherits="Page_Cupos_UserControl_DatosContrato" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<link type="text/css" href="../../../Styles/global.css" rel="Stylesheet" />
<link type="text/css" href="../../../Styles/direccion.css" rel="Stylesheet" />

<table width="90%" align="center">
    <tr>
        <td align="center" colspan="2">
            <asp:Label runat="server" ID="lblError" Visible="false" CssClass="lbE"></asp:Label>
        </td>
    </tr>
    <tr class="rowB">
        <td>
            Consecutivo Interno           
        </td>
        <td>Dependencia Kactus *
            <%--<asp:RequiredFieldValidator runat="server" ID="rfvIdDependenciaKactus" ControlToValidate="ddlIdDependenciaKactus"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvIdDependenciaKactus" ControlToValidate="ddlIdDependenciaKactus"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox runat="server" ID="txtConsecutivoInterno" MaxLength="50" Enabled="false"></asp:TextBox>
          
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlIdDependenciaKactus"></asp:DropDownList>
        </td>
    </tr>
    <tr class="rowB">
        <td colspan="2">Objeto *
            <%--<asp:RequiredFieldValidator runat="server" ID="rfvObjeto" ControlToValidate="rdlObjeto"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="cvObjeto" ControlToValidate="rdlObjeto"
                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
        </td>       
    </tr>
    <tr class="rowA">
        <td colspan="2">
             <asp:DropDownList runat="server" id="rdlObjeto" Enable="false"  AutoPostBack="True" OnSelectedIndexChanged="rdlObjeto_SelectedIndexChanged"></asp:DropDownList>
        </td>
    </tr>
    <tr class="rowB">
        <td colspan="2">
            <asp:TextBox runat="server" ID="txtObjeto" TextMode="MultiLine" Height="100px" Width="98%" Enabled="false"></asp:TextBox>          
        </td>

    </tr>
</table>
