﻿using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Cupos_UserControlNovCand_ucHonorario : System.Web.UI.UserControl
{
    #region Enabled Objetos
    SIAService vSIAService = new SIAService();

    public void ModoConsulta(Boolean pEstado)
    {

        pEstado = false;
        txtFechaInicialProyectado.Enabled = pEstado;
        txtFechaFinalProyectado.Enabled = pEstado;
        //txtFechaIngresoICBF.Enabled = pEstado;
        ddlCategoriaValores.Enabled = pEstado;
        ddlCategoriaEmpleado.Enabled = pEstado;
        txtHonorarioBase.Enabled = pEstado;
        btnCalcular.Enabled = pEstado;
    }

    #endregion
    public object ddlCategoriaEmpleadoSource
    {
        set
        {
            ddlCategoriaEmpleado.DataSource = value;
            ddlCategoriaEmpleado.DataValueField = "IdCategoria";
            ddlCategoriaEmpleado.DataTextField = "Descripcion";
            ddlCategoriaEmpleado.DataBind();
            ddlCategoriaEmpleado.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public object ddlCategoriaValoresSource
    {
        set
        {
            ddlCategoriaValores.DataSource = value;
            ddlCategoriaValores.DataTextField = "Nivel";
            ddlCategoriaValores.DataValueField = "IdCategoriaValor";
            ddlCategoriaValores.DataBind();
            ddlCategoriaValores.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string txtVigencia
    {
        set
        {
            HfdidVigencia.Value = value;
        }
        get
        {
            return HfdidVigencia.Value;
        }
    }

    public string razonRechazo
    {
        set
        {
            
        }
        get
        {
            return txtRechazo.Text; 
        }
    }

    public string usuarioActual
    {
        set
        {
            hfUsuarioActual.Value = value;
        }
        get
        {
            return hfUsuarioActual.Value.ToString();
        }
    }

    public string IdCupoValue
    {
        set
        {
            HfIdCupo.Value = value;
        }
        get
        {
            return HfIdCupo.Value;
        }
    }
    public string hfidProyeccionPresupuestoValue
    {
        set
        {
            hfidProyeccionPresupuesto.Value = value;
        }
        get
        {
            return hfidProyeccionPresupuesto.Value;
        }
    }

    public bool prorrogascheck
    {
        get {
            return cbProrrogas.Checked;
        }
        set { }
    }

    public DateTime fechaProrrogaFinal
    {
        get
        {
            return txtFechaFinalProrroga.Date;
        }
        set { }
    }

    public DateTime fechaProrrogaInicial
    {
        get
        {
            return txtFechaInicialProrroga.Date;
        }
        set { }
    }
    public string ddlCategoriaValoresValue
    {
        get
        {
            return ddlCategoriaValores.SelectedValue;
        }
        set
        {
            ddlCategoriaValores.SelectedValue = value;
        }
    }

    public List<AdicionesYProrrogas> gvAdicionesProrrogasValuegv
    {
        get
        {
            return new List<AdicionesYProrrogas>();
        }
        set
        {
            gvAdicionesProrrogas.DataSource = value;
            gvAdicionesProrrogas.DataBind();
        }
    }

    public List<AdicionesYProrrogas> gvGrillaAprobValuegv
    {
        get
        {
            return new List<AdicionesYProrrogas>();
        }
        set
        {
            gvGrillaAprob.DataSource = value;
            gvGrillaAprob.DataBind();
        }
    }
    

    public DateTime FechaInicialProyectado
    {
        get
        {
            return txtFechaInicialProyectado.Date;
        }
        set
        {
            txtFechaInicialProyectado.Date = value;
        }

    }

    public DateTime FechaFinalProyectado
    {
        get
        {
            return txtFechaFinalProyectado.Date;
        }
        set
        {
            txtFechaFinalProyectado.Date = value;
        }

    }

    //private int _IdProyeccion;
    public string IdProyeccion
    {
        get { return HfdidProyeccion.Value; }
        set { HfdidProyeccion.Value = value; }
    }


    public string ddlCategoriaEmpleadoValue
    {
        get
        {
            return ddlCategoriaEmpleado.SelectedValue;
        }
        set
        {
            ddlCategoriaEmpleado.SelectedValue = value;
        }
    }
    //public string Nivel {
    //    get
    //    {
    //        return txtNivel.Text;
    //    }
    //    set
    //    {
    //        txtNivel.Text = value;
    //    }
    //}

    public string HonorarioBase
    {
        get
        {
            return txtHonorarioBase.Text;
        }
        set
        {
            txtHonorarioBase.Text = value;
        }
    }

    public string IvaHonorario
    {
        get
        {
            return txtIvaHonorario.Text;
        }
        set
        {
            txtIvaHonorario.Text = value;
        }
    }

    public string PorIvaHonorario
    {
        get
        {
            return hfPorIvaHonorario.Value;
        }
        set
        {
            hfPorIvaHonorario.Value = value;
        }
    }

    public string TotalHonorario
    {
        get
        {
            return txtTotalProyectado.Text;
        }
        set
        {
            txtTotalProyectado.Text = value;
        }
    }

    public string TiempoProyectadoMese
    {
        get
        {
            return txtTiempoProyectadoMeses.Text;
        }
        set
        {
            txtTiempoProyectadoMeses.Text = value;
        }
    }
    public string TiempoProyectadoDias
    {
        get
        {
            return txtTiempoProyectadoDias.Text;
        }
        set
        {
            txtTiempoProyectadoDias.Text = value;
        }
    }
    public string TiempoProyectadoAnio
    {
        get
        {
            return txtTiempoProyectadoAnio.Text;
        }
        set
        {
            txtTiempoProyectadoAnio.Text = value;
        }
    }

    public string TotalHonorarioTiempo
    {
        get
        {
            return txtTotalHonorarioTiempo.Text;
        }
        set
        {
            txtTotalHonorarioTiempo.Text = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCalcular_Click(object sender, EventArgs e)
    {
        CalcularTotalesHonorario();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        //SetSessionParameter("CupoAreas.IdcuposAreasNecesidad", Convert.ToInt32(hf.Value));

        //NavigateTo(SolutionPage.Add);

        try
        {
            ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IdcuposAreasNecesidad", HfIdCupo.Value);

            ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdProyeccionPresuCuposA", HfdidProyeccion.Value);
            ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdAdicionProrroga", 0);

            Page.Response.Redirect("~/Page/Cupos/NovedadCandidato/Add.aspx");
        }
        catch (UserInterfaceException ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }
    public void CalcularTotalesHonorario()
    {
        try
        {
            //ValidacionesPantalla();

            lblError.Visible = false;
            lblError.Text = "";
            if(cbProrrogas.Checked == true)
            {
                txtFechaFinalProyectado.Date = txtFechaFinalProrroga.Date;
                if (txtFechaFinalProyectado.Date.ToString("yyyyMMdd") == "")
                    throw new Exception("Debe digitar Una Fecha Final Prorroga");

            }




            if (string.IsNullOrEmpty(txtHonorarioBase.Text))
                throw new Exception("Debe digitar Un Honorario Base");

            if (txtFechaFinalProyectado.Date.ToString("yyyyMMdd") == "19000101")
                throw new Exception("Debe digitar Una Fecha Final Proyectado");

            if (txtFechaInicialProyectado.Date.ToString("yyyyMMdd") == "19000101")
                throw new Exception("Debe digitar Una Fecha Inicial Proyectado");

            ValidacionesPantalla();

            TimeSpan ts = Convert.ToDateTime(txtFechaFinalProyectado.Date) - Convert.ToDateTime(txtFechaInicialProyectado.Date);
            int pDias = 0, pMeses = 0, pAnios = 0;
            //txtTiempoProyectadoDias.Text = ts.Days.ToString();
            //txtTiempoProyectadoMeses.Text = Convert.ToString(Math.Round(ts.TotalDays / 30, 0));
            CalcularDiferenciaFEchas(Convert.ToDateTime(txtFechaInicialProyectado.Date), Convert.ToDateTime(txtFechaFinalProyectado.Date), ref pDias, ref pMeses, ref pAnios);
            txtTiempoProyectadoDias.Text = pDias.ToString();
            txtTiempoProyectadoMeses.Text = pMeses.ToString();
            txtTiempoProyectadoAnio.Text = pAnios.ToString();


            //decimal vPorcenIva = Convert.ToDecimal(PorIvaHonorario);
            //decimal vIvaHonorario = Convert.ToDecimal(txtHonorarioBase.Text)* (vPorcenIva / 100);
            txtIvaHonorario.Text = "0"; // vIvaHonorario.ToString("C");
            decimal vTotalPRoyectado = Convert.ToDecimal(txtHonorarioBase.Text);// + vIvaHonorario;
            txtTotalProyectado.Text = vTotalPRoyectado.ToString("C");


            txtTotalHonorarioTiempo.Text = ((vTotalPRoyectado * (pAnios * 12)) + (vTotalPRoyectado * pMeses) + ((vTotalPRoyectado / 30) * pDias)).ToString("C");

            
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    public void CalcularDiferenciaFEchas(DateTime FEchaInicio, DateTime FechaFinal, ref int pDias, ref int pMeses, ref int pAnios)
    {
        DateTime fi = FEchaInicio;
        DateTime ft = FechaFinal;

        //int AntDias = 0, AntMeses = 0, AntAños = 0;

        int DiasFI = fi.Day, MesFI = fi.Month, AñoFI = fi.Year;
        int DiasFT = ft.Day, MesFT = ft.Month, AñoFT = ft.Year;

        if (DiasFI == 31)
            DiasFI = 30;

        if (DiasFT == 31)
            DiasFT = 30;

        if (DiasFT < DiasFI)
        {
            DiasFT += 30;
            MesFT -= 1;
        }

        // calcula dias de antiguedad
        pDias = (DiasFT - DiasFI) + 1;

        if (MesFT < MesFI)
        {
            MesFT += 12;
            AñoFT -= 1;
        }

        // calcula meses de antiguedad
        pMeses = MesFT - MesFI;

        // calcula años antiguedad
        pAnios = AñoFT - AñoFI;

    }
    public void ValidacionesPantalla()
    {
        if (txtFechaInicialProyectado.Date > txtFechaFinalProyectado.Date)
        {
            throw new Exception("La Fecha Inicial de La Proyeccion No Puede Ser Mayor Que La Final");
        }

        //if (txtFechaIngresoICBF.Date > txtFechaInicialProyectado.Date)
        //{
        //    throw new Exception("La Fecha De Ingreso al ICBF No puede Ser Mayor Que La Fecha Inicial Proyectado");
        //}

        if (ddlCategoriaValores.SelectedValue == "-1")
        {
            throw new Exception("Por favor Seleccione un Nivel");
        }

        if (txtVigencia == string.Empty)
        {
            throw new Exception("No Se Ha Seleccionado una vigencia");
        }
        
        int nivelSelc = Convert.ToInt32(ddlCategoriaValores.SelectedValue);

        var categoriaValor = vSIAService.ConsultarCategoriaValores(nivelSelc);

        decimal valorMinimo = categoriaValor.ValorMinimo;
        decimal valorMaximo = categoriaValor.ValorMaximo;
        decimal valorEsrito = 0;

        if (!string.IsNullOrEmpty(txtHonorarioBase.Text))
            valorEsrito = Convert.ToDecimal(txtHonorarioBase.Text);

        if(valorEsrito > valorMaximo)
        {
            throw new Exception("El Valor Del Honorario Base Mensual Digitado Supera El Valor Permitido.");
        }

        if (valorEsrito < valorMinimo)
        {
            throw new Exception("El Valor Del Honorario Base Mensual Digitado Es Inferior Al Valor Permitido.");
        }
        //var vHonorariosTercero = vSIAService.ConsultarCategoriaValoress(Convert.ToInt32(txtNivel.Text),Convert.ToInt32(txtVigencia), true).Take(1).SingleOrDefault();

        //if(vHonorariosTercero == null)
        //{
        //    throw new Exception("No se encunetra el nivel digitado para la vigencia de la proyeccion");
        //}

        //if (txtHonorarioBase.Text == string.Empty)
        //{
        //    throw new Exception("Por favor digite el valor Base del Honorario");
        //}

        //if(Convert.ToDecimal(txtHonorarioBase.Text) > vHonorariosTercero.ValorMaximo)
        //{
        //    throw new Exception("El Valor Del Honorario Supera El Permitido en la Vigencia");
        //}

        //if (Convert.ToDecimal(txtHonorarioBase.Text) < vHonorariosTercero.ValorMinimo)
        //{
        //    throw new Exception("El Valor Del Honorario Es Inferior a Lo Permitido En La Vigencia");
        //}
    }

    protected void cbAdiciones_CheckedChanged(object sender, EventArgs e)
    {
        //cbProrrogas.Checked = false;    

        if (cbAdiciones.Checked == true)
        {
            txtHonorarioBase.Enabled = true;
            ddlCategoriaEmpleado.Enabled = true;
            ddlCategoriaValores.Enabled = true;
        }
        else
        {
            txtHonorarioBase.Enabled = false;
            ddlCategoriaEmpleado.Enabled = false;
            ddlCategoriaValores.Enabled = false;

        }

    }

    protected void cbProrrogas_CheckedChanged(object sender, EventArgs e)
    {
        //cbAdiciones.Checked = false;
       

        if (cbProrrogas.Checked == true)
        {
            tbFechasProrroga.Visible = true;
        }
        else
        {
            tbFechasProrroga.Visible = false;

        }
    }
    protected void ddlCategoriaEmpleado_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlCategoriaValores.DataSource = null;
            ddlCategoriaValores.DataBind();

            lblError.Visible = false;
            lblError.Text = "";

            int vIdCategoriaEmpleados;

            vIdCategoriaEmpleados = Convert.ToInt32(ddlCategoriaEmpleado.SelectedValue);

            int vidproyeccion = Convert.ToInt32(IdProyeccion);

            ddlCategoriaValores.DataSource = vSIAService.ConsultarCategoriaValoresAsignada(vidproyeccion, vIdCategoriaEmpleados);
            ddlCategoriaValores.DataTextField = "Nivel";
            ddlCategoriaValores.DataValueField = "IdCategoriaValor";
            ddlCategoriaValores.DataBind();
            ddlCategoriaValores.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void ddlCategoriaValores_SelectedIndexChanged(object sender, EventArgs e)
    {
        CalcularTotalesHonorario();
    }


    //eventos Grilla Editar
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        try { 
                int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
                var dataKey = gvAdicionesProrrogas.DataKeys[rowIndex];

                var vAdicionProyeccion = vSIAService.ConsultarAdicionProrrogaCandidato(Convert.ToInt32(dataKey.Value));
                if (vAdicionProyeccion.Aprobado)
                {
                    throw new Exception("Esta adición no puede editarse ya se encuentra aprobada.");
                }
                else
                {

                ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IdcuposAreasNecesidad", HfIdCupo.Value);

                ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdProyeccionPresuCuposA", HfdidProyeccion.Value);
                ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdAdicionProrroga", Convert.ToInt32(dataKey.Value));

                Page.Response.Redirect("~/Page/Cupos/NovedadCandidato/Add.aspx");
            }

            }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvAdicionesProrrogas.DataKeys[rowIndex];

            var vAdicionProyeccion = vSIAService.ConsultarAdicionProrrogaCandidato(Convert.ToInt32(dataKey.Value));
            if (vAdicionProyeccion.Aprobado)
            {
                throw new Exception("Esta adición no puede editarse ya se encuentra aprobada.");
            }
            else
            {
                var vResultado = vSIAService.EliminarAdicionProrrogaCandidato(Convert.ToInt32(dataKey.Value));

                ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdProyeccionPresuCuposA", vAdicionProyeccion.IdProyeccionPresuspuesto);
                ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IdcuposAreasNecesidad", vAdicionProyeccion.IdCupoArea);

                Page.Response.Redirect("~/Page/Cupos/NovedadCandidato/Detail.aspx");

            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
        //EliminarRegistro();
    }
    //eventos Grilla Aprobar
    protected void btnAprobar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvGrillaAprob.DataKeys[rowIndex];

            int vResultado;
            var vAdicionProyeccion = vSIAService.ConsultarAdicionProrrogaCandidato(Convert.ToInt32(dataKey.Value));
            if (vAdicionProyeccion.Aprobado==true || vAdicionProyeccion.Rechazado == true)
            {
                throw new Exception("Esta adicción ya ha sido Gestionada.");
            }
            else
            {
                AdicionProrrogaCandidato vAdicionProrrogaCandidato = vSIAService.ConsultarAdicionProrrogaCandidato(Convert.ToInt32(dataKey.Value));
                vAdicionProrrogaCandidato.IdAdicionProrrogaCandidato = Convert.ToInt32(dataKey.Value);
                vAdicionProrrogaCandidato.Aprobado = true;
                vAdicionProrrogaCandidato.Rechazado = false;
                vAdicionProrrogaCandidato.RazonRechazo = "";
                vAdicionProrrogaCandidato.UsuarioModifica= hfUsuarioActual.Value.ToString();
                vResultado = vSIAService.ModificarAdicionProrrogaCandidato(vAdicionProrrogaCandidato);

                ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdProyeccionPresuCuposA", vAdicionProrrogaCandidato.IdProyeccionPresuspuesto);
                ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IdcuposAreasNecesidad", vAdicionProrrogaCandidato.IdCupoArea);

                Page.Response.Redirect("~/Page/Cupos/AprobarModificacionesCandidato/Detail.aspx");


            }


        }
    catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }


    protected void btnRechazar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {

            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvGrillaAprob.DataKeys[rowIndex];

            int vResultado;
            AdicionProyeccion vAdicionProyeccion = new AdicionProyeccion();
            vAdicionProyeccion = vSIAService.ConsultarAdicionProyeccion(Convert.ToInt32(dataKey.Value));

            if (vAdicionProyeccion.Aprobado || vAdicionProyeccion.Rechazado)
            {
                throw new Exception("Esta adicción ya ha sido Gestionada.");
            }
            else
            {
                ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IdcuposAreasNecesidad", HfIdCupo.Value);

                ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdProyeccionPresuCuposA", HfdidProyeccion.Value);
                ((Parameters)Session["sessionParameters"]).Set("AprobacionPresupuestos.IdAdicionProrroga", Convert.ToInt32(dataKey.Value));

                Page.Response.Redirect("~/Page/Cupos/AprobarModificacionesCandidato/Add.aspx");


            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }
  
    //ocultar y visualizar 
    public void DeshabilitarAdd()
    {
        tbAnadirNuevo.Visible = false;
        gvGrillaAprob.Visible = false;
        tbRechazo.Visible = false;
    }

    public void DeshabilitarDetail()
    {
        tbHabilitarEdiciones.Visible = false;
        tbFechasProrroga.Visible = false;
        gvGrillaAprob.Visible = false;
        tbRechazo.Visible = false;
    }

    public void DeshabilitarAprob()
    {
        tbAnadirNuevo.Visible = false;
        tbHabilitarEdiciones.Visible = false;
        tbFechasProrroga.Visible = false;
        gvGrillaAprob.Visible = true;
        tbRechazo.Visible = false;
    }

    public void DeshabilitarAddAprob()
    {
        tbAnadirNuevo.Visible = false;
        gvGrillaAprob.Visible = false;
        tbHabilitarEdiciones.Visible = false;
        tbFechasProrroga.Visible = false;
        gvGrillaAprob.Visible = false;
        tbRechazo.Visible = true;
    }


}
