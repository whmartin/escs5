﻿using Icbf.SIA.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Cupos_UserControl_DatosContrato : System.Web.UI.UserControl
{
    SIAService vSIAService = new SIAService();

    public void ModoConsulta(Boolean pEstado)
    {
        //txtConsecutivoInterno.Enabled = pEstado;
        ddlIdDependenciaKactus.Enabled = pEstado;
        rdlObjeto.Enabled = pEstado;
        
    }
    #region Propiedades

    public string ConsecutivoInterno
    {
        get
        {
            return txtConsecutivoInterno.Text;
        }
        set
        {
            txtConsecutivoInterno.Text = value;
        }
    }

    public object DependenciaKactusSource
    {        
        set
        {
            ddlIdDependenciaKactus.DataSource = value;
            ddlIdDependenciaKactus.DataValueField = "IdDependenciaSolicitante";
            ddlIdDependenciaKactus.DataTextField = "NombreDependenciaSolicitante";
            ddlIdDependenciaKactus.DataBind();
            ddlIdDependenciaKactus.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string DependenciaKactusValue
    {
        get
        {
            return ddlIdDependenciaKactus.SelectedValue;
        }
        set
        {
            ddlIdDependenciaKactus.SelectedValue = value;            
        }
    }

    public object ObjetoSource
    {
        set
        {
            rdlObjeto.DataSource = value;
            rdlObjeto.DataValueField = "IdObjetoCupos";
            rdlObjeto.DataTextField = "Titulo";
            rdlObjeto.DataBind();
            rdlObjeto.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
    }

    public string ObjetoValue
    {
        get
        {
            return rdlObjeto.SelectedValue;
        }
        set
        {
            rdlObjeto.SelectedValue = value;
        }
    }
    public string ObjetoText
    {
        get
        {
            return txtObjeto.Text;
        }
        set
        {
            txtObjeto.Text = value;
        }
    }

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void rdlObjeto_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtObjeto.Text = vSIAService.ConsultarObjeto(Convert.ToInt32(rdlObjeto.SelectedValue)).DescripcionObjeto;

    }
}
