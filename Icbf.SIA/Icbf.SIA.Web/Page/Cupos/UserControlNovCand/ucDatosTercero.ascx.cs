﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Session;

public partial class Page_Cupos_UserControlCupArea_ucDatosTercero : System.Web.UI.UserControl
{
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ContratoService vContratoService = new ContratoService();
    SIAService vSiaServices = new SIAService();
    
        public void ModoConsulta(Boolean pEstado)
    {
        imgNombreSolicitante.Enabled = pEstado;        

    }
    public string IdEntidad
    {
        get
        {
            return hfIdEntidad.Value;
        }
        set
        {
            hfIdEntidad.Value = value;
        }
    }

    public string PorcentajeIVa
    {
        get
        {
            return hfPorIva.Value;
        }
        set
        {
            hfPorIva.Value = value;
        }
    }
    public DateTime FechaIngresoICBF
    {
        get
        {
            return txtFechaIngresoICBF.Date;
        }
        set
        {
            txtFechaIngresoICBF.Date = value;
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
        else
        {
            #region AdministraPostBack
            string sControlName = Request.Params.Get("__EVENTTARGET");
            switch (sControlName)
            {
                case "cphCont_ucDatosTercero_hfIdEntidad":
                    LlenarDatosTercro();
                    break;
                default:
                    break;
            }
            #endregion
        }
    }

    public void LlenarDatosTercro()
    {
        if (!string.IsNullOrEmpty(hfIdEntidad.Value))
        {
            int idEntidad = Convert.ToInt32(hfIdEntidad.Value);

            var vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(idEntidad);
            FechaIngresoICBF = vEntidadProvOferente.FechaIngresoICBF;

            vEntidadProvOferente.TerceroProveedor = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            var tipoDocumento = vSiaServices.ConsultarTipoDocumento(vEntidadProvOferente.TerceroProveedor.IdDListaTipoDocumento);

            txtNombre.Text = vEntidadProvOferente.TerceroProveedor.PrimerNombre + " " + vEntidadProvOferente.TerceroProveedor.SegundoNombre;
            txtIdentificacionProveedor.Text = vEntidadProvOferente.TerceroProveedor.NumeroIdentificacion;
            txtTipoDocumento.Text = tipoDocumento.Nombre;
            txtApellidos.Text = vEntidadProvOferente.TerceroProveedor.PrimerApellido + " " + vEntidadProvOferente.TerceroProveedor.SegundoApellido;

            var vInfoAdminEntidad = vProveedorService.ConsultarInfoAdminEntidads(idEntidad).OrderBy(x => x.FechaCrea).First();
            var vRegimenProveedor = vProveedorService.ConsultarTipoRegimenTributario(Convert.ToInt32(vInfoAdminEntidad.IdTipoRegTrib));

            txtRegimen.Text = vRegimenProveedor.Descripcion;

            switch (vRegimenProveedor.CodigoRegimenTributario)
            {
                case "002":
                    hfPorIva.Value = "16";
                    ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IvaProveedorSelec", "16");                    
                    break;
                case "004":
                    hfPorIva.Value = "16";
                    ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IvaProveedorSelec", "16");
                    break;
                default:
                    hfPorIva.Value = "0";
                    ((Parameters)Session["sessionParameters"]).Set("CupoAreas.IvaProveedorSelec", "0");
                    break;

            }


            var vFormacion = vProveedorService.ConsultarFormacions(idEntidad, null, null, null, true);
            gvOtrosEstudios.DataSource = vFormacion.Where(x => !x.EsFormacionPrincipal);
            gvOtrosEstudios.DataBind();

            if (vFormacion != null)
            {
                if (vFormacion.Count > 0)
                {
                    var formacioPpal = vFormacion.Where(x => x.EsFormacionPrincipal);
                    if (formacioPpal.Count() != 0)
                    {
                        string modalidad = formacioPpal.Take(1).SingleOrDefault().ModalidadAcademica;
                        txtModalidadEducativa.Text = vFormacion.Where(x => x.EsFormacionPrincipal).Take(1).SingleOrDefault().ModalidadAcademica;
                        txtProfesion.Text = vFormacion.Where(x => x.EsFormacionPrincipal).Take(1).SingleOrDefault().Profesion;
                    }
                }
            }

        }
    }

    private string buscarModalidad(string pIdModalidadAcademica)
    {

        var vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, pIdModalidadAcademica, null);

        if (vLModalidadesAcademicas.Count > 0)
            return vLModalidadesAcademicas[0].Descripcion;

        return string.Empty;


    }
}