using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad RubrosCupos
/// </summary>
public partial class Page_RubrosCupos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    string PageName = "Cupos/RubrosCupos";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad RubrosCupos
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            RubrosCupos vRubrosCupos = new RubrosCupos();

            vRubrosCupos.CodigoRubro = Convert.ToString(txtCodigoRubro.Text);
            vRubrosCupos.DescripcionRubro = Convert.ToString(txtDescripcionRubro.Text);
            vRubrosCupos.Estado = Convert.ToBoolean(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vRubrosCupos.IdRubroCupos = Convert.ToInt32(hfIdRubroCupos.Value);
                vRubrosCupos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRubrosCupos, this.PageName, vSolutionPage);
                vResultado = vSIAService.ModificarRubrosCupos(vRubrosCupos);
            }
            else
            {
                vRubrosCupos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRubrosCupos, this.PageName, vSolutionPage);
                vResultado = vSIAService.InsertarRubrosCupos(vRubrosCupos);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("RubrosCupos.IdRubroCupos", vRubrosCupos.IdRubroCupos);
                SetSessionParameter("RubrosCupos.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Rubros Prestación de Servicios", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdRubroCupos = Convert.ToInt32(GetSessionParameter("RubrosCupos.IdRubroCupos"));
            RemoveSessionParameter("RubrosCupos.Id");

            RubrosCupos vRubrosCupos = new RubrosCupos();
            vRubrosCupos = vSIAService.ConsultarRubrosCupos(vIdRubroCupos);
            hfIdRubroCupos.Value = vRubrosCupos.IdRubroCupos.ToString();
            txtCodigoRubro.Text = vRubrosCupos.CodigoRubro;
            txtDescripcionRubro.Text = vRubrosCupos.DescripcionRubro;
            rblEstado.SelectedValue = vRubrosCupos.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRubrosCupos.UsuarioCrea, vRubrosCupos.FechaCrea, vRubrosCupos.UsuarioModifica, vRubrosCupos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "true"));
            rblEstado.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblEstado.SelectedValue = "true";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
