<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RubrosCupos_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIdRubroCupos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código Rubro *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoRubro" ControlToValidate="txtCodigoRubro"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Descripción Rubro *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionRubro" ControlToValidate="txtDescripcionRubro"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoRubro" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoRubro" runat="server" TargetControlID="txtCodigoRubro"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionRubro" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionRubro" runat="server" TargetControlID="txtDescripcionRubro"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
