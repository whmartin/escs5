using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de visualización detallada para la entidad RubrosCupos
/// </summary>
public partial class Page_RubrosCupos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/RubrosCupos";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("RubrosCupos.IdRubroCupos", hfIdRubroCupos.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdRubroCupos = Convert.ToInt32(GetSessionParameter("RubrosCupos.IdRubroCupos"));
            RemoveSessionParameter("RubrosCupos.IdRubroCupos");

            if (GetSessionParameter("RubrosCupos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("RubrosCupos.Guardado");


            RubrosCupos vRubrosCupos = new RubrosCupos();
            vRubrosCupos = vSIAService.ConsultarRubrosCupos(vIdRubroCupos);
            hfIdRubroCupos.Value = vRubrosCupos.IdRubroCupos.ToString();
            txtCodigoRubro.Text = vRubrosCupos.CodigoRubro;
            txtDescripcionRubro.Text = vRubrosCupos.DescripcionRubro;
            rblEstado.SelectedValue = vRubrosCupos.Estado.ToString().Trim().ToLower();
            ObtenerAuditoria(PageName, hfIdRubroCupos.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRubrosCupos.UsuarioCrea, vRubrosCupos.FechaCrea, vRubrosCupos.UsuarioModifica, vRubrosCupos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdRubroCupos = Convert.ToInt32(hfIdRubroCupos.Value);

            RubrosCupos vRubrosCupos = new RubrosCupos();
            vRubrosCupos = vSIAService.ConsultarRubrosCupos(vIdRubroCupos);
            InformacionAudioria(vRubrosCupos, this.PageName, vSolutionPage);
            int vResultado = vSIAService.EliminarRubrosCupos(vRubrosCupos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("RubrosCupos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Rubros Prestación de Servicios", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "true"));
            rblEstado.Items.Insert(0, new ListItem("Inactivo", "false"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
