﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;

public partial class Page_Cupos_AprobacionSecretaria_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/AprobacionSecretaria";
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

   
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        Aprobar();      
    }
    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        Rechazar();
    }

    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {


            int vIdCupoArea = Convert.ToInt32(GetSessionParameter("CupoAreas.IdcuposAreasNecesidadAprobar"));
            RemoveSessionParameter("CupoAreas.IdcuposAreasNecesidadAprobar");

            hfIdCupoArea.Value = vIdCupoArea.ToString();
            
            ucHonorario.ModoConsulta(false);
            ucDatosContrato.ModoConsulta(false);
            ucDatosTercero.ModoConsulta(false);

            CupoAreas vCupoAreas = new CupoAreas();
            vCupoAreas = vSIAService.ConsultarCupoAreas(vIdCupoArea);
            ucDatosContrato.ConsecutivoInterno = vCupoAreas.ConsecutivoInterno;
            ucDatosContrato.DependenciaKactusValue = vCupoAreas.IdDependenciaKactus;
            ucDatosContrato.ObjetoValue = vCupoAreas.IdObjeto.ToString();
            var vobjeto = vSIAService.ConsultarObjeto(vCupoAreas.IdObjeto);
            ucDatosContrato.ObjetoText = vobjeto.DescripcionObjeto;
            ucDatosTercero.IdEntidad = vCupoAreas.IdProveedor.ToString();
            ucDatosTercero.LlenarDatosTercro();
            ucHonorario.PorIvaHonorario = vCupoAreas.PorcentajeIVA.ToString();
            ucHonorario.FechaInicialProyectado = vCupoAreas.FechaInicialProyectado;
            ucHonorario.FechaFinalProyectado = vCupoAreas.FechaFinalProyectado;
            //ucHonorario.FechaIngresoICBF = vCupoAreas.FechaIngresoICBF;

            var vCategoriaValores = vSIAService.ConsultarCategoriaValores(vCupoAreas.IdCategoriaValores);
            ucHonorario.ddlCategoriaEmpleadoSource = vSIAService.ConsultarCategoriaEmpleados(null, true,null,null);
            ucHonorario.ddlCategoriaEmpleadoValue = vCategoriaValores.IdCategoriaEmpleados.ToString();
            ucHonorario.ddlCategoriaValoresSource = vSIAService.ConsultarCategoriaValoress(null, null, true, vCategoriaValores.IdCategoriaEmpleados);
            ucHonorario.ddlCategoriaValoresValue = vCupoAreas.IdCategoriaValores.ToString();

            //ucHonorario.ddlIdCategoriaValoresValue = vCupoAreas.IdCategoriaValores.ToString();
            ////var vCategoriaValores = vSIAService.ConsultarCategoriaValores(vCupoAreas.IdCategoriaValores);
            ////ucHonorario.Nivel = vCategoriaValores.Nivel.ToString("N");
            //ucHonorario.ddlNivelSource = vSIAService.ConsultarCategoriaValoress(null, null, true, vCupoAreas.IdCategoriaValores);

            ucHonorario.HonorarioBase = vCupoAreas.HonorarioBase.ToString("C");
            ucHonorario.IvaHonorario = vCupoAreas.IvaHonorario.ToString("C");
            ucHonorario.TotalHonorario = (vCupoAreas.HonorarioBase + vCupoAreas.IvaHonorario).ToString("C");
            ucHonorario.TiempoProyectadoMese = vCupoAreas.TiempoProyectadoMeses.ToString("N");
            ucHonorario.TiempoProyectadoDias = vCupoAreas.TiempoProyectadoDias.ToString("N");
            ucHonorario.TiempoProyectadoAnio = vCupoAreas.TiempoProyectadoAnios.ToString();
            ucHonorario.TotalHonorarioTiempo = vCupoAreas.TotalHonorarioTiempoProyectado.ToString("C");

            ObtenerAuditoria(PageName, hfIdCupoArea.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCupoAreas.UsuarioCrea, vCupoAreas.FechaCrea, vCupoAreas.UsuarioModifica, vCupoAreas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;           
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.eventoRechazar += new ToolBarDelegate(btnRechazar_Click);


            toolBar.EstablecerTitulos("Aprobación Proyección Necesidades Áreas/Dependencia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int IdProyeccionPresuCuposA = Convert.ToInt32(GetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposAprobar"));
            RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposAprobar");

            hfidProyeccionPresupuesto.Value = IdProyeccionPresuCuposA.ToString();
            CupoAreas vCupoAreas = new CupoAreas();
            var vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(IdProyeccionPresuCuposA);
            var regionalcupo = vSIAService.ConsultarRegional(vProyeccionPresupuestos.IdRegional);
            var vVigencia = vSIAService.ConsultarVigencia(vProyeccionPresupuestos.IdVigencia);
            var vAreas = vSIAService.ConsultarAreas(vProyeccionPresupuestos.IdArea);
            var rubro = vSIAService.ConsultarRubrosCupos(vProyeccionPresupuestos.IdRubro);
            hfiCodgioRegional.Value = regionalcupo.CodigoRegional;
            txtRegional.Text = regionalcupo.NombreRegional;
            txtVigencia.Text = vVigencia.AcnoVigencia.ToString();
            txtTotalCupo.Text = vProyeccionPresupuestos.TotalCupos.ToString("N0");
            txtValorAprobado.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            txtRubro.Text = rubro.CodigoRubro;
            if (vAreas.Count > 0)
                txtArea.Text = vAreas[0].NombreArea;
            ucDatosContrato.DependenciaKactusSource = vContratoService.ConsultarDependenciaSolicitante(null, null, regionalcupo.CodigoRegional);
            ucDatosContrato.ObjetoSource = vSIAService.ConsultarObjetos(null, null, true);
            //ucHonorario.ddlIdCategoriaValoresSource = vSIAService.ConsultarCategoriaEmpleados(null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Rechazar()
    {
        try
        {
            int IdCupoAreas = Convert.ToInt32(hfIdCupoArea.Value);

            List<CupoAreas> ListCupoAreas = new List<CupoAreas>();

            CupoAreas vCupoAreas = new CupoAreas()
            {
                IdCupoArea = IdCupoAreas,
                CodEstadoProceso = "APROB"
            };

            ListCupoAreas.Add(vCupoAreas);

            vSIAService.CambiarEstadoCupoAreas(ListCupoAreas, GetSessionUser().NombreUsuario, 0);

            SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposAprobar", hfidProyeccionPresupuesto.Value);
            Response.Redirect("ListAprobaNec.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Aprobar()
    {
        try
        {
            int IdCupoAreas = Convert.ToInt32(hfIdCupoArea.Value);

            List<CupoAreas> ListCupoAreas = new List<CupoAreas>();

            CupoAreas vCupoAreas = new CupoAreas()
            {
                IdCupoArea = IdCupoAreas,
                CodEstadoProceso = "APRSEC"
            };

            ListCupoAreas.Add(vCupoAreas);

            vSIAService.CambiarEstadoCupoAreas(ListCupoAreas, GetSessionUser().NombreUsuario, 0);

            SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposAprobar", hfidProyeccionPresupuesto.Value);
            Response.Redirect("ListAprobaNec.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}