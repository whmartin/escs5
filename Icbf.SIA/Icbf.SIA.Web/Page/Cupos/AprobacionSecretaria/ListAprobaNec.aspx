﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListAprobaNec.aspx.cs" Inherits="Page_Cupos_AprobacionSecretaria_ListAprobaNec" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIfProyeccionPresupuesto" runat="server" />
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">Proyecci&oacute;n
                </td>
                <td class="Cell"></td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:TextBox ID="txtIdProyeccion" runat="server" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell"></td>
            </tr>

            <tr class="rowB">
                <td class="Cell">Total Necesidades Proyectadas
                </td>
                <td class="Cell">Valor Total Necesidades Proyectadas
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:TextBox ID="txtNoCuposProyectado" runat="server" Enabled="false"></asp:TextBox>
                    <%--<asp:TextBox id="txtTotalCuposProyectado" runat="server" Enabled="false"></asp:TextBox>                    --%>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtValorTotalCupoProyectado" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">Valor Proyectado Disponible
                </td>
                <td class="Cell">Necesidades Proyectadas Disponible
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:TextBox ID="txtValorProyectadoDisponible" runat="server" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtCupoProyectadoDisponible" runat="server" Enabled="false"></asp:TextBox>
                </td>
            </tr>


            <tr class="rowB">
                <td class="Cell">Consecutivo Interno
                </td>
                <td class="Cell">Proveedor
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtConsecutivoInterno" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftConsecutivoInterno" runat="server" TargetControlID="txtConsecutivoInterno"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtIdentificacion" MaxLength="50"></asp:TextBox>
                </td>
            </tr>




        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto;width:100%">
                        <asp:GridView runat="server" ID="gvListCupo" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="180%" DataKeyNames="IdCupoArea" CellPadding="0" Height="16px"
                            OnSorting="gvListCupo_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvListCupo_PageIndexChanging" OnSelectedIndexChanged="gvListCupo_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Aprobar">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkAprobado" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Devolver">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkDevolver" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Consecutivo Interno" DataField="ConsecutivoInterno" SortExpression="ConsecutivoInterno" />
                                <asp:BoundField HeaderText="Contratista proyectado" DataField="NombreTercero" SortExpression="NombreTercero" />
                                <%--<asp:BoundField HeaderText="Fecha Ingreso ICBF" DataField="FechaIngresoICBF" DataFormatString="{0:d}" SortExpression="FechaIngresoICBF" />--%>
                                 <asp:TemplateField HeaderText="Fecha Ingreso ICBF" SortExpression="FechaIngresoICBF">
                                    <ItemTemplate>
                                         <asp:Label ID="lblFechaIngresoICBF" runat="server" Text='<%#  Convert.ToString(Eval("FechaIngresoICBF")) == "1900-01-01 00:00:00.000"   ? Convert.ToString(Eval("FechaIngresoICBF")) : "" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Categoría Perfil" DataField="CategoriaEmpleados" SortExpression="CategoriaEmpleados" />
                                <asp:BoundField HeaderText="Honorario Base" DataField="HonorarioBase" DataFormatString="{0:C2}" SortExpression="HonorarioBase" />
                                <asp:BoundField HeaderText="IVA Honorario" DataField="IvaHonorario" DataFormatString="{0:C2}" SortExpression="IvaHonorario" />
                                <asp:BoundField HeaderText="Porcentaje IVA" DataField="PorcentajeIVA" SortExpression="PorcentajeIVA" />
                                <asp:BoundField HeaderText="Dependencia" DataField="NombreDependenciaSolicitante" SortExpression="NombreDependenciaSolicitante" />
                                <asp:BoundField HeaderText="Tiempo Proyectado Meses" DataField="TiempoProyectadoMeses" SortExpression="TiempoProyectadoMeses" />
                                <asp:BoundField HeaderText="Tiempo Proyectado Días" DataField="TiempoProyectadoDias" SortExpression="TiempoProyectadoDias" />
                                <%--<asp:TemplateField HeaderText="Aprobado" SortExpression="Aprobado">
                                <ItemTemplate>
                                    <asp:Label ID="lblAprobado" runat="server" Text='<%# (bool) Eval("Aprobado") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:BoundField HeaderText="Usuario Aprobó" DataField="UsuarioAprobo" SortExpression="UsuarioAprobo" />
                                <asp:BoundField HeaderText="Fecha Aprobación" DataField="FechaAprobacion" DataFormatString="{0:d}" SortExpression="FechaAprobacion" />

                                

                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

