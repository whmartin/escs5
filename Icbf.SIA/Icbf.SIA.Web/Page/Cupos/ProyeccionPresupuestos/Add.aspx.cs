using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;

/// <summary>
/// Página de registro y edición para la entidad ProyeccionPresupuestos
/// </summary>
public partial class Page_ProyeccionPresupuestos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();

    string PageName = "Cupos/ProyeccionPresupuestos";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad ProyeccionPresupuestos
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();

            vProyeccionPresupuestos.IdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            vProyeccionPresupuestos.IdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            vProyeccionPresupuestos.IdRecurso = Convert.ToInt32(ddlIdRecurso.SelectedValue);
            vProyeccionPresupuestos.IdRubro = Convert.ToInt32(ddlIdRubro.SelectedValue);
            vProyeccionPresupuestos.IdArea = Convert.ToInt32(ddlIdArea.SelectedValue);
            vProyeccionPresupuestos.ValorCupo = Convert.ToDecimal(txtValorCupo.Text);
            vProyeccionPresupuestos.TotalCupos = Convert.ToInt32(txtTotalCupos.Text);

            if (Request.QueryString["oP"] == "E")
            {
                vProyeccionPresupuestos.IdProyeccionPresupuestos = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);
                vProyeccionPresupuestos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProyeccionPresupuestos, this.PageName, vSolutionPage);
                vResultado = vSIAService.ModificarProyeccionPresupuestos(vProyeccionPresupuestos);
            }
            else
            {
                vProyeccionPresupuestos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProyeccionPresupuestos, this.PageName, vSolutionPage);
                vResultado = vSIAService.InsertarProyeccionPresupuestos(vProyeccionPresupuestos);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1 || vResultado == 2)
            {
                SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", vProyeccionPresupuestos.IdProyeccionPresupuestos);

                if (hfTieneCupos.Value != "0" && !string.IsNullOrEmpty(hfCuposAnteriores.Value) && Convert.ToInt32(hfCuposAnteriores.Value) > vProyeccionPresupuestos.TotalCupos)
                    SetSessionParameter("ProyeccionPresupuestos.Guardado", "2");
                else if (hfTieneCupos.Value != "0" && !string.IsNullOrEmpty(hfValorAnterior.Value) && Convert.ToDecimal(hfValorAnterior.Value) > vProyeccionPresupuestos.ValorCupo)
                    SetSessionParameter("ProyeccionPresupuestos.Guardado", "2");
                else
                    SetSessionParameter("ProyeccionPresupuestos.Guardado", "1");

                if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
                {
                    SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);
                    Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
                }
                else
                {
                    NavigateTo(SolutionPage.Detail);
                }      
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Proyección Necesidades", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {            
            int vIdProyeccionPresupuestos = Convert.ToInt32(GetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos"));
            RemoveSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos");

            hfDesdeModificar.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
            RemoveSessionParameter("PestanaUserControl.Numero");

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();

            hfTieneCupos.Value = Convert.ToString(vSIAService.ConsultarCupoAreass(null, null, null, null, vIdProyeccionPresupuestos, null, null).Count());

            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccionPresupuestos);
            hfIdProyeccionPresupuestos.Value = vProyeccionPresupuestos.IdProyeccionPresupuestos.ToString();
            ddlIdRegional.SelectedValue = vProyeccionPresupuestos.IdRegional.ToString();
            ddlIdVigencia.SelectedValue = vProyeccionPresupuestos.IdVigencia.ToString();
            ddlIdRecurso.SelectedValue = vProyeccionPresupuestos.IdRecurso.ToString();
            ddlIdRubro.SelectedValue = vProyeccionPresupuestos.IdRubro.ToString();
            ddlIdArea.SelectedValue = vProyeccionPresupuestos.IdArea.ToString();

            int vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            ddlIdArea.Enabled = true;
            ddlIdArea.DataSource = vSIAService.ConsultarGlobalArea(null, vIdRegional);
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataBind();

            txtValorCupo.Text = vProyeccionPresupuestos.ValorCupo.ToString();
            hfValorAnterior.Value = vProyeccionPresupuestos.ValorCupo.ToString();
            txtTotalCupos.Text = vProyeccionPresupuestos.TotalCupos.ToString();
            hfCuposAnteriores.Value = vProyeccionPresupuestos.TotalCupos.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProyeccionPresupuestos.UsuarioCrea, vProyeccionPresupuestos.FechaCrea, vProyeccionPresupuestos.UsuarioModifica, vProyeccionPresupuestos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ftValorCupo.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            
            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
                       
            ddlIdRegional.DataSource = vSIAService.ConsultarRegionals(null, null);
            ddlIdRegional.DataTextField = "NombreRegional";
            ddlIdRegional.DataValueField = "IdRegional";
            ddlIdRegional.DataBind();
            ddlIdRegional.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdRecurso.DataSource = vSIAService.ConsultarTipoRecursoFinPptal();
            ddlIdRecurso.DataTextField = "DescTipoRecurso";
            ddlIdRecurso.DataValueField = "IdTipoRecursoFinPptal";
            ddlIdRecurso.DataBind();
            ddlIdRecurso.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdRubro.DataSource = vSIAService.ConsultarRubrosCuposs(null, null, true);
            ddlIdRubro.DataTextField = "DescripcionRubroCompleto";
            ddlIdRubro.DataValueField = "IdRubroCupos";
            ddlIdRubro.DataBind();
            ddlIdRubro.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIdRegional_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            ddlIdArea.Enabled = true;
            ddlIdArea.DataSource = vSIAService.ConsultarGlobalArea(null, vIdRegional);
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataBind();
            ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
