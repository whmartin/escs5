﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using System.Globalization;
using System.Linq.Expressions;

public partial class Page_Cupos_AprobacionPresupuesto_DetailCategoria : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/AprobacionPresupuesto";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        Aprobar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
   
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.EstablecerTitulos("Aprobación Proyección Necesidades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlIdRegional.DataSource = vSIAService.ConsultarRegionals(null, null);
            ddlIdRegional.DataTextField = "NombreRegional";
            ddlIdRegional.DataValueField = "IdRegional";
            ddlIdRegional.DataBind();
            ddlIdRegional.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdRecurso.DataSource = vSIAService.ConsultarTipoRecursoFinPptal();
            ddlIdRecurso.DataTextField = "DescTipoRecurso";
            ddlIdRecurso.DataValueField = "IdTipoRecursoFinPptal";
            ddlIdRecurso.DataBind();
            ddlIdRecurso.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdArea.DataSource = vSIAService.ConsultarAreas(null);
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataBind();
            ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdRubro.DataSource = vSIAService.ConsultarRubrosCuposs(null, null, true);
            ddlIdRubro.DataTextField = "DescripcionRubroCompleto";
            ddlIdRubro.DataValueField = "IdRubroCupos";
            ddlIdRubro.DataBind();
            ddlIdRubro.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            rblAprobado.Items.Insert(0, new ListItem("Aprobar", "true"));
            rblAprobado.Items.Insert(0, new ListItem("Rechazar", "false"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatos()
    {
        try
        {
            int vIdProyeccionPresupuestos = Convert.ToInt32(GetSessionParameter("ProyeccionCategoria.IdProyeccion"));
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccion");
            hfIdProyeccionPresupuestos.Value = vIdProyeccionPresupuestos.ToString();

            int vIdProyeccionCategoria = Convert.ToInt32(GetSessionParameter("ProyeccionCategoria.IdProyeccionCategoria"));
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccionCategoria");
            hfIdProyeccionCategoria.Value = vIdProyeccionCategoria.ToString();

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccionPresupuestos);
            ddlIdRegional.SelectedValue = vProyeccionPresupuestos.IdRegional.ToString();
            ddlIdVigencia.SelectedValue = vProyeccionPresupuestos.IdVigencia.ToString();
            ddlIdRecurso.SelectedValue = vProyeccionPresupuestos.IdRecurso.ToString();
            ddlIdRubro.SelectedValue = vProyeccionPresupuestos.IdRubro.ToString();

            ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
            vProyeccionCategoria = vSIAService.ConsultarProyeccionCategoria(vIdProyeccionCategoria, vIdProyeccionPresupuestos);
            txtCategoria.Text = vProyeccionCategoria.Descripcion;
            txtNivel.Text = vProyeccionCategoria.Nivel.ToString();
            txtCantidad.Text = vProyeccionCategoria.Cantidad.ToString();

            ObtenerAuditoria(PageName, hfIdProyeccionPresupuestos.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProyeccionPresupuestos.UsuarioCrea, vProyeccionPresupuestos.FechaCrea, vProyeccionPresupuestos.UsuarioModifica, vProyeccionPresupuestos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Aprobar()
    {
        try
        {
            if (string.IsNullOrEmpty(rblAprobado.SelectedValue))
                throw new Exception("Seleccione Aprobar o Rechazar");
        
            ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
            vProyeccionCategoria.Aprobado = Convert.ToBoolean(rblAprobado.SelectedValue);
            vProyeccionCategoria.IDProyeccionCategoria = Convert.ToInt32(hfIdProyeccionCategoria.Value);

            if (rblAprobado.SelectedValue == "false")
            {
                if (txtDescripcion.Text == string.Empty)
                {
                    throw new Exception("Se debe digitar Un motivo de devolución para continuar");
                }                
                vProyeccionCategoria.CodEstadoProceso = "RECH";
                vProyeccionCategoria.Cantidad = Convert.ToInt32(txtCantidad.Text);
                vProyeccionCategoria.CantidadAprobada = 0;
                vProyeccionCategoria.DescripcionRechazo = txtDescripcion.Text;
                vProyeccionCategoria.UsuarioModifica = GetSessionUser().NombreUsuario;
            }
            else
            {
                vProyeccionCategoria.CodEstadoProceso = "APROB";
                vProyeccionCategoria.Cantidad = Convert.ToInt32(txtCantidad.Text);
                vProyeccionCategoria.CantidadAprobada = Convert.ToInt32(txtCantidad.Text);
                vProyeccionCategoria.DescripcionRechazo = "";
                vProyeccionCategoria.FechaModifica = DateTime.Now;
                vProyeccionCategoria.UsuarioModifica = GetSessionUser().NombreUsuario;
            }

            int vResultado = vSIAService.AprobarProyeccionCategoria(vProyeccionCategoria);
            InformacionAudioria(vProyeccionCategoria, this.PageName, vSolutionPage);

            SetSessionParameter("AprobacionPresupuesto.Aprobado", "1");
            NavigateTo(SolutionPage.List);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}