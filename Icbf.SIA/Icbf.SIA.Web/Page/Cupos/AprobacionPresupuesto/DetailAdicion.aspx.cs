using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using System.Globalization;
using System.Linq.Expressions;

/// <summary>
/// Página de visualización detallada para la entidad AprobacionPresupuestos
/// </summary>
public partial class Page_Cupos_AprobacionPresupuestos_DetailAdicion : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/AprobacionAdicion";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //vSolutionPage = SolutionPage.Detail;
        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
                Buscar();
            }
        //}
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        Aprobar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();

            CargarGrilla(gvProyeccionCategoria, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProyeccionCategoria.DataKeys[rowIndex].Values[0].ToString();
            SetSessionParameter("ProyeccionCategoria.IdProyeccionCategoria", strValue);

            string strValue2 = gvProyeccionCategoria.DataKeys[rowIndex].Values[1].ToString();
            SetSessionParameter("ProyeccionCategoria.IdProyeccion", strValue2);

            Response.Redirect("DetailCategoria.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProyeccionCategoria_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProyeccionCategoria.SelectedRow);
    }

    protected void gvProyeccionCategoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProyeccionCategoria.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvProyeccionCategoria_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int? vIDProyeccionCategoria = null;
            vIDProyeccionCategoria = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);

            var myGridResults = vSIAService.ConsultarProyeccionCategorias(vIDProyeccionCategoria, "CREA");

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(ProyeccionCategoria), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ProyeccionCategoria, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdAprobacionPresupuestos = Convert.ToInt32(GetSessionParameter("AprobacionPresupuestos.IdAprobacionPresupuestos"));
            RemoveSessionParameter("AprobacionPresupuestos.IdAprobacionPresupuestos");

            if (GetSessionParameter("AprobacionPresupuestos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("AprobacionPresupuestos.Guardado");


            ProyeccionPresupuestos vAprobacionPresupuestos = new ProyeccionPresupuestos();
            vAprobacionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdAprobacionPresupuestos);
            hfIdProyeccionPresupuestos.Value = vAprobacionPresupuestos.IdProyeccionPresupuestos.ToString();
            ddlIdRegional.SelectedValue = vAprobacionPresupuestos.IdRegional.ToString();
            ddlIdVigencia.SelectedValue = vAprobacionPresupuestos.IdVigencia.ToString();
            ddlIdRecurso.SelectedValue = vAprobacionPresupuestos.IdRecurso.ToString();
            ddlIdRubro.SelectedValue = vAprobacionPresupuestos.IdRubro.ToString();
            ddlIdArea.SelectedValue = vAprobacionPresupuestos.IdArea.ToString();
            txtValorCupo.Text = vAprobacionPresupuestos.ValorCupo.ToString("C");
            txtTotalCupos.Text = vAprobacionPresupuestos.TotalCupos.ToString("N0");
            //rblAprobado.SelectedValue = vAprobacionPresupuestos.Aprobado.ToString().Trim().ToLower();

            CargarGrillaAdicion();


            int? vIDProyeccionCategoria = null;

            vIDProyeccionCategoria = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);
            var canCategoria = vSIAService.ConsultarProyeccionCategorias(vIDProyeccionCategoria, null);
            int cantidadNecesidades = canCategoria.Sum(x => x.Cantidad);
            //CuposFinalAprobado.Text = cantidadNecesidades.ToString("N0");

            //CuposFinalAprobado.Text = vAprobacionPresupuestos.TotalCupos.ToString("N0");
            //ValorFinalAprobado.Text = vAprobacionPresupuestos.ValorCupo.ToString("N0");



            ObtenerAuditoria(PageName, hfIdProyeccionPresupuestos.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vAprobacionPresupuestos.UsuarioCrea, vAprobacionPresupuestos.FechaCrea, vAprobacionPresupuestos.UsuarioModifica, vAprobacionPresupuestos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void CargarGrillaAdicion()
    {
        ProyeccionPresupuestos vAprobacionPresupuestos = new ProyeccionPresupuestos();
        vAprobacionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(Convert.ToInt32(hfIdProyeccionPresupuestos.Value));
        var myGridResults = vSIAService.ConsultarProyeccionAdicionesIndividual(Convert.ToInt32(hfIdProyeccionPresupuestos.Value), null, null, null,
            null, null, null, "APROB");
        if (myGridResults.Count == 0)
        {
            gvAdiciones.Visible = false;

        }
        else
        {
            gvAdiciones.Visible = true;
            List<AdicionGrillaDatos> datosAdicion = new List<AdicionGrillaDatos>();
            foreach (var dato in myGridResults)
            {
                AdicionGrillaDatos datoAdicion = new AdicionGrillaDatos(vAprobacionPresupuestos.ValorCupo, vAprobacionPresupuestos.ValorInicial, dato.ValorAdicionado, dato.IdAdicionProyeccion, dato.Aprobacion, dato.Rechazado);
                if (!dato.Rechazado)
                    datosAdicion.Add(datoAdicion);

            }


            gvAdiciones.DataSource = datosAdicion;

            gvAdiciones.DataBind();

        }
    }

    protected void btnAprobar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvAdiciones.DataKeys[rowIndex];

            int vResultado;
            AdicionProyeccion vAdicionProyeccion = new AdicionProyeccion();
            vAdicionProyeccion = vSIAService.ConsultarAdicionProyeccion(Convert.ToInt32(dataKey.Value));
            if(vAdicionProyeccion.Aprobado || vAdicionProyeccion.Rechazado)
            {
                toolBar.MostrarMensajeError("Esta adicción ya ha sido Gestionada.");
            }
            else { 
                vAdicionProyeccion.Aprobado = true;
                vAdicionProyeccion.Rechazado = false;
                vAdicionProyeccion.UsuarioModifica = GetSessionUser().NombreUsuario;
                vResultado = vSIAService.ModificarAdicionProyeccion(vAdicionProyeccion);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else
                {
                    toolBar.MostrarMensajeGuardado("La operación se completo satisfactoriamente, verifique por favor.");
                }

                CargarGrillaAdicion();
            }


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void btnRechazar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {

            int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
            var dataKey = gvAdiciones.DataKeys[rowIndex];

            int vResultado;
            AdicionProyeccion vAdicionProyeccion = new AdicionProyeccion();
            vAdicionProyeccion = vSIAService.ConsultarAdicionProyeccion(Convert.ToInt32(dataKey.Value));

            if (vAdicionProyeccion.Aprobado || vAdicionProyeccion.Rechazado)
            {
                toolBar.MostrarMensajeError("Esta adicción ya ha sido Gestionada.");
            }
            else
            {
                SetSessionParameter("PestanaUserControl.Numero", Convert.ToInt32(dataKey.Value));
                Response.Redirect("~/Page/Cupos/AprobacionPresupuesto/Add.aspx");

                CargarGrillaAdicion();

                    }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Aprobación Adicion Proyección ", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlIdRegional.DataSource = vSIAService.ConsultarRegionals(null, null);
            ddlIdRegional.DataTextField = "NombreRegional";
            ddlIdRegional.DataValueField = "IdRegional";
            ddlIdRegional.DataBind();
            ddlIdRegional.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdRecurso.DataSource = vSIAService.ConsultarTipoRecursoFinPptal();
            ddlIdRecurso.DataTextField = "DescTipoRecurso";
            ddlIdRecurso.DataValueField = "IdTipoRecursoFinPptal";
            ddlIdRecurso.DataBind();
            ddlIdRecurso.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdArea.DataSource = vSIAService.ConsultarAreas(null);
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataBind();
            ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdRubro.DataSource = vSIAService.ConsultarRubrosCuposs(null, null, true);
            ddlIdRubro.DataTextField = "DescripcionRubroCompleto";
            ddlIdRubro.DataValueField = "IdRubroCupos";
            ddlIdRubro.DataBind();
            ddlIdRubro.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            //rblAprobado.Items.Insert(0, new ListItem("Aprobar", "true"));
            //rblAprobado.Items.Insert(0, new ListItem("Rechazar", "false"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public class AdicionGrillaDatos{
        public string ValorIncial { get; set; }
        public string ValorAdicionado { get; set; }
        public string ValorFinal { get; set; }
        public string Estado { get; set; }

        public int IDAdicionProyeccion { get; set; }

        public AdicionGrillaDatos(decimal valorCupo, decimal valorInicial, decimal valorAdicionado, int idAdicionProyeccion, bool aprobada, bool rechazado)
        {
            ValorIncial = Convert.ToString(valorInicial);
            ValorAdicionado = Convert.ToString(valorAdicionado);
            if (aprobada)
                ValorFinal = Convert.ToString(valorCupo);
            else
                ValorFinal = Convert.ToString(valorCupo + valorAdicionado);
            IDAdicionProyeccion = idAdicionProyeccion;
            //            Estado= aprobada? "Aprobada" : (rechazado ? "Rechazado":"Espera");
            Estado = aprobada ? "Aprobada" :  "Espera";
        }
    }

    private void Aprobar()
    {
        try
        {
            toolBar.LipiarMensajeError();

            var listSelectTodos = (from item in gvProyeccionCategoria.Rows.Cast<GridViewRow>()
                                   let chkAprobado = (CheckBox)item.FindControl("ChkAprobado")
                                   let chkDevolver = (CheckBox)item.FindControl("ChkDevolver")
                                   where chkAprobado.Checked && chkDevolver.Checked
                                   select new ProyeccionCategoria
                                   {
                                       IDProyeccionCategoria = Convert.ToInt32(gvProyeccionCategoria.DataKeys[item.RowIndex].Values["IDProyeccionCategoria"]),
                                   }).ToList();

            if (listSelectTodos.Count > 0)
            {
                throw new Exception("Seleccione una sola opción Aprobar o Devolver");
            }

            List<ProyeccionCategoria> ListProyeccionCategoria = (from item in gvProyeccionCategoria.Rows.Cast<GridViewRow>()
                                                                 let chkAprobado = (CheckBox)item.FindControl("ChkAprobado")
                                                                 where chkAprobado.Checked
                                                                 select new ProyeccionCategoria
                                                                 {
                                                                     IDProyeccionCategoria = Convert.ToInt32(gvProyeccionCategoria.DataKeys[item.RowIndex].Values["IDProyeccionCategoria"]),
                                                                     CodEstadoProceso = "APROB",
                                                                     Cantidad = Convert.ToInt32(gvProyeccionCategoria.DataKeys[item.RowIndex].Values["Cantidad"]),
                                                                     CantidadAprobada = Convert.ToInt32(gvProyeccionCategoria.DataKeys[item.RowIndex].Values["Cantidad"]),
                                                                     Aprobado = true,
                                                                     DescripcionRechazo = ""
                                                                 }).ToList();

            ListProyeccionCategoria.AddRange((from item in gvProyeccionCategoria.Rows.Cast<GridViewRow>()
                                              let chkDevolver = (CheckBox)item.FindControl("ChkDevolver")
                                              where chkDevolver.Checked
                                              select new ProyeccionCategoria
                                              {
                                                  IDProyeccionCategoria = Convert.ToInt32(gvProyeccionCategoria.DataKeys[item.RowIndex].Value),
                                                  CodEstadoProceso = "RECH",
                                                  Cantidad = Convert.ToInt32(gvProyeccionCategoria.DataKeys[item.RowIndex].Values["Cantidad"]),
                                                  CantidadAprobada = 0,
                                                  Aprobado = false,
                                                  DescripcionRechazo = ""
                                              }).ToList());

            if (ListProyeccionCategoria.Count == 0)
            {
                throw new Exception("Seleccione una opción para procesar el registro");
            }
            
            vSIAService.AprobarProyeccionCategorias(ListProyeccionCategoria, GetSessionUser().NombreUsuario);

            toolBar.MostrarMensajeGuardado();

            if (ListProyeccionCategoria.Count > 0)
                Buscar();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
