﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="DetailCategoria.aspx.cs" Inherits="Page_Cupos_AprobacionPresupuesto_DetailCategoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Regional
            </td>
            <td>Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdRegional" Enabled="false"></asp:DropDownList>
            </>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdVigencia" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">Recurso
            </td>
            <td class="Cell">Área
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdRecurso" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdArea" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">Rubro
            </td>
            <td>Categoria
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell"">
                <asp:DropDownList runat="server" ID="ddlIdRubro" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCategoria" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">Cantidad
            </td> 
            <td>Nivel                
            </td>
        </tr>
        <tr class="rowA"> 
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCantidad" Enabled="false"></asp:TextBox>
            </td>           
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNivel" Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">Aprobar: 
                <asp:RadioButtonList runat="server" ID="rblAprobado" RepeatDirection="Horizontal" ></asp:RadioButtonList>
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Justificaci&oacute;n De La Devoluci&oacute;n *
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" Width="700px" Height="100px"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>