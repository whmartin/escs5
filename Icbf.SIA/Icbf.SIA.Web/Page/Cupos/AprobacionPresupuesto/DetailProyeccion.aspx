<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="DetailProyeccion.aspx.cs" Inherits="Page_Cupos_AprobacionPresupuestos_DetailProyeccion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>Regional
                </td>
                <td>Vigencia
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdRegional" Enabled="false"></asp:DropDownList>
                    </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdVigencia" Enabled="false"></asp:DropDownList>
            </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">Recurso
                </td>
                <td class="Cell">Área
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdRecurso" Enabled="false"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdArea" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">Rubro
                </td>
                <td>Total Necesidades
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdRubro" Enabled="false"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtTotalCupos" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">

                <td>Valor Necesidades                
                </td>
            </tr>
            <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtValorCupo" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProyeccionCategoria" AutoGenerateColumns="False" AllowPaging="True"
                        DataKeyNames="IDProyeccionCategoria, IDProyeccion, Cantidad"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px"
                        OnSorting="gvProyeccionCategoria_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvProyeccionCategoria_PageIndexChanging"
                        OnSelectedIndexChanged="gvProyeccionCategoria_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" SortExpression="Cantidad" />
                            <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion" />
                            <asp:BoundField HeaderText="Nivel" DataField="Nivel" SortExpression="Nivel" />
                            <asp:TemplateField HeaderText="Aprobar">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkAprobado" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Devolver">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkDevolver" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>




  
</asp:Content>
