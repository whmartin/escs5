<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ProyeccionPresupuestos_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <asp:HiddenField ID="hfDesdeModificar" runat="server" />
    <asp:HiddenField ID="hfTieneCupos" runat="server" />
    <asp:HiddenField ID="hfCuposAnteriores" runat="server" />
    <asp:HiddenField ID="hfValorAnterior" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>Vigencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="ddlIdVigencia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVigencia" ControlToValidate="ddlIdVigencia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional" OnSelectedIndexChanged="ddlIdRegional_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Área *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdArea" ControlToValidate="ddlIdArea"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdArea" ControlToValidate="ddlIdArea"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>Recurso *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRecurso" ControlToValidate="ddlIdRecurso"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRecurso" ControlToValidate="ddlIdRecurso"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>


        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdArea"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRecurso"></asp:DropDownList>
            </td>

        </tr>
        <tr class="rowB">
            <td colspan="2">Rubro *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRubro" ControlToValidate="ddlIdRubro"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRubro" ControlToValidate="ddlIdRubro"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdRubro"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Valor Necesidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorCupo" ControlToValidate="txtValorCupo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Total Necesidades *
                <asp:RequiredFieldValidator runat="server" ID="rfvTotalCupos" ControlToValidate="txtTotalCupos"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorCupo" MaxLength="26" data-thousands="." data-decimal=","></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorCupo" runat="server" TargetControlID="txtValorCupo"
                    FilterType="Numbers,Custom" ValidChars=",." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTotalCupos" MaxLength="4"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTotalCupos" runat="server" TargetControlID="txtTotalCupos"
                    FilterType="Numbers" />
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        $("#<%= txtValorCupo.ClientID %>").maskMoney();
    </script>
</asp:Content>
