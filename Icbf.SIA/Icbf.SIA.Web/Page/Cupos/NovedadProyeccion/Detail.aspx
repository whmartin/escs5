<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ProyeccionPresupuestos_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <asp:HiddenField ID="hfDesdeModificar" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Regional
            </td>
            <td>Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Recurso
            </td>
            <td>Área
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRecurso" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdArea" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Rubro
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdRubro" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Valor Necesidad
            </td>
            <td>Total Necesidades
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtValorCupo" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtTotalCupos" Enabled="false"></asp:TextBox>
            </td>
        </tr>

        <tr class="rowB">
            <td colspan="2">
                <asp:Panel runat="Server" ID="PnlMotivoRechazo" Visible="false">
                    <table width="90%">
                        <tr class="rowB">
                            <td >
                                Motivo Rechazo 
                            </td>
                            
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" id="txtMotivoRechazo" TextMode="MultiLine" Width="80%" Height="150px" Enabled="false"></asp:TextBox>
                            </td>
                            
                        </tr>
                    </table>
                </asp:Panel>

            </td>

        </tr>

        <%--<tr class="rowB">
            <td>
                Aprobado
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblAprobado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Usuario Aprobó
            </td>
            <td>
                Fecha Aprobación
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtUsuarioAprobo"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaAprobacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>
    </table>
     <asp:Panel runat="server" ID="pnlLista">
        <table width="80%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto; width: 100%">

                        <asp:GridView runat="server" ID="gvDescripcionRechazo" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="110%" CellPadding="0" Height="16px"
                            OnSorting="gvDescripcionRechazo_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvDescripcionRechazo_PageIndexChanging" OnSelectedIndexChanged="gvDescripcionRechazo_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField HeaderText="Motivo rechazo" DataField="MotivoDevolucion" SortExpression="MotivoDevolucion" />
                                <asp:BoundField HeaderText="Usuario rechazó" DataField="UsuarioRechazo" SortExpression="UsuarioRechazo" />
                                <asp:BoundField HeaderText="Fecha rechazo" DataField="FechaRechazo" SortExpression="FechaRechazo" />
                              
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
    </style>
</asp:Content>

