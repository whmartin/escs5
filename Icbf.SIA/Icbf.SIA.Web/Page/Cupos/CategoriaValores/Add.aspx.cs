using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad CategoriaValores
/// </summary>
public partial class Page_CategoriaValores_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    string PageName = "Cupos/CategoriaValores";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad CategoriaValores
        /// </summary>
    private void Guardar()
    {
        try
        {          
            int vResultado;
            CategoriaValores vCategoriaValores = new CategoriaValores();

            Int32 vNivel = Convert.ToInt32(txtNivel.Text);
            decimal vValorMinimo;

            if (txtValorMinimo.Text.Length > 24)
                throw new Exception("El valor mínimo ingresado supera el valor permitido");

            if (txtValorMaximo.Text.Length > 24)
                throw new Exception("El valor máximo ingresado supera el valor permitido");

            if (Decimal.TryParse(txtValorMinimo.Text,out vValorMinimo) == false)
                throw new Exception("El valor mínimo ingresado supera el valor permitido");

            decimal vValorMaximo;

            if (Decimal.TryParse(txtValorMaximo.Text, out vValorMaximo) == false)
                throw new Exception("El valor máximo ingresado supera el valor permitido");


            if (vNivel > 50)
                throw new Exception("El nivel máximo permitido es de 50");

            if(vValorMinimo > vValorMaximo)
                throw new Exception("El valor mínimo no puede superar el valor máximo");

            vCategoriaValores.IdCategoriaEmpleados = Convert.ToInt32(ddlIdCategoriaEmpleados.SelectedValue);
            vCategoriaValores.Nivel = vNivel;
            vCategoriaValores.ValorMinimo = vValorMinimo;
            vCategoriaValores.ValorMaximo = vValorMaximo;
            vCategoriaValores.IdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            vCategoriaValores.Activo = Convert.ToBoolean(rblActivo.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
                vCategoriaValores.IdCategoriaValor = Convert.ToInt32(hfIdCategoriaValor.Value);
                vCategoriaValores.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCategoriaValores, this.PageName, vSolutionPage);
                vResultado = vSIAService.ModificarCategoriaValores(vCategoriaValores);
            }
            else
            {
                vCategoriaValores.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCategoriaValores, this.PageName, vSolutionPage);
                vResultado = vSIAService.InsertarCategoriaValores(vCategoriaValores);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("CategoriaValores.IdCategoriaValor", vCategoriaValores.IdCategoriaValor);
                SetSessionParameter("CategoriaValores.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Categoría Valores", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdCategoriaValor = Convert.ToInt32(GetSessionParameter("CategoriaValores.IdCategoriaValor"));
            RemoveSessionParameter("CategoriaValores.Id");

            CategoriaValores vCategoriaValores = new CategoriaValores();
            vCategoriaValores = vSIAService.ConsultarCategoriaValores(vIdCategoriaValor);
            hfIdCategoriaValor.Value = vCategoriaValores.IdCategoriaValor.ToString();
            ddlIdCategoriaEmpleados.SelectedValue = vCategoriaValores.IdCategoriaEmpleados.ToString();
            txtNivel.Text = vCategoriaValores.Nivel.ToString();
            txtValorMinimo.Text = vCategoriaValores.ValorMinimo.ToString();
            txtValorMaximo.Text = vCategoriaValores.ValorMaximo.ToString();
            ddlIdVigencia.SelectedValue = vCategoriaValores.IdVigencia.ToString();
            rblActivo.SelectedValue = vCategoriaValores.Activo.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCategoriaValores.UsuarioCrea, vCategoriaValores.FechaCrea, vCategoriaValores.UsuarioModifica, vCategoriaValores.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
         ftValorMinimo.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
         ftValorMaximo.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            rblActivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblActivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblActivo.SelectedValue = "true";

            ddlIdCategoriaEmpleados.DataSource = vSIAService.ConsultarCategoriaEmpleados(null,true,null,null);
            ddlIdCategoriaEmpleados.DataTextField = "Descripcion";
            ddlIdCategoriaEmpleados.DataValueField = "IdCategoria";
            ddlIdCategoriaEmpleados.DataBind();
            ddlIdCategoriaEmpleados.Items.Insert(0,new ListItem("Seleccione>> ", "-1"));

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0,new ListItem("Seleccione>>","-1"));

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
