using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de visualización detallada para la entidad CategoriaValores
/// </summary>
public partial class Page_CategoriaValores_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/CategoriaValores";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("CategoriaValores.IdCategoriaValor", hfIdCategoriaValor.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdCategoriaValor = Convert.ToInt32(GetSessionParameter("CategoriaValores.IdCategoriaValor"));
            RemoveSessionParameter("CategoriaValores.IdCategoriaValor");

            if (GetSessionParameter("CategoriaValores.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("CategoriaValores.Guardado");


            CategoriaValores vCategoriaValores = new CategoriaValores();
            vCategoriaValores = vSIAService.ConsultarCategoriaValores(vIdCategoriaValor);
            hfIdCategoriaValor.Value = vCategoriaValores.IdCategoriaValor.ToString();
            ddlIdCategoriaEmpleados.SelectedValue = vCategoriaValores.IdCategoriaEmpleados.ToString();
            txtNivel.Text = vCategoriaValores.Nivel.ToString();
            txtValorMinimo.Text = vCategoriaValores.ValorMinimo.ToString("C");
            txtValorMaximo.Text = vCategoriaValores.ValorMaximo.ToString("C");
            ddlIdVigencia.SelectedValue = vCategoriaValores.IdVigencia.ToString();
            rblActivo.SelectedValue = vCategoriaValores.Activo.ToString().Trim().ToLower();
            ObtenerAuditoria(PageName, hfIdCategoriaValor.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCategoriaValores.UsuarioCrea, vCategoriaValores.FechaCrea, vCategoriaValores.UsuarioModifica, vCategoriaValores.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdCategoriaValor = Convert.ToInt32(hfIdCategoriaValor.Value);

            CategoriaValores vCategoriaValores = new CategoriaValores();
            vCategoriaValores = vSIAService.ConsultarCategoriaValores(vIdCategoriaValor);
            InformacionAudioria(vCategoriaValores, this.PageName, vSolutionPage);
            int vResultado = vSIAService.EliminarCategoriaValores(vCategoriaValores);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("CategoriaValores.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Categoría Valores", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblActivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblActivo.Items.Insert(0, new ListItem("Inactivo", "false"));

            ddlIdCategoriaEmpleados.DataSource = vSIAService.ConsultarCategoriaEmpleados(null, true,null, null);
            ddlIdCategoriaEmpleados.DataTextField = "Descripcion";
            ddlIdCategoriaEmpleados.DataValueField = "IdCategoria";
            ddlIdCategoriaEmpleados.DataBind();
            ddlIdCategoriaEmpleados.Items.Insert(0, new ListItem("Seleccione>> ", "-1"));

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
