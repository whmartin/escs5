<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_CategoriaValores_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Nivel
            </td>
            <td class="Cell">
                Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNivel" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNivel" runat="server" TargetControlID="txtNivel"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdVigencia"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td  class="Cell" colspan="2">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:RadioButtonList runat="server" ID="rblActivo" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvCategoriaValores" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdCategoriaValor" CellPadding="0" Height="16px"
                        OnSorting="gvCategoriaValores_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvCategoriaValores_PageIndexChanging" OnSelectedIndexChanged="gvCategoriaValores_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nivel" DataField="Nivel"  SortExpression="Nivel"/>
                            <asp:BoundField HeaderText="Vigencia" DataField="AcnioVigencia"  SortExpression="IdVigencia"/>
                            <asp:BoundField HeaderText="Descripci&oacute;n Categoria" DataField="CategoriaEmpleado"  SortExpression="CategoriaEmpleado"/>
                            
                            <asp:TemplateField HeaderText="Estado" SortExpression="Activo">  
                                 <ItemTemplate> 
                                     <asp:Label ID="lblActivo"  runat="server" Text='<%# (bool) Eval("Activo") ? "Activo" : "Inactivo" %>'></asp:Label> 
                                 </ItemTemplate> 
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
