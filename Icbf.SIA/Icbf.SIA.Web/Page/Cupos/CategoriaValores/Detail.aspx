<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_CategoriaValores_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdCategoriaValor" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Categoría / Perfil
            </td>
            <td>
                Nivel
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaEmpleados"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNivel"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Mínimo
            </td>
            <td>
                Valor Máximo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorMinimo"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorMaximo"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia
            </td>
            <td>
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblActivo" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
