<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_CategoriaValores_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIdCategoriaValor" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Categoría / Perfil*
                <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaEmpleados" ControlToValidate="ddlIdCategoriaEmpleados"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdCategoriaEmpleados" ControlToValidate="ddlIdCategoriaEmpleados"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Nivel *
                <asp:RequiredFieldValidator runat="server" ID="rfvNivel" ControlToValidate="txtNivel"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaEmpleados"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNivel" MaxLength="0" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNivel" runat="server" TargetControlID="txtNivel"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Mínimo *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorMinimo" ControlToValidate="txtValorMinimo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Valor Máximo *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorMaximo" ControlToValidate="txtValorMaximo" 
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorMinimo" data-thousands="." data-decimal="," MaxLength="26"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorMinimo" runat="server" TargetControlID="txtValorMinimo"
                    FilterType="Numbers,Custom" ValidChars=",."  />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorMaximo" data-thousands="." data-decimal="," MaxLength="26"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorMaximo" runat="server" TargetControlID="txtValorMaximo"
                    FilterType="Numbers,Custom" ValidChars=",." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvActivo" ControlToValidate="rblActivo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblActivo" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $("#<%= txtValorMinimo.ClientID %>").maskMoney();
        $("#<%= txtValorMaximo.ClientID %>").maskMoney();
    </script>
</asp:Content>

