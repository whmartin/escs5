<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_CategoriaEmpleado_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">

    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Código
            </td >
            <td>
                Descripción              
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCodigo" MaxLength="4" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigo" runat="server" TargetControlID="txtCodigo"
                    FilterType="Numbers"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="100" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ" />
            </td>
        </tr>


        <tr class="rowB">
            <td class="Cell">
                Código del producto
            </td >
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCodigoProducto" MaxLength="15" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoProducto" runat="server" TargetControlID="txtCodigoProducto"
                    FilterType="Numbers" />
            </td>
            <td class="Cell"></td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2" >
            Estado
            </td>
         </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:RadioButtonList runat="server" ID="rblActivo" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvCategoriaEmpleado" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="85%" DataKeyNames="IdCategoria" CellPadding="0" Height="16px"
                        OnSorting="gvCategoriaEmpleado_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvCategoriaEmpleado_PageIndexChanging" OnSelectedIndexChanged="gvCategoriaEmpleado_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código" DataField="Codigo"  SortExpression="Codigo"/>
                            <asp:BoundField HeaderText="Descripción" DataField="Descripcion"  SortExpression="Descripcion"/>
                            <asp:BoundField HeaderText="Código producto" DataField="CodigoProducto"  SortExpression="CodigoProducto"/>  
                            <asp:BoundField HeaderText="Vigencia" DataField="Vigencia"  SortExpression="Vigencia"/>                             
                            <asp:TemplateField HeaderText="Estado" SortExpression="Activo">  
                                 <ItemTemplate> 
                                     <asp:Label ID="lblActivo"  runat="server" Text='<%# (bool) Eval("Activo") ? "Activo" : "Inactivo" %>'></asp:Label> 
                                 </ItemTemplate> 
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
