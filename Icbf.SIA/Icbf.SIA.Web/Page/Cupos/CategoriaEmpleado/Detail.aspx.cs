using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de visualización detallada para la entidad CategoriaEmpleado
/// </summary>
public partial class Page_CategoriaEmpleado_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/CategoriaEmpleado";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("CategoriaEmpleado.IdCategoria", hfIdCategoria.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdCategoria = Convert.ToInt32(GetSessionParameter("CategoriaEmpleado.IdCategoria"));
            RemoveSessionParameter("CategoriaEmpleado.IdCategoria");

            if (GetSessionParameter("CategoriaEmpleado.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("CategoriaEmpleado.Guardado");


            CategoriaEmpleado vCategoriaEmpleado = new CategoriaEmpleado();
            vCategoriaEmpleado = vSIAService.ConsultarCategoriaEmpleado(vIdCategoria);
            hfIdCategoria.Value = vCategoriaEmpleado.IdCategoria.ToString();
            txtDescripcion.Text = vCategoriaEmpleado.Descripcion;
            txtCodigo.Text = vCategoriaEmpleado.Codigo;
            txtCodigoProducto.Text = vCategoriaEmpleado.CodigoProducto;
            ddlIdVigencia.SelectedValue = vCategoriaEmpleado.Vigencia.ToString();
            rblActivo.SelectedValue = vCategoriaEmpleado.Activo.ToString().Trim().ToLower();
            ObtenerAuditoria(PageName, hfIdCategoria.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCategoriaEmpleado.UsuarioCrea, vCategoriaEmpleado.FechaCrea, vCategoriaEmpleado.UsuarioModifica, vCategoriaEmpleado.FechaModifica);

           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdCategoria = Convert.ToInt32(hfIdCategoria.Value);

            CategoriaEmpleado vCategoriaEmpleado = new CategoriaEmpleado();
            vCategoriaEmpleado = vSIAService.ConsultarCategoriaEmpleado(vIdCategoria);
            InformacionAudioria(vCategoriaEmpleado, this.PageName, vSolutionPage);
            int vResultado = vSIAService.EliminarCategoriaEmpleado(vCategoriaEmpleado);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("CategoriaEmpleado.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Categoría Perfil", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();

            rblActivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblActivo.Items.Insert(0, new ListItem("Inactivo", "false"));            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
