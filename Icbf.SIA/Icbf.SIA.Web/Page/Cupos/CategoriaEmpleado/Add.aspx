<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_CategoriaEmpleado_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIdCategoria" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Descripción *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Código *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigo" ControlToValidate="txtCodigo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="100" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,UppercaseLetters,Lowercaseletters" ValidChars="/áéíóúÁÉÍÓÚñÑ" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCodigo" MaxLength="4" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigo" runat="server" TargetControlID="txtCodigo"
                    FilterType="Numbers" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Código del producto*
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoProducto" ControlToValidate="txtCodigoProducto"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">Vigencia *
                  <asp:RequiredFieldValidator runat="server" ID="rfvVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoProducto" MaxLength="15" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoProducto" runat="server" TargetControlID="txtCodigoProducto"
                    FilterType="Numbers" />
            </td>
            <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
                </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvActivo" ControlToValidate="rblActivo"
                 ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblActivo" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
