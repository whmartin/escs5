using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad CategoriaEmpleado
/// </summary>
public partial class Page_CategoriaEmpleado_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    string PageName = "Cupos/CategoriaEmpleado";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad CategoriaEmpleado
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            CategoriaEmpleado vCategoriaEmpleado = new CategoriaEmpleado();

            vCategoriaEmpleado.Descripcion = Convert.ToString(txtDescripcion.Text);
            vCategoriaEmpleado.Codigo = Convert.ToString(txtCodigo.Text);
            vCategoriaEmpleado.CodigoProducto = Convert.ToString(txtCodigoProducto.Text);
            vCategoriaEmpleado.Vigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            vCategoriaEmpleado.Activo = Convert.ToBoolean(rblActivo.Text);

            if (Request.QueryString["oP"] == "E")
            {
            vCategoriaEmpleado.IdCategoria = Convert.ToInt32(hfIdCategoria.Value);
                vCategoriaEmpleado.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCategoriaEmpleado, this.PageName, vSolutionPage);
                vResultado = vSIAService.ModificarCategoriaEmpleado(vCategoriaEmpleado);
            }
            else
            {
                vCategoriaEmpleado.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCategoriaEmpleado, this.PageName, vSolutionPage);
                vResultado = vSIAService.InsertarCategoriaEmpleado(vCategoriaEmpleado);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("CategoriaEmpleado.IdCategoria", vCategoriaEmpleado.IdCategoria);
                SetSessionParameter("CategoriaEmpleado.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Categoría Perfil", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdCategoria = Convert.ToInt32(GetSessionParameter("CategoriaEmpleado.IdCategoria"));
            RemoveSessionParameter("CategoriaEmpleado.Id");

            CategoriaEmpleado vCategoriaEmpleado = new CategoriaEmpleado();
            vCategoriaEmpleado = vSIAService.ConsultarCategoriaEmpleado(vIdCategoria);
            hfIdCategoria.Value = vCategoriaEmpleado.IdCategoria.ToString();
            txtDescripcion.Text = vCategoriaEmpleado.Descripcion;
            txtCodigo.Text = vCategoriaEmpleado.Codigo;
            txtCodigoProducto.Text = vCategoriaEmpleado.CodigoProducto;
            ddlIdVigencia.SelectedValue = vCategoriaEmpleado.Vigencia.ToString();
            rblActivo.SelectedValue = vCategoriaEmpleado.Activo.ToString().Trim().ToLower();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCategoriaEmpleado.UsuarioCrea, vCategoriaEmpleado.FechaCrea, vCategoriaEmpleado.UsuarioModifica, vCategoriaEmpleado.FechaModifica);

            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            rblActivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblActivo.Items.Insert(0, new ListItem("Inactivo", "false"));          
            rblActivo.SelectedValue = "true";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
