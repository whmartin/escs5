﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaProveedoresCupos.aspx.cs" Inherits="Page_Cupos_LupaProveedor_LupaProveedoresCupos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    
<asp:Panel runat="server" ID="Panel1">
    <asp:Panel runat="server" ID="pnlConsulta">
                   
            <table width="90%" align="center">
                 <tr class="rowB">
                    <td>
                       Tipo de Persona
                    </td>
                    <td>
                       Tipo Identificación
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList ID="ddlTipoPersona" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="ddlTipoPersona_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlIdentificacion" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        N&uacute;mero de Documento
                    </td>
                    <td>
                        Proveedor
                        <%--<asp:RequiredFieldValidator runat="server" ID="rfvMunicipio" ControlToValidate="lbxMunicipio"
                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="AgregarLugar"
                             ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox ID="txtNumeroDocumento" runat="server" Width="80%" MaxLength="16"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtNumeroDocumento"
                                                FilterType="Numbers"  />
                        
                    </td>
                    <td >
                        <asp:TextBox ID="txtProveedor" runat="server" Width="80%" MaxLength="200"></asp:TextBox>
                           <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtProveedor"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
                       
                    </td>
                </tr>
               
            </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlLista">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvProveedores" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdEntidad" CellPadding="0" Height="16px"
                                OnSorting="gvProveedores_Sorting" AllowSorting="True" 
                                OnPageIndexChanging="gvProveedores_PageIndexChanging" OnSelectedIndexChanged="gvProveedores_SelectedIndexChanged">
                                <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo de Persona" DataField="TipoPersonaNombre" SortExpression="TipoPersonaNombre"  />
                            <asp:BoundField HeaderText="Tipo de Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                            <asp:TemplateField HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center" SortExpression="Proveedor">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Proveedor")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

</asp:Panel>
</asp:Content>

