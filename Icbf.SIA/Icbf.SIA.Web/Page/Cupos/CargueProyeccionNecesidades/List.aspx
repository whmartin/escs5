<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_CargueProyeccionNecesidades_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
	<script type="text/javascript" language="javascript">
		function limitText(limitField, limitNum) {
			if (limitField.value.length > limitNum) {
				limitField.value = limitField.value.substring(0, limitNum);
			}
		}
	</script>
	<asp:Panel runat="server" ID="pnlConsulta">
	<table width="90%" align="center">
		 <tr class="rowB">
            <td>Archivo*</td>
            <td>
                <asp:Button ID="btnCargar" runat="server" OnClick="btnCargar_Click" Text="Cargar" Width="100px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <input type="file" runat="server" id="fptArchivoCuentasPagar0" name="fptArchivoCuentasPagar"/>
            </td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto; width: 100%">

                        <asp:GridView runat="server" ID="gvErrores" AutoGenerateColumns="False"
                            GridLines="None" Width="99%" CellPadding="0" Height="16px"
                            AllowSorting="True">
                            <Columns>
                                <asp:BoundField HeaderText="Error" DataField="DescripcionError" SortExpression="DescripcionError" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
