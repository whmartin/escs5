using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using System.Data.OleDb;
using System.Data;
using System.IO;

/// <summary>
/// Página de consulta a través de filtros para la entidad CategoriaValores
/// </summary>
public partial class Page_CargueProyeccionNecesidades_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/CargueProyeccionNecesidades";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                //CargarDatosIniciales();
                //if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

        /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Cargue masivo Proyección de Necesidades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnCargar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        HttpPostedFile file = Request.Files[0];
        if (file != null && file.ContentLength > 0)
        {
            string TimeTick = System.DateTime.Now.ToUniversalTime().Ticks.ToString();
            string Extension = System.IO.Path.GetExtension(file.FileName);
            string fname = Path.GetFileName(file.FileName.Replace(Extension, "") + TimeTick + Extension);
            Session["ArchivoNombre"] = file.FileName.Replace(Extension, "") + TimeTick + Extension;
            Session["ArchivoRuta"] = Server.MapPath(Path.Combine("~/Archivos/", fname));
            file.SaveAs(Server.MapPath(Path.Combine("~/Archivos/", fname)));
            var myGridResults= vSIAService.CargarDatosArchivo(CargarXml());
            gvErrores.DataSource = myGridResults;
            gvErrores.DataBind();
            if (gvErrores.PageCount == 0)
                toolBar.MostrarMensajeGuardado();
        }
        else
        {
            toolBar.MostrarMensajeError("Debe especificar el archivo, verifique por favor.");
            return;
        }

    }
    private string CargarXml()
    {
        String ExcelString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" 
                            + Session["ArchivoRuta"].ToString() + ";" 
                            + "Extended Properties='Excel 8.0;HDR=Yes'";

        OleDbConnection _OleDbConnection = new OleDbConnection(ExcelString);
        OleDbCommand _OleDbCommand = new OleDbCommand();
        try
        {
            _OleDbCommand.Connection = _OleDbConnection;
            _OleDbConnection.Open();

            DataTable _DataTable;
            _DataTable = _OleDbConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            _OleDbConnection.Close();

            _OleDbConnection.Open();
            OleDbDataAdapter _OleDbDataAdapter = new OleDbDataAdapter();
            DataSet _DataSet = new DataSet();
            string SheetName = _DataTable.Rows[0]["TABLE_NAME"].ToString();
            _OleDbCommand.CommandText = "SELECT * From [" + SheetName + "]";

            _OleDbDataAdapter.SelectCommand = _OleDbCommand;
            _OleDbDataAdapter.Fill(_DataSet);
            _OleDbConnection.Close();

            return _DataSet.GetXml();
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            _OleDbCommand.Dispose();
            _OleDbConnection.Dispose();
        }
    }

}
