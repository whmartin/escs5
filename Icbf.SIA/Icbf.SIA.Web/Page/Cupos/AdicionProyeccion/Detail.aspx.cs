using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using System.Linq.Expressions;

/// <summary>
/// Página de visualización detallada para la entidad ProyeccionPresupuestos
/// </summary>
public partial class Page_Cupos_AdicionProyeccion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/ProyeccionPresupuestos";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {                
                CargarDatosIniciales();
                CargarDatos();
                CargarGrilla(gvDescripcionRechazo, GridViewSortExpression, true);
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {

        SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", Convert.ToInt32(hfIdProyeccionPresupuestos.Value));

        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {

        int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
        var dataKey = gvAdiciones.DataKeys[rowIndex];

        var vAdicionProyeccion = vSIAService.ConsultarAdicionProyeccion(Convert.ToInt32(dataKey.Value));
        if (vAdicionProyeccion.Aprobado)
        {
            toolBar.MostrarMensajeError("Esta adición no puede editarse ya se encuentra aprobada.");
        }
        else { 
        SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", Convert.ToInt32(dataKey.Value));

        if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
            SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);

        NavigateTo(SolutionPage.Edit);
    }
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        int rowIndex = ((GridViewRow)(((WebControl)(sender)).Parent.Parent)).RowIndex;
        var dataKey = gvAdiciones.DataKeys[rowIndex];
        var vAdicionProyeccion = vSIAService.ConsultarAdicionProyeccion(Convert.ToInt32(dataKey.Value));
        if (vAdicionProyeccion.Aprobado)
        {
            toolBar.MostrarMensajeError("Esta adición no puede editarse ya se encuentra aprobada.");    
        }
        else
        {

            vSIAService.EliminarAdicionProyeccion(vAdicionProyeccion);
            CargarDatos();
        }

        //EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);

        if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
        {
            SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);
            Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
        }
        else
        {
            NavigateTo(SolutionPage.List);
        }
    }

    protected void gvDescripcionRechazo_SelectedIndexChanged(object sender, EventArgs e)
    {
      
    }

    protected void gvAdiciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i =0;
    }
    protected void gvDescripcionRechazo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDescripcionRechazo.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvDescripcionRechazo_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int vIdProyeccion = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);
            var myGridResults = vSIAService.ConsultarHistoricoProyeccionRechazados(vIdProyeccion);
            
            gridViewsender.DataSource = myGridResults;
            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdProyeccionPresupuestos = Convert.ToInt32(GetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos"));
            RemoveSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos");

            hfDesdeModificar.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
            RemoveSessionParameter("PestanaUserControl.Numero");

            if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
            {
                toolBar = (masterPrincipal)this.Master;
                toolBar.MostrarBotonEliminar(false);
            }

            if (GetSessionParameter("ProyeccionPresupuestos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            if (GetSessionParameter("ProyeccionPresupuestos.Guardado").ToString() == "2")
                toolBar.MostrarMensajeGuardado("La proyección tiene asignado un tercero, por favor ajuste");

            RemoveSessionParameter("ProyeccionPresupuestos.Guardado");

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccionPresupuestos);
            //AdicionProyeccion vAdicionProyeccion = new AdicionProyeccion();
            //vAdicionProyeccion = vSIAService.ConsultarAdicionProyeccions(vProyeccionPresupuestos.IdProyeccionPresupuestos, null, null);
            hfIdProyeccionPresupuestos.Value = vProyeccionPresupuestos.IdProyeccionPresupuestos.ToString();
            ddlIdRegional.SelectedValue = vProyeccionPresupuestos.IdRegional.ToString();
            ddlIdVigencia.SelectedValue = vProyeccionPresupuestos.IdVigencia.ToString();
            ddlIdRecurso.SelectedValue = vProyeccionPresupuestos.IdRecurso.ToString();
            ddlIdRubro.SelectedValue = vProyeccionPresupuestos.IdRubro.ToString();
            ddlIdArea.SelectedValue = vProyeccionPresupuestos.IdArea.ToString();
            txtValorCupo.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            txtTotalCupos.Text = vProyeccionPresupuestos.TotalCupos.ToString("N0");
            //txtValorAdicionar.Text = vAdicionProyeccion.ValorAdicionado.ToString("C");

            var myGridResults = vSIAService.ConsultarProyeccionAdicionesIndividual(vIdProyeccionPresupuestos, null, null, null,
             null, null, null, "APROB");
            // vFechaAprobacion, vIdRegional, vIdRubro, "APROB");
            gvAdiciones.Visible = true;
            gvAdiciones.DataSource = myGridResults;
            gvAdiciones.DataBind();

            ObtenerAuditoria(PageName, hfIdProyeccionPresupuestos.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProyeccionPresupuestos.UsuarioCrea, vProyeccionPresupuestos.FechaCrea, vProyeccionPresupuestos.UsuarioModifica, vProyeccionPresupuestos.FechaModifica);

            var conteo = vSIAService.ConsultarProyeccionCategorias(vIdProyeccionPresupuestos, null);
            var adicion = vSIAService.ConsultarAdicionProyeccions(vProyeccionPresupuestos.IdProyeccionPresupuestos, null, null);
            int cantidadNecesidades = conteo.Sum(x => x.Cantidad);

            if (vProyeccionPresupuestos.TotalCupos < cantidadNecesidades)
                toolBar.MostrarMensajeError("Proyección actualizada: Debe redistribuir las categorías");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

  
 
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdProyeccionPresupuestos = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccionPresupuestos);
            InformacionAudioria(vProyeccionPresupuestos, this.PageName, vSolutionPage);
            int vResultado = vSIAService.EliminarProyeccionPresupuestos(vProyeccionPresupuestos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ProyeccionPresupuestos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Adición a la Proyección", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlIdRegional.DataSource = vSIAService.ConsultarRegionals(null, null);
            ddlIdRegional.DataTextField = "NombreRegional";
            ddlIdRegional.DataValueField = "IdRegional";
            ddlIdRegional.DataBind();          

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();

            ddlIdRecurso.DataSource = vSIAService.ConsultarTipoRecursoFinPptal();
            ddlIdRecurso.DataTextField = "DescTipoRecurso";
            ddlIdRecurso.DataValueField = "IdTipoRecursoFinPptal";
            ddlIdRecurso.DataBind();

            ddlIdArea.DataSource = vSIAService.ConsultarGlobalArea(null, null);
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataBind();
        
            ddlIdRubro.DataSource = vSIAService.ConsultarRubrosCuposs(null, null, true);
            ddlIdRubro.DataTextField = "DescripcionRubroCompleto";
            ddlIdRubro.DataValueField = "IdRubroCupos";
            ddlIdRubro.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


   
}
