<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Cupos_AdicionProyeccion_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <asp:HiddenField ID="hfDesdeModificar" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Regional
            </td>
            <td>Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Recurso
            </td>
            <td>Área
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRecurso" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdArea" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Rubro
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdRubro" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>Valor Necesidad
            </td>
            <td>Total Necesidades
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtValorCupo" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtTotalCupos" Enabled="false"></asp:TextBox>
            </td>
        </tr>

        <tr class="rowB">
            <td colspan="2">
                <asp:Panel runat="Server" ID="PnlMotivoRechazo" Visible="false">
                    <table width="90%">
                        <tr class="rowB">
                            <td>Motivo Rechazo 
                            </td>

                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtMotivoRechazo" TextMode="MultiLine" Width="80%" Height="150px" Enabled="false"></asp:TextBox>
                            </td>

                        </tr>
                    </table>
                </asp:Panel>

            </td>

        </tr>

<%--        <tr class="rowB">
            <td>Valor a Adicionar
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtValorAdicionar" Enabled="false"></asp:TextBox>
            </td>

        </tr>--%>
    </table>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="80%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto; width: 100%">

                        <asp:GridView runat="server" ID="gvDescripcionRechazo" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="110%" CellPadding="0" Height="16px"
                            OnSorting="gvDescripcionRechazo_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvDescripcionRechazo_PageIndexChanging" OnSelectedIndexChanged="gvDescripcionRechazo_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField HeaderText="Motivo rechazo" DataField="MotivoDevolucion" SortExpression="MotivoDevolucion" />
                                <asp:BoundField HeaderText="Usuario rechazó" DataField="UsuarioRechazo" SortExpression="UsuarioRechazo" />
                                <asp:BoundField HeaderText="Fecha rechazo" DataField="FechaRechazo" SortExpression="FechaRechazo" />

                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table width="90%" align="center">
        <tr class="rowB">
            <td>Añadir Adicion
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:ImageButton  ID="btnNuevaAdicion" OnClick="btnNuevo_Click" runat="server" CommandName="Select" ImageUrl="~/Image/btn/add.gif"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
            </td>
        </tr>

        </table>
    <asp:Panel runat="server" ID="pnAdiccion">
        <table width="99%" align="center">
            <tr class="rowAG">
                <td>
                    <div  style="overflow-x: auto; width: 100%">
                    <asp:GridView runat="server" ID="gvAdiciones" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="IdAdicionProyeccion"
                        OnSelectedIndexChanged="gvAdiciones_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="Valor Inicial de la Proyección" DataField="ValorInicial" />
                            <asp:BoundField HeaderText="Valor adicionado " DataField="ValorAdicionado" />
                            <asp:BoundField HeaderText="Valor Cupo " DataField="ValorCupo" />
                            <asp:BoundField HeaderText="Aprobado" DataField="Aprobacion" />
                            <asp:BoundField HeaderText="Rechazado" DataField="Rechazado" />
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:ImageButton  ID="btnEditarAdicion"   runat="server" OnClick="btnEditar_Click"  ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Borrar">
                                <ItemTemplate>
                                    <asp:ImageButton  ID="btnEliminarAdicion"  runat="server" OnClick="btnEliminar_Click" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observación" DataField="RazonRechazo" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                        </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
    </style>
</asp:Content>

