﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListCertNoPlanta.aspx.cs" Inherits="Page_Cupos_CertificacionNoPlanta_ListCertNoPlanta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell" style=" width: 50%">Regional</td>
                <td class="Cell" style=" width: 50%">Vigencia</td>
            </tr>
            <tr class="rowA" >
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtRegional" Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdVigencia"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">&Aacute;rea
                </td>
                <td class="Cell">Rubro
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdArea"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtIdRubro"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
                <td class="Cell">Consecutivo Interno
                </td>
                <td class="Cell">Proveedor
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtConsecutivoInterno" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftConsecutivoInterno" runat="server" TargetControlID="txtConsecutivoInterno"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtIdentificacion" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto;width:100%">
                        <asp:GridView runat="server" ID="gvListCupo" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" DataKeyNames="IdCupoArea" CellPadding="0" Height="16px"
                            OnSorting="gvListCupo_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvListCupo_PageIndexChanging" OnSelectedIndexChanged="gvListCupo_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:BoundField HeaderText="Categoría Perfil" DataField="CategoriaEmpleados" SortExpression="CategoriaEmpleados" />                                
                                <asp:BoundField HeaderText="Área" DataField="NombreArea" SortExpression="NombreArea" />
                                <asp:BoundField HeaderText="Proveedor proyectado" DataField="NombreTercero" SortExpression="NombreTercero" />
                               
                                <%--<asp:TemplateField HeaderText="Aprobado" SortExpression="Aprobado">
                                <ItemTemplate>
                                    <asp:Label ID="lblAprobado" runat="server" Text='<%# (bool) Eval("Aprobado") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                <asp:BoundField HeaderText="Usuario Aprobó" DataField="UsuarioAprobo" SortExpression="UsuarioAprobo" />
                                <asp:BoundField HeaderText="Fecha Aprobación" DataField="FechaAprobacion" DataFormatString="{0:d}" SortExpression="FechaAprobacion" />

                                

                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
