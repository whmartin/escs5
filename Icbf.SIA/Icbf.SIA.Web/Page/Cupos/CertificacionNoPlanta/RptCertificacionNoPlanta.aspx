﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" 
    AutoEventWireup="true" CodeFile="RptCertificacionNoPlanta.aspx.cs" 
    Inherits="Page_Cupos_CertificacionNoPlanta_RptCertificacionNoPlanta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
     <asp:HiddenField ID="HfIdCuposArea" runat="server" />
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <table width="90%" align="center">  
        <tr class="rowB">
            <td class="style1" style="width: 50%">
               Revisado por *
                <asp:RequiredFieldValidator runat="server" ID="rfvReviso" ControlToValidate="txtReviso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="style1" style="width: 50%">
               Cargo Revisado por *
                <asp:RequiredFieldValidator runat="server" ID="rfvCargoReviso" ControlToValidate="txtCargoReviso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtReviso" Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftReviso" runat="server" TargetControlID="txtReviso"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCargoReviso" Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCargoReviso" runat="server" TargetControlID="txtCargoReviso"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
        </tr>
    </table>
</asp:Content>


