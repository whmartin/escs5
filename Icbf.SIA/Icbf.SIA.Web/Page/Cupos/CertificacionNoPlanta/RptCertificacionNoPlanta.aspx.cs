﻿using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Cupos_CertificacionNoPlanta_RptCertificacionNoPlanta : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/CertificacionNoPlanta";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;

        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                string vIdProyeccionPresupuestos = GetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposAprobar").ToString();
                RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposAprobar");
                hfIdProyeccionPresupuestos.Value = vIdProyeccionPresupuestos;

                string vIdCupoArea = GetSessionParameter("CupoAreas.IdcuposAreasNecesidadAprobar").ToString();
                RemoveSessionParameter("CupoAreas.IdcuposAreasNecesidadAprobar");
                HfIdCuposArea.Value = vIdCupoArea;


                

            }
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.eventoReporte += ToolBar_eventoReporte;
            toolBar.EstablecerTitulos("Certificaci&oacute;n no planta", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ToolBar_eventoReporte(object sender, EventArgs e)
    {
        GenerarReporte();
    }

    private void GenerarReporte()
    {

        try
        {
            #region Creacion reporte


            //Crea un objeto de tipo reporte
            Report objReport;
            objReport = new Report("CertificacionNoPlanta", true, PageName, "Certificación no planta");


            //Adiciona los parametros al objeto reporte
            objReport.AddParameter("IdCupo", HfIdCuposArea.Value);
            objReport.AddParameter("IdProyeccion", hfIdProyeccionPresupuestos.Value);
            objReport.AddParameter("UsuarioReviso", txtReviso.Text.ToUpper());
            objReport.AddParameter("CargoReviso", txtCargoReviso.Text.ToUpper());
            objReport.AddParameter("UsuarioGenero", GetSessionUser().NombreUsuario);

            //

            //Crea un session con el objeto Reporte
            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
            #endregion

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }


    }
}