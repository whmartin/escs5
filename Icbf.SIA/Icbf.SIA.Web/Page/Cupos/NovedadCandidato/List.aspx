﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_NovedadCandidato_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccion" runat="server" />
    <asp:HiddenField ID="hfPostbck" runat="server" />
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">Regional
                </td>
                <td class="Cell">Vigencia
                </td>

            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlRegional" OnSelectedIndexChanged="ddlRegional_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">Área
                </td>
                <td class="Cell"></td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdArea"></asp:DropDownList>
                </td>
                <td class="Cell"></td>
            </tr>

        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAprobacionPresupuestos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdProyeccionPresupuestos" CellPadding="0" Height="16px"
                        OnSorting="gvAprobacionPresupuestos_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvAprobacionPresupuestos_PageIndexChanging" OnSelectedIndexChanged="gvAprobacionPresupuestos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Vigencia" DataField="AcnoVigencia" SortExpression="AcnoVigencia" />
                            <asp:BoundField HeaderText="Área" DataField="DescrArea" SortExpression="DescrArea" />
                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional" SortExpression="NombreRegional" />
                            <asp:BoundField HeaderText="Valor Necesidad" DataField="ValorCupo" DataFormatString="{0:C2}" SortExpression="ValorCupo" />
                            <asp:BoundField HeaderText="No Total Necesidad" DataField="TotalCupos" DataFormatString="{0:N0}" SortExpression="TotalCupos" />
                            <asp:BoundField HeaderText="Estado" DataField="DescripcionEstado" SortExpression="DescripcionEstado" />
                            <asp:BoundField HeaderText="Usuario Aprobó" />
                            <asp:BoundField HeaderText="Fecha Aprobación" DataField="FechaAprobacion" DataFormatString="{0:d}" SortExpression="FechaAprobacion" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" Enabled="true" HeaderText="Documentos"
                                        Height="16px" ImageUrl="~/Image/btn/list.png" onClick='<%# "GetContratista("+ Eval("IdProyeccionPresupuestos") + "); return false;" %>' ToolTip="Mostrar" Width="16px" />
                                </ItemTemplate>
                                <ItemStyle Width="2%" />
                                <HeaderTemplate>
                                    Ver categorias
                                </HeaderTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function GetContratista(idProyeccion) {
            window_showModalDialog('../../../Page/Cupos/CupoNecesidad/ListDistribucion.aspx?Text=' + idProyeccion, null, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
    </script>
</asp:Content>

