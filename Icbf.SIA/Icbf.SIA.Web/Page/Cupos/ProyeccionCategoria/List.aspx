﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Cupos_ProyeccionCategoria_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">Regional *
                     <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaEmpleados" ControlToValidate="ddlRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdCategoriaEmpleados" ControlToValidate="ddlRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
                <td class="Cell">Vigencia *
                  <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlRegional" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlRegional_SelectedIndexChanged"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
                </td>
            </tr>

            <tr class="rowB">
                <td class="Cell">Área
                </td>
                <td class="Cell">Rubro
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdArea"></asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdRubro"></asp:DropDownList>
                </td>
            </tr>
            <%--<tr class="rowB">          
            <td class="Cell" colspan="2">
                Aprobado
            </td>
        </tr>
        <tr class="rowA">           
            <td class="Cell" colspan="2">
                <asp:RadioButtonList runat="server" ID="rblAprobado" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>--%>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="98%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto; width: 100%">

                        <asp:GridView runat="server" ID="gvProyeccionPresupuestos" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="110%" DataKeyNames="IdProyeccionPresupuestos" CellPadding="0" Height="16px"
                            OnSorting="gvProyeccionPresupuestos_Sorting" AllowSorting="True"
                            OnPageIndexChanging="gvProyeccionPresupuestos_PageIndexChanging" OnSelectedIndexChanged="gvProyeccionPresupuestos_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Vigencia" DataField="AcnoVigencia" SortExpression="AcnoVigencia" />
                                <asp:BoundField HeaderText="Regional" DataField="NombreRegional" SortExpression="NombreRegional" />
                                <asp:BoundField HeaderText="Área" DataField="DescrArea" SortExpression="DescrArea" />
                                <asp:BoundField HeaderText="Proyección" DataField="IdProyeccionPresupuestos" SortExpression="IdProyeccionPresupuestos" />
                                <asp:BoundField HeaderText="Rubro" DataField="CodigoRubro" SortExpression="CodigoRubro" />
                                <asp:BoundField HeaderText="Recurso" DataField="DESCTIPORECURSO" SortExpression="DESCTIPORECURSO" />                                
                                <asp:BoundField HeaderText="N. Total Necesidades" DataField="TotalCupos" DataFormatString="{0:N0}" SortExpression="TotalCupos" />
                                <asp:BoundField HeaderText="N. de Necesidades Asignadas" DataField="NecesidadesAsignadas" DataFormatString="{0:N0}" SortExpression="NecesidadesAsignadas" />
                                <asp:BoundField HeaderText="N. de Necesidades Disponibles" DataField="NecesidadesDisponibles" DataFormatString="{0:N0}" SortExpression="NecesidadesDisponibles" />
                                <asp:BoundField HeaderText="Valor Total Necesidades" DataField="ValorCupo" DataFormatString="{0:C2}" SortExpression="ValorCupo" />
                                <asp:BoundField HeaderText="Estado" DataField="DescripcionEstado" SortExpression="DescripcionEstado" />                                
                                <asp:BoundField HeaderText="Fecha Estado" DataField="FechaModifica" DataFormatString="{0:d}" SortExpression="FechaAprobacion" />
                                <asp:BoundField HeaderText="Usuario" DataField="UsuarioAprobo" SortExpression="UsuarioAprobo" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>