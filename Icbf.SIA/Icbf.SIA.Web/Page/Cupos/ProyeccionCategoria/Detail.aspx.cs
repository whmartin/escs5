using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de visualización detallada para la entidad ProyeccionCategoria
/// </summary>
public partial class Page_ProyeccionCategoria_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/ProyeccionCategoria";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
        SetSessionParameter("ProyeccionCategoria.IdProyeccionCategoria", hfIdProyeccionCategoria.Value);
        SetSessionParameter("ProyeccionCategoria.NecesidadesDisponibles", hfNecDisponibles.Value);
        SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);

        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
        {
            SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
            SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);
            Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
            Response.Redirect("ListDistribucion.aspx");
        }
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIDProyeccionCategoria = Convert.ToInt32(GetSessionParameter("ProyeccionCategoria.IdProyeccionCategoria"));
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccionCategoria");

            ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
            vProyeccionCategoria = vSIAService.ConsultarProyeccionCategoria(vIDProyeccionCategoria, null);
            hfIdProyeccionCategoria.Value = vProyeccionCategoria.IDProyeccionCategoria.ToString();
            hfIdProyeccionPresupuestos.Value = vProyeccionCategoria.IDProyeccion.ToString();
            txtIDCategoria.Text = vProyeccionCategoria.Descripcion.ToString();
            txtCantidad.Text = vProyeccionCategoria.Cantidad.ToString();
            txtNivel.Text = vProyeccionCategoria.Nivel.ToString();
            ObtenerAuditoria(PageName, hfIdProyeccionCategoria.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProyeccionCategoria.UsuarioCrea, vProyeccionCategoria.FechaCrea, vProyeccionCategoria.UsuarioModifica, vProyeccionCategoria.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIDProyeccionCategoria = Convert.ToInt32(hfIdProyeccionCategoria.Value);

            ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
            vProyeccionCategoria = vSIAService.ConsultarProyeccionCategoria(vIDProyeccionCategoria, null);
            InformacionAudioria(vProyeccionCategoria, this.PageName, vSolutionPage);
            int vResultado = vSIAService.EliminarProyeccionCategoria(vProyeccionCategoria);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {               
                SetSessionParameter("ProyeccionCategoria.Eliminado", "1");

                if (string.IsNullOrEmpty(hfDesdeModificar.Value))
                {
                    SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
                    Response.Redirect("ListDistribucion.aspx");
                }
                else
                {
                    SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);
                    SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
                    Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
                }                
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar+= new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.EstablecerTitulos("Proyeccion Categoría", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            hfIdProyeccionPresupuestos.Value = GetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos").ToString();
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos");

            hfDesdeModificar.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
            RemoveSessionParameter("PestanaUserControl.Numero");

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(Convert.ToInt32(hfIdProyeccionPresupuestos.Value));

            txtRegional.Text = vProyeccionPresupuestos.NombreRegional;
            txtIdVigencia.Text = vProyeccionPresupuestos.AcnoVigencia;
            txtIdRubro.Text = vProyeccionPresupuestos.DescripcionRubro;
            txtIdArea.Text = vProyeccionPresupuestos.NombreArea;
            hfNecDisponibles.Value = GetSessionParameter("ProyeccionCategoria.NecesidadesDisponibles").ToString();
                                     RemoveSessionParameter("ProyeccionCategoria.NecesidadesDisponibles");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
