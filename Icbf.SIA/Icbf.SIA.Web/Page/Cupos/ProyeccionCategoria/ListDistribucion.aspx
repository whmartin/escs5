<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListDistribucion.aspx.cs" Inherits="Page_ProyeccionCategoria_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
    <asp:HiddenField ID="txtCantidad" runat="server" ></asp:HiddenField>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell" style=" width: 50%">Regional</td>
                <td class="Cell" style=" width: 50%">Vigencia</td>
            </tr>
            <tr class="rowA" >
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtRegional" Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdVigencia"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">&Aacute;rea
                </td>
                <td class="Cell">Rubro
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdArea"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtIdRubro"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">N. total de necesidades</td>
                <td class="Cell">N. de necesidades asignadas</td>                
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtNecesidades"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                 <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtNecAsignadas"  Width="200px" Enabled="false"></asp:TextBox>
                </td>           
            </tr>
            <tr class="rowB">
                <td class="cell">Valor Necesidad</td>
                <td class="Cell">N. de necesidades disponibles</td>       
            </tr>
            <tr class="rowA"> 
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtValorNecesidad"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtNecDisponibles"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProyeccionCategoria" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDProyeccionCategoria, IdProyeccion" CellPadding="0" Height="16px"
                        OnSorting="gvProyeccionCategoria_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvProyeccionCategoria_PageIndexChanging" OnSelectedIndexChanged="gvProyeccionCategoria_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Categor&iacute;a" DataField="Descripcion"  SortExpression="Descripcion"/>
                            <asp:BoundField HeaderText="Nivel" DataField="Nivel"  SortExpression="Nivel"/>
                            <asp:BoundField HeaderText="Cantidad" DataField="Cantidad"  SortExpression="Cantidad"/>                            
                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="&Aacute;rea" DataField="NombreArea"  SortExpression="NombreArea"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
