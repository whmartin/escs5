using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad ProyeccionCategoria
/// </summary>
public partial class Page_ProyeccionCategoria_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    string PageName = "Cupos/ProyeccionCategoria";
    static bool tieneTercero = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();                
            }
        }
        txtIdArea.Enabled = false;
        txtIdRubro.Enabled = false;
        txtIdVigencia.Enabled = false;
        txtRegional.Enabled = false;        
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(txtCantidad.Text) <= 0)
            toolBar.MostrarMensajeError("La cantidad debe ser mayor a 0 (cero)");
        else if (Convert.ToInt32(ddlIDCategoria.SelectedValue) == -1 || Convert.ToInt32(ddlCategoriaValores.SelectedValue) == -1)
            toolBar.MostrarMensajeError("Seleccione categoria y nivel");
        else
            Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        string strValue = hfIdProyeccionPresupuestos.Value;
        SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", strValue);
        Response.Redirect("ListDistribucion.aspx");
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad ProyeccionCategoria
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ProyeccionCategoria vProyeccionPresupuestos = new ProyeccionCategoria();

            vProyeccionPresupuestos.IdProyeccionPresupuestos = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);
            vProyeccionPresupuestos.IDProyeccion = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);
            vProyeccionPresupuestos.IDCategoria = Convert.ToInt32(ddlIDCategoria.SelectedValue);
            vProyeccionPresupuestos.Cantidad = Convert.ToInt32(txtCantidad.Text);
            vProyeccionPresupuestos.IdCategoriaValor = Convert.ToInt32(ddlCategoriaValores.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {              
                vProyeccionPresupuestos.IDProyeccionCategoria = Convert.ToInt32(hfIdProyeccionCategoria.Value);
                vProyeccionPresupuestos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProyeccionPresupuestos, this.PageName, vSolutionPage);
                vResultado = vSIAService.ModificarProyeccionCategoria(vProyeccionPresupuestos);               
            }
            else
            {
                vProyeccionPresupuestos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vProyeccionPresupuestos, this.PageName, vSolutionPage);
                vResultado = vSIAService.InsertarProyeccionCategoria(vProyeccionPresupuestos);                          
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else
            {   
                if (!string.IsNullOrEmpty(hfIdCategoriaValores.Value))
                {
                    if (tieneTercero == true && Convert.ToInt32(hfIdCategoriaValores.Value) != vProyeccionPresupuestos.IdCategoriaValor)
                        SetSessionParameter("ProyeccionCategoria.Guardado", "2");
                    else
                        SetSessionParameter("ProyeccionCategoria.Guardado", "1");
                }               
                else
                    SetSessionParameter("ProyeccionCategoria.Guardado", "1");                

                if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
                {
                    SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
                    SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);
                    Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
                }                    
                else
                {
                    SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", hfIdProyeccionPresupuestos.Value);
                    Response.Redirect("ListDistribucion.aspx");
                }                                   
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Proyección Categoria", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIDProyeccionCategoria = Convert.ToInt32(GetSessionParameter("ProyeccionCategoria.IdProyeccionCategoria"));
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccionCategoria");

            hfDesdeModificar.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
            RemoveSessionParameter("PestanaUserControl.Numero");

            ProyeccionCategoria vProyeccionCategoria = new ProyeccionCategoria();
            vProyeccionCategoria = vSIAService.ConsultarProyeccionCategoria(vIDProyeccionCategoria, null);
            hfIdProyeccionCategoria.Value = vProyeccionCategoria.IDProyeccionCategoria.ToString();
            txtCantidad.Text = vProyeccionCategoria.Cantidad.ToString();
            ddlIDCategoria.SelectedValue = vProyeccionCategoria.IDCategoria.ToString();
            ddlCategoriaValores.Enabled = true;
            ddlCategoriaValores.DataSource = vSIAService.ConsultarCategoriaValoress(null, null, true, vProyeccionCategoria.IDCategoria);
            ddlCategoriaValores.DataTextField = "Nivel";
            ddlCategoriaValores.DataValueField = "IdCategoriaValor";
            ddlCategoriaValores.DataBind();
            ddlCategoriaValores.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
            ddlCategoriaValores.SelectedValue = vProyeccionCategoria.IdCategoriaValor.ToString();
            hfIdCategoriaValores.Value = vProyeccionCategoria.IdCategoriaValor.ToString();

            CupoAreas vCupo = vSIAService.ConsultarCupoAreasCategoria(vProyeccionCategoria.IDProyeccion, vProyeccionCategoria.IdCategoriaValor);

            if (vCupo.IdCupoArea != 0)
                tieneTercero = true;

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProyeccionCategoria.UsuarioCrea, vProyeccionCategoria.FechaCrea, vProyeccionCategoria.UsuarioModifica, vProyeccionCategoria.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            string strValue = GetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos").ToString();
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos");
            hfIdProyeccionPresupuestos.Value = strValue;

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(Convert.ToInt32(hfIdProyeccionPresupuestos.Value));

            txtRegional.Text = vProyeccionPresupuestos.NombreRegional;
            txtIdVigencia.Text = vProyeccionPresupuestos.AcnoVigencia;
            txtIdRubro.Text = vProyeccionPresupuestos.DescripcionRubro;
            txtIdArea.Text = vProyeccionPresupuestos.NombreArea;
            txtNecDisponibles.Text= GetSessionParameter("ProyeccionCategoria.NecesidadesDisponibles").ToString();
                                    //RemoveSessionParameter("ProyeccionCategoria.NecesidadesDisponibles");           

            ddlIDCategoria.DataSource = vSIAService.ConsultarCategoriaEmpleados(null, true, null, null);
            ddlIDCategoria.DataTextField = "Descripcion";
            ddlIDCategoria.DataValueField = "IdCategoria";
            ddlIDCategoria.DataBind();
            ddlIDCategoria.Items.Insert(0, new ListItem("Seleccione>>", "-1"));            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIDCategoria_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int vIdCategoriaEmpleados = Convert.ToInt32(ddlIDCategoria.SelectedValue);
            ddlCategoriaValores.Enabled = true;
            ddlCategoriaValores.DataSource = vSIAService.ConsultarCategoriaValoress(null, null, true, vIdCategoriaEmpleados);
            ddlCategoriaValores.DataTextField = "Nivel";
            ddlCategoriaValores.DataValueField = "IdCategoriaValor";
            ddlCategoriaValores.DataBind();
            ddlCategoriaValores.Items.Insert(0, new ListItem("Seleccione>>", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
