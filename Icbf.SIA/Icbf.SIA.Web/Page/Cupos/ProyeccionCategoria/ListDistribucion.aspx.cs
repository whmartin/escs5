using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;

/// <summary>
/// Página de consulta a través de filtros para la entidad ProyeccionCategoria
/// </summary>
public partial class Page_ProyeccionCategoria_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/ProyeccionCategoria";
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {    
                CargarDatosIniciales();
                //if (GetState(Page.Master, PageName)) { Buscar(); }
                Buscar();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        //CargarGrilla();

        if (Convert.ToInt32(txtNecDisponibles.Text) <= 0)
        {
            toolBar.MostrarMensajeError("Todas las necesidades han sido asignadas");
        }
        else
        {
            string strValue = hfIdProyeccionPresupuestos.Value;
            SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", strValue);

            string necDisponibles = txtNecDisponibles.Text;
            SetSessionParameter("ProyeccionCategoria.NecesidadesDisponibles", necDisponibles);

            NavigateTo(SolutionPage.Add);
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../Cupos/ProyeccionCategoria/List.aspx");
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {

            CargarGrilla(gvProyeccionCategoria, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvProyeccionCategoria.PageSize = PageSize();
            gvProyeccionCategoria.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Proyección Categoria", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método para redirigir a la página detalle del registro seleccionado 
        /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProyeccionCategoria.DataKeys[rowIndex].Value.ToString();
            string ppidValue = hfIdProyeccionPresupuestos.Value;
            string necDisponibles = txtNecDisponibles.Text;
            SetSessionParameter("ProyeccionCategoria.IdProyeccionCategoria", strValue);
            SetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos", ppidValue);
            SetSessionParameter("ProyeccionCategoria.NecesidadesDisponibles", necDisponibles);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProyeccionCategoria_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProyeccionCategoria.SelectedRow);
    }
    protected void gvProyeccionCategoria_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProyeccionCategoria.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvProyeccionCategoria_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var myGridResults = vSIAService.ConsultarProyeccionCategorias(Convert.ToInt32(hfIdProyeccionPresupuestos.Value), "CREA;RECH");

            if (Convert.ToInt32(txtNecDisponibles.Text) < 0)
                toolBar.MostrarMensajeError("Redistribuya las categorias");

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(ProyeccionCategoria), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ProyeccionCategoria, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList(); 
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            string vIdProyeccionPresupuestos = GetSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos").ToString();
            RemoveSessionParameter("ProyeccionCategoria.IdProyeccionPresupuestos");
            hfIdProyeccionPresupuestos.Value = vIdProyeccionPresupuestos;
            int vIdProyeccion = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);            

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccion);            

            txtRegional.Text = vProyeccionPresupuestos.NombreRegional;
            txtIdVigencia.Text = vProyeccionPresupuestos.AcnoVigencia;
            txtIdRubro.Text = vProyeccionPresupuestos.DescripcionRubro;
            txtIdArea.Text = vProyeccionPresupuestos.NombreArea;
            txtNecesidades.Text = vProyeccionPresupuestos.TotalCupos.ToString();
            txtValorNecesidad.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            List<ProyeccionCategoria> proyeccionesCat = vSIAService.ConsultarProyeccionCategorias(vIdProyeccion, null);

            txtNecAsignadas.Text = proyeccionesCat.Sum(x => x.Cantidad).ToString();
            txtNecDisponibles.Text = (vProyeccionPresupuestos.TotalCupos - Convert.ToInt32(txtNecAsignadas.Text)).ToString();            

            if (GetSessionParameter("ProyeccionCategoria.Guardado").ToString() == "1")
               toolBar.MostrarMensajeGuardado();

            if (GetSessionParameter("ProyeccionCategoria.Guardado").ToString() == "2")
                toolBar.MostrarMensajeGuardado("La proyección modificada estaba asociada a un Tercero, verifique");

            RemoveSessionParameter("ProyeccionCategoria.Guardado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
