<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ProyeccionCategoria_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
<asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
<asp:HiddenField ID="hfNecDisponibles" runat="server" />
<asp:HiddenField ID="hfDesdeModificar" runat="server" />
    <table width="90%" align="center">
         <tr class="rowB">
                <td class="Cell" style=" width: 50%">Regional</td>
                <td class="Cell" style=" width: 50%">Vigencia</td>
            </tr>
            <tr class="rowA" >
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtRegional" Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdVigencia"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
                <td class="Cell">&Aacute;rea
                </td>
                <td class="Cell">Rubro
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdArea"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdRubro"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
       
        <tr class="rowB">
            <td>
                Categoria
            </td>
            <%--<td>
                Categoria
            </td>--%>
            <td colspan="2">
                Nivel
            </td>
        </tr>
        <tr class="rowA">
           
            <td>
                <asp:TextBox runat="server" ID="txtIDCategoria"  Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtNivel" Enabled="false"></asp:TextBox>
            </td>
        </tr>

        <tr class="rowB">
            <td colspan="2">
                Cantidad
            </td>
        </tr>
        <tr class="rowA">           
            <td>
                <asp:TextBox runat="server" ID="txtCantidad"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
