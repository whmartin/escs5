<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ProyeccionCategoria_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

    </script>
<asp:HiddenField ID="hfIdProyeccionCategoria" runat="server" />
<asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
<asp:HiddenField ID="hfDesdeModificar" runat="server" />
<asp:HiddenField ID="hfIdCategoriaValores" runat="server" />
    <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell" style=" width: 50%">Regional</td>
                <td class="Cell" style=" width: 50%">Vigencia</td>
            </tr>
            <tr class="rowA" >
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtRegional" Width="200px"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdVigencia"  Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">&Aacute;rea
                </td>
                <td class="Cell">Rubro
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdArea"  Width="200px"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdRubro"  Width="200px"></asp:TextBox>
                </td>
            </tr>
        </table>
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="cell" style=" width: 50%">
                Categor&iacute;a
            </td>
            <td class="cell" style="width:50%">
                Nivel *
            </td>             
        </tr>
        <tr class="rowA">           
            <td class="cell" style=" width: 50%">
                <asp:DropDownList runat="server" ID="ddlIDCategoria"  Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlIDCategoria_SelectedIndexChanged"></asp:DropDownList>
            </td>
            <td class="cell" style=" width: 50%">
                <asp:DropDownList runat="server" ID="ddlCategoriaValores"  Width="200px" ></asp:DropDownList>
            </td>            
        </tr>
        <tr class="rowB">
            <td class="cell" style=" width: 50%">
               Cantidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvCantidad" ControlToValidate="txtCantidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
             <td class="cell" style=" width: 50%">
                Necesidades disponibles
            </td>
        </tr>
        <tr class="rowA">
            <td class="cell" style=" width: 50%">
                 <asp:TextBox ID="txtCantidad" runat="server"  />
                <Ajax:FilteredTextBoxExtender ID="ftCantidad" runat="server" TargetControlID="txtCantidad"
                    FilterType="Numbers" />
            </td> 
            <td class="cell" style=" width: 50%">
                 <asp:TextBox runat="server" ID="txtNecDisponibles" Width="200px" Enabled="false"></asp:TextBox>
            </td>            
        </tr>
    </table>
</asp:Content>
