﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;
using System.Text;

public partial class Page_AprobarModificacionesCandidato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/NovedadCandidato";
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }


    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvAprobacionPresupuestos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvAprobacionPresupuestos.PageSize = PageSize();
            gvAprobacionPresupuestos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Aprobar Novedades a Candidatos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA");
            int rowIndex = pRow.RowIndex;
            string strValue = gvAprobacionPresupuestos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", strValue);
            Response.Redirect("ListNecesidad.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAprobacionPresupuestos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvAprobacionPresupuestos.SelectedRow);
    }
    protected void gvAprobacionPresupuestos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAprobacionPresupuestos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvAprobacionPresupuestos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int? vIdVigencia = null;
            int? vIdArea = null;
            int? vIdRegional = null;
            
            Decimal? vValorCupo = null;
            int? vTotalCupos = null;
            Boolean? vAprobado = null;
            String vUsuarioAprobo = null;
            DateTime? vFechaAprobacion = null;
            if (ddlIdVigencia.SelectedValue != "-1")
            {
                vIdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            }
            if (ddlIdArea.SelectedValue != "-1")
            {
                vIdArea = Convert.ToInt32(ddlIdArea.SelectedValue);
            }   
            if (ddlRegional.SelectedValue != "-1")
            {
                vIdRegional = Convert.ToInt32(ddlRegional.SelectedValue);
            }
           
            var myGridResults = vSIAService.ConsultarProyeccionPresupuestosAdiciones(vIdVigencia, vIdArea,vUsuarioAprobo, vFechaAprobacion, vIdRegional,null, "APROB");

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(ProyeccionPresupuestos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ProyeccionPresupuestos, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("AprobacionPresupuestos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("AprobacionPresupuestos.Eliminado");
            //rblAprobado.Items.Insert(0, new ListItem("Activo", "true"));
            //rblAprobado.Items.Insert(0, new ListItem("Inactivo", "false"));
            //rblAprobado.Items.Insert(0, new ListItem("Todos", "-1"));
            //rblAprobado.SelectedValue = "-1";

            ddlIdVigencia.DataSource = vSIAService.ConsultarVigencias(true);
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            //ddlIdArea.DataSource = vSIAService.ConsultarAreas(null);
            //ddlIdArea.DataTextField = "Descripcion";
            //ddlIdArea.DataValueField = "IdAreasInternas";
            //ddlIdArea.DataBind();
            //ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlRegional.DataSource = vSIAService.ConsultarRegionals(null, null);
            ddlRegional.DataTextField = "NombreRegional";
            ddlRegional.DataValueField = "IdRegional";
            ddlRegional.DataBind();
            ddlRegional.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

            ddlIdArea.Items.Insert(0, new ListItem("Seleccione Regional", "-1"));
            ddlIdArea.Enabled = false;


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int vIdRegional = Convert.ToInt32(ddlRegional.SelectedValue);
            ddlIdArea.Enabled = true;
            ddlIdArea.DataSource = vSIAService.ConsultarGlobalArea(null, vIdRegional);
            ddlIdArea.DataTextField = "NombreArea";
            ddlIdArea.DataValueField = "IdArea";
            ddlIdArea.DataBind();
            ddlIdArea.Items.Insert(0, new ListItem("Seleccione>>", "-1"));

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnMostrar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;
            if (dataItem != null)
            {
                Show(dataItem);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Show(GridViewRow pRow)
    {
        int rowIndex = pRow.RowIndex;
        string valueId = gvAprobacionPresupuestos.DataKeys[rowIndex].Value.ToString();

        hfIdProyeccion.Value = valueId;
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "GetContratista()", true);
    }
}
