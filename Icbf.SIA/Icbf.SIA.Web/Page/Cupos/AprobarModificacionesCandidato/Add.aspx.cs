using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;
using System.Globalization;

/// <summary>
/// Página de registro y edición para la entidad CupoAreas
/// </summary>
public partial class Page_AprobarModificacionesCandidato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();
    string PageName = "Cupos/NovedadCandidato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        toolBar.LipiarMensajeError();
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
                    CargarRegistro();                    
            }
        }

        if (this.IsPostBack)
        {
            TabName.Value = Request.Form[TabName.UniqueID];
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();                
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("CupoAreas.IvaProveedorSelec");
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {        
        RemoveSessionParameter("CupoAreas.IvaProveedorSelec");
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad CupoAreas
    /// </summary>
    private void Guardar()
    {
        try
        {
            //RemoveSessionParameter("CupoAreas.IvaProveedorSelec");

            //if (ucHonorario.FechaInicialProyectado.ToString("yyyyMMdd") == "19000101")
            //    throw new Exception("Se Debe Seleccionar Una Fecha Inicial de Proyeccion");

            //if (ucHonorario.FechaFinalProyectado.ToString("yyyyMMdd") == "19000101")
            //    throw new Exception("Se Debe Seleccionar Una Fecha final de Proyeccion");

            //ucHonorario.ValidacionesPantalla();

            //ucHonorario.CalcularTotalesHonorario();

            if (string.IsNullOrEmpty(ucDatosTercero.IdEntidad))
                throw new Exception("Debe Seleccionar un Proveedor para Continuar");

            if ((ucHonorario.FechaInicialProyectado.Year.ToString() != txtVigencia.Text))
                throw new Exception("La Fecha inicial proyectada debe estar dentro de la Vigencia Aprobada");

            if (ucHonorario.TiempoProyectadoMese == string.Empty)
                throw new Exception("Presione el boton calcular para continuar");

            if (ucHonorario.TiempoProyectadoDias == string.Empty)
                throw new Exception("Presione el boton calcular para continuar");

            if (string.IsNullOrEmpty(ucDatosContrato.DependenciaKactusValue) || ucDatosContrato.DependenciaKactusValue == "-1")
                throw new Exception("Debe seleccionar una dependencia para continuar");

            if (string.IsNullOrEmpty(ucDatosContrato.ObjetoValue) || ucDatosContrato.ObjetoValue == "-1")
                throw new Exception("Debe seleccionar el objeto del contrato para continuar");

            int? idProveedor = null;
            int idProyeccionPresupuesto = Convert.ToInt32(hfidProyeccionPresupuesto.Value);
            List<CupoAreas> vExiteProveedorCupo = null;

            if (!string.IsNullOrEmpty(ucDatosTercero.IdEntidad))
            {
                idProveedor = Convert.ToInt32(ucDatosTercero.IdEntidad);
                vExiteProveedorCupo = vSIAService.ConsultarCupoAreass(null, idProveedor, null, null, idProyeccionPresupuesto, null);
            }


            int vResultado;
            CupoAreas vCupoAreas = new CupoAreas();

            vCupoAreas.IdProyeccionPresupuestos = idProyeccionPresupuesto;
            vCupoAreas.ConsecutivoInterno = ucDatosContrato.ConsecutivoInterno;
            vCupoAreas.IdObjeto = Convert.ToInt32(ucDatosContrato.ObjetoValue);
            vCupoAreas.IdDependenciaKactus = ucDatosContrato.DependenciaKactusValue;
            vCupoAreas.IdCategoriaEmpleado = Convert.ToInt32(ucHonorario.ddlCategoriaEmpleadoValue);
            vCupoAreas.IdProveedor = idProveedor;
            vCupoAreas.FechaInicialProyectado = Convert.ToDateTime(ucHonorario.FechaInicialProyectado);
            vCupoAreas.FechaFinalProyectado = Convert.ToDateTime(ucHonorario.FechaFinalProyectado);
            vCupoAreas.FechaIngresoICBF = ucDatosTercero.FechaIngresoICBF;
            vCupoAreas.IdCategoriaValores = Convert.ToInt32(ucHonorario.ddlCategoriaValoresValue);
            vCupoAreas.HonorarioBase = Convert.ToDecimal(ucHonorario.HonorarioBase);
            if (!string.IsNullOrEmpty(ucHonorario.PorIvaHonorario))
            {
                vCupoAreas.PorcentajeIVA = Convert.ToDecimal(ucHonorario.PorIvaHonorario);
                vCupoAreas.IvaHonorario = vCupoAreas.HonorarioBase * (vCupoAreas.PorcentajeIVA / 100);
            }
            else
            {
                vCupoAreas.PorcentajeIVA = 0;
                vCupoAreas.IvaHonorario = 0;
            }

            vCupoAreas.TiempoProyectadoMeses = Convert.ToInt32(ucHonorario.TiempoProyectadoMese);
            vCupoAreas.TiempoProyectadoDias = Convert.ToInt32(ucHonorario.TiempoProyectadoDias);
            vCupoAreas.TiempoProyectadoAnios = Convert.ToInt32(ucHonorario.TiempoProyectadoAnio);


            vCupoAreas.TotalHonorarioTiempoProyectado = QuitarFormatoMoneda(ucHonorario.TotalHonorarioTiempo);

            vCupoAreas.IdAdicionProrroga = Convert.ToInt32(GetSessionParameter("AprobacionPresupuestos.IdAdicionProrroga"));
            RemoveSessionParameter("AprobacionPresupuestos.IdAdicionProrroga");



            decimal totalProyectadoExistente = 0;

            if (idProveedor != null)
            {
                if (vExiteProveedorCupo.Count > 0)
                    totalProyectadoExistente = vExiteProveedorCupo[0].TotalHonorarioTiempoProyectado;
            }

            if (QuitarFormatoMoneda(ucHonorario.TotalHonorarioTiempo) > (QuitarFormatoMoneda(txtValorAprobado.Text) + totalProyectadoExistente))
                throw new Exception("Los Honorarios Totales Superan El Valor Aprobado");

            if (ucHonorario.prorrogascheck)
            {

                if (ucHonorario.fechaProrrogaFinal.Date.ToString("yyyyMMdd") == "")
                    throw new Exception("Debe digitar Una Fecha Final Prorroga");

                vCupoAreas.FechaFinalProyectado = ucHonorario.fechaProrrogaFinal;
                if (ucHonorario.fechaProrrogaInicial.Date.ToString("yyyyMMdd") == "")
                    throw new Exception("Debe digitar Una Fecha Incial Prorroga");

                vCupoAreas.FechaInicialProyectado = ucHonorario.fechaProrrogaInicial;



            }

            vCupoAreas.IdCupoArea = Convert.ToInt32(hfIdCupoArea.Value);
            vCupoAreas.UsuarioModifica = GetSessionUser().NombreUsuario;
            InformacionAudioria(vCupoAreas, this.PageName, vSolutionPage);




            AdicionProrrogaCandidato vAdicionProrrogaCandidato = vSIAService.ConsultarAdicionProrrogaCandidato(vCupoAreas.IdAdicionProrroga);
            vAdicionProrrogaCandidato.IdAdicionProrrogaCandidato = vCupoAreas.IdAdicionProrroga;
            vAdicionProrrogaCandidato.Aprobado = false;
            vAdicionProrrogaCandidato.Rechazado = true;
            vAdicionProrrogaCandidato.RazonRechazo = ucHonorario.razonRechazo;
            vAdicionProrrogaCandidato.UsuarioModifica =  GetSessionUser().NombreUsuario; 
            vResultado = vSIAService.ModificarAdicionProrrogaCandidato(vAdicionProrrogaCandidato);

            SetSessionParameter("CupoAreas.Guardado", "1");            

            if (!string.IsNullOrEmpty(hfDesdeModificar.Value))
            {
                SetSessionParameter("PestanaUserControl.Numero", hfDesdeModificar.Value);
                SetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos", hfidProyeccionPresupuesto.Value);
                Response.Redirect("~/Page/Cupos/ModCupoNecesidad/Detail.aspx");
            }
            else
            {
                SetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA", hfidProyeccionPresupuesto.Value);
                SetSessionParameter("CupoAreas.IdcuposAreasNecesidad", vCupoAreas.IdCupoArea);
                NavigateTo(SolutionPage.Detail);
            }                
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private decimal QuitarFormatoMoneda(string pValor)
    {
        decimal digito = decimal.Parse(pValor, NumberStyles.Currency, CultureInfo.CurrentCulture);
        string texto = digito.ToString("G0");
        return Convert.ToDecimal(texto);
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdCupoArea = Convert.ToInt32(GetSessionParameter("CupoAreas.IdcuposAreasNecesidad"));
            RemoveSessionParameter("CupoAreas.IdcuposAreasNecesidad");
            hfIdCupoArea.Value = vIdCupoArea.ToString();

            //hfDesdeModificar.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
            //RemoveSessionParameter("PestanaUserControl.Numero");

            int IdProyeccionPresuCuposA = Convert.ToInt32(hfidProyeccionPresupuesto.Value);

            CupoAreas vCupoAreas = new CupoAreas();
            vCupoAreas = vSIAService.ConsultarCupoAreas(vIdCupoArea);
            ucDatosContrato.ConsecutivoInterno = vCupoAreas.ConsecutivoInterno;
            ucDatosContrato.DependenciaKactusValue = vCupoAreas.IdDependenciaKactus;
            ucDatosContrato.ObjetoValue = vCupoAreas.IdObjeto.ToString();
            var vobjeto = vSIAService.ConsultarObjeto(vCupoAreas.IdObjeto);
            ucDatosContrato.ObjetoText = vobjeto.DescripcionObjeto;
            ucDatosTercero.IdEntidad = vCupoAreas.IdProveedor.ToString();
            ucDatosTercero.LlenarDatosTercro();
            ucHonorario.PorIvaHonorario = vCupoAreas.PorcentajeIVA.ToString();
            ucHonorario.FechaInicialProyectado = vCupoAreas.FechaInicialProyectado;
            ucHonorario.FechaFinalProyectado = vCupoAreas.FechaFinalProyectado;
            //ucHonorario.FechaIngresoICBF = vCupoAreas.FechaIngresoICBF;

            var vCategoriaValores = vSIAService.ConsultarCategoriaValores(vCupoAreas.IdCategoriaValores);
            ucHonorario.ddlCategoriaEmpleadoSource = vSIAService.ConsultarCategoriaEmpleadoAsignada(IdProyeccionPresuCuposA, "APROB");
            ucHonorario.ddlCategoriaEmpleadoValue = vCategoriaValores.IdCategoriaEmpleados.ToString();
            ucHonorario.ddlCategoriaValoresSource = vSIAService.ConsultarCategoriaValoresAsignada(IdProyeccionPresuCuposA, vCategoriaValores.IdCategoriaEmpleados);
            ucHonorario.ddlCategoriaValoresValue = vCupoAreas.IdCategoriaValores.ToString();

            ucHonorario.HonorarioBase = vCupoAreas.HonorarioBase.ToString();
            ucHonorario.IvaHonorario = vCupoAreas.IvaHonorario.ToString("C");
            ucHonorario.TotalHonorario = (vCupoAreas.HonorarioBase + vCupoAreas.IvaHonorario).ToString("C");
            ucHonorario.TiempoProyectadoMese = vCupoAreas.TiempoProyectadoMeses.ToString();
            ucHonorario.TiempoProyectadoDias = vCupoAreas.TiempoProyectadoDias.ToString();
            ucHonorario.TiempoProyectadoAnio = vCupoAreas.TiempoProyectadoAnios.ToString();
            ucHonorario.TotalHonorarioTiempo = vCupoAreas.TotalHonorarioTiempoProyectado.ToString("C");
            
            var vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(IdProyeccionPresuCuposA);
            ucHonorario.txtVigencia = vProyeccionPresupuestos.IdVigencia.ToString();
            ucHonorario.IdProyeccion = IdProyeccionPresuCuposA.ToString();
            ucHonorario.DeshabilitarAddAprob();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatos()
    {
        try
        {

            ucHonorario.ModoConsulta(false);
            ucDatosContrato.ModoConsulta(false);
            ucDatosTercero.ModoConsulta(false);
            int IdProyeccionPresuCuposA = Convert.ToInt32(hfidProyeccionPresupuesto.Value); //Convert.ToInt32(GetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA"));
            //RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA");

            //hfidProyeccionPresupuesto.Value = IdProyeccionPresuCuposA.ToString();
            CupoAreas vCupoAreas = new CupoAreas();
            //vCupoAreas = vSIAService.ConsultarCupoAreas(vIdCupoArea);
            //hfIdCupoArea.Value = vCupoAreas.IdCupoArea.ToString();
            var vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(IdProyeccionPresuCuposA);
            var regionalcupo = vSIAService.ConsultarRegional(vProyeccionPresupuestos.IdRegional);
            var vVigencia = vSIAService.ConsultarVigencia(vProyeccionPresupuestos.IdVigencia);
            var vAreas = vSIAService.ConsultarAreas(vProyeccionPresupuestos.IdArea);
            var rubro = vSIAService.ConsultarRubrosCupos(vProyeccionPresupuestos.IdRubro);
            hfiCodgioRegional.Value = regionalcupo.CodigoRegional;
            txtRegional.Text = regionalcupo.NombreRegional;
            txtVigencia.Text = vVigencia.AcnoVigencia.ToString();

            ucHonorario.txtVigencia = vProyeccionPresupuestos.IdVigencia.ToString();

            //txtTotalCupo.Text = vProyeccionPresupuestos.TotalCupos.ToString("N");
            //txtValorAprobado.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            txtRubro.Text = rubro.CodigoRubro;
            if (vAreas.Count > 0)
                txtArea.Text = vAreas[0].NombreArea;
            ucDatosContrato.DependenciaKactusSource = vContratoService.ConsultarDependenciaSolicitante(null, null, regionalcupo.CodigoRegional);
            ucDatosContrato.ObjetoSource = vSIAService.ConsultarObjetos(null, null, true);

            ucHonorario.ddlCategoriaEmpleadoSource = vSIAService.ConsultarCategoriaEmpleadoAsignada(IdProyeccionPresuCuposA, "APROB");
            ucHonorario.IdProyeccion = IdProyeccionPresuCuposA.ToString();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCupoAreas.UsuarioCrea, vCupoAreas.FechaCrea, vCupoAreas.UsuarioModifica, vCupoAreas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int IdProyeccionPresuCuposA = Convert.ToInt32(GetSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA"));
            RemoveSessionParameter("AprobacionPresupuestos.IdProyeccionPresuCuposA");

            hfidProyeccionPresupuesto.Value = IdProyeccionPresuCuposA.ToString();
            CupoAreas vCupoAreas = new CupoAreas();
            var vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(IdProyeccionPresuCuposA);
            var regionalcupo = vSIAService.ConsultarRegional(vProyeccionPresupuestos.IdRegional);
            var vVigencia = vSIAService.ConsultarVigencia(vProyeccionPresupuestos.IdVigencia);
            var vAreas = vSIAService.ConsultarAreas(vProyeccionPresupuestos.IdArea);
            var rubro = vSIAService.ConsultarRubrosCupos(vProyeccionPresupuestos.IdRubro);
            hfiCodgioRegional.Value = regionalcupo.CodigoRegional;
            txtRegional.Text = regionalcupo.NombreRegional;
            txtVigencia.Text = vVigencia.AcnoVigencia.ToString();

            var CuposAreasASignadas = vSIAService.ConsultarCupoAreass(null, null, null
                                  , null, IdProyeccionPresuCuposA, null);

            decimal vTotalValorAsignado = CuposAreasASignadas.Sum(x => x.TotalHonorarioTiempoProyectado);
            int vTotalCuposAsignado = CuposAreasASignadas.Count();
            txtValorAprobado.Text = (vProyeccionPresupuestos.ValorCupo - vTotalValorAsignado).ToString("C");
            txtTotalCupo.Text =(vProyeccionPresupuestos.TotalCupos - vTotalCuposAsignado).ToString("N0");

            //txtTotalCupo.Text = vProyeccionPresupuestos.TotalCupos.ToString("N");
            //txtValorAprobado.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            txtRubro.Text = rubro.CodigoRubro;
            if (vAreas.Count > 0)
                txtArea.Text = vAreas[0].NombreArea;
            ucDatosContrato.DependenciaKactusSource = vContratoService.ConsultarDependenciaSolicitante(null, null, regionalcupo.CodigoRegional);
            ucDatosContrato.ObjetoSource = vSIAService.ConsultarObjetos(null, null, true);
            //ucHonorario.ddlIdCategoriaValoresSource = vSIAService.ConsultarCategoriaEmpleados(null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}