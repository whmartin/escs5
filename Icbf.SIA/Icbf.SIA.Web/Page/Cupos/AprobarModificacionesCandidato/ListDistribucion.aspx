﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="ListDistribucion.aspx.cs" Inherits="Page_Cupos_AprobarModificacionesCandidato_ListDistribucion" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProyeccion" runat="server" />
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedores" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDProyeccion" CellPadding="0" Height="16px"
                        OnSorting="gvProveedores_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvProveedores_PageIndexChanging" OnSelectedIndexChanged="gvProveedores_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="Categor&iacute;a" DataField="Descripcion" SortExpression="Descripcion" />
                            <asp:BoundField HeaderText="Nivel" DataField="Nivel" SortExpression="Nivel" />
                            <asp:BoundField HeaderText="Cantidad Proyectada" DataField="Cantidad" SortExpression="Cantidad" />
                            <asp:BoundField HeaderText="Cantidad Asignada" DataField="CantidadAsignada" SortExpression="CantidadAsignada" />
                            <asp:BoundField HeaderText="Cantidad Disponible" DataField="CantidadDisponible" SortExpression="CantidadDisponible" />
                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional" SortExpression="NombreRegional" />
                            <asp:BoundField HeaderText="&Aacute;rea" DataField="NombreArea" SortExpression="NombreArea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
