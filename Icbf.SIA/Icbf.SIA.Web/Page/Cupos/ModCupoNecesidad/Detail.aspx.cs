using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Contrato.Service;

/// <summary>
/// Página de visualización detallada para la entidad CupoAreas
/// </summary>
public partial class Page_ModCupoAreas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Cupos/ModCupoNecesidad";
    SIAService vSIAService = new SIAService();    
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
                TabName.Value = GetSessionParameter("PestanaUserControl.Numero").ToString();
                RemoveSessionParameter("PestanaUserControl.Numero");
            }
            else
            {
                TabName.Value = Request.Form[TabName.UniqueID];
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void CargarDatos()
    {
        try
        {
            int vIdProyeccion = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccion);

            ucProyeccion.IdProyeccion = hfIdProyeccionPresupuestos.Value;
            ucProyeccion.RegionalSource = vSIAService.ConsultarRegionals(null, null);
            ucProyeccion.RegionalValue = vProyeccionPresupuestos.IdRegional.ToString();
            ucProyeccion.AreaSource = vSIAService.ConsultarGlobalArea(null, null);
            ucProyeccion.AreaValue = vProyeccionPresupuestos.IdArea.ToString();
            ucProyeccion.VigenciaSource = vSIAService.ConsultarVigencias(true);
            ucProyeccion.VigenciaValue = vProyeccionPresupuestos.IdVigencia.ToString();
            ucProyeccion.RecursoSource = vSIAService.ConsultarTipoRecursoFinPptal();
            ucProyeccion.RecursoValue = vProyeccionPresupuestos.IdRecurso.ToString();
            ucProyeccion.RubroSource = vSIAService.ConsultarRubrosCuposs(null, null, true);
            ucProyeccion.RubroValue = vProyeccionPresupuestos.IdRubro.ToString();
            ucProyeccion.ValorNecesidad = vProyeccionPresupuestos.ValorCupo.ToString("C");
            ucProyeccion.TotalNecesidades = vProyeccionPresupuestos.TotalCupos.ToString();

            ucCategorias.IdProyeccion = hfIdProyeccionPresupuestos.Value;
            ucCandidatos.IdProyeccion = hfIdProyeccionPresupuestos.Value;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
     
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Modificación Proyección Necesidades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            string vIdProyeccionPresupuestos = GetSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos").ToString();
            RemoveSessionParameter("ProyeccionPresupuestos.IdProyeccionPresupuestos");
            hfIdProyeccionPresupuestos.Value = vIdProyeccionPresupuestos;
            int vIdProyeccion = Convert.ToInt32(hfIdProyeccionPresupuestos.Value);

            ProyeccionPresupuestos vProyeccionPresupuestos = new ProyeccionPresupuestos();
            vProyeccionPresupuestos = vSIAService.ConsultarProyeccionPresupuestos(vIdProyeccion);

            txtRegional.Text = vProyeccionPresupuestos.NombreRegional;
            txtIdVigencia.Text = vProyeccionPresupuestos.AcnoVigencia;
            txtIdRubro.Text = vProyeccionPresupuestos.DescripcionRubro;
            txtIdArea.Text = vProyeccionPresupuestos.NombreArea;
            txtNecesidades.Text = vProyeccionPresupuestos.TotalCupos.ToString();
            txtValorNecesidad.Text = vProyeccionPresupuestos.ValorCupo.ToString("C");
            txtNecAsignadas.Text = vSIAService.ConsultarProyeccionCategorias(vIdProyeccion, null).Sum(x => x.Cantidad).ToString();
            txtNecDisponibles.Text = (vProyeccionPresupuestos.TotalCupos - Convert.ToInt32(txtNecAsignadas.Text)).ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}