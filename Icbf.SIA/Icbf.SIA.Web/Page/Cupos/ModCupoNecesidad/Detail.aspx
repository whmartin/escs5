<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ModCupoAreas_Detail" %>

<%@ Register Src="../UserControlModProyeccion/ucProyeccion.ascx" TagName="DatosProyeccion" TagPrefix = "uc1" %>
<%@ Register Src="../UserControlModProyeccion/ucCategorias.ascx" TagName="DatosCategorias" TagPrefix = "uc1" %>
<%@ Register Src="../UserControlModProyeccion/ucCandidatos.ascx" TagName="DatosCandidatos" TagPrefix = "uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdCupoArea" runat="server" />   
    <asp:HiddenField ID="TabName" runat="server" />
    <asp:HiddenField ID="hfiCodgioRegional" runat="server" />
     <asp:HiddenField ID="hfIdProyeccionPresupuestos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
                <td class="Cell" style=" width: 50%">Regional</td>
                <td class="Cell" style=" width: 50%">Vigencia</td>
            </tr>
            <tr class="rowA" >
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtRegional" Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdVigencia"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">&Aacute;rea
                </td>
                <td class="Cell">Rubro
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtIdArea"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtIdRubro"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">N. total de necesidades</td>
                <td class="Cell">N. de necesidades asignadas</td>                
            </tr>
            <tr class="rowA">
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtNecesidades"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                 <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtNecAsignadas"  Width="200px" Enabled="false"></asp:TextBox>
                </td>           
            </tr>
            <tr class="rowB">
                <td class="cell">Valor Necesidad</td>
                <td class="Cell">N. de necesidades disponibles</td>       
            </tr>
            <tr class="rowA"> 
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtValorNecesidad"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
                <td class="Cell" style=" width: 50%">
                    <asp:TextBox runat="server" ID="txtNecDisponibles"  Width="200px" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        <tr>
            <td colspan="2">
                <div id="tabs">
                    <ul>
                        <li><a href="#dvDatosProyeccion" data-tab-index="0">Datos Proyecci&oacute;n</a></li>
                        <li><a href="#dvDatosCategorias" data-tab-index="1">Datos Categor&iacute;as</a></li>
                        <li><a href="#dvDatosCandidatos" data-tab-index="2">Datos Candidatos</a></li>
                    </ul>                    
                    <div id="dvDatosProyeccion">
                        <uc1:DatosProyeccion ID="ucProyeccion" runat="server" />
                    </div>
                    <div id="dvDatosCategorias">
                        <uc1:DatosCategorias ID="ucCategorias" runat="server" />
                    </div>
                    <div id="dvDatosCandidatos">
                        <uc1:DatosCandidatos ID="ucCandidatos" runat="server" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="../../../Scripts/jquery-ui.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#tabs").tabs();
                        
            $('#tabs a').click(function (event) {
                
                if ($(this).data("tab-index") != undefined) {
                //if ($("[id*=TabName]").val() != $(this).data("tab-index")) {
                    $("[id*=TabName]").val($(this).data("tab-index"));
                }
            });

            activaTab();

        });

        function activaTab() {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "dvDatosProyeccion";            
            $('#tabs').tabs("option", "active",tabName);            
        };
    </script>

</asp:Content>
