<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" 
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ModCupoAreas_Add" %>

<%@ Register Src="../UserControlCupArea/DatosContrato.ascx" TagName="DatosContrato" TagPrefix="uc1" %>

<%@ Register Src="../UserControlCupArea/ucDatosTercero.ascx" TagName="DatosTercero" TagPrefix="uc1" %>
<%@ Register Src="../UserControlCupArea/ucHonorario.ascx" TagName="Honorario" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">



    <%--<script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
     
    </script>--%>



    <asp:HiddenField ID="hfIdCupoArea" runat="server" />
    <asp:HiddenField ID="TabName" runat="server" />
    <asp:HiddenField ID="hfiCodgioRegional" runat="server" />
     <asp:HiddenField ID="hfidProyeccionPresupuesto" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Regional
            </td>
            <td>Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <%--<asp:TextBox runat="server" ID="txtIdProyeccion" MaxLength="0"></asp:TextBox>--%>
                <asp:TextBox runat="server" ID="txtRegional" Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVigencia" Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Rubro
            </td>
            <td>&Aacute;rea
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRubro" Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtArea" Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Total Proyecci&oacute;n Necesidad                
            </td>
            <td>Valor Aprobado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTotalCupo" Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAprobado" Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>

      

        <tr>
            <td colspan="2">
                <div id="tabs">
                    <ul>
                        <li><a href="#dvDatosContrato" data-tab-index="0">Datos Contrato</a></li>
                        <li><a href="#dvDatosTercero" data-tab-index="1">Datos Tercero</a></li>
                        <li><a href="#dvHonorarios" data-tab-index="2">Honorarios / Tiempo Proyecto</a></li>
                        <%--<li><a href="#dvTiempoProyecto" data-tab-index="3"></a></li>--%>
                    </ul>
                    <div id="dvDatosContrato">
                        <uc1:DatosContrato ID="ucDatosContrato" runat="server" />
                    </div>
                    <div id="dvDatosTercero">
                        <uc1:DatosTercero ID="ucDatosTercero" runat="server" />
                    </div>
                    <div id="dvHonorarios">
                        <uc1:Honorario ID="ucHonorario" runat="server" />
                    </div>
                    <%--<div id="dvTiempoProyecto">
                        <uc1:TiempoProyectado ID="ucTiempoProyectado" runat="server" />
                    </div>--%>
                </div>

                <%-- <div id="tabs" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Personal
                        </a></li>
                        <li><a href="#employment" aria-controls="employment" role="tab" data-toggle="tab">Employment</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" style="padding-top: 20px">
                        <div role="tabpanel" class="tab-pane active" id="personal">
                            This is Personal Information Tab
                        </div>
                        <div role="tabpanel" class="tab-pane" id="employment">
                            <uc1:DatosTercero ID="ucDatosTercero" runat="server" />
                                                    </div>
                    </div>
                </div>--%>

            </td>
        </tr>
    </table>

    <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="../../../Scripts/jquery-ui.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#tabs").tabs();
                        
            $('#tabs a').click(function (event) {
                
                if ($(this).data("tab-index") != undefined) {
                //if ($("[id*=TabName]").val() != $(this).data("tab-index")) {
                    $("[id*=TabName]").val($(this).data("tab-index"));
                }
            });

            activaTab();

        });

        function activaTab() {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "dvDatosContrato";            
            $('#tabs').tabs("option", "active",tabName);            
        };
    </script>

</asp:Content>
