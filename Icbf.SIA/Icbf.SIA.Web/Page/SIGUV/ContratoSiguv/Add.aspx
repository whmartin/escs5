<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ContratoSiguv_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContratoSiguv" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegionalPCI" ControlToValidate="ddlIdRegionalPCI"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegionalPCI" ControlToValidate="ddlIdRegionalPCI"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                MacroRegional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdMacroRegional" ControlToValidate="ddlIdMacroRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdMacroRegional" ControlToValidate="ddlIdMacroRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegionalPCI"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdMacroRegional"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="ddlIdContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdContrato" ControlToValidate="ddlIdContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdContrato"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
