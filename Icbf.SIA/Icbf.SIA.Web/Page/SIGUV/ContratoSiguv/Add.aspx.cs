using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_ContratoSiguv_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIGUVService vSIGUVService = new SIGUVService();
    string PageName = "SIGUV/ContratoSiguv";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            ContratoSiguv vContratoSiguv = new ContratoSiguv();

            vContratoSiguv.IdRegionalPCI = Convert.ToInt32(ddlIdRegionalPCI.SelectedValue);
            vContratoSiguv.IdMacroRegional = Convert.ToInt32(ddlIdMacroRegional.SelectedValue);
            vContratoSiguv.IdContrato = Convert.ToInt32(ddlIdContrato.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vContratoSiguv.IdContratoSiguv = Convert.ToInt32(hfIdContratoSiguv.Value);
                vContratoSiguv.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContratoSiguv, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.ModificarContratoSiguv(vContratoSiguv);
            }
            else
            {
                vContratoSiguv.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContratoSiguv, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.InsertarContratoSiguv(vContratoSiguv);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ContratoSiguv.IdContratoSiguv", vContratoSiguv.IdContratoSiguv);
                SetSessionParameter("ContratoSiguv.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("ContratoSiguv", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdContratoSiguv = Convert.ToInt32(GetSessionParameter("ContratoSiguv.IdContratoSiguv"));
            RemoveSessionParameter("ContratoSiguv.Id");

            ContratoSiguv vContratoSiguv = new ContratoSiguv();
            vContratoSiguv = vSIGUVService.ConsultarContratoSiguv(vIdContratoSiguv);
            hfIdContratoSiguv.Value = vContratoSiguv.IdContratoSiguv.ToString();
            ddlIdRegionalPCI.SelectedValue = vContratoSiguv.IdRegionalPCI.ToString();
            ddlIdMacroRegional.SelectedValue = vContratoSiguv.IdMacroRegional.ToString();
            ddlIdContrato.SelectedValue = vContratoSiguv.IdContrato.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContratoSiguv.UsuarioCrea, vContratoSiguv.FechaCrea, vContratoSiguv.UsuarioModifica, vContratoSiguv.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdRegionalPCI.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdRegionalPCI.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdMacroRegional.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdMacroRegional.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContrato.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
