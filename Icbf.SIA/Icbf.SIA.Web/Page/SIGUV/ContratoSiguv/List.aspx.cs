using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_ContratoSiguv_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/ContratoSiguv";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdRegionalPCI = null;
            int? vIdMacroRegional = null;
            int? vIdContrato = null;
            if (ddlIdRegionalPCI.SelectedValue!= "-1")
            {
                vIdRegionalPCI = Convert.ToInt32(ddlIdRegionalPCI.SelectedValue);
            }
            if (ddlIdMacroRegional.SelectedValue!= "-1")
            {
                vIdMacroRegional = Convert.ToInt32(ddlIdMacroRegional.SelectedValue);
            }
            if (ddlIdContrato.SelectedValue!= "-1")
            {
                vIdContrato = Convert.ToInt32(ddlIdContrato.SelectedValue);
            }
            gvContratoSiguv.DataSource = vSIGUVService.ConsultarContratoSiguvs( vIdRegionalPCI, vIdMacroRegional, vIdContrato);
            gvContratoSiguv.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvContratoSiguv.PageSize = PageSize();
            gvContratoSiguv.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("ContratoSiguv", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContratoSiguv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ContratoSiguv.IdContratoSiguv", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContratoSiguv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContratoSiguv.SelectedRow);
    }
    protected void gvContratoSiguv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratoSiguv.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ContratoSiguv.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ContratoSiguv.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdRegionalPCI.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdRegionalPCI.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdMacroRegional.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdMacroRegional.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContrato.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
