<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ContratoSiguv_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContratoSiguv" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Regional *
            </td>
            <td>
                MacroRegional *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegionalPCI"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdMacroRegional"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdContrato"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
