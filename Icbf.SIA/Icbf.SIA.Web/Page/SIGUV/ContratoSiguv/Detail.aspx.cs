using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_ContratoSiguv_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/ContratoSiguv";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ContratoSiguv.IdContratoSiguv", hfIdContratoSiguv.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdContratoSiguv = Convert.ToInt32(GetSessionParameter("ContratoSiguv.IdContratoSiguv"));
            RemoveSessionParameter("ContratoSiguv.IdContratoSiguv");

            if (GetSessionParameter("ContratoSiguv.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ContratoSiguv");


            ContratoSiguv vContratoSiguv = new ContratoSiguv();
            vContratoSiguv = vSIGUVService.ConsultarContratoSiguv(vIdContratoSiguv);
            hfIdContratoSiguv.Value = vContratoSiguv.IdContratoSiguv.ToString();
            ddlIdRegionalPCI.SelectedValue = vContratoSiguv.IdRegionalPCI.ToString();
            ddlIdMacroRegional.SelectedValue = vContratoSiguv.IdMacroRegional.ToString();
            ddlIdContrato.SelectedValue = vContratoSiguv.IdContrato.ToString();
            ObtenerAuditoria(PageName, hfIdContratoSiguv.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContratoSiguv.UsuarioCrea, vContratoSiguv.FechaCrea, vContratoSiguv.UsuarioModifica, vContratoSiguv.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdContratoSiguv = Convert.ToInt32(hfIdContratoSiguv.Value);

            ContratoSiguv vContratoSiguv = new ContratoSiguv();
            vContratoSiguv = vSIGUVService.ConsultarContratoSiguv(vIdContratoSiguv);
            InformacionAudioria(vContratoSiguv, this.PageName, vSolutionPage);
            int vResultado = vSIGUVService.EliminarContratoSiguv(vContratoSiguv);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ContratoSiguv.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("ContratoSiguv", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIdRegionalPCI.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdRegionalPCI.SelectedValue = "-1";
            ddlIdMacroRegional.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdMacroRegional.SelectedValue = "-1";
            ddlIdContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContrato.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
