using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;
using System.Data;
using System.Linq.Expressions;

public partial class Page_Vehiculos_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/Vehiculos";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        LlenarGrilla();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private List<Vehiculos> Buscar()
    {
        try
        {
            int? vPropio = null;
            int? vRelevo = null;
            int? vIdClase = null;
            String vPlaca = null;
            int? vCilindraje = null;
            int? vModelo = null;
            int? vCapacidad = null;
            int? vEstado = null;
            if (ddlPropio.SelectedValue != "-1")
            {
                vPropio = Convert.ToInt32(ddlPropio.SelectedValue);
            }
            if (ddlIdClase.SelectedValue != "-1")
            {
                vRelevo = Convert.ToInt32(ddlRelevo.SelectedValue);
            }
            if (ddlIdClase.SelectedValue != "-1")
            {
                vIdClase = Convert.ToInt32(ddlIdClase.SelectedValue);
            }
            if (txtPlaca.Text != "")
            {
                vPlaca = Convert.ToString(txtPlaca.Text);
            }
            if (txtCilindraje.Text != "")
            {
                vCilindraje = Convert.ToInt32(txtCilindraje.Text);
            }
            if (txtModelo.Text != "")
            {
                vModelo = Convert.ToInt32(txtModelo.Text);
            }
            if (txtCapacidad.Text != "")
            {
                vCapacidad = Convert.ToInt32(txtCapacidad.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            List<Vehiculos> ListaVehiculos = vSIGUVService.ConsultarVehiculoss(vPropio, vRelevo, vIdClase, vPlaca, vCilindraje, vModelo, vCapacidad, vEstado);
            return ListaVehiculos;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return null;
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvVehiculos.PageSize = PageSize();
            gvVehiculos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Veh&iacute;culos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvVehiculos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Vehiculos.IdVehiculo", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvVehiculos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvVehiculos.SelectedRow);
    }
    protected void gvVehiculos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvVehiculos.PageIndex = e.NewPageIndex;
        LlenarGrilla();
        OrdenarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Vehiculos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Vehiculos.Eliminado");
            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.Items.Insert(1, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(2, new ListItem("Inactivo", "False"));
            rblEstado.SelectedValue = "-1";
            // LLenar combo de tipo de veiculos
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (gvVehiculos.Rows.Count > 0)
        {
            Exportar();
        }
        else
        {
            toolBar.MostrarMensajeError("No se encontraron registros para exportar la consulta");
        }
    }

    private void Exportar()
    {
        toolBar.LipiarMensajeError();

        DataTable datos = new DataTable();
        Boolean vPaginador = this.gvVehiculos.AllowPaging;
        Boolean vPaginadorError = gvVehiculos.AllowPaging;
        ExportarExcel vExportarExcel = new ExportarExcel();
        gvVehiculos.AllowPaging = false;

        List<Vehiculos> vListaActividadess = LlenarGrilla();

        datos = ManejoControlesContratos.GenerarDataTable(this.gvVehiculos, vListaActividadess);

        if (datos != null)
        {
            if (datos.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                for (int i = 1; i < gvVehiculos.Columns.Count; i++)
                {
                    dt.Columns.Add(gvVehiculos.Columns[i].HeaderText);
                }
                foreach (GridViewRow row in gvVehiculos.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 1; j < gvVehiculos.Columns.Count; j++)
                    {
                        dr[gvVehiculos.Columns[j].HeaderText] = ManejoControlesContratos.convertirCaracteresEspeciales(row.Cells[j].Text);
                    }

                    dt.Rows.Add(dr);
                }

                //dt.Columns.Remove("Estado");
                GridView datosexportar = new GridView();
                datosexportar.DataSource = dt;
                datosexportar.DataBind();
                vExportarExcel.ExportarGridViewExcel(datosexportar, this, false, "ParametricaActividades");
                gvVehiculos.AllowPaging = vPaginador;
            }
            else
                toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
        }
        else
            toolBar.MostrarMensajeError("No existe informaci&#243;n para el informe");
    }


    protected List<Vehiculos> LlenarGrilla()
    {
        toolBar.LipiarMensajeError();

        List<Vehiculos> listaActividadess = Buscar();
        gvVehiculos.DataSource = listaActividadess;
        gvVehiculos.DataBind();

        return listaActividadess;
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void OrdenarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            //Aqui va el c�digo de llenado de datos para la grilla 
            List<Vehiculos> listaActividadess = Buscar();

            //Fin del c�digo de llenado de datos para la grilla 

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (listaActividadess != null)
                {
                    var param = Expression.Parameter(typeof(Vehiculos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<Vehiculos, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = listaActividadess.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = listaActividadess;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvVehiculos_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarGrilla((GridView)sender, e.SortExpression, false);
    }
}
