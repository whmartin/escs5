using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_Vehiculos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIGUVService vSIGUVService = new SIGUVService();
    string PageName = "SIGUV/Vehiculos";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            Vehiculos vVehiculos = new Vehiculos();

            vVehiculos.Propio = Convert.ToInt32(ddlPropio.SelectedValue);
            vVehiculos.Relevo = Convert.ToInt32(txtRelevo.Text);
            vVehiculos.IdClase = Convert.ToInt32(ddlIdClase.SelectedValue);
            vVehiculos.Placa = Convert.ToString(txtPlaca.Text);
            vVehiculos.Cilindraje = Convert.ToInt32(txtCilindraje.Text);
            vVehiculos.Modelo = Convert.ToInt32(txtModelo.Text);
            vVehiculos.Capacidad = Convert.ToInt32(txtCapacidad.Text);
            vVehiculos.Estado = Convert.ToInt32(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vVehiculos.IdVehiculo = Convert.ToInt32(hfIdVehiculo.Value);
                vVehiculos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vVehiculos, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.ModificarVehiculos(vVehiculos);
            }
            else
            {
                vVehiculos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vVehiculos, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.InsertarVehiculos(vVehiculos);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Vehiculos.IdVehiculo", vVehiculos.IdVehiculo);
                SetSessionParameter("Vehiculos.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Vehiculos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdVehiculo = Convert.ToInt32(GetSessionParameter("Vehiculos.IdVehiculo"));
            RemoveSessionParameter("Vehiculos.Id");

            Vehiculos vVehiculos = new Vehiculos();
            vVehiculos = vSIGUVService.ConsultarVehiculos(vIdVehiculo);
            hfIdVehiculo.Value = vVehiculos.IdVehiculo.ToString();
            ddlPropio.SelectedValue = vVehiculos.Propio.ToString();
            txtRelevo.Text = vVehiculos.Relevo.ToString();
            ddlIdClase.SelectedValue = vVehiculos.IdClase.ToString();
            txtPlaca.Text = vVehiculos.Placa;
            txtCilindraje.Text = vVehiculos.Cilindraje.ToString();
            txtModelo.Text = vVehiculos.Modelo.ToString();
            txtCapacidad.Text = vVehiculos.Capacidad.ToString();
            rblEstado.SelectedValue = vVehiculos.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vVehiculos.UsuarioCrea, vVehiculos.FechaCrea, vVehiculos.UsuarioModifica, vVehiculos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlPropio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlPropio.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdClase.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdClase.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
