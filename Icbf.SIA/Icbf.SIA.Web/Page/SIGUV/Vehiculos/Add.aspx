<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Vehiculos_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdVehiculo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Propio *
                <asp:RequiredFieldValidator runat="server" ID="rfvPropio" ControlToValidate="ddlPropio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvPropio" ControlToValidate="ddlPropio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Relevo *
                <asp:RequiredFieldValidator runat="server" ID="rfvRelevo" ControlToValidate="txtRelevo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlPropio"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRelevo"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftRelevo" runat="server" TargetControlID="txtRelevo"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Clase *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdClase" ControlToValidate="ddlIdClase"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdClase" ControlToValidate="ddlIdClase"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Placa *
                <asp:RequiredFieldValidator runat="server" ID="rfvPlaca" ControlToValidate="txtPlaca"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdClase"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPlaca"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPlaca" runat="server" TargetControlID="txtPlaca"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Cilindraje *
                <asp:RequiredFieldValidator runat="server" ID="rfvCilindraje" ControlToValidate="txtCilindraje"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Modelo *
                <asp:RequiredFieldValidator runat="server" ID="rfvModelo" ControlToValidate="txtModelo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCilindraje"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCilindraje" runat="server" TargetControlID="txtCilindraje"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtModelo"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftModelo" runat="server" TargetControlID="txtModelo"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Capacidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvCapacidad" ControlToValidate="txtCapacidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCapacidad"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCapacidad" runat="server" TargetControlID="txtCapacidad"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
