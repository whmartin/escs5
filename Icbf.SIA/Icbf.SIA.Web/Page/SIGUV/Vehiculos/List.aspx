<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Vehiculos_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Propio *
            </td>
            <td>
                Relevo *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlPropio"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlRelevo" Width="33%"  >
                    <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                    <asp:ListItem Value="0">SI</asp:ListItem>
                    <asp:ListItem Value="1">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Clase *
            </td>
            <td>
                Placa *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdClase"  ></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPlaca" MaxLength="6" ></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftPlaca" runat="server" TargetControlID="txtPlaca"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Cilindraje *
            </td>
            <td>
                Modelo *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCilindraje"  MaxLength="5"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCilindraje" runat="server" TargetControlID="txtCilindraje"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtModelo"  MaxLength="4"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftModelo" runat="server" TargetControlID="txtModelo"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Capacidad *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCapacidad"  MaxLength="2"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCapacidad" runat="server" TargetControlID="txtCapacidad"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvVehiculos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdVehiculo" CellPadding="0" Height="16px" OnSorting="gvVehiculos_Sorting"
                        OnPageIndexChanging="gvVehiculos_PageIndexChanging" OnSelectedIndexChanged="gvVehiculos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Propio" DataField="Propio" SortExpression="IdClase" />
                            <asp:BoundField HeaderText="Relevo" DataField="Relevo" SortExpression="IdClase" />
                            <asp:BoundField HeaderText="Clase" DataField="IdClase"  SortExpression="IdClase"/>
                            <asp:BoundField HeaderText="Placa" DataField="Placa" SortExpression="IdClase" />
                            <asp:BoundField HeaderText="Cilindraje" DataField="Cilindraje" SortExpression="IdClase" />
                            <asp:BoundField HeaderText="Modelo" DataField="Modelo"  SortExpression="IdClase"/>
                            <asp:BoundField HeaderText="Capacidad" DataField="Capacidad"  SortExpression="IdClase"/>
                            <asp:BoundField HeaderText="Estado" DataField="Estado" SortExpression="IdClase"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
