using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_Vehiculos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/Vehiculos";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Vehiculos.IdVehiculo", hfIdVehiculo.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdVehiculo = Convert.ToInt32(GetSessionParameter("Vehiculos.IdVehiculo"));
            RemoveSessionParameter("Vehiculos.IdVehiculo");

            if (GetSessionParameter("Vehiculos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Vehiculos");


            Vehiculos vVehiculos = new Vehiculos();
            vVehiculos = vSIGUVService.ConsultarVehiculos(vIdVehiculo);
            hfIdVehiculo.Value = vVehiculos.IdVehiculo.ToString();
            ddlPropio.SelectedValue = vVehiculos.Propio.ToString();
            txtRelevo.Text = vVehiculos.Relevo.ToString();
            ddlIdClase.SelectedValue = vVehiculos.IdClase.ToString();
            txtPlaca.Text = vVehiculos.Placa;
            txtCilindraje.Text = vVehiculos.Cilindraje.ToString();
            txtModelo.Text = vVehiculos.Modelo.ToString();
            txtCapacidad.Text = vVehiculos.Capacidad.ToString();
            rblEstado.SelectedValue = vVehiculos.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdVehiculo.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vVehiculos.UsuarioCrea, vVehiculos.FechaCrea, vVehiculos.UsuarioModifica, vVehiculos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdVehiculo = Convert.ToInt32(hfIdVehiculo.Value);

            Vehiculos vVehiculos = new Vehiculos();
            vVehiculos = vSIGUVService.ConsultarVehiculos(vIdVehiculo);
            InformacionAudioria(vVehiculos, this.PageName, vSolutionPage);
            int vResultado = vSIGUVService.EliminarVehiculos(vVehiculos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Vehiculos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Vehiculos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlPropio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlPropio.SelectedValue = "-1";
            ddlIdClase.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdClase.SelectedValue = "-1";
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
