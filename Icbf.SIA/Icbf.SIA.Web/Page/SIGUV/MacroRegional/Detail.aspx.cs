using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_MacroRegional_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/MacroRegional";
    SIGUVService vSIGUVService = new SIGUVService();
    ManejoControlesContratos vManejoControlesContratos = new ManejoControlesContratos();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("MacroRegional.IdMacroRegional", hfIdMacroRegional.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdMacroRegional = Convert.ToInt32(GetSessionParameter("MacroRegional.IdMacroRegional"));
            RemoveSessionParameter("MacroRegional.IdMacroRegional");

            if (GetSessionParameter("MacroRegional.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("MacroRegional.Guardado");


            MacroRegional vMacroRegional = new MacroRegional();
            vMacroRegional = vSIGUVService.ConsultarMacroRegional(vIdMacroRegional);
            hfIdMacroRegional.Value = vMacroRegional.IdMacroRegional.ToString();
            ddlRegional.SelectedValue = vMacroRegional.IdMacroRegional.ToString();
            rblEstado.SelectedValue = vMacroRegional.Estado.ToString();
            
            ICollection<MacroRegional> vMacroRegionalMacroRegionalPCI = new List<MacroRegional>();
            vMacroRegionalMacroRegionalPCI = vSIGUVService.ConsultarMacroRegionalMacroRegionalPCI(vIdMacroRegional);

            gvRegional.DataSource = vMacroRegionalMacroRegionalPCI;
            gvRegional.DataBind();

            ObtenerAuditoria(PageName, hfIdMacroRegional.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vMacroRegional.UsuarioCrea, vMacroRegional.FechaCrea, vMacroRegional.UsuarioModifica, vMacroRegional.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdMacroRegional = Convert.ToInt32(hfIdMacroRegional.Value);

            MacroRegional vMacroRegional = new MacroRegional();
            vMacroRegional = vSIGUVService.ConsultarMacroRegional(vIdMacroRegional);
            InformacionAudioria(vMacroRegional, this.PageName, vSolutionPage);
            int vResultado = vSIGUVService.EliminarMacroRegional(vMacroRegional);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("MacroRegional.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("MacroRegional", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
