<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_MacroRegional_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdMacroRegional" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre Macroregional *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:DropDownList runat="server" ID="ddlRegional" Width="20%" TabIndex="0" enabled="false"></asp:DropDownList>
                                    
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
     <table width="90%" align="center" border="1" style="border-collapse: collapse;" cellpadding="0" cellspacing="0">
                <tr class="rowB">
                    <td>
                        <table width="95%" align="center">
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="gvRegional" AutoGenerateColumns="False" AllowPaging="True"
                                        GridLines="None" Width="100%" CellPadding="0" Height="16px">
                                        <Columns>
                                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional"/>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
</asp:Content>
