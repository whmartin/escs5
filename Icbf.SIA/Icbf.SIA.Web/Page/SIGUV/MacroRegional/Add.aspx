<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_MacroRegional_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdMacroRegional" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre Macroregional *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombre" ControlToValidate="txtNombre"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre" MaxLength="100"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <br />
    <table width="90%" align="center" border="1" style="border-collapse: collapse;" cellpadding="0" cellspacing="0">
                <tr class="rowB">
                    <td>
                        <table width="95%" align="center">
                            <tr class="rowB">
                                <td>
                                    Regional:
                                    <asp:DropDownList runat="server" ID="ddlRegional" Width="20%" TabIndex="0" AutoPostBack="false" OnSelectedIndexChanged="ddlRegional_SelectedIndexChanged"></asp:DropDownList>
                                    &nbsp;&nbsp; 
                                    <asp:LinkButton ID="btnAdicionarRegional" runat="server" OnClick="btnAdicionarRegional_Click"  ValidationGroup="btnAdicionarRegional"><img alt="Dar Click Para Adicionar Regionales" src="../../../Image/btn/add.gif" title="Dar Click Para Adicionar Regionales" /></asp:LinkButton>
                                </td>
                            </tr>
                            <tr class="rowAG">
                                <td>
                                    <asp:GridView runat="server" ID="gvRegional" DataKeyNames="idMacroRegionalRegionalPCI,idMacroRegional,idRegionalPCI,Grabado,IdRegionalGrilla" AutoGenerateColumns="False" AllowPaging="True"
                                        GridLines="None" Width="100%" CellPadding="0" Height="16px" OnPageIndexChanging="gvRegional_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional"/>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnRegionalDelete" runat="server" CommandName="Delete" ImageUrl="~/Image/btn/delete.gif"
                                                        Height="16px" Width="16px" ToolTip="Eliminar" OnClick="btnRegionalDelete_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
</asp:Content>
