using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;
using System.Data;


public partial class Page_MacroRegional_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIGUVService vSIGUVService = new SIGUVService();
    ManejoControlesContratos vManejoControlesContratos = new ManejoControlesContratos();
    string PageName = "SIGUV/MacroRegional";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            MacroRegional vMacroRegional = new MacroRegional();

            vMacroRegional.Nombre = Convert.ToString(txtNombre.Text);
            vMacroRegional.Estado = Convert.ToInt32(Convert.ToBoolean(rblEstado.SelectedValue));

            if (Request.QueryString["oP"] == "E")
            {
                vMacroRegional.IdMacroRegional = Convert.ToInt32(hfIdMacroRegional.Value);
                vMacroRegional.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vMacroRegional, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.ModificarMacroRegional(vMacroRegional);
            }
            else
            {
                vMacroRegional.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vMacroRegional, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.InsertarMacroRegional(vMacroRegional);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("MacroRegional.IdMacroRegional", vMacroRegional.IdMacroRegional);
                SetSessionParameter("MacroRegional.Guardado", "1");
                GuardarRegionales();
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Macro Regional", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdMacroRegional = Convert.ToInt32(GetSessionParameter("MacroRegional.IdMacroRegional"));
            RemoveSessionParameter("MacroRegional.Id");
            hfIdMacroRegional.Value = vIdMacroRegional.ToString();

            if (!vIdMacroRegional.Equals(0))
            {

                ICollection<MacroRegional> vMacroRegionalMacroRegionalPCI = new List<MacroRegional>();
                vMacroRegionalMacroRegionalPCI = vSIGUVService.ConsultarMacroRegionalMacroRegionalPCI(vIdMacroRegional);
                DataTable dt = (DataTable)Session["MacroRegionales"];
                foreach (var item in vMacroRegionalMacroRegionalPCI)
                {
                    DataRow row = dt.NewRow();
                    row["IdMacroRegionalRegionalPCI"] = item.IdMacroRegionalRegionalPCI;
                    row["IdMacroRegional"] = item.IdMacroRegional;
                    row["Nombre"] = item.Nombre;
                    row["IdRegionalPCI"] = item.IdRegionalPCI;
                    row["Estado"] = item.Estado;
                    row["Grabado"] = 1;
                    dt.Rows.Add(row);
                    Session["MacroRegionales"] = dt;
                }
                dt.AcceptChanges();
                DataRow rowLectura = dt.Rows[0];
                txtNombre.Text = rowLectura["nombre"].ToString().ToUpper();
                rblEstado.SelectedValue = rowLectura["Estado"].ToString();

                gvRegional.DataSource = dt;
                gvRegional.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "true"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "false"));
            rblEstado.SelectedIndex = 0;

            vManejoControlesContratos.LlenarRegional(ddlRegional, null, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnAdicionarRegional_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (!ValidarRegional())
            TempRegionales();
    }

    protected void btnRegionalDelete_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton img = (ImageButton)sender;
        GridViewRow gvRow = (GridViewRow)img.NamingContainer;
        int idMacroRegionalRegionalPCI = Convert.ToInt32(gvRegional.DataKeys[gvRow.RowIndex].Values[0]);
        int idMacroRegional = Convert.ToInt32(gvRegional.DataKeys[gvRow.RowIndex].Values[1]);
        int idRegionalPCI = Convert.ToInt32(gvRegional.DataKeys[gvRow.RowIndex].Values[2]);
        int IdRegionalGrilla = Convert.ToInt32(gvRegional.DataKeys[gvRow.RowIndex].Values[4]);
        int Grabado = Convert.ToInt32(gvRegional.DataKeys[gvRow.RowIndex].Values[3]);

        DataTable dt = (DataTable)Session["Regionales"];
        DataRowCollection itemColumns = dt.Rows;
        try
        {
            if (Grabado.Equals(1))
            {
                MacroRegionalRegionalPCI pMacroRegionalRegionalPCI = new MacroRegionalRegionalPCI();
                pMacroRegionalRegionalPCI.IdRegionalPCI = Convert.ToInt32(idRegionalPCI);
                pMacroRegionalRegionalPCI.IdMacroRegional = Convert.ToInt32(idMacroRegional);
                pMacroRegionalRegionalPCI.IdMacroRegionalRegionalPCI = Convert.ToInt32(idMacroRegionalRegionalPCI);
                vSIGUVService.EliminarMacroRegionalRegionalPCI(pMacroRegionalRegionalPCI);
            }

            itemColumns[IdRegionalGrilla].Delete();
            dt.AcceptChanges();
            Session["Regionales"] = dt;

            gvRegional.DataSource = Session["Regionales"];
            gvRegional.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private Boolean ValidarRegional()
    {
        bool sw = false;

        for (int i = 0; i < gvRegional.Rows.Count; i++)
        {
            if (gvRegional.DataKeys[i].Values[2].ToString() == ddlRegional.SelectedValue.ToUpper())
            {
                sw = true;
            }
        }
        if (sw)
        {
            toolBar.MostrarMensajeError("Regional, ya se encuentra en la grilla, por favor Verifique!!");
            return sw;
        }
        return sw;
    }

    protected void TempRegionales()
    {
        int Editar = 0;
        if (Session["Regionales"] == null)
        {
            DataTable dt = new DataTable("dtRegionales");
            DataColumn dc = dt.Columns.Add("IdRegionalGrilla");
            dc.AutoIncrement = true;
            dt.Columns.Add(new DataColumn("idMacroRegionalRegionalPCI", typeof(int)));
            dt.Columns.Add(new DataColumn("idMacroRegional", typeof(int)));
            dt.Columns.Add(new DataColumn("idRegionalPCI", typeof(int)));
            dt.Columns.Add(new DataColumn("NombreRegional", typeof(String)));
            dt.Columns.Add(new DataColumn("Grabado", typeof(int)));

            Session["Regionales"] = dt;

        }
        else
        {
            DataTable dt = (DataTable)Session["Regionales"];

            DataRow row = dt.NewRow();
            row["idMacroRegionalRegionalPCI"] = 0;
            row["idMacroRegional"] = 0;
            row["idRegionalPCI"] = Convert.ToInt32(ddlRegional.SelectedValue);
            row["NombreRegional"] = ddlRegional.SelectedItem.Text.ToUpper();
            row["Grabado"] = 0;
            dt.Rows.Add(row);
            dt.AcceptChanges();
            Session["Regionales"] = dt;

        }

        gvRegional.DataSource = Session["Regionales"];
        gvRegional.DataBind();

        //limpiar Control de regional
        ddlRegional.SelectedValue = "1";
    }

    protected void ddlRegional_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void GuardarRegionales()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["Regionales"];
            foreach (DataRow row in dt.Rows)
            {
                int vResultado = 1;

                if (row["Grabado"].Equals(0))
                {
                    MacroRegionalRegionalPCI vMacroRegionalRegionalPCI = new MacroRegionalRegionalPCI();
                    vMacroRegionalRegionalPCI.IdMacroRegional = Convert.ToInt32(hfIdMacroRegional.Value);
                    vMacroRegionalRegionalPCI.IdRegionalPCI = Convert.ToInt32(row["IdRegionalPCI"].ToString());
                    vMacroRegionalRegionalPCI.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultado = vSIGUVService.InsertarMacroRegionalRegionalPCI(vMacroRegionalRegionalPCI);
                    InformacionAudioria(vMacroRegionalRegionalPCI, this.PageName, vSolutionPage);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void gvRegional_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
}
