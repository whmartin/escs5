using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_ContratoSiguvVehiculos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/ContratoSiguvVehiculos";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ContratoSiguvVehiculos.IdContratoSiguvVehiculos", hfIdContratoSiguvVehiculos.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdContratoSiguvVehiculos = Convert.ToInt32(GetSessionParameter("ContratoSiguvVehiculos.IdContratoSiguvVehiculos"));
            RemoveSessionParameter("ContratoSiguvVehiculos.IdContratoSiguvVehiculos");

            if (GetSessionParameter("ContratoSiguvVehiculos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ContratoSiguvVehiculos");


            ContratoSiguvVehiculos vContratoSiguvVehiculos = new ContratoSiguvVehiculos();
            vContratoSiguvVehiculos = vSIGUVService.ConsultarContratoSiguvVehiculos(vIdContratoSiguvVehiculos);
            hfIdContratoSiguvVehiculos.Value = vContratoSiguvVehiculos.IdContratoSiguvVehiculos.ToString();
            ddlIdContratoSiguv.SelectedValue = vContratoSiguvVehiculos.IdContratoSiguv.ToString();
            ddlIdVehiculo.SelectedValue = vContratoSiguvVehiculos.IdVehiculo.ToString();
            ddlIdRegionalPCI.SelectedValue = vContratoSiguvVehiculos.IdRegionalPCI.ToString();
            ddlIdCentroZonal.SelectedValue = vContratoSiguvVehiculos.IdCentroZonal.ToString();
            ObtenerAuditoria(PageName, hfIdContratoSiguvVehiculos.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContratoSiguvVehiculos.UsuarioCrea, vContratoSiguvVehiculos.FechaCrea, vContratoSiguvVehiculos.UsuarioModifica, vContratoSiguvVehiculos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdContratoSiguvVehiculos = Convert.ToInt32(hfIdContratoSiguvVehiculos.Value);

            ContratoSiguvVehiculos vContratoSiguvVehiculos = new ContratoSiguvVehiculos();
            vContratoSiguvVehiculos = vSIGUVService.ConsultarContratoSiguvVehiculos(vIdContratoSiguvVehiculos);
            InformacionAudioria(vContratoSiguvVehiculos, this.PageName, vSolutionPage);
            int vResultado = vSIGUVService.EliminarContratoSiguvVehiculos(vContratoSiguvVehiculos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ContratoSiguvVehiculos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("ContratoSiguvVehiculos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIdContratoSiguv.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContratoSiguv.SelectedValue = "-1";
            ddlIdVehiculo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculo.SelectedValue = "-1";
            ddlIdRegionalPCI.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdRegionalPCI.SelectedValue = "-1";
            ddlIdCentroZonal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdCentroZonal.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
