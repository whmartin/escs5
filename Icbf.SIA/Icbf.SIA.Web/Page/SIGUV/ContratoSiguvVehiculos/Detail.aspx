<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ContratoSiguvVehiculos_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContratoSiguvVehiculos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                ContratoSiguv *
            </td>
            <td>
                Vehículo *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdContratoSiguv"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculo"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional *
            </td>
            <td>
                Centro Zonal *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegionalPCI"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCentroZonal"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
