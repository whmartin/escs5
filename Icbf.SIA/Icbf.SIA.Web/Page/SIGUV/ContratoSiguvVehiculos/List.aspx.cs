using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_ContratoSiguvVehiculos_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/ContratoSiguvVehiculos";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdContratoSiguv = null;
            int? vIdVehiculo = null;
            int? vIdRegionalPCI = null;
            int? vIdCentroZonal = null;
            if (ddlIdContratoSiguv.SelectedValue!= "-1")
            {
                vIdContratoSiguv = Convert.ToInt32(ddlIdContratoSiguv.SelectedValue);
            }
            if (ddlIdVehiculo.SelectedValue!= "-1")
            {
                vIdVehiculo = Convert.ToInt32(ddlIdVehiculo.SelectedValue);
            }
            if (ddlIdRegionalPCI.SelectedValue!= "-1")
            {
                vIdRegionalPCI = Convert.ToInt32(ddlIdRegionalPCI.SelectedValue);
            }
            if (ddlIdCentroZonal.SelectedValue!= "-1")
            {
                vIdCentroZonal = Convert.ToInt32(ddlIdCentroZonal.SelectedValue);
            }
            gvContratoSiguvVehiculos.DataSource = vSIGUVService.ConsultarContratoSiguvVehiculoss( vIdContratoSiguv, vIdVehiculo, vIdRegionalPCI, vIdCentroZonal);
            gvContratoSiguvVehiculos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvContratoSiguvVehiculos.PageSize = PageSize();
            gvContratoSiguvVehiculos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("ContratoSiguvVehiculos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContratoSiguvVehiculos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ContratoSiguvVehiculos.IdContratoSiguvVehiculos", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContratoSiguvVehiculos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContratoSiguvVehiculos.SelectedRow);
    }
    protected void gvContratoSiguvVehiculos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratoSiguvVehiculos.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ContratoSiguvVehiculos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ContratoSiguvVehiculos.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdContratoSiguv.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContratoSiguv.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdVehiculo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculo.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdRegionalPCI.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdRegionalPCI.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdCentroZonal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdCentroZonal.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
