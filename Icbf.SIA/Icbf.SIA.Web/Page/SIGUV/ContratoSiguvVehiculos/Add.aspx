<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ContratoSiguvVehiculos_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContratoSiguvVehiculos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                ContratoSiguv *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContratoSiguv" ControlToValidate="ddlIdContratoSiguv"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdContratoSiguv" ControlToValidate="ddlIdContratoSiguv"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Vehículo *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVehiculo" ControlToValidate="ddlIdVehiculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVehiculo" ControlToValidate="ddlIdVehiculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdContratoSiguv"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculo"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegionalPCI" ControlToValidate="ddlIdRegionalPCI"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegionalPCI" ControlToValidate="ddlIdRegionalPCI"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Centro Zonal *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdCentroZonal" ControlToValidate="ddlIdCentroZonal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdCentroZonal" ControlToValidate="ddlIdCentroZonal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegionalPCI"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCentroZonal"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
