using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_ContratoSiguvVehiculos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIGUVService vSIGUVService = new SIGUVService();
    string PageName = "SIGUV/ContratoSiguvVehiculos";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            ContratoSiguvVehiculos vContratoSiguvVehiculos = new ContratoSiguvVehiculos();

            vContratoSiguvVehiculos.IdContratoSiguv = Convert.ToInt32(ddlIdContratoSiguv.SelectedValue);
            vContratoSiguvVehiculos.IdVehiculo = Convert.ToInt32(ddlIdVehiculo.SelectedValue);
            vContratoSiguvVehiculos.IdRegionalPCI = Convert.ToInt32(ddlIdRegionalPCI.SelectedValue);
            vContratoSiguvVehiculos.IdCentroZonal = Convert.ToInt32(ddlIdCentroZonal.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vContratoSiguvVehiculos.IdContratoSiguvVehiculos = Convert.ToInt32(hfIdContratoSiguvVehiculos.Value);
                vContratoSiguvVehiculos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContratoSiguvVehiculos, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.ModificarContratoSiguvVehiculos(vContratoSiguvVehiculos);
            }
            else
            {
                vContratoSiguvVehiculos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContratoSiguvVehiculos, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.InsertarContratoSiguvVehiculos(vContratoSiguvVehiculos);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ContratoSiguvVehiculos.IdContratoSiguvVehiculos", vContratoSiguvVehiculos.IdContratoSiguvVehiculos);
                SetSessionParameter("ContratoSiguvVehiculos.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("ContratoSiguvVehiculos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdContratoSiguvVehiculos = Convert.ToInt32(GetSessionParameter("ContratoSiguvVehiculos.IdContratoSiguvVehiculos"));
            RemoveSessionParameter("ContratoSiguvVehiculos.Id");

            ContratoSiguvVehiculos vContratoSiguvVehiculos = new ContratoSiguvVehiculos();
            vContratoSiguvVehiculos = vSIGUVService.ConsultarContratoSiguvVehiculos(vIdContratoSiguvVehiculos);
            hfIdContratoSiguvVehiculos.Value = vContratoSiguvVehiculos.IdContratoSiguvVehiculos.ToString();
            ddlIdContratoSiguv.SelectedValue = vContratoSiguvVehiculos.IdContratoSiguv.ToString();
            ddlIdVehiculo.SelectedValue = vContratoSiguvVehiculos.IdVehiculo.ToString();
            ddlIdRegionalPCI.SelectedValue = vContratoSiguvVehiculos.IdRegionalPCI.ToString();
            ddlIdCentroZonal.SelectedValue = vContratoSiguvVehiculos.IdCentroZonal.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContratoSiguvVehiculos.UsuarioCrea, vContratoSiguvVehiculos.FechaCrea, vContratoSiguvVehiculos.UsuarioModifica, vContratoSiguvVehiculos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdContratoSiguv.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContratoSiguv.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdVehiculo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculo.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdRegionalPCI.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdRegionalPCI.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdCentroZonal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdCentroZonal.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
