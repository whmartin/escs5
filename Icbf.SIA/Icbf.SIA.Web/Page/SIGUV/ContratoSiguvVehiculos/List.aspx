<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ContratoSiguvVehiculos_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                ContratoSiguv *
            </td>
            <td>
                Vehículo *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdContratoSiguv"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculo"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional *
            </td>
            <td>
                Centro Zonal *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegionalPCI"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCentroZonal"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratoSiguvVehiculos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContratoSiguvVehiculos" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContratoSiguvVehiculos_PageIndexChanging" OnSelectedIndexChanged="gvContratoSiguvVehiculos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="ContratoSiguv" DataField="IdContratoSiguv" />
                            <asp:BoundField HeaderText="Vehículo" DataField="IdVehiculo" />
                            <asp:BoundField HeaderText="Regional" DataField="IdRegionalPCI" />
                            <asp:BoundField HeaderText="Centro Zonal" DataField="IdCentroZonal" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
