<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Relevo_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdRelevo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Contrato Siguv *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContratoSiguv" ControlToValidate="ddlIdContratoSiguv"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdContratoSiguv" ControlToValidate="ddlIdContratoSiguv"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Vehículo Principal *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVehiculoPrincipal" ControlToValidate="ddlIdVehiculoPrincipal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVehiculoPrincipal" ControlToValidate="ddlIdVehiculoPrincipal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdContratoSiguv"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculoPrincipal"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vehículo Reemplazo *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVehiculoReemplazo" ControlToValidate="ddlIdVehiculoReemplazo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVehiculoReemplazo" ControlToValidate="ddlIdVehiculoReemplazo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                fecha Inicio *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaInicio" ControlToValidate="txtFechaInicio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculoReemplazo"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicio"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                fecha Fin *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaFin" ControlToValidate="txtFechaFin"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtFechaFin"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
