<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Relevo_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdRelevo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Contrato Siguv *
            </td>
            <td>
                Vehículo Principal *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdContratoSiguv"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculoPrincipal"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vehículo Reemplazo *
            </td>
            <td>
                fecha Inicio *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculoReemplazo"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                fecha Fin *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtFechaFin"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
