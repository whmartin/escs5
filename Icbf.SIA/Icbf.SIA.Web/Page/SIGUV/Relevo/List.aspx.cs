using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_Relevo_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/Relevo";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdContratoSiguv = null;
            int? vIdVehiculoPrincipal = null;
            int? vIdVehiculoReemplazo = null;
            DateTime? vFechaInicio = null;
            DateTime? vFechaFin = null;
            if (ddlIdContratoSiguv.SelectedValue!= "-1")
            {
                vIdContratoSiguv = Convert.ToInt32(ddlIdContratoSiguv.SelectedValue);
            }
            if (ddlIdVehiculoPrincipal.SelectedValue!= "-1")
            {
                vIdVehiculoPrincipal = Convert.ToInt32(ddlIdVehiculoPrincipal.SelectedValue);
            }
            if (ddlIdVehiculoReemplazo.SelectedValue!= "-1")
            {
                vIdVehiculoReemplazo = Convert.ToInt32(ddlIdVehiculoReemplazo.SelectedValue);
            }
            if (txtFechaInicio.Text!= "")
            {
                vFechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
            }
            if (txtFechaFin.Text!= "")
            {
                vFechaFin = Convert.ToDateTime(txtFechaFin.Text);
            }
            gvRelevo.DataSource = vSIGUVService.ConsultarRelevos( vIdContratoSiguv, vIdVehiculoPrincipal, vIdVehiculoReemplazo, vFechaInicio, vFechaFin);
            gvRelevo.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvRelevo.PageSize = PageSize();
            gvRelevo.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Relevo", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRelevo.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Relevo.IdRelevo", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRelevo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRelevo.SelectedRow);
    }
    protected void gvRelevo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRelevo.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Relevo.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Relevo.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdContratoSiguv.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContratoSiguv.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdVehiculoPrincipal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculoPrincipal.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdVehiculoReemplazo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculoReemplazo.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
