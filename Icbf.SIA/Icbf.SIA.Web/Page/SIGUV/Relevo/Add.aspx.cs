using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_Relevo_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIGUVService vSIGUVService = new SIGUVService();
    string PageName = "SIGUV/Relevo";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            Relevo vRelevo = new Relevo();

            vRelevo.IdContratoSiguv = Convert.ToInt32(ddlIdContratoSiguv.SelectedValue);
            vRelevo.IdVehiculoPrincipal = Convert.ToInt32(ddlIdVehiculoPrincipal.SelectedValue);
            vRelevo.IdVehiculoReemplazo = Convert.ToInt32(ddlIdVehiculoReemplazo.SelectedValue);
            vRelevo.FechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
            vRelevo.FechaFin = Convert.ToDateTime(txtFechaFin.Text);

            if (Request.QueryString["oP"] == "E")
            {
            vRelevo.IdRelevo = Convert.ToInt32(hfIdRelevo.Value);
                vRelevo.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRelevo, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.ModificarRelevo(vRelevo);
            }
            else
            {
                vRelevo.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRelevo, this.PageName, vSolutionPage);
                vResultado = vSIGUVService.InsertarRelevo(vRelevo);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Relevo.IdRelevo", vRelevo.IdRelevo);
                SetSessionParameter("Relevo.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Relevo", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdRelevo = Convert.ToInt32(GetSessionParameter("Relevo.IdRelevo"));
            RemoveSessionParameter("Relevo.Id");

            Relevo vRelevo = new Relevo();
            vRelevo = vSIGUVService.ConsultarRelevo(vIdRelevo);
            hfIdRelevo.Value = vRelevo.IdRelevo.ToString();
            ddlIdContratoSiguv.SelectedValue = vRelevo.IdContratoSiguv.ToString();
            ddlIdVehiculoPrincipal.SelectedValue = vRelevo.IdVehiculoPrincipal.ToString();
            ddlIdVehiculoReemplazo.SelectedValue = vRelevo.IdVehiculoReemplazo.ToString();
            txtFechaInicio.Text = vRelevo.FechaInicio.ToString();
            txtFechaFin.Text = vRelevo.FechaFin.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRelevo.UsuarioCrea, vRelevo.FechaCrea, vRelevo.UsuarioModifica, vRelevo.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdContratoSiguv.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContratoSiguv.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdVehiculoPrincipal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculoPrincipal.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdVehiculoReemplazo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculoReemplazo.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
