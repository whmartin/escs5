using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIGUV.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIGUV.Entity;

public partial class Page_Relevo_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "SIGUV/Relevo";
    SIGUVService vSIGUVService = new SIGUVService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Relevo.IdRelevo", hfIdRelevo.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdRelevo = Convert.ToInt32(GetSessionParameter("Relevo.IdRelevo"));
            RemoveSessionParameter("Relevo.IdRelevo");

            if (GetSessionParameter("Relevo.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Relevo");


            Relevo vRelevo = new Relevo();
            vRelevo = vSIGUVService.ConsultarRelevo(vIdRelevo);
            hfIdRelevo.Value = vRelevo.IdRelevo.ToString();
            ddlIdContratoSiguv.SelectedValue = vRelevo.IdContratoSiguv.ToString();
            ddlIdVehiculoPrincipal.SelectedValue = vRelevo.IdVehiculoPrincipal.ToString();
            ddlIdVehiculoReemplazo.SelectedValue = vRelevo.IdVehiculoReemplazo.ToString();
            txtFechaInicio.Text = vRelevo.FechaInicio.ToString();
            txtFechaFin.Text = vRelevo.FechaFin.ToString();
            ObtenerAuditoria(PageName, hfIdRelevo.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRelevo.UsuarioCrea, vRelevo.FechaCrea, vRelevo.UsuarioModifica, vRelevo.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdRelevo = Convert.ToInt32(hfIdRelevo.Value);

            Relevo vRelevo = new Relevo();
            vRelevo = vSIGUVService.ConsultarRelevo(vIdRelevo);
            InformacionAudioria(vRelevo, this.PageName, vSolutionPage);
            int vResultado = vSIGUVService.EliminarRelevo(vRelevo);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Relevo.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Relevo", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIdContratoSiguv.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdContratoSiguv.SelectedValue = "-1";
            ddlIdVehiculoPrincipal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculoPrincipal.SelectedValue = "-1";
            ddlIdVehiculoReemplazo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdVehiculoReemplazo.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
