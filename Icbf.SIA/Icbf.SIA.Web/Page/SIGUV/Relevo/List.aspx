<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Relevo_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Contrato Siguv *
            </td>
            <td>
                Vehículo Principal *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdContratoSiguv"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculoPrincipal"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vehículo Reemplazo *
            </td>
            <td>
                fecha Inicio *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVehiculoReemplazo"  ></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicio"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                fecha Fin *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtFechaFin"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRelevo" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRelevo" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRelevo_PageIndexChanging" OnSelectedIndexChanged="gvRelevo_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Contrato Siguv" DataField="IdContratoSiguv" />
                            <asp:BoundField HeaderText="Vehículo Principal" DataField="IdVehiculoPrincipal" />
                            <asp:BoundField HeaderText="Vehículo Reemplazo" DataField="IdVehiculoReemplazo" />
                            <asp:BoundField HeaderText="fecha Inicio" DataField="FechaInicio" />
                            <asp:BoundField HeaderText="fecha Fin" DataField="FechaFin" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
