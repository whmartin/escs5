﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.Seguridad.Service;
using Icbf.Seguridad.Entity;
using System.Configuration;

public partial class Page_RubOnline_TransversalReportes_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Reportes/TransversalReportes";
    SIAService vSIAService = new SIAService();
    SeguridadService vSeguridadFAC = new SeguridadService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        toolBar = (masterPrincipal)this.Master;
        ((ScriptManager)toolBar.FindControl("ScriptManager1")).EnablePartialRendering = false;
        ((ScriptManager)toolBar.FindControl("ScriptManager1")).ScriptMode = ScriptMode.Release;
        //if (Request.QueryString["ReportName"] == null) { Iniciar(); }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Iniciar();
        SolutionPage vSolutionPage = SolutionPage.List;
        //if (ValidateAccess(toolBar, PageName, vSolutionPage))
        if (ValidateAccess(toolBar, InformeConsultado(), vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                ConfigurarReporte();
            }
        }
    }
    protected String InformeConsultado()
    {
        Programa vPrograma = new Programa();
        try
        {
            int IdPrograma = Convert.ToInt32(Request.QueryString["oRp"]);
            vPrograma = vSeguridadFAC.ConsultarPrograma(IdPrograma);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        return vPrograma.CodigoPrograma;



      }
    protected void ConfigurarReporte()
    {
        Programa vPrograma = new Programa();
        Reporte vReporte = new Reporte();

        try
        {

            string vReportServerUrl = ConfigurationManager.AppSettings["ReportServerUrl"];
            string vReportPath = ConfigurationManager.AppSettings["ReportPath"];
            string vUserName = ConfigurationManager.AppSettings["UserName"];
            string vPassword = ConfigurationManager.AppSettings["Password"];
            string vDomainName = ConfigurationManager.AppSettings["DomainName"];

            int IdPrograma = Convert.ToInt32(Request.QueryString["oRp"]);
            vPrograma = vSeguridadFAC.ConsultarPrograma(IdPrograma);
            vReporte = vSIAService.ConsultarReporte(Convert.ToInt32(vPrograma.IdReporte));

            //rvTransversarReportes.Visible = true;
            //rvTransversarReportes.ShowParameterPrompts = false;
            //rvTransversarReportes.ShowCredentialPrompts = false;
            
            ////Crea un objeto de tipo reporte
            Report objReport = new Report(vReporte.NombreReporte, true, PageName, vReporte.Descripcion);
            
            string Reportpath = "/" + vReporte.Carpeta + "/" + vReporte.NombreArchivo;
            
            
            //set the url for the reporting service web service
            rvTransversarReportes.ServerReport.ReportServerUrl = new System.Uri(vReporte.Servidor);
             
            //gets, checks and assign the report path for the reports
            if (vReporte.Carpeta != string.Empty)
                rvTransversarReportes.ServerReport.ReportPath = "/" + vReporte.Carpeta + "/" + vReporte.NombreArchivo;
            else
                rvTransversarReportes.ServerReport.ReportPath = "/" + vReporte.NombreReporte;

            //set the report credentias
            rvTransversarReportes.ServerReport.ReportServerCredentials = new CustomReportCredentials(vUserName,vPassword,vDomainName);

            //set the Processing Mode
            rvTransversarReportes.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
            
            //set the Processing Mode
            rvTransversarReportes.ServerReport.Refresh();

            //set the ShowExportControls Option
            rvTransversarReportes.ShowExportControls = objReport.ShowExportControls;
            
            

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnBuscar_Click(object sender, EventArgs e) { }
}