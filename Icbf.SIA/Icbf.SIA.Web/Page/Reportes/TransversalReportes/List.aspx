﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RubOnline_TransversalReportes_List" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <style>
        input { width:auto; height:auto}
    </style>
    <div>
        <rsweb:ReportViewer ID="rvTransversarReportes" runat="server" ProcessingMode="Remote" Height="1200px" Width="100%"></rsweb:ReportViewer>    
    </div>
</asp:Content>
