﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.ComparadorJuridico.Entity;
using Icbf.ComparadorJuridico.Service;

public partial class Page_Parametricas_CuentaContableNICSP_CodCtaConSiif : GeneralWeb
{
    Poveedor_VentanaPopup toolBar;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            CargarDatosIniciales();
        }
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (Poveedor_VentanaPopup)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate_Poveedor_VentanaPopup(btnBuscar_Click);
            gvTCON01CodCuentasContablesSIIFDebito.PageSize = PageSize();
            gvTCON01CodCuentasContablesSIIFDebito.EmptyDataText = EmptyDataText();
            toolBar.EstablecerTitulos("Cuentas Contables SIIF", "CuentasSiif");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvTCON01CodCuentasContablesSIIFDebito, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            //string caracteres = gvTCON01CodCuentasContablesSIIFDebito.Rows[gvTCON01CodCuentasContablesSIIFDebito.SelectedIndex].Cells[1].Text;
            //int numcaracteres = caracteres.Length;

            //if (numcaracteres == 6)
            //{

            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvTCON01CodCuentasContablesSIIFDebito.DataKeys[gvTCON01CodCuentasContablesSIIFDebito.SelectedRow.RowIndex].Value.ToString()) + "';";
            for (int c = 1; c < 3; c++)
            {
                if (gvTCON01CodCuentasContablesSIIFDebito.Rows[gvTCON01CodCuentasContablesSIIFDebito.SelectedIndex].Cells[c].Text != "&nbsp;")
                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvTCON01CodCuentasContablesSIIFDebito.Rows[gvTCON01CodCuentasContablesSIIFDebito.SelectedIndex].Cells[c].Text) + "';";
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterClientScriptBlock(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTCON01CodCuentasContablesSIIFDebito_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTCON01CodCuentasContablesSIIFDebito.SelectedRow);
    }
    protected void gvTCON01CodCuentasContablesSIIFDebito_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTCON01CodCuentasContablesSIIFDebito.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvTCON01CodCuentasContablesSIIFDebito_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vCodCtaContSiif = null;
            String vDescCtaContSiif = null;
            // Boolean? vEstado = null;
            if (txtCodCtaContSiif.Text != "")
            {
                vCodCtaContSiif = Convert.ToString(txtCodCtaContSiif.Text);
            }
            if (txtDescCtaContSiif.Text != "")
            {
                vDescCtaContSiif = Convert.ToString(txtDescCtaContSiif.Text);
            }

            var myGridResults = new ComparadorJuridicoService().ConsultarTCON01(vCodCtaContSiif, vDescCtaContSiif);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(TCON01Resumido), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<TCON01Resumido, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            //rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            //rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            //rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}