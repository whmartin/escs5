﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/VentanaPopup.master" AutoEventWireup="true" CodeFile="CodCtaConSiif.aspx.cs" Inherits="Page_Parametricas_CuentaContableNICSP_CodCtaConSiif" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
<script type="text/javascript" language="javascript">
    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        }
    }
    function EsEspacio(evt, control) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode != 32) {
            return true;
        } else if (charCode == 32 && control.value.length >= 1)
            return true
        else if (charCode == 32 && control.value.length == 0)
            return false;
        else
            return false;
    }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Código Cuenta Contable
            </td>
            <td class="Cell">
                Descripción Cuenta Contable
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCodCtaContSiif" MaxLength="24" Width="85%" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodCtaContSiif" runat="server" TargetControlID="txtCodCtaContSiif"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtDescCtaContSiif"   Width="85%" CssClass="TextBoxGrande" MaxLength="500" onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescCtaContSiif" runat="server" TargetControlID="txtDescCtaContSiif"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;û" />
            </td>
        </tr>
        <tr class="rowB">
            <%--<td  class="Cell" colspan="2">
                Vigente
            </td>--%>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" Visible="false" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTCON01CodCuentasContablesSIIFDebito" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdCodCtaConSiif" CellPadding="0" Height="16px"
                        OnSorting="gvTCON01CodCuentasContablesSIIFDebito_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvTCON01CodCuentasContablesSIIFDebito_PageIndexChanging" OnSelectedIndexChanged="gvTCON01CodCuentasContablesSIIFDebito_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código Cuenta Contable Siif" DataField="CodCtaContSiif"  SortExpression="CodCtaContSiif"/>
                            <asp:BoundField HeaderText="Descripción Cuenta Contable Siif" DataField="DescCtaContSiif"  SortExpression="DescCtaContSiif"/>
                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

