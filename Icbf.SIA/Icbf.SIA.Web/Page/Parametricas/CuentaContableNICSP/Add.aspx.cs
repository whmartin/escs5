using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.ComparadorJuridico.Entity;
using Icbf.ComparadorJuridico.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_CuentaContableNICSP_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ComparadorJuridicoService vComparadorJuridicoService = new ComparadorJuridicoService();
    string PageName = "Parametricas/CuentaContableNICSP";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            CuentasContablesNICSP vPlanContable = new CuentasContablesNICSP();

            vPlanContable.IdCuentasContablesNICSP = string.IsNullOrWhiteSpace(hfIdCuentasContablesNICSP.Value)? 0 : Convert.ToInt32(hfIdCuentasContablesNICSP.Value);

            vPlanContable.CodCtaContSiifCredito = hfCodigoCuentaCredito.Value;
            vPlanContable.CodCtaContSiifDebito = hfCodigoCuentaDebito.Value;
            vPlanContable.CodCtaContSiifIntArea = hfCodigoCuentaIntArea.Value;
            vPlanContable.DescCtaContSiifCredito = hfNombreCuentaCredito.Value;
            vPlanContable.DescCtaContSiifDebito = hfNombreCuentaDebito.Value;
            vPlanContable.DescCtaContSiifIntArea = hfNombreCuentaIntArea.Value;
            vPlanContable.Estado = rblEstado.SelectedValue == "1" ? true : false;
            vPlanContable.IdCodCtaConSiifConIntArea = string.IsNullOrWhiteSpace(hfIdCuentaIntArea.Value) ? 0 : Convert.ToInt32(hfIdCuentaIntArea.Value);
            vPlanContable.IdCodCtaConSiifCredito = string.IsNullOrWhiteSpace(hfIdCuentaCredito.Value) ? 0 : Convert.ToInt32(hfIdCuentaCredito.Value);
            vPlanContable.IdCodCtaConSiifDebito = string.IsNullOrWhiteSpace(hfIdCuentaDebito.Value) ? 0 : Convert.ToInt32(hfIdCuentaDebito.Value);
            vPlanContable.IdFaseProceso = string.IsNullOrWhiteSpace(ddlFase.SelectedValue) ? 0 : Convert.ToInt32(ddlFase.SelectedValue);
            vPlanContable.IdSubFaseProceso = string.IsNullOrWhiteSpace(ddlSubFase.SelectedValue) ? 0 : Convert.ToInt32(ddlSubFase.SelectedValue);
            vPlanContable.IdNaturalezaProceso = string.IsNullOrWhiteSpace(ddlNaturaleza.SelectedValue) ? 0 : Convert.ToInt32(ddlNaturaleza.SelectedValue);
            vPlanContable.IdTipoProceso = string.IsNullOrWhiteSpace(ddlTipoProceso.SelectedValue) ? 0 : Convert.ToInt32(ddlTipoProceso.SelectedValue);

            bool existe = vComparadorJuridicoService.ValidaContablesNICSP(vPlanContable.IdCuentasContablesNICSP, vPlanContable.IdTipoProceso, vPlanContable.IdCodCtaConSiifDebito, vPlanContable.IdCodCtaConSiifCredito);

            if(existe)
            {
                toolBar.MostrarMensajeError("La cuenta contable seleccionada se encuentra parametrizada, verifique por favor.");

                return;
            }

            if (Request.QueryString["oP"] == "E")
            {
                vPlanContable.UsuarioModifica = GetSessionUser().NombreUsuario;
            }
            else
            {
                vPlanContable.UsuarioCrea = GetSessionUser().NombreUsuario;
            }

            InformacionAudioria(vPlanContable, this.PageName, vSolutionPage);
            vResultado = vComparadorJuridicoService.GuardarCuentasContablesNICSP(vPlanContable);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 1)
            {
                SetSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP", vPlanContable.IdCuentasContablesNICSP);
                SetSessionParameter("CuentasContablesNICSP.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Parametrización Contable", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdPlanContable = Convert.ToInt32(GetSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP"));
            RemoveSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP");

            IEnumerable<CuentasContablesNICSP> vlstCuentasContablesNICSP = vComparadorJuridicoService.ConsultarCuentasContablesNICSP(
                null,
                vIdPlanContable,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

            if (vlstCuentasContablesNICSP.Count() > 0)
            {
                CuentasContablesNICSP vCuentasContablesNICSP = vlstCuentasContablesNICSP.FirstOrDefault();

                hfIdCuentasContablesNICSP.Value = vCuentasContablesNICSP.IdCuentasContablesNICSP.ToString();

                txtCodigoCuentaIntArea.Text = vCuentasContablesNICSP.CodCtaContSiifIntArea;
                txtNombreCuentaIntArea.Text = vCuentasContablesNICSP.DescCtaContSiifIntArea;
                hfIdCuentaIntArea.Value = vCuentasContablesNICSP.IdCodCtaConSiifConIntArea.ToString();
                hfCodigoCuentaIntArea.Value = vCuentasContablesNICSP.CodCtaContSiifIntArea;
                hfNombreCuentaIntArea.Value = vCuentasContablesNICSP.DescCtaContSiifIntArea;

                txtCodigoCuentaCredito.Text = vCuentasContablesNICSP.CodCtaContSiifCredito;
                txtNombreCuentaCredito.Text = vCuentasContablesNICSP.DescCtaContSiifCredito;
                hfIdCuentaCredito.Value = vCuentasContablesNICSP.IdCodCtaConSiifCredito.ToString();
                hfCodigoCuentaCredito.Value = vCuentasContablesNICSP.CodCtaContSiifCredito;
                hfNombreCuentaCredito.Value = vCuentasContablesNICSP.DescCtaContSiifCredito;

                txtCodigoCuentaDebito.Text = vCuentasContablesNICSP.CodCtaContSiifDebito;
                txtNombreCuentaDebito.Text = vCuentasContablesNICSP.DescCtaContSiifDebito;
                hfIdCuentaDebito.Value = vCuentasContablesNICSP.IdCodCtaConSiifDebito.ToString();
                hfCodigoCuentaDebito.Value = vCuentasContablesNICSP.CodCtaContSiifDebito;
                hfNombreCuentaDebito.Value = vCuentasContablesNICSP.DescCtaContSiifDebito;

                ddlTipoProceso.SelectedValue = vCuentasContablesNICSP.IdTipoProceso.ToString();
                ddlFase.SelectedValue = vCuentasContablesNICSP.IdFaseProceso.ToString();
                ddlNaturaleza.SelectedValue = vCuentasContablesNICSP.IdNaturalezaProceso.ToString();
                ddlSubFase.SelectedValue = vCuentasContablesNICSP.IdSubFaseProceso.ToString();

                rblEstado.SelectedValue = vCuentasContablesNICSP.Estado ? "1" : "0";

                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCuentasContablesNICSP.UsuarioCrea, vCuentasContablesNICSP.FechaCrea, vCuentasContablesNICSP.UsuarioModifica, vCuentasContablesNICSP.FechaModifica);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("CuentasContablesNICSP.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("CuentasContablesNICSP.Eliminado");

            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlTipoProceso,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarTodosTipoProceso().Select(
                        x => new BaseDto
                        {
                            Id = x.IdTipoProceso.ToString(),
                            Name = x.NombreTipoProcesol
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlFase,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarFaseProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdFaseProceso.ToString(),
                            Name = x.DescripcionFaseProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlSubFase,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarSubFaseProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdSubFaseProceso.ToString(),
                            Name = x.DescripcionSubFaseProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlNaturaleza,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarNaturalezaProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdNaturalezaProceso.ToString(),
                            Name = x.DescripcionNaturalezaProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void hfIdCuentaIntArea_ValueChanged(object sender, EventArgs e)
    {
        txtCodigoCuentaIntArea.Text = hfCodigoCuentaIntArea.Value;
        txtNombreCuentaIntArea.Text = hfNombreCuentaIntArea.Value;
    }

    protected void hfIdCuentaCredito_ValueChanged(object sender, EventArgs e)
    {
        txtCodigoCuentaCredito.Text = hfCodigoCuentaCredito.Value;
        txtNombreCuentaCredito.Text = hfNombreCuentaCredito.Value;
    }

    protected void hfIdCuentaDebito_ValueChanged(object sender, EventArgs e)
    {
        txtCodigoCuentaDebito.Text = hfCodigoCuentaDebito.Value;
        txtNombreCuentaDebito.Text = hfNombreCuentaDebito.Value;
    }

}
