using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.ComparadorJuridico.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.ComparadorJuridico.Entity;

public partial class Page_CuentaContableNICSP_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Parametricas/CuentaContableNICSP";
    ComparadorJuridicoService vComparadorJuridicoService = new ComparadorJuridicoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP", hfIdCuentasContablesNICSP.Value);
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdPlanContable = Convert.ToInt32(GetSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP"));
            RemoveSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP");

            IEnumerable<CuentasContablesNICSP> vlstCuentasContablesNICSP = vComparadorJuridicoService.ConsultarCuentasContablesNICSP(
                null,
                vIdPlanContable,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

            if (vlstCuentasContablesNICSP.Count() > 0)
            {
                CuentasContablesNICSP vCuentasContablesNICSP = vlstCuentasContablesNICSP.FirstOrDefault();

                hfIdCuentasContablesNICSP.Value = vCuentasContablesNICSP.IdCuentasContablesNICSP.ToString();

                txtCodigoCuentaIntArea.Text = vCuentasContablesNICSP.CodCtaContSiifIntArea;
                txtNombreCuentaIntArea.Text = vCuentasContablesNICSP.DescCtaContSiifIntArea;
                hfIdCuentaIntArea.Value = vCuentasContablesNICSP.IdCodCtaConSiifConIntArea.ToString();

                txtCodigoCuentaCredito.Text = vCuentasContablesNICSP.CodCtaContSiifCredito;
                txtNombreCuentaCredito.Text = vCuentasContablesNICSP.DescCtaContSiifCredito;
                hfIdCuentaCredito.Value = vCuentasContablesNICSP.IdCodCtaConSiifCredito.ToString();

                txtCodigoCuentaDebito.Text = vCuentasContablesNICSP.CodCtaContSiifDebito;
                txtNombreCuentaDebito.Text = vCuentasContablesNICSP.DescCtaContSiifDebito;
                hfIdCuentaDebito.Value = vCuentasContablesNICSP.IdCodCtaConSiifDebito.ToString();

                ddlTipoProceso.SelectedValue = vCuentasContablesNICSP.IdTipoProceso.ToString();
                ddlFase.SelectedValue = vCuentasContablesNICSP.IdFaseProceso.ToString();
                ddlNaturaleza.SelectedValue = vCuentasContablesNICSP.IdNaturalezaProceso.ToString();
                ddlSubFase.SelectedValue = vCuentasContablesNICSP.IdSubFaseProceso.ToString();

                rblEstado.SelectedValue = vCuentasContablesNICSP.Estado ? "1" : "0";

                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCuentasContablesNICSP.UsuarioCrea, vCuentasContablesNICSP.FechaCrea, vCuentasContablesNICSP.UsuarioModifica, vCuentasContablesNICSP.FechaModifica);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Parametrización Contable", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("CuentasContablesNICSP.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("CuentasContablesNICSP.Eliminado");

            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlTipoProceso,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarTodosTipoProceso().Select(
                        x => new BaseDto
                        {
                            Id = x.IdTipoProceso.ToString(),
                            Name = x.NombreTipoProcesol
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlFase,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarFaseProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdFaseProceso.ToString(),
                            Name = x.DescripcionFaseProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlSubFase,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarSubFaseProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdSubFaseProceso.ToString(),
                            Name = x.DescripcionSubFaseProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlNaturaleza,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarNaturalezaProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdNaturalezaProceso.ToString(),
                            Name = x.DescripcionNaturalezaProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
