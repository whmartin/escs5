﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.ComparadorJuridico.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_CuentaContableNICSP_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Parametricas/CuentaContableNICSP";
    ComparadorJuridicoService vComparadorJuridicoService = new ComparadorJuridicoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdTipoProceso =  null;
            int? vIdFaseProceso = null;
            int? vIdSubFaseProceso = null;
            int? vIdNaturalezaProceso = null;
            int? vIdCodCtaConSiifConIntArea = null;
            int? vIdCodCtaConSiifDebito = null;
            int? vIdCodCtaConSiifCredito = null;

            if(ddlTipoProceso.SelectedValue != "-1" && ddlTipoProceso.SelectedValue != null)
            {
                vIdTipoProceso = Convert.ToInt32(ddlTipoProceso.SelectedValue);
            }

            if (ddlFase.SelectedValue != "-1" && ddlFase.SelectedValue != null)
            {
                vIdFaseProceso = Convert.ToInt32(ddlFase.SelectedValue);
            }

            if (ddlSubFase.SelectedValue != "-1" && ddlSubFase.SelectedValue != null)
            {
                vIdSubFaseProceso = Convert.ToInt32(ddlSubFase.SelectedValue);
            }

            if (ddlNaturaleza.SelectedValue != "-1" && ddlNaturaleza.SelectedValue != null)
            {
                vIdNaturalezaProceso = Convert.ToInt32(ddlNaturaleza.SelectedValue);
            }

            if (ddlNaturaleza.SelectedValue != "-1" && ddlNaturaleza.SelectedValue != null)
            {
                vIdNaturalezaProceso = Convert.ToInt32(ddlNaturaleza.SelectedValue);
            }

            if (!String.IsNullOrWhiteSpace(hfIdCuentaIntArea.Value))
            {
                vIdCodCtaConSiifConIntArea = Convert.ToInt32(hfIdCuentaIntArea.Value);
            }

            if (!String.IsNullOrWhiteSpace(hfIdCuentaDebito.Value))
            {
                vIdCodCtaConSiifDebito = Convert.ToInt32(hfIdCuentaDebito.Value);
            }

            if (!String.IsNullOrWhiteSpace(hfIdCuentaCredito.Value))
            {
                vIdCodCtaConSiifCredito= Convert.ToInt32(hfIdCuentaCredito.Value);
            }

            gvPlanContable.DataSource = vComparadorJuridicoService.ConsultarCuentasContablesNICSPResumen(
                vIdTipoProceso,
                null,
                vIdFaseProceso,
                vIdSubFaseProceso,
                vIdNaturalezaProceso,
                vIdCodCtaConSiifConIntArea,
                vIdCodCtaConSiifDebito,
                vIdCodCtaConSiifCredito,
                null);

            gvPlanContable.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvPlanContable.PageSize = PageSize();
            gvPlanContable.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Parametrización Contable", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvPlanContable.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("CuentasContablesNICSP.idCuentasContablesNICSP", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvPlanContable_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvPlanContable.SelectedRow);
    }
    protected void gvPlanContable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPlanContable.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("CuentasContablesNICSP.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("CuentasContablesNICSP.Eliminado");

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlTipoProceso,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarTodosTipoProceso().Select(
                        x => new BaseDto
                        {
                            Id = x.IdTipoProceso.ToString(),
                            Name = x.NombreTipoProcesol
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlFase,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarFaseProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdFaseProceso.ToString(),
                            Name = x.DescripcionFaseProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlSubFase,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarSubFaseProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdSubFaseProceso.ToString(),
                            Name = x.DescripcionSubFaseProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );

            UtilComparadorJuridico.LlenarDropDownList
                (
                ddlNaturaleza,
                UtilComparadorJuridico.ToSelectedList(
                    vComparadorJuridicoService.ConsultarNaturalezaProceso(null, "", "", 1).Select(
                        x => new BaseDto
                        {
                            Id = x.IdNaturalezaProceso.ToString(),
                            Name = x.DescripcionNaturalezaProceso
                        }).ToList(),
                    true,
                    "-1",
                    true)
                    );
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void hfIdCuentaIntArea_ValueChanged(object sender, EventArgs e)
    {
        txtCodigoCuentaIntArea.Text = hfCodigoCuentaIntArea.Value;
        txtNombreCuentaIntArea.Text = hfNombreCuentaIntArea.Value;
    }

    protected void hfIdCuentaCredito_ValueChanged(object sender, EventArgs e)
    {
        txtCodigoCuentaCredito.Text = hfCodigoCuentaCredito.Value;
        txtNombreCuentaCredito.Text = hfNombreCuentaCredito.Value;
    }

    protected void hfIdCuentaDebito_ValueChanged(object sender, EventArgs e)
    {
        txtCodigoCuentaDebito.Text = hfCodigoCuentaDebito.Value;
        txtNombreCuentaDebito.Text = hfNombreCuentaDebito.Value;
    }
}
