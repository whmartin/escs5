﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_CuentaContableNICSP_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" lang="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
        $(function () {

            $('#<%= imgBuscarCuentaIntArea.ClientID %>').click(function (e) {
                window_showModalDialog('../../../Page/Parametricas/CuentaContableNICSP/CodCtaConSiif.aspx', setReturnGetCodCtaConIntArea, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            });
        });

        function setReturnGetCodCtaConIntArea(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 1) {
                    document.getElementById('<%= txtCodigoCuentaIntArea.ClientID %>').value = retLupa[1];
                    document.getElementById('<%= hfCodigoCuentaIntArea.ClientID %>').value = retLupa[1];
                    document.getElementById('<%= txtNombreCuentaIntArea.ClientID %>').value = retLupa[2];
                    document.getElementById('<%= hfNombreCuentaIntArea.ClientID %>').value = retLupa[2];
                    document.getElementById('<%= hfIdCuentaIntArea.ClientID %>').value = retLupa[0];
                }
            }

            return false;
        }

        $(function () {

            $('#<%= imgBuscarCuentaCredito.ClientID %>').click(function (e) {
                window_showModalDialog('../../../Page/Parametricas/CuentaContableNICSP/CodCtaConSiif.aspx', setReturnGetCodCtaConCredito, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            });
        });

            function setReturnGetCodCtaConCredito(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 1) {
                        document.getElementById('<%= txtCodigoCuentaCredito.ClientID %>').value = retLupa[1];
                        document.getElementById('<%= hfCodigoCuentaCredito.ClientID %>').value = retLupa[1];
                        document.getElementById('<%= txtNombreCuentaCredito.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= hfNombreCuentaCredito.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= hfIdCuentaCredito.ClientID %>').value = retLupa[0];
                    }
                }
            }

            $(function () {

                $('#<%= imgBuscarCuentaDebito.ClientID %>').click(function (e) {
                    window_showModalDialog('../../../Page/Parametricas/CuentaContableNICSP/CodCtaConSiif.aspx', setReturnGetCodCtaConDebito, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                });
            });
            function setReturnGetCodCtaConDebito(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 1) {
                        document.getElementById('<%= txtCodigoCuentaDebito.ClientID %>').value = retLupa[1];
                        document.getElementById('<%= hfCodigoCuentaDebito.ClientID %>').value = retLupa[1];
                        document.getElementById('<%= txtNombreCuentaDebito.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= hfNombreCuentaDebito.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= hfIdCuentaDebito.ClientID %>').value = retLupa[0];
                    }
                }
            }

            function cleanCodCtaConIntArea() {
                document.getElementById('<%= txtCodigoCuentaIntArea.ClientID %>').value = '';
                document.getElementById('<%= txtNombreCuentaIntArea.ClientID %>').value = '';
                document.getElementById('<%= hfIdCuentaIntArea.ClientID %>').value = '';
                document.getElementById('<%= hfCodigoCuentaIntArea.ClientID %>').value = '';
                document.getElementById('<%= hfNombreCuentaIntArea.ClientID %>').value = '';

            }

            function cleanCodCtaConCredito() {
                document.getElementById('<%= txtCodigoCuentaCredito.ClientID %>').value = '';
                document.getElementById('<%= txtNombreCuentaCredito.ClientID %>').value = '';
                document.getElementById('<%= hfIdCuentaCredito.ClientID %>').value = '';
                document.getElementById('<%= hfCodigoCuentaCredito.ClientID %>').value = '';
                document.getElementById('<%= hfNombreCuentaCredito.ClientID %>').value = '';

            }

            function cleanCodCtaConDebito() {
                document.getElementById('<%= txtCodigoCuentaDebito.ClientID %>').value = '';
                document.getElementById('<%= txtNombreCuentaDebito.ClientID %>').value = '';
                document.getElementById('<%= hfIdCuentaDebito.ClientID %>').value = '';
                document.getElementById('<%= hfCodigoCuentaDebito.ClientID %>').value = '';
                document.getElementById('<%= hfNombreCuentaDebito.ClientID %>').value = '';

            }
    </script>

    <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="conditional">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlConsulta">
                <table align="Center" style="width: 90%">
                    <tr class="rowB">
                        <td>Código Cuenta Interáreas</td>
                        <td>Nombre de la Cuenta Interáreas</td>
                        <td>Tipo Proceso</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtCodigoCuentaIntArea" ReadOnly="True"></asp:TextBox>
                            <asp:Image ID="imgBuscarCuentaIntArea" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif" Style="cursor: pointer;" TabIndex="26" ToolTip="Buscar" Visible="True" />
                            <asp:HiddenField runat="server" ID="hfIdCuentaIntArea" OnValueChanged="hfIdCuentaIntArea_ValueChanged"></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfCodigoCuentaIntArea"></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfNombreCuentaIntArea"></asp:HiddenField>
                            <asp:LinkButton ID="btnLimpiarCuentaIntArea" runat="server" CssClass="vinculo" OnClientClick="cleanCodCtaConIntArea();return false">Limpiar</asp:LinkButton>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreCuentaIntArea" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipoProceso" runat="server" Height="22px" Width="128px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Código Cuenta Crédito</td>
                        <td>Nombre de la Crédito</td>
                        <td>Fase</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtCodigoCuentaCredito" ReadOnly="True"></asp:TextBox>
                            <asp:Image ID="imgBuscarCuentaCredito" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif" Style="cursor: pointer;" TabIndex="26" ToolTip="Buscar" Visible="True" />
                            <asp:HiddenField runat="server" ID="hfIdCuentaCredito" OnValueChanged="hfIdCuentaCredito_ValueChanged"></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfCodigoCuentaCredito"></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfNombreCuentaCredito"></asp:HiddenField>
                            <asp:LinkButton ID="btnLimpiarCuentaCredito" runat="server" CssClass="vinculo" OnClientClick="cleanCodCtaConCredito();return false">Limpiar</asp:LinkButton>

                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreCuentaCredito" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFase" runat="server" Height="22px" Width="128px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Código Cuenta Debito</td>
                        <td>Nombre Cuenta Debito*
                        </td>
                        <td>Sub Fase</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtCodigoCuentaDebito" ReadOnly="True"></asp:TextBox>
                            <asp:Image ID="imgBuscarCuentaDebito" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif" Style="cursor: pointer;" TabIndex="26" ToolTip="Buscar" Visible="True" />
                            <asp:HiddenField runat="server" ID="hfIdCuentaDebito" OnValueChanged="hfIdCuentaDebito_ValueChanged"></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfCodigoCuentaDebito"></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfNombreCuentaDebito"></asp:HiddenField>
                            <asp:LinkButton ID="btnLimpiarCuentaDebito" runat="server" CssClass="vinculo" OnClientClick="cleanCodCtaConDebito();return false">Limpiar</asp:LinkButton>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreCuentaDebito" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSubFase" runat="server" Height="22px" Width="128px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Naturaleza</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlNaturaleza" runat="server" Height="22px" Width="128px">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlLista">
                <table align="Center" style="width: 90%">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvPlanContable" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdCuentasContablesNICSP" CellPadding="0" Height="16px"
                                OnPageIndexChanging="gvPlanContable_PageIndexChanging" OnSelectedIndexChanged="gvPlanContable_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                Height="16px" Width="16px" ToolTip="Detalle" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Tipo Proceso" DataField="TipoProceso" ItemStyle-Wrap="true">
                                        <ControlStyle Width=""/>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Naturaleza" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" SortExpression="Naturaleza">
                                        <ItemTemplate>
                                            <div style="word-wrap: break-word; max-width: 300px; width: auto">
                                                <%#Eval("Naturaleza")%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fase" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" SortExpression="Fase">
                                        <ItemTemplate>
                                            <div style="word-wrap: break-word; max-width: 300px; width: auto">
                                                <%#Eval("Fase")%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SubFase" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" SortExpression="SubFase">
                                        <ItemTemplate>
                                            <div style="word-wrap: break-word; max-width: 300px; width: auto">
                                                <%#Eval("SubFase")%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="CuentaDebito" DataField="CuentaDebito" ItemStyle-Wrap="true" />
                                    <asp:BoundField HeaderText="CuentaCredito" DataField="CuentaCredito" ItemStyle-Wrap="true" />
                                    <asp:BoundField HeaderText="CuentaInterAreas" DataField="CuentaInterAreas" ItemStyle-Wrap="true" />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
