<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_CuentaContableNICSP_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="conditional">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlConsulta">
                <asp:HiddenField runat="server" ID="hfIdCuentasContablesNICSP"></asp:HiddenField>

                <asp:HiddenField ID="hfIdCuentaIntArea" runat="server" />
                <asp:HiddenField ID="hfIdCuentaCredito" runat="server" />
                <asp:HiddenField ID="hfIdCuentaDebito" runat="server" />

                <table align="Center" style="width: 90%">
                    <tr class="rowB">
                        <td>C�digo Cuenta Inter�reas*</td>
                        <td>Nombre de la Cuenta Inter�reas *
                        </td>
                        <td>Tipo Proceso *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtCodigoCuentaIntArea" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreCuentaIntArea" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipoProceso" runat="server" Height="22px" Width="128px" Enabled="False">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>C�digo Cuenta Cr�dito*
                        </td>
                        <td>Nombre de la Cr�dito*
                        </td>
                        <td>Fase *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtCodigoCuentaCredito" ReadOnly="True"></asp:TextBox>

                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreCuentaCredito" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFase" runat="server" Height="22px" Width="128px" Enabled="False">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>C�digo Cuenta Debito*
                        </td>
                        <td>Nombre Cuenta Debito*
                        </td>
                        <td>Sub Fase *</td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtCodigoCuentaDebito" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNombreCuentaDebito" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSubFase" runat="server" Height="22px" Width="128px" Enabled="False">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>Naturaleza*
                        </td>
                        <td>Estado *</td>
                        <td></td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList ID="ddlNaturaleza" runat="server" Height="22px" Width="128px" Enabled="False">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" style="text-align: left">
                            </asp:RadioButtonList>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
