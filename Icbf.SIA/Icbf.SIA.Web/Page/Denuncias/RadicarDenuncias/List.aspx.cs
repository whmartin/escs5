﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Entity;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Utilities.Session;


/// <summary>
/// Pàgina inicial de login de OAC
/// </summary>
public partial class Page_Denuncias_RadicarDenuncias_List : GeneralWeb
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GetSessionUser();


        masterPrincipal toolBar;
        toolBar = (masterPrincipal)this.Master;
        toolBar.EstablecerTitulos("Registro de Denuncias Bienes Vacantes, Mostrencos y Vocaciones Hereditarias", SolutionPage.Detail.ToString());

        Usuario pUsuario = new Usuario();
        pUsuario = (Usuario)(GetSessionParameter("App.User"));

        ProveedorService vProveedorService = new ProveedorService();
        Tercero vTercero = vProveedorService.ConsultarTerceroProviderUserKey(pUsuario.Providerkey);
        if (vTercero.Direccion == null || vTercero.Direccion.Trim() == "")
        {
            NavigateTo(@"~/Page/PROVEEDOR/GestionTercero/List.aspx", true);
        }
        else
        {
            string urlProcessMaker = System.Configuration.ConfigurationManager.AppSettings.Get("URLProccessMaker").ToString();
            //this.Response.Write("<iframe id='Iframe1' name='center' class='ui-layout-center' noresize='true' overflow='scroll' width='99%' height='99%' src='http://172.20.1.47/sysicbf/es/neoclassic/37813404656e2fc7364a807037718290/13849173956e301c33e7147057915547.php?id='" + pUsuario.IdUsuario + " frameborder='0' marginheight='0' marginwidth='0' runat='server'></iframe>");
            divProccesMaker.InnerHtml = string.Format("<iframe id='Iframe2' name='center' class='ui-layout-center' noresize='true' overflow='scroll' width='100%' height='100%' src='" + urlProcessMaker + "?id={0}' frameborder='0' marginheight='0' marginwidth='0' runat='server'></iframe>", pUsuario.IdUsuario);
            //this.Response.Write(string.Format("<iframe id='Iframe2' name='center' class='ui-layout-center' noresize='true' overflow='scroll' width='100%' height='100%' src='" + urlProcessMaker + "?id={0}' frameborder='0' marginheight='0' marginwidth='0' runat='server'></iframe>", pUsuario.IdUsuario));
        }


    }
}