﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.SIA.Entity.Concursales;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_Concursales_Procesos_Detail : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "Concursales/Procesos";

    SIAService vSIAService = new SIAService();

    public List<ProcesoConcursalDocumento> documentosAdjuntos
    {
        get
        {
            List<ProcesoConcursalDocumento> resultList = new List<ProcesoConcursalDocumento>();

            if(ViewState["DocumentosAdjuntos"] != null)
                resultList = ViewState["DocumentosAdjuntos"] as List<ProcesoConcursalDocumento>;

            return resultList;
        }
        set
        {
            ViewState["DocumentosAdjuntos"] = value;
        }
    } 

    ConcursalesService vConcursalesService = new ConcursalesService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if(!Page.IsPostBack)
            {
                CargarDatos();
            }
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        ManageNavigateTo();
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Detalle Proceso Concursal", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

       /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if(VerificarQueryStrings())
            {
                int id = int.Parse(hfIdentificacion.Value.ToString());
                var item = vConcursalesService.ObtenerProcesosConcursalPorId(id);

                txtRegional.Text = item.Regional.ToUpper();
                txtRazonSocial.Text = item.RazonSocial.ToUpper();
                txtCapitalInicial.Text = string.Format("{0:$#,##0}", item.CapitalInicial);
                txtInteresesInicial.Text = string.Format("{0:$#,##0}", item.InteresesInicial);
                txtSaldoCapital.Text = string.Format("{0:$#,##0}", item.SaldoCapital);
                txtSaldoIntereses.Text = string.Format("{0:$#,##0}", item.SaldoInteres);

                imgHistorico.OnClientClick = string.Format("ShowHistorico('{0}'); return false;", hfIdentificacion.Value.ToString());

                var items = vConcursalesService.ObtenerDocumentosPorIdProceso(id);
                gvDocumentos.DataSource = items;
                gvDocumentos.DataBind();
            }
            else
                ManageNavigateTo();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ManageNavigateTo()
    {
        if(Request.QueryString["Origen"] == "R")
            NavigateTo("../Reparto/List.aspx");
        else
            NavigateTo(SolutionPage.List);
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["Id"]))
        {
          hfIdentificacion.Value = Request.QueryString["Id"];
            isvalid = true;
        }

        return isvalid;
    }

}

