<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Concursales_Solicitudes_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

                 <script type="text/javascript" language="javascript">

                 function ShowHistorico(IdProceso) {
                 var url = '../../../Page/Concursales/Lupas/LupaHistoricoProceso.aspx?IdProceso=' + IdProceso;
                 window_showModalDialog(url, setReturnHistorico, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
             }

                 function setReturnHistorico() {

             }

             </script>

 <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Indentificaci&oacute;n</td>
            <td class="Cell">
                Usuario Asignado</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdentificacion" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdentificacion" FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:TextBox ID="txtUsuarioAsignado" runat="server" Height="16px" MaxLength="30" Width="245px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio</td>
            <td class="Cell">
                Estado</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlEstados" runat="server"></asp:DropDownList>
            </td>
        </tr> 
                <tr class="rowB">
            <td class="Cell">
                Consecutivo</td>
            <td class="Cell">
                 
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox ID="txtConsecutivo" runat="server" Height="16px" MaxLength="30" Width="245px"></asp:TextBox>
            </td>
            <td class="Cell">

            </td>
        </tr> 
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProcesoConcursal" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdProceso" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvProcesoConcursal_PageIndexChanging" 
                        OnSorting="gvProcesoConcursal_Sorting" 
                        OnRowDataBound="gvProcesoConcursal_RowDataBound"
                        OnSelectedIndexChanged="gvProcesoConcursal_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Hist&oacute;rico">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnHistorico"  runat="server" AutoPostBack="false"   Height="16px" Width="16px" >
                                    <img alt="Editar" src="../../../Image/btn/icoPagBuscar.gif"  height="16px" width="16px"  title="Hist&oacute;rico" ToolTip="ver hist&oacute;rico" />
                                    </asp:LinkButton>
                                 </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                                <asp:BoundField HeaderText="Raz&oacute;n Social" DataField="RazonSocial"  SortExpression="razonSocial"/>
                                <asp:BoundField HeaderText="Capital Inicial" DataField="CapitalInicial"  DataFormatString="{0:C}"  SortExpression="capitalInicial"/>
                                <asp:BoundField HeaderText="Intereses Inicial" DataField="InteresesInicial"  DataFormatString="{0:C}" SortExpression="interesesInicial"/>
                                <asp:BoundField HeaderText="Saldo Capital" DataField="SaldoCapital"  DataFormatString="{0:C}"  SortExpression="saldoCapital"/>
                                <asp:BoundField HeaderText="Saldo Intereses" DataField="SaldoInteres"  DataFormatString="{0:C}"  SortExpression="saldoIntereses"/>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstado"  SortExpression="NombreEstado"/>
                              
                             </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
