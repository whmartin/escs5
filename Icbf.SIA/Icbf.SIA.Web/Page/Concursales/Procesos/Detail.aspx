﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Concursales_Procesos_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  

             <script type="text/javascript" language="javascript">

                 function ShowHistorico(idProceso) {

                     var url = '../../../Page/Concursales/Lupas/LupaHistoricoProceso.aspx?idProceso=' + idProceso;
                 window_showModalDialog(url, setReturnHistorico, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
             }

                 function setReturnHistorico() {

             }

             </script>

        <table width="90%" id="pnInfoSolicitud" runat="server" align="center" >
        <tr class="rowB">
            <td>
                Regional          
            </td>
            <td>
             Hist&oacute;rico de la solicitud                         
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtRegional" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                        <asp:ImageButton ID="imgHistorico" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                         Style="cursor: hand" ToolTip="Ver Hist&oacute;rico" />
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
                Raz&oacute;n Social
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                    <asp:TextBox ID="txtRazonSocial" runat="server" Enabled="false" Width="90%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Capital Inicial
            </td>
            <td>
               Intereses Inicial                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtCapitalInicial" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtInteresesInicial" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Saldo Capital
            </td>
            <td>
               Saldo Intereses                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtSaldoCapital" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtSaldoIntereses" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
                Documentos        
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px" DataKeyNames="IdDocumento"
                        >
                        <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                     <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreDocumento", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=ProcesosConcursales") %>'>Archivo</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>  
                                <asp:BoundField HeaderText="NombreOriginal" DataField="NombreOriginal" />
                                <asp:BoundField HeaderText="Tipo de Documento" DataField="TipoDocumento" />
                                <asp:BoundField HeaderText="Tipo de Publicac&oacute;n" DataField="TipoPublicacion"  />
                                <asp:BoundField HeaderText="Medio de Publicaci&oacute;n" DataField="MedioPublicacion"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
        </table>

        <asp:HiddenField ID="hfIdentificacion" runat="server" />

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
    </style>
</asp:Content>

