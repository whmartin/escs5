using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;
using System.Linq.Expressions;
using Icbf.SIA.Entity.Concursales;

public partial class Page_Concursales_Solicitudes_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "Concursales/Solicitudes";

    SIAService vRuboService = new SIAService();

    ConcursalesService vConcursalesService = new ConcursalesService();

    ManejoControlesConcursales manejoControles = new ManejoControlesConcursales();


    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();

            }
           
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void gvProcesoConcursal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProcesoConcursal.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvProcesoConcursal_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProcesoConcursal.SelectedRow);
    }

    protected void gvProcesoConcursal_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvProcesoConcursal, GridViewSortExpression, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += Nuevo;
            gvProcesoConcursal.PageSize = PageSize();
            gvProcesoConcursal.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gesti&oacute;n de Procesos Concursales", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        string controlFechaValidando = string.Empty;
        try
        {
            int vIDRegional = 0;
            string vIdentificacion = null;
            string usuarioasignado = null;
            string consecutivo = null;
            int? vIdEstado = null;

            if (txtIdentificacion.Text != "")
                vIdentificacion = txtIdentificacion.Text;

            if(txtUsuarioAsignado.Text != "")
                usuarioasignado = txtUsuarioAsignado.Text;

            if(txtConsecutivo.Text != "")
                consecutivo = txtConsecutivo.Text;

            if(ddlIDRegional.SelectedValue != "-1")
                vIDRegional = int.Parse(ddlIDRegional.SelectedValue.ToString());

            if(ddlEstados.SelectedValue != "-1")
                vIdEstado = int.Parse(ddlEstados.SelectedValue.ToString());
            
            var myGridResults = vConcursalesService.ObtenerProcesosConcursales(vIDRegional, vIdentificacion, vIdEstado, usuarioasignado, consecutivo);

            int nRegistros = 0;

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if(nRegistros < NumRegConsultaGrilla)
            {
                if(expresionOrdenamiento != null)
                {
                    if(string.IsNullOrEmpty(GridViewSortExpression))
                        GridViewSortDirection = SortDirection.Ascending;
                    else if(GridViewSortExpression != expresionOrdenamiento)
                        GridViewSortDirection = SortDirection.Descending;

                    if(myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(ProcesoConcursal), expresionOrdenamiento);

                        var prop = Expression.Property(param, expresionOrdenamiento);

                        var sortExpression = Expression.Lambda<Func<ProcesoConcursal, object>>(Expression.Convert(prop, typeof(object)), param);

                        if(GridViewSortDirection == SortDirection.Ascending)
                        {
                            if(cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            if(cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                    gridViewsender.DataSource = myGridResults;

                gridViewsender.DataBind();
            }
            else
                toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inv�lido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if(GetSessionUser().IdTipoUsuario != 1)
            {
                txtUsuarioAsignado.Text = GetSessionUser().NombreUsuario;
                txtUsuarioAsignado.Enabled = false;
            }

            CargarEstados();
            CargarListaTiposDocumentos();
            CargarListaRegional();

            if(!string.IsNullOrEmpty(Request.QueryString["carteraDuplicada"]))
                toolBar.MostrarMensajeGuardado("La cartera seleccionada ya tiene un proceso concursal activo.");

            if (!string.IsNullOrEmpty(Request.QueryString["consecutivoProcesoConcursal"]))
            {
                string consecutivo = Request.QueryString["consecutivoProcesoConcursal"];
                toolBar.MostrarMensajeGuardado("Se inicio el proceso concursal, con el consecutivo N&uacute;mero: "+ consecutivo);
               
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaTiposDocumentos()
    { 

    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if(usuario != null)
        {
            if(usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIsNMF(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");

                if(ddlIDRegional.Items.Count > 0)
                {
                    if(usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                    }
                }
            }
            else
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
        }
    }

    public void CargarEstados()
    {
        string[] estadosOmitir = new string [] { "REP" };

        List<ProcesoConcursalEstado> estados = vConcursalesService.ConsultarEstadosOmitir(estadosOmitir);

        ManejoControlesContratos.LlenarComboLista(ddlEstados, estados, "IdEstado", "Nombre");
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string id = gvProcesoConcursal.DataKeys[rowIndex].Value.ToString();
            string url = string.Format("Detail.aspx?Id={0}&Origen=G",id);
            NavigateTo(url);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProcesoConcursal_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            string estadoSol = DataBinder.Eval(e.Row.DataItem, "CodigoEstado").ToString();

            LinkButton button = (LinkButton)e.Row.FindControl("btnHistorico");
            string id = DataBinder.Eval(e.Row.DataItem, "IdProceso").ToString();
            string script = string.Format("ShowHistorico('{0}'); return false;", id);
            button.Attributes.Add("onclick", script);
        }
    }

}
