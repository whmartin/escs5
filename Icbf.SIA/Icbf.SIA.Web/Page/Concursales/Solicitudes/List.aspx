<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Concursales_Solicitudes_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
 <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Indentificaci&oacute;n</td>
            <td class="Cell">
                Tipo de Documento</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdentificacion" MaxLength="20" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdentificacion" FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoDocumentos"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Cartera Activa</td>
            <td class="Cell">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                &nbsp;</td>
        </tr> 

    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSolicitudesCartera" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="identificacion,capitalInicial,interesesInicial,saldoCapital,saldoIntereses" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvSolicitudesCartera_PageIndexChanging" OnSorting="gvSolicitudesCartera_Sorting" OnSelectedIndexChanged="gvSolicitudesCartera_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                                <asp:BoundField HeaderText="Raz&oacute;n Social" DataField="razonSocial"  SortExpression="razonSocial"/>
                                <asp:BoundField HeaderText="Identificaci&oacute;n" DataField="identificacion"  SortExpression="identificacion"/>
                                <asp:BoundField HeaderText="Capital Inicial" DataField="capitalInicial"  DataFormatString="{0:C}"  SortExpression="capitalInicial"/>
                                <asp:BoundField HeaderText="Intereses Inicial" DataField="interesesInicial"  DataFormatString="{0:C}" SortExpression="interesesInicial"/>
                                <asp:BoundField HeaderText="Saldo Capital" DataField="saldoCapital"  DataFormatString="{0:C}"  SortExpression="saldoCapital"/>
                                <asp:BoundField HeaderText="Saldo Intereses" DataField="saldoIntereses"  DataFormatString="{0:C}"  SortExpression="saldoIntereses"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
