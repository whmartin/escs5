using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;
using System.Linq.Expressions;
using Icbf.SIA.Entity.Concursales;

public partial class Page_Concursales_Solicitudes_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "Concursales/Solicitudes";

    SIAService vRuboService = new SIAService();

    ConcursalesService vConcursalesService = new ConcursalesService();

    ManejoControlesConcursales manejoControles = new ManejoControlesConcursales();


    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void gvSolicitudesCartera_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSolicitudesCartera.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvSolicitudesCartera_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSolicitudesCartera.SelectedRow);
    }

    protected void gvSolicitudesCartera_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvSolicitudesCartera, GridViewSortExpression, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += Nuevo;
            gvSolicitudesCartera.PageSize = PageSize();
            gvSolicitudesCartera.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registro de Solicitud de Proceso Concursal", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        string controlFechaValidando = string.Empty;
        try
        {
            int? vIdTipoDocumento = null;
            string vIDRegional = null;
            string vIdentificacion = null;

            if (txtIdentificacion.Text != "")
                vIdentificacion = txtIdentificacion.Text;
            
            if (ddlTipoDocumentos.SelectedValue != "-1")
                vIdTipoDocumento = Convert.ToInt32(ddlTipoDocumentos.SelectedValue);

            if(ddlIDRegional.SelectedValue != "-1")
            {
                var regional = vRuboService.ConsultarRegionalPCIsNMF(null, ddlIDRegional.SelectedItem.Text);

                if(regional != null)
                    vIDRegional = regional.First().CodigoRegionalPCI;
            }

            var myGridResults = vConcursalesService.ConsultarCarteraActivaAgrupada(vIdentificacion,vIdTipoDocumento,vIDRegional);

            int nRegistros = 0;

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if(nRegistros < NumRegConsultaGrilla)
            {
                if(expresionOrdenamiento != null)
                {
                    if(string.IsNullOrEmpty(GridViewSortExpression))
                        GridViewSortDirection = SortDirection.Ascending;
                    else if(GridViewSortExpression != expresionOrdenamiento)
                        GridViewSortDirection = SortDirection.Descending;

                    if(myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(CarteraAgrupada), expresionOrdenamiento);

                        var prop = Expression.Property(param, expresionOrdenamiento);

                        var sortExpression = Expression.Lambda<Func<CarteraAgrupada, object>>(Expression.Convert(prop, typeof(object)), param);

                        if(GridViewSortDirection == SortDirection.Ascending)
                        {
                            if(cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            if(cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                    gridViewsender.DataSource = myGridResults;

                gridViewsender.DataBind();
            }
            else
                toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inv�lido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaTiposDocumentos();
            CargarListaRegional();

            if(!string.IsNullOrEmpty(Request.QueryString["carteraDuplicada"]))
                toolBar.MostrarMensajeGuardado("La cartera seleccionada ya tiene un proceso concursal activo.");

            if(!string.IsNullOrEmpty(Request.QueryString["consecutivoProcesoConcursal"]))
            {
                string consecutivo = Request.QueryString["consecutivoProcesoConcursal"];
                toolBar.MostrarMensajeGuardado("Se inicio el proceso concursal, con el consecutivo N&uacute;mero: " + consecutivo);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaTiposDocumentos()
    { 
      manejoControles.LlenarComboTipoIdentificacionNMF(ddlTipoDocumentos, null,true);
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if(usuario != null)
        {
            if(usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIsNMF(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");

                if(ddlIDRegional.Items.Count > 0)
                {
                    if(usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        //ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        //ddlIDRegional.Enabled = false;
                    }
                }
            }
            else
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;

            string identificacion = gvSolicitudesCartera.DataKeys[rowIndex].Values[0].ToString();
            string capitalInicial = gvSolicitudesCartera.DataKeys[rowIndex].Values[1].ToString();
            string interesesInicial = gvSolicitudesCartera.DataKeys[rowIndex].Values[2].ToString();
            string saldoCapital = gvSolicitudesCartera.DataKeys[rowIndex].Values[3].ToString();
            string saldoIntereses = gvSolicitudesCartera.DataKeys[rowIndex].Values[4].ToString();

            KeyValuePair<int, bool> ValidarCartera = vConcursalesService.ValidarCarteraTercero(identificacion);

            if(ValidarCartera.Key > 0 && !ValidarCartera.Value)
            {
                string url = string.Format
                                (
                                "Detail.aspx?identificacion={0}&idTercero={1}&capitalInicial={2}&interesesInicial={3}&saldoCapital={4}&saldoIntereses={5}", 
                                identificacion, 
                                ValidarCartera.Key,
                                capitalInicial,
                                interesesInicial,
                                saldoCapital,
                                saldoIntereses
                                );

                NavigateTo(url);
            }
            else
            {
                if(ValidarCartera.Value)
                    toolBar.MostrarMensajeError("La cartera ya se encuentra asociada a un proceso concursal.");
                else
                    toolBar.MostrarMensajeError("El tercero no se encuentra registrado en el sistema de informaci&oacute;n de terceros, por favor registrelo e intente nuevamente");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


}
