﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Concursales_Solicitudes_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">


                function ConfirmarAprobar() {
                    return confirm('Esta seguro de que desea iniciar un proceso concursal?');
                }

            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }

            function ReturnCallBack() {
                ocultaImagenLoading();
            }

            function ConfirmarAprobar() {
                return confirm('Esta seguro de que desea iniciar un proceso concursal. ');
            }


            function ValidaEliminacion(tipo) {

                 if (tipo == 'Documento')
                    return confirm('Esta seguro de que desea eliminar el Documento?');
            }


        </script>

        <table width="90%" id="pnInfoSolicitud" runat="server" align="center" >
        <tr class="rowB">
            <td>
                Regional          
            </td>
            <td>
                                      
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtRegional" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
                Raz&oacute;n Social
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                    <asp:TextBox ID="txtRazonSocial" runat="server" Enabled="false" Width="90%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Capital Inicial
            </td>
            <td>
               Intereses Inicial                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtCapitalInicial" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtInteresesInicial" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Saldo Capital
            </td>
            <td>
               Saldo Intereses                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtSaldoCapital" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtSaldoIntereses" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
            <td colspan="2">
                Detalle        
                Cartera        
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" class="Cell">
        <table width="100%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSolicitudesCartera" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px"
                        >
                        <Columns>
                                <asp:BoundField HeaderText="Resoluci&oacute;n" DataField="resolucion"  SortExpression="resolucion"/>
                                <asp:BoundField HeaderText="Capital Inicial" DataField="capitalInicial"  DataFormatString="{0:C}"  SortExpression="capitalInicial"/>
                                <asp:BoundField HeaderText="Intereses Inicial" DataField="interesesInicial"  DataFormatString="{0:C}" SortExpression="interesesInicial"/>
                                <asp:BoundField HeaderText="Saldo Capital" DataField="saldoCapital"  DataFormatString="{0:C}"  SortExpression="saldoCapital"/>
                                <asp:BoundField HeaderText="Saldo Intereses" DataField="saldoIntereses"  DataFormatString="{0:C}"  SortExpression="saldoIntereses"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>        
            </td>
            </tr>
        <tr class="rowB">
            <td>
               Documento Soporte
               <asp:RequiredFieldValidator ID="rfvDocumentoSoporte" runat="server" ControlToValidate="ddlTipoDocumento" Display="Dynamic" Enabled="true" InitialValue="-1" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnAdjuntar"></asp:RequiredFieldValidator>
            </td>
            <td>
               Fecha de Publicaci&oacute;n
                <asp:RequiredFieldValidator runat="server" ID="rfFechaProrroga" ControlToValidate="txtFechaPublicacionP"
                 SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnAdjuntar" 
                 ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="REFechaProrroga" ControlToValidate="txtFechaPublicacionP" SetFocusOnError="true" ErrorMessage="Formato Incorrecto"
                   Display="Dynamic" ValidationExpression="(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)"
                     ValidationGroup="btnAdjuntar" ForeColor="Red" ></asp:RegularExpressionValidator>                                   
            </td>
        </tr>
       <tr class="rowA">
               <td class="Cell">
                     <asp:DropDownList runat="server" ID="ddlTipoDocumento" DataTextField="Nombre" DataValueField="IdValor" ></asp:DropDownList>
                </td>
                <td class="Cell">
                   <asp:TextBox ID="txtFechaPublicacionP" runat="server" Enabled="true" Width="80%"></asp:TextBox>
                     <asp:Image ID="imgFechaPublicacionP" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetFechaPublicacionP" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaPublicacionP" TargetControlID="txtFechaPublicacionP"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meeFechaPublicacionP"  runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaPublicacionP">
                    </Ajax:MaskedEditExtender>
                </td>
         </tr>
          <tr class="rowB">
            <td>
              Tipo de Publicaci&oacute;n 
                               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTipoPublicacion" Display="Dynamic" Enabled="true" InitialValue="-1" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnAdjuntar"></asp:RequiredFieldValidator>
            </td>
            <td>
               Medio de Publicaci&oacute;n    
                               <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlMedioPublicacion" Display="Dynamic" Enabled="true" InitialValue="-1" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnAdjuntar"></asp:RequiredFieldValidator>                                         
            </td>
        </tr>
         <tr class="rowA">
               <td class="Cell">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                          <asp:DropDownList runat="server" ID="ddlTipoPublicacion" DataTextField="Nombre" DataValueField="IdValor" AutoPostBack="true"  OnSelectedIndexChanged="ddlTipoPublicacion_SelectedIndexChanged" ></asp:DropDownList>
                          </ContentTemplate>
                         <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlTipoPublicacion" EventName="selectedindexchanged" />
                        </Triggers>
                     </asp:UpdatePanel>    
            </td>
                <td class="Cell">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>                    
                               <asp:DropDownList runat="server" ID="ddlMedioPublicacion" DataTextField="Nombre" DataValueField="IdValor"  ></asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
         </tr>
         <tr class="rowB">
            <td colspan="2" class="auto-style1">
              Archivo
            </td>
        </tr>
        <tr class="rowA">
               <td class="Cell">
                                  <asp:FileUpload Width="100%" ID="FileUploadArchivo" runat="server" /> 
                </td>
            <td class="Cell">
                <asp:ImageButton ID="ImgCargarDocumento" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="25px" Width="25px" ToolTip="Agregar"  ValidationGroup="btnAdjuntar"  OnClick="CargarDocumentoClick" />
            </td>
         </tr>
        <tr class="rowB">
            <td colspan="2">
                Documentos        
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px" DataKeyNames="IdDocumento"
                        >
                        <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEliminarDocumentos" runat="server" OnClick="btnEliminarDocumentosClick"
                                        OnClientClick="return ValidaEliminacion('Documento');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                        <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Nombre" DataField="NombreDocumento" />
                                <asp:BoundField HeaderText="Tipo de Documento" DataField="TipoDocumento" />
                                <asp:BoundField HeaderText="Tipo de Publicac&oacute;n" DataField="TipoPublicacion"  />
                                <asp:BoundField HeaderText="Medio de Publicaci&oacute;n" DataField="MedioPublicacion"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
        </table>

<%--        <asp:HiddenField ID="hfIdCarteraActiva" runat="server" />--%>
        <asp:HiddenField ID="hfIdentificacion" runat="server" />
        <asp:HiddenField ID="hfIdTercero" runat="server" />
<%--        <asp:HiddenField ID="hfResolucion" runat="server" />--%>
        <asp:HiddenField ID="hfCodigoRegional" runat="server" />
        <asp:HiddenField ID="hfCodigoRegionalPCI" runat="server" />

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
    </style>
</asp:Content>

