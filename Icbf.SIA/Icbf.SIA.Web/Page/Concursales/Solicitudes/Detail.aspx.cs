﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.SIA.Entity.Concursales;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_Concursales_Solicitudes_Detail : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "Concursales/Solicitudes";

    SIAService vSIAService = new SIAService();

    public List<ProcesoConcursalDocumento> documentosAdjuntos
    {
        get
        {
            List<ProcesoConcursalDocumento> resultList = new List<ProcesoConcursalDocumento>();

            if(ViewState["DocumentosAdjuntos"] != null)
                resultList = ViewState["DocumentosAdjuntos"] as List<ProcesoConcursalDocumento>;

            return resultList;
        }
        set
        {
            ViewState["DocumentosAdjuntos"] = value;
        }
    } 

    ConcursalesService vConcursalesService = new ConcursalesService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if(!Page.IsPostBack)
            {
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
            CarteraActiva item = new CarteraActiva();
            item.capitalInicial = decimal.Parse(txtCapitalInicial.Text.Replace("$",string.Empty));
            item.interesesInicial = decimal.Parse(txtInteresesInicial.Text.Replace("$", string.Empty));
            item.saldoCapital = decimal.Parse(txtSaldoCapital.Text.Replace("$", string.Empty));
            item.saldoIntereses = decimal.Parse(txtSaldoIntereses.Text.Replace("$", string.Empty));
            item.CodRegionalICBF = hfCodigoRegional.Value;
            item.CodRegionalPCI = hfCodigoRegionalPCI.Value;
            item.IdTercero = int.Parse(hfIdTercero.Value);
            item.UsuarioCrea = GetSessionUser().NombreUsuario;

            StringBuilder documentos = new StringBuilder();

            var itemsDocumentos = documentosAdjuntos;

            foreach(var itemDoc in itemsDocumentos)
                documentos.Append(itemDoc.IdDocumento + ",");

            item.IdsDocumentos = documentos.ToString();

            if(! string.IsNullOrEmpty(item.IdsDocumentos))
            {
                var result = vConcursalesService.IniciarProcesoConcursal(item);

                if(!string.IsNullOrEmpty(result))
                    NavigateTo("List.aspx?consecutivoProcesoConcursal=" + result);
                else
                    toolBar.MostrarMensajeError("Sucedio un error iniciado el proceso concursal, por favor intente nuevamente.");
            }
            toolBar.MostrarMensajeError("No existen documentos asociados al proceso concursal.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Registro de Inicio Proceso Concursal", SolutionPage.Detail.ToString());
            toolBar.SetAprobarConfirmation("return ConfirmarAprobar();");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

       /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if(VerificarQueryStrings())
            {
                string identifcacion = hfIdentificacion.Value.ToString();

                var validar = vConcursalesService.ValidarCarteraTercero(identifcacion);

                if(!validar.Value && validar.Key > 0)
                {
                    var listDetalles = vConcursalesService.ConsultarCarteraActiva(identifcacion,null,null);

                    decimal capitalInicial = decimal.Parse(Request.QueryString["capitalInicial"]);
                    decimal interesesInicial = decimal.Parse(Request.QueryString["interesesInicial"]);
                    decimal saldoCapital = decimal.Parse(Request.QueryString["saldoCapital"]);
                    decimal saldoIntereses = decimal.Parse(Request.QueryString["saldoIntereses"]);

                    if(listDetalles != null && listDetalles.Count() > 0)
                    {
                        var itemDetalle = listDetalles.FirstOrDefault();
                        txtCapitalInicial.Text = string.Format("{0:$#,##0}", capitalInicial);
                        txtInteresesInicial.Text = string.Format("{0:$#,##0}", interesesInicial);
                        txtRazonSocial.Text = itemDetalle.razonSocial;
                        txtSaldoCapital.Text = string.Format("{0:$#,##0}", saldoCapital);
                        txtSaldoIntereses.Text = string.Format("{0:$#,##0}", saldoIntereses);
                        hfCodigoRegionalPCI.Value = itemDetalle.CodRegionalPCI;
                        hfCodigoRegional.Value = itemDetalle.CodRegionalICBF;
                        toolBar.SetAprobarConfirmation("return ConfirmarAprobar();");
                        txtRegional.Text = itemDetalle.DescripcionPCI;

                        gvSolicitudesCartera.DataSource = listDetalles;
                        gvSolicitudesCartera.DataBind();
                    }

                    var tipoDocumentos = vConcursalesService.ConsultarParametricaPorCodigo("TDOC");
                    ddlTipoDocumento.DataSource = tipoDocumentos;
                    ddlTipoDocumento.DataBind();
                    ddlTipoDocumento.Items.Insert(0, new ListItem() { Text = "-- Seleccione --", Value = "-1" });

                    var tipoPublicacion = vConcursalesService.ConsultarParametricaPorCodigo("TPUB");
                    ddlTipoPublicacion.DataSource = tipoPublicacion;
                    ddlTipoPublicacion.DataBind();
                    ddlTipoPublicacion.Items.Insert(0, new ListItem() { Text="-- Seleccione --", Value = "-1"  });

                   ddlMedioPublicacion.Items.Insert(0, new ListItem() { Text = "-- Seleccione --", Value = "-1" });
                }
                else
                    Response.Redirect("List.aspx?carteraDuplicada=1");
            }
            else
                NavigateTo(SolutionPage.List);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["identificacion"]))
        {
          hfIdentificacion.Value = Request.QueryString["identificacion"];
            isvalid = true;
        }

        if(!string.IsNullOrEmpty(Request.QueryString["idTercero"]))
        {
            hfIdTercero.Value = Request.QueryString["idTercero"];
        }

        return isvalid;
    }

    protected void btnEliminarDocumentosClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            var idDocumento = Convert.ToInt32(gvDocumentos.DataKeys[rowIndex]["IdDocumento"]);
            int result = vConcursalesService.EliminarDocumentoProcesoConcursal(idDocumento);


            if(result > 0)
            {
                var midocumento = documentosAdjuntos.FirstOrDefault(e1 => e1.IdDocumento == idDocumento);
                List<ProcesoConcursalDocumento> misDocumentos = documentosAdjuntos;
                misDocumentos.Remove(midocumento);
                documentosAdjuntos = misDocumentos;
                CargarDocumentos();
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDocumentos()
    {
        gvDocumentos.EmptyDataText = EmptyDataText();
        gvDocumentos.PageSize = PageSize();
        gvDocumentos.DataSource = documentosAdjuntos;
        gvDocumentos.DataBind();
    }

    protected void CargarDocumentoClick(object sender, ImageClickEventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();

            DateTime fechaDoc;

            if(DateTime.TryParse(txtFechaPublicacionP.Text, out fechaDoc) && FileUploadArchivo.HasFile)
            {
                ProcesoConcursalDocumento documento = new ProcesoConcursalDocumento();
                documento.IdMedioPublicacion = int.Parse(ddlMedioPublicacion.SelectedValue);
                documento.MedioPublicacion = ddlMedioPublicacion.SelectedItem.Text;
                documento.IdTipoDocumento = int.Parse(ddlTipoDocumento.SelectedValue);
                documento.TipoDocumento = ddlTipoDocumento.SelectedItem.Text;
                documento.IdTipoPublicacion = int.Parse(ddlTipoPublicacion.SelectedValue);
                documento.TipoPublicacion = ddlTipoPublicacion.SelectedItem.Text;
                documento.FechaPublicacion = fechaDoc;
                documento.NombreDocumento = Guid.NewGuid().ToString().Replace("-", string.Empty);
                documento.UsuarioCrea = GetSessionUser().NombreUsuario;
                documento.NombreOriginal = FileUploadArchivo.PostedFile.FileName;

                ManejoControlesConcursales mControles = new ManejoControlesConcursales();

                int idDocumento = mControles.CargarArchivoFTPConcursales(FileUploadArchivo, documento);

                if(idDocumento > 0)
                {
                    documento.IdDocumento = idDocumento;
                    List<ProcesoConcursalDocumento> documentos = documentosAdjuntos;
                    documentos.Add(documento);
                    documentosAdjuntos = documentos;
                    CargarDocumentos();
                }
                else
                    toolBar.MostrarMensajeError("Sucedio un error adjuntando el documento, por favor intente nuevamente.");
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlTipoPublicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int idTipoPublicacion = int.Parse(ddlTipoPublicacion.SelectedValue.ToString());
            ddlMedioPublicacion.DataSource = vConcursalesService.ConsultarParametricaPorPadre(idTipoPublicacion);
            ddlMedioPublicacion.DataBind();
            ddlMedioPublicacion.Items.Insert(0, new ListItem() { Text = "-- Seleccione --", Value = "-1" });
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}

