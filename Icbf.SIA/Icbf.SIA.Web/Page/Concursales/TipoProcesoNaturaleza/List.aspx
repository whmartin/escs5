﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Concursales_TipoProcesoNaturaleza_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

    <asp:HiddenField ID="HfIdClasificador" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" style="width: 50%">
                    Naturaleza del Proceso</td>   
                <td style="width: 50%">
                    Tipos de Proceso
                </td>                             
            </tr>
            <tr class="rowA">                
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtNaturaleza" Height="25px" MaxLength="150"
                        Width="320px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNaturaleza" runat="server" TargetControlID="txtNaturaleza"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ /.,@_():;" />
                </td> 
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtTipoProceso" Height="25px" MaxLength="150"
                        Width="320px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftTipoProceso" runat="server" TargetControlID="txtTipoProceso"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ /.,@_():;" />
                </td>               
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado
                </td>
                <td colspan="2"
                    
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td colspan="2">
                    
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvNaturalezaTipoProceso" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoProcesoNaturaleza" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvNaturalezaTipoProceso_PageIndexChanging" OnSelectedIndexChanged="gvNaturalezaTipoProceso_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvNaturalezaTipoProceso_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="Naturaleza de la Obligaci&#243;n" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>                           
                            <asp:TemplateField HeaderText="Tipo de Proceso" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre2">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre2")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblInactivo" runat="server" Text='<%# (bool) Eval("Estado") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>  
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
