﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Service;

/// <summary>
/// Página que despliega la consulta basada en filtros de tipos de cláusula
/// </summary>
public partial class Page_Concursales_TipoProcesoNaturaleza_List : GeneralWeb
{
    
   
    masterPrincipal toolBar;
    private ConcursalesService service = new ConcursalesService();
    string PageName = "Concursales/TipoProcesoNaturaleza";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNaturaleza = null;
            String vTipoProceso = null;
            Boolean? vEstado = null;
            if (rblEstado.SelectedValue == "-1")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "1" ? true : false;
            }
            if (txtNaturaleza.Text != "")
            {
                vNaturaleza = Convert.ToString(txtNaturaleza.Text);
            }
            if (txtTipoProceso.Text != "")
            {
                vTipoProceso = Convert.ToString(txtTipoProceso.Text);
            }
            gvNaturalezaTipoProceso.DataSource = service.ConsultarTiposdeProcesosporNaturaleza(vTipoProceso,vNaturaleza, vEstado);
            gvNaturalezaTipoProceso.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.LipiarMensajeError();
            gvNaturalezaTipoProceso.PageSize = PageSize();
            gvNaturalezaTipoProceso.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Asociar Naturaleza del Proceso y Tipos de Procesos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvNaturalezaTipoProceso.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza", strValue);
            SetSessionParameter("Paremetrica.IdParametrica", HfIdClasificador.Value);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvNaturalezaTipoProceso_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvNaturalezaTipoProceso.SelectedRow);
    }
    protected void gvNaturalezaTipoProceso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvNaturalezaTipoProceso.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            HfIdClasificador.Value = GetSessionParameter("Paremetrica.IdParametrica").ToString();            
            RemoveSessionParameter("Paremetrica.IdParametrica");

            if (GetSessionParameter("TipoProcesoNaturaleza.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoProcesoNaturaleza.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/

            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(0, new ListItem("Inactivo", "0"));
            rblEstado.Items.Insert(0, new ListItem("Todos", "-1"));
            rblEstado.SelectedValue = "-1";
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvNaturalezaTipoProceso_Sorting(object sender, GridViewSortEventArgs e)
    {
       Ordenar(e);      
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNaturaleza = null;
        String vTipoProceso = null;
        Boolean? vEstado = null;
        if (rblEstado.SelectedValue == "-1")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "1" ? true : false;
        }
        if (txtNaturaleza.Text != "")
        {
            vNaturaleza = Convert.ToString(txtNaturaleza.Text);
        }
        if (txtTipoProceso.Text != "")
        {
            vTipoProceso = Convert.ToString(txtTipoProceso.Text);
        }
        var myGridResults = service.ConsultarTiposdeProcesosporNaturaleza(vTipoProceso, vNaturaleza, vEstado);
        if (myGridResults != null)
        {
            var param = Expression.Parameter(typeof(TipoProcesoNaturaleza), e.SortExpression);

            var prop = Expression.Property(param, e.SortExpression);

            var sortExpression = Expression.Lambda<Func<TipoProcesoNaturaleza, object>>(Expression.Convert(prop, typeof(object)), param);

            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvNaturalezaTipoProceso.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                gvNaturalezaTipoProceso.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvNaturalezaTipoProceso.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Concursales/Parametricas/List.aspx");
    }
}
