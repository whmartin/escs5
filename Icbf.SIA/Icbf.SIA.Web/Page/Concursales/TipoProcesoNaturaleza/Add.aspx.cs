﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de tipos de amparo
/// </summary>
public partial class Page_Concursales_TipoProcesoNaturaleza_Add : GeneralWeb
{
    masterPrincipal toolBar;
    private ConcursalesService vService = new ConcursalesService();
    private string PageName = "Concursales/TipoProcesoNaturaleza";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {

        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoProcesoNaturaleza vTipoProcesoNaturaleza = new TipoProcesoNaturaleza();


            if (!ddlNaturaleza.SelectedValue.Equals("-1"))
                vTipoProcesoNaturaleza.IdNaturaleza = Convert.ToInt32(ddlNaturaleza.SelectedValue);
            

            if (!ddlTipoProceso.SelectedValue.Equals("-1"))
                vTipoProcesoNaturaleza.IdTipoProceso = Convert.ToInt32(ddlTipoProceso.SelectedValue);
            
            
            vTipoProcesoNaturaleza.Estado = rblEstado.SelectedValue == "1" ? true : false;

            if (Request.QueryString["oP"] == "E")
            {
                vTipoProcesoNaturaleza.IdTipoProcesoNaturaleza = Convert.ToInt32(hfIdTipoProcesoNaturaleza.Value);
                vTipoProcesoNaturaleza.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoProcesoNaturaleza, this.PageName, vSolutionPage);
                vResultado = vService.ModificarTipoProcesoNaturaleza(vTipoProcesoNaturaleza);
            }
            else
            {
                vTipoProcesoNaturaleza.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoProcesoNaturaleza, this.PageName, vSolutionPage);
                vResultado = vService.InsertarTipoProcesoNaturaleza(vTipoProcesoNaturaleza);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza", vTipoProcesoNaturaleza.IdTipoProcesoNaturaleza);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("TipoProcesoNaturaleza.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("TipoProcesoNaturaleza.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }

            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Asociar Naturaleza del Proceso y Tipos de Procesos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTipoProcesoNaturaleza = 0;
           // int vIdTipoAmparo = 0;

            if (int.TryParse(GetSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza").ToString(), out vIdTipoProcesoNaturaleza))
            {
                RemoveSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza");
                hfIdTipoProcesoNaturaleza.Value = vIdTipoProcesoNaturaleza.ToString();
                TipoProcesoNaturaleza vTipoProcesoNaturaleza = new TipoProcesoNaturaleza();
                vTipoProcesoNaturaleza = vService.ConsultarTipoProcesoNaturaleza(vIdTipoProcesoNaturaleza);
                ddlNaturaleza.SelectedValue = vTipoProcesoNaturaleza.IdNaturaleza.ToString();
                ddlTipoProceso.SelectedValue = vTipoProcesoNaturaleza.IdTipoProceso.ToString();
                rblEstado.SelectedValue = vTipoProcesoNaturaleza.Estado == true ? "1" : "0";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoProcesoNaturaleza.UsuarioCrea, vTipoProcesoNaturaleza.FechaCrea, vTipoProcesoNaturaleza.UsuarioModifica, vTipoProcesoNaturaleza.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            /*Coloque aqui el codigo de llenar el combo Naturaleza.*/
            string vNaturaleza= "Naturaleza del Proceso";         
            ddlNaturaleza.DataSource = vService.ConsultarParameticasNaturalezaTipoProceso(true, vNaturaleza);
            ddlNaturaleza.DataTextField = "Nombre";
            ddlNaturaleza.DataValueField = "IdValor";
            ddlNaturaleza.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlNaturaleza);

            /*Coloque aqui el codigo de llenar el combo TipoProceso.*/
            string vTipoProceso = "Tipos de Proceso";       
            ddlTipoProceso.DataSource = vService.ConsultarParameticasNaturalezaTipoProceso(true, vTipoProceso);
            ddlTipoProceso.DataTextField = "Nombre";
            ddlTipoProceso.DataValueField = "IdValor";
            ddlTipoProceso.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlTipoProceso);


            HfIdClasificador.Value = GetSessionParameter("Paremetrica.IdParametrica").ToString();
            RemoveSessionParameter("Paremetrica.IdParametrica");

            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
