﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de tipo cláusula
/// </summary>
public partial class Page_Concursales_TipoProcesoNaturaleza_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    private ConcursalesService vService = new ConcursalesService();
    private string PageName = "Concursales/TipoProcesoNaturaleza";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("TipoClausula.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza", hfIdTipoProcesoNaturaleza.Value);
        SetSessionParameter("Paremetrica.IdParametrica", HfIdClasificador.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Paremetrica.IdParametrica", HfIdClasificador.Value);
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoProcesoNaturaleza = Convert.ToInt32(GetSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza"));
            RemoveSessionParameter("TipoProcesoNaturaleza.IdTipoProcesoNaturaleza");

            if (GetSessionParameter("TipoProcesoNaturaleza.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoProcesoNaturaleza.Guardado");

            if (GetSessionParameter("TipoProcesoNaturaleza.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
            RemoveSessionParameter("TipoProcesoNaturaleza.Modificado");

            hfIdTipoProcesoNaturaleza.Value = vIdTipoProcesoNaturaleza.ToString();
            TipoProcesoNaturaleza vTipoProcesoNaturaleza = new TipoProcesoNaturaleza();
            vTipoProcesoNaturaleza = vService.ConsultarTipoProcesoNaturaleza(vIdTipoProcesoNaturaleza);            
            txtNaturaleza.Text = vTipoProcesoNaturaleza.Nombre;
            txtTipoProceso.Text = vTipoProcesoNaturaleza.Nombre2;
            rblEstado.SelectedValue = vTipoProcesoNaturaleza.Estado == true ? "1" : "0";
            ObtenerAuditoria(PageName, hfIdTipoProcesoNaturaleza.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoProcesoNaturaleza.UsuarioCrea, vTipoProcesoNaturaleza.FechaCrea, vTipoProcesoNaturaleza.UsuarioModifica, vTipoProcesoNaturaleza.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoClausula = Convert.ToInt32(hfIdTipoProcesoNaturaleza.Value);

            TipoProcesoNaturaleza vTipoProcesoNaturaleza = new TipoProcesoNaturaleza();
            vTipoProcesoNaturaleza = vService.ConsultarTipoProcesoNaturaleza(Convert.ToInt32(hfIdTipoProcesoNaturaleza.Value));
            InformacionAudioria(vTipoProcesoNaturaleza, this.PageName, vSolutionPage);
            int vResultado = vService.EliminarTipoProcesoNaturaleza(vTipoProcesoNaturaleza);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoProcesoNaturaleza.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Asociar Naturaleza del Proceso y Tipos de Procesos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            HfIdClasificador.Value = GetSessionParameter("Paremetrica.IdParametrica").ToString();
            RemoveSessionParameter("Paremetrica.IdParametrica");
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
