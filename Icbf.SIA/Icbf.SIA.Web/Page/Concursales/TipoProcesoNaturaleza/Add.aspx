﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Concursales_TipoProcesoNaturaleza_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
 <asp:HiddenField ID="HfIdClasificador" runat="server" />
<asp:HiddenField ID="hfIdTipoProcesoNaturaleza" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" width="50%">
                Naturaleza del
                Proceso *
                <asp:RequiredFieldValidator runat="server" ID="rfvNaturaleza" ControlToValidate="ddlNaturaleza"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator runat="server" ID="cvNaturaleza" ControlToValidate="ddlNaturaleza"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td width="50%">
                Tipo de
                Proceso *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoProceso" ControlToValidate="ddlTipoProceso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvTipoProceso" ControlToValidate="ddlTipoProceso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlNaturaleza" Height="25px"></asp:DropDownList>
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlTipoProceso" Height="25px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>            
            
            <td>
                &nbsp;</td>            
            
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 339px;
        }
    </style>
</asp:Content>

