<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Concursales_Solicitudes_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    
             <script type="text/javascript" language="javascript">

                 function ShowHistorico(IdProceso) {
                 var url = '../../../Page/Concursales/Lupas/LupaHistoricoProceso.aspx?IdProceso=' + IdProceso;
                 window_showModalDialog(url, setReturnHistorico, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
             }

                 function setReturnHistorico() {

             }

             </script>

 <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Indentificaci&oacute;n</td>
            <td class="Cell">
                Usuario Asignado</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdentificacion" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdentificacion" FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:TextBox ID="txtUsuarioAsignado" runat="server" Height="16px" MaxLength="30" Width="245px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio</td>
            <td class="Cell">
                Consecutivo</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:TextBox ID="txtConsecutivo" runat="server" Height="16px" MaxLength="30" Width="245px" />
            </td>
        </tr> 

    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvNovedadesProcesoConcursal" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdProceso,CodigoEstado" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvNovedadesProcesoConcursal_PageIndexChanging" 
                        OnRowDataBound="gvNovedadesProcesoConcursal_RowDataBound"
                        OnSorting="gvNovedadesProcesoConcursal_Sorting" 
                       >
                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Hist&oacute;rico">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnHistorico"  runat="server" AutoPostBack="false"   Height="16px" Width="16px" >
                                        <img alt="Editar" src="../../../Image/btn/icoPagBuscar.gif"  height="16px" width="16px"  title="Hist&oacute;rico" ToolTip="Ver Hist&oacute;rico" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:BoundField HeaderText="Raz&oacute;n Social" DataField="RazonSocial" ItemStyle-Width="250px"  SortExpression="razonSocial"/>
                                <asp:BoundField HeaderText="Capital Inicial" DataField="CapitalInicial"  DataFormatString="{0:C}"  SortExpression="capitalInicial"/>
                                <asp:BoundField HeaderText="Intereses Inicial" DataField="InteresesInicial"  DataFormatString="{0:C}" SortExpression="interesesInicial"/>
                                <asp:BoundField HeaderText="Saldo Capital" DataField="SaldoCapital"  DataFormatString="{0:C}"  SortExpression="saldoCapital"/>
                                <asp:BoundField HeaderText="Saldo Intereses" DataField="SaldoInteres"  DataFormatString="{0:C}"  SortExpression="saldoIntereses"/>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstado"  SortExpression="NombreEstado"/>
                                <asp:TemplateField HeaderText ="Cambio de Regional" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                            <asp:LinkButton ID="btnInfoCambioRegional"  runat="server" OnClick="btnInfoNovedades" 
                                                     CommandName="Traslado" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                    <img alt="Cambio Regional" src="../../../Image/btn/edit.gif" title="Cambio Regional" />
                                                </asp:LinkButton>
                                    </ItemTemplate>
                            </asp:TemplateField>  
                             <asp:TemplateField HeaderText ="Anulaci&oacute;n de Proceso"  ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Right"  ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                                <asp:LinkButton ID="btnInfoTerminacion" runat="server" OnClick="btnInfoNovedades"  
                                                     CommandName="Anulacion" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                    <img alt="Anulaci&oacute;n" src="../../../Image/btn/edit.gif" title="Anulaci&oacute;n" />
                                                </asp:LinkButton>
                                    </ItemTemplate>
                            </asp:TemplateField>  
                             </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
