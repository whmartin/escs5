﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;
using Icbf.SIA.Entity.Concursales;
using System.Text;
using System.Web.Security;

/// <summary>
/// Página que despliega el detalle del registro del inicio de un proceso concursal.
/// </summary>
public partial class Page_Concursales_Lupas_LupaRepartoUsuario : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "Concursales/Reparto";

    SIAService vSIAService = new SIAService();

    ConcursalesService vConcursalesService = new ConcursalesService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if(!Page.IsPostBack)
            {
                CargarDatos();
            }
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }
    private void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            int id = int.Parse(hfIdentificacion.Value.ToString());
            string usuario = ddlUsuarioAsignar.SelectedValue.ToString();
            int result = vConcursalesService.AsignarUsuarioProceso(id, usuario, GetSessionUser().NombreUsuario);

            if(result > 0)
            {
                var mensaje = string.Format("Se realizo la asignaci&oacute;n del Proceso con Id {0} al usuario {1}", id, usuario);
                SetSessionParameter("Concursales.AsignoUusario", mensaje);
                GetScriptCloseDialogCallback(string.Empty);
            }
            else
                toolBar.MostrarMensajeError("Se produjo un error realizando la asignaci&oacute;n del usuario.");
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Detalle Proceso Concursal", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if(VerificarQueryStrings())
            {
                int id = int.Parse(hfIdentificacion.Value.ToString());
                var item = vConcursalesService.ObtenerProcesosConcursalPorId(id);

                txtRegional.Text = item.Regional.ToUpper();
                txtRazonSocial.Text = item.RazonSocial.ToUpper();
                txtCapitalInicial.Text = string.Format("{0:$#,##0}", item.CapitalInicial);
                txtInteresesInicial.Text = string.Format("{0:$#,##0}", item.InteresesInicial);
                txtSaldoCapital.Text = string.Format("{0:$#,##0}", item.SaldoCapital);
                txtSaldoIntereses.Text = string.Format("{0:$#,##0}", item.SaldoInteres);
                hfIdRegional.Value = item.CodigoRegional;
                CargarUsuariosRol();
                var items = vConcursalesService.ObtenerDocumentosPorIdProceso(id);
                gvDocumentos.DataSource = items;
                gvDocumentos.DataBind();

                if(!string.IsNullOrEmpty(item.UsuarioAsignado))
                    ddlUsuarioAsignar.SelectedValue = item.UsuarioAsignado;
            }
            else
                GetScriptCloseDialogCallback(string.Empty);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool VerificarQueryStrings()
    {
        bool isvalid = false;

        if(!string.IsNullOrEmpty(Request.QueryString["IdProceso"]))
        {
          hfIdentificacion.Value = Request.QueryString["IdProceso"];
            isvalid = true;
        }

        return isvalid;
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarUsuariosRol()
    {

        var consumeServicio = vConcursalesService.ConsultarParametricaPorCodigo("SPARAM", "RolAbogadoReparto");

        var usuarios = Roles.GetUsersInRole(consumeServicio.Auxiliar1);
        List<string> usuariosList = new List<string>();

        if(usuarios != null)
        {
            foreach(var item in usuarios)
            {
                var itemUsuarioSIA = vSIAService.ConsultarUsuario(item);

                if(itemUsuarioSIA != null && !string.IsNullOrEmpty(itemUsuarioSIA.Providerkey))
                {
                    var regional = vSIAService.ConsultarRegionals(hfIdRegional.Value, null);

                    int idRegional = regional.FirstOrDefault().IdRegional;

                    if((itemUsuarioSIA.IdRegional.HasValue && itemUsuarioSIA.IdRegional.Value == idRegional) || itemUsuarioSIA.IdTipoUsuario == 1)
                        usuariosList.Add(item.ToUpper());
                }
            }
        }

        ddlUsuarioAsignar.DataSource = usuariosList;
        ddlUsuarioAsignar.DataBind();
        ddlUsuarioAsignar.Items.Insert(0,new ListItem() { Text = "-- Seleccione --", Value="0", Selected=true });
    }
}

