<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaGestionarNovedad.aspx.cs" Inherits="Page_Concursales_Lupas_LupaGestionarNovedad" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <table width="90%" id="pnInfoTraslado" style="display:none" runat="server" align="center" >
        <tr class="rowB">
            <td colspan="2">
                Usuario    
                <asp:CompareValidator runat="server" ID="cvModalidadSeleccion" ControlToValidate="ddlUsuarioAsignar"
                SetFocusOnError="true" ErrorMessage="*" ValidationGroup="btnGuardar"
                ForeColor="Red" Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>  
            </td>
        </tr>
        <tr class="rowA">
                <td class="Cell" colspan="2">
                 <asp:DropDownList ID="ddlUsuarioAsignar" runat="server" ValidationGroup="btnGuardar" />
                </td>
            </tr>
      </table>

    <table width="90%" runat="server" id="PnlObservacionesDocumentos" align="center">
                       <tr class="rowB" >
                            <td class="Cell" colspan="2">
                                Observaciones
                                <asp:RequiredFieldValidator runat="server" ID="rfObservaciones" ControlToValidate="txtObservacionesEstado"
                                SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar" 
                                ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>     
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="2">
                                <asp:TextBox ID="txtObservacionesEstado" runat="server" TextMode="MultiLine" MaxLength="500" Height="160px" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
    </table>

    <asp:HiddenField ID="hfidProceso" runat="server" />
    <asp:HiddenField ID="hfaccion" runat="server" />
    <asp:HiddenField ID="hfEstado" runat="server" />
    

    <script type="text/javascript">



    </script>

</asp:Content>



