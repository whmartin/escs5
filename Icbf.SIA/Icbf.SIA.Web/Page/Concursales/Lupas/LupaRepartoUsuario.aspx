﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="LupaRepartoUsuario.aspx.cs" Inherits="Page_Concursales_Lupas_LupaRepartoUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        
    <asp:HiddenField ID="hfIdRegional" runat="server" />
        <script type="text/javascript" language="javascript">




        </script>

        <table width="90%" id="pnInfoSolicitud" runat="server" align="center" >
        <tr class="rowB">
            <td>
                Regional          
            </td>
            <td>
                                      
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtRegional" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
                Raz&oacute;n Social
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                    <asp:TextBox ID="txtRazonSocial" runat="server" Enabled="false" Width="90%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Capital Inicial
            </td>
            <td>
               Intereses Inicial                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtCapitalInicial" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtInteresesInicial" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td>
               Saldo Capital
            </td>
            <td>
               Saldo Intereses                                             
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                    <asp:TextBox ID="txtSaldoCapital" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtSaldoIntereses" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
                Documentos        
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px" DataKeyNames="IdDocumento"
                        >
                        <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                     <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreDocumento", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=ProcesosConcursales") %>'>Archivo</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>  
                                <asp:BoundField HeaderText="NombreOriginal" DataField="NombreOriginal" />
                                <asp:BoundField HeaderText="Tipo de Documento" DataField="TipoDocumento" />
                                <asp:BoundField HeaderText="Tipo de Publicac&oacute;n" DataField="TipoPublicacion"  />
                                <asp:BoundField HeaderText="Medio de Publicaci&oacute;n" DataField="MedioPublicacion"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
                                    <tr class="rowA">
                            <td class="Cell">
                            <asp:DropDownList ID="ddlUsuarioAsignar" runat="server" ValidationGroup="btnGuardar" />
                                       <asp:CompareValidator runat="server" ID="cvModalidadSeleccion" ControlToValidate="ddlUsuarioAsignar"
                                            SetFocusOnError="true" ErrorMessage="*" ValidationGroup="btnGuardar"
                                            ForeColor="Red" Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
                            </td>
                            <td class="Cell">
                            </td>
                        </tr>
        </table>

        <asp:HiddenField ID="hfIdentificacion" runat="server" />

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
    </style>
</asp:Content>

