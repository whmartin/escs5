<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaEnviarNovedad.aspx.cs" Inherits="Page_Concursales_Lupas_LupaEnviarNovedad" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <table width="90%" id="pnInfoTraslado" style="display:none" runat="server" align="center" >
        <tr class="rowB">
            <td colspan="2">
                Regional del Traslado    
                <asp:RequiredFieldValidator runat="server" ID="rfRegionalTraslado" Enabled="false" ControlToValidate="ddlIDRegional"
                SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"  InitialValue="-1"
                ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>     
            </td>
        </tr>
        <tr class="rowA">
                <td class="Cell" colspan="2">
                  <asp:DropDownList runat="server" ID="ddlIDRegional" ></asp:DropDownList>
                </td>
            </tr>
      </table>

    <table width="90%" runat="server" id="PnlObservacionesDocumentos" align="center">
                       <tr class="rowB" >
                            <td class="Cell" colspan="2">
                                Observaciones
                                <asp:RequiredFieldValidator runat="server" ID="rfObservaciones" ControlToValidate="txtObservacionesEstado"
                                SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar" 
                                ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>     
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="2">
                                <asp:TextBox ID="txtObservacionesEstado" runat="server" TextMode="MultiLine" MaxLength="1000" Height="160px" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
    </table>

    <asp:HiddenField ID="hfidProceso" runat="server" />
    <asp:HiddenField ID="hfaccion" runat="server" />
    

    <script type="text/javascript">



    </script>

</asp:Content>



