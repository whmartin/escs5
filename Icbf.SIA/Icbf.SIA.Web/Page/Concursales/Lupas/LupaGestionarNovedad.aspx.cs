using System;
using System.Web.UI;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;

using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Concursales_Lupas_LupaGestionarNovedad : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    SIAService vSIAService = new SIAService();

    ConcursalesService vConcursalesService = new ConcursalesService();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    private void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            ProcesoConcursalGestionNovedad item = new ProcesoConcursalGestionNovedad();
            item.Observaciones = ObtenerObservaciones(); 
            item.IdProceso = int.Parse(hfidProceso.Value);
            item.Accion = hfaccion.Value.ToString();
            item.UsuarioNovedad = GetSessionUser().NombreUsuario;
            item.Estado = hfEstado.Value.ToString();

            if(hfEstado.Value.ToString() == "TRASA")
                item.UsuarioAsignar = ddlUsuarioAsignar.SelectedValue.ToString();

            int result = vConcursalesService.IngresarGestionNovedad(item);

            if(result > 0)
            {
                string mensaje = string.Empty;

                if(hfaccion.Value.ToString() == "1")
                mensaje =  string.Format("Se acepto la solicitud de {0}, para el proceso con Id {1}", hfEstado.Value, hfidProceso.Value);
                else
                mensaje = string.Format("Se rechazo la solicitud de {0}, para el proceso con Id {1}", hfEstado.Value, hfidProceso.Value);

                SetSessionParameter("Concursales.CambioEstado", mensaje);
                GetScriptCloseDialogCallback(string.Empty);
            }
            else
                toolBar.MostrarMensajeError("No se pudo realizar el ingreso de la novedad, por favor intente nuevamente.");
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private string ObtenerObservaciones()
    {
        string result = string.Empty;

        switch(hfEstado.Value.ToString())
        {
            case "TRAS":
             if(hfaccion.Value.ToString() == "1")
                 result = string.Format("Se aprobo la solicitud del traslado en el origen - {0}", txtObservacionesEstado.Text);
             else
                result = string.Format("Se rechazo la solicitud del traslado en el origen - {0}", txtObservacionesEstado.Text);
            break;
            case "TRASA":
            if(hfaccion.Value.ToString() == "1")
                result = string.Format("Se aprobo la solicitud del traslado en el destino, y se asigno al usuario: {0} - {1}",ddlUsuarioAsignar.SelectedValue.ToString(),txtObservacionesEstado.Text);
            else
                result = string.Format("Se rechazo la solicitud del traslado en el destino - {0}", txtObservacionesEstado.Text);
            break;
            case "PANUL":
            if(hfaccion.Value.ToString() == "1")
                result = string.Format("Se aprobo la solicitud de anulac&oacute;n del proceso - {0}", txtObservacionesEstado.Text);
            else
                result = string.Format("Se rechazo la solicitud de anulac&oacute;n del proceso - {0}", txtObservacionesEstado.Text);
            break;
        }

        return result;
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.EstablecerTitulos("Novedades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if(
               !string.IsNullOrEmpty(Request.QueryString["accion"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idProceso"]) &&
               !string.IsNullOrEmpty(Request.QueryString["estado"])
               
               )
            {
                hfidProceso.Value = Request.QueryString["idProceso"];
                hfaccion.Value = Request.QueryString["accion"];
                hfEstado.Value = Request.QueryString["estado"];

                if(hfEstado.Value.ToString() == "TRASA" && hfaccion.Value.ToString() == "1")
                {
                    var idRegional = Request.QueryString["idRegionalTraslado"];
                    pnInfoTraslado.Style.Add("display", "");
                    CargarUsuariosRol(idRegional);
                }
            }
            else
                GetScriptCloseDialogCallback(string.Empty);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarUsuariosRol(string idRegionalTraslado)
    {

        var consumeServicio = vConcursalesService.ConsultarParametricaPorCodigo("SPARAM", "RolAbogadoReparto");

        var usuarios = Roles.GetUsersInRole(consumeServicio.Auxiliar1);
        List<string> usuariosList = new List<string>();

        if(usuarios != null)
        {
            foreach(var item in usuarios)
            {
                var itemUsuarioSIA = vSIAService.ConsultarUsuario(item);

                if(itemUsuarioSIA != null && !string.IsNullOrEmpty(itemUsuarioSIA.Providerkey))
                {
                    int idRegionalT = int.Parse(idRegionalTraslado);

                    if((itemUsuarioSIA.IdRegional.HasValue && itemUsuarioSIA.IdRegional.Value == idRegionalT) || itemUsuarioSIA.IdTipoUsuario == 1)
                        usuariosList.Add(item.ToUpper());
                }
            }
        }

        ddlUsuarioAsignar.DataSource = usuariosList;
        ddlUsuarioAsignar.DataBind();
        ddlUsuarioAsignar.Items.Insert(0, new System.Web.UI.WebControls.ListItem() { Text = "-- Seleccione --", Value = "0", Selected = true });
    }


    #endregion     
}
