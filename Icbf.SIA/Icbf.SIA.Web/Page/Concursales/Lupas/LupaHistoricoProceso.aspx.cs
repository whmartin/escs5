using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Concursales_Lupas_LupaHistoricoProceso : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    ConcursalesService vConcursalesService = new ConcursalesService();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.EstablecerTitulos("Historico de Proceso", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int idProceso = int.Parse(Request.QueryString["idProceso"]);
            var misiitems = vConcursalesService.ObtenerGestionProcesoPorIdProceso(idProceso);
            gvHistoricoSolicitud.EmptyDataText = EmptyDataText();
            gvHistoricoSolicitud.PageSize = PageSize();
            gvHistoricoSolicitud.DataSource = misiitems;
            gvHistoricoSolicitud.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion 

    protected void gvHistoricoSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHistoricoSolicitud.PageIndex = e.NewPageIndex;
        int idSolicitudContrato = int.Parse(Request.QueryString["idProceso"]);
        var misiitems = vConcursalesService.ObtenerGestionProcesoPorIdProceso(idSolicitudContrato);
        gvHistoricoSolicitud.DataSource = misiitems;
        gvHistoricoSolicitud.DataBind();
    }
}
