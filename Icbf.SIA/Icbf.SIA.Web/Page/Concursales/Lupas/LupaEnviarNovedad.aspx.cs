using System;
using System.Web.UI;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;

using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Concursales_Lupas_LupaEnviarNovedad : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    SIAService vSIAService = new SIAService();

    ConcursalesService vConcursalesService = new ConcursalesService();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    private void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            ProcesoConcursalNovedad item = new ProcesoConcursalNovedad();
            item.Observaciones = string.Format("{0} - {1}",hfaccion.Value.ToString(), txtObservacionesEstado.Text);
            item.IdProceso = int.Parse(hfidProceso.Value);
            item.Accion = hfaccion.Value.ToString();
            item.UsuarioNovedad = GetSessionUser().NombreUsuario;

            if(hfaccion.Value.ToString() == "Traslado")
                item.IdRegionalTraslado = int.Parse(ddlIDRegional.SelectedValue.ToString());

            int result = vConcursalesService.IngresarNovedad(item);

            if(result > 0)
            {
                string mensaje = string.Format("Se ingreso la novedad de {0}, para el proceso con Id {1}", hfaccion.Value, hfidProceso.Value);
                SetSessionParameter("Concursales.CambioEstado", mensaje);
                GetScriptCloseDialogCallback(string.Empty);
            }
            else
                toolBar.MostrarMensajeError("No se pudo realizar el ingreso de la novedad, por favor intente nuevamente.");
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.EstablecerTitulos("Novedades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if(!string.IsNullOrEmpty(Request.QueryString["accion"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idProceso"]))
            {
                hfidProceso.Value = Request.QueryString["idProceso"];
                hfaccion.Value = Request.QueryString["accion"];

                if(hfaccion.Value.ToString() == "Traslado")
                {
                    pnInfoTraslado.Style.Add("display", "");
                    rfRegionalTraslado.Enabled = true;
                    ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vSIAService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");

                    var usuario = vSIAService.ConsultarUsuario(GetSessionUser().IdUsuario);

                    if(usuario != null)
                        ddlIDRegional.Items.Remove(usuario.IdRegional.ToString());
                }
            }
            else
                GetScriptCloseDialogCallback(string.Empty);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    #endregion     
}
