﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Concursales_Parametricas_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdParametrica" runat="server" />
    <asp:HiddenField ID="hfIdValorParametrica" runat="server" />
    <asp:HiddenField ID="HfIdNombreParametrica" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="auto-style1">
                Nombre&nbsp;
                <asp:RequiredFieldValidator runat="server" ID="rvlCodigo" ControlToValidate="txtCodigo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
             <td>
                <asp:Label ID="lblDescripcion" runat="server" />               
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtCodigo" TextMode="MultiLine" Width="400px" MaxLength2="200" Enabled="false"
                     Rows="2" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" Width="400px" Height="42px" MaxLength="250" Visible="false" Enabled="false"
                     Rows="3" ></asp:TextBox>
                
            </td>           
        </tr>  

            </table>
    <table width="90%" id="PnReferencias" runat="server" style="display:none" align="center">
        <tr  class="rowB">
            <td class="auto-style1">
                <asp:Label ID="lblReferencia1" runat="server" />
                <asp:RequiredFieldValidator runat="server" ID="rvlReferencia1" ControlToValidate="txtReferencia1"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
             <td>
                <asp:Label ID="lblReferencia2" runat="server" />
                <asp:RequiredFieldValidator runat="server" ID="rvlReferencia2" ControlToValidate="txtReferencia2"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
             </td>            
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtReferencia1" TextMode="MultiLine" Width="400px" MaxLength2="200" Visible="false"
                     Rows="2" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtReferencia2" TextMode="MultiLine" Width="400px" Height="42px" MaxLength="250"  Visible="false"
                     Rows="3" Enabled="false"></asp:TextBox>
                
            </td>           
        </tr>               
        
    </table>
    <table width="90%" align="center">
        <tr class="rowB">
             <td class="auto-style1">
                
                 Estado&nbsp;
                <asp:RequiredFieldValidator runat="server" ID="rfvInactivo" ControlToValidate="rblInactivo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>            
                
            </td>
        </tr>
        <tr class="rowA">
            
            <td class="auto-style1">
                <asp:RadioButtonList runat="server" ID="rblInactivo" Enabled="false" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>      
        
    </table>

     <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }        
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
    .auto-style1 {
        width: 500px;
    }
</style>
</asp:Content>




