﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Service;


/// <summary>
/// Página de visualización detallada para la entidad NumeroProcesos
/// </summary>
public partial class Page_Concursales_Parametricas_Edit : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Concursales/Parametricas";
    ConcursalesService vService = new ConcursalesService();    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Paremetrica.IdParametrica", hfIdParametrica.Value);        
        SetSessionParameter("Paremetrica.Nombre", HfIdNombreParametrica.Value);
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Paremetrica.IdParametrica", hfIdParametrica.Value);
        SetSessionParameter("ValorParametrica.IdValorParametrica", hfIdValorParametrica.Value);
        SetSessionParameter("Paremetrica.Nombre", HfIdNombreParametrica.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Paremetrica.IdParametrica", hfIdParametrica.Value);
        NavigateTo("ListDetalle.aspx");
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdValorParametrica = 0;

            if (int.TryParse(GetSessionParameter("ValorParametrica.IdValorParametrica").ToString(), out vIdValorParametrica))
            {
                RemoveSessionParameter("ValorParametrica.IdValorParametrica");

                if (GetSessionParameter("ValorParametrica.Guardado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado();
                RemoveSessionParameter("ValorParametrica.Guardado");

                 if (GetSessionParameter("ValorParametrica.Modificado").ToString() == "1")
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                RemoveSessionParameter("ValorParametrica.Modificado");

                Valor vValorParametrica = new Valor();
                vValorParametrica = vService.ConsultarValorParametrica(vIdValorParametrica);
                hfIdValorParametrica.Value = vValorParametrica.IdValor.ToString();
                txtCodigo.Text = vValorParametrica.Nombre;
                if (!string.IsNullOrEmpty(vValorParametrica.Descripcion))
                    txtDescripcion.Text = vValorParametrica.Descripcion;
                if (!string.IsNullOrEmpty(vValorParametrica.Auxiliar1))
                    txtReferencia1.Text = vValorParametrica.Auxiliar1;
                if (!string.IsNullOrEmpty(vValorParametrica.Auxiliar2))
                    txtReferencia2.Text = vValorParametrica.Auxiliar2;

                rblInactivo.SelectedValue = vValorParametrica.Estado.ToString().Trim().ToLower(); 

            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            //int vIdCodigoSECOP = Convert.ToInt32(hfIdCodigoSECOP.Value);

            //CodigosSECOP vCodigoSECOP = new CodigosSECOP();
            //vCodigoSECOP = vContratoService.ConsultarCodigoSECOP(vIdCodigoSECOP);

            ////if (vContratoService.ConsultarNumeroProcesosPorContrato(vNumeroProcesos.IdNumeroProceso))
            ////{
            ////    toolBar.MostrarMensajeError("El registro tiene elementos  que dependen de él, verifique por favor.");
            ////    return;
            ////}

            //InformacionAudioria(vCodigoSECOP, this.PageName, vSolutionPage);
            //int vResultado = vContratoService.EliminarCodigoSECOP(vCodigoSECOP);
            //if (vResultado == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            //    SetSessionParameter("CodigoSECOP.Eliminado", "1");
            //    NavigateTo(SolutionPage.List);
            //}
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            HfIdNombreParametrica.Value = GetSessionParameter("Paremetrica.Nombre").ToString();
            RemoveSessionParameter("Paremetrica.Nombre");

            toolBar.EstablecerTitulos(HfIdNombreParametrica.Value, SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdParametrica = 0;

            if (int.TryParse(GetSessionParameter("Paremetrica.IdParametrica").ToString(), out vIdParametrica))
            {
                RemoveSessionParameter("Paremetrica.IdParametrica");

                Clasificador vClasificador = new Clasificador();
                vClasificador = vService.ConsultarClasificador(vIdParametrica);
                hfIdParametrica.Value = vClasificador.IdClasificador.ToString();
                if (vClasificador.RequiereObservaciones == true)
                {
                    txtDescripcion.Visible = true;
                    lblDescripcion.Visible = true;
                    lblDescripcion.Text = "Descripción";
                }

                if (vClasificador.RequiereAuxiliar1 == true || vClasificador.RequiereAuxiliar2 == true)
                {
                    PnReferencias.Style.Add("display", "");
                    lblReferencia1.Text = vClasificador.LabelAuxiliar1;
                    txtReferencia1.Visible = true;

                    if (vClasificador.RequiereAuxiliar2 == true)
                    {
                        lblReferencia2.Text = vClasificador.LabelAuxiliar2;
                        txtReferencia2.Visible = true;
                    }

                }
                else
                    PnReferencias.Style.Add("display", "none");

            }
            else
            {
                NavigateTo(SolutionPage.List);
            }

            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.SelectedValue = "true";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }



}
