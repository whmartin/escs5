<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListDetalle.aspx.cs" Inherits="Page_Concursales_Parametricas_ListDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="HfIdValorClasificador" runat="server" />
    <asp:HiddenField ID="HfNombreParametrica" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                   <asp:Label runat="server" ID="lbNombre"></asp:Label>
                </td>
                <td class="Cell">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNombre" Height="25px" MaxLength="150"
                        Width="320px"></asp:TextBox>
                      <%--<Ajax:FilteredTextBoxExtender ID="ftNombre" TargetControlID="txtNombre" runat="server"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="������������ /.,@_():;" />--%>
                </td>
                <td class="Cell">
                    <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>            
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvParametricas" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdValor" CellPadding="0"
                        Height="16px" OnSorting="gvParametricas_Sorting" AllowSorting="True" OnPageIndexChanging="gvParametricas_PageIndexChanging"
                        OnSelectedIndexChanged="gvParametricas_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>                                                      
                            <asp:BoundField HeaderText="Nombre" DataField="Nombre" SortExpression="Nombre"/>
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblInactivo" runat="server" Text='<%# (bool) Eval("Estado") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
           
        </table>
    </asp:Panel>
</asp:Content>
