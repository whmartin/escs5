using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Service;
using System.Linq.Expressions;

public partial class Page_Concursales_Parametricas_List : GeneralWeb
{
    masterPrincipal toolBar;

    private ConcursalesService service = new ConcursalesService();

    string PageName = "Concursales/Parametricas";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvParametricas, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            gvParametricas.PageSize = PageSize();
            gvParametricas.EmptyDataText = EmptyDataText();
            toolBar.EstablecerTitulos("Gesti&oacute;n de Parametricas Concursales", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;

            bool esCustom = bool.Parse(gvParametricas.DataKeys[rowIndex].Values[1].ToString());
            string idClasificador = gvParametricas.DataKeys[rowIndex].Values[0].ToString();

            if (esCustom)
            {
                string parametrica = gvParametricas.DataKeys[rowIndex].Values[2].ToString();
                SetSessionParameter("Paremetrica.IdParametrica", idClasificador);
                string url = string.Format("~/Page/Concursales/{0}/List.aspx",parametrica);
                NavigateTo(url);
            }
            else
            {
                SetSessionParameter("Paremetrica.IdParametrica", idClasificador);
                NavigateTo("ListDetalle.aspx");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvParametricas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvParametricas.SelectedRow);
    }

    protected void gvParametricas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvParametricas.PageIndex = e.NewPageIndex;
        Buscar();
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.Items.Insert(0, new ListItem("Todos", "-1"));
            rblInactivo.SelectedValue = "-1";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gvParametricas_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el c�digo de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {

            Boolean? vEstado = null;
            String vNombre = null;

            vNombre = txtNombre.Text;

            if (rblInactivo.SelectedValue != "-1")
            {
                vEstado = Convert.ToBoolean(rblInactivo.SelectedValue);
            }

            //service
            var myGridResults = service.ConsultarParameticasConcursales(vEstado, vNombre);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del c�digo de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Clasificador), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<Clasificador, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
