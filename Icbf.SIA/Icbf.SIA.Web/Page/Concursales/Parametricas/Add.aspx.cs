﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity.Concursales;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición para la entidad NumeroProcesos
/// </summary>
public partial class Page_Concursales_Parametricas_Add : GeneralWeb
{
    private masterPrincipal toolBar;
    private ConcursalesService vService = new ConcursalesService();
    private string PageName = "Concursales/Parametricas";
  

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Paremetrica.IdParametrica", hfIdParametrica.Value);
        SetSessionParameter("Paremetrica.Nombre", HfIdNombreParametrica.Value);
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Paremetrica.IdParametrica", hfIdParametrica.Value);
        NavigateTo("ListDetalle.aspx");
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad NumeroProcesos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Valor vValorParametrica = new Valor();


            vValorParametrica.Nombre = txtNombre.Text.Trim();
            vValorParametrica.Estado = Convert.ToBoolean(rblInactivo.SelectedValue);
            vValorParametrica.IdClasificador = Convert.ToInt32(hfIdParametrica.Value);

            if(!string.IsNullOrEmpty(txtDescripcion.Text))
               vValorParametrica.Descripcion = txtDescripcion.Text.Trim();

            if (!string.IsNullOrEmpty(txtReferencia1.Text))
                vValorParametrica.Auxiliar1 = txtReferencia1.Text;

            if (!string.IsNullOrEmpty(txtReferencia2.Text))
                vValorParametrica.Auxiliar1 = txtReferencia2.Text;


            if (Request.QueryString["oP"] == "E")
            {

                vValorParametrica.IdValor = Convert.ToInt32(hfIdValorParametrica.Value);
                vValorParametrica.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vValorParametrica, this.PageName, vSolutionPage);
                vResultado = vService.ModificarValorParametrica(vValorParametrica);
            }
            else
            {
                vValorParametrica.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vValorParametrica, this.PageName, vSolutionPage);
                vResultado = vService.InsertarValorParametrica(vValorParametrica);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Paremetrica.IdParametrica", hfIdParametrica.Value);
                SetSessionParameter("ValorParametrica.IdValorParametrica", vValorParametrica.IdValor);
                SetSessionParameter("Paremetrica.Nombre", HfIdNombreParametrica.Value);
                                

                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("ValorParametrica.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("ValorParametrica.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }



                
            }
            else
            {
                toolBar.MostrarMensajeError(
                    "La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            HfIdNombreParametrica.Value = GetSessionParameter("Paremetrica.Nombre").ToString();
            RemoveSessionParameter("Paremetrica.Nombre");

            toolBar.EstablecerTitulos(HfIdNombreParametrica.Value, SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {

            int vIdValorParametrica = 0;

            if (int.TryParse(GetSessionParameter("ValorParametrica.IdValorParametrica").ToString(), out vIdValorParametrica))
            {
                RemoveSessionParameter("ValorParametrica.IdValorParametrica");

                Valor vValorParametrica = new Valor();
                vValorParametrica = vService.ConsultarValorParametrica(vIdValorParametrica);
                hfIdValorParametrica.Value = vValorParametrica.IdValor.ToString();
                txtNombre.Text = vValorParametrica.Nombre;
                if(!string.IsNullOrEmpty(vValorParametrica.Descripcion))
                    txtDescripcion.Text = vValorParametrica.Descripcion;
                if (!string.IsNullOrEmpty(vValorParametrica.Auxiliar1))
                    txtReferencia1.Text = vValorParametrica.Auxiliar1;
                if (!string.IsNullOrEmpty(vValorParametrica.Auxiliar2))
                    txtReferencia2.Text = vValorParametrica.Auxiliar2;

                rblInactivo.SelectedValue = vValorParametrica.Estado.ToString().Trim().ToLower(); 

            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdParametrica = 0;

            if (int.TryParse(GetSessionParameter("Paremetrica.IdParametrica").ToString(), out vIdParametrica))
            {
                RemoveSessionParameter("Paremetrica.IdParametrica");                

                Clasificador vClasificador = new Clasificador();
                vClasificador = vService.ConsultarClasificador(vIdParametrica);
                hfIdParametrica.Value = vClasificador.IdClasificador.ToString();
                if (vClasificador.RequiereObservaciones == true)
                {
                    txtDescripcion.Visible = true;
                    lblDescripcion.Visible = true;
                    rvlDescripcion.Visible = true;
                    lblDescripcion.Text = "Descripción *";
                }
                else{
                    txtDescripcion.Visible = false;
                    lblDescripcion.Visible = false;
                    rvlDescripcion.Visible = false;
                }

                if (vClasificador.RequiereAuxiliar1 == true || vClasificador.RequiereAuxiliar2 == true)
                {
                    PnReferencias.Style.Add("display","");
                    lblReferencia1.Text = vClasificador.LabelAuxiliar1;
                    txtReferencia1.Visible = true;

                    if(vClasificador.RequiereAuxiliar2 == true)
                    {
                        lblReferencia2.Text = vClasificador.LabelAuxiliar2;
                        txtReferencia2.Visible = true;
                    }

                }
                else
                    PnReferencias.Style.Add("display", "none");

            }
            else
            {
                NavigateTo(SolutionPage.List);
            }

            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.SelectedValue = "true";


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
