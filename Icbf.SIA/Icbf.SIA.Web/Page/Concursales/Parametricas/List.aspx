<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Concursales_Parametricas_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">    
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Nombre</td>
            <td class="Cell">
                Estado</td>
        </tr>
        <tr class="rowA">
            <td> 
                <asp:TextBox ID="txtNombre" runat="server" Height="25px" MaxLength="150"
                        Width="320px"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="ftCodigo" runat="server" TargetControlID="txtNombre" FilterMode="InvalidChars"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="<>" />--%>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal">
                </asp:RadioButtonList>   
            </td>
        </tr>
       
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvParametricas" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdClasificador,EsCustom,ParametricaCustom" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvParametricas_PageIndexChanging" OnSelectedIndexChanged="gvParametricas_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvParametricas_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:BoundField HeaderText="Nombre" DataField="Nombre"  SortExpression="Nombre" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
