<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Acuerdos_Detail"
    ValidateRequest="false" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <script type="text/javascript" src="http://code.jquery.com/jquery.min.js" charset="utf-8"></script>
    <script src="../../../Scripts/jquery-te-1.4.0.min.js" type="text/javascript"></script>
    <link href="../../../Styles/jquery-te-1.4.0.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdAcuerdo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre Acuerdo *
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <%--<asp:TextBox runat="server" ID="txtContenidoHtml"  Enabled="false"></asp:TextBox>--%>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                Contenido HTML *
                <asp:RequiredFieldValidator runat="server" ID="rfvContenidoHtml" ControlToValidate="txtContenidoHtml"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <div align="center">
                    <asp:TextBox runat="server" ID="txtContenidoHtml" class="jqte" ClientIDMode="Static" />
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $('.jqte').jqte();
    </script>
</asp:Content>
