<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_DocExperienciaEntidad_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdDocAdjunto" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Experiencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdExpEntidad" ControlToValidate="txtIdExpEntidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Id Tipo Grupo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdExpEntidad"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdExpEntidad" runat="server" TargetControlID="txtIdExpEntidad"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoDocGrupo"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdTipoDocGrupo" runat="server" TargetControlID="txtIdTipoDocGrupo"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Descripción *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Link *
                <asp:RequiredFieldValidator runat="server" ID="rfvLinkDocumento" ControlToValidate="txtLinkDocumento"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtLinkDocumento"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftLinkDocumento" runat="server" TargetControlID="txtLinkDocumento"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
    </table>
</asp:Content>
