<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_DocExperienciaEntidad_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdDocAdjunto" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Experiencia *
            </td>
            <td>
                Id Tipo Grupo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdExpEntidad"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoDocGrupo"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Descripción *
            </td>
            <td>
                Link *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtLinkDocumento"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
