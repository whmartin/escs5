﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;

public delegate void ImageClickDelegate();

/// <summary>
/// Control de usuario que despliega el historial de revisiones del proveedor
/// </summary>
public partial class Page_Proveedor_UserControl_ucHistorialRevisiones : System.Web.UI.UserControl
{
    #region propiedades
    
    public event ImageClickDelegate eventoGuardarObservaciones;

    public string Titulo { get; set; }

    private int vNroRevision;

    private String vObservaciones;

    private bool? vConfirmaYAprueba;

    public int NroRevision
    {
        set 
        {   vNroRevision = value;             
        }
    }

    public String Observaciones
    {
        set
        {
            vObservaciones = value;
            
        }
    }

    public bool? ConfirmaYAprueba
    {
        set
        {
            vConfirmaYAprueba = value;
            
        }
    }

    public string MensajeGuardar { get; set; }
    
    public ValidarInfo vValidarInfo 
    {
        get 
        {
            if (ViewState["vValidarInfo"] == null)
            {
                return new ValidarInfo();
            }
            else
            {
                return (ValidarInfo)ViewState["vValidarInfo"];
            }
        }
        set 
        {
            ViewState["vValidarInfo"] = value;
        }
    }

    public List<ValidarInfo> DataSource
    {
        get {
            if (ViewState["_datasource"] == null)
            {
                return new List<ValidarInfo>();
            }
            else
            {
                return (List<ValidarInfo>)ViewState["_datasource"];
            }             
        }
        set 
        {
            ViewState["_datasource"] = value;
            Buscar();
        }
    }

    public bool ConfirmaVisible { get; set; }

    public string TituloConfirmaYAprueba { get;  set; }

    public string MensajeErrorConfirmaYAprueba { get; set; }
    
    public string MensajeErrorSinObservaciones { get; set; }
    #endregion

    protected void Page_PreInit()
    {
    
    }

    /// <summary>
    /// Manejador del evento cargar de la página
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblTitulo.Text = Titulo;
            lblConfirma.Text = TituloConfirmaYAprueba;
            if (!string.IsNullOrEmpty(MensajeErrorConfirmaYAprueba))
                rfvrblConfirma.ErrorMessage = MensajeErrorConfirmaYAprueba;
            if (!string.IsNullOrEmpty(MensajeErrorSinObservaciones))
                rfvtxtObservaciones.ErrorMessage = MensajeErrorSinObservaciones;
            btnGuardarObservaciones.OnClientClick = string.Format("javascript:return confirm('{0}');",MensajeGuardar);
            txtObservaciones.Text = vObservaciones;
            rblConfirma.Text = vConfirmaYAprueba.ToString();
            if (vConfirmaYAprueba == true)
                txtObservaciones.Enabled = false;
            txtNroRevision.Text = vNroRevision.ToString(); 
            Buscar();
            pnlConfirma.Visible = ConfirmaVisible;
            
        }
    }

    private void Buscar()
    {

        gvObservaciones.DataSource = DataSource;
        gvObservaciones.DataBind();

        if (DataSource.Count == 0)
        {
            pnlUCObservaciones.Visible = false;
        }
        else
        {
            pnlUCObservaciones.Visible = true;
        }
    }

    /// <summary>
    /// Método para manipular el cambio de índice de la paginación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvObservacionesPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvObservaciones.PageIndex = e.NewPageIndex;
        Buscar();
    }

    protected void rblConfirma_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(rblConfirma.SelectedValue))
        {
            lblObservaciones.Visible = false;
            txtObservaciones.Text = string.Empty;
            rfvtxtObservaciones.Enabled = false;
            //pnlConfirma.Enabled = false;
            txtObservaciones.Enabled = false;
            
        }
        else
        {
            lblObservaciones.Visible = true;
            rfvtxtObservaciones.Enabled = true;
            //pnlConfirma.Enabled = true;
            txtObservaciones.Enabled = true;
        }

    }
        
    protected void txtObservaciones_TextChanged(object sender, EventArgs e)
    {
    
    }

    /// <summary>
    /// Almacena información de controles en una fuente de datos
    /// </summary>
    protected void Guardar()
    {
        ValidarInfo _ValidarInfo = new ValidarInfo();
        _ValidarInfo.Observaciones = txtObservaciones.Text;
        _ValidarInfo.NroRevision = Convert.ToInt32(txtNroRevision.Text);
        _ValidarInfo.ConfirmaYAprueba = Convert.ToBoolean(rblConfirma.SelectedValue);

        vValidarInfo = _ValidarInfo;

        Page_Proveedor_UserControl_ucHistorialRevisiones tmp = (Page_Proveedor_UserControl_ucHistorialRevisiones)Session["ucHistorialRevisiones1"];
        tmp.vValidarInfo = _ValidarInfo;
        Session["ucHistorialRevisiones1"] = tmp;

        if (eventoGuardarObservaciones != null)
        {
            eventoGuardarObservaciones();
        }
        else
        {
            eventoGuardarObservaciones += ((Page_Proveedor_UserControl_ucHistorialRevisiones)Session["ucHistorialRevisiones1"]).eventoGuardarObservaciones;
            eventoGuardarObservaciones();
        }
    }

    /// <summary>
    /// Método IsValid para validad controles de validación
    /// </summary>
    /// <returns></returns>
    protected bool IsValid()
    {
        if (rblConfirma.SelectedIndex < 0)
        {
            rfvrblConfirma.IsValid = false;
            return false;
        }
        else
        {
            if (txtObservaciones.Text.Length == 0 && rblConfirma.SelectedValue == "False")
            {
                rfvtxtObservaciones.IsValid = false;
                return false;
            }
            else
            {
                if (rblConfirma.SelectedValue == "False")
                {
                    string vObservaciones = txtObservaciones.Text.Trim();
                    if (vObservaciones.Length >= 1)
                    {
                        string[] palabras = vObservaciones.Split(' ');
                        if (palabras.Length <= 1)
                        {
                            rfvtxtObservaciones2.ErrorMessage = "Registre una observación válida";
                            rfvtxtObservaciones2.IsValid = false;
                            return false;
                        }
                        else if (palabras.Length == 2)
                        {
                            if (palabras[0] == palabras[1])
                            {
                                rfvtxtObservaciones2.ErrorMessage = "Registre una observación válida";
                                rfvtxtObservaciones2.IsValid = false;
                                return false;
                            }
                            else
                            {
                                rfvtxtObservaciones2.IsValid = true;
                                return true;
                            }
                        }
                        else
                        {
                            rfvtxtObservaciones2.IsValid = true;
                            return true;
                        }
                    }
                    else
                    {
                        rfvtxtObservaciones2.ErrorMessage = "Registre una observación válida";
                        rfvtxtObservaciones2.IsValid = false;
                        return false;
                    }
                }
                else
                {
                    rfvtxtObservaciones2.IsValid = true;
                    return true;
                }
            }
        }
    }

    /// <summary>
    /// Manejador del evento click para el botón GuardarObservaciones
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardarObservaciones_Click(object sender, EventArgs e)
    {
        if (IsValid())
        {
            Guardar();
            CurrentPage.ClientScript.RegisterStartupScript(typeof(System.Web.UI.Page), "ScriptRedirect", "window.location.href='../ValidarProveedor/Revision.aspx'", true);            
        }
    }

    private static Page CurrentPage
    {
        get
        {
            try
            {
                return (Page)HttpContext.Current.Handler;
            }
            catch
            {
                return null;
            }
        }
    }


    
}