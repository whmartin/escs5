﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

using Icbf.SIA.Service;
using Icbf.SIA.Entity;

using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;

/// <summary>
/// Control de usuario para ingresar la información generald el proveedor
/// </summary>
public partial class Page_Proveedor_UserControl_ucDatosProveedor : System.Web.UI.UserControl
{

    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRuboService = new SIAService();

    string vIdEntidad;

    public string IdEntidad
    {
        get { return vIdEntidad; }
        set { vIdEntidad = value; }
    }


    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Carga datos iniciales
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (!string.IsNullOrEmpty(IdEntidad))
            {
                int vIdEntidad = int.Parse(IdEntidad);
                int vTercero = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad).IdTercero;
                Icbf.Proveedor.Entity.EntidadProvOferente entidadProveedor =
                    vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
                Tercero tercero = vProveedorService.ConsultarTercero(entidadProveedor.IdTercero);
                TipoPersona tipoPersona = vOferenteService.ConsultarTipoPersona(tercero.IdTipoPersona.Value);
                TipoDocumento tipoDocumento = vRuboService.ConsultarTipoDocumento(tercero.IdDListaTipoDocumento);
                txtTipoPersona.Text = tipoPersona.NombreTipoPersona;
                if (tipoPersona.NombreTipoPersona.Equals("NATURAL"))
                {
                    txtDIV.Visible = false;
                    lblDIV.Visible = false;
                }
                else
                {
                    if (tercero.DigitoVerificacion !=null)
                    {
                    txtDIV.Text = tercero.DigitoVerificacion.Value.ToString();
                    }
                    txtDIV.Visible = true;
                    lblDIV.Visible = true;                
                }
                txtTipoIdentificacion.Text = tipoDocumento.Codigo;
                txtNumeroIdentificacion.Text = tercero.NumeroIdentificacion;
                txtProveedor.Text = tercero.Nombre_Razonsocial.Trim();
                txtEmail.Text = tercero.Email;
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}