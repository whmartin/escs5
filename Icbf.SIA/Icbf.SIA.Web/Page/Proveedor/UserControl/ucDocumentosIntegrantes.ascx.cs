﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Net.Mail;
using System.Configuration;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using System.IO;
using System.Data;
using Icbf.Seguridad.Service;

public partial class Page_Proveedor_UserControl_ucDocumentosIntegrantes : GeneralUserControl
{
    ProveedorService vProveedorService = new ProveedorService();
    SIAService vRUBOService = new SIAService();
    OferenteService vOferenteService = new OferenteService();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Cargar info a grilla gvDocExperienciaEntidad
    /// </summary>
    /// <param name="pIdExpEntidad"></param>
    public void CargarInfo(int vIdIntegrante)
    {
        try
        {
            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ConsultarIntegrantes(vIdIntegrante);
            if (vIntegrantes.IdTipoPersona == 1)
            {
                pnlDocumentos.Visible = true;
                pnlDocumentosProv.Visible = false;
                BuscaDatosTercero(vIntegrantes.IdTipoIdentificacionPersonaNatural, vIntegrantes.NumeroIdentificacion);
            }
            else if (vIntegrantes.IdTipoPersona == 2)
            {
                pnlDocumentos.Visible = false;
                pnlDocumentosProv.Visible = true;
                BuscaDatosProveedor(Convert.ToString(vIntegrantes.IdTipoPersona), Convert.ToString(vIntegrantes.IdTipoIdentificacionPersonaNatural), vIntegrantes.NumeroIdentificacion);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BuscaDatosTercero(int vIdIdTipoIdentificacion, string vNumeroIdentificacion)
    {
        try
        {
            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTerceroTipoNumeroIdentificacion(vIdIdTipoIdentificacion, vNumeroIdentificacion);
            BuscarDocumentos(vTercero.IdTercero, Convert.ToString(vTercero.IdTipoPersona));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BuscaDatosProveedor(string vTipoPersona, string vIdTipoIdentificacion, string vNumeroIdentificacion)
    {
        try
        {

            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProv_SectorPrivado(vTipoPersona, vIdTipoIdentificacion, vNumeroIdentificacion, null, null, null);

            vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
            vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);
            vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);

            int vIdEntidad = vEntidadProvOferente.IdEntidad;
            string vIdTipoSector = Convert.ToString(vEntidadProvOferente.IdTipoSector);
            string vTipoEntOfProv = Convert.ToString(vEntidadProvOferente.TipoEntOfProv);
            SetSessionParameter("vIdEntidad", vIdEntidad);
            SetSessionParameter("vIdTipoSector", vIdTipoSector);
            SetSessionParameter("vTipoEntOfProv", vTipoEntOfProv);
            BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);

        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    // #region Documentos

    /// <summary>
    /// Busca Documentos de Tercero y llena grilla
    /// </summary>
    private void BuscarDocumentos(int pIdTercero, string pIdTipoPersona)
    {
        try
        {
            gvAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(pIdTercero, pIdTipoPersona, "");
            gvAdjuntos.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BuscarDocumentosDatosBasicos(int vIdEntidad, string vTipoProveedor, string vTipoSector, string vTipoEntidad)
    {
        try
        {

            gvDocDatosBasicoProv.DataSource = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, vTipoProveedor, vTipoSector, "", vTipoEntidad);
            gvDocDatosBasicoProv.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Comando de renglón para la grilla Adjuntos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewLogo":

                    MemoryStream ArchivoImagen = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (ArchivoImagen != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", ArchivoImagen.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        var strUrl = "../../../Page/Proveedor/Archivo/MostrarArchivo.aspx";
                        var guidKey = Guid.NewGuid().ToString();
                        var sbScript = "<script>window.open('" + strUrl + "', null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), guidKey, sbScript, false);
                    }
                    else
                    {
                        throw new Exception("No se logro obtener el archivo desde el servidor.");
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    /// <summary>
    /// Comandos de renglón para la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    //RUBOService vRUBOService = new RUBOService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        var strUrl = "../../../Page/Proveedor/Archivo/MostrarArchivo.aspx";
                        var guidKey = Guid.NewGuid().ToString();
                        var sbScript = "<script>window.open('" + strUrl + "', null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), guidKey, sbScript, false);
                    }
                    else
                    {
                        throw new Exception("No se logró obtener el archivo desde el servidor.");
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}



//protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
//{
//    gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;

//    int vIdEntidad = Convert.ToInt32(GetSessionParameter("vIdEntidad"));
//    string vIdTipoSector = GetSessionParameter("vIdTipoSector").ToString();
//    string vTipoEntOfProv = GetSessionParameter("vTipoEntOfProv").ToString();
//    string vTipoPersona = ddlIdTipoPersona.SelectedValue;

//    BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);

//}
//protected void gvDocDatosBasicoProv_SelectedIndexChanged(object sender, EventArgs e)
//{
//    SeleccionarRegistro(gvDocDatosBasicoProv.SelectedRow);
//}

///// <summary>
///// Buscar Documentos
///// </summary>
//private void BuscarDocumentosDatosBasicos(int vIdEntidad, string vTipoProveedor, string vTipoSector, string vTipoEntidad)
//{
//    try
//    {

//        gvDocDatosBasicoProv.DataSource = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, vTipoProveedor, vTipoSector, "", vTipoEntidad);
//        gvDocDatosBasicoProv.DataBind();

//    }
//    catch (UserInterfaceException ex)
//    {
//        toolBar.MostrarMensajeError(ex.Message);
//    }
//    catch (Exception ex)
//    {
//        toolBar.MostrarMensajeError(ex.Message);
//    }
//}

///// <summary>
///// Selecciona registro de la grilla
///// </summary>
///// <param name="pRow"></param>
//private void SeleccionarRegistro(GridViewRow pRow)
//{
//    try
//    {
//        int rowIndex = pRow.RowIndex;
//        string strValue = gvDocDatosBasicoProv.DataKeys[rowIndex].Value.ToString();
//        SetSessionParameter("DocDatosBasicoProv.IdDocAdjunto", strValue);
//        NavigateTo(SolutionPage.Detail);
//    }
//    catch (UserInterfaceException ex)
//    {
//        toolBar.MostrarMensajeError(ex.Message);
//    }
//    catch (Exception ex)
//    {
//        toolBar.MostrarMensajeError(ex.Message);
//    }
//}

//#endregion