﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTelefono.ascx.cs" Inherits="Page_Proveedor_UserControl_ucTelefono" %>
<table>
    <tr>
        <td>
            Indicativo
        </td>
        <td>
            <asp:TextBox runat="server" ID="TxtIndicativo" MaxLength="1" Width="30px"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftIndicativo" runat="server" TargetControlID="TxtIndicativo"
                FilterType="Numbers" ValidChars="/1234567890" />
        </td>
        <td>
            Teléfono
        </td>
        <td>
            <asp:TextBox runat="server" ID="TxtTelefono" MaxLength="7" Width="80px"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftTelefono" runat="server" TargetControlID="TxtTelefono"
                FilterType="Numbers" ValidChars="/1234567890" />
        </td>
        <td>
            Extensión
        </td>
        <td>
            <asp:TextBox runat="server" ID="TxtExtension" MaxLength="10" Width="80px"></asp:TextBox>
            <Ajax:FilteredTextBoxExtender ID="ftExtension" runat="server" TargetControlID="TxtExtension"
                FilterType="Numbers" ValidChars="/1234567890" />
        </td>
    </tr>
</table>
