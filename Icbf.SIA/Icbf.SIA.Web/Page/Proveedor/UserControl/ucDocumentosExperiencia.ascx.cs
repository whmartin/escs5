﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.SIA.Service;
using System.IO;
using Icbf.Utilities.Presentation;

//Control de usuario para desplegar los documentos adjuntos en el módulo de experiencia
public partial class Page_Proveedor_UserControl_ucDocumentosExperiencia : GeneralUserControl
{
    ProveedorService vProveedorService = new ProveedorService();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Cargar info a grilla gvDocExperienciaEntidad
    /// </summary>
    /// <param name="pIdExpEntidad"></param>
    public void CargarInfo(int pIdExpEntidad)
    {
        try
        {
            gvDocExperienciaEntidad.DataSource = vProveedorService.ConsultarDocExperienciaEntidads(pIdExpEntidad, null, null);
            gvDocExperienciaEntidad.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Comando de renglón para una grilla DocExperienciaEntidad
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocExperienciaEntidad_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewArchivo":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream ArchivoDoc = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (ArchivoDoc != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", ArchivoDoc.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        var strUrl = "../../../Page/Proveedor/Archivo/MostrarArchivo.aspx";
                        var guidKey = Guid.NewGuid().ToString();
                        var sbScript = "<script>window.open('" + strUrl + "', null, 'dialogWidth:1250px;dialogHeight:650px;resizable:yes;');</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), guidKey, sbScript, false);
                    }
                    else
                    {
                        throw new Exception("No se logro obtener el archivo desde el servidor.");
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}