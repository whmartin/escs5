﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucHistorialRevisiones.ascx.cs" Inherits="Page_Proveedor_UserControl_ucHistorialRevisiones" %>

<asp:Panel runat="server" ID="pnlUCObservaciones" >
  
     <table width="90%" align="center">     
        <tr>
        <td>
            <h3 class="lbBloque">
                <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label>
            </h3>
        </td>
        </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObservaciones" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px"   OnPageIndexChanging="gvObservacionesPageIndexChanging" >
                        <Columns>
                            <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                            <asp:TemplateField HeaderStyle-Width="120px" >
                                <HeaderTemplate>
                                    <asp:Label ID="lblTituloEstado" runat="server" Text="<%# TituloConfirmaYAprueba %>"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (Eval("ConfirmaYAprueba") == null) ? "N/A" : ((bool)Eval("ConfirmaYAprueba") ? "Si" : "No")) %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />--%>
                            <asp:TemplateField  HeaderText="Observaciones">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtObervacionesRev" runat="server" TextMode="MultiLine" Rows="8" 
                                    Text='<%#Eval("Observaciones")%>' Width="400px" Enabled="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Observaciones" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Observaciones")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField HeaderText="Fecha Observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                            <asp:BoundField HeaderText="Gestor Documental" DataField="UsuarioCrea" HeaderStyle-Width="180px" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>       
</asp:Panel>    
<asp:Panel runat="server" ID="pnlConfirma" >
 <table width="90%" align="center">
        <tr>
        <td>
            <h3 class="lbBloque">
                <asp:Label ID="lbRevisiones" runat="server" Text="Revisiones"></asp:Label>
            </h3>
        </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblNroRevision" runat="server" Text="No. Revisi&oacute;n" ></asp:Label>                 
            </td>
       </tr>
       <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNroRevision" Enabled="False"></asp:TextBox>                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblConfirma" runat="server" Text="" ></asp:Label> *
                <asp:RequiredFieldValidator runat="server" ID="rfvrblConfirma" ErrorMessage="Confirme si los datos coinciden con el documento adjunto y aprueba el documento"
                    ControlToValidate="rblConfirma" SetFocusOnError="true" ValidationGroup="btnGuardarObservaciones"
                    ForeColor="Red" Display="Dynamic" Enabled = "True" InitialValue="-1"></asp:RequiredFieldValidator>   
            </td>
       </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:RadioButtonList runat="server" ID="rblConfirma"  AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblConfirma_SelectedIndexChanged" Enabled="True" >
                    <asp:ListItem Text="Si" Value="True" ></asp:ListItem>
                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>            
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Observaciones <asp:Label ID="lblObservaciones" runat="server" Text="*" Visible = "false"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtObservaciones" ErrorMessage="Ingrese las observaciones de la validaci&oacute;n documental por el cual no fue aprobado"
                    ControlToValidate="txtObservaciones" SetFocusOnError="true" ValidationGroup="btnGuardarObservaciones"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator> 
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtObservaciones2" ErrorMessage=""
                    ControlToValidate="txtObservaciones" SetFocusOnError="true" ValidationGroup="btnGuardarObservaciones"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtObservaciones" MaxLength="100" Rows="4" Enabled="false" 
                    ontextchanged="txtObservaciones_TextChanged" CssClass="TextBoxGrande" style="width:80%; height:50%" onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);" TextMode="MultiLine"></asp:TextBox>                
            </td>
        </tr>
         <tr class="rowA">
             <td class="Cell" style="text-align: right">
                 <asp:ImageButton ID="btnGuardarObservaciones" runat="server" 
                     ImageUrl="~/Image/btn/save.gif" style="text-align: right" Width="30px" Height= "30px" 
                     ValidationGroup="btnGuardarObservaciones" OnClick = "btnGuardarObservaciones_Click" />
             </td>
         </tr>
       </table>
</asp:Panel>
<script type="text/javascript">
    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        }
        else {
            //limitCount.value = limitNum - limitField.value.length;
        }
    }
      </script>
<p style="text-align: right">
    &nbsp;</p>


