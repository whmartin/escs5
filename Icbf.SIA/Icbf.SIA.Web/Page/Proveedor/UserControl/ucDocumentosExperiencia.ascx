﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDocumentosExperiencia.ascx.cs"
    Inherits="Page_Proveedor_UserControl_ucDocumentosExperiencia" %>

<asp:Panel ID="pnlDocumentos" runat="server" ScrollBars="Auto">
    <asp:GridView runat="server" ID="gvDocExperienciaEntidad" AutoGenerateColumns="False"
        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdExpEntidad"
        EmptyDataText="No se encontraron documentos asociados" CellPadding="0" Height="16px"
        OnRowCommand="gvDocExperienciaEntidad_RowCommand">
        <Columns>
            <asp:BoundField HeaderText="Nombre Documento" DataField="DescripcionDocumento" />
            <%--<asp:BoundField HeaderText="Nombre Archivo" DataField="LinkDocumento" />--%>
            <asp:TemplateField HeaderText="Documento">
                <ItemTemplate>
                    <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorExperiencia_") + "ProveedorExperiencia_".Length) %>' ></asp:Label>               
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Visualizar Archivo">
                <ItemTemplate>
                    <asp:ImageButton ID="btnArchivo" runat="server" CommandName="viewArchivo" CommandArgument='<%#Eval("LinkDocumento")%>'
                        ImageUrl="~/Image/btn/icoPagBuscar.gif" Height="16px" Width="16px" ToolTip="Ver Archivo" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <AlternatingRowStyle CssClass="rowBG" />
        <EmptyDataRowStyle CssClass="headerForm" />
        <HeaderStyle CssClass="headerForm" />
        <RowStyle CssClass="rowAG" />
    </asp:GridView>
</asp:Panel>
