﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Control de usuario para la recepción de documentos adjuntos
/// </summary>
public partial class Page_Proveedor_UserControl_ucDocumentoAdjunto : System.Web.UI.UserControl
{
    #region Propiedades

    public bool HasFile
    {
        get
        {
            return fuArchivoDocumento.HasFile;
        }
    }

    public HttpPostedFile PostedFile
    {
        get
        {
            return fuArchivoDocumento.PostedFile;
        }
    }

    public string Descripcion
    {
        get 
        {
            return txtDescripcion.Text;
        }
    }

    #endregion

    /// <summary>
    /// Manejador del evento cargar página
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
}