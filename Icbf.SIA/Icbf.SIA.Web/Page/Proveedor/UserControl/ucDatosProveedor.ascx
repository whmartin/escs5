﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDatosProveedor.ascx.cs"
    Inherits="Page_Proveedor_UserControl_ucDatosProveedor" %>
<table width="90%" align="center">
    <tr class="rowB">
        <td>
            Tipo de persona o asociación
        </td>
        <td>
            Tipo de Identificaci&oacute;n
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox runat="server" ID="txtTipoPersona" Enabled="false"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr class="rowB">
        <td>
            N&uacute;mero de Identificaci&oacute;n
        </td>
        <td>
            <asp:Label runat="server" ID="lblDIV" Text="DV"></asp:Label>
        </td>
    </tr>
    <tr class="rowA">
        <td>
            <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Enabled="false"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtDIV" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr class="rowB">
        <td>
            Proveedor
        </td>
        <td>
            Correo Electr&oacute;nico
        </td>
    </tr>
    <tr class="rowA">
        <td class="Cell" width="50%;">
            <asp:TextBox runat="server" ID="txtProveedor" Enabled="false" Width="80%"></asp:TextBox>
        </td>
        <td class="Cell" width="50%;">
            <asp:TextBox runat="server" ID="txtEmail" Enabled="false" Width="80%"></asp:TextBox>
        </td>
    </tr>
    <tr class="rowB">
        <td colspan="2">
            <hr />
        </td>
    </tr>
</table>
