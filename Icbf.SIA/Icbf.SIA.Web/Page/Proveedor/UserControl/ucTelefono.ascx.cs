﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Controlde usuario utilizado para ingresar los datos asociados al teléfono
/// </summary>
public partial class Page_Proveedor_UserControl_ucTelefono : System.Web.UI.UserControl, IValidator
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region Propiedades

    public int ?Indicativo
    {
        get 
        {
            if (!String.IsNullOrEmpty(TxtIndicativo.Text))
            {
                return Int32.Parse(TxtIndicativo.Text);
            }
            else { return null; };
        }
        set { TxtIndicativo.Text = value.ToString(); }
    }

    public long ?Telefono
    {
        get 
        {
            if (!String.IsNullOrEmpty(TxtTelefono.Text))
            {
                return long.Parse(TxtTelefono.Text); 
            }
            else { return null; };
        }
        set { TxtTelefono.Text = value.ToString(); }
    }

    public long ?Extension 
    {
        get 
        {
            if (!String.IsNullOrEmpty(TxtExtension.Text))
            {
                return long.Parse(TxtExtension.Text); 
            }
            else { return null; };
        }
        set { TxtExtension.Text = value.ToString(); }
    }

    #endregion

    /// <summary>
    /// habilita controles
    /// </summary>
    /// <param name="pHabilitar"></param>
    public void Habilitar(bool pHabilitar) 
    {
        TxtIndicativo.Enabled = pHabilitar;
        TxtTelefono.Enabled = pHabilitar;
        TxtExtension.Enabled = pHabilitar;
    }


    #region IValidator Members

    public string ErrorMessage
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public bool IsValid
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void Validate()
    {
        throw new NotImplementedException();
    }

    #endregion
}