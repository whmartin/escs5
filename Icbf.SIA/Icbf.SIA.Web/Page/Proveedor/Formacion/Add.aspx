<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Formacion_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
    <asp:HiddenField ID="hfIdFormacion" runat="server" />
    <table width="90%" align="center">
         <tr class="rowb">
             <td class="auto-style1">Modalidad *
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlCodigoModalidad"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="ddlCodigoModalidad"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>Profesi&oacute;n *
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlProfesion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="ddlProfesion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            
        </tr>
       
        <tr class="rowA">
            <td class="auto-style1">
                <asp:DropDownList runat="server" ID="ddlCodigoModalidad" AutoPostBack="True" OnSelectedIndexChanged="ddlModalidadAcademicaSelectedIndexChanged"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlProfesion"></asp:DropDownList>
            </td>
           
        </tr>
        <tr class="rowB">
              <td class="auto-style1">Formaci&oacute;n Principal *
                <asp:RequiredFieldValidator runat="server" ID="rfvEsFormacionPrincipal" ControlToValidate="rblEsFormacionPrincipal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
             <td>Agregar Estudio
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:RadioButtonList ID="rblEsFormacionPrincipal" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="eventoRadioButtonFormacion">
                </asp:RadioButtonList>
            </td>
             <td style="width: 120px; text-align: left">
                <asp:LinkButton runat="server" ID="LinkButton1" OnClick="btnAgregarOtrosEstudios_Click" ValidationGroup="btnGuardar">
                        <img alt="Nuevo" src="../../../Image/btn/add.gif" title="Agregar" />
                </asp:LinkButton>
            </td>
        </tr>
        <tr class="rowB">
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvOtrosEstudios" runat="server" AutoGenerateColumns="false" CellPadding="0" Height="16px" Width="511px" HorizontalAlign="Center">
                    <Columns>
                        <asp:BoundField DataField="NombreModalidad" HeaderText="Modalidad" ItemStyle-Width="200px" />
                        <asp:BoundField DataField="nombreProfesion" HeaderText="Profesi&oacute;n" ItemStyle-Width="200px" />
                        <asp:BoundField DataField="esformacion" HeaderText="Tipo Estudio" ItemStyle-Width="200px" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
    </table>

    <%--<asp:Panel ID="pOtroEstudios" runat="server" Visible="false" Width="100%"  >
        <table style="width: 90%" align="center">
            <tr>
                <td colspan="3" >
                    <h3 class="lbBloque" style="margin-left:40px">
                        <asp:Label ID="lblTitulo" runat="server" Text="Otros Estudios"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowB">
                <td >Modalidad                 
                </td>
                <td>Profesi&oacute;n 
                </td>
                <td>Agregar Estudio
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlModalidad1" OnSelectedIndexChanged="ddlModalidadAcademicaOtrosEstudiosSelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlProfesion1"></asp:DropDownList>
                </td>
                <td style="width:120px;text-align:right">
                    <asp:LinkButton runat="server" ID="btnAgregarOtrosEstudios" OnClick="btnAgregarOtrosEstudios_Click" ValidationGroup="btnGuardar">
                        <img alt="Nuevo" src="../../../Image/btn/add.gif" title="Agregar" />
                    </asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="gvOtrosEstudios" runat="server" AutoGenerateColumns="false" CellPadding="0" Height="16px" Width="511px" HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="CodigoModalidad" HeaderText="Modalidad" ItemStyle-Width="200px" />
                            <asp:BoundField DataField="IdProfesion" HeaderText="Profesi&oacute;n" ItemStyle-Width="200px"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 232px;
        }
    </style>
</asp:Content>


