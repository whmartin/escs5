using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using System.Data;

/// <summary>
/// Página de registro y edición para la entidad Formacion
/// </summary>
public partial class Page_Formacion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ContratoService vContratoService = new ContratoService();
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
    string PageName = "Proveedor/Formacion";
    string modalidad = "";
    string profesion = "";

    #region inicio
    /// <summary>
    /// metodo inicial de la página
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Formacion", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// control de autoposback y control de navegacion cuando viene de edit
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }
    /// <summary>
    /// metodo contrl de cambio de ddlModalidad
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlModalidadAcademicaSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarFormacion(ddlCodigoModalidad.SelectedValue);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    /// <summary>
    /// Inicio metodo delegado boton buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAgregarOtrosEstudios_Click(object sender, EventArgs e)
    {
        if (!this.AgregarOtrosEstudios())
        {
            return;
        }

    }
    #endregion

    #region metodos
    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad Formacion
    /// </summary>
    private void Guardar()
    {
        try
        {                
            
            int vResultado = 0;
            Formacion vFormacion = new Formacion();
            DataTable totalEstudios = (DataTable)ViewState["vsOtrosEstudios"];
            var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
            int vIdEntidad = Convert.ToInt32(identida);       
            vFormacion.IdEntidad = vIdEntidad;

            //no permite guardar la grilla la grilla esta vacia
            if (totalEstudios == null)
            {
                toolBar.MostrarMensajeError("Debe agregar usando el botón + como minimo un estudio");
                return;
            }

            
            //verificar si ya hay una formación principal en la grilla si no hay no permitir guardar
            //primero verifica si ya tiene esudios es  porque que tiene formación principal
            int contarFormacion = 0;
            if ( vProveedorService.ConsultarFormacions(vIdEntidad, null, null, null, null).Count()< 1 )
            {
                foreach (DataRow dr in totalEstudios.Rows)
                {
                    if (dr["esformacion"].ToString().Equals("Formación Principal"))
                    {
                        contarFormacion++;
                    }
                }
                if (contarFormacion == 0)
                {
                    toolBar.MostrarMensajeError("Debe existir una formación principal");
                    return;
                }
            }
           
           

            //verificar si ya tiene estudios, si tiene ya cuenta con una formación principal
            if (vProveedorService.ConsultarFormacions(vIdEntidad, null, null, null, null).Count() > 0 && rblEsFormacionPrincipal.SelectedValue.Equals(true))
            {
                toolBar.MostrarMensajeError("ya existe una formacion Principal");
                return;
            }


            //guardar cunado viene por la opcion editar
            if (Request.QueryString["oP"] == "E")
            {
                vFormacion.EsFormacionPrincipal = Convert.ToBoolean(rblEsFormacionPrincipal.SelectedValue.ToString());
                vFormacion.CodigoModalidad = ddlCodigoModalidad.SelectedValue;
                vFormacion.IdProfesion = ddlProfesion.SelectedValue;
                vFormacion.IdFormacion = Convert.ToInt32(hfIdFormacion.Value);
                vFormacion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vFormacion, this.PageName, vSolutionPage);

                vResultado = vProveedorService.ModificarFormacion(vFormacion);
            }
            else
            {
                vFormacion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vFormacion, this.PageName, vSolutionPage);
                foreach (DataRow item in totalEstudios.Rows)
                {
                    vFormacion.CodigoModalidad = item["CodigoModalidad"].ToString();
                    vFormacion.IdProfesion = item["IdProfesion"].ToString();
                    vFormacion.EsFormacionPrincipal = Convert.ToBoolean(item["EsFormacionPrincipal"].ToString());
                    vFormacion.Estado = true;
                    vResultado = vProveedorService.InsertarFormacion(vFormacion);
                }

            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Formacion.IdFormacion", vFormacion.IdFormacion);
                SetSessionParameter("Formacion.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Cargar la modalidad académica
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEsFormacionPrincipal.Items.Insert(0, new ListItem("Otro Estudio", "false"));
            rblEsFormacionPrincipal.Items.Insert(0, new ListItem("Formación Principal", "true"));
            //rblEsFormacionPrincipal.SelectedValue = vFormacion.EsFormacionPrincipal.ToString().Trim().ToLower();
            ddlCodigoModalidad.Items.Clear();
            List<ModalidadAcademicaKactus> vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, null, null);
            foreach (ModalidadAcademicaKactus tD in vLModalidadesAcademicas)
            {
                ddlCodigoModalidad.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
            }
            ddlCodigoModalidad.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlProfesion.Enabled = false;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdFormacion = Convert.ToInt32(GetSessionParameter("Formacion.IdFormacion"));
            RemoveSessionParameter("Formacion.Id");
            Formacion vFormacion = new Formacion();
            vFormacion = vProveedorService.ConsultarFormacion(vIdFormacion);
            hfIdFormacion.Value = vFormacion.IdFormacion.ToString();
            ddlCodigoModalidad.SelectedValue = vFormacion.CodigoModalidad;
            ddlProfesion.Items.Insert(0, new ListItem(vFormacion.nombreProfesion.ToString(), vFormacion.IdProfesion));               
            rblEsFormacionPrincipal.SelectedValue = vFormacion.EsFormacionPrincipal.ToString().Trim().ToLower();
            rblEsFormacionPrincipal.Enabled = false;
            
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vFormacion.UsuarioCrea, vFormacion.FechaCrea, vFormacion.UsuarioModifica, vFormacion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="habilitar"></param>
    private void HabilitarNombreProfesion(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlProfesion.SelectedIndex = 0;
            ddlProfesion.Enabled = false;
        }
        else if (habilitar == true)
        {
            ddlProfesion.Enabled = true;

        }
        else
        {
            ddlProfesion.Enabled = false;
            ddlProfesion.SelectedIndex = 0; //Se coloca para que limpie en los casos en que se cambia de marco a adhesion, o cambio de categoria, que afecta el tipo de contrato y por ende este campo            
            ///pOtroEstudios.Visible = false;          
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idModalidadAcademicaSeleccionada"></param>
    protected void SeleccionarFormacion(string idModalidadAcademicaSeleccionada)
    {
        #region LlenarComboNombreProfesion
        ddlProfesion.Items.Clear();
        List<ProfesionKactus> vLTiposContratos = vContratoService.ConsultarProfesionesKactus(idModalidadAcademicaSeleccionada, null, null, null);
        foreach (ProfesionKactus tD in vLTiposContratos)
        {
            ddlProfesion.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
            // ddlProfesion1.Items.Add(new ListItem(tD.Descripcion,tD.Codigo.ToString()));
        }
        ddlProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));

        #endregion
        //control para habilitar profecion si tiene una modalidad selecciona
        if (idModalidadAcademicaSeleccionada.Equals("-1"))
        {
            HabilitarNombreProfesion(false);
        }
        else
        {
            HabilitarNombreProfesion(true);
        }

        //HabilitarModalidadSeleccion(false);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idModalidadAcademicaSeleccionada"></param>
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    #endregion

    #region otrosEstudios
    /// <summary>
    /// cargar a la entidad los valores de los textbox y demás
    /// </summary>
    /// <returns></returns>
    private bool AgregarOtrosEstudios()
    {
        var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
        //RemoveSessionParameter("EntidadProvOferente.IdEntidad");
        int vIdEntidad = Convert.ToInt32(identida);       
        // validar si el usuario ya tiene una formación principal
        Formacion otrosEstudios = new Formacion();
       

        if (Request.QueryString["oP"] == "E" )
        {
            LinkButton1.Visible = false;           
        }        
        else if(vProveedorService.ConsultarFormacions(vIdEntidad, null, null, null, null).Count() > 0 && rblEsFormacionPrincipal.SelectedValue.Equals("true"))
        {
            toolBar.MostrarMensajeError("ya existe una formacion Principal para este Usuario");
            return false;
        }
        

        
        DataTable dt = (DataTable)this.ViewState["vsOtrosEstudios"];
        if (dt != null)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["esformacion"].ToString().Equals(rblEsFormacionPrincipal.SelectedItem.Text.ToString()))
                {
                    toolBar.MostrarMensajeError("Solo se acepta una formación como principal");
                    return false;
                }
                if (dr["Idprofesion"].ToString().Equals(ddlProfesion.SelectedValue.Trim().ToString()))
                {
                    toolBar.MostrarMensajeError("El estudio seleccionado ya existe");
                    return false;
                }                
            }
        }

        toolBar.LipiarMensajeError();
        modalidad = ddlCodigoModalidad.SelectedItem.Text;
        profesion = ddlProfesion.SelectedItem.Text;
        otrosEstudios.IdEntidad = 1;
        otrosEstudios.CodigoModalidad = ddlCodigoModalidad.SelectedValue;
        otrosEstudios.IdProfesion = ddlProfesion.SelectedValue;
        otrosEstudios.EsFormacionPrincipal = false;
        otrosEstudios.Estado = true;
        otrosEstudios.UsuarioCrea = "admin";
        otrosEstudios.FechaCrea = DateTime.Now;
        otrosEstudios.UsuarioModifica = "admin";
        otrosEstudios.FechaModifica = DateTime.Now;
        this.CrearOtrosEstudios(otrosEstudios);
        //rblEsFormacionPrincipal.Enabled = false;
        return true;
    }
    /// <summary>
    /// Crear un dataTable y mapearlo para agregarlo al viewstate y la grilla
    /// </summary>
    /// <param name="pOtrosEstudios"></param>
    private void CrearOtrosEstudios(Formacion pOtrosEstudios)
    {
        DataTable dtOtrosEstudios = this.ObtenerDataTableOtrosEstudios();
        ////crea data row y asigna valores
        DataRow drOtrosEstudios = dtOtrosEstudios.NewRow();
        bool esformacion = bool.Parse(rblEsFormacionPrincipal.SelectedValue);
        drOtrosEstudios["NombreModalidad"] = modalidad;
        drOtrosEstudios["nombreProfesion"] = profesion;
        drOtrosEstudios["CodigoModalidad"] = ddlCodigoModalidad.SelectedValue;
        drOtrosEstudios["Idprofesion"] = ddlProfesion.SelectedValue;
        drOtrosEstudios["EsFormacionPrincipal"] = rblEsFormacionPrincipal.SelectedValue;
        if (esformacion)
        {
            drOtrosEstudios["esformacion"] = "Formación Principal";
        }
        else
        {
            drOtrosEstudios["esformacion"] = "Otros Estudios";
        }
        dtOtrosEstudios.Rows.Add(drOtrosEstudios);
        this.ViewState["vsOtrosEstudios"] = dtOtrosEstudios;
        this.gvOtrosEstudios.DataSource = dtOtrosEstudios;
        this.gvOtrosEstudios.DataBind();
    }
    /// <summary>
    /// crear datable y asignarle lo que tiene viewstate
    /// agregarle al dataTable los nuevos valores de columna
    /// </summary>
    /// <returns></returns>
    private DataTable ObtenerDataTableOtrosEstudios()
    {
        DataTable dtOtrosEstudios = (DataTable)this.ViewState["vsOtrosEstudios"];
        ////incializa dataTable si es nulo
        if (dtOtrosEstudios == null)
        {
            dtOtrosEstudios = new DataTable();
            dtOtrosEstudios.Columns.Add(new DataColumn("CodigoModalidad", typeof(string)));
            dtOtrosEstudios.Columns.Add(new DataColumn("Idprofesion", typeof(int)));
            dtOtrosEstudios.Columns.Add(new DataColumn("EsFormacionPrincipal", typeof(string)));
            dtOtrosEstudios.Columns.Add(new DataColumn("formacionPrincipal", typeof(bool)));
            // se agregan para mostrar nombres claros en el grid view
            dtOtrosEstudios.Columns.Add(new DataColumn("NombreModalidad", typeof(string)));
            dtOtrosEstudios.Columns.Add(new DataColumn("esformacion", typeof(string)));
            dtOtrosEstudios.Columns.Add(new DataColumn("nombreProfesion", typeof(string)));
            //dtOtrosEstudios.Columns.Add(new DataColumn("Estado", typeof(bool)));
        }
        return dtOtrosEstudios;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void eventoRadioButtonFormacion(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
    }
    #endregion

}
