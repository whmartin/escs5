using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de visualización detallada para la entidad Formacion
/// </summary>
public partial class Page_Formacion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/Formacion";
    ProveedorService vProveedorService = new ProveedorService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Formacion.IdFormacion", hfIdFormacion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    //se comenta porque no se permite eliminar estudios
    //protected void btnEliminar_Click(object sender, EventArgs e)
    //{
    //    EliminarRegistro();
    //}
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdFormacion = Convert.ToInt32(GetSessionParameter("Formacion.IdFormacion"));
            RemoveSessionParameter("Formacion.IdFormacion");

            if (GetSessionParameter("Formacion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Formacion.Guardado");


            Formacion vFormacion = new Formacion();
            vFormacion = vProveedorService.ConsultarFormacion(vIdFormacion);
            hfIdFormacion.Value = vFormacion.IdFormacion.ToString();
            ddlCodigoModalidad.Items.Insert(0, new ListItem(vFormacion.nombreModalidadAcademica.ToString()));
            ddlIdProfesion.Items.Insert(0, new ListItem(vFormacion.nombreProfesion));
            rblEsFormacionPrincipal.SelectedValue = vFormacion.EsFormacionPrincipal.ToString().Trim().ToLower();
            ObtenerAuditoria(PageName, hfIdFormacion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vFormacion.UsuarioCrea, vFormacion.FechaCrea, vFormacion.UsuarioModifica, vFormacion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// comentado porque no se permite eliminar estudios
        /// </summary>
    //private void EliminarRegistro()
    //{
    //    try
    //    {
    //        int vIdFormacion = Convert.ToInt32(hfIdFormacion.Value);

    //        Formacion vFormacion = new Formacion();
    //        vFormacion = vProveedorService.ConsultarFormacion(vIdFormacion);
    //        InformacionAudioria(vFormacion, this.PageName, vSolutionPage);
    //        int vResultado = vProveedorService.EliminarFormacion(vFormacion);
    //        if (vResultado == 0)
    //        {
    //            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
    //        }
    //        else if (vResultado == 1)
    //        {
    //            toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
    //            SetSessionParameter("Formacion.Eliminado", "1");
    //            NavigateTo(SolutionPage.List);
    //        }
    //        else
    //        {
    //            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
    //        }
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}
    /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Formacion", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEsFormacionPrincipal.Items.Insert(0, new ListItem("Otros Estudios", "false"));
            rblEsFormacionPrincipal.Items.Insert(0, new ListItem("Formación Principal", "true"));            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
