<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Formacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdFormacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Modalidad
            </td>
            <td>
                Profesi&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlCodigoModalidad"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdProfesion"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Formacion Principal
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEsFormacionPrincipal" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
        </tr>       
    </table>
</asp:Content>
