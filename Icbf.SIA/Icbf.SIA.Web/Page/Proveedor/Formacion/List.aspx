<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Formacion_List" %>
<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
 
    <asp:Panel runat="server" ID="pnlConsulta">
      
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
<%--        <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />--%>
          <asp:UpdatePanel ID="upPanel" runat="server">
              
        <ContentTemplate>
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvFormacion" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdFormacion" CellPadding="0" Height="16px"
                        OnSorting="gvFormacion_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvFormacion_PageIndexChanging" OnSelectedIndexChanged="gvFormacion_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Modalidad" DataField="ModalidadAcademica"  SortExpression="IdEntidad"/>
                            <asp:BoundField HeaderText="Profesi&oacute;n" DataField="Profesion"  SortExpression="CodigoModalidad"/>
                            <asp:BoundField HeaderText="Tipo Estudio" DataField="tipoFormacion" />                
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
              </ContentTemplate>
              </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
