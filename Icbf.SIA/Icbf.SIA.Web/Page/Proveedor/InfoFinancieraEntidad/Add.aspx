<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_InfoFinancieraEntidad_Add"
    MaintainScrollPositionOnPostback="True" %>

<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdInfoFin" runat="server" />
    <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    <script src="../../../Scripts/Jqfc.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.numeric.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            $(".decimal").numeric(",");

            $('.currency').blur(function () {
                $('.currency').formatCurrency({ decimalSymbol: ',', digitGroupSymbol: '.' });
            });

            $("input").keypress(function (evt) {
                //Deterime where our character code is coming from within the event
                var charCode = evt.charCode || evt.keyCode;
                if (charCode == 13) { //Enter key's keycode
                    return false;
                }
            });
        }
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }

        function LimpiarNumero(object) {
            var numero = object;
            numero = replaceAll(numero, "$", "");
            numero = replaceAll(numero, ".", "");
            return parseFloat(numero);
        }
        function replaceAll(text, busca, reemplaza) {
            while (text.toString().indexOf(busca) != -1)
                text = text.toString().replace(busca, reemplaza);
            return text;
        }

        function Calcular() {
            var activoCte = LimpiarNumero(document.getElementById('<%= txtActivoCte.ClientID %>').value);
            var pasivoCte = LimpiarNumero(document.getElementById('<%= txtPasivoCte.ClientID %>').value);
            var activoTot = LimpiarNumero(document.getElementById('<%= txtActivoTotal.ClientID %>').value);
            var pasivoTot = LimpiarNumero(document.getElementById('<%= txtPasivoTotal.ClientID %>').value);
            var utilidadOp = LimpiarNumero(document.getElementById('<%= txtUtilidadOperacional.ClientID %>').value);
            var gastosInt = LimpiarNumero(document.getElementById('<%= txtGastosInteresFinancieros.ClientID %>').value);
            var patrimonio = LimpiarNumero(document.getElementById('<%= txtPatrimonio.ClientID %>').value);
            var salarioMin = LimpiarNumero(document.getElementById('<%= txtSalarioMinimo.ClientID %>').value);

            if (pasivoCte != '' && activoCte != '' && pasivoCte > 0.0) {
                if (isNaN(Math.round((activoCte / pasivoCte) * 1000) / 1000))
                    document.getElementById('<%= txtLiquidez.ClientID %>').value = '';
                else if (pasivoCte != '' && pasivoCte == 0.0)
                {
                    document.getElementById('<%= txtLiquidez.ClientID %>').value = '';
                    document.getElementById('<%= lblLiquidez.ClientID %>').innerHTML = 'La liquidez es indeterminada, puesto que Pasivo Corriente es cero';
                }
                else{
                document.getElementById('<%= txtLiquidez.ClientID %>').value = Math.round((activoCte / pasivoCte) * 1000) / 1000;
                    document.getElementById('<%= lblLiquidez.ClientID %>').innerHTML = '';
                }
        }

        else {
            document.getElementById('<%= txtLiquidez.ClientID %>').value = '';
            document.getElementById('<%= lblLiquidez.ClientID %>').innerHTML = 'La liquidez es indeterminada, puesto que Pasivo Corriente es cero';
        }
        if (pasivoTot != '' && activoTot != '' && activoTot > 0.0) {
            if (isNaN(Math.round((pasivoTot / activoTot) * 1000) / 1000))
                document.getElementById('<%= txtEndeudamiento.ClientID %>').value = '';
            else
                document.getElementById('<%= txtEndeudamiento.ClientID %>').value = Math.round((pasivoTot / activoTot) * 1000) / 1000;
        }
        else {
            document.getElementById('<%= txtEndeudamiento.ClientID %>').value = '';
        }
        if (utilidadOp != '' && gastosInt != '' && gastosInt > 0.0) {
            if (isNaN(Math.round((utilidadOp / gastosInt) * 1000) / 1000))
                document.getElementById('<%= txtRazonCoberturaInteres.ClientID %>').value = '';
                else
                    document.getElementById('<%= txtRazonCoberturaInteres.ClientID %>').value = Math.round((utilidadOp / gastosInt) * 1000) / 1000;
                document.getElementById('<%= lblCoberturaInteres.ClientID %>').innerHTML = ''
            }
            else {
                document.getElementById('<%= txtRazonCoberturaInteres.ClientID %>').value = '';
                document.getElementById('<%= lblCoberturaInteres.ClientID %>').innerHTML = 'La Razón de cobertura es indeterminada, puesto que los gastos de interés son cero'

            }

            if (patrimonio != '' && salarioMin != '' && salarioMin > 0.0) {
                if (isNaN(Math.round((patrimonio / salarioMin) * 1000) / 1000))
                    document.getElementById('<%= txtPatrimonioSMLV.ClientID %>').value = '';
                else
                    document.getElementById('<%= txtPatrimonioSMLV.ClientID %>').value = Math.round((patrimonio / salarioMin) * 1000) / 1000;
            }
            else {
                document.getElementById('<%= txtPatrimonioSMLV.ClientID %>').value = '';
            }
        }

        function ValidaActivoTotal() {
            var ActivoTotal = document.getElementById('<%= txtActivoTotal.ClientID %>').value;
            var PasivoTotal = document.getElementById('<%= txtPasivoTotal.ClientID %>').value;
            var Patrimonio = document.getElementById('<%= txtPatrimonio.ClientID %>').value;
            var chkActivo = document.getElementById('cphCont_rblConfirmado_0');

            var ActivoTotalNumero = 0;
            if (ActivoTotal != '') {
                var ActivoTotalTemp = ActivoTotal.replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '');
                ActivoTotalNumero = parseFloat(ActivoTotalTemp.replace(',', '.'));
            }
            var PasivoTotalNumero = 0;
            if (PasivoTotal != '')
                PasivoTotalNumero = parseFloat(PasivoTotal.replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.'));

            var PatrimonioNumero = 0;
            if (Patrimonio != '')
                PatrimonioNumero = parseFloat(Patrimonio.replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.'));



            if (ActivoTotalNumero == (PasivoTotalNumero + PatrimonioNumero)) {

                chkActivo.disabled = false;

            }
            else {
                chkActivo.disabled = true;
                chkActivo.checked = false;
            }
        }

        function funValidaActivoCheckbox(source, args) {
            if (args.Value.length > 0) {
                args.IsValid = ValidaActivoCheckbox();
            } else {
                args.IsValid = true;
            }
        }

        function ValidaActivoCheckbox() {
            var ActivoTotal = document.getElementById('<%= txtActivoTotal.ClientID %>').value;
            var PasivoTotal = document.getElementById('<%= txtPasivoTotal.ClientID %>').value;
            var Patrimonio = document.getElementById('<%= txtPatrimonio.ClientID %>').value;
            var chkActivo = document.getElementById('cphCont_rblConfirmado_0');

            var ActivoTotalNumero = -1;
            if (ActivoTotal != '')
                ActivoTotalNumero = parseFloat(ActivoTotal.replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.'));

            var PasivoTotalNumero = 0;
            if (PasivoTotal != '')
                PasivoTotalNumero = parseFloat(PasivoTotal.replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.'));

            var PatrimonioNumero = 0;
            if (Patrimonio != '')
                PatrimonioNumero = parseFloat(Patrimonio.replace('$', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.'));


            if (ActivoTotalNumero == (PasivoTotalNumero + PatrimonioNumero)) {
                chkActivo.checked = true;
                return true;
            }
            else {
                chkActivo.checked = false;
                alert('Verifique la ecuación Activo Total = Pasivo total + Patrimonio');
                return false;
            }
        }

        $(document).ready(function () {
            if (document.getElementById('<%= txtActivoCte.ClientID %>').value.length > 0)
                if (document.getElementById('<%= txtPasivoCte.ClientID %>').value.length > 0)
                    if (document.getElementById('<%= txtActivoTotal.ClientID %>').value.length > 0)
                        if (document.getElementById('<%= txtPasivoTotal.ClientID %>').value.length > 0)
                            if (document.getElementById('<%= txtUtilidadOperacional.ClientID %>').value.length > 0)
                                if (document.getElementById('<%= txtGastosInteresFinancieros.ClientID %>').value.length > 0)
                                    if (document.getElementById('<%= txtPatrimonio.ClientID %>').value.length > 0)
                                        if (document.getElementById('<%= txtSalarioMinimo.ClientID %>').value.length > 0)
                                            Calcular();
        });
    </script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>Vigencia Fiscal *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="ddlIdVigencia"
                    SetFocusOnError="true" ErrorMessage="Debe seleccionar una vigencia" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
            <td>Salario M&iacute;nimo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlIdVigencia" runat="server" OnSelectedIndexChanged="ddlIdVigencia_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSalarioMinimo" Enabled="false" CssClass="decimal currency"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftSalarioMinimo" runat="server" TargetControlID="txtSalarioMinimo"
                    FilterType="Custom, Numbers" ValidChars="$,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>Activo Total *
                <asp:RequiredFieldValidator runat="server" ID="rfvActivoTotal" ControlToValidate="txtActivoTotal"
                    SetFocusOnError="true" ErrorMessage="Debe registrar el activo total a esta vigencia"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Pasivo Total *
                <asp:RequiredFieldValidator runat="server" ID="rfvPasivoTotal" ControlToValidate="txtPasivoTotal"
                    SetFocusOnError="true" ErrorMessage="Debe registrar el pasivo total a esta vigencia"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtActivoTotal" onchange="javascript:ValidaActivoTotal();"
                    MaxLength="18" CssClass="decimal currency" onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftActivoTotal" runat="server" TargetControlID="txtActivoTotal"
                    FilterType="Custom, Numbers" ValidChars="$,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPasivoTotal" onchange="javascript:ValidaActivoTotal();"
                    MaxLength="18" CssClass="decimal currency" onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPasivoTotal" runat="server" TargetControlID="txtPasivoTotal"
                    FilterType="Custom, Numbers" ValidChars="$,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>Activo Corriente *
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtActivoCte"
                    SetFocusOnError="true" ErrorMessage="Debe registrar el Activo corriente a esta vigencia"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Pasivo Corriente *
                <asp:RequiredFieldValidator runat="server" ID="rfvPasivoCte" ControlToValidate="txtPasivoCte"
                    SetFocusOnError="true" ErrorMessage="Debe registrar el pasivo corriente a esta vigencia"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtActivoCte" MaxLength="18" CssClass="decimal currency"
                    onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftActivoCte" runat="server" TargetControlID="txtActivoCte"
                    FilterType="Custom, Numbers" ValidChars="$,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPasivoCte" MaxLength="18" CssClass="decimal currency"
                    onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPasivoCte" runat="server" TargetControlID="txtPasivoCte"
                    FilterType="Custom, Numbers" ValidChars="$,." />
            </td>
        </tr>
        <tr class="rowB">
            <td></td>
            <td>Patrimonio *
                <asp:RequiredFieldValidator runat="server" ID="rfvPatrimonio" ControlToValidate="txtPatrimonio"
                    SetFocusOnError="true" ErrorMessage="Debe registrar el patrimonio a esta vigencia"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td></td>
            <td>
                <asp:TextBox runat="server" ID="txtPatrimonio" onchange="javascript:ValidaActivoTotal();"
                    MaxLength="18" CssClass="decimal currency" onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPatrimonio" runat="server" TargetControlID="txtPatrimonio"
                    FilterType="Custom, Numbers" ValidChars="$,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>Utilidad operacional *
                <asp:RequiredFieldValidator runat="server" ID="rfvUtilidadOperacional" ControlToValidate="txtUtilidadOperacional"
                    SetFocusOnError="true" ErrorMessage="Debe registrar la utilidad operacional"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>Gastos de Inter&eacute;s *
                <asp:RequiredFieldValidator runat="server" ID="rfvGastosInteresFinancieros" ControlToValidate="txtGastosInteresFinancieros"
                    SetFocusOnError="true" ErrorMessage="Debe ingresar los gastos de intereses" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUtilidadOperacional" MaxLength="18" CssClass="decimal currency"
                    onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftUtilidadOperacional" runat="server" TargetControlID="txtUtilidadOperacional"
                    FilterType="Custom, Numbers" ValidChars="$.," />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtGastosInteresFinancieros" MaxLength="18" CssClass="decimal currency"
                    onblur="Calcular();"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftGastosInteresFinancieros" runat="server" TargetControlID="txtGastosInteresFinancieros"
                    FilterType="Custom, Numbers" ValidChars="$.," />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Observaciones sobre la informaci&oacute;n financiera
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtObservacionesInformacionFinanciera" MaxLength="50"
                    Width="250px" Height="60px" CssClass="TextBoxGrande" TextMode="MultiLine" onKeyDown="limitText(this,256);"
                    onKeyUp="limitText(this,256);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObservacionesInformacionFinanciera" runat="server"
                    TargetControlID="txtObservacionesInformacionFinanciera" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                    ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Indicadores financieros calculados a partir de los datos ingresados anteriormente
            </td>
        </tr>
        <tr class="rowA">
            <td>Liquidez (activo corriente/pasivo corriente)
            </td>
            <td>Endeudamiento (Pasivo total/activo total)
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtLiquidez" Enabled="false"></asp:TextBox>
                <asp:Label runat="server" ID="lblLiquidez" ClientIDMode="AutoID" ForeColor="Red" Text=""></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEndeudamiento" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>Raz&oacute;n de cobertura de inter&eacute;s (utilidad operacional/gastos de inter&eacute;s)
            </td>
            <td>Patrimonio expresado SMLV (patrimonio/salario m&iacute;nimo)
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtRazonCoberturaInteres" Enabled="false"></asp:TextBox>
                <asp:Label runat="server" ID="lblCoberturaInteres" ClientIDMode="AutoID" ForeColor="Red"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPatrimonioSMLV" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <input type="button" value="Calcular" style="width: 80px" onclick="Calcular();" />
                <asp:CheckBoxList ID="rblConfirmado" runat="server">
                    <asp:ListItem Text="Confirma los indicadores financieros?" Value="True"></asp:ListItem>
                </asp:CheckBoxList>
                <asp:CustomValidator runat="server" Text="" Display="Dynamic" SetFocusOnError="true"
                    ValidationGroup="btnGuardar" EnableClientScript="True" ClientValidationFunction="funValidaActivoCheckbox"
                    ID="cvEcuacionActivoTotal" ErrorMessage="Verifique la ecuación: Activo Total = Pasivo Total + Patrimonio">
                </asp:CustomValidator>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">Cuenta con registro &uacute;nico de proponente RUP renovado a la vigencia fiscal
                <asp:RadioButtonList ID="rblRupRenovado" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rblRupRenovado_SelectedIndexChanged">
                    <asp:ListItem Text="Si" Value="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="False" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <asp:Panel ID="panelDocumentos" runat="server" Enabled="True">
            <tr class="rowA">
                <td colspan="2">Estados financieros que adjunte debe tener corte al 31 de diciembre a&ntilde;o inmediatamente
                    anterior
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:Label ID="lblNota" Text="Nota: para eliminar el documento debe remplazarlo por el soporte legal"
                        runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr class="rowBG">
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocFinancieraProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto,IdTipoDocumento,MaxPermitidoKB,ExtensionesPermitidas"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocFinancieraProv_RowCommand" OnRowEditing="gvDocFinancieraProv_RowEditing"
                        OnRowCancelingEdit="gvDocFinancieraProv_RowCancelingEdit" OnRowUpdating="gvDocFinancieraProv_RowUpdating"
                        OnRowDeleting="gvDocFinancieraProv_RowDeleting" OnPageIndexChanging="gvDocFinancieraProv_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdDocAdjunto" runat="server" Text='<%# Bind("IdDocAdjunto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre Documento" SortExpression="NombreDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link Documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'></asp:Label>--%>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorInfoFinanciera_") + "ProveedorInfoFinanciera_".Length) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--<asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'
                                        Visible="false"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorInfoFinanciera_") + "ProveedorInfoFinanciera_".Length) %>'
                                        Visible="false"></asp:TextBox>
                                    <asp:FileUpload ID="fuDocumento" runat="server"></asp:FileUpload>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Observaciones" SortExpression="Observaciones">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                        TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtObservaciones" runat="server" Width="150px" Height="80px" CssClass="TextBoxGrande"
                                        TextMode="MultiLine" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);"
                                        Text='<%# Bind("Observaciones") %>' MaxLength="50"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEditar" ImageUrl="~/Image/btn/attach.jpg"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Edit" ToolTip="Editar"></asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgEliminar" ImageUrl="~/Image/btn/delete.gif"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Delete" ToolTip="Eliminar"
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) && !Page.ClientQueryString.Contains("oP=E") %>'
                                        OnClientClick="return confirm('Está seguro que desea eliminar el documento?');"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar"></asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)(Request.QueryString["oP"] == "E") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID="pnlObservacionesOld" runat="server" Visible="false">
            <tr class="rowA">
                <td colspan="2">Observaciones por el validador documental ICBF
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesValidadorICBF" Enabled="false"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftObservacionesValidadorICBF" runat="server" TargetControlID="txtObservacionesValidadorICBF"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID="pnEstado" runat="server" Visible="false">
            <tr class="rowA">
                <td>Estado Validación documental
                </td>
                <td>Registrado Por
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:HiddenField ID="hfIDEstadoValidacionDocumental" runat="server"></asp:HiddenField>
                    <asp:TextBox runat="server" ID="txtEstadoValidacionDocumental" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRegistradorPor" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </asp:Panel>
    </table>
</asp:Content>
