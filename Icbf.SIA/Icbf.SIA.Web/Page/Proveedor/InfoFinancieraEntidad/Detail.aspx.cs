using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

using Icbf.SIA.Service;
using Icbf.SIA.Entity;

using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using System.IO;

/// <summary>
/// Página que despliega el detalle del registro de información módulo financiera
/// </summary>
public partial class Page_InfoFinancieraEntidad_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/InfoFinancieraEntidad";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRuboService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();       
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SolutionPage vSolutionPage = SolutionPage.Detail;
            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {

                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("InfoFinancieraEntidad.IdInfoFin", hfIdInfoFin.Value);
        NavigateTo(SolutionPage.Edit);
    }
    
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("List.aspx");
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));
            //RemoveSessionParameter("InfoFinancieraEntidad.IdInfoFin");

            if (GetSessionParameter("InfoFinancieraEntidad.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("InfoFinancieraEntidad.Guardado");

                    

            InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();
            vInfoFinancieraEntidad = vProveedorService.ConsultarInfoFinancieraEntidad(vIdInfoFin);
            hfIdInfoFin.Value = vInfoFinancieraEntidad.IdInfoFin.ToString();
            ddlIdVigencia.SelectedValue = vInfoFinancieraEntidad.IdVigencia.ToString();
            ddlIdVigencia.Enabled = false;

            //Logica para traer el mínimo 
            string descripcion = "minimo";
            int anio = int.Parse(ddlIdVigencia.SelectedItem.Text);
            SalarioMinimo salarioMinimo = vOferenteService.ConsultarSalarioMinimo(anio,descripcion);
            txtSalarioMinimo.Text = salarioMinimo.Valor.ToString("C2");

            txtActivoCte.Text = vInfoFinancieraEntidad.ActivoCte.ToString("C2");
            txtActivoTotal.Text = vInfoFinancieraEntidad.ActivoTotal.ToString("C2");
            txtPasivoCte.Text = vInfoFinancieraEntidad.PasivoCte.ToString("C2");
            txtPasivoTotal.Text = vInfoFinancieraEntidad.PasivoTotal.ToString("C2");
            txtUtilidadOperacional.Text = vInfoFinancieraEntidad.UtilidadOperacional.ToString("C2");
            txtPatrimonio.Text = vInfoFinancieraEntidad.Patrimonio.ToString("C2");
            txtGastosInteresFinancieros.Text = vInfoFinancieraEntidad.GastosInteresFinancieros.ToString("C2");

            //Se consulta el estado de validacion documental
            hfIDEstadoValidacionDocumental.Value = vInfoFinancieraEntidad.EstadoValidacion.ToString();
            txtEstadoValidacionDocumental.Text = vProveedorService.ConsultarEstadoValidacionDocumental(vInfoFinancieraEntidad.EstadoValidacion).Descripcion;
            txtRegistradorPor.Text = vInfoFinancieraEntidad.UsuarioCrea;

            //Se aplica regla de negocio para mostrar el estado
            //AplicaReglaNegocioEdicion(vInfoFinancieraEntidad.UsuarioCrea, vInfoFinancieraEntidad.EstadoValidacion);
            AplicaReglaNegocioEdicion(GetSessionUser().NombreUsuario, vInfoFinancieraEntidad.EstadoValidacion);

            txtObservacionesInformacionFinanciera.Text = vInfoFinancieraEntidad.ObservacionesInformacionFinanciera;
            txtObservacionesValidadorICBF.Text = vInfoFinancieraEntidad.ObservacionesValidadorICBF;

            rblConfirmado.SelectedValue = vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros.ToString();
            rblRupRenovado.SelectedValue = vInfoFinancieraEntidad.RupRenovado.ToString();
            

            //Datos calculados

            txtLiquidez.Text = vInfoFinancieraEntidad.Liquidez.ToString("N2");
            txtEndeudamiento.Text = vInfoFinancieraEntidad.Endeudamiento.ToString("N2");
            try
            {
                txtPatrimonioSMLV.Text = (vInfoFinancieraEntidad.Patrimonio / salarioMinimo.Valor).ToString("N2");
            }
            catch (DivideByZeroException)
            {
                toolBar.MostrarMensajeError("No es posible divir por cero.");
            }
            txtRazonCoberturaInteres.Text = vInfoFinancieraEntidad.RazonCoberturaInteres.ToString("N2");

            //Llenamos las grillas de los documentos
            
            string vRupRenovado = bool.Parse(rblRupRenovado.SelectedValue)?"1":"0";


            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vInfoFinancieraEntidad.IdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            gvDocFinancieraProv.DataSource = vProveedorService.ConsultarDocFinancieraProv_IdInfoFin_Rup(vIdInfoFin, vRupRenovado, vTipoPersona, vTipoSector);

            gvDocFinancieraProv.DataBind();


            BuscarObservaciones();

            ObtenerAuditoria(PageName, hfIdInfoFin.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInfoFinancieraEntidad.UsuarioCrea, vInfoFinancieraEntidad.FechaCrea, vInfoFinancieraEntidad.UsuarioModifica, vInfoFinancieraEntidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void EliminarRegistro()
    {
        try
        {
            int vIdInfoFin = Convert.ToInt32(hfIdInfoFin.Value);

            InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();
            vInfoFinancieraEntidad = vProveedorService.ConsultarInfoFinancieraEntidad(vIdInfoFin);
            InformacionAudioria(vInfoFinancieraEntidad, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarInfoFinancieraEntidad(vInfoFinancieraEntidad);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("InfoFinancieraEntidad.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            
             //Se revisa si está en modo de validacion proveedor sino está en modo validación está en modo edición
            if (string.IsNullOrEmpty(GetSessionParameter("ValidarProveedor.NroRevision").ToString()))
            {


                int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));
                InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();
                vInfoFinancieraEntidad = vProveedorService.ConsultarInfoFinancieraEntidad(vIdInfoFin);

                SIAService vRUBOService = new SIAService();
                if (vRUBOService.ConsultarUsuario(vInfoFinancieraEntidad.UsuarioCrea).Rol.Split(';').Contains("PROVEEDORES") && vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol != "PROVEEDORES")
                {
                    if (vInfoFinancieraEntidad.UsuarioCrea == GetSessionUser().NombreUsuario)
                    {
                        toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                    }
                }
                else
                {
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }

                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            }
            else
            {
                toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
            }

            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);    
            //toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            

            gvDocFinancieraProv.PageSize = PageSize();
            gvDocFinancieraProv.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Información Financiera Entidad", SolutionPage.Detail.ToString());
            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();

            //AplicaReglaNegocioEdicion(string usuario, int IdEstado);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Habililta controles según el tipo de usuario, si es proveedor
    /// </summary>
    /// <param name="usuario"></param>
    /// <param name="IdEstado"></param>
    private void AplicaReglaNegocioEdicion(string usuario, int IdEstado)
    {
        //Se verifica si el usuario es interno o externo y aplica regla
        if (vRuboService.ConsultarUsuario(usuario).Rol.Split(';').Contains("PROVEEDORES"))
        {//Es Externo
            pnlEstado.Visible = false;
        }
        else
        {//Es interno
            pnlEstado.Visible = true;
        }

        //Si el estado es “validado” o “en validación” no permite la edición solo visualización

        if (!GetSessionUser().NombreUsuario.Equals(usuario) || IdEstado==4 || IdEstado ==5 )
        {
            toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar -= new ToolBarDelegate(btnEliminar_Click);    
        }

    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            /*Coloque aqui el codigo para llenar los DropDownList*/

            ddlIdVigencia.DataSource = vRuboService .ConsultarVigenciasSinAnnoActual(null);
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataTextField = "AcnoVigencia";            
            ddlIdVigencia.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocFinancieraProv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocFinancieraProv.SelectedRow);
    }
    
    protected void gvDocFinancieraProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocFinancieraProv.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Commando de renglón para la grilla DocFinancieraProv
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocFinancieraProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Considera un filtro para realizar una búsqueda y asignarla a una grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
          
            int vIdInfoFin = Convert.ToInt32(hfIdInfoFin.Value);
            string vRupRenovado = bool.Parse(rblRupRenovado.SelectedValue) ? "1" : "0";

            InfoFinancieraEntidad vInfoFinancieraEntidad = vProveedorService.ConsultarInfoFinancieraEntidad(vIdInfoFin);
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vInfoFinancieraEntidad.IdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            gvDocFinancieraProv.DataSource = vProveedorService.ConsultarDocFinancieraProv_IdInfoFin_Rup(vIdInfoFin, vRupRenovado, vTipoPersona, vTipoSector);
            gvDocFinancieraProv.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocFinancieraProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocFinancieraProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIdVigencia_SelectedIndexChanged(object sender, EventArgs e)
    {
        int anio = int.Parse(ddlIdVigencia.Text);
        string descripcion = "minimo";
        SalarioMinimo salarioMinimo = vOferenteService.ConsultarSalarioMinimo(anio,descripcion);
        txtSalarioMinimo.Text = txtSalarioMinimo.Text;
    }

    #region Observaciones
    
    private void BuscarObservaciones()
    {        
        ucHistorialRevisiones1.Titulo = "Historial de observaciones del módulo Financiero";
        ucHistorialRevisiones1.MensajeGuardar = "Está seguro del resultado de su revisión para esta vigencia?";
        ucHistorialRevisiones1.TituloConfirmaYAprueba = "Confirma que los datos coinciden con el documento adjunto y lo aprueba?";
        int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));
        List<ValidarInfo> lista = vProveedorService.ConsultarValidarInfoFinancieraEntidads(vIdInfoFin, null, null).ToList<ValidarInfo>();
        ucHistorialRevisiones1.DataSource = lista;
        ucHistorialRevisiones1.ConfirmaVisible = false;
        //
        //Si está activa la opcion de ver Panel para agregar Observaciones
        //

        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoFinancieraEntidad.NroRevision").ToString()))
        {

            //Despues de cargar se cambia el estado a En Validación
            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
            Int32 iIdEntidad = Convert.ToInt32(idEntidad);

            Icbf.Proveedor.Entity.InfoFinancieraEntidad info = vProveedorService.ConsultarInfoFinancieraEntidad(vIdInfoFin);
            if (!info.Finalizado) // SI NO ESTA FINALIZADO
            {
                ucHistorialRevisiones1.ConfirmaVisible = true;               
                ucHistorialRevisiones1.eventoGuardarObservaciones += new ImageClickDelegate(OnGuardarObservaciones);
            
                info.EstadoValidacion = 5; //EN VALIDACION vProveedorService.ConsultarEstadoValidacionDocumental().IdEstadoValidacionDocumental; //Estado En Validación
                info.UsuarioModifica = GetSessionUser().NombreUsuario;
                info.FechaModifica = DateTime.Now;
                info.Finalizado = false;
                try
                {

                    Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(iIdEntidad);
                    string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
                    Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                    string vTipoPersona = vTercero.IdTipoPersona.ToString();
                    int resultado = vProveedorService.ModificarInfoFinancieraEntidad(info, vTipoPersona, vTipoSector);

                    if (resultado >= 1)
                    {
                        //Exitoso
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No pudo cambiarse al estado En Validación");
                    }
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }

                //Se carga el control con la última información
                ValidarInfoFinancieraEntidad vValidarInfoFin = vProveedorService.ConsultarValidarInfoFinancieraEntidad_Ultima(iIdEntidad, Convert.ToInt16(ddlIdVigencia.SelectedValue));
                if (vValidarInfoFin.NroRevision > 0)
                {
                    //revision existente
                    ucHistorialRevisiones1.Observaciones = vValidarInfoFin.Observaciones;
                    ucHistorialRevisiones1.ConfirmaYAprueba = vValidarInfoFin.ConfirmaYAprueba;
                    ucHistorialRevisiones1.NroRevision = vValidarInfoFin.NroRevision;
                }
                else
                {
                    //nueva revisión
                    ucHistorialRevisiones1.Observaciones = string.Empty;
                    ucHistorialRevisiones1.ConfirmaYAprueba = false;
                    ucHistorialRevisiones1.NroRevision = info.NroRevision;
                }

                
                Session["ucHistorialRevisiones1"] = ucHistorialRevisiones1;
                
            }
        }
        
    }

    private void OnGuardarObservaciones()
    {
        try
        {
            //inicializa variables
            int vResultado;
            string vObservaciones = string.Empty;
            bool bNuevo = false;
            ValidarInfoFinancieraEntidad vValidarInfoFinancieraEntidad = new ValidarInfoFinancieraEntidad();


            //toma la entidad en session
            String vIdEntidad = Convert.ToString(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));


            //Obtiene la info del usercontrol
            ValidarInfo info = ((Page_Proveedor_UserControl_ucHistorialRevisiones)Session["ucHistorialRevisiones1"]).vValidarInfo;
            Int32 iIdEntidad = Convert.ToInt32(vIdEntidad);

            //obtiene la ultima observacion disponible
            vValidarInfoFinancieraEntidad = vProveedorService.ConsultarValidarInfoFinancieraEntidad_Ultima(iIdEntidad, Convert.ToInt16(ddlIdVigencia.SelectedValue));

            //si existe en la bd la actualiza
            if (vValidarInfoFinancieraEntidad.IdInfoFin == 0)
            {
                //No existe
                vValidarInfoFinancieraEntidad.IdInfoFin = vIdInfoFin ;
                vValidarInfoFinancieraEntidad.IdVigencia = Convert.ToInt16(ddlIdVigencia.SelectedValue);
                bNuevo = true;
            }

            if (info.Observaciones != null)
                vObservaciones = Convert.ToString(info.Observaciones).Trim();
            vValidarInfoFinancieraEntidad.Observaciones = vObservaciones;
            vValidarInfoFinancieraEntidad.ConfirmaYAprueba = Convert.ToBoolean(info.ConfirmaYAprueba);
            vValidarInfoFinancieraEntidad.NroRevision = info.NroRevision;

            //InformacionAudioria(vValidarInfoFinancieraEntidad, this.PageName, vSolutionPage);

            int idEstado = 0;
            if (vValidarInfoFinancieraEntidad.ConfirmaYAprueba == true)
                idEstado = 4;//Se obtiene el estado Validado
            else
                idEstado = 2;//Se obtiene el estado Por Ajustar

            if (bNuevo)
            {
                vValidarInfoFinancieraEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.InsertarValidarInfoFinancieraEntidad(vValidarInfoFinancieraEntidad, idEstado);
            }
            else
            {
                vValidarInfoFinancieraEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.ModificarValidarInfoFinancieraEntidad(vValidarInfoFinancieraEntidad, idEstado);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ValidarProveedor.ObservacionGuardada", "1");
                //toolBar.MostrarMensajeGuardado("Guardado exitosamente");
                //BuscarObservacionesDatosBasicos();
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    #endregion

}
