<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_InfoFinancieraEntidad_List" %>
<%@ Register src="../UserControl/ucDatosProveedor.ascx" tagname="ucDatosProveedor" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    </asp:Panel> 
        <table width="90%" align="center">
            <tr class="rowAG">
                <td width="100%">
                    <asp:Panel runat="server" ID="pnlLista" align="center" width="1000"  ScrollBars="Horizontal">
                        <asp:GridView runat="server" ID="gvInfoFinancieraEntidad" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" 
                        DataKeyNames="IdInfoFin,IdVigencia,RupRenovado" CellPadding="0" Height="16px"                        
                        OnPageIndexChanging="gvInfoFinancieraEntidad_PageIndexChanging" 
                        OnSelectedIndexChanged="gvInfoFinancieraEntidad_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vigencia">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblVigencia" Text='<%# GetAnioVigencia(Container.DataItemIndex) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                                                         
                            <asp:TemplateField HeaderText="Activo Total" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblActivoTotal" Text='<%# Bind("ActivoTotal", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Activo Corriente" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblActivoCte" Text='<%# Bind("ActivoCte", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pasivo Total" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblPasivoTotal" Text='<%# Bind("PasivoTotal", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pasivo Corriente" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblPasivoCte" Text='<%# Bind("PasivoCte", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Patrimonio" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblPatrimonio" Text='<%# Bind("Patrimonio", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Utilidad Operacional" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblUtilidad" Text='<%# Bind("UtilidadOperacional", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gastos de Intereses" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblGastos" Text='<%# Bind("GastosInteresFinancieros", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Liquidez" ControlStyle-Width="200px" >
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblLiquidez" Text='<%# Bind("Liquidez", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Endeudamiento" ControlStyle-Width="200px" >
                                <ItemTemplate>
                                    <asp:Label ID="lblEndeudamiento" runat="server" 
                                        Text='<%# string.Format("{0:N2}", Eval("Endeudamiento") ) %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Raz&oacute;n de cobertura de inter&eacute;s" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="200px">
                                <ItemTemplate >
                                    <asp:Label ID="lblRazonCoberturaInteres" runat="server" 
                                        Text='<%# string.Format("{0:N2}", Eval("RazonCoberturaInteres") ) %>'></asp:Label>
                                </ItemTemplate>
                                <ControlStyle Width="100px"/>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Patrimonio expresado SMLV" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="200px" >
                                <ItemTemplate>
                                    <asp:Label ID="lblPatrimonioSMLV" runat="server" Text='<%# string.Format("{0:N2}", Eval("PatrimonioSMLV") ) %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Documentos  " ItemStyle-HorizontalAlign ="Center"  >
                                <ItemTemplate>
                                    <asp:ImageButton ID="lnkVerDocumentos" runat="server" cssClass="docgrid"
                                        CommandArgument="<%# Container.DataItemIndex %>"  
                                        ImageUrl="~/Image/btn/list.png" OnClick="lnkVerDocumentos_Click" 
                                        ToolTip="Detalle" />
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <asp:BoundField ControlStyle-Width="200px"  HeaderText=" Aprobada "  DataField="Aprobada"/>
                            <asp:BoundField />
                            <asp:BoundField />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG"/>
                        <EmptyDataTemplate>
                        No se encontraron datos asociados a este proveedor.
                        </EmptyDataTemplate>
                    </asp:GridView>                 
                </td>
            </tr>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlHistorial" Visible = "false">
            <tr>
            <td>
                <h3 class="lbBloque">
                <asp:Label ID="lblTitulo" runat="server" Text="Hist&oacute;rico de observaciones del m&oacute;dulo Financiero"></asp:Label>
                </h3>
            </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="gvDatosFinancieros" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px">
                        <Columns>
                            <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                            <asp:BoundField HeaderText="No. Vigencia" DataField="Anno" />
                            <asp:TemplateField HeaderText="Confirma que los datos coinciden con el documento adjunto y lo aprueba?" HeaderStyle-Width="120px" >
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("ConfirmaYAprueba") ? "Si" : "No") %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />
                            <asp:BoundField HeaderText="Fecha de Observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                        </Columns>                        
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                        No hay datos
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
           </tr>
        </asp:Panel>
     </table>    
     
</asp:Content>
