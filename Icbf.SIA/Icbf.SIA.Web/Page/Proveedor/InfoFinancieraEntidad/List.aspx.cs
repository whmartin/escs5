using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

using Icbf.SIA.Service;
using Icbf.SIA.Entity;

using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de informaci�n financiera del proveedor
/// </summary>
public partial class Page_InfoFinancieraEntidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/InfoFinancieraEntidad";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRuboService = new SIAService();
    int? vIdEntidad = null;

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>     
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (!Page.IsPostBack)
        {
            if (ValidateAccess(Master, PageName, vSolutionPage))
            {            
                CargarDatosIniciales();
                Buscar();
            }
        }
    }


    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoFinancieraEntidad.NroRevision").ToString()))
        {
            NavigateTo("../ValidarProveedor/Revision.aspx");
        }
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {

            vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
             
            gvInfoFinancieraEntidad.DataSource = vProveedorService.ConsultarInfoFinancieraEntidades( vIdEntidad);
            gvInfoFinancieraEntidad.DataBind();
            
            if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoFinancieraEntidad.NroRevision").ToString()))
            {
                pnlHistorial.Visible = true;
                gvDatosFinancieros.DataSource = vProveedorService.ConsultarValidarInfoFinancieraEntidadsResumen(vIdEntidad.Value, null, null);
                gvDatosFinancieros.DataBind();
            }
            else
            {
                gvDatosFinancieros.DataSource = null;
                gvDatosFinancieros.DataBind();
                pnlHistorial.Visible = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (ex.InnerException != null)
                toolBar.MostrarMensajeError(ex.InnerException.Message);
            else
                toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            if (string.IsNullOrEmpty(GetSessionParameter("ValidarInfoFinancieraEntidad.NroRevision").ToString()))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoRetornar -= new ToolBarDelegate(btnRetornar_Click);
            }
            else
            {
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }
            
            gvInfoFinancieraEntidad.PageSize = PageSize();
            gvInfoFinancieraEntidad.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Informaci&oacute;n Financiera Entidad", SolutionPage.List.ToString());

            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();

            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvInfoFinancieraEntidad.DataKeys[rowIndex].Value.ToString();
            string vIdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
            SetSessionParameter("InfoFinancieraEntidad.IdInfoFin", strValue);
            SetSessionParameter("EntidadProvOferente.IdEntidad", vIdEntidad);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvInfoFinancieraEntidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvInfoFinancieraEntidad.SelectedRow);
    }
    
    protected void gvInfoFinancieraEntidad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInfoFinancieraEntidad.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
             
            if (GetSessionParameter("InfoFinancieraEntidad.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("InfoFinancieraEntidad.Eliminado");
           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    /// <summary>
    /// Manejador del evento para el bot�n Ver documentos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkVerDocumentos_Click(object sender, EventArgs e)
    {
        try
        {
            int sDataItemIndex = int.Parse(((ImageButton)sender).CommandArgument);
            string sIdInfoFin = gvInfoFinancieraEntidad.DataKeys[sDataItemIndex]["IdInfoFin"].ToString();
            string sRupRenovado = bool.Parse(gvInfoFinancieraEntidad.DataKeys[sDataItemIndex]["RupRenovado"].ToString()) ? "1" : "0";

            //
            // Ejemplo para validar documentos obligatorios
            //
            InfoFinancieraEntidad vFinancieraEntidad =
                vProveedorService.ConsultarInfoFinancieraEntidad(int.Parse(sIdInfoFin));

            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vFinancieraEntidad.IdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            vProveedorService.ValidarDocumentosObligatorios(vFinancieraEntidad, vTipoSector,vTipoPersona);
            string path = string.Format("'../../../Page/Proveedor/DocFinancieraProv/List.aspx?IdInfoFin={0}&RupRenovado={1}'", sIdInfoFin, sRupRenovado);

             string returnValues = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                                "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                                "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                                "<script>window_showModalDialog(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>";

             ClientScript.RegisterStartupScript(this.GetType(), "myScript", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    /// Obtiene el a�o de vigencia
    /// </summary>
    /// <param name="pIndex"></param>
    /// <returns></returns>
    protected int GetAnioVigencia(int pIndex)
    {
        int pIdVigencia = int.Parse(gvInfoFinancieraEntidad.DataKeys[pIndex]["IdVigencia"].ToString());
        return vRuboService.ConsultarVigencia(pIdVigencia).AcnoVigencia ;
    }
}
