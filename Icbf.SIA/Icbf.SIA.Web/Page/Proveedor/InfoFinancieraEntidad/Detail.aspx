<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_InfoFinancieraEntidad_Detail" %>

<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/ucHistorialRevisiones.ascx" TagName="ucHistorialRevisiones"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdInfoFin" runat="server" />
    <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    <script src="../../../Scripts/Jqfc.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.numeric.js" type="text/javascript"></script>
    <script>
        function pageLoad() {
            $(".decimal").numeric(",");

            $('.currency').blur(function () {
                $('.currency').formatCurrency({ decimalSymbol: ',', digitGroupSymbol: '.' });
            });

            $("input").keypress(function (evt) {
                //Deterime where our character code is coming from within the event
                var charCode = evt.charCode || evt.keyCode;
                if (charCode == 13) { //Enter key's keycode
                    return false;
                }
            });
        }
    </script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Vigencia Fiscal
            </td>
            <td>
                Salario M&iacute;nimo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlIdVigencia" runat="server" OnSelectedIndexChanged="ddlIdVigencia_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSalarioMinimo" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Activo Total
            </td>
            <td>
                Pasivo Total
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtActivoTotal" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPasivoTotal" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Activo Corriente
            </td>
            <td>
                Pasivo Corriente
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtActivoCte" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPasivoCte" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
            </td>
            <td>
                Patrimonio
            </td>
        </tr>
        <tr class="rowA">
            <td>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPatrimonio" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Utilidad operacional
            </td>
            <td>
                Gastos de Inter&eacute;s
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUtilidadOperacional" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtGastosInteresFinancieros" Enabled="false" CssClass="decimal currency"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Observaciones sobre la informaci&oacute;n financiera
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtObservacionesInformacionFinanciera" Enabled="false"
                    TextMode="MultiLine" Style="width: 40%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Indicadores financieros calculados a partir de los datos ingresados anteriormente
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Liquidez (activo corriente/pasivo corriente)
            </td>
            <td>
                Endeudamiento (Pasivo total/activo total)
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtLiquidez" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEndeudamiento" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Raz&oacute;n de cobertura de inter&eacute;s (utilidad operacional/gastos de inter&eacute;s)
            </td>
            <td>
                Patrimonio expresado SMLV (patrimonio/salario m&iacute;nimo)
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtRazonCoberturaInteres" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPatrimonioSMLV" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                Confirma indicadores financieros?
                <asp:CheckBoxList ID="rblConfirmado" runat="server" Enabled="false">
                    <asp:ListItem Text="Si" Value="True"></asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Cuenta con registro &uacute;nico de proponente RUP renovado a la vigencia fiscal
                <asp:RadioButtonList ID="rblRupRenovado" runat="server" Enabled="false">
                    <asp:ListItem Text="Si" Value="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                Estados financieros que adjunte debe tener corte al 31 de diciembre a&ntilde;o inmediatamente
                anterior
            </td>
        </tr>
        <tr class="rowBG">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvDocFinancieraProv" AutoGenerateColumns="False"
                    AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                    CellPadding="0" Height="16px" OnRowCommand="gvDocFinancieraProv_RowCommand" OnPageIndexChanging="gvDocFinancieraProv_PageIndexChanging"
                    OnSelectedIndexChanged="gvDocFinancieraProv_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre del Documento">
                            <ItemTemplate>
                                <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                        <asp:TemplateField HeaderText="Documento">
                            <ItemTemplate>
                                <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorInfoFinanciera_") + "ProveedorInfoFinanciera_".Length) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Observaciones">
                            <ItemTemplate>
                                <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                    TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                    Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                    Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
        <asp:Panel ID="pnlObservacionesOld" runat="server" Visible="false">
            <tr class="rowA">
                <td colspan="2">
                    Observaciones por el validador documental ICBF
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtObservacionesValidadorICBF" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel ID="pnlEstado" runat="server">
            <tr class="rowA">
                <td>
                    Estado Validaci&oacute;n documental
                </td>
                <td>
                    Registrado Por
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:HiddenField ID="hfIDEstadoValidacionDocumental" runat="server"></asp:HiddenField>
                    <asp:TextBox runat="server" ID="txtEstadoValidacionDocumental" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRegistradorPor" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </asp:Panel>
    </table>
    <br />
    <br />
    <table width="90%" align="center">
        <tr>
            <td>
                <uc2:ucHistorialRevisiones ID="ucHistorialRevisiones1" runat="server" style="width: 90%" />
            </td>
        </tr>
    </table>
</asp:Content>
