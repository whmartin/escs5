using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Presentation;

using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;
using Icbf.SIA.Service;

using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición de información financiera del proveedor
/// </summary>
public partial class Page_InfoFinancieraEntidad_Add : GeneralWeb
{
    [Serializable]
    public class DocOperationState
    {
        public DocOperation operation;
        public DocFinancieraProv doc;
        public DocOperationState(DocOperation pOperation, DocFinancieraProv pDoc)
        {
            this.operation = pOperation;
            this.doc = pDoc;
        }
    }

    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRuboService = new SIAService();
    string PageName = "Proveedor/InfoFinancieraEntidad";

    int? vIdEntidad = null;

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        if (!Page.IsPostBack)
        {

            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
            ViewState["DocOperationState"] = new List<DocOperationState>();
        }
        AddAttributoCheck();
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("List.aspx");
    }

    private void AddAttributoCheck()
    {


        foreach (ListItem item in rblConfirmado.Items)
        {
            item.Attributes.Add("onclick", "ValidaActivoCheckbox();");
        }

    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {

            if (string.IsNullOrEmpty(rblConfirmado.SelectedValue) || !Convert.ToBoolean(rblConfirmado.SelectedValue))
            {
                throw new Exception("Debe confirmar los indicadores financieros");
            }

            //Validar los documentos obligatorios (contra la grilla, no contra data)
            ValidarDocumentosObligatorios();

            EjecutarOperacionesDocs();

            int vResultado;
            InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();

            vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());

            vInfoFinancieraEntidad.IdEntidad = vIdEntidad.Value;//Convert.ToInt32(txtIdEntidad.Text);
            vInfoFinancieraEntidad.IdVigencia = Convert.ToInt32(ddlIdVigencia.Text);
            vInfoFinancieraEntidad.ActivoCte = Convert.ToDecimal(txtActivoCte.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.ActivoTotal = Convert.ToDecimal(txtActivoTotal.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.PasivoCte = Convert.ToDecimal(txtPasivoCte.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.PasivoTotal = Convert.ToDecimal(txtPasivoTotal.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.Patrimonio = Convert.ToDecimal(txtPatrimonio.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.GastosInteresFinancieros = Convert.ToDecimal(txtGastosInteresFinancieros.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.UtilidadOperacional = Convert.ToDecimal(txtUtilidadOperacional.Text.Replace(".", "").Replace("$", ""));
            vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros = Convert.ToBoolean(rblConfirmado.SelectedValue);
            vInfoFinancieraEntidad.RupRenovado = Convert.ToBoolean(rblRupRenovado.SelectedValue);

            vInfoFinancieraEntidad.ObservacionesInformacionFinanciera = Convert.ToString(txtObservacionesInformacionFinanciera.Text).Trim();
            vInfoFinancieraEntidad.ObservacionesValidadorICBF = Convert.ToString(txtObservacionesValidadorICBF.Text).Trim();

            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(vIdEntidad));
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            if (Request.QueryString["oP"] == "E")
            {
                vInfoFinancieraEntidad.IdInfoFin = Convert.ToInt32(hfIdInfoFin.Value);
                //
                //TODO, quien cambia el estado??
                //vInfoFinancieraEntidad.EstadoValidacion = Convert.ToInt32(txtEstadoValidacionDocumental.Text);
                //Se consulta el estado de validacion documental
                // Se coloca Estao POR VALIDAR
                vInfoFinancieraEntidad.EstadoValidacion = 6;
                vInfoFinancieraEntidad.Finalizado = false;
                //
                vInfoFinancieraEntidad.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vInfoFinancieraEntidad, this.PageName, vSolutionPage);



                vResultado = vProveedorService.ModificarInfoFinancieraEntidad(vInfoFinancieraEntidad, vTipoPersona, vTipoSector);
            }
            else
            {
                //
                //Es creacion
                //
                vInfoFinancieraEntidad.IdTemporal = ViewState["IdTemporal"].ToString(); //Se usa para hacer el update de los hijos
                vInfoFinancieraEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                vInfoFinancieraEntidad.EstadoValidacion = 1;//Estado Pendiente de Validación, (REGISTRADO)
                vInfoFinancieraEntidad.Finalizado = false;
                InformacionAudioria(vInfoFinancieraEntidad, this.PageName, vSolutionPage);

                vResultado = vProveedorService.InsertarInfoFinancieraEntidad(vInfoFinancieraEntidad, vTipoPersona, vTipoSector);

                //
                //Se limpia el IdTemporal usado para subir documentos
                //
                ViewState["IdTemporal"] = null;


            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)  // Se realizan varias operaciones
            {
                SetSessionParameter("InfoFinancieraEntidad.IdInfoFin", vInfoFinancieraEntidad.IdInfoFin);
                SetSessionParameter("InfoFinancieraEntidad.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.EstablecerTitulos("Informaci&oacute;n Financiera Entidad", SolutionPage.Add.ToString());
            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));
            RemoveSessionParameter("InfoFinancieraEntidad.IdInfoFin");

            lblNota.Visible = true;

            InfoFinancieraEntidad vInfoFinancieraEntidad = new InfoFinancieraEntidad();
            vInfoFinancieraEntidad = vProveedorService.ConsultarInfoFinancieraEntidad(vIdInfoFin);
            hfIdInfoFin.Value = vInfoFinancieraEntidad.IdInfoFin.ToString();

            ddlIdVigencia.SelectedValue = vInfoFinancieraEntidad.IdVigencia.ToString();
            //Logica para traer el mínimo 
            string descripcion ="minimo";
            int anio = int.Parse(ddlIdVigencia.SelectedItem.Text);
            SalarioMinimo salarioMinimo = vOferenteService.ConsultarSalarioMinimo(anio,descripcion);
            txtSalarioMinimo.Text = salarioMinimo.Valor.ToString();

            txtActivoCte.Text = vInfoFinancieraEntidad.ActivoCte.ToString("N2");
            txtActivoTotal.Text = vInfoFinancieraEntidad.ActivoTotal.ToString("N2");
            txtPasivoCte.Text = vInfoFinancieraEntidad.PasivoCte.ToString("N2");
            txtPasivoTotal.Text = vInfoFinancieraEntidad.PasivoTotal.ToString("N2");
            txtUtilidadOperacional.Text = vInfoFinancieraEntidad.UtilidadOperacional.ToString("N2");
            txtPatrimonio.Text = vInfoFinancieraEntidad.Patrimonio.ToString("N2");
            txtGastosInteresFinancieros.Text = vInfoFinancieraEntidad.GastosInteresFinancieros.ToString("N2");

            //Se consulta el estado de validacion documental
            hfIDEstadoValidacionDocumental.Value = vInfoFinancieraEntidad.EstadoValidacion.ToString();
            txtEstadoValidacionDocumental.Text = vProveedorService.ConsultarEstadoValidacionDocumental(vInfoFinancieraEntidad.EstadoValidacion).Descripcion;
            txtRegistradorPor.Text = vInfoFinancieraEntidad.UsuarioCrea;

            txtObservacionesInformacionFinanciera.Text = vInfoFinancieraEntidad.ObservacionesInformacionFinanciera;
            txtObservacionesValidadorICBF.Text = vInfoFinancieraEntidad.ObservacionesValidadorICBF;

            rblConfirmado.SelectedValue = vInfoFinancieraEntidad.ConfirmaIndicadoresFinancieros.ToString();
            rblRupRenovado.SelectedValue = vInfoFinancieraEntidad.RupRenovado.ToString();

            //Datos calculados

            txtLiquidez.Text = vInfoFinancieraEntidad.Liquidez.ToString("N2");
            txtEndeudamiento.Text = vInfoFinancieraEntidad.Endeudamiento.ToString("N2");
            try
            {
                txtPatrimonioSMLV.Text = (vInfoFinancieraEntidad.Patrimonio / salarioMinimo.Valor).ToString("N2");
            }
            catch (DivideByZeroException)
            {
                toolBar.MostrarMensajeError("No es posible divir por cero.");
            }
            txtRazonCoberturaInteres.Text = vInfoFinancieraEntidad.RazonCoberturaInteres.ToString("N2");

            ViewState["docs"] = null;
            BuscarDocumentos();

            //Si existe padre habilita el panel para mostrar los documentos hijos
            panelDocumentos.Enabled = true;

            //Se aplica regla de negocio para permitir editar
            AplicaReglaNegocioEdicion(vInfoFinancieraEntidad.UsuarioCrea, vInfoFinancieraEntidad.EstadoValidacion);


            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInfoFinancieraEntidad.UsuarioCrea, vInfoFinancieraEntidad.FechaCrea, vInfoFinancieraEntidad.UsuarioModifica, vInfoFinancieraEntidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Habilita panel de estado dependiendo del tipo de usuario
    /// </summary>
    /// <param name="usuario"></param>
    /// <param name="IdEstado"></param>
    private void AplicaReglaNegocioEdicion(string usuario, int IdEstado)
    {
        //Se verifica si el usuario es interno o externo y aplica regla

        //if (vOferenteService.ConsultarTerceroProviderUserKey(GetSessionUser().Providerkey).ProviderUserKey != null)
        if (vRuboService.ConsultarUsuario(usuario).Rol.Split(';').Contains("PROVEEDORES"))
        {//Es Externo
            pnEstado.Visible = false;
        }
        else
        {//Es interno
            pnEstado.Visible = true;
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ftActivoCte.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftActivoTotal.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftPasivoCte.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftPasivoTotal.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftPatrimonio.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftGastosInteresFinancieros.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftUtilidadOperacional.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();


            SeguridadService vSeguridadService = new SeguridadService();
            Parametro vParametro = vSeguridadService.ConsultarParametro(6);
            int numfiscal = Convert.ToInt32(vParametro.ValorParametro);
            List<Vigencia> lstVigencia = vRuboService.ConsultarVigenciasSinAnnoActual(null);
            List<Vigencia> lstVigenciaSource = new List<Vigencia>();
            int numVigencias = lstVigencia.Count;
            if (numVigencias > 0)
            {
                for (int i = 0; i < numfiscal; i++)
                {
                    if (i < numVigencias)
                    {
                        Vigencia vVigencia = new Vigencia();
                        vVigencia.IdVigencia = lstVigencia[i].IdVigencia;
                        vVigencia.AcnoVigencia = lstVigencia[i].AcnoVigencia;
                        lstVigenciaSource.Add(vVigencia);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            ddlIdVigencia.DataSource = lstVigenciaSource;
            ddlIdVigencia.DataValueField = "IdVigencia";
            ddlIdVigencia.DataTextField = "AcnoVigencia";
            ddlIdVigencia.DataBind();
            ddlIdVigencia.Items.Insert(0, new ListItem("--seleccione--", "-1"));
            ddlIdVigencia.SelectedIndex = -1;

            //Llenamos las grillas de los documentos

            if (ViewState["IdTemporal"] == null)
            {
                ViewState["IdTemporal"] = DateTime.Now.Ticks.ToString();
            }

            vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(vIdEntidad));
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            List<DocFinancieraProv> lstDocFinancieraProv = vProveedorService.ConsultarDocFinancieraProv_IdInfoFin_Rup(0, "0", vTipoPersona, vTipoSector);
            gvDocFinancieraProv.DataSource = lstDocFinancieraProv;
            gvDocFinancieraProv.DataBind();
            ViewState["docs"] = lstDocFinancieraProv;





        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIdVigencia_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdVigencia.SelectedIndex > 0)
        {
            string descripcion = "minimo";
            int anio = int.Parse(ddlIdVigencia.SelectedItem.Text);
            vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());

            SalarioMinimo salarioMinimo = vOferenteService.ConsultarSalarioMinimo(anio,descripcion);
            txtSalarioMinimo.Text = salarioMinimo.Valor.ToString();
        }
    }

    protected void gvDocFinancieraProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocFinancieraProv.PageIndex = e.NewPageIndex;

        BuscarDocumentos();
    }

    protected void gvDocFinancieraProv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            GridViewRow row = gvDocFinancieraProv.Rows[e.RowIndex];
            int idDocAdjunto = (int)gvDocFinancieraProv.DataKeys[e.RowIndex].Value;
            DocFinancieraProv doc = vProveedorService.ConsultarDocFinancieraProv(idDocAdjunto);
            vProveedorService.EliminarDocFinancieraProv(doc);
            BuscarDocumentos();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void gvDocFinancieraProv_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDocFinancieraProv.EditIndex = e.NewEditIndex;
        BuscarDocumentos();
    }

    protected void gvDocFinancieraProv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDocFinancieraProv.EditIndex = -1;
        BuscarDocumentos();
    }

    protected void gvDocFinancieraProv_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow row = gvDocFinancieraProv.Rows[e.RowIndex];

            int vIdDocAdjunto = int.Parse(gvDocFinancieraProv.DataKeys[e.RowIndex]["IdDocAdjunto"].ToString());
            int vMaxPermitidoKB = int.Parse(gvDocFinancieraProv.DataKeys[e.RowIndex]["MaxPermitidoKB"].ToString());
            string vExtensionesPermitidas = gvDocFinancieraProv.DataKeys[e.RowIndex]["ExtensionesPermitidas"].ToString();

            DocFinancieraProv doc = new DocFinancieraProv();
            if (vIdDocAdjunto > 0)
            {
                doc = vProveedorService.ConsultarDocFinancieraProv(vIdDocAdjunto);
            }
            else
            {
                if (!string.IsNullOrEmpty(hfIdInfoFin.Value))
                {
                    int vIdInfoFin = Convert.ToInt32(hfIdInfoFin.Value);
                    doc.IdInfoFin = vIdInfoFin;
                }
                else
                {
                    //Se asocia un Id temporal 
                    doc.IdTemporal = ViewState["IdTemporal"].ToString();
                }
                doc.NombreDocumento = ((Label)(row.Cells[0].FindControl("lblNombreDocumento"))).Text.Trim();
                doc.IdTipoDocumento = int.Parse(gvDocFinancieraProv.DataKeys[e.RowIndex]["IdTipoDocumento"].ToString());
                doc.FechaCrea = DateTime.Now;
                doc.UsuarioCrea = GetSessionUser().NombreUsuario;
            }
            //doc.LinkDocumento = ((TextBox)(row.Cells[3].FindControl("txtLinkDocumento"))).Text;        
            FileUpload fuDocumento = ((FileUpload)(row.Cells[1].FindControl("fuDocumento")));

            //Se envia por ftp
            doc.LinkDocumento = SubirArchivo(fuDocumento, vMaxPermitidoKB, vExtensionesPermitidas);

            doc.Observaciones = ((TextBox)(row.Cells[2].FindControl("txtObservaciones"))).Text;
            doc.FechaModifica = DateTime.Now;
            doc.UsuarioModifica = GetSessionUser().NombreUsuario;


            InformacionAudioria(doc, this.PageName, vSolutionPage);

            List<DocOperationState> lstDocOperationState = (List<DocOperationState>)ViewState["DocOperationState"];

            if (vIdDocAdjunto > 0)//Existe
            {
                //vProveedorService.ModificarDocFinancieraProv(doc);
                lstDocOperationState.Add(new DocOperationState(DocOperation.Update, doc));
            }
            else
            {
                //vProveedorService.InsertarDocFinancieraProv(doc);
                lstDocOperationState.Add(new DocOperationState(DocOperation.Add, doc));
            }

            gvDocFinancieraProv.EditIndex = -1;

            //BuscarDocumentos();

            AddDocListInterfaz(doc);

            ViewState["DocOperationState"] = lstDocOperationState;

            toolBar.LipiarMensajeError();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }


    /// <summary>
    /// Comando de renglón para la grilla DocFinancieraProv
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocFinancieraProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga archivo al servidor
    /// </summary>
    /// <param name="fuArchivo"></param>
    /// <param name="pMaxPermitidoKB"></param>
    /// <param name="pExtensionesPermitidas"></param>
    /// <returns></returns>
    protected string SubirArchivo(FileUpload fuArchivo, int pMaxPermitidoKB, string pExtensionesPermitidas)
    {

        if (!fuArchivo.HasFile)
        {
            throw new Exception("Debe seleccionar un archivo.");
        }


        int idformatoArchivo;

        string filename = Path.GetFileName(fuArchivo.FileName);

        String NombreArchivoOri = filename;

        string tipoArchivo = "";
        foreach (string sExt in pExtensionesPermitidas.Split(','))
        {
            if (filename.Substring(filename.LastIndexOf(".") + 1).ToUpper() == sExt.ToUpper())
            {
                tipoArchivo = string.Format("General.archivo{0}", sExt.ToUpper());
                break;
            }
        }

        if (tipoArchivo == "")
        {
            throw new Exception(string.Format("Solo se permiten los siguientes archivos: {0}", pExtensionesPermitidas));
        }



        if (fuArchivo.PostedFile.ContentLength / 1024 > pMaxPermitidoKB)
        {
            throw new Exception(string.Format("Solo se permiten archivos inferiores a {0} KB", (pMaxPermitidoKB)));
        }

        SIAService vRUBOService = new SIAService();

        List<FormatoArchivo> vFormatoArchivo = vRUBOService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

        if (vFormatoArchivo.Count == 0)
        {
            throw new Exception("No se ha parametrizado la extensión del archivo a cargar");
        }

        idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

        //Creación del Nombre del archivo
        string NombreArchivo = filename.Substring(0, filename.IndexOf("."));

        NombreArchivo = vRUBOService.QuitarEspacios(NombreArchivo);

        NombreArchivo = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString()
            + "_ProveedorInfoFinanciera_" + NombreArchivo + filename.Substring(filename.LastIndexOf("."));

        //NombreArchivo = Guid.NewGuid().ToString() + NombreArchivo;
        string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

        //Guardado del archivo en el servidor
        HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;
        Boolean ArchivoSubido = vRUBOService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

        if (ArchivoSubido)
        {
            return NombreArchivo;
        }
        else
        {
            throw new Exception("No se pudo subir el archivo al ftp.");
        }


        return null;
    }

    /// <summary>
    /// Busca documentos
    /// </summary>
    private void BuscarDocumentos()
    {
        try
        {
            if (ViewState["docs"] == null)
            {
                if (!String.IsNullOrEmpty(hfIdInfoFin.Value))
                {
                    int vIdInfoFin = Convert.ToInt32(hfIdInfoFin.Value);
                    string vRupRenovado = bool.Parse(rblRupRenovado.SelectedValue) ? "1" : "0";

                    vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
                    Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente =
                        vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(vIdEntidad));
                    string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
                    Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                    string vTipoPersona = vTercero.IdTipoPersona.ToString();

                    List<DocFinancieraProv> lstDocFinancieraProv =
                        vProveedorService.ConsultarDocFinancieraProv_IdInfoFin_Rup(vIdInfoFin, vRupRenovado,
                                                                                   vTipoPersona, vTipoSector);
                    gvDocFinancieraProv.DataSource = lstDocFinancieraProv;
                    gvDocFinancieraProv.DataBind();
                    ViewState["docs"] = lstDocFinancieraProv;

                }
                else
                {
                    string vRupRenovado = bool.Parse(rblRupRenovado.SelectedValue) ? "1" : "0";
                    string vIdTemporal = ViewState["IdTemporal"].ToString();

                    vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
                    Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente =
                        vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(vIdEntidad));
                    string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
                    Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                    string vTipoPersona = vTercero.IdTipoPersona.ToString();

                    List<DocFinancieraProv> lstDocFinancieraProv =
                        vProveedorService.ConsultarDocFinancieraProv_IdTemporal_Rup(vIdTemporal, vRupRenovado,
                                                                                    vTipoPersona, vTipoSector);
                    gvDocFinancieraProv.DataSource = lstDocFinancieraProv;
                    gvDocFinancieraProv.DataBind();
                    ViewState["docs"] = lstDocFinancieraProv;
                }
            }
            else
            {
                List<DocFinancieraProv> lstDocFinancieraProv = (List<DocFinancieraProv>)ViewState["docs"];
                gvDocFinancieraProv.DataSource = lstDocFinancieraProv;
                gvDocFinancieraProv.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocFinancieraProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocFinancieraProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void rblRupRenovado_SelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        //Se eliminan los documentos
        EliminarDocumentos();
        ViewState["docs"] = null;
        BuscarDocumentos();
        LimpiarDocsInterfaz();


    }

    /// <summary>
    /// Elimina documentos
    /// </summary>
    protected void EliminarDocumentos()
    {

        try
        {
            List<DocOperationState> lstDocOperationState = (List<DocOperationState>)ViewState["DocOperationState"];
            foreach (GridViewRow row in gvDocFinancieraProv.Rows)
            {
                Label lblDocAdjunto = (Label)row.Cells[0].Controls[1];
                int idDocAdjunto = Convert.ToInt32(lblDocAdjunto.Text);
                if (idDocAdjunto > 0)
                {
                    DocFinancieraProv doc = vProveedorService.ConsultarDocFinancieraProv(idDocAdjunto);


                    //vProveedorService.EliminarDocFinancieraProv(doc);
                    lstDocOperationState.Add(new DocOperationState(DocOperation.Delete, doc));
                }

            }
            ViewState["DocOperationState"] = lstDocOperationState;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Limpia documentos de la intefaz
    /// </summary>
    private void LimpiarDocsInterfaz()
    {
        List<DocFinancieraProv> lstDocFinancieraProv = (List<DocFinancieraProv>)ViewState["docs"];
        foreach (DocFinancieraProv vDocFinancieraProv in lstDocFinancieraProv)
        {
            vDocFinancieraProv.IdDocAdjunto = 0;
            vDocFinancieraProv.IdInfoFin = 0;
            vDocFinancieraProv.LinkDocumento = "";
            vDocFinancieraProv.Observaciones = "";
            vDocFinancieraProv.UsuarioCrea = "";
            vDocFinancieraProv.UsuarioModifica = "";
            vDocFinancieraProv.IdTemporal = "";
        }
        gvDocFinancieraProv.DataSource = lstDocFinancieraProv;
        gvDocFinancieraProv.DataBind();
        ViewState["docs"] = lstDocFinancieraProv;
    }

    /// <summary>
    /// Ejecuta Operaciones sobre documentos
    /// </summary>
    private void EjecutarOperacionesDocs()
    {
        List<DocOperationState> lstDocOperationState = (List<DocOperationState>)ViewState["DocOperationState"];
        foreach (DocOperationState docOperationState in lstDocOperationState)
        {
            switch (docOperationState.operation)
            {
                case DocOperation.Add:
                    vProveedorService.InsertarDocFinancieraProv(docOperationState.doc);
                    break;

                case DocOperation.Update:
                    vProveedorService.ModificarDocFinancieraProv(docOperationState.doc);
                    break;

                case DocOperation.Delete:
                    vProveedorService.EliminarDocFinancieraProv(docOperationState.doc);
                    break;
            }
        }
    }

    /// <summary>
    /// Adiciona un documento a la lista
    /// </summary>
    /// <param name="doc"></param>
    private void AddDocListInterfaz(DocFinancieraProv doc)
    {
        List<DocFinancieraProv> lstDocFinancieraProv = (List<DocFinancieraProv>)ViewState["docs"];
        DocFinancieraProv docActual = lstDocFinancieraProv.Find(docFin => docFin.NombreDocumento.Equals(doc.NombreDocumento));

        docActual.FechaCrea = doc.FechaCrea;
        docActual.FechaModifica = doc.FechaModifica;
        docActual.IdDocAdjunto = doc.IdDocAdjunto;
        docActual.IdInfoFin = doc.IdInfoFin;
        docActual.IdTemporal = doc.IdTemporal;
        docActual.LinkDocumento = doc.LinkDocumento;
        docActual.NombreDocumento = doc.NombreDocumento;
        docActual.Observaciones = doc.Observaciones;
        docActual.UsuarioCrea = doc.UsuarioCrea;
        docActual.UsuarioModifica = doc.UsuarioModifica;

        gvDocFinancieraProv.DataSource = lstDocFinancieraProv;
        gvDocFinancieraProv.DataBind();

        ViewState["docs"] = lstDocFinancieraProv;
    }

    /// <summary>
    /// Valida documentos obligatorios
    /// </summary>
    private void ValidarDocumentosObligatorios()
    {
        List<DocFinancieraProv> lstDocFinancieraProvGrilla = (List<DocFinancieraProv>)ViewState["docs"];

        string mensaje = string.Empty;
        foreach (DocFinancieraProv doc in lstDocFinancieraProvGrilla)
        {
            if (string.IsNullOrEmpty(doc.LinkDocumento))
            {
                mensaje += (string.Format("Falta adjuntar el documento {0}. ", doc.NombreDocumento));
            }
        }

        if (!string.IsNullOrEmpty(mensaje))
        {
            throw new Exception(mensaje);
        }
    }
}
