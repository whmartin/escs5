<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_EstadoDatosBasicos_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdEstadoDatosBasicos" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Descripci&oacute;n Estado Datos B&aacute;sicos *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
