using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página que despliega el detalle del registro de estados básicos
/// </summary>
public partial class Page_EstadoDatosBasicos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/EstadoDatosBasicos";
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("EstadoDatosBasicos.IdEstadoDatosBasicos", hfIdEstadoDatosBasicos.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdEstadoDatosBasicos = Convert.ToInt32(GetSessionParameter("EstadoDatosBasicos.IdEstadoDatosBasicos"));
            RemoveSessionParameter("EstadoDatosBasicos.IdEstadoDatosBasicos");

            if (GetSessionParameter("EstadoDatosBasicos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("EstadoDatosBasicos.Guardado");


            EstadoDatosBasicos vEstadoDatosBasicos = new EstadoDatosBasicos();
            vEstadoDatosBasicos = vProveedorService.ConsultarEstadoDatosBasicos(vIdEstadoDatosBasicos);
            hfIdEstadoDatosBasicos.Value = vEstadoDatosBasicos.IdEstadoDatosBasicos.ToString();
            txtDescripcion.Text = vEstadoDatosBasicos.Descripcion;
            rblEstado.SelectedValue = Convert.ToString(Convert.ToInt32(vEstadoDatosBasicos.Estado));
            ObtenerAuditoria(PageName, hfIdEstadoDatosBasicos.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vEstadoDatosBasicos.UsuarioCrea, vEstadoDatosBasicos.FechaCrea, vEstadoDatosBasicos.UsuarioModifica, vEstadoDatosBasicos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdEstadoDatosBasicos = Convert.ToInt32(hfIdEstadoDatosBasicos.Value);

            EstadoDatosBasicos vEstadoDatosBasicos = new EstadoDatosBasicos();
            vEstadoDatosBasicos = vProveedorService.ConsultarEstadoDatosBasicos(vIdEstadoDatosBasicos);
            InformacionAudioria(vEstadoDatosBasicos, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarEstadoDatosBasicos(vEstadoDatosBasicos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("EstadoDatosBasicos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.EstablecerTitulos("Estado Datos B&aacute;sicos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador del evento click para el botón retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/" + PageName + "/List.aspx");
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
