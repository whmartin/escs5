<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_ValidarProveedores_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDocumento").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td width="50%">
                    Estado
                    <br />
                </td>
                <td>
                    Tipo de persona o asociación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlEstado" runat="server" AutoPostBack="false" Width="40%">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged"
                        Width="40%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Tipo de identificación
                </td>
                <td>
                    Número de identificación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" Width="40%"  OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtIdentificacion" runat="server" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                    <%--<Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" FilterType="Numbers"
                        TargetControlID="txtIdentificacion" ValidChars="/1234567890" />--%>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:Label ID="LblRazonSocial" runat="server" Text="Nombres Apellidos / Razón Social"
                        Visible="True"></asp:Label>
                </td>
                <td>
                    Actividad CIIU Principal
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TxtRazonSocial" runat="server" Visible="True" MaxLength="100"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="TxtActividadCiiuPrin" Enabled="False" ViewStateMode="Enabled"></asp:TextBox>
                    <asp:Image ID="imgActividadCiiuPrin" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetCiiuPrin()" Style="cursor: hand" ToolTip="Buscar" Visible="True" />
                    <asp:LinkButton ID="btnLimpiarCiiuprin" runat="server" CssClass="vinculo" OnClientClick="LimpiarCiiuPrin()">Limpiar</asp:LinkButton>
                    <asp:HiddenField runat="server" ID="hfIdActividadCiiuPrin"></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="hfDesActividadCiiuPrin"></asp:HiddenField>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Departamento Ubicación
                </td>
                <td>
                    Municipio Ubicación
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlDepartamentoDireccionComercial" runat="server" Width="40%"
                        AutoPostBack="True" OnSelectedIndexChanged="DdlDepartamentoDireccionComercialSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlMunicipioDireccionComercial" runat="server" Width="40%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:Label ID="lblSector" runat="server" Text="Sector"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblregimentrib" runat="server" Text="Régimen Tributario"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList ID="ddlSector" runat="server" Width="40%">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlRegimenTributario" runat="server" Width="40%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvEntidadProvOferente" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdEntidad,Proveedor"
                        AllowSorting="true" CellPadding="0" Height="16px" OnPageIndexChanging="GvEntidadProvOferentePageIndexChanging"
                        OnSorting="GvEntidadProvOferentePage_Sorting" OnSelectedIndexChanged="GvEntidadProvOferenteSelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo de Identificación" DataField="TipoIdentificacion"
                                SortExpression="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número de identificación" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <asp:TemplateField HeaderText="Nombres Apellidos / Razón Social" ItemStyle-HorizontalAlign="Center"
                                SortExpression="Proveedor">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Proveedor")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Actividad CIIU Principal" DataField="ActividadCiiuPrincipal" />
                            <asp:BoundField HeaderText="Sector" DataField="SectorEntidad" />
                            <asp:BoundField HeaderText="Régimen Tributario" DataField="RegimenTributario" />
                            <asp:BoundField HeaderText="Departamento Ubicación" DataField="NombreDepartamento" />
                            <asp:BoundField HeaderText="Municipio Ubicación" DataField="NombreMunicipio" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoProveedor" />
                            <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaCrea" DataFormatString="{0:dd/MM/yyyy}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                            Esta consulta no tiene resultados en proveedores.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function LimpiarCiiuPrin() {
            document.getElementById('<%= TxtActividadCiiuPrin.ClientID %>').value = "";
            document.getElementById('<%= hfIdActividadCiiuPrin.ClientID %>').value = null;
            document.getElementById('<%= hfDesActividadCiiuPrin.ClientID %>').value = null;
        }

        function GetCiiuPrin() {
            window_showModalDialog('../../../Page/Proveedor/GestionProveedores/ConsultarCiiu.aspx', setReturnGetCiiuPrin, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetCiiuPrin(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 2) {
                    document.getElementById('<%= TxtActividadCiiuPrin.ClientID %>').value = retLupa[2];
                    document.getElementById('<%= hfDesActividadCiiuPrin.ClientID %>').value = retLupa[2];
                    document.getElementById('<%= hfIdActividadCiiuPrin.ClientID %>').value = retLupa[0];
                }
            }
        }
    </script>
</asp:Content>
