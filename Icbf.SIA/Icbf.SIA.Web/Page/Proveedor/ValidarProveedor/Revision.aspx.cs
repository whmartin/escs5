﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using Icbf.Seguridad.Service;

/// <summary>
/// Página para ingresar los resultados de revisón de los diferentes módulos
/// </summary>
public partial class Page_Proveedor_ValidarProveedor_Revision : GeneralWeb
{
    masterPrincipal toolBar;
    bool _sorted = false;
    string PageName = "Proveedor/ValidarProveedor";
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Buscar();
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
        ucDatosProveedor1.IdEntidad = idEntidad;

        if (GetSessionParameter("ValidarProveedor.ObservacionGuardada").ToString() == "1")
            toolBar.MostrarMensajeGuardado("Observación guardada exitosamente");
        RemoveSessionParameter("ValidarProveedor.ObservacionGuardada");

        List<ValidarInfo> listaValidarInfo = vProveedorService.ConsultarValidarModulosResumen(Convert.ToInt32(idEntidad));
        gvRevisiones.DataSource = listaValidarInfo;
        gvRevisiones.DataBind();

        MostrarOcultarBotones(listaValidarInfo);
    }

    /// <summary>
    /// Valida Modulo resumen
    /// </summary>
    /// <param name="pIdEntidad"></param>
    /// <returns></returns>
    public List<ValidarInfo> ConsultarValidarModulosResumen(Int32 pIdEntidad)
    {
        try
        {
            List<ValidarInfo> lista = new List<ValidarInfo>();
            ValidarInfo info = new ValidarInfo();
            info.NroRevision = 1;
            info.Componente = "Datos básicos";
            info.ConfirmaYAprueba = true;
            lista.Add(info);

            ValidarInfo info2 = new ValidarInfo();
            info2.NroRevision = 1;
            info2.Componente = "Financieros";
            info2.ConfirmaYAprueba = true;
            lista.Add(info2);


            ValidarInfo info3 = new ValidarInfo();
            info3.NroRevision = 1;
            info3.Componente = "Experiencias";
            info3.ConfirmaYAprueba = true;
            lista.Add(info3);

            return lista;
        }
        catch (UserInterfaceException)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    /// <summary>
    /// Mostrar u ocultar botones
    /// </summary>
    /// <param name="listaValidarInfo"></param>
    private void MostrarOcultarBotones(List<ValidarInfo> listaValidarInfo)
    {
        btnFinalizar.Visible = true;
        int cantFinalizados = 0;
        int cantSinInfo = 0;
        int cantNOs = 0;
        int cantAprob = 0;

        foreach(ValidarInfo info in listaValidarInfo)
        {
            if (info.iConfirmaYAprueba == -2)
            {
                cantSinInfo++;
            }
            if (info.iConfirmaYAprueba >= 0 && info.Finalizado)
            {
                cantFinalizados++;                
            }
            if (info.iConfirmaYAprueba==0 && info.CorreoEnviado==0 /*&& info.Finalizado*/)
            {
                cantNOs++;
            }
            if (info.iConfirmaYAprueba == 1)
            {
                cantAprob++;
            }
        }

        int cantElementos = listaValidarInfo.Count - cantSinInfo;

        if (cantFinalizados == listaValidarInfo.Count)
        {
            btnFinalizar.Visible = false ;
        }
        else
        {
            btnFinalizar.Visible = true;
        }               
        if (cantNOs >= 1 && GetSessionParameter("Revision.Finalizada.ListoParaEnviarMail").ToString().Equals("1"))
        {
            btnEnviarComunicacion.Visible = true;            
        }
        else
        {
            btnEnviarComunicacion.Visible = false;
        }
        if (cantElementos == cantAprob)
        {
            btnFinalizar.Enabled = false;
            int pidEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            FinalizaModulos(pidEntidad);
            List<ValidarInfo> listaValidar = vProveedorService.ConsultarValidarModulosResumen(pidEntidad);
            gvRevisiones.DataSource = listaValidar;
            gvRevisiones.DataBind();
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            gvRevisiones.PageSize = PageSize();
            gvRevisiones.EmptyDataText = EmptyDataText();
            toolBar.EstablecerTitulos("Lista Revisiones Proveedores ", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Manejador del evento click para el botón retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Manejador del evento click para un botón Histórico
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnHistorico_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
        Int32 iIdEntidad = Convert.ToInt32(idEntidad);
        pnlHistorial.Visible = true;
        gvDatosBasicos.DataSource = vProveedorService.ConsultarValidarInfoDatosBasicosEntidadsResumen(iIdEntidad, null, null);
        gvDatosBasicos.DataBind();
        Icbf.Proveedor.Entity.EntidadProvOferente entidadProveedor =
        vProveedorService.ConsultarEntidadProvOferente(iIdEntidad);
        Tercero tercero = vProveedorService.ConsultarTercero(entidadProveedor.IdTercero);
        int IdTipoPersona = int.Parse(tercero.IdTipoPersona.ToString());
        if (IdTipoPersona == 1 || IdTipoPersona == 2)
        {
            gvDatosFinancieros.DataSource = vProveedorService.ConsultarValidarInfoFinancieraEntidadsResumen(iIdEntidad, null, null);
            gvDatosFinancieros.DataBind();
            gvExperiencia.DataSource = vProveedorService.ConsultarValidarInfoExperienciaEntidadsResumen(iIdEntidad, null, null);
            gvExperiencia.DataBind();
            TabPanel1.Visible = true;
            TabPanel2.Visible = true;
            tpIntegrantes.Visible = false;
        }
        else
        {
            gvIntegrantes.DataSource = vProveedorService.ConsultarValidarInfoIntegrantesEntidadsResumen(iIdEntidad);
            gvIntegrantes.DataBind();
            TabPanel1.Visible = false;
            TabPanel2.Visible = false;
            tpIntegrantes.Visible = true;
        }

    }
    
    /// <summary>
    /// Manejador del evento para un botón EnciarComunicación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEnviarComunicacion_Click(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();
            pnlHistorial.Visible = false;

            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
            Int32 iIdEntidad = Convert.ToInt32(idEntidad);
            var vSeguridadService = new SeguridadService();
            string pAsunto = vSeguridadService.ConsultarParametro(27) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(27).ValorParametro;
            int env = vProveedorService.EnviarMailsInconsistenciasProveedor(iIdEntidad, pAsunto);
            //string []valoresCorreo = vProveedorService.EstructurarMailsInconsistenciasProveedor(int.Parse(idEntidad));
            //Validar si ya fue enviada la comunicación
            toolBar.MostrarMensajeGuardado("La comunicación fue enviada exitosamente.");
            RemoveSessionParameter("Revision.Finalizada.ListoParaEnviarMail");
            btnEnviarComunicacion.Visible = false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
   
    /// <summary>
    /// Manejador del evento click para un botón Finalizar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnFinalizar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        pnlHistorial.Visible = false;
        String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");        
        int retorno = vProveedorService.FinalizarRevision(Convert.ToInt32(idEntidad));
        if (retorno >= 1)
        {
            SetSessionParameter("Revision.Finalizada", "1");
            SetSessionParameter("Revision.Finalizada.ListoParaEnviarMail", "1");
            //NavigateTo(SolutionPage.List);
            toolBar.MostrarMensajeGuardado("Revisión finalizada correctamente");
            Buscar();
        }
        else
        {
            toolBar.MostrarMensajeError("No hay registros para finalizar.");
        }
    }

    protected void gvRevisiones_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRevisiones.SelectedRow);
    }

    /// <summary>
    /// Validación de datos básicos
    /// </summary>
    /// <param name="Componente"></param>
    /// <returns></returns>
    public bool DatosBasicosEnValidacion(string Componente)
    {
        if (Componente.Equals("Datos Básicos", StringComparison.OrdinalIgnoreCase))
        {
            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(idEntidad));
            int idEstadoEnValidacion = 5;
            
            if (vEntidadProvOferente.IdEstado == idEstadoEnValidacion)  //Solo si está en estado En Validación se puede devolver a Registrado
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Comando de renglón para una grilla Revisiones
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvRevisiones_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {

            if(e.CommandName.Equals("Liberar"))
            {
                String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
                
            
                    if (e.CommandArgument.ToString().Equals("Datos Básicos"))
                    {
                        Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
                        vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(idEntidad));
                        vEntidadProvOferente.IdEstado = 1;// EstadoPorValidar; 
                        vEntidadProvOferente.UsuarioModifica = GetSessionUser().NombreUsuario;
                        vEntidadProvOferente.FechaModifica = DateTime.Now;
                        vEntidadProvOferente.Finalizado = false;

                        int resultado = vProveedorService.ModificarEntidadProvOferente_EstadoDocumental(vEntidadProvOferente);
                        Buscar();

                        if (resultado >= 1)
                        {
                            toolBar.MostrarMensajeGuardado("Se ha cambiado Datos Básicos al estado Por Validar");

                        }
                        else
                        {
                            toolBar.MostrarMensajeError("No pudo cambiarse Datos Básicos al estado Por Validar");
                        }

                    }
                    if (e.CommandArgument.ToString().Equals("Financiera"))
                    {
                        InfoFinancieraEntidad vInfoFinanciera = new InfoFinancieraEntidad();
                        vInfoFinanciera.IdEntidad = Convert.ToInt32(idEntidad);
                        vInfoFinanciera.EstadoValidacion = 6;// Pendiente de Validación; 
                        vInfoFinanciera.UsuarioModifica = GetSessionUser().NombreUsuario;
                        vInfoFinanciera.FechaModifica = DateTime.Now;
                        vInfoFinanciera.Finalizado = false;

                        int resultado = vProveedorService.ModificarInfoFinancieraEntidad_Liberar(vInfoFinanciera);
                        Buscar();

                        if (resultado >= 1)
                        {
                            toolBar.MostrarMensajeGuardado("Se han cambiado todas las vigencias al estado Por Validar");
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("No pudo cambiarse vigencias al estado Por Validar");
                        }

                    }
                    if (e.CommandArgument.ToString().Equals("Experiencias"))
                    {
                        InfoExperienciaEntidad vInfoExperiencia = new InfoExperienciaEntidad();
                        vInfoExperiencia.EstadoDocumental = 6;// Pendiente de Validación; 
                        vInfoExperiencia.UsuarioModifica = GetSessionUser().NombreUsuario;
                        vInfoExperiencia.FechaModifica = DateTime.Now;
                        vInfoExperiencia.Finalizado = false;
                        vInfoExperiencia.IdEntidad = Convert.ToInt32(idEntidad);

                        int resultado = vProveedorService.ModificarInfoExperienciaEntidad_Liberar(vInfoExperiencia);
                        Buscar();

                        if (resultado >= 1)
                        {
                            toolBar.MostrarMensajeGuardado("Se han cambiado todas las experiencias al estado Por Validar");

                        }
                        else
                        {
                            toolBar.MostrarMensajeError("No pudo cambiarse experiencias al estado Por Validar");
                        }

                    }
                    if (e.CommandArgument.ToString().Equals("Integrantes"))
                    {
                        Icbf.Proveedor.Entity.ValidacionIntegrantesEntidad vValidacionIntegrantes = new Icbf.Proveedor.Entity.ValidacionIntegrantesEntidad();
                        vValidacionIntegrantes = vProveedorService.Consultar_ValidacionIntegrantesEntidad(Convert.ToInt32(idEntidad));

                        vValidacionIntegrantes.IdEstadoValidacionIntegrantes = 2;// EstadoPorValidar; 
                        vValidacionIntegrantes.UsuarioModifica = GetSessionUser().NombreUsuario;
                        vValidacionIntegrantes.FechaModifica = DateTime.Now;
                        vValidacionIntegrantes.Finalizado = false;

                        int resultado = vProveedorService.ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(vValidacionIntegrantes);
                        Buscar();

                        if (resultado >= 1)
                        {
                            toolBar.MostrarMensajeGuardado("Se han cambiado todos los integrantes al estado Por Validar");

                        }
                        else
                        {
                            toolBar.MostrarMensajeError("No pudo cambiarse integrantes al estado Por Validar");
                        }

                    }
                    
            }           

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {

            int rowIndex = pRow.RowIndex;
            int edoConfimacion = 5;
            int NroRevision = Convert.ToInt16(gvRevisiones.DataKeys[rowIndex].Values[0].ToString());
            string Componente = gvRevisiones.DataKeys[rowIndex].Values[1].ToString();

            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");

            List<ValidarInfo> listaValidarInfo = vProveedorService.ConsultarValidarModulosResumen(Convert.ToInt32(idEntidad));
            
            
            if(Componente.Equals("Datos Básicos"))
            {

                var vDatosBasicos = from vDatos in listaValidarInfo
                                    where vDatos.Componente == "Datos Básicos"
                                    select vDatos;
                foreach (var item in vDatosBasicos)
                {
                    edoConfimacion = item.iConfirmaYAprueba;
                }
                
                //Se utiliza en las páginas para validar el módulo 
                SetSessionParameter("ValidarProveedor.NroRevision", NroRevision);
                NavigateTo("~/Page/Proveedor/GestionProveedores/DetailDatosBasicos.aspx");
            }
            if(Componente.Equals("Financiera"))
            {
                var vDatosBasicos = from vDatos in listaValidarInfo
                                    where vDatos.Componente == "Financiera"
                                    select vDatos;
                foreach (var item in vDatosBasicos)
                {
                    edoConfimacion = item.iConfirmaYAprueba;
                }

                if (edoConfimacion != -2)
                {
                    SetSessionParameter("ValidarInfoFinancieraEntidad.NroRevision", NroRevision);
                    NavigateTo("~/Page/Proveedor/InfoFinancieraEntidad/List.aspx");
                }
            }
            if(Componente.Equals("Experiencias"))
            {
                var vDatosBasicos = from vDatos in listaValidarInfo
                                    where vDatos.Componente == "Experiencias"
                                    select vDatos;
                foreach (var item in vDatosBasicos)
                {
                    edoConfimacion = item.iConfirmaYAprueba;
                }

                if (edoConfimacion != -2)
                {
                    SetSessionParameter("ValidarInfoExperienciaEntidad.NroRevision", NroRevision);
                    NavigateTo("~/Page/Proveedor/InfoExperienciaEntidad/List.aspx");
                }
            }

            if (Componente.Equals("Integrantes"))
            {
                var vDatosBasicos = from vDatos in listaValidarInfo
                                    where vDatos.Componente == "Integrantes"
                                    select vDatos;
                foreach (var item in vDatosBasicos)
                {
                    edoConfimacion = item.iConfirmaYAprueba;
                }

                if (edoConfimacion != -2)
                {
                    SetSessionParameter("ValidarInfoIntegrantesEntidad.NroRevision", NroRevision);
                    NavigateTo("~/Page/Proveedor/ValidarIntegrantes/Detail.aspx");
                }
            }
                        
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void FinalizaModulos(int vIdEntidad)
    {
        try
        {
            int resultadoDB = vProveedorService.FinalizaEntidadProvOferente(vIdEntidad);//FinalizaDatosBasicos
            int resultadoIF = vProveedorService.FinalizaInfoFinancieraEnidad(vIdEntidad);
            int resultadoIE = vProveedorService.FinalizaInfoExperienciaEntidad(vIdEntidad);
            int resultadoII = vProveedorService.FinalizaValidacionIntegrantesEntidad(vIdEntidad);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDatosBasicos_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDatosBasicos.PageIndex = e.NewPageIndex;
        Int32 iIdEntidad = Convert.ToInt32((String)GetSessionParameter("EntidadProvOferente.IdEntidad"));
        gvDatosBasicos.DataSource = vProveedorService.ConsultarValidarInfoDatosBasicosEntidadsResumen(iIdEntidad, null, null);
        gvDatosBasicos.DataBind();
    }

    
    protected void gvDatosFinancieros_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDatosFinancieros.PageIndex = e.NewPageIndex;
        Int32 iIdEntidad = Convert.ToInt32((String)GetSessionParameter("EntidadProvOferente.IdEntidad"));
        gvDatosFinancieros.DataSource = vProveedorService.ConsultarValidarInfoFinancieraEntidadsResumen(iIdEntidad, null, null);
        gvDatosFinancieros.DataBind();
    }

    protected void gvExperiencia_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvExperiencia.PageIndex = e.NewPageIndex;
        Int32 iIdEntidad = Convert.ToInt32((String)GetSessionParameter("EntidadProvOferente.IdEntidad"));
        gvExperiencia.DataSource = vProveedorService.ConsultarValidarInfoExperienciaEntidadsResumen(iIdEntidad, null, null);
        gvExperiencia.DataBind();
    }

    protected void gvIntegrantes_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvIntegrantes.PageIndex = e.NewPageIndex;
        Int32 iIdEntidad = Convert.ToInt32((String)GetSessionParameter("EntidadProvOferente.IdEntidad"));
        gvIntegrantes.DataSource = vProveedorService.ConsultarValidarInfoIntegrantesEntidadsResumen(iIdEntidad);
        gvIntegrantes.DataBind();
    }




}
