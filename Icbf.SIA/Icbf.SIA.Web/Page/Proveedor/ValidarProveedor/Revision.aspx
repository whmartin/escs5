﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Revision.aspx.cs" Inherits="Page_Proveedor_ValidarProveedor_Revision"
    MasterPageFile="~/General/General/Master/main2.master" %>

<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <link href="../../../Styles/TabContainer.css" rel="stylesheet" type="text/css" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <h3>
                    Información de componentes validados</h3>
            </td>
        </tr>
        <tr class="rowAG">
            <td>
                <asp:GridView ID="gvRevisiones" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvRevisiones_SelectedIndexChanged"
                    OnRowCommand="gvRevisiones_RowCommand" DataKeyNames="NroRevision,Componente">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl='<%# Eval("iConfirmaYAprueba").Equals(-2) ? "~/Image/btn/ico_info.jpg" : "~/Image/btn/info.jpg" %>'
                                    Height="16px" Width="16px" ToolTip="Validar" Visible='<%# !(bool)Eval("Finalizado") %>'
                                    Enabled='<%# (int)Eval("iConfirmaYAprueba") == -2 ? false : true %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnLiberar" runat="server" CommandName="Liberar" CommandArgument='<%# Eval("Componente")%>'
                                    ImageUrl="~/Image/btn/cancel.jpg" Height="16px" Width="16px" ToolTip="Liberar"
                                    Visible='<%# Eval("Liberar")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" ItemStyle-Width="100px" />
                        <asp:BoundField HeaderText="Componente a validar" DataField="Componente" ItemStyle-Width="200px" />
                        <asp:TemplateField HeaderText="¿Confirma que los datos coinciden con el documento adjunto y lo aprueba?"
                            HeaderStyle-Width="120px">
                            <ItemTemplate>
                                <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (int)Eval("iConfirmaYAprueba")==1 ? "Si" : (int)Eval("iConfirmaYAprueba")==0 ? "No" : (int)Eval("iConfirmaYAprueba")==-1 ? "Sin Validar o En Proceso": "Sin Información") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                    <EmptyDataTemplate>
                        No hay datos
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Button ID="btnFinalizar" runat="server" Text="Finalizar Revisión" Width="150px"
                    OnClick="btnFinalizar_Click" OnClientClick="return confirm('Está seguro de terminar esta revisión?');" />&nbsp;
                <asp:Button ID="btnEnviarComunicacion" runat="server" Text="Enviar Comunicación"
                    Width="150px" OnClick="btnEnviarComunicacion_Click" />&nbsp;
                <asp:Button ID="btnHistorico" runat="server" Text="Histórico Revisiones" Width="150px"
                    OnClick="btnHistorico_Click" />
            </td>
        </tr>
        <asp:Panel ID="pnlHistorial" Visible="false" runat="server">
            <tr class="rowA">
                <td>
                    <h3>
                        Historial de revisiones</h3>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <Ajax:TabContainer ID="tcInfoProveedor" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_green-theme"
                        Width="100%" AutoPostBack="false" Visible="true">
                        <Ajax:TabPanel ID="tpDatosBasicos" runat="server" HeaderText="Datos Básicos">
                            <HeaderTemplate>
                                Datos Básicos
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvDatosBasicos" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    GridLines="None" Width="100%" CellPadding="0" Height="16px" OnPageIndexChanging="gvDatosBasicos_OnPageIndexChanging">
                                    <Columns>
                                        <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                                        <asp:TemplateField HeaderText="¿Confirma que los datos coinciden con el documento adjunto y lo aprueba?"
                                            HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}",(Eval("ConfirmaYAprueba") == null) ? "N/A" : ( (bool)Eval("ConfirmaYAprueba") ? "Si" : "No")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />--%>
                                        <asp:TemplateField HeaderText="Observaciones" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="word-wrap: break-word; width: 300px;">
                                                    <%#Eval("Observaciones")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Fecha observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                                        <asp:BoundField HeaderText="Gestor documental" DataField="UsuarioCrea" HeaderStyle-Width="180px" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                    <EmptyDataTemplate>
                                        No hay datos
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </Ajax:TabPanel>
                        <Ajax:TabPanel ID="TabPanel1" runat="server" HeaderText="Datos Financieros">
                            <HeaderTemplate>
                                Datos Financieros
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvDatosFinancieros" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="True" GridLines="None" Width="100%" CellPadding="0" Height="16px" OnPageIndexChanging="gvDatosFinancieros_OnPageIndexChanging">
                                    <Columns>
                                        <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                                        <asp:BoundField HeaderText="No Vigencia" DataField="Anno" />
                                        <asp:TemplateField HeaderText="¿Confirma que los datos coinciden con el documento adjunto y lo aprueba?"
                                            HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("ConfirmaYAprueba") ? "Si" : "No") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />--%>
                                        <asp:TemplateField HeaderText="Observaciones" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="word-wrap: break-word; width: 300px;">
                                                    <%#Eval("Observaciones")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Fecha observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                                        <asp:BoundField HeaderText="Gestor documental" DataField="UsuarioCrea" HeaderStyle-Width="180px" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                    <EmptyDataTemplate>
                                        No hay datos
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </Ajax:TabPanel>
                        <Ajax:TabPanel ID="TabPanel2" runat="server" HeaderText="Experiencia">
                            <HeaderTemplate>
                                Experiencia
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvExperiencia" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    GridLines="None" Width="100%" CellPadding="0" Height="16px" OnPageIndexChanging="gvExperiencia_OnPageIndexChanging">
                                    <Columns>
                                        <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                                        <asp:BoundField HeaderText="Id. Experiencia" DataField="IdExpEntidad" />
                                        <asp:TemplateField HeaderText="¿El documento adjunto re&uacute;ne los requisitos m&iacute;nimos y aprueba los datos?"
                                            HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# string.Format("{0}",(Eval("ConfirmaYAprueba") == null) ? "N/A" : ( (bool)Eval("ConfirmaYAprueba") ? "Si" : "No")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />--%>
                                        <asp:TemplateField HeaderText="Observaciones" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="word-wrap: break-word; width: 300px;">
                                                    <%#Eval("Observaciones")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Fecha observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                                        <asp:BoundField HeaderText="Gestor documental" DataField="UsuarioCrea" HeaderStyle-Width="180px" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                    <EmptyDataTemplate>
                                        No hay datos
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </Ajax:TabPanel>
                        <Ajax:TabPanel ID="tpIntegrantes" runat="server" HeaderText="Datos de Integrantes">
                            <HeaderTemplate>
                                Datos de Integrantes
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:GridView ID="gvIntegrantes" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    GridLines="None" Width="100%" CellPadding="0" Height="16px" OnPageIndexChanging="gvIntegrantes_OnPageIndexChanging">
                                    <Columns>
                                        <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                                        <asp:TemplateField HeaderText="¿Confirma que los datos coinciden con el documento adjunto y lo aprueba?"
                                            HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}",(Eval("ConfirmaYAprueba") == null) ? "" : ( (bool)Eval("ConfirmaYAprueba") ? "Si" : "No")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />--%>
                                        <asp:TemplateField HeaderText="Observaciones" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="word-wrap: break-word; width: 300px;">
                                                    <%#Eval("Observaciones")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Fecha observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                                        <asp:BoundField HeaderText="Gestor documental" DataField="UsuarioCrea" HeaderStyle-Width="180px" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                    <EmptyDataTemplate>
                                        No hay datos
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </Ajax:TabPanel>
                    </Ajax:TabContainer>
                </td>
            </tr>
        </asp:Panel>
    </table>
</asp:Content>
