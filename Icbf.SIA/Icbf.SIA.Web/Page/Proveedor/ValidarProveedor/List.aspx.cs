using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;

/// <summary>
/// P�gina que despliega el estado y permite el acceso para validar los proveedores
/// </summary>
public partial class Page_Proveedor_ValidarProveedores_List : GeneralWeb
{
    masterPrincipal toolBar;
    bool _sorted = false;
    string PageName = "Proveedor/ValidarProveedor";
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                toolBar.LipiarMensajeError();
                toolBar.LimpiarOpcionesAdicionales();
                CargarDatosIniciales();
                //Buscar();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
            else
            {
                TxtActividadCiiuPrin.Text = hfDesActividadCiiuPrin.Value;
            }
        }
    }

    /// <summary>
    /// Manejador del evento click del bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar Tercero
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnBuscarTercerroClick(object sender, EventArgs e)
    {
        
        
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

     /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            toolBar.LipiarMensajeError();
            toolBar.LimpiarOpcionesAdicionales();
            String vTipoPersona = null;
            String vTipoidentificacion = null;
            String vIdentificacion = null;
            String vProveedor = null;
            String vEstado = null;
            String vUsuarioCrea = null;
            
            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vTipoPersona = Convert.ToString(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDocumento.SelectedValue != "-1" && ddlTipoDocumento.SelectedValue != "")
            {
                vTipoidentificacion = Convert.ToString(ddlTipoDocumento.SelectedValue);
            }
            if (txtIdentificacion.Text != "")
            {
                vIdentificacion = Convert.ToString(txtIdentificacion.Text.Trim());
            }
            if (TxtRazonSocial.Text != "")
            {
                vProveedor = Convert.ToString(TxtRazonSocial.Text.Trim());
            }
            if (ddlEstado.SelectedValue != "-1" && ddlEstado.SelectedValue != "")
            {
                vEstado = Convert.ToString(ddlEstado.SelectedValue);
            }

            String vIdTipoCiiuPrincipal = null;
            if (!string.IsNullOrEmpty(hfIdActividadCiiuPrin.Value) && hfIdActividadCiiuPrin.Value!="null")
            {
                vIdTipoCiiuPrincipal = hfIdActividadCiiuPrin.Value;
            }
            
            String vIdMunicipioDirComercial = null;
            if (ddlMunicipioDireccionComercial.SelectedValue != "-1" && ddlMunicipioDireccionComercial.SelectedValue != "")
            {
                vIdMunicipioDirComercial = Convert.ToString(ddlMunicipioDireccionComercial.SelectedValue);
            }

            String vIdDepartamentoDirComercial = null;
            if (ddlDepartamentoDireccionComercial.SelectedValue != "-1" && ddlDepartamentoDireccionComercial.SelectedValue != "")
            {
                vIdDepartamentoDirComercial = Convert.ToString(ddlDepartamentoDireccionComercial.SelectedValue);
            }

            String vIdTipoSector = null;
            if (ddlSector.SelectedValue != "-1" && ddlSector.SelectedValue != "")
            {
                vIdTipoSector = Convert.ToString(ddlSector.SelectedValue);
            }

            String vIdTipoRegimenTributario = null;
            if (ddlRegimenTributario.SelectedValue != "-1" && ddlRegimenTributario.SelectedValue != "")
            {
                vIdTipoRegimenTributario = Convert.ToString(ddlRegimenTributario.SelectedValue);
            }

            vUsuarioCrea=GetSessionUser().NombreUsuario;
            var myGridResults = vProveedorService.ConsultarEntidadProvOferentes_Validar(vEstado, vTipoPersona,
                                                vTipoidentificacion, vIdentificacion, vIdTipoCiiuPrincipal, vIdMunicipioDirComercial,
                                                vIdDepartamentoDirComercial, vIdTipoSector, vIdTipoRegimenTributario, vProveedor);
            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Proveedor.Entity.EntidadProvOferente), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<Icbf.Proveedor.Entity.EntidadProvOferente, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvEntidadProvOferente, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            gvEntidadProvOferente.PageSize = PageSize();
            gvEntidadProvOferente.EmptyDataText = EmptyDataText();
            
            if(GetSessionParameter("Revision.Finalizada").ToString()=="1")
                toolBar.MostrarMensajeGuardado("Revisi�n finalizada correctamente");
            RemoveSessionParameter("Revision.Finalizada");

            toolBar.EstablecerTitulos("Lista Validar Proveedores ", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            var dataKey = gvEntidadProvOferente.DataKeys[rowIndex];
            if (dataKey != null)
            {
                string strValue = dataKey.Value.ToString();
                SetSessionParameter("EntidadProvOferente.IdEntidad", strValue);
            
                NavigateTo("~/Page/Proveedor/ValidarProveedor/Revision.aspx");                
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void GvEntidadProvOferenteSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvEntidadProvOferente.SelectedRow);
    }
    protected void GvEntidadProvOferentePageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEntidadProvOferente.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
         
            if (GetSessionParameter("EntidadProvOferente.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("EntidadProvOferente.Eliminado");

            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            
            // lista documento reprecentante legal
            ManejoControles.LlenarEstadoProveedor(ddlEstado, "-1", true);

            ManejoControles.LlenarTipoSectorEntidad(ddlSector, "-1", true);

            
            
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamentoDireccionComercial, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarTipoDocumentotipoPersona();
      
    }

    /// <summary>
    /// Habilita panel Natural si la persona es tal
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlNatural(bool state)
    {
        //PnlNatural.Visible = state;
    }

    /// <summary>
    /// Habilita controles para una persona jur�dica
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlJuridica(bool state)
    {
        LblRazonSocial.Visible = state;
        TxtRazonSocial.Visible = state;
    }

    /// <summary>
    /// Carga Tipo de documento de acuerdo al tipo de persona
    /// </summary>
    protected void CargarTipoDocumentotipoPersona()
    {
        new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDocumento, "-1", true);
        }
        else if (tipoper.CodigoTipoPersona == "002" || tipoper.CodigoTipoPersona == "003" || tipoper.CodigoTipoPersona == "004")
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDocumento, "-1", true);
        }

        ManejoControles.LlenarRegimenTributario(ddlRegimenTributario, Convert.ToInt32(ddlTipoPersona.SelectedValue), "-1", true);
        if (tipoper.CodigoTipoPersona == "001" || tipoper.CodigoTipoPersona == "002")
        {
            ddlRegimenTributario.Visible = true;
            lblregimentrib.Visible = true;
            lblSector.Visible = true;
            ddlSector.Visible = true;
        }
        else
        {
            ddlRegimenTributario.Visible = false;
            lblregimentrib.Visible = false;
            lblSector.Visible = false;
            ddlSector.Visible = false;
        }
           
    }

    #region ordenarGrilla
    private SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }

    }
    private string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }

    }

    protected void GvEntidadProvOferentePage_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    #endregion

    /// <summary>
    /// Manejador del evento para un bot�n LimpiarCiiu
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnLimpiarCiiuprinClick(object sender, EventArgs e)
    {
        btnLimpiarCiiuprin.Text = string.Empty;
        hfIdActividadCiiuPrin.Value = null;
    }

    protected void DdlDepartamentoDireccionComercialSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDepartamentoDireccionComercial();

    }

    /// <summary>
    /// Carga valores para una lista desplegable
    /// </summary>
    protected void CargarDepartamentoDireccionComercial()
    {
        CargarMunicipioXDepartamento(ddlMunicipioDireccionComercial, Convert.ToInt32(ddlDepartamentoDireccionComercial.SelectedValue));
    }

    /// <summary>
    /// Llena una lista desplegable de municipios filtrador por determinado departamento
    /// </summary>
    /// <param name="PddlMunicipioConstituida"></param>
    /// <param name="pidDepartament"></param>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipioConstituida, int pidDepartament)
    {
        //ManejoControles Controles = new ManejoControles();
        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipioConstituida, "-1", true, pidDepartament);
    }


    new public Boolean GetState(MasterPage vMaster, String pPrefijo)
    {

        Boolean vBuscar = false;

        Object vValue = null;

        vValue = GetSessionParameter(pPrefijo + "." + ddlEstado.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            ddlEstado.SelectedIndex = -1;

            ddlEstado.SelectedIndex = Convert.ToInt32(vValue.ToString());
            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + ddlTipoPersona.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            ddlTipoPersona.SelectedIndex = -1;

            ddlTipoPersona.SelectedIndex = Convert.ToInt32(vValue.ToString());

            ddlTipoPersona_SelectedIndexChanged(this, new EventArgs());

            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + ddlTipoDocumento.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
           
                ddlTipoDocumento.SelectedIndex = -1;

                ddlTipoDocumento.SelectedIndex = Convert.ToInt32(vValue.ToString());

                vBuscar = true;
            
        }

        vValue = GetSessionParameter(pPrefijo + "." + txtIdentificacion.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            txtIdentificacion.Text = vValue.ToString();
            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + TxtRazonSocial.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            TxtRazonSocial.Text = vValue.ToString();
            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + TxtActividadCiiuPrin.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            TxtActividadCiiuPrin.Text = vValue.ToString();
            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + ddlDepartamentoDireccionComercial.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            ddlDepartamentoDireccionComercial.SelectedIndex = -1;

            ddlDepartamentoDireccionComercial.SelectedIndex = Convert.ToInt32(vValue.ToString());

            DdlDepartamentoDireccionComercialSelectedIndexChanged(this, new EventArgs());

            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + ddlMunicipioDireccionComercial.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            
                ddlMunicipioDireccionComercial.SelectedIndex = -1;

                ddlMunicipioDireccionComercial.SelectedIndex = Convert.ToInt32(vValue.ToString());

                vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + ddlSector.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            ddlSector.SelectedIndex = -1;

            ddlSector.SelectedIndex = Convert.ToInt32(vValue.ToString());

            vBuscar = true;
        }

        vValue = GetSessionParameter(pPrefijo + "." + ddlRegimenTributario.ClientID);

        if (vValue != null && vValue.ToString() != "")
        {
            ddlRegimenTributario.SelectedIndex = -1;

            ddlRegimenTributario.SelectedIndex = Convert.ToInt32(vValue.ToString());

            vBuscar = true;
        }

        

        return vBuscar;

    }

    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDocumento.SelectedValue != "-1")
        {
            this.txtIdentificacion.Text = String.Empty;
            this.txtIdentificacion.Focus();
        }
    }
    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDocumento.SelectedItem.Text == "CC" || this.ddlTipoDocumento.SelectedItem.Text == "NIT" || this.ddlTipoDocumento.SelectedItem.Text == "PA")
        {
            this.txtIdentificacion.MaxLength = 11;

        }
        else if (this.ddlTipoDocumento.SelectedItem.Text == "CE")
        {
            this.txtIdentificacion.MaxLength = 6;
        }
        else
        {
            this.txtIdentificacion.MaxLength = 17;
        }
    }
}
