﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Icbf.Utilities.Presentation;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Xml;
using System.IO;

/// <summary>
/// Permite la descarga al usuario de un archivo que ya fue descargado a una ruta del servidor
/// </summary>
public partial class Page_Proveedor_Archivo_ObtenerArchivo : GeneralWeb
{

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            if (GetSessionParameter("Proveedor.Archivo") != null)
            {
                var vImagen = (Byte[])GetSessionParameter("Proveedor.Archivo");
                RemoveSessionParameter("Proveedor.Archivo");
                var vContentType = (string)GetSessionParameter("Proveedor.ContentType");
                RemoveSessionParameter("Proveedor.ContentType");
                var vNombreArchivo = (string)GetSessionParameter("Proveedor.NombreArchivo");
                RemoveSessionParameter("Proveedor.NombreArchivo");
                var vLongitud = (string)GetSessionParameter("Proveedor.Longitud");
                RemoveSessionParameter("Proveedor.Longitud");


                Response.Clear();
                Response.AddHeader("Content-Disposition", "inline; filename=" + vNombreArchivo);
                Response.AddHeader("Content-Length", vImagen.Length.ToString());
                Response.ContentType = vContentType;
                Response.Buffer = true;
                Response.BinaryWrite(vImagen);
                Response.Flush();
                Response.Close();
                Response.End();
            }
        }

    }
}