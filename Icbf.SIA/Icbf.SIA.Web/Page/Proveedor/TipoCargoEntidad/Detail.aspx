<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoCargoEntidad_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoCargoEntidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código *
            </td>
            <td>
                Descripción *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoTipoCargoEntidad"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false"></asp:TextBox>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                
            </td>
        </tr>
    </table>
</asp:Content>
