﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

/// <summary>
/// Página que despliegalos motivos de cambio de estado del proveedor
/// </summary>
public partial class Page_Proveedor_CambiarEstadoTercero_MotivoCambio : GeneralWeb
{

    Poveedor_VentanaPopup toolBar;

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar = (Poveedor_VentanaPopup)this.Master;
        if (!IsPostBack)
        {
            toolBar.EstablecerTitulos("Cambio de Estado");
            if (Request.QueryString["oP"] == "T")
            {
                tblProveedor.Visible = false;
                tblProvIntegrantes.Visible = false;
            }
            else
            {

                string tipoPersona = Request.QueryString["tP"].ToString();
                if (tipoPersona.Equals("1") || tipoPersona.Equals("2"))
                {
                    tblProveedor.Visible = true;
                    tblProvIntegrantes.Visible = false;
                }
                else if (tipoPersona.Equals("3") || tipoPersona.Equals("4"))
                {
                    tblProveedor.Visible = false;
                    tblProvIntegrantes.Visible = true;
                }
            }
        }

    }

    protected void Page_UnLoad(object sender, EventArgs e)
    {
        txtMotivoCambio.Text = "";
    }

    /// <summary>
    /// Manejador de evento click para el botón Aceptar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            //Si es proveedor, debe marcar al menos un checkbox
            if (tblProveedor.Visible)
            {
                if (!chkDatosBasicos.Checked && !chkFinanciera.Checked && !chkExperiencia.Checked)
                    throw new Exception("Debe seleccionar un módulo a cambiar el estado.");
            }
            else if (tblProvIntegrantes.Visible)
            {
                if (!CheckDatosBasic.Checked && !CheckDatosInt.Checked)
                    throw new Exception("Debe seleccionar un módulo a cambiar el estado.");
            }

            if (txtMotivoCambio.Text.Trim() != "")
            {
                string returnValues =
                "<script language='javascript'> " +
                "var pObj = Array();";

                returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(txtMotivoCambio.Text) + "';";
                returnValues += "pObj[" + (1) + "] = '" + HttpUtility.HtmlDecode(chkDatosBasicos.Checked.ToString()) + "';";
                returnValues += "pObj[" + (2) + "] = '" + HttpUtility.HtmlDecode(chkFinanciera.Checked.ToString()) + "';";
                returnValues += "pObj[" + (3) + "] = '" + HttpUtility.HtmlDecode(chkExperiencia.Checked.ToString()) + "';";
                returnValues += "pObj[" + (4) + "] = '" + HttpUtility.HtmlDecode("true") + "';";
                returnValues += "pObj[" + (5) + "] = '" + HttpUtility.HtmlDecode(CheckDatosBasic.Checked.ToString()) + "';";
                returnValues += "pObj[" + (6) + "] = '" + HttpUtility.HtmlDecode(CheckDatosInt.Checked.ToString()) + "';";
                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                returnValues += " parent.parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                                           " window.parent.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                    "</script>";
                ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
            }
            else
            {
                throw new Exception("Debe ingresar el motivo del cambio de estado");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}