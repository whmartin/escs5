﻿<%@ Page Title="Proveedores" Language="C#" MasterPageFile="~/General/General/Master/VentanaPopup.master" AutoEventWireup="true" CodeFile="MotivoCambio.aspx.cs" Inherits="Page_Proveedor_CambiarEstadoTercero_MotivoCambio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <script src="../../../Scripts/jq.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.numeric.js" type="text/javascript"></script>
        <script type="text/javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } 
        }
        </script>
        <div>
            <asp:Label ID="LbError" runat="server" Text=""></asp:Label>
        </div>
        <table id="tblProveedor" width="90%" align="center" runat="server" visible="false">
                    <tr class="rowB">
                        <td>
                            <asp:CheckBox ID="chkDatosBasicos" runat="server" Text ="Datos B&aacute;sicos" />  
                        </td>
                        <td>
                            <asp:CheckBox ID="chkFinanciera" runat="server" Text ="Financiera" />  
                        </td>
                        <td>
                            <asp:CheckBox ID="chkExperiencia" runat="server" Text ="Experiencia" />  
                        </td>
                    </tr>
         </table>
         <table id="tblProvIntegrantes" width="90%" align="center" runat="server" visible="false">
                    <tr class="rowB">
                        <td>
                            <asp:CheckBox ID="CheckDatosBasic" runat="server" Text ="Datos B&aacute;sicos" />  
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckDatosInt" runat="server" Text ="Integrantes" />  
                        </td>
                    </tr>
         </table>
        <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            Motivo del Cambio de estado 
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtMotivoCambio" Width="150px" Height="80px" CssClass="TextBoxGrande"
                                        TextMode="MultiLine" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,100);"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" Width ="100px" 
                                onclick="btnAceptar_Click"/>
                        </td>
                    </tr>
         </table>
</asp:Content>

