﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using System.Web.Services;
using System.Configuration;
using System.Net.Mail;
using System.Threading;

/// <summary>
/// Página que despliega la consulta basada en filtros de cambos de estado
/// </summary>
public partial class Page_Proveedor_CambiarEstadoTercero_list : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/CambiarEstadoTercero";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    //RUBOService vRUBOService = new RUBOService();
    ManejoControles ManejoControles = new ManejoControles();
    bool _sorted;

    private static string idsSePuedenCambairEdo = "";
    private static bool sePuedenCambiarEdo = false;
    //Tercero vTercero = new Tercero();
    #endregion

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Usuario que inició sesión
            CargarDatosIniciales();
        }
    }
    private void deshabilitarControlesConsulta(bool p)
    {
        ddlTipoDoc.Enabled = p;
        ddlTipoPersona.Enabled = p;
        txtNumIdentificacion.Enabled = p;
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ViewState["TipoConsulta"] = "";
            ViewState["TipoPersona"] = "";
            btnCambiarValidacion.Enabled = false;
            deshabilitarControlesConsulta(false);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.EstablecerTitulos("Cambiar Estado", SolutionPage.Add.ToString());
            ManejoControles.LlenarTipoDocumentoF(ddlTipoDoc, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
        }
        ViewState["TipoPersona"] = ddlTipoPersona.SelectedValue;
        gvDatos.DataSource = null;
        gvDatos.DataBind();
        if (ddlTipoPersona.SelectedIndex > 0)
        {


            if (rbSistemaaTrabajar.SelectedItem.Value.Equals("T") || rbSistemaaTrabajar.SelectedItem.Value.Equals("P"))
                Buscar();
        }
    }


    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int vIdTipoPersona = 0;
            int vIdTipoDocIdentifica = 0;
            string vNumeroIdentificacion = "";
            string vTercero = null;
            string vTipoCOnsulta = "";
            int consulta;
            btnCambiarValidacion.Enabled = false;
            vTipoCOnsulta = ViewState["TipoConsulta"].ToString();

            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDoc.SelectedValue != "")
            {
                //vIdTipoDocIdentifica = int.Parse(ddlTipoPersona.SelectedValue); 
                vIdTipoDocIdentifica = int.Parse(ddlTipoDoc.SelectedValue);
            }
            if (txtNumIdentificacion.Text != "")
            {
                vNumeroIdentificacion = txtNumIdentificacion.Text.Trim();
            }
            //Pregunta si esta buscando por tercero o por proveedor
            if (vTipoCOnsulta == "") throw new Exception("Se debe seleccionar un sistema a trabajar");
            else toolBar.LipiarMensajeError();
            //Si se ha seleccionado Provedor, Obligar a que se seleccione un tipo persona
            if (vTipoCOnsulta.Equals("P"))
            {
                if (vIdTipoPersona == 0) throw new Exception("Se debe seleccionar un tipo persona");
                else toolBar.LipiarMensajeError();
            }
            if (vTipoCOnsulta == "T")
            {
                consulta = 1;
                if (txtTercero.Text.Trim() != "") vTercero = txtTercero.Text;
            }
            else
            {
                consulta = 2;
                if (txtProveedor.Text.Trim() != "") vTercero = txtProveedor.Text;
            }


            string vUsuario = GetSessionUser().NombreUsuario;
            var myGridResults = vProveedorService.ConsultarTerceros_Proveedores_Validados(consulta, vIdTipoPersona, vIdTipoDocIdentifica, vNumeroIdentificacion, vTercero, null);
            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Tercero), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Tercero, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvDatos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }



    }

    protected void rbSistemaaTrabajar_SelectedIndexChanged(object sender, EventArgs e)
    {
        string seleccionado = rbSistemaaTrabajar.SelectedItem.Value;
        gvDatos.DataSource = null;
        gvDatos.DataBind();
        if (seleccionado == "T")
        {
            ViewState["TipoConsulta"] = "T";
            txtTercero.Visible = true;
            lblTercero.Visible = true;
            txtProveedor.Visible = false;
            lblProveedor.Visible = false;
            lblProveedor.Visible = false;
            lblTercero.Visible = true;
            this.gvDatos.Columns[3].Visible = false;
            this.gvDatos.Columns[4].Visible = true;
            Buscar();

        }
        if (seleccionado == "P")
        {
            ViewState["TipoConsulta"] = "P";
            txtTercero.Visible = false;
            lblTercero.Visible = false;
            txtProveedor.Visible = true;
            lblProveedor.Visible = true;
            lblProveedor.Visible = true;
            lblTercero.Visible = false;
            this.gvDatos.Columns[3].Visible = true;
            this.gvDatos.Columns[4].Visible = false;
            if (ddlTipoPersona.SelectedIndex > 0)
                Buscar();

        }
        deshabilitarControlesConsulta(true);
        btnCambiarValidacion.Enabled = false;
        //Buscar();
    }


    protected void gvDatos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDatos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    [WebMethod]
    public static  bool webMethodSePuedeCambiarEstado(string idsTercero)
    {
        //Esta bien que retorne Bool.. para saber si al menos un registro de los seleccionado puede cambiar de estado...
        //.... y bueno, false si ninguno puede cambiar de estado.°!
        ProveedorService vProveedorService = new ProveedorService();
        string[] valores;
        valores = idsTercero.Split('|');
        string valoresParaServer = "";
        sePuedenCambiarEdo = false;
        for (int i = 0; i < valores.Length; i++)
        {
            if (vProveedorService.PuedeTerceroCambiarEstado(int.Parse(valores[i].Split(',')[0])))
            {
                sePuedenCambiarEdo = true;
                valoresParaServer = valoresParaServer + valores[i] + ",1|";
            }
            else
            {
                valoresParaServer = valoresParaServer + valores[i] + ",0|";
            }
        }
        valoresParaServer = valoresParaServer.Substring(0, valoresParaServer.Length - 1);
        idsSePuedenCambairEdo = valoresParaServer;
        return sePuedenCambiarEdo;
    }

    protected void btnCambiarValidacion_Click(object sender, EventArgs e)
    {
        int vResultado = -1;
        bool resultadoCambio = false;
        string idTemporal = "";
        string idsTercerosACambiar = "";
        string numIdentNoCambio = "";
        string numIdentSiCambio = "";
        if (ViewState["IdTemporal"] == null)
        {
            ViewState["IdTemporal"] = DateTime.Now.Ticks.ToString();
        }
        idTemporal = ViewState["IdTemporal"].ToString();

        try
        {
            if (hfTextoMotivo.Value.Trim() == "") return;
            if (rbSistemaaTrabajar.SelectedValue.Equals("T"))
            {
                if (!sePuedenCambiarEdo)
                {
                    toolBar.MostrarMensajeError("No se puede cambiar el estado de un Tercero, cuando el estado del Proveedor asociado es Validado.");
                    btnCambiarValidacion.Enabled = false;
                    limpiarDatos();
                    UnCheckGrid();
                    return;
                }
                //idsSePuedenCambairEdo
                string[] valoresTerceros = idsSePuedenCambairEdo.Split('|');
                string[] arrayTemporal;
                for (int i = 0; i < valoresTerceros.Length; i++)
                {
                    arrayTemporal = valoresTerceros[i].Split(',');
                    if (arrayTemporal[2].Equals("1"))
                    {
                        idsTercerosACambiar = idsTercerosACambiar + arrayTemporal[0] + ",";
                        numIdentSiCambio = numIdentSiCambio + arrayTemporal[1] + ",";
                    }
                    else if (arrayTemporal[2].Equals("0"))
                    {
                        numIdentNoCambio = numIdentNoCambio + arrayTemporal[1] + ",";
                    }
                }
                if (idsTercerosACambiar.Length > 1)
                    idsTercerosACambiar = idsTercerosACambiar.Substring(0, idsTercerosACambiar.Length - 1);
                if (numIdentNoCambio.Length > 1)
                    numIdentNoCambio = numIdentNoCambio.Substring(0, numIdentNoCambio.Length - 1);
                if (numIdentSiCambio.Length > 1)
                    numIdentSiCambio = numIdentSiCambio.Substring(0, numIdentSiCambio.Length - 1);

                resultadoCambio = cambiarEstadoTerceros(idsTercerosACambiar);
            }
            else if (rbSistemaaTrabajar.SelectedValue.Equals("P"))
            {
                resultadoCambio = cambiarEstadoProveedores();
            }
            Buscar();
            //udpGrid.Update();
            //Llamar procedimiento y enviarle idtemporal
            string Tipo = "";
            if (ViewState["TipoConsulta"].ToString() == "T") Tipo = "Tercero";
            else Tipo = "Proveedor";
            //Envia correso a los gestores documentales
            string appName = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
            string rolName = System.Configuration.ConfigurationManager.AppSettings["RolName"];

            //string remitente = ConfigurationManager.AppSettings["RemitenteProveedores"];
            //string linkSitioWeb = ConfigurationManager.AppSettings["URLProveedores"];
            //ObtenerMailsGestorProveedor
            //string pPara = vProveedorService.ObtenerMailsGestorProveedor(appName, rolName);
            //if (string.IsNullOrEmpty(pPara)) { throw new Exception("No hay usuarios con el perfil GESTOR PROVEEDOR"); }
            //else
            //{
                //if (pPara.Substring(pPara.Length - 1, 1).Equals(";"))
                //    pPara = pPara.Substring(0, pPara.Length - 1);
                
                //ViewState["IdTemporal"] = null;
                //EnviarMensaje_CambioEstado(remitente, pPara, linkSitioWeb, Tipo, idTemporal);
            //}


            vResultado = vProveedorService.EnviarMailsCambioEstadoTerceroProveedor(Tipo, idTemporal, appName, rolName);
            //Aquí se deben especificar los mensajes.... de acuerdo a cada caso.
            if (vResultado == -1)
            {
                if (Tipo.Equals("Proveedor"))
                {
                    if (resultadoCambio)
                        toolBar.MostrarMensajeGuardado("Se ha realizado el cambio de estado de proveedor(es) de forma exitosa");
                    else
                        toolBar.MostrarMensajeError("No se ha realizado el cambio de estado de proveedor(es) de forma exitosa, verifique por favor.");
                }
                else if (Tipo.Equals("Tercero"))
                {
                    if (numIdentNoCambio.Length > 0)
                    {
                        if (numIdentSiCambio.Length == 0)
                        {
                            toolBar.MostrarMensajeError("No se puede cambiar el estado de un Tercero cuando tiene asociado a un proveedor en estado Validado.");
                        }
                        else if (numIdentSiCambio.Length > 0)
                        {
                            if (resultadoCambio)
                            {
                                toolBar.MostrarMensajeError("No se pudo cambiar el estado a los siguinetes terceros debido a que tienen un proveedor asociado en estado validado: " + numIdentNoCambio + ".");
                            }
                            else
                            {
                                toolBar.MostrarMensajeError("No se ha realizado el cambio de estado de forma exitosa verifique por favor.");
                            }
                        }
                    }
                    else if (numIdentNoCambio.Length == 0)
                    {
                        if (resultadoCambio)
                            toolBar.MostrarMensajeGuardado("Se ha realizado el cambio de estado del tercero de forma exitosa");
                        else
                            toolBar.MostrarMensajeError("No Se ha realizado el cambio de estado del tercero de forma exitosa, verifique por favor.");
                    }
                }



                btnCambiarValidacion.Enabled = false;
            }
            //------------------------------------------------------------------
            limpiarDatos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EnviarMensaje_CambioEstado(string pDe, string pPara, string pLinkUrl, string Tipo, string idTemporal)
    {
        try
        {
            var sendMailThread = new Thread(() =>
                {
                    string body = vProveedorService.ObtenerCuerpoDeCorreo_CambioEstadoTerceroProveedor(Tipo, idTemporal);

                    var myMailMessage = new MailMessage();
                    // Armar Lista de Correos
                    myMailMessage.From = new MailAddress(pDe);
                    myMailMessage.To.Add(new MailAddress(pPara));
                    myMailMessage.Subject = "Validar " + Tipo;

                    myMailMessage.IsBodyHtml = true;


                    myMailMessage.Body = body;


                    var mySmtpClient = new SmtpClient();

                    object userState = myMailMessage;

                    mySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

                    mySmtpClient.SendAsync(myMailMessage, userState);

                });

            sendMailThread.Start();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo,");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
    }


    private bool cambiarEstadoTerceros(string idsTercerosCambioEstado)
    {
        string motivo = hfTextoMotivo.Value;
        int vResultado = -1;
        bool aceptado = hfAceptar.Value == "" ? false : bool.Parse(hfAceptar.Value);
        string idTemporal = "";
        bool huboCambio = false;
        //Realizo el cambio de estado
        string vUsuario = GetSessionUser().NombreUsuario;
        if (motivo.Trim() != "" && aceptado)
        {
            //Recorrer String IdsTerceros.
            string[] valoresIDS = idsTercerosCambioEstado.Split(',');
            for (int i = 0; i < valoresIDS.Length; i++)
            {
                Tercero vTercero = new Tercero();
                int idTercero = int.Parse(valoresIDS[i]);
                idTemporal = ViewState["IdTemporal"].ToString();
                vResultado = vProveedorService.InsertarMotivoCambioEstadoTercero(idTercero,  idTemporal, motivo, vUsuario);
                if (vResultado != -1)
                {
                    huboCambio = true;
                }
            }
        }
        else
        {
            throw new Exception("Especifique el motivo de cambio de estado.");
            //return huboCambio;
        }

        return huboCambio;
    }
    private void UnCheckGrid()
    {
        foreach (GridViewRow item in gvDatos.Rows)
        {
            CheckBox chkSelec = (CheckBox)item.FindControl("chkseleccionado");
            if (chkSelec.Checked)
            {
                chkSelec.Checked = false;
            }
        }
        btnCambiarValidacion.Enabled = false;
    }
    private bool cambiarEstadoProveedores()
    {
        string motivo = hfTextoMotivo.Value;
        int vResultado = -1;
        bool datosbasicos;
        if(ddlTipoPersona.SelectedValue.Equals("1") ||ddlTipoPersona.SelectedValue.Equals("2"))
            datosbasicos = hfDatosBasic.Value == "" ? false : bool.Parse(hfDatosBasic.Value);
        else
            datosbasicos = hfDatosBasicos.Value == "" ? false : bool.Parse(hfDatosBasicos.Value);
        bool financiera = hfFinanciera.Value == "" ? false : bool.Parse(hfFinanciera.Value);
        bool experiencia = hfExperiencia.Value == "" ? false : bool.Parse(hfExperiencia.Value);
        bool aceptado = hfAceptar.Value == "" ? false : bool.Parse(hfAceptar.Value);
        bool integrantes = hfIntegrantes.Value == "" ? false : bool.Parse(hfIntegrantes.Value);
        string idTemporal = "";
        bool huboCambio = false;
        //Realizo el cambio de estado
        string vUsuario = GetSessionUser().NombreUsuario;
        if (motivo.Trim() != "" && aceptado)
        {
            foreach (GridViewRow item in gvDatos.Rows)
            {
                CheckBox chkSelec = (CheckBox)item.FindControl("chkseleccionado");
                if (chkSelec.Checked)
                {
                    Tercero vTercero = new Tercero();
                    int idTercero = int.Parse(gvDatos.DataKeys[item.RowIndex].Value.ToString());
                    idTemporal = ViewState["IdTemporal"].ToString();
                    vResultado = vProveedorService.InsertarMotivoCambioEstado(idTercero, datosbasicos, financiera, experiencia, integrantes, idTemporal, motivo, vUsuario);
                    if (vResultado != -1)
                    {
                        huboCambio = true;
                    }
                }
            }
        }
        else
        {
            throw new Exception("Especifique el motivo de cambio de estado.");
            //return huboCambio;
        }

        if (!huboCambio)
        {
            throw new Exception("Seleccione registro a cambiar");
            //return huboCambio;
        }

        return huboCambio;
    }


    private bool pudeCambiarEstado(int idTercero)
    {
        return vProveedorService.PuedeTerceroCambiarEstado(idTercero);
    }
    protected void limpiarDatos()
    {
        hfTextoMotivo.Value = "";
        hfDatosBasicos.Value = "";
        hfFinanciera.Value = "";
        hfExperiencia.Value = "";
        hfAceptar.Value = "";
    }

    protected void chkseleccionado_OnCheckedChanged(object sender, EventArgs e)
    {
        //seleccionar control
        CheckBox rb = (CheckBox)sender;
        GridViewRow row = (GridViewRow)rb.NamingContainer;
        CheckBox chkSelec = (CheckBox)row.FindControl("chkseleccionado");
        if (chkSelec.Checked)
        {
            btnCambiarValidacion.Enabled = true;
        }
        else
        {
            bool ingreso = false;
            foreach (GridViewRow item in gvDatos.Rows)
            {
                CheckBox chkSelec2 = (CheckBox)item.FindControl("chkseleccionado");
                if (chkSelec2.Checked)
                {
                    ingreso = true;
                }
            }
            if (ingreso) btnCambiarValidacion.Enabled = true;
            else btnCambiarValidacion.Enabled = false;
        }
        toolBar.LipiarMensajeError();
    }

    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumIdentificacion.Text = String.Empty;
            this.txtNumIdentificacion.Focus();
        }
    }

    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDoc.SelectedItem.Text == "CC" || this.ddlTipoDoc.SelectedItem.Text == "NIT" || this.ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.txtNumIdentificacion.MaxLength = 11;

        }
        else if (this.ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.txtNumIdentificacion.MaxLength = 6;
        }
        else
        {
            this.txtNumIdentificacion.MaxLength = 17;
        }
    }

    #region ordenarGrilla
    private SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }

    }
    private string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }

    }
    protected void gvDatos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }


    #endregion


}