﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_CambiarEstadoTercero_list"
    Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>

    <asp:Panel runat="server" ID="pnlConsulta">
        <asp:HiddenField ID="hfTextoMotivo" runat="server" />
        <asp:HiddenField ID="hfDatosBasicos" runat="server" />
        <asp:HiddenField ID="hfFinanciera" runat="server" />
        <asp:HiddenField ID="hfExperiencia" runat="server" />
        <asp:HiddenField ID="hfAceptar" runat="server" />
        <asp:HiddenField ID="hfDatosBasic" runat="server" />
        <asp:HiddenField ID="hfIntegrantes" runat="server" />
        <table id="tabla" width="90%" align="center">
            <tr class="rowB">
                <td colspan="2">
                    Sistema a Trabajar
                    <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rbSistemaaTrabajar"
                        SetFocusOnError="true" ErrorMessage="Se debe seleccionar un sistema a trabajar"
                        Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2" align="left">
                    <asp:RadioButtonList runat="server" ID="rbSistemaaTrabajar" RepeatDirection="Horizontal"
                        AutoPostBack="True" OnSelectedIndexChanged="rbSistemaaTrabajar_SelectedIndexChanged">
                        <asp:ListItem Text="Tercero" Value="T"></asp:ListItem>
                        <asp:ListItem Text="Proveedor" Value="P"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de Persona o asociaci&oacute;n"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona"
                        ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un tipo de persona</asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Seleccione un tipo de identificación"
                        ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un tipo de identificaci&oacute;n</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="True" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoDoc" OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" width="50%">
                    <asp:Label ID="lblIdentificaNat" runat="server" Text="N&uacute;mero de Identificaci&oacute;n"></asp:Label>
                    <%--<Ajax:FilteredTextBoxExtender ID="ftxtDv" runat="server" TargetControlID="txtNumIdentificacion"
                        FilterType="Numbers" ValidChars="/1234567890" />--%>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="Registre el número de identificación"
                        ControlToValidate="txtNumIdentificacion" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic">Ingrese el n&uacute;mero de Identificaci&oacute;n</asp:RequiredFieldValidator>
                </td>
                <td class="Cell" width="50%">
                    <asp:Label ID="lblTercero" runat="server" Text="Tercero" Visible="false"></asp:Label>
                    <asp:Label ID="lblProveedor" runat="server" Text="Proveedor" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" width="50%">
                    <asp:TextBox runat="server" ID="txtNumIdentificacion" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                </td>
                <td class="Cell" width="50%">
                    <asp:TextBox runat="server" ID="txtTercero" MaxLength="50" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtProveedor" MaxLength="50" Visible="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDatos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTercero" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvDatos_PageIndexChanging" AllowSorting="true" OnSorting="gvDatos_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkseleccionado" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        AutoPostBack="true" OnCheckedChanged="chkseleccionado_OnCheckedChanged" Height="16px"
                                        Width="16px" ToolTip="Seleccione" />
                                    <asp:HiddenField ID="hdfNumIdentificacion" runat="server" Value='<%# String.Format("{0},{1}", Eval("IdTercero"), Eval("NumeroIdentificacion")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Identificaci&oacute;n" DataField="NombreListaTipoDocumento"
                                SortExpression="NombreListaTipoDocumento" />
                            <asp:BoundField HeaderText="N&uacute;mero Identificaci&oacute;n" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Proveedor" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial" />
                            <asp:BoundField HeaderText="Tercero" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstadoTercero" SortExpression="NombreEstadoTercero" />
                            <asp:BoundField HeaderText="Fecha del Cambio" DataField="FechaCrea" DataFormatString="{0:dd/M/yyyy}"
                                SortExpression="FechaCrea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table id="Table1" width="90%" align="center">
        <tr class="rowB">
            <td width="25%">
            </td>
            <td width="25%">
            </td>
            <td width="25%">
            </td>
            <td width="25%" align="right">
                <asp:Button ID="btnCambiarValidacion" runat="server" Text="Cambiar Estado" Width="150px"
                     OnClientClick="GetMotivo(); return false;" />
                <asp:Button ID="btnPreCambiarValidacion" runat="server" style="width:0px; height:0px; display:none;"
                    OnClick="btnCambiarValidacion_Click"  />
            </td>
        </tr>
    </table>
    <%--<script src="../../../Scripts/jquery-te-1.4.0.min.js" type="text/javascript"></script>
    --%><script type="text/javascript" language="javascript">
        function GetMotivo() {
            var TipoConsulta = '<%=ViewState["TipoConsulta"]%>';
            var TipoPersona = '<%=ViewState["TipoPersona"]%>';
            var prevReturnValue;
            var retLupa;
            if (TipoConsulta == "T") {
                var idsTerceros = "";
                $('#<%=gvDatos.ClientID %>').find("input:checkbox:checked").each(function (item, index) {
                    var Name = $(this).closest("td").find('input[type=hidden]').val();
                    idsTerceros += Name + "|";
                });

                idsTerceros = idsTerceros.substr(0, idsTerceros.length - 1);
                jQuery.ajax({
                    async: false,
                    url: 'List.aspx/webMethodSePuedeCambiarEstado',
                    type: "POST",
                    dataType: "json",
                    data: "{'idsTercero': '" + idsTerceros + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d.toString() == "false") {
                            return;
                        }
                        else if (data.d.toString() == "true") {
                            window_showModalDialog('../../../Page/Proveedor/CambiarEstadoTercero/MotivoCambio.aspx?oP=' + TipoConsulta + '&tP=' + TipoPersona + '', setResultMotivoCambioT, 'dialogWidth:600px;dialogHeight:300px;resizable:yes;');
                        }
                    }
                });
            }
            else if (TipoConsulta == "P") {
                window_showModalDialog('../../../Page/Proveedor/CambiarEstadoTercero/MotivoCambio.aspx?oP=' + TipoConsulta + '&tP=' + TipoPersona + '', setResultMotivoCambioP, 'dialogWidth:600px;dialogHeight:300px;resizable:yes;');
            }
        }
        function setResultMotivoCambioT(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 4) {
                    document.getElementById('cphCont_hfTextoMotivo').value = retLupa[0];
                    document.getElementById('cphCont_hfDatosBasicos').value = retLupa[1];
                    document.getElementById('cphCont_hfFinanciera').value = retLupa[2];
                    document.getElementById('cphCont_hfExperiencia').value = retLupa[3];
                    document.getElementById('cphCont_hfAceptar').value = retLupa[4];
                    if (retLupa[0] != "")
                        $("#<%= btnPreCambiarValidacion.ClientID %>").trigger("click");
                }
            }
            
        }
        function setResultMotivoCambioP(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 4) {
                    document.getElementById('cphCont_hfTextoMotivo').value = retLupa[0];
                    document.getElementById('cphCont_hfDatosBasicos').value = retLupa[1];
                    document.getElementById('cphCont_hfFinanciera').value = retLupa[2];
                    document.getElementById('cphCont_hfExperiencia').value = retLupa[3];
                    document.getElementById('cphCont_hfAceptar').value = retLupa[4];
                    document.getElementById('cphCont_hfDatosBasic').value = retLupa[5];
                    document.getElementById('cphCont_hfIntegrantes').value = retLupa[6];
                    if (retLupa[0] != "")
                        $("#<%= btnPreCambiarValidacion.ClientID %>").trigger("click");
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPrincipalButton" runat="Server">
</asp:Content>
