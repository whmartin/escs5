<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Proveedor_Add" Async="true" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<%@ Register Src="../../../General/General/Control/IcbfDireccion.ascx" TagName="IcbfDireccion" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdUsuario" runat="server" />
    <asp:HiddenField ID="hfCambioCorreo" runat="server" />
    <script src="../../../Scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/MaxLength.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            $("[id*=txtObservacionesCorreccion]").MaxLength({ MaxLength: 200, DisplayCharacterCount: false });

        }
        function funLongCelular_ClientValidate(sender, args) {
            if (document.all(sender.controltovalidate).value.length % 10 > 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        function funTelefono_ClientValidate(sender, args) {

            sender.innerHTML = IsTelefono_IndicadorExtension(document.all("<% = txtIndicativo.ClientID%>").value,
                                                             document.all("<% = txtTelefono.ClientID%>").value,
                                                             document.all("<% = txtExtension.ClientID%>").value)

            args.IsValid = (sender.innerHTML == "");
        }

        function funTelefono1_ClientValidate(sender, args) {
            sender.innerHTML = IsTelefono_IndicadorExtension(document.all("<% = txtIndicativo1.ClientID%>").value,
                                                             document.all("<% = txtTelefono1.ClientID%>").value,
                                                             document.all("<% = txtExtension1.ClientID%>").value)
            args.IsValid = (sender.innerHTML == "");
        }

        function funTelefono2_ClientValidate(sender, args) {
            sender.innerHTML = IsTelefono_IndicadorExtension(document.all("<% = txtIndicativo2.ClientID%>").value,
                                                             document.all("<% = txtTelefono2.ClientID%>").value,
                                                             document.all("<% = txtExtension2.ClientID%>").value)
            args.IsValid = (sender.innerHTML == "");
        }

        function IsTelefono_IndicadorExtension(ind, tel, ext) {
            if (tel.length % 7 > 0) {
                return "Ingrese los 7 dígitos del teléfono";
            } else {
                if ((ind == "") != (tel == "")) {
                    return "El teléfono se debe ingresar simultáneo a el indicativo";
                } else
                    if ((ext != "") && (tel == "")) {
                        return "El teléfono se debe ingresar cuando existe extensión";
                    } else {
                        return "";
                    }
            }
        }

        function EsNumero(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function CheckLengthPrimerNombre(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");
            var flagEspacios = false;
            if (args.Value.length >= 2) {
                //----validar que no tenga espcaios al inicio y final del nombre
                //for (i = 0; i <= strSplitDatos.length - 1; i++) {
                //    if (strSplitDatos[i] == ' ') {
                //        args.IsValid = false;
                //        sender.innerHTML = "No se permiten espacios en blanco en su primer nombre";
                //        flagEspacios = true;
                //        break;
                //    }
                //}
                if (!flagEspacios) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su primer nombre";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "Primer nombre inválido";
            }
        }

        function CheckLengthSegundoNombre(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (args.Value.length >= 2) {
                if (args.Value.length == 2) {
                    if (strSplitDatos[0] == (strSplitDatos[1])) {
                        args.IsValid = false;
                        sender.innerHTML = "Caracteres iguales no válidos, registre su segundo nombre";
                    }
                    else {
                        args.IsValid = true;
                    }
                }
                if (args.Value.length > 2) {
                    args.IsValid = true;
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "Segundo nombre inválido";
            }
        }

        function CheckLengthPrimerApellido(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");
            var flagEspacios = false;
            //for (i = 0; i <= strSplitDatos.length - 1; i++) {
            //    if (strSplitDatos[i] == ' ') {
            //        args.IsValid = false;
            //        sender.innerHTML = "No se permiten espacios en blanco en su primer apellido";
            //        flagEspacios = true;
            //        break;
            //    }
            //}
            if (args.Value.length >= 2) {
                if (!flagEspacios) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su primer apellido";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "Primer apellido  inválido";
            }
        }

        function CheckLengthSegundoApellido(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (args.Value.length >= 2) {
                if (args.Value.length == 2) {
                    if (strSplitDatos[0] == (strSplitDatos[1])) {
                        args.IsValid = false;
                        sender.innerHTML = "Caracteres iguales no válidos, registre su segundo apellido";
                    }
                    else {
                        args.IsValid = true;
                    }
                }
                if (args.Value.length > 2) {
                    args.IsValid = true;
                }
            }
            else if (args.Value.length == 1) {
                args.IsValid = false;
                sender.innerHTML = "Segundo apellido  inválido";
            }
        }

        function Confirmacion() {
            var seleccion = confirm("¿Está seguro de eliminar este documento?");
            if (seleccion)
                return true;
            else
                return false;
        }

        function funValidaRazonSocial(source, args) {
            args.IsValid = validaRazonSocial(args.Value);
        }


        function funValidaNumIdentificacion(source, args) {
            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                args.IsValid = validaNumIdentificacion(args.Value);
            }
        }

        function validaNumIdentificacion(numero) {
            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText != 9) {
                var tipodoc = document.getElementById('<%= ddlTipoDoc.ClientID %>').value;
                if (tipodoc == "1") {
                    if (numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6 && numero.length != 7 && numero.length != 8 && numero.length != 10 && numero.length != 11)
                        return false;
                    else
                        return true;
                }
                if (tipodoc == "2") {
                    if (numero.length != 1 && numero.length != 2 && numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6)
                        return false;
                    else
                        return true;
                }
                if (tipodoc == "7") {
                    if (numero.length != 9)
                        return false;
                    else
                        return true;
                }
                return true;
            }
        }

        function validaRazonSocial(nombre) {
            if (nombre.length > 2) {
                return true;
            } else if (nombre.length == 2) {
                var car1 = nombre.charAt(0);
                var car2 = nombre.charAt(1);

                if (car1 != car2) {
                    return true;
                }
            }
            return false;
        }

        function validaNroDocumento(e) {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>
    <style type="text/css">
        .popup {
            display: none;
        }
    </style>
    <asp:HiddenField ID="hfCorreoElectronico" runat="server" />
    <asp:HiddenField ID="hfValidateDocumentsTerceros" runat="server" Value="0" />
    <asp:HiddenField ID="hfIDDocYNombreAActualizar" runat="server" Value="" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell"></td>
            <td class="Cell">
                <asp:LinkButton ID="lnkQueEsTercero" Style="float: right" runat="server" OnClientClick="return false;" Text="&iquest;Qu&eacute; es un tercero?"></asp:LinkButton>
                <asp:Label runat="server" Text="" ID="lbQueEsTercero" CssClass="popup"></asp:Label>
                <Ajax:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="lnkQueEsTercero"
                    BalloonPopupControlID="lbQueEsTercero" Position="BottomLeft" BalloonStyle="Rectangle"
                    BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                    UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true" />
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                <asp:Label ID="lblConsecutivo" runat="server" Text="Consecutivo interno Registro"></asp:Label>
            </td>
            <td width="50%"></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoInterno" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblCuentaUsuario" runat="server" Text="Cuenta de Usuario"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblCorreoElectronico" runat="server" Text="Correo Electr&oacute;nico"></asp:Label>
                <asp:Label ID="lblCorreoElectronicoAS" runat="server" Text="*"></asp:Label>
                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                    ControlToValidate="txtCuentaCorreo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator runat="server" ID="rfvCorreoElectronico" ErrorMessage="Registre un correo electrónico válido"
                    ControlToValidate="txtCuentaCorreo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic">Registre un correo electrónico válido</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtCuentaUsuario" Width="80%" Enabled="false"></asp:TextBox>
            </td>
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtCuentaCorreo" Width="80%" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteCuentaCorreo" runat="server" TargetControlID="txtCuentaCorreo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-@._" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona o asociaci&oacute;n"></asp:Label>
                <asp:Label ID="lblTipoPersonaAS" runat="server" Text="*"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona o asociaci&oacute;n"
                    ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un tipo de persona o asociaci&oacute;n</asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
                <asp:Label ID="lblTipoDocumentoAS" runat="server" Text="*"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Seleccione un tipo de identificación"
                    ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un tipo de identificaci&oacute;n</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="True" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoDoc" AutoPostBack="True" OnSelectedIndexChanged="ddlTipoDoc_OnSelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlJuridica" Visible="True" Style="width: 100%">
            <tr>
                <td class="Cell" colspan="2">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td class="Cell" width="50%;">
                                <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de identificaci&oacute;n"></asp:Label>
                                <asp:Label ID="lblNumeroDocAS" runat="server" Text="*"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtNumeroDoc"
                                    FilterType="Numbers" ValidChars="/1234567890" />
                                <asp:RegularExpressionValidator runat="server" ID="revNumDocCeroNIT" ErrorMessage="El documento no debe empezar con cero (0)"
                                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic" ValidationExpression="^[A-Za-z1-9]+.*$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator runat="server" ID="revNIT" ErrorMessage="Registre el número de identificación"
                                    ControlToValidate="txtNumeroDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">Registre el número de identificación</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvNIT" runat="server" ControlToValidate="txtNumeroDoc" SetFocusOnError="true"
                                    ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic" ClientValidationFunction="funValidaNumIdentificacion">El número de identificación no es válido</asp:CustomValidator>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razón Social"></asp:Label>
                                <asp:Label ID="lblRazonSocialAS" runat="server" Text="*"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="fteRazonSocial" runat="server" TargetControlID="txtRazonSocial"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvRazonSocial" ErrorMessage="Registre su razón social"
                                    ControlToValidate="txtRazonSocial" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">Registre su raz&oacute;n social</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="customVaildaRazonSocial" runat="server" ControlToValidate="txtRazonSocial"
                                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                                    ClientValidationFunction="funValidaRazonSocial">Caracteres iguales no válidos, registre su raz&oacute;n social</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtNumeroDoc" Width="80%" MaxLength="11"
                                    OnTextChanged="txtNumeroDoc_TextChanged" AutoPostBack="true">
                                </asp:TextBox>
                                <asp:TextBox runat="server" ID="txtDV" Style="text-align: center;" Width="3%" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Width="80%" MaxLength="128"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel runat="server" ID="PnlNatural" Visible="True" Style="width: 100%">
            <tr>
                <td class="Cell" colspan="2">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td class="Cell" width="50%">
                                <asp:Label ID="lblIdentificaNat" runat="server" Text="N&uacute;mero de Identificaci&oacute;n"></asp:Label>
                                <asp:Label ID="lblIdentificaNatAS" runat="server" Text="*"></asp:Label>
                                <%--<Ajax:FilteredTextBoxExtender ID="ftxtDv" runat="server" TargetControlID="txtNumIdentificacion"
                                    FilterType="Numbers" ValidChars="/1234567890" />--%>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="Registre el número de identificación"
                                    ControlToValidate="txtNumIdentificacion" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">Ingrese el n&uacute;mero de Identificaci&oacute;n</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="customValidatxtNumIdentificacion" runat="server" ControlToValidate="txtNumIdentificacion"
                                    SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                                    ClientValidationFunction="funValidaNumIdentificacion">El número de identificación no es válido</asp:CustomValidator>
                                <asp:RegularExpressionValidator runat="server" ID="revNumDocCero" ErrorMessage="El documento no debe empezar con cero (0)"
                                    ControlToValidate="txtNumIdentificacion" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic" ValidationExpression="^[A-Za-z1-9]+.*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="Cell" width="50%">
                                <asp:Label ID="lblFechaExp" runat="server" Text="Fecha de Expedici&oacute;n *"></asp:Label>
                                <asp:CompareValidator ID="cvFechaExp" runat="server" Display="Dynamic" ControlToValidate="cuFechaExp$txtFecha"
                                    ValidationGroup="btnGuardar" ErrorMessage="Fecha inválida" Type="Date" Operator="GreaterThan"
                                    ForeColor="Red" ValueToCompare="31/12/1899">
                                </asp:CompareValidator>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%">
                                <asp:TextBox runat="server" ID="txtNumIdentificacion" Width="80%" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%">
                                <uc1:fecha ID="cuFechaExp" runat="server" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblFechaNac" runat="server" Text="Fecha de Nacimiento *"></asp:Label>
                                <asp:CompareValidator ID="cvFechaNac" runat="server" Display="Dynamic" ControlToValidate="cuFechaNac$txtFecha"
                                    ValidationGroup="btnGuardar" ErrorMessage="Fecha inválida" Type="Date" Operator="GreaterThan"
                                    ForeColor="Red" ValueToCompare="31/12/1899">
                                </asp:CompareValidator>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSexo" runat="server" Text="Sexo *"></asp:Label>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Seleccione un tipo de identificación"
                                    ControlToValidate="ddlSexo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un valor de la lista</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <uc1:fecha ID="cuFechaNac" runat="server" />
                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlSexo">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblPrimerNombe" runat="server" Text="Primer Nombre *"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" InvalidChars=" " ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                <asp:RequiredFieldValidator runat="server" ID="rqFvTxtPrimerNombre" SetFocusOnError="true"
                                    ErrorMessage="Registre su primer nombre" ValidationGroup="btnGuardar" ControlToValidate="txtPrimerNombre"
                                    Display="Dynamic" ForeColor="Red" />
                                <asp:CustomValidator runat="server" ID="cvTxtPrimerNombre" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLengthPrimerNombre" ErrorMessage="Registre su primer nombre"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtPrimerNombre" Display="Dynamic"
                                    ForeColor="Red" />
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSegundoNombe" runat="server" Text="Segundo Nombre"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                <asp:CustomValidator runat="server" ID="cvTxtSegundoNombre" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLengthSegundoNombre" ErrorMessage="Registre su segundo nombre"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtSegundoNombre" Display="Dynamic"
                                    ForeColor="Red" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtPrimerNombre" Width="80%" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Width="80%" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblPrimerApellido" runat="server" Text="Primer Apellido *"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                <asp:RequiredFieldValidator runat="server" ID="rqFvTxtPrimerApellido" SetFocusOnError="true"
                                    ErrorMessage="Registre su primer apellido" ValidationGroup="btnGuardar" ControlToValidate="txtPrimerApellido"
                                    Display="Dynamic" ForeColor="Red" />
                                <asp:CustomValidator runat="server" ID="cvTxtPrimerApellido" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLengthPrimerApellido" ErrorMessage="Registre su primer apellido"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtPrimerApellido" Display="Dynamic"
                                    ForeColor="Red" />
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSegundoApellido" runat="server" Text="Segundo Apellido"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ .,@_():;" />
                                <asp:CustomValidator runat="server" ID="cvTxtSegundoApellido" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLengthSegundoApellido" ErrorMessage="Registre su segundo apellido"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtSegundoApellido" Display="Dynamic"
                                    ForeColor="Red" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtPrimerApellido" Width="80%" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Width="80%" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel runat="server" ID="PnlContacto" Visible="True" Style="width: 100%">
            <tr>
                <td class="Cell" colspan="2">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="lblIndicativo" runat="server" Text="Indicativo"></asp:Label>
                                <asp:Label ID="lblIndicativoAS" runat="server" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator runat="server" ID="rfvIndicativo" SetFocusOnError="true"
                                    ErrorMessage=" Registre el número de indicativo" ValidationGroup="btnGuardar" ControlToValidate="txtIndicativo"
                                    Display="Dynamic" ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="ftbIndicativo" runat="server" TargetControlID="txtIndicativo"
                                    FilterType="Numbers" />
                            </td>
                            <td>
                                <asp:Label ID="lblTelefono" runat="server" Text="Tel&eacute;fono"></asp:Label>
                                <asp:Label ID="lblTelefonoAS" runat="server" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator runat="server" ID="rfvTelefono" SetFocusOnError="true"
                                    ErrorMessage=" Registre el número de teléfono" ValidationGroup="btnGuardar" ControlToValidate="txtTelefono"
                                    Display="Dynamic" ForeColor="Red" />
                                <asp:CustomValidator ID="cvTelefonoIndicativo" runat="server" ErrorMessage="*"
                                    Display="Dynamic" ClientValidationFunction="funTelefono_ClientValidate" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                    ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="ftbTelefono" runat="server" TargetControlID="txtTelefono"
                                    FilterType="Numbers" />
                            </td>
                            <td>
                                <asp:Label ID="lblExtension" runat="server" Text="Extensi&oacute;n"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftbExtension" runat="server" TargetControlID="txtExtension"
                                    FilterType="Numbers" />
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCelular" runat="server" Text="Celular"></asp:Label>
                                <asp:Label ID="lblCelularAS" runat="server" Text="*"></asp:Label>
                                <asp:RequiredFieldValidator runat="server" ID="rfvCelular" SetFocusOnError="true"
                                    ErrorMessage=" Registre el número de celular" ValidationGroup="btnGuardar" ControlToValidate="txtCelular"
                                    Display="Dynamic" ForeColor="Red" />
                                <asp:CustomValidator ID="cvLongCelular" runat="server" ErrorMessage=" Ingrese los 10 dígitos del celular" ControlToValidate="txtCelular"
                                    Display="Dynamic" ClientValidationFunction="funLongCelular_ClientValidate" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                    ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="ftbCelular" runat="server" TargetControlID="txtCelular"
                                    FilterType="Numbers" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtIndicativo" Width="80%" MaxLength="1"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono" Width="80%" MaxLength="7"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExtension" Width="80%" MaxLength="10"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCelular" Width="50%" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Cell" colspan="2">
                    <table width="100%" align="center">
                        <tr>
                            <td colspan="2">
                                <h3 class="lbBloque" style="width: 98%; margin-bottom: 0px;">
                                    <asp:Label ID="LabelTitulo" runat="server" Text="Datos de Ubicaci&oacute;n / Direcci&oacute;n"></asp:Label>
                                </h3>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblDepartamento" runat="server" Text="Departamento"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblMunicipio" runat="server" Text="Municipio"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlDepartamento" Width="70%" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlMunicipio" Width="70%"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td style="width: 90%" colspan="2">
                                <uc2:IcbfDireccion ID="txtDireccionResidencia" runat="server" Requerido="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlDatosAdicionales" Visible="True" Style="width: 100%">
            <tr>
                <td class="Cell" colspan="2">
                    <table width="100%" align="center">
                        <tr>
                            <td colspan="2">
                                <h3 class="lbBloque" style="width: 98%">
                                    <asp:Label ID="lblTitleDatosAdicionales" runat="server" Text="Datos Adicionales"></asp:Label>
                                    <asp:HiddenField ID="hdfIdTerceroDA" runat="server" />
                                </h3>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblNivelInteres" runat="server" Text="Nivel de Inter&eacute;s"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblClaseActividad" runat="server" Text="Clase de Actividad *"></asp:Label>
                                <asp:RequiredFieldValidator runat="server" ID="rfvClaseActividad" ErrorMessage="Seleccione una clase de actividad"
                                    ControlToValidate="ddlClaseActividad" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic" InitialValue="-1"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlNivelInteres" Width="80%">
                                </asp:DropDownList>
                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlClaseActividad" Width="80%"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblObservacion" runat="server" Text="Observaciones"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtObservacion" TextMode="MultiLine" onKeyDown="limitText(this,250);"
                                    onKeyUp="limitText(this,250);" Height="100px" Width="400px"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="fteObservacion" runat="server" TargetControlID="txtObservacion"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                            </td>
                            <td></td>
                        </tr>


                    </table>
                </td>
            </tr>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlReferente" Visible="True" Style="width: 100%">
            <tr>
                <td class="Cell" colspan="2">
                    <table width="100%" align="center">
                        <tr>
                            <td colspan="4">
                                <h3 class="lbBloque" style="width: 98%">
                                    <asp:Label ID="lblTitleReferente" runat="server" Text="Referentes"></asp:Label>
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblReferente1" runat="server" Text="Referente 1" Width="40%" CssClass="Subtitle"></asp:Label>
                                <asp:HiddenField ID="hdfIfReferente1" runat="server" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblDepartamento1" runat="server" Text="Departamento" Width="40%"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblMunicipio1" runat="server" Text="Municipio" Width="40%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:DropDownList runat="server" ID="ddlDepartamento1" Width="70%" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlDepartamento1_SelectedIndexChanged">
                                </asp:DropDownList>

                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlMunicipio1" Width="70%"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblNombre1" runat="server" Text="Nombre"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCorreo1" runat="server" Text="Correo Electr&oacute;nico"></asp:Label>
                                <asp:RegularExpressionValidator runat="server" ID="revCorreo1" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                                    ControlToValidate="txtCorreo1" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:TextBox runat="server" ID="txtNombre1" Width="80%" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCorreo1" Width="80%" MaxLength="50"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftbCorreo1" runat="server" TargetControlID="txtCorreo1"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-@._" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="lblIndicativo1" runat="server" Text="Indicativo"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="fteIndicativo1" runat="server" TargetControlID="txtIndicativo1"
                                    FilterType="Numbers" />
                            </td>
                            <td>
                                <asp:Label ID="lblTelefono1" runat="server" Text="Tel&eacute;fono"></asp:Label>
                                <asp:CustomValidator ID="cvTelefono1Indicativo" runat="server" ErrorMessage="*"
                                    Display="Dynamic" ClientValidationFunction="funTelefono1_ClientValidate" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                    ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="fteTelefono1" runat="server" TargetControlID="txtTelefono1"
                                    FilterType="Numbers" />
                            </td>
                            <td>
                                <asp:Label ID="lblExtension1" runat="server" Text="Extensi&oacute;n"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="fteExtension1" runat="server" TargetControlID="txtExtension1"
                                    FilterType="Numbers" />
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCelular1" runat="server" Text="Celular"></asp:Label>
                                <asp:CustomValidator ID="cvLongCelular1" runat="server" ErrorMessage=" Ingrese los 10 dígitos del celular" ControlToValidate="txtCelular1"
                                    Display="Dynamic" ClientValidationFunction="funLongCelular_ClientValidate" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                    ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="fteCelular1" runat="server" TargetControlID="txtCelular1"
                                    FilterType="Numbers" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtIndicativo1" Width="80%" MaxLength="1"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono1" Width="80%" MaxLength="7"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExtension1" Width="80%" MaxLength="10"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCelular1" Width="50%" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>


                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblReferente2" runat="server" Text="Referente 2" Width="40%" CssClass="Subtitle"></asp:Label>
                                <asp:HiddenField ID="hdfIfReferente2" runat="server" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblDepartamento2" runat="server" Text="Departamento" Width="40%"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblMunicipio2" runat="server" Text="Municipio" Width="40%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:DropDownList runat="server" ID="ddlDepartamento2" Width="70%" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlDepartamento2_SelectedIndexChanged">
                                </asp:DropDownList>

                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlMunicipio2" Width="70%"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblNombre2" runat="server" Text="Nombre"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCorreo2" runat="server" Text="Correo Electr&oacute;nico"></asp:Label>
                                <asp:RegularExpressionValidator runat="server" ID="revCorreo2" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                                    ControlToValidate="txtCorreo2" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:TextBox runat="server" ID="txtNombre2" Width="80%" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCorreo2" Width="80%" MaxLength="50"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftbCorreo2" runat="server" TargetControlID="txtCorreo2"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-@._" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="lblIndicativo2" runat="server" Text="Indicativo"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="fteIndicativo2" runat="server" TargetControlID="txtIndicativo2"
                                    FilterType="Numbers" />
                            </td>
                            <td>
                                <asp:Label ID="lblTelefono2" runat="server" Text="Tel&eacute;fono"></asp:Label>
                                <asp:CustomValidator ID="cvTelefono2Indicativo" runat="server" ErrorMessage="*"
                                    Display="Dynamic" ClientValidationFunction="funTelefono2_ClientValidate" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                    ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="fteTelefono2" runat="server" TargetControlID="txtTelefono2"
                                    FilterType="Numbers" />
                            </td>
                            <td>
                                <asp:Label ID="lblExtension2" runat="server" Text="Extensi&oacute;n"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="fteExtension2" runat="server" TargetControlID="txtExtension2"
                                    FilterType="Numbers" />
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCelular2" runat="server" Text="Celular"></asp:Label>
                                <asp:CustomValidator ID="cvLongCelular2" runat="server" ErrorMessage=" Ingrese los 10 dígitos del celular" ControlToValidate="txtCelular2"
                                    Display="Dynamic" ClientValidationFunction="funLongCelular_ClientValidate" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                    ForeColor="Red" />
                                <Ajax:FilteredTextBoxExtender ID="fteCelular2" runat="server" TargetControlID="txtCelular2"
                                    FilterType="Numbers" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtIndicativo2" Width="80%" MaxLength="1"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono2" Width="80%" MaxLength="7"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExtension2" Width="80%" MaxLength="10"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCelular2" Width="50%" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlEstado" Visible="True" Style="width: 100%">
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblEstado" runat="server" Text="Estado *"></asp:Label>
                </td>
                <td class="Cell"></td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlEstadoTercero">
                    </asp:DropDownList>
                </td>
                <td class="Cell"></td>
            </tr>
        </asp:Panel>
    </table>
    <asp:Panel runat="server" ID="pnlListaObservaciones">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTerceroObservaciones" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />
                            <asp:BoundField HeaderText="Fecha de Observaci&oacute;n" DataField="FechaCrea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelAdjuntos">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td style="width: 100%">
                    <asp:GridView runat="server" ID="gvDocAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto,IdDocumento,MaxPermitidoKB,ExtensionesPermitidas"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocAdjuntos_RowCommand" OnRowEditing="gvDocAdjuntos_RowEditing"
                        OnRowCancelingEdit="gvDocAdjuntos_RowCancelingEdit" OnRowUpdating="gvDocAdjuntos_RowUpdating"
                        OnRowDeleting="gvDocAdjuntos_RowDeleting" OnPageIndexChanging="gvDocAdjuntos_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre Documento" SortExpression="NombreDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreTipoDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (Int16)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link Documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'
                                        Visible="false"></asp:TextBox>
                                    <asp:FileUpload ID="fuDocumento" runat="server"></asp:FileUpload>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Extensiones Permitidas">
                                <ItemTemplate>
                                    <asp:Label ID="lblExtensionesPermitidas" runat="server" Text='<%# Bind("ExtensionesPermitidas") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Máximo Permitido KB">
                                <ItemTemplate>
                                    <asp:Label ID="lblMaxPermitidoKB" runat="server" Text='<%# string.Format("{0}", (Int32)Eval("MaxPermitidoKB") )%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEditar" ImageUrl="~/Image/btn/attach.jpg"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Edit" ToolTip="Editar"></asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgEliminar" ImageUrl="~/Image/btn/delete.gif"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Delete" ToolTip="Eliminar"
                                        Visible='<%# !(bool)(Request.QueryString["oP"] == "E") %>' OnClientClick="return confirm('¿Está seguro de eliminar este documento?');"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar"></asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <asp:HiddenField ID="hdUpdatedDoc" runat="server" Value="0" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pObservacionesCorreccion" Visible="False">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 100%">
                    <asp:Label ID="lblObservacionesCorreccion" runat="server" Text="Observaciones"></asp:Label>
                    <asp:Label ID="lblObservacionesCorreccionAS" runat="server" Text="*"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvlblObservacionesCorreccion" ErrorMessage="Campo Obligatorio"
                        ControlToValidate="txtObservacionesCorreccion" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic">Campo Obligatorio</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td style="width: 100%">
                    <asp:TextBox runat="server" ID="txtObservacionesCorreccion" Width="100%" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteObservacionesCorreccion" runat="server" TargetControlID="txtObservacionesCorreccion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters, Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
