<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Proveedor_Detail" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<%@ Register Src="../../../General/General/Control/IcbfDireccion.ascx" TagName="IcbfDireccion" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script>

        function CheckLength(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (strSplitDatos[0] == (strSplitDatos[1])) {
                args.IsValid = false;
                sender.errormessage = "Caracteres iguales no v�lidos";
            } else {
                args.IsValid = (args.Value.length >= 2);
                if (args.IsValid == false) {
                    sender.errormessage = "No cumple con la longitud permitida";
                }
            }
        }
    </script>

    <style type="text/css">
        .popup {
            display: none;
        }
    </style>
    <asp:HiddenField ID="hfCorreoElectronico" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell"></td>
            <td class="Cell">
                <asp:LinkButton ID="lnkQueEsTercero" Style="float: right;" runat="server" OnClientClick="return false;">Que es un tercero?</asp:LinkButton>
                <asp:Label runat="server" Text="" ID="lbQueEsTercero" CssClass="popup"></asp:Label>
                <Ajax:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="lnkQueEsTercero"
                    BalloonPopupControlID="lbQueEsTercero" Position="BottomLeft" BalloonStyle="Rectangle"
                    BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                    UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true" />
                <br />
                <asp:LinkButton runat="server" ID="lbRegistroDenuncia" Text="Registrar Denuncia de Bienes" 
                    PostBackUrl="~/Page/Mostrencos/RegistroDenuncia/List.aspx" Style="float: right;"></asp:LinkButton>
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                <asp:Label ID="lblConsecutivo" runat="server" Text="Consecutivo interno Registro"></asp:Label>
            </td>
            <td width="50%"></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivoInterno" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblCuentaUsuario" runat="server" Text="Cuenta de Usuario"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblCorreoElectronico" runat="server" Text="Correo Electr&oacute;nico "></asp:Label>
                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&acute;lida, int&ecute;ntelo de nuevo"
                    ControlToValidate="txtCuentaCorreo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator runat="server" ID="rfvCorreoElectronico" ErrorMessage="Correo electr�nico en blanco, Registre un correo v�lido"
                    ControlToValidate="txtCuentaCorreo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic">Correo electr�nico en blanco, Registre un correo v�lido</asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%">
                <asp:TextBox runat="server" ID="txtCuentaUsuario" Width="80%" Enabled="false"></asp:TextBox>
            </td>
            <td class="Cell" width="50%">
                <asp:TextBox runat="server" ID="txtCuentaCorreo" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona o asociaci&oacute;n"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="True"></asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona o asociaci&oacute;n"
                    ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>

            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoDoc"></asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Seleccione un tipo de identificaci�n"
                    ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>

            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="pnlJuridica" Visible="True" Style="width: 100%">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td class="Cell" width="50%;">
                                <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de identificaci&oacute;n" Width="76%"></asp:Label>DV
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Raz&oacute;n Social" Width="25%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtNumeroDoc" onkeypress="return EsNumero(event)"
                                    Width="74%"></asp:TextBox>
                                <asp:TextBox runat="server" ID="txtDV" Width="3%"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Width="80%" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="fteRazonSocial" runat="server" TargetControlID="txtRazonSocial"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <asp:RequiredFieldValidator runat="server" ID="rfvRazonSocial" ErrorMessage="Registre su raz�n social"
                                    ControlToValidate="txtRazonSocial" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="PnlNatural" Visible="True" Style="width: 100%">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td class="Cell" width="50%">
                                <asp:Label ID="lblIdentificaNat" runat="server" Text="N&uacute;mero de Identificaci&oacute;n"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblFechaExp" runat="server" Text="Fecha de Expedici&oacute;n"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtNumIdentificacion" onkeypress="return EsNumero(event)"></asp:TextBox>

                            </td>
                            <td class="Cell">
                                <uc1:fecha ID="cuFechaExp" runat="server" Requerid="true" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblFechaNac" runat="server" Text="Fecha de Nacimiento"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSexo" runat="server" Text="Sexo"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <uc1:fecha ID="cuFechaNac" runat="server" Requerid="true" />
                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlSexo"></asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Seleccione un tipo de identificaci�n"
                                    ControlToValidate="ddlSexo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un valor de la lista</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblPrimerNombe" runat="server" Text="Primer Nombre"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSegundoNombe" runat="server" Text="Segundo Nombre"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtPrimerNombre" Width="80%"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <asp:RequiredFieldValidator runat="server" ID="rfvPrimerNombre" ErrorMessage="Dato en blanco, Registre su primer nombre"
                                    ControlToValidate="txtPrimerNombre" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="server" TargetControlID="rfvPrimerNombre"
                                    WarningIconImageUrl="../../../Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle" />
                                <asp:CustomValidator runat="server" ID="cvTxtPrimerNombre" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLength" ErrorMessage="registre su primer nombre"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtPrimerNombre" Display="Dynamic"
                                    ForeColor="Red" Text="*"></asp:CustomValidator>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Width="80%"></asp:TextBox>
                                <%--<Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <asp:CustomValidator runat="server" ID="cvTxtSegundoNombre" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLength" ErrorMessage="registre su segundo nombre"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtSegundoNombre" Display="Dynamic"
                                    ForeColor="Red" Text="*"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblPrimerApellido" runat="server" Text="Primer Apellido"></asp:Label>

                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSegundoApellido" runat="server" Text="Segundo Apellido"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtPrimerApellido" Width="80%"></asp:TextBox>
                                <%--<Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <asp:RequiredFieldValidator runat="server" ID="rfvPrimerApellido" ErrorMessage="Dato en blanco, Registre su primer apellido"
                                    ControlToValidate="txtPrimerApellido" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="server" TargetControlID="rfvPrimerApellido"
                                    WarningIconImageUrl="../../../Image/Proveedores/warning.png" CssClass="CustomValidatorCalloutStyle" />
                                <asp:CustomValidator runat="server" ID="cvTxtPrimerApellido" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLength" ErrorMessage="registre su primer apellido"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtPrimerApellido" Display="Dynamic"
                                    ForeColor="Red" Text="*"></asp:CustomValidator>

                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Width="80%"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <asp:CustomValidator runat="server" ID="cvTxtSegundoApellido" SetFocusOnError="True"
                                    ClientValidationFunction="CheckLength" ErrorMessage="registre su segundo apellido"
                                    ValidationGroup="btnGuardar" ControlToValidate="txtSegundoApellido" Display="Dynamic"
                                    ForeColor="Red" Text="*"></asp:CustomValidator>

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="PnlContacto" Visible="True" Style="width: 100%">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="lblIndicativo" runat="server" Text="Indicativo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTelefono" runat="server" Text="Tel&eacute;fono"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblExtension" runat="server" Text="Extensi&oacute;n"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCelular" runat="server" Text="Celular"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtIndicativo" Width="80%" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono" Width="80%" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExtension" Width="80%" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCelular" Width="50%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" align="center">
                   
                        <tr >
                            <td colspan="2">
                                <h3 class="lbBloque" style="width: 98%">
                                    <asp:Label ID="LabelTitulo" runat="server" Text="Datos de Ubicaci&oacute;n / Direcci&oacute;n"></asp:Label>
                                </h3>
                            </td>
                        </tr>
     <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblDepartamento" runat="server" Text="Departamento" Width="40%"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblMunicipio" runat="server" Text="Municipio" Width="40%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlDepartamento" Width="70%" Enabled="false">
                                </asp:DropDownList>

                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlMunicipio" Width="70%" Enabled="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td style="width: 90%" colspan="2">
                                <uc2:IcbfDireccion ID="txtDireccionResidencia" runat="server" Requerido="false" Enabled="false"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="pnlDatosAdicionales" Visible="True" Style="width: 100%">

                    <table width="100%" align="center">
                        <tr>
                            <td colspan="2">
                                <h3 class="lbBloque" style="width: 98%">
                                    <asp:Label ID="lblTitleDatosAdicionales" runat="server" Text="Datos Adicionales"></asp:Label>
                                    <asp:HiddenField ID="hdfIdTerceroDA" runat="server" />
                                </h3>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblNivelInteres" runat="server" Text="Nivel de Inter&eacute;s"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblClaseActividad" runat="server" Text="Clase de Actividad"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlNivelInteres" Width="80%"  Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlClaseActividad" Width="80%"  Enabled="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblObservacion" runat="server" Text="Observaciones" ></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtObservacion" TextMode="MultiLine"  Enabled="false"
                                    Height="100px" Width="400px"></asp:TextBox>
                             </td>
                            <td></td>
                        </tr>


                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="pnlReferente" Visible="True" Style="width: 100%">
                    <table width="100%" align="center">
                        <tr >
                            <td colspan="4">
                                <h3 class="lbBloque" style="width: 98%">
                                    <asp:Label ID="lblTitleReferente" runat="server" Text="Referentes"></asp:Label>
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblReferente1" runat="server" Text="Referente 1" Width="40%" CssClass="Subtitle"></asp:Label>
                                <asp:HiddenField ID="hdfIfReferente1" runat="server" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblDepartamento1" runat="server" Text="Departamento" Width="40%"></asp:Label>
                            </td>
                            <td class="Cell" >
                                <asp:Label ID="lblMunicipio1" runat="server" Text="Municipio" Width="40%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:DropDownList runat="server" ID="ddlDepartamento1" Width="70%"  Enabled="false">
                                </asp:DropDownList>

                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlMunicipio1" Width="70%"  Enabled="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblNombre1" runat="server" Text="Nombre"  Enabled="false"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCorreo1" runat="server" Text="Correo Electr&oacute;nico"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:TextBox runat="server" ID="txtNombre1" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                            <td class="Cell" >
                                <asp:TextBox runat="server" ID="txtCorreo1" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="lblIndicativo1" runat="server" Text="Indicativo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTelefono1" runat="server" Text="Tel&eacute;fono"></asp:Label>
                                </td>
                            <td>
                                <asp:Label ID="lblExtension1" runat="server" Text="Extensi&oacute;n"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCelular1" runat="server" Text="Celular"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtIndicativo1" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono1" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExtension1" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCelular1" Width="50%"  Enabled="false"></asp:TextBox>
                            </td>
                        </tr>

                    
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblReferente2" runat="server" Text="Referente 2" Width="40%" CssClass="Subtitle"></asp:Label>
                                <asp:HiddenField ID="hdfIfReferente2" runat="server" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblDepartamento2" runat="server" Text="Departamento" Width="40%"></asp:Label>
                            </td>
                            <td class="Cell" >
                                <asp:Label ID="lblMunicipio2" runat="server" Text="Municipio" Width="40%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:DropDownList runat="server" ID="ddlDepartamento2" Width="70%"  Enabled="false">
                                </asp:DropDownList>

                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlMunicipio2" Width="70%"  Enabled="false"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" colspan="3">
                                <asp:Label ID="lblNombre2" runat="server" Text="Nombre"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCorreo2" runat="server" Text="Correo Electr&oacute;nico"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="3">
                                <asp:TextBox runat="server" ID="txtNombre2" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                            <td class="Cell" >
                                <asp:TextBox runat="server" ID="txtCorreo2" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="lblIndicativo2" runat="server" Text="Indicativo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTelefono2" runat="server" Text="Tel&eacute;fono"></asp:Label>
                             </td>
                            <td>
                                <asp:Label ID="lblExtension2" runat="server" Text="Extensi&oacute;n"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblCelular2" runat="server" Text="Celular"></asp:Label>
                               </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox runat="server" ID="txtIndicativo2" Width="80%"  Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono2" Width="80%" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExtension2" Width="80%" Enabled="false"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox runat="server" ID="txtCelular2" Width="50%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
            </td>
            <td class="Cell"></td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlEstadoTercero"></asp:DropDownList>
            </td>
            <td class="Cell"></td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlObservaciones">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObservaciones" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:TemplateField HeaderText="�Confirma que los datos coinciden con el documento adjunto y lo aprueba?">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("ConfirmaYAprueba") ? "Si" : "No") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Observaci&oacute;n" DataField="Observaciones" />--%>
                            <asp:TemplateField HeaderText="Observaci&oacute;n">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtObservaciones" runat="server" Enabled="false" Text='<%# Eval("Observaciones") %>' TextMode="MultiLine"
                                        Width="300px" Rows="8"></asp:TextBox>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Observaci&oacute;n" DataField="FechaCrea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
    <asp:Panel runat="server" ID="PanelAdjuntos">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px"
                        DataKeyName="IDDOCADJUNTO" OnRowCommand="gvAdjuntos_RowCommand"
                        OnSelectedIndexChanged="gvAdjuntos_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreTipoDocumento" />
                            <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>

                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'></asp:Label>--%>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="viewLogo" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                        CommandArgument='<%#Eval("LinkDocumento")%>'
                                        Height="16px" Width="16px" ToolTip="Detalle" Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
</asp:Content>
