using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Seguridad.Service;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de terceros
/// </summary>
public partial class Page_Proveedor_List : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/GestionTercero";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRUBOService = new SIAService();
    bool _sorted = false;
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(Master, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();

                if (GetState(Page.Master, PageName))
                {
                    CargarTipoDocumentotipoPersona();
                    GetState(Page.Master, PageName);
                    Buscar();
                }
            }
        }
    }
    
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Proveedor/blank.aspx");
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdjuntar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/" + PageName + "/CargarTercero.aspx");
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        
        NavigateTo(SolutionPage.Add);

    }
    
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarTipoDocumentotipoPersona();
    }

    private void CargarTipoDocumentotipoPersona()
    {
        string idTipoPersona = new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
        }
    }

    protected void gvProveedor_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProveedor.SelectedRow);
    }
    
    protected void gvProveedor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedor.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Manejador de evento OnSelectedIndexChanged para el dropdownlist ddlTipoDoc
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoDoc_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumeroDoc.Text = String.Empty;
            this.txtNumeroDoc.Focus();
        }
    }

    #endregion

    #region M�todos

    private void Buscar()
    {
        try
        {
            CargarGrilla(gvProveedor, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el c�digo de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdTipoPersona = 0;
            int vIdTipoDocIdentifica = 0;
            int? vClaseActividad = null;
            string vNumeroIdentificacion = "";
            string vTercero = null;
            int vEstado = 0;

            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDoc.SelectedValue != "")
            {
                vIdTipoDocIdentifica = int.Parse(ddlTipoDoc.SelectedValue);
            }
            if (txtNumeroDoc.Text != "")
            {
                vNumeroIdentificacion = txtNumeroDoc.Text.Trim();
            }
            if (txtTercero.Text.Trim() != "")
            {
                vTercero = txtTercero.Text;
            }
            if (ddlEstado.SelectedValue != "-1")
            {
                vEstado = int.Parse(ddlEstado.SelectedValue);
            }
            if (ddlActividad.SelectedValue != "-1")
            {
                vClaseActividad = int.Parse(ddlActividad.SelectedValue);
            }

            //string vUsuario = GetSessionUser().NombreUsuario;
            string vUsuario = null;
            var myGridResults = vProveedorService.ConsultarTerceros(vIdTipoDocIdentifica, vEstado, vIdTipoPersona, vNumeroIdentificacion, vUsuario, vTercero, vClaseActividad);

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Tercero), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<Tercero, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //Cargar datos si es usuario  Externo
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {

                //Se pregunta si existe el tercero si no existe se lleva a creaci�n y si existe se lleva a detail
                Tercero vTercero = vProveedorService.ConsultarTerceroProviderUserKey(GetSessionUser().Providerkey.ToUpper());
                if (vTercero.IdTercero == 0)
                    NavigateTo(SolutionPage.Add);
                else
                {
                    SetSessionParameter("Proveedor.IdTercero", vTercero.IdTercero);
                    NavigateTo(SolutionPage.Detail);
                }
            }
            else
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                toolBar.eventoAdjuntar += new ToolBarDelegate(btnAdjuntar_Click);
                mostrarParametrosBusqueda(true);
            }


            gvProveedor.PageSize = PageSize();
            gvProveedor.EmptyDataText = EmptyDataText();
            toolBar.EstablecerTitulos("Listar Terceros", SolutionPage.List.ToString());

            var vSeguridadService = new SeguridadService();
            lbQueEsTercero.Text = vSeguridadService.ConsultarParametro(29) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(29).ValorParametro;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Habilita visibilidad de panel de b�squeda
    /// </summary>
    /// <param name="p"></param>
    private void mostrarParametrosBusqueda(bool p)
    {
        pnlConsulta.Visible = p;
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strCorreoValue = gvProveedor.DataKeys[rowIndex].Values[0].ToString();
            string strIdTerceroValue = gvProveedor.DataKeys[rowIndex].Values[1].ToString();
            SetSessionParameter("Proveedor.CorreoElectronico", strCorreoValue);
            SetSessionParameter("Proveedor.IdTercero", strIdTerceroValue);
            Response.Redirect("~/page/" + PageName + "/Detail.aspx");
            //NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles.ClaseActividad(ddlActividad, "-1", true);
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstado, "-1", true);
            if (GetSessionParameter("Proveedor.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Proveedor.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// CambiarValidadorDocumentoUsuario cambia tama�o de texto dependiendo del tipo de documento seleccionado
    /// </summary>
    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDoc.SelectedItem.Text == "CC" || this.ddlTipoDoc.SelectedItem.Text == "NIT" || this.ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.txtNumeroDoc.MaxLength = 11;
        }
        else if (this.ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.txtNumeroDoc.MaxLength = 6;
        }
        else
        {
            this.txtNumeroDoc.MaxLength = 17;
        }
    }

    #endregion

    #region ordenarGrilla

    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvProveedor_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    #endregion

    
}
