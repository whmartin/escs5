using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Data;
using Icbf.Seguridad.Service;

/// <summary>
/// P�gina que despliega el detalle del registro de tercero
/// </summary>
public partial class Page_Proveedor_Detail : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/GestionTercero";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRUBOService = new SIAService();
    Tercero vTercero = new Tercero();

    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {

            if (!Page.IsPostBack)
            {
                toolBar.LipiarMensajeError();
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Proveedor.CorreoElectronico");
        RemoveSessionParameter("Proveedor.IdTercero");
        NavigateTo(SolutionPage.Add);

    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/page/" + PageName + "/Add.aspx?oP=E");
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            String vCorreoElectronico = Convert.ToString(GetSessionParameter("Proveedor.CorreoElectronico"));
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));

            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));
            txtCuentaUsuario.Text = vTercero.NumeroIdentificacion;
            //Consecutivo interno
            txtConsecutivoInterno.Text = vTercero.ConsecutivoInterno;
            if (vTercero.Email != null)
                txtCuentaCorreo.Text = vTercero.Email.ToLower();

            ddlTipoPersona.SelectedValue = vTercero.IdTipoPersona.ToString();
            TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
            if (tipoper.CodigoTipoPersona == "001")
            {
                ViewControlJuridica(true);
                ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
            }
            else
            {
                ViewControlJuridica(false);
                ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
            }

            ddlTipoDoc.SelectedValue = vTercero.IdDListaTipoDocumento.ToString();
            txtNumeroDoc.Text = vTercero.NumeroIdentificacion;
            txtNumIdentificacion.Text = vTercero.NumeroIdentificacion;
            txtDV.Text = vTercero.DigitoVerificacion.ToString();

            if (vTercero.FechaExpedicionId != null)
                cuFechaExp.Date = DateTime.Parse(vTercero.FechaExpedicionId.ToString());
            if (vTercero.FechaNacimiento != null)
                cuFechaNac.Date = DateTime.Parse(vTercero.FechaNacimiento.ToString());
            ddlSexo.SelectedValue = vTercero.Sexo;
            txtRazonSocial.Text = vTercero.RazonSocial;
            txtPrimerNombre.Text = vTercero.PrimerNombre;
            txtSegundoNombre.Text = vTercero.SegundoNombre;
            txtPrimerApellido.Text = vTercero.PrimerApellido;
            txtSegundoApellido.Text = vTercero.SegundoApellido;

            txtIndicativo.Text = vTercero.Indicativo;
            txtTelefono.Text = vTercero.Telefono;
            txtExtension.Text = vTercero.Extension;
            txtCelular.Text = vTercero.Celular;


            if (vTercero.IdDepartamento != 0)
                ddlDepartamento.SelectedValue = vTercero.IdDepartamento.ToString();

            CargarMunicipio(ddlDepartamento, ddlMunicipio);
            if (vTercero.IdMunicipio != 0)
                ddlMunicipio.SelectedValue = vTercero.IdMunicipio.ToString();


            if (vTercero.IdZona.HasValue)
            {
                txtDireccionResidencia.TipoZona = vTercero.IdZona == 1 ? "U" : "R";
            }

            txtDireccionResidencia.Text = (vTercero.Direccion != null ? vTercero.Direccion : "");
            txtDireccionResidencia.Enabled = false;

            ddlEstadoTercero.SelectedValue = vTercero.IdEstadoTercero.ToString();


            CamposDatosAdicionales(vTercero.IdEstadoTercero);

            if (!new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
            {
                TerceroDatoAdicional vTerceroDatoAdicional = vOferenteService.ConsultarTerceroDatoAdicional(int.Parse(vIdTercero));

                if (vTerceroDatoAdicional != null && vTerceroDatoAdicional.IdTercero != 0)
                {
                    hdfIdTerceroDA.Value = vTerceroDatoAdicional.IdTercero.ToString();
                    ddlNivelInteres.SelectedValue = vTerceroDatoAdicional.NivelInteres;
                    ddlClaseActividad.SelectedValue = vTerceroDatoAdicional.IdClaseActividad.ToString();
                    txtObservacion.Text = vTerceroDatoAdicional.Observaciones;
                }

                List<Referente> vLstReferente = vOferenteService.ConsultarReferentes(int.Parse(vIdTercero));
                if (vLstReferente.Count > 0)
                {
                    hdfIfReferente1.Value = vLstReferente[0].IdReferente.ToString();

                    ddlDepartamento1.SelectedValue = vLstReferente[0].IdDepartamento.ToString();
                    CargarMunicipio(ddlDepartamento1, ddlMunicipio1);
                    ddlMunicipio1.SelectedValue = vLstReferente[0].IdMunicipio.ToString();

                    txtNombre1.Text = vLstReferente[0].Nombre;
                    txtCorreo1.Text = vLstReferente[0].Correo;
                    txtIndicativo1.Text = vLstReferente[0].Indicativo;
                    txtTelefono1.Text = vLstReferente[0].Telefono;
                    txtExtension1.Text = vLstReferente[0].Extension;
                    txtCelular1.Text = vLstReferente[0].Celular;
                }
                if (vLstReferente.Count > 1)
                {
                    hdfIfReferente2.Value = vLstReferente[1].IdReferente.ToString();

                    ddlDepartamento2.SelectedValue = vLstReferente[1].IdDepartamento.ToString();
                    CargarMunicipio(ddlDepartamento2, ddlMunicipio2);
                    ddlMunicipio2.SelectedValue = vLstReferente[1].IdMunicipio.ToString();

                    txtNombre2.Text = vLstReferente[1].Nombre;
                    txtCorreo2.Text = vLstReferente[1].Correo;
                    txtIndicativo2.Text = vLstReferente[1].Indicativo;
                    txtTelefono2.Text = vLstReferente[1].Telefono;
                    txtExtension2.Text = vLstReferente[1].Extension;
                    txtCelular2.Text = vLstReferente[1].Celular;
                }
            }


            ddlEstadoTercero.SelectedValue = vTercero.IdEstadoTercero.ToString();
            cargarGrillaImagenes(int.Parse(vIdTercero));

            cargarObservaciones();
            BuscarDocumentos();
            deshabilitarTercero(false);

            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                lblEstado.Visible = false;
                ddlEstadoTercero.Visible = false;
            }
            if (Request.QueryString["oP"] == "E")
            {
                toolBar.MostrarMensajeGuardado("Datos registrados de forma exitosa");
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga observaciones en grilla
    /// </summary>
    private void cargarObservaciones()
    {

        String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
        gvObservaciones.DataSource = vProveedorService.ConsultarValidarTerceros(Convert.ToInt32(vIdTercero), null, null);
        gvObservaciones.DataBind();

    }
    protected void gvAdjuntos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int rowIndex = gvAdjuntos.SelectedIndex;
        string strValue = gvAdjuntos.DataKeys[rowIndex].Value.ToString();
        ViewState["IdDocTercero"] = strValue;
    }

    /// <summary>
    /// Busca Documentos de Tercero y llena grilla
    /// </summary>
    private void BuscarDocumentos()
    {
        try
        {
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
            string sPersona = ddlTipoPersona.SelectedValue;
            gvAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(Convert.ToInt32(vIdTercero), sPersona, "");
            gvAdjuntos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region m�todos
    /// <summary>
    /// Deshabilita controles si es Tercero
    /// </summary>
    /// <param name="p"></param>
    private void deshabilitarTercero(bool p)
    {
        txtCuentaCorreo.Enabled = p;
        txtCuentaUsuario.Enabled = p;
        ddlTipoPersona.Enabled = p;
        ddlTipoDoc.Enabled = p;
        txtNumeroDoc.Enabled = p;
        txtNumIdentificacion.Enabled = p;
        txtDV.Enabled = p;
        cuFechaExp.Enabled = p;
        cuFechaNac.Enabled = p;
        ddlSexo.Enabled = p;
        txtRazonSocial.Enabled = p;
        txtPrimerNombre.Enabled = p;
        txtPrimerApellido.Enabled = p;
        txtSegundoNombre.Enabled = p;
        txtSegundoApellido.Enabled = p;
        ddlEstadoTercero.Enabled = p;
    }

    /// <summary>
    /// Habilita controles si es Entrada Tercero
    /// </summary>
    /// <param name="p"></param>
    private void habilitarEntradaTercero(bool p)
    {
        txtCuentaCorreo.Enabled = p;
        cuFechaExp.Enabled = p;
        ddlSexo.Enabled = p;
        txtRazonSocial.Enabled = p;
        txtPrimerNombre.Enabled = p;
        txtPrimerApellido.Enabled = p;
        txtSegundoNombre.Enabled = p;
        txtSegundoApellido.Enabled = p;
        txtNumeroDoc.Enabled = p;
        txtNumIdentificacion.Enabled = p;
    }

    /// <summary>
    /// Habilita la seccion de datos adicionales para proveedores
    /// </summary>
    /// <param name="mostrar"></param>
    private void CamposDatosAdicionales(int? Estado)
    {
        string currentApp = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
        bool p = (currentApp == "F" ? !new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES") : false);

        pnlDatosAdicionales.Visible = p;
        pnlReferente.Visible = p;

        if (Estado == vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero)//Se obtiene el estado Por Validar
        {
            PnlContacto.Enabled = false;
            txtDireccionResidencia.Requerido = false;
            txtDireccionResidencia.Enabled = false;

            pnlDatosAdicionales.Enabled = false;
            pnlReferente.Enabled = false;
        }

    }
    /// <summary>
    /// Habilita visibilidad de controles si la persona es jur�dica
    /// </summary>
    /// <param name="p"></param>
    private void ViewControlJuridica(bool p)
    {
        PnlNatural.Visible = p;
        cuFechaExp.Visible = p;
        lblFechaExp.Visible = p;
        lblRazonSocial.Visible = !p;
        txtRazonSocial.Visible = !p;
        pnlJuridica.Visible = !p;
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Detalle Tercero", SolutionPage.Detail.ToString());
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {

                Tercero lista = new Tercero();
                lista = vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(vusuario.IdTipoDocumento, vusuario.NumeroDocumento);
                int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
                int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
                int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado

                if (lista.IdEstadoTercero == idEstadoValidado || lista.IdEstadoTercero == idEstadoRegistrado || lista.IdEstadoTercero == idEstadoPorAjustar || vTercero.IdEstadoTercero == idEstadoPorValidar)
                {
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }
                else
                {
                    habilitarEntradaTercero(false);
                    txtNumeroDoc.Enabled = false;
                    txtNumIdentificacion.Enabled = false;
                    lblEstado.Visible = false;
                    ddlEstadoTercero.Visible = false;
                }



            }
            else
            {
                String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
                Tercero vTercero = new Tercero();
                vTercero = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));
                //Deshabilita los campos del tercero
                int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
                int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
                int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado PorValidar
                if (vTercero.IdEstadoTercero == idEstadoRegistrado || vTercero.IdEstadoTercero == idEstadoPorAjustar || vTercero.IdEstadoTercero == idEstadoValidado)
                {
                    //if (vTercero.IdEstadoTercero == idEstadoValidado)
                    //{
                    //    if (vTercero.IdTipoPersona == 2 || vTercero.IdTipoPersona == 3 || vTercero.IdTipoPersona == 4)
                    //    {
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                    toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                    toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                    //    }
                    //}
                    //else
                    //{
                    //    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                    //    toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                    //    toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                    //}
                }
                else if (vTercero.IdEstadoTercero == idEstadoPorValidar)
                {
                    toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }


            }

            var vSeguridadService = new SeguridadService();
            lbQueEsTercero.Text = vSeguridadService.ConsultarParametro(29) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(29).ValorParametro;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlSexo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlSexo.Items.Insert(0, new ListItem("Masculino", "M"));
            ddlSexo.Items.Insert(0, new ListItem("Femenino", "F"));
            ddlSexo.SelectedValue = "-1";
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstadoTercero, "-1", true);
            ManejoControles.LlenarTipoDocumento(ddlTipoDoc, "-1", true);

            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento, "-1", true);
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento1, "-1", true);
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento2, "-1", true);

            hdfIfReferente1.Value = "";
            ddlNivelInteres.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlNivelInteres.Items.Insert(1, new ListItem("Alto", "Alto"));
            ddlNivelInteres.Items.Insert(2, new ListItem("Medio", "Medio"));
            ddlNivelInteres.Items.Insert(3, new ListItem("Bajo", "Bajo"));
            ddlNivelInteres.SelectedValue = "-1";
            ManejoControles.ClaseActividad(ddlClaseActividad, "-1", true);

            hdfIfReferente2.Value = "";
            hdfIdTerceroDA.Value = "";

            //Cargar datos si es usuario  Externo
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            //Tercero vTercero = new Tercero();
            Tercero vTercero = vProveedorService.ConsultarTerceroProviderUserKey(GetSessionUser().Providerkey.ToUpper());
            //vTercero = vOferenteService.ConsultarTerceroProviderUserKey(vusuario.Providerkey);
            if (vTercero != null)
            {
                ViewState["IdTercero"] = vTercero.IdTercero;
                Icbf.Oferente.Entity.EstadoTercero estadotercero = vProveedorService.ConsultarEstadoTercero(int.Parse(ddlEstadoTercero.SelectedValue));
                if (estadotercero.CodigoEstadoTercero == "002" || estadotercero.CodigoEstadoTercero == "003")
                {
                    toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }
                if (estadotercero.CodigoEstadoTercero == "001" || estadotercero.CodigoEstadoTercero == "004")
                {
                    toolBar.eventoListar += new ToolBarDelegate(btnBuscar_Click);
                }
                if (vTercero.IdEstadoTercero == vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero)
                {
                    toolBar.MostrarMensajeError("Se encuentra en estado en validaci&oacute;n");
                }
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar datos en grilla
    /// </summary>
    /// <param name="idTercero"></param>
    private void cargarGrillaImagenes(int idTercero)
    {
        try
        {
            List<DocAdjuntoTercero> lista = vProveedorService.ConsultarDocAdjuntoTercero(idTercero, "005", this.PageName);
            if (lista.Count > 0)
            {
                gvAdjuntos.DataSource = lista;
                gvAdjuntos.DataBind();
            }
            else
            {
                List<DocAdjuntoTercero> listaDefecto = vProveedorService.ConsultarDocAdjuntoTercero(null, "005", this.PageName);
                gvAdjuntos.DataSource = listaDefecto;
                gvAdjuntos.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    /// <summary>
    /// Llena lista desplegable corrrespondiente a Municipio filtrado por departamento
    /// </summary>
    protected void CargarMunicipio(DropDownList ddlDep, DropDownList ddlMun)
    {
        CargarMunicipioXDepartamento(ddlMun, Convert.ToInt32(ddlDep.SelectedValue));
    }
    /// <summary>
    /// Llena la lista desplegable Municipio con el filtro departamento
    /// </summary>
    /// <param name="PddlMunicipio"></param>
    /// <param name="pidDepartament"></param>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipio, int pidDepartament)
    {

        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipio, "-1", true, pidDepartament);
    }

    /// <summary>
    /// Comando de rengl�n para la grilla Adjuntos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewLogo":

                    SIAService vRUBOService = new SIAService();

                    MemoryStream ArchivoImagen = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (ArchivoImagen != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", ArchivoImagen.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:650px;resizable:yes;');</script>");

                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se logro obtener la imagen desde el servidor.");
                        return;
                    }

                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
}
