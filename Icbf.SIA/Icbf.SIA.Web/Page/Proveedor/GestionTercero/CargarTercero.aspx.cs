using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Business;
using Icbf.Oferente.Business.Utilidad;
using Icbf.Oferente.DataAccess;
using Icbf.Oferente.Service;

public partial class Page_Proveedor_GestionTercero_CargarTercero : GeneralWeb
{
    #region Propiedades

    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vSiaService = new SIAService();
    DatosGeneralesCargaDAL vDatosCargaDAL = new DatosGeneralesCargaDAL();
    string PageName = "Proveedor/GestionTercero";

    #endregion

    #region "Eventos"

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    public int TipoSalida { get{ return (int)ViewState["TipoSalida"];} set{ViewState["TipoSalida"] = value;} }
    public object ValidacionIngreso { get { return (object)ViewState["ValidacionIngreso"]; } set { ViewState["ValidacionIngreso"] = value; } }
    public object RespuestaValidacion { get { return (object)ViewState["RespuestaValicacion"]; } set { ViewState["RespuestaValicacion"] = value; } }
    
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        this.CargarDatosIniciales();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        this.CargarDatosIniciales();
        Buscar();
    }
    
    protected void grvHistorico_Sorting(object sender, GridViewSortEventArgs e)
    {
        OrdenarHistorico(e);
    }
    
    /// <summary>
    /// Descarga archivo de inconsitencias
    /// </summary>
    /// <param name="sender">Objeto sender</param>
    /// <param name="e">Objeto e</param>
    protected void hlInconsistencias_Click(object sender, EventArgs e)
    {
        DataTable vData = (DataTable)Session["InconsistenciasData"];
        string vFileName = Session["InconsistenciasFileName"].ToString();

        if (vData != null)
            Util.GenerateExcelFromDataTable(Response, vData, vFileName);
    }

    protected void grvHistorico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.grvHistorico.PageIndex = e.NewPageIndex;

        int? IdTipoArchivo = null;

        if (ddlTipoArchivo.SelectedValue != "-1")
            IdTipoArchivo = Convert.ToInt32(ddlTipoArchivo.SelectedValue);  

        this.grvHistorico.DataSource = vDatosCargaDAL.ConsultarDatosGeneralesCargas(null, null, IdTipoArchivo, null, null, null, null);
        this.grvHistorico.DataBind();
     }

    #endregion

    private void Guardar()
    {
        try
        {
            LimpiarGridDatos();

            string vFilePath = CargarArchivoServidor(fuplArchivoCargue);
            if (vFilePath == "1")
            {
                toolBar.MostrarMensajeError("El tipo de archivo seleccionado no es correcto, por favor seleccione un archivo Excel");
                return;
            }

            if (vFilePath == "2")
            {
                toolBar.MostrarMensajeError("Debe cargar un archivo");
                return;
            }

            if (vDatosCargaDAL.ConsultarDatosGeneralesCargas(null, null, null, fuplArchivoCargue.FileName, null, null, null).Count > 0)
            {
                toolBar.MostrarMensajeError("El archivo a cargar ya fue seleccionado anteriormente, por favor verifique el nombre del mismo");
                return;
            }

            int vIdTipoArchivo = Util.GetIntOrDefault(ddlTipoArchivo.SelectedValue);

            CargaArchivoResult vResult = CargarArchivo(vFilePath, this.GetSessionUser().NombreUsuario, this.GetSessionUser().IdUsuario, fuplArchivoCargue.FileName, vIdTipoArchivo);

            if (vResult.NumErrors > 0)
            {
                TipoSalida = 1;
                toolBar.MostrarMensajeError("No se pudo cargar el archivo, descarga archivo para ver inconsistencias");
                Session["InconsistenciasData"] = vResult.DataResult;
                Session["InconsistenciasFileName"] = fuplArchivoCargue.FileName;
                lbInconsistencias.Visible = true;
                pnlResultado.Visible = false;
            }
            else
            {
                TipoSalida = 0;
                lbInconsistencias.Visible = false;
                toolBar.MostrarMensajeGuardado("Información cargada con éxito");
                pnlResultado.Visible = true;
                lblRegistrosBD.Text = vResult.DatosCarga.CantidadRegistrosDB.ToString();
                lblRegistrosExcel.Text = vResult.DatosCarga.CantidadRegistrosExcel.ToString();
                lblArchivoCargado.Text = vResult.DatosCarga.NombreArchivo;
                lblFechaCargue.Text = vResult.DatosCarga.FechaCarga.ToString("MM/dd/yyyy HH:mm:ss");
                lblNombreUsuario.Text = vResult.DatosCarga.UsuarioCrea.ToString();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            lbInconsistencias.Visible = false;
            pnlResultado.Visible = false;
            pnlTablaHistorico.Visible = false;
        }
    }


    /// <summary>
    /// Carga archivo en base de datos
    /// </summary>
    /// <param name="pFilePath">Valor ruta del archivo cargado</param>
    /// <param name="pUsuario">Valor usuario</param>
    /// <param name="pIdUsuario">Valor IdUsuario</param>
    /// <param name="pNombreArchivo">Valor Nombre Archivo</param>
    /// <returns>Entidad CargaArchivoResult</returns>
    public CargaArchivoResult CargarArchivo(string pFilePath, string pUsuario, int pIdUsuario, string pNombreArchivo, int pIdTipoArchivo)
    {
        try
        {
            int vResultado;
            int index = 0;
            int Fila = 0;
            int numErrors = 0;
            String MensajeError = "";

            DataTable vData = Util.GetDataTableFromFile3(pFilePath, pIdTipoArchivo);

            DataTable vErrorData = new DataTable();

            //// agrega columna Fila a DataSetCargado
            DataColumn comentsColumn = new DataColumn();
            comentsColumn.ColumnName = "Fila";
            comentsColumn.AllowDBNull = true;
            comentsColumn.DataType = typeof(int);
            vErrorData.Columns.Add(comentsColumn);

            //// agrega columna Identificación a DataSetCargado
            DataColumn comentsColumn1 = new DataColumn();
            comentsColumn1.ColumnName = "Número de identificación";
            comentsColumn1.AllowDBNull = true;
            comentsColumn1.DataType = typeof(decimal);
            vErrorData.Columns.Add(comentsColumn1);

            //// agrega columna Razon Social a DataSetCargado
            DataColumn comentsColumn2 = new DataColumn();
            comentsColumn2.ColumnName = "Razón social";
            comentsColumn2.AllowDBNull = true;
            comentsColumn2.DataType = typeof(string);
            vErrorData.Columns.Add(comentsColumn2);

            //// agrega columna de inconsistencias a DataSetCargado
            DataColumn comentsColumn3 = new DataColumn();
            comentsColumn3.ColumnName = "Descripción del error";
            comentsColumn3.AllowDBNull = true;
            comentsColumn3.DataType = typeof(string);
            vErrorData.Columns.Add(comentsColumn3);
            
            //// importa cada fila del datatable inicial al que tiene las validaciones por columna
            foreach (DataRow row in vData.Rows)
            {
                try
                {
                    MensajeError = ValidarDatos(row, Convert.ToString(index +2));
                    index++;

                    if (numErrors == 0 && MensajeError == "")
                    {                        
                        bool isPersona = true;

                        Tercero vTercero = new Tercero();

                        int IdTipoPersona = vOferenteService.ConsultarTipoPersonas(null, row[1].ToString(), true)[0].IdTipoPersona;
                        vTercero.IdTipoPersona = IdTipoPersona;
                        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(IdTipoPersona);
                        if (tipoper.CodigoTipoPersona != "001")
                        {
                            isPersona = false;
                        }

                        int IdTipoDocumento = vSiaService.ConsultarTiposDocumento(row[2].ToString(),null)[0].IdTipoDocumento;
                        vTercero.IdDListaTipoDocumento = IdTipoDocumento;

                        if (isPersona)
                        {
                            vTercero.NumeroIdentificacion = row[3].ToString();
                            vTercero.RazonSocial = row[4].ToString().ToUpper();
                        }
                        else
                        {
                            vTercero.NumeroIdentificacion = row[3].ToString();
                            vTercero.RazonSocial = row[4].ToString().ToUpper();
                        }

                        vTercero.DigitoVerificacion = CalcularDiV(vTercero.NumeroIdentificacion);

                        vTercero.Indicativo = row[5].ToString();
                        vTercero.Telefono = row[6].ToString();
                        vTercero.Extension = row[7].ToString();
                        vTercero.Celular = row[8].ToString();

                        if (row[9].ToString().ToUpper() == "URBANA")
                        {
                            vTercero.IdZona = 1;
                        }
                        if (row[9].ToString().ToUpper() == "RURAL")
                        {
                            vTercero.IdZona = 2;
                        }
                        
                        int? IdDepartamento = row[10].ToString() == "" ? 0 : vSiaService.ConsultarDepartamentos(null, null, row[10].ToString())[0].IdDepartamento;
                        if (IdDepartamento == 0) IdDepartamento = null;
                        int IdMunicipio = row[11].ToString() == "" ? 0 : vSiaService.ConsultarMunicipios(IdDepartamento, null, row[11].ToString())[0].IdMunicipio;
                        vTercero.IdMunicipio = IdMunicipio;

                        vTercero.Email = row[12].ToString().ToUpper();
                        //Inicio - Cambio de estado al modificar el tercero si el estador es por ajustar lo cambio a registrado ----------------------
                        int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
                        int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                        int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
                        int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
                        if (row[15].ToString() == "Registrado")
                        {
                            vTercero.IdEstadoTercero = idEstadoRegistrado;
                        }
                        if (row[15].ToString() == "Por Ajustar")
                        {
                            vTercero.IdEstadoTercero = idEstadoPorAjustar;
                        }
                        if (row[15].ToString() == "Por Validar")
                        {
                            vTercero.IdEstadoTercero = idEstadoPorValidar;
                        }
                        if (row[15].ToString() == "Validado")
                        {
                            vTercero.IdEstadoTercero = idEstadoValidado;
                        }

                        vTercero.UsuarioCrea = GetSessionUser().NombreUsuario;
                        vTercero.FechaCrea = DateTime.Now;
                        int vIdTercero = vOferenteService.InsertarCargarTercero(vTercero);

                        if (vIdTercero != 0)
                        {
                            int IdClaseActividad = row[0].ToString() == "" ? 0 : vProveedorService.ConsultarClaseActividads(null, row[0].ToString(), true)[0].IdClaseActividad;

                            TerceroDatoAdicional vTerceroDatoAdicional = new TerceroDatoAdicional();
                            vTerceroDatoAdicional.IdTercero = vIdTercero;
                            vTerceroDatoAdicional.NivelInteres = row[13].ToString().ToUpper();
                            vTerceroDatoAdicional.IdClaseActividad = IdClaseActividad;
                            vTerceroDatoAdicional.Observaciones = row[14].ToString().ToUpper() + ' ' + String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now);
                            vTerceroDatoAdicional.UsuarioCrea = GetSessionUser().NombreUsuario;
                            vResultado = vOferenteService.InsertarTerceroDatoAdicional(vTerceroDatoAdicional);

                            IdDepartamento = row[16].ToString() == "" ? 0 : vSiaService.ConsultarDepartamentos(null, null, row[16].ToString())[0].IdDepartamento;
                            if (IdDepartamento == 0) IdDepartamento = null;
                            IdMunicipio = row[17].ToString() == "" ? 0 : vSiaService.ConsultarMunicipios(IdDepartamento, null, row[17].ToString())[0].IdMunicipio;

                            if (IdMunicipio != 0 && row[18].ToString() != "" && row[19].ToString() != "")
                            {
                                Referente vReferente1 = new Referente();
                                vReferente1.IdTercero = vIdTercero;
                                vReferente1.IdMunicipio = IdMunicipio;
                                vReferente1.Nombre = row[18].ToString().ToUpper();
                                vReferente1.Correo = row[19].ToString().ToUpper();
                                vReferente1.Indicativo = row[20].ToString();
                                vReferente1.Telefono = row[21].ToString();
                                vReferente1.Extension = row[22].ToString();
                                vReferente1.Celular = row[23].ToString();
                                vReferente1.UsuarioCrea = GetSessionUser().NombreUsuario;
                                vResultado = vOferenteService.InsertarReferente(vReferente1);
                            }

                            IdDepartamento = row[24].ToString() == "" ? 0 : vSiaService.ConsultarDepartamentos(null, null, row[24].ToString())[0].IdDepartamento;
                            if (IdDepartamento == 0) IdDepartamento = null;
                            IdMunicipio = row[25].ToString() == "" ? 0 : vSiaService.ConsultarMunicipios(IdDepartamento, null, row[25].ToString())[0].IdMunicipio;

                            if (IdMunicipio != 0 && row[26].ToString() != "" && row[27].ToString() != "")
                            {
                                Referente vReferente2 = new Referente();
                                vReferente2.IdTercero = vIdTercero;
                                vReferente2.IdMunicipio = IdMunicipio;
                                vReferente2.Nombre = row[26].ToString().ToUpper();
                                vReferente2.Correo = row[27].ToString().ToUpper();
                                vReferente2.Indicativo = row[28].ToString();
                                vReferente2.Telefono = row[29].ToString();
                                vReferente2.Extension = row[30].ToString();
                                vReferente2.Celular = row[31].ToString();
                                vReferente2.UsuarioCrea = GetSessionUser().NombreUsuario;
                                vResultado = vOferenteService.InsertarReferente(vReferente2);
                            }
                        }
                    }
                    else
                    {
                        if (MensajeError != "") throw new Exception(MensajeError);
                    }
                }
                catch (Exception ex)
                {
                    DataRow newRow = vErrorData.NewRow();
                    newRow["Fila"] = Fila + 2;
                    newRow["Número de identificación"] = string.IsNullOrEmpty(row[3].ToString()) ? "0" : Util.IsValidNumbers(row[3].ToString()) ? row[3].ToString() : "0";
                    newRow["Razón social"] = row[4].ToString();
                    newRow["Descripción del error"] = HttpUtility.HtmlEncode(ex.Message);
                    vErrorData.Rows.Add(newRow);
                    numErrors++;
                }

                Fila++;
            }

            DatosGeneralesCarga vDatosCarga = new DatosGeneralesCarga();
            if (numErrors == 0)
            {
                DateTime Fecha = new DateTime();
                Fecha = DateTime.Now;
                //// inserta datos generales carga
                vDatosCarga.NombreArchivo = pNombreArchivo;
                vDatosCarga.IdTipoArchivo = pIdTipoArchivo;
                vDatosCarga.IdUsuario = pIdUsuario;
                vDatosCarga.Vigencia = Fecha.Year;
                vDatosCarga.Mes = Fecha.Month.ToString();
                vDatosCarga.FechaCarga = Fecha;
                vDatosCarga.IdEstadoAnalisis = 1; // pendiente
                vDatosCarga.Observaciones = string.Empty;
                vDatosCarga.CantidadRegistrosExcel = vData.Rows.Count;
                vDatosCarga.CantidadRegistrosDB = index;
                vDatosCarga.UsuarioCrea = pUsuario;

                int vResult = vDatosCargaDAL.InsertarDatosGeneralesCarga(vDatosCarga);

                vDatosCarga = vDatosCargaDAL.ConsultarDatosGeneralesCarga(vDatosCarga.IdAnalisis);
            }

            return new CargaArchivoResult() { NumErrors = numErrors, DataResult = vErrorData, DatosCarga = vDatosCarga };
        }
        catch (UserInterfaceException)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    protected String ValidarDatos(DataRow row, string index)
    {
        String MensajeError = "";

        Tercero vTercero = vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(null, row[3].ToString());

        if (vTercero.IdTercero > 0)
        {
            MensajeError = "La fila " + index + " ya se encuentra registrado en la base de datos";
        }
        else
        {
            for (int Column = 0; Column < 32; Column++)
            {
                switch (Column)
                {
                    case 0:
                        if (string.IsNullOrEmpty(row[Column].ToString()))
                            MensajeError = MensajeError + "El campo Clase actividad no tiene información, es un dato obligatorio. Por favor verificar. ";
                        else
                        {
                            if (vProveedorService.ConsultarClaseActividads(null, row[Column].ToString(), true).Count == 0)
                                MensajeError = MensajeError + "La clase de actividad, no ha sido creada en el sistema. Por favor revisar. ";
                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "La clase de actividad, debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 1:
                        if (string.IsNullOrEmpty(row[Column].ToString()))
                            MensajeError = MensajeError + "El campo Tipo persona no tiene información, es un dato obligatorio. Por favor verificar. ";
                        else if (vOferenteService.ConsultarTipoPersonas(null,row[Column].ToString(),true).Count == 0 || !Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El tipo de persona no es correcto. Por favor revisar las únicas opciones son natural, juridica, consorcio, unión temporal. ";
                        break;
                    case 2:
                        if (string.IsNullOrEmpty(row[Column].ToString()))
                            MensajeError = MensajeError + "El campo Tipo documento no tiene información, es un dato obligatorio. Por favor verificar. ";
                        else if (vSiaService.ConsultarTiposDocumento(row[Column].ToString(), null).Count == 0 || !Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El tipo de identificación no es correcto. Por favor revisar, las únicas opciones son CC, CE, NIT. ";
                        break;
                    case 3:
                        if (string.IsNullOrEmpty(row[Column].ToString()))
                            MensajeError = MensajeError + "El campo Número de identificación no tiene información, es un dato obligatorio. Por favor verificar. ";
                        else if (!Util.IsValidNumbers(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El número de identificación únicamente debe ser números. Por favor revisar. ";
                        break;
                    case 4:
                        if (string.IsNullOrEmpty(row[Column].ToString()))
                            MensajeError = MensajeError + "El campo Razón social no tiene información, es un dato obligatorio. Por favor verificar. ";
                        break;
                    case 5:
                        //"Indicativo";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Indicativo = row[Column].ToString();
                            if (!Util.IsValidNumbers(Indicativo) || Indicativo.Length != 1)
                                MensajeError = MensajeError + "El campo indicativo solamente debe tener un digito. Por favor revisar. ";
                        }
                        break;
                    case 6:
                        //"Teléfono";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Telefono = row[Column].ToString();
                            if (!Util.IsValidNumbers(Telefono))
                                MensajeError = MensajeError + "El teléfono únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 7:
                        //"Extensión";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Extension = row[Column].ToString();
                            if (!Util.IsValidNumbers(Extension))
                                MensajeError = MensajeError + "La extensión únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 8:
                        //"Celular";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Celular = row[Column].ToString();
                            if (!Util.IsValidNumbers(Celular))
                                MensajeError = MensajeError + "El celular únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 9:
                        //"Zona";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "La zona únicamente debe ser texto. Por favor revisar. ";
                            if (row[Column].ToString().ToUpper() != "RURAL" && row[Column].ToString().ToUpper() != "URBANA")
                                MensajeError = MensajeError + "La zona no es correcta. Por favor revisar, las únicas opciones son rural o urbana. ";                      
                        }
                        break;
                    case 10:
                        //"Departamento";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (vSiaService.ConsultarDepartamentos(null, null, row[Column].ToString()).Count == 0)
                            {
                                MensajeError = MensajeError + "El departamento no es correcto. Por favor revisar. ";
                            }

                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El departamento únicamente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 11:
                        //"Municipio";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (vSiaService.ConsultarMunicipios(null, null, row[Column].ToString()).Count == 0)
                            {
                                MensajeError = MensajeError + "El municipio no es correcto. Por favor revisar. ";
                            }

                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El municipio únicamente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 12:
                        if (string.IsNullOrEmpty(row[Column].ToString()))
                            MensajeError = MensajeError + "El campo Correo electrónico no tiene información, es un dato obligatorio. Por favor verificar. ";
                        else if (!Util.IsValidEmail(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El correo electrónico definido no corresponde al formato válido: ejemplo usuario@organización.com. ";
                        break;
                    case 13:
                        //"Nivel de interés";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El Nivel de interés únicamente debe ser texto. Por favor revisar. ";

                            if (row[Column].ToString().ToUpper() != "ALTO" && row[Column].ToString().ToUpper() != "MEDIO" && row[Column].ToString().ToUpper() != "BAJO")
                                MensajeError = MensajeError + "El nivel de interés no es correcto. Por favor revisar, las únicas opciones son alto, medio, bajo. "; 
                        }
                        break;
                    case 14:
                        //"Observaciones";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        { 
                        }
                        break;
                    case 15:
                        //"Estado";
                        if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                            MensajeError = MensajeError + "El Estado únicamente debe ser texto. Por favor revisar. ";
                        if (row[Column].ToString().Trim() != "Registrado")
                            MensajeError = MensajeError + "El estado de la fila, no es correcto. Por favor revisar. ";
                        break;
                    case 16:
                        //"Departamento";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (vSiaService.ConsultarDepartamentos(null, null, row[Column].ToString()).Count == 0)
                            {
                                MensajeError = MensajeError + "El departamento no es correcto. Por favor revisar. ";
                            }

                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El departamento únicamente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 17:
                        //"Municipio";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (vSiaService.ConsultarMunicipios(null, null, row[Column].ToString()).Count == 0)
                            {
                                MensajeError = MensajeError + "El municipio no es correcto. Por favor revisar. ";
                            }

                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El municipio únicamente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 18:
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "“El nombre del referente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 19:
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (!Util.IsValidEmail(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El correo electrónico definido no corresponde al formato válido: ejemplo usuario@organización.com. ";
                        }
                        break;
                    case 20:
                        //"Indicativo";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Indicativo = row[Column] == "" ? "0" : row[Column].ToString();
                            if (!Util.IsValidNumbers(Indicativo) || Indicativo.Length != 1)
                                MensajeError = MensajeError + "El indicativo únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 21:
                        //"Teléfono";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Telefono = row[Column].ToString();
                            if (!Util.IsValidNumbers(Telefono))
                                MensajeError = MensajeError + "El teléfono únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 22:
                        //"Extensión";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Extension = row[Column].ToString();
                            if (!Util.IsValidNumbers(Extension))
                                MensajeError = MensajeError + "La extensión únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 23:
                        //"Celular";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Celular = row[Column].ToString();
                            if (!Util.IsValidNumbers(Celular))
                                MensajeError = MensajeError + "El celular únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 24:
                        //"Departamento";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (vSiaService.ConsultarDepartamentos(null, null, row[Column].ToString()).Count == 0)
                            {
                                MensajeError = MensajeError + "El departamento no es correcto. Por favor revisar. ";
                            }

                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El departamento únicamente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 25:
                        //"Municipio";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (vSiaService.ConsultarMunicipios(null, null, row[Column].ToString()).Count == 0)
                            {
                                MensajeError = MensajeError + "El municipio no es correcto. Por favor revisar. ";
                            }

                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El municipio únicamente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 26:
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (!Util.IsValidLetters(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "“El nombre del referente debe ser texto. Por favor revisar. ";
                        }
                        break;
                    case 27:
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            if (!Util.IsValidEmail(row[Column].ToString().Trim()))
                                MensajeError = MensajeError + "El correo electrónico definido no corresponde al formato válido: ejemplo usuario@organización.com. ";
                        }
                        break;
                    case 28:
                        //"Indicativo";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Indicativo = row[Column] == "" ? "0" : row[Column].ToString();
                            if (!Util.IsValidNumbers(Indicativo) || Indicativo.Length != 1)
                                MensajeError = MensajeError + "El indicativo únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 29:
                        //"Teléfono";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Telefono = row[Column].ToString();
                            if (!Util.IsValidNumbers(Telefono))
                                MensajeError = MensajeError + "El teléfono únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 30:
                        //"Extensión";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Extension = row[Column].ToString();
                            if (!Util.IsValidNumbers(Extension))
                                MensajeError = MensajeError + "La extensión únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                    case 31:
                        //"Celular";
                        if (!string.IsNullOrEmpty(row[Column].ToString()))
                        {
                            string Celular = row[Column].ToString();
                            if (!Util.IsValidNumbers(Celular))
                                MensajeError = MensajeError + "El celular únicamente debe ser números. Por favor revisar. ";
                        }
                        break;
                }
            }
        }
        return MensajeError;
    }    

    private void LimpiarGridDatos()
    {
        toolBar.LipiarMensajeError();
        grvHistorico.DataSource = null;
        grvHistorico.DataBind();
        pnlTablaHistorico.Visible = false;
    }

    public SortDirection GridViewSortDirectionHistorico
    {
        get
        {
            if (ViewState["sortDirectionHistorico"] == null)
                ViewState["sortDirectionHistorico"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirectionHistorico"];
        }
        set { ViewState["sortDirectionHistorico"] = value; }
    }

    private void OrdenarHistorico(GridViewSortEventArgs e)
    {
        int? IdTipoArchivo = null;

        if (ddlTipoArchivo.SelectedValue != "-1")
            IdTipoArchivo = Convert.ToInt32(ddlTipoArchivo.SelectedValue);

        DataTable lstHistorico = ((DataTable)RespuestaValidacion);
        //Dependiendo del modo de ordenamiento . . .
        if (GridViewSortDirectionHistorico == SortDirection.Ascending)
        {
            switch (e.SortExpression)
            {
                case ("idAnalisis"):
                    lstHistorico = lstHistorico.AsEnumerable().OrderBy(x => x.Field<string>("idAnalisis")).CopyToDataTable();
                    break;
                case ("nombreArchivo"):
                    lstHistorico = lstHistorico.AsEnumerable().OrderBy(x => x.Field<string>("nombreArchivo")).CopyToDataTable();
                    break;
                case ("fechaCarga"):
                    lstHistorico = lstHistorico.AsEnumerable().OrderBy(x => x.Field<string>("fechaCarga")).CopyToDataTable();
                    break;
                default:
                    break;
            }
            RespuestaValidacion = lstHistorico;
            GridViewSortDirectionHistorico = SortDirection.Descending;
        }
        else
        {
            switch (e.SortExpression)
            {
                case ("idAnalisis"):
                    lstHistorico = lstHistorico.AsEnumerable().OrderBy(x => x.Field<int>("idAnalisis")).CopyToDataTable();
                    break;
                case ("nombreArchivo"):
                    lstHistorico = lstHistorico.AsEnumerable().OrderBy(x => x.Field<string>("nombreArchivo")).CopyToDataTable();
                    break;
                case ("fechaCarga"):
                    lstHistorico = lstHistorico.AsEnumerable().OrderBy(x => x.Field<string>("fechaCarga")).CopyToDataTable();
                    break;
                default:
                    break;
            }
            RespuestaValidacion = lstHistorico;
            grvHistorico.DataSource = vDatosCargaDAL.ConsultarDatosGeneralesCargas(null, null, IdTipoArchivo, null, null, null, null);
            GridViewSortDirectionHistorico = SortDirection.Ascending;
        }
        grvHistorico.DataBind();
    }

    #region "Metodos"

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoLimpiar += new ToolBarDelegate(btnLimpiar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.EstablecerTitulos("Cargar Terceros", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles ManejoControles = new ManejoControles();
            ManejoControles.LlenarTiposArchivo(ddlTipoArchivo, "-1", true);

            fuplArchivoCargue.Attributes.Clear();
            toolBar.LipiarMensajeError();
            lbInconsistencias.Visible = false;
            pnlResultado.Visible = false;
            pnlTablaHistorico.Visible = false;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public string CargarArchivoServidor(FileUpload pControlFileUpload)
    {
        this.toolBar.LipiarMensajeError();

        //// Se obtiene la ruta físca para guardar el archivo
        string vFilePath = Server.MapPath(@"~\Page\Proveedor\GestionTercero\Archivos\");
        Directory.CreateDirectory(vFilePath);
        string vFilename = string.Empty;

        try
        {
            ////Se valida si se adjunto algún archivo en el control
            if (pControlFileUpload.HasFile)
            {
                string[] vAllowdFile = { ".xls", ".xlsx" };
                ////Se extrae la extensión  del archivo
                string vFileExt = Path.GetExtension(pControlFileUpload.PostedFile.FileName).ToLower();
                bool EsValiFile = vAllowdFile.Contains(vFileExt);
                ////Se valida si el archivo una extensión diferente a la de excel
                if (!EsValiFile)
                {
                    return "1";
                }
                else
                {
                    vFilename = string.Format("{0:yyyyMMddHHmmss}-{1}", DateTime.Now, Path.GetFileName(Server.MapPath(pControlFileUpload.FileName)));
                    vFilePath += vFilename;
                    pControlFileUpload.SaveAs(vFilePath);
                }
            }
            else
            {
                return "2";
            }

            return vFilePath;
        }
        catch (Exception ex)
        {
            this.toolBar.MostrarMensajeError(string.Empty + ex.Message);
            throw;
        }
    }

    /// <summary>
    /// Cálculo de la DIV
    /// </summary>
    private int? CalcularDiV(String NumeroDoc)
    {
        int? DV;
        int lenTxtNumeroDoc = 15 - NumeroDoc.Length;

        if (lenTxtNumeroDoc >= 0)
        {
            var strDato = Left("000000000000000", lenTxtNumeroDoc) + NumeroDoc;
            char[] strArray = (strDato.ToCharArray());

            int result = Convert.ToInt32(Convert.ToString(strArray[14])) * 3 +
                         Convert.ToInt32(Convert.ToString(strArray[13])) * 7 +
                         Convert.ToInt32(Convert.ToString(strArray[12])) * 13 +
                         Convert.ToInt32(Convert.ToString(strArray[11])) * 17 +
                         Convert.ToInt32(Convert.ToString(strArray[10])) * 19 +
                         Convert.ToInt32(Convert.ToString(strArray[9])) * 23 +
                         Convert.ToInt32(Convert.ToString(strArray[8])) * 29 +
                         Convert.ToInt32(Convert.ToString(strArray[7])) * 37 +
                         Convert.ToInt32(Convert.ToString(strArray[6])) * 41 +
                         Convert.ToInt32(Convert.ToString(strArray[5])) * 43 +
                         Convert.ToInt32(Convert.ToString(strArray[4])) * 47 +
                         Convert.ToInt32(Convert.ToString(strArray[3])) * 53 +
                         Convert.ToInt32(Convert.ToString(strArray[2])) * 59 +
                         Convert.ToInt32(Convert.ToString(strArray[1])) * 67 +
                         Convert.ToInt32(Convert.ToString(strArray[0])) * 71;

            int residuo = result % 11;

            if (residuo == 0)
            {
                DV = 0;
            }
            else
            {
                if (residuo == 1)
                {
                    DV = 1;
                }
                else
                {
                    residuo = 11 - residuo;
                    DV = residuo;
                }
            }
        } //Fin if (lenTxtNumeroDoc >= 0)
        else
        {
            DV = null;
        } // Fin else if (lenTxtNumeroDoc >= 0)

        return DV;
    }

    /// <summary>
    /// Rompe una cade hacia izquierda
    /// </summary>
    /// <param name="param"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    private static string Left(string param, int length)
    {
        string result = param.Substring(0, length);
        return result;
    }

    protected void Buscar()
    {
        int? IdTipoArchivo = null;

        if (ddlTipoArchivo.SelectedValue != "-1")
            IdTipoArchivo = Convert.ToInt32(ddlTipoArchivo.SelectedValue);

        this.grvHistorico.DataSource = vDatosCargaDAL.ConsultarDatosGeneralesCargas(null, null, IdTipoArchivo, null, null, null, null);
        this.grvHistorico.DataBind();

        pnlTablaHistorico.Visible = true;
    }

    #endregion
}
