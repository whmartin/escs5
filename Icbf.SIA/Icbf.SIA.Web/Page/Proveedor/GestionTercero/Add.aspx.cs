using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Service;
using Icbf.Oferente.Entity;
using Icbf.SIA.Entity;
using System.IO;
using Icbf.SIA.Service;
using System.Reflection;
using System.Activities.Statements;
using Icbf.Seguridad.Service;
using System.Configuration;
using System.Net.Mail;
using System.Web.Security;

/// <summary>
/// Página de registro y edición de terceros
/// </summary>
public partial class Page_Proveedor_Add : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/GestionTercero";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();
    ManejoControles ManejoControles = new ManejoControles();
    Tercero vTercero = new Tercero();
    DateTime fechaInicioCalendarios = Convert.ToDateTime("01/01/1900");
    DateTime fechaFinCalendarios = DateTime.Now;
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                //Usuario que inició sesión
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                }
                else
                {
                    //Cargar datos si es usuario  Externo
                    Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
                    if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
                    {
                        Tercero lista = new Tercero();
                        lista = vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(vusuario.IdTipoDocumento, vusuario.NumeroDocumento);
                        txtCuentaCorreo.Text = vusuario.CorreoElectronico;
                        ddlTipoPersona.SelectedValue = vusuario.IdTipoPersona.ToString();
                        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
                        if (tipoper.CodigoTipoPersona == "001")
                        {
                            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
                            txtNumIdentificacion.Text = vusuario.NumeroDocumento;
                            terceroVisible(true);
                            ViewControlJuridica(true);
                            //Cargo datos de tipo persona
                            txtPrimerNombre.Text = vusuario.PrimerNombre;
                            txtSegundoNombre.Text = vusuario.SegundoNombre;
                            txtPrimerApellido.Text = vusuario.PrimerApellido;
                            txtSegundoApellido.Text = vusuario.SegundoApellido;
                            if (lista.IdTercero != 0)
                            {
                                ddlSexo.SelectedValue = lista.Sexo;
                                cuFechaExp.Date = DateTime.Parse(lista.FechaExpedicionId.ToString());
                                cuFechaNac.Date = DateTime.Parse(lista.FechaNacimiento.ToString());
                                txtPrimerNombre.Text = lista.PrimerNombre.Replace(" ", "");
                                txtSegundoNombre.Text = lista.SegundoNombre;
                                txtPrimerApellido.Text = lista.PrimerApellido;
                                txtSegundoApellido.Text = lista.SegundoApellido;
                            }
                        }
                        else
                        {
                            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
                            terceroVisible(true);
                            ViewControlJuridica(false);
                            //Cargo datos de tipo Juridico
                            txtDV.Text = vusuario.DV;
                            if (lista.IdTercero != 0)
                            {
                                txtRazonSocial.Text = lista.RazonSocial; txtNumeroDoc.Text = lista.NumeroIdentificacion;
                            }
                            else { txtRazonSocial.Text = vusuario.RazonSocial; txtNumeroDoc.Text = vusuario.NumeroDocumento; }
                        }
                        if (lista.IdTercero != 0)
                        {
                            PropertyInfo isReadOnly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
                            isReadOnly.SetValue(this.Request.QueryString, false, null);
                            this.Request.QueryString.Set("oP", "E");
                            isReadOnly.SetValue(this.Request.QueryString, true, null);
                            SetSessionParameter("Proveedor.IdTercero", lista.IdTercero);
                            ViewState["IdTercero"] = lista.IdTercero;
                            ddlEstadoTercero.SelectedValue = lista.IdEstadoTercero.ToString();
                            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
                            int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                            if (lista.IdEstadoTercero == idEstadoRegistrado || lista.IdEstadoTercero == idEstadoPorAjustar)
                            {
                                txtNumeroDoc.Enabled = false;
                                txtNumIdentificacion.Enabled = false;
                            }
                            else
                            {
                                habilitarEntradaTercero(false);
                                txtNumeroDoc.Enabled = false;
                                txtNumIdentificacion.Enabled = false;
                            }
                        }
                        else
                        {
                            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrado
                            ddlEstadoTercero.SelectedValue = idEstadoRegistrado.ToString().Trim();  //Cambiar esto por enumeraciones globales del sistema  4 Signigica estado registrado (defecto)
                        }

                        ddlTipoDoc.SelectedValue = vusuario.IdTipoDocumento.ToString();
                        txtCuentaUsuario.Enabled = false;
                        txtCuentaCorreo.Enabled = false;
                        ddlTipoDoc.Enabled = false;
                        txtNumIdentificacion.Enabled = false;
                        txtNumeroDoc.Enabled = false;
                        ddlTipoPersona.Enabled = false;
                        ddlEstadoTercero.Visible = false;
                        lblEstado.Visible = false;
                        pnlEstado.Visible = false;


                        cargaUbicaAdicionales(lista.IdTercero.ToString());

                    }
                    else
                    {
                        int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrado
                        ddlEstadoTercero.SelectedValue = idEstadoRegistrado.ToString().Trim();  //Cambiar esto por enumeraciones globales del sistema  4 Signigica estado registrado (defecto)
                    }
                    txtCuentaUsuario.Text = vusuario.NumeroDocumento;
                    txtCuentaUsuario.Enabled = false;
                    ddlEstadoTercero.Enabled = false;
                    BuscarDocumentos();
                    //CargarObservaciones();
                }


                pnlListaObservaciones.Visible = (gvTerceroObservaciones.Rows.Count > 0);
                cuFechaExp.HabilitarObligatoriedadYMessage("Registre la fecha de expedición del documento de identificación", true);
                cuFechaNac.HabilitarObligatoriedadYMessage("Registre la fecha de nacimiento", true);
                cuFechaExp.fechasMayoresA(fechaInicioCalendarios);
                cuFechaNac.fechasMayoresA(fechaInicioCalendarios);
                cuFechaExp.fechasMenoresA(fechaFinCalendarios);
                cuFechaNac.fechasMenoresA(fechaFinCalendarios);



            }

        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                if (Request.QueryString["oP"] == "E")
                    GuardarUsuario();

                Guardar();
            }
            else
            {
                GuardarUsuario();
                Guardar();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Proveedor.CorreoElectronico");
        RemoveSessionParameter("Proveedor.IdTercero");

        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }//

    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
            terceroVisible(true);
            ViewControlJuridica(true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
            terceroVisible(true);
            ViewControlJuridica(false);
        }

        PanelAdjuntos.Visible = true;
        BuscarDocumentos();

        ResetForm();
    }

    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipio(ddlDepartamento, ddlMunicipio);
    }

    protected void ddlDepartamento1_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipio(ddlDepartamento1, ddlMunicipio1);
    }

    protected void ddlDepartamento2_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipio(ddlDepartamento2, ddlMunicipio2);
    }

    private void ResetForm()
    {
        txtNumeroDoc.Text = "";
        txtNumIdentificacion.Text = "";
        txtRazonSocial.Text = "";
        ddlSexo.SelectedIndex = 0;
        txtPrimerNombre.Text = "";
        txtSegundoNombre.Text = "";
        txtPrimerApellido.Text = "";
        txtSegundoApellido.Text = "";
        cuFechaExp.Date = Convert.ToDateTime("31/12/1899");
        cuFechaNac.Date = Convert.ToDateTime("31/12/1899");
        txtDV.Text = "";
    }

    protected void txtNumeroDoc_TextChanged(object sender, EventArgs e)
    {
        revNIT.ValidationGroup = "vgRevNIT";
        revNumDocCeroNIT.ValidationGroup = "vgRevNIT";
        cvNIT.ValidationGroup = "vgRevNIT";
        Validate("vgRevNIT");

        if (IsValid)
        {
            if (PnlNatural.Visible == false)
            {
                CalcularDiV();
                txtRazonSocial.Focus();
                revNIT.ValidationGroup = "btnGuardar";
                revNumDocCeroNIT.ValidationGroup = "btnGuardar";
                cvNIT.ValidationGroup = "btnGuardar";
            }
            else
            {
                txtPrimerNombre.Focus();
            }
        }
        else
        {
            txtDV.Text = "";
            revNIT.ValidationGroup = "btnGuardar";
            revNumDocCeroNIT.ValidationGroup = "btnGuardar";
            cvNIT.ValidationGroup = "btnGuardar";
        }
    }

    /// <summary>
    /// Manejador de evento click para el drop ddlTipoDoc ddlTipoDoc
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoDoc_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumIdentificacion.Text = String.Empty;
            this.txtNumIdentificacion.Focus();
        }
    }


    #endregion

    #region Métodos

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            bool isPersona = true;
            Referente vRef1 = new Referente();
            Referente vRef2 = new Referente();
            Tercero vterceroOriginal = new Tercero();
            TerceroDatoAdicional vTerceroDatoAdicional = new TerceroDatoAdicional();

            //Inicio - Cambio de estado al modificar el tercero si el estador es por ajustar lo cambio a registrado ----------------------
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
            int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
            int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
            int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado

            if (ViewState["IdTemporal"] == null)
            {
                ViewState["IdTemporal"] = DateTime.Now.Ticks.ToString();
            }

            if (Request.QueryString["oP"] == "E")
            {
                String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
                vTercero = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));
                vterceroOriginal = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));
                ViewState["IdTercero"] = vIdTercero;
            }
            TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
            if (tipoper.CodigoTipoPersona != "001")
            {
                isPersona = false;
            }

            //Es Persona Natural
            if (isPersona)
            {
                //Si esta en estado Validado no se permite modificar datos distintos a los de ubicación (Télefono, Dirección,..)
                if (int.Parse(ddlEstadoTercero.SelectedValue) != idEstadoValidado)
                {
                    vTercero.Email = txtCuentaCorreo.Text.ToLower();
                    vTercero.NumeroIdentificacion = txtNumIdentificacion.Text.Trim().ToUpper();
                    string fechanac = cuFechaNac.Date.ToString();

                    //-----------
                    // validar fechas--- diferencia de 18 Años. cuFechaExp
                    if (cuFechaExp.Date <= cuFechaNac.Date)
                    {
                        toolBar.MostrarMensajeError("La fecha de expedición no puede ser menor al de nacimiento, inténtelo de nuevo");
                        return;
                    }

                    TimeSpan ts = cuFechaExp.Date - cuFechaNac.Date;
                    DateTime Age = DateTime.MinValue + ts;
                    if ((Age.Year - 1) < 18)
                    {
                        toolBar.MostrarMensajeError("Entre las fechas de expedición y de nacimiento debe haber diferencia mínima 18 años, inténtelo de nuevo");
                        return;
                    }
                    // Colocar Aqui los nombres y apellidos.

                    vTercero.FechaNacimiento = cuFechaNac.Date;
                    vTercero.FechaExpedicionId = cuFechaExp.Date; //validar
                    vTercero.Sexo = ddlSexo.SelectedValue;
                    vTercero.PrimerNombre = txtPrimerNombre.Text.Trim().ToUpper();
                    vTercero.SegundoNombre = txtSegundoNombre.Text.Trim().ToUpper();
                    vTercero.PrimerApellido = txtPrimerApellido.Text.Trim().ToUpper();
                    vTercero.SegundoApellido = txtSegundoApellido.Text.Trim().ToUpper();
                }
            }
            else
            {
                vTercero.Email = txtCuentaCorreo.Text.ToLower();
                vTercero.NumeroIdentificacion = txtNumeroDoc.Text.Trim();
                vTercero.RazonSocial = txtRazonSocial.Text.Trim();
            }

            vTercero.Indicativo = txtIndicativo.Text.Trim();
            vTercero.Telefono = txtTelefono.Text.Trim();
            vTercero.Extension = txtExtension.Text.Trim();
            vTercero.Celular = txtCelular.Text.Trim();

            vTercero.IdDepartamento = Convert.ToInt32(ddlDepartamento.SelectedValue);
            vTercero.IdMunicipio = Convert.ToInt32(ddlMunicipio.SelectedValue);

            if (txtDireccionResidencia.TipoZona == "U")
            {
                vTercero.IdZona = 1;
            }
            if (txtDireccionResidencia.TipoZona == "R")
            {
                vTercero.IdZona = 2;
            }
            vTercero.Direccion = txtDireccionResidencia.Text.Trim();

            if (!new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
            {
                vRef1.IdReferente = hdfIfReferente1.Value == "" ? 0 : Convert.ToInt32(hdfIfReferente1.Value);
                vRef1.IdMunicipio = Convert.ToInt32(ddlMunicipio1.SelectedValue);
                vRef1.IdDepartamento = Convert.ToInt32(ddlDepartamento1.SelectedValue);
                vRef1.Nombre = txtNombre1.Text;
                vRef1.Correo = txtCorreo1.Text;
                vRef1.Indicativo = txtIndicativo1.Text;
                vRef1.Telefono = txtTelefono1.Text;
                vRef1.Extension = txtExtension1.Text;
                vRef1.Celular = txtCelular1.Text;
                if (vRef1.IdMunicipio == -1 && vRef1.Nombre == "" && vRef1.Correo == "" && vRef1.Telefono == "" && vRef1.Celular == "")
                {
                    vRef1.IdReferente = -1;
                }

                vRef2.IdReferente = hdfIfReferente2.Value == "" ? 0 : Convert.ToInt32(hdfIfReferente2.Value);
                vRef2.IdMunicipio = Convert.ToInt32(ddlMunicipio2.SelectedValue);
                vRef2.IdDepartamento = Convert.ToInt32(ddlDepartamento2.SelectedValue);
                vRef2.Nombre = txtNombre2.Text;
                vRef2.Correo = txtCorreo2.Text;
                vRef2.Indicativo = txtIndicativo2.Text;
                vRef2.Telefono = txtTelefono2.Text;
                vRef2.Extension = txtExtension2.Text;
                vRef2.Celular = txtCelular2.Text;

                if (vRef2.IdMunicipio == -1 && vRef2.Nombre == "" && vRef2.Correo == "" && vRef2.Telefono == "" && vRef2.Celular == "")
                {
                    vRef2.IdReferente = -1;
                }

                vTerceroDatoAdicional.IdTercero = hdfIdTerceroDA.Value == "" ? 0 : Convert.ToInt32(hdfIdTerceroDA.Value);
                vTerceroDatoAdicional.NivelInteres = ddlNivelInteres.SelectedValue;
                vTerceroDatoAdicional.IdClaseActividad = Convert.ToInt32(ddlClaseActividad.SelectedValue);
                vTerceroDatoAdicional.Observaciones = txtObservacion.Text;
            }

            vTercero.CreadoPorInterno = !new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES");

            if (vterceroOriginal.CreadoPorInterno == true)
                vTercero.CreadoPorInterno = true;

            if (Request.QueryString["oP"] == "E")
            {
                vTercero.UsuarioModifica = new GeneralWeb().GetSessionUser().NombreUsuario;
                vTercero.FechaModifica = DateTime.Now;
                //validar docs
                //if (new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
                //{
                //    ValidarDocumentosObligatorios(vTercero, ViewState["IdTemporal"].ToString());
                //    ValidarDocumentosObligatoriosEdicion(vTercero);
                //}
                if (conCambioValidar(isPersona, vterceroOriginal, vTercero))
                {
                    if (int.Parse(ddlEstadoTercero.SelectedValue) == idEstadoPorAjustar)
                    {
                        vTercero.IdEstadoTercero = idEstadoPorValidar;
                    }
                    //Fin- cambio de estado al modificar el tercero si el estador es por ajustar lo cambio a registrado--------------------
                    //Inicio - Cambio de estado al modificar el tercero si el estador es VALIDADO lo cambio a Por Validar ----------------------
                    if (int.Parse(ddlEstadoTercero.SelectedValue) == idEstadoValidado)
                    {
                        vTercero.IdEstadoTercero = idEstadoPorValidar;
                    }
                    //FIn - Cambio de estado al modificar el tercero si el estador es VALIDADO lo cambio a Por Validar ----------------------

                    int resultado = vProveedorService.ModificarTercero_EstadoTercero(vTercero);
                }

                if (new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
                    vTercero.ProviderUserKey = vRUBOService.ConsultarUsuario(GetSessionUser().IdUsuario).Providerkey;

                Tercero vTer = vProveedorService.ConsultarTerceroPorEmail(txtCuentaCorreo.Text);

                if (vTer.IdTercero != 0)
                {
                    if (vTer.IdTercero != vTercero.IdTercero)
                        throw new Exception("El correo electrónico ingresado ya existe");
                }

                vResultado = vOferenteService.ModificarTercero(vTercero, ViewState["IdTemporal"].ToString());
                vRef1.IdTercero = vTercero.IdTercero;
                vRef2.IdTercero = vTercero.IdTercero;
            }
            else
            {
                vTercero.DigitoVerificacion = txtDV.Text != "" ? int.Parse(txtDV.Text.Trim()) : 0;
                vTercero.FechaNacimiento = cuFechaNac.Date;
                vTercero.IdDListaTipoDocumento = int.Parse(ddlTipoDoc.SelectedValue);
                vTercero.IdTipoPersona = int.Parse(ddlTipoPersona.SelectedValue);
                vTercero.IdEstadoTercero = int.Parse(ddlEstadoTercero.SelectedValue);

                //Se agrega el provider user key
                if (!string.IsNullOrEmpty(hfIdUsuario.Value))
                    vTercero.ProviderUserKey = vRUBOService.ConsultarUsuario(Convert.ToInt32(hfIdUsuario.Value)).Providerkey;
                else
                    vTercero.ProviderUserKey = GetSessionUser().Providerkey;

                vTercero.UsuarioCrea = new GeneralWeb().GetSessionUser().NombreUsuario.Trim();
                vTercero.FechaCrea = DateTime.Now;
                //validar docs
                //if (new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
                //{
                //    ValidarDocumentosObligatorios(vTercero, ViewState["IdTemporal"].ToString());
                //}

                String correo = vProveedorService.ConsultarTerceroPorEmail(txtCuentaCorreo.Text).Email;

                if (!string.IsNullOrEmpty(correo))
                    throw new Exception("El correo electrónico ingresado ya existe");

                vResultado = vOferenteService.InsertarTercero(vTercero, ViewState["IdTemporal"].ToString());

                vTercero.IdTercero = vResultado;


            }


            ///Se almacena la información de los Referentes
            if (vResultado != 0)
            {
                ViewState["IdTercero"] = vTercero.IdTercero;
                ViewState["IdTemporal"] = null;
                vRef1.IdTercero = vTercero.IdTercero;
                vRef2.IdTercero = vTercero.IdTercero;

                string currentApp = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
                if (!new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES") &&
                    (currentApp == "F"))
                {
                    if (vRef1.IdReferente >= 0)
                    {
                        if (vRef1.IdReferente == 0)
                        {
                            vRef1.UsuarioCrea = new GeneralWeb().GetSessionUser().NombreUsuario.Trim();
                            vResultado = vOferenteService.InsertarReferente(vRef1);
                        }
                        else
                        {
                            vRef1.UsuarioModifica = new GeneralWeb().GetSessionUser().NombreUsuario;
                            vResultado = vOferenteService.ModificarReferente(vRef1);
                        }
                    }
                    if (vResultado != 0)
                    {
                        if (vRef2.IdReferente >= 0)
                        {
                            if (vRef2.IdReferente == 0)
                            {
                                vRef2.UsuarioCrea = new GeneralWeb().GetSessionUser().NombreUsuario.Trim();
                                vResultado = vOferenteService.InsertarReferente(vRef2);
                            }
                            else
                            {
                                vRef2.UsuarioModifica = new GeneralWeb().GetSessionUser().NombreUsuario;
                                vResultado = vOferenteService.ModificarReferente(vRef2);
                            }
                        }
                    }
                    if (vResultado != 0)
                    {
                        if (vTerceroDatoAdicional.IdTercero == 0)
                        {
                            vTerceroDatoAdicional.IdTercero = vTercero.IdTercero;
                            vTerceroDatoAdicional.UsuarioCrea = new GeneralWeb().GetSessionUser().NombreUsuario.Trim();
                            vResultado = vOferenteService.InsertarTerceroDatoAdicional(vTerceroDatoAdicional);
                        }
                        else
                        {
                            vTerceroDatoAdicional.UsuarioModifica = new GeneralWeb().GetSessionUser().NombreUsuario;
                            vResultado = vOferenteService.ModificarTerceroDatoAdicional(vTerceroDatoAdicional);
                        }
                    }
                }
            }


            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else
            {
                toolBar.MostrarMensajeGuardado("Se envió al correo ingresado el usuario y contraseña para ingresar al sistema");
                deshabilitarTercero(false);
            }

            //Caso de uso 74 - Observaciones y envio de correo
            if (pObservacionesCorreccion.Visible)
            {
                if (!string.IsNullOrEmpty(txtObservacionesCorreccion.Text))
                {
                    if (GuardarObservacionesCorreccion())
                    {
                        Tercero vEntidadTerceroActual = new Tercero();
                        Referente vReferente1Actual = new Referente();
                        Referente vReferente2Actual = new Referente();
                        TerceroDatoAdicional vTerceroDatoAdicionalActual = new TerceroDatoAdicional();
                        vEntidadTerceroActual = Session["vEntidadTerceroActual"] as Tercero;
                        vReferente1Actual = Session["vReferente1Actual"] as Referente;
                        vReferente2Actual = Session["vReferente2Actual"] as Referente;
                        vTerceroDatoAdicionalActual = Session["vTerceroAdicionalActual"] as TerceroDatoAdicional;
                        string vCorreoElectronico = txtCuentaCorreo.Text;
                        string vEmailPara = vCorreoElectronico;
                        string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
                        string vAsunto = string.Empty;
                        string vMensaje = string.Empty;
                        int vIdUsuario = 0;
                        string vMensajeJuridico = string.Empty;
                        string vArchivo = string.Empty;

                        vAsunto = HttpContext.Current.Server.HtmlDecode("Notificaci&oacute;n sistema de proveedores ICBF.");


                        vMensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B>Notificaci&oacute;n sistema de proveedores ICBF. </B> </CENTER> </font> ") +
                                    "</br> Observaciones:" + txtObservacionesCorreccion.Text +
                                    "</br> </br> <table style='border: 1px solid #000000; width:50%;' align='center'>" +
                                    "<tr> <td style='border: 1px solid #000000; width:40%;'>Nombre Campo</td>" +
                                    " <td style='border: 1px solid #000000; width:30%;'>Actual</td>" +
                                    "<td style='border: 1px solid #000000; width:30%;'>Nuevo</td>" +
                                    "</tr>";

                        vMensaje += (vEntidadTerceroActual.Email != txtCuentaCorreo.Text ?
                                    CuerpoCorreoModificaciones(lblCorreoElectronico.Text, vEntidadTerceroActual.Email, txtCuentaCorreo.Text) : string.Empty);

                        vMensaje += (vEntidadTerceroActual.IdTipoPersona.ToString() != ddlTipoPersona.SelectedValue ?
                                    CuerpoCorreoModificaciones(lblTipoPersona.Text, vEntidadTerceroActual.IdTipoPersona.ToString(), ddlTipoPersona.SelectedValue) : string.Empty);

                        vMensaje += (vEntidadTerceroActual.IdDListaTipoDocumento.ToString() != ddlTipoDoc.SelectedValue ?
                                    CuerpoCorreoModificaciones("Tipo Documento", vEntidadTerceroActual.IdDListaTipoDocumento.ToString(), ddlTipoDoc.SelectedValue) : string.Empty);

                        vMensaje += (vEntidadTerceroActual.NumeroIdentificacion != txtNumIdentificacion.Text ?
                                    CuerpoCorreoModificaciones(lblIdentificaNat.Text, vEntidadTerceroActual.NumeroIdentificacion, txtNumIdentificacion.Text) : string.Empty);

                        vMensaje += (vEntidadTerceroActual.DigitoVerificacion.ToString() != txtDV.Text ?
                                    CuerpoCorreoModificaciones("Digito Verificación", vEntidadTerceroActual.DigitoVerificacion.ToString(), txtDV.Text) : string.Empty);

                        vMensaje += (vEntidadTerceroActual.ConsecutivoInterno != txtConsecutivoInterno.Text ?
                                    CuerpoCorreoModificaciones(lblConsecutivo.Text, vEntidadTerceroActual.ConsecutivoInterno, txtConsecutivoInterno.Text) : string.Empty);


                        if (ddlSexo.Visible == true)
                        {
                            string vSexoAntiguo = string.Empty;
                            string vSexoNuevo = ddlSexo.SelectedItem.Text;
                            ddlSexo.SelectedValue = vEntidadTerceroActual.Sexo.ToString();
                            vSexoAntiguo = ddlSexo.SelectedItem.Text;
                            vMensaje += (vSexoAntiguo != vSexoNuevo ?
                                        CuerpoCorreoModificaciones(lblSexo.Text, vSexoAntiguo, vSexoNuevo) : string.Empty);

                        }

                        if (ddlTipoDoc.SelectedItem.Text != "NIT")
                        {
                            vMensaje += (vEntidadTerceroActual.FechaNacimiento != cuFechaNac.Date ?
                                   CuerpoCorreoModificaciones(lblFechaNac.Text, string.Format("{0:dd/MM/yyyy}", vEntidadTerceroActual.FechaNacimiento), string.Format("{0:dd/MM/yyyy}", cuFechaNac.Date)) : string.Empty);

                            vMensaje += (vEntidadTerceroActual.FechaExpedicionId != cuFechaExp.Date ?
                                    CuerpoCorreoModificaciones(lblFechaExp.Text, string.Format("{0:dd/MM/yyyy}", vEntidadTerceroActual.FechaExpedicionId), string.Format("{0:dd/MM/yyyy}", cuFechaExp.Date)) : string.Empty);

                            vMensaje += (vEntidadTerceroActual.PrimerNombre != txtPrimerNombre.Text ?
                                        CuerpoCorreoModificaciones(lblPrimerNombe.Text, vEntidadTerceroActual.PrimerNombre, txtPrimerNombre.Text) : string.Empty);

                            if (!string.IsNullOrEmpty(txtSegundoNombre.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.SegundoNombre)))
                            {
                                vMensaje += (vEntidadTerceroActual.SegundoNombre != txtSegundoNombre.Text ?
                                         CuerpoCorreoModificaciones(lblSegundoNombe.Text, vEntidadTerceroActual.SegundoNombre, txtSegundoNombre.Text) : string.Empty);
                            }

                            vMensaje += (vEntidadTerceroActual.PrimerApellido != txtPrimerApellido.Text ?
                                        CuerpoCorreoModificaciones(lblPrimerApellido.Text, vEntidadTerceroActual.PrimerApellido, txtPrimerApellido.Text) : string.Empty);

                            if (!string.IsNullOrEmpty(txtSegundoApellido.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.SegundoApellido)))
                            {
                                vMensaje += (vEntidadTerceroActual.SegundoApellido != txtSegundoApellido.Text ?
                                         CuerpoCorreoModificaciones(lblSegundoApellido.Text, vEntidadTerceroActual.SegundoApellido, txtSegundoApellido.Text) : string.Empty);
                            }

                        }

                        if (tipoper.CodigoTipoPersona != "001")
                        {
                            vMensaje += (vEntidadTerceroActual.RazonSocial != txtRazonSocial.Text ?
                                    CuerpoCorreoModificaciones(lblRazonSocial.Text, vEntidadTerceroActual.RazonSocial, txtRazonSocial.Text) : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(txtIndicativo.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.Indicativo)))
                        {
                            vMensaje += (vEntidadTerceroActual.Indicativo != txtIndicativo.Text ?
                                    CuerpoCorreoModificaciones(lblIndicativo.Text, vEntidadTerceroActual.Indicativo, txtIndicativo.Text) : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(txtTelefono.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.Telefono)))
                        {
                            vMensaje += (vEntidadTerceroActual.Telefono != txtTelefono.Text ?
                                     CuerpoCorreoModificaciones(lblTelefono.Text, vEntidadTerceroActual.Telefono, txtTelefono.Text) : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(txtExtension.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.Extension)))
                        {
                            vMensaje += (vEntidadTerceroActual.Extension != txtExtension.Text ?
                                     CuerpoCorreoModificaciones(lblExtension.Text, vEntidadTerceroActual.Extension, txtExtension.Text) : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(txtCelular.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.Celular)))
                        {
                            vMensaje += (vEntidadTerceroActual.Celular != txtCelular.Text ?
                                     CuerpoCorreoModificaciones(lblCelular.Text, vEntidadTerceroActual.Celular, txtCelular.Text) : string.Empty);
                        }

                        string vDepartamentoAntiguo = string.Empty;
                        string vDepartamentoNuevo = string.Empty;
                        string vIdDepartamento = vTercero.IdDepartamento.ToString();
                        string vIdDepartamentoAntiguo = vEntidadTerceroActual.IdDepartamento.ToString();
                        ddlDepartamento.SelectedValue = vIdDepartamento;
                        vDepartamentoNuevo = ddlDepartamento.SelectedItem.Text;
                        if (vIdDepartamentoAntiguo != "0")
                        {
                            ddlDepartamento.SelectedValue = vIdDepartamentoAntiguo;
                            vDepartamentoAntiguo = ddlDepartamento.SelectedItem.Text;
                        }

                        if (ddlDepartamento.SelectedValue != "-1")
                        {
                            vMensaje += (vDepartamentoAntiguo != vDepartamentoNuevo ?
                                        CuerpoCorreoModificaciones(lblDepartamento.Text, vDepartamentoAntiguo, vDepartamentoNuevo) : string.Empty);
                        }

                        string vMunicipioAntiguo = string.Empty;
                        string vMunicipioNuevo = string.Empty;
                        string vIdMunicipioAntiguo = vEntidadTerceroActual.IdMunicipio.ToString();
                        vMunicipioNuevo = ddlMunicipio.SelectedItem.Text;

                        if (vIdMunicipioAntiguo != "-1" && vIdMunicipioAntiguo != "0")
                        {
                            CargarMunicipio(ddlDepartamento, ddlMunicipio);
                            ddlMunicipio.SelectedValue = vIdMunicipioAntiguo;
                            vMunicipioAntiguo = ddlMunicipio.SelectedItem.Text;
                        }

                        if (ddlMunicipio.SelectedValue != "-1")
                        {
                            vMensaje += (vMunicipioAntiguo != vMunicipioNuevo ?
                                        CuerpoCorreoModificaciones(lblMunicipio.Text, vMunicipioAntiguo, vMunicipioNuevo) : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(txtDireccionResidencia.TipoZona))
                        {
                            vMensaje += ((vEntidadTerceroActual.IdZona == 1 ? "U" : "R") != txtDireccionResidencia.TipoZona ?
                                         CuerpoCorreoModificaciones("Zona", vEntidadTerceroActual.IdZona == 1 ? "Urbano" : "Rural", txtDireccionResidencia.TipoZona == "U" ? "Urbano" : "Rural") : string.Empty);
                        }

                        if (!string.IsNullOrEmpty(txtDireccionResidencia.Text) || (!string.IsNullOrEmpty(vEntidadTerceroActual.Direccion)))
                        {
                            vMensaje += (vEntidadTerceroActual.Direccion != txtDireccionResidencia.Text ?
                                          CuerpoCorreoModificaciones("Dirección Residencia", vEntidadTerceroActual.Direccion, txtDireccionResidencia.Text) : string.Empty);
                        }

                        if (vTerceroDatoAdicionalActual != null && vTerceroDatoAdicionalActual.IdTercero != 0)
                        {
                            string vNivelInteresAntiguo = string.Empty;
                            string vNivelInteresNuevo = ddlNivelInteres.SelectedItem.Text;
                            ddlNivelInteres.SelectedValue = vTerceroDatoAdicionalActual.NivelInteres.ToString();
                            vNivelInteresAntiguo = ddlNivelInteres.SelectedItem.Text;
                            vMensaje += (vNivelInteresAntiguo != vNivelInteresNuevo ?
                                   CuerpoCorreoModificaciones(lblNivelInteres.Text, vNivelInteresAntiguo, vNivelInteresNuevo) : string.Empty);

                            string vClaseActividadAntiguo = string.Empty;
                            string vClaseActividadNuevo = ddlClaseActividad.SelectedItem.Text;
                            ddlClaseActividad.SelectedValue = vTerceroDatoAdicionalActual.IdClaseActividad.ToString();
                            vClaseActividadAntiguo = ddlClaseActividad.SelectedItem.Text;
                            vMensaje += (vClaseActividadAntiguo != vClaseActividadNuevo ?
                                        CuerpoCorreoModificaciones(lblClaseActividad.Text, vClaseActividadAntiguo, vClaseActividadNuevo) : string.Empty);

                            vMensaje += (vTerceroDatoAdicionalActual.Observaciones != txtObservacion.Text ?
                                        CuerpoCorreoModificaciones(lblObservacion.Text, vTerceroDatoAdicionalActual.Observaciones, txtObservacion.Text) : string.Empty);
                        }
                        else
                        {
                            vMensaje += (ddlNivelInteres.SelectedValue != "-1" ?
                                        CuerpoCorreoModificaciones(lblNivelInteres.Text, "", ddlNivelInteres.SelectedItem.Text) : string.Empty);

                            vMensaje += (ddlClaseActividad.SelectedValue != "-1" ?
                                        CuerpoCorreoModificaciones(lblClaseActividad.Text, "", ddlClaseActividad.SelectedItem.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtObservacion.Text) ?
                                        CuerpoCorreoModificaciones(lblObservacion.Text, "", txtObservacion.Text) : string.Empty);
                        }

                        if (vReferente1Actual != null)
                        {
                            string vDepartamentoRef1Antiguo = string.Empty;
                            string vDepartamentoRef1Nuevo = string.Empty;
                            vIdDepartamento = vRef1.IdDepartamento.ToString();
                            vIdDepartamentoAntiguo = vReferente1Actual.IdDepartamento.ToString();
                            if (vIdDepartamento != "0")
                                ddlDepartamento1.SelectedValue = vIdDepartamento;
                            else
                                ddlDepartamento1.SelectedValue = "-1";
                            vDepartamentoRef1Nuevo = ddlDepartamento1.SelectedItem.Text;
                            if (vDepartamentoRef1Nuevo == "Seleccione")
                                vDepartamentoRef1Nuevo = "";
                            if (vIdDepartamentoAntiguo != "0")
                            {
                                ddlDepartamento1.SelectedValue = vIdDepartamentoAntiguo;
                                vDepartamentoRef1Antiguo = ddlDepartamento1.SelectedItem.Text;
                            }
                            vMensaje += (vDepartamentoRef1Antiguo != vDepartamentoRef1Nuevo ?
                                    CuerpoCorreoModificaciones(lblDepartamento1.Text + " Referente1", vDepartamentoRef1Antiguo, vDepartamentoRef1Nuevo) : string.Empty);


                            string vMunicipioRef1Antiguo = string.Empty;
                            string vMunicipioRef1Nuevo = string.Empty;
                            vIdMunicipioAntiguo = vReferente1Actual.IdMunicipio.ToString();
                            vMunicipioRef1Nuevo = ddlMunicipio1.SelectedItem.Text;
                            if (vMunicipioRef1Nuevo == "Seleccione")
                                vMunicipioRef1Nuevo = "";

                            if (vIdMunicipioAntiguo != "-1" && vIdMunicipioAntiguo != "0")
                            {
                                CargarMunicipio(ddlDepartamento1, ddlMunicipio1);
                                ddlMunicipio1.SelectedValue = vIdMunicipioAntiguo;
                                vMunicipioRef1Antiguo = ddlMunicipio1.SelectedItem.Text;
                            }

                            vMensaje += (vMunicipioRef1Antiguo != vMunicipioRef1Nuevo ?
                                        CuerpoCorreoModificaciones(lblMunicipio1.Text + " Referente1", vMunicipioRef1Antiguo, vMunicipioRef1Nuevo) : string.Empty);

                            vMensaje += (vReferente1Actual.Nombre != txtNombre1.Text ?
                                        CuerpoCorreoModificaciones(lblNombre1.Text + " Referente1", vReferente1Actual.Nombre, txtNombre1.Text) : string.Empty);

                            vMensaje += (vReferente1Actual.Correo != txtCorreo1.Text ?
                                        CuerpoCorreoModificaciones(lblCorreo1.Text + " Referente1", vReferente1Actual.Correo, txtCorreo1.Text) : string.Empty);

                            vMensaje += (vReferente1Actual.Indicativo != txtIndicativo1.Text ?
                                        CuerpoCorreoModificaciones(lblIndicativo1.Text + " Referente1", vReferente1Actual.Indicativo, txtIndicativo1.Text) : string.Empty);

                            vMensaje += (vReferente1Actual.Telefono != txtTelefono1.Text ?
                                        CuerpoCorreoModificaciones(lblTelefono1.Text + " Referente1", vReferente1Actual.Telefono, txtTelefono1.Text) : string.Empty);

                            vMensaje += (vReferente1Actual.Extension != txtExtension1.Text ?
                                        CuerpoCorreoModificaciones(lblExtension1.Text + " Referente1", vReferente1Actual.Extension, txtExtension1.Text) : string.Empty);

                            vMensaje += (vReferente1Actual.Celular != txtCelular1.Text ?
                                        CuerpoCorreoModificaciones(lblCelular1.Text + " Referente1", vReferente1Actual.Celular, txtCelular1.Text) : string.Empty);
                        }
                        else
                        {
                            vMensaje += (ddlDepartamento1.SelectedValue != "-1" ?
                                    CuerpoCorreoModificaciones(lblDepartamento1.Text + " Referente1", "", ddlDepartamento1.SelectedItem.Text) : string.Empty);

                            vMensaje += (ddlMunicipio1.SelectedValue != "-1" ?
                                        CuerpoCorreoModificaciones(lblMunicipio1.Text + " Referente1", "", ddlMunicipio1.SelectedItem.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtNombre1.Text) ?
                                        CuerpoCorreoModificaciones(lblNombre1.Text + " Referente1", "", txtNombre1.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtCorreo1.Text) ?
                                        CuerpoCorreoModificaciones(lblCorreo1.Text + " Referente1", "", txtCorreo1.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtIndicativo1.Text) ?
                                        CuerpoCorreoModificaciones(lblIndicativo1.Text + " Referente1", "", txtIndicativo1.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtTelefono1.Text) ?
                                        CuerpoCorreoModificaciones(lblTelefono1.Text + " Referente1", "", txtTelefono1.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtExtension1.Text) ?
                                        CuerpoCorreoModificaciones(lblExtension1.Text + " Referente1", "", txtExtension1.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtCelular1.Text) ?
                                        CuerpoCorreoModificaciones(lblCelular1.Text + " Referente1", "", txtCelular1.Text) : string.Empty);
                        }

                        if (vReferente2Actual != null)
                        {

                            string vDepartamentoRef2Antiguo = string.Empty;
                            string vDepartamentoRef2Nuevo = string.Empty;
                            vIdDepartamento = vRef2.IdDepartamento.ToString();
                            vIdDepartamentoAntiguo = vReferente2Actual.IdDepartamento.ToString();
                            if (vIdDepartamento != "0")
                                ddlDepartamento2.SelectedValue = vIdDepartamento;
                            else
                                ddlDepartamento2.SelectedValue = "-1";
                            vDepartamentoRef2Nuevo = ddlDepartamento2.SelectedItem.Text;
                            if (vDepartamentoRef2Nuevo == "Seleccione")
                                vDepartamentoRef2Nuevo = "";
                            if (vIdDepartamentoAntiguo != "0")
                            {
                                ddlDepartamento2.SelectedValue = vIdDepartamentoAntiguo;
                                vDepartamentoRef2Antiguo = ddlDepartamento2.SelectedItem.Text;
                            }
                            vMensaje += (vDepartamentoRef2Antiguo != vDepartamentoRef2Nuevo ?
                                 CuerpoCorreoModificaciones(lblDepartamento2.Text + " Referente2", vDepartamentoRef2Antiguo, vDepartamentoRef2Nuevo) : string.Empty);

                            string vMunicipioRef2Antiguo = string.Empty;
                            string vMunicipioRef2Nuevo = string.Empty;
                            vIdMunicipioAntiguo = vReferente2Actual.IdMunicipio.ToString();
                            vMunicipioRef2Nuevo = ddlMunicipio2.SelectedItem.Text;
                            if (vMunicipioRef2Nuevo == "Seleccione")
                                vMunicipioRef2Nuevo = "";
                            if (vIdMunicipioAntiguo != "-1" && vIdMunicipioAntiguo != "0")
                            {
                                CargarMunicipio(ddlDepartamento2, ddlMunicipio2);
                                ddlMunicipio2.SelectedValue = vIdMunicipioAntiguo;
                                vMunicipioRef2Antiguo = ddlMunicipio2.SelectedItem.Text;
                            }


                            vMensaje += (vMunicipioRef2Antiguo != vMunicipioRef2Nuevo ?
                                        CuerpoCorreoModificaciones(lblMunicipio2.Text + " Referente2", vMunicipioRef2Antiguo, vMunicipioRef2Nuevo) : string.Empty);

                            vMensaje += (vReferente2Actual.Nombre != txtNombre2.Text ?
                                        CuerpoCorreoModificaciones(lblNombre2.Text + " Referente2", vReferente2Actual.Nombre, txtNombre2.Text) : string.Empty);

                            vMensaje += (vReferente2Actual.Correo != txtCorreo2.Text ?
                                        CuerpoCorreoModificaciones(lblCorreo2.Text + " Referente2", vReferente2Actual.Correo, txtCorreo2.Text) : string.Empty);

                            vMensaje += (vReferente2Actual.Indicativo != txtIndicativo2.Text ?
                                        CuerpoCorreoModificaciones(lblIndicativo2.Text + " Referente2", vReferente2Actual.Indicativo, txtIndicativo2.Text) : string.Empty);

                            vMensaje += (vReferente2Actual.Telefono != txtTelefono2.Text ?
                                        CuerpoCorreoModificaciones(lblTelefono2.Text + " Referente2", vReferente2Actual.Telefono, txtTelefono2.Text) : string.Empty);

                            vMensaje += (vReferente2Actual.Extension != txtExtension2.Text ?
                                        CuerpoCorreoModificaciones(lblExtension2.Text + " Referente2", vReferente2Actual.Extension, txtExtension2.Text) : string.Empty);

                            vMensaje += (vReferente2Actual.Celular != txtCelular2.Text ?
                                        CuerpoCorreoModificaciones(lblCelular2.Text + " Referente2", vReferente2Actual.Celular, txtCelular2.Text) : string.Empty);
                        }
                        else
                        {
                            vMensaje += (ddlDepartamento2.SelectedValue != "-1" ?
                                        CuerpoCorreoModificaciones(lblDepartamento2.Text + " Referente2", "", ddlDepartamento2.SelectedItem.Text) : string.Empty);

                            vMensaje += (ddlMunicipio2.SelectedValue != "-1" ?
                                        CuerpoCorreoModificaciones(lblMunicipio2.Text + " Referente2", "", ddlMunicipio2.SelectedItem.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtNombre2.Text) ?
                                        CuerpoCorreoModificaciones(lblNombre2.Text + " Referente2", "", txtNombre2.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtCorreo2.Text) ?
                                        CuerpoCorreoModificaciones(lblCorreo2.Text + " Referente2", "", txtCorreo2.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtIndicativo2.Text) ?
                                        CuerpoCorreoModificaciones(lblIndicativo2.Text + " Referente2", "", txtIndicativo2.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtTelefono2.Text) ?
                                        CuerpoCorreoModificaciones(lblTelefono2.Text + " Referente2", "", txtTelefono2.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtExtension2.Text) ?
                                        CuerpoCorreoModificaciones(lblExtension2.Text + " Referente2", "", txtExtension2.Text) : string.Empty);

                            vMensaje += (!string.IsNullOrEmpty(txtCelular2.Text) ?
                                        CuerpoCorreoModificaciones(lblCelular2.Text + " Referente2", "", txtCelular2.Text) : string.Empty);
                        }


                        vMensaje += "</table>";

                        bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);

                    }

                }
            }

            RemoveSessionParameter("vEntidadTerceroActual");
            RemoveSessionParameter("vReferente1Actual");
            RemoveSessionParameter("vReferente2Actual");
            RemoveSessionParameter("vTerceroAdicionalActual");
            if (hfCambioCorreo.Value == "1")
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Only alert Message');", true

                string App = "";
                MembershipUser membershipUser = Membership.GetUser();

                if (membershipUser != null)
                {
                    string[] vRoles = Roles.GetRolesForUser(membershipUser.UserName);
                    App = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
                }

                Session.Clear();
                Session.Abandon();

               ClientScript.RegisterStartupScript(this.GetType(), "Success", "<script type='text/javascript'>alert('Your email has been sent successfully!');window.location='~/Default" + App + ".aspx';</script>'");
                
               Response.Redirect("~/Default" + App + ".aspx");
            }
            else
            {
                SetSessionParameter("Proveedor.IdTercero", int.Parse(ViewState["IdTercero"].ToString()));
                Response.Redirect("~/page/" + PageName + "/Detail.aspx?oP=E");
            }            
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    private void GuardarUsuario()
    {
        try
        {
            string numDocumento = "";

            if (ddlTipoPersona.SelectedItem.Text != "NATURAL")
                numDocumento = txtNumeroDoc.Text.Trim();
            else
                numDocumento = txtNumIdentificacion.Text.Trim();

            var vUsuario = new Usuario
            {
                NumeroDocumento = numDocumento,
                DV = txtDV.Text.Trim(),
                PrimerNombre = txtPrimerNombre.Text.ToUpper().Trim(),
                SegundoNombre = txtSegundoNombre.Text.ToUpper().Trim(),
                PrimerApellido = txtPrimerApellido.Text.ToUpper().Trim(),
                SegundoApellido = txtSegundoApellido.Text.ToUpper().Trim(),
                CorreoElectronico = txtCuentaCorreo.Text.Trim(),
                TelefonoContacto = txtCelular.Text.Trim(),
                IdTipoDocumento = Convert.ToInt32(ddlTipoDoc.SelectedValue),
                IdTipoPersona = Convert.ToInt32(ddlTipoPersona.SelectedValue),
                IdTipoUsuario = 1,
                RazonSocial = txtRazonSocial.Text.ToUpper().Trim(),
                Rol = "PROVEEDORES",
                Estado = true
            };

            int vResultado;

            if (Request.QueryString["oP"] == "E")
            {
               // vUsuario.NombreUsuario = vRUBOService.ConsultarUsuario(Convert.ToInt32(ddlTipoDoc.SelectedValue), numDocumento).CorreoElectronico;

                Usuario vUserActual = vRUBOService.ConsultarUsuario(Convert.ToInt32(ddlTipoDoc.SelectedValue), numDocumento);

                vUsuario.NombreUsuario = vUserActual.CorreoElectronico.ToLower();

                bool existeCorreoElectronico = false;
                MembershipUserCollection oMuColection = Membership.FindUsersByEmail(txtCuentaCorreo.Text.Trim());
                if (oMuColection.Count > 0) { existeCorreoElectronico = true; }

                //sumary
                //ajuste para permitir cambio de correo de usuario comprobando si existe en el sistema
                //Néstor Amaya
                if (vUsuario.Rol.Contains("PROVEEDORES"))
                {
                    if (txtCuentaCorreo.Text.ToLower() != vUserActual.CorreoElectronico.ToLower())
                    {
                        //NavigateTo(SolutionPage.Detail);
                        if (existeCorreoElectronico == true)
                        {
                            toolBar.MostrarMensajeError("El correo electrónico ya existe en el sistema");
                            return;
                        }

                        vResultado = vRUBOService.modificarCorreoUsuario(vUsuario);
                        vUsuario.NombreUsuario = txtCuentaCorreo.Text.ToLower();
                        hfCambioCorreo.Value = "1";
                    }
                }

                vUsuario.IdUsuario = vUserActual.IdUsuario;
                vUsuario.Contrasena = "";
                vUsuario.UsuarioModificacion = GetSessionUser().NombreUsuario;
                vResultado = vRUBOService.ModificarUsuario(vUsuario);
            }
            else
            {
                vUsuario.NombreUsuario = txtCuentaCorreo.Text.Trim();
                vUsuario.Contrasena = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 10) + "D";
                vUsuario.UsuarioCreacion = GetSessionUser().NombreUsuario;
                vResultado = vRUBOService.InsertarUsuarioInterno(vUsuario);

                hfIdUsuario.Value = vUsuario.IdUsuario.ToString();

                string remitente = ConfigurationManager.AppSettings["RemitenteProveedores"];
                string asunto = "Usuario Sistema de Información de Apoyo (SIA) creado";
                string linkSitioWeb = ConfigurationManager.AppSettings["URLProveedores"];
                string name = vUsuario.IdTipoPersona == 1 ? vUsuario.PrimerNombre + " " + vUsuario.SegundoNombre + " " + vUsuario.PrimerApellido + " " + vUsuario.SegundoApellido : vUsuario.RazonSocial;

                EnviarMensajeBienvenida(remitente, vUsuario.CorreoElectronico, asunto, linkSitioWeb, name, vUsuario.CorreoElectronico, vUsuario.Contrasena);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EnviarMensajeBienvenida(string pDe, string pPara, string pAsunto, string pLinkUrl, string pNombre, string pUsuario, string pContrasena)
    {
        var myMailMessage = new MailMessage();

        myMailMessage.From = new MailAddress(pDe);
        myMailMessage.To.Add(new MailAddress(pPara));
        myMailMessage.Subject = pAsunto;
        myMailMessage.IsBodyHtml = true;
        myMailMessage.Body = "<html><body><table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'><tr><td colspan='3' " +
                            " bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td width='10%' " +
                            " bgcolor='#81BA3D'>&nbsp;</td><td><table width='100%' border='0' align='center'><tr><td width='5%'>&nbsp;</td><td  " +
                            " align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr><tr><td>&nbsp;</td><td>Usted ha sido registrado en el sistema de proveedores del ICBF, si requiere ingresar y actualizar su información, lo puede hacer con el siguiente usuario y contraseña.</td><td> " +
                            " <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr> " +
                            " <tr><td>&nbsp;</td><td>URL        : <a href='https://proveedores.icbf.gov.co/'>https://proveedores.icbf.gov.co/</a></td><td>&nbsp;</td></tr> " +
                            " <tr><td>&nbsp;</td><td>Usuario    : " + pUsuario + "</td><td>&nbsp;</td></tr> " +
                            " <tr><td>&nbsp;</td><td>Contraseña : " + pContrasena + "</td><td>&nbsp;</td></tr> " +
                            " </td></tr></table></td><td width='10%' bgcolor='#81BA3D'> " +
                            " &nbsp;</td><tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' align='center'  " +
                            " bgcolor='#81BA3D'>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo. </td></tr><tr> " +
                            " <td colspan='3' align='center' bgcolor='#81BA3D'>Si tienes alguna duda, puedes dirigirte a nuestra sección de  " +
                            " <a href='" + pLinkUrl + "' target='_blank'>Asistencia y Soporte.</a></td></tr><tr><td colspan='3' align='center'  " +
                            " bgcolor='#81BA3D'>&nbsp;</td></tr></table></body></html> ";

        var mySmtpClient = new SmtpClient();

        object userState = myMailMessage;

        mySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

        try
        {
            mySmtpClient.SendAsync(myMailMessage, userState);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo de activación fue cancelado");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
        else
        {
            toolBar.MostrarMensajeGuardado("Se envió al correo ingresado el usuario y contraseña para ingresar al sistema");
        }
    }


    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDoc.SelectedItem.Text == "CC" || this.ddlTipoDoc.SelectedItem.Text == "NIT" || this.ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.txtNumIdentificacion.MaxLength = 11;
        }
        else if (this.ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.txtNumIdentificacion.MaxLength = 6;
        }
        else if (ddlTipoDoc.SelectedItem.Text == "CARNÉ DIPLOMÁTICO")
        {
            this.txtNumIdentificacion.MaxLength = 17;
        }
    }

    private bool conCambioValidar(bool isPersona, Tercero Terceroi, Tercero Tercerof)
    {
        bool cambio = true;

        cambio = ((Terceroi.Email != null ? Terceroi.Email.Trim().ToLower() : "") == (Tercerof.Email != null ? Tercerof.Email.Trim().ToLower() : "")) && cambio;
        cambio = ((Terceroi.NumeroIdentificacion != null ? Terceroi.NumeroIdentificacion.Trim().ToLower() : "") == (Tercerof.NumeroIdentificacion != null ? Tercerof.NumeroIdentificacion.Trim().ToLower() : "")) && cambio;
        cambio = (hdUpdatedDoc.Value == "0") && cambio;

        if (isPersona)
        {
            cambio = ((Terceroi.Sexo != null ? Terceroi.Sexo.Trim().ToLower() : "") == (Tercerof.Sexo != null ? Tercerof.Sexo.Trim().ToLower() : "")) && cambio;
            cambio = ((Terceroi.PrimerNombre != null ? Terceroi.PrimerNombre.Trim().ToLower() : "") == (Tercerof.PrimerNombre != null ? Tercerof.PrimerNombre.Trim().ToLower() : "")) && cambio;
            cambio = ((Terceroi.SegundoNombre != null ? Terceroi.SegundoNombre.Trim().ToLower() : "") == (Tercerof.SegundoNombre != null ? Tercerof.SegundoNombre.Trim().ToLower() : "")) && cambio;
            cambio = ((Terceroi.PrimerApellido != null ? Terceroi.PrimerApellido.Trim().ToLower() : "") == (Tercerof.PrimerApellido != null ? Tercerof.PrimerApellido.Trim().ToLower() : "")) && cambio;
            cambio = ((Terceroi.SegundoApellido != null ? Terceroi.SegundoApellido.Trim().ToLower() : "") == (Tercerof.SegundoApellido != null ? Tercerof.SegundoApellido.Trim().ToLower() : "")) && cambio;
            cambio = (Terceroi.FechaNacimiento == Tercerof.FechaNacimiento) && cambio;
            cambio = (Terceroi.FechaExpedicionId == Tercerof.FechaExpedicionId) && cambio;
        }
        else
        {
            cambio = ((Terceroi.RazonSocial != null ? Terceroi.RazonSocial.Trim().ToLower() : "") == (Tercerof.RazonSocial != null ? Tercerof.RazonSocial.Trim().ToLower() : "")) && cambio;
        }


        return !cambio;
    }
    /// <summary>
    /// Este método se utiliza para validar que los documentos que son obligatorios se hayan ingresado
    /// </summary>
    /// <param name="pInfoFinancieraEntidad"></param>
    public void ValidarDocumentosObligatorios(Tercero pTercero, string IdTemporal)
    {
        List<DocAdjuntoTercero> documentosTerceros = new List<DocAdjuntoTercero>();
        //if (pTercero.IdTercero > 0)
        //    documentosTerceros = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(pTercero.IdTercero, pTercero.IdTipoPersona.ToString(), varIdTemporal);
        //else
        documentosTerceros = vProveedorService.ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(IdTemporal, pTercero.IdTipoPersona.ToString());

        string mensaje = string.Empty;
        foreach (DocAdjuntoTercero doc in documentosTerceros)
        {
            if (doc.Obligatorio == 1 && string.IsNullOrEmpty(doc.LinkDocumento))
            {
                mensaje += (string.Format("Falta adjuntar el documento {0}. ", doc.NombreTipoDocumento));
            }
        }

        if (!string.IsNullOrEmpty(mensaje))
        {
            throw new Exception(mensaje);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            //Cargar datos si es usuario  Externo
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                Tercero objTer = new Tercero();
                objTer = vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(vusuario.IdTipoDocumento, vusuario.NumeroDocumento);
                int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
                int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
                if (objTer.IdTercero == 0 || (objTer.IdEstadoTercero == idEstadoRegistrado || objTer.IdEstadoTercero == idEstadoPorAjustar || objTer.IdEstadoTercero == idEstadoValidado))
                {
                    //if (objTer.IdEstadoTercero == idEstadoValidado)
                    //{
                    //    //if (objTer.IdTipoPersona == 2 || objTer.IdTipoPersona == 3 || objTer.IdTipoPersona == 4)
                    //    //{
                    //    //    toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
                    //    //}
                    //}
                    //else
                    //{
                    toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
                    //}
                }
                else
                {
                    SetSessionParameter("Proveedor.IdTercero", objTer.IdTercero);
                    NavigateTo(SolutionPage.Detail);

                }
                ocultarCamposObligatorios(false);
            }
            else
            {
                //Activar asteriscos para interno
                ocultarCamposObligatorios(true);
                toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            }
            toolBar.EstablecerTitulos("Registrar Tercero", SolutionPage.Add.ToString());

            var vSeguridadService = new SeguridadService();
            lbQueEsTercero.Text = vSeguridadService.ConsultarParametro(29) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(29).ValorParametro;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Ocaulta campos obligatorios
    /// </summary>
    /// <param name="p"></param>
    private void ocultarCamposObligatorios(bool p)
    {
        lblCorreoElectronicoAS.Visible = p;
        lblIdentificaNatAS.Visible = p;
        if (p) lblNumeroDocAS.Text = "*";
        else lblNumeroDocAS.Text = "";
        lblTipoDocumentoAS.Visible = p;
        lblTipoPersonaAS.Visible = p;

        lblRazonSocialAS.Visible = p;
    }

    /// <summary>
    /// habilita los campos obligatorios para el caso de terceros externos
    /// </summary>
    /// <param name="mostrar"></param>
    private void CamposObligatoriosExterno()
    {
        bool p = new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES");
        lblIndicativoAS.Visible = p;
        rfvIndicativo.Enabled = p;

        lblTelefonoAS.Visible = p;
        rfvTelefono.Enabled = p;

        lblCelularAS.Visible = p;
        rfvCelular.Enabled = p;

        txtDireccionResidencia.Requerido = p;

    }
    ///// <summary>
    ///// habilita los campos de ubicación
    ///// </summary>
    ///// <param name="mostrar"></param>
    //private void EnabledCamposUbicacion(bool p)
    //{
    //    txtIndicativo.Enabled = p;
    //    txtTelefono.Enabled = p;
    //    txtExtension.Enabled = p;
    //    txtCelular.Enabled = p;

    //    ddlDepartamento.Enabled = p;
    //    ddlMunicipio.Enabled = p;
    //    txtDireccionResidencia.Enabled = p;

    //} 

    /// <summary>
    /// Habilita la seccion de datos adicionales para proveedores
    /// </summary>
    /// <param name="mostrar"></param>
    private void CamposDatosAdicionales(int? Estado)
    {
        bool p = (!new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"));

        string currentApp = System.Configuration.ConfigurationManager.AppSettings["CurrentApp"].ToString();
        p = (currentApp == "F" ? p : false);

        pnlDatosAdicionales.Visible = p;
        rfvClaseActividad.Enabled = p;
        pnlReferente.Visible = p;

        if (Estado == vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero)//Se obtiene el estado Por Validar
        {
            PnlContacto.Enabled = false;
            rfvIndicativo.Enabled = false;
            rfvTelefono.Enabled = false;
            cvTelefonoIndicativo.Enabled = false;
            rfvCelular.Enabled = false;
            cvLongCelular.Enabled = false;
            txtDireccionResidencia.Requerido = false;
            txtDireccionResidencia.Enabled = false;


            pnlDatosAdicionales.Enabled = false;
            rfvClaseActividad.Enabled = false;
            pnlReferente.Enabled = false;
        }

    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            String vCorreoElectronico = Convert.ToString(GetSessionParameter("Proveedor.CorreoElectronico"));
            //RemoveSessionParameter("Proveedor.CorreoElectronico");
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
            //RemoveSessionParameter("Proveedor.IdTercero");
            ViewState["IdTercero"] = vIdTercero;
            vTercero = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));
            txtCuentaCorreo.Text = vTercero.Email;
            txtCuentaUsuario.Text = vTercero.NumeroIdentificacion;
            ddlTipoPersona.SelectedValue = vTercero.IdTipoPersona.ToString();
            TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
            if (tipoper.CodigoTipoPersona == "001")
            {
                ViewControlJuridica(true);
                ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
            }
            else
            {
                ViewControlJuridica(false);
                ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
            }
            ddlTipoDoc.SelectedValue = vTercero.IdDListaTipoDocumento.ToString();
            txtNumeroDoc.Text = vTercero.NumeroIdentificacion;
            txtNumIdentificacion.Text = vTercero.NumeroIdentificacion;
            txtDV.Text = vTercero.DigitoVerificacion.ToString();
            txtConsecutivoInterno.Text = vTercero.ConsecutivoInterno;
            if (vTercero.FechaExpedicionId != null)
                cuFechaExp.Date = DateTime.Parse(vTercero.FechaExpedicionId.ToString());
            if (vTercero.FechaNacimiento != null)
                cuFechaNac.Date = DateTime.Parse(vTercero.FechaNacimiento.ToString());
            if (tipoper.CodigoTipoPersona == "001")
            {
                ddlSexo.SelectedValue = vTercero.Sexo;
                txtPrimerNombre.Text = vTercero.PrimerNombre;
                txtSegundoNombre.Text = vTercero.SegundoNombre;
                txtPrimerApellido.Text = vTercero.PrimerApellido;
                txtSegundoApellido.Text = vTercero.SegundoApellido;
            }
            else
            {
                txtRazonSocial.Text = vTercero.RazonSocial;
            }

            cargaUbicaAdicionales(vIdTercero);
            //Deshabilita los campos del tercero
            if (Request.QueryString["oP"] == "E")
            {
                deshabilitarTercero(false);
                //Habilitar o deshabilitar de acuerdo al estado
                int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
                int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
                int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                int idEstadoPorValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado                
                if (vTercero.IdEstadoTercero == idEstadoRegistrado || vTercero.IdEstadoTercero == idEstadoPorAjustar || vTercero.IdEstadoTercero == idEstadoPorValidado || vTercero.IdEstadoTercero == idEstadoPorValidar)
                {
                    if (vTercero.IdEstadoTercero == idEstadoPorValidado) // PREGUNTAR SI ES JURIDICO, CONSORCIO O UNIONTEMPORAL
                    {
                        int idTipoPer = Convert.ToInt32(ddlTipoPersona.SelectedValue);
                        if (idTipoPer == 2 || idTipoPer == 3 || idTipoPer == 4)
                        {
                            habilitarEntradaTercero(true);
                            txtNumeroDoc.Enabled = false;
                            txtNumIdentificacion.Enabled = false;
                        }
                    }
                    else
                    {
                        habilitarEntradaTercero(true);
                        txtNumeroDoc.Enabled = false;
                        txtNumIdentificacion.Enabled = false;
                    }
                }
                else
                {
                    habilitarEntradaTercero(false);
                    txtNumeroDoc.Enabled = false;
                    txtNumIdentificacion.Enabled = false;
                }

                if (vTercero.IdEstadoTercero == idEstadoPorAjustar || vTercero.IdEstadoTercero == idEstadoPorValidar)
                {
                    pObservacionesCorreccion.Visible = true;
                }
            }
            else
            {
                deshabilitarTercero(true);
            }
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                lblEstado.Visible = false;
                ddlEstadoTercero.Visible = false;
                pnlEstado.Visible = false;
            }
            //BuscarDocumentos();

            string sPersona = ddlTipoPersona.SelectedValue;
            string sIdTemporal = "";
            if (ViewState["IdTemporal"] != null)
                sIdTemporal = ViewState["IdTemporal"].ToString();
            string valoresDocs = "";
            List<DocAdjuntoTercero> listaDocs = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(int.Parse(vIdTercero), sPersona, sIdTemporal);
            if (hfValidateDocumentsTerceros.Value.Equals("0"))
            {
                foreach (DocAdjuntoTercero doc in listaDocs)
                {
                    valoresDocs = valoresDocs + doc.IdDocumento.ToString() + ",0|";
                    hfIDDocYNombreAActualizar.Value = hfIDDocYNombreAActualizar.Value + doc.IdDocumento + "," + doc.NombreTipoDocumento + "|";
                }
                hfValidateDocumentsTerceros.Value = valoresDocs;
            }

            gvDocAdjuntos.DataSource = listaDocs;
            gvDocAdjuntos.DataBind();


            CargarObservaciones();

            Session["vEntidadTerceroActual"] = vTercero;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar observaciones
    /// </summary>
    private void CargarObservaciones()
    {

        String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
        if (vIdTercero != "")
        {
            gvTerceroObservaciones.DataSource = vProveedorService.ConsultarValidarTerceros(Convert.ToInt32(vIdTercero), null, null);
            gvTerceroObservaciones.DataBind();
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            ddlSexo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlSexo.Items.Insert(1, new ListItem("Masculino", "M"));
            ddlSexo.Items.Insert(2, new ListItem("Femenino", "F"));
            ddlSexo.SelectedValue = "-1";
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstadoTercero, "-1", true);

            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento, "-1", true);
            CargarMunicipio(ddlDepartamento, ddlMunicipio);
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento1, "-1", true);
            CargarMunicipio(ddlDepartamento1, ddlMunicipio1);
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento2, "-1", true);
            CargarMunicipio(ddlDepartamento2, ddlMunicipio2);

            hdfIfReferente1.Value = "";
            ddlNivelInteres.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlNivelInteres.Items.Insert(1, new ListItem("Alto", "Alto"));
            ddlNivelInteres.Items.Insert(2, new ListItem("Medio", "Medio"));
            ddlNivelInteres.Items.Insert(3, new ListItem("Bajo", "Bajo"));
            ddlNivelInteres.SelectedValue = "-1";
            ManejoControles.ClaseActividad(ddlClaseActividad, "-1", true);

            hdfIfReferente2.Value = "";
            hdfIdTerceroDA.Value = "";
            //ManejoControles.LlenarTipoDocumento(ddlTipoDoc, "-1", true); //Se comentó para que de inicio la lista tipo de identificación este vaica!  


            CamposObligatoriosExterno();
            CamposDatosAdicionales(02);

            //Cargar datos si es usuario  Externo
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                ViewState["IdTercero"] = vTercero.IdTercero;
                Icbf.Oferente.Entity.EstadoTercero estadotercero = vProveedorService.ConsultarEstadoTercero(int.Parse(ddlEstadoTercero.SelectedValue));
                if (estadotercero.CodigoEstadoTercero == "002" || estadotercero.CodigoEstadoTercero == "003")
                {
                    toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                }
                if (estadotercero.CodigoEstadoTercero == "001" || estadotercero.CodigoEstadoTercero == "004")
                {
                    toolBar.eventoListar += new ToolBarDelegate(btnBuscar_Click);
                }
            }


            if (ViewState["IdTemporal"] == null)
            {
                ViewState["IdTemporal"] = DateTime.Now.Ticks.ToString();
            }
            ViewState["IdTercero"] = 0;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Habilita controles si es persona jurídica
    /// </summary>
    /// <param name="p"></param>
    private void ViewControlJuridica(bool p)
    {
        PnlNatural.Visible = p;
        cuFechaExp.Visible = p;
        lblFechaExp.Visible = p;
        lblRazonSocial.Visible = !p;
        txtRazonSocial.Visible = !p;
        pnlJuridica.Visible = !p;
    }

    /// <summary>
    /// Deshabilitar controles si es Tercero
    /// </summary>
    /// <param name="p"></param>
    private void deshabilitarTercero(bool p)
    {
        txtCuentaCorreo.Enabled = p;
        txtCuentaUsuario.Enabled = p;
        ddlTipoPersona.Enabled = p;
        ddlTipoDoc.Enabled = p;
        txtNumeroDoc.Enabled = p;
        txtNumIdentificacion.Enabled = p;
        txtDV.Enabled = p;
        cuFechaExp.Enabled = p;
        cuFechaNac.Enabled = p;
        ddlSexo.Enabled = p;
        txtRazonSocial.Enabled = p;
        txtPrimerNombre.Enabled = p;
        txtPrimerApellido.Enabled = p;
        txtSegundoNombre.Enabled = p;
        txtSegundoApellido.Enabled = p;
        ddlEstadoTercero.Enabled = p;
    }

    /// <summary>
    /// Habilita visibilidad de controles si es Tercero
    /// </summary>
    /// <param name="p"></param>
    private void terceroVisible(bool p)
    {
        pnlJuridica.Visible = p;
        lblFechaExp.Visible = p;
        cuFechaExp.Visible = p;
        lblFechaNac.Visible = p;
        cuFechaNac.Visible = p;
        ddlSexo.Visible = p;
        lblSexo.Visible = p;
        txtPrimerNombre.Visible = p;
        lblPrimerNombe.Visible = p;
        txtPrimerApellido.Visible = p;
        lblPrimerApellido.Visible = p;
        txtSegundoNombre.Visible = p;
        lblSegundoNombe.Visible = p;
        txtSegundoApellido.Visible = p;
        lblSegundoApellido.Visible = p;
        ddlEstadoTercero.Visible = p;
        lblEstado.Visible = p;
        pnlEstado.Visible = p;
    }

    /// <summary>
    /// Habilita controles si es Entrada Tercero
    /// </summary>
    /// <param name="p"></param>
    private void habilitarEntradaTercero(bool p)
    {
        txtCuentaCorreo.Enabled = p;
        cuFechaExp.Enabled = p;
        cuFechaNac.Enabled = p;
        ddlSexo.Enabled = p;
        txtRazonSocial.Enabled = p;
        txtPrimerNombre.Enabled = p;
        txtPrimerApellido.Enabled = p;
        txtSegundoNombre.Enabled = p;
        txtSegundoApellido.Enabled = p;
        txtNumeroDoc.Enabled = p;
        txtNumIdentificacion.Enabled = p;
        PanelAdjuntos.Enabled = p;
    }

    /// <summary>
    /// Cálculo de la DIV
    /// </summary>
    private void CalcularDiV()
    {
        int lenTxtNumeroDoc = 15 - txtNumeroDoc.Text.Length;

        if (lenTxtNumeroDoc >= 0)
        {
            var strDato = Left("000000000000000", lenTxtNumeroDoc) + txtNumeroDoc.Text;
            char[] strArray = (strDato.ToCharArray());

            int result = Convert.ToInt32(Convert.ToString(strArray[14])) * 3 +
                         Convert.ToInt32(Convert.ToString(strArray[13])) * 7 +
                         Convert.ToInt32(Convert.ToString(strArray[12])) * 13 +
                         Convert.ToInt32(Convert.ToString(strArray[11])) * 17 +
                         Convert.ToInt32(Convert.ToString(strArray[10])) * 19 +
                         Convert.ToInt32(Convert.ToString(strArray[9])) * 23 +
                         Convert.ToInt32(Convert.ToString(strArray[8])) * 29 +
                         Convert.ToInt32(Convert.ToString(strArray[7])) * 37 +
                         Convert.ToInt32(Convert.ToString(strArray[6])) * 41 +
                         Convert.ToInt32(Convert.ToString(strArray[5])) * 43 +
                         Convert.ToInt32(Convert.ToString(strArray[4])) * 47 +
                         Convert.ToInt32(Convert.ToString(strArray[3])) * 53 +
                         Convert.ToInt32(Convert.ToString(strArray[2])) * 59 +
                         Convert.ToInt32(Convert.ToString(strArray[1])) * 67 +
                         Convert.ToInt32(Convert.ToString(strArray[0])) * 71;

            int residuo = result % 11;

            if (residuo == 0)
            {
                txtDV.Text = "0";
            }
            else
            {
                if (residuo == 1)
                {
                    txtDV.Text = "1";
                }
                else
                {
                    residuo = 11 - residuo;
                    txtDV.Text = Convert.ToString(residuo);
                }
            }
        } //Fin if (lenTxtNumeroDoc >= 0)
        else
        {
            txtDV.Text = "";
        } // Fin else if (lenTxtNumeroDoc >= 0)
    }

    /// <summary>
    /// Rompe una cade hacia izquierda
    /// </summary>
    /// <param name="param"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    private static string Left(string param, int length)
    {
        string result = param.Substring(0, length);
        return result;
    }

    /// <summary>
    /// Rompe una cadena hacia derecha
    /// </summary>
    /// <param name="param"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    private static string Right(string param, int length)
    {

        int value = param.Length - length;
        string result = param.Substring(value, length);
        return result;
    }
    /// <summary>
    /// Llena lista desplegable corrrespondiente a Municipio filtrado por departamento
    /// </summary>
    protected void CargarMunicipio(DropDownList ddlDep, DropDownList ddlMun)
    {
        CargarMunicipioXDepartamento(ddlMun, Convert.ToInt32(ddlDep.SelectedValue));
    }
    /// <summary>
    /// Llena la lista desplegable Municipio con el filtro departamento
    /// </summary>
    /// <param name="PddlMunicipio"></param>
    /// <param name="pidDepartament"></param>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipio, int pidDepartament)
    {

        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipio, "-1", true, pidDepartament);
    }

    private void cargaUbicaAdicionales(string vIdTercero)
    {
        Tercero vTercero = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));

        txtIndicativo.Text = vTercero.Indicativo;
        txtTelefono.Text = vTercero.Telefono;
        txtExtension.Text = vTercero.Extension;
        txtCelular.Text = vTercero.Celular;



        if (vTercero.IdDepartamento != 0)
            ddlDepartamento.SelectedValue = vTercero.IdDepartamento.ToString();

        CargarMunicipio(ddlDepartamento, ddlMunicipio);
        if (vTercero.IdMunicipio != 0)
            ddlMunicipio.SelectedValue = vTercero.IdMunicipio.ToString();


        if (vTercero.IdZona.HasValue)
        {
            txtDireccionResidencia.TipoZona = vTercero.IdZona == 1 ? "U" : "R";
        }

        txtDireccionResidencia.Text = (vTercero.Direccion != null ? vTercero.Direccion : "");

        ddlEstadoTercero.SelectedValue = vTercero.IdEstadoTercero.ToString();


        CamposObligatoriosExterno();
        CamposDatosAdicionales(vTercero.IdEstadoTercero);
        if (!new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
        {
            TerceroDatoAdicional vTerceroDatoAdicional = vOferenteService.ConsultarTerceroDatoAdicional(int.Parse(vIdTercero));

            if (vTerceroDatoAdicional != null && vTerceroDatoAdicional.IdTercero != 0)
            {
                hdfIdTerceroDA.Value = vTerceroDatoAdicional.IdTercero.ToString();
                ddlNivelInteres.SelectedValue = vTerceroDatoAdicional.NivelInteres;
                ddlClaseActividad.SelectedValue = vTerceroDatoAdicional.IdClaseActividad.ToString();
                txtObservacion.Text = vTerceroDatoAdicional.Observaciones;
                Session["vTerceroAdicionalActual"] = vTerceroDatoAdicional;
            }

            List<Referente> vLstReferente = vOferenteService.ConsultarReferentes(int.Parse(vIdTercero));
            if (vLstReferente.Count > 0)
            {
                hdfIfReferente1.Value = vLstReferente[0].IdReferente.ToString();

                ddlDepartamento1.SelectedValue = vLstReferente[0].IdDepartamento.ToString();
                CargarMunicipio(ddlDepartamento1, ddlMunicipio1);
                ddlMunicipio1.SelectedValue = vLstReferente[0].IdMunicipio.ToString();

                txtNombre1.Text = vLstReferente[0].Nombre;
                txtCorreo1.Text = vLstReferente[0].Correo;
                txtIndicativo1.Text = vLstReferente[0].Indicativo;
                txtTelefono1.Text = vLstReferente[0].Telefono;
                txtExtension1.Text = vLstReferente[0].Extension;
                txtCelular1.Text = vLstReferente[0].Celular;
                Session["vReferente1Actual"] = vLstReferente[0];
            }
            if (vLstReferente.Count > 1)
            {
                hdfIfReferente2.Value = vLstReferente[1].IdReferente.ToString();

                ddlDepartamento2.SelectedValue = vLstReferente[1].IdDepartamento.ToString();
                CargarMunicipio(ddlDepartamento2, ddlMunicipio2);
                ddlMunicipio2.SelectedValue = vLstReferente[1].IdMunicipio.ToString();

                txtNombre2.Text = vLstReferente[1].Nombre;
                txtCorreo2.Text = vLstReferente[1].Correo;
                txtIndicativo2.Text = vLstReferente[1].Indicativo;
                txtTelefono2.Text = vLstReferente[1].Telefono;
                txtExtension2.Text = vLstReferente[1].Extension;
                txtCelular2.Text = vLstReferente[1].Celular;
                Session["vReferente2Actual"] = vLstReferente[1];
            }
        }

    }

    #region documentos

    protected void gvDocAdjuntos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocAdjuntos.PageIndex = e.NewPageIndex;

        BuscarDocumentos();
    }

    protected void gvDocAdjuntos_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            GridViewRow row = gvDocAdjuntos.Rows[e.RowIndex];
            int idDocAdjunto = (int)gvDocAdjuntos.DataKeys[e.RowIndex].Value;
            DocAdjuntoTercero doc = vProveedorService.ConsultarDocAdjuntoTercero(idDocAdjunto);
            vProveedorService.EliminarDocAdjuntoTercero(doc);
            BuscarDocumentos();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void gvDocAdjuntos_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDocAdjuntos.EditIndex = e.NewEditIndex;
        BuscarDocumentos();
    }

    protected void gvDocAdjuntos_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDocAdjuntos.EditIndex = -1;
        BuscarDocumentos();
    }

    protected void gvDocAdjuntos_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow row = gvDocAdjuntos.Rows[e.RowIndex];

            int vIdDocAdjunto = int.Parse(gvDocAdjuntos.DataKeys[e.RowIndex]["IdDocAdjunto"].ToString());
            int vMaxPermitidoKB = int.Parse(gvDocAdjuntos.DataKeys[e.RowIndex]["MaxPermitidoKB"].ToString());
            string vExtensionesPermitidas = gvDocAdjuntos.DataKeys[e.RowIndex]["ExtensionesPermitidas"].ToString();
            DocAdjuntoTercero doc = new DocAdjuntoTercero();

            if (vIdDocAdjunto > 0)
            {
                doc = vProveedorService.ConsultarDocAdjuntoTercero(vIdDocAdjunto);
            }
            else
            {
                //int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));

                //int vIdTercero = Convert.ToInt32(ViewState["IdTercero"].ToString());
                //if (vIdTercero != 0)
                //{
                //    doc.IdTercero = vIdTercero;
                //}
                //else
                //{
                //    //Se asocia un Id temporal 
                //    doc.IdTemporal = ViewState["IdTemporal"].ToString();
                //}

                doc.NombreTipoDocumento = ((Label)(row.Cells[0].FindControl("lblNombreDocumento"))).Text;
                doc.Descripcion = string.Empty;
                doc.IdDocumento = int.Parse(gvDocAdjuntos.DataKeys[e.RowIndex]["IdDocumento"].ToString());
                doc.FechaCrea = DateTime.Now;
                doc.UsuarioCrea = GetSessionUser().NombreUsuario;
            }

            FileUpload fuDocumento = ((FileUpload)(row.Cells[1].FindControl("fuDocumento")));

            //Se envia por ftp
            doc.LinkDocumento = SubirArchivo(fuDocumento, vMaxPermitidoKB, vExtensionesPermitidas);
            doc.FechaModifica = DateTime.Now;
            doc.UsuarioModifica = GetSessionUser().NombreUsuario;
            //Se asocia un Id temporal 
            doc.IdTemporal = ViewState["IdTemporal"].ToString();

            InformacionAudioria(doc, this.PageName, vSolutionPage);


            //if (vIdDocAdjunto > 0)//Existe
            //{
            //    vProveedorService.ModificarDocAdjuntoTercero(doc);
            //}
            //else
            //{
            //    vProveedorService.InsertarDocAdjuntoTercero(doc);
            //}
            vProveedorService.InsertarDocAdjuntoTercero(doc);
            //ya se actualizó el Doc, entonces actualizar el string del campo oculto que nos servira para las validaciones.
            ActualizaValoresDocTerceros(doc.IdDocumento.ToString());
            gvDocAdjuntos.EditIndex = -1;

            hdUpdatedDoc.Value = "1";
            BuscarDocumentos();
            toolBar.LipiarMensajeError();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void gvDocAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected string SubirArchivo(FileUpload fuArchivo, int pMaxPermitidoKB, string pExtensionesPermitidas)
    {
        try
        {
            if (!fuArchivo.HasFile)
            {
                throw new Exception("Debe seleccionar un archivo.");
            }


            int idformatoArchivo;

            string filename = Path.GetFileName(fuArchivo.FileName);

            String NombreArchivoOri = filename;

            string tipoArchivo = "";
            foreach (string sExt in pExtensionesPermitidas.Split(','))
            {
                if (filename.Substring(filename.LastIndexOf(".") + 1).ToUpper() == sExt.ToUpper())
                {
                    tipoArchivo = string.Format("General.archivo{0}", sExt.ToUpper());
                    break;
                }
            }

            if (tipoArchivo == "")
            {
                throw new Exception(string.Format("Solo se permiten los siguientes archivos: {0}", pExtensionesPermitidas));
            }

            if (fuArchivo.PostedFile.ContentLength / 1024 > pMaxPermitidoKB)
            {
                throw new Exception(string.Format("Solo se permiten archivos inferiores a {0} KB", (pMaxPermitidoKB)));
            }

            SIAService vRUBOService = new SIAService();

            List<FormatoArchivo> vFormatoArchivo = vRUBOService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

            if (vFormatoArchivo.Count == 0)
            {
                throw new Exception("No se ha parametrizado la extensión del archivo a cargar");
            }

            idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

            //Creación del Nombre del archivo
            string NombreArchivo = filename.Substring(0, filename.IndexOf("."));

            NombreArchivo = vRUBOService.QuitarEspacios(NombreArchivo);

            NombreArchivo = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString()
                + "_ProveedorDatosBasico_" + NombreArchivo + filename.Substring(filename.LastIndexOf("."));

            //NombreArchivo = Guid.NewGuid().ToString() + NombreArchivo;
            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

            //Guardado del archivo en el servidor
            HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;
            Boolean ArchivoSubido = vRUBOService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

            if (ArchivoSubido)
            {
                return NombreArchivo;
            }
            else
            {
                throw new Exception("No se pudo subir el archivo al ftp.");
            }


            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BuscarDocumentos()
    {
        try
        {

            //int vIdTercero = int.Parse(ViewState["IdTercero"].ToString());
            string sPersona = ddlTipoPersona.SelectedValue;
            //if (vIdTercero > 0)
            //{
            //    gvDocAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(vIdTercero, sPersona);
            //    gvDocAdjuntos.DataBind();
            //}
            //else 
            //{
            string vIdTemporal = ViewState["IdTemporal"].ToString();
            string valoresDocs = "";
            List<DocAdjuntoTercero> listaDocs = vProveedorService.ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(vIdTemporal, sPersona);

            if (hfValidateDocumentsTerceros.Value.Equals("0"))
            {
                foreach (DocAdjuntoTercero doc in listaDocs)
                {
                    valoresDocs = valoresDocs + doc.IdDocumento.ToString() + ",0|";
                    hfIDDocYNombreAActualizar.Value = hfIDDocYNombreAActualizar.Value + doc.IdDocumento + "," + doc.NombreTipoDocumento + "|";
                }
                if (!valoresDocs.Equals(""))
                {
                    hfValidateDocumentsTerceros.Value = valoresDocs;
                }
            }

            gvDocAdjuntos.DataSource = listaDocs;
            gvDocAdjuntos.DataBind();
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Método que revisa el valor del campo oculto que contiene el control de las validaciones
    /// para documentos registrados en terceros. Retorna IdsTipoDocumentos separados por comas que deben
    /// Ser Actualizados, y retorna "0" si ya todos están actualizados.
    /// </summary>
    /// 
    private string checkActualizaDocumentosTerceros()
    {
        string docsActualizar = "";
        string valor = hfValidateDocumentsTerceros.Value;
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);

        string[] documentos = valor.Split('|');
        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[1].Equals("0"))
                docsActualizar = docsActualizar + documentos[i].Split(',')[0] + ",";
        }

        if (docsActualizar.Length > 0)
            if (docsActualizar.Substring(docsActualizar.Length - 1, 1).Equals(","))
                docsActualizar = docsActualizar.Substring(0, docsActualizar.Length - 1);

        return docsActualizar;
    }

    /// <summary>
    /// Valida documentos oblilgatorios Al momento de Editar.
    /// Recibe como parametro un Tercero.
    ///objNuevo que viene como parametro es el objeto a Guardar osea el que crea apartir del formulario.
    /// </summary>
    public void ValidarDocumentosObligatoriosEdicion(Tercero objNuevo)
    {
        string vTipoPersoona = ddlTipoPersona.SelectedValue;
        Tercero varTerceroActual = vOferenteService.ConsultarTercero(objNuevo.IdTercero);
        List<DocAdjuntoTercero> ListaDocsTercero = null;
        bool updateDocuments = false;
        string documentosTercerosPorActualizar = "";
        if (objNuevo.IdTipoPersona != 1 && (!objNuevo.RazonSocial.Equals(varTerceroActual.RazonSocial)))
        {
            updateDocuments = true;
        }

        string mensaje = string.Empty;
        if (updateDocuments)
        {
            documentosTercerosPorActualizar = checkActualizaDocumentosTerceros();
            ListaDocsTercero = vProveedorService.ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(ViewState["IdTemporal"].ToString(), vTipoPersoona);
            string[] arrUpDocsTerceros = documentosTercerosPorActualizar.Split(',');

            foreach (DocAdjuntoTercero doc in ListaDocsTercero)
            {
                for (int t = 0; t < arrUpDocsTerceros.Length; t++)
                {
                    if (arrUpDocsTerceros[t].Equals(doc.IdDocumento.ToString()))
                        mensaje += (string.Format("Debe actualizar el documento {0}. ", doc.NombreTipoDocumento));
                }
            }
        }


        if (!string.IsNullOrEmpty(mensaje))
        {
            throw new Exception(mensaje);
        }

    }

    /// <summary>
    /// Método que va actualizando el string contenido en el campo oculto hfValidateDocumentsTerceros.
    /// tal valor nos ayudará en las validaciones de los documentos, este valor se actualiza cada vez
    /// que un documento ya adjunto se actualiza.
    /// </summary>
    private void ActualizaValoresDocTerceros(string idTipoDoc)
    {
        string valor = hfValidateDocumentsTerceros.Value;
        string nuevoValor = "";
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);
        string[] documentos = valor.Split('|');

        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[0].Equals(idTipoDoc))
            {
                nuevoValor = nuevoValor + idTipoDoc + ",1|";
            }
            else
            {
                nuevoValor = nuevoValor + documentos[i] + "|";
            }
        }
        hfValidateDocumentsTerceros.Value = nuevoValor;
    }
    #endregion documentos

    /// <summary>
    /// Autor: Ingenian Software - Cristhian López - 23/03/2017
    /// Metodo que concatena el cuerpo del mensaje para enviar el correo de las modificaciones realizadas al tercero.
    /// </summary>
    /// <param name="pNombreCampo">Nombre del campo que se modificó</param>
    /// <param name="pCampoActual">Valor modificado del campo</param>
    /// <param name="pCampoNuevo">Nuevo valor del campo modificado.</param>
    /// <returns>Retorna un string con el cuerpo del mensaje concatenado</returns>
    private string CuerpoCorreoModificaciones(string pNombreCampo, string pCampoActual, string pCampoNuevo)
    {
        string MensajeCorreo = string.Empty;
        MensajeCorreo = "<tr> <td style='border: 1px solid #000000; width:40%;'> " + pNombreCampo + "</td>" +
                       " <td style='border: 1px solid #000000; width:30%;'>" + pCampoActual + "</td>" +
                       "<td style='border: 1px solid #000000; width:30%;'>" + pCampoNuevo + "</td>" +
                       "</tr>";
        return MensajeCorreo;
    }

    /// <summary>
    /// Autor: Ingenian Software - Cristhian López - 23/03/2017
    /// Método que guarda las observaciones realizadas, según las modificaciones al tercero.
    /// </summary>
    /// <returns>Resultado de la operación.</returns>
    private bool GuardarObservacionesCorreccion()
    {
        try
        {
            int vResultado;
            int vidEstado = 0;
            ValidarTercero vValidarTercero = new ValidarTercero();
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
            vValidarTercero.IdTercero = Convert.ToInt32(vIdTercero);
            vValidarTercero.Observaciones = Convert.ToString(txtObservacionesCorreccion.Text).Trim();
            vValidarTercero.ConfirmaYAprueba = true;
            vValidarTercero.UsuarioCrea = GetSessionUser().CorreoElectronico;
            vValidarTercero.FechaCrea = DateTime.Now;

            InformacionAudioria(vValidarTercero, this.PageName, vSolutionPage);


            if (vValidarTercero.ConfirmaYAprueba)
            {
                vidEstado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
                vValidarTercero.TipoIncidente = (int)Enumeradores.TipoIncidente.Ninguno; // No muestra los checks de tipo incidente.
            }

            vResultado = vProveedorService.InsertarValidarTercero(vValidarTercero, vidEstado);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operaci&oacute;n no se complet&oacute; satisfactoriamente, verifique por favor.");
                return false;
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ValidarTercero.IdValidarTercero", vValidarTercero.IdValidarTercero);
                return true;
            }
            return true;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }
    #endregion


}
