<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="CargarTercero.aspx.cs" Inherits="Page_Proveedor_GestionTercero_CargarTercero" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Tipo de archivo *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoArchivo" ControlToValidate="ddlTipoArchivo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoArchivo"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Archivo a cargar *
                <asp:RequiredFieldValidator runat="server" ID="rfvArchivo" ControlToValidate="fuplArchivoCargue"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload runat="server" ID="fuplArchivoCargue"></asp:FileUpload>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" class="auto-style1">
                <asp:LinkButton runat="server" ID="lbInconsistencias" Visible="false" Text="Descargar Archivo Inconsistencias" OnClick="hlInconsistencias_Click"></asp:LinkButton>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlResultado" Visible="false">
        <br />
        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2" style="text-align: center;">
                    <b>Información cargada con éxito</b>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Número de registros cargados a BD
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRegistrosBD"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <b>Número de registros de Excel </b>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRegistrosExcel" style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre del archivo cargado
                </td>
                <td>
                    <asp:Label runat="server" ID="lblArchivoCargado"></asp:Label>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha de cargue
                </td>
                <td>
                    <asp:Label runat="server" ID="lblFechaCargue"></asp:Label>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Nombre de usuario
                </td>
                <td>
                    <asp:Label runat="server" ID="lblNombreUsuario"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlTablaHistorico" Visible="false">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="grvHistorico" runat="server" AutoGenerateColumns="false" AllowPaging="True"
                        OnPageIndexChanging="grvHistorico_PageIndexChanging" AllowSorting="true" OnSorting="grvHistorico_Sorting">
                        <Columns>
                            <asp:BoundField HeaderText="No." DataField="idAnalisis"  SortExpression="idAnalisis" />
                            <asp:BoundField HeaderText="Nombre de archivo" DataField="nombreArchivo" SortExpression="nombreArchivo"/>
                            <asp:BoundField HeaderText="Fecha" DataField="fechaCarga" SortExpression="fechaCarga" DataFormatString="{0:MM/dd/yyyy HH:mm}"/>
                            <asp:BoundField HeaderText="Registros cargados" DataField="CantidadRegistrosDB" />
                            <asp:BoundField HeaderText="Nombre de usuario" DataField="UsuarioCrea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
        <script type="text/javascript">
            //document.getElementById('btnGuardar').addEventListener("click", LoadingBuscar);
            function LoadingBuscar() {
                if (document.getElementById('<%= fuplArchivoCargue.ClientID %>').files.length != 0 && document.getElementById('<%= ddlTipoArchivo.ClientID %>').value != -1) {
                    var lblloading = document.getElementById("lblloading");
                    lblloading.style.visibility = "visible";
                }
                return true;
            }
    </script>
</asp:Content>
