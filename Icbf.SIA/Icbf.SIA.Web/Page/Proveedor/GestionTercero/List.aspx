<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>
    
    <style type="text/css">
        .popup
        {
            display: none;
        }
    </style>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                </td>
                <td class="Cell" >
                    <asp:LinkButton ID="lnkQueEsTercero"  style="float:right" runat="server" OnClientClick="return false;">&iquest;Qu&eacute; es un tercero?</asp:LinkButton>
                    <asp:Label runat="server" Text="" ID="lbQueEsTercero" CssClass="popup"></asp:Label>
                    <Ajax:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="lnkQueEsTercero"
                        BalloonPopupControlID="lbQueEsTercero" Position="BottomLeft" BalloonStyle="Rectangle"
                        BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                        UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona o asociaci&oacute;n."></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoPersona" Width="90%" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoDoc" Width="90%" AutoPostBack="True" OnSelectedIndexChanged="ddlTipoDoc_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de identificaci&oacute;n"></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTercero" runat="server" Text="Tercero"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumeroDoc" Width="89%" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                    <%--<Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtNumeroDoc"
                        FilterType="Numbers" ValidChars="/1234567890" />--%>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtTercero" Width="89%" MaxLength="256" AutoPostBack="True"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblActividad" runat="server" Text="Clase de Actividad"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlEstado" Width="90%">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlActividad" Width="90%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedor" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Email,IdTercero" CellPadding="0"
                        Height="16px" AllowSorting="true" OnPageIndexChanging="gvProveedor_PageIndexChanging"
                        OnSelectedIndexChanged="gvProveedor_SelectedIndexChanged" 
                        OnSorting="gvProveedor_Sorting" 
                        EmptyDataText="No se encuentran terceros registrados por este usuario">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Tipo de Identificaci&oacute;n" DataField="NombreListaTipoDocumento"
                                SortExpression="NombreListaTipoDocumento" />
                            <asp:BoundField HeaderText="N&uacute;mero Identificaci&oacute;n" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <%--<asp:BoundField HeaderText="Tercero" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial"/>--%>
                            <asp:TemplateField HeaderText="Tercero" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre_Razonsocial">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre_Razonsocial")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Correo Electr&oacute;nico" DataField="Email" SortExpression="Email" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstadoTercero" SortExpression="NombreEstadoTercero" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
