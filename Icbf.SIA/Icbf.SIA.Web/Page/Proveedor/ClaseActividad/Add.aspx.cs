using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de registro y edición de tipos de cargo
/// </summary>
public partial class Page_ClaseActividad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/ClaseActividad";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ClaseActividad vClaseActividad = new ClaseActividad();

            vClaseActividad.Codigo = Convert.ToString(txtCodigo.Text).Trim();
            vClaseActividad.Descripcion = Convert.ToString(txtDescripcion.Text).Trim();
            vClaseActividad.Estado = Convert.ToBoolean(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vClaseActividad.IdClaseActividad = Convert.ToInt32(hfIdClaseActividad.Value);
                vClaseActividad.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vClaseActividad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarClaseActividad(vClaseActividad);
            }
            else
            {
                vClaseActividad.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vClaseActividad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarClaseActividad(vClaseActividad);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ClaseActividad.IdClaseActividad", vClaseActividad.IdClaseActividad);
                if (Request.QueryString["oP"] == "E")
                    SetSessionParameter("ClaseActividad.Modificado", "1");
                else
                    SetSessionParameter("ClaseActividad.Creado", "1");
               
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó más registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Clase de Actividad", SolutionPage.Add.ToString());
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdClaseActividad = Convert.ToInt32(GetSessionParameter("ClaseActividad.IdClaseActividad"));
            RemoveSessionParameter("ClaseActividad.Id");

            ClaseActividad vClaseActividad = new ClaseActividad();
            vClaseActividad = vProveedorService.ConsultarClaseActividad(vIdClaseActividad);
            hfIdClaseActividad.Value = vClaseActividad.IdClaseActividad.ToString();
            txtDescripcion.Text = vClaseActividad.Descripcion;
            txtCodigo.Text = vClaseActividad.Codigo;
            rblEstado.SelectedValue = vClaseActividad.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vClaseActividad.UsuarioCrea, vClaseActividad.FechaCrea, vClaseActividad.UsuarioModifica, vClaseActividad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            if (Request.QueryString["oP"] == null)
            {
                rblEstado.SelectedIndex = 0;
                rblEstado.Enabled = false;
            }


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
