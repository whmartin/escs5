using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página que despliega el detalle del registro de Clase de Actividad
/// </summary>
public partial class Page_ClaseActividad_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/ClaseActividad";
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ClaseActividad.IdClaseActividad", hfIdClaseActividad.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdClaseActividad = Convert.ToInt32(GetSessionParameter("ClaseActividad.IdClaseActividad"));
            RemoveSessionParameter("ClaseActividad.IdClaseActividad");

            if (GetSessionParameter("ClaseActividad.Creado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("Registro almacenado en forma exitosa");
            RemoveSessionParameter("ClaseActividad.Creado");

            if (GetSessionParameter("ClaseActividad.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("Registro modificado en forma exitosa");
            RemoveSessionParameter("ClaseActividad.Modificado");


            ClaseActividad vClaseActividad = new ClaseActividad();
            vClaseActividad = vProveedorService.ConsultarClaseActividad(vIdClaseActividad);
            hfIdClaseActividad.Value = vClaseActividad.IdClaseActividad.ToString();
            txtCodigo.Text = vClaseActividad.Codigo;
            txtDescripcion.Text = vClaseActividad.Descripcion;
            rblEstado.SelectedValue = vClaseActividad.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdClaseActividad.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vClaseActividad.UsuarioCrea, vClaseActividad.FechaCrea, vClaseActividad.UsuarioModifica, vClaseActividad.FechaModifica);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Clase de Actividad", SolutionPage.Detail.ToString());
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));

            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
