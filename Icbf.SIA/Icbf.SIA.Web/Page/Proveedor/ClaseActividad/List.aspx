<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ClaseActividad_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código 
            </td>
            <td>
                Descripción 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigo" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="50" Width="300px"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado 
            </td>
            <td>
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo" Value="True" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvClaseActividad" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdClaseActividad" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvClaseActividad_PageIndexChanging" 
                        OnSelectedIndexChanged="gvClaseActividad_SelectedIndexChanged"
                        AllowSorting="true" OnSorting="gvClaseActividad_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código Clase de Actividad" DataField="Codigo" SortExpression="Codigo"/>
                            <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion"/>
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("Estado") ? "Activo" : "Inactivo") %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <div >
                                <table cellpadding="0" cellspacing="0" summary="" width="90%" align="center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Código Clase de Actividad</th>
                                            <th scope="col">Descripción</th>
                                            <th scope="col">Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="rowB">
                                            <td colspan="3">
                                                No existen listas parametrizadas<strong></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </EmptyDataTemplate>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
