<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ClaseActividad_Add" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<script type="text/javascript" language="javascript">

    function funValidaDescipcionLongitud(source, args) {
        if (args.Value.length > 0) {
            args.IsValid = EsDescripcionValido(args.Value);
        } else {
            args.IsValid = true;
        }
    }

    function EsDescripcionValido(nombre) {
        
        if (nombre.length > 3) {
            return true;
        } else if (nombre.length == 2) {
            var car1 = nombre.charAt(0);
            var car2 = nombre.charAt(1);

            if (car1 != car2) {
                return true;
            }
        } else if (nombre.length == 3) {
            
            var cara1 = nombre.charAt(0);
            var cara2 = nombre.charAt(1);
            var cara3 = nombre.charAt(2);

            if (nombre.indexOf(' ') > -1)
                return false;

            if (cara1 != cara2 && cara2 != cara3) {
                return true;
            }
            
        }
        return false;
    }

    function CheckLength(sender, args) {
        var control = $('#' + sender.controltovalidate + '');
        var strDatos = control.val();
        var strSplitDatos = strDatos.split("");

        if (args.Value.length == 2) {
            if (strSplitDatos[0] == (strSplitDatos[1])) {
                args.IsValid = false;
                sender.errormessage = "Caracteres iguales no válidos";
                return;
            }
        }

        if (args.Value.length < 2) {
            args.IsValid = false;
            sender.errormessage = "No cumple con la longitud permitida";
            return;
        }

        if (args.Value.length == 3) {
            if ((strSplitDatos[0] == strSplitDatos[1]) && (strSplitDatos[1] == strSplitDatos[2])) {
                args.IsValid = false;
                sender.errormessage = "Caracteres iguales no válidos";
                return;
            }
        }

        if (args.Value.length < 3) {
            args.IsValid = false;
            sender.errormessage = "No cumple con la longitud permitida";
            return;
        }

    }

</script>

<asp:HiddenField ID="hfIdClaseActividad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigo" ControlToValidate="txtCodigo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>

            </td>
            <td>
                Descripción *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                 <asp:CustomValidator runat="server" ID="cvDescripcion" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtDescripcion" ClientValidationFunction="funValidaDescipcionLongitud"
                        ErrorMessage="Registre una descripción válida"></asp:CustomValidator>
                 <asp:CustomValidator runat="server" ID="cvTxtDescripcion" SetFocusOnError="True"
                        ClientValidationFunction="CheckLength" ErrorMessage="Caracteres no válidos, registre una descripción"
                        ValidationGroup="btnGuardar" ControlToValidate="txtDescripcion" Display="Dynamic"
                        ForeColor="Red"></asp:CustomValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigo" MaxLength="10" ToolTip="C&oacute;digo de la clase de actividad"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigo" runat="server" TargetControlID="txtCodigo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="0123456789" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="50" ToolTip="Descripci&oacute;n de la clase de actividad" Width="300px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789 " />
            </td>
            
        </tr>
        <tr class="rowB">
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Debe seleccionar un estado" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo" Value="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
          </tr>        
    </table>
</asp:Content>
