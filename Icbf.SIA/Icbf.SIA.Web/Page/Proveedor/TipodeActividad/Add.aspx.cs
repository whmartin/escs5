using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de registro y edición de tipos de actividad
/// </summary>
public partial class Page_TipodeActividad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/TipodeActividad";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipodeActividad vTipodeActividad = new TipodeActividad();

            vTipodeActividad.CodigoTipodeActividad = Convert.ToString(txtCodigoTipodeActividad.Text).Trim();
            vTipodeActividad.Descripcion = Convert.ToString(txtDescripcion.Text).Trim();
            vTipodeActividad.Estado = Convert.ToBoolean(Convert.ToInt16(rblEstado.SelectedValue));
            if (Request.QueryString["oP"] == "E")
            {
            vTipodeActividad.IdTipodeActividad = Convert.ToInt32(hfIdTipodeActividad.Value);
                vTipodeActividad.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipodeActividad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarTipodeActividad(vTipodeActividad);
            }
            else
            {
                vTipodeActividad.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipodeActividad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarTipodeActividad(vTipodeActividad);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipodeActividad.IdTipodeActividad", vTipodeActividad.IdTipodeActividad);
                SetSessionParameter("TipodeActividad.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tipo de Actividad", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTipodeActividad = Convert.ToInt32(GetSessionParameter("TipodeActividad.IdTipodeActividad"));
            RemoveSessionParameter("TipodeActividad.Id");

            TipodeActividad vTipodeActividad = new TipodeActividad();
            vTipodeActividad = vProveedorService.ConsultarTipodeActividad(vIdTipodeActividad);
            hfIdTipodeActividad.Value = vTipodeActividad.IdTipodeActividad.ToString();
            txtCodigoTipodeActividad.Text = vTipodeActividad.CodigoTipodeActividad;
            txtDescripcion.Text = vTipodeActividad.Descripcion;
            rblEstado.SelectedValue = Convert.ToString(Convert.ToInt32(vTipodeActividad.Estado));
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipodeActividad.UsuarioCrea, vTipodeActividad.FechaCrea, vTipodeActividad.UsuarioModifica, vTipodeActividad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
