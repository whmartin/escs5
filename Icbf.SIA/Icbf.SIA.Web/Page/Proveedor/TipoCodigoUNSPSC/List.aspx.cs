using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tipos de c�digo UNSPSC
/// </summary>
public partial class Page_TipoCodigoUNSPSC_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/TipoCodigoUNSPSC";
    ProveedorService vProveedorService = new ProveedorService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Proveedor/TablaParametrica/List.aspx");
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigo = null;
            String vDescripcion = null;
            Boolean? vEstado = null;
            if (txtCodigo.Text!= "")
            {
                vCodigo = Convert.ToString(txtCodigo.Text);
            }
            if (txtDescripcion.Text!= "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }
            if (rblEstado.SelectedValue!= "-1")
            {
                vEstado = Convert.ToBoolean(rblEstado.SelectedValue);
            }
            gvTipoCodigoUNSPSC.DataSource = vProveedorService.ConsultarTipoCodigoUNSPSCs( vCodigo, vDescripcion, vEstado, null,null,null,null);
            SetSessionParameter("gvTipoCodigoUNSPSC.DataSource", gvTipoCodigoUNSPSC.DataSource);
            gvTipoCodigoUNSPSC.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvTipoCodigoUNSPSC.PageSize = PageSize();
            gvTipoCodigoUNSPSC.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo C&oacute;digo UNSPSC", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoCodigoUNSPSC.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoCodigoUNSPSC.IdTipoCodUNSPSC", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoCodigoUNSPSC_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoCodigoUNSPSC.SelectedRow);
    }
    protected void gvTipoCodigoUNSPSC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoCodigoUNSPSC.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoCodigoUNSPSC.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoCodigoUNSPSC.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Ordenar Grilla (Revisar si se generaliza)

    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<TipoCodigoUNSPSC> SortList(List<TipoCodigoUNSPSC> data, bool isPageIndexChanging)
    {
        List<TipoCodigoUNSPSC> result = new List<TipoCodigoUNSPSC>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }
    protected void gvTipoCodigoUNSPSC_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvTipoCodigoUNSPSC.PageIndex;

        if (GetSessionParameter("gvTipoCodigoUNSPSC.DataSource") != null)
        {
            if (gvTipoCodigoUNSPSC.DataSource == null) { gvTipoCodigoUNSPSC.DataSource = (List<TipoCodigoUNSPSC>)GetSessionParameter("gvTipoCodigoUNSPSC.DataSource"); }
            gvTipoCodigoUNSPSC.DataSource = SortList((List<TipoCodigoUNSPSC>)gvTipoCodigoUNSPSC.DataSource, false);
            gvTipoCodigoUNSPSC.DataBind();
        }
        gvTipoCodigoUNSPSC.PageIndex = pageIndex;
    }

    #endregion
}
