using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Seguridad.Business;
using Icbf.Seguridad.Entity;

/// <summary>
/// Página de registro y edición de códigos UNSPSC
/// </summary>
public partial class Page_TipoCodigoUNSPSC_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/TipoCodigoUNSPSC";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();

            vTipoCodigoUNSPSC.Codigo = Convert.ToString(txtCodigo.Text).Trim();
            vTipoCodigoUNSPSC.Descripcion = Convert.ToString(txtDescripcion.Text).Trim();
            vTipoCodigoUNSPSC.Estado = Convert.ToBoolean(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vTipoCodigoUNSPSC.IdTipoCodUNSPSC = Convert.ToInt32(hfIdTipoCodUNSPSC.Value);
                vTipoCodigoUNSPSC.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoCodigoUNSPSC, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarTipoCodigoUNSPSC(vTipoCodigoUNSPSC);
            }
            else
            {
                vTipoCodigoUNSPSC.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoCodigoUNSPSC, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarTipoCodigoUNSPSC(vTipoCodigoUNSPSC);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoCodigoUNSPSC.IdTipoCodUNSPSC", vTipoCodigoUNSPSC.IdTipoCodUNSPSC);
                SetSessionParameter("TipoCodigoUNSPSC.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tipo Código UNSPSC", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTipoCodUNSPSC = Convert.ToInt32(GetSessionParameter("TipoCodigoUNSPSC.IdTipoCodUNSPSC"));
            RemoveSessionParameter("TipoCodigoUNSPSC.Id");

            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
            vTipoCodigoUNSPSC = vProveedorService.ConsultarTipoCodigoUNSPSC(vIdTipoCodUNSPSC);
            hfIdTipoCodUNSPSC.Value = vTipoCodigoUNSPSC.IdTipoCodUNSPSC.ToString();
            txtCodigo.Text = vTipoCodigoUNSPSC.Codigo;
            txtDescripcion.Text = vTipoCodigoUNSPSC.Descripcion;
            rblEstado.SelectedValue = vTipoCodigoUNSPSC.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoCodigoUNSPSC.UsuarioCrea, vTipoCodigoUNSPSC.FechaCrea, vTipoCodigoUNSPSC.UsuarioModifica, vTipoCodigoUNSPSC.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //Se restringe la búsqueda de la Logitud UNSPSC configurada
            ParametroBLL objParametro = new ParametroBLL();
            List<Parametro> listParametro = objParametro.ConsultarParametros("Logitud UNSPSC", null, null, null);
            if (listParametro.Count > 0)
            {
                int nLongitud;
                if (int.TryParse(listParametro[0].ValorParametro, out nLongitud))
                {
                    txtCodigo.MaxLength = nLongitud;
                    SetSessionParameter("Proveedor.LogitudUNSPSC", nLongitud);
                }
                else
                {
                    toolBar.MostrarMensajeError("El parámetro configurado para la longitud de los códigos UNSPSC no es numérico.");
                }
            } 

            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
