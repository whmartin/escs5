using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;
using System.IO;
using EntidadProvOferente = Icbf.Proveedor.Entity.EntidadProvOferente;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de documentos adjuntos a m�dulo financiero
/// </summary>
public partial class Page_DocFinancieraProv_List : GeneralWeb
{
    //General_General_Master_Lupa toolBar;
    string PageName = "Proveedor/DocFinancieraProv";
    ProveedorService vProveedorService = new ProveedorService();


    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;

        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }

    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Manejador de evento click para el bot�n Retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {

    }

    protected void gvDocFinancieraProv_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void gvDocFinancieraProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocFinancieraProv.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            string vIdInfoFin = Request.QueryString["IdInfoFin"].ToString();
            string vRupRenovado = Request.QueryString["RupRenovado"].ToString();

            InfoFinancieraEntidad infoFinancieraEntidad = vProveedorService.ConsultarInfoFinancieraEntidad(int.Parse(vIdInfoFin));

            SIAService vRuboService = new SIAService();


            EntidadProvOferente entidadProveedor = vProveedorService.ConsultarEntidadProvOferente(infoFinancieraEntidad.IdEntidad);

            Icbf.SIA.Entity.Vigencia vigencia = vRuboService.ConsultarVigencia(infoFinancieraEntidad.IdVigencia);

            Icbf.Oferente.Entity.Tercero tercero = vProveedorService.ConsultarTercero(entidadProveedor.IdTercero);


            txtProveedor.Text = tercero.Nombre_Razonsocial;
            txtVigencia.Text = vigencia.AcnoVigencia.ToString();

            OferenteService vOferenteService = new OferenteService();
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(infoFinancieraEntidad.IdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            gvDocFinancieraProv.DataSource = vProveedorService.ConsultarDocFinancieraProv_IdInfoFin_Rup(int.Parse(vIdInfoFin), vRupRenovado, vTipoPersona, vTipoSector);
            gvDocFinancieraProv.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Procedimiento para comandos del rengl�n para la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocFinancieraProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", '', 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        return;
                    }
                    break;
            }
        }
        catch (Exception) { }
    }
}
