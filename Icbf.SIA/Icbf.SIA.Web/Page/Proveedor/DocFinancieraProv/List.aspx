<%@ Page Title="Documentos Financieros" Language="C#" MasterPageFile="~/General/General/Master/main3.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_DocFinancieraProv_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <div class="f"></div>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Proveedor
            </td>
            <td>
                Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtProveedor" Enabled="false" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVigencia"  Enabled="false"></asp:TextBox>
            </td>
        </tr>        
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDocFinancieraProv" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvDocFinancieraProv_PageIndexChanging" 
                        OnSelectedIndexChanged="gvDocFinancieraProv_SelectedIndexChanged"
                        OnRowCommand = "gvDocFinancieraProv_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png"
                                        Height="18px" Width="18px" ToolTip="Detalle" CommandName="VerDocumento" 
                                        CommandArgument='<%# Eval("LinkDocumento") %>' 
                                        Visible = '<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre del documento" DataField="NombreDocumento" />
                            <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorInfoFinanciera_") + "ProveedorInfoFinanciera_".Length) %>' ></asp:Label>               
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>                    
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
