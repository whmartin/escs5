<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" 
Inherits="Page_Proveedor_ValidarTercero_List" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>

    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
       <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona o asociaci&oacute;n"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoPersona"
                    Width="90%" AutoPostBack="True" 
                    onselectedindexchanged="ddlTipoPersona_SelectedIndexChanged">
                </asp:DropDownList>               
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoDoc" OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged" AutoPostBack="true" 
                    Width="90%">
                </asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de identificaci&oacute;n"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblTercero" runat="server" Text="Tercero"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroDoc" Width="89%"  onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="fttxtNumeroDoc" runat="server" TargetControlID="txtNumeroDoc"
                    FilterType="Numbers" ValidChars="" />--%>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtTercero" 
                    Width="89%"  AutoPostBack="True" MaxLength="256"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="Label1" runat="server" Text="Fecha Registro"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                 <asp:DropDownList runat="server" ID="ddlEstado" 
                    Width="90%">
                </asp:DropDownList>
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFecha" runat="server" />
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedor" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Email,IdTercero" 
                        CellPadding="0" Height="16px"
                        AllowSorting="true"
                        
                        OnRowCommand="gvProveedor_RowCommand"
                        OnPageIndexChanging="gvProveedor_PageIndexChanging" 
                        OnSelectedIndexChanged="gvProveedor_SelectedIndexChanged" 
                        onsorting="gvProveedor_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnLiberar" runat="server" CommandName="Liberar" CommandArgument='<%# Eval("IdTercero") %>' ImageUrl="~/Image/btn/cancel.jpg"
                                        Height="16px" Width="16px" ToolTip="Liberar" Visible = '<%# (bool)Eval("IdEstadoTercero").Equals(vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Tipo de Identificaci&oacute;n" DataField="NombreListaTipoDocumento" SortExpression="NombreListaTipoDocumento" />
                            <asp:BoundField HeaderText="N&uacute;mero Identificaci&oacute;n" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"/>
                           <%-- <asp:BoundField HeaderText="Tercero" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial"/>--%>
                           <asp:TemplateField HeaderText="Tercero" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre_Razonsocial">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre_Razonsocial")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Correo Electr&oacute;nico" DataField="Email" SortExpression="Email"/>
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstadoTercero" SortExpression="NombreEstadoTercero" />
                            <asp:BoundField HeaderText="Fecha Registro" DataField="FechaCrea" SortExpression="FechaCrea"  DataFormatString="{0:d}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
