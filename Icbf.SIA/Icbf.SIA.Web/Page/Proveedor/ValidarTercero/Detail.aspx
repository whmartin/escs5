<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Proveedor_ValidarTercero_Detail"
    Async="true" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }

        function CheckLength(sender, args) {
            var control = $('#' + sender.controltovalidate + '');
            var strDatos = control.val();
            var strSplitDatos = strDatos.split("");

            if (strSplitDatos[0] == (strSplitDatos[1])) {
                args.IsValid = false;
                sender.errormessage = "Caracteres iguales no v�lidos";
            } else {
                args.IsValid = (args.Value.length >= 2);
                if (args.IsValid == false) {
                    sender.errormessage = "No cumple con la longitud permitida";
                }
            }
        }

        function Subsanable() {
            if (confirm('Esta seguro que el incidente es NO SUBSANABLE?')) {
                return true;
            }
            else { return false; }
        }


        function enviarComunicado() {
            if (Page_ClientValidate('btnGuardar')) {
                var yaEnvioComunicado = '<%=hdfEnvioComunicado.Value %>';
                if (yaEnvioComunicado == 0) {
                    document.getElementById('<%=btnEnviarComunicado.ClientID%>').style.display = 'none'
                    document.getElementById('<%=lblEnviando.ClientID%>').style.display = 'inherit'
                    return true;
                }
                else {
                    if (confirm('Esta comunicaci\u00f3n ya ha sido enviada. Confirma enviarla nuevamente?')) {
                        document.getElementById('<%=btnEnviarComunicado.ClientID%>').style.display = 'none'
                        document.getElementById('<%=lblEnviando.ClientID%>').style.display = 'inherit'
                        return true;
                    }
                    else
                    { return false; }
                }
            }
        }

        function soloLetrasYnumeros(e) {
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            caracteresEspeciales = "#%=�|!&*{}[]+";

            if (caracteresEspeciales.indexOf(tecla) >= 0)
                return false;
            else if (caracteresEspeciales.indexOf(tecla) == -1)
                return true;
        }

        function validaNroDocumento(e) {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;

            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>
    <asp:HiddenField ID="hfCorreoElectronico" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblCuentaUsuario" runat="server" Text="Cuenta de Usuario"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblCorreoElectronico" runat="server" Text="Correo Electr&oacute;nico"></asp:Label>
                <%-- <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&acute;lida, int&ecute;ntelo de nuevo"
                        ControlToValidate="txtCuentaCorreo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                </asp:RegularExpressionValidator>--%>
                <%-- <asp:RequiredFieldValidator runat="server" ID="rfvCorreoElectronico" ErrorMessage="Correo electr�nico en blanco, Registre un correo v�lido"
                    ControlToValidate="txtCuentaCorreo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic">Correo electr�nico en blanco, Registre un correo v�lido</asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtCuentaUsuario" Width="80%" Enabled="false"></asp:TextBox>
            </td>
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtCuentaCorreo" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona o asociaci&oacute;n"></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona o asociaci&oacute;n"
                    ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="rfvTipoPersona"
                    WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlTipoDoc" OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Seleccione un tipo de identificaci�n"
                    ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="rfvTipoDoc"
                    WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="pnlJuridica" Visible="True" Style="width: 100%">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td class="Cell" width="50%;">
                                <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de identificaci&oacute;n"
                                    Width="80%"></asp:Label>DV
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:Label ID="lblRazonSocial" runat="server" Text="Razon Social" Width="25%"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtNumeroDoc" onkeypress="return validaNroDocumento(event)"
                                    Width="74%"></asp:TextBox>
                                <asp:TextBox runat="server" ID="txtDV" Width="3%"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtRazonSocial" Width="80%" TextMode="MultiLine"
                                    Rows="3"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="fteRazonSocial" runat="server" TargetControlID="txtRazonSocial"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <%--<asp:RequiredFieldValidator runat="server" ID="rfvRazonSocial" ErrorMessage="Registre su raz�n social"
                                    ControlToValidate="txtRazonSocial" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                                <%--  <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="rfvRazonSocial"
                                    WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2">
                <asp:Panel runat="server" ID="PnlNatural" Visible="True" Style="width: 100%">
                    <table style="width: 100%">
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblIdentificaNat" runat="server" Text="N&uacute;mero de Identificaci&oacute;n"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblFechaExp" runat="server" Text="Fecha de Expedici&oacute;n"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtNumIdentificacion" Width="80%" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                            </td>
                            <td class="Cell" width="50%;">
                                <uc1:fecha ID="cuFechaExp" runat="server" Requerid="false" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblFechaNac" runat="server" Text="Fecha de Nacimiento"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSexo" runat="server" Text="Sexo"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <uc1:fecha ID="cuFechaNac" runat="server" Requerid="false" />
                            </td>
                            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlSexo">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Seleccione un tipo de identificaci�n"
                                    ControlToValidate="ddlSexo" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un valor de la lista</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblPrimerNombe" runat="server" Text="Primer Nombre"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSegundoNombe" runat="server" Text="Segundo Nombre"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtPrimerNombre" Width="80%"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <%--  <asp:RequiredFieldValidator runat="server" ID="rfvPrimerNombre" ErrorMessage="Dato en blanco, Registre su primer nombre"
                                        ControlToValidate="txtPrimerNombre" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                                <%-- <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="server" TargetControlID="rfvPrimerNombre"
                                        WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                                <%-- <asp:CustomValidator runat="server" ID="cvTxtPrimerNombre" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLength" ErrorMessage="registre su primer nombre"
                                        ValidationGroup="btnGuardar" ControlToValidate="txtPrimerNombre" Display="Dynamic"
                                        ForeColor="Red" Text="*"></asp:CustomValidator>--%>
                                <%-- <Ajax:ValidatorCalloutExtender ID="vceCvTxtPrimerNombre" runat="server" TargetControlID="cvTxtPrimerNombre"
                                        WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtSegundoNombre" Width="80%"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <%-- <asp:CustomValidator runat="server" ID="cvTxtSegundoNombre" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLength" ErrorMessage="registre su segundo nombre"
                                        ValidationGroup="btnGuardar" ControlToValidate="txtSegundoNombre" Display="Dynamic"
                                        ForeColor="Red" Text="*"></asp:CustomValidator>--%>
                                <%-- <Ajax:ValidatorCalloutExtender ID="vceCvTxtSegundoNombre" runat="server" TargetControlID="cvTxtSegundoNombre"
                                        WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                <asp:Label ID="lblPrimerApellido" runat="server" Text="Primer Apellido"></asp:Label>
                            </td>
                            <td class="Cell">
                                <asp:Label ID="lblSegundoApellido" runat="server" Text="Segundo Apellido"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtPrimerApellido" Width="80%"></asp:TextBox>
                                <%--  <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <%--     <asp:RequiredFieldValidator runat="server" ID="rfvPrimerApellido" ErrorMessage="Dato en blanco, Registre su primer apellido"
                                        ControlToValidate="txtPrimerApellido" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                                <%-- <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="server" TargetControlID="rfvPrimerApellido"
                                        WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                                <%--  <asp:CustomValidator runat="server" ID="cvTxtPrimerApellido" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLength" ErrorMessage="registre su primer apellido"
                                        ValidationGroup="btnGuardar" ControlToValidate="txtPrimerApellido" Display="Dynamic"
                                        ForeColor="Red" Text="*"></asp:CustomValidator>--%>
                                <%-- <Ajax:ValidatorCalloutExtender ID="vceCvTxtPrimerApellido" runat="server" TargetControlID="cvTxtPrimerApellido"
                                        WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="txtSegundoApellido" Width="80%"></asp:TextBox>
                                <%--  <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="������������ .,@_():;" />--%>
                                <%-- <asp:CustomValidator runat="server" ID="cvTxtSegundoApellido" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLength" ErrorMessage="registre su segundo apellido"
                                        ValidationGroup="btnGuardar" ControlToValidate="txtSegundoApellido" Display="Dynamic"
                                        ForeColor="Red" Text="*"></asp:CustomValidator>--%>
                                <%-- <Ajax:ValidatorCalloutExtender ID="vceCvTxtSegundoApellido" runat="server" TargetControlID="cvTxtSegundoApellido"
                                        WarningIconImageUrl="../../../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
            </td>
            <td class="Cell">
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlEstadoTercero">
                </asp:DropDownList>
            </td>
            <td class="Cell">
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="PanelAdjuntos">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px" DataKeyName="IDDOCADJUNTO"
                        OnRowCommand="gvAdjuntos_RowCommand" OnSelectedIndexChanged="gvAdjuntos_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreTipoDocumento" />
                            <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Formato" DataField="ExtensionesPermitidas" Visible="false" />
                            <asp:BoundField HeaderText="Tama&ntilde;o KB" DataField="MaxPermitidoKB" Visible="false" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="viewLogo" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                        CommandArgument='<%#Eval("LinkDocumento")%>' Height="16px" Width="16px" ToolTip="Detalle"
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlObservaciones">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObservaciones" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:TemplateField HeaderText="Confirma que los datos coinciden con el documento adjunto y lo aprueba?">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("ConfirmaYAprueba") ? "Si" : "No") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Observaci&oacute;n" DataField="Observaciones" />--%>
                            <asp:TemplateField HeaderText="Tipo de Incidente">
                                <ItemTemplate>
                                    <asp:Label ID="lblTipoIncidente" runat="server" Text='<%# (int)Eval("TipoIncidente") == 0 ? "" : (int)Eval("TipoIncidente") == 1 ? "Subsanable" : "No Subsanable" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Observaci&oacute;n">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtObservaciones" runat="server" Enabled="false" Text='<%# Eval("Observaciones") %>'
                                        TextMode="MultiLine" Width="300px" Rows="8"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Observaci&oacute;n" DataField="FechaCrea" />
                            <asp:BoundField HeaderText="Validado Por" DataField="UsuarioCrea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlConfirma">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblConfirma" runat="server" Text="Confirma que los datos coinciden con el documento adjunto y aprueba el documento?"></asp:Label>
                    *
                    <asp:RequiredFieldValidator runat="server" ID="rfvrblConfirma" ErrorMessage="Confirme si los datos coinciden con el documento adjunto y aprueba el documento"
                        ControlToValidate="rblConfirma" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" Enabled="True"></asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTipoIncidente" runat="server" Visible="false" Text="Tipo de Incidente"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="rblConfirma" AutoPostBack="True" RepeatDirection="Horizontal"
                        OnSelectedIndexChanged="rblConfirma_SelectedIndexChanged">
                        <asp:ListItem Text="Seleccione" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Si" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:RadioButton id="rdTipoIncidente1" Text="Subsanable  " Checked="True" GroupName="RadioGroup1" Visible="false" runat="server" />
                    <asp:RadioButton id="rdTipoIncidente2" Text="No Subsanable" GroupName="RadioGroup1" Visible="false" runat="server" onclick="return Subsanable();"/>
                    <br />
                    <br/>
                    <asp:Button ID="btnEnviarComunicado" runat="server" Visible="false" ValidationGroup="btnGuardar"
                        Text="Enviar Comunicaci&oacute;n" OnClientClick="return enviarComunicado();"
                        OnClick="btnEnviarComunicado_Click" />
                    <asp:Label ID="notaComunicado" runat="server" Text="Para enviar el comunicado, antes guarde la informaci&oacute;n."
                        Visible="false" />
                    <asp:Label ID="lblEnviando" runat="server" Style="display: none" Text="...Enviando" />
                    <asp:HiddenField ID="hdfEnvioComunicado" runat="server" Value="0" />
                </td>
            </tr>
        </table>
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell" style="width:100%">
                    Observaciones
                    <asp:Label ID="lblObservaciones" runat="server" Text="*" Visible="false"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtObservaciones" ErrorMessage="Ingrese las observaciones de la validaci&oacute;n documental por el cual no fue aprobado"
                        ControlToValidate="txtObservaciones" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" colspan="2" style="width:100%">
                    <asp:TextBox runat="server" ID="txtObservaciones" CssClass="TextBoxGrande" Style="width: 85%;
                        height: 50px;" onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);"
                        TextMode="MultiLine" onkeypress="return soloLetrasYnumeros(event)"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
