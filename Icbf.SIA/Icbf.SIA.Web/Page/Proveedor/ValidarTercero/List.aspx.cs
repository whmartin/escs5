using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.Linq.Expressions;

/// <summary>
/// P�gina de validaci�n y aprobaci�n de terceros registrados
/// </summary>
public partial class Page_Proveedor_ValidarTercero_List : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/ValidarTercero";
    public ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();
    ManejoControles ManejoControles = new ManejoControles();
    bool _sorted = false;
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
        
       
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) {
                    CargarTipoDocumentotipoPersona();
                    GetState(Page.Master, PageName);
                    Buscar(); 
                }
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
       
    }
    
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarTipoDocumentotipoPersona();
    }

    protected void CargarTipoDocumentotipoPersona()
    {
        string idTipoPersona = new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
        }
    }
    
    protected void gvProveedor_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProveedor.SelectedRow);
    }
    
    /// <summary>
    /// Comando de rengl�n para una grilla Proveedor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvProveedor_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Liberar":
                    //Despues de cargar se cambia el estado a Registrado
                    int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;
                    int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
                    int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;
                    //String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
                    Tercero vTercero = new Tercero();
                    vTercero = vProveedorService.ConsultarTercero(int.Parse(e.CommandArgument.ToString()));

                    if (vTercero.IdEstadoTercero == idEstadoEnValidacion)  //Solo si est� en estado En Validaci�n se puede devolver a Registrado
                    {
                        vTercero.IdEstadoTercero = idEstadoRegistrado; //Estado En Validaci�n
                        vTercero.UsuarioModifica = GetSessionUser().NombreUsuario;
                        vTercero.FechaModifica = DateTime.Now;

                        int resultado = vProveedorService.ModificarTercero_EstadoTercero(vTercero);
                       Buscar();

                        if (resultado == 1)
                        {
                            toolBar.MostrarMensajeGuardado("Se ha cambiado al estado Registrado");
                            
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("No pudo cambiarse al estado En Validaci�n");
                        }
                    }
                    break;

            }

        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
    
    }

    protected void gvProveedor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedor.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    private void Buscar()
    {
        try
        {
            CargarGrilla(gvProveedor, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    #region M�todos

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int vIdTipoPersona = 0;
            int vIdTipoDocIdentifica = 0;
            string  vNumeroIdentificacion = "";
            string vTercero = "";
            int vEstado = 0;
            DateTime? vFechaRegistro = null ;
            
            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDoc.SelectedValue != "")
            {
                
                vIdTipoDocIdentifica = int.Parse(ddlTipoDoc.SelectedValue); 
            }
            if (txtNumeroDoc.Text != "")
            {
                vNumeroIdentificacion = txtNumeroDoc.Text.Trim();
            }
            if (txtTercero.Text != "")
            {
                vTercero = txtTercero.Text;
            }
            if (ddlEstado.SelectedValue != "-1") 
            {
                vEstado = int.Parse(ddlEstado.SelectedValue); 
            }

            if (txtFecha.Date != Convert.ToDateTime("1900/01/01"))
            {
                vFechaRegistro = txtFecha.Date;
            }
            else
            {
                vFechaRegistro = DateTime.MinValue;
            }

            string vUsuario = GetSessionUser().NombreUsuario;
            var myGridResults = vProveedorService.ConsultarTerceros(vIdTipoDocIdentifica, vEstado, vIdTipoPersona, vNumeroIdentificacion, vFechaRegistro.Value, vTercero);
            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Tercero), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<Tercero, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //Consultando si la persona que inici� sesi�n es externo o interno
            Usuario vusuario = (Usuario)(GetSessionParameter("App.User"));
            if (vRUBOService.ConsultarUsuario(vusuario.NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                
                mostrarParametrosBusqueda(false);
            }
            else
            {
                
                toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                mostrarParametrosBusqueda(true);
            }
            
            gvProveedor.PageSize = PageSize();
            gvProveedor.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Validar Terceros", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Mostrar/ocultar panel de consulta
    /// </summary>
    /// <param name="p"></param>
    private void mostrarParametrosBusqueda(bool p)
    {
        pnlConsulta.Visible = p;
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strCorreoValue = gvProveedor.DataKeys[rowIndex].Values[0].ToString();
            string strIdTerceroValue = gvProveedor.DataKeys[rowIndex].Values[1].ToString();
            SetSessionParameter("Proveedor.CorreoElectronico", strCorreoValue);
            SetSessionParameter("Proveedor.IdTercero", strIdTerceroValue);

            Icbf.Oferente.Entity.Tercero tercero = vProveedorService.ConsultarTercero(Convert.ToInt32(strIdTerceroValue));
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;//Se obtiene el estado En Validaci�n

            Response.Redirect("~/page/" + PageName + "/Detail.aspx");
                
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstado, "-1", true);
            if (GetSessionParameter("Proveedor.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Proveedor.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Proteger registro
    /// </summary>
    /// <param name="pRow"></param>
    private void ProtegerRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strCorreoValue = gvProveedor.DataKeys[rowIndex].Values[0].ToString();
            string strIdTerceroValue = gvProveedor.DataKeys[rowIndex].Values[1].ToString();
            //SetSessionParameter("Proveedor.CorreoElectronico", strCorreoValue);
            //SetSessionParameter("Proveedor.IdTercero", strIdTerceroValue);

            Icbf.Oferente.Entity.Tercero tercero = vProveedorService.ConsultarTercero(Convert.ToInt32(strIdTerceroValue));
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;//Se obtiene el estado En Validaci�n
            tercero.IdEstadoTercero = idEstadoEnValidacion; //Estado En Validaci�n
            tercero.UsuarioModifica = GetSessionUser().NombreUsuario;
            tercero.FechaModifica = DateTime.Now;

            int resultado = vProveedorService.ModificarTercero_EstadoTercero(tercero);

            if (resultado == 1)
            {
                toolBar.MostrarMensajeError("Registro protegido para validaci�n");
            }
            else
            {
                toolBar.MostrarMensajeError("No se pudo proteger el registro para validaci�n");
            }
            //NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    #region ordenarGrilla

    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvProveedor_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumeroDoc.Text = String.Empty;
            this.txtNumeroDoc.Focus();
        }
    }
    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDoc.SelectedItem.Text == "CC" || this.ddlTipoDoc.SelectedItem.Text == "NIT" || this.ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.txtNumeroDoc.MaxLength = 11;

        }
        else if (this.ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.txtNumeroDoc.MaxLength = 6;
        }
        else
        {
            this.txtNumeroDoc.MaxLength = 17;
        }
    }
    #endregion

}
