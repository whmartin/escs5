using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using Icbf.Seguridad.Service;


/// <summary>
/// P�gina que despliega el detalle del registro de m�dulo de validaci�n de proveedores
/// </summary>
public partial class Page_Proveedor_ValidarTercero_Detail : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/ValidarTercero";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vSIAServ = new SIAService();
    static Tercero objValidar = new Tercero();
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);

    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void btnEditar_Click(object sender, EventArgs e)
    //{

    //    Response.Redirect("~/page/" + PageName + "/Add.aspx?oP=E");
    //}

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            String vCorreoElectronico = Convert.ToString(GetSessionParameter("Proveedor.CorreoElectronico"));
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));

            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTercero(int.Parse(vIdTercero));
            txtCuentaUsuario.Text = vTercero.UsuarioCrea;
            objValidar = vTercero;
            if (vTercero.Email != null)
                txtCuentaCorreo.Text = vTercero.Email.ToLower();

            ddlTipoPersona.SelectedValue = vTercero.IdTipoPersona.ToString();
            TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
            if (tipoper.CodigoTipoPersona == "001")
            {
                ViewControlJuridica(true);
                ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
            }
            else
            {
                ViewControlJuridica(false);
                ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
            }

            ddlTipoDoc.SelectedValue = vTercero.IdDListaTipoDocumento.ToString();
            txtNumeroDoc.Text = vTercero.NumeroIdentificacion;
            txtNumIdentificacion.Text = vTercero.NumeroIdentificacion;
            txtDV.Text = vTercero.DigitoVerificacion.ToString();

            if (vTercero.FechaExpedicionId != null)
                cuFechaExp.Date = DateTime.Parse(vTercero.FechaExpedicionId.ToString());
            if (vTercero.FechaNacimiento != null)
                cuFechaNac.Date = DateTime.Parse(vTercero.FechaNacimiento.ToString());
            if (vTercero.Sexo == null ||vTercero.Sexo.Equals(" "))
                ddlSexo.SelectedValue = "-1";
            else
                ddlSexo.SelectedValue = vTercero.Sexo;

            txtRazonSocial.Text = vTercero.RazonSocial;
            txtPrimerNombre.Text = vTercero.PrimerNombre;
            txtSegundoNombre.Text = vTercero.SegundoNombre;
            txtPrimerApellido.Text = vTercero.PrimerApellido;
            txtSegundoApellido.Text = vTercero.SegundoApellido;

            ddlEstadoTercero.SelectedValue = vTercero.IdEstadoTercero.ToString();

            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;
            int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;

            if (ddlEstadoTercero.SelectedValue == idEstadoValidado.ToString())
            {
                pnlConfirma.Enabled = false;
                pnlConfirma.Visible = false;
                toolBar.MostrarMensajeGuardado("El tercero ya fue validado");
            }
            if (ddlEstadoTercero.SelectedValue == idEstadoEnValidacion.ToString() && vTercero.UsuarioModifica != GetSessionUser().NombreUsuario)
            {
                pnlConfirma.Visible = false;
                pnlConfirma.Enabled = false;
                toolBar.MostrarMensajeGuardado("El tercero est&aacute; siendo validado por otro usuario");
            }


            cargarGrillaImagenes(int.Parse(vIdTercero));

            cargarObservaciones();
            BuscarDocumentos();

            deshabilitarTercero(false);
            if (Request.QueryString["oP"] == "E")
            {
                toolBar.MostrarMensajeGuardado("La informaci&oacute;n ha sido registrada en forma exitosa");
            }

            cuFechaExp.NoChecarFormato();
            cuFechaNac.NoChecarFormato();
            //Despues de cargar se cambia el estado a En Validaci�n

            if (vTercero.IdEstadoTercero == idEstadoRegistrado)  //Solo si est� en estado Registrado se puede Validar
            {
                vTercero.IdEstadoTercero = idEstadoEnValidacion; //Estado En Validaci�n
                vTercero.UsuarioModifica = GetSessionUser().NombreUsuario;
                vTercero.FechaModifica = DateTime.Now;

                int resultado = vProveedorService.ModificarTercero_EstadoTercero(vTercero);

                if (resultado == 1)
                {
                    //El estado del tercero ahora est� En Validaci�n
                }
                else
                {
                    toolBar.MostrarMensajeError("No pudo cambiarse al estado En Validaci&oacute;n");
                }
            }


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    /// Carga observaciones
    /// </summary>
    private void cargarObservaciones()
    {
        String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
        gvObservaciones.DataSource = vProveedorService.ConsultarValidarTerceros(Convert.ToInt32(vIdTercero), null, null);
        gvObservaciones.DataBind();


    }

    protected void gvAdjuntos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int rowIndex = gvAdjuntos.SelectedIndex;
        string strValue = gvAdjuntos.DataKeys[rowIndex].Value.ToString();
        ViewState["IdDocTercero"] = strValue;
    }

    /// <summary>
    /// Busca documentos y los asigna a una grilla
    /// </summary>
    private void BuscarDocumentos()
    {
        try
        {
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
            string sPersona = ddlTipoPersona.SelectedValue;
            gvAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(Convert.ToInt32(vIdTercero), sPersona,"");
            gvAdjuntos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda datos del formulario a una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ValidarTercero vValidarTercero = new ValidarTercero();
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
            vValidarTercero.IdTercero = Convert.ToInt32(vIdTercero);
            vValidarTercero.Observaciones = Convert.ToString(txtObservaciones.Text).Trim();
            vValidarTercero.ConfirmaYAprueba = Convert.ToBoolean(rblConfirma.SelectedValue);
            vValidarTercero.UsuarioCrea = GetSessionUser().CorreoElectronico;
            vValidarTercero.FechaCrea = DateTime.Now;

            InformacionAudioria(vValidarTercero, this.PageName, vSolutionPage);

            int idEstado = 0;
            if (vValidarTercero.ConfirmaYAprueba)
            {
                idEstado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
                vValidarTercero.TipoIncidente = (int)Enumeradores.TipoIncidente.Ninguno; // No muestra los checks de tipo incidente.
            }
            else if (rdTipoIncidente1.Checked)
            {
                idEstado = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
                vValidarTercero.TipoIncidente = (int)Enumeradores.TipoIncidente.Subsanable;
            }

            if (rdTipoIncidente2.Checked)
            {
                idEstado = vProveedorService.ConsultarEstadoTercero("006").IdEstadoTercero;//Se obtiene el estado Por Eliminar
                vValidarTercero.TipoIncidente = (int)Enumeradores.TipoIncidente.NoSubsanable;
            }

            vResultado = vProveedorService.InsertarValidarTercero(vValidarTercero, idEstado);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operaci&oacute;n no se complet&oacute; satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ValidarTercero.IdValidarTercero", vValidarTercero.IdValidarTercero);
                SetSessionParameter("ValidarTercero.Guardado", "1");
                cargarObservaciones();
                toolBar.MostrarMensajeGuardado("La operaci&oacute;n se complet&oacute; satisfactoriamente");
                //NavigateTo(SolutionPage.Detail);  En vez de recargar la p�gina es mejor recargar la informaci�n.
                // para que en caso de haber elejido "NO", se mantenga visible el bot�n de enviar comunicado.
                btnEnviarComunicado.Enabled = true;
                notaComunicado.Visible = false;
                CargarDatos();
                //rblConfirma.SelectedIndex = 0;
            }
            else
            {
                toolBar.MostrarMensajeError("La operaci&oacute;n no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region m�todos

    /// <summary>
    /// deshabilitar controles si Tercero
    /// </summary>
    /// <param name="p"></param>
    private void deshabilitarTercero(bool p)
    {
        txtCuentaCorreo.Enabled = p;
        txtCuentaUsuario.Enabled = p;
        ddlTipoPersona.Enabled = p;
        ddlTipoDoc.Enabled = p;
        txtNumeroDoc.Enabled = p;
        txtNumIdentificacion.Enabled = p;
        txtDV.Enabled = p;
        cuFechaExp.Enabled = p;
        cuFechaNac.Enabled = p;
        ddlSexo.Enabled = p;
        txtRazonSocial.Enabled = p;
        txtPrimerNombre.Enabled = p;
        txtPrimerApellido.Enabled = p;
        txtSegundoNombre.Enabled = p;
        txtSegundoApellido.Enabled = p;
        ddlEstadoTercero.Enabled = p;
    }

    /// <summary>
    /// Habilita controles Entrada Tercero
    /// </summary>
    /// <param name="p"></param>
    private void habilitarEntradaTercero(bool p)
    {
        txtCuentaCorreo.Enabled = p;
        cuFechaExp.Enabled = p;
        ddlSexo.Enabled = p;
        txtRazonSocial.Enabled = p;
        txtPrimerNombre.Enabled = p;
        txtPrimerApellido.Enabled = p;
        txtSegundoNombre.Enabled = p;
        txtSegundoApellido.Enabled = p;
        txtNumeroDoc.Enabled = p;
        txtNumIdentificacion.Enabled = p;
    }

    /// <summary>
    /// Habilita visibilidad de controles si es jur�dica
    /// </summary>
    /// <param name="p"></param>
    private void ViewControlJuridica(bool p)
    {
        PnlNatural.Visible = p;
        cuFechaExp.Visible = p;
        lblFechaExp.Visible = p;
        lblRazonSocial.Visible = !p;
        txtRazonSocial.Visible = !p;
        pnlJuridica.Visible = !p;
    }


    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Detalle Validar Tercero", SolutionPage.Detail.ToString());
            String vIdTercero = Convert.ToString(GetSessionParameter("Proveedor.IdTercero"));
            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTercero(Int32.Parse(vIdTercero));
            //Deshabilita los campos del tercero
            int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
            //int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;

            //if (vTercero.IdEstadoTercero == idEstadoPorValidar || vTercero.IdEstadoTercero == idEstadoRegistrado || vTercero.IdEstadoTercero == idEstadoPorAjustar ||  
            //    (idEstadoEnValidacion==vTercero.IdEstadoTercero && vTercero.UsuarioModifica.ToUpper() == GetSessionUser().NombreUsuario.ToUpper()))
            int muestrabtnGuardar = 0;
            if (vTercero.IdEstadoTercero == idEstadoPorValidar || vTercero.IdEstadoTercero == idEstadoRegistrado || vTercero.IdEstadoTercero == idEstadoEnValidacion)
            {
                //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);                
                //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
                toolBar.SetSaveConfirmation("return confirm('Esta seguro de guardar esta informaci\u00f3n?')");
                
                muestrabtnGuardar = 1;
            }
            SetSessionParameter("muestrabtnGuardar", muestrabtnGuardar);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlSexo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlSexo.Items.Insert(0, new ListItem("MASCULINO", "M"));
            ddlSexo.Items.Insert(0, new ListItem("FEMENINO", "F"));
            ddlSexo.SelectedValue = "-1";
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstadoTercero, "-1", true);
            ManejoControles.LlenarTipoDocumento(ddlTipoDoc, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga grilla respecto a Adjuntos
    /// </summary>
    /// <param name="idTercero"></param>
    private void cargarGrillaImagenes(int idTercero)
    {
        try
        {
            List<DocAdjuntoTercero> lista = vProveedorService.ConsultarDocAdjuntoTercero(idTercero, "005", this.PageName);
            if (lista.Count > 0)
            {
                gvAdjuntos.DataSource = lista;
                gvAdjuntos.DataBind();
            }
            else
            {
                List<DocAdjuntoTercero> listaDefecto = vProveedorService.ConsultarDocAdjuntoTercero(null, "005", this.PageName);
                gvAdjuntos.DataSource = listaDefecto;
                gvAdjuntos.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    /// <summary>
    /// Comando de rengl�n para una grilla Adjuntos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewLogo":

                    SIAService vRUBOService = new SIAService();

                    MemoryStream ArchivoImagen = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (ArchivoImagen != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", ArchivoImagen.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se logro obtener la imagen desde el servidor.");
                        return;
                    }

                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
    protected void rblConfirma_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!rblConfirma.SelectedValue.ToString().Equals(""))
        {
            if (Convert.ToBoolean(rblConfirma.SelectedValue))
            {
                lblObservaciones.Visible = false;
                txtObservaciones.Text = string.Empty;
                rfvtxtObservaciones.Enabled = false;
                //pnlConfirma.Enabled = false;
                txtObservaciones.Enabled = false;
                btnEnviarComunicado.Visible = false;
                notaComunicado.Visible = false;
                lblTipoIncidente.Visible = false;
                rdTipoIncidente1.Visible = false;
                rdTipoIncidente2.Visible = false;
            }
            else
            {
                lblObservaciones.Visible = true;
                rfvtxtObservaciones.Enabled = true;
                //pnlConfirma.Enabled = true;
                txtObservaciones.Enabled = true;
                lblTipoIncidente.Visible = true;
                rdTipoIncidente1.Visible = true;
                rdTipoIncidente2.Visible = true;

                int muestrabtnGuardar = Convert.ToInt32(GetSessionParameter("muestrabtnGuardar"));
                if (muestrabtnGuardar == 1)
                {
                    btnEnviarComunicado.Visible = true;
                    btnEnviarComunicado.Enabled = false;
                    notaComunicado.Visible = true;
                }
                
            }

        }
        else
        {
            rfvrblConfirma.IsValid = false;
            txtObservaciones.Enabled = false;
        }
        toolBar = (masterPrincipal)this.Master;
        toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
        toolBar.SetSaveConfirmation("return confirm('Esta seguro de guardar esta informaci\u00f3n?')");
    }
    protected void btnEnviarComunicado_Click(object sender, EventArgs e)
    {
        lblEnviando.Style["display"] = "none";
        notaComunicado.Visible = false;
        try
        {
            string remitente = ConfigurationManager.AppSettings["RemitenteProveedores"];
            string destinatario = "";
            // Si es creado por interno....
            if ((bool)objValidar.CreadoPorInterno)
            {
                destinatario = vSIAServ.ConsultarUsuarioPorproviderKey(objValidar.ProviderUserKey).CorreoElectronico;
            }
            else
                destinatario = txtCuentaCorreo.Text;
            string observaciones = txtObservaciones.Text;
            var vSeguridadService = new SeguridadService();
            string pAsunto = vSeguridadService.ConsultarParametro(27) == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametro(27).ValorParametro;
            string pNombre = "";
            if (ddlTipoPersona.SelectedValue.Equals("1"))
            {
                pNombre = txtPrimerNombre.Text + " " + txtSegundoNombre.Text + " " + txtPrimerApellido.Text + " " + txtSegundoApellido.Text;
            }
            else if (ddlTipoPersona.SelectedValue.Equals("2"))
            {
                pNombre = txtRazonSocial.Text;
            }
            EnviarMensajeBienvenida(remitente, destinatario, pAsunto, pNombre, observaciones);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }
    private void EnviarMensajeBienvenida(string pDe, string pPara, string pAsunto, string pNombre, string pobservaciones)
    {

        var myMailMessage = new MailMessage();

        myMailMessage.From = new MailAddress(pDe);
        myMailMessage.To.Add(new MailAddress(pPara));
        myMailMessage.Subject = pAsunto;

        myMailMessage.IsBodyHtml = true;


        myMailMessage.Body = "<html><body><table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'><tr><td colspan='3' " +
                            " bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td width='10%' " +
                            " bgcolor='#81BA3D'>&nbsp;</td><td><table width='100%' border='0' align='center'><tr><td width='5%'>&nbsp;</td><td  " +
                            " align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr><tr><td>&nbsp;</td><td align='center'><strong> " + pAsunto +
                            " </strong></td><td>&nbsp;</td></tr><tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'> " +
                            " &nbsp;</td></tr><tr><td>&nbsp;</td><td><strong>Apreciado(a) usuario:</strong>&nbsp;" + pNombre + "</td><td>&nbsp;</td></tr><tr><td> " +
                            " &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>Los siguientes son datos que est&aacute;n inconsistentes, en el registro de Terceros</td><td> " +
                            " &nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td><strong>Observaci&oacute;n:</strong> " + pobservaciones +
                            " </td><td>&nbsp;</td></tr><tr><td>&nbsp; " +
                            " </td><td>&nbsp;</td><td>&nbsp;</td></tr><tr> " +
                            " <td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>&nbsp;</td></tr></table></td><td width='10%' bgcolor='#81BA3D'> " +
                            " &nbsp;</td></tr><tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' align='center'  " +
                            " bgcolor='#81BA3D'>Este correo electr&oacute;nico fue enviado por un sistema autom&aacute;tico. Por favor <br/> no responder este correo.</td></tr>" +
                            "<tr><td colspan='3' align='center'  " +
                            " bgcolor='#81BA3D'>&nbsp;</td></tr></table></body></html>";


        var mySmtpClient = new SmtpClient();

        object userState = myMailMessage;

        mySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

        try
        {
            mySmtpClient.SendAsync(myMailMessage, userState);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo de comunicaci&oacute;n fue cancelado");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
        else
        {
            toolBar.MostrarMensajeGuardado("El env&iacute;o del comunicado se realiz&oacute; exitosamente.");
            hdfEnvioComunicado.Value = "1";
        }
    }

    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumeroDoc.Text = String.Empty;
            this.txtNumeroDoc.Focus();
        }
    }
    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDoc.SelectedItem.Text == "CC" || this.ddlTipoDoc.SelectedItem.Text == "NIT" || this.ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.txtNumeroDoc.MaxLength = 11;
            this.txtNumIdentificacion.MaxLength = 11;

        }
        else if (this.ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.txtNumeroDoc.MaxLength = 6;
            this.txtNumIdentificacion.MaxLength = 6;
        }
        else
        {
            this.txtNumeroDoc.MaxLength = 17;
            this.txtNumIdentificacion.MaxLength = 17;
        }
    }

}
