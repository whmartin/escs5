using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

using Icbf.SIA.Service;

/// <summary>
/// Página que despliega el detalle del registro de sucursal
/// </summary>
public partial class Page_Proveedor_Sucursal_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/Sucursal";
    ProveedorService vProveedorService = new ProveedorService();
    SIAService vRuboService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador del evento click para el botón Reportan
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("List.aspx");
    }

    /// <summary>
    /// Manejador del evento click para el botón Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Sucursal.IdSucursal", hfIdSucursal.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdSucursal = Convert.ToInt32(GetSessionParameter("Sucursal.IdSucursal"));
            RemoveSessionParameter("Sucursal.IdSucursal");

            if (GetSessionParameter("Sucursal.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Sucursal.Guardado");


            Sucursal vSucursal = new Sucursal();
            vSucursal = vProveedorService.ConsultarSucursal(vIdSucursal);
            hfIdSucursal.Value = vSucursal.IdSucursal.ToString();
            txtNombre.Text = vSucursal.Nombre;            
            txtIndicativo.Text = vSucursal.Indicativo.ToString();
            txtTelefono.Text = vSucursal.Telefono.ToString();
            txtExtension.Text = vSucursal.Extension.ToString();
            txtCelular.Text = vSucursal.Celular.ToString();
            txtCorreo.Text = vSucursal.Correo;
            txtEstado.Text = vSucursal.Estado==1?"Activo":"Inactivo";
            txtDepartamento.Text = vRuboService.ConsultarDepartamento(vSucursal.Departamento).NombreDepartamento;
            txtMunicipio.Text = vRuboService.ConsultarMunicipio(vSucursal.Municipio).NombreMunicipio;
            IcbfDireccion1.TipoZona = vSucursal.IdZona==1?"U":"R" ;
            IcbfDireccion1.Text = vSucursal.Direccion;
            txtFechaRegistro.Text = vSucursal.FechaCrea.ToString("dd/MM/yyyy"); 
            txtRegistradorPor.Text = vSucursal.UsuarioCrea;
            ObtenerAuditoria(PageName, hfIdSucursal.Value);
            if (vSucursal.Editable == 0)
            {
                toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
            }
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSucursal.UsuarioCrea, vSucursal.FechaCrea, vSucursal.UsuarioModifica, vSucursal.FechaModifica);
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdSucursal = Convert.ToInt32(hfIdSucursal.Value);

            Sucursal vSucursal = new Sucursal();
            vSucursal = vProveedorService.ConsultarSucursal(vIdSucursal);
            InformacionAudioria(vSucursal, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarSucursal(vSucursal);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Sucursal.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Datos de Sucursal", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
