<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_Sucursal_List" %>
<%@ Register src="../UserControl/ucDatosProveedor.ascx" tagname="ucDatosProveedor" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="width: 1000px"></div>
                    <asp:GridView runat="server" ID="gvSucursal" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdSucursal" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvSucursal_PageIndexChanging" 
                        OnSelectedIndexChanged="gvSucursal_SelectedIndexChanged" 
                        OnRowDataBound = "gvSucursal_RowDataBound"
                         AllowSorting="true" OnSorting="gvSucursal_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <%--<asp:BoundField HeaderText="Nombre Sucursal" DataField="Nombre" SortExpression="Nombre" />--%>
                            
                            <asp:TemplateField HeaderText="Nombre Sucursal" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Tel&eacute;fono" DataField="Telefono"  />
                            <asp:BoundField HeaderText="Departamento" DataField="Departamento"  SortExpression="Departamento" />
                            <asp:BoundField HeaderText="Municipio" DataField="Municipio" SortExpression="Municipio" />
                            <asp:BoundField HeaderText="Direcci&oacute;n" DataField="Direccion" SortExpression="Direccion" />
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblEstado" Text='<%# ((int)Eval("Estado"))==1?"Activo":"Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:BoundField HeaderText="Registrado Por" DataField="UsuarioCrea" SortExpression="UsuarioCrea"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                        No se encontraron sucursales para este proveedor
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
