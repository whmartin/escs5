<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Proveedor_Sucursal_Add" %>
<%@ Register src="../../../General/General/Control/IcbfDireccion.ascx" tagname="IcbfDireccion" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdSucursal" runat="server" />
<asp:HiddenField ID="hfIdEntidad" runat="server" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            
            validatorIndicativo();
        });

        function validatorIndicativo(source, arguments) {
            var vIndicativo = document.getElementById('<%= txtIndicativo.ClientID %>').value;
            var vTelefono = document.getElementById('<%= txtTelefono.ClientID %>').value;
            if(vTelefono.length == 0 ) {
                ValidatorEnable(document.getElementById('<%=cvTelefono.ClientID %>'), true);
                ValidatorValidate(document.getElementById('<%=cvTelefono.ClientID %>'));
            }
            else {
               ValidatorEnable(document.getElementById('<%=cvTelefono.ClientID %>'), false);
            }          
        }
    </script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Nombre Sucursal *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombre" ControlToValidate="txtNombre"
                 SetFocusOnError="true" ErrorMessage="Registre una sucursal" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
            <table border="0" width="350px">
            <tr>
               <td width="33%">Indicativo
                   
               
                 </td>
                <td width="33%">Teléfono*
                    <asp:CustomValidator ID="cvTelefono" runat="server" ErrorMessage="Campo Requerido" 
                    ForeColor = "Red" ClientValidationFunction="validatorIndicativo" ControlToValidate="txtTelefono" 
                    Display="Dynamic" ValidationGroup="btnGuardar" Enabled="false"></asp:CustomValidator>
                 </td>
                <td width="34%">Extensi&oacute;n</td>
            </tr>
            </table>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtNombre" MaxLength= "256" CssClass="SinUpper" width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td width="50%">
              <table border="0" width="350px">
                <tr><td width="33%"><asp:TextBox runat="server" ID="txtIndicativo" Width="50px"  MaxLength = "1"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftIndicativo" runat="server" TargetControlID="txtIndicativo"
                            FilterType="Numbers" ValidChars="" />
                    
                    </td>
                    <td width="33%"><asp:TextBox runat="server" ID="txtTelefono" Width="100px" MaxLength = "7" onblur="validatorIndicativo();"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftTelefono" runat="server" TargetControlID="txtTelefono"
                            FilterType="Numbers" ValidChars="" />                           
                    </td>
                    <td width="34%"><asp:TextBox runat="server" ID="txtExtension" Width="80px" MaxLength = "10"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftExtension" runat="server" TargetControlID="txtExtension"
                            FilterType="Numbers" ValidChars="" />
                    </td>
                 </tr>
              </table>
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                Celular *
                <asp:RequiredFieldValidator runat="server" ID="rfvCelular" ControlToValidate="txtCelular"
                 SetFocusOnError="true" ErrorMessage="Registre un número de celular" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                
            </td>
            <td width="50%">
                Correo Electrónico * 
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtCorreo" ControlToValidate="txtCorreo"
                 SetFocusOnError="true" ErrorMessage="Registre un correo electrónico" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCorreo"
                                    Display="Dynamic" ValidationGroup="btnGuardar" 
                ErrorMessage="Correo electrónico no tiene la estructura válida"
                ForeColor="Red" ValidationExpression="^[\w\.-]+@[\w-]+\.[\w\.-]+$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtCelular"  MaxLength = "10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftcelular" runat="server" TargetControlID="txtCelular"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td width="50%">
                <asp:TextBox runat="server" ID="txtCorreo" width="80%"  MaxLength = "50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteCorreo" runat="server" TargetControlID="txtCorreo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-@._" />
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                Estado 
            </td>
            <td width="50%">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                Si la sucursal no está vigente, usted puede cambiar el estado a inactivo
                <asp:RadioButtonList ID="rblEstado" runat="server">
                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>                    
                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>                    
                </asp:RadioButtonList>                 
            </td>
            <td width="50%">
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                Departamento *
                <asp:RequiredFieldValidator ID="rvDepartamento" runat="server" ControlToValidate="ddlDepartamento"
                    ErrorMessage="Seleccione departamento" SetFocusOnError="True" Enabled="True" InitialValue="-1"
                    ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                </asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Municipio *
                <asp:RequiredFieldValidator ID="rvMunicipio" runat="server" ControlToValidate="ddlMunicipio"
                    ErrorMessage="Seleccione municipio" SetFocusOnError="True" Enabled="True" InitialValue="-1"
                    ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:DropDownList ID="ddlDepartamento" runat="server" Width="40%" AutoPostBack="True"
                    OnSelectedIndexChanged="DdlDepartamentoSelectedIndexChanged" TabIndex="40">
                </asp:DropDownList>
            </td>
            <td width="50%">
                <asp:DropDownList ID="ddlMunicipio" runat="server" Width="40%" TabIndex="41">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan = "2">
            <uc1:IcbfDireccion ID="IcbfDireccion1" runat="server" Requerido ="true" />
            </td>           
        </tr>
        <tr class="rowA">
            <td width="50%">
                Fecha Registro
            </td>
            <td width="50%">
                Registrado Por
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtFechaRegistro"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegistradorPor"  Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
