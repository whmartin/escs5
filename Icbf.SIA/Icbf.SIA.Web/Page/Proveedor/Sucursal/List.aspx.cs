using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de sucursales
/// </summary>
public partial class Page_Proveedor_Sucursal_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/Sucursal";
    ProveedorService vProveedorService = new ProveedorService();
    SIAService vRuboService = new SIAService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdEntidad = null;
            int? vEstado = null;

            vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            
            gvSucursal.DataSource = vProveedorService.ConsultarSucursals( vIdEntidad, vEstado);
            SetSessionParameter("gvSucursal.DataSource", gvSucursal.DataSource);
            gvSucursal.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            
            gvSucursal.PageSize = PageSize();
            gvSucursal.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Datos de Sucursal", SolutionPage.List.ToString());

            //SetSessionParameter("EntidadProvOferente.IdEntidad", "1");
            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSucursal.DataKeys[rowIndex].Value.ToString();
            string vIdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
            SetSessionParameter("Sucursal.IdSucursal", strValue);
            SetSessionParameter("EntidadProvOferente.IdEntidad", vIdEntidad);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSucursal_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSucursal.SelectedRow);
    }

    protected void gvSucursal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSucursal.PageIndex = e.NewPageIndex;
        Buscar();
    }

    protected void gvSucursal_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Display the company name in italics.
            e.Row.Cells[3].Text = vRuboService.ConsultarDepartamento(((Sucursal)(e.Row.DataItem)).Departamento).NombreDepartamento;
            e.Row.Cells[4].Text = vRuboService.ConsultarMunicipio(((Sucursal)(e.Row.DataItem)).Municipio).NombreMunicipio;
        }

    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>    
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Sucursal.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Sucursal.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Ordenar Grilla (Revisar si se generaliza)

    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<Sucursal> SortList(List<Sucursal> data, bool isPageIndexChanging)
    {
        List<Sucursal> result = new List<Sucursal>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }
    protected void gvSucursal_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvSucursal.PageIndex;

        if (GetSessionParameter("gvSucursal.DataSource") != null)
        {
            if (gvSucursal.DataSource == null) { gvSucursal.DataSource = (List<Sucursal>)GetSessionParameter("gvSucursal.DataSource"); }
            gvSucursal.DataSource = SortList((List<Sucursal>)gvSucursal.DataSource, false);
            gvSucursal.DataBind();
        }
        gvSucursal.PageIndex = pageIndex;
    }

    #endregion
}
