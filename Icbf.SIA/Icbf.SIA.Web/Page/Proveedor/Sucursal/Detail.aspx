<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Proveedor_Sucursal_Detail" %>
<%@ Register src="../../../General/General/Control/IcbfDireccion.ascx" tagname="IcbfDireccion" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdSucursal" runat="server" />
<asp:HiddenField ID="hfIdEntidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Nombre Sucursal 
                 
            </td>
            <td width="50%">
            <table border="0" width="350px">
              <tr>
                <td width="33%">Indicativo</td>
                <td width="33%">Tel&eacute;fono</td>
                <td width="34%">Extensi&oacute;n</td>
               </tr>
            </table>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtNombre" Enabled = "false" width="80%"></asp:TextBox>
                 
            </td>
            <td width="50%">
            <table border="0" width="350px">
                <tr>
                <td width="33%"><asp:TextBox runat="server" ID="txtIndicativo" Width="50px" Enabled = "false"></asp:TextBox></td>
                <td width="33%"><asp:TextBox runat="server" ID="txtTelefono" Width="100px" Enabled = "false"></asp:TextBox></td>
                <td width="34%"><asp:TextBox runat="server" ID="txtExtension" Width="80px" Enabled = "false"></asp:TextBox></td>
                </tr>
             </table>   
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                Celular
            </td>
            <td>
                Correo Electr&oacute;nico
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtCelular" Enabled = "false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCorreo" Enabled = "false" width="80%"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                Estado
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtEstado" Enabled = "false"></asp:TextBox>
              
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                Departamento 
                
            </td>
            <td>
                Municipio 
                
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtDepartamento" Enabled = "false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMunicipio" Enabled = "false" width="80%"></asp:TextBox>
                 
            </td>
        </tr>
        <tr class="rowB">
            <td colspan = "2">
            <uc1:IcbfDireccion ID="IcbfDireccion1" runat="server" Enabled = "false" ReadOnly = "true"/>
            </td>           
        </tr>
         <tr class="rowA">
            <td width="50%" >
                Fecha Registro
            </td>
            <td >
                Registrado Por
            </td>
        </tr>
        <tr class="rowB">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtFechaRegistro"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegistradorPor"  Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowA">
            <td width="50%">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>