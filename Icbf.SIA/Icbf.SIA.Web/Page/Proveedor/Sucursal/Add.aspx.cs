using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición de sucusales
/// </summary>
public partial class Page_Proveedor_Sucursal_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRUBOService = new SIAService();
  
    string PageName = "Proveedor/Sucursal";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("List.aspx");
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Sucursal vSucursal = new Sucursal();

            vSucursal.Nombre = txtNombre.Text.Trim();
            if (!string.IsNullOrEmpty(txtIndicativo.Text))
            {
                vSucursal.Indicativo = Convert.ToInt32(txtIndicativo.Text);
            }

            if (!string.IsNullOrEmpty(txtTelefono.Text))
            {

                if (txtTelefono.Text.Length != 7)
                {
                    throw new Exception("Registre un número de teléfono válido");                
                }

                vSucursal.Telefono = Convert.ToInt32(txtTelefono.Text);
            }            
            if (!string.IsNullOrEmpty(txtExtension.Text))
            {
                vSucursal.Extension = Convert.ToInt64(txtExtension.Text);
            }
            if (!string.IsNullOrEmpty(txtCelular.Text))
            {
                vSucursal.Celular = Convert.ToInt64(txtCelular.Text);
            } 
            
            vSucursal.Correo = Convert.ToString(txtCorreo.Text);
            vSucursal.Estado = Convert.ToInt32(rblEstado.SelectedValue);
            if (!string.IsNullOrEmpty(IcbfDireccion1.TipoZona))
            {
                vSucursal.IdZona =  IcbfDireccion1.TipoZona.Equals("U")?1:2;    
            }
            

            if (!string.IsNullOrEmpty(ddlDepartamento.SelectedValue))
            {
                vSucursal.Departamento = Convert.ToInt32(ddlDepartamento.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlMunicipio.SelectedValue))
            {
                vSucursal.Municipio =  Convert.ToInt32(ddlMunicipio.SelectedValue);
            }
            vSucursal.Direccion = Convert.ToString(IcbfDireccion1.Text).Trim();
            vSucursal.Editable = 1;

            if (Request.QueryString["oP"] == "E")
            {
                vSucursal.IdSucursal = Convert.ToInt32(hfIdSucursal.Value);
                vSucursal.IdEntidad = Convert.ToInt32(hfIdEntidad.Value);
                vSucursal.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSucursal, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarSucursal(vSucursal);
            }
            else
            {
                vSucursal.IdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
                vSucursal.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSucursal, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarSucursal(vSucursal);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Sucursal.IdSucursal", vSucursal.IdSucursal);
                SetSessionParameter("Sucursal.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            if (ex.Message.Contains("duplicado"))
            {
                toolBar.MostrarMensajeError("Sucursal  ya existe");
            }
            else
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            IcbfDireccion1.ZonaErrorMessage = "Seleccione la zona";

            toolBar.EstablecerTitulos("Datos de Sucursal", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdSucursal = Convert.ToInt32(GetSessionParameter("Sucursal.IdSucursal"));
            RemoveSessionParameter("Sucursal.Id");
            rblEstado.Enabled = true;

            Sucursal vSucursal = new Sucursal();
            vSucursal = vProveedorService.ConsultarSucursal(vIdSucursal);
            hfIdSucursal.Value = vSucursal.IdSucursal.ToString();
            hfIdEntidad.Value = vSucursal.IdEntidad.ToString();
            txtNombre.Text = vSucursal.Nombre.Trim();            
            txtIndicativo.Text = vSucursal.Indicativo.ToString();
            txtTelefono.Text = vSucursal.Telefono.ToString();
            txtExtension.Text = vSucursal.Extension.ToString();
            txtCelular.Text = vSucursal.Celular.ToString();
            txtCorreo.Text = vSucursal.Correo;
            rblEstado.SelectedValue = vSucursal.Estado.ToString();
            ddlDepartamento.SelectedValue = vSucursal.Departamento.ToString();
            CargarMunicipioXDepartamento();
            ddlMunicipio.SelectedValue = vSucursal.Municipio.ToString();
            IcbfDireccion1.TipoZona = vSucursal.IdZona == 1 ? "U" : "R";
            IcbfDireccion1.Text = vSucursal.Direccion;
            txtFechaRegistro.Text = vSucursal.FechaCrea.ToString("dd/MM/yyyy");
            txtRegistradorPor.Text = vSucursal.UsuarioCrea;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSucursal.UsuarioCrea, vSucursal.FechaCrea, vSucursal.UsuarioModifica, vSucursal.FechaModifica);

            if (vSucursal.Editable == 0)
            {
                DeshabilitaControles();
            }

            //Solo se permite activar/desactivar para usuarios internos 
            if (vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                //es externo
            }
            else
            {
                //Si no es el mismo usuario que lo creó solo puede activar/desactivar
                if (!vSucursal.UsuarioCrea.Equals(GetSessionUser().NombreUsuario))
                {
                    DeshabilitaControles();
                }
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Deshabilita controles
    /// </summary>
    private void DeshabilitaControles()
    {
        try
        {
            txtNombre.Enabled = false;
            txtIndicativo.Enabled = false;
            txtTelefono.Enabled = false;
            txtExtension.Enabled = false;
            txtCelular.Enabled = false;
            txtCorreo.Enabled = false;
            rblEstado.Enabled = true;
            ddlDepartamento.Enabled = false;
            ddlMunicipio.Enabled = false;
            IcbfDireccion1.Enabled = false;
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            // Lisata todos los de partamento  Domicilio legal en Colombia Donde está constituida
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento,"-1", true);
            rblEstado.SelectedValue = "1";
            rblEstado.Enabled = false;
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void DdlDepartamentoSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipioXDepartamento();

    }

    /// <summary>
    /// Llena lista desplegable Municipio con valores respectivos a filtro por departamento
    /// </summary>
    protected void CargarMunicipioXDepartamento()
    {
        CargarMunicipioXDepartamento(ddlMunicipio, Convert.ToInt32(ddlDepartamento.SelectedValue));

    }

    /// <summary>
    /// Llena lista desplegable Municipio con valores respectivos a filtro por departamento
    /// </summary>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipio, int pidDepartament)
    {
        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipio,"-1", true, pidDepartament);
    }
}
