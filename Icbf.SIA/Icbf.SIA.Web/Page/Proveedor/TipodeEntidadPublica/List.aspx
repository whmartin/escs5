<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipodeentidadPublica_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Código Tipo de entidad Pública *
                </td>
                <td>
                    Descripción Tipo de entidad Pública *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtCodigoTipodeentidadPublica" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado *
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipodeentidadPublica" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdTipodeentidadPublica"
                        EmptyDataText="No existen listas parametrizadas<strong></strong>"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvTipodeentidadPublica_PageIndexChanging"
                        OnSelectedIndexChanged="gvTipodeentidadPublica_SelectedIndexChanged" AllowSorting="true"
                        OnSorting="gvTipodeentidadPublica_Sorting">
                         
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código Tipo de entidad Pública" DataField="CodigoTipodeentidadPublica"
                                SortExpression="CodigoTipodeentidadPublica" />
                            <asp:BoundField HeaderText="Descripción Tipo de entidad Pública" DataField="Descripcion"
                                SortExpression="Descripcion" />
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("Estado") ? "Activo" : "Inactivo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <div >
                                <table cellpadding="0" cellspacing="0" summary="" width="90%" align="center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Código Tipo de entidad Pública</th>
                                            <th scope="col">Descripción Tipo de entidad Pública</th>
                                            <th scope="col">Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="rowB">
                                            <td colspan="3">
                                                No existen listas parametrizadas<strong></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </EmptyDataTemplate>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
