<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Tipoentidad_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoentidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código Tipo Entidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoTipoentidad" ControlToValidate="txtCodigoTipoentidad"
                 SetFocusOnError="true" ErrorMessage="Código se encuentra en blanco" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Descripción Entidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="La descripción no puede estar en blanco" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoTipoentidad" MaxLength = "10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoTipoentidad" runat="server" TargetControlID="txtCodigoTipoentidad"
                    FilterType="LowercaseLetters,UppercaseLetters,Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" MaxLength = "50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Debe seleccionar un estado" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
