using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Seguridad.Business;
using Icbf.Seguridad.Entity;
using System.Data;

/// <summary>
/// P�gina que permite consultar los diferentes c�digos UNSPSC a trav�s de filtros
/// </summary>
public partial class Page_TipoCodigoUNSPSC_ConsultaUNSPSC : GeneralWeb
{
    Poveedor_VentanaPopup toolBar;
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CargarDatosIniciales();
            //Buscar();
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigo = "";
            String vDescripcion = null;
            Boolean? vEstado = null;
            int? vLongitudUNSPSC = null;
            String vCodigoSegmento = "";
            String vCodigoFamilia = "";
            String vCodigoClase = "";

            if (ddlSegmento.SelectedValue != "-1")
            {
                vCodigoSegmento = ddlSegmento.SelectedValue;
            }
            if (ddlFamilia.SelectedValue != "-1")
            {
                vCodigoFamilia = ddlFamilia.SelectedValue;
            }
            if (ddlClase.SelectedValue != "-1")
            {
                vCodigoClase = ddlClase.SelectedValue;
            }
            if (ddlProducto.SelectedValue != "-1")
            {
                vCodigo = ddlProducto.SelectedValue;
            }

            vEstado = true;
            //if (GetSessionParameter("Proveedor.LogitudUNSPSC") != null)
            //{
            //    vLongitudUNSPSC = int.Parse(GetSessionParameter("Proveedor.LogitudUNSPSC").ToString());
            //}

            gvTipoCodigoUNSPSC.DataSource = vProveedorService.ConsultarTipoCodigoUNSPSCs(vCodigo, vDescripcion, vEstado, vLongitudUNSPSC, vCodigoSegmento, vCodigoFamilia, vCodigoClase);
            gvTipoCodigoUNSPSC.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (Poveedor_VentanaPopup)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate_Poveedor_VentanaPopup(btnBuscar_Click);

            gvTipoCodigoUNSPSC.PageSize = PageSize();
            gvTipoCodigoUNSPSC.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Producto o servicio UNSPSC", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla de codigos UNSPSC
    /// </summary>
    /// <param name="pRow"></param>
    /// 
   
    protected void gvTipoCodigoUNSPSC_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoCodigoUNSPSC.SelectedRow);
    }


    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues = "";
            returnValues += HttpUtility.HtmlDecode(gvTipoCodigoUNSPSC.DataKeys[gvTipoCodigoUNSPSC.SelectedRow.RowIndex].Value.ToString()) + ";";

            for (int c = 1;c < gvTipoCodigoUNSPSC.Columns.Count; c++)
            {
                if (gvTipoCodigoUNSPSC.Rows[gvTipoCodigoUNSPSC.SelectedIndex].Cells[c].Text != "&nbsp;")
                    returnValues += HttpUtility.HtmlDecode(gvTipoCodigoUNSPSC.Rows[gvTipoCodigoUNSPSC.SelectedIndex].Cells[c].Text) + ";";
                else
                    returnValues += ";";
            }
            List<string> parametros = new List<string>();
            parametros = returnValues.Split(';').ToList();
            GetScriptCloseDialogCallback(string.Empty, parametros);
            if (GetSessionParameter("Proveedor.dtUNSPSC") != null)
            {
                DataTable dtTemp = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");

                int idUNSPSC = int.Parse(gvTipoCodigoUNSPSC.DataKeys[gvTipoCodigoUNSPSC.SelectedRow.RowIndex].Value.ToString());
                if (!dtTemp.Rows.Contains(idUNSPSC))
                {
                    DataRow drTemp = dtTemp.NewRow();
                    drTemp["idTipoCodUNSPSC"] = idUNSPSC;
                    drTemp["codigo"] = HttpUtility.HtmlDecode(gvTipoCodigoUNSPSC.Rows[gvTipoCodigoUNSPSC.SelectedIndex].Cells[1].Text);
                    drTemp["descripcion"] = HttpUtility.HtmlDecode(gvTipoCodigoUNSPSC.Rows[gvTipoCodigoUNSPSC.SelectedIndex].Cells[2].Text);
                    drTemp["existe"] = false;
                    dtTemp.Rows.Add(drTemp);
                    SetSessionParameter("Proveedor.dtUNSPSC", dtTemp);
                }
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoCodigoUNSPSC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoCodigoUNSPSC.PageIndex = e.NewPageIndex;
        Buscar();
    }


    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarSegmento();
            CargarFamilia("");
            CargarClase("");
            CargarProducto("");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Metodo que Carga ddlSegmento
    /// </summary>
    private void CargarSegmento()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarSegmento(ddlSegmento, "-1", true);
    }
    /// <summary>
    /// M�todo que Carga ddlFamilia
    /// </summary>
    /// <param name="vCodigoSegmento">Codigo del Segmento</param>
    private void CargarFamilia(String vCodigoSegmento)
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarFamilia(ddlFamilia, "-1", true, vCodigoSegmento);
    }
    /// <summary>
    /// M�todo que Carga ddlClase
    /// </summary>
    /// <param name="vCodigoFamilia">C�digo de la Familia</param>
    private void CargarClase(String vCodigoFamilia)
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarClase(ddlClase, "-1", true, vCodigoFamilia);
    }
    private void CargarProducto(String vCodigoClase)
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarProducto(ddlProducto, "-1", true, vCodigoClase);
    }
    protected void ddlSegmento_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarFamilia(ddlSegmento.SelectedValue);
        CargarClase("");
        CargarProducto("");
    }
    protected void ddlFamilia_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarClase(ddlFamilia.SelectedValue);
        CargarProducto("");
    }
    protected void ddlClase_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarProducto(ddlClase.SelectedValue);
    }
    protected void ddlProducto_SelectedIndexChanged(object sender, EventArgs e)
    {
        int vIdProducto = 0;
        vIdProducto = Convert.ToInt32(ddlProducto.SelectedValue);
        if (vIdProducto != -1)
        {
            Buscar();
        }
    }
}
