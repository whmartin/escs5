using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de informaci�n del m�dulo de experiencia
/// </summary>
public partial class Page_InfoExperienciaEntidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/InfoExperienciaEntidad";
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoExperienciaEntidad.NroRevision").ToString()))
        {
            NavigateTo("../ValidarProveedor/Revision.aspx");
        }
    }


    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvInfoExperienciaEntidad, GridViewSortExpression, true);
            int? vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());

            if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoFinancieraEntidad.NroRevision").ToString()))
            {
                pnlHistorial.Visible = true;
                gvExperiencia.DataSource = vProveedorService.ConsultarValidarInfoExperienciaEntidadsResumen(vIdEntidad.Value, null, null);
                gvExperiencia.DataBind();
            }
            else
            {
                gvExperiencia.DataSource = null;
                gvExperiencia.DataBind();
                pnlHistorial.Visible = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            if (string.IsNullOrEmpty(GetSessionParameter("ValidarInfoExperienciaEntidad.NroRevision").ToString()))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoRetornar -= new ToolBarDelegate(btnRetornar_Click);
            }
            else
            {
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }
            

            gvInfoExperienciaEntidad.PageSize = PageSize();
            gvInfoExperienciaEntidad.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Experiencia de la Entidad", SolutionPage.List.ToString());
            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvInfoExperienciaEntidad.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("InfoExperienciaEntidad.IdExpEntidad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvInfoExperienciaEntidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvInfoExperienciaEntidad.SelectedRow);
    }

    protected void gvInfoExperienciaEntidad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInfoExperienciaEntidad.PageIndex = e.NewPageIndex;
        //Buscar();
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("InfoExperienciaEntidad.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("InfoExperienciaEntidad.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Comando de rengl�n para la grilla InfoExperienciaEntidad
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvInfoExperienciaEntidad_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewArchivo":
                    SetSessionParameter("InfoExperienciaEntidad.IdExpEntidad", e.CommandArgument);
                    ManejoControles vManejoControles = new ManejoControles();
                    vManejoControles.OpenWindowDescargaExperiencia(this, "../../../Page/Proveedor/InfoExperienciaEntidad/ConsultaDocumentos.aspx");

                    break;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvInfoExperienciaEntidad_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el c�digo de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {

            var myGridResults = vProveedorService.ConsultarInfoExperienciaEntidads(Int32.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString()));
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del c�digo de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(InfoExperienciaEntidad), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<InfoExperienciaEntidad, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
