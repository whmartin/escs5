﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/VentanaPopup.master"
    EnableEventValidation="true" AutoEventWireup="true" CodeFile="ConsultaDocumentos.aspx.cs"
    Inherits="Page_Proveedor_InfoExperienciaEntidad_ConsultaDocumentos" %>

<%@ Register Src="../UserControl/ucDocumentosExperiencia.ascx" TagName="docs" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upDocumento" runat="server">
        <ContentTemplate>
            <uc2:docs runat="server" ID="ucDocumentos" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
