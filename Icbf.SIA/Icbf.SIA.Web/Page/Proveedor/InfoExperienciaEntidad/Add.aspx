<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_InfoExperienciaEntidad_Add" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<%@ Register Src="../../../General/General/Control/fechaJS.ascx" TagName="fechaJS" TagPrefix="uc3" %>
<%@ Register Src="../UserControl/ucDocumentoAdjunto.ascx" TagName="adjunto" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdExpEntidad" runat="server" />
    <script language="javascript" type="text/javascript">
        function GetCodUNSPSC() {
            var prevReturnValue = window.returnValue;
            window.returnValue = undefined;

            var retLupa = window.showModalDialog('../../Proveedor/InfoExperienciaEntidad/ConsultaUNSPSC.aspx', null, 'dialogWidth:800px;dialogHeight:600px;resizable:yes;');
            if (retLupa == undefined) {
                retLupa = window.returnValue;
            }
            if (retLupa != null) {
                document.getElementById('<%= hfIdTipoCodUNSPSC.ClientID %>').value = retLupa[0];

                var objLimpiar = document.getElementById('linkLimpiar');
                objLimpiar.style.visibility = 'visible';

                DoPostBack();
            }
        }

        function DoPostBack() {
            __doPostBack('<%= gvCodigoUNSPSC.ClientID %>', '');
        }

        function LimpiarUNSPSC() {
            var codigo = document.getElementById('<%= hfIdTipoCodUNSPSC.ClientID %>');

            if (confirm('¿Está seguro de eliminar esta clasificación?')) {
                descripcion.value = "";
                codigo.value = "";

                var objLimpiar = document.getElementById('linkLimpiar');
                objLimpiar.style.visibility = 'hidden';
            }
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
        function ValidaFechaInicio(source, arguments) {
            var fechaActual = new Date();
            var fechaFormato = (fechaActual.getMonth() + 1) + "/" + fechaActual.getDate() + "/" + fechaActual.getFullYear()
            var fechacontrol = document.getElementById('cphCont_cuwFechaInicio_txtFecha').value;
            var cadenafecha = fechacontrol.split("/");
            var newfecha = cadenafecha[1] + "/" + cadenafecha[0] + "/" + cadenafecha[2];
            var fechaactualcero = new Date(fechaFormato);
            var fechacompara = new Date(newfecha);
            if (fechacompara <= fechaactualcero) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
        function ValidaFechaFinal(source, arguments) {
            var fechaActual = new Date();
            var fechaFormato = (fechaActual.getMonth() + 1) + "/" + fechaActual.getDate() + "/" + fechaActual.getFullYear()
            var fechacontrol = document.getElementById('cphCont_cuwFechaFin_txtFecha').value;
            var cadenafecha = fechacontrol.split("/");
            var newfecha = cadenafecha[1] + "/" + cadenafecha[0] + "/" + cadenafecha[2];
            var fechaactualcero = new Date(fechaFormato);
            var fechacompara = new Date(newfecha);

            if (fechacompara <= fechaactualcero) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
        function ValidaObjetoContrato(source, arguments) {
            var objetoContrato = document.getElementById('<%= txtObjetoContrato.ClientID %>').value;
            objetoContrato = objetoContrato.trim();
            var palabrasObjeto = objetoContrato.split(' ');
            if (palabrasObjeto.length == 3) {
                for (var i = 0; i < palabrasObjeto.length - 1; i++) {
                    var compara = palabrasObjeto[i];

                    for (var j = 0; j < palabrasObjeto.length - 1; j++) {
                        if (i != j) {
                            var palabracompara = palabrasObjeto[j];
                            if (compara == palabracompara) {
                                arguments.IsValid = false;
                                break;
                            }
                            else {
                                arguments.IsVaild = true;
                            }
                        }
                    }

                }
            }
            else {
                arguments.IsVaild = true;
            }
        }

        function ValidaLongitudObjeto(source, arguments) {
            var objetoContrato = document.getElementById('<%= txtObjetoContrato.ClientID %>').value;
            var palabrasObjeto = objetoContrato.split(' ');
            if (palabrasObjeto.length < 3) {
                arguments.IsValid = false;
            }
            else
                arguments.IsVaild = true;
        }
    </script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Entidad o empresa *
                <asp:RequiredFieldValidator runat="server" ID="rfvEntidadContratante" ControlToValidate="txtEntidadContratante"
                    SetFocusOnError="true" ErrorMessage="Registre el nombre de la entidad o empresa"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Sector *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoSector" ControlToValidate="ddlIdTipoSector"
                    SetFocusOnError="true" ErrorMessage="Debe seleccionar el sector" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEntidadContratante" MaxLength="256" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftEntidadContratante" runat="server" TargetControlID="txtEntidadContratante"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoSector">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicio *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaIncio" ControlToValidate="cuwFechaInicio$txtFecha"
                    SetFocusOnError="true" ErrorMessage="Falta ingresar la fecha de inicio" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Fecha Terminación *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaTerminacion" ControlToValidate="cuwFechaFin$txtFecha"
                    SetFocusOnError="true" ErrorMessage="Falta ingresar la fecha de terminación"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc3:fechaJS ID="cuwFechaInicio" runat="server" Width="80%" Enabled="True" Requerid="True" />
                <asp:CustomValidator ID="cvFechaInicio" runat="server" ErrorMessage="La Fecha debe ser menor o igual a la Fecha Actual"
                ForeColor="Red" ClientValidationFunction="ValidaFechaInicio" ControlToValidate="cuwFechaInicio$txtFecha"
                Display="Dynamic" ValidationGroup="btnGuardar"></asp:CustomValidator>
            </td>
            <td>
                <uc3:fechaJS ID="cuwFechaFin" runat="server" Requerid="true" Width="80%" Enabled="True" />
                <asp:CustomValidator ID="cuvFechaFin" runat="server" ErrorMessage="La Fecha debe ser menor o igual a la Fecha Actual"
                ForeColor="Red" ClientValidationFunction="ValidaFechaFinal" ControlToValidate="cuwFechaFin$txtFecha" 
                Display="Dynamic" ValidationGroup="btnGuardar" ></asp:CustomValidator>
                <asp:CompareValidator runat="server" ID="cvFechaFin" ErrorMessage="Fecha Terminación no puede ser menor que la fecha inicio."
                    ControlToValidate="cuwFechaFin$txtFecha" Type="Date" Operator="GreaterThanEqual" ValidationGroup="btnGuardar"
                    Display="Dynamic" ForeColor="Red" ControlToCompare="cuwFechaInicio$txtFecha"></asp:CompareValidator>

                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvObjetoContrato" ControlToValidate="txtObjetoContrato"
                    SetFocusOnError="true" ErrorMessage="Registre el Objeto del contrato" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvObjetoContratoPalabras" runat="server" ErrorMessage="Las palabras deben ser diferentes entres sí"
                ForeColor="Red" ClientValidationFunction="ValidaObjetoContrato" ControlToValidate="txtObjetoContrato" 
                Display="Dynamic" ValidationGroup="btnGuardar"></asp:CustomValidator>
                <asp:CustomValidator ID="cvObjetoContratoLongitud" runat="server" ErrorMessage="Al menos debe haber 3 palabras"
                ForeColor="Red" ClientValidationFunction="ValidaLongitudObjeto" ControlToValidate="txtObjetoContrato" 
                Display="Dynamic" ValidationGroup="btnGuardar" ></asp:CustomValidator>
            </td>
            <td>
                Clasifique el producto o servicio según código UNSPSC *
                <asp:ImageButton ID="imgIdTipoCodUNSPSC" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClientClick="GetCodUNSPSC()" Style="cursor: hand" ToolTip="Buscar" />
                <a href="javascript:LimpiarUNSPSC();" id="linkLimpiar" style="visibility: hidden">Limpiar</a>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" MaxLength="256" Width="250px" Height="60px" CssClass="TextBoxGrande"
                    TextMode="MultiLine" onKeyDown="limitText(this,256);" onKeyUp="limitText(this,256);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjetoContrato" runat="server" TargetControlID="txtObjetoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                
            </td>
            <td>
                <asp:HiddenField runat="server" ID="hfIdTipoCodUNSPSC"></asp:HiddenField>
                <asp:GridView runat="server" ID="gvCodigoUNSPSC" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" DataKeyNames="idTipoCodUNSPSC" CellPadding="0" Height="16px"
                    OnPageIndexChanging="gvCodigoUNSPSC_PageIndexChanging" OnSelectedIndexChanged="gvCodigoUNSPSC_SelectedIndexChanged"
                    OnRowCommand="gvCodigoUNSPSC_RowCommand" OnRowDeleting="gvCodigoUNSPSC_RowDeleting">
                    <Columns>
                        <asp:BoundField HeaderText="Código" DataField="Codigo" SortExpression="Codigo" />
                        <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lblDocumentos" runat="server" CommandArgument='<%#Eval("idTipoCodUNSPSC")%>' ToolTip="Eliminar"
                                    CommandName="Delete" Visible='<%# !(bool)Eval("idTipoCodUNSPSC").Equals(string.Empty) %>' OnClientClick="return confirm('Está seguro que desea eliminar esta clasificación?');">
                                    <img src="../../../Image/btn/delete.gif" alt="Eliminar" height="16px" width="16px" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Cuantía del Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvCuantia" ControlToValidate="txtCuantia"
                    SetFocusOnError="true" ErrorMessage="Registre el valor del contrato" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                 <asp:CompareValidator ID="cvFechaVigencia" runat="server" Display="Dynamic" ControlToValidate="txtCuantia"
                    ValidationGroup="btnGuardar" ErrorMessage="Registre el valor del contrato"
                    Type="Currency" Operator="GreaterThan" ForeColor="Red" ValueToCompare="0">
                 </asp:CompareValidator>
            </td>
            <td>
                Experiencia (Meses)
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtCuantia" MaxLength="18"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCuantia" runat="server" TargetControlID="txtCuantia"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtExperiencia" MaxLength="18" Enabled="False"></asp:TextBox>
                <asp:HiddenField runat="server" ID="hfExperiencia" Value="" />
            </td>
        </tr>
        <tr class="rowA">
            <td>
                ¿Actualmente el contrato se encuentra en ejecución? *
                <asp:RequiredFieldValidator runat="server" ID="rfvContratoEjecucion" ControlToValidate="rblContratoEjecucion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowB"> 
            <td>
                <asp:RadioButtonList ID="rblContratoEjecucion" runat="server" >
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Adjuntar Documentos
            </td>
            <td>
                <asp:HyperLink ID="hlRequisitosMinimos" runat="server" NavigateUrl="javascript:void(0);"
                    Text="Ver requisitos mínimos sobre la certificación exigida"> </asp:HyperLink>
                <Ajax:BalloonPopupExtender ID="bpeRequisitos" runat="server" TargetControlID="hlRequisitosMinimos"
                    BalloonPopupControlID="pnlBalloon" Position="Auto" BalloonStyle="Rectangle" BalloonSize="Medium"
                    UseShadow="true" ScrollBars="Auto" DisplayOnMouseOver="false" DisplayOnFocus="false"
                    DisplayOnClick="true" />
                <asp:Panel runat="server" ID="pnlBalloon">
                </asp:Panel>
            </td>
        </tr>
        
        <asp:Panel ID="panelDocumentos" runat="server" Enabled="True">
            <tr class="rowA">
                <td  colspan="2">
                    <asp:Label ID="lblNota" Text="Nota: para eliminar el documento debe remplazarlo por el soporte legal"
                        runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr class="rowBG">
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocExperienciaProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto,IdTipoDocumento,MaxPermitidoKB,ExtensionesPermitidas"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocExperienciaProv_RowCommand"
                        OnRowEditing="gvDocExperienciaProv_RowEditing" OnRowCancelingEdit="gvDocExperienciaProv_RowCancelingEdit"
                        OnRowUpdating="gvDocExperienciaProv_RowUpdating" OnRowDeleting="gvDocExperienciaProv_RowDeleting"
                        OnPageIndexChanging="gvDocExperienciaProv_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre Documento" SortExpression="NombreDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link Documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>' style="width:150px;"></asp:Label>--%>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorExperiencia_") + "ProveedorExperiencia_".Length) %>' style="width:150px;"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--<asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'
                                        Visible="false"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorExperiencia_") + "ProveedorExperiencia_".Length) %>'
                                        Visible="false"></asp:TextBox>
                                    <asp:FileUpload ID="fuDocumento" runat="server"></asp:FileUpload>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Descripci&oacute;n">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                        TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtObservaciones" runat="server" Width="150px" Height="60px" CssClass="TextBoxGrande"
                                        TextMode="MultiLine" onKeyDown="limitText(this,100);" onKeyUp="limitText(this,100);"
                                        Text='<%# Bind("Descripcion") %>' MaxLength="100"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEditar" ImageUrl="~/Image/btn/attach.jpg"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Edit" ToolTip="Editar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgEliminar" ImageUrl="~/Image/btn/delete.gif"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Delete" ToolTip="Eliminar"
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty)  && !Page.ClientQueryString.Contains("oP=E") %>' OnClientClick="return confirm('Está seguro que desea eliminar el documento?');">
                                    </asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar">
                                    </asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </asp:Panel>

        
          <asp:Panel ID="pnlEstado" runat="server" Visible="false">
            <tr class="rowA">
                <td>
                    Estado Validación documental
                </td>
                <td>
                    
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:HiddenField ID="hfIDEstadoValidacionDocumental" runat="server"></asp:HiddenField>
                    <asp:TextBox runat="server" ID="txtEstadoValidacionDocumental" Enabled="false"></asp:TextBox>
                </td>
                <td>
                    
                </td>
            </tr>
        </asp:Panel>
            <tr class="rowA">
                <td>
                    Registrado Por
                </td>
                <td>
                    Fecha Registro
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:TextBox runat="server" ID="txtRegistradorPor" Enabled="false" Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaRegistro" Enabled="false"></asp:TextBox>
                </td>
            </tr>
    </table>
    <script type="text/javascript" language="javascript">
        //La forma en como se calculaba la experiencia en meses de acuerdo a los requerimientos del ICBF, era incorrecta 
        // JV 26/NOV/2014 00:14 AM.
        //        function EjecutarJS() {
        //            var fechaInicio = document.getElementById('cphCont_cuwFechaInicio_txtFecha').value;

        //            //Get 1 day in milliseconds
        //            var one_day = 1000 * 60 * 60 * 24;

        //            var year2 = Number(fechaInicio.substr(6, 4));
        //            var month2 = Number(fechaInicio.substr(3, 2)) - 1;
        //            var day2 = Number(fechaInicio.substr(0, 2));
        //            // Convert both dates to milliseconds
        //            var date1;
        //            var fechaFin = document.getElementById('cphCont_cuwFechaFin_txtFecha').value;
        //            var year1 = Number(fechaFin.substr(6, 4));
        //            var month1 = Number(fechaFin.substr(3, 2)) - 1;
        //            var day1 = Number(fechaFin.substr(0, 2));
        //            date1 = new Date(year1, month1, day1);

        //            var date2 = new Date(year2, month2, day2);
        //            if (date2 <= date1) {
        //                var date1_ms = date1.getTime();
        //                var date2_ms = date2.getTime();

        //                // Calculate the difference in milliseconds
        //                var difference_ms = date1_ms - date2_ms;

        //                // Convert back to days and return
        //                var meses = (Math.round(difference_ms / one_day)) / 30;
        //                var strMeses = String(meses);

        //                var mesFinal = '';
        //                var i = 0;
        //                var decimal = false;
        //                var limiteDecimales = 2;
        //                var contadorDecimales = 0;
        //                for (i = 0; i < strMeses.length; i++) {
        //                    if (!decimal) {
        //                        mesFinal += strMeses.charAt(i);
        //                        if (strMeses.charAt(i) == ',') {
        //                            decimal = true;
        //                        }
        //                    } else {
        //                        if (contadorDecimales < limiteDecimales) {
        //                            contadorDecimales++;
        //                            mesFinal += strMeses.charAt(i);
        //                        } else {
        //                            break;
        //                        }
        //                    }
        //                }
        //                document.getElementById('cphCont_txtExperiencia').value = (Math.round(mesFinal * 1000) / 1000);
        //                document.getElementById('cphCont_hfExperiencia').value = (Math.round(mesFinal * 1000) / 1000);
        //            }
        //            else {
        //                document.getElementById('cphCont_txtExperiencia').value = 0;
        //                document.getElementById('cphCont_hfExperiencia').value = 0;
        //            }
        //        }
        //Forma Correcta de calcular la diferencia en meses de fechaInicio a fechaFinDe la Experiencia.
        //JV 26/NOV/2014 00:15 AM
        function EjecutarJS() {
            var fechaInicio = document.getElementById('cphCont_cuwFechaInicio_txtFecha').value;
            var fechaFin = document.getElementById('cphCont_cuwFechaFin_txtFecha').value;
            var year1 = Number(fechaInicio.substr(6, 4));
            var month1 = Number(fechaInicio.substr(3, 2));
            var day1 = Number(fechaInicio.substr(0, 2));
            var year2 = Number(fechaFin.substr(6, 4));
            var month2 = Number(fechaFin.substr(3, 2));
            var day2 = Number(fechaFin.substr(0, 2)) + 1;

            var date1 = new Date(year1, month1, day1);
            var date2 = new Date(year2, month2, day2);

            if (date1 <= date2) {
                //var f1= new Date(year1, month1-1, day1);
                //var f2= new Date(year2, month2-1, day2);
                var DifAnios = 0;
                var DifMeses = 0;
                var DifDias = 0;
                var TotalDiasMes = 30;
                var ArrMonths = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
                //Diferencia de años
                if (year1 != year2) {
                    DifAnios = year2 - year1;
                    if (month1 > month2) {
                        DifAnios = DifAnios - 1;
                    }
                    if (month1 == month2) {
                        if (day1 > (day2 - 1))
                            DifAnios = DifAnios - 1;
                    }
                }
                //Diferencia de meses
                if (month1 != month2) {
                    var indiceEncontrado = 0;
                    for (m in ArrMonths) {
                        if (m == month1)
                            break;
                        else
                            indiceEncontrado = indiceEncontrado + 1;
                    }
                    DifMeses = 1;
                    for (var i = indiceEncontrado; i < ArrMonths.length; i++) {
                        if (ArrMonths[i] == month2)
                            break;
                        else
                            DifMeses = DifMeses + 1;
                    }

                    if (day1 > (day2 - 1))
                        DifMeses = DifMeses - 1;
                }


                //Diferencia en Días
                var ArrDias;
                if (day1 != (day2 - 1)) {
                    if (month1 == 2 && month2 == 2) {
                        ArrDias = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]
                        TotalDiasMes = 28;
                    }
                    else {
                        ArrDias = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
                        TotalDiasMes = 30;
                    }

                    var indiceDiaEncontrado = 0;
                    for (d in ArrDias) {
                        if (d == day1)
                            break;
                        else
                            indiceDiaEncontrado = indiceDiaEncontrado + 1;
                    }
                    DifDias = 1;
                    for (var j = indiceDiaEncontrado; j < ArrDias.length; j++) {
                        if (ArrDias[j] == (day2))
                            break;
                        else
                            DifDias = DifDias + 1;
                    }

                }
                else {
                    if (year1 == year2 && month1 == month2)
                        DifDias = 1;
                }
                if (DifDias == 31)
                    DifDias = 30;

                var totalDifMeses = (((DifAnios * 12) + DifMeses) + (DifDias / TotalDiasMes));
                var strMeses = String(totalDifMeses);

                var mesFinal = '';
                var i = 0;
                var decimal = false;
                var limiteDecimales = 2;
                var contadorDecimales = 0;
                for (i = 0; i < strMeses.length; i++) {
                    if (!decimal) {
                        mesFinal += strMeses.charAt(i);
                        if (strMeses.charAt(i) == ',') {
                            decimal = true;
                        }
                    } else {
                        if (contadorDecimales < limiteDecimales) {
                            contadorDecimales++;
                            mesFinal += strMeses.charAt(i);
                        } else {
                            break;
                        }
                    }
                }
                document.getElementById('cphCont_txtExperiencia').value = (Math.round(mesFinal * 1000) / 1000);
                document.getElementById('cphCont_hfExperiencia').value = (Math.round(mesFinal * 1000) / 1000);
            }
            else {
                document.getElementById('cphCont_txtExperiencia').value = 0;
                document.getElementById('cphCont_hfExperiencia').value = 0;
            }
        }
    </script>
</asp:Content>
