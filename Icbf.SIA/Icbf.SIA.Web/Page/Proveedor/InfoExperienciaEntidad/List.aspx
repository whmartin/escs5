<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_InfoExperienciaEntidad_List" %>

<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista" Width="100%">
        <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
        <asp:UpdatePanel ID="upPanel" runat="server">
            <ContentTemplate>
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <div style="width: 1000px">
                            </div>
                            <asp:GridView runat="server" ID="gvInfoExperienciaEntidad" AutoGenerateColumns="False"
                                AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdExpEntidad"
                                CellPadding="0" Height="16px" OnPageIndexChanging="gvInfoExperienciaEntidad_PageIndexChanging"
                                OnSorting="gvInfoExperienciaEntidad_Sorting" AllowSorting="True" 
                                OnSelectedIndexChanged="gvInfoExperienciaEntidad_SelectedIndexChanged" OnRowCommand="gvInfoExperienciaEntidad_RowCommand">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                Height="16px" Width="16px" ToolTip="Detalle" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Id Experiencia" DataField="IdExpEntidad" SortExpression="IdExpEntidad" Visible = "False" />
                                   <%-- <asp:BoundField HeaderText="Entidad o empresa" DataField="EntidadContratante" SortExpression="EntidadContratante" />--%>
                                   
                                   <asp:TemplateField HeaderText="Entidad o empresa" ItemStyle-HorizontalAlign="Center" SortExpression="EntidadContratante">
                                        <ItemTemplate>
                                            <div style="word-wrap: break-word; width: 300px;">
                                                <%#Eval("EntidadContratante")%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField HeaderText="Sector" DataField="TipoSector" SortExpression="TipoSector" />
                                    <asp:BoundField HeaderText="Fecha Inicio" DataField="FechaInicio" SortExpression="FechaInicio"
                                        DataFormatString="{0:d}" />
                                    <asp:BoundField HeaderText="Experiencia (meses)" DataField="ExperienciaMeses" SortExpression="ExperienciaMeses" />
                                    <asp:BoundField HeaderText="Cuant&iacute;a del  Contrato" DataField="Cuantia" SortExpression="Cuantia"
                                         DataFormatString="{0:c}" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField HeaderText="Aprobado" DataField="Aprobado" SortExpression="Aprobado" />
                                    <asp:TemplateField HeaderText="Documentos">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblDocumentos" runat="server" CommandArgument='<%#Eval("IdExpEntidad")%>'
                                                CommandName="viewArchivo">
                                                <img src="../../../Image/btn/list.png" alt="Documentos" height="16px" width="16px" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                                <EmptyDataTemplate>
                                No se han registrado experiencias para este proveedor.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlHistorial" Visible = "false">
    <table width="90%" align="center">     
        <tr>
        <td>
            <h3 class="lbBloque">
                <asp:Label ID="lblTitulo" runat="server" Text="Hist&oacute;rico de observaciones del m&oacute;dulo Experiencias"></asp:Label>
            </h3>
        </td>
        </tr>
            <tr class="rowAG">
                <td>
                   <asp:GridView ID="gvExperiencia" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px" >
                        <Columns>
                            <asp:BoundField HeaderText="No. Revisi&oacute;n" DataField="NroRevision" />
                            <asp:BoundField HeaderText="Id. Experiencia" DataField="IdExpEntidad" />
                            <asp:TemplateField HeaderText="Confirma que los datos coinciden con el documento adjunto y lo aprueba?" HeaderStyle-Width="120px" >
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (Eval("ConfirmaYAprueba") == null) ? "N/A" : ((bool)Eval("ConfirmaYAprueba") ? "Si" : "No")) %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Observaciones" DataField="Observaciones" />
                            <asp:BoundField HeaderText="Fecha de Observaci&oacute;n" DataField="FechaCrea" HeaderStyle-Width="180px" />
                        </Columns>                        
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                        No hay datos
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
     </table>    
     </asp:Panel>
</asp:Content>
