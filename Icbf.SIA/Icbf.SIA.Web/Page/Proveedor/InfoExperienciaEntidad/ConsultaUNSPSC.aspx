<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/VentanaPopup.master" AutoEventWireup="true" CodeFile="ConsultaUNSPSC.aspx.cs" Inherits="Page_TipoCodigoUNSPSC_ConsultaUNSPSC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Segmento *
                <asp:RequiredFieldValidator runat="server" ID="rfvSegmento" ControlToValidate="ddlSegmento"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvSegmento" ControlToValidate="ddlSegmento"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Familia *
                <asp:RequiredFieldValidator runat="server" ID="rfvFamilia" ControlToValidate="ddlFamilia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvFamilia" ControlToValidate="ddlFamilia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlSegmento" AutoPostBack="true"
                    onselectedindexchanged="ddlSegmento_SelectedIndexChanged" ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlFamilia"  AutoPostBack="true"
                    onselectedindexchanged="ddlFamilia_SelectedIndexChanged" ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Clase*
                <asp:RequiredFieldValidator runat="server" ID="rfvClase" ControlToValidate="ddlClase"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvClase" ControlToValidate="ddlClase"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Producto*
                <asp:RequiredFieldValidator runat="server" ID="rfvProducto" ControlToValidate="ddlProducto"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvProducto" ControlToValidate="ddlProducto"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlClase" AutoPostBack="true"
                    onselectedindexchanged="ddlClase_SelectedIndexChanged" ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlProducto" 
                    onselectedindexchanged="ddlProducto_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="width: 500px"></div>
                    <asp:GridView runat="server" ID="gvTipoCodigoUNSPSC" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoCodUNSPSC" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoCodigoUNSPSC_PageIndexChanging" OnSelectedIndexChanged="gvTipoCodigoUNSPSC_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:BoundField HeaderText="Código" DataField="Codigo" />
                            <asp:BoundField HeaderText="Nombre" DataField="Descripcion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
