<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_InfoExperienciaEntidad_Detail" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/ucDocumentosExperiencia.ascx" TagName="docs" TagPrefix="uc2" %>
<%@ Register Src="../UserControl/ucHistorialRevisiones.ascx" TagName="ucHistorialRevisiones"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdExpEntidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Entidad o empresa
            </td>
            <td>
                Sector
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEntidadContratante" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoSector" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicio
            </td>
            <td>
                Fecha Terminación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="cuwFechaIncio" runat="server" Width="80%" Enabled="false" Requerid="True" />
            </td>
            <td>
                <uc1:fecha ID="cuwFechaTerminacion" runat="server" Width="80%" Enabled="false" Requerid="True" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato
            </td>
            <td>
                Clasifique el producto o servicio seg&uacute;n c&oacute;digo UNSPSC
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" Enabled="true" MaxLength="256"
                    Width="250px" Height="60px" CssClass="TextBoxGrande" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
            </td>
            <td>
                <asp:GridView runat="server" ID="gvCodigoUNSPSC" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" DataKeyNames="idTipoCodUNSPSC" CellPadding="0"
                    Height="16px" OnPageIndexChanging="gvCodigoUNSPSC_PageIndexChanging" OnSelectedIndexChanged="gvCodigoUNSPSC_SelectedIndexChanged"
                    OnRowCommand="gvCodigoUNSPSC_RowCommand">
                    <Columns>
                        <asp:BoundField HeaderText="Código" DataField="Codigo" SortExpression="Codigo" />
                        <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Cuant&iacute;a del contrato
            </td>
            <td>
                Experiencia Meses
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCuantia" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtExperienciaMeses" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                ¿Actualmente el contrato se encuentra en ejecución? *
                <asp:RequiredFieldValidator runat="server" ID="rfvContratoEjecucion" ControlToValidate="rblContratoEjecucion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:RadioButtonList ID="rblContratoEjecucion" runat="server" Enabled="false">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Documentos Adjuntos
            </td>
        </tr>
        <tr class="rowBG">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvDocExperienciaEntidad" AutoGenerateColumns="False"
                    AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                    CellPadding="0" Height="16px" OnRowCommand="gvDocExperienciaEntidad_RowCommand"
                    OnPageIndexChanging="gvDocExperienciaEntidad_PageIndexChanging" OnSelectedIndexChanged="gvDocExperienciaEntidad_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre del Documento">
                            <ItemTemplate>
                                <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                        <asp:TemplateField HeaderText="Documento">
                            <ItemTemplate>
                                <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorExperiencia_") + "ProveedorExperiencia_".Length) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descipci&oacute;n">
                            <ItemTemplate>
                                <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                    TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                    Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                    Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
        <asp:Panel ID="pnlEstado" runat="server" Visible="false">
            <tr class="rowA">
                <td>
                    Estado Validación documental
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    <asp:HiddenField ID="hfIDEstadoValidacionDocumental" runat="server"></asp:HiddenField>
                    <asp:TextBox runat="server" ID="txtEstadoValidacionDocumental" Enabled="false"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
        </asp:Panel>
        <tr class="rowA">
            <td>
                Registrado Por
            </td>
            <td>
                Fecha Registro
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="txtRegistradorPor" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistro" Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table width="90%" align="center">
        <tr>
            <td>
                <uc3:ucHistorialRevisiones ID="ucHistorialRevisiones1" runat="server" style="width: 90%" />
            </td>
        </tr>
    </table>
</asp:Content>
