using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.IO;
using Icbf.SIA.Service;
using System.Text;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Business;
using Icbf.SIA.Entity;
using System.Data;

/// <summary>
/// Página de registro y edición de programas de la aplicación
/// </summary>
public partial class Page_InfoExperienciaEntidad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    string PageName = "Proveedor/InfoExperienciaEntidad";
    DataTable dtUNSPSC;
    SIAService vRuboService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (!Page.IsPostBack)
        {
            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
        else
        {
            if (hfExperiencia.Value != "")
                txtExperiencia.Text = hfExperiencia.Value;

            if (GetSessionParameter("Proveedor.dtUNSPSC") != null)
            {
                if (Request.QueryString["oP"] == "E")
                {
                    DataTable dtCodUNSPSC = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");
                    foreach (DataRow cod in dtCodUNSPSC.Rows)
                    {
                        if (!bool.Parse(cod["existe"].ToString()))
                        {
                            ExperienciaCodUNSPSCEntidad vExperienciaCodUNSPSCEntidad = new ExperienciaCodUNSPSCEntidad();
                            vExperienciaCodUNSPSCEntidad.IdExpEntidad = int.Parse(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad").ToString());
                            vExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC = int.Parse(cod["idTipoCodUNSPSC"].ToString());
                            vExperienciaCodUNSPSCEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                            vProveedorService.InsertarExperienciaCodUNSPSCEntidad(vExperienciaCodUNSPSCEntidad);
                            cod["existe"] = true;
                        }
                    }
                }
                gvCodigoUNSPSC.DataSource = GetSessionParameter("Proveedor.dtUNSPSC");
                gvCodigoUNSPSC.DataBind();
            }

            if (GetSessionParameter("Proveedor.Requisitos") != null)
            {
                pnlBalloon.Controls.Clear();
                Literal ltMensaje = new Literal();
                ltMensaje.Text = (String)GetSessionParameter("Proveedor.Requisitos");
                pnlBalloon.Controls.Add(ltMensaje);
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (GetSessionParameter("Proveedor.dtUNSPSC") != null)
        {
            if (((DataTable)GetSessionParameter("Proveedor.dtUNSPSC")).Rows.Count > 0)
            {
                Guardar();
            }
            else
            {
                toolBar.MostrarMensajeError("Seleccione una clasificación según su criterio para el código UNSPSC");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Seleccione una clasificación según su criterio para el código UNSPSC");
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Valida documentos obligatorios
    /// </summary>
    public void ValidarDocumentosObligatorios()
    {

        int vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
        Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente =
            vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(vIdEntidad));
        string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
        OferenteService vOferenteService = new OferenteService();
        Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
        string vTipoPersona = vTercero.IdTipoPersona.ToString();



        List<DocExperienciaEntidad> ListaDocs = null;

        if (!String.IsNullOrEmpty(hfIdExpEntidad.Value))
        {
            vIdEntidad = Convert.ToInt32(hfIdExpEntidad.Value);

            ListaDocs = vProveedorService.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(vIdEntidad, "0", vTipoPersona, vTipoSector);
        }
        else
        {
            string vIdTemporal = ViewState["IdTemporal"].ToString();
            ListaDocs = vProveedorService.ConsultarDocExperienciaEntidad_IdTemporal_Rup(vIdTemporal, "0", vTipoPersona, vTipoSector);
        }

        string mensaje = string.Empty;
        foreach (DocExperienciaEntidad doc in ListaDocs)
        {
            if (doc.Obligatorio == 1 && string.IsNullOrEmpty(doc.LinkDocumento))
            {
                mensaje += (string.Format("Falta adjuntar el documento {0}. ", doc.NombreDocumento));
            }
        }

        if (!string.IsNullOrEmpty(mensaje))
        {
            throw new Exception(mensaje);
        }
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();

            vInfoExperienciaEntidad.IdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            vInfoExperienciaEntidad.IdTipoSector = Convert.ToInt32(ddlIdTipoSector.SelectedValue);
            vInfoExperienciaEntidad.IdTipoCodUNSPSC = Convert.ToInt32(hfIdTipoCodUNSPSC.Value);
            vInfoExperienciaEntidad.EntidadContratante = Convert.ToString(txtEntidadContratante.Text);
            vInfoExperienciaEntidad.FechaInicio = Convert.ToDateTime(cuwFechaInicio.Date);
            vInfoExperienciaEntidad.FechaFin = Convert.ToDateTime(cuwFechaFin.Date);
            vInfoExperienciaEntidad.ContratoEjecucion = Convert.ToBoolean(rblContratoEjecucion.SelectedValue);

            //Ajustado por Mauricio M.
            if (hfExperiencia.Value != "")
                vInfoExperienciaEntidad.ExperienciaMeses = Decimal.Parse(hfExperiencia.Value.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString()));
            else
                vInfoExperienciaEntidad.ExperienciaMeses = Decimal.Parse(txtExperiencia.Text.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString()));

            vInfoExperienciaEntidad.ObjetoContrato = Convert.ToString(txtObjetoContrato.Text);
            vInfoExperienciaEntidad.Cuantia = Convert.ToDecimal(txtCuantia.Text);

            vInfoExperienciaEntidad.EstadoDocumental = 1; //PENDIENTE DE VALIDACIÓN
            vInfoExperienciaEntidad.Finalizado = false;


            if (Request.QueryString["oP"] == "E")
            {
                vInfoExperienciaEntidad.IdExpEntidad = Convert.ToInt32(hfIdExpEntidad.Value);
                vInfoExperienciaEntidad.UsuarioModifica = GetSessionUser().NombreUsuario;

                ValidarDocumentosObligatorios();

                InformacionAudioria(vInfoExperienciaEntidad, this.PageName, vSolutionPage);
                vInfoExperienciaEntidad.EstadoDocumental = 6;
                vResultado = vProveedorService.ModificarInfoExperienciaEntidad(vInfoExperienciaEntidad);
                int vEdoDocumental = Convert.ToInt32(GetSessionParameter("vInfoExperienciaEntidad.EstadoDocumental"));
                if (vEdoDocumental == 4)
                {
                    CambiaPorValidar(vInfoExperienciaEntidad.IdExpEntidad, vInfoExperienciaEntidad.IdEntidad);
                }
            }
            else
            {
                vInfoExperienciaEntidad.IdTemporal = ViewState["IdTemporal"].ToString();
                vInfoExperienciaEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;

                ValidarDocumentosObligatorios();

                InformacionAudioria(vInfoExperienciaEntidad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarInfoExperienciaEntidad(vInfoExperienciaEntidad);

                //Se registran los códigos UNSPSC
                if (vResultado >= 1 && GetSessionParameter("Proveedor.dtUNSPSC") != null)
                {
                    DataTable dtCodUNSPSC = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");
                    foreach (DataRow cod in dtCodUNSPSC.Rows)
                    {
                        ExperienciaCodUNSPSCEntidad vExperienciaCodUNSPSCEntidad = new ExperienciaCodUNSPSCEntidad();
                        vExperienciaCodUNSPSCEntidad.IdExpEntidad = vInfoExperienciaEntidad.IdExpEntidad;
                        vExperienciaCodUNSPSCEntidad.IdTipoCodUNSPSC = int.Parse(cod["idTipoCodUNSPSC"].ToString());
                        vExperienciaCodUNSPSCEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                        vProveedorService.InsertarExperienciaCodUNSPSCEntidad(vExperienciaCodUNSPSCEntidad);
                    }
                }
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)
            {
                SetSessionParameter("InfoExperienciaEntidad.IdExpEntidad", vInfoExperienciaEntidad.IdExpEntidad);
                SetSessionParameter("InfoExperienciaEntidad.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Experiencia de la Entidad", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdExpEntidad = Convert.ToInt32(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad"));
            int vEstadoDoc = 0;

            lblNota.Visible = true;

            InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();
            vInfoExperienciaEntidad = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);

            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
            vTipoCodigoUNSPSC = vProveedorService.ConsultarTipoCodigoUNSPSC(vInfoExperienciaEntidad.IdTipoCodUNSPSC);

            hfIdExpEntidad.Value = vInfoExperienciaEntidad.IdExpEntidad.ToString();
            ddlIdTipoSector.SelectedValue = vInfoExperienciaEntidad.IdTipoSector.ToString();
            hfIdTipoCodUNSPSC.Value = vInfoExperienciaEntidad.IdTipoCodUNSPSC.ToString();
            txtEntidadContratante.Text = vInfoExperienciaEntidad.EntidadContratante;
            cuwFechaInicio.Date = DateTime.Parse(vInfoExperienciaEntidad.FechaInicio.ToString());
            cuwFechaFin.Date = DateTime.Parse(vInfoExperienciaEntidad.FechaFin.ToString());
            txtObjetoContrato.Text = vInfoExperienciaEntidad.ObjetoContrato;
            var cuanti = Convert.ToInt64(vInfoExperienciaEntidad.Cuantia);
            txtCuantia.Text = cuanti.ToString();

            //Agregado por Mauricio M.
            string expMesesStr = System.Convert.ToString(vInfoExperienciaEntidad.ExperienciaMeses);
            if (expMesesStr.Split(',')[1].Equals("000"))
                expMesesStr = expMesesStr.Split(',')[0];
            txtExperiencia.Text = expMesesStr;

            // Se llena la grila con los códigos UNSPSC y la variable de Session
            List<ExperienciaCodUNSPSCEntidad> listaUNSPSC = vProveedorService.ConsultarExperienciaCodUNSPSCEntidads(vInfoExperienciaEntidad.IdExpEntidad);
            gvCodigoUNSPSC.DataSource = listaUNSPSC;
            gvCodigoUNSPSC.DataBind();

            dtUNSPSC = CrearEstructuraUNSPSC();
            foreach (ExperienciaCodUNSPSCEntidad cod in listaUNSPSC)
            {
                DataRow drCod = dtUNSPSC.NewRow();
                drCod["idTipoCodUNSPSC"] = cod.IdTipoCodUNSPSC;
                drCod["codigo"] = cod.Codigo;
                drCod["descripcion"] = cod.Descripcion;
                drCod["existe"] = true;
                dtUNSPSC.Rows.Add(drCod);
            }
            dtUNSPSC.AcceptChanges();


            //Se consulta el estado de validacion documental
            hfIDEstadoValidacionDocumental.Value = vInfoExperienciaEntidad.EstadoDocumental.ToString();
            txtEstadoValidacionDocumental.Text = vProveedorService.ConsultarEstadoValidacionDocumental(vInfoExperienciaEntidad.EstadoDocumental).Descripcion;
            SetSessionParameter("vInfoExperienciaEntidad.EstadoDocumental", vInfoExperienciaEntidad.EstadoDocumental);


            BuscarDocumentos();
            if (vInfoExperienciaEntidad.ContratoEjecucion != null)
                rblContratoEjecucion.SelectedValue = vInfoExperienciaEntidad.ContratoEjecucion.ToString().Trim().ToLower();

            txtRegistradorPor.Text = vInfoExperienciaEntidad.UsuarioCrea;
            txtFechaRegistro.Text = vInfoExperienciaEntidad.FechaCrea.ToString("dd/MM/yyyy");


            //Se aplica regla de negocio para permitir editar
            AplicaReglaNegocioEdicion(GetSessionUser().NombreUsuario, vInfoExperienciaEntidad.EstadoDocumental);


            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInfoExperienciaEntidad.UsuarioCrea, vInfoExperienciaEntidad.FechaCrea, vInfoExperienciaEntidad.UsuarioModifica, vInfoExperienciaEntidad.FechaModifica);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ftCuantia.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ManejoControles.LlenarTipoSectorEntidad(ddlIdTipoSector, "-1", true);

            Literal ltParemetroRequisitos = new Literal();
            ParametroBLL objParametro = new ParametroBLL();
            List<Parametro> listParametro = objParametro.ConsultarParametros("REQUISITOS MINIMOS CERTIFICACION EXIGIDA", null, null, null);
            if (listParametro.Count > 0)
            {
                ltParemetroRequisitos.Text = listParametro[0].ValorParametro;
                SetSessionParameter("Proveedor.Requisitos", ltParemetroRequisitos.Text);
                pnlBalloon.Controls.Add(ltParemetroRequisitos);
            }

            //Llenamos las grillas de los documentos
            if (ViewState["IdTemporal"] == null)
            {
                ViewState["IdTemporal"] = DateTime.Now.Ticks.ToString();
            }

            OferenteService vOferenteService = new OferenteService();
            int vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            gvDocExperienciaProv.DataSource = vProveedorService.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(0, "0", vTipoPersona, vTipoSector);
            gvDocExperienciaProv.DataBind();

            //DataTable temporal para almacenar códigos UNSPSC
            dtUNSPSC = CrearEstructuraUNSPSC();
            rblContratoEjecucion.Items.Insert(0, new ListItem("SI", "true"));
            rblContratoEjecucion.Items.Insert(0, new ListItem("NO", "false"));

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Crea estructura UNSPSC
    /// </summary>
    /// <returns></returns>
    private DataTable CrearEstructuraUNSPSC()
    {
        dtUNSPSC = new DataTable();

        DataColumn idUNSPSC = dtUNSPSC.Columns.Add("idTipoCodUNSPSC", typeof(int));
        idUNSPSC.AllowDBNull = false;

        DataColumn existe = dtUNSPSC.Columns.Add("existe", typeof(bool));
        existe.AllowDBNull = false;
        existe.ReadOnly = false;

        dtUNSPSC.Columns.Add("codigo", typeof(string));
        dtUNSPSC.Columns.Add("descripcion", typeof(string));

        dtUNSPSC.PrimaryKey = new DataColumn[] { idUNSPSC };
        dtUNSPSC.AcceptChanges();

        SetSessionParameter("Proveedor.dtUNSPSC", dtUNSPSC);
        return dtUNSPSC;
    }

    protected void gvDocExperienciaProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocExperienciaProv.PageIndex = e.NewPageIndex;

        BuscarDocumentos();
    }

    protected void gvDocExperienciaProv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow row = gvDocExperienciaProv.Rows[e.RowIndex];
            int idDocAdjunto = (int)gvDocExperienciaProv.DataKeys[e.RowIndex].Value;
            DocExperienciaEntidad documentoExperiencia = vProveedorService.ConsultarDocExperienciaEntidad(idDocAdjunto);
            vProveedorService.EliminarDocExperienciaEntidad(documentoExperiencia);
            BuscarDocumentos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocExperienciaProv_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDocExperienciaProv.EditIndex = e.NewEditIndex;
        BuscarDocumentos();
    }

    protected void gvDocExperienciaProv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDocExperienciaProv.EditIndex = -1;
        BuscarDocumentos();
    }

    protected void gvDocExperienciaProv_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow row = gvDocExperienciaProv.Rows[e.RowIndex];

            int vIdDocAdjunto = int.Parse(gvDocExperienciaProv.DataKeys[e.RowIndex]["IdDocAdjunto"].ToString());
            int vMaxPermitidoKB = int.Parse(gvDocExperienciaProv.DataKeys[e.RowIndex]["MaxPermitidoKB"].ToString());
            string vExtensionesPermitidas = gvDocExperienciaProv.DataKeys[e.RowIndex]["ExtensionesPermitidas"].ToString();
            string[] lstExtensionesPermitidas = vExtensionesPermitidas.Split(',');
            DocExperienciaEntidad documentoExperiencia = new DocExperienciaEntidad();
            if (vIdDocAdjunto > 0)
            {
                documentoExperiencia = vProveedorService.ConsultarDocExperienciaEntidad(vIdDocAdjunto);
            }
            else
            {
                if (!string.IsNullOrEmpty(hfIdExpEntidad.Value))
                {
                    int vIdExpEntidad = Convert.ToInt32(hfIdExpEntidad.Value);
                    documentoExperiencia.IdExpEntidad = vIdExpEntidad;
                }
                else
                {
                    //Se asocia un Id temporal 
                    documentoExperiencia.IdTemporal = ViewState["IdTemporal"].ToString();
                }
                documentoExperiencia.NombreDocumento = ((Label)(row.Cells[0].FindControl("lblNombreDocumento"))).Text.Trim();
                documentoExperiencia.IdTipoDocumento = int.Parse(gvDocExperienciaProv.DataKeys[e.RowIndex]["IdTipoDocumento"].ToString());
                documentoExperiencia.FechaCrea = DateTime.Now;
                documentoExperiencia.UsuarioCrea = GetSessionUser().NombreUsuario;
            }
            //doc.LinkDocumento = ((TextBox)(row.Cells[3].FindControl("txtLinkDocumento"))).Text;        
            FileUpload fuDocumento = ((FileUpload)(row.Cells[1].FindControl("fuDocumento")));

            //Validaciones
            string filename = Path.GetFileName(fuDocumento.FileName);
            string extension = filename.Substring(filename.LastIndexOf(".") + 1).ToUpper();
            bool extensionCorrecta = false;
            foreach (string vExtension in lstExtensionesPermitidas)
            {
                if (vExtension.ToUpper() == extension)
                {
                    extensionCorrecta = true;
                    break;
                }
            }
            if (!extensionCorrecta)
            {
                throw new GenericException("El formato del archivo no es permitido");
            }



            if (fuDocumento.PostedFile.ContentLength / 1024 > vMaxPermitidoKB) { throw new GenericException("El archivo excede el tamaño permitido"); }


            //Se envia por ftp
            documentoExperiencia.LinkDocumento = SubirArchivo(fuDocumento, vMaxPermitidoKB, vExtensionesPermitidas);

            documentoExperiencia.Descripcion = ((TextBox)(row.Cells[2].FindControl("txtObservaciones"))).Text;
            documentoExperiencia.FechaModifica = DateTime.Now;
            documentoExperiencia.UsuarioModifica = GetSessionUser().NombreUsuario;


            InformacionAudioria(documentoExperiencia, this.PageName, vSolutionPage);


            if (vIdDocAdjunto > 0)//Existe
            {
                vProveedorService.ModificarDocExperienciaEntidad(documentoExperiencia);
            }
            else
            {
                vProveedorService.InsertarDocExperienciaEntidad(documentoExperiencia);
            }

            gvDocExperienciaProv.EditIndex = -1;

            BuscarDocumentos();
            toolBar.LipiarMensajeError();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    /// Comando de renglón para la grilla Doc ExperienciasProv
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocExperienciaProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        //SetSessionParameter("Proveedor.Longitud", Archivo.Length.ToString());
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));


                        //ManejoControles obj = new ManejoControles();
                        //obj.OpenWindowPcUpdatePanel(this, "../../../General/General/MostrarImagenes/MostrarImagenes.aspx", 200, 200);

                        string path = "'../Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.showModalDialog(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");


                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCodigoUNSPSC_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Comando de renglón para la grilla CodigoUNSPSC
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvCodigoUNSPSC_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Delete" && GetSessionParameter("Proveedor.dtUNSPSC") != null)
            {
                dtUNSPSC = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");
                var rows = dtUNSPSC.Select("idTipoCodUNSPSC = " + e.CommandArgument);
                foreach (var row in rows)
                {
                    row.Delete();
                }
                dtUNSPSC.AcceptChanges();
                SetSessionParameter("Proveedor.dtUNSPSC", dtUNSPSC);

                if (Request.QueryString["oP"] == "E")
                {
                    List<ExperienciaCodUNSPSCEntidad> listaUNSPSC = vProveedorService.ConsultarExperienciaCodUNSPSCEntidads(int.Parse(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad").ToString()));
                    foreach (ExperienciaCodUNSPSCEntidad cod in listaUNSPSC)
                    {
                        if (cod.IdTipoCodUNSPSC == int.Parse(e.CommandArgument.ToString()))
                        {
                            vProveedorService.EliminarExperienciaCodUNSPSCEntidad(cod);
                        }
                    }
                }

                gvCodigoUNSPSC.DataSource = GetSessionParameter("Proveedor.dtUNSPSC");
                gvCodigoUNSPSC.DataBind();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCodigoUNSPSC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    { }

    protected void gvCodigoUNSPSC_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
    }

    /// <summary>
    /// Cargar archivo al servidor
    /// </summary>
    /// <param name="fuArchivo"></param>
    /// <param name="pMaxPermitidoKB"></param>
    /// <param name="pExtensionesPermitidas"></param>
    /// <returns></returns>
    protected string SubirArchivo(FileUpload fuArchivo, int pMaxPermitidoKB, string pExtensionesPermitidas)
    {

        if (!fuArchivo.HasFile)
        {
            throw new Exception("Debe seleccionar un archivo.");
        }

        int idformatoArchivo;
        string filename = Path.GetFileName(fuArchivo.FileName);
        String NombreArchivoOri = filename;

        string tipoArchivo = "";
        foreach (string sExt in pExtensionesPermitidas.Split(','))
        {
            if (filename.Substring(filename.LastIndexOf(".") + 1).ToUpper() == sExt.ToUpper())
            {
                tipoArchivo = string.Format("General.archivo{0}", sExt.ToUpper());
                break;
            }
        }

        if (tipoArchivo == "")
        {
            throw new Exception(string.Format("Solo se permiten los siguientes archivos: {0}", pExtensionesPermitidas));
        }

        if (fuArchivo.PostedFile.ContentLength / 1024 > pMaxPermitidoKB)
        {
            throw new Exception(string.Format("Solo se permiten archivos inferiores a {0} KB", (pMaxPermitidoKB)));
        }

        SIAService vRUBOService = new SIAService();
        List<FormatoArchivo> vFormatoArchivo = vRUBOService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);
        if (vFormatoArchivo.Count == 0)
        {
            throw new Exception("No se ha parametrizado la extensión del archivo a cargar");
        }
        idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

        //Creación del Nombre del archivo
        string NombreArchivo = filename.Substring(0, filename.IndexOf("."));
        NombreArchivo = vRUBOService.QuitarEspacios(NombreArchivo);
        NombreArchivo = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString()
            + "_ProveedorExperiencia_" + NombreArchivo + filename.Substring(filename.LastIndexOf("."));

        string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

        //Guardado del archivo en el servidor
        HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;
        Boolean ArchivoSubido = vRUBOService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

        if (ArchivoSubido)
        {
            return NombreArchivo;
        }
        else
        {
            throw new Exception("No se pudo subir el archivo al ftp.");
        }

        return null;
    }

    /// <summary>
    /// Crea documento
    /// </summary>
    /// <param name="file"></param>
    /// <param name="destripcion"></param>
    /// <param name="idExperiencia"></param>
    private void CrearDocumento(HttpPostedFile file, string destripcion, int idExperiencia)
    {
        int vResultado;
        try
        {
            string filename = Path.GetFileName(file.FileName);
            string extension = Path.GetExtension(filename);

            DocExperienciaEntidad vDocExperienciaEntidad = new DocExperienciaEntidad();
            vDocExperienciaEntidad.IdExpEntidad = idExperiencia;
            vDocExperienciaEntidad.Descripcion = destripcion;



            //Creación del Nombre del archivo
            string NombreArchivo = filename.Substring(0, filename.IndexOf("."));
            SIAService vRUBOService = new SIAService();
            NombreArchivo = vRUBOService.QuitarEspacios(NombreArchivo);

            NombreArchivo = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() + "_Parametro_" + NombreArchivo + extension;
            string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

            //Guardado del archivo en el servidor
            HttpPostedFile vHttpPostedFile = file;
            Boolean ArchivoSubido = vRUBOService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

            if (ArchivoSubido)
            {
                toolBar.MostrarMensajeGuardado();
                vDocExperienciaEntidad.LinkDocumento = NombreArchivo;
            }
            vDocExperienciaEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;
            InformacionAudioria(vDocExperienciaEntidad, this.PageName, vSolutionPage);
            vResultado = vProveedorService.InsertarDocExperienciaEntidad(vDocExperienciaEntidad);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Valida archivo
    /// </summary>
    /// <param name="archivo"></param>
    /// <returns></returns>
    public string ValidaArchivo(HttpPostedFile archivo)
    {
        try
        {
            string filename = Path.GetFileName(archivo.FileName);
            string extension = Path.GetExtension(filename);

            if (extension != ".jpg" && extension != ".jpeg" && extension != ".gif" && extension != ".pdf")
            {
                return filename + ": Extensión no válida, solo se permiten archivos JPG, GIF y PDF</br>";
            }
            if (archivo.ContentLength / 1024 > 4096)
            {
                return filename + ": Tamaño no válidos, solo se permiten archivos inferiores a cuatro megas 4MB</br>";
            }
            return String.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void BuscarDocumentos()
    {
        try
        {
            OferenteService vOferenteService = new OferenteService();
            if (!String.IsNullOrEmpty(hfIdExpEntidad.Value))
            {
                int vIdExpEntidad = Convert.ToInt32(hfIdExpEntidad.Value);


                InfoExperienciaEntidad vInfoExperienciaEntidad = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);
                Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vInfoExperienciaEntidad.IdEntidad);
                string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
                Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                string vTipoPersona = vTercero.IdTipoPersona.ToString();

                gvDocExperienciaProv.DataSource = vProveedorService.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(vIdExpEntidad, "0", vTipoPersona, vTipoSector);
                gvDocExperienciaProv.DataBind();
            }
            else
            {
                string vIdTemporal = ViewState["IdTemporal"].ToString();

                int vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
                Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
                string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
                Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                string vTipoPersona = vTercero.IdTipoPersona.ToString();

                gvDocExperienciaProv.DataSource = vProveedorService.ConsultarDocExperienciaEntidad_IdTemporal_Rup(vIdTemporal, "0", vTipoPersona, vTipoSector);
                gvDocExperienciaProv.DataBind();

            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Habilita visibilidad del panel que expone el estado, validando si es proveedor
    /// </summary>
    /// <param name="usuario"></param>
    /// <param name="IdEstado"></param>
    private void AplicaReglaNegocioEdicion(string usuario, int IdEstado)
    {
        //Se verifica si el usuario es interno o externo y aplica regla
        if (vRuboService.ConsultarUsuario(usuario).Rol.Split(';').Contains("PROVEEDORES"))
        {//Es Externo
            pnlEstado.Visible = false;
        }
        else
        {//Es interno
            pnlEstado.Visible = true;
        }
    }
    /// <summary>
    /// Sí el registro esta validado y después se edita, cambia el estado a Sin Validar o en Proceso (-1)
    /// </summary>
    private void CambiaPorValidar(int vIdExpEntidad, int vIdEntidad)
    {
        ValidarInfoExperienciaEntidad vValidarInfoExperienciaEntidad = new ValidarInfoExperienciaEntidad();
        //obtiene la ultima observacion disponible
        vValidarInfoExperienciaEntidad = vProveedorService.ConsultarValidarInfoExperienciaEntidad_Ultima(vIdEntidad, vIdExpEntidad);
        if (vValidarInfoExperienciaEntidad.ConfirmaYAprueba == true)
        {

            int vIdEstado = 6; //Por Validar

            vValidarInfoExperienciaEntidad.ConfirmaYAprueba = null;
            vValidarInfoExperienciaEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
            int vResultado = vProveedorService.ModificarValidarInfoExperienciaEntidad(vValidarInfoExperienciaEntidad, vIdEstado);

            if (vResultado != 1)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
        }
    }
}
