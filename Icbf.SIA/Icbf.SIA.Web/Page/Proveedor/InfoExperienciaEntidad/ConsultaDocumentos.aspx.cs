﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;

/// <summary>
/// Página que despliega los documentos asociados a los registros de experiencia del proveedor
/// </summary>
public partial class Page_Proveedor_InfoExperienciaEntidad_ConsultaDocumentos : GeneralWeb
{

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad") != null)
            {
                ucDocumentos.CargarInfo(Int32.Parse(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad").ToString()));
            }
        }
    }
}