using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;
using System.IO;

/// <summary>
/// Página que despliega el detalle del registro de información módulo experiencia
/// </summary>
public partial class Page_InfoExperienciaEntidad_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/InfoExperienciaEntidad";
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRuboService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (!Page.IsPostBack)
        {
            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("InfoExperienciaEntidad.IdExpEntidad", hfIdExpEntidad.Value);
        NavigateTo(SolutionPage.Edit);
    }


    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void gvDocExperienciaEntidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocExperienciaEntidad.SelectedRow);
    }

    protected void gvDocExperienciaEntidad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocExperienciaEntidad.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Comando de renglón para la grilla DocExperienciaEntidad
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocExperienciaEntidad_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Seleccionar registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocExperienciaEntidad.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocExperienciaEntidad.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {

            int vIdExpEntidad = Convert.ToInt32(hfIdExpEntidad.Value);
            OferenteService vOferenteService = new OferenteService();
            InfoExperienciaEntidad vInfoExperienciaEntidad = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vInfoExperienciaEntidad.IdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            gvDocExperienciaEntidad.DataSource = vProveedorService.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(vIdExpEntidad, "0", vTipoPersona, vTipoSector);
            gvDocExperienciaEntidad.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles proveniendo de una fuente de datos
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdExpEntidad = Convert.ToInt32(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad"));

            if (GetSessionParameter("InfoExperienciaEntidad.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("InfoExperienciaEntidad.Guardado");
            RemoveSessionParameter("Proveedor.dtUNSPSC");


            InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();
            vInfoExperienciaEntidad = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);

            TipoCodigoUNSPSC vTipoCodigoUNSPSC = new TipoCodigoUNSPSC();
            vTipoCodigoUNSPSC = vProveedorService.ConsultarTipoCodigoUNSPSC(vInfoExperienciaEntidad.IdTipoCodUNSPSC);


            hfIdExpEntidad.Value = vInfoExperienciaEntidad.IdExpEntidad.ToString();
            ddlIdTipoSector.SelectedValue = vInfoExperienciaEntidad.IdTipoSector.ToString();
            txtEntidadContratante.Text = vInfoExperienciaEntidad.EntidadContratante;
            cuwFechaIncio.Date = DateTime.Parse(vInfoExperienciaEntidad.FechaInicio.ToString());
            cuwFechaTerminacion.Date = DateTime.Parse(vInfoExperienciaEntidad.FechaFin.ToString());
            txtObjetoContrato.Text = vInfoExperienciaEntidad.ObjetoContrato;
            //txtCuantia.Text = vInfoExperienciaEntidad.Cuantia.ToString();
            txtCuantia.Text = string.Format("{0:C}", vInfoExperienciaEntidad.Cuantia);

            txtExperienciaMeses.Text = vInfoExperienciaEntidad.ExperienciaMeses.ToString();

            // Se llena la grila con los códigos UNSPSC
            gvCodigoUNSPSC.DataSource = vProveedorService.ConsultarExperienciaCodUNSPSCEntidads(vInfoExperienciaEntidad.IdExpEntidad);
            gvCodigoUNSPSC.DataBind();

            //Llenamos las grillas de los documentos

            OferenteService vOferenteService = new OferenteService();
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vInfoExperienciaEntidad.IdEntidad);
            string vTipoSector = vEntidadProvOferente.IdTipoSector.ToString();
            Tercero vTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            string vTipoPersona = vTercero.IdTipoPersona.ToString();

            gvDocExperienciaEntidad.DataSource = vProveedorService.ConsultarDocExperienciaEntidad_IdExpEntidad_Rup(vIdExpEntidad, "0", vTipoPersona, vTipoSector);
            gvDocExperienciaEntidad.DataBind();

            //Se consulta el estado de validacion documental
            hfIDEstadoValidacionDocumental.Value = vInfoExperienciaEntidad.EstadoDocumental.ToString();
            txtEstadoValidacionDocumental.Text = vProveedorService.ConsultarEstadoValidacionDocumental(vInfoExperienciaEntidad.EstadoDocumental).Descripcion;
            txtRegistradorPor.Text = vInfoExperienciaEntidad.UsuarioCrea;
            txtFechaRegistro.Text = vInfoExperienciaEntidad.FechaCrea.ToString("dd/MM/yyyy");

            rblContratoEjecucion.SelectedValue = vInfoExperienciaEntidad.ContratoEjecucion.ToString().Trim().ToLower();

            //Se aplica regla de negocio para permitir editar
            AplicaReglaNegocioEdicion(GetSessionUser().NombreUsuario, vInfoExperienciaEntidad.EstadoDocumental, vInfoExperienciaEntidad.ContratoEjecucion);

            //Se llena el historial de observaciones
            BuscarObservaciones();

            ObtenerAuditoria(PageName, hfIdExpEntidad.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInfoExperienciaEntidad.UsuarioCrea, vInfoExperienciaEntidad.FechaCrea, vInfoExperienciaEntidad.UsuarioModifica, vInfoExperienciaEntidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdExpEntidad = Convert.ToInt32(hfIdExpEntidad.Value);

            InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();
            vInfoExperienciaEntidad = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);
            InformacionAudioria(vInfoExperienciaEntidad, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarInfoExperienciaEntidad(vInfoExperienciaEntidad);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("InfoExperienciaEntidad.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            if (string.IsNullOrEmpty(GetSessionParameter("ValidarProveedor.NroRevision").ToString()))
            {

                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);

                int vIdExpEntidad = Convert.ToInt32(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad"));
                InfoExperienciaEntidad vInfoExperienciaEntidad = new InfoExperienciaEntidad();
                vInfoExperienciaEntidad = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);

                SIAService vRUBOService = new SIAService();
                if (vRUBOService.ConsultarUsuario(vInfoExperienciaEntidad.UsuarioCrea).Rol.Split(';').Contains("PROVEEDORES") && vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol != "PROVEEDORES")
                {
                    if (vInfoExperienciaEntidad.UsuarioCrea == GetSessionUser().NombreUsuario)
                    {
                        toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                    }
                }
                else
                {
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }




            }
            else
            {
                toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);

            }

            toolBar.EstablecerTitulos("Datos de Experiencia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/

            ManejoControles.LlenarTipoSectorEntidad(ddlIdTipoSector, "-1", true);
            rblContratoEjecucion.Items.Insert(0, new ListItem("SI", "true"));
            rblContratoEjecucion.Items.Insert(0, new ListItem("NO", "false"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCodigoUNSPSC_SelectedIndexChanged(object sender, EventArgs e)
    { }

    protected void gvCodigoUNSPSC_RowCommand(object sender, GridViewCommandEventArgs e)
    { }

    protected void gvCodigoUNSPSC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    { }

    /// <summary>
    /// Habilita visibilidad de panel estado 
    /// </summary>
    /// <param name="usuario"></param>
    /// <param name="IdEstado"></param>
    private void AplicaReglaNegocioEdicion(string usuario, int IdEstado, Boolean vContratoEjecucion)
    {
        //Se verifica si el usuario es interno o externo y aplica regla
        if (vRuboService.ConsultarUsuario(usuario).Rol.Split(';').Contains("PROVEEDORES"))
        {//Es Externo
            pnlEstado.Visible = false;
        }
        else
        {//Es interno
            pnlEstado.Visible = true;
        }

        //Si el estado es “validado” o “en validación” no permite la edición solo visualización
        if (!GetSessionUser().NombreUsuario.Equals(usuario) || IdEstado == 5)
        {

            toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar -= new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);

        }

        if (IdEstado == 4 && !vContratoEjecucion)
        {
            toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar -= new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
        }
    }

    #region Observaciones

    private void BuscarObservaciones()
    {
        ucHistorialRevisiones1.Titulo = "Histórico de Observaciones del Módulo Experiencias";
        ucHistorialRevisiones1.MensajeGuardar = "Está seguro del resultado de su revisión para esta experiencia?";
        ucHistorialRevisiones1.TituloConfirmaYAprueba = "¿El documento adjunto reúne los requisitos mínimos y aprueba los datos?";
        int vIdExpEntidad = Convert.ToInt32(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad"));
        List<ValidarInfo> lista = vProveedorService.ConsultarValidarInfoExperienciaEntidads(vIdExpEntidad).ToList<ValidarInfo>();
        ucHistorialRevisiones1.DataSource = lista;
        ucHistorialRevisiones1.ConfirmaVisible = false;

        //
        //Si está activa la opcion de ver Panel para agregar Observaciones
        //        
        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoExperienciaEntidad.NroRevision").ToString()))
        {
            //
            //Despues de cargar se cambia el estado a En Validación
            //
            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
            Int32 iIdEntidad = Convert.ToInt32(idEntidad);

            Icbf.Proveedor.Entity.InfoExperienciaEntidad info = vProveedorService.ConsultarInfoExperienciaEntidad(vIdExpEntidad);

            if (!info.Finalizado)
            {
                ucHistorialRevisiones1.ConfirmaVisible = true;
                ucHistorialRevisiones1.eventoGuardarObservaciones += new ImageClickDelegate(OnGuardarObservaciones);

                info.EstadoDocumental = 5; //EN VALIDACION vProveedorService.ConsultarEstadoValidacionDocumental().IdEstadoValidacionDocumental; //Estado En Validación
                info.UsuarioModifica = GetSessionUser().NombreUsuario;
                info.FechaModifica = DateTime.Now;
                info.Finalizado = false;

                int resultado = vProveedorService.ModificarInfoExperienciaEntidad(info);

                if (resultado >= 1)
                {
                    //Exitoso
                }
                else
                {
                    toolBar.MostrarMensajeError("No pudo cambiarse al estado En Validación");
                }

                //Se carga el control con la última información
                ValidarInfoExperienciaEntidad vValidarInfoExp = vProveedorService.ConsultarValidarInfoExperienciaEntidad_Ultima(iIdEntidad, Convert.ToInt32(hfIdExpEntidad.Value));
                if (vValidarInfoExp.NroRevision > 0)
                {
                    //revision existente
                    ucHistorialRevisiones1.Observaciones = vValidarInfoExp.Observaciones;
                    ucHistorialRevisiones1.ConfirmaYAprueba = vValidarInfoExp.ConfirmaYAprueba;
                    ucHistorialRevisiones1.NroRevision = vValidarInfoExp.NroRevision;
                }
                else
                {
                    //nueva revisión
                    ucHistorialRevisiones1.Observaciones = string.Empty;
                    ucHistorialRevisiones1.ConfirmaYAprueba = false;
                    ucHistorialRevisiones1.NroRevision = info.NroRevision;
                }

                Session["ucHistorialRevisiones1"] = ucHistorialRevisiones1;

            }
        }
    }

    private void OnGuardarObservaciones()
    {
        try
        {
            //inicializa variables
            int vResultado;
            string vObservaciones = string.Empty;
            bool bNuevo = false;
            ValidarInfoExperienciaEntidad vValidarInfoExperienciaEntidad = new ValidarInfoExperienciaEntidad();


            //toma la entidad en session
            String vIdEntidad = Convert.ToString(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            int vIdExpEntidad = Convert.ToInt32(GetSessionParameter("InfoExperienciaEntidad.IdExpEntidad"));


            //Obtiene la info del usercontrol
            ValidarInfo info = ((Page_Proveedor_UserControl_ucHistorialRevisiones)Session["ucHistorialRevisiones1"]).vValidarInfo;
            Int32 iIdEntidad = Convert.ToInt32(vIdEntidad);

            //obtiene la ultima observacion disponible
            vValidarInfoExperienciaEntidad = vProveedorService.ConsultarValidarInfoExperienciaEntidad_Ultima(iIdEntidad, vIdExpEntidad);

            //si existe en la bd la actualiza
            if (vValidarInfoExperienciaEntidad.IdExpEntidad == 0)
            {
                //No existe
                vValidarInfoExperienciaEntidad.IdExpEntidad = vIdExpEntidad;
                bNuevo = true;
            }

            if (info.Observaciones != null)
                vObservaciones = Convert.ToString(info.Observaciones).Trim();
            vValidarInfoExperienciaEntidad.Observaciones = vObservaciones;
            vValidarInfoExperienciaEntidad.ConfirmaYAprueba = Convert.ToBoolean(info.ConfirmaYAprueba);
            vValidarInfoExperienciaEntidad.NroRevision = info.NroRevision;

            //InformacionAudioria(vValidarInfoFinancieraEntidad, this.PageName, vSolutionPage);

            int idEstado = 0;
            if (vValidarInfoExperienciaEntidad.ConfirmaYAprueba == true)
                idEstado = 4;//Se obtiene el estado Validado
            else
                idEstado = 2;//Se obtiene el estado Por Ajustar

            if (bNuevo)
            {
                vValidarInfoExperienciaEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.InsertarValidarInfoExperienciaEntidad(vValidarInfoExperienciaEntidad, idEstado);
            }
            else
            {
                vValidarInfoExperienciaEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.ModificarValidarInfoExperienciaEntidad(vValidarInfoExperienciaEntidad, idEstado);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ValidarProveedor.ObservacionGuardada", "1");
                //toolBar.MostrarMensajeGuardado("Guardado exitosamente");
                //BuscarObservacionesDatosBasicos();
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    #endregion
}
