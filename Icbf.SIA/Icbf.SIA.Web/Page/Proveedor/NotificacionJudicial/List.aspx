﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_NotificacionJudicial_List" %>
<%@ Register src="../UserControl/ucDatosProveedor.ascx" tagname="ucDatosProveedor" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvNotificacionesJudiciales" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdNotJudicial" CellPadding="0" 
                        Height="16px" 
                        AllowSorting="true"
                        onpageindexchanging="gvNotificacionesJudiciales_PageIndexChanging" 
                        onselectedindexchanged="gvNotificacionesJudiciales_SelectedIndexChanged" 
                        onsorting="gvNotificacionesJudiciales_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Departamento" DataField="NombreDepartamento" SortExpression="NombreDepartamento" />
                            <asp:BoundField HeaderText="Municipio" DataField="NombreMunicipio" SortExpression="NombreMunicipio"/>
                            <asp:BoundField HeaderText="Direcci&oacute;n" DataField="Direccion" SortExpression="Direccion"/>
                            <asp:BoundField HeaderText="Registrado Por" DataField="UsuarioCrea" SortExpression="UsuarioCrea"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                        No se encuentraron datos de notificación judicial para este proveedor.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>

