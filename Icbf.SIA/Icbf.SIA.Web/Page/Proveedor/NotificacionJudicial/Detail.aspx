﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Proveedor_NotificacionJudicial_Detail" %>
<%@ Register src="../../../General/General/Control/direccion.ascx" tagname="direccion" tagprefix="uc1" %>
<%@ Register src="../../../General/General/Control/IcbfDireccion.ascx" tagname="Icbfdireccion" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
     <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell" >
                <asp:Label ID="lblDepartamento" runat="server" Text="Departamento" ></asp:Label>
            </td>
            <td class="Cell">
                <asp:Label ID="lblMunicipio" runat="server" Text="Municipio" ></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" >
                <asp:DropDownList runat="server" ID="ddlDepartamento"  AutoPostBack="True" 
                    onselectedindexchanged="ddlDepartamento_SelectedIndexChanged"  ></asp:DropDownList>
                
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlMunicipio"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
             <td class="Cell" colspan ="2" >
                <uc2:Icbfdireccion ID="Direccion" runat="server" Requerid="true"  />
             </td>

        </tr>
        <%--<tr class="rowB">
            <td class="Cell" >
                <asp:Label ID="lblEstadoDocumento" runat="server" Text="Estado Validaci&oacute;n Documental" ></asp:Label>
            </td>
            
        </tr>
        <tr class="rowA">
            <td class="Cell" >
                <asp:TextBox runat="server" ID="txtEstadoValDocumento" Enabled ="false"  ></asp:TextBox>
                
            </td>
           <td class="Cell" >
                               
            </td>
        </tr>--%>
        <tr class="rowA">
            <td >
                Registrado Por
            </td>
            <td >
                Fecha de Registro
            </td>
        </tr>
        <tr class="rowB">
            <td>
                 <asp:TextBox runat="server" ID="txtRegistradorPor"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistro"  Enabled="false"></asp:TextBox>
            </td>
        </tr>           
    </table>
    <asp:Panel runat="server" ID="PanelAdjuntos" >
     <%--<table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px" 
                        DataKeyName="IdNotJudicial" onrowcommand="gvAdjuntos_RowCommand" >
                        <Columns>
                            <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreTipoDocumento" />
                            <asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="viewLogo" ImageUrl="~/Image/btn/icoPagBuscar.gif" CommandArgument='<%#Eval("LinkDocumento")%>'
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>--%>
         <h3 class="lbBloque">
            <asp:Label ID="Label6" runat="server" Text="Documento digital/ certificados a Adjuntar"></asp:Label>
        </h3>
       <table width="80%" align="center">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblDocumentosPersonal" Text="Documentos informaci&oacute;n de personal"
                        runat="server" Visible="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocDatosBasicoProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocDatosBasicoProv_RowCommand"
                        OnPageIndexChanging="gvDocDatosBasicoProv_PageIndexChanging" OnSelectedIndexChanged="gvDocDatosBasicoProv_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                            <asp:TemplateField HeaderText="Link Documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                        TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <br />
      
    </asp:Panel>

</asp:Content>


