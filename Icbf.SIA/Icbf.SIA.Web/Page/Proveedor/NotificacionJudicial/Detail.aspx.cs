﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;
using System.IO;

/// <summary>
/// Página que despliega el detalle del registro de notificación judicial
/// </summary>
public partial class Page_Proveedor_NotificacionJudicial_Detail : GeneralWeb
{
    #region Priopiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/NotificacionJudicial";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRUBOService = new SIAService();
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        //NavigateTo(SolutionPage.Add);
        Response.Redirect("~/page/" + PageName + "/Add.aspx?oP=E");
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    

    #endregion
    #region Métodos

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.EstablecerTitulos("Editar Tercero", SolutionPage.Detail.ToString());
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.EstablecerTitulos("Datos de Notificaci&oacute;n Judicial", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int IdNotJudicial = Convert.ToInt32(GetSessionParameter("Proveedor.IdNotJudicial"));
            
            NotificacionJudicial obj = new NotificacionJudicial();
            obj = vProveedorService.ConsultarNotificacionJudiciales(IdNotJudicial, null, null, null)[0];
            ddlDepartamento.SelectedValue = obj.IdDepartamento.ToString();
            txtRegistradorPor.Text = obj.UsuarioCrea;
            txtFechaRegistro.Text = obj.FechaCrea.ToString("d") ;
            CargarMunicipio();
            ddlMunicipio.SelectedValue = obj.IdMunicipio.ToString();
            if (obj.Direccion != "")
            {
                Direccion.Text = obj.Direccion;
                RadioButtonList rbtnDetalleZonaGeo = (RadioButtonList)Direccion.FindControl("rbtnDetalleZonaGeo");
                if (obj.IdZona == 2)
                {
                    rbtnDetalleZonaGeo.SelectedValue = "R";
                }
                if (obj.IdZona == 1)
                {

                    rbtnDetalleZonaGeo.SelectedValue = "U";
                }
            }

            EntidadProvOferente objEntidad = new EntidadProvOferente();
            objEntidad = vProveedorService.ConsultarEntidadProvOferente(obj.IdEntidad);
            objEntidad.TerceroProveedor= vOferenteService.ConsultarTercero(objEntidad.IdTercero);
            BuscarDocumentos(objEntidad.IdEntidad.ToString(), objEntidad.TerceroProveedor.IdTipoPersona.ToString(), objEntidad.IdTipoSector.ToString(), objEntidad.InfoAdminEntidadProv.IdTipoEntidad.ToString());
            
            deshabilitar(false);

            cargarGrillaImagenes(IdNotJudicial);

            if (GetSessionParameter("IdNotJudicial.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("IdNotJudicial.Guardado");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Deshabilita controles
    /// </summary>
    /// <param name="p"></param>
    protected void deshabilitar(bool p) 
    {
        ddlDepartamento.Enabled = p;
        ddlMunicipio.Enabled = p;
        Direccion.Enabled = p;

    }

    /// <summary>
    /// Llena lista desplegable Municipio
    /// </summary>
    protected void CargarMunicipio()
    {
        CargarMunicipioXDepartamento(ddlMunicipio, Convert.ToInt32(ddlDepartamento.SelectedValue));
    }

    /// <summary>
    /// Llena lista desplegable Municipio
    /// </summary>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipio, int pidDepartament)
    {
        
        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipio, "-1", true, pidDepartament);
           
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        // Lisata todos los de partamento  Domicilio legal en Colombia Donde está constituida
       
        ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento, "-1", true);
    }
    private void cargarGrillaImagenes(int idNotJudicial)
    {

    }
    #endregion

    #region ObservacionesDatosBasicos


    protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;
        //BuscarDocumentos();
    }
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
   
      private void BuscarDocumentos(string pIdEntidad, string pTipoPersona, string pTipoSector, string pTipoEntidad)
    {
        try
        {

            int vIdEntidad = Convert.ToInt32(pIdEntidad);
            gvDocDatosBasicoProv.DataSource = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, pTipoPersona, pTipoSector, "", pTipoEntidad);
            gvDocDatosBasicoProv.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvDocDatosBasicoProv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocDatosBasicoProv.SelectedRow);
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocDatosBasicoProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocDatosBasicoProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
    
}