﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.IO;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición de notificaciones judiciales
/// </summary>
public partial class Page_Proveedor_NotificacionJudicial_Add : GeneralWeb
{
    #region Priopiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/NotificacionJudicial";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRUBOService = new SIAService();
    EntidadProvOferente objEntidad = new EntidadProvOferente();
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                ViewState["IdNotJudicial"] = 0;
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                }
            }

        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();

    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipio();
    }

    /// <summary>
    /// Llena la lista desplegable Municipio con el filtro departamento
    /// </summary>
    /// <param name="PddlMunicipio"></param>
    /// <param name="pidDepartament"></param>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipio, int pidDepartament)
    {
        
         ManejoControles.LlenarExperienciaMunicipio(PddlMunicipio, "-1", true, pidDepartament);
    }
   
    #endregion

    #region Metodos

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            if (Request.QueryString["oP"] == "E")
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Datos de Notificaci&oacute;n Judicial", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int IdNotJudicial = Convert.ToInt32(GetSessionParameter("Proveedor.IdNotJudicial"));
            RemoveSessionParameter("Proveedor.IdNotJudicial");
            ViewState["IdNotJudicial"] = IdNotJudicial;
            NotificacionJudicial obj = new NotificacionJudicial();
            obj = vProveedorService.ConsultarNotificacionJudiciales(IdNotJudicial, null, null, null)[0];
            ddlDepartamento.SelectedValue = obj.IdDepartamento.ToString();
            CargarMunicipio();
            ddlMunicipio.SelectedValue = obj.IdMunicipio.ToString();
            if (obj.Direccion != "")
            {
                Direccion.Text = obj.Direccion;
                RadioButtonList rbtnDetalleZonaGeo = (RadioButtonList)Direccion.FindControl("rbtnDetalleZonaGeo");
                if (obj.IdZona == 2)
                {
                    rbtnDetalleZonaGeo.SelectedValue = "R";
                }
                if (obj.IdZona == 1)
                {

                    rbtnDetalleZonaGeo.SelectedValue = "U";
                }
            }

            txtRegistradorPor.Text = obj.UsuarioCrea;
            txtFechaRegistro.Text = obj.FechaCrea.ToString("d");

            objEntidad = vProveedorService.ConsultarEntidadProvOferente(obj.IdEntidad);
            objEntidad.TerceroProveedor = vOferenteService.ConsultarTercero(objEntidad.IdTercero);
            BuscarDocumentos(objEntidad.IdEntidad.ToString(), objEntidad.TerceroProveedor.IdTipoPersona.ToString(), objEntidad.IdTipoSector.ToString(), objEntidad.InfoAdminEntidadProv.IdTipoEntidad.ToString());
            //txtEstadoValDocumento.Text = vProveedorService.ConsultarEstadoValidacionDocumental((int) objEntidad.IdEstado).Descripcion;
            deshabilitar(true);
            //BuscarDocumentos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Deshabilita controles
    /// </summary>
    /// <param name="p"></param>
    protected void deshabilitar(bool p)
    {
        ddlDepartamento.Enabled = p;
        ddlMunicipio.Enabled = p;
        Direccion.Enabled = p;
        
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            // Lisata todos los de partamento  Domicilio legal en Colombia Donde está constituida
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamento, "-1", true);
            int idEntidad =  Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));  //Pendiente por colocar la sesion del proveedor
            objEntidad = vProveedorService.ConsultarEntidadProvOferente(idEntidad);
            objEntidad.TerceroProveedor = vOferenteService.ConsultarTercero(objEntidad.IdTercero);
            

            BuscarDocumentos(objEntidad.IdEntidad.ToString(), objEntidad.TerceroProveedor.IdTipoPersona.ToString(), objEntidad.IdTipoSector.ToString(), objEntidad.InfoAdminEntidadProv.IdTipoEntidad.ToString());
            objEntidad.InfoAdminEntidadProv = new InfoAdminEntidad();
            objEntidad.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(objEntidad.IdEntidad);
            ddlDepartamento.SelectedValue = objEntidad.InfoAdminEntidadProv.IdDepartamentoDirComercial.ToString();
            CargarMunicipioXDepartamento(ddlMunicipio, Convert.ToInt32(ddlDepartamento.SelectedValue));
            ddlMunicipio.SelectedValue = objEntidad.InfoAdminEntidadProv.IdMunicipioDirComercial.ToString();
            if (!string.IsNullOrEmpty(objEntidad.InfoAdminEntidadProv.IdZona))
            {
                if (objEntidad.InfoAdminEntidadProv.IdZona == "1")
                {
                    Direccion.TipoZona = "U";
                }
                else
                {
                    Direccion.TipoZona = "R";
                }
            }
            if (!string.IsNullOrEmpty(objEntidad.InfoAdminEntidadProv.DireccionComercial))
            {
                Direccion.Text = objEntidad.InfoAdminEntidadProv.DireccionComercial;
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int idEntidad =  Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            NotificacionJudicial obj = new NotificacionJudicial();
            if (Request.QueryString["oP"] == "E")
            {
                int IdNotJudicial = int.Parse(ViewState["IdNotJudicial"].ToString());
                obj = vProveedorService.ConsultarNotificacionJudiciales(IdNotJudicial, null, null, null)[0];
                idEntidad = obj.IdEntidad;
                obj.UsuarioModifica = new GeneralWeb().GetSessionUser().NombreUsuario;
                obj.FechaModifica = DateTime.Now;
            }
            else 
            {
                obj.UsuarioCrea = new GeneralWeb().GetSessionUser().NombreUsuario;
                obj.FechaCrea = DateTime.Now;
            }
            if (ddlDepartamento.SelectedValue == "-1" || ddlDepartamento.SelectedValue == "0") throw new Exception("Seleccione un Departamento");
            if (ddlMunicipio.SelectedValue == "-1" || ddlMunicipio.SelectedValue == "0") throw new Exception("Seleccione un Municipio");

            obj.IdDepartamento = int.Parse(ddlDepartamento.SelectedValue);
            obj.IdMunicipio = int.Parse(ddlMunicipio.SelectedValue);
            obj.IdEntidad = idEntidad;
            if (Direccion.Visible && Direccion.Text != "")
            {
                obj.Direccion = Convert.ToString(Direccion.Text);
                RadioButtonList rbtnDetalleZonaGeo = (RadioButtonList)Direccion.FindControl("rbtnDetalleZonaGeo");
                if (rbtnDetalleZonaGeo.SelectedValue == "R")
                {
                    obj.IdZona = 2;
                }
                if (rbtnDetalleZonaGeo.SelectedValue == "U")
                {
                    obj.IdZona = 1;
                }
            }
            int vResultado = 0;
            if (Request.QueryString["oP"] == "E")
            {
                 vResultado = vProveedorService.ModificarNotificacionJudicial(obj); 
            }
            else 
            {
                 vResultado = vProveedorService.InsertarNotificacionJudicial(obj);
                 ViewState["IdNotJudicial"] = vResultado;
            }
                     

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else
            {
                toolBar.MostrarMensajeGuardado("La información ha sido registrada en forma exitosa");
                deshabilitar(false);
                SetSessionParameter("IdNotJudicial.Guardado", "1");
            }
            //BuscarDocumentos();
            SetSessionParameter("Proveedor.IdNotJudicial", int.Parse(ViewState["IdNotJudicial"].ToString()));
            NavigateTo(SolutionPage.Detail);

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    /// <summary>
    /// Llena lista desplegable corrrespondiente a Municipio filtrado por departamento
    /// </summary>
    protected void CargarMunicipio()
    {
        CargarMunicipioXDepartamento(ddlMunicipio, Convert.ToInt32(ddlDepartamento.SelectedValue));
    }
    #endregion

    #region ObservacionesDatosBasicos


    protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;
        //BuscarDocumentos();
    }
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void BuscarDocumentos(string pIdEntidad, string pTipoPersona, string pTipoSector, string pTipoEntidad)
    {
        try
        {

            int vIdEntidad = Convert.ToInt32(pIdEntidad);
            gvDocDatosBasicoProv.DataSource = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, pTipoPersona, pTipoSector, "", pTipoEntidad);
            gvDocDatosBasicoProv.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvDocDatosBasicoProv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocDatosBasicoProv.SelectedRow);
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocDatosBasicoProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocDatosBasicoProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion
    
   
}