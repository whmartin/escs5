﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Oferente.Entity;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// Página que despliega la consulta basada en filtros de notificación judicial
/// </summary>
public partial class Page_Proveedor_NotificacionJudicial_List : GeneralWeb
{
    #region Priopiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/NotificacionJudicial";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    bool _sorted = false;
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    
    protected void gvNotificacionesJudiciales_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvNotificacionesJudiciales.SelectedRow);
    }
    #endregion

    #region Métodos

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvNotificacionesJudiciales.PageSize = PageSize();
            gvNotificacionesJudiciales.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Notificaciones Judiciales", SolutionPage.List.ToString());
            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
            Buscar();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Proveedor.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Proveedor.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdEntidad = null;
            
            vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            List<NotificacionJudicial> lista = vProveedorService.ConsultarNotificacionJudiciales(vIdEntidad);
            if (lista.Count > 0)
            {
                gvNotificacionesJudiciales.DataSource = lista;
                SetSessionParameter("gvProveedor.DataSource", gvNotificacionesJudiciales.DataSource);
                gvNotificacionesJudiciales.DataBind();

                //Si ya hay datos se oculta
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);

            }else
            {
                gvNotificacionesJudiciales.DataSource = null;
                gvNotificacionesJudiciales.DataBind();
                //toolBar.MostrarMensajeGuardado("No se encuentran Notificaciones Judiciales");
            }
            


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvNotificacionesJudiciales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvNotificacionesJudiciales.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string IdNotJudicial = gvNotificacionesJudiciales.DataKeys[rowIndex].Values[0].ToString();
            SetSessionParameter("Proveedor.IdNotJudicial", IdNotJudicial);
            Response.Redirect("~/page/" + PageName + "/Detail.aspx");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    #region ordenarGrilla
    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<NotificacionJudicial> SortList(List<NotificacionJudicial> data, bool isPageIndexChanging)
    {
        List<NotificacionJudicial> result = new List<NotificacionJudicial>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }

 
    #endregion



    protected void gvNotificacionesJudiciales_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvNotificacionesJudiciales.PageIndex;

        if (GetSessionParameter("gvNotificacionesJudiciales.DataSource") != null)
        {
            if (gvNotificacionesJudiciales.DataSource == null) { gvNotificacionesJudiciales.DataSource = (List<Tercero>)GetSessionParameter("gvNotificacionesJudiciales.DataSource"); }
            gvNotificacionesJudiciales.DataSource = SortList((List<NotificacionJudicial>)gvNotificacionesJudiciales.DataSource, false);
            gvNotificacionesJudiciales.DataBind();
        }
        gvNotificacionesJudiciales.PageIndex = pageIndex;
    }
}