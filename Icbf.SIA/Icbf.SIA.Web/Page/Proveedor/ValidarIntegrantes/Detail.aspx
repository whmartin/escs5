<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Valida_Integrantes_Detail" %>

<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/ucHistorialRevisiones.ascx" TagName="ucHistorialRevisiones"
    TagPrefix="uc3" %>
<%@ Reference Page="~/Page/Proveedor/Integrantes/List.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
        <asp:HiddenField ID="hfIdEntidad" runat="server" />
        <table width="90%" align="center">
            <tr class="rowB">
                <td width="50%;">
                    <label style="width: 45%">
                        N&uacute;mero de integrantes</label>
                </td>
                <td width="50%;">
                    <label style="width: 70%">
                        Estado validaci&oacute;n documental</label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtNumIntegrantes" Enabled="false" Width="30%"></asp:TextBox>
                </td>
                <td>
                <asp:TextBox runat="server" ID="txtEdoValidacion" Enabled="false" Width="50%"></asp:TextBox>
                <td />
            </tr>
        </table>
        <table width="80%" align="center">
            <tr>
                <td>
                    <h3 class="lbBloque">
                        <asp:Label ID="Label6" runat="server" Text="Documentos digitales"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocDatosBasicoProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocDatosBasicoProv_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre del Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Observaciones">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                        TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <%--   <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEditar" ImageUrl="~/Image/btn/attach.jpg"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Edit" ToolTip="Editar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgEliminar" ImageUrl="~/Image/btn/delete.gif"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Delete" ToolTip="Eliminar"
                                        Visible='<%# !(bool)(Request.QueryString["oP"] == "E") %>' OnClientClick="return confirm('¿Está seguro de eliminar este documento?');">
                                    </asp:ImageButton>
                                </ItemTemplate>--%>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar">
                                    </asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr>
                <td>
                    <h3 class="lbBloque">
                        <asp:Label ID="Label1" runat="server" Text="Integrantes"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvIntegrantes" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdIntegrante" CellPadding="0" Height="16px"
                        OnSorting="gvIntegrantes_Sorting" AllowSorting="True" OnPageIndexChanging="gvIntegrantes_PageIndexChanging"
                        OnSelectedIndexChanged="gvIntegrantes_SelectedIndexChanged" OnRowCommand="gvIntegrantes_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo de persona o asociaci&oacute;n" DataField="NombreTipoPersona"
                                SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Integrante" DataField="Integrante" SortExpression="Integrante" />
                            <asp:BoundField HeaderText="Porcentaje de Participación" DataField="PorcentajeParticipacion"
                                SortExpression="PorcentajeParticipacion" />
                            <asp:BoundField HeaderText="Representante legal" DataField="RepresentanteLegal" SortExpression="RepresentanteLegal" />
                            <asp:TemplateField HeaderText="Documentos">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblDocumentos" runat="server" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        CommandName="viewArchivo" Visible='<%# verDocumento(Eval("IdTipoPersona")) %>'>
                                                <img src="../../../Image/btn/list.png" alt="Documentos" height="16px" width="16px" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlConfirma">
        <table width="90%" align="center">
            <tr>
                <td>
                    <br />
                    <br />
                    <uc3:ucHistorialRevisiones ID="ucHistorialRevisiones1" runat="server" style="width:90%" />
                </td>
            </tr>
            <%--<tr class="rowB">
                <td class="Cell" colspan="2">
                    <asp:Label ID="lblConfirma" runat="server" Text="Confirmo que los datos coinciden con los documentos adjuntos y aprueba?"></asp:Label>
                    *
                    <asp:RequiredFieldValidator runat="server" ID="rfvrblConfirma" ErrorMessage="Confirmo que los datos coinciden con los documentos adjuntos y aprueba"
                        ControlToValidate="rblConfirma" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" Enabled="True"></asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="rblConfirma" Width="150 px" AutoPostBack="True" RepeatDirection="Horizontal" >
                        <asp:ListItem Text="Seleccione" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Si" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                
            </tr>
            <tr class="rowB">
                <td class="Cell" colspan="2">
                    Observaciones
                    <asp:Label ID="lblObservaciones" runat="server" Text="*" Visible="false"></asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtObservaciones" ErrorMessage="Registre una observación válida "
                        ControlToValidate="txtObservaciones" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" colspan="2">
                    <asp:TextBox runat="server" ID="txtObservaciones" CssClass="TextBoxGrande" onKeyDown="limitText(this,200);"
                        onKeyUp="limitText(this,200);" TextMode="MultiLine" onkeypress="return soloLetrasYnumeros(event)"></asp:TextBox>
                </td>
            </tr>--%>
        </table>
    </asp:Panel>
</asp:Content>
