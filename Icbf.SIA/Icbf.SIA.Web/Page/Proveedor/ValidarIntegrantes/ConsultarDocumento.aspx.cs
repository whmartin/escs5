﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;

public partial class Page_Proveedor_Integrantes_ConsultarDocumento : GeneralWeb
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (GetSessionParameter("Integrantes.IdIntegrante") != null)
            {
                ucDocumentos.CargarInfo(Int32.Parse(GetSessionParameter("Integrantes.IdIntegrante").ToString()));
            }
        }
    }
}