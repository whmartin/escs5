﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Net.Mail;
using System.Configuration;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using System.IO;
using System.Data;
using Icbf.Seguridad.Service;

public partial class Page_Proveedor_ValidarIntegrantes_DetailIntegrante : GeneralWeb
{
    string PageName = "Proveedor/Integrantes";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            CargarDatos();
        }

    }


    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdIntegrante = Convert.ToInt32(GetSessionParameter("Integrantes.IdIntegrante"));
            RemoveSessionParameter("Integrantes.IdIntegrante");



            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ConsultarIntegrantes(vIdIntegrante);
            hfIdIntegrante.Value = vIntegrantes.IdIntegrante.ToString();
            ddlIdTipoPersona.SelectedValue = vIntegrantes.IdTipoPersona.ToString();
            CargarIdTipoIdentificacionPersonaNatural();
            ddlIdTipoIdentificacionPersonaNatural.SelectedValue = vIntegrantes.IdTipoIdentificacionPersonaNatural.ToString();
            txtNumeroIdentificacion.Text = vIntegrantes.NumeroIdentificacion;

            try
            {
                int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
                if (vTipoPersona == 1)
                {
                    BuscaDatosTercero(vIntegrantes.IdTipoIdentificacionPersonaNatural, vIntegrantes.NumeroIdentificacion);
                }
                else if (vTipoPersona == 2)
                {
                    BuscaDatosProveedor();
                }
            }
            catch (Exception ex)
            {
            }


            txtPorcentajeParticipacion.Text = vIntegrantes.PorcentajeParticipacion.ToString();
            ddlConfirmaCertificado.SelectedValue = vIntegrantes.ConfirmaCertificado;
            ddlConfirmaPersona.SelectedValue = vIntegrantes.ConfirmaPersona;
            ucFechaRegistro.Date = vIntegrantes.FechaCrea;


            SIAService vRUBOService = new SIAService();
            if (!vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                lblRegistradoPor.Visible = true;
                txtRegistradoPor.Visible = true;
                txtRegistradoPor.Text = vIntegrantes.UsuarioCrea;
            }



            ObtenerAuditoria(PageName, hfIdIntegrante.Value);


        }
        catch (UserInterfaceException ex)
        {
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            ((Label)Master.FindControl("lblTit")).Text = "Información de Integrante";
        }
        catch (Exception ex)
        {
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarIdTipoPersona();
            CargarIdTipoIdentificacionPersonaNatural();
            CargarIdTipoDocRepLegal();
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// Método de carga de los datos de IdTipoPersona
    /// </summary>
    private void CargarIdTipoPersona()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoPersonaIntegrante(ddlIdTipoPersona, "-1", true);
    }

    /// <summary>
    /// Método de carga de los datos de IdTipoIdentificacionPersonaNatural
    /// </summary>
    private void CargarIdTipoIdentificacionPersonaNatural()
    {
        int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
        ManejoControles Controles = new ManejoControles();
        if (vTipoPersona == 1)
        {
            Controles.LlenarTipoDocumentoRL(ddlIdTipoIdentificacionPersonaNatural, "-1", true);
        }
        else
        {
            Controles.LlenarTipoDocumentoN(ddlIdTipoIdentificacionPersonaNatural, "-1", true);
        }
    }
    private void CargarIdTipoDocRepLegal()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoDocumentoRL(ddlTipoDocumentoRepr, "-1", true);
    }
    private void BuscaDatosTercero(int vIdIdTipoIdentificacion, string vNumeroIdentificacion)
    {
        OcultaControles();
        try
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));

            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTerceroTipoNumeroIdentificacion(vIdIdTipoIdentificacion, vNumeroIdentificacion);

            MuestraControlesPersonaNatural();
            txtDV.Text = Convert.ToString(vTercero.DigitoVerificacion);
            txtPrimerNombre.Text = vTercero.PrimerNombre;
            txtSegundoNombre.Text = vTercero.SegundoNombre;
            txtPrimerApellido.Text = vTercero.PrimerApellido;
            txtSegundoApellido.Text = vTercero.SegundoApellido;
            cuFechaNac.Date = Convert.ToDateTime(vTercero.FechaNacimiento);
            if (vTercero.Sexo.Equals("F"))
                txtSexo.Text = "Femenino";
            else if (vTercero.Sexo.Equals("M"))
                txtSexo.Text = "Masculino";
            else
                txtSexo.Text = vTercero.Sexo;

        }
        catch (UserInterfaceException ex)
        {
        }
        catch (Exception ex)
        {
        }
    }
    private void OcultaControles()
    {
        pnlConfirma.Visible = false;
        pnlRepresentanteLegal.Visible = false;
        pnlTercero.Visible = false;
        lblDV.Visible = false;
        txtDV.Visible = false;
        lblRazon.Visible = false;
        txtRazon.Visible = false;
        lblParticipacion.Visible = false;
        txtPorcentajeParticipacion.Visible = false;
    }
    /// <summary>
    /// Muestra los controles si el tipo de Persona es Natural
    /// </summary>
    private void MuestraControlesPersonaNatural()
    {
        pnlTercero.Visible = true;
        //pnlConfirma.Visible = true;
        lblParticipacion.Visible = true;
        txtPorcentajeParticipacion.Visible = true;
    }


    /// <summary>
    /// Busca en proveedores datos básicos si el tipo de persona es juridico
    /// </summary>
    private void BuscaDatosProveedor()
    {
        OcultaControles();
        try
        {

            string vTipoPersona = ddlIdTipoPersona.SelectedValue;
            string vIdTipoIdentificacion = ddlIdTipoIdentificacionPersonaNatural.SelectedValue;
            string vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            lblDV.Visible = true;
            txtDV.Visible = true;
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProv_SectorPrivado(vTipoPersona, vIdTipoIdentificacion, vNumeroIdentificacion, null, null, null);

            MuestraControlesPersonaJuridica();
            txtRazon.Text = vEntidadProvOferente.Proveedor;
            txtDV.Text = Convert.ToString(vEntidadProvOferente.DV);

            vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
            vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);

            if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
            {
                vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);
                ddlTipoDocumentoRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
                txtIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
                txtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
                txtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                if (!string.IsNullOrEmpty(vEntidadProvOferente.RepresentanteLegal.Sexo))
                    ddlSexoRepLegal.SelectedValue = vEntidadProvOferente.RepresentanteLegal.Sexo;
                if (vEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                    cuFechaNacRepLegal.Date = DateTime.Parse(vEntidadProvOferente.RepresentanteLegal.FechaNacimiento.ToString());
            }
            int vIdEntidad = vEntidadProvOferente.IdEntidad;
            string vIdTipoSector = Convert.ToString(vEntidadProvOferente.IdTipoSector);
            string vTipoEntOfProv = Convert.ToString(vEntidadProvOferente.TipoEntOfProv);
            SetSessionParameter("vIdEntidad", vIdEntidad);
            SetSessionParameter("vIdTipoSector", vIdTipoSector);
            SetSessionParameter("vTipoEntOfProv", vTipoEntOfProv);

        }
        catch (UserInterfaceException ex)
        {
        }
        catch (Exception ex)
        {
        }

    }
    private void MuestraControlesPersonaJuridica()
    {
        pnlRepresentanteLegal.Visible = true;
        pnlConfirma.Visible = true;
        lblRazon.Visible = true;
        txtRazon.Visible = true;
        lblParticipacion.Visible = true;
        txtPorcentajeParticipacion.Visible = true;
    }

}
