using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;
using System.IO;
/// <summary>
/// Página de consulta a través de filtros para la entidad Integrantes
/// </summary>
public partial class Page_Valida_Integrantes_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/ValidarIntegrantes";
    ProveedorService vProveedorService = new ProveedorService();
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
    SIAService vRUBOService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {

        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
            }
            SaveState(this.Master, PageName);
        }
    }



    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }


    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvIntegrantes, GridViewSortExpression, true);

            //Se muestran las observaciones
            BuscarObservacionesIntegrantes();
            BuscarDocumentos();
            ConsultarEstadoValidacion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    /// 
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            //Se revisa si está en modo de validacion proveedor sino está en modo validación está en modo edición
            if (string.IsNullOrEmpty(GetSessionParameter("ValidarInfoIntegrantesEntidad.NroRevision").ToString()))
            {

                var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
                int vIdEntidad = Convert.ToInt32(identida);
                vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
                ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
                ValidaNumerodeIntegrantes(Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
                if (vRUBOService.ConsultarUsuario(vEntidadProvOferente.UsuarioCrea).Rol.Split(';').Contains("PROVEEDORES") && vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol != "PROVEEDORES")
                {
                    if (vEntidadProvOferente.UsuarioCrea == GetSessionUser().NombreUsuario)
                    {

                        toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                    }
                }
                else
                {
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }


            }
            else
            {
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

                ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
                ValidaNumerodeIntegrantes(Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
            }



            toolBar.EstablecerTitulos("Datos Básicos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Comando de renglón para la grilla gvIntegrantes
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvIntegrantes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewArchivo":

                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvIntegrantes.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Integrantes.IdIntegrante", strValue);
            ManejoControles vManejoControles = new ManejoControles();
            vManejoControles.OpenWindowDetailIntegrante(this, "DetailIntegrante.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvIntegrantes_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvIntegrantes.SelectedRow);
    }
    protected void gvIntegrantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvIntegrantes.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvIntegrantes_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));

            var myGridResults = vProveedorService.ConsultarIntegrantess(vIdEntidad);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Integrantes), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Integrantes, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Integrantes.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Integrantes.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ValidaNumerodeIntegrantes(int pIdEntidad)
    {
        try
        {
            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ValidaNumIntegrantes(pIdEntidad);

            if (vIntegrantes.NumIntegrantes != 0)
            {
                txtNumIntegrantes.Text = vIntegrantes.NumIntegrantes.ToString();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ConsultarEstadoValidacion()
    {
        int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
        ValidacionIntegrantesEntidad validaInt = vProveedorService.Consultar_ValidacionIntegrantesEntidad(vIdEntidad);
        txtEdoValidacion.Text = validaInt.EstadoValidacionIntegrantes;
    }

    private void BuscarObservacionesIntegrantes()
    {
        ucHistorialRevisiones1.Titulo = "Historial de observaciones del módulo Integrantes";
        ucHistorialRevisiones1.MensajeGuardar = "¿Está seguro del resultado de su revisión?";
        ucHistorialRevisiones1.TituloConfirmaYAprueba = "¿Confirma que los datos coinciden con los documentos adjuntos y lo aprueba?";
        ucHistorialRevisiones1.MensajeErrorConfirmaYAprueba = "Confirme si los datos coinciden con los documentos adjuntos y los aprueba";
        ucHistorialRevisiones1.MensajeErrorSinObservaciones = "Registre una observaci&oacute;n v&aacute;lida";

        int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
        List<ValidarInfo> lista = vProveedorService.ConsultarValidarInfoIntegrantesEntidad(vIdEntidad, null, null).ToList<ValidarInfo>();
        ucHistorialRevisiones1.DataSource = lista;
        ucHistorialRevisiones1.ConfirmaVisible = false;
        //
        //Si está activa la opcion de ver Panel para agregar Observaciones
        //

        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarInfoIntegrantesEntidad.NroRevision").ToString()))
        {
            int NroRevision = Convert.ToInt16(GetSessionParameter("ValidarInfoIntegrantesEntidad.NroRevision").ToString());
            ucHistorialRevisiones1.NroRevision = NroRevision;
            ucHistorialRevisiones1.ConfirmaVisible = true;
            ucHistorialRevisiones1.eventoGuardarObservaciones += new ImageClickDelegate(OnGuardarObservacionesDatosBasicos);

            //Despues de cargar se cambia el estado a En Validación
            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
            Int32 iIdEntidad = Convert.ToInt32(idEntidad);
            int resultado = 0;


            Icbf.Proveedor.Entity.ValidacionIntegrantesEntidad validaInt = vProveedorService.Consultar_ValidacionIntegrantesEntidad(iIdEntidad);
            if (!validaInt.Finalizado) // SI NO ESTA FINALIZADO
            {
                validaInt.IdEstadoValidacionIntegrantes = 5; //Estado En Validación
                validaInt.Finalizado = false;
                validaInt.UsuarioModifica = GetSessionUser().NombreUsuario;
                validaInt.FechaModifica = DateTime.Now;

                resultado = vProveedorService.ModificarValidacionIntegrantesEntidad_EstadoIntegrantes(validaInt);


                if (resultado >= 1)
                {
                    //Exitoso
                    //Se carga el control con la última información
                    ValidarInfoIntegrantesEntidad vValidarInfointegrantes = vProveedorService.ConsultarValidarInfoIntegrantesEntidad_Ultima(iIdEntidad);
                    ucHistorialRevisiones1.Observaciones = vValidarInfointegrantes.Observaciones;
                    ucHistorialRevisiones1.ConfirmaYAprueba = vValidarInfointegrantes.ConfirmaYAprueba;

                    Session["ucHistorialRevisiones1"] = ucHistorialRevisiones1;

                }
                else
                {
                    toolBar.MostrarMensajeError("No pudo cambiarse al estado En Validación");
                }
            }
        }
    }

    private void OnGuardarObservacionesDatosBasicos()
    {
        try
        {
            //inicializa variables
            int vResultado;
            string vObservaciones = string.Empty;
            bool bNuevo = false;
            ValidarInfoIntegrantesEntidad vValidarInfointegrantes = new ValidarInfoIntegrantesEntidad();


            //toma la entidad en session
            String vIdEntidad = Convert.ToString(GetSessionParameter("EntidadProvOferente.IdEntidad"));


            //Obtiene la info del usercontrol
            ValidarInfo info = ((Page_Proveedor_UserControl_ucHistorialRevisiones)Session["ucHistorialRevisiones1"]).vValidarInfo;
            Int32 iIdEntidad = Convert.ToInt32(vIdEntidad);

            //obtiene la ultima observacion disponible
            vValidarInfointegrantes = vProveedorService.ConsultarValidarInfoIntegrantesEntidad_Ultima(iIdEntidad);

            //si existe en la bd la actualiza
            if (vValidarInfointegrantes.IdEntidad == null)
                vValidarInfointegrantes.IdEntidad = 0;
            if (vValidarInfointegrantes.IdEntidad == 0)
            {
                //No existe
                vValidarInfointegrantes.IdEntidad = iIdEntidad;
                bNuevo = true;
            }

            if (info.Observaciones != null)
                vObservaciones = Convert.ToString(info.Observaciones).Trim();
            vValidarInfointegrantes.Observaciones = vObservaciones;
            vValidarInfointegrantes.ConfirmaYAprueba = Convert.ToBoolean(info.ConfirmaYAprueba);
            vValidarInfointegrantes.NroRevision = info.NroRevision;

            //InformacionAudioria(vValidarInfoFinancieraEntidad, this.PageName, vSolutionPage);

            int idEstado = 0;
            if (vValidarInfointegrantes.ConfirmaYAprueba == true)
                idEstado = 4;//Se obtiene el estado Validado
            else
                idEstado = 3;//Se obtiene el estado Por Ajustar

            if (bNuevo)
            {
                vValidarInfointegrantes.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.InsertarValidarInfoIntegrantesEntidad(vValidarInfointegrantes, idEstado);
            }
            else
            {
                vValidarInfointegrantes.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.ModificarValidarInfoIntegrantesEntidad(vValidarInfointegrantes, idEstado);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)
            {
                SetSessionParameter("ValidarProveedor.ObservacionGuardada", "1");
                //toolBar.MostrarMensajeGuardado("Guardado exitosamente");
                //BuscarObservacionesIntegrantes();
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }


    /// <summary>
    /// Manejador del evento click para el botón Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        var identi = GetSessionParameter("EntidadProvOferente.IdEntidad");
        int vIdEntidad = Convert.ToInt32(identi);
        vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
        if (vEntidadProvOferente.IdEstado == 5) //estado EN VALIDACIÓN 
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript", "<script>javascript: alert('Esta información no puede ser editada en este momento, intente más tarde');</script>");
        }
        else
        {
            SetSessionParameter("EntidadProvOferente.IdEntidad", hfIdEntidad.Value);
            NavigateTo(SolutionPage.Edit);
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("../ValidarProveedor/Revision.aspx");
    }

    /// <summary>
    /// Buscar Documentos
    /// </summary>
    private void BuscarDocumentos()
    {
        try
        {
            int vIdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            EntidadProvOferente prov = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            List<DocDatosBasicoProv> listDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, prov.IdTipoPersona.ToString(), prov.IdTipoSector.ToString(), "", "-1");
            List<DocDatosBasicoProv> listDocsNew = new List<DocDatosBasicoProv>();
            foreach (DocDatosBasicoProv doc in listDocs)
            {
                if (!doc.NombreDocumento.Equals("Cédula Representante Legal"))
                    listDocsNew.Add(doc);
            }
            gvDocDatosBasicoProv.DataSource = listDocsNew;
            gvDocDatosBasicoProv.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":

                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected bool verDocumento(object idTipoPersona)
    {
        int variable = 0;
        variable = Convert.ToInt32(idTipoPersona);
        if (variable == 2)
            return true;
        else
            return false;
    }

}
