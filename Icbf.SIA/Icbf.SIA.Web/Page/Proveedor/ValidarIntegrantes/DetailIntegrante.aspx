﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/VentanaPopup.master"
    AutoEventWireup="true" CodeFile="DetailIntegrante.aspx.cs" Inherits="Page_Proveedor_ValidarIntegrantes_DetailIntegrante" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="UcFecha" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/Jqfc.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.numeric.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function pageLoad() {
            $(".decimal").numeric(",");

            $('.currency').blur(function () {
                $('.currency').formatPercentage({ decimalSymbol: ',', digitGroupSymbol: '.' });
            });

            $("input").keypress(function (evt) {
                //Deterime where our character code is coming from within the event
                var charCode = evt.charCode || evt.keyCode;
                if (charCode == 13) { //Enter key's keycode
                    return false;
                }
            });
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upIntegrante" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfIdIntegrante" runat="server" />
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        Tipo de Persona o Asociación
                    </td>
                    <td>
                        Tipo de Identificación
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdTipoPersona" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlIdTipoIdentificacionPersonaNatural" Enabled="false"
                            TabIndex="1">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        Número de Identificación
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblDV" Visible="false">DV</asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="20" Enabled="false"
                            TabIndex="2"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDV" Enabled="false" Visible="false" TabIndex="3"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:Panel runat="server" ID="pnlTercero" Visible="false">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td>
                            Primer Nombre
                        </td>
                        <td>
                            Segundo Nombre
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" MaxLength="128"
                                TabIndex="4"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" MaxLength="128"
                                TabIndex="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Primer Apellido
                        </td>
                        <td>
                            Segundo Apellido
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" MaxLength="128"
                                TabIndex="6"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" MaxLength="128"
                                TabIndex="7"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Fecha de Nacimiento
                        </td>
                        <td>
                            Sexo
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <uc1:fecha ID="cuFechaNac" runat="server" Enabled="False" TabIndex="8" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSexo" Enabled="false" TabIndex="9"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        <asp:Label runat="server" ID="lblRazon" Visible="false">Razón</asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtRazon" Enabled="false" Visible="false" MaxLength="128"
                            TabIndex="10"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:Panel runat="server" ID="pnlRepresentanteLegal" Visible="false">
                <table width="90%" align="center">
                    <tr>
                        <td colspan="2">
                            <h3 class="lbBloque">
                                <asp:Label ID="lblRepresentante" runat="server" Text="Representante Legal"></asp:Label>
                            </h3>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Tipo identificación
                        </td>
                        <td>
                            Número de Identificación
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td style="width: 50%">
                            <asp:DropDownList ID="ddlTipoDocumentoRepr" runat="server" Enabled="False" TabIndex="11"
                                Width="300px">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hfRepreLegal" runat="server" />
                        </td>
                        <td style="width: 50%">
                            <asp:TextBox ID="txtIdentificacionRepr" runat="server" Enabled="False" MaxLength="128"
                                TabIndex="12"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftIdentificacionRepr" runat="server" FilterType="Numbers"
                                TargetControlID="txtIdentificacionRepr" ValidChars="/áéíóúÁÉÍÓÚñÑ.,@_():;1234567890" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Primer Nombre
                        </td>
                        <td>
                            Segundo Nombre
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox ID="txtPrimerNombreRepr" runat="server" Enabled="False" MaxLength="128"
                                TabIndex="13" Width="80%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSegundoNombreRepr" runat="server" Enabled="False" MaxLength="128"
                                TabIndex="14" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Primer Apellido
                        </td>
                        <td>
                            Segundo Apellido
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:TextBox ID="TxtPrimerApellidoRepr" runat="server" Enabled="False" MaxLength="128"
                                TabIndex="15" Width="80%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TxtSegundoApellidoRepr" runat="server" Enabled="False" MaxLength="128"
                                TabIndex="16" Width="80%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            Fecha de nacimiento
                        </td>
                        <td>
                            Sexo
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell" width="50%;">
                            <UcFecha:fecha ID="cuFechaNacRepLegal" runat="server" Enabled="false" Requerid="false"
                                TabIndex="17" />
                        </td>
                        <td class="Cell" width="50%;">
                            <asp:DropDownList runat="server" ID="ddlSexoRepLegal" Enabled="false" TabIndex="18">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlConfirma" Visible="false">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td width="50%">
                            Confirma que el certificado de existencia y representación legal esta vigente (inferior
                            o igual a 30 días)
                        </td>
                        <td>
                            Confirma que esta persona representa legalmente la Entidad
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:DropDownList runat="server" ID="ddlConfirmaCertificado" AutoPostBack="true"
                                Enabled="false" TabIndex="19">
                                <asp:ListItem Text=" " Value="-1"></asp:ListItem>
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlConfirmaPersona" AutoPostBack="true" Enabled="false"
                                TabIndex="20">
                                <asp:ListItem Text=" " Value="-1"></asp:ListItem>
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        <asp:Label runat="server" ID="lblParticipacion" Visible="false">
                Porcentaje de Participación *</asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtPorcentajeParticipacion" MaxLength="8" CssClass="decimal currency"
                            Visible="false" Enabled="false" TabIndex="21"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        Fecha Registro
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblRegistradoPor" Visible="false">
                Registrado Por
                        </asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <UcFecha:fecha ID="ucFechaRegistro" runat="server" Enabled="false" Requerid="false"
                            TabIndex="22" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtRegistradoPor" Visible="false" Enabled="false"
                            TabIndex="23"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
