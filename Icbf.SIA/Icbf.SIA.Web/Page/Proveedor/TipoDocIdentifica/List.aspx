<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoDocIdentifica_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código Documento *
            </td>
            <td>
                Descripción *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoDocIdentifica"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoDocIdentifica" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoDocIdentifica" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoDocIdentifica_PageIndexChanging" 
                        OnSelectedIndexChanged="gvTipoDocIdentifica_SelectedIndexChanged"
                        AllowSorting="true" OnSorting="gvTipoDocIdentifica_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Código Documento" DataField="CodigoDocIdentifica" SortExpression="CodigoDocIdentifica"/>
                            <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
