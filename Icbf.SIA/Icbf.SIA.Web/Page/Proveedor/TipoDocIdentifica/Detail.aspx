<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoDocIdentifica_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoDocIdentifica" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código Documento *
            </td>
            <td>
                Descripción *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoDocIdentifica"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
