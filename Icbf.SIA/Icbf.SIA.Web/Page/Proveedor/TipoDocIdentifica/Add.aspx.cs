using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de registro y edición de programas de tipos de identificación
/// </summary>
public partial class Page_TipoDocIdentifica_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/TipoDocIdentifica";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoDocIdentifica vTipoDocIdentifica = new TipoDocIdentifica();

            vTipoDocIdentifica.CodigoDocIdentifica = Convert.ToDecimal(txtCodigoDocIdentifica.Text);
            vTipoDocIdentifica.Descripcion = Convert.ToString(txtDescripcion.Text).Trim();

            if (Request.QueryString["oP"] == "E")
            {
            vTipoDocIdentifica.IdTipoDocIdentifica = Convert.ToInt32(hfIdTipoDocIdentifica.Value);
                vTipoDocIdentifica.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoDocIdentifica, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarTipoDocIdentifica(vTipoDocIdentifica);
            }
            else
            {
                vTipoDocIdentifica.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoDocIdentifica, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarTipoDocIdentifica(vTipoDocIdentifica);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoDocIdentifica.IdTipoDocIdentifica", vTipoDocIdentifica.IdTipoDocIdentifica);
                SetSessionParameter("TipoDocIdentifica.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("TipoDocIdentifica", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTipoDocIdentifica = Convert.ToInt32(GetSessionParameter("TipoDocIdentifica.IdTipoDocIdentifica"));
            RemoveSessionParameter("TipoDocIdentifica.Id");

            TipoDocIdentifica vTipoDocIdentifica = new TipoDocIdentifica();
            vTipoDocIdentifica = vProveedorService.ConsultarTipoDocIdentifica(vIdTipoDocIdentifica);
            hfIdTipoDocIdentifica.Value = vTipoDocIdentifica.IdTipoDocIdentifica.ToString();
            txtCodigoDocIdentifica.Text = vTipoDocIdentifica.CodigoDocIdentifica.ToString();
            txtDescripcion.Text = vTipoDocIdentifica.Descripcion;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoDocIdentifica.UsuarioCrea, vTipoDocIdentifica.FechaCrea, vTipoDocIdentifica.UsuarioModifica, vTipoDocIdentifica.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
