using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de registro y edición de tipos de sector
/// </summary>
public partial class Page_TipoSectorEntidad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/TipoSectorEntidad";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoSectorEntidad vTipoSectorEntidad = new TipoSectorEntidad();

            vTipoSectorEntidad.CodigoSectorEntidad = Convert.ToString(txtCodigoSectorEntidad.Text).Trim();
            vTipoSectorEntidad.Descripcion = Convert.ToString(txtDescripcion.Text).Trim();
            vTipoSectorEntidad.Estado = Convert.ToBoolean(Convert.ToInt16(rblEstado.SelectedValue));

            if (Request.QueryString["oP"] == "E")
            {
            vTipoSectorEntidad.IdTipoSectorEntidad = Convert.ToInt32(hfIdTipoSectorEntidad.Value);
                vTipoSectorEntidad.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoSectorEntidad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarTipoSectorEntidad(vTipoSectorEntidad);
            }
            else
            {
                vTipoSectorEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoSectorEntidad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarTipoSectorEntidad(vTipoSectorEntidad);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoSectorEntidad.IdTipoSectorEntidad", vTipoSectorEntidad.IdTipoSectorEntidad);
                SetSessionParameter("TipoSectorEntidad.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tipo Sector Entidad", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTipoSectorEntidad = Convert.ToInt32(GetSessionParameter("TipoSectorEntidad.IdTipoSectorEntidad"));
            RemoveSessionParameter("TipoSectorEntidad.Id");

            TipoSectorEntidad vTipoSectorEntidad = new TipoSectorEntidad();
            vTipoSectorEntidad = vProveedorService.ConsultarTipoSectorEntidad(vIdTipoSectorEntidad);
            hfIdTipoSectorEntidad.Value = vTipoSectorEntidad.IdTipoSectorEntidad.ToString();
            txtCodigoSectorEntidad.Text = vTipoSectorEntidad.CodigoSectorEntidad;
            txtDescripcion.Text = vTipoSectorEntidad.Descripcion;
            rblEstado.SelectedValue = Convert.ToString(Convert.ToInt32(vTipoSectorEntidad.Estado));
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoSectorEntidad.UsuarioCrea, vTipoSectorEntidad.FechaCrea, vTipoSectorEntidad.UsuarioModifica, vTipoSectorEntidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
