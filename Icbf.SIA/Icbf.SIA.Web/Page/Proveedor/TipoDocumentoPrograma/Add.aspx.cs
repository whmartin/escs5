using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;

/// <summary>
/// Página de registro y edición de tipos de documento en los programas
/// </summary>
public partial class Page_TipoDocumentoPrograma_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    SeguridadService vSeguridadService = new SeguridadService();
    string PageName = "Proveedor/TipoDocumentoPrograma";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;


            TipoDocumentoPrograma vTipoDocumentoPrograma = new TipoDocumentoPrograma();
            if (ddlTipoDocumento.SelectedValue == "-1" || ddlTipoDocumento.SelectedValue == "0") throw new Exception("Seleccione el tipo de documento");
            if (ddlPrograma.SelectedValue == "-1" || ddlPrograma.SelectedValue == "0") throw new Exception("Seleccione el programa");
            if (cblExtensionesPermitidas.SelectedValue == "" ) throw new Exception("Seleccione al menos una extensión");
            vTipoDocumentoPrograma.IdTipoDocumento = int.Parse(ddlTipoDocumento.SelectedValue);
            vTipoDocumentoPrograma.IdPrograma = int.Parse(ddlPrograma.SelectedValue);
            vTipoDocumentoPrograma.Estado = Convert.ToBoolean(rblEstado.SelectedValue);
            vTipoDocumentoPrograma.MaxPermitidoKB = Convert.ToInt32(txtMaxPermitidoKB.Text);
            //vTipoDocumentoPrograma.ExtensionesPermitidas = Convert.ToString(txtExtensionesPermitidas.Text);
            string sList = string.Empty;
            foreach (ListItem list in cblExtensionesPermitidas.Items)
            {
                sList += list.Selected ? list + "," : "";
            }
            if (sList.Length > 2)
            {
                vTipoDocumentoPrograma.ExtensionesPermitidas = sList.Remove(sList.Length - 1);
            }
            vTipoDocumentoPrograma.ObligRupNoRenovado = Convert.ToInt32(chkObligRupNoRenovado.Checked);
            vTipoDocumentoPrograma.ObligRupRenovado = Convert.ToInt32(chkObligRupRenovado.Checked);
            vTipoDocumentoPrograma.ObligPersonaJuridica = Convert.ToInt32(chkObligPersonaJuridica.Checked);
            vTipoDocumentoPrograma.ObligPersonaNatural = Convert.ToInt32(chkObligPersonaNatural.Checked);
            vTipoDocumentoPrograma.ObligSectorPrivado = Convert.ToInt32(chkObligSectorPrivado.Checked);
            vTipoDocumentoPrograma.ObligSectorPublico = Convert.ToInt32(chkObligSectorPublico.Checked);
            vTipoDocumentoPrograma.ObligENtidadNAcional = chkObligEntNacional.Checked;
            vTipoDocumentoPrograma.ObligENtidadExtrajera = chkObligEntExtrajera.Checked;
            vTipoDocumentoPrograma.ObligConsorcio = chkObligConsorcio.Checked;
            vTipoDocumentoPrograma.ObligUnionTemporal = chkObligUnionTemporal.Checked;
            
            


            if (Request.QueryString["oP"] == "E")
            {
            vTipoDocumentoPrograma.IdTipoDocumentoPrograma = Convert.ToInt32(hfIdTipoDocumentoPrograma.Value);
                vTipoDocumentoPrograma.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoDocumentoPrograma, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarTipoDocumentoPrograma(vTipoDocumentoPrograma);
            }
            else
            {
                vTipoDocumentoPrograma.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoDocumentoPrograma, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarTipoDocumentoPrograma(vTipoDocumentoPrograma);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoDocumentoPrograma.IdTipoDocumentoPrograma", vTipoDocumentoPrograma.IdTipoDocumentoPrograma);
                SetSessionParameter("TipoDocumentoPrograma.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tipo Documento Programa", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTipoDocumentoPrograma = Convert.ToInt32(GetSessionParameter("TipoDocumentoPrograma.IdTipoDocumentoPrograma"));
            RemoveSessionParameter("TipoDocumentoPrograma.Id");

            TipoDocumentoPrograma vTipoDocumentoPrograma = new TipoDocumentoPrograma();
            vTipoDocumentoPrograma = vProveedorService.ConsultarTipoDocumentoPrograma(vIdTipoDocumentoPrograma);
            hfIdTipoDocumentoPrograma.Value = vTipoDocumentoPrograma.IdTipoDocumentoPrograma.ToString();
            ddlTipoDocumento.SelectedValue = vTipoDocumentoPrograma.IdTipoDocumento.ToString();
            ddlPrograma.SelectedValue  = vTipoDocumentoPrograma.IdPrograma.ToString();
            rblEstado.SelectedValue = vTipoDocumentoPrograma.Estado.ToString();
            txtMaxPermitidoKB.Text = vTipoDocumentoPrograma.MaxPermitidoKB.ToString();
            //txtExtensionesPermitidas.Text = vTipoDocumentoPrograma.ExtensionesPermitidas;
            string sList = vTipoDocumentoPrograma.ExtensionesPermitidas;
            foreach (string item in sList.Split(','))
            {
                foreach (ListItem list in cblExtensionesPermitidas.Items)
                {
                    if (list.Value == item)
                    {
                        list.Selected = true;
                        break;
                    }
                }                            
            }            
            chkObligRupNoRenovado.Checked = vTipoDocumentoPrograma.ObligRupNoRenovado==1?true:false  ;
            chkObligRupRenovado.Checked = vTipoDocumentoPrograma.ObligRupRenovado == 1 ? true : false;
            chkObligPersonaJuridica.Checked = vTipoDocumentoPrograma.ObligPersonaJuridica == 1 ? true : false;
            chkObligPersonaNatural.Checked = vTipoDocumentoPrograma.ObligPersonaNatural == 1 ? true : false;
            chkObligSectorPublico.Checked = vTipoDocumentoPrograma.ObligSectorPublico == 1 ? true : false;
            chkObligSectorPrivado.Checked = vTipoDocumentoPrograma.ObligSectorPrivado == 1 ? true : false;
            chkObligEntNacional.Checked = vTipoDocumentoPrograma.ObligENtidadNAcional;
            chkObligEntExtrajera.Checked = vTipoDocumentoPrograma.ObligENtidadExtrajera;
            chkObligConsorcio.Checked = vTipoDocumentoPrograma.ObligConsorcio;
            chkObligUnionTemporal.Checked = vTipoDocumentoPrograma.ObligUnionTemporal;


            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoDocumentoPrograma.UsuarioCrea, vTipoDocumentoPrograma.FechaCrea, vTipoDocumentoPrograma.UsuarioModifica, vTipoDocumentoPrograma.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            List<Programa> lista = new List<Programa>();
            int idModulo = vSeguridadService.ConsultarModulos("Proveedores",true)[0].IdModulo;
            lista = vProveedorService.ConsultarProgramas(idModulo);
            ddlPrograma.DataSource = lista;
            ddlPrograma.DataTextField = "CodigoPrograma";
            ddlPrograma.DataValueField = "IdPrograma";
            ddlPrograma.DataBind();


            ManejoControles.LlenarTipoDocumentos(ddlTipoDocumento, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
