using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página que despliega el detalle del registro de tipos de documento por programa
/// </summary>
public partial class Page_TipoDocumentoPrograma_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/TipoDocumentoPrograma";
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }


    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoDocumentoPrograma.IdTipoDocumentoPrograma", hfIdTipoDocumentoPrograma.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoDocumentoPrograma = Convert.ToInt32(GetSessionParameter("TipoDocumentoPrograma.IdTipoDocumentoPrograma"));
            RemoveSessionParameter("TipoDocumentoPrograma.IdTipoDocumentoPrograma");

            if (GetSessionParameter("TipoDocumentoPrograma.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoDocumentoPrograma");


            TipoDocumentoPrograma vTipoDocumentoPrograma = new TipoDocumentoPrograma();
            vTipoDocumentoPrograma = vProveedorService.ConsultarTipoDocumentoProgramas(vIdTipoDocumentoPrograma,null,null)[0];
            hfIdTipoDocumentoPrograma.Value = vTipoDocumentoPrograma.IdTipoDocumentoPrograma.ToString();
            txtCodigoTipoDocumento.Text = vTipoDocumentoPrograma.CodigoTipoDocumento; 
            txtTipoDocumento.Text = vTipoDocumentoPrograma.Descripcion.ToString();
            txtCodigoPrograma.Text = vTipoDocumentoPrograma.CodigoPrograma.ToString();
            txtPrograma.Text = vTipoDocumentoPrograma.NombrePrograma;
            rblEstado.SelectedValue = (vTipoDocumentoPrograma.Estado.ToString());
            txtMaxPermitidoKB.Text = vTipoDocumentoPrograma.MaxPermitidoKB.ToString();
            txtExtensionesPermitidas.Text = vTipoDocumentoPrograma.ExtensionesPermitidas;
            chkObligRupNoRenovado.Checked = vTipoDocumentoPrograma.ObligRupNoRenovado == 1 ? true : false;
            chkObligRupRenovado.Checked = vTipoDocumentoPrograma.ObligRupRenovado == 1 ? true : false;
            chkObligPersonaJuridica.Checked = vTipoDocumentoPrograma.ObligPersonaJuridica == 1 ? true : false;
            chkObligPersonaNatural.Checked = vTipoDocumentoPrograma.ObligPersonaNatural== 1 ? true : false;
            chkObligSectorPublico.Checked = vTipoDocumentoPrograma.ObligSectorPublico == 1 ? true : false;
            chkObligSectorPrivado.Checked = vTipoDocumentoPrograma.ObligSectorPrivado == 1 ? true : false;

            chkObligEntNacional.Checked = vTipoDocumentoPrograma.ObligENtidadNAcional;
            chkObligEntExtrajera.Checked = vTipoDocumentoPrograma.ObligENtidadExtrajera;
            chkObligConsorcio.Checked = vTipoDocumentoPrograma.ObligConsorcio;
            chkObligUnionTemporal.Checked = vTipoDocumentoPrograma.ObligUnionTemporal;

            ObtenerAuditoria(PageName, hfIdTipoDocumentoPrograma.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoDocumentoPrograma.UsuarioCrea, vTipoDocumentoPrograma.FechaCrea, vTipoDocumentoPrograma.UsuarioModifica, vTipoDocumentoPrograma.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Eliminar registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoDocumentoPrograma = Convert.ToInt32(hfIdTipoDocumentoPrograma.Value);

            TipoDocumentoPrograma vTipoDocumentoPrograma = new TipoDocumentoPrograma();
            vTipoDocumentoPrograma = vProveedorService.ConsultarTipoDocumentoPrograma(vIdTipoDocumentoPrograma);
            InformacionAudioria(vTipoDocumentoPrograma, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarTipoDocumentoPrograma(vTipoDocumentoPrograma);
            if (vResultado == 0 && vTipoDocumentoPrograma.IdTipoDocumentoPrograma != 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoDocumentoPrograma.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else if (vResultado == 0 && vTipoDocumentoPrograma.IdTipoDocumentoPrograma == 0)
            {
                toolBar.MostrarMensajeError("Este registro ya está eliminado.");
                SetSessionParameter("TipoDocumentoPrograma.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Detalle Tipo Documento Programa", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
