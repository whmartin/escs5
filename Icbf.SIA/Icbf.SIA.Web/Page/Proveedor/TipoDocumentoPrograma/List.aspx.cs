using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Seguridad.Entity;
using Icbf.Seguridad.Service;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tipos de documento asociados al programa
/// </summary>
public partial class Page_TipoDocumentoPrograma_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/TipoDocumentoPrograma";
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdTipoDocumentoPrograma = null;
            int? vIdTipoDocumento = null;
            int? vIdPrograma = null;

            if (ddlPrograma.SelectedValue == "-1" || ddlPrograma.SelectedValue == "0")
            {
                vIdPrograma = null;
            }
            else { vIdPrograma = int.Parse(ddlPrograma.SelectedValue); }
            
            gvTipoDocumentoPrograma.DataSource = vProveedorService.ConsultarTipoDocumentoProgramas( vIdTipoDocumentoPrograma, vIdTipoDocumento, vIdPrograma);
            gvTipoDocumentoPrograma.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvTipoDocumentoPrograma.PageSize = PageSize();
            gvTipoDocumentoPrograma.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo Documento Programa", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoDocumentoPrograma.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoDocumentoPrograma.IdTipoDocumentoPrograma", strValue);
            SetSessionParameter("TipoDocumentoPrograma.Guardado", "0");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoDocumentoPrograma_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoDocumentoPrograma.SelectedRow);
    }
    protected void gvTipoDocumentoPrograma_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoDocumentoPrograma.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //ManejoControles.LlenarTipoDocumentos(ddlTipoDocumento, "-1", true);
            List<Programa> lista = new List<Programa>();
            int idModulo = vSeguridadService.ConsultarModulos("Proveedores", true)[0].IdModulo;
            lista = vProveedorService.ConsultarProgramas(idModulo);
            
            ddlPrograma.DataSource = lista;
            ddlPrograma.DataTextField = "CodigoPrograma";
            ddlPrograma.DataValueField = "IdPrograma";
            ddlPrograma.DataBind();
            if (GetSessionParameter("TipoDocumentoPrograma.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoDocumentoPrograma.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
