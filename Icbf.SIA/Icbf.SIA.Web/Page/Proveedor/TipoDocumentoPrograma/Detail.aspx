<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoDocumentoPrograma_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdTipoDocumentoPrograma" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                C&oacute;digo Programa
            </td>
            <td>
                Programa
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoPrograma" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPrograma" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                C&oacute;digo Tipo Documento
            </td>
            <td>
                Tipo Documento
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoTipoDocumento" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoDocumento" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Extensiones Permitidas
            </td>
            <td>
                MaxPermitido KB
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtExtensionesPermitidas" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMaxPermitidoKB" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Persona Natural
            </td>
            <td>
                Obligatorio Persona Jur&iacute;dica
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligPersonaNatural" Enabled="false"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligPersonaJuridica" Enabled="false"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Sector Privado
            </td>
            <td>
                Obligatorio Sector P&uacute;blico
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligSectorPrivado" Enabled="false"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligSectorPublico" Enabled="false"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Rup Renovado
            </td>
            <td>
                Obligatorio Rup No Renovado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligRupRenovado" Enabled="false"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligRupNoRenovado" Enabled="false"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Entidad Nacional
            </td>
            <td>
                Obligatorio Entidad Extranjera
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:CheckBox runat="server" ID="chkObligEntNacional" Enabled="false"></asp:CheckBox>
            </td>
            <td class="style1">
                <asp:CheckBox runat="server" ID="chkObligEntExtrajera" Enabled="false"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Consorcio
            </td>
            <td>
                Obligatorio Uni&oacute;n Temporal
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:CheckBox runat="server" ID="chkObligConsorcio" Enabled="false"></asp:CheckBox>
            </td>
            <td>
            <asp:CheckBox runat="server" ID="chkObligUnionTemporal" Enabled="false"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Activo" Value="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            height: 27px;
        }
    </style>
</asp:Content>
