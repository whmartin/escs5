<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_TipoDocumentoPrograma_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdTipoDocumentoPrograma" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Programa
            </td>
            <td>
                Tipo Documento
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Seleccione un tipo de identificación"
                    ControlToValidate="ddlTipoDocumento" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un valor de la lista</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlPrograma">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoDocumento">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Extensiones Permitidas
                <%--<asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                        ControlToValidate="txtExtensionesPermitidas" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^((jpg|JPG|jpeg|JPEG|png|PNG|PDF|pdf)|,)*$">Extensi&oacute;n Inv&aacute;lida
                </asp:RegularExpressionValidator>--%>
                <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="Seleccione al menos una extensión"
                                        ControlToValidate="cblExtensionesPermitidas" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Display="Dynamic" InitialValue="-1"></asp:RequiredFieldValidator>--%>
            </td>
            <td>
                MaxPermitido KB
                <Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtMaxPermitidoKB"
                    FilterType="Numbers" ValidChars="/1234567890" />
                <asp:RequiredFieldValidator runat="server" ID="revNIT" ErrorMessage="Registre el número de identificación"
                    ControlToValidate="txtMaxPermitidoKB" SetFocusOnError="true" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic">Registre el peso max del archivo</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBoxList ID="cblExtensionesPermitidas" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="jpg" Value="jpg"></asp:ListItem>
                    <asp:ListItem Text="jpeg" Value="jpeg"></asp:ListItem>
                    <asp:ListItem Text="gif" Value="gif"></asp:ListItem>
                    <asp:ListItem Text="png" Value="png"></asp:ListItem>
                    <asp:ListItem Text="pdf" Value="pdf"></asp:ListItem>
                </asp:CheckBoxList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMaxPermitidoKB" MaxLength="6"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Persona Natural
            </td>
            <td>
                Obligatorio Persona Juridica
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligPersonaNatural"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligPersonaJuridica"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Sector Privado
            </td>
            <td>
                Obligatorio Sector Público
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligSectorPrivado"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligSectorPublico"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Rup Renovado
            </td>
            <td>
                Obligatorio Rup No Renovado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligRupRenovado"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligRupNoRenovado"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Entidad Nacional
            </td>
            <td>
                Obligatorio Entidad Extrajera
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligEntNacional"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligEntExtrajera"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Obligatorio Consorcio
            </td>
            <td>
                Obligatorio Uni&oacute;n Temporal
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox runat="server" ID="chkObligConsorcio"></asp:CheckBox>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkObligUnionTemporal"></asp:CheckBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Debe seleccionar un estado" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" align="left">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Activo" Value="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
