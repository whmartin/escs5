<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoDocumentoPrograma_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Programa
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
               <asp:DropDownList runat="server" ID="ddlPrograma"   ></asp:DropDownList>
            </td>
            <td>
                
            </td>
        </tr>
        
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoDocumentoPrograma" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoDocumentoPrograma" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoDocumentoPrograma_PageIndexChanging" OnSelectedIndexChanged="gvTipoDocumentoPrograma_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="C&oacute;digo Tipo Documento" DataField="CodigoTipoDocumento" />
                            <asp:BoundField HeaderText="Descripci&oacute;n" DataField="Descripcion" />
                            <asp:BoundField HeaderText="C&oacute;digo Programa" DataField="CodigoPrograma" />
                            <asp:BoundField HeaderText="Nombre Programa" DataField="NombrePrograma" />
                            <asp:BoundField HeaderText="MaxPermitido KB" DataField="MaxPermitidoKB" />
                            <asp:BoundField HeaderText="Extensiones Permitidas" DataField="ExtensionesPermitidas" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
