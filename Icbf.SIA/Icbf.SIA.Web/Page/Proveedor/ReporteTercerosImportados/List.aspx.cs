﻿using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

public partial class Page_Proveedor_ReporteTercerosImportados_List : GeneralWeb
{

    #region Instances and var

    ManejoControles vManejoControles= new ManejoControles();
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();

    /// <summary>
    /// Variable global del path de la página
    /// </summary>
    string vPageName = "Proveedor/ReporteTercerosImportados";
    /// <summary>
    /// Instancia de masterPrincipal
    /// </summary>
    private masterPrincipal toolBar;

    #endregion

    #region Load form

    /// <summary>
    /// Evento para cargar los controles y eventos
    /// </summary>
    /// <param name="sender">the page</param>
    /// <param name="e">thePreInit</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            toolBar.LipiarMensajeError();
            CargarDatosIniciales();
        }        
    }

    #endregion 

    #region METHODS

    /// <summary>
    /// Método para cargar los datos iniciales de la página
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnReporte_Click);
            //toolBar.eventoLimpiar += new ToolBarDelegate(btnLimpiar_Click);
            CargarDatosIniciales();

            toolBar.EstablecerTitulos("Reporte de terceros importados", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para obtener la sesión e inicializar controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        ListItem ls = new ListItem();        
        ls = new ListItem();
        ls.Text = "TODAS";
        ls.Value = "0";
        CheckBoxTodosClaseActividad.Items.Add(ls);
            
        llenarClaseActividad();
        llenarNivelInteres();
        llenarDepartamento();
        ddlMunicipio.Enabled = true;
        CheckBoxTodosMunicipio.Enabled = true;
                
    }

    /// <summary>
    /// Método para agregar cargar ListBox de Nivel de Interes
    /// </summary>
    public void llenarNivelInteres()
    {
        ddlNivelInteres.Items.Clear();
        CheckBoxTodosNivelInteres.Items.Add(new ListItem("TODAS", "0"));
        ddlNivelInteres.Items.Insert(0, new ListItem("Alto", "Alto"));
        ddlNivelInteres.Items.Insert(1, new ListItem("Medio", "Medio"));
        ddlNivelInteres.Items.Insert(2, new ListItem("Bajo", "Bajo"));
        //ddlNivelInteres.SelectedValue = "-1";
    }

    /// <summary>
    /// Método para agregar cargar ListBox de Clase Actividad
    /// </summary>
    public void llenarClaseActividad()
    {
        List<ClaseActividad> vDClaseActividad = vProveedorService.ConsultarClaseActividads(null, null, true).OrderByDescending(x => x.Descripcion).ToList<ClaseActividad>();
        ddlClaseActividad.Items.Clear();
        if (vDClaseActividad.Count() > 1)
        {
            LoadCheckClaseActividad(vDClaseActividad);
        }
    }

    /// <summary>
    /// Método para agregar ClaseActividad seleccionadas
    /// </summary>
    /// <param name="lista"></param>
    public void LoadCheckClaseActividad(List<ClaseActividad> lista)
    {
        int var;
        ListItem ls = new ListItem();
        //ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_ddlClaseActividad');");

        foreach (ClaseActividad pci in lista.OrderBy(t => t.Descripcion))
        {
            ls = new ListItem();
            ls.Text = pci.Descripcion;
            var = pci.IdClaseActividad;
            ls.Value = var.ToString();
            ddlClaseActividad.Items.Add(ls);
        }
    }

    /// <summary>
    /// Método para agregar cargar ListBox de Clase Actividad
    /// </summary>
    public void llenarDepartamento()
    {
        List<ExperienciaDepartamento> vDClaseActividad = vOferenteService.ConsultarExperienciaDepartamento().OrderBy(t => t.NombreDepartamento).ToList();
        ddlDepartamento.Items.Clear();
        CheckBoxTodosDepartamento.Items.Add(new ListItem("TODOS", "0"));
        if (vDClaseActividad.Count() > 1)
        {
            LoadCheckDepartamento(vDClaseActividad);
        }

    }

    public void llenarMunicipiosXDepto()
    {
        List<ExperienciaMunicipio> vListExperienciaMunicipio = vProveedorService.ConsultarMunicipiosXDepto(CargarSeleccion(this.ddlDepartamento)).OrderBy(t => t.NombreMunicipio).ToList();
        ddlMunicipio.Items.Clear();
        CheckBoxTodosMunicipio.Items.Add(new ListItem("TODOS", "0"));
        if (vListExperienciaMunicipio.Count() > 1)
        {
            LoadCheckMunicipio(vListExperienciaMunicipio);
        }
    }

    /// <summary>
    /// Método para agregar ClaseActividad seleccionadas
    /// </summary>
    /// <param name="lista"></param>
    public void LoadCheckDepartamento(List<ExperienciaDepartamento> lista)
    {
        int var;
        ListItem ls = new ListItem();
        ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_chbxListDepartamento');");

        foreach (ExperienciaDepartamento pci in lista)
        {
            ls = new ListItem();
            ls.Text = pci.NombreDepartamento;
            var = pci.IdDepartamento;
            ls.Value = var.ToString();
            ddlDepartamento.Items.Add(ls);
        }
    }

    /// <summary>
    /// Método para agregar ClaseActividad seleccionadas
    /// </summary>
    /// <param name="lista"></param>
    public void LoadCheckMunicipio(List<ExperienciaMunicipio> lista)
    {
        int var;
        ListItem ls = new ListItem();
        //ls.Attributes.Add("onclick", "javascript:CheckBoxListSelect('cphCont_ddlMunicipio');");

        foreach (ExperienciaMunicipio pci in lista)
        {
            ls = new ListItem();
            ls.Text = pci.NombreMunicipio;
            var = pci.IdMunicipio;
            ls.Value = var.ToString();
            ddlMunicipio.Items.Add(ls);
        }
    }

    /// <summary>
    /// Método que selecciona todas el checkBoxList recibido
    /// </summary>
    public void SelecionarTodos(CheckBoxList ddlList)
    {
        for (int i = 0; ddlList.Items.Count > i; i++)
        {
            ddlList.Items[i].Selected = true;
        }
    }

    // Método que limpiar el checkBoxList seleccionadas
    /// </summary>
    public void SelecionarNinguno(CheckBoxList ddlList)
    {
        for (int i = 0; ddlList.Items.Count > i; i++)
        {
            ddlList.Items[i].Selected = false;
        }
    }


    /// <summary>
    /// Método que carga las regionales seleccionadas para consulta
    /// </summary>
    public string CargarSeleccion(CheckBoxList list)
    {
        string vCadena = "";

        for (int i = 0; list.Items.Count > i; i++)
        {
            if (list.Items[i].Selected)
            {
                if (vCadena.Equals(""))
                {
                    vCadena = list.Items[i].Value.ToString();
                }
                else
                {
                    vCadena = vCadena + "," + list.Items[i].Value.ToString();
                }
            }
        }
        return vCadena;
    }

    /// <summary>
    /// Método que carga los objetos y parámetros para el reporte
    /// </summary>
    private void GenerarConsultaGenerica()
    {
        string mensaje = String.Empty;

        try
        {
            string vClaseActividad = CargarSeleccion(ddlClaseActividad);
            string vNivelInteres = CargarSeleccion(ddlNivelInteres);
            string vDepartamento = CargarSeleccion(ddlDepartamento);
            string vMunicipios = CargarSeleccion(ddlMunicipio);

            ReporteTercerosImportados vReporte = new ReporteTercerosImportados();
            vReporte.ClaseActividad = vClaseActividad;
            vReporte.NivelInteres = vNivelInteres;
            vReporte.Departamento = vDepartamento;
            vReporte.Municipio = vMunicipios;
            vReporte.NumeroIdentificacion = txtNumeroIdentificacion.Text;
            vReporte.NombreRazonSocial = txtNombre.Text;
            /// No realiza el llamado al reporte si no hay datos
            if (!vProveedorService.ConsultarDatosReporte(vReporte))
            {
                toolBar.MostrarMensajeError("No se encontraron resultados, verifique por favor");
                return;
            }

            //Crea un objeto de tipo reporte
            Report objReport = new Report("ReporteTercerosImportados", true, vPageName, "Reporte de terceros importados");
            
            //Adiciona los parametros al objeto reporte
            objReport.AddParameter("IdClaseActividad", string.IsNullOrEmpty(vClaseActividad) ? null : vClaseActividad);
            objReport.AddParameter("NivelInteres", string.IsNullOrEmpty(vNivelInteres) ? null : vNivelInteres);
            objReport.AddParameter("IdDepartamentos", string.IsNullOrEmpty(vDepartamento) ? null : vDepartamento);
            objReport.AddParameter("IdMunicipios", string.IsNullOrEmpty(vMunicipios) ? null : vMunicipios);

            objReport.AddParameter("NumeroIdentificacion", string.IsNullOrEmpty(txtNumeroIdentificacion.Text) ? null : txtNumeroIdentificacion.Text);
            objReport.AddParameter("NombrerazonSocial", string.IsNullOrEmpty(txtNombre.Text) ? null : txtNombre.Text);
            
            //Crea un session con el objeto Reporte
            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    #endregion

    #region EVENTS

    /// <summary>
    /// Evento para generar consulta generica
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void btnReporte_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        GenerarConsultaGenerica();

    }

    ///// <summary>
    ///// Evento para limpiar la pantalla y controles
    ///// </summary>
    ///// <param name="sender">the Page</param>
    ///// <param name="e">the Click</param>
    //protected void btnLimpiar_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        LimpiarControles(ObtenerContenedorMaster(toolBar));
    //        CargarDatosIniciales();
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    /// <summary>
    /// Evento para seleccionar todas las clases de actividad
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void CheckBoxTodosClaseActividad_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CheckBoxTodosClaseActividad.Items[0].Selected)
        {
            SelecionarTodos(this.ddlClaseActividad);
        }
        else
        {
            SelecionarNinguno(this.ddlClaseActividad);
        }
    }

    /// <summary>
    /// Evento para seleccionar todos los niveles de interés
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void CheckBoxTodosNivelInteres_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CheckBoxTodosNivelInteres.Items[0].Selected)
        {
            SelecionarTodos(this.ddlNivelInteres);
        }
        else
        {
            SelecionarNinguno(this.ddlNivelInteres);
        }
    }

    /// <summary>
    /// Evento para seleccionar todos los departamentos
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void CheckBoxTodosDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CheckBoxTodosDepartamento.Items[0].Selected)
        {
            SelecionarTodos(this.ddlDepartamento);            
            llenarMunicipiosXDepto();
            SelecionarTodos(this.ddlMunicipio);
            CheckBoxTodosMunicipio.Items[0].Selected = true;
        }
        else
        {
            SelecionarNinguno(this.ddlDepartamento);
            SelecionarNinguno(this.ddlMunicipio);
            CheckBoxTodosMunicipio.Items[0].Selected = false;
            CheckBoxTodosMunicipio.Items.Clear();
            ddlMunicipio.DataSource = new List<ExperienciaMunicipio>();
            ddlMunicipio.DataBind();
            ddlMunicipio.Enabled = true;
            CheckBoxTodosMunicipio.Enabled = true;            
        }
        
    }

    /// <summary>
    /// Evento para seleccionar todos los municicpios
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void CheckBoxTodosMunicipio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CheckBoxTodosMunicipio.Items[0].Selected)
        {
            SelecionarTodos(this.ddlMunicipio);
        }
        else
        {
            SelecionarNinguno(this.ddlMunicipio);
        }
    }

    /// <summary>
    /// Evento para seleccionar todos los municipios
    /// </summary>
    /// <param name="sender">the Page</param>
    /// <param name="e">the Click</param>
    protected void chbxListDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlMunicipio.Enabled = true;
        CheckBoxTodosMunicipio.Enabled = true;
        llenarMunicipiosXDepto();
    }
    #endregion 
}