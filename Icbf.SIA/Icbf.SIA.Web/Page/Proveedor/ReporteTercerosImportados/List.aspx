﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/General/General/Master/main2.master" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_ReporteTercerosImportados_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <table width="90%" align="center">
        <tr class="rowB">
            <td>Clase de actividad *
                  <asp:CustomValidator runat="server" ID="cvmodulelist"
                  ClientValidationFunction="ValidateModuleList" ForeColor="Red"
                  ErrorMessage="Campo Requerido" ValidationGroup="btnBuscar"></asp:CustomValidator>
            </td>            
            <td>Nivel de Inter&eacute;s</td>            
        </tr>
        <tr class="rowA">
            <td class="Cell" style="height: 80px">
                   <div style="width: 400px; height: 25px; overflow-y: hidden; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="CheckBoxTodosClaseActividad" runat="server" Width="400px" AutoPostBack="true"
                            OnSelectedIndexChanged="CheckBoxTodosClaseActividad_SelectedIndexChanged" />
                    </div>                   
                    <div style="width: 400px; height: 80px; overflow-y: scroll; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="ddlClaseActividad" runat="server" Width="400px"/>
                    </div>                    
             </td>
            <td class="Cell" style="height: 80px">
                   <div style="width: 400px; height: 25px; overflow-y: hidden; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="CheckBoxTodosNivelInteres" runat="server" Width="400px" AutoPostBack="true"
                            OnSelectedIndexChanged="CheckBoxTodosNivelInteres_SelectedIndexChanged" />
                    </div>                   
                    <div style="width: 400px; height: 80px; overflow-y: scroll; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="ddlNivelInteres" runat="server" Width="400px" />
                    </div>                    
             </td>
        </tr>
        <tr class="rowB">
            <td>Departamento</td>
            <td>Municipio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell" style="height: 80px">
                   <div style="width: 400px; height: 25px; overflow-y: hidden; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="CheckBoxTodosDepartamento" runat="server" Width="400px" AutoPostBack="true"
                            OnSelectedIndexChanged="CheckBoxTodosDepartamento_SelectedIndexChanged" />
                    </div>                   
                    <div style="width: 400px; height: 80px; overflow-y: scroll; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="ddlDepartamento" runat="server" Width="400px" AutoPostBack="true"
                            OnSelectedIndexChanged="chbxListDepartamento_SelectedIndexChanged" />
                    </div>                    
             </td>
            <td class="Cell" style="height: 80px">
                   <div style="width: 400px; height: 25px; overflow-y: hidden; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="CheckBoxTodosMunicipio" runat="server" Width="400px" AutoPostBack="true"
                            OnSelectedIndexChanged="CheckBoxTodosMunicipio_SelectedIndexChanged" />
                    </div>                   
                    <div style="width: 400px; height: 80px; overflow-y: scroll; overflow-x: hidden;
                        border-style: solid; border-width: thin; border-color: Gray;">                        
                        <asp:CheckBoxList ID="ddlMunicipio" runat="server" Width="400px" />
                    </div>                    
             </td>
        </tr>
        <tr class="rowB">
            <td>N&uacute;mero de Identificaci&oacute;n</td>
            <td>Nombre o Raz&oacute;n Social</td>
        </tr>
        <tr class="rowA">
            <td><asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="256" Width="80%"></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNumeroIdentificacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="&aacute;&eacute;&iacute;&oacute;&uacute;&Aacute;&Eacute;&Iacute;&Oacute;&Uacute;&ntilde;&Ntilde;@/.,_():; " />
            </td>

            <td><asp:TextBox runat="server" ID="txtNombre" MaxLength="128" Width="80%"></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="&aacute;&eacute;&iacute;&oacute;&uacute;&Aacute;&Eacute;&Iacute;&Oacute;&Uacute;&ntilde;&Ntilde;@/.,_():; " />
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        label {
            font-weight:400;
            margin-bottom: 0px;
        }
    </style>
    <script type="text/javascript">
        // javascript to add to your aspx page
        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= ddlClaseActividad.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>
