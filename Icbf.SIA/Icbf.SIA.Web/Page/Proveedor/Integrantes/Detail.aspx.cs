using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Net.Mail;
using System.Configuration;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using System.IO;
using System.Data;
using Icbf.Seguridad.Service;

/// <summary>
/// Página de visualización detallada para la entidad Integrantes
/// </summary>
public partial class Page_Integrantes_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/Integrantes";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Integrantes.IdIntegrante", hfIdIntegrante.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdIntegrante = Convert.ToInt32(GetSessionParameter("Integrantes.IdIntegrante"));
            RemoveSessionParameter("Integrantes.IdIntegrante");

            if (GetSessionParameter("Integrantes.Guardado").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado();
            }
            if (GetSessionParameter("Integrantes.Guardado").ToString() == "4")
            {
                toolBar.MostrarMensajeGuardado("Usted ha completado el registro de integrantes de forma exitosa");
            }


            RemoveSessionParameter("Integrantes.Guardado");


            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ConsultarIntegrantes(vIdIntegrante);
            hfIdIntegrante.Value = vIntegrantes.IdIntegrante.ToString();
            ddlIdTipoPersona.SelectedValue = vIntegrantes.IdTipoPersona.ToString();
            CargarIdTipoIdentificacionPersonaNatural();
            ddlIdTipoIdentificacionPersonaNatural.SelectedValue = vIntegrantes.IdTipoIdentificacionPersonaNatural.ToString();
            txtNumeroIdentificacion.Text = vIntegrantes.NumeroIdentificacion;

            try
            {
                int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
                if (vTipoPersona == 1)
                {
                    BuscaDatosTercero(vIntegrantes.IdTipoIdentificacionPersonaNatural, vIntegrantes.NumeroIdentificacion);
                }
                else if (vTipoPersona == 2)
                {
                    BuscaDatosProveedor();
                }
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }


            txtPorcentajeParticipacion.Text = vIntegrantes.PorcentajeParticipacion.ToString();
            ddlConfirmaCertificado.SelectedValue = vIntegrantes.ConfirmaCertificado;
            ddlConfirmaPersona.SelectedValue = vIntegrantes.ConfirmaPersona;
            ucFechaRegistro.Date = vIntegrantes.FechaCrea;


            SIAService vRUBOService = new SIAService();
            if (!vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                lblRegistradoPor.Visible = true;
                txtRegistradoPor.Visible = true;
                txtRegistradoPor.Text = vIntegrantes.UsuarioCrea;
            }



            ObtenerAuditoria(PageName, hfIdIntegrante.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vIntegrantes.UsuarioCrea, vIntegrantes.FechaCrea, vIntegrantes.UsuarioModifica, vIntegrantes.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdIntegrante = Convert.ToInt32(hfIdIntegrante.Value);

            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ConsultarIntegrantes(vIdIntegrante);
            InformacionAudioria(vIntegrantes, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarIntegrantes(vIntegrantes);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Integrantes.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            Icbf.Proveedor.Entity.EntidadProvOferente vProv = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            if (vProv.UsuarioCrea.Equals(GetSessionUser().NombreUsuario))
            {
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
                toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            }
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            //JV Se activa botón ELIMINAR Solo si es el mismo usuario que registro la información y si el estado del mod Integrantes es Registrado, Por ajustar o Por validar.
            ValidacionIntegrantesEntidad validaInt = vProveedorService.Consultar_ValidacionIntegrantesEntidad(vIdEntidad);
            if (vProv.UsuarioCrea.Equals(GetSessionUser().NombreUsuario))
            {
                if (validaInt.EstadoValidacionIntegrantes.Equals("REGISTRADO") || validaInt.EstadoValidacionIntegrantes.Equals("POR AJUSTAR") || validaInt.EstadoValidacionIntegrantes.Equals("POR VALIDAR"))
                    toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            }


            toolBar.EstablecerTitulos("Integrantes", SolutionPage.Detail.ToString());
            ValidaNumerodeIntegrantes(vIdEntidad);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarIdTipoPersona();
            CargarIdTipoIdentificacionPersonaNatural();
            CargarIdTipoDocRepLegal();
            CargarListSexo();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarListSexo()
    {
        ddlSexoRepLegal.Items.Insert(0, new ListItem("Seleccione", "-1"));
        ddlSexoRepLegal.Items.Insert(1, new ListItem("MASCULINO", "M"));
        ddlSexoRepLegal.Items.Insert(2, new ListItem("FEMENINO", "F"));
        ddlSexoRepLegal.SelectedValue = "-1";
    }

    /// <summary>
    /// Método de carga de los datos de IdTipoPersona
    /// </summary>
    private void CargarIdTipoPersona()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoPersonaIntegrante(ddlIdTipoPersona, "-1", true);
    }

    /// <summary>
    /// Método de carga de los datos de IdTipoIdentificacionPersonaNatural
    /// </summary>
    private void CargarIdTipoIdentificacionPersonaNatural()
    {
        int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
        ManejoControles Controles = new ManejoControles();
        if (vTipoPersona == 1)
        {
            Controles.LlenarTipoDocumentoRL(ddlIdTipoIdentificacionPersonaNatural, "-1", true);
        }
        else
        {
            Controles.LlenarTipoDocumentoN(ddlIdTipoIdentificacionPersonaNatural, "-1", true);
        }
    }
    private void CargarIdTipoDocRepLegal()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoDocumentoRL(ddlTipoDocumentoRepr, "-1", true);
    }
    private void BuscaDatosTercero(int vIdIdTipoIdentificacion, string vNumeroIdentificacion)
    {
        OcultaControles();
        try
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));

            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTerceroTipoNumeroIdentificacion(vIdIdTipoIdentificacion, vNumeroIdentificacion);

            MuestraControlesPersonaNatural();
            txtDV.Text = Convert.ToString(vTercero.DigitoVerificacion);
            txtPrimerNombre.Text = vTercero.PrimerNombre;
            txtSegundoNombre.Text = vTercero.SegundoNombre;
            txtPrimerApellido.Text = vTercero.PrimerApellido;
            txtSegundoApellido.Text = vTercero.SegundoApellido;
            cuFechaNac.Date = Convert.ToDateTime(vTercero.FechaNacimiento);
            if (vTercero.Sexo.Equals("F"))
                txtSexo.Text = "Femenino";
            else if (vTercero.Sexo.Equals("M"))
                txtSexo.Text = "Masculino";
            else
                txtSexo.Text = vTercero.Sexo;
            //JV-21/08/2014 Si está aqui buscando en TERCEROS es porque es perosna Natural, y si es Natural no debe precargar Documentos.
            //BuscarDocumentos(vTercero.IdTercero, Convert.ToString(vTercero.IdTipoPersona));
            pnlDocumentos.Visible = false;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void OcultaControles()
    {
        pnlConfirma.Visible = false;
        pnlDocumentos.Visible = false;
        pnlDocumentosJuridica.Visible = false;
        pnlRepresentanteLegal.Visible = false;
        pnlTercero.Visible = false;
        lblDV.Visible = false;
        txtDV.Visible = false;
        lblRazon.Visible = false;
        txtRazon.Visible = false;
        lblParticipacion.Visible = false;
        txtPorcentajeParticipacion.Visible = false;
    }
    /// <summary>
    /// Muestra los controles si el tipo de Persona es Natural
    /// </summary>
    private void MuestraControlesPersonaNatural()
    {
        pnlTercero.Visible = true;
        //pnlConfirma.Visible = true;
        pnlDocumentos.Visible = true;
        lblParticipacion.Visible = true;
        txtPorcentajeParticipacion.Visible = true;
    }

    #region Documentos

    /// <summary>
    /// Busca Documentos de Tercero y llena grilla
    /// </summary>
    private void BuscarDocumentos(int pIdTercero, string pIdTipoPersona)
    {
        try
        {
            gvAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(pIdTercero, pIdTipoPersona, "");
            gvAdjuntos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAdjuntos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int rowIndex = gvAdjuntos.SelectedIndex;
        string strValue = gvAdjuntos.DataKeys[rowIndex].Value.ToString();
        ViewState["IdDocTercero"] = strValue;
    }

    /// <summary>
    /// Comando de renglón para la grilla Adjuntos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewLogo":

                    MemoryStream ArchivoImagen = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (ArchivoImagen != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", ArchivoImagen.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se logro obtener la imagen desde el servidor.");
                        return;
                    }

                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Comandos de renglón para la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;

        int vIdEntidad = Convert.ToInt32(GetSessionParameter("vIdEntidad"));
        string vIdTipoSector = GetSessionParameter("vIdTipoSector").ToString();
        string vTipoEntOfProv = GetSessionParameter("vTipoEntOfProv").ToString();
        string vTipoPersona = ddlIdTipoPersona.SelectedValue;

        BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);

    }
    protected void gvDocDatosBasicoProv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocDatosBasicoProv.SelectedRow);
    }

    /// <summary>
    /// Buscar Documentos
    /// </summary>
    private void BuscarDocumentosDatosBasicos(int vIdEntidad, string vTipoProveedor, string vTipoSector, string vTipoEntidad)
    {
        try
        {

            List<DocDatosBasicoProv> listDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, vTipoProveedor, vTipoSector, "", vTipoEntidad);
            List<DocDatosBasicoProv> documentoAMostrar = new List<DocDatosBasicoProv>();
            foreach (DocDatosBasicoProv doc in listDocs)
            {
                if (doc.NombreDocumento.Equals("Certificado de existencia y representación legal o de domicilio para entidades extranjeras"))
                {
                    documentoAMostrar.Add(doc);
                    break;
                }
            }

            gvDocDatosBasicoProv.DataSource = documentoAMostrar;
            gvDocDatosBasicoProv.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocDatosBasicoProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocDatosBasicoProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    /// <summary>
    /// Busca en proveedores datos básicos si el tipo de persona es juridico
    /// </summary>
    private void BuscaDatosProveedor()
    {
        OcultaControles();
        try
        {

            string vTipoPersona = ddlIdTipoPersona.SelectedValue;
            string vIdTipoIdentificacion = ddlIdTipoIdentificacionPersonaNatural.SelectedValue;
            string vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            lblDV.Visible = true;
            txtDV.Visible = true;
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProv_SectorPrivado(vTipoPersona, vIdTipoIdentificacion, vNumeroIdentificacion, null, null, null);

            MuestraControlesPersonaJuridica();
            txtRazon.Text = vEntidadProvOferente.Proveedor;
            txtDV.Text = Convert.ToString(vEntidadProvOferente.DV);

            vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
            vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);

            if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
            {
                vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);
                ddlTipoDocumentoRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
                txtIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
                txtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
                txtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                if (!string.IsNullOrEmpty(vEntidadProvOferente.RepresentanteLegal.Sexo))
                    ddlSexoRepLegal.SelectedValue = vEntidadProvOferente.RepresentanteLegal.Sexo;
                if (vEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                    cuFechaNacRepLegal.Date = DateTime.Parse(vEntidadProvOferente.RepresentanteLegal.FechaNacimiento.ToString());
            }
            int vIdEntidad = vEntidadProvOferente.IdEntidad;
            string vIdTipoSector = Convert.ToString(vEntidadProvOferente.IdTipoSector);
            string vTipoEntOfProv = Convert.ToString(vEntidadProvOferente.TipoEntOfProv);
            SetSessionParameter("vIdEntidad", vIdEntidad);
            SetSessionParameter("vIdTipoSector", vIdTipoSector);
            SetSessionParameter("vTipoEntOfProv", vTipoEntOfProv);
            BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }
    private void MuestraControlesPersonaJuridica()
    {
        pnlRepresentanteLegal.Visible = true;
        pnlDocumentosJuridica.Visible = true;
        pnlConfirma.Visible = true;
        lblRazon.Visible = true;
        txtRazon.Visible = true;
        lblParticipacion.Visible = true;
        txtPorcentajeParticipacion.Visible = true;
    }

    protected void ValidaNumerodeIntegrantes(int pIdEntidad)
    {
        try
        {
            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ValidaNumIntegrantes(pIdEntidad);

            if (vIntegrantes.NumIntegrantes == vIntegrantes.IntegrantesReg)
            {
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
