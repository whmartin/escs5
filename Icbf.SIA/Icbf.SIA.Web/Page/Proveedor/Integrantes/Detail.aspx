<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Integrantes_Detail" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="UcFecha" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdIntegrante" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo de persona o asociaci&oacute;n
            </td>
            <td>
                Tipo de identificaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoPersona" Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoIdentificacionPersonaNatural" Enabled="false"
                    TabIndex="1">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                N&uacute;mero de identificaci&oacute;n
            </td>
            <td>
                <asp:Label runat="server" ID="lblDV" Visible="false">DV</asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="20" Enabled="false"
                    TabIndex="2"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDV" Enabled="false" Visible="false" TabIndex="3"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlTercero" Visible="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Primer nombre
                </td>
                <td>
                    Segundo nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" MaxLength="128"
                        TabIndex="4"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" MaxLength="128"
                        TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer apellido
                </td>
                <td>
                    Segundo apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" MaxLength="128"
                        TabIndex="6"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" MaxLength="128"
                        TabIndex="7"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha de nacimiento
                </td>
                <td>
                    Sexo
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha ID="cuFechaNac" runat="server" Enabled="False" TabIndex="8" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSexo" Enabled="false" TabIndex="9"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label runat="server" ID="lblRazon" Visible="false">Raz&oacute;n Social</asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRazon" Enabled="false" Visible="false" MaxLength="128"
                    TabIndex="10" Width="100%"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label runat="server" ID="lblParticipacion" Visible="false">
                Porcentaje de participaci&oacute;n</asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td align="left">
                <div style="width: 8%">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 75%">
                                <Ajax:FilteredTextBoxExtender ID="ftetxtPorcentajeParticipacion" runat="server" TargetControlID="txtPorcentajeParticipacion"
                                    FilterType="Numbers,Custom" ValidChars="%,." />
                                <asp:TextBox runat="server" ID="txtPorcentajeParticipacion" MaxLength="8"
                                    Visible="false" Enabled="false" TabIndex="21" Width="90%"></asp:TextBox>
                            </td>
                            <td style="width: 25%">
                                %
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlRepresentanteLegal" Visible="false">
        <table width="90%" align="center">
            <tr>
                <td colspan="2">
                    <h3 class="lbBloque">
                        <asp:Label ID="lblRepresentante" runat="server" Text="Representante Legal"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Tipo identificación
                </td>
                <td>
                    Número de identificación
                </td>
            </tr>
            <tr class="rowA">
                <td style="width: 50%">
                    <asp:DropDownList ID="ddlTipoDocumentoRepr" runat="server" Enabled="False" TabIndex="11"
                        Width="300px">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hfRepreLegal" runat="server" />
                </td>
                <td style="width: 50%">
                    <asp:TextBox ID="txtIdentificacionRepr" runat="server" Enabled="False" MaxLength="128"
                        TabIndex="12"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftIdentificacionRepr" runat="server" FilterType="Numbers"
                        TargetControlID="txtIdentificacionRepr" ValidChars="/áéíóúÁÉÍÓÚñÑ.,@_():;1234567890" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer nombre
                </td>
                <td>
                    Segundo nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtPrimerNombreRepr" runat="server" Enabled="False" MaxLength="128"
                        TabIndex="13" Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSegundoNombreRepr" runat="server" Enabled="False" MaxLength="128"
                        TabIndex="14" Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer apellido
                </td>
                <td>
                    Segundo apellido
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TxtPrimerApellidoRepr" runat="server" Enabled="False" MaxLength="128"
                        TabIndex="15" Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TxtSegundoApellidoRepr" runat="server" Enabled="False" MaxLength="128"
                        TabIndex="16" Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha de nacimiento
                </td>
                <td>
                    Sexo
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" width="50%;">
                    <UcFecha:fecha ID="cuFechaNacRepLegal" runat="server" Enabled="false" Requerid="false"
                        TabIndex="17" />
                </td>
                <td class="Cell" width="50%;">
                    <asp:DropDownList runat="server" ID="ddlSexoRepLegal" Enabled="false" TabIndex="18">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentos" Visible="false">
        <table width="90%" align="center">
            <tr>
                <td colspan="2">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label2" runat="server" Text="Documentos"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px" DataKeyName="IDDOCADJUNTO"
                        OnRowCommand="gvAdjuntos_RowCommand" OnSelectedIndexChanged="gvAdjuntos_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreTipoDocumento" />
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="viewLogo" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                        CommandArgument='<%#Eval("LinkDocumento")%>' Height="16px" Width="16px" ToolTip="Detalle"
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentosJuridica" Visible="false">
        <table width="90%" align="center">
            <tr>
                <td colspan="2">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label3" runat="server" Text="Documentos"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDocDatosBasicoProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocDatosBasicoProv_RowCommand"
                        OnPageIndexChanging="gvDocDatosBasicoProv_PageIndexChanging" OnSelectedIndexChanged="gvDocDatosBasicoProv_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre del Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar">
                                    </asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlConfirma" Visible="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td width="50%">
                    Confirma que el certificado de existencia y representación legal esta vigente (inferior
                    o igual a 30 días)
                </td>
                <td>
                    Confirma que esta persona representa legalmente la Entidad
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlConfirmaCertificado" AutoPostBack="true"
                        Enabled="false" TabIndex="19">
                        <asp:ListItem Text=" " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlConfirmaPersona" AutoPostBack="true" Enabled="false"
                        TabIndex="20">
                        <asp:ListItem Text=" " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha registro
            </td>
            <td>
                <asp:Label runat="server" ID="lblRegistradoPor" Visible="false">
                Registrado Por
                </asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <UcFecha:fecha ID="ucFechaRegistro" runat="server" Enabled="false" Requerid="false"
                    TabIndex="22" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegistradoPor" Visible="false" Enabled="false"
                    TabIndex="23"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
