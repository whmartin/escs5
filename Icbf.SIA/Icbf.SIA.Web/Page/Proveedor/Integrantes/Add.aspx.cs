using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Net.Mail;
using System.Configuration;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using System.IO;
using System.Data;
using Icbf.Seguridad.Service;

/// <summary>
/// Página de registro y edición para la entidad Integrantes
/// </summary>
public partial class Page_Integrantes_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();
    string PageName = "Proveedor/Integrantes";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        toolBar.LipiarMensajeError();
        toolBar.LimpiarOpcionesAdicionales();
        if (Request.QueryString["oP"] == "E")
        {
            vSolutionPage = SolutionPage.Edit;
        }
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                }
            }
            ValidaNumerodeIntegrantes(Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
        }

    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            Guardar();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        //LinkButton btn = (LinkButton)this.Master.FindControl("btnGuardar");
        //Response.Write("<script>if(confirm('Existen datos que no se ha guardado. ¿Desea hacerlo ahora?')){document.getElementById('" + btn.ClientID + "').click()}else{window.location= '../Integrantes/List.aspx'}</script>");
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad Integrantes
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (pnlPorcentaje.Visible)
            {
                string mensaje = "";
                bool continuarGuardando = true;
                if (Request.QueryString["oP"] == "E")
                {
                    mensaje = validarTerceroOProveedor_EDIT();
                    if (!mensaje.Equals(""))
                        continuarGuardando = false;
                }
                if (continuarGuardando)
                {
                    int vResultado;
                    Integrantes vIntegrantes = new Integrantes();
                    Integrantes vIntegrantesEnBD = new Integrantes();
                    vIntegrantes.IdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
                    vIntegrantes.IdTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
                    vIntegrantes.IdTipoIdentificacionPersonaNatural = Convert.ToInt32(ddlIdTipoIdentificacionPersonaNatural.SelectedValue);
                    vIntegrantes.NumeroIdentificacion = txtNumeroIdentificacion.Text;
                    vIntegrantes.PorcentajeParticipacion = Convert.ToDecimal(txtPorcentajeParticipacion.Text);
                    vIntegrantes.ConfirmaCertificado = Convert.ToString(ddlConfirmaCertificado.SelectedValue);
                    vIntegrantes.ConfirmaPersona = Convert.ToString(ddlConfirmaPersona.SelectedValue);

                    //-----------------
                    vIntegrantesEnBD = vProveedorService.ValidaNumIntegrantes(Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
                    vIntegrantesEnBD.IntegrantesReg = vIntegrantesEnBD.IntegrantesReg + 1;

                    Icbf.Proveedor.Entity.EntidadProvOferente entidad = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
                    if (Request.QueryString["oP"] == "E")
                    {
                        vIntegrantesEnBD.IntegrantesReg = vIntegrantesEnBD.IntegrantesReg - 1;
                        int vIdIntegrante = Convert.ToInt32(GetSessionParameter("Integrantes.IdIntegrante"));
                        Integrantes vIntegranteEditando = new Integrantes();
                        vIntegranteEditando = vProveedorService.ConsultarIntegrantes(vIdIntegrante);

                        vIntegrantesEnBD.PorcentajeParticipacion = (vIntegrantesEnBD.PorcentajeParticipacion - vIntegranteEditando.PorcentajeParticipacion + vIntegrantes.PorcentajeParticipacion);
                    }
                    else
                    {
                        vIntegrantesEnBD.PorcentajeParticipacion = (vIntegrantesEnBD.PorcentajeParticipacion + vIntegrantes.PorcentajeParticipacion);
                    }
                    //if (entidad.IdTipoPersona == 3)
                    //{
                    //    if (vIntegrantesEnBD.PorcentajeParticipacion >= (decimal)95.5)
                    //        vIntegrantesEnBD.PorcentajeParticipacion = Math.Ceiling(vIntegrantesEnBD.PorcentajeParticipacion);
                    //}

                    if (vIntegrantesEnBD.PorcentajeParticipacion == 100 && vIntegrantesEnBD.NumIntegrantes != vIntegrantesEnBD.IntegrantesReg)
                    {
                        //toolBar.MostrarMensajeError("La distribución de participación se ha completado y hace falta asociar más integrantes, por favor revisar");
                        toolBar.MostrarMensajeError("No es posible registrar esta participación, la distribución se completaría y hace falta asociar más integrantes, por favor revisar");
                        txtPorcentajeParticipacion.Focus();
                        return;
                    }
                    else if (vIntegrantesEnBD.PorcentajeParticipacion < 100 && vIntegrantesEnBD.NumIntegrantes == vIntegrantesEnBD.IntegrantesReg)
                    {
                        toolBar.MostrarMensajeError("Este Integrante es el último que puede asociarse y hace falta distribuir participación, por favor revisar");
                        txtPorcentajeParticipacion.Focus();
                        return;
                    }
                    else if (vIntegrantesEnBD.PorcentajeParticipacion == 100 && vIntegrantesEnBD.NumIntegrantes == vIntegrantesEnBD.IntegrantesReg)
                    {
                        SetSessionParameter("Integrantes.Guardado", "4");
                    }
                    else if (vIntegrantesEnBD.PorcentajeParticipacion < 100 && vIntegrantesEnBD.NumIntegrantes != vIntegrantesEnBD.IntegrantesReg)
                    {
                        SetSessionParameter("Integrantes.Guardado", "1");
                    }

                    if (Request.QueryString["oP"] == "E")
                    {
                        vIntegrantes.IdIntegrante = Convert.ToInt32(hfIdIntegrante.Value);
                        vIntegrantes.UsuarioModifica = GetSessionUser().NombreUsuario;
                        InformacionAudioria(vIntegrantes, this.PageName, vSolutionPage);
                        vResultado = vProveedorService.ModificarIntegrantes(vIntegrantes);
                    }
                    else
                    {
                        vIntegrantes.UsuarioCrea = GetSessionUser().NombreUsuario;
                        InformacionAudioria(vIntegrantes, this.PageName, vSolutionPage);
                        vResultado = vProveedorService.InsertarIntegrantes(vIntegrantes);
                    }
                    SetSessionParameter("Integrantes.IdIntegrante", vIntegrantes.IdIntegrante);
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado >= 1)
                    {
                        NavigateTo(SolutionPage.Detail);
                    }
                }
                else
                {
                    Response.Write("<script>alert('" + mensaje + "');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Debe actualizar el formulario.');</script>");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.EstablecerTitulos("Integrantes", SolutionPage.Add.ToString());
            ucFechaRegistro.NoChecarFormato();
            cuFechaNacRepLegal.NoChecarFormato();
            cuFechaNac.NoChecarFormato();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {

            int vIdIntegrante = Convert.ToInt32(GetSessionParameter("Integrantes.IdIntegrante"));
            RemoveSessionParameter("Integrantes.Id");

            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ConsultarIntegrantes(vIdIntegrante);
            hfIdIntegrante.Value = vIntegrantes.IdIntegrante.ToString();
            ddlIdTipoPersona.SelectedValue = vIntegrantes.IdTipoPersona.ToString();
            CargarIdTipoIdentificacionPersonaNatural();
            ddlIdTipoIdentificacionPersonaNatural.SelectedValue = vIntegrantes.IdTipoIdentificacionPersonaNatural.ToString();
            txtNumeroIdentificacion.Text = vIntegrantes.NumeroIdentificacion;
            txtPorcentajeParticipacion.Text = vIntegrantes.PorcentajeParticipacion.ToString();
            hfPorcentajeConsultado.Value = vIntegrantes.PorcentajeParticipacion.ToString();
            ddlConfirmaCertificado.SelectedValue = vIntegrantes.ConfirmaCertificado;
            ddlConfirmaPersona.SelectedValue = vIntegrantes.ConfirmaPersona;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vIntegrantes.UsuarioCrea, vIntegrantes.FechaCrea, vIntegrantes.UsuarioModifica, vIntegrantes.FechaModifica);

            ucFechaRegistro.Date = vIntegrantes.FechaCrea;
            lblFechaRegistro.Visible = true;
            ucFechaRegistro.Visible = true;
            SIAService vRUBOService = new SIAService();
            if (!vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                lblRegistradoPor.Visible = true;
                txtRegistradoPor.Visible = true;
                txtRegistradoPor.Text = vIntegrantes.UsuarioCrea;
            }
            ddlIdTipoPersona.Enabled = true;
            ddlIdTipoIdentificacionPersonaNatural.Enabled = true;
            rfvIdTipoIdentificacionPersonaNatural.Enabled = true;
            txtNumeroIdentificacion.Enabled = true;
            rfvNumeroIdentificacion.Enabled = true;
            validarTerceroOProveedor();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarIdTipoPersona();
            CargarIdTipoIdentificacionPersonaNatural();
            CargarIdTipoDocRepLegal();
            CargarListSexo();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarListSexo()
    {
        ddlSexoRepLegal.Items.Insert(0, new ListItem("Seleccione", "-1"));
        ddlSexoRepLegal.Items.Insert(1, new ListItem("MASCULINO", "M"));
        ddlSexoRepLegal.Items.Insert(2, new ListItem("FEMENINO", "F"));
        ddlSexoRepLegal.SelectedValue = "-1";
    }
    /// <summary>
    /// Método de carga de datos de IdTipoPersona
    /// </summary>
    private void CargarIdTipoPersona()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoPersonaIntegrante(ddlIdTipoPersona, "-1", true);
    }

    /// <summary>
    /// Método de carga de datos de IdTipoIdentificacionPersonaNatural
    /// </summary>
    private void CargarIdTipoIdentificacionPersonaNatural()
    {
        int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
        ManejoControles Controles = new ManejoControles();
        if (vTipoPersona == 1)
        {
            Controles.LlenarTipoDocumentoRL(ddlIdTipoIdentificacionPersonaNatural, "-1", true);
        }
        else
        {
            Controles.LlenarTipoDocumentoN(ddlIdTipoIdentificacionPersonaNatural, "-1", true);
        }
    }
    private void CargarIdTipoDocRepLegal()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoDocumentoRL(ddlTipoDocumentoRepr, "-1", true);
    }

    protected void ddlIdTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!ddlIdTipoPersona.SelectedValue.Equals("-1"))
        {

            ddlIdTipoIdentificacionPersonaNatural.Enabled = true;
            rfvIdTipoIdentificacionPersonaNatural.Enabled = true;

        }
        else
        {
            ddlIdTipoIdentificacionPersonaNatural.Enabled = false;
            rfvIdTipoIdentificacionPersonaNatural.Enabled = false;
            txtNumeroIdentificacion.Enabled = false;
            rfvNumeroIdentificacion.Enabled = false;
            txtDV.Visible = false;
            lblDV.Visible = false;
        }

        CargarIdTipoIdentificacionPersonaNatural();
        OcultaControles();


        if (!ddlIdTipoPersona.SelectedValue.Equals("-1") && !ddlIdTipoPersona.SelectedValue.Equals("1"))
        {
            txtDV.Visible = true;
            lblDV.Visible = true;
        }
        else
        {
            txtDV.Visible = false;
            lblDV.Visible = false;
        }
        txtNumeroIdentificacion.Text = "";
    }
    private void CardaTipoDocRep()
    {
        ManejoControles Controles = new ManejoControles();
        Controles.LlenarTipoDocumentoRL(ddlTipoDocumentoRepr, "-1", true);
    }

    public void validarTerceroOProveedor()
    {
        try
        {
            int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
            if (vTipoPersona == 1)
            {
                BuscaDatosTercero();
            }
            else if (vTipoPersona == 2)
            {
                BuscaDatosProveedor();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    public string validarTerceroOProveedor_EDIT()
    {
        string mensaje = "";
        try
        {
            int vTipoPersona = Convert.ToInt32(ddlIdTipoPersona.SelectedValue);
            if (vTipoPersona == 1)
            {
                mensaje = BuscaDatosTercero_EDIT();
            }
            else if (vTipoPersona == 2)
            {
                mensaje = BuscaDatosProveedor_EDIT();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return mensaje;
    }
    //protected void txtNumeroIdentificacion_TextChanged(object sender, EventArgs e)
    //{
    //    validarTerceroOProveedor();
    //}
    /// <summary>
    /// Busca los Datos del Tercero si el Tipo de Persona es Natural
    /// </summary>
    private void BuscaDatosTercero()
    {
        OcultaControles();
        try
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            int vIdIdTipoIdentificacion = Convert.ToInt32(ddlIdTipoIdentificacionPersonaNatural.SelectedValue);
            string vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTerceroTipoNumeroIdentificacion(vIdIdTipoIdentificacion, vNumeroIdentificacion);
            int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
            int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
            int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;//Se obtiene el estado EnValidación

            if (vTercero.IdTercero == 0)//no existe el Tercero
            {
                Response.Write("<script>alert('El integrante debe estar registrado previamente en el sistema');</script>");
            }
            else if (vTercero.IdEstadoTercero == idEstadoPorValidar || vTercero.IdEstadoTercero == idEstadoRegistrado || vTercero.IdEstadoTercero == idEstadoEnValidacion)
            {
                string sTipoIdentificacion = Convert.ToString(ddlIdTipoIdentificacionPersonaNatural.SelectedItem);
                string sNombre = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " + vTercero.PrimerApellido + " " + vTercero.SegundoApellido;
                EnviaCorreo(sTipoIdentificacion, vNumeroIdentificacion, sNombre);
                Response.Write("<script>alert('Este Integrante actualmente no puede ser asociado. Se encuentra en Validación por parte del ICBF, intente más tarde');</script>");
            }
            else if (vTercero.IdEstadoTercero == idEstadoPorAjustar)
            {
                Response.Write("<script>alert('Este Integrante actualmente no puede ser asociado. Tiene pendiente actualizar información');</script>");
            }
            else if (vTercero.IdEstadoTercero == idEstadoValidado)//validado
            {
                MuestraControlesPersonaNatural();
                txtDV.Text = Convert.ToString(vTercero.DigitoVerificacion);
                txtPrimerNombre.Text = vTercero.PrimerNombre;
                txtSegundoNombre.Text = vTercero.SegundoNombre;
                txtPrimerApellido.Text = vTercero.PrimerApellido;
                txtSegundoApellido.Text = vTercero.SegundoApellido;
                cuFechaNac.Date = Convert.ToDateTime(vTercero.FechaNacimiento);
                if (vTercero.Sexo.Equals("F"))
                    txtSexo.Text = "FEMENINO";
                else if (vTercero.Sexo.Equals("M"))
                    txtSexo.Text = "MASCULINO";
                else
                    txtSexo.Text = vTercero.Sexo;
                //JV-21/08/2014 se quita la parte de cargar Docs del Tercero. ya que solo se cargaran cuando
                // se trate de un Juridico (Documentode de Datos BAsicos)
                //BuscarDocumentos(vTercero.IdTercero, Convert.ToString(vTercero.IdTipoPersona));
                pnlDocumentos.Visible = false;
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private string BuscaDatosTercero_EDIT()
    {
        string mensaje = "";
        try
        {
            OcultaControles();
            //int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            int vIdIdTipoIdentificacion = Convert.ToInt32(ddlIdTipoIdentificacionPersonaNatural.SelectedValue);
            string vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            Tercero vTercero = new Tercero();
            vTercero = vProveedorService.ConsultarTerceroTipoNumeroIdentificacion(vIdIdTipoIdentificacion, vNumeroIdentificacion);
            int idEstadoPorValidar = vProveedorService.ConsultarEstadoTercero("001").IdEstadoTercero;//Se obtiene el estado Por Validar
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoTercero("002").IdEstadoTercero;//Se obtiene el estado Registrador
            int idEstadoPorAjustar = vProveedorService.ConsultarEstadoTercero("003").IdEstadoTercero;//Se obtiene el estado Por Ajustar
            int idEstadoValidado = vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero;//Se obtiene el estado Validado
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoTercero("005").IdEstadoTercero;//Se obtiene el estado EnValidación

            if (vTercero.IdTercero == 0)//no existe el Tercero
            {
                mensaje = "El integrante debe estar registrado previamente en el sistema";
            }
            else if (vTercero.IdEstadoTercero == idEstadoPorValidar || vTercero.IdEstadoTercero == idEstadoRegistrado || vTercero.IdEstadoTercero == idEstadoEnValidacion)
            {
                string sTipoIdentificacion = Convert.ToString(ddlIdTipoIdentificacionPersonaNatural.SelectedItem);
                string sNombre = vTercero.PrimerNombre + " " + vTercero.SegundoNombre + " " + vTercero.PrimerApellido + " " + vTercero.SegundoApellido;
                EnviaCorreo(sTipoIdentificacion, vNumeroIdentificacion, sNombre);
                mensaje = "Este Integrante actualmente no puede ser asociado. Se encuentra en Validación por parte del ICBF, intente más tarde";

            }
            else if (vTercero.IdEstadoTercero == idEstadoPorAjustar)
            {
                mensaje = "Este Integrante actualmente no puede ser asociado. Tiene pendiente actualizar información";
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return mensaje;
    }

    private void MostrarControlesvalidacionEdit_Tercero()
    {
        //    txtDV.Text = Convert.ToString(vTercero.DigitoVerificacion);
        //    txtPrimerNombre.Text = vTercero.PrimerNombre;
        //    txtSegundoNombre.Text = vTercero.SegundoNombre;
        //    txtPrimerApellido.Text = vTercero.PrimerApellido;
        //    txtSegundoApellido.Text = vTercero.SegundoApellido;
        //    cuFechaNac.Date = Convert.ToDateTime(vTercero.FechaNacimiento);
        //    txtSexo.Text = vTercero.Sexo;
        //    //JV-21/08/2014 se quita la parte de cargar Docs del Tercero. ya que solo se cargaran cuando
        //    // se trate de un Juridico (Documentode de Datos BAsicos)
        //    //BuscarDocumentos(vTercero.IdTercero, Convert.ToString(vTercero.IdTipoPersona));
        //    pnlDocumentos.Visible = false;
    }
    /// <summary>
    /// Muestra los controles si el tipo de Persona es Natural
    /// </summary>
    private void MuestraControlesPersonaNatural()
    {
        pnlTercero.Visible = true;
        //pnlConfirma.Visible = true;
        pnlDocumentos.Visible = true;
        pnlPorcentaje.Visible = true;
    }
    /// <summary>
    /// Busca en proveedores datos básicos si el tipo de persona es juridico
    /// </summary>
    private void BuscaDatosProveedor()
    {
        OcultaControles();
        try
        {
            string vTipoPersona = ddlIdTipoPersona.SelectedValue;
            string vIdTipoIdentificacion = ddlIdTipoIdentificacionPersonaNatural.SelectedValue;
            string vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProv_SectorPrivado(vTipoPersona, vIdTipoIdentificacion, vNumeroIdentificacion, null, null, null);
            txtDV.Text = Convert.ToString(vEntidadProvOferente.DV);
            lblDV.Visible = true;
            txtDV.Visible = true;

            int idEstadoPorValidar = vProveedorService.ConsultarEstadoProveedor("POR VALIDAR").IdEstadoProveedor;//Se obtiene el estado Por Validar
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoProveedor("REGISTRADO").IdEstadoProveedor;//Se obtiene el estado Registrador
            int idEstadoPorAjustar = vProveedorService.ConsultarEstadoProveedor("POR AJUSTAR").IdEstadoProveedor;//Se obtiene el estado Por Ajustar
            int idEstadoValidado = vProveedorService.ConsultarEstadoProveedor("VALIDADO").IdEstadoProveedor;//Se obtiene el estado Validado
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoProveedor("EN VALIDACIÓN").IdEstadoProveedor;//Se obtiene el estado EnValidación
            int idEstadoParcial = vProveedorService.ConsultarEstadoProveedor("PARCIAL").IdEstadoProveedor;//Se obtiene el estado Parcial


            if (vEntidadProvOferente.IdEntidad == 0)// El integrante no Existe
            {
                Response.Write("<script>alert('El integrante debe estar registrado previamente en el sistema');</script>");
            }
            else if (vEntidadProvOferente.IdEstadoProveedor == idEstadoRegistrado || vEntidadProvOferente.IdEstadoProveedor == idEstadoEnValidacion || vEntidadProvOferente.IdEstadoProveedor == idEstadoPorValidar)
            {
                string sTipoIdentificacion = Convert.ToString(ddlIdTipoIdentificacionPersonaNatural.SelectedItem);
                string sNombre = vEntidadProvOferente.Proveedor;
                EnviaCorreo(sTipoIdentificacion, vNumeroIdentificacion, sNombre);
                Response.Write("<script>alert('Este Integrante actualmente no puede ser asociado. Se encuentra en Validación por parte del ICBF, intente más tarde');</script>");
            }
            else if (vEntidadProvOferente.IdEstadoProveedor == idEstadoPorAjustar || vEntidadProvOferente.IdEstadoProveedor == idEstadoParcial)
            {
                Response.Write("<script>alert('Este Integrante actualmente no puede ser asociado. Tiene pendiente actualizar información');</script>");
            }
            else if (vEntidadProvOferente.IdEstadoProveedor == idEstadoValidado)//Validado
            {
                MuestraControlesPersonaJuridica();
                txtRazon.Text = vEntidadProvOferente.Proveedor;

                vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
                vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);

                if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
                {
                    vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);

                    ddlTipoDocumentoRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
                    txtIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
                    txtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
                    txtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                    TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                    TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                    if (!string.IsNullOrEmpty(vEntidadProvOferente.RepresentanteLegal.Sexo))
                        ddlSexoRepLegal.SelectedValue = vEntidadProvOferente.RepresentanteLegal.Sexo;
                    if (vEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                        cuFechaNacRepLegal.Date = DateTime.Parse(vEntidadProvOferente.RepresentanteLegal.FechaNacimiento.ToString());
                }
                int vIdEntidad = vEntidadProvOferente.IdEntidad;
                string vIdTipoSector = Convert.ToString(vEntidadProvOferente.IdTipoSector);
                string vTipoEntOfProv = Convert.ToString(vEntidadProvOferente.TipoEntOfProv);
                SetSessionParameter("vIdEntidad", vIdEntidad);
                SetSessionParameter("vIdTipoSector", vIdTipoSector);
                SetSessionParameter("vTipoEntOfProv", vTipoEntOfProv);
                BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }
    private string BuscaDatosProveedor_EDIT()
    {
        string mensaje = "";
        try
        {
            OcultaControles();
            string vTipoPersona = ddlIdTipoPersona.SelectedValue;
            string vIdTipoIdentificacion = ddlIdTipoIdentificacionPersonaNatural.SelectedValue;
            string vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProv_SectorPrivado(vTipoPersona, vIdTipoIdentificacion, vNumeroIdentificacion, null, null, null);
            txtDV.Text = Convert.ToString(vEntidadProvOferente.DV);

            int idEstadoPorValidar = vProveedorService.ConsultarEstadoProveedor("POR VALIDAR").IdEstadoProveedor;//Se obtiene el estado Por Validar
            int idEstadoRegistrado = vProveedorService.ConsultarEstadoProveedor("REGISTRADO").IdEstadoProveedor;//Se obtiene el estado Registrador
            int idEstadoPorAjustar = vProveedorService.ConsultarEstadoProveedor("POR AJUSTAR").IdEstadoProveedor;//Se obtiene el estado Por Ajustar
            int idEstadoValidado = vProveedorService.ConsultarEstadoProveedor("VALIDADO").IdEstadoProveedor;//Se obtiene el estado Validado
            int idEstadoEnValidacion = vProveedorService.ConsultarEstadoProveedor("EN VALIDACIÓN").IdEstadoProveedor;//Se obtiene el estado EnValidación
            int idEstadoParcial = vProveedorService.ConsultarEstadoProveedor("PARCIAL").IdEstadoProveedor;//Se obtiene el estado Parcial


            if (vEntidadProvOferente.IdEntidad == 0)// El integrante no Existe
            {
                mensaje = "El integrante debe estar registrado previamente en el sistema";
            }
            else if (vEntidadProvOferente.IdEstadoProveedor == idEstadoRegistrado || vEntidadProvOferente.IdEstadoProveedor == idEstadoEnValidacion || vEntidadProvOferente.IdEstadoProveedor == idEstadoPorValidar)
            {
                string sTipoIdentificacion = Convert.ToString(ddlIdTipoIdentificacionPersonaNatural.SelectedItem);
                string sNombre = vEntidadProvOferente.Proveedor;
                EnviaCorreo(sTipoIdentificacion, vNumeroIdentificacion, sNombre);
                MostrarControles_ValidacionEditProveedores(vEntidadProvOferente, vTipoPersona);
                mensaje = "Este Integrante actualmente no puede ser asociado. Se encuentra en Validación por parte del ICBF, intente más tarde";

            }
            else if (vEntidadProvOferente.IdEstadoProveedor == idEstadoPorAjustar || vEntidadProvOferente.IdEstadoProveedor == idEstadoParcial)
            {
                MostrarControles_ValidacionEditProveedores(vEntidadProvOferente, vTipoPersona);
                mensaje = "Este Integrante actualmente no puede ser asociado. Tiene pendiente actualizar información";

            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return mensaje;

    }

    private void MostrarControles_ValidacionEditProveedores(Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente, string vTipoPersona)
    {
        MuestraControlesPersonaJuridica();
        txtRazon.Text = vEntidadProvOferente.Proveedor;

        vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
        vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);

        if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
        {
            vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);

            ddlTipoDocumentoRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
            txtIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
            txtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
            txtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
            TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
            TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
            if (!string.IsNullOrEmpty(vEntidadProvOferente.RepresentanteLegal.Sexo))
                ddlSexoRepLegal.SelectedValue = vEntidadProvOferente.RepresentanteLegal.Sexo;
            if (vEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                cuFechaNacRepLegal.Date = DateTime.Parse(vEntidadProvOferente.RepresentanteLegal.FechaNacimiento.ToString());
        }
        int vIdEntidad = vEntidadProvOferente.IdEntidad;
        string vIdTipoSector = Convert.ToString(vEntidadProvOferente.IdTipoSector);
        string vTipoEntOfProv = Convert.ToString(vEntidadProvOferente.TipoEntOfProv);
        SetSessionParameter("vIdEntidad", vIdEntidad);
        SetSessionParameter("vIdTipoSector", vIdTipoSector);
        SetSessionParameter("vTipoEntOfProv", vTipoEntOfProv);
        BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);
    }

    private void MuestraControlesPersonaJuridica()
    {
        pnlRepresentanteLegal.Visible = true;
        pnlDocumentosJuridica.Visible = true;
        pnlConfirma.Visible = true;
        rvConfirmaCertificado.Enabled = true;
        rvConfirmaPersona.Enabled = true;
        lblRazon.Visible = true;
        txtRazon.Visible = true;
        pnlPorcentaje.Visible = true;
    }

    private void OcultaControles()
    {
        pnlConfirma.Visible = false;
        pnlDocumentos.Visible = false;
        pnlDocumentosJuridica.Visible = false;
        pnlRepresentanteLegal.Visible = false;
        pnlTercero.Visible = false;
        lblRazon.Visible = false;
        txtRazon.Visible = false;
        pnlPorcentaje.Visible = false;
        rvConfirmaCertificado.Enabled = false;
        rvConfirmaPersona.Enabled = false;
    }

    #region EnviaCorreo
    /// <summary>
    /// Metodo que envía correo si el Estado del Tercero o Proveedor es diferente de Validado
    /// </summary>
    private void EnviaCorreo(string pIdTipoIdentificacion, string pNumeroIdentificacion, string pNombre)
    {

        string pDe = ConfigurationManager.AppSettings["RemitenteProveedores"];
        string pPara = ConfigurationManager.AppSettings["DestinatarioIntegrantes"];
        string pAsunto = ConfigurationManager.AppSettings["AsuntoProveedoresPassword"];
        string pLinkUrl = ConfigurationManager.AppSettings["URLProveedores"];// nombreUsuario, nuevaClave

        var myMailMessage = new MailMessage();

        myMailMessage.From = new MailAddress(pDe);
        myMailMessage.To.Add(new MailAddress(pPara));
        myMailMessage.Subject = pAsunto;

        myMailMessage.IsBodyHtml = true;


        myMailMessage.Body = "<html><body><table width='100%' height='412' border='0' cellpadding='0' cellspacing='0'> " +
                            "<tbody><tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr><tr><td colspan='3' bgcolor='#81BA3D'>&nbsp;</td></tr> " +
                            "<tr><td width='10%' bgcolor='#81BA3D'>&nbsp;</td><td><table width='100%' border='0' align='center'><tbody> " +
                            "<tr><td width='5%'>&nbsp;</td><td align='center'>&nbsp;</td><td width='5%'>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td align='center'><strong>Sistema de Información Proveedores del Instituto Colombiano de Bienestar Familiar</strong></td> " +
                            "<td>&nbsp;</td></tr><tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>  &nbsp;</td></tr> " +
                            "<tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>  &nbsp;</td></tr> " +
                            "<tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>  &nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td>El siguiente usuario:</td><td>&nbsp;</td></tr> " +
                            "<tr><td>  &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td></td><td>  &nbsp;</td></tr>   " +
                            "<tr><td>&nbsp;</td><td>" + pIdTipoIdentificacion + " " + pNumeroIdentificacion + " " + pNombre + "</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td>Requiere ser asociado a un consorcio o Unión Temporal y no ha sido validado</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td>  &nbsp;</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td>Agradecemos su gestión en realizar la validación lo antes posible</td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td></td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;</td><td>  </td></tr> " +
                            "<tr><td>&nbsp;</td><td>  </td><td>&nbsp;</td></tr> " +
                            "<tr><td>&nbsp;  </td><td>&nbsp;</td><td>&nbsp;</td></tr> " +
                            "<tr><td width='5%'>&nbsp;</td><td>&nbsp;</td><td width='5%'>&nbsp;</td></tr></tbody></table></td> " +
                            "<td width='10%' bgcolor='#81BA3D'>  &nbsp;</td></tr> " +
                            "<tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr> " +
                            "<tr><td colspan='3' align='center' bgcolor='#81BA3D'>Este correo electrónico fue enviado por un sistema automático, favor de no responderlo. </td></tr> " +
                            "<tr><td colspan='3' align='center' bgcolor='#81BA3D'>Si tienes alguna duda, puedes dirigirte a nuestra sección de   <a href='" + pLinkUrl + "' target='_blank'>Asistencia y Soporte.</a></td> " +
                            "</tr><tr><td colspan='3' align='center' bgcolor='#81BA3D'>&nbsp;</td></tr></tbody></table></body></html>";




        var mySmtpClient = new SmtpClient();

        object userState = myMailMessage;

        mySmtpClient.SendCompleted += this.SmtpClient_OnCompleted;

        try
        {
            mySmtpClient.SendAsync(myMailMessage, userState);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void SmtpClient_OnCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        if (e.Cancelled)
        {
            toolBar.MostrarMensajeError("El envio del correo de activación fue cancelado");
        }
        if (e.Error != null)
        {
            toolBar.MostrarMensajeError("Error: " + e.Error.Message.ToString());
        }
    }
    #endregion

    #region Documentos

    /// <summary>
    /// Busca Documentos de Tercero y llena grilla
    /// </summary>
    private void BuscarDocumentos(int pIdTercero, string pIdTipoPersona)
    {
        try
        {
            gvAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(pIdTercero, pIdTipoPersona, "");
            gvAdjuntos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAdjuntos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int rowIndex = gvAdjuntos.SelectedIndex;
        string strValue = gvAdjuntos.DataKeys[rowIndex].Value.ToString();
        ViewState["IdDocTercero"] = strValue;
    }

    /// <summary>
    /// Comando de renglón para la grilla Adjuntos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewLogo":

                    MemoryStream ArchivoImagen = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (ArchivoImagen != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", ArchivoImagen.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se logro obtener la imagen desde el servidor.");
                        return;
                    }

                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Comandos de renglón para la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;

        int vIdEntidad = Convert.ToInt32(GetSessionParameter("vIdEntidad"));
        string vIdTipoSector = GetSessionParameter("vIdTipoSector").ToString();
        string vTipoEntOfProv = GetSessionParameter("vTipoEntOfProv").ToString();
        string vTipoPersona = ddlIdTipoPersona.SelectedValue;

        BuscarDocumentosDatosBasicos(vIdEntidad, vTipoPersona, vIdTipoSector, vTipoEntOfProv);

    }
    protected void gvDocDatosBasicoProv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocDatosBasicoProv.SelectedRow);
    }

    /// <summary>
    /// Buscar Documentos
    /// </summary>
    private void BuscarDocumentosDatosBasicos(int vIdEntidad, string vTipoProveedor, string vTipoSector, string vTipoEntidad)
    {
        try
        {
            List<DocDatosBasicoProv> listDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, vTipoProveedor, vTipoSector, "", vTipoEntidad);
            List<DocDatosBasicoProv> documentoAMostrar = new List<DocDatosBasicoProv>();
            foreach (DocDatosBasicoProv doc in listDocs)
            {
                if (doc.NombreDocumento.Equals("Certificado de existencia y representación legal o de domicilio para entidades extranjeras"))
                {
                    documentoAMostrar.Add(doc);
                    break;
                }
            }

            gvDocDatosBasicoProv.DataSource = documentoAMostrar;
            gvDocDatosBasicoProv.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocDatosBasicoProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocDatosBasicoProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
    protected void ddlConfirmaCertificado_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlConfirmaCertificado.SelectedValue == "0")
        {
            Response.Write("<script>alert('No es posible asociarlo. El integrante debe actualizar el certificado de existencia y representación legal');" +
                         "window.location= '../Integrantes/List.aspx'</script>");
        }
    }
    protected void ddlConfirmaPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlConfirmaPersona.SelectedValue == "0")
        {
            Response.Write("<script>alert('No es posible asociarlo. El integrante debe actualizar la información del Representante Legal');" +
             "window.location= '../Integrantes/List.aspx'</script>");
        }
    }
    protected void ValidaNumerodeIntegrantes(int pIdEntidad)
    {
        try
        {
            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ValidaNumIntegrantes(pIdEntidad);

            hfIntegrantesReg.Value = Convert.ToString(vIntegrantes.IntegrantesReg);
            hfNumIntegrates.Value = Convert.ToString(vIntegrantes.NumIntegrantes);
            hfSumaPorcentajes.Value = Convert.ToString(vIntegrantes.PorcentajeParticipacion);
            //if (vIntegrantes.IdTipoPersona == 3)//si es consorcio
            //{
            //    //String.Format("{0:f2}",value);
            //    double cPorcentajeParticipacion = 100.0 / vIntegrantes.NumIntegrantes;
            //    txtPorcentajeParticipacion.Text = String.Format("{0:f2}", cPorcentajeParticipacion);
            //    txtPorcentajeParticipacion.Enabled = false;
            //}
            //else
            //{
            //    txtPorcentajeParticipacion.Enabled = true;
            //}
            txtPorcentajeParticipacion.Enabled = true;

            if (vIntegrantes.NumIntegrantes != vIntegrantes.IntegrantesReg)
            {
                int vIntegrantesfaltantes = vIntegrantes.NumIntegrantes - vIntegrantes.IntegrantesReg;
                toolBar.MostrarMensajeError("Hace falta asociar " + vIntegrantesfaltantes + " integrantes");
            }
            else
            {
                toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void imgBtnUpdate_Click(object sender, ImageClickEventArgs e)
    {
        ddlConfirmaCertificado.SelectedValue = "-1";
        ddlConfirmaPersona.SelectedValue = "-1";
        validarTerceroOProveedor();
    }
    protected void ddlIdTipoIdentificacionPersonaNatural_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!ddlIdTipoIdentificacionPersonaNatural.SelectedValue.Equals("-1"))
        {
            txtNumeroIdentificacion.Enabled = true;
            rfvNumeroIdentificacion.Enabled = true;
        }
        else
        {
            txtNumeroIdentificacion.Enabled = false;
            rfvNumeroIdentificacion.Enabled = false;
        }
        txtNumeroIdentificacion.Text = "";


    }
}
