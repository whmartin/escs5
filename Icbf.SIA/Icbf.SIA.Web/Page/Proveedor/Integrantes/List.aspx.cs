using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;
using System.IO;
/// <summary>
/// Página de consulta a través de filtros para la entidad Integrantes
/// </summary>
public partial class Page_Integrantes_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/Integrantes";
    ProveedorService vProveedorService = new ProveedorService();
    SIAService vRUBOService = new SIAService();
    

    protected void Page_PreInit(object sender, EventArgs e)
    {

        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvIntegrantes, GridViewSortExpression, true);
            ConsultarEstadoValidacion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            Icbf.Proveedor.Entity.EntidadProvOferente vProv = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            if (vProv.UsuarioCrea.Equals(GetSessionUser().NombreUsuario))
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvIntegrantes.PageSize = PageSize();
            gvIntegrantes.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Integrantes", SolutionPage.List.ToString());
            ucDatosProveedor1.IdEntidad = vIdEntidad.ToString();
            ValidaNumerodeIntegrantes(vIdEntidad);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Comando de renglón para la grilla gvIntegrantes
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvIntegrantes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                //case "viewArchivo":
                //    SetSessionParameter("Integrantes.IdIntegrante", e.CommandArgument);
                //    ManejoControles vManejoControles = new ManejoControles();
                //    vManejoControles.OpenWindowDescargaExperiencia(this, "ConsultarDocumento.aspx");

                //    break;
                case "viewArchivo":

                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        string path = "'../../../Page/Proveedor/Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
                case "eliminar":
                    Integrantes provInt= new Integrantes();
                    provInt.IdIntegrante = int.Parse(e.CommandArgument.ToString());
                    InformacionAudioria(provInt, this.PageName, vSolutionPage);
                    int vResultado = vProveedorService.EliminarIntegrantes(provInt);
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado >= 1)
                    {
                        toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                        SetSessionParameter("Integrantes.Eliminado", "1");
                        NavigateTo(SolutionPage.List);
                    }
                    break;

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvIntegrantes.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Integrantes.IdIntegrante", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvIntegrantes_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvIntegrantes.SelectedRow);
    }
    protected void gvIntegrantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvIntegrantes.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvIntegrantes_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));

            var myGridResults = vProveedorService.ConsultarIntegrantess(vIdEntidad);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Integrantes), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Integrantes, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Integrantes.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Integrantes.Eliminado");
            gvIntegrantes.EmptyDataText = "No se han asociado integrantes para este proveedor.";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ValidaNumerodeIntegrantes(int pIdEntidad)
    {
        try
        {
            Integrantes vIntegrantes = new Integrantes();
            vIntegrantes = vProveedorService.ValidaNumIntegrantes(pIdEntidad);

            if (vIntegrantes.NumIntegrantes != 0)
            {
                if (vIntegrantes.NumIntegrantes == vIntegrantes.IntegrantesReg)
                {
                    toolBar.eventoNuevo -= new ToolBarDelegate(btnNuevo_Click);
                }
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ConsultarEstadoValidacion()
    {
        int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
        ValidacionIntegrantesEntidad validaInt = vProveedorService.Consultar_ValidacionIntegrantesEntidad(vIdEntidad);
        txtEdoValidacion.Text = validaInt.EstadoValidacionIntegrantes;
    }

    protected bool verOpcionEliminar()
    {
        int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
        ValidacionIntegrantesEntidad validaInt = vProveedorService.Consultar_ValidacionIntegrantesEntidad(vIdEntidad);
        EntidadProvOferente vProv = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
        if (vProv.UsuarioCrea.Equals(GetSessionUser().NombreUsuario))
        {
            if (validaInt.EstadoValidacionIntegrantes.Equals("REGISTRADO") || validaInt.EstadoValidacionIntegrantes.Equals("POR AJUSTAR") || validaInt.EstadoValidacionIntegrantes.Equals("POR VALIDAR"))
                return true;
            else
                return false;
        }
        else
            return false;
    }

    protected bool verDocumento(object idTipoPersona)
    {
        int variable = 0;
        variable = Convert.ToInt32(idTipoPersona);
        if (variable == 2)
            return true;
        else
            return false;
    }

}
