<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Integrantes_List" %>

<%@ Register Src="../UserControl/ucDatosProveedor.ascx" TagName="ucDatosProveedor"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    </asp:Panel>
    <table width="90%" align="center">
            <tr class="rowB">
                <td width="50%;">
                    <label style="width: 70%">
                        Estado validaci&oacute;n documental</label>
                </td>
                <td width="50%;">
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtEdoValidacion" Enabled="false" Width="50%"></asp:TextBox>
                </td>
                <td>
                
                <td />
            </tr>
        </table>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvIntegrantes" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdIntegrante" CellPadding="0" Height="16px"
                        OnSorting="gvIntegrantes_Sorting" AllowSorting="True" OnPageIndexChanging="gvIntegrantes_PageIndexChanging"
                        OnSelectedIndexChanged="gvIntegrantes_SelectedIndexChanged" OnRowCommand="gvIntegrantes_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Integrante" DataField="Integrante" SortExpression="Integrante" />
                            <asp:BoundField HeaderText="Tipo de Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Porcentaje de Participación" DataField="PorcentajeParticipacion"
                                SortExpression="PorcentajeParticipacion" />
                            <asp:TemplateField HeaderText="Documentos">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblDocumentos" runat="server" CommandArgument='<%#Eval("LinkDocumento")%>'
                                        CommandName="viewArchivo" Visible='<%# verDocumento(Eval("IdTipoPersona")) %>'>
                                                <img src="../../../Image/btn/list.png" alt="Documentos" height="16px" width="16px" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Eliminar">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblEliminar" runat="server" CommandArgument='<%#Eval("IdIntegrante")%>'
                                        CommandName="eliminar"  Visible='<%# verOpcionEliminar() %>' OnClientClick="return confirm('&#191;Est&#225; seguro de que desea eliminar el registro?')">
                                                <img src="../../../Image/btn/delete.gif" alt="Eliminar" height="16px" width="16px" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
