<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Integrantes_Add"
    MaintainScrollPositionOnPostback="true" Async="true" %>

<%@ Register Src="~/General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="UcFecha" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">

        function ValidaValorPorcentaje(source, args) {
            var percent = parseFloat((document.getElementById('<%= txtPorcentajeParticipacion.ClientID %>').value).replace(",", "."));
            if (percent >= parseFloat("0.01") & percent <= parseFloat("99.99")) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }

        function ValidaPorcentajeTotal(source, args) {
            var sumPorcentaje = document.getElementById('<%= hfSumaPorcentajes.ClientID %>').value;
            sumPorcentaje = sumPorcentaje.replace(",",".")
            var porcentajeConsultado = document.getElementById('<%= hfPorcentajeConsultado.ClientID %>').value;
            porcentajeConsultado = porcentajeConsultado.replace(",", ".")
            var percent = document.getElementById('<%= txtPorcentajeParticipacion.ClientID %>').value;
            percent = percent.replace(",", ".")

            var nuevoPorcentaje = parseFloat(((parseFloat(sumPorcentaje) - parseFloat(porcentajeConsultado)) + parseFloat(percent)));
            nuevoPorcentaje = nuevoPorcentaje.toFixed(2)
            if (nuevoPorcentaje > 100.00) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }


        function funValidaNumIdentificacion(source, args) {
            args.IsValid = validaNumIdentificacion(args.Value);
        }

        function validaNumIdentificacion(numero) {
            var tipodoc = document.getElementById('<%= ddlIdTipoIdentificacionPersonaNatural.ClientID %>').value;
            if (tipodoc == "1") {
                if (numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6 && numero.length != 7 && numero.length != 8 && numero.length != 10 && numero.length != 11)
                    return false;
                else
                    return true;
            }
            if (tipodoc == "2") {
                if (numero.length != 1 && numero.length != 2 && numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6)
                    return false;
                else
                    return true;
            }
            if (tipodoc == "7") {
                if (numero.length != 9)
                    return false;
                else
                    return true;
            }
            return true;
        }

        
    </script>
    <asp:HiddenField ID="hfIdIntegrante" runat="server" />
    <asp:HiddenField ID="hfSumaPorcentajes" runat="server" />
    <asp:HiddenField ID="hfIntegrantesReg" runat="server" />
    <asp:HiddenField ID="hfNumIntegrates" runat="server" />
    <asp:HiddenField ID="hfPorcentajeConsultado" runat="server" Value="0" />
    <asp:Panel ID="pnlIntegrantes" runat="server">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Tipo de Persona o Asociaci&oacute;n *
                    <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoPersona" ControlToValidate="ddlIdTipoPersona"
                        SetFocusOnError="true" ErrorMessage="Seleccione un tipo de persona o asociaci&oacute;n" InitialValue="-1"
                        Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
<%--                    <asp:CompareValidator runat="server" ID="cvIdTipoPersona" ControlToValidate="ddlIdTipoPersona"
                        SetFocusOnError="true" ErrorMessage="Seleccione un tipo de persona o asociaci&oacute;n"
                        ValidationGroup="btnGuardar" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1"
                        Display="Dynamic"></asp:CompareValidator>--%>
                </td>
                <td>
                    Tipo de Identificaci&oacute;n *
                    <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoIdentificacionPersonaNatural"
                        ControlToValidate="ddlIdTipoIdentificacionPersonaNatural" SetFocusOnError="true"
                        ErrorMessage="Seleccione un tipo de identificaci&oacute;n" Display="Dynamic" InitialValue="-1"
                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
<%--                    <asp:CompareValidator runat="server" ID="cvIdTipoIdentificacionPersonaNatural" ControlToValidate="ddlIdTipoIdentificacionPersonaNatural"
                        SetFocusOnError="true" ErrorMessage="Seleccione un tipo de identificaci&oacute;n"
                        ValidationGroup="btnGuardar" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1"
                        Display="Dynamic"></asp:CompareValidator>--%>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdTipoPersona" AutoPostBack="true" TabIndex="1"
                        OnSelectedIndexChanged="ddlIdTipoPersona_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdTipoIdentificacionPersonaNatural" TabIndex="2"
                        Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlIdTipoIdentificacionPersonaNatural_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Número de Identificaci&oacute;n *
                    <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacion" ControlToValidate="txtNumeroIdentificacion"
                        SetFocusOnError="true" ErrorMessage="Ingrese n&uacute;mero de identificaci&oacute;n"
                        Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidatortxtIdentificacion" runat="server" ControlToValidate="txtNumeroIdentificacion"
                        SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                        ClientValidationFunction="funValidaNumIdentificacion">El n&uacute;mero de identificaci&oacute;n no es v&aacute;lido</asp:CustomValidator>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblDV" Visible="false">DV</asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="11" TabIndex="3"
                        Enabled="false"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                        FilterType="Numbers" ValidChars="" />
                    <asp:ImageButton ID="imgBtnUpdate" runat="server" ImageUrl="~/Image/btn/refresh.png"
                        OnClick="imgBtnUpdate_Click" ValidationGroup="btnGuardar" TabIndex="4" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDV" Enabled="false" Visible="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlTercero" Visible="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Primer Nombre
                </td>
                <td>
                    Segundo Nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Enabled="false" MaxLength="128"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Enabled="false" MaxLength="128"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Apellido
                </td>
                <td>
                    Segundo Apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" MaxLength="128"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" MaxLength="128"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha de nacimiento
                </td>
                <td>
                    Sexo
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha ID="cuFechaNac" runat="server" Enabled="false" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSexo" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <asp:Label runat="server" ID="lblRazon" Visible="false">Raz&oacute;n Social</asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRazon" Enabled="false" Visible="false" MaxLength="128" Width="100%"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlRepresentanteLegal" Visible="false">
        <table width="90%" align="center">
            <tr>
                <td colspan="2">
                    <h3 class="lbBloque">
                        <asp:Label ID="lblRepresentante" runat="server" Text="Representante Legal"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Tipo identificación
                </td>
                <td>
                    Número de Identificación
                </td>
            </tr>
            <tr class="rowA">
                <td style="width: 50%">
                    <asp:DropDownList ID="ddlTipoDocumentoRepr" runat="server" Enabled="False" Width="300px">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hfRepreLegal" runat="server" />
                </td>
                <td style="width: 50%">
                    <asp:TextBox ID="txtIdentificacionRepr" runat="server" Enabled="False" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftIdentificacionRepr" runat="server" FilterType="Numbers"
                        TargetControlID="txtIdentificacionRepr" ValidChars="/áéíóúÁÉÍÓÚñÑ.,@_():;1234567890" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Nombre
                </td>
                <td>
                    Segundo Nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="txtPrimerNombreRepr" runat="server" Enabled="False" MaxLength="256"
                        Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtSegundoNombreRepr" runat="server" Enabled="False" MaxLength="256"
                        Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Apellido
                </td>
                <td>
                    Segundo Apellido
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox ID="TxtPrimerApellidoRepr" runat="server" Enabled="False" MaxLength="256"
                        Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TxtSegundoApellidoRepr" runat="server" Enabled="False" MaxLength="256"
                        Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha de nacimiento
                </td>
                <td>
                    Sexo
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" width="50%;">
                    <UcFecha:fecha ID="cuFechaNacRepLegal" runat="server" Enabled="false" Requerid="false" />
                </td>
                <td class="Cell" width="50%;">
                    <asp:DropDownList runat="server" ID="ddlSexoRepLegal" Enabled="false">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentos" Visible="false">
        <table width="90%" align="center">
            <tr>
                <td colspan="2">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label2" runat="server" Text="Documentos"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px" DataKeyName="IDDOCADJUNTO"
                        OnRowCommand="gvAdjuntos_RowCommand" OnSelectedIndexChanged="gvAdjuntos_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreTipoDocumento" />
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="viewLogo" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                        CommandArgument='<%#Eval("LinkDocumento")%>' Height="16px" Width="16px" ToolTip="Detalle"
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDocumentosJuridica" Visible="false">
        <table width="90%" align="center">
            <tr>
                <td colspan="2">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label3" runat="server" Text="Documentos"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDocDatosBasicoProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocDatosBasicoProv_RowCommand"
                        OnPageIndexChanging="gvDocDatosBasicoProv_PageIndexChanging" OnSelectedIndexChanged="gvDocDatosBasicoProv_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre del Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar">
                                    </asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlConfirma" Visible="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Confirma que el certificado de existencia y representación legal esta vigente (inferior
                    o igual a 30 días) *
                    <asp:RequiredFieldValidator ID="rvConfirmaCertificado" runat="server" ControlToValidate="ddlConfirmaCertificado"
                        ErrorMessage="Seleccione un dato" SetFocusOnError="True" Enabled="False" InitialValue="-1"
                        ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                </td>
                <td>
                    Confirma que esta persona representa legalmente la Entidad *
                    <asp:RequiredFieldValidator ID="rvConfirmaPersona" runat="server" ControlToValidate="ddlConfirmaPersona"
                        ErrorMessage="Seleccione un dato" SetFocusOnError="True" Enabled="False" InitialValue="-1"
                        ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlConfirmaCertificado" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlConfirmaCertificado_SelectedIndexChanged" TabIndex="5">
                        <asp:ListItem Text=" " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlConfirmaPersona" AutoPostBack="true" OnSelectedIndexChanged="ddlConfirmaPersona_SelectedIndexChanged"
                        TabIndex="6">
                        <asp:ListItem Text=" " Value="-1"></asp:ListItem>
                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlPorcentaje" Visible="false">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Porcentaje de Participación *
                    <asp:RequiredFieldValidator runat="server" ID="rfvPorcentajeParticipacion" ControlToValidate="txtPorcentajeParticipacion"
                        SetFocusOnError="true" ErrorMessage="Registre el porcentaje de participaci&oacute;n para este integrante"
                        Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvPorcentajeParticipacion" runat="server" ErrorMessage="El porcentaje de participaci&oacute;n del integrante debe ser mayor a 0,01 y menor a 99,99 inténtelo nuevamente"
                        SetFocusOnError="true" ClientValidationFunction="ValidaValorPorcentaje" ValidationGroup="btnGuardar"
                        ControlToValidate="txtPorcentajeParticipacion" Display="Dynamic" ForeColor="Red"></asp:CustomValidator>
                        <br />
                    <asp:CustomValidator ID="cvPorcentajeParticipacionSumas" runat="server" ErrorMessage="La distribución de participación ha superado el 100%, no es posible asociar con esta participación"
                        SetFocusOnError="true" ClientValidationFunction="ValidaPorcentajeTotal" ValidationGroup="btnGuardar"
                        ControlToValidate="txtPorcentajeParticipacion" Display="Dynamic" ForeColor="Red"></asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td align="left">
                    <div style="width: 8%">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 75%">
                                    <Ajax:FilteredTextBoxExtender ID="ftetxtPorcentajeParticipacion" runat="server" TargetControlID="txtPorcentajeParticipacion"
                                        FilterType="Numbers,Custom" ValidChars=",." />
                                    <asp:TextBox runat="server" ID="txtPorcentajeParticipacion" MaxLength="21" Enabled="false"
                                        TabIndex="7" Width="90%"></asp:TextBox>
                                </td>
                                <td style="width: 25%">
                                    %
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:CompareValidator ID="compValDecimal" runat="server" ControlToValidate="txtPorcentajeParticipacion"
                        Type="Double" Operator="DataTypeCheck" EnableClientScript="true" Display="Dynamic" Font-Bold="true"
                        ErrorMessage="Formato no válido. Utilice ',' como separador decimal." ValidationGroup="btnGuardar" ForeColor="Red"></asp:CompareValidator>
                </td>
            </tr>
        </table>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    <asp:Label runat="server" ID="lblFechaRegistro" Visible="false">
                Fecha Registro
                    </asp:Label>
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRegistradoPor" Visible="false">
                Registrado Por
                    </asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <UcFecha:fecha ID="ucFechaRegistro" runat="server" Visible="false" Enabled="false"
                        Requerid="false" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRegistradoPor" Visible="false" Enabled="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
