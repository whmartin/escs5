<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ContactoEntidad_List" %>
<%@ Register src="../UserControl/ucDatosProveedor.ascx" tagname="ucDatosProveedor" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <uc1:ucDatosProveedor ID="ucDatosProveedor1" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista" Width="1000px" ScrollBars="Horizontal">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContactoEntidad" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContacto" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContactoEntidad_PageIndexChanging" OnSelectedIndexChanged="gvContactoEntidad_SelectedIndexChanged"
                        AllowSorting="true" OnSorting="gvContactoEntidad_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Contacto" DataField="NombreCompleto" SortExpression="NombreCompleto" />
                            <asp:BoundField HeaderText="Cargo" DataField="Cargo"  SortExpression="Cargo" />
                            <asp:BoundField HeaderText="Celular" DataField="Celular"  SortExpression="Celular" />
                            <asp:BoundField HeaderText="Teléfono" DataField="TelefonoCompleto" SortExpression="TelefonoCompleto" />
                            <asp:BoundField HeaderText="Correo electrónico" DataField="Email" SortExpression="Email" />
                            <asp:BoundField HeaderText="Registrado Por" DataField="UsuarioCrea" SortExpression="UsuarioCrea" />
                            <asp:BoundField HeaderText="Principal o sucursal" DataField="Sucursal" SortExpression="Sucursal" />
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("Estado") ? "Activo" : "Inactivo") %>'></asp:Label>
                                </ItemTemplate> 
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                        No se encontraron contactos para este proveedor.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
