using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de contacto de la entidad
/// </summary>
public partial class Page_ContactoEntidad_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/ContactoEntidad";
    ProveedorService vProveedorService = new ProveedorService();
    bool _sorted = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int ?vIdEntidad = null;
            Decimal ?vNumeroIdentificacion = null;
            String vPrimerNombre = null;
            String vSegundoNombre = null;
            String vPrimerApellido = null;
            String vSegundoApellido = null;
            String vDependencia = null;
            String vEmail = null;
            Boolean? vEstado = null;
            

            if (GetSessionParameter("EntidadProvOferente.IdEntidad") != null)
                vIdEntidad = Int32.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            gvContactoEntidad.DataSource = vProveedorService.ConsultarContactoEntidads(vIdEntidad, vNumeroIdentificacion, vPrimerNombre, vSegundoNombre, vPrimerApellido, vSegundoApellido, vDependencia, vEmail, vEstado);
            SetSessionParameter("gvContactoEntidad.DataSource", gvContactoEntidad.DataSource);
            gvContactoEntidad.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvContactoEntidad.PageSize = PageSize();
            gvContactoEntidad.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Datos de Contacto", SolutionPage.List.ToString());
            ucDatosProveedor1.IdEntidad = GetSessionParameter("EntidadProvOferente.IdEntidad").ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContactoEntidad.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ContactoEntidad.IdContacto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContactoEntidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContactoEntidad.SelectedRow);
    }
    protected void gvContactoEntidad_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContactoEntidad.PageIndex = e.NewPageIndex;
        Buscar();
    }


    #region Ordenar Grilla (Revisar si se generaliza)

    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<ContactoEntidad> SortList(List<ContactoEntidad> data, bool isPageIndexChanging)
    {
        List<ContactoEntidad> result = new List<ContactoEntidad>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }
    protected void gvContactoEntidad_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvContactoEntidad.PageIndex;

        if (GetSessionParameter("gvContactoEntidad.DataSource") != null) 
        {
            if (gvContactoEntidad.DataSource == null) { gvContactoEntidad.DataSource = (List<ContactoEntidad>)GetSessionParameter("gvContactoEntidad.DataSource"); }
            gvContactoEntidad.DataSource = SortList((List<ContactoEntidad>)gvContactoEntidad.DataSource, false);
            gvContactoEntidad.DataBind();   
        }
        gvContactoEntidad.PageIndex = pageIndex;
    }

    #endregion


    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ContactoEntidad.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ContactoEntidad.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/            
           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
