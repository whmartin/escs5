<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ContactoEntidad_Detail" %>

<%@ Register Src="../UserControl/ucTelefono.ascx" TagName="ucTelefono" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContacto" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td style="width: 50%">
                Correo electrónico *
            </td>
            <td style="width: 50%">
                Principal o sucursal *
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtEmail" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td class="Cell" width="50%;">
                <asp:DropDownList ID="ddlSucursal" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Nombre *
            </td>
            <td>
                Segundo Nombre
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtPrimerNombre"  Width="80%" Enabled="false"></asp:TextBox>
            </td>
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtSegundoNombre" Width="80%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Apellido *
            </td>
            <td>
                Segundo Apellido *
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtPrimerApellido" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtSegundoApellido" Enabled="false" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Celular *
            </td>
            <td>
                Teléfono *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCelular" MaxLength="10" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <uc2:ucTelefono ID="ucTelefono1" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Cargo o Nivel *
            </td>
            <td>
                Área o dependencia *
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%;">
                <asp:DropDownList ID="ddlTipoCargoEntidad" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtDependencia" Width="80%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <%--<tr class="rowB">
            <td>
                Tipo Documento de Identificación *
            </td>
            <td>
                Número Identificación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoDocIdentificacion" Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Activo" Value="True" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Registro
            </td>
            <td>
                Registrado Por
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistro" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegistradorPor" Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
