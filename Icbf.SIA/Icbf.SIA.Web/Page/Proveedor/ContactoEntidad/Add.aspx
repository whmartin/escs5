<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ContactoEntidad_Add" %>

<%@ Register Src="../UserControl/ucTelefono.ascx" TagName="ucTelefono" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContacto" runat="server" />
    <script type="text/javascript" language="javascript">
        function funValidaNombreObligatorio(source, args) {
            args.IsValid = EsNombreValido(args.Value);
        }

        function funValidaNombreOpcional(source, args) {
            if (args.Value.length > 0) {
                args.IsValid = EsNombreValido(args.Value);
            } else {
                args.IsValid = true;
            }
        }

        function EsNombreValido(nombre) {
            if (nombre.length > 2) {
                return true;
            } else if (nombre.length == 2) {
                var car1 = nombre.charAt(0);
                var car2 = nombre.charAt(1);

                if (car1 != car2) {
                    return true;
                }
            }
            return false;
        }


        function NombreSinEspacios(source, args) {
            args.IsValid = fnNombreSinEspacios(args.Value);
        }

        function fnNombreSinEspacios(nombre) {
            if (nombre.length > 0) {
                if (nombre.indexOf(' ') > -1) {
                    return false;
                }
                if (nombre.indexOf(' ') == 0) {
                    return false;
                }
                if (nombre.lastIndexOf(' ') == nombre.length - 1) {
                    //alert(nombre.length);
                    return false;
                }

                return true;
            }
            return false;
        }

        function funValidaIndicativo(source, args) {
            var $telefono = $('#cphCont_ucTelefono1_TxtTelefono');
            if (args.Value > 0) {
                args.IsValid = true;
            } else {
                source.innerText = "Indicativo debe tener un valor diferente de cero";
                args.IsValid = false;
            }
        }

        function funValidaIndicativoExisteTelefono(source, args) {
            var $telefono = $('#cphCont_ucTelefono1_TxtTelefono');

            if (args.Value.length > 0) {

                if ($telefono.val().length == 0) {
                    source.innerText = "Registre un teléfono";
                    args.IsValid = false;
                } else {
                    args.IsValid = true;
                }

            } else {
                args.IsValid = false;
            }
        }

        function funValidaExtensionExisteTelefono(source, args) {
            var $telefono = $('#cphCont_ucTelefono1_TxtTelefono');

            if (args.Value.length > 0) {

                if ($telefono.val().length == 0) {
                    source.innerText = "Registre un teléfono";
                    args.IsValid = false;
                } else {
                    args.IsValid = true;
                }

            } else {
                args.IsValid = false;
            }
        }

        function funValidaTelefono(source, args) {
            var $indicativo = $('#cphCont_ucTelefono1_TxtIndicativo');

            if (args.Value.length > 0) {
                if (args.Value.length == 7) {
                    if ($indicativo.val().length == 0) {
                        source.innerText = "Registre un indicativo";
                        args.IsValid = false;
                    } else {
                        args.IsValid = true;
                    }
                }
                else {
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        function ValidaTelefonoConIndicativo(obj, decimalPlaces, EntPlaces, e) {
            var temp = obj.value;

            //caso: No permite punto al inicio
            if (temp.indexOf('.') == 0) {
                obj.value = "";
                return true;
            }
            //Caso:permite un digito entero
            if (EntPlaces != 0) {
                if (temp.indexOf('.') < 0) {//temp.indexOf('\u2022') > 0) {

                    temp = temp.substring(0, EntPlaces);
                    obj.value = temp;

                    return false;
                }
            }
            //Caso: decimales
            if (decimalPlaces != 0) {
                if (temp.indexOf('.') > 0) {
                    var parteIzquierdaPPto = temp.substring(0, temp.indexOf('.') + 1)
                    var parteDerecha = temp.substring(temp.indexOf('.') + 1);

                    if (parteDerecha.indexOf('.') > -1) {
                        //Mas de un punto
                        temp = temp.substring(0, temp.indexOf('.') + parteDerecha.indexOf('.') + 1);
                    }
                    //Caso: n decimales
                    temp = temp.substring(0, temp.indexOf('.') + decimalPlaces + 1);
                }
            }
            obj.value = temp;
        }


        function funValidaInterespacios(source, args) {
            if (args.Value.length > 0) {
                args.IsValid = ValidaInterespacios(args.Value);
            } else {
                args.IsValid = true;
            }
        }

        function ValidaInterespacios(nombre) {

            if (nombre.indexOf(' ') > -1)
                return false;
            if (nombre.length == 2) {
                var car1 = nombre.charAt(0);
                var car2 = nombre.charAt(1);

                if (car1 != car2) {
                    return true;
                }
            } else if (nombre.length >= 3) {

                var cara1 = nombre.charAt(0);
                var cara2 = nombre.charAt(1);
                var cara3 = nombre.charAt(2);



                if (cara1 != cara2 && cara2 != cara3) {
                    return true;
                }

            }
            return false;
        }


    </script>
    <script runat="server">
        protected void txtEmail_Validate(object sender, ServerValidateEventArgs e)
        {
            CustomValidator uxSender = (CustomValidator)sender;
            e.IsValid = false;
        }
    </script>

    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>

    <table width="90%" align="center">
        <asp:Panel ID="pnlEstado" runat="server">
            <tr class="rowA">
                <td style="width: 50%">
                    Correo electrónico *
                    <asp:RequiredFieldValidator runat="server" ID="rfvEmail" ControlToValidate="txtEmail"
                        SetFocusOnError="true" ErrorMessage="Registre un correo electrónico válido" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revEmail" Display="Dynamic" ValidationGroup="btnGuardar"
                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ForeColor="Red" ErrorMessage="Correo electrónico no tiene la estructura valida">
                    </asp:RegularExpressionValidator>
                    <asp:CustomValidator runat="server" Text="" Display="Dynamic" SetFocusOnError="true"
                        ValidationGroup="btnGuardar" EnableClientScript="false" OnServerValidate="txtEmail_Validate"
                        ID="cvEmail" ErrorMessage="Correo electrónico ya existe, ingrese un nuevo correo"
                        ControlToValidate="txtEmail">
                    </asp:CustomValidator>
                </td>
                <td style="width: 50%">
                    Principal o sucursal *
                    <asp:RequiredFieldValidator runat="server" ID="rfvSucursal" ControlToValidate="ddlSucursal"
                        SetFocusOnError="true" ErrorMessage="Seleccione principal o sucursal" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtEmail" Width="80%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteEmail" runat="server" TargetControlID="txtEmail"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-@._" />
                </td>
                <td class="Cell" width="50%;">
                    <asp:DropDownList ID="ddlSucursal" runat="server">
                    </asp:DropDownList>
                    <asp:ImageButton ID="btnRefrescarSucursal" runat="server" OnClick="OnClick_RefrescarSucursal"
                        ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Refrescar" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    Primer Nombre *
                    <asp:RequiredFieldValidator runat="server" ID="rfvPrimerNombre" ControlToValidate="txtPrimerNombre"
                        SetFocusOnError="true" ErrorMessage="Registre el  primer nombre" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CustomValidator runat="server" ID="cvPrimerNombre" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtPrimerNombre" ClientValidationFunction="funValidaNombreObligatorio"
                        ErrorMessage="Registre un nombre válido">
                    </asp:CustomValidator>
                    <asp:CustomValidator runat="server" ID="cvNombreSinExpasios" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtPrimerNombre" ClientValidationFunction="NombreSinEspacios"
                        ErrorMessage="No se permiten espacios en blanco en el primer nombre">
                    </asp:CustomValidator>
                    <%--<asp:CustomValidator runat="server" ID="cvSinInterespacios" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtPrimerNombre" ClientValidationFunction="funValidaInterespacios"
                        ErrorMessage="Registre un nombre válido">
                    </asp:CustomValidator>--%>
                </td>
                <td>
                    Segundo Nombre
                    <asp:CustomValidator runat="server" ID="cvSegundoNombre" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtSegundoNombre" ClientValidationFunction="funValidaNombreOpcional"
                        ErrorMessage="Registre un nombre válido">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtPrimerNombre" Width="80%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtSegundoNombre" Width="80%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    Primer Apellido *
                    <asp:RequiredFieldValidator runat="server" ID="rfvPrimerApellido" ControlToValidate="txtPrimerApellido"
                        SetFocusOnError="true" ErrorMessage="Registre el  primer apellido" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CustomValidator runat="server" ID="cvPrimerApellido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtPrimerApellido" ClientValidationFunction="funValidaNombreObligatorio"
                        ErrorMessage="Registre un apellido válido">
                    </asp:CustomValidator>
                    <asp:CustomValidator runat="server" ID="cvPrimerApellidoEspacio" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtPrimerApellido" ClientValidationFunction="NombreSinEspacios"
                        ErrorMessage="No se permiten espacios en blanco en el primer apellido">
                    </asp:CustomValidator>
                </td>
                <td>
                    Segundo Apellido
                    <asp:CustomValidator runat="server" ID="cvSegundoApellido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" ControlToValidate="txtSegundoApellido" ClientValidationFunction="funValidaNombreOpcional"
                        ErrorMessage="Registre un apellido válido">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtPrimerApellido" Width="80%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtSegundoApellido" Width="80%" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    Celular *
                    <asp:RequiredFieldValidator runat="server" ID="rfvCelular" ControlToValidate="txtCelular"
                        SetFocusOnError="true" ErrorMessage="Registre un número de celular" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revCelular" Display="Dynamic"
                        ValidationGroup="btnGuardar" ControlToValidate="txtCelular" ValidationExpression="^[\s\S]{10,}$"
                        ForeColor="Red" ErrorMessage="Número de celular no válido">
                    </asp:RegularExpressionValidator>
                </td>
                <td>
                    Teléfono&nbsp;
                    <asp:CustomValidator runat="server" ID="rfvTelefono" ControlToValidate="ucTelefono1$TxtTelefono"
                        SetFocusOnError="true" ErrorMessage="Registre un número de teléfono válido" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red" ClientValidationFunction="funValidaTelefono">
                    </asp:CustomValidator>
                    <asp:CustomValidator runat="server" ID="rfvIndicativo" ControlToValidate="ucTelefono1$TxtIndicativo"
                        SetFocusOnError="true" ErrorMessage="Registre un indicativo" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red" ClientValidationFunction="funValidaIndicativo">
                    </asp:CustomValidator>
                    <asp:CustomValidator runat="server" ID="cvExisteTelefonoSiIndicativo" ControlToValidate="ucTelefono1$TxtIndicativo"
                        SetFocusOnError="true" ErrorMessage="Registre un teléfono" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red" ClientValidationFunction="funValidaIndicativoExisteTelefono">
                    </asp:CustomValidator>
                    <asp:CustomValidator runat="server" ID="cvExisteTelefonoSiExtension" ControlToValidate="ucTelefono1$TxtExtension"
                        SetFocusOnError="true" ErrorMessage="Registre un teléfono" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red" ClientValidationFunction="funValidaExtensionExisteTelefono">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtCelular" MaxLength="10" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="feCelular" runat="server" TargetControlID="txtCelular"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td class="Cell" width="50%;">
                    <uc2:ucTelefono ID="ucTelefono1" runat="server" />
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    Cargo o Nivel *
                    <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoCargoEntidad" ControlToValidate="ddlTipoCargoEntidad"
                        SetFocusOnError="true" ErrorMessage="Registre un cargo o nivel" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
                <td>
                    Área o dependencia *
                    <asp:RequiredFieldValidator runat="server" ID="rfvDependencia" ControlToValidate="txtDependencia"
                        SetFocusOnError="true" ErrorMessage="Registre una área o dependencia" Display="Dynamic"
                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" width="50%;">
                    <asp:DropDownList ID="ddlTipoCargoEntidad" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="Cell" width="50%;">
                    <asp:TextBox runat="server" ID="txtDependencia" MaxLength="50" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDependencia" runat="server" TargetControlID="txtDependencia"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
            <%--<tr class="rowB">
            <td>
                Tipo Documento de Identificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoDocIdentificacion" ControlToValidate="ddlTipoDocIdentificacion"
                 SetFocusOnError="true" ErrorMessage="Seleccione un Tipo de Identificación" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
            <td>
                Número Identificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacion" ControlToValidate="txtNumeroIdentificacion"
                 SetFocusOnError="true" ErrorMessage="Registre un número de identificación" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoDocIdentificacion"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"></asp:TextBox>
            </td>
        </tr>--%>
        </asp:Panel>
        <tr class="rowB">
            <td>
                Estado * <small>Si el contacto no tiene relación vigente con la compañía, usted puede
                    cambiar el estado a inactivo</small>
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Activo" Value="True" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivo" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Registro
            </td>
            <td>
                Registrado Por
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtFechaRegistro" Width="80%" Enabled="false"></asp:TextBox>
            </td>
            <td class="Cell" width="50%;">
                <asp:TextBox runat="server" ID="txtRegistradorPor" Width="80%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
