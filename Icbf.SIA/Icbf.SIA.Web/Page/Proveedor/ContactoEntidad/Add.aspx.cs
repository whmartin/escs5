using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Business;
using Icbf.Oferente.Service;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición de contactos de entidad
/// </summary>
public partial class Page_ContactoEntidad_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    string PageName = "Proveedor/ContactoEntidad";
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();

                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ContactoEntidad vContactoEntidad = new ContactoEntidad();
            OferenteService vOferenteService = new OferenteService();
            EntidadProvOferente vEntidadProvOferente = new ProveedorService().ConsultarEntidadProvOferente(Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));

            //Se registra el teléfono en TelTercero
            Icbf.Oferente.Entity.TelTerceros vTelTercero = new Icbf.Oferente.Entity.TelTerceros();
            vTelTercero.IdTercero = vEntidadProvOferente.IdTercero;
            vTelTercero.Movil = long.Parse(txtCelular.Text.Trim());
            if (ucTelefono1.Indicativo > 0) vTelTercero.IndicativoTelefono = ucTelefono1.Indicativo;
            //if (ucTelefono1.Telefono > 0) vTelTercero.NumeroTelefono = ucTelefono1.Telefono;
            vTelTercero.NumeroTelefono = ucTelefono1.Telefono;
            if (ucTelefono1.Extension > 0) vTelTercero.ExtensionTelefono = ucTelefono1.Extension;
            vTelTercero.UsuarioCrea = GetSessionUser().NombreUsuario;
            vOferenteService.InsertarTelTerceros(vTelTercero);


            //Se registra el contacto
            vContactoEntidad.IdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            vContactoEntidad.IdSucursal = Convert.ToInt32(ddlSucursal.SelectedValue);
            vContactoEntidad.IdTelContacto = vTelTercero.IdTelTercero;
            //vContactoEntidad.IdTipoDocIdentifica = Convert.ToInt32(ddlTipoDocIdentificacion.SelectedValue);
            vContactoEntidad.IdTipoCargoEntidad = Convert.ToInt32(ddlTipoCargoEntidad.SelectedValue);
            //vContactoEntidad.NumeroIdentificacion = Convert.ToDecimal(txtNumeroIdentificacion.Text);
            vContactoEntidad.PrimerNombre = Convert.ToString(txtPrimerNombre.Text.Trim());
            vContactoEntidad.SegundoNombre = Convert.ToString(txtSegundoNombre.Text.Trim());
            vContactoEntidad.PrimerApellido = Convert.ToString(txtPrimerApellido.Text.Trim());
            vContactoEntidad.SegundoApellido = Convert.ToString(txtSegundoApellido.Text.Trim());
            vContactoEntidad.Dependencia = Convert.ToString(txtDependencia.Text.Trim());
            vContactoEntidad.Email = Convert.ToString(txtEmail.Text.Trim());
            vContactoEntidad.Estado = Convert.ToBoolean(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vContactoEntidad.IdContacto = Convert.ToInt32(hfIdContacto.Value);
                vContactoEntidad.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContactoEntidad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarContactoEntidad(vContactoEntidad);
            }
            else
            {
                vContactoEntidad.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContactoEntidad, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarContactoEntidad(vContactoEntidad);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ContactoEntidad.IdContacto", vContactoEntidad.IdContacto);
                SetSessionParameter("ContactoEntidad.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            if (ex.Message.Contains("duplicado"))
            {
                toolBar.MostrarMensajeError("Correo electrónico ya existe, ingrese un nuevo correo");
            }
            else
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
           

            toolBar.EstablecerTitulos("Datos de contacto", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdContacto = Convert.ToInt32(GetSessionParameter("ContactoEntidad.IdContacto"));
            RemoveSessionParameter("ContactoEntidad.Id");

            ContactoEntidad vContactoEntidad = new ContactoEntidad();
            vContactoEntidad = vProveedorService.ConsultarContactoEntidad(vIdContacto);
            hfIdContacto.Value = vContactoEntidad.IdContacto.ToString();
            
            ddlSucursal.SelectedValue = vContactoEntidad.IdSucursal.ToString();
           
            ddlTipoCargoEntidad.SelectedValue = vContactoEntidad.IdTipoCargoEntidad.ToString();
            
            txtPrimerNombre.Text = vContactoEntidad.PrimerNombre;
            txtSegundoNombre.Text = vContactoEntidad.SegundoNombre;
            txtPrimerApellido.Text = vContactoEntidad.PrimerApellido;
            txtSegundoApellido.Text = vContactoEntidad.SegundoApellido;
            txtDependencia.Text = vContactoEntidad.Dependencia;
            txtEmail.Text = vContactoEntidad.Email;
            
            rblEstado.SelectedValue = vContactoEntidad.Estado.ToString();
            rblEstado.Enabled = true;
            //Datos del Registro
            txtFechaRegistro.Text = vContactoEntidad.FechaCrea.ToString("dd/MM/yyyy");
            txtRegistradorPor.Text = vContactoEntidad.UsuarioCrea;


            txtCelular.Text = vContactoEntidad.Celular;
            if (!string.IsNullOrEmpty(vContactoEntidad.IndicativoTelefono))
                ucTelefono1.Indicativo = Int32.Parse(vContactoEntidad.IndicativoTelefono);
            if(!string.IsNullOrEmpty(vContactoEntidad.NumeroTelefono))
                ucTelefono1.Telefono = long.Parse(vContactoEntidad.NumeroTelefono);
            if (!string.IsNullOrEmpty(vContactoEntidad.ExtensionTelefono))
                ucTelefono1.Extension = long.Parse(vContactoEntidad.ExtensionTelefono);

            //Se aplica regla de negocio para permitir editar
            AplicaReglaNegocioEdicion(vContactoEntidad.UsuarioCrea);

            //Solo se permite activar/desactivar para usuarios internos 
            if (vRuboService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                //es externo
            }
            else
            {
                //Si no es el mismo usuario que lo creó solo puede activar/desactivar
                if (!vContactoEntidad.UsuarioCrea.Equals(GetSessionUser().NombreUsuario))
                {
                    DeshabilitaControles();
                }
            }

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContactoEntidad.UsuarioCrea, vContactoEntidad.FechaCrea, vContactoEntidad.UsuarioModifica, vContactoEntidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            //rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";

            ManejoControles.LlenarSucursalEntidad(ddlSucursal, "-1", true, Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
            ManejoControles.LlenarTipoCargoEntidad(ddlTipoCargoEntidad, "-1", true);
            //ManejoControles.LlenarTipoDocIdentificacion(ddlTipoDocIdentificacion, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Aplica regla de negocio para habillitar controles en edición
    /// </summary>
    /// <param name="usuario"></param>
    private void AplicaReglaNegocioEdicion(string usuario)
    {
        //Si el usuario autenticado no es el mismo que registró el contacto no muestra habilitada la opción Editar.
        //Ó Se verifica si el usuario es interno o externo y aplica regla
        if (!GetSessionUser().NombreUsuario.Equals(usuario))
        {
            if (!vRuboService.ConsultarUsuario(usuario).Rol.Split(';').Contains("PROVEEDORES"))
            {
                pnlEstado.Enabled = false;
            }
        }
        else 
        {
            pnlEstado.Enabled = true;
        }
    }

    /// <summary>
    /// Deshabilita controles
    /// </summary>
    private void DeshabilitaControles()
    {
        try
        {
            ddlSucursal.Enabled = false;
            
            ddlTipoCargoEntidad.Enabled = false;
            
            txtPrimerNombre.Enabled = false;
            txtSegundoNombre.Enabled = false;
            txtPrimerApellido.Enabled = false;
            txtSegundoApellido.Enabled = false;
            txtDependencia.Enabled = false;
            txtEmail.Enabled = false;
            //rblEstado.Enabled = false;
            txtCelular.Enabled = false;
                       
            

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador del evento click del botón RefrescarSucursal para luego cargar valores de lista desplegable
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void OnClick_RefrescarSucursal(object sender, EventArgs e)
    {
        ManejoControles.LlenarSucursalEntidad(ddlSucursal, "-1", true, Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
        ManejoControles.LlenarTipoCargoEntidad(ddlTipoCargoEntidad, "-1", true);
    }

}
