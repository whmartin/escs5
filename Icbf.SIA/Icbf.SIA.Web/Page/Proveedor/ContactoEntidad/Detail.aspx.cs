using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página que despliega el detalle del registro de contacto entidad
/// </summary>
public partial class Page_ContactoEntidad_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/ContactoEntidad";
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ContactoEntidad.IdContacto", hfIdContacto.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento para el botón Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdContacto = Convert.ToInt32(GetSessionParameter("ContactoEntidad.IdContacto"));
            RemoveSessionParameter("ContactoEntidad.IdContacto");

            if (GetSessionParameter("ContactoEntidad.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("Registro almacenado en forma exitosa");
            RemoveSessionParameter("ContactoEntidad.Guardado");


            ContactoEntidad vContactoEntidad = new ContactoEntidad();
            vContactoEntidad = vProveedorService.ConsultarContactoEntidad(vIdContacto);
            hfIdContacto.Value = vContactoEntidad.IdContacto.ToString();
            ddlSucursal.SelectedValue = vContactoEntidad.IdSucursal.ToString();
                      
            //Se cargan los datos del Teléfono
            txtCelular.Text = vContactoEntidad.Celular;
            if (vContactoEntidad.IndicativoTelefono != null) ucTelefono1.Indicativo = Int32.Parse(vContactoEntidad.IndicativoTelefono);
            if (vContactoEntidad.NumeroTelefono != null) ucTelefono1.Telefono = long.Parse(vContactoEntidad.NumeroTelefono);
            if (vContactoEntidad.ExtensionTelefono != null) ucTelefono1.Extension = long.Parse(vContactoEntidad.ExtensionTelefono);
            ucTelefono1.Habilitar(false);

            
            ddlTipoCargoEntidad.SelectedValue = vContactoEntidad.IdTipoCargoEntidad.ToString();
            
            txtPrimerNombre.Text = vContactoEntidad.PrimerNombre;
            txtSegundoNombre.Text = vContactoEntidad.SegundoNombre;
            txtPrimerApellido.Text = vContactoEntidad.PrimerApellido;
            txtSegundoApellido.Text = vContactoEntidad.SegundoApellido;
            txtDependencia.Text = vContactoEntidad.Dependencia;
            txtEmail.Text = vContactoEntidad.Email;
            rblEstado.SelectedValue = vContactoEntidad.Estado.ToString();

            //Datos del Registro
            txtFechaRegistro.Text = vContactoEntidad.FechaCrea.ToString("dd/MM/yyyy");
            txtRegistradorPor.Text = vContactoEntidad.UsuarioCrea;

            ObtenerAuditoria(PageName, hfIdContacto.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContactoEntidad.UsuarioCrea, vContactoEntidad.FechaCrea, vContactoEntidad.UsuarioModifica, vContactoEntidad.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdContacto = Convert.ToInt32(hfIdContacto.Value);

            ContactoEntidad vContactoEntidad = new ContactoEntidad();
            vContactoEntidad = vProveedorService.ConsultarContactoEntidad(vIdContacto);
            InformacionAudioria(vContactoEntidad, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarContactoEntidad(vContactoEntidad);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ContactoEntidad.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Datos de contacto", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.SelectedValue = "-1";

            ManejoControles.LlenarSucursalEntidad(ddlSucursal, "-1", true, Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad")));
            ManejoControles.LlenarTipoCargoEntidad(ddlTipoCargoEntidad, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Aplica regla de negocio para colocar botones
    /// </summary>
    /// <param name="usuario"></param>
    private void AplicaReglaNegocioEdicion(string usuario)
    {
        //Si el usuario autenticado no es el mismo que registró el contacto no muestra habilitada la opción Editar.
        //Ó Se verifica si el usuario es interno o externo y aplica regla
        if (!GetSessionUser().NombreUsuario.Equals(usuario))
        {
            toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar -= new ToolBarDelegate(btnEliminar_Click);
        }
    }
}
