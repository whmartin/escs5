using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página que despliega el detalle del registro de TipoCiiu
/// </summary>
public partial class Page_TipoCiiu_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/TipoCiiu";
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }


    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoCiiu.IdTipoCiiu", hfIdTipoCiiu.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoCiiu = Convert.ToInt32(GetSessionParameter("TipoCiiu.IdTipoCiiu"));
            RemoveSessionParameter("TipoCiiu.IdTipoCiiu");

            if (GetSessionParameter("TipoCiiu.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoCiiu.Guardado");


            TipoCiiu vTipoCiiu = new TipoCiiu();
            vTipoCiiu = vProveedorService.ConsultarTipoCiiu(vIdTipoCiiu);
            hfIdTipoCiiu.Value = vTipoCiiu.IdTipoCiiu.ToString();
            txtCodigoCiiu.Text = vTipoCiiu.CodigoCiiu;
            txtDescripcion.Text = vTipoCiiu.Descripcion;
            rblEstado.SelectedValue = vTipoCiiu.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdTipoCiiu.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoCiiu.UsuarioCrea, vTipoCiiu.FechaCrea, vTipoCiiu.UsuarioModifica, vTipoCiiu.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoCiiu = Convert.ToInt32(hfIdTipoCiiu.Value);

            TipoCiiu vTipoCiiu = new TipoCiiu();
            vTipoCiiu = vProveedorService.ConsultarTipoCiiu(vIdTipoCiiu);
            InformacionAudioria(vTipoCiiu, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarTipoCiiu(vTipoCiiu);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoCiiu.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Tipo Ciiu", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
