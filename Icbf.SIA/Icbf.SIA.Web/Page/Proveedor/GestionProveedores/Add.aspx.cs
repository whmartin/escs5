using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.IO;
using EntidadProvOferente = Icbf.Proveedor.Entity.EntidadProvOferente;
using AjaxControlToolkit;
using System.Configuration;
using System.Data;
using Icbf.Seguridad.Service;


/// <summary>
/// Página de registro y edición de proveedores
/// </summary>
public partial class Page_EntidadProvOferente_Add : GeneralWeb
{

    #region Propiedades
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vRUBOService = new SIAService();
    string PageName = "Proveedor/GestionProveedores";
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
    static EntidadProvOferente vEntidadProvOferenteAEditar = new EntidadProvOferente();
    static InfoAdminEntidad vInfoAdmin = new InfoAdminEntidad();
    DataTable dtUNSPSC;
    List<CodigoUNSPSCProveedor> vUNSPSCActual = new List<CodigoUNSPSCProveedor>();
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        MaintainScrollPositionOnPostBack = true;
        ScriptManager.GetCurrent(this).RegisterPostBackControl(CalFechaConstitucion);
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                LimpiarErrore();
                CargarDatosIniciales();
                LimpiarErrore();

                if (Request.QueryString["oP"] == "E")
                {
                    CargarRegistro();
                    LimpiarErrore();
                }
                else
                {
                    CrearEstructuraUNSPSC();
                }
            }
            else
            {
                TxtActividadCiiuPrin.Text = hfDesActividadCiiuPrin.Value;
                TxtActividadCiiuSegu.Text = hfDesActividadCiiuSegu.Value;

                // Se llena grilla de codigos UNSPSC
                // Luz Angela Borda
                // Mayo 13 de 2015

                if (GetSessionParameter("Proveedor.dtUNSPSC") != null && GetSessionParameter("Proveedor.dtUNSPSC") != string.Empty)
                {
                    if (Request.QueryString["oP"] == "E")
                    {
                        DataTable dtCodUNSPSC = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");
                        foreach (DataRow cod in dtCodUNSPSC.Rows)
                        {
                            if (!bool.Parse(cod["existe"].ToString()))
                            {
                                CodigoUNSPSCProveedor vCodUNSPSCProveedor = new CodigoUNSPSCProveedor();
                                int vIdTercero = Convert.ToInt32(GetSessionParameter("Proveedor.IdTercero"));

                                vCodUNSPSCProveedor.IDTERCERO = vIdTercero;
                                vCodUNSPSCProveedor.IdTipoCodUNSPSC = int.Parse(cod["idTipoCodUNSPSC"].ToString());
                                vCodUNSPSCProveedor.UsuarioCrea = GetSessionUser().NombreUsuario;
                                vProveedorService.InsertarCodUNSPSCProveedor(vCodUNSPSCProveedor);
                                cod["existe"] = true;
                            }
                        }
                    }
                    gvCodigoUNSPSC.DataSource = GetSessionParameter("Proveedor.dtUNSPSC");
                    gvCodigoUNSPSC.DataBind();
                }

            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnGuardarClick(object sender, EventArgs e)
    {

        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnNuevoClick(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnBuscarClick(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Manejador del evento click para el botón Retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnRetornarClick(object sender, EventArgs e)
    {
        if (Request.QueryString["oP"] == "E")
        {


            SetSessionParameter("EntidadProvOferente.IdEntidad", hfIdEntidad.Value);
            NavigateTo("~/Page/" + PageName + "/DetailDatosBasicos.aspx");

        }
        else
        {
            NavigateTo("~/Page/" + PageName + "/List.aspx");
        }


    }
    protected void DdlDepartamentoConstituidaSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipioConstituida();
        CargarDepartamentoDireccionComercial();
        //CargarClasedeEntidad();
    }
    protected void DdlSectorSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDatosSector();
        llenarddlTipoEntidad();
        if (ddlSector.SelectedValue == "2") // sector Publico
        {
            ddlTipoEntidad.SelectedValue = "1";
            ddlTipoActividad.SelectedValue = "-1";
            ddlClaseEntidad.SelectedValue = "-1";
            ddlExtecionMatruculaMercantil.SelectedValue = "-1";
            TxtNumeroMatriculaMercantil.Text = "";
            CalFechaMatriculaMercantil.InitNull = true;
            if (ddlTipoPersona.SelectedValue == "2")
                llenarRdbLVigencia(true);
            else if (ddlTipoPersona.SelectedValue == "1")
                llenarRdbLVigencia(false);
        }
        else if (ddlSector.SelectedValue == "1")   // sector Privado
        {
            ddlRamaoestructura.SelectedValue = "-1";
            ddlNivelgobierno.SelectedValue = "-1";
            ddlNivelOrganizacional.SelectedValue = "-1";
            ddlTipoEntidadPublica.SelectedValue = "-1";
            if (ddlTipoPersona.SelectedValue.Equals("1"))
                llenarRdbLVigencia(true);
            else if (ddlTipoPersona.SelectedValue.Equals("2"))
                llenarRdbLVigencia(false);
        }
        ddlClaseEntidadVisible();
        if (ddlSector.SelectedValue == "2")
        {
            BuscarDocumentosDatosBasic_ByIdTemporal();
        }
        else if (ddlSector.SelectedValue == "1")   // sector Privado
        {
            gvDocDatosBasicoProv.DataSource = null;
            gvDocDatosBasicoProv.DataBind();
        }
        //BuscarDocumentosDatosBasic_ByIdTemporal();
    }

    protected void DdlTipoActividadSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarClasedeEntidad();
    }
    protected void DdlDepartamentoDireccionComercialSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDepartamentoDireccionComercial();

    }

    protected void DdlExtecionMatruculaMercantilSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarExtecionMatruculaMercantil();
        TxtNumeroMatriculaMercantil.Text = "";
        CalFechaMatriculaMercantil.InitNull = true;
    }


    protected void RblVigenciaSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblVigencia != null)
        {
            CalFechaVigencia.InitNull = true;

            CalFechaVigencia.Visible = rblVigencia.SelectedValue == "1";
            lbVigenciaHasta.Visible = rblVigencia.SelectedValue == "1";
            rvFechaVigencia.Enabled = rblVigencia.SelectedValue == "1";
            if (CalFechaConstitucion.Date.ToString() != "01/01/1900 12:00:00 a.m.")
            {
                cvFechaVigencia.ValueToCompare = CalFechaConstitucion.Date.ToString("dd-MM-yyyy");
            }
            else
            {
                rvFechaConstitucion.IsValid = false;
                rblVigencia.SelectedValue = Convert.ToString(Convert.ToInt32("0"));
            }
        }
        if (rblVigencia.SelectedValue == "0")
        {
            CalFechaVigencia.InitNull = true;

        }
    }
    protected void BtnLimpiarCiiuprinClick(object sender, EventArgs e)
    {
        btnLimpiarCiiuprin.Text = string.Empty;
        hfIdActividadCiiuPrin.Value = null;
    }


    protected void gvDocAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        //SetSessionParameter("Proveedor.Longitud", Archivo.Length.ToString());
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));


                        //ManejoControles obj = new ManejoControles();
                        //obj.OpenWindowPcUpdatePanel(this, "../../../General/General/MostrarImagenes/MostrarImagenes.aspx", 200, 200);

                        string path = "'../Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");



                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    //protected void gvDocAdjuntos_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    gvDocAdjuntos.EditIndex = e.NewEditIndex;
    //    BuscarDocumentosRegTercero_ByIdTemporal();
    //}

    //protected void gvDocAdjuntos_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{
    //    gvDocAdjuntos.EditIndex = -1;
    //    BuscarDocumentosRegTercero_ByIdTemporal();
    //}

    //protected void gvDocAdjuntos_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    try
    //    {
    //        GridViewRow row = gvDocAdjuntos.Rows[e.RowIndex];

    //        int vIdDocAdjunto = int.Parse(gvDocAdjuntos.DataKeys[e.RowIndex]["IdDocAdjunto"].ToString());
    //        int vMaxPermitidoKB = int.Parse(gvDocAdjuntos.DataKeys[e.RowIndex]["MaxPermitidoKB"].ToString());
    //        string vExtensionesPermitidas = gvDocAdjuntos.DataKeys[e.RowIndex]["ExtensionesPermitidas"].ToString();

    //        DocAdjuntoTercero doc = new DocAdjuntoTercero();
    //        if (vIdDocAdjunto > 0)
    //        {
    //            doc = vProveedorService.ConsultarDocAdjuntoTercero(vIdDocAdjunto);
    //        }
    //        else
    //        {
    //            //int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));
    //            int vIdTercero;
    //            if (Request.QueryString["oP"] == "E")
    //            {
    //                vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(hfIdEntidad.Value));
    //                vIdTercero = vEntidadProvOferente.IdTercero;
    //            }
    //            else
    //            {
    //                vIdTercero = int.Parse(hfIdTercero.Value);
    //            }

    //            doc.NombreTipoDocumento = ((Label)(row.Cells[0].FindControl("lblNombreDocumento"))).Text;
    //            doc.Descripcion = string.Empty;
    //            doc.IdDocumento = int.Parse(gvDocAdjuntos.DataKeys[e.RowIndex]["IdDocumento"].ToString());
    //            doc.FechaCrea = DateTime.Now;
    //            doc.UsuarioCrea = GetSessionUser().NombreUsuario;
    //        }

    //        FileUpload fuDocumento = ((FileUpload)(row.Cells[1].FindControl("fuDocumento")));

    //        //Se envia por ftp
    //        doc.LinkDocumento = SubirArchivo(fuDocumento, vMaxPermitidoKB, vExtensionesPermitidas);
    //        doc.FechaModifica = DateTime.Now;
    //        doc.UsuarioModifica = GetSessionUser().NombreUsuario;
    //        //Se asocia un Id temporal 
    //        //doc.IdTemporal = ViewState["IdTemporal"].ToString();
    //        doc.IdTemporal = varIdTemporal;
    //        InformacionAudioria(doc, this.PageName, vSolutionPage);


    //        //if (vIdDocAdjunto > 0)//Existe
    //        //{
    //        //    vProveedorService.ModificarDocAdjuntoTercero(doc);
    //        //    //ya se actualizó el Doc, entonces actualizar el string del campo oculto que nos servira para las validaciones.
    //        //    ActualizaValoresDocTerceros(doc.IdDocumento.ToString());
    //        //}
    //        //else
    //        //{
    //        //    vProveedorService.InsertarDocAdjuntoTercero(doc);
    //        //}

    //        vProveedorService.InsertarDocAdjuntoTercero(doc);
    //        //ya se actualizó el Doc, entonces actualizar el string del campo oculto que nos servira para las validaciones.
    //        ActualizaValoresDocTerceros(doc.IdDocumento.ToString());

    //        gvDocAdjuntos.EditIndex = -1;

    //        BuscarDocumentosRegTercero_ByIdTemporal();
    //        toolBar.LipiarMensajeError();
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }

    //}


    protected void gvDocAdjuntos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocAdjuntos.PageIndex = e.NewPageIndex;

        BuscarDocumentosRegTercero_ByIdTemporal();
    }
    #endregion

    #region Métodos

    /// <summary>
    /// Valida datos
    /// </summary>
    /// <returns></returns>
    protected bool ValidarDatos()
    {
        int errores = 0;

        if (!CalFechaActividadPrincipal.Date.ToString("dd/MM/yyyy").Equals("01/01/1900") && hfIdActividadCiiuPrin.Value.Trim().Length == 0)
        {
            errores++;
            CustomValidatorFechaActividadPri.IsValid = false;
            return false;

        }
        else
        {
            CustomValidatorFechaActividadPri.IsValid = true;
        }

        if (!CalFechaActividadSegundaria.Date.ToString("dd/MM/yyyy").Equals("01/01/1900") && HfActividadCiiuSegu.Value.Trim().Length == 0)
        {

            errores++;
            CustomValidatorFechaActividadSec.IsValid = false;
            return false;

        }
        else
        {
            CustomValidatorFechaActividadSec.IsValid = true;

        }


        if (HfActividadCiiuSegu.Value != "")
        //if (TxtActividadCiiuSegu.Text.Trim().Length > 0)
        {
            if (CalFechaActividadSegundaria.Date == null || CalFechaActividadSegundaria.Date.ToString("dd/MM/yyyy").Equals("01/01/1900"))
            {
                rfvCalFechaActividadSegundaria.Enabled = true;
                rfvCalFechaActividadSegundaria.IsValid = false;
                errores++;
                return false;
            }
            else
            {
                rfvCalFechaActividadSegundaria.Enabled = false;
                rfvCalFechaActividadSegundaria.IsValid = true;
            }
        }
        else
        {
            rfvCalFechaActividadSegundaria.Enabled = false;
            rfvCalFechaActividadSegundaria.IsValid = true;
        }

        //Validar selección de un codigo UNSPSC para el proveedor
        //si el usuario es externo y el sector es público,  este campo no es obligatorio
        //Luz Angela Borda
        //May0 14 de 2015

        int vTipoUsuarioRegistra = BuscarTipoUsuarioRegistra();
        if (!(vTipoUsuarioRegistra == 0 && ddlSector.SelectedValue == "2")) // usuario externo y sector Publico
        {

            if (((DataTable)GetSessionParameter("Proveedor.dtUNSPSC")).Rows.Count == 0)
            {
                toolBar.MostrarMensajeError("Seleccione una clasificación según su criterio para el código UNSPSC");
                return false;
            }
        }

        //si el usuario es interno con cualquier sector,  este campo no es obligatorio
        //Luz Angela Borda
        //May0 15 de 2015

        if (!(vTipoUsuarioRegistra == 1)) // usuario ineterno
        {

            if (((DataTable)GetSessionParameter("Proveedor.dtUNSPSC")).Rows.Count == 0)
            {
                toolBar.MostrarMensajeError("Seleccione una clasificación según su criterio para el código UNSPSC");
                return false;
            }
        }

        //si el usuario es externo y el sector es privado,  este campo es obligatorio
        //Luz Angela Borda
        //May0 15 de 2015

        if (vTipoUsuarioRegistra == 0 && ddlSector.SelectedValue == "1") // usuario externo y sector privado
        {

            if (((DataTable)GetSessionParameter("Proveedor.dtUNSPSC")).Rows.Count == 0)
            {
                toolBar.MostrarMensajeError("Seleccione una clasificación según su criterio para el código UNSPSC");
                return false;
            }
        }


        if (ddlTipoPersona.SelectedValue.ToString(CultureInfo.InvariantCulture) == "1") //PERSONA NATURAL
        {
            if (string.IsNullOrEmpty(TxtCelular.Text))
            {

                errores++;
                ddlTipoPersona.Focus();
                throw new Exception("Registre un número de celular");
            }
            else
            {
                if (TxtCelular.Text.Length != 10)
                {
                    errores++;
                    TxtCelular.Focus();
                    throw new Exception("la longitud del número de celular no es valida");
                }

            }
            if (TxtTelefono.Text.Length > 1)
            {
                if (TxtTelefono.Text.Length != 7)
                {

                    errores++;
                    throw new Exception("La longitud del número de teléfono no es valida");
                }
                else
                {
                    //valida indicativo
                    if (string.IsNullOrEmpty(TxtIndicativo.Text))
                    {
                        errores++;

                        TxtIndicativo.Focus();
                        throw new Exception("Registre un indicativo");
                    }
                    else
                    {
                        if (TxtIndicativo.Text == "0")
                        {
                            errores++;
                            throw new Exception("Indicativo debe tener un valor diferente de cero");
                        }
                    }

                }
            }

        }
        if (ddlTipoPersona.SelectedValue.ToString(CultureInfo.InvariantCulture) == "2") // PERSONA JURIDICA
        {
            // valida telefono
            if (string.IsNullOrEmpty(TxtIndicativo.Text))
            {

                errores++;
                throw new Exception("Registre un número de teléfono válido");
            }
            else
            {
                if (TxtTelefono.Text.Length != 7)
                {

                    errores++;
                    throw new Exception("La longitud del número de teléfono no es valida");
                }
                else
                {
                    //valida indicativo
                    if (string.IsNullOrEmpty(TxtIndicativo.Text))
                    {
                        errores++;

                        TxtIndicativo.Focus();
                        throw new Exception("Registre un indicativo");
                    }
                    else
                    {
                        if (TxtIndicativo.Text == "0")
                        {
                            errores++;
                            throw new Exception("Indicativo debe tener un valor diferente de cero");
                        }



                    }
                }

            }

            if (string.IsNullOrEmpty(ddlTipoDocumentoRepr.SelectedValue) || ddlTipoDocumentoRepr.SelectedValue == "-1" ||
                ddlTipoDocumentoRepr.SelectedValue == "0")
            {

                errores++;
                throw new Exception(" Seleccione un tipo de identificación Representante Legal");
            }

            if (string.IsNullOrEmpty(txtIdentificacionRepr.Text))
            {
                errores++;
                throw new Exception("Registre el número de identificación Representante Legal");
            }

            if (string.IsNullOrEmpty(TxtPrimerNombreRepr.Text))
            {
                errores++;
                throw new Exception("Registre el primer nombre Representante Legal");
            }

            if (string.IsNullOrEmpty(TxtPrimerApellidoRepr.Text))
            {
                errores++;
                throw new Exception("Registre el primer apellido Representante Legal");
            }



            if (string.IsNullOrEmpty(TxtCelularRepr.Text))
            {
                errores++;
                throw new Exception("Registre un número de celular Representante Legal");
            }
            else
            {
                if (TxtCelularRepr.Text.Length != 10)
                {
                    errores++;
                    throw new Exception("la longitud del número de celular del Representante Legal no es valida");
                }

            }

            // valida telefono
            if (!string.IsNullOrEmpty(TxtTelefonoRepr.Text))
            {

                //errores++;
                //throw new Exception("Registre un número de teléfono válido para Representante Legal");
                //}
                //else
                //{
                if (TxtTelefonoRepr.Text.Length != 7)
                {

                    errores++;
                    throw new Exception("La longitud del número de teléfono del Representante Legal no es válida");
                }
                else
                {
                    //valida indicativo rep legal 
                    if (string.IsNullOrEmpty(TxtIndicativoRepr.Text))
                    {

                        errores++;
                        throw new Exception("Registre un indicativo Representante Legal");
                    }
                    else
                    {
                        if (TxtIndicativo.Text == "0")
                        {
                            lbIndicativoRepr.Text = "Indicativo del Representante Legal debe tener un valor diferente de cero";
                            errores++;
                            throw new Exception("Indicativo del Representante Legal debe tener un valor diferente de cero");
                        }
                        else
                        {
                            lbIndicativoRepr.Text = "";
                        }
                    }
                }
            }
            //valida mail repre
            if (string.IsNullOrEmpty(TxtCorreoElectronicoRepr.Text))
            {

                errores++;
                throw new Exception("Registre un correo válido para el Representante Legal");
            }

            //faiber
            //}
        }
        // valida sectro
        if ((!ddlTipoPersona.SelectedValue.Equals("3") && !ddlTipoPersona.SelectedValue.Equals("4")) & (string.IsNullOrEmpty(ddlSector.SelectedValue) || ddlSector.SelectedValue == "-1" || ddlSector.SelectedValue == "0"))
        {
            rvSector.Validate();
            errores++;
            throw new Exception("Debe seleccionar el sector");
        }
        // valida Regimen Tributario
        if ((!ddlTipoPersona.SelectedValue.Equals("3") && !ddlTipoPersona.SelectedValue.Equals("4")) & (string.IsNullOrEmpty(ddlRegimenTributario.SelectedValue) || ddlRegimenTributario.SelectedValue == "-1" || ddlRegimenTributario.SelectedValue == "0"))
        {
            rvRegimenTributario.Validate();
            errores++;
            throw new Exception("Debe seleccionar el régimen tributario ");
        }
        // valida Fecha constitución o de establecimiento de comercio
        if (CalFechaConstitucion.Date.ToString(CultureInfo.InvariantCulture) == "" || CalFechaConstitucion.Date == null)
        {
            errores++;
            throw new Exception("Registre la Fecha constitución o de establecimiento de comercio");
        }

        // valida Regimen Tributario
        if ((!ddlTipoPersona.SelectedValue.Equals("3") && !ddlTipoPersona.SelectedValue.Equals("4")) & (string.IsNullOrEmpty(ddlTipoEntidad.SelectedValue) || ddlTipoEntidad.SelectedValue == "-1" || ddlTipoEntidad.SelectedValue == "0"))
        {
            rvTipoEntidad.Validate();
            errores++;
            throw new Exception("Debe seleccionar  un Tipo de entidad");
        }
        // valida Actividad CIIU principal
        if (string.IsNullOrEmpty(hfIdActividadCiiuPrin.Value))
        {
            //rtActividadCiiuPrin.Validate();
            errores++;
            throw new Exception("Debe seleccionar una actividad CIIU principal");

        }
        // valida  Fecha inicio actividad económica principal 
        if (CalFechaActividadPrincipal.Date.ToString(CultureInfo.InvariantCulture) == "" || CalFechaActividadPrincipal.Date == null || CalFechaActividadPrincipal.Date.ToString("dd/MM/yyyy").Equals("01/01/1900"))
        {
            lbFechaActividadPrincipal.Text = "Registre la fecha de inicio de la actividad principal CIIU";
            errores++;
            throw new Exception("Registre la fecha de inicio de la actividad principal CIIU ");
        }
        else
        {
            lbFechaActividadPrincipal.Text = "";
        }
        // valida Actividad CIIU secundaria
        if (HfActividadCiiuSegu.Value != null && HfActividadCiiuSegu.Value != "")
        {
            if (hfIdActividadCiiuPrin.Value == HfActividadCiiuSegu.Value)
            {
                errores++;
                throw new Exception("Si cuenta con una actividad CIIU secundaria, registre una diferente a la principal");

            }

            if (CalFechaActividadSegundaria.Date.ToString(CultureInfo.InvariantCulture) == "" || CalFechaActividadSegundaria.Date == null || CalFechaActividadSegundaria.Date.ToString("dd/MM/yyyy").Equals("01/01/1900"))
            {
                lbCFechaActividadSegundaria.Text = "Registre  la fecha de inicio de la actividad secundaria CIIU";
                errores++;
                throw new Exception("Registre  la fecha de inicio de la actividad secundaria CIIU");
            }
            else
            {
                lbCFechaActividadSegundaria.Text = "";
            }
        }
        if ((!ddlTipoPersona.SelectedValue.Equals("3") && !ddlTipoPersona.SelectedValue.Equals("4")) & (string.IsNullOrEmpty(ddlDepartamentoConstituida.SelectedValue) || ddlDepartamentoConstituida.SelectedValue == "-1" || ddlDepartamentoConstituida.SelectedValue == "0"))
        {
            rvDepartamentoConstituida.Validate();
            errores++;
            throw new Exception("Seleccione departamento de domicilio");
        }
        if ((!ddlTipoPersona.SelectedValue.Equals("3") && !ddlTipoPersona.SelectedValue.Equals("4")) & (string.IsNullOrEmpty(ddlMunicipioConstituida.SelectedValue) || ddlMunicipioConstituida.SelectedValue == "-1" || ddlMunicipioConstituida.SelectedValue == "0"))
        {
            rvMunicipioConstituida.Validate();
            errores++;
            throw new Exception("Seleccione municipio de domicilio");
        }
        if (string.IsNullOrEmpty(ddlDepartamentoDireccionComercial.SelectedValue) || ddlDepartamentoDireccionComercial.SelectedValue == "-1" || ddlDepartamentoDireccionComercial.SelectedValue == "0")
        {
            rvDepartamentoDireccionComercial.Validate();
            errores++;
            throw new Exception("Seleccione departamentode ubicación, de dirección comercial o de gerencia");
        }
        if (string.IsNullOrEmpty(ddlMunicipioDireccionComercial.SelectedValue) || ddlMunicipioDireccionComercial.SelectedValue == "-1" || ddlMunicipioDireccionComercial.SelectedValue == "0")
        {
            rvMunicipioDireccionComercial.Validate();
            errores++;
            throw new Exception("Seleccione municipio de ubicación, de dirección comercial o de gerencia");
        }
        //valida Zona
        if (string.IsNullOrEmpty(txtDireccionResidencia.TipoZona))
        {
            lbDireccionResidencia.Text = "Seleccione la zona de ubicación de dirección comercial o de gerencia";
            errores++;
            throw new Exception("Seleccione la zona de ubicación de dirección comercial o de gerencia");
        }
        else
        {
            lbDireccionResidencia.Text = "";
        }
        //valida Zona
        if (string.IsNullOrEmpty(txtDireccionResidencia.Text))
        {
            lbDireccionResidencia.Text = "Dirección comercial o de gerencia";
            errores++;
            throw new Exception("Dirección comercial o de gerencia");
        }
        else
        {
            lbDireccionResidencia.Text = "";
        }

        // valida vigencia
        if ((!ddlTipoPersona.SelectedValue.Equals("3") || !ddlTipoPersona.SelectedValue.Equals("4")) & rblVigencia.SelectedValue == "0")
        {
            if (CalFechaVigencia.Date.ToString(CultureInfo.InvariantCulture) == "" || CalFechaVigencia.Date == null)
            {
                errores++;
                throw new Exception("Registre la fecha de inicio de la actividad principal CIIU");
            }

        }


        if (ddlSector.SelectedValue == "1")//Privado
        {
            if (string.IsNullOrEmpty(ddlTipoActividad.SelectedValue) || ddlTipoActividad.SelectedValue == "-1" || ddlTipoActividad.SelectedValue == "0")
            {

                errores++;
                throw new Exception("Debe seleccionar  un Tipo de actividad");
            }
            else
            {
                //lbTipoActividad.Text = "";
            }
            if (rvddlClaseEntidad.Enabled && (string.IsNullOrEmpty(ddlClaseEntidad.SelectedValue) || ddlClaseEntidad.SelectedValue == "-1" || ddlClaseEntidad.SelectedValue == "0"))
            {
                errores++;
                throw new Exception("Debe seleccionar una clase de entidad");
            }
            else
            {
                //lbClaseEntidad.Text = "";
            }
            if (string.IsNullOrEmpty(ddlExtecionMatruculaMercantil.SelectedValue) || ddlExtecionMatruculaMercantil.SelectedValue == "-1")
            {

                errores++;
                throw new Exception(" ");
            }
            else
            {
                //lbExtecionMatruculaMercantil.Text = "";
                if (ddlExtecionMatruculaMercantil.SelectedValue == "0")
                {
                    if (string.IsNullOrEmpty(TxtNumeroMatriculaMercantil.Text))
                    {

                        errores++;
                        throw new Exception("Registre Número de matrícula mercantil");
                    }
                    else
                    {
                        //lbNumeroMatriculaMercan.Text = "";
                    }
                    if (CalFechaMatriculaMercantil.Date.ToString(CultureInfo.InvariantCulture) == "" || CalFechaMatriculaMercantil.Date == null)
                    {
                        lbFechaMatriculaMercan.Text = "Registre la fecha de matrícula mercantil";
                        errores++;
                        throw new Exception("Registre la fecha de matrícula mercantil");
                    }
                    else
                    {
                        lbFechaMatriculaMercan.Text = "";
                        if (CalFechaConstitucion.Date > CalFechaMatriculaMercantil.Date)
                        {
                            errores++;
                            throw new Exception("La fecha de matrícula mercantil no puede ser menor a la fecha de constitución o de establecimiento de comercio.");
                        }
                    }
                }
            }
        }
        else if (ddlSector.SelectedValue == "2") //Publico
        {
            if (string.IsNullOrEmpty(ddlRamaoestructura.SelectedValue) || ddlRamaoestructura.SelectedValue == "-1" || ddlRamaoestructura.SelectedValue == "0")
            {

                errores++;
                throw new Exception("Clasifique la entidad en una rama o estructura");
            }
            else
            {
                //lbRamaoestructura.Text = "";
            }
            if (string.IsNullOrEmpty(ddlNivelgobierno.SelectedValue) || ddlNivelgobierno.SelectedValue == "-1" || ddlNivelgobierno.SelectedValue == "0")
            {

                errores++;
                throw new Exception("Clasifiquela entidad en un nivel de gobierno");
            }
            else
            {
                //lbNivelgobierno.Text = "";
            }
            if (string.IsNullOrEmpty(ddlNivelOrganizacional.SelectedValue) || ddlNivelOrganizacional.SelectedValue == "-1" || ddlNivelOrganizacional.SelectedValue == "0")
            {

                errores++;
                throw new Exception("Seleccione un nivel organizacional");
            }
            else
            {
                //lbNivelOrganizacional.Text = "";
            }
            if (string.IsNullOrEmpty(ddlTipoEntidadPublica.SelectedValue) || ddlTipoEntidadPublica.SelectedValue == "-1" || ddlTipoEntidadPublica.SelectedValue == "0")
            {

                errores++;
                throw new Exception("Seleccione un Tipo de entidad");
            }
            else
            {
                //lbTipoEntidadPublica.Text = "";
            }
        }

        if (errores == 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    /// <summary>
    /// Limpia errores en pantalla
    /// </summary>
    private void LimpiarErrore()
    {
        toolBar.LipiarMensajeError();
        toolBar.LimpiarOpcionesAdicionales();
        lbFechaActividadPrincipal.Text = "";
        lbCFechaActividadSegundaria.Text = "";
        lbFechaMatriculaMercan.Text = "";
        lbDireccionResidencia.Text = "";
        lbIndicativoRepr.Text = "";
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {

        LimpiarErrore();
        try
        {
            if (!ValidarDatos())
            {
            }
            else
            {
                int vResultado;

                if (!string.IsNullOrEmpty(hfIdEntidad.Value))
                {
                    vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(hfIdEntidad.Value));
                }
                else
                {
                    vEntidadProvOferente = new EntidadProvOferente();
                    vEntidadProvOferente.IdTercero = Convert.ToInt32(hfIdTercero.Value);
                }
                vEntidadProvOferente.InfoAdminEntidadProv = vInfoAdmin;
                vEntidadProvOferente.IdTipoSector = Convert.ToInt32(ddlSector.SelectedValue);
                vEntidadProvOferente.FechaConstitucion = CalFechaConstitucion.Date;

                vEntidadProvOferente.FechaIngresoICBF = FechaICBF.Date;
                if (!string.IsNullOrEmpty(rblVigencia.SelectedValue) && rblVigencia.SelectedValue != "-1")
                {
                    vEntidadProvOferente.TipoVigencia = Convert.ToBoolean(Convert.ToInt16(rblVigencia.SelectedValue));
                }

                if (!CalFechaVigencia.Date.ToString("dd/MM/yyyy").Equals("01/01/1900"))
                {
                    vEntidadProvOferente.FechaVigencia = CalFechaVigencia.Date;
                }

                vEntidadProvOferente.UsuarioCrea = GetSessionUser().NombreUsuario;

                //SE FIJA EN ESTADO POR VALIDAR SIEMPRE
                //
                //**Estado Inicial**//
                vEntidadProvOferente.IdEstado = 4;// Convert.ToInt32(ddlEstadoValidacion.SelectedValue);
                //}
                vEntidadProvOferente.ObserValidador = "";
                vEntidadProvOferente.FechaCiiuPrincipal = CalFechaActividadPrincipal.Date;

                if (!CalFechaActividadSegundaria.Date.ToString("dd/MM/yyyy").Equals("01/01/1900"))
                {
                    vEntidadProvOferente.FechaCiiuSecundario = CalFechaActividadSegundaria.Date;
                }
                else
                {
                    vEntidadProvOferente.FechaCiiuSecundario = null;
                }



                if (!string.IsNullOrEmpty(ddlClaseEntidad.SelectedValue) && ddlClaseEntidad.SelectedValue != "-1")
                {
                    vEntidadProvOferente.IdTipoClaseEntidad = Convert.ToInt32(ddlClaseEntidad.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlExtecionMatruculaMercantil.SelectedValue) && ddlExtecionMatruculaMercantil.SelectedValue != "-1")
                {
                    vEntidadProvOferente.ExenMatriculaMer =
                        Convert.ToBoolean(Convert.ToInt32(ddlExtecionMatruculaMercantil.SelectedValue));
                }
                vEntidadProvOferente.MatriculaMercantil = TxtNumeroMatriculaMercantil.Text.ToUpper();

                if (!CalFechaMatriculaMercantil.Date.ToString("dd/MM/yyyy").Equals("01/01/1900"))
                {
                    vEntidadProvOferente.FechaMatriculaMerc = CalFechaMatriculaMercantil.Date;
                }

                if (!string.IsNullOrEmpty(ddlRamaoestructura.SelectedValue) && ddlRamaoestructura.SelectedValue != "-1")
                {
                    vEntidadProvOferente.IdTipoRamaPublica = Convert.ToInt32(ddlRamaoestructura.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlNivelgobierno.SelectedValue) && ddlNivelgobierno.SelectedValue != "-1")
                {
                    vEntidadProvOferente.IdTipoNivelGob = Convert.ToInt32(ddlNivelgobierno.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlNivelOrganizacional.SelectedValue) &&
                    ddlNivelOrganizacional.SelectedValue != "-1")
                {
                    vEntidadProvOferente.IdTipoNivelOrganizacional =
                        Convert.ToInt32(ddlNivelOrganizacional.SelectedValue);
                }
                if (!string.IsNullOrEmpty(hfIdActividadCiiuPrin.Value))
                {
                    vEntidadProvOferente.IdTipoCiiuPrincipal = Convert.ToInt32(hfIdActividadCiiuPrin.Value);
                }
                vEntidadProvOferente.UsuarioCrea = GetSessionUser().NombreUsuario;
                if (!string.IsNullOrEmpty(HfActividadCiiuSegu.Value))
                {
                    vEntidadProvOferente.IdTipoCiiuSecundario = Convert.ToInt32(HfActividadCiiuSegu.Value);
                }
                else
                {
                    vEntidadProvOferente.IdTipoCiiuSecundario = null;
                }
                if (!string.IsNullOrEmpty(hfIdEntidad.Value))
                {
                    vEntidadProvOferente.IdEntidad = Convert.ToInt32(hfIdEntidad.Value);
                }
                if (vEntidadProvOferente.IdTercero != 0)
                {
                    vEntidadProvOferente.TerceroProveedor = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                }
                else
                {
                    vEntidadProvOferente.TerceroProveedor = new Tercero();
                }

                if (TxtCorreoElectronico.Text != "")
                {
                    vEntidadProvOferente.TerceroProveedor.Email = TxtCorreoElectronico.Text;
                }
                else
                {
                    vEntidadProvOferente.TerceroProveedor.Email = "";
                }

                if (vEntidadProvOferente.IdTercero != 0)
                {
                    vEntidadProvOferente.TelTerceroProveedor = vOferenteService.ConsultarTelTercerosIdTercero(vEntidadProvOferente.IdTercero);
                }
                else
                {
                    vEntidadProvOferente.TelTerceroProveedor = new TelTerceros();
                }

                if (vEntidadProvOferente.IdEntidad != 0)
                {
                    vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);
                }
                else
                {
                    vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
                }
                vEntidadProvOferente.InfoAdminEntidadProv.SitioWeb = TxtSitioWeb.Text;
                if (!string.IsNullOrEmpty(ddlRegimenTributario.SelectedValue) &&
                    ddlRegimenTributario.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib =
                        Convert.ToInt32(ddlRegimenTributario.SelectedValue);
                }
                vEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento = TxtNombreEstablecimiento.Text;
                if (!string.IsNullOrEmpty(ddlTipoEntidad.SelectedValue) && ddlTipoEntidad.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad =
                        Convert.ToInt32(ddlTipoEntidad.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlTipoActividad.SelectedValue) && ddlTipoActividad.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad =
                        Convert.ToInt32(ddlTipoActividad.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlTipoEntidadPublica.SelectedValue) &&
                    ddlTipoEntidadPublica.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica =
                        Convert.ToInt32(ddlTipoEntidadPublica.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlDepartamentoConstituida.SelectedValue) &&
                    ddlDepartamentoConstituida.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida =
                        Convert.ToInt32(ddlDepartamentoConstituida.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlMunicipioConstituida.SelectedValue) &&
                    ddlMunicipioConstituida.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida =
                        Convert.ToInt32(ddlMunicipioConstituida.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlDepartamentoDireccionComercial.SelectedValue) &&
                    ddlDepartamentoDireccionComercial.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial =
                        Convert.ToInt32(ddlDepartamentoDireccionComercial.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlMunicipioDireccionComercial.SelectedValue) &&
                    ddlMunicipioDireccionComercial.SelectedValue != "-1")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial =
                        Convert.ToInt32(ddlMunicipioDireccionComercial.SelectedValue);
                }
                if (txtDireccionResidencia.TipoZona == "U")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdZona = "1";
                }
                if (txtDireccionResidencia.TipoZona == "R")
                {
                    vEntidadProvOferente.InfoAdminEntidadProv.IdZona = "2";
                }

                #region Datos tel proveedor
                if (vEntidadProvOferente.IdTercero != 0)
                {
                    vEntidadProvOferente.TelTerceroProveedor = vOferenteService.ConsultarTelTercerosIdTercero(vEntidadProvOferente.IdTercero);
                }
                else
                {
                    vEntidadProvOferente.TelTerceroProveedor = new TelTerceros();
                }
                if (!string.IsNullOrEmpty(TxtIndicativo.Text))
                {
                    vEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono = Convert.ToInt32(TxtIndicativo.Text);
                }
                else
                {
                    vEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono = null;
                }
                if (!string.IsNullOrEmpty(TxtTelefono.Text))
                {
                    vEntidadProvOferente.TelTerceroProveedor.NumeroTelefono = Convert.ToInt32(TxtTelefono.Text);
                }
                else
                {
                    vEntidadProvOferente.TelTerceroProveedor.NumeroTelefono = null;
                }
                if (!string.IsNullOrEmpty(TxtExtencion.Text))
                {
                    vEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono = Convert.ToInt64(TxtExtencion.Text);
                }
                else
                {
                    vEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono = null;
                }
                if (!string.IsNullOrEmpty(TxtCelular.Text))
                {
                    vEntidadProvOferente.TelTerceroProveedor.Movil = Convert.ToInt64(TxtCelular.Text);
                }
                else
                {
                    vEntidadProvOferente.TelTerceroProveedor.Movil = null;
                }

                #endregion

                vEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial = txtDireccionResidencia.Text;
                if (ddlTipoPersona.SelectedValue == "2" || ddlTipoPersona.SelectedValue == "3" || ddlTipoPersona.SelectedValue == "4")
                {

                    // Si es persona Juídica, actualizamos en el Objeto la Razón Social
                    vEntidadProvOferente.TerceroProveedor.RazonSocial = TxtRazonSocial.Text.ToUpper();
                    //if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null && vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != 0)
                    //{
                    //    vEntidadProvOferente.RepresentanteLegal =
                    //        vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);
                    //}
                    //else
                    //{
                    //    vEntidadProvOferente.RepresentanteLegal =
                    //        vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(
                    //            Convert.ToInt32(ddlTipoDocumentoRepr.SelectedValue), txtIdentificacionRepr.Text);
                    //    vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal = vEntidadProvOferente.RepresentanteLegal.IdTercero;
                    //}
                    vEntidadProvOferente.RepresentanteLegal =
                    vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(
                    Convert.ToInt32(ddlTipoDocumentoRepr.SelectedValue), txtIdentificacionRepr.Text);
                    vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal = vEntidadProvOferente.RepresentanteLegal.IdTercero;

                    //Agregamos al representante legal los datos Sexo y Fecha de Nacimiento.
                    vEntidadProvOferente.RepresentanteLegal.Sexo = ddlSexoRepLegal.SelectedValue;
                    vEntidadProvOferente.RepresentanteLegal.FechaNacimiento = cuFechaNacRepLegal.Date;

                    if (!string.IsNullOrEmpty(ddlTipoDocumentoRepr.SelectedValue) &&
                        ddlTipoDocumentoRepr.SelectedValue != "-1")
                    {
                        vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento =
                            Convert.ToInt32(ddlTipoDocumentoRepr.SelectedValue);
                    }

                    vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion = txtIdentificacionRepr.Text.ToUpper();
                    vEntidadProvOferente.RepresentanteLegal.PrimerNombre = TxtPrimerNombreRepr.Text.Trim().ToUpper();
                    vEntidadProvOferente.RepresentanteLegal.SegundoNombre = TxtSegundoNombreRepr.Text.Trim().ToUpper();
                    vEntidadProvOferente.RepresentanteLegal.PrimerApellido = TxtPrimerApellidoRepr.Text.Trim().ToUpper();
                    vEntidadProvOferente.RepresentanteLegal.SegundoApellido = TxtSegundoApellidoRepr.Text.Trim().ToUpper();
                    vEntidadProvOferente.RepresentanteLegal.Email = TxtCorreoElectronicoRepr.Text.Trim();
                    if (vEntidadProvOferente.RepresentanteLegal.IdTercero != 0 && vEntidadProvOferente.RepresentanteLegal.IdTercero != null)
                    {
                        vEntidadProvOferente.TelRepresentanteLegal =
                            vOferenteService.ConsultarTelTercerosIdTercero(
                                Convert.ToInt32(vEntidadProvOferente.RepresentanteLegal.IdTercero));
                    }
                    else
                    {
                        vEntidadProvOferente.TelRepresentanteLegal = new TelTerceros();
                    }
                    if (!string.IsNullOrEmpty(TxtCelularRepr.Text))
                        vEntidadProvOferente.TelRepresentanteLegal.Movil = Convert.ToInt64(TxtCelularRepr.Text);
                    else
                        vEntidadProvOferente.TelRepresentanteLegal.Movil = null;

                    if (!string.IsNullOrEmpty(TxtIndicativoRepr.Text))
                        vEntidadProvOferente.TelRepresentanteLegal.IndicativoTelefono = Convert.ToInt32(TxtIndicativoRepr.Text);
                    else
                        vEntidadProvOferente.TelRepresentanteLegal.IndicativoTelefono = null;

                    if (!string.IsNullOrEmpty(TxtTelefonoRepr.Text))
                        vEntidadProvOferente.TelRepresentanteLegal.NumeroTelefono = Convert.ToInt32(TxtTelefonoRepr.Text);
                    else
                        vEntidadProvOferente.TelRepresentanteLegal.NumeroTelefono = null;

                    if (!string.IsNullOrEmpty(TxtExtencionRepr.Text))
                        vEntidadProvOferente.TelRepresentanteLegal.ExtensionTelefono = Convert.ToInt64(TxtExtencionRepr.Text);
                    else
                        vEntidadProvOferente.TelRepresentanteLegal.ExtensionTelefono = null;

                    vEntidadProvOferente.RepresentanteLegal.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.TelRepresentanteLegal.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.RepresentanteLegal.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.TelRepresentanteLegal.UsuarioCrea = GetSessionUser().NombreUsuario;
                }
                else
                {
                    vEntidadProvOferente.RepresentanteLegal = new Tercero();
                    vEntidadProvOferente.TelRepresentanteLegal = new TelTerceros();
                }
                vEntidadProvOferente.TipoEntOfProv = true;

                //--Agregando las datos: 1erNombre,2doNombre,1erApellido, 2doApellido y correo Electronico para actualizar en Terceros.
                vEntidadProvOferente.TerceroProveedor.Email = TxtCorreoElectronico.Text;
                vEntidadProvOferente.TerceroProveedor.PrimerNombre = TxtPrimerNombre.Text.ToUpper();
                vEntidadProvOferente.TerceroProveedor.SegundoNombre = TxtSegundoNombre.Text.ToUpper();
                vEntidadProvOferente.TerceroProveedor.PrimerApellido = TxtPrimerApellido.Text.ToUpper();
                vEntidadProvOferente.TerceroProveedor.SegundoApellido = TxtSegundoApellido.Text.ToUpper();

                vEntidadProvOferente.IdTemporal = ViewState["IdTemporal"].ToString(); //Se usa para hacer el update de documentos.
                if (!new GeneralWeb().GetSessionUser().Rol.Split(';').Contains("PROVEEDORES"))
                    vEntidadProvOferente.TerceroProveedor.CreadoPorInterno = true;
                else
                    vEntidadProvOferente.TerceroProveedor.CreadoPorInterno = false;
                if (Request.QueryString["oP"] == "E")
                {
                    vEntidadProvOferente.IdEntidad = Convert.ToInt32(hfIdEntidad.Value);
                    vEntidadProvOferente.UsuarioModifica = GetSessionUser().NombreUsuario;

                    vEntidadProvOferente.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.TerceroProveedor.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.TelTerceroProveedor.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.InfoAdminEntidadProv.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.IdEstado = 1; // Coloca en Estado POR VALIDAR
                    vEntidadProvOferente.Finalizado = false;


                    if (ddlTipoPersona.SelectedValue.Equals("3") || ddlTipoPersona.SelectedValue.Equals("4"))
                    {
                        vEntidadProvOferente.NumIntegrantes = int.Parse(txtNumInt.Text);
                        /**
                        AQUI PARA VALIDAR EL NUMERO DE INTEGRANTES.
                        **/
                        if (vEntidadProvOferente.NumIntegrantes < vProveedorService.ConsultarIntegrantess(vEntidadProvOferente.IdEntidad).Count)
                        {
                            throw new Exception("Número de Integrantes es inferior a los asociados al Proveedor.");
                        }
                    }

                    ValidarDocumentosObligatorios(vEntidadProvOferente);

                    InformacionAudioria(vEntidadProvOferente, this.PageName, vSolutionPage);
                    vResultado = vProveedorService.ModificarEntidadProvOferente(vEntidadProvOferente);
                    ModificaSucursalDefault();
                    //if (vResultado == 1)
                    //{
                    //    vProveedorService.ModificarConfirmaYApreba_InfoDatosBasicosEntidad(vEntidadProvOferente.IdEntidad);
                    //}
                    int vIdEstadoCompara = Convert.ToInt32(GetSessionParameter("vEntidadProvOferente.IdEstado"));
                    if (vIdEstadoCompara == 2)
                    {
                        CambiaEstado(vEntidadProvOferente.IdEntidad);
                    }


                    if (ddlTipoPersona.SelectedValue.Equals("2"))
                    {
                        vEntidadProvOferente.TerceroProveedor.RazonSocial = TxtRazonSocial.Text;
                        if (!vEntidadProvOferenteAEditar.TerceroProveedor.RazonSocial.Equals(vEntidadProvOferente.TerceroProveedor.RazonSocial))
                        {
                            // ACTUALIZAR RAzón SOCIAL EN TERCEROS Y EN USUARIOS
                            vOferenteService.ModificarRazonSocialTercero(vEntidadProvOferente.IdTercero, vEntidadProvOferente.TerceroProveedor.RazonSocial);
                            if (!(bool)vEntidadProvOferente.TerceroProveedor.CreadoPorInterno)
                                vRUBOService.ModificarRazonSocialDeUsuario(vEntidadProvOferente.TerceroProveedor.RazonSocial, vEntidadProvOferente.TerceroProveedor.ProviderUserKey);
                        }
                    }

                    //Caso de uso 74 - Observaciones y envio de correo
                    if (pObservacionesCorreccion.Visible)
                    {
                        if (!string.IsNullOrEmpty(txtObservacionesCorreccion.Text))
                        {
                            EntidadProvOferente vEntidadAntigua = new EntidadProvOferente();
                            vEntidadAntigua = Session["vEntidadProvOferenteActual"] as EntidadProvOferente;
                            string vCorreoElectronico = TxtCorreoElectronico.Text;
                            string vEmailPara = vCorreoElectronico;
                            string vNombreOrigen = ConfigurationManager.AppSettings["MailFrom"].ToString();
                            string vAsunto = string.Empty;
                            string vMensaje = string.Empty;
                            string vMensajeJuridico = string.Empty;
                            int vIdUsuario = 0;
                            string vArchivo = string.Empty;
                            if (GuardarObservacionesCorreccion())
                            {

                                vAsunto = HttpContext.Current.Server.HtmlDecode("Notificaci&oacute;n sistema de proveedores ICBF.");


                                vMensaje = HttpContext.Current.Server.HtmlDecode("<div style='color: #000000;'> <font color='black'> <CENTER> <B>Notificaci&oacute;n sistema de proveedores ICBF. </B> </CENTER> </font> ") +
                                            "</br> Observaciones:" + txtObservacionesCorreccion.Text +
                                            "</br> </br> <table style='border: 1px solid #000000; width:50%;' align='center'>" +
                                            "<tr> <td style='border: 1px solid #000000; width:40%;'>Nombre Campo</td>" +
                                            " <td style='border: 1px solid #000000; width:30%;'>Actual</td>" +
                                            "<td style='border: 1px solid #000000; width:30%;'>Nuevo</td>" +
                                            "</tr>";

                                ///DATOS BÁSICOS
                                vMensaje += (vEntidadAntigua.TerceroProveedor.PrimerNombre != TxtPrimerNombre.Text ?
                                    CuerpoCorreoModificaciones(LblPrimerNombre.Text, vEntidadAntigua.TerceroProveedor.PrimerNombre, TxtPrimerNombre.Text) : string.Empty);

                                if (!string.IsNullOrEmpty(TxtSegundoNombre.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TerceroProveedor.SegundoNombre)))
                                {
                                    vMensaje += (vEntidadAntigua.TerceroProveedor.SegundoNombre != TxtSegundoNombre.Text ?
                                         CuerpoCorreoModificaciones(LblSegundoNombre.Text, vEntidadAntigua.TerceroProveedor.SegundoNombre, TxtSegundoNombre.Text) : string.Empty);
                                }

                                vMensaje += (vEntidadAntigua.TerceroProveedor.PrimerApellido != TxtPrimerApellido.Text ?
                                    CuerpoCorreoModificaciones(LblPrimerApellido.Text, vEntidadAntigua.TerceroProveedor.PrimerApellido, TxtPrimerApellido.Text) : string.Empty);

                                if (!string.IsNullOrEmpty(TxtSegundoApellido.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TerceroProveedor.SegundoApellido)))
                                {
                                    vMensaje += (vEntidadAntigua.TerceroProveedor.SegundoApellido != TxtSegundoApellido.Text ?
                                       CuerpoCorreoModificaciones(LblSegundoApellido.Text, vEntidadAntigua.TerceroProveedor.SegundoApellido, TxtSegundoApellido.Text) : string.Empty);
                                }

                                if (!string.IsNullOrEmpty(TxtCorreoElectronico.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TerceroProveedor.Email)))
                                {
                                    vMensaje += (vEntidadAntigua.TerceroProveedor.Email != TxtCorreoElectronico.Text ?
                                         CuerpoCorreoModificaciones("Correo electr&oacute;nico", vEntidadAntigua.TerceroProveedor.Email, TxtCorreoElectronico.Text) : string.Empty);
                                }

                                if (!string.IsNullOrEmpty(TxtIndicativo.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TelTerceroProveedor.IndicativoTelefono.ToString())))
                                {
                                    vMensaje += (vEntidadAntigua.TelTerceroProveedor.IndicativoTelefono.ToString() != TxtIndicativo.Text ?
                                         CuerpoCorreoModificaciones("Indicativo", vEntidadAntigua.TelTerceroProveedor.IndicativoTelefono.ToString(), TxtIndicativo.Text) : string.Empty);
                                }

                                if (!string.IsNullOrEmpty(TxtTelefono.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TelTerceroProveedor.NumeroTelefono.ToString())))
                                {
                                    vMensaje += (vEntidadAntigua.TelTerceroProveedor.NumeroTelefono.ToString() != TxtTelefono.Text ?
                                         CuerpoCorreoModificaciones("Teléfono", vEntidadAntigua.TelTerceroProveedor.NumeroTelefono.ToString(), TxtTelefono.Text) : string.Empty);
                                }

                                if (!string.IsNullOrEmpty(TxtExtencion.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TelTerceroProveedor.ExtensionTelefono.ToString())))
                                {
                                    vMensaje += (vEntidadAntigua.TelTerceroProveedor.ExtensionTelefono.ToString() != TxtExtencion.Text ?
                                         CuerpoCorreoModificaciones("Extensión", vEntidadAntigua.TelTerceroProveedor.ExtensionTelefono.ToString(), TxtExtencion.Text) : string.Empty);

                                }

                                if (!string.IsNullOrEmpty(TxtCelular.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.TelTerceroProveedor.Movil.ToString())))
                                {
                                    vMensaje += (vEntidadAntigua.TelTerceroProveedor.Movil.ToString() != TxtCelular.Text ?
                                         CuerpoCorreoModificaciones("Celular", vEntidadAntigua.TelTerceroProveedor.Movil.ToString(), TxtCelular.Text) : string.Empty);
                                }

                                if (!string.IsNullOrEmpty(TxtSitioWeb.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.InfoAdminEntidadProv.SitioWeb)))
                                {
                                    vMensaje += (vEntidadAntigua.InfoAdminEntidadProv.SitioWeb != TxtSitioWeb.Text ?
                                         CuerpoCorreoModificaciones("Sitio Web", vEntidadAntigua.InfoAdminEntidadProv.SitioWeb, TxtSitioWeb.Text) : string.Empty);
                                }

                                if (ddlSector.Visible)
                                {
                                    string vSectorAntiguo = string.Empty;
                                    string vSectorNuevo = ddlSector.SelectedItem.Text;
                                    ddlSector.SelectedValue = vEntidadAntigua.IdTipoSector.ToString();
                                    vSectorAntiguo = ddlSector.SelectedItem.Text;
                                    vMensaje += (vSectorAntiguo != vSectorNuevo ?
                                        CuerpoCorreoModificaciones(lblSector.Text, vSectorAntiguo, vSectorNuevo) : string.Empty);
                                }

                                if (ddlRegimenTributario.Visible)
                                {
                                    string vRegimenTributarioAntiguo = string.Empty;
                                    string vRegimenTributarioNuevo = ddlRegimenTributario.SelectedItem.Text;
                                    ddlRegimenTributario.SelectedValue = vEntidadAntigua.InfoAdminEntidadProv.IdTipoRegTrib.ToString();
                                    vRegimenTributarioAntiguo = ddlRegimenTributario.SelectedItem.Text;
                                    vMensaje += (vRegimenTributarioAntiguo != vRegimenTributarioNuevo ?
                                        CuerpoCorreoModificaciones(lblRegTrib.Text, vRegimenTributarioAntiguo, vRegimenTributarioNuevo) : string.Empty);
                                }

                                if (vEntidadAntigua.FechaConstitucion != null || CalFechaConstitucion.Date != null)
                                {
                                    vMensaje += (vEntidadAntigua.FechaConstitucion != CalFechaConstitucion.Date ?
                                    CuerpoCorreoModificaciones("Fecha constitución", string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.FechaConstitucion), string.Format("{0:dd/MM/yyyy}", CalFechaConstitucion.Date)) : string.Empty);
                                }

                                if (vEntidadAntigua.FechaVigencia != null || (CalFechaVigencia.Date != null && CalFechaVigencia.Date.ToString() != "1/01/1900 12:00:00 a.m."))
                                {
                                    if (!CalFechaVigencia.Date.ToString("dd/MM/yyyy").Equals("01/01/1900") && (vEntidadAntigua.FechaVigencia != CalFechaVigencia.Date))
                                    {
                                        vMensaje += (vEntidadAntigua.FechaVigencia.ToString() != cvFechaVigencia.ValueToCompare ?
                                   CuerpoCorreoModificaciones("Fecha Vigencia", string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.FechaVigencia), string.Format("{0:dd/MM/yyyy}", CalFechaVigencia.Date)) : string.Empty);
                                    }
                                }

                                vMensaje += (vEntidadAntigua.InfoAdminEntidadProv.NombreEstablecimiento != TxtNombreEstablecimiento.Text ?
                                    CuerpoCorreoModificaciones("Nombre comercial", vEntidadAntigua.InfoAdminEntidadProv.NombreEstablecimiento, TxtNombreEstablecimiento.Text) : string.Empty);

                                if (ddlTipoEntidad.Visible)
                                {
                                    string vTipoEntidadAntiguo = string.Empty;
                                    string vTipoEntidadNuevo = ddlTipoEntidad.SelectedItem.Text;
                                    ddlTipoEntidad.SelectedValue = vEntidadAntigua.InfoAdminEntidadProv.IdTipoEntidad.ToString();
                                    vTipoEntidadAntiguo = ddlTipoEntidad.SelectedItem.Text;
                                    vMensaje += (vTipoEntidadAntiguo != vTipoEntidadNuevo ?
                                        CuerpoCorreoModificaciones(lblTipoEntidad.Text, vTipoEntidadAntiguo, vTipoEntidadNuevo) : string.Empty);
                                }

                                ///FIN DATOS BASICOS

                                ///ACTIVIDAD CIIU BIENES Y SERVICIOS UNSPSC
                                var vTipoCiiu = new TipoCiiu();
                                if (vEntidadAntigua.IdTipoCiiuPrincipal != null)
                                {
                                    string vCiuuPrincipal = string.Empty;
                                    string vCiuuPrincipalNuevo = string.Empty;
                                    vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadAntigua.IdTipoCiiuPrincipal);
                                    vCiuuPrincipal = vTipoCiiu.Descripcion;
                                    vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadProvOferente.IdTipoCiiuPrincipal);
                                    vCiuuPrincipalNuevo = vTipoCiiu.Descripcion;
                                    vMensaje += (vCiuuPrincipal != vCiuuPrincipalNuevo ?
                                        CuerpoCorreoModificaciones("Actividad CIIU principal", vCiuuPrincipal, vCiuuPrincipalNuevo) : string.Empty);
                                }

                                if (vEntidadAntigua.IdTipoCiiuSecundario != null || vEntidadProvOferente.IdTipoCiiuSecundario != null)
                                {
                                    string vCiuuSecundario = string.Empty;
                                    string vCiuuSecundarioNuevo = string.Empty;
                                    if (vEntidadProvOferente.IdTipoCiiuSecundario != null)
                                    {
                                        vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadAntigua.IdTipoCiiuSecundario);
                                        vCiuuSecundario = vTipoCiiu.Descripcion;
                                        vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadProvOferente.IdTipoCiiuSecundario);
                                        vCiuuSecundarioNuevo = vTipoCiiu.Descripcion;
                                        vMensaje += (vCiuuSecundario != vCiuuSecundarioNuevo ?
                                           CuerpoCorreoModificaciones("Actividad CIIU secundaria", vCiuuSecundario, vCiuuSecundarioNuevo) : string.Empty);
                                    }
                                    else
                                    {
                                        vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadAntigua.IdTipoCiiuSecundario);
                                        vCiuuSecundario = vTipoCiiu.Descripcion;
                                        vMensaje += (vCiuuSecundario != vCiuuSecundarioNuevo ?
                                           CuerpoCorreoModificaciones("Actividad CIIU secundaria", vCiuuSecundario, vCiuuSecundarioNuevo) : string.Empty);
                                    }

                                }

                                if (vEntidadAntigua.FechaCiiuPrincipal != null)
                                {
                                    vMensaje += (vEntidadAntigua.FechaCiiuPrincipal != CalFechaActividadPrincipal.Date ?
                                       CuerpoCorreoModificaciones("Fecha inicio actividad econ&oacute;mica principal", string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.FechaCiiuPrincipal), string.Format("{0:dd/MM/yyyy}", CalFechaActividadPrincipal.Date)) : string.Empty);
                                }
                                if (vEntidadAntigua.FechaCiiuSecundario != null)
                                {
                                    if (vEntidadProvOferente.IdTipoCiiuSecundario != null)
                                    {
                                        vMensaje += (vEntidadAntigua.FechaCiiuSecundario != CalFechaActividadSegundaria.Date ?
                                             CuerpoCorreoModificaciones("Fecha inicio actividad econ&oacute;mica secundaria", string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.FechaCiiuSecundario), string.Format("{0:dd/MM/yyyy}", CalFechaActividadSegundaria.Date)) : string.Empty);
                                    }
                                    else
                                    {
                                        vMensaje += (vEntidadAntigua.FechaCiiuSecundario != CalFechaActividadSegundaria.Date ?
                                             CuerpoCorreoModificaciones("Fecha inicio actividad econ&oacute;mica secundaria", string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.FechaCiiuSecundario), "") : string.Empty);
                                    }

                                }
                                else
                                {
                                    if (vEntidadProvOferente.IdTipoCiiuSecundario != null)
                                    {
                                        vMensaje += (vEntidadAntigua.FechaCiiuSecundario != CalFechaActividadSegundaria.Date ?
                                             CuerpoCorreoModificaciones("Fecha inicio actividad econ&oacute;mica secundaria", "", string.Format("{0:dd/MM/yyyy}", CalFechaActividadSegundaria.Date)) : string.Empty);
                                    }
                                }

                                ///FIN ACTIVIDAD CIIU BIENES Y SERVICIOS UNSPSC

                                ///COMPLEMENTO DE DATOS BASICOS
                                if (PnComplementoPrivada.Visible)
                                {
                                    string vTipoActividadAntiguo = string.Empty;
                                    string vTipoActividadNuevo = ddlTipoActividad.SelectedItem.Text;
                                    ddlTipoActividad.SelectedValue = vEntidadAntigua.InfoAdminEntidadProv.IdTipoActividad.ToString();
                                    vTipoActividadAntiguo = ddlTipoActividad.SelectedItem.Text;
                                    vMensaje += (vTipoActividadAntiguo != vTipoActividadNuevo ?
                                           CuerpoCorreoModificaciones("Tipo de actividad", vTipoActividadAntiguo, vTipoActividadNuevo) : string.Empty);

                                    if (ddlClaseEntidad.Visible)
                                    {
                                        string vClaseEntidadAntiguo = string.Empty;
                                        string vClaseEntidadNuevo = ddlClaseEntidad.SelectedItem.Text;
                                        foreach (ListItem li in ddlClaseEntidad.Items)
                                        {
                                            if (li.Value == vEntidadAntigua.IdTipoClaseEntidad.ToString())
                                            {
                                                ddlClaseEntidad.SelectedValue = vEntidadAntigua.IdTipoClaseEntidad.ToString();
                                                vClaseEntidadAntiguo = ddlClaseEntidad.SelectedItem.Text;
                                                break;
                                            }
                                        }   
                                        
                                        vMensaje += (vClaseEntidadAntiguo != vClaseEntidadNuevo ?
                                               CuerpoCorreoModificaciones(lblClaseEntidad.Text, vClaseEntidadAntiguo, vClaseEntidadNuevo) : string.Empty);
                                    }

                                    string vExtecionMatruculaMercantilAntiguo = string.Empty;
                                    string vExtecionMatruculaMercantilNuevo = ddlExtecionMatruculaMercantil.SelectedItem.Text;

                                    vExtecionMatruculaMercantilAntiguo = vEntidadAntigua.ExenMatriculaMer == true ? "SI" : "NO";
                                    vMensaje += (vExtecionMatruculaMercantilAntiguo != vExtecionMatruculaMercantilNuevo ?
                                           CuerpoCorreoModificaciones("Exenci&oacute;n Matr&iacute;cula Mercantil", vExtecionMatruculaMercantilAntiguo, vExtecionMatruculaMercantilNuevo) : string.Empty);

                                    vMensaje += (vEntidadAntigua.MatriculaMercantil != TxtNumeroMatriculaMercantil.Text ?
                                           CuerpoCorreoModificaciones(LBNumeroMatriculaMercantil.Text, vEntidadAntigua.MatriculaMercantil, TxtNumeroMatriculaMercantil.Text) : string.Empty);

                                    if (vEntidadAntigua.FechaMatriculaMerc != null || (CalFechaMatriculaMercantil.Date != null && CalFechaMatriculaMercantil.Date.ToString() != "1/01/1900 12:00:00 a.m."))
                                    {
                                        if (!CalFechaMatriculaMercantil.Date.ToString("dd/MM/yyyy").Equals("01/01/1900") && CalFechaMatriculaMercantil.Visible)
                                        {
                                            vMensaje += (vEntidadAntigua.FechaMatriculaMerc != CalFechaMatriculaMercantil.Date ?
                                           CuerpoCorreoModificaciones(LbFechaMatriculaMercantil.Text, string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.FechaMatriculaMerc), string.Format("{0:dd/MM/yyyy}", CalFechaMatriculaMercantil.Date)) : string.Empty);

                                        }
                                    }
                                }


                                if (PnComplementoPublicas.Visible)
                                {
                                    string vRamaAntiguo = string.Empty;
                                    string vRamaNuevo = ddlRamaoestructura.SelectedItem.Text;
                                    foreach (ListItem li in ddlRamaoestructura.Items)
                                    {
                                        if (li.Value == vEntidadAntigua.IdTipoRamaPublica.ToString())
                                        {
                                            ddlRamaoestructura.SelectedValue = vEntidadAntigua.IdTipoRamaPublica.ToString();
                                            vRamaAntiguo = ddlRamaoestructura.SelectedItem.Text;
                                            break;
                                        }
                                    }                                 
                                    
                                    vMensaje += (vRamaAntiguo != vRamaNuevo ?
                                           CuerpoCorreoModificaciones("Rama o estructura", vRamaAntiguo, vRamaNuevo) : string.Empty);

                                    string vNivelGobiernoAntiguo = string.Empty;
                                    string vNivelGobiernoNuevo = ddlNivelgobierno.SelectedItem.Text;
                                    ddlNivelgobierno.SelectedValue = vEntidadAntigua.IdTipoNivelGob.ToString();
                                    vNivelGobiernoAntiguo = ddlNivelgobierno.SelectedItem.Text;
                                    vMensaje += (vNivelGobiernoAntiguo != vNivelGobiernoNuevo ?
                                           CuerpoCorreoModificaciones("Nivel de gobierno", vNivelGobiernoAntiguo, vNivelGobiernoNuevo) : string.Empty);

                                    string vNivelOrganizacionalAntiguo = string.Empty;
                                    string vNivelOrganizacionalNuevo = ddlNivelOrganizacional.SelectedItem.Text;
                                    ddlNivelOrganizacional.SelectedValue = vEntidadAntigua.IdTipoNivelOrganizacional.ToString();
                                    vNivelOrganizacionalAntiguo = ddlNivelOrganizacional.SelectedItem.Text;
                                    vMensaje += (vNivelOrganizacionalAntiguo != vNivelOrganizacionalNuevo ?
                                           CuerpoCorreoModificaciones("Nivel de gobierno", vNivelOrganizacionalAntiguo, vNivelOrganizacionalNuevo) : string.Empty);

                                    string vTipoEntidadPublicoAntiguo = string.Empty;
                                    string vTipoEntidadPublicoNuevo = ddlTipoEntidadPublica.SelectedItem.Text;
                                    ddlTipoEntidadPublica.SelectedValue = vEntidadAntigua.InfoAdminEntidadProv.IdTipoEntidadPublica.ToString();
                                    vTipoEntidadPublicoAntiguo = ddlTipoEntidadPublica.SelectedItem.Text;
                                    vMensaje += (vTipoEntidadPublicoAntiguo != vTipoEntidadPublicoNuevo ?
                                           CuerpoCorreoModificaciones("Tipo de entidad", vTipoEntidadPublicoAntiguo, vTipoEntidadPublicoNuevo) : string.Empty);


                                }
                                ///FIN COMPLEMENTO DE DATOS BASICOS

                                ///DOMICILIO LEGAL EN COLOMBIA DONDE ESTA CONSTITUIDA
                                string vIdDepartamento = string.Empty;
                                string vIdDepartamentoAntiguo = string.Empty;
                                string vMunicipioConstituidaAntiguo = string.Empty;
                                string vIdMunicipioAntiguo = string.Empty;
                                if (pnlDomicilioLegal.Visible)
                                {
                                    string vDepartamentoConstituidaAntiguo = string.Empty;
                                    string vDepartamentoConstituidaNuevo = string.Empty;
                                    vIdDepartamento = vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida.ToString();
                                    vIdDepartamentoAntiguo = vEntidadAntigua.InfoAdminEntidadProv.IdDepartamentoConstituida.ToString();
                                    ddlDepartamentoConstituida.SelectedValue = vIdDepartamento;
                                    vDepartamentoConstituidaNuevo = ddlDepartamentoConstituida.SelectedItem.Text;
                                    if (vIdDepartamentoAntiguo != "0" && vIdDepartamentoAntiguo != "-1")
                                    {
                                        ddlDepartamentoConstituida.SelectedValue = vIdDepartamentoAntiguo;
                                        vDepartamentoConstituidaAntiguo = ddlDepartamentoConstituida.SelectedItem.Text;
                                    }

                                    vMensaje += (vDepartamentoConstituidaAntiguo != vDepartamentoConstituidaNuevo ?
                                           CuerpoCorreoModificaciones("Departamento", vDepartamentoConstituidaAntiguo, vDepartamentoConstituidaNuevo) : string.Empty);


                                    string vMunicipioConstituidaNuevo = string.Empty;
                                    vIdMunicipioAntiguo = vEntidadAntigua.InfoAdminEntidadProv.IdMunicipioConstituida.ToString();
                                    vMunicipioConstituidaNuevo = ddlMunicipioConstituida.SelectedItem.Text;

                                    if (vMunicipioConstituidaAntiguo != "0" && vMunicipioConstituidaAntiguo != "-1")
                                    {
                                        CargarMunicipioConstituida();
                                        ddlMunicipioConstituida.SelectedValue = vIdMunicipioAntiguo;
                                        vMunicipioConstituidaAntiguo = ddlMunicipioConstituida.SelectedItem.Text;
                                    }

                                    vMensaje += (vMunicipioConstituidaAntiguo != vMunicipioConstituidaNuevo ?
                                           CuerpoCorreoModificaciones("Municipio", vMunicipioConstituidaAntiguo, vMunicipioConstituidaNuevo) : string.Empty);
                                }

                                /// FIN DOMICILIO LEGAL EN COLOMBIA DONDE ESTA CONSTITUIDA 

                                ///DATOS DE UBICACIÓN DIRECCION COMERCIAL O GERENCIA
                                string vDepartamentoDireccionComercialAntiguo = string.Empty;
                                string vDepartamentoDireccionComercialNuevo = string.Empty;

                                vIdDepartamento = vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial.ToString();
                                vIdDepartamentoAntiguo = vEntidadAntigua.InfoAdminEntidadProv.IdDepartamentoDirComercial.ToString();

                                ddlDepartamentoDireccionComercial.SelectedValue = vIdDepartamento;
                                vDepartamentoDireccionComercialNuevo = ddlDepartamentoDireccionComercial.SelectedItem.Text;

                                if (vIdDepartamentoAntiguo != "0" && vIdDepartamentoAntiguo != "-1")
                                {
                                    ddlDepartamentoDireccionComercial.SelectedValue = vIdDepartamentoAntiguo;
                                    vDepartamentoDireccionComercialAntiguo = ddlDepartamentoDireccionComercial.SelectedItem.Text;
                                }

                                vMensaje += (vDepartamentoDireccionComercialAntiguo != vDepartamentoDireccionComercialNuevo ?
                                       CuerpoCorreoModificaciones("Departamento Ubicación", vDepartamentoDireccionComercialAntiguo, vDepartamentoDireccionComercialNuevo) : string.Empty);

                                string vMunicipioDireccionComercialAntiguo = string.Empty;
                                string vMunicipioDireccionComercialNuevo = string.Empty;
                                vIdMunicipioAntiguo = vEntidadAntigua.InfoAdminEntidadProv.IdMunicipioDirComercial.ToString();
                                vMunicipioDireccionComercialNuevo = ddlMunicipioDireccionComercial.SelectedItem.Text;

                                if (vMunicipioConstituidaAntiguo != "0" && vMunicipioConstituidaAntiguo != "-1")
                                {
                                    CargarDepartamentoDireccionComercial();
                                    ddlMunicipioDireccionComercial.SelectedValue = vIdMunicipioAntiguo;
                                    vMunicipioDireccionComercialAntiguo = ddlMunicipioDireccionComercial.SelectedItem.Text;
                                }

                                vMensaje += (vMunicipioDireccionComercialAntiguo != vMunicipioDireccionComercialNuevo ?
                                       CuerpoCorreoModificaciones("Municipio Ubicación", vMunicipioDireccionComercialAntiguo, vMunicipioDireccionComercialNuevo) : string.Empty);

                                //string vZona = string.Empty;
                                //if (vEntidadAntigua.InfoAdminEntidadProv.IdZona == "1")
                                //{
                                //    vZona = "U";
                                //}
                                //else
                                //{
                                //    vZona = "R";
                                //}

                                //vMensaje += (vZona != txtDireccionResidencia.TipoZona ?
                                //       CuerpoCorreoModificaciones("Zona", vZona, txtDireccionResidencia.TipoZona) : string.Empty);

                                if (!string.IsNullOrEmpty(txtDireccionResidencia.TipoZona))
                                {
                                    vMensaje += ((vEntidadAntigua.InfoAdminEntidadProv.IdZona == "1" ? "U" : "R") != txtDireccionResidencia.TipoZona ?
                                                 CuerpoCorreoModificaciones("Zona", vEntidadAntigua.InfoAdminEntidadProv.IdZona == "1" ? "Urbano" : "Rural", txtDireccionResidencia.TipoZona == "U" ? "Urbano" : "Rural") : string.Empty);
                                }

                                if (!string.IsNullOrEmpty(txtDireccionResidencia.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.InfoAdminEntidadProv.DireccionComercial)))
                                {
                                    vMensaje += (vEntidadAntigua.InfoAdminEntidadProv.DireccionComercial != txtDireccionResidencia.Text ?
                                        CuerpoCorreoModificaciones("Direcci&oacute;n", vEntidadAntigua.InfoAdminEntidadProv.DireccionComercial, txtDireccionResidencia.Text) : string.Empty);
                                    txtDireccionResidencia.Text = vEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial;
                                }
                                ///FIN DATOS DE UBICACIÓN DIRECCION COMERCIAL O GERENCIA

                                if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                                {

                                    vMensaje += (vEntidadAntigua.TerceroProveedor.RazonSocial != TxtRazonSocial.Text ?
                                        CuerpoCorreoModificaciones("Razón Social", vEntidadAntigua.TerceroProveedor.RazonSocial, TxtRazonSocial.Text) : string.Empty);

                                    ///REPRESENTANTE LEGAL

                                    vMensaje += (vEntidadAntigua.RepresentanteLegal.PrimerNombre != TxtPrimerNombreRepr.Text ?
                                        CuerpoCorreoModificaciones("Primer nombre", vEntidadAntigua.RepresentanteLegal.PrimerNombre, TxtPrimerNombreRepr.Text) : string.Empty);

                                    if (!string.IsNullOrEmpty(TxtSegundoNombreRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.SegundoNombre)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.SegundoNombre != TxtSegundoNombreRepr.Text ?
                                             CuerpoCorreoModificaciones("Segundo nombre", vEntidadAntigua.RepresentanteLegal.SegundoNombre, TxtSegundoNombreRepr.Text) : string.Empty);

                                    }

                                    vMensaje += (vEntidadAntigua.RepresentanteLegal.PrimerApellido != TxtPrimerApellidoRepr.Text ?
                                        CuerpoCorreoModificaciones("Primer apellido", vEntidadAntigua.RepresentanteLegal.PrimerApellido, TxtPrimerApellidoRepr.Text) : string.Empty);

                                    if (!string.IsNullOrEmpty(TxtSegundoApellidoRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.SegundoApellido)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.SegundoApellido != TxtSegundoApellidoRepr.Text ?
                                          CuerpoCorreoModificaciones("Segundo apellido", vEntidadAntigua.RepresentanteLegal.SegundoApellido, TxtSegundoApellidoRepr.Text) : string.Empty);

                                    }

                                    vMensaje += (vEntidadAntigua.RepresentanteLegal.Sexo != ddlSexoRepLegal.SelectedValue ?
                                        CuerpoCorreoModificaciones("Sexo", vEntidadAntigua.RepresentanteLegal.Sexo, ddlSexoRepLegal.SelectedValue) : string.Empty);

                                    vMensaje += (vEntidadAntigua.RepresentanteLegal.FechaNacimiento != cuFechaNacRepLegal.Date ?
                                        CuerpoCorreoModificaciones("Fecha Nacimiento", string.Format("{0:dd/MM/yyyy}", vEntidadAntigua.RepresentanteLegal.FechaNacimiento), string.Format("{0:dd/MM/yyyy}", cuFechaNacRepLegal.Date)) : string.Empty);

                                    if (!string.IsNullOrEmpty(TxtCelularRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.Celular)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.Celular != TxtCelularRepr.Text ?
                                          CuerpoCorreoModificaciones("Celular Representante", vEntidadAntigua.RepresentanteLegal.Celular, TxtCelularRepr.Text) : string.Empty);

                                    }

                                    if (!string.IsNullOrEmpty(TxtIndicativoRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.Indicativo)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.Indicativo != TxtIndicativoRepr.Text ?
                                          CuerpoCorreoModificaciones("Indicativo Representante", vEntidadAntigua.RepresentanteLegal.Indicativo, TxtIndicativoRepr.Text) : string.Empty);

                                    }

                                    if (!string.IsNullOrEmpty(TxtTelefonoRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.Telefono)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.Telefono != TxtTelefonoRepr.Text ?
                                          CuerpoCorreoModificaciones("Teléfono Representante", vEntidadAntigua.RepresentanteLegal.Telefono, TxtTelefonoRepr.Text) : string.Empty);

                                    }

                                    if (!string.IsNullOrEmpty(TxtExtencionRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.Extension)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.Extension != TxtExtencionRepr.Text ?
                                          CuerpoCorreoModificaciones("Extensión Representante", vEntidadAntigua.RepresentanteLegal.Extension, TxtExtencionRepr.Text) : string.Empty);

                                    }

                                    if (!string.IsNullOrEmpty(TxtCorreoElectronicoRepr.Text) || (!string.IsNullOrEmpty(vEntidadAntigua.RepresentanteLegal.Email)))
                                    {
                                        vMensaje += (vEntidadAntigua.RepresentanteLegal.Email != TxtCorreoElectronicoRepr.Text ?
                                          CuerpoCorreoModificaciones("Email Representante", vEntidadAntigua.RepresentanteLegal.Email, TxtCorreoElectronicoRepr.Text) : string.Empty);

                                    }


                                    ///FIN REPRESENTANTE LEGAL

                                    if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                                    {
                                        vMensaje += (vEntidadAntigua.NumIntegrantes.ToString() != txtNumInt.Text ?
                                        CuerpoCorreoModificaciones("Cantidad Integrantes", vEntidadAntigua.NumIntegrantes.ToString(), txtNumInt.Text) : string.Empty);
                                        txtNumInt.Text = vEntidadProvOferente.NumIntegrantes.ToString();
                                    }
                                }

                                vMensaje += "</table>";

                                bool result = CorreoUtil.EnviarCorreo(vEmailPara, vNombreOrigen, vAsunto, vMensaje, vIdUsuario, vArchivo);

                                RemoveSessionParameter("vEntidadProvOferenteActual");
                            }
                        }
                    }
                }
                else
                {

                    //Se registran los códigos UNSPSC
                    //Luz Angela Borda
                    //14 mayo 2015

                    if (GetSessionParameter("Proveedor.dtUNSPSC") != null)
                    {
                        DataTable dtCodUNSPSC = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");
                        foreach (DataRow cod in dtCodUNSPSC.Rows)
                        {
                            CodigoUNSPSCProveedor vCodigoUNSPSCProveedor = new CodigoUNSPSCProveedor();
                            int vIdTercero = Convert.ToInt32(GetSessionParameter("Proveedor.IdTercero"));

                            vCodigoUNSPSCProveedor.IDTERCERO = vIdTercero;
                            vCodigoUNSPSCProveedor.IdTipoCodUNSPSC = int.Parse(cod["idTipoCodUNSPSC"].ToString());
                            vCodigoUNSPSCProveedor.UsuarioCrea = GetSessionUser().NombreUsuario;
                            vProveedorService.InsertarCodUNSPSCProveedor(vCodigoUNSPSCProveedor);
                        }
                    }

                    vEntidadProvOferente.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.TerceroProveedor.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.TelTerceroProveedor.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.InfoAdminEntidadProv.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vEntidadProvOferente.InfoAdminEntidadProv.UsuarioModifica = GetSessionUser().NombreUsuario;
                    DateTime hoy;
                    int anio, mes, dia;
                    hoy = DateTime.Now;
                    anio = hoy.Year;
                    mes = hoy.Month;
                    if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2)
                    {
                        vEntidadProvOferente.ConsecutivoInterno = "J" + anio.ToString() + mes.ToString("D2") + "-";
                    }
                    else if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1)
                    {
                        vEntidadProvOferente.ConsecutivoInterno = "N" + anio.ToString() + mes.ToString("D2") + "-";
                    }
                    else if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3)
                    {
                        vEntidadProvOferente.ConsecutivoInterno = "C" + anio.ToString() + mes.ToString("D2") + "-";
                    }
                    else if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                    {
                        vEntidadProvOferente.ConsecutivoInterno = "U" + anio.ToString() + mes.ToString("D2") + "-";
                    }

                    if (ddlTipoPersona.SelectedValue.Equals("3") || ddlTipoPersona.SelectedValue.Equals("4"))
                    {
                        vEntidadProvOferente.NumIntegrantes = int.Parse(txtNumInt.Text);
                    }


                    ValidarDocumentosObligatorios(vEntidadProvOferente);

                    InformacionAudioria(vEntidadProvOferente, this.PageName, vSolutionPage);

                    vResultado = vProveedorService.InsertarEntidadProvOferente(vEntidadProvOferente);

                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado >= 1) //Ahora se devuelve uno o dos porque se actualiza el codigo interno
                {
                    SetSessionParameter("EntidadProvOferente.IdEntidad", vEntidadProvOferente.IdEntidad);
                    SetSessionParameter("EntidadProvOferente.Guardado", "1");

                    //  toolBar.MostrarMensajeGuardado("Registros de datos básicos enviados con éxito. Le invitamos a registrar sus datos mínimos de su información financiera");
                    //
                    //Se limpia el IdTemporal usado para subir documentos
                    //
                    ViewState["IdTemporal"] = null;
                    if (Request.QueryString["oP"] == "E")
                    {
                        if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                        {
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript",
                                       "<script>javascript: alert('Registros de datos básicos enviados con éxito. Le invitamos a registrar los datos mínimos de sus integrantes. ');window.location ='DetailDatosBasicos.aspx' ;</script>");
                        }
                        else
                        {
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript",
                                                                   "<script>javascript: alert('Registros de datos básicos enviados con éxito. Le invitamos a registrar sus datos mínimos de su información financiera');window.location ='DetailDatosBasicos.aspx' ;</script>");
                        }

                    }
                    else
                    {
                        if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                        {
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript",
                                     "<script>javascript: alert('Registros de datos básicos enviados con éxito. Le invitamos a registrar los datos mínimos de sus integrantes.');window.location ='Detail.aspx' ;</script>");

                        }
                        else
                        {
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript",
                                                                 "<script>javascript: alert('Registros de datos básicos enviados con éxito. Le invitamos a registrar sus datos mínimos de su información financiera');window.location ='Detail.aspx' ;</script>");
                            GuardaSucursalDefault();
                        }
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError(
                        "La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            if (ex.InnerException != null)
                toolBar.MostrarMensajeError(ex.InnerException.Message);
            else
                toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(BtnGuardarClick);
            toolBar.eventoRetornar += new ToolBarDelegate(BtnRetornarClick);
            toolBar.EstablecerTitulos("Entidad Proveedor", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        var vSeguridadService = new SeguridadService();
        lbQueRegistrar.Text = vSeguridadService.ConsultarParametros(null, null, null, "GestionProveedor") == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametros(null, null, null, "GestionProveedor")[0].ValorParametro;

    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            int vIdTercero = Convert.ToInt32(GetSessionParameter("Proveedor.IdTercero"));

            int vEstado = 0;
            hfIdEntidad.Value = vIdEntidad.ToString();

            lblNota.Visible = true;
            TxtCorreoElectronico.Enabled = false;

            EntidadProvOferente vEntidadProvOferente;
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            vEstado = Convert.ToInt32(vEntidadProvOferente.IdEstado);
            SetSessionParameter("vEntidadProvOferente.IdEstado", vEstado);
            vEntidadProvOferente.TerceroProveedor = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            TxtConsecutivoInterno.Text = vEntidadProvOferente.ConsecutivoInterno;
            vEntidadProvOferente.TelTerceroProveedor = vOferenteService.ConsultarTelTercerosIdTercero(vEntidadProvOferente.IdTercero);
            vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
            vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);
            ddlTipoPersona.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdTipoPersona.ToString();
            FechaICBF.Date = vEntidadProvOferente.FechaIngresoICBF;
            CargarDatosTipoPersona();

            if (vEntidadProvOferente.TerceroProveedor != null)
                TxtCorreoElectronico.Text = vEntidadProvOferente.TerceroProveedor.Email.ToString();
            ddlTipoDocumento.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdDListaTipoDocumento.ToString();
            hdIdTreceroProveedro.Value = vEntidadProvOferente.TerceroProveedor.IdTercero.ToString();
            txtIdentificacion.Text = vEntidadProvOferente.TerceroProveedor.NumeroIdentificacion.ToString();
            TxtRazonSocial.Text = vEntidadProvOferente.TerceroProveedor.RazonSocial;
            TxtPrimerNombre.Text = vEntidadProvOferente.TerceroProveedor.PrimerNombre;
            TxtSegundoNombre.Text = vEntidadProvOferente.TerceroProveedor.SegundoNombre;
            TxtPrimerApellido.Text = vEntidadProvOferente.TerceroProveedor.PrimerApellido;
            TxtSegundoApellido.Text = vEntidadProvOferente.TerceroProveedor.SegundoApellido;
            txtDv.Text = vEntidadProvOferente.TerceroProveedor.DigitoVerificacion.ToString();
            TxtIndicativo.Text = vEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono.ToString();
            TxtTelefono.Text = vEntidadProvOferente.TelTerceroProveedor.NumeroTelefono.ToString();
            TxtExtencion.Text = vEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono.ToString();
            TxtCelular.Text = vEntidadProvOferente.TelTerceroProveedor.Movil.ToString();
            TxtSitioWeb.Text = vEntidadProvOferente.InfoAdminEntidadProv.SitioWeb;
            ddlSector.SelectedValue = vEntidadProvOferente.IdTipoSector.ToString();
            CargarDatosSector();
            ddlRegimenTributario.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib.ToString();
            if (vEntidadProvOferente.FechaConstitucion != null)
            {
                CalFechaConstitucion.Date = Convert.ToDateTime(vEntidadProvOferente.FechaConstitucion);
            }
            if (vEntidadProvOferente.FechaConstitucion != null)
            {
                cvFechaVigencia.ValueToCompare =
                    Convert.ToDateTime(vEntidadProvOferente.FechaConstitucion).ToShortDateString();
            }
            else
            {
                cvFechaVigencia.ValueToCompare = DateTime.Now.ToShortDateString();
            }

            if ((vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1 && vEntidadProvOferente.IdTipoSector == 1) ||
                (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 && vEntidadProvOferente.IdTipoSector == 2))
            {
                llenarRdbLVigencia(true);
            }
            else
            {
                if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona != 3 && vEntidadProvOferente.TerceroProveedor.IdTipoPersona != 4)
                {
                    if (vEntidadProvOferente.TipoVigencia != null)
                        rblVigencia.SelectedValue = Convert.ToString(Convert.ToInt32(vEntidadProvOferente.TipoVigencia));
                    else
                        rblVigencia.SelectedIndex = -1;
                    if (vEntidadProvOferente.TipoVigencia != null)
                    {
                        lbVigenciaHasta.Visible = (bool)vEntidadProvOferente.TipoVigencia;
                        CalFechaVigencia.Visible = (bool)vEntidadProvOferente.TipoVigencia;
                    }

                }
            }

            if (vEntidadProvOferente.FechaVigencia != null)
            {
                CalFechaVigencia.Date = Convert.ToDateTime(vEntidadProvOferente.FechaVigencia);
            }
            TxtNombreEstablecimiento.Text = vEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento;
            ddlTipoEntidad.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad.ToString();
            ddlEstadoValidacion.SelectedValue = vEntidadProvOferente.IdEstado.ToString();
            txtRegistradorPor.Text = vEntidadProvOferente.UsuarioCrea;
            txtFechaRegistro.Text = vEntidadProvOferente.FechaCrea.Value.ToString("dd/MM/yyyy");

            var vTipoCiiu = new TipoCiiu();
            if (vEntidadProvOferente.IdTipoCiiuPrincipal != null)
            {
                vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadProvOferente.IdTipoCiiuPrincipal);
                TxtActividadCiiuPrin.Text = vTipoCiiu.Descripcion;
                hfDesActividadCiiuPrin.Value = vTipoCiiu.Descripcion;
                hfIdActividadCiiuPrin.Value = vTipoCiiu.IdTipoCiiu.ToString();
            }
            if (vEntidadProvOferente.IdTipoCiiuSecundario != null)
            {
                vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadProvOferente.IdTipoCiiuSecundario);
                TxtActividadCiiuSegu.Text = vTipoCiiu.Descripcion;
                hfDesActividadCiiuSegu.Value = vTipoCiiu.Descripcion;
                HfActividadCiiuSegu.Value = vTipoCiiu.IdTipoCiiu.ToString();
            }

            if (vEntidadProvOferente.FechaCiiuPrincipal != null)
            {
                CalFechaActividadPrincipal.Date = Convert.ToDateTime(vEntidadProvOferente.FechaCiiuPrincipal);
            }
            if (vEntidadProvOferente.FechaCiiuSecundario != null)
            {
                CalFechaActividadSegundaria.Date = Convert.ToDateTime(vEntidadProvOferente.FechaCiiuSecundario);
            }
            ddlTipoActividad.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad.ToString();
            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona != 3 && vEntidadProvOferente.TerceroProveedor.IdTipoPersona != 4)
                CargarClasedeEntidad();
            if (vEntidadProvOferente.IdTipoClaseEntidad != null)
                ddlClaseEntidad.SelectedValue = vEntidadProvOferente.IdTipoClaseEntidad.ToString();
            if (vEntidadProvOferente.ExenMatriculaMer != null)
                ddlExtecionMatruculaMercantil.SelectedValue = Convert.ToInt16(vEntidadProvOferente.ExenMatriculaMer).ToString();
            CargarExtecionMatruculaMercantil();
            TxtNumeroMatriculaMercantil.Text = vEntidadProvOferente.MatriculaMercantil;
            if (vEntidadProvOferente.FechaMatriculaMerc != null)
            {
                CalFechaMatriculaMercantil.Date = Convert.ToDateTime(vEntidadProvOferente.FechaMatriculaMerc);
            }
            ddlRamaoestructura.SelectedValue = vEntidadProvOferente.IdTipoRamaPublica.ToString();
            if (!ddlRamaoestructura.SelectedValue.Equals("-1"))
            {
                // se carga dependendencia de ramaoestructura - nivel de gobierno
                CargarNiveldeGobierno(ddlNivelgobierno, Convert.ToInt32(ddlRamaoestructura.SelectedValue));
                ddlNivelgobierno.SelectedValue = vEntidadProvOferente.IdTipoNivelGob.ToString();
                //llenar ddlNivelOrganizacional
                ManejoControles.LlenarNivelOrganizacional(ddlNivelOrganizacional, "-1", true, int.Parse(vEntidadProvOferente.IdTipoRamaPublica.ToString()), int.Parse(vEntidadProvOferente.IdTipoNivelGob.ToString()));
                ddlNivelOrganizacional.SelectedValue = vEntidadProvOferente.IdTipoNivelOrganizacional.ToString();
                //Llenar  ddlTipoEntidadPublica
                ManejoControles.TipoEntidadPublica(ddlTipoEntidadPublica, "-1", true, int.Parse(vEntidadProvOferente.IdTipoRamaPublica.ToString()), int.Parse(vEntidadProvOferente.IdTipoNivelGob.ToString()), int.Parse(vEntidadProvOferente.IdTipoNivelOrganizacional.ToString()));
                ddlTipoEntidadPublica.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica.ToString();
            }
            ddlDepartamentoConstituida.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida.ToString();
            CargarMunicipioConstituida();
            ddlMunicipioConstituida.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida.ToString();
            ddlDepartamentoDireccionComercial.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial.ToString();
            CargarDepartamentoDireccionComercial();
            ddlMunicipioDireccionComercial.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial.ToString();
            if (!string.IsNullOrEmpty(vEntidadProvOferente.InfoAdminEntidadProv.IdZona))
            {
                if (vEntidadProvOferente.InfoAdminEntidadProv.IdZona == "1")
                {
                    txtDireccionResidencia.TipoZona = "U";
                }
                else
                {
                    txtDireccionResidencia.TipoZona = "R";
                }
            }
            if (!string.IsNullOrEmpty(vEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial))
            {
                txtDireccionResidencia.Text = vEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial;
            }
            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
            {
                vEntidadProvOferente.RepresentanteLegal = new Tercero();
                if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
                {
                    vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);
                }
                ddlTipoDocumentoRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
                txtIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
                TxtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
                TxtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                TxtPrimerNombreRepr.Enabled = false;
                TxtSegundoNombreRepr.Enabled = false;
                TxtPrimerApellidoRepr.Enabled = false;
                TxtSegundoApellidoRepr.Enabled = false;
                TxtCorreoElectronicoRepr.Text = vEntidadProvOferente.RepresentanteLegal.Email;
                hfRepreLegal.Value = vEntidadProvOferente.RepresentanteLegal.IdTercero.ToString();
                vEntidadProvOferente.TelRepresentanteLegal =
                    vOferenteService.ConsultarTelTercerosIdTercero(Convert.ToInt32(vEntidadProvOferente.RepresentanteLegal.IdTercero));
                TxtCelularRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.Movil.ToString();
                TxtIndicativoRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.IndicativoTelefono.ToString();
                TxtTelefonoRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.NumeroTelefono.ToString();
                TxtExtencionRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.ExtensionTelefono.ToString();
                hfTelTelRepreLega.Value = vEntidadProvOferente.TelRepresentanteLegal.IdTelTercero.ToString();
                if (!string.IsNullOrEmpty(vEntidadProvOferente.RepresentanteLegal.Sexo))
                    ddlSexoRepLegal.SelectedValue = vEntidadProvOferente.RepresentanteLegal.Sexo;
                if (vEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                    cuFechaNacRepLegal.Date = DateTime.Parse(vEntidadProvOferente.RepresentanteLegal.FechaNacimiento.ToString());
            }

            ///ANTERIOR;EMTE solo estabas asignando el OBJETO para PERSONA NATURAL.... CHECANDO.
            //else if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1)
            //    //---------------Copiamos la EntProvOferente en la Variable de clase, para que comparemos al momento de guardar
            //    //---------------Y asi saber si ha habido cambios en algunos campos, para detonar la validación pertinente.
            vEntidadProvOferenteAEditar = vEntidadProvOferente;

            if ((ddlTipoPersona.SelectedValue.Equals("2") || ddlTipoPersona.SelectedValue.Equals("3") || ddlTipoPersona.SelectedValue.Equals("4")) && ddlSector.SelectedValue.Equals("1") && ddlTipoEntidad.SelectedValue.Equals("2"))
            {
                ddlClaseEntidad.Visible = false;
                lblClaseEntidad.Visible = false;
                rvddlClaseEntidad.Enabled = false;
            }

            //
            DeshabilitaParaEdicion(vEntidadProvOferente);
            if (vEntidadProvOferente.IdTipoPersona == 2 || vEntidadProvOferente.IdTipoPersona == 3 || vEntidadProvOferente.IdTipoPersona == 4)
            {
                lblClaseEntidad.Text = "Clase de Entidad *";
            }

            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
            {
                txtNumInt.Text = vEntidadProvOferente.NumIntegrantes.ToString();
                TxtCorreoElectronico.Enabled = false;
                CalFechaConstitucion.Enabled = false;
                TxtPrimerNombreRepr.Enabled = false;
                TxtSegundoNombreRepr.Enabled = false;
                TxtPrimerApellidoRepr.Enabled = false;
                TxtSegundoApellidoRepr.Enabled = false;
                ddlSexoRepLegal.Enabled = false;
                cuFechaNacRepLegal.Enabled = false;
                rvFechaConstitucion.Enabled = false;
                cvFechaConstitucion.Enabled = false;
                cvFechaNacRepLegal.Enabled = false;
                CalFechaConstitucion.Controlar_cvFecha = false;
                cuFechaNacRepLegal.NoChecarFormato();
                if (vEntidadProvOferente.Estado.Equals("VALIDADO"))
                    txtNumInt.Enabled = false;
            }

            ///Se valida los estados Por Ajustar y Por Validar con el fin de permitir la edición.
            ///Caso de uso 0074 Gestión de Proveedores.
            if (vEntidadProvOferente.IdEstado.ToString() == "1" || vEntidadProvOferente.IdEstado.ToString() == "3")
            {
                TxtPrimerNombre.Enabled = true;
                TxtSegundoNombre.Enabled = true;
                TxtPrimerApellido.Enabled = true;
                TxtSegundoApellido.Enabled = true;
                TxtRazonSocial.Enabled = true;
                ddlSector.Enabled = true;
            }


            //Llenamos las grillas de los documentos
            BuscarDocumentosDatosBasic_ByEntidad();
            BuscarDocumentosRegTercero_ByTercero();

            //Se cargan codigos de UNSPSC
            List<CodigoUNSPSCProveedor> listaUNSPSC = vProveedorService.ConsultarCodigoUNSPSCProveedor(vIdTercero);
            gvCodigoUNSPSC.DataSource = listaUNSPSC;
            gvCodigoUNSPSC.DataBind();

            vUNSPSCActual = listaUNSPSC;

            dtUNSPSC = CrearEstructuraUNSPSC();
            foreach (CodigoUNSPSCProveedor cod in listaUNSPSC)
            {
                DataRow drCod = dtUNSPSC.NewRow();
                drCod["idTipoCodUNSPSC"] = cod.IdTipoCodUNSPSC;
                drCod["codigo"] = cod.Codigo;
                drCod["descripcion"] = cod.Descripcion;
                drCod["existe"] = true;
                dtUNSPSC.Rows.Add(drCod);
            }
            dtUNSPSC.AcceptChanges();

            Session["vEntidadProvOferenteActual"] = vEntidadProvOferente;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }



    private void DeshabilitaParaEdicion(EntidadProvOferente objEdit)
    {
        if (objEdit.Estado.Equals("VALIDADO"))
        {
            if (objEdit.TerceroProveedor.IdTipoPersona == 2 && objEdit.IdTipoSector == 1)
            {
                CalFechaConstitucion.Enabled = false;
                ddlTipoEntidad.Enabled = false;
                ddlTipoActividad.Enabled = false;
                ddlSector.Enabled = false;
                cvFechaConstitucion.Enabled = false;
            }
            else if (objEdit.TerceroProveedor.IdTipoPersona == 2 && objEdit.IdTipoSector == 2)
            {
                CalFechaConstitucion.Enabled = false;
                ddlTipoEntidad.Enabled = false;
                rblVigencia.Enabled = false;
                ddlSector.Enabled = false;
                cvFechaConstitucion.Enabled = false;
            }
            else if (objEdit.TerceroProveedor.IdTipoPersona == 1 && objEdit.IdTipoSector == 1)
            {
                CalFechaConstitucion.Enabled = false;
                ddlTipoEntidad.Enabled = false;
                rblVigencia.Enabled = false;
                ddlTipoActividad.Enabled = false;
                cvFechaConstitucion.Enabled = false;
            }
        }
    }
    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            var terceroProveedor = vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario);
            if (terceroProveedor.Rol.Split(';').Contains("AdministradorSIA"))
            {
                this.lbEstadoValidacion.Visible = true;
                this.ddlEstadoValidacion.Visible = true;
                this.lnkbtnAuditoria.Visible = true;
                this.llenarGridAuditoria();
                this.txtObservacionesCorreccion.Visible = true;
                this.rfvlblObservacionesCorreccion.Enabled = true;
                this.pObservacionesCorreccion.Visible = true;
            }
            else
            {
                this.lbEstadoValidacion.Visible = false;
                this.ddlEstadoValidacion.Visible = false;
                this.lnkbtnAuditoria.Visible = false;
                this.txtObservacionesCorreccion.Visible = false;
                this.rfvlblObservacionesCorreccion.Enabled = false;
                this.pObservacionesCorreccion.Visible = false;
            }


            int VidTercero;
            if (GetSessionParameter("Proveedor.IdTercero") != "" && GetSessionParameter("Proveedor.IdTercero") != null)
            {
                VidTercero = Convert.ToInt32(GetSessionParameter("Proveedor.IdTercero"));
                hfIdTercero.Value = VidTercero.ToString();
                if (hfIdTercero.Value != "")
                {
                    vEntidadProvOferente.TerceroProveedor = vOferenteService.ConsultarTercero(VidTercero);
                    vEntidadProvOferente.TelTerceroProveedor = vOferenteService.ConsultarTelTercerosIdTercero(VidTercero);
                }
            }

            hfTipoPersona.Value = vEntidadProvOferente.TerceroProveedor.IdTipoPersona.ToString();


            CalFechaVigencia.ErrorMessage = "Registre la fecha límite de la vigencia";
            CalFechaActividadPrincipal.ErrorMessage = "Registre la fecha de inicio de la actividad principal CIIU";
            CalFechaActividadSegundaria.ErrorMessage = "Registre la fecha de inicio de la actividad Segundaria CIIU";
            CalFechaMatriculaMercantil.ErrorMessage = "Registre la fecha de matrícula mercantil";
            CalFechaConstitucion.ErrorMessage = "Registre la Fecha constitución o de establecimiento de comercio";
            CalFechaActividadSegundaria.InitNull = true;
            CalFechaActividadPrincipal.InitNull = true;
            CalFechaConstitucion.InitNull = true;
            CalFechaVigencia.InitNull = true;
            CalFechaMatriculaMercantil.InitNull = true;


            ResetLista(ddlNivelgobierno);
            ResetLista(ddlNivelOrganizacional);
            ResetLista(ddlTipoEntidadPublica);

            txtDireccionResidencia.ZonaErrorMessage = "Seleccione la zona de Ubicación de Dirección comercial o de gerencia";
            txtDireccionResidencia.DireccionErrorMessage = "Registre la Dirección comercial o de gerencia";
            cvFechaConstitucion.ValueToCompare = DateTime.Now.ToString("dd-MM-yyyy");
            cvFechaVigencia.ValueToCompare = DateTime.Now.ToString("dd-MM-yyyy");
            cvFechaActividadPrincipal.ValueToCompare = DateTime.Now.ToString("dd-MM-yyyy");
            cvFechaActividadSegundaria.ValueToCompare = DateTime.Now.ToString("dd-MM-yyyy");
            cvFechaMatriculaMercantil.ValueToCompare = DateTime.Now.ToString("dd-MM-yyyy");
            // lista documento reprecentante legal
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDocumentoRepr, "-1", true);
            // lista tipo sector publico o privado 
            ManejoControles.LlenarTipoSectorEntidad(ddlSector, "-1", true);
            // Lisata tipo persona Natura o Juridica
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            // Lisata todos los de partamento  Domicilio legal en Colombia Donde está constituida
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamentoConstituida, "-1", true);
            //Lista Rama o estructura
            ManejoControles.LlenarRamaoestructuraAll(ddlRamaoestructura, "-1", true);
            // Lista Nivel Gobierno 
            //ManejoControles.LlenarNivelGobierno(ddlNivelgobierno, "-1", true);
            // lista NivelOrganizacional
            //ManejoControles.LlenarNivelOrganizacional(ddlNivelOrganizacional, "-1", true);
            //lista TipoEntidadPublica ddlTipoEntidadPublica
            //ManejoControles.TipoEntidadPublica(ddlTipoEntidadPublica, "-1", true);
            ddlTipoPersona.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdTipoPersona.ToString();
            if (ddlTipoPersona.SelectedValue == "1")
            {
                ddlSector.SelectedValue = "1";
                Label1.Text = @"Complemento de datos básicos";
                this.txtNombreComercial.Text = @"Nombre comercial";
            }
            ddlSector.Enabled = ddlTipoPersona.SelectedValue == "2";
            //Lista Tipo de Entidad  Nacional Extranjera
            llenarddlTipoEntidad();
            // Lisata TipoActividad ddlTipoActividad
            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1)
            {
                ManejoControles.TipoActividad(ddlTipoActividad, "1", true);
                CargarClasedeEntidad();
            }
            else if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2)
                ManejoControles.TipoActividad(ddlTipoActividad, "-1", true);
            else
            {
                Label5.Text = "Representante Legal y/o Mandatario debidamente facultado";
                ddlTipoEntidad.TabIndex = 100;
                txtNumInt.TabIndex = 22;
            }
            // lista documento reprecentante legal
            ManejoControles.LlenarEstadoDatosBasicos(ddlEstadoValidacion, "1", true);
            //Exención Matricula Mercantil 
            //ddlExtecionMatruculaMercantil.Items.Insert(-1, new ListItem("Seleccione..", "2"));
            //ddlExtecionMatruculaMercantil.Items.Insert(0, new ListItem("NO", "0"));
            //ddlExtecionMatruculaMercantil.Items.Insert(1, new ListItem("SI", "1"));
            //ddlExtecionMatruculaMercantil.SelectedValue = "-1";
            // Lisata todos los de partamento  Datos de Ubicación/Dirección comercial o de gerencia
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamentoDireccionComercial, "-1", true);



            //Se inicializa la variable temporal para adjuntar documentos 
            if (ViewState["IdTemporal"] == null)
            {
                ViewState["IdTemporal"] = DateTime.Now.Ticks.ToString();
            }


            if (hfIdTercero.Value != "")
            {
                if (vEntidadProvOferente.TerceroProveedor != null)
                {
                    ddlTipoPersona.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdTipoPersona.ToString();

                    CargarDatosTipoPersona();

                    TxtCorreoElectronico.Text = vEntidadProvOferente.TerceroProveedor.Email.ToString();
                    ddlTipoDocumento.SelectedValue =
                        vEntidadProvOferente.TerceroProveedor.IdDListaTipoDocumento.ToString();
                    hdIdTreceroProveedro.Value = vEntidadProvOferente.TerceroProveedor.IdTercero.ToString();
                    txtIdentificacion.Text = vEntidadProvOferente.TerceroProveedor.NumeroIdentificacion.ToString();
                    TxtRazonSocial.Text = vEntidadProvOferente.TerceroProveedor.RazonSocial;
                    TxtPrimerNombre.Text = vEntidadProvOferente.TerceroProveedor.PrimerNombre;
                    TxtSegundoNombre.Text = vEntidadProvOferente.TerceroProveedor.SegundoNombre;
                    TxtPrimerApellido.Text = vEntidadProvOferente.TerceroProveedor.PrimerApellido;
                    TxtSegundoApellido.Text = vEntidadProvOferente.TerceroProveedor.SegundoApellido;
                    FechaICBF.Date = vEntidadProvOferente.FechaIngresoICBF;
                    TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
                    if (tipoper.CodigoTipoPersona == "001")
                    {

                        if (string.IsNullOrEmpty(vEntidadProvOferente.TerceroProveedor.PrimerNombre))
                        {
                            TxtPrimerNombre.Enabled = true;
                            CustomValidator2.Enabled = true;
                        }
                        if (string.IsNullOrEmpty(vEntidadProvOferente.TerceroProveedor.SegundoNombre))
                            TxtSegundoNombre.Enabled = true;
                        if (string.IsNullOrEmpty(vEntidadProvOferente.TerceroProveedor.PrimerApellido))
                            TxtPrimerApellido.Enabled = true;
                        if (string.IsNullOrEmpty(vEntidadProvOferente.TerceroProveedor.SegundoApellido))
                            TxtSegundoApellido.Enabled = true;

                        //Desactiva RfNombreEstablecimiento para campo Nombre Comercial cuando Dropdownlist Tipo de persona o asociacion es Natural
                        this.RfNombreEstablecimiento.Enabled = false;
                        //this.RfNombreEstablecimiento.IsValid = true;
                    }
                    else if (tipoper.CodigoTipoPersona == "002" || tipoper.CodigoTipoPersona == "003" || tipoper.CodigoTipoPersona == "004")
                    {
                        //SE TRATA DE PERSONA JURÍDICA , CONSORCIO O UNIÓN TEMPORAL
                        //llenar Lista de Sexo de Representante legal.
                        ddlSexoRepLegal.Items.Insert(0, new ListItem("Ninguno", "-1"));
                        ddlSexoRepLegal.Items.Insert(1, new ListItem("Masculino", "M"));
                        ddlSexoRepLegal.Items.Insert(2, new ListItem("Femenino", "F"));
                        ddlSexoRepLegal.SelectedValue = "-1";
                        DateTime fechaToCompare = DateTime.Now.AddYears(-18);
                        cuFechaNacRepLegal.HabilitarObligatoriedadYMessage("Registre la fecha de nacimiento", true);
                        cuFechaNacRepLegal.fechasMenoresA(DateTime.Now);
                        cvFechaNacRepLegal.ValueToCompare = fechaToCompare.Date.ToShortDateString();

                        //Activa RfNombreEstablecimiento para campo Nombre Comercial cuando Dropdownlist Tipo de persona o asociacion es diferente a Natural
                        this.RfNombreEstablecimiento.Enabled = true;
                        //this.RfNombreEstablecimiento.IsValid = false;
                    }

                    txtDv.Text = vEntidadProvOferente.TerceroProveedor.DigitoVerificacion.ToString();
                }
                if (vEntidadProvOferente.TerceroProveedor != null)
                {
                    TxtIndicativo.Text = vEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono.ToString();
                    TxtTelefono.Text = vEntidadProvOferente.TelTerceroProveedor.NumeroTelefono.ToString();
                    TxtExtencion.Text = vEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono.ToString();
                    TxtCelular.Text = vEntidadProvOferente.TelTerceroProveedor.Movil.ToString();
                }
            }

            //llena tipo de vigencia 
            if ((vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1 && ddlSector.SelectedValue == "1") ||
                (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 && ddlSector.SelectedValue == "2"))
                llenarRdbLVigencia(true);
            else
                llenarRdbLVigencia(false);


            if (ddlTipoPersona.SelectedValue.Equals("2") && ddlSector.SelectedValue.Equals("1") && ddlTipoEntidad.SelectedValue.Equals("2"))
            {
                ddlClaseEntidad.Visible = false;
                lblClaseEntidad.Visible = false;
            }

            // Cargar documentos a Adjuntar    
            //Llenamos las grillas de los documentos
            BuscarDocumentosDatosBasic_ByIdTemporal();
            if (ddlTipoPersona.SelectedValue.Equals("2"))
            {
                gvDocDatosBasicoProv.DataSource = null;
                gvDocDatosBasicoProv.DataBind();
            }
            BuscarDocumentosRegTercero_ByTercero();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void DdlTipoPersonaSelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDatosTipoPersona();
        ddlClaseEntidadVisible();
        CargarClasedeEntidad();

    }

    /// <summary>
    /// Carga datos dependiendo del tipo de persona
    /// </summary>
    protected void CargarDatosTipoPersona()
    {
        CargarRegimenTributario();
        CargarTipoDocumentotipoPersona();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        txtDv.Visible = tipoper.CodigoTipoPersona == "001";
        lblDV.Visible = tipoper.CodigoTipoPersona == "001";
        if (tipoper.CodigoTipoPersona == "001")
        {
            ViewControlJuridica(false);
            ViewControlNatural(true);
            lblClaseEntidad.Text = "Clase de Entidad o Establecimiento * ";
        }
        else
        {
            ViewControlNatural(false);
        }

        if (tipoper.CodigoTipoPersona == "002")
        {
            ViewControlJuridica(true);
            lblClaseEntidad.Text = "Clase de Entidad * ";
        }
        else if (tipoper.CodigoTipoPersona == "003" || tipoper.CodigoTipoPersona == "004")
        {
            ViewControlJuridica(false);
            ViewControlConsorcioOUnion(true);
            lblClaseEntidad.Text = "Clase de Entidad * ";

        }

        rvTelefono.Enabled = ddlTipoPersona.SelectedValue == "2";
        rvIndicativo.Enabled = ddlTipoPersona.SelectedValue == "2";

        rvCelular.Enabled = ddlTipoPersona.SelectedValue == "1" || ddlTipoPersona.SelectedValue == "3" || ddlTipoPersona.SelectedValue == "4";
        ddlSector.Enabled = ddlTipoPersona.SelectedValue == "1";
        PnlReprLegal.Visible = ddlTipoPersona.SelectedValue == "2" || ddlTipoPersona.SelectedValue == "3" || ddlTipoPersona.SelectedValue == "4";
        PnComplementoPrivada.Visible = ddlTipoPersona.SelectedValue == "1";
        if (ddlTipoPersona.SelectedValue == "1")
        {
            ddlSector.SelectedValue = "1";

        }
        ddlSector.Enabled = ddlTipoPersona.SelectedValue == "2";

    }

    /// <summary>
    /// Habilita controles para persona tipo Natural
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlNatural(bool state)
    {
        PnlNatural.Visible = state;
        PnlReprLegal.Visible = state;
        lblCelularOblig.Visible = state;
        rvTipoEntidad.Enabled = state;
        lblTipoEntidad.Visible = state;
        ddlTipoEntidad.Visible = state;
        pnlDomicilioLegal.Visible = state;
        rvDepartamentoConstituida.Enabled = state;
        rvMunicipioConstituida.Enabled = state;
        lblSector.Visible = state;
        rvSector.Enabled = state;
        ddlSector.Visible = state;
        ddlRegimenTributario.Visible = state;
        lblRegTrib.Visible = state;
        rvRegimenTributario.Enabled = state;
        lblVigencia.Visible = state;
        rblVigencia.Visible = state;
        rfvEstado.Enabled = state; // rfvVigencia
        //Activa RFV RfNombreEstablecimiento Tipo persona natural
        this.RfNombreEstablecimiento.Enabled = state;
    }

    /// <summary>
    /// Habilita controles para persona jurídica
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlJuridica(bool state)
    {
        LblRazonSocial.Visible = state;
        TxtRazonSocial.Visible = state;
        PnlReprLegal.Visible = state;
        txtDv.Visible = state;
        lblDV.Visible = state;
        lblTelefonoOblig.Visible = state;
        lblVigencia.Visible = state;
        rfvEstado.Enabled = state; // rfvVigencia
        rblVigencia.Visible = state;
        rvTipoEntidad.Enabled = state;
        lblTipoEntidad.Visible = state;
        ddlTipoEntidad.Visible = state;
        lblSector.Visible = state;
        rvSector.Enabled = state;
        ddlSector.Visible = state;
        ddlRegimenTributario.Visible = state;
        lblRegTrib.Visible = state;
        rvRegimenTributario.Enabled = state;
        pnlDomicilioLegal.Visible = state;
        rvDepartamentoConstituida.Enabled = state;
        rvMunicipioConstituida.Enabled = state;
        lblObligTelRep.Visible = state;
        //Activa RFV RfNombreEstablecimiento Tipo persona natural
        this.RfNombreEstablecimiento.Enabled = state;
    }

    /// <summary>
    /// Habilita controles para persona Consorcio o Unión Temporal
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlConsorcioOUnion(bool state)
    {
        LblRazonSocial.Visible = state;
        TxtRazonSocial.Visible = state;
        PnlReprLegal.Visible = state;
        txtDv.Visible = state;
        lblDV.Visible = state;
        lblNumInt.Visible = state;
        compValidTxtNumInt.Enabled = state;
        txtNumInt.Visible = state;
        rfvTxtNumInt.Enabled = state;
        lblCelularOblig.Visible = state;

    }


    /// <summary>
    /// Carga municipio en lista desplegable
    /// </summary>
    protected void CargarMunicipioConstituida()
    {
        CargarMunicipioXDepartamento(ddlMunicipioConstituida, Convert.ToInt32(ddlDepartamentoConstituida.SelectedValue));
        ddlDepartamentoDireccionComercial.SelectedValue = ddlDepartamentoConstituida.SelectedValue;
    }
    /// <summary>
    /// Carga municipio por departamento en lista desplegable
    /// </summary>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipioConstituida, int pidDepartament)
    {
        //ManejoControles Controles = new ManejoControles();
        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipioConstituida, "-1", true, pidDepartament);
    }
    /// <summary>
    /// Carga datos en lista desplegable
    /// </summary>
    protected void CargarRegimenTributario()
    {
        if (ddlTipoPersona.SelectedValue.Equals("1"))
            ManejoControles.LlenarRegimenTributario(ddlRegimenTributario, Convert.ToInt32(ddlTipoPersona.SelectedValue), "-1", true);
        else if (ddlTipoPersona.SelectedValue.Equals("2"))
            ManejoControles.LlenarRegimenTributario(ddlRegimenTributario, Convert.ToInt32(ddlTipoPersona.SelectedValue), null, false);
    }

    /// <summary>
    /// Carga Tpo de documento dependiendo el tipo de persona
    /// </summary>
    protected void CargarTipoDocumentotipoPersona()
    {
        string idTipoPersona = new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        var tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDocumento, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDocumento, "-1", true);
        }

    }

    /// <summary>
    /// Carga datos del sectos
    /// </summary>
    protected void CargarDatosSector()
    {
        var tipoSetor = Convert.ToInt32(ddlSector.SelectedValue);
        PnComplementoPrivada.Visible = tipoSetor == 1;
        PnComplementoPublicas.Visible = tipoSetor == 2;
        rvddlRamaoestructura.Enabled = tipoSetor == 2;
        rvddlNivelgobierno.Enabled = tipoSetor == 2;
        rvddlNivelOrganizacional.Enabled = tipoSetor == 2;
        rvddlTipoEntidadPublica.Enabled = tipoSetor == 2;
    }

    /// <summary>
    /// Llena valores de lista desplegable Clase Entidad
    /// </summary>
    protected void CargarClasedeEntidad()
    {
        int? vIdRegimenTributario;
        if (ddlRegimenTributario.SelectedValue != "")
        {
            vIdRegimenTributario = Convert.ToInt32(ddlRegimenTributario.SelectedValue);
        }
        else
        {
            vIdRegimenTributario = null;
        }

        ManejoControles.LlenarClasedeEntidadTipodeActividadTipoEntidad(ddlClaseEntidad, Convert.ToInt32(ddlTipoActividad.SelectedValue), Convert.ToInt32(ddlTipoEntidad.SelectedValue), "-1", true, int.Parse(ddlTipoPersona.SelectedValue), int.Parse(ddlSector.SelectedValue), vIdRegimenTributario);
    }
    protected void CargarExtecionMatruculaMercantil()
    {
        rvTxtNumeroMatriculaMercantil.Enabled = Convert.ToInt32(ddlExtecionMatruculaMercantil.SelectedValue) == 0;
        cvFechaMatriculaMercantil.Enabled = Convert.ToInt32(ddlExtecionMatruculaMercantil.SelectedValue) == 0;
        if (ddlExtecionMatruculaMercantil.SelectedValue == "1")
        {
            TxtNumeroMatriculaMercantil.Visible = false;
            CalFechaMatriculaMercantil.Visible = false;
            LBNumeroMatriculaMercantil.Visible = false;
            LbFechaMatriculaMercantil.Visible = false;
        }
        else
        {
            TxtNumeroMatriculaMercantil.Visible = true;
            CalFechaMatriculaMercantil.Visible = true;
            LBNumeroMatriculaMercantil.Visible = true;
            LbFechaMatriculaMercantil.Visible = true;
        }
    }

    /// <summary>
    /// Llena de valoress a lista desplegable Departamento dirección comercial
    /// </summary>
    protected void CargarDepartamentoDireccionComercial()
    {
        CargarMunicipioXDepartamento(ddlMunicipioDireccionComercial, Convert.ToInt32(ddlDepartamentoDireccionComercial.SelectedValue));
    }

    protected void tcInfoProveedor_ActiveTabChanged(object sender, EventArgs e)
    {
        toolBar = (masterPrincipal)this.Master;
        TabContainer tab = (TabContainer)sender;
        switch (tab.ActiveTabIndex)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                toolBar.FindControl("btnGuardar").Visible = false;
                break;
            case 5:
                break;
        }
    }

    /// <summary>
    /// Corta una cadena hacia izquierda
    /// </summary>
    /// <param name="param"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    private static string Left(string param, int length)
    {
        string result = param.Substring(0, length);
        return result;
    }

    /// <summary>
    /// Corta una cadena hacia derecha
    /// </summary>
    /// <param name="param"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    private static string Right(string param, int length)
    {

        int value = param.Length - length;
        string result = param.Substring(value, length);
        return result;
    }

    /// <summary>
    /// Valida indicativo
    /// </summary>
    /// <param name="objSource"></param>
    /// <param name="objArgs"></param>
    public void ValidaIndicativoserv(Object objSource, ServerValidateEventArgs objArgs)
    {
        if (TxtTelefonoRepr.Text != "" && TxtIndicativoRepr.Text == "")
        {
            rfvIndicativoRepr.Enabled = true;
            rfvIndicativoRepr.IsValid = false;
        }
        else
        {
            rfvIndicativoRepr.Enabled = false;
            rfvIndicativoRepr.IsValid = true;
        }

    }
    //protected void TxtTelefonoRepr_TextChanged(object sender, EventArgs e)
    //{
    //    if (TxtTelefonoRepr.Text.Length == 7)
    //    {
    //        if (TxtTelefonoRepr.Text != "" && TxtIndicativoRepr.Text == "")
    //        {
    //            rfvIndicativoRepr.Enabled = true;
    //            rfvIndicativoRepr.IsValid = false;
    //        }
    //        else
    //        {
    //            rfvIndicativoRepr.Enabled = false;
    //            rfvIndicativoRepr.IsValid = true;
    //        }
    //    }
    //}

    /// <summary>
    /// Autor: Ingenian Software - Cristhian López - 23/03/2017
    /// Método que guarda las observaciones realizadas, según las modificaciones al tercero.
    /// </summary>
    /// <returns>Resultado de la operación.</returns>
    private bool GuardarObservacionesCorreccion()
    {
        try
        {
            int vResultado;
            string vObservaciones = string.Empty;

            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));

            ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();

            Int32 iIdEntidad = Convert.ToInt32(vIdEntidad);

            //obtiene la ultima observacion disponible
            vValidarInfoDatosBasicosEntidad = vProveedorService.ConsultarValidarInfoDatosBasicosEntidad_Ultima(iIdEntidad);

            vValidarInfoDatosBasicosEntidad.IdEntidad = iIdEntidad;
            vValidarInfoDatosBasicosEntidad.NroRevision = vValidarInfoDatosBasicosEntidad.NroRevision;
            vValidarInfoDatosBasicosEntidad.Observaciones = txtObservacionesCorreccion.Text;
            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = true;
            int idEstado = 0;
            //Se obtiene el estado Validado
            idEstado = 2;
            vValidarInfoDatosBasicosEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
            vResultado = vProveedorService.InsertarValidarInfoDatosBasicosEntidad(vValidarInfoDatosBasicosEntidad, idEstado);
            if (vResultado > 0)
            {
                return true;
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                return false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Autor: Ingenian Software - Cristhian López - 23/03/2017
    /// Metodo que concatena el cuerpo del mensaje para enviar el correo de las modificaciones realizadas al tercero.
    /// </summary>
    /// <param name="pNombreCampo">Nombre del campo que se modificó</param>
    /// <param name="pCampoActual">Valor modificado del campo</param>
    /// <param name="pCampoNuevo">Nuevo valor del campo modificado.</param>
    /// <returns>Retorna un string con el cuerpo del mensaje concatenado</returns>
    private string CuerpoCorreoModificaciones(string pNombreCampo, string pCampoActual, string pCampoNuevo)
    {
        string MensajeCorreo = string.Empty;
        MensajeCorreo = "<tr> <td style='border: 1px solid #000000; width:40%;'> " + pNombreCampo + "</td>" +
                       " <td style='border: 1px solid #000000; width:30%;'>" + pCampoActual + "</td>" +
                       "<td style='border: 1px solid #000000; width:30%;'>" + pCampoNuevo + "</td>" +
                       "</tr>";
        return MensajeCorreo;
    }

    #endregion

    #region documentos

    protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;

        BuscarDocumentosDatosBasic_ByIdTemporal();
    }

    //protected void gvDocDatosBasicoProv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{

    //    try
    //    {
    //        GridViewRow row = gvDocDatosBasicoProv.Rows[e.RowIndex];
    //        int idDocAdjunto = (int)gvDocDatosBasicoProv.DataKeys[e.RowIndex].Value;
    //        DocDatosBasicoProv doc = vProveedorService.ConsultarDocDatosBasicoProv(idDocAdjunto);
    //        vProveedorService.EliminarDocDatosBasicoProv(doc);
    //        BuscarDocumentos();

    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }

    //}

    protected void gvDocDatosBasicoProv_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDocDatosBasicoProv.EditIndex = e.NewEditIndex;
        BuscarDocumentosDatosBasic_ByIdTemporal();
    }

    protected void gvDocDatosBasicoProv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDocDatosBasicoProv.EditIndex = -1;
        BuscarDocumentosDatosBasic_ByIdTemporal();
    }

    protected void gvDocDatosBasicoProv_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow row = gvDocDatosBasicoProv.Rows[e.RowIndex];

            int vIdDocAdjunto = int.Parse(gvDocDatosBasicoProv.DataKeys[e.RowIndex]["IdDocAdjunto"].ToString());
            int vMaxPermitidoKB = int.Parse(gvDocDatosBasicoProv.DataKeys[e.RowIndex]["MaxPermitidoKB"].ToString());
            string vExtensionesPermitidas = gvDocDatosBasicoProv.DataKeys[e.RowIndex]["ExtensionesPermitidas"].ToString();

            DocDatosBasicoProv doc = new DocDatosBasicoProv();
            if (vIdDocAdjunto > 0)
            {
                doc = vProveedorService.ConsultarDocDatosBasicoProv(vIdDocAdjunto);
            }
            else
            {
                //int vIdInfoFin = Convert.ToInt32(GetSessionParameter("InfoFinancieraEntidad.IdInfoFin"));

                if (!string.IsNullOrEmpty(hfIdEntidad.Value))
                {
                    int vIdEntidad = Convert.ToInt32(hfIdEntidad.Value);
                    doc.IdEntidad = vIdEntidad;
                }
                else
                {
                    //Se asocia un Id temporal 
                    doc.IdTemporal = ViewState["IdTemporal"].ToString();
                }
                doc.NombreDocumento = ((Label)(row.Cells[0].FindControl("lblNombreDocumento"))).Text;
                doc.IdTipoDocumento = int.Parse(gvDocDatosBasicoProv.DataKeys[e.RowIndex]["IdTipoDocumento"].ToString());
                doc.FechaCrea = DateTime.Now;
                doc.UsuarioCrea = GetSessionUser().NombreUsuario;

            }
            //doc.LinkDocumento = ((TextBox)(row.Cells[3].FindControl("txtLinkDocumento"))).Text;        
            FileUpload fuDocumento = ((FileUpload)(row.Cells[1].FindControl("fuDocumento")));

            //Se envia por ftp
            doc.LinkDocumento = SubirArchivo(fuDocumento, vMaxPermitidoKB, vExtensionesPermitidas);

            doc.Observaciones = ((TextBox)(row.Cells[2].FindControl("txtObservaciones"))).Text;
            doc.FechaModifica = DateTime.Now;
            doc.UsuarioModifica = GetSessionUser().NombreUsuario;
            doc.IdTemporal = ViewState["IdTemporal"].ToString();
            InformacionAudioria(doc, this.PageName, vSolutionPage);


            //if (vIdDocAdjunto > 0)//Existe
            //{
            //    vProveedorService.ModificarDocDatosBasicoProv(doc);
            //    //ya se actualizó el Doc, entonces actualizar el string del campo oculto que nos servira para las validaciones.
            //    ActualizaValoresDocBasicos(doc.IdTipoDocumento.ToString());
            //}
            //else
            //{
            //    vProveedorService.InsertarDocDatosBasicoProv(doc);
            //}

            vProveedorService.InsertarDocDatosBasicoProv(doc);
            ActualizaValoresDocBasicos(doc.IdTipoDocumento.ToString());
            gvDocDatosBasicoProv.EditIndex = -1;

            BuscarDocumentosDatosBasic_ByIdTemporal();
            toolBar.LipiarMensajeError();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":

                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        //SetSessionParameter("Proveedor.Longitud", Archivo.Length.ToString());
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));


                        //ManejoControles obj = new ManejoControles();
                        //obj.OpenWindowPcUpdatePanel(this, "../../../General/General/MostrarImagenes/MostrarImagenes.aspx", 200, 200);

                        string path = "'../Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.showModalDialog(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");

                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected string SubirArchivo(FileUpload fuArchivo, int pMaxPermitidoKB, string pExtensionesPermitidas)
    {

        if (!fuArchivo.HasFile)
        {
            throw new Exception("Debe seleccionar un archivo.");
        }


        int idformatoArchivo;

        string filename = Path.GetFileName(fuArchivo.FileName);

        String NombreArchivoOri = filename;

        string tipoArchivo = "";
        foreach (string sExt in pExtensionesPermitidas.Split(','))
        {
            if (filename.Substring(filename.LastIndexOf(".") + 1).ToUpper() == sExt.ToUpper())
            {
                tipoArchivo = string.Format("General.archivo{0}", sExt.ToUpper());
                break;
            }
        }

        if (tipoArchivo == "")
        {
            throw new Exception(string.Format("Solo se permiten los siguientes archivos: {0}", pExtensionesPermitidas));
        }

        if (fuArchivo.PostedFile.ContentLength / 1024 > pMaxPermitidoKB)
        {
            throw new Exception(string.Format("Solo se permiten archivos inferiores a {0} KB", (pMaxPermitidoKB)));
        }

        SIAService vRUBOService = new SIAService();

        List<FormatoArchivo> vFormatoArchivo = vRUBOService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

        if (vFormatoArchivo.Count == 0)
        {
            throw new Exception("No se ha parametrizado la extensión del archivo a cargar");
        }

        idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

        //Creación del Nombre del archivo
        string NombreArchivo = filename.Substring(0, filename.IndexOf("."));

        NombreArchivo = vRUBOService.QuitarEspacios(NombreArchivo);

        NombreArchivo = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString()
            + "_ProveedorDatosBasico_" + NombreArchivo + filename.Substring(filename.LastIndexOf("."));

        //NombreArchivo = Guid.NewGuid().ToString() + NombreArchivo;
        string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

        //Guardado del archivo en el servidor
        HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;
        Boolean ArchivoSubido = vRUBOService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);

        if (ArchivoSubido)
        {
            return NombreArchivo;
        }
        else
        {
            throw new Exception("No se pudo subir el archivo al ftp.");
        }


        return null;
    }

    private void BuscarDocumentosDatosBasic_ByEntidad()
    {
        try
        {
            int vIdEntidad = Convert.ToInt32(hfIdEntidad.Value);
            string sPersona = ddlTipoPersona.SelectedValue;
            string sSector = ddlSector.SelectedValue;
            string sTipoEntidad = ddlTipoEntidad.SelectedValue;
            string valoresDocs = "";
            string svarIdTemporal = "";
            if (ViewState["IdTemporal"] != null)
                svarIdTemporal = ViewState["IdTemporal"].ToString().ToString();
            List<DocDatosBasicoProv> listaDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, sPersona, sSector, svarIdTemporal, sTipoEntidad);
            gvDocDatosBasicoProv.DataSource = listaDocs;
            gvDocDatosBasicoProv.DataBind();

            if (hfValidateDocumentsBasicos.Value.Equals("0"))
            {
                foreach (DocDatosBasicoProv doc in listaDocs)
                {
                    valoresDocs = valoresDocs + doc.IdTipoDocumento.ToString() + ",0|";
                }
                hfValidateDocumentsBasicos.Value = valoresDocs;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    private void BuscarDocumentosDatosBasic_ByIdTemporal()
    {
        try
        {
            string sPersona = ddlTipoPersona.SelectedValue;
            string sSector = ddlSector.SelectedValue;
            string valoresDocs = "";
            string vIdTemporal = ViewState["IdTemporal"].ToString();
            string sTipoEntidad = ddlTipoEntidad.SelectedValue;

            //if (ddlSector.SelectedItem.Text.Equals("PRIVADO"))
            //{
            //    if (ddlTipoEntidad.SelectedItem.Text.Equals("NACIONAL"))
            //        sSector = ddlSector.Items.FindByText("PUBLICO").Value;
            //    //else if (ddlTipoEntidad.SelectedItem.Text.Equals("EXTRANJERA"))
            //    //    sSector = ddlSector.Items.FindByText("PRIVADO").Value;
            //}

            List<DocDatosBasicoProv> listaDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdTemporal_TipoPersona(vIdTemporal, sPersona, sSector, sTipoEntidad);
            gvDocDatosBasicoProv.DataSource = listaDocs;
            gvDocDatosBasicoProv.DataBind();

            if (hfValidateDocumentsBasicos.Value.Equals("0"))
            {
                foreach (DocDatosBasicoProv doc in listaDocs)
                {
                    valoresDocs = valoresDocs + doc.IdTipoDocumento.ToString() + ",0|";
                }
                hfValidateDocumentsBasicos.Value = valoresDocs;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    private void BuscarDocumentosRegTercero_ByTercero()
    {
        try
        {
            int vIdTercero;
            if (Request.QueryString["oP"] == "E")
            {
                vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(hfIdEntidad.Value));
                vIdTercero = vEntidadProvOferente.IdTercero;
            }
            else
            {
                vIdTercero = int.Parse(hfIdTercero.Value);
            }
            string sPersona = ddlTipoPersona.SelectedValue;
            string valoresDocs = "";
            string svarIdTemporal = "";
            if (ViewState["IdTemporal"] != null)
                svarIdTemporal = ViewState["IdTemporal"].ToString().ToString();
            List<DocAdjuntoTercero> listaDocs = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(vIdTercero, sPersona, svarIdTemporal);
            gvDocAdjuntos.DataSource = listaDocs;
            gvDocAdjuntos.DataBind();

            if (hfValidateDocumentsTerceros.Value.Equals("0"))
            {
                foreach (DocAdjuntoTercero doc in listaDocs)
                {
                    valoresDocs = valoresDocs + doc.IdDocumento.ToString() + ",0|";
                    if (doc.NombreTipoDocumento.Equals("Cédula Representante Legal"))
                    {
                        hfIDDocYNombreAActualizar.Value = hfIDDocYNombreAActualizar.Value + doc.IdDocumento + "," + doc.NombreTipoDocumento + "|";
                    }
                }
                hfValidateDocumentsTerceros.Value = valoresDocs;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void BuscarDocumentosRegTercero_ByIdTemporal()
    {
        try
        {
            int vIdTercero;
            if (Request.QueryString["oP"] == "E")
            {
                vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(Convert.ToInt32(hfIdEntidad.Value));
                vIdTercero = vEntidadProvOferente.IdTercero;
            }
            else
            {
                vIdTercero = int.Parse(hfIdTercero.Value);
            }
            string sPersona = ddlTipoPersona.SelectedValue;
            string valoresDocs = "";
            //List<DocAdjuntoTercero> listaDocs = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(vIdTercero, sPersona, varIdTemporal);
            string svarIdTemporal = "";
            if (ViewState["IdTemporal"] != null)
                svarIdTemporal = ViewState["IdTemporal"].ToString().ToString();
            List<DocAdjuntoTercero> listaDocs = vProveedorService.ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(svarIdTemporal, sPersona);
            gvDocAdjuntos.DataSource = listaDocs;
            gvDocAdjuntos.DataBind();

            if (hfValidateDocumentsTerceros.Value.Equals("0"))
            {
                foreach (DocAdjuntoTercero doc in listaDocs)
                {
                    valoresDocs = valoresDocs + doc.IdDocumento.ToString() + ",0|";
                    if (doc.Descripcion.Equals("Cédula Representante Legal"))
                    {
                        hfIDDocYNombreAActualizar.Value = hfIDDocYNombreAActualizar.Value + doc.IdDocumento + "," + doc.Descripcion + "|";
                    }
                }
                hfValidateDocumentsTerceros.Value = valoresDocs;
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Elimina documentos
    /// </summary>
    protected void EliminarDocumentos()
    {

        try
        {
            foreach (GridViewRow row in gvDocDatosBasicoProv.Rows)
            {
                Label lblDocAdjunto = (Label)row.Cells[0].Controls[1];
                int idDocAdjunto = Convert.ToInt32(lblDocAdjunto.Text);
                if (idDocAdjunto > 0)
                {
                    DocDatosBasicoProv doc = vProveedorService.ConsultarDocDatosBasicoProv(idDocAdjunto);
                    vProveedorService.EliminarDocDatosBasicoProv(doc);
                }

            }


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Valida documentos oblilgatorios
    /// </summary>
    public void ValidarDocumentosObligatorios(EntidadProvOferente objNuevo)
    {

        string vTipoProveedor = ddlTipoPersona.SelectedValue;
        string vTipoSector = ddlSector.SelectedValue;
        string vTipoEntidad = ddlTipoEntidad.SelectedValue;
        List<DocDatosBasicoProv> ListaDocs = null;
        DateTime fechaHoy = DateTime.Now;
        hfIdTercero.Value = GetSessionParameter("Proveedor.IdTercero").ToString();
        bool isUpdate = false;
        bool updateDocuments = false;
        string documentosBasicosPorActualizar = "";

        if (Request.QueryString["oP"] == "E")
        {
            string limiteDiasTransCurridos = ConfigurationManager.AppSettings["valDiasTranscurridosProv_basicos"].ToString();
            hfIdTercero.Value = vEntidadProvOferente.IdTercero.ToString();
            isUpdate = true;
            TimeSpan ts;
            if (vEntidadProvOferente.FechaModifica == null)
                ts = DateTime.Now.Date - vEntidadProvOferente.FechaCrea.Value.Date;
            else
                ts = DateTime.Now.Date - vEntidadProvOferente.FechaModifica.Value.Date;
            if (ts.Days >= int.Parse(limiteDiasTransCurridos))
                updateDocuments = true;
            else if ((objNuevo.TerceroProveedor.RazonSocial != null && !objNuevo.TerceroProveedor.RazonSocial.Equals(vEntidadProvOferenteAEditar.TerceroProveedor.RazonSocial))
                || objNuevo.IdTipoClaseEntidad != vEntidadProvOferenteAEditar.IdTipoClaseEntidad
                || objNuevo.IdTipoCiiuPrincipal != vEntidadProvOferenteAEditar.IdTipoCiiuPrincipal
                || objNuevo.InfoAdminEntidadProv.IdTipoRegTrib != vEntidadProvOferenteAEditar.InfoAdminEntidadProv.IdTipoRegTrib
                || objNuevo.InfoAdminEntidadProv.IdDepartamentoConstituida != vEntidadProvOferenteAEditar.InfoAdminEntidadProv.IdDepartamentoConstituida
                || objNuevo.InfoAdminEntidadProv.IdMunicipioConstituida != vEntidadProvOferenteAEditar.InfoAdminEntidadProv.IdMunicipioConstituida
                || (objNuevo.InfoAdminEntidadProv.NombreEstablecimiento != null && objNuevo.InfoAdminEntidadProv.NombreEstablecimiento != vEntidadProvOferenteAEditar.InfoAdminEntidadProv.NombreEstablecimiento)
                || (objNuevo.TipoVigencia != vEntidadProvOferenteAEditar.TipoVigencia || objNuevo.FechaVigencia != vEntidadProvOferenteAEditar.FechaVigencia))
            {
                updateDocuments = true;
            }


            if (updateDocuments)
            {
                documentosBasicosPorActualizar = checkActualizaDocumentosBasicos();
                // documentosTercerosPorActualizar = checkActualizaDocumentosTerceros();
            }
            //else
            //{
            //    if (ddlTipoPersona.SelectedItem.Text.Equals("JURIDICA"))
            //    {
            //        if (!objNuevo.RepresentanteLegal.NumeroIdentificacion.Equals(vEntidadProvOferenteAEditar.RepresentanteLegal.NumeroIdentificacion))
            //        {
            //            updateCedulaRep = true;
            //            string idDoc = getIdDocAdjuntoAValidar("Cédula Representante Legal");
            //            if (!string.IsNullOrEmpty(idDoc))
            //                documentosTercerosPorActualizar = checkActualizaDocumentosTerceros_IdDoc(idDoc);
            //            else
            //                documentosTercerosPorActualizar = "";
            //        }
            //    }
            //}
        }

        //if (hfIdEntidad != null && hfIdEntidad.Value != "")
        //{
        //    int vIdEntidad = Convert.ToInt32(hfIdEntidad.Value);

        //    ListaDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, vTipoProveedor, vTipoSector);
        //}
        //else
        //{
        //    string vIdTemporal = ViewState["IdTemporal"].ToString();
        //    ListaDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdTemporal_TipoPersona(vIdTemporal, vTipoProveedor, vTipoSector);
        //}

        string svarIdTemporal = "";
        if (ViewState["IdTemporal"] != null)
            svarIdTemporal = ViewState["IdTemporal"].ToString().ToString();
        ListaDocs = vProveedorService.ConsultarDocDatosBasicooProv_IdTemporal_TipoPersona(svarIdTemporal, vTipoProveedor, vTipoSector, vTipoEntidad);
        //int idTercero = Convert.ToInt32(hfIdTercero.Value);
        //ListaDocsTercero = vProveedorService.ConsultarDocAdjuntoTerceros_IdTemporal_TipoPersona(varIdTemporal, vTipoProveedor);



        string mensaje = string.Empty;
        string[] arrUpDocsBasicos = documentosBasicosPorActualizar.Split(',');
        //string[] arrUpDocsTerceros = documentosTercerosPorActualizar.Split(',');
        foreach (DocDatosBasicoProv doc in ListaDocs)
        {
            if (doc.Obligatorio == 1 && string.IsNullOrEmpty(doc.LinkDocumento))
            {
                mensaje += (string.Format("Falta adjuntar el documento {0}. ", doc.NombreDocumento));
            }
            else if (isUpdate)
            {
                if (updateDocuments)
                {
                    for (int j = 0; j < arrUpDocsBasicos.Length; j++)
                    {
                        if (arrUpDocsBasicos[j].Equals(doc.IdTipoDocumento.ToString()))
                            mensaje += (string.Format("Debe actualizar el documento {0}. ", doc.NombreDocumento));
                    }
                }
            }
        }

        //foreach (DocAdjuntoTercero doc in ListaDocsTercero)
        //{
        //    if (doc.Obligatorio == 1 && string.IsNullOrEmpty(doc.LinkDocumento))
        //    {
        //        mensaje += (string.Format("Falta adjuntar el documento {0}. ", doc.NombreTipoDocumento));
        //    }
        //    else if (isUpdate)
        //    {
        //        if (updateDocuments)
        //        {
        //            for (int t = 0; t < arrUpDocsTerceros.Length; t++)
        //            {
        //                if (arrUpDocsTerceros[t].Equals(doc.IdDocumento.ToString()))
        //                    mensaje += (string.Format("Debe actualizar el documento {0}. ", doc.NombreTipoDocumento));
        //            }

        //        }
        //        else if (updateCedulaRep)
        //        {
        //            for (int t = 0; t < arrUpDocsTerceros.Length; t++)
        //            {
        //                if (arrUpDocsTerceros[t].Equals(doc.IdDocumento.ToString()))
        //                    mensaje += (string.Format("Debe actualizar el documento {0}. ", doc.NombreTipoDocumento));
        //            }
        //        }
        //    }

        //}


        if (!string.IsNullOrEmpty(mensaje))
        {
            throw new Exception(mensaje);
        }
    }
    #endregion documentos


    /// <summary>
    /// Carga valores para lista desplegable Nivel de gobierno
    /// </summary>
    /// <param name="PddlNivelgobierno"></param>
    /// <param name="pidDepartament"></param>
    private void CargarNiveldeGobierno(DropDownList PddlNivelgobierno, int pidDepartament)
    {

        ManejoControles.LlenarNivelGobiernoRamaoEstructura(PddlNivelgobierno, "-1", true, pidDepartament);
    }
    protected void ddlTipoEntidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClaseEntidad.Items.Clear();
        ddlClaseEntidad.DataBind();
        ddlClaseEntidad.Items.Insert(0, new ListItem("Seleccione", "-1"));
        ddlClaseEntidad.SelectedValue = "-1";
        if (ddlTipoPersona.SelectedValue.Equals("1"))
        {
            ddlTipoActividad.SelectedValue = "1";
            CargarClasedeEntidad();
        }
        else
            ddlTipoActividad.SelectedValue = "-1";

        ddlClaseEntidadVisible();
        BuscarDocumentosDatosBasic_ByIdTemporal();
    }
    protected void btnLimpiarCiiuSegun_Click(object sender, EventArgs e)
    {
        CalFechaActividadSegundaria.InitNull = true;
        TxtActividadCiiuSegu.Text = "";
        HfActividadCiiuSegu.Value = null;
        hfDesActividadCiiuSegu.Value = null;
        ((TextBox)CalFechaActividadSegundaria.FindControl("txtFecha")).Text = "";
        rfvCalFechaActividadSegundaria.Enabled = false;
    }
    protected void btnLimpiarCiiuprin_Click(object sender, EventArgs e)
    {
        CalFechaActividadPrincipal.InitNull = true;
        TxtActividadCiiuPrin.Text = "";
        hfIdActividadCiiuPrin.Value = null;
        RequiredFieldValidator1.Enabled = false;
        hfDesActividadCiiuPrin.Value = null;
        ((TextBox)CalFechaActividadPrincipal.FindControl("txtFecha")).Text = "";
    }

    /// <summary>
    /// Guarda la sucursal por default
    /// </summary>
    protected void GuardaSucursalDefault()
    {
        try
        {
            int vResultado;
            Sucursal vSucursal = new Sucursal();

            vSucursal.Nombre = "Principal";
            if (!string.IsNullOrEmpty(TxtIndicativo.Text))
            {
                vSucursal.Indicativo = Convert.ToInt32(TxtIndicativo.Text);
            }

            if (!string.IsNullOrEmpty(TxtTelefono.Text))
            {
                vSucursal.Telefono = Convert.ToInt32(TxtTelefono.Text);
            }
            if (!string.IsNullOrEmpty(TxtExtencion.Text))
            {
                vSucursal.Extension = Convert.ToInt64(TxtExtencion.Text);
            }
            if (!string.IsNullOrEmpty(TxtCelular.Text))
            {
                vSucursal.Celular = Convert.ToInt64(TxtCelular.Text);
            }
            if (TxtCorreoElectronico.Text != "")
            {
                vSucursal.Correo = Convert.ToString(TxtCorreoElectronico.Text);
            }
            vSucursal.Estado = 1;

            if (ddlDepartamentoDireccionComercial.SelectedValue != "-1")
            {
                vSucursal.Departamento = Convert.ToInt32(ddlDepartamentoDireccionComercial.SelectedValue);
            }
            if (ddlMunicipioDireccionComercial.SelectedValue != "-1")
            {
                vSucursal.Municipio = Convert.ToInt32(ddlMunicipioDireccionComercial.SelectedValue);
            }
            if (txtDireccionResidencia.TipoZona == "U")
            {
                vSucursal.IdZona = int.Parse("1");
            }
            if (txtDireccionResidencia.TipoZona == "R")
            {
                vSucursal.IdZona = int.Parse("2");
            }
            vSucursal.Direccion = txtDireccionResidencia.Text;
            vSucursal.Editable = 0;


            vSucursal.IdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            vSucursal.UsuarioCrea = GetSessionUser().NombreUsuario;
            InformacionAudioria(vSucursal, this.PageName, vSolutionPage);
            vResultado = vProveedorService.InsertarSucursal(vSucursal);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Sucursal.IdSucursal", vSucursal.IdSucursal);
                SetSessionParameter("Sucursal.Guardado", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la sucursal por default
    /// </summary>
    protected void ModificaSucursalDefault()
    {
        try
        {
            int vResultado;
            Sucursal vSucursal = new Sucursal();

            vSucursal.Nombre = "Principal";
            if (!string.IsNullOrEmpty(TxtIndicativo.Text))
            {
                vSucursal.Indicativo = Convert.ToInt32(TxtIndicativo.Text);
            }

            if (!string.IsNullOrEmpty(TxtTelefono.Text))
            {
                vSucursal.Telefono = Convert.ToInt32(TxtTelefono.Text);
            }
            if (!string.IsNullOrEmpty(TxtExtencion.Text))
            {
                vSucursal.Extension = Convert.ToInt64(TxtExtencion.Text);
            }
            if (!string.IsNullOrEmpty(TxtCelular.Text))
            {
                vSucursal.Celular = Convert.ToInt64(TxtCelular.Text);
            }
            if (TxtCorreoElectronico.Text != "")
            {
                vSucursal.Correo = Convert.ToString(TxtCorreoElectronico.Text);
            }
            vSucursal.Estado = 1;

            if (ddlDepartamentoDireccionComercial.SelectedValue != "-1")
            {
                vSucursal.Departamento = Convert.ToInt32(ddlDepartamentoDireccionComercial.SelectedValue);
            }
            if (ddlMunicipioDireccionComercial.SelectedValue != "-1")
            {
                vSucursal.Municipio = Convert.ToInt32(ddlMunicipioDireccionComercial.SelectedValue);
            }
            vSucursal.Direccion = txtDireccionResidencia.Text;
            if (txtDireccionResidencia.TipoZona == "U")
            {
                vSucursal.IdZona = int.Parse("1");
            }
            if (txtDireccionResidencia.TipoZona == "R")
            {
                vSucursal.IdZona = int.Parse("2");
            }
            vSucursal.IdEntidad = int.Parse(GetSessionParameter("EntidadProvOferente.IdEntidad").ToString());
            vSucursal.UsuarioCrea = GetSessionUser().NombreUsuario;
            InformacionAudioria(vSucursal, this.PageName, vSolutionPage);
            vResultado = vProveedorService.ModificarSucursalDefault(vSucursal);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Sucursal.IdSucursal", vSucursal.IdSucursal);
                SetSessionParameter("Sucursal.Guardado", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que cambia el estado a por ajustar cuando ya se ha validado el modulo
    /// </summary>
    private void CambiaEstado(int vIdEntidad)
    {
        try
        {
            //inicializa variables
            int vResultado;

            ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();
            Int32 iIdEntidad = Convert.ToInt32(vIdEntidad);

            //obtiene la ultima observacion disponible
            vValidarInfoDatosBasicosEntidad = vProveedorService.ConsultarValidarInfoDatosBasicosEntidad_Ultima(iIdEntidad);
            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = null; //Estado pendiente de revisión
            vValidarInfoDatosBasicosEntidad.Finalizado = false;

            int idEstado = 1; // Estado Por Validar

            vValidarInfoDatosBasicosEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
            //Modifica el estado de la última revisión
            vResultado = vProveedorService.ModificarValidarInfoDatosBasicosEntidad(vValidarInfoDatosBasicosEntidad, idEstado);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ValidarProveedor.ObservacionGuardada", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que va actualizando el string contenido en el campo oculto hfValidateDocumentsBasicos.
    /// tal valor nos ayudará en las validaciones de los documentos, este valor se actualiza cada vez
    /// que un documento ya adjunto se actualiza.
    /// </summary>
    private void ActualizaValoresDocBasicos(string idTipoDoc)
    {
        string valor = hfValidateDocumentsBasicos.Value;
        string nuevoValor = "";
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);
        string[] documentos = valor.Split('|');

        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[0].Equals(idTipoDoc))
            {
                nuevoValor = nuevoValor + idTipoDoc + ",1|";
            }
            else
            {
                nuevoValor = nuevoValor + documentos[i] + "|";
            }
        }
        hfValidateDocumentsBasicos.Value = nuevoValor;
    }

    /// <summary>
    /// Método que va actualizando el string contenido en el campo oculto hfValidateDocumentsBasicos.
    /// tal valor nos ayudará en las validaciones de los documentos, este valor se actualiza cada vez
    /// que un documento ya adjunto se actualiza.
    /// </summary>
    private void ActualizaValoresDocTerceros(string idTipoDoc)
    {
        string valor = hfValidateDocumentsTerceros.Value;
        string nuevoValor = "";
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);
        string[] documentos = valor.Split('|');

        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[0].Equals(idTipoDoc))
            {
                nuevoValor = nuevoValor + idTipoDoc + ",1|";
            }
            else
            {
                nuevoValor = nuevoValor + documentos[i] + "|";
            }
        }
        hfValidateDocumentsTerceros.Value = nuevoValor;
    }

    /// <summary>
    /// Método que revisa el valor del campo oculto que contiene el control de las validaciones
    /// para documentos básicos del proveedor. Retorna IdsTipoDocumentos separados por comas que deben
    /// Ser Actualizados, y retorna "0" si ya todos están actualizados.
    /// </summary>
    /// 
    private string checkActualizaDocumentosBasicos()
    {
        string docsActualizar = "";
        string valor = hfValidateDocumentsBasicos.Value;
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);

        string[] documentos = valor.Split('|');
        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[1].Equals("0"))
                docsActualizar = docsActualizar + documentos[i].Split(',')[0] + ",";
        }

        if (docsActualizar.Length > 0)
            if (docsActualizar.Substring(docsActualizar.Length - 1, 1).Equals(","))
                docsActualizar = docsActualizar.Substring(0, docsActualizar.Length - 1);

        return docsActualizar;
    }

    /// <summary>
    /// Método que revisa el valor del campo oculto que contiene el control de las validaciones
    /// para documentos registrados en terceros. Retorna IdsTipoDocumentos separados por comas que deben
    /// Ser Actualizados, y retorna "0" si ya todos están actualizados.
    /// </summary>
    /// 
    private string checkActualizaDocumentosTerceros()
    {
        string docsActualizar = "";
        string valor = hfValidateDocumentsTerceros.Value;
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);

        string[] documentos = valor.Split('|');
        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[1].Equals("0"))
                docsActualizar = docsActualizar + documentos[i].Split(',')[0] + ",";
        }

        if (docsActualizar.Length > 0)
            if (docsActualizar.Substring(docsActualizar.Length - 1, 1).Equals(","))
                docsActualizar = docsActualizar.Substring(0, docsActualizar.Length - 1);

        return docsActualizar;
    }

    private string checkActualizaDocumentosTerceros_IdDoc(string idDocAdjunto)
    {
        string docsActualizar = "";
        string valor = hfValidateDocumentsTerceros.Value;
        if (valor.Substring(valor.Length - 1, 1).Equals("|"))
            valor = valor.Substring(0, valor.Length - 1);

        string[] documentos = valor.Split('|');
        for (int i = 0; i < documentos.Length; i++)
        {
            if (documentos[i].Split(',')[0].Equals(idDocAdjunto))
            {
                if (documentos[i].Split(',')[1].Equals("0"))
                    docsActualizar = docsActualizar + documentos[i].Split(',')[0] + ",";
            }
        }

        if (docsActualizar.Length > 0)
            if (docsActualizar.Substring(docsActualizar.Length - 1, 1).Equals(","))
                docsActualizar = docsActualizar.Substring(0, docsActualizar.Length - 1);

        return docsActualizar;
    }

    private string getIdDocAdjuntoAValidar(string desc)
    {
        string idDoc = "";
        string valor = hfIDDocYNombreAActualizar.Value;
        if (!string.IsNullOrEmpty(valor))
        {
            if (valor.Substring(valor.Length - 1, 1).Equals("|"))
                valor = valor.Substring(0, valor.Length - 1);

            string[] documentos = valor.Split('|');
            for (int i = 0; i < documentos.Length; i++)
            {
                if (documentos[i].Split(',')[1].Equals("Cédula Representante Legal"))
                {
                    idDoc = documentos[i].Split(',')[0];
                    break;
                }
            }

            return idDoc;
        }
        else
            return valor;

    }

    private void llenarRdbLVigencia(bool indefinida)
    {
        rblVigencia.Items.Clear();
        if (!indefinida)
        {
            rblVigencia.Items.Insert(0, new ListItem("Vigencia indefinida", "0"));
            rblVigencia.Items.Insert(1, new ListItem("Vigencia definida", "1"));
        }
        else
        {
            rblVigencia.Items.Insert(0, new ListItem("Vigencia indefinida", "0"));
            rblVigencia.SelectedIndex = 0;
            lbVigenciaHasta.Visible = false;
            CalFechaVigencia.Visible = false;
            rvFechaVigencia.Enabled = false;

        }
    }

    private void ddlClaseEntidadVisible()
    {
        if (ddlTipoPersona.SelectedValue.Equals("2") && ddlSector.SelectedValue.Equals("1") && ddlTipoEntidad.SelectedValue.Equals("2"))
        {
            ddlClaseEntidad.Visible = false;
            lblClaseEntidad.Visible = false;
            rvddlClaseEntidad.Enabled = false;
        }
        else
        {
            ddlClaseEntidad.Visible = true;
            lblClaseEntidad.Visible = true;
            rvddlClaseEntidad.Enabled = true;
        }
    }

    //Lista Tipo de Entidad  Nacional Extranjera
    private void llenarddlTipoEntidad()
    {
        ddlTipoEntidad.Items.Clear();
        if ((ddlTipoPersona.SelectedValue.Equals("1") && ddlSector.SelectedValue.Equals("1")) || (ddlTipoPersona.SelectedValue.Equals("2") && ddlSector.SelectedValue.Equals("2")))
            ManejoControles.LlenarTipoEntidadAll(ddlTipoEntidad, "1", false, int.Parse(ddlTipoPersona.SelectedValue), int.Parse(ddlSector.SelectedValue));
        else
            ManejoControles.LlenarTipoEntidadAll(ddlTipoEntidad, "-1", true, null, null);
    }

    #region ManejoEntidadesPublicas
    /// <summary>
    /// control de lista desplegables
    /// </summary>
    /// <param name="pDropDownList"></param>
    public void ResetLista(DropDownList pDropDownList)
    {
        pDropDownList.Items.Clear();
        List<RamaoEstructura> vDepartamentos = vProveedorService.ConsultarRamaoEstructurasAll();
        pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1"));
        pDropDownList.SelectedValue = "-1";
    }


    protected void ddlRamaoestructura_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarNiveldeGobierno(ddlNivelgobierno, Convert.ToInt32(ddlRamaoestructura.SelectedValue));
        ResetLista(ddlNivelOrganizacional);
        ResetLista(ddlTipoEntidadPublica);
    }
    protected void ddlNivelgobierno_SelectedIndexChanged(object sender, EventArgs e)
    {
        ResetLista(ddlTipoEntidadPublica);
        ManejoControles.LlenarNivelOrganizacional(ddlNivelOrganizacional, "-1", true, int.Parse(ddlRamaoestructura.SelectedValue), int.Parse(ddlNivelgobierno.SelectedValue));
    }
    protected void ddlNivelOrganizacional_SelectedIndexChanged(object sender, EventArgs e)
    {
        ManejoControles.TipoEntidadPublica(ddlTipoEntidadPublica, "-1", true, int.Parse(ddlRamaoestructura.SelectedValue), int.Parse(ddlNivelgobierno.SelectedValue), int.Parse(ddlNivelOrganizacional.SelectedValue));
    }

    #endregion
    protected void txtIdentificacionRepr_TextChanged(object sender, EventArgs e)
    {

        string numIdentificacionRepLegal = txtIdentificacionRepr.Text;
        string idTipoIdentRepLegal = ddlTipoDocumentoRepr.SelectedValue;
        Tercero RepLegal = new Tercero();
        if (!numIdentificacionRepLegal.Equals("") && !idTipoIdentRepLegal.Equals("-1"))
            loadRepresentanteLegal(int.Parse(idTipoIdentRepLegal), numIdentificacionRepLegal);
        else
        {
            //Limpiar Todos los controle s de Representante Legal y Colocar Editables.
            TxtPrimerNombreRepr.Text = "";
            TxtSegundoNombreRepr.Text = "";
            TxtPrimerApellidoRepr.Text = "";
            TxtSegundoApellidoRepr.Text = "";
            TxtPrimerNombreRepr.Enabled = true;
            TxtSegundoNombreRepr.Enabled = true;
            TxtPrimerApellidoRepr.Enabled = true;
            TxtSegundoApellidoRepr.Enabled = true;
            ddlSexoRepLegal.SelectedIndex = 0;
            cuFechaNacRepLegal.Date = Convert.ToDateTime("31/12/1899");
            cuFechaNacRepLegal.SiChecarFormato();
            TxtCelularRepr.Text = "";
            TxtIndicativoRepr.Text = "";
            TxtTelefonoRepr.Text = "";
            TxtExtencionRepr.Text = "";
            TxtCorreoElectronicoRepr.Text = "";

        }
    }

    private void loadRepresentanteLegal(int idTipoIdentificacion, string numeroIdentificacion)
    {
        Tercero RepLegal = new Tercero();
        RepLegal = vOferenteService.ConsultarTerceroTipoNumeroIdentificacion(idTipoIdentificacion, numeroIdentificacion);

        vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferenteAEditar.IdEntidad);
        vInfoAdmin = vEntidadProvOferente.InfoAdminEntidadProv;
        //vEntidadProvOferenteAEditar
        cuFechaNacRepLegal.SiChecarFormato();
        if (RepLegal.IdTercero != 0)
        {
            vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal = RepLegal.IdTercero;
            TelTerceros telefonos = vOferenteService.ConsultarTelTercerosIdTercero(RepLegal.IdTercero);
            //Cargar Datos Representante Legal
            TxtPrimerNombreRepr.Text = RepLegal.PrimerNombre;
            TxtSegundoNombreRepr.Text = RepLegal.SegundoNombre;
            TxtPrimerApellidoRepr.Text = RepLegal.PrimerApellido;
            TxtSegundoApellidoRepr.Text = RepLegal.SegundoApellido;
            TxtPrimerNombreRepr.Enabled = false;
            TxtSegundoNombreRepr.Enabled = false;
            TxtPrimerApellidoRepr.Enabled = false;
            TxtSegundoApellidoRepr.Enabled = false;
            if (string.IsNullOrEmpty(RepLegal.Sexo) || RepLegal.Sexo.Equals("-"))
                ddlSexoRepLegal.SelectedValue = "-1";
            else
                ddlSexoRepLegal.SelectedValue = RepLegal.Sexo;
            if (RepLegal.FechaNacimiento == null)
                cuFechaNacRepLegal.Date = Convert.ToDateTime("31/12/1899");
            else
                cuFechaNacRepLegal.Date = DateTime.Parse(RepLegal.FechaNacimiento.ToString());

            if (ddlTipoPersona.SelectedValue.Equals("3") || ddlTipoPersona.SelectedValue.Equals("4"))
            {
                ddlSexoRepLegal.Enabled = false;
                cuFechaNacRepLegal.Enabled = false;
            }

            TxtCelularRepr.Text = telefonos.Movil.ToString();
            TxtIndicativoRepr.Text = telefonos.IndicativoTelefono.ToString();
            TxtTelefonoRepr.Text = telefonos.NumeroTelefono.ToString();
            TxtExtencionRepr.Text = telefonos.ExtensionTelefono.ToString();
            TxtCorreoElectronicoRepr.Text = RepLegal.Email;
        }
        else
        {
            vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal = 0;
            //Reset Controles
            TxtPrimerNombreRepr.Text = "";
            TxtSegundoNombreRepr.Text = "";
            TxtPrimerApellidoRepr.Text = "";
            TxtSegundoApellidoRepr.Text = "";
            ddlSexoRepLegal.SelectedValue = "-1";
            cuFechaNacRepLegal.Date = Convert.ToDateTime("31/12/1899");
            TxtCelularRepr.Text = "";
            TxtIndicativoRepr.Text = "";
            TxtTelefonoRepr.Text = "";
            TxtExtencionRepr.Text = "";
            TxtCorreoElectronicoRepr.Text = "";
            TxtPrimerNombreRepr.Enabled = true;
            TxtSegundoNombreRepr.Enabled = true;
            TxtPrimerApellidoRepr.Enabled = true;
            TxtSegundoApellidoRepr.Enabled = true;
            ddlSexoRepLegal.Enabled = true;
            cuFechaNacRepLegal.Enabled = true;

        }
    }
    protected void ddlTipoDocumentoRepr_SelectedIndexChanged(object sender, EventArgs e)
    { 
        if (!ddlTipoPersona.SelectedValue.Equals("3") && !ddlTipoPersona.SelectedValue.Equals("4"))
        {
            string numIdentificacionRepLegal = txtIdentificacionRepr.Text;
            string idTipoIdentRepLegal = ddlTipoDocumentoRepr.SelectedValue;
            Tercero RepLegal = new Tercero();
            if (!numIdentificacionRepLegal.Equals("") && !idTipoIdentRepLegal.Equals("-1"))
                loadRepresentanteLegal(int.Parse(idTipoIdentRepLegal), numIdentificacionRepLegal);
        }

        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDocumentoRepr.SelectedValue != "-1")
        {
            this.txtIdentificacionRepr.Text = String.Empty;
            this.txtIdentificacionRepr.Focus();
        }
    }
    protected void lnkbtnAuditoria_Click(object sender, EventArgs e)
    {
        this.mdpDatosAuditoria.Show();
    }

    private void llenarGridAuditoria()
    {

        if (!GetSessionParameter("EntidadProvOferente.IdEntidad").ToString().Equals(""))
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            List<AuditoriaAccionesEntidad> listaAuditoria = new List<AuditoriaAccionesEntidad>();
            listaAuditoria = vProveedorService.GetAuditoriaAccionesEntidad(vIdEntidad);

            this.gridAuditoriaDatBAsicos.DataSource = listaAuditoria;
            this.gridAuditoriaDatBAsicos.DataBind();
        }
        else
        {
            this.gridAuditoriaDatBAsicos.DataSource = null;
            this.gridAuditoriaDatBAsicos.DataBind();
        }
    }
    protected void gridAuditoriaDatBAsicos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gridAuditoriaDatBAsicos.PageIndex = e.NewPageIndex;
        this.llenarGridAuditoria();
        this.mdpDatosAuditoria.Show();
    }

    /// <summary>
    /// Crea estructura UNSPSC
    /// </summary>
    /// <returns></returns>
    private DataTable CrearEstructuraUNSPSC()
    {
        dtUNSPSC = new DataTable();

        DataColumn idUNSPSC = dtUNSPSC.Columns.Add("idTipoCodUNSPSC", typeof(int));
        idUNSPSC.AllowDBNull = false;

        DataColumn existe = dtUNSPSC.Columns.Add("existe", typeof(bool));
        existe.AllowDBNull = false;
        existe.ReadOnly = false;

        dtUNSPSC.Columns.Add("codigo", typeof(string));
        dtUNSPSC.Columns.Add("descripcion", typeof(string));

        dtUNSPSC.PrimaryKey = new DataColumn[] { idUNSPSC };
        dtUNSPSC.AcceptChanges();

        SetSessionParameter("Proveedor.dtUNSPSC", dtUNSPSC);
        return dtUNSPSC;
    }

    protected void gvCodigoUNSPSC_SelectedIndexChanged(object sender, EventArgs e)
    { }

    /// <summary>
    /// Comando de renglón para la grilla CodigoUNSPSC
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvCodigoUNSPSC_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Delete" && GetSessionParameter("Proveedor.dtUNSPSC") != null)
            {
                int vIdTercero = Convert.ToInt32(GetSessionParameter("Proveedor.IdTercero"));
                dtUNSPSC = (DataTable)GetSessionParameter("Proveedor.dtUNSPSC");
                var rows = dtUNSPSC.Select("idTipoCodUNSPSC = " + e.CommandArgument);
                foreach (var row in rows)
                {
                    row.Delete();
                }
                dtUNSPSC.AcceptChanges();
                SetSessionParameter("Proveedor.dtUNSPSC", dtUNSPSC);

                if (Request.QueryString["oP"] == "E")
                {

                    List<CodigoUNSPSCProveedor> listaUNSPSC = vProveedorService.ConsultarCodigoUNSPSCProveedor(vIdTercero);
                    foreach (CodigoUNSPSCProveedor cod in listaUNSPSC)
                    {
                        if (cod.IdTipoCodUNSPSC == int.Parse(e.CommandArgument.ToString()))
                        {
                            vProveedorService.EliminarCodigoUNSPSCProveedor(cod);
                        }
                    }
                }

                gvCodigoUNSPSC.DataSource = GetSessionParameter("Proveedor.dtUNSPSC");
                gvCodigoUNSPSC.DataBind();
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    //Buscar tipo de usuario que registra: 1: Interno, 0: Externo
    //Luz Angela Borda
    //Mayo 15 de 2015
    protected int BuscarTipoUsuarioRegistra()
    {
        string providerUserkey = new GeneralWeb().GetSessionUser().Providerkey;
        Tercero objtmp = vProveedorService.ConsultarTerceroProviderUserKey(providerUserkey);
        //Usuario externo
        if (objtmp.IdTercero != 0)
        {
            return 0; // Externo
        }
        else
        {
            return 1; // Interno
        }

    }

    protected void gvCodigoUNSPSC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    { }

    protected void gvCodigoUNSPSC_RowDeleting(object sender, GridViewDeleteEventArgs e)
    { }
    protected void ddlRegimenTributario_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarClasedeEntidad();
    }

    private void CambiarValidadorDocumentoUsuario()
    {
        if (this.ddlTipoDocumentoRepr.SelectedItem.Text == "CC" || this.ddlTipoDocumentoRepr.SelectedItem.Text == "NIT" || this.ddlTipoDocumentoRepr.SelectedItem.Text == "PA")
        {
            this.txtIdentificacionRepr.MaxLength = 11;
        }
        else if (this.ddlTipoDocumentoRepr.SelectedItem.Text == "CE")
        {
            this.txtIdentificacionRepr.MaxLength = 6;
        }
        else
        {
            this.txtIdentificacionRepr.MaxLength = 17;
        }
    }
}





