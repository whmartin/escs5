using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using EntidadProvOferente = Icbf.Proveedor.Entity.EntidadProvOferente;
using Icbf.SIA.Service;
using Icbf.Seguridad.Service;

/// <summary>
/// Página que despliega del detalle de los datos básicos de un proveedor
/// </summary>
public partial class Page_EntidadProvOferente_Detail : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/GestionProveedores";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
    SIAService vRUBOService = new SIAService();
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                toolBar.LipiarMensajeError();
                toolBar.LimpiarOpcionesAdicionales();
                CargarDatosIniciales();
                CargarRegistro();
                CargarCodigosUNSPSC();

                var vSeguridadService = new SeguridadService();
                lbQueRegistrar.Text = vSeguridadService.ConsultarParametros(null, null, null, "GestionProveedor") == null ? "No se encontro el parametro" : vSeguridadService.ConsultarParametros(null, null, null, "GestionProveedor")[0].ValorParametro;

            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador del evento click para el botón Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        var identi = GetSessionParameter("EntidadProvOferente.IdEntidad");
        int vIdEntidad = Convert.ToInt32(identi);
        vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
        if (vEntidadProvOferente.IdEstado == 5) //estado EN VALIDACIÓN 
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript", "<script>javascript: alert('Esta información no puede ser editada en este momento, intente más tarde');</script>");
        }
        else
        {
            SetSessionParameter("EntidadProvOferente.IdEntidad", hfIdEntidad.Value);
            NavigateTo(SolutionPage.Edit);
        }
    }

    /// <summary>
    /// Manejador del evento click para el botón Eliminar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDatosTipoPersona();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarProveedor.NroRevision").ToString()))
        {
            NavigateTo("../ValidarProveedor/Revision.aspx");
        }
    }

    protected void ddlDepartamentoConstituida_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarMunicipioConstituida();

    }

    protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDatosSector();
        CargarClasedeEntidad();
    }

    protected void ddlTipoActividad_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarClasedeEntidad();
    }

    protected void ddlExtecionMatruculaMercantil_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarExtecionMatruculaMercantil();
    }

    protected void ddlDepartamentoDireccionComercial_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDepartamentoDireccionComercial();

    }

    protected void gvDocDatosBasicoProv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocDatosBasicoProv.PageIndex = e.NewPageIndex;
        BuscarDocumentos();
    }
    /// <summary>
    /// Comandos de renglón para la grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocDatosBasicoProv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    //RUBOService vRUBOService = new RUBOService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        //SetSessionParameter("Proveedor.Longitud", Archivo.Length.ToString());
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));

                        //ManejoControles obj = new ManejoControles();
                        //obj.OpenWindowPcUpdatePanel(this, "../../../General/General/MostrarImagenes/MostrarImagenes.aspx", 200, 200);

                        string path = "'../Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocDatosBasicoProv_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDocDatosBasicoProv.SelectedRow);
    }

    protected void gvDocAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VerDocumento":
                    SIAService vRUBOService = new SIAService();
                    MemoryStream Archivo = vRUBOService.DescargarArchivoFtp(System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/", e.CommandArgument.ToString());

                    if (Archivo != null)
                    {
                        //SetSessionParameter("Proveedor.Longitud", Archivo.Length.ToString());
                        SetSessionParameter("Proveedor.Archivo", Archivo.ToArray());
                        SetSessionParameter("Proveedor.NombreArchivo", e.CommandArgument.ToString());
                        SetSessionParameter("Proveedor.ContentType", UtilityProveedor.GetContentType(e.CommandArgument.ToString()));


                        //ManejoControles obj = new ManejoControles();
                        //obj.OpenWindowPcUpdatePanel(this, "../../../General/General/MostrarImagenes/MostrarImagenes.aspx", 200, 200);

                        string path = "'../Archivo/MostrarArchivo.aspx'";
                        ClientScript.RegisterStartupScript(this.GetType(), "myScript", "<script>window.open(" + path + ", null, 'dialogWidth:1250px;dialogHeight:450px;resizable:yes;');</script>");



                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se encontró el archivo en el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocAdjuntos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocAdjuntos.PageIndex = e.NewPageIndex;

        BuscarDocumentosRegTercero();
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Carga datos de la fuente de datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
            int vIdEntidad = Convert.ToInt32(identida);
            RemoveSessionParameter("Proveedor.IdTercero");


            vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            if (hfIdEntidad != null) hfIdEntidad.Value = vEntidadProvOferente.IdEntidad.ToString();
            vEntidadProvOferente.TerceroProveedor = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            TxtConsecutivoInterno.Text = vEntidadProvOferente.ConsecutivoInterno;
            vEntidadProvOferente.TelTerceroProveedor = vOferenteService.ConsultarTelTercerosIdTercero(vEntidadProvOferente.IdTercero);
            vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
            vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);
            ddlTipoPersona.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdTipoPersona.ToString();

            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1)
            {
                Label1.Text = "Complemento de datos básicos";
            }

            CargarDatosTipoPersona();
            if (vEntidadProvOferente.TerceroProveedor.Email != null)
            {
                TxtCorreoElectronico.Text = vEntidadProvOferente.TerceroProveedor.Email;
            }
            ddlTipoDocumento.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdDListaTipoDocumento.ToString();
            hdIdTreceroProveedro.Value = vEntidadProvOferente.TerceroProveedor.IdTercero.ToString();


            ///Se adjunta la línea que trae el parametro de tercero para que no se pierda el id en la consulta 
               SetSessionParameter("Proveedor.IdTercero", hdIdTreceroProveedro.Value);


            FechaICBF.Date = vEntidadProvOferente.FechaIngresoICBF;
            FechaICBF.Enabled = false; ;
            txtIdentificacion.Text = vEntidadProvOferente.TerceroProveedor.NumeroIdentificacion.ToString();
            TxtRazonSocial.Text = vEntidadProvOferente.TerceroProveedor.RazonSocial;
            TxtPrimerNombre.Text = vEntidadProvOferente.TerceroProveedor.PrimerNombre;
            TxtSegundoNombre.Text = vEntidadProvOferente.TerceroProveedor.SegundoNombre;
            TxtPrimerApellido.Text = vEntidadProvOferente.TerceroProveedor.PrimerApellido;
            TxtSegundoApellido.Text = vEntidadProvOferente.TerceroProveedor.SegundoApellido;
            TxtIndicativo.Text = vEntidadProvOferente.TelTerceroProveedor.IndicativoTelefono.ToString();
            txtDv.Text = vEntidadProvOferente.TerceroProveedor.DigitoVerificacion.ToString();
            TxtTelefono.Text = vEntidadProvOferente.TelTerceroProveedor.NumeroTelefono.ToString();
            TxtExtencion.Text = vEntidadProvOferente.TelTerceroProveedor.ExtensionTelefono.ToString();
            TxtCelular.Text = vEntidadProvOferente.TelTerceroProveedor.Movil.ToString();
            TxtSitioWeb.Text = vEntidadProvOferente.InfoAdminEntidadProv.SitioWeb;
            ddlSector.SelectedValue = vEntidadProvOferente.IdTipoSector.ToString();

            HfUsuarioCrea.Value = vEntidadProvOferente.UsuarioCrea;

            CargarDatosSector();
            PnComplementoPrivada.Visible = vEntidadProvOferente.IdTipoSector == 1;
            PnComplementoPublicas.Visible = vEntidadProvOferente.IdTipoSector == 2;
            ddlRegimenTributario.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoRegTrib.ToString();
            if (vEntidadProvOferente.FechaConstitucion != null)
                CalFechaConstitucion.Date = Convert.ToDateTime(vEntidadProvOferente.FechaConstitucion);



            TxtNombreEstablecimiento.Text = vEntidadProvOferente.InfoAdminEntidadProv.NombreEstablecimiento;
            ddlTipoEntidad.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidad.ToString();
            var terceroProveedor = vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario);
            if (terceroProveedor.Rol.Split(';').Contains("PROVEEDORES"))
            {
                lbEstadoValidacion.Visible = false;
                ddlEstadoValidacion.Visible = false;
            }
            else
            {
                lbEstadoValidacion.Visible = true;
                ddlEstadoValidacion.Visible = true;
            }
            ddlEstadoValidacion.SelectedValue = vEntidadProvOferente.IdEstado.ToString();

            /**
             * Las Siguiente Lineas antes estaban sólo así:
             *             if (vEntidadProvOferente.IdEstado == 2) // estado VALIDADO
            {
                toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
            }
            ... Pero de acuerdo al requerimiento... si el Usuario Logueado es quien registro el Proveedor
             * y es estado Validado hay que mostrar el botón Editar... (ó mejor dicho no hay que ocultarlo, ya que en 
             * la método iniciar lo deja visible.
             **/
            if (vEntidadProvOferente.IdEstado == 2) // estado VALIDADO
            {
                if (vEntidadProvOferente.UsuarioCrea != GetSessionUser().NombreUsuario)
                {
                    toolBar.eventoEditar -= new ToolBarDelegate(btnEditar_Click);
                }
            }


            txtRegistradorPor.Text = vEntidadProvOferente.UsuarioCrea;
            txtFechaRegistro.Text = vEntidadProvOferente.FechaCrea.Value.ToString("dd/MM/yyyy");


            var vTipoCiiu = new TipoCiiu();

            //2014-11-05- CCMigración. JV. Sólo si es IdTipoCiiuPrincipal != null se carga. ya que para todos los registros migrados este dato quedó en NULL
            if (vEntidadProvOferente.IdTipoCiiuPrincipal != null)
            {
                vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadProvOferente.IdTipoCiiuPrincipal);
                TxtActividadCiiuPrin.Text = vTipoCiiu.CodigoCiiu + " - " + vTipoCiiu.Descripcion;
                hfIdActividadCiiuPrin.Value = vTipoCiiu.IdTipoCiiu.ToString();
            }
            if (vEntidadProvOferente.IdTipoCiiuSecundario != null)
            {
                vTipoCiiu = vProveedorService.ConsultarTipoCiiu((int)vEntidadProvOferente.IdTipoCiiuSecundario);
                TxtActividadCiiuSegu.Text = vTipoCiiu.CodigoCiiu + " - " + vTipoCiiu.Descripcion;
                HfActividadCiiuSegu.Value = vTipoCiiu.IdTipoCiiu.ToString();
            }
            //2014-11-05- CCMigración. JV. Sólo si es Fecha != null se carga. ya que para todos los registros migrados este dato quedó en NULL
            if (vEntidadProvOferente.FechaCiiuPrincipal != null)
                CalFechaActividadPrincipal.Date = Convert.ToDateTime(vEntidadProvOferente.FechaCiiuPrincipal);

            if (vEntidadProvOferente.FechaCiiuSecundario != null)
            {
                CalFechaActividadSegundaria.Date = Convert.ToDateTime(vEntidadProvOferente.FechaCiiuSecundario);
            }
            ddlTipoActividad.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoActividad.ToString();
            CargarClasedeEntidad();
            if (vEntidadProvOferente.IdTipoClaseEntidad != null)
                ddlClaseEntidad.SelectedValue = vEntidadProvOferente.IdTipoClaseEntidad.ToString();
            if (vEntidadProvOferente.ExenMatriculaMer != null)
                ddlExtecionMatruculaMercantil.SelectedValue = Convert.ToInt16(vEntidadProvOferente.ExenMatriculaMer).ToString();
            CargarExtecionMatruculaMercantil();
            TxtNumeroMatriculaMercantil.Text = vEntidadProvOferente.MatriculaMercantil;

            if (vEntidadProvOferente.FechaMatriculaMerc != null)
            {
                CalFechaMatriculaMercantil.Date = Convert.ToDateTime(vEntidadProvOferente.FechaMatriculaMerc);
            }

            ddlRamaoestructura.SelectedValue = vEntidadProvOferente.IdTipoRamaPublica.ToString();
            if (vEntidadProvOferente.IdTipoRamaPublica != null)
            {
                CargarNiveldeGobierno(ddlNivelgobierno, Convert.ToInt32(vEntidadProvOferente.IdTipoRamaPublica.ToString()));
            }
            ddlNivelgobierno.SelectedValue = vEntidadProvOferente.IdTipoNivelGob.ToString();
            ddlNivelOrganizacional.SelectedValue = vEntidadProvOferente.IdTipoNivelOrganizacional.ToString();
            ddlTipoEntidadPublica.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdTipoEntidadPublica.ToString();
            ddlDepartamentoConstituida.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoConstituida.ToString();
            CargarMunicipioConstituida();
            ddlMunicipioConstituida.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioConstituida.ToString();
            ddlDepartamentoDireccionComercial.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdDepartamentoDirComercial.ToString();
            CargarDepartamentoDireccionComercial();
            ddlMunicipioDireccionComercial.SelectedValue = vEntidadProvOferente.InfoAdminEntidadProv.IdMunicipioDirComercial.ToString();
            if (!string.IsNullOrEmpty(vEntidadProvOferente.InfoAdminEntidadProv.IdZona))
            {
                if (vEntidadProvOferente.InfoAdminEntidadProv.IdZona == "1")
                {
                    txtDireccionResidencia.TipoZona = "U";
                }
                else
                {
                    txtDireccionResidencia.TipoZona = "R";
                }
            }
            if (!string.IsNullOrEmpty(vEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial))
            {
                txtDireccionResidencia.Text = vEntidadProvOferente.InfoAdminEntidadProv.DireccionComercial;
            }
            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 2 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
            {
                vEntidadProvOferente.RepresentanteLegal = new Tercero();
                if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
                {
                    vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);
                }

                ddlTipoDocumentoRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
                txtIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
                TxtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
                TxtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                TxtCorreoElectronicoRepr.Text = vEntidadProvOferente.RepresentanteLegal.Email;
                hfRepreLegal.Value = vEntidadProvOferente.RepresentanteLegal.IdTercero.ToString();
                vEntidadProvOferente.TelRepresentanteLegal =
                    vOferenteService.ConsultarTelTercerosIdTercero(Convert.ToInt32(vEntidadProvOferente.RepresentanteLegal.IdTercero));
                TxtCelularRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.Movil.ToString();
                TxtIndicativoRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.IndicativoTelefono.ToString();
                TxtTelefonoRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.NumeroTelefono.ToString();
                TxtExtencionRepr.Text = vEntidadProvOferente.TelRepresentanteLegal.ExtensionTelefono.ToString();
                hfTelTelRepreLega.Value = vEntidadProvOferente.TelRepresentanteLegal.IdTelTercero.ToString();
                if (!string.IsNullOrEmpty(vEntidadProvOferente.RepresentanteLegal.Sexo))
                    ddlSexoRepLegal.SelectedValue = vEntidadProvOferente.RepresentanteLegal.Sexo;
                if (vEntidadProvOferente.RepresentanteLegal.FechaNacimiento != null)
                    cuFechaNacRepLegal.Date = DateTime.Parse(vEntidadProvOferente.RepresentanteLegal.FechaNacimiento.ToString());
                lblClaseEntidad.Text = "Clase de Entidad";
            }
            else if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 1)
                lblClaseEntidad.Text = "Clase de entidad o Establecimiento";

            if ((ddlTipoPersona.SelectedValue == "1" && ddlSector.SelectedValue == "1") || (ddlTipoPersona.SelectedValue == "2" && ddlSector.SelectedValue == "2"))
                llenarRdbLVigencia(true);
            else
                llenarRdbLVigencia(false);
            if (vEntidadProvOferente.TipoVigencia != null)
                rblVigencia.SelectedValue = Convert.ToString(Convert.ToInt32(vEntidadProvOferente.TipoVigencia));
            else
                rblVigencia.SelectedIndex = -1;
            CalFechaVigencia.Visible = rblVigencia.SelectedValue == "1";
            lbVigenciaHasta.Visible = rblVigencia.SelectedValue == "1";
            if ((vEntidadProvOferente.FechaVigencia != null) && (rblVigencia.SelectedValue == "1"))
            {
                CalFechaVigencia.Date = Convert.ToDateTime(vEntidadProvOferente.FechaVigencia);
            }

            if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 3 || vEntidadProvOferente.TerceroProveedor.IdTipoPersona == 4)
                txtNumInt.Text = vEntidadProvOferente.NumIntegrantes.ToString();
            //Llenamos las grillas de los documentos
            BuscarDocumentos();
            BuscarDocumentosRegTercero();

            //Se muestran las observaciones
            BuscarObservacionesDatosBasicos();
    
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vEntidadProvOferente.UsuarioCrea, (DateTime)vEntidadProvOferente.FechaCrea, vEntidadProvOferente.UsuarioModifica, (DateTime)vEntidadProvOferente.FechaModifica);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    // Se llena la grila con los códigos UNSPSC
    // Luz Angela Borda
    // Mayo 13 de 2015

    public void CargarCodigosUNSPSC()
    {

        var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
        int vIdTercero = Convert.ToInt32(GetSessionParameter("Proveedor.IdTercero"));
        int vIdEntidad = Convert.ToInt32(identida);

        gvCodigoUNSPSC.DataSource = vProveedorService.ConsultarCodigoUNSPSCProveedor(vIdTercero);
        gvCodigoUNSPSC.DataBind();

    }

    /// <summary>
    /// Carga datos iniciales a controles del formulario
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            var terceroProveedor = vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario);
            if (terceroProveedor.Rol.Split(';').Contains("AdministradorSIA"))
            {
                lbEstadoValidacion.Visible = true;
                ddlEstadoValidacion.Visible = true;
                this.lnkbtnAuditoria.Visible = true;
                llenarGridAuditoria();
            }
            else
            {
                lbEstadoValidacion.Visible = false;
                ddlEstadoValidacion.Visible = false;
                this.lnkbtnAuditoria.Visible = false;
            }

            // lista documento reprecentante legal
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDocumentoRepr, "-1", true);
            // lista tipo sector publico o privado 
            ManejoControles.LlenarTipoSectorEntidad(ddlSector, "-1", true);
            // Lisata tipo persona Natura o Juridica
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            // Lisata todos los de partamento  Domicilio legal en Colombia Donde está constituida
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamentoConstituida, "-1", true);
            //Lista Tipo de Entidad  Nacional Extranjera
            ManejoControles.LlenarTipoEntidadAll(ddlTipoEntidad, "-1", true, null, null);
            //Lista Rama o estructura
            ManejoControles.LlenarRamaoestructuraAll(ddlRamaoestructura, "-1", true);
            // Lista Nivel Gobierno 
            //ManejoControles.LlenarNivelGobierno(ddlNivelgobierno, "-1", true);
            // lista NivelOrganizacional
            ManejoControles.LlenarNivelOrganizacional(ddlNivelOrganizacional, "-1", true, 0, 0);
            //lista TipoEntidadPublica ddlTipoEntidadPublica
            ManejoControles.TipoEntidadPublica(ddlTipoEntidadPublica, "-1", true, 0, 0, 0);
            // Lisata TipoActividad ddlTipoActividad
            ManejoControles.TipoActividad(ddlTipoActividad, "-1", true);
            // lista documento reprecentante legal
            ManejoControles.LlenarEstadoDatosBasicos(ddlEstadoValidacion, "1", true);
            //Exención Matricula Mercantil 
            //ddlExtecionMatruculaMercantil.Items.Insert(0, new ListItem("NO", "0"));
            //ddlExtecionMatruculaMercantil.Items.Insert(1, new ListItem("SI", "1"));
            // Lisata todos los de partamento  Datos de Ubicación/Dirección comercial o de gerencia
            ManejoControles.LlenarExperienciaDepartamento(ddlDepartamentoDireccionComercial, "-1", true);
            //llena tipo de vigencia 
            rblVigencia.Items.Insert(0, new ListItem("Vigencia indefinida", "0"));
            rblVigencia.Items.Insert(1, new ListItem("Vigencia definida", "1"));
            //rblVigencia.SelectedValue = "0";
            // Cargar documentos a Adjuntar            
            ddlSexoRepLegal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlSexoRepLegal.Items.Insert(1, new ListItem("Masculino", "M"));
            ddlSexoRepLegal.Items.Insert(2, new ListItem("Femenino", "F"));
            ddlSexoRepLegal.SelectedValue = "-1";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void llenarRdbLVigencia(bool indefinida)
    {
        rblVigencia.Items.Clear();
        if (!indefinida)
        {
            rblVigencia.Items.Insert(0, new ListItem("Vigencia indefinida", "0"));
            rblVigencia.Items.Insert(1, new ListItem("Vigencia definida", "1"));
        }
        else
        {
            rblVigencia.Items.Insert(0, new ListItem("Vigencia indefinida", "0"));
            rblVigencia.SelectedIndex = 0;
        }
    }

    /// <summary>
    /// Carga valores para la lista desplegable Niver de gobierno
    /// </summary>
    /// <param name="PddlNivelgobierno"></param>
    /// <param name="pidDepartament"></param>
    private void CargarNiveldeGobierno(DropDownList PddlNivelgobierno, int pidDepartament)
    {

        ManejoControles.LlenarNivelGobiernoRamaoEstructura(PddlNivelgobierno, "-1", true, pidDepartament);
    }
    /// <summary>
    /// Elimina registro de la fuente de datos
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdEntidad = Convert.ToInt32(hfIdEntidad.Value);

            EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            InformacionAudioria(vEntidadProvOferente, this.PageName, vSolutionPage);
            int vResultado = vProveedorService.EliminarEntidadProvOferente(vEntidadProvOferente);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("EntidadProvOferente.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //Se revisa si está en modo de validacion proveedor sino está en modo validación está en modo edición
            if (string.IsNullOrEmpty(GetSessionParameter("ValidarProveedor.NroRevision").ToString()))
            {

                var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
                int vIdEntidad = Convert.ToInt32(identida);
                vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
                if (vRUBOService.ConsultarUsuario(vEntidadProvOferente.UsuarioCrea).Rol.Split(';').Contains("PROVEEDORES") && vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol != "PROVEEDORES")
                {
                    if (vEntidadProvOferente.UsuarioCrea == GetSessionUser().NombreUsuario)
                    {

                        toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                    }
                    else if (vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("AdministradorSIA"))
                    {
                        if ((bool)vEntidadProvOferente.OferenteMigrado)
                        {
                            //Obtener fecha de puesta de migración en producción.
                            DateTime fechaPuesta = vProveedorService.GetFechaMigracion(vEntidadProvOferente.IdEntidad);
                            DateTime fechaActual = DateTime.Now;
                            int DiasPermitidosEditar = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DiasPermitidosEditarRegistrosMigrados"]);

                            // Difference in days, hours, and minutes.
                            TimeSpan ts = fechaActual - fechaPuesta;
                            // Difference in days.
                            int differenceInDays = ts.Days;
                            if (differenceInDays <= DiasPermitidosEditar)
                            {
                                toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                            }
                        }
                    }
                }
                else
                {
                    toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
                }


            }
            else
            {
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            }

            toolBar.EstablecerTitulos("Datos Básicos", SolutionPage.Detail.ToString());

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    ///Habilita controles dependiendo del tipo de persona
    /// </summary>
    protected void CargarDatosTipoPersona()
    {
        CargarRegimenTributario();
        CargarTipoDocumentotipoPersona();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ViewControlJuridica(false);
            ViewControlNatural(true);
        }
        else
        {
            ViewControlNatural(false);
        }

        if (tipoper.CodigoTipoPersona == "002")
        {
            ViewControlJuridica(true);
        }
        else if (tipoper.CodigoTipoPersona == "003" || tipoper.CodigoTipoPersona == "004")
        {
            ViewControlJuridica(false);
            ViewControlConsorcioOUnion(true);

        }
    }
    /// <summary>
    /// Habilita paneles para una persona Natural
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlNatural(bool state)
    {
        PnlNatural.Visible = state;
        lblSector.Visible = state;
        lblRegimenTri.Visible = state;
        lblTipoEnt.Visible = state;
        lblVigencia.Visible = state;
        pnlDomicilioLegal.Visible = state;
        ddlTipoEntidad.Visible = state;
        ddlSector.Visible = state;
        ddlRegimenTributario.Visible = state;
        pnlDomicilioLegal.Visible = state;
        rblVigencia.Visible = state;

    }
    /// <summary>
    /// Habilita controles para persona jurídica
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlJuridica(bool state)
    {
        LblRazonSocial.Visible = state;
        TxtRazonSocial.Visible = state;
        PnlReprLegal.Visible = state;
        LbDv.Visible = state;
        txtDv.Visible = state;
        rblVigencia.Visible = state;
        ddlTipoEntidad.Visible = state;
        ddlSector.Visible = state;
        ddlRegimenTributario.Visible = state;
        lblSector.Visible = state;
        lblRegimenTri.Visible = state;
        lblTipoEnt.Visible = state;
        lblVigencia.Visible = state;
        pnlDomicilioLegal.Visible = state;

    }
    /// <summary>
    /// Habilita controles para persona Consorcio o Unión Temporal
    /// </summary>
    /// <param name="state"></param>
    private void ViewControlConsorcioOUnion(bool state)
    {
        LblRazonSocial.Visible = state;
        TxtRazonSocial.Visible = state;
        PnlReprLegal.Visible = state;
        txtDv.Visible = state;
        lblNumInt.Visible = state;
        txtNumInt.Visible = state;
        LbDv.Visible = state;
        txtDv.Visible = state;

    }
    /// <summary>
    /// Llena de valores lista desplegable Departamento Constituida
    /// </summary>
    protected void CargarMunicipioConstituida()
    {
        CargarMunicipioXDepartamento(ddlMunicipioConstituida, Convert.ToInt32(ddlDepartamentoConstituida.SelectedValue));
    }
    /// <summary>
    /// Llena de valores a lista desplegable Municipio constituida
    /// </summary>
    /// <param name="PddlMunicipioConstituida"></param>
    /// <param name="pidDepartament"></param>
    private void CargarMunicipioXDepartamento(DropDownList PddlMunicipioConstituida, int pidDepartament)
    {
        ManejoControles.LlenarExperienciaMunicipio(PddlMunicipioConstituida, "-1", true, pidDepartament);
    }
    /// <summary>
    /// Carga valores de régimen tributario en lista desplegable
    /// </summary>
    protected void CargarRegimenTributario()
    {
        ManejoControles.LlenarRegimenTributario(ddlRegimenTributario, Convert.ToInt32(ddlTipoPersona.SelectedValue), "-1", true);
    }
    /// <summary>
    /// Cargar tipo de documento dependiendo del tipo de persona
    /// </summary>
    protected void CargarTipoDocumentotipoPersona()
    {
        string idTipoPersona = new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDocumento, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDocumento, "-1", true);
        }

    }
    /// <summary>
    /// Cargar datos del sector habilitando panel correspondiente
    /// </summary>
    protected void CargarDatosSector()
    {
        Int32 TipoSetor = Convert.ToInt32(ddlSector.SelectedValue);
        if (TipoSetor == 1)
        {
            PnComplementoPrivada.Visible = true;

        }
        else
        {
            PnComplementoPrivada.Visible = false;

        }

        if (TipoSetor == 2)
        {
            PnComplementoPublicas.Visible = true;
        }
        else
        {
            PnComplementoPublicas.Visible = false;
        }
    }
    /// <summary>
    /// Llenar en lista desplegable Clase Entidad los datos correpondientes
    /// </summary>
    protected void CargarClasedeEntidad()
    {
        ManejoControles.LlenarClasedeEntidadTipodeActividadTipoEntidad(ddlClaseEntidad, Convert.ToInt32(ddlTipoActividad.SelectedValue), Convert.ToInt32(ddlTipoEntidad.SelectedValue), "-1", true, int.Parse(ddlTipoPersona.SelectedValue), int.Parse(ddlSector.SelectedValue), int.Parse(ddlRegimenTributario.SelectedValue));
    }

    protected void CargarExtecionMatruculaMercantil()
    {
        if (ddlExtecionMatruculaMercantil.SelectedValue == "1")
        {
            TxtNumeroMatriculaMercantil.Visible = false;
            CalFechaMatriculaMercantil.Visible = false;
            LBNumeroMatriculaMercantil.Visible = false;
            LbFechaMatriculaMercantil.Visible = false;
        }
        else
        {
            TxtNumeroMatriculaMercantil.Visible = true;
            CalFechaMatriculaMercantil.Visible = true;
            LBNumeroMatriculaMercantil.Visible = true;
            LbFechaMatriculaMercantil.Visible = true;
        }
    }
    /// <summary>
    /// Cargar valores Departamento Dirección comercial en lista desplegable
    /// </summary>
    protected void CargarDepartamentoDireccionComercial()
    {
        CargarMunicipioXDepartamento(ddlMunicipioDireccionComercial, Convert.ToInt32(ddlDepartamentoDireccionComercial.SelectedValue));
    }
    /// <summary>
    /// Buscar Documentos
    /// </summary>
    private void BuscarDocumentos()
    {
        try
        {

            int vIdEntidad = Convert.ToInt32(hfIdEntidad.Value);
            string vTipoProveedor = ddlTipoPersona.SelectedValue;
            string vTipoSector = ddlSector.SelectedValue;
            string vTipoEntidad = ddlTipoEntidad.SelectedValue;

            gvDocDatosBasicoProv.DataSource = vProveedorService.ConsultarDocDatosBasicooProv_IdEntidad_TipoPersona(vIdEntidad, vTipoProveedor, vTipoSector, "", vTipoEntidad);
            gvDocDatosBasicoProv.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Buscar Documentos
    /// </summary>
    private void BuscarDocumentosRegTercero()
    {
        try
        {
            int vIdTercero = vEntidadProvOferente.IdTercero;
            string sPersona = ddlTipoPersona.SelectedValue;
            if (vIdTercero > 0)
            {
                gvDocAdjuntos.DataSource = vProveedorService.ConsultarDocAdjuntoTerceros_IdTercero_TipoPersona(vIdTercero, sPersona, null);
                gvDocAdjuntos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvDocDatosBasicoProv.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("DocDatosBasicoProv.IdDocAdjunto", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region ObservacionesDatosBasicos

    private void BuscarObservacionesDatosBasicos()
    {
        ucHistorialRevisiones1.Titulo = "Historial de observaciones del módulo Datos Básicos";
        ucHistorialRevisiones1.MensajeGuardar = "Está seguro del resultado de su revisión?";
        ucHistorialRevisiones1.TituloConfirmaYAprueba = "¿Confirma que los datos coinciden con el documento  adjunto y lo aprueba?";
        int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
        List<ValidarInfo> lista = vProveedorService.ConsultarValidarInfoDatosBasicosEntidads(vIdEntidad, null, null).ToList<ValidarInfo>();
        ucHistorialRevisiones1.DataSource = lista;
        ucHistorialRevisiones1.ConfirmaVisible = false;
        //
        //Si está activa la opcion de ver Panel para agregar Observaciones
        //

        if (!string.IsNullOrEmpty(GetSessionParameter("ValidarProveedor.NroRevision").ToString()))
        {
            int NroRevision = Convert.ToInt16(GetSessionParameter("ValidarProveedor.NroRevision").ToString());
            ucHistorialRevisiones1.NroRevision = NroRevision;
            ucHistorialRevisiones1.ConfirmaVisible = true;
            ucHistorialRevisiones1.eventoGuardarObservaciones += new ImageClickDelegate(OnGuardarObservacionesDatosBasicos);

            //Despues de cargar se cambia el estado a En Validación
            String idEntidad = (String)GetSessionParameter("EntidadProvOferente.IdEntidad");
            Int32 iIdEntidad = Convert.ToInt32(idEntidad);
            int resultado = 0;

            Icbf.Proveedor.Entity.EntidadProvOferente prov = vProveedorService.ConsultarEntidadProvOferente(iIdEntidad);
            if (!prov.Finalizado) // SI NO ESTA FINALIZADO
            {
                prov.IdEstado = 5; //Estado En Validación
                prov.Finalizado = false;
                prov.UsuarioModifica = GetSessionUser().NombreUsuario;
                prov.FechaModifica = DateTime.Now;

                resultado = vProveedorService.ModificarEntidadProvOferente_EstadoDocumental(prov);


                if (resultado >= 1)
                {
                    //Exitoso
                    //Se carga el control con la última información
                    ValidarInfoDatosBasicosEntidad vValidarInfoDB = vProveedorService.ConsultarValidarInfoDatosBasicosEntidad_Ultima(iIdEntidad);
                    ucHistorialRevisiones1.Observaciones = vValidarInfoDB.Observaciones;
                    ucHistorialRevisiones1.ConfirmaYAprueba = vValidarInfoDB.ConfirmaYAprueba;

                    Session["ucHistorialRevisiones1"] = ucHistorialRevisiones1;

                }
                else
                {
                    toolBar.MostrarMensajeError("No pudo cambiarse al estado En Validación");
                }
            }
        }
    }

    private void OnGuardarObservacionesDatosBasicos()
    {
        try
        {
            //inicializa variables
            int vResultado;
            string vObservaciones = string.Empty;
            bool bNuevo = false;
            ValidarInfoDatosBasicosEntidad vValidarInfoDatosBasicosEntidad = new ValidarInfoDatosBasicosEntidad();


            //toma la entidad en session
            String vIdEntidad = Convert.ToString(GetSessionParameter("EntidadProvOferente.IdEntidad"));


            //Obtiene la info del usercontrol
            ValidarInfo info = ((Page_Proveedor_UserControl_ucHistorialRevisiones)Session["ucHistorialRevisiones1"]).vValidarInfo;
            Int32 iIdEntidad = Convert.ToInt32(vIdEntidad);

            //obtiene la ultima observacion disponible
            vValidarInfoDatosBasicosEntidad = vProveedorService.ConsultarValidarInfoDatosBasicosEntidad_Ultima(iIdEntidad);

            //si existe en la bd la actualiza
            if (vValidarInfoDatosBasicosEntidad.IdEntidad == 0)
            {
                //No existe
                vValidarInfoDatosBasicosEntidad.IdEntidad = iIdEntidad;
                bNuevo = true;
            }

            if (info.Observaciones != null)
                vObservaciones = Convert.ToString(info.Observaciones).Trim();
            vValidarInfoDatosBasicosEntidad.Observaciones = vObservaciones;
            vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba = Convert.ToBoolean(info.ConfirmaYAprueba);
            vValidarInfoDatosBasicosEntidad.NroRevision = info.NroRevision;

            //InformacionAudioria(vValidarInfoFinancieraEntidad, this.PageName, vSolutionPage);

            int idEstado = 0;
            if (vValidarInfoDatosBasicosEntidad.ConfirmaYAprueba == true)
                idEstado = 2;//Se obtiene el estado Validado
            else
                idEstado = 3;//Se obtiene el estado Por Ajustar

            if (bNuevo)
            {
                vValidarInfoDatosBasicosEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.InsertarValidarInfoDatosBasicosEntidad(vValidarInfoDatosBasicosEntidad, idEstado);
            }
            else
            {
                vValidarInfoDatosBasicosEntidad.UsuarioCrea = GetSessionUser().CorreoElectronico;
                vResultado = vProveedorService.ModificarValidarInfoDatosBasicosEntidad(vValidarInfoDatosBasicosEntidad, idEstado);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ValidarProveedor.ObservacionGuardada", "1");
                //toolBar.MostrarMensajeGuardado("Guardado exitosamente");
                //BuscarObservacionesDatosBasicos();
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void llenarGridAuditoria()
    {

        if (!GetSessionParameter("EntidadProvOferente.IdEntidad").ToString().Equals(""))
        {
            int vIdEntidad = Convert.ToInt32(GetSessionParameter("EntidadProvOferente.IdEntidad"));
            List<AuditoriaAccionesEntidad> listaAuditoria = new List<AuditoriaAccionesEntidad>();
            listaAuditoria = vProveedorService.GetAuditoriaAccionesEntidad(vIdEntidad);

            this.gridAuditoriaDatBAsicos.DataSource = listaAuditoria;
            this.gridAuditoriaDatBAsicos.DataBind();
        }
        else
        {
            this.gridAuditoriaDatBAsicos.DataSource = null;
            this.gridAuditoriaDatBAsicos.DataBind();
        }
    }
    protected void gridAuditoriaDatBAsicos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gridAuditoriaDatBAsicos.PageIndex = e.NewPageIndex;
        this.llenarGridAuditoria();
        this.mdpDatosAuditoria.Show();
    }
    #endregion
    protected void lnkbtnAuditoria_Click1(object sender, EventArgs e)
    {
        //this.llenarGridAuditoria();
        this.mdpDatosAuditoria.Show();

    }

    protected void gvCodigoUNSPSC_SelectedIndexChanged(object sender, EventArgs e)
    { }

    protected void gvCodigoUNSPSC_RowCommand(object sender, GridViewCommandEventArgs e)
    { }

    protected void gvCodigoUNSPSC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    { }

    protected void gvCodigoUNSPSC_RowDeleting(object sender, GridViewDeleteEventArgs e)
    { }

    protected void ddlTipoPersona_SelectedIndexChanged1(object sender, EventArgs e)
    {
        CargarClasedeEntidad();
    }

    protected void ddlRegimenTributario_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarClasedeEntidad();
    }
}
