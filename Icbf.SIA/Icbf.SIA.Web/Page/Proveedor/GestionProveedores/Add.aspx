<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_EntidadProvOferente_Add"
    MaintainScrollPositionOnPostback="false" Async="true" %>


<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fechaICBF" TagPrefix="uc4" %>

<%@ Register Src="../../../General/General/Control/fechaJS.ascx" TagName="fechaJS"
    TagPrefix="uc3" %>
<%@ Register Src="../../../General/General/Control/IcbfDireccion.ascx" TagName="IcbfDireccion"
    TagPrefix="uc2" %>
<asp:Content ID="ContentPrincipal" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdTercero" runat="server" />
    <asp:HiddenField ID="hfIdEntidad" runat="server" />
    <asp:HiddenField ID="HfIdArchivo" runat="server" />
    <asp:HiddenField ID="hfValidateDocumentsBasicos" runat="server" Value="0" />
    <asp:HiddenField ID="hfValidateDocumentsTerceros" runat="server" Value="0" />
    <asp:HiddenField ID="hfIDDocYNombreAActualizar" runat="server" Value="" />
    <asp:HiddenField ID="hfTipoPersona" runat="server" Value="0" />
    <div>
        <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="../../../Scripts/jquery_blockUI.js" type="text/javascript"></script>
        <script src="../../../Scripts/MaxLength.min.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript">
            function pageLoad() {
                $("[id*=txtObservacionesCorreccion]").MaxLength({ MaxLength: 200, DisplayCharacterCount: false });

            }

            function BuscarInputs() {
                if (Page_IsValid) {
                    Loading();
                    var actividadPrincipal = document.getElementById('<%= TxtActividadCiiuPrin.ClientID %>').value;
                    var fechaPrincipal = document.getElementById('cphCont_CalFechaActividadPrincipal_txtFecha').value;
                    var actividadSecundaria = document.getElementById('<%= TxtActividadCiiuSegu.ClientID %>').value;
                    var fechaSecundaria = document.getElementById('cphCont_CalFechaActividadSegundaria_txtFecha').value;
                    if ((actividadPrincipal != '' && fechaPrincipal == '') || (actividadPrincipal == '' && fechaPrincipal != '')) {
                        window.setTimeout(function () { document.getElementById('cphCont_CalFechaActividadPrincipal_txtFecha').focus(); }, 0);
                    }
                    if ((actividadSecundaria != '' && fechaSecundaria == '') || (actividadSecundaria == '' && fechaSecundaria != '')) {
                        window.setTimeout(function () { document.getElementById('cphCont_CalFechaActividadSegundaria_txtFecha').focus(); }, 0);
                    }
                }
            }

            function limitText(limitField, limitNum) {
                if (limitField.value.length > limitNum) {
                    limitField.value = limitField.value.substring(0, limitNum);
                } else {
                    //limitCount.value = limitNum - limitField.value.length;
                }
            }

            // Actividad economica principal

            function GetCiiuPrin() {
                window_showModalDialog('../../../Page/Proveedor/GestionProveedores/ConsultarCiiu.aspx', setCiiuPrin, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setCiiuPrin(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 0) {
                        document.getElementById('<%= TxtActividadCiiuPrin.ClientID %>').value = retLupa[1] + " - " + retLupa[2] ;
                        document.getElementById('<%= hfDesActividadCiiuPrin.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= hfIdActividadCiiuPrin.ClientID %>').value = retLupa[0];
                        validarActividades();
                    } else {
                        ocultaImagenLoading();
                    }
                } else {
                    ocultaImagenLoading();
                }
            }


            // Actividad economica secundaria
            function GetCiiuSegun() {
                window_showModalDialog('../../../Page/Proveedor/GestionProveedores/ConsultarCiiu.aspx', SetCiiuSec, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function SetCiiuSec(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 0) {
                        document.getElementById('<%= TxtActividadCiiuSegu.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= hfDesActividadCiiuSegu.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= HfActividadCiiuSegu.ClientID %>').value = retLupa[0];
                        validarActividades();
                    } else {
                        ocultaImagenLoading();
                    }
                } else {
                    ocultaImagenLoading();
                }
            }


            function validarActividades() {
                ValidatorValidate(document.getElementById('<%=rvTxtActividadCiiuSecund.ClientID %>'));
            }



            function validaActividadesDiferentes(source, args) {
                if (document.getElementById('<%= hfIdActividadCiiuPrin.ClientID %>').value == document.getElementById('<%= HfActividadCiiuSegu.ClientID %>').value) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }


            function funValidaNombreObligatorio(source, args) {
                args.IsValid = EsNombreValido(args.Value);
            }

            function funValidaNombreOpcional(source, args) {
                if (args.Value.length > 0) {
                    args.IsValid = EsNombreValido(args.Value);
                } else {
                    args.IsValid = true;
                }
            }

            function EsNombreValido(nombre) {
                if (nombre.length > 2) {
                    return true;
                } else if (nombre.length == 2) {
                    var car1 = nombre.charAt(0);
                    var car2 = nombre.charAt(1);

                    if (car1 != car2) {
                        return true;
                    }
                }
                return false;
            }
            function ValidaIndicativo(sender, args) {

                var control = $('#' + sender.controltovalidate + '');
                var intValue = control.val();
                var ntele = document.getElementById('<%= TxtIndicativoRepr.ClientID %>').value;
                var rfcIndicativo = document.getElementById('<%= rfvIndicativoRepr.ClientID %>');
                var cvIndicativo = document.getElementById('<%= cvIndicativoRep.ClientID %>');
                if (cvIndicativo) {
                    if (intValue != "") {
                        if (ntele != "") {
                            rfcIndicativo.Enabled = false;
                            cvIndicativo.isValid = true;
                            rfcIndicativo.isValid = true;
                            return true;
                        } else {
                            rfcIndicativo.Enabled = true;
                            cvIndicativo.isValid = false;
                            rfcIndicativo.isValid = false;
                            return false;
                        }
                    } else {
                        rfcIndicativo.Enabled = false;
                        cvIndicativo.IsValid = true;
                        rfcIndicativo.isValid = true;
                        return true;
                    }
                }

            }

            function ValidaIndicativo1(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var intValue = control.val();
                var ntele = document.getElementById('<%= TxtTelefonoRepr.ClientID %>').value;
                var rfcIndicativo = document.getElementById('<%= rfvIndicativoRepr.ClientID %>');
                var cvIndicativo = document.getElementById('<%= cvIndicativoRep.ClientID %>');
                if (ntele != "") {
                    if (intValue != "") {
                        args.IsValid = true;
                        rfcIndicativo.Enabled = false;
                        cvIndicativo.IsValid = true;
                        return true;
                    } else {
                        rfcIndicativo.Enabled = true;
                        cvIndicativo.isValid = false;
                        args.IsValid = false;
                        return false;
                    }
                } else {
                    args.IsValid = true;
                    rfcIndicativo.Enabled = false;
                    cvIndicativo.IsValid = true;
                    return true;
                }
            }

            function Confirmacion() {
                var confirm_value = document.createElement("INPUT");

                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";

                if (confirm("Existen datos que no se han guardado. Desea hacerlo ahora?")) {

                    confirm_value.value = "Yes";

                } else {

                    confirm_value.value = "No";

                }

                document.forms[0].appendChild(confirm_value);

                //                var seleccion = confirm("Existen datos que no se han guardado. Desea hacerlo ahora?");
                //                //usado para que no haga postback el boton de asp.net cuando 
                //                //no se acepte el confirm
                //                return seleccion;

            }
            function Confirm() {

                var confirm_value = document.createElement("INPUT");

                confirm_value.type = "hidden";

                confirm_value.name = "confirm_value";

                if (confirm("Existen datos que no se han guardado. Desea hacerlo ahora?")) {

                    confirm_value.value = "Yes";

                } else {

                    confirm_value.value = "No";

                }

                document.forms[0].appendChild(confirm_value);
            }
            function EjecutarJS() {

            }

            function funValidaNumIdentificacion(source, args) {
                args.IsValid = validaNumIdentificacion(args.Value);
            }

            function funValidaNumIdentificacionRepr(source, args) {
                args.IsValid = validaNumIdentificacionRepr(args.Value);
            }


            function validaNumIdentificacion(numero) {
                var tipodoc = document.getElementById('<%= ddlTipoDocumento.ClientID %>').value;
                if (tipodoc == "1") {
                    if (numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6 && numero.length != 7 && numero.length != 8 && numero.length != 10 && numero.length != 11)
                        return false;
                    else
                        return true;
                }
                if (tipodoc == "2") {
                    if (numero.length != 1 && numero.length != 2 && numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6)
                        return false;
                    else
                        return true;
                }
                if (tipodoc == "7") {
                    if (numero.length != 9)
                        return false;
                    else
                        return true;
                }
                return true;
            }

            function validaNumIdentificacionRepr(numero) {
                var tipodoc = document.getElementById('<%= ddlTipoDocumentoRepr.ClientID %>').value;
                if (tipodoc == "1") {
                    if (numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6 && numero.length != 7 && numero.length != 8 && numero.length != 10 && numero.length != 11)
                        return false;
                    else
                        return true;
                }
                if (tipodoc == "2") {
                    if (numero.length != 1 && numero.length != 2 && numero.length != 3 && numero.length != 4 && numero.length != 5 && numero.length != 6)
                        return false;
                    else
                        return true;
                }
                if (tipodoc == "7") {
                    if (numero.length != 9)
                        return false;
                    else
                        return true;
                }
                return true;
            }
            function funValidaFechaActividadPrincipal(source, args) {
                args.IsValid = validaFechaActividadPrincipal(args.Value);
            }
            function funValidaFechaActividadSecundaria(source, args) {
                args.IsValid = validaFechaActividadSecundaria(args.Value);
            }
            function funValidaFechaActividadPrincipal(fecha) {
                var actividad = document.getElementById('<%= TxtActividadCiiuPrin.ClientID %>').value;
                if (fecha.length > 0 && actividad.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
            function funValidaFechaActividadSecundaria(fecha) {
                var actividad = document.getElementById('<%= TxtActividadCiiuSegu.ClientID %>').value;
                if (fecha.length > 0 && actividad.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            function CheckLengthPrimerNombre(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");
                var flagEspacios = false;
                if (args.Value.length >= 2) {
                    //----validar que no tenga espcaios al inicio y final del nombre
                    for (i = 0; i <= strSplitDatos.length - 1; i++) {
                        if (strSplitDatos[i] == ' ') {
                            args.IsValid = false;
                            sender.innerHTML = "No se permiten espacios en blanco en su primer nombre";
                            flagEspacios = true;
                            break;
                        }
                    }
                    if (!flagEspacios) {
                        if (args.Value.length == 2) {
                            if (strSplitDatos[0] == (strSplitDatos[1])) {
                                args.IsValid = false;
                                sender.innerHTML = "Caracteres iguales no válidos, registre su primer nombre";
                            }
                            else {
                                args.IsValid = true;
                            }
                        }
                        if (args.Value.length > 2) {
                            args.IsValid = true;
                        }
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Primer nombre inválido";
                }
            }

            function CheckLengthSegundoNombre(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");

                if (args.Value.length >= 2) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su segundo nombre";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Segundo nombre inválido";
                }
            }

            function CheckLengthPrimerApellido(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");

                if (args.Value.length >= 2) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su primer apellido";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Primer apellido  inválido";
                }
            }

            function CheckLengthSegundoApellido(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");

                if (args.Value.length >= 2) {
                    if (args.Value.length == 2) {
                        if (strSplitDatos[0] == (strSplitDatos[1])) {
                            args.IsValid = false;
                            sender.innerHTML = "Caracteres iguales no válidos, registre su segundo apellido";
                        }
                        else {
                            args.IsValid = true;
                        }
                    }
                    if (args.Value.length > 2) {
                        args.IsValid = true;
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Segundo apellido  inválido";
                }
            }

            function CheckLengthPrimerNombreRep(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");
                var flagEspacios = false;
                if (args.Value.length >= 2) {
                    //----validar que no tenga espcaios al inicio y final del nombre
                    for (i = 0; i <= strSplitDatos.length - 1; i++) {
                        if (strSplitDatos[i] == ' ') {
                            args.IsValid = false;
                            sender.innerHTML = "No se permiten espacios en blanco en el primer nombre del representante legal";
                            flagEspacios = true;
                            break;
                        }
                    }
                    if (!flagEspacios) {
                        if (args.Value.length == 2) {
                            if (strSplitDatos[0] == (strSplitDatos[1])) {
                                args.IsValid = false;
                                sender.innerHTML = "Caracteres iguales no válidos, registre el primer nombre de representante legal";
                            }
                            else {
                                args.IsValid = true;
                            }
                        }
                        if (args.Value.length > 2) {
                            args.IsValid = true;
                        }
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Primer nombre de representante legal inválido";
                }
            }

            //CheckLengthSegundoNombreRep
            function CheckLengthSegundoNombreRep(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");
                var flagEspacios = false;
                if (args.Value.length >= 2) {
                    //----validar que no tenga espcaios al inicio y final del nombre
                    if (strSplitDatos[0] == ' ') {
                        args.IsValid = false;
                        sender.innerHTML = "No se permiten espacios en blanco al incio del segundo nombre del representante legal";
                        flagEspacios = true;
                    }
                    else if (strSplitDatos[strSplitDatos.length - 1] == ' ') {
                        args.IsValid = false;
                        sender.innerHTML = "No se permiten espacios en blanco al final segundo nombre del representante legal";
                        flagEspacios = true;
                    }
                    if (!flagEspacios) {
                        if (args.Value.length == 2) {
                            if (strSplitDatos[0] == (strSplitDatos[1])) {
                                args.IsValid = false;
                                sender.innerHTML = "Caracteres iguales no válidos, registre el segundo nombre de representante legal";
                            }
                            else {
                                args.IsValid = true;
                            }
                        }
                        if (args.Value.length > 2) {
                            args.IsValid = true;
                        }
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Segundo nombre de representante legal inválido";
                }
            }

            //CheckLengthPrimerApellidoRep
            function CheckLengthPrimerApellidoRep(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");
                var flagEspacios = false;
                if (args.Value.length >= 2) {
                    //----validar que no tenga espcaios al inicio y final del nombre
                    for (i = 0; i <= strSplitDatos.length - 1; i++) {
                        if (strSplitDatos[i] == ' ') {
                            args.IsValid = false;
                            sender.innerHTML = "No se permiten espacios en blanco en el primer apellido del representante legal";
                            flagEspacios = true;
                            break;
                        }
                    }
                    if (!flagEspacios) {
                        if (args.Value.length == 2) {
                            if (strSplitDatos[0] == (strSplitDatos[1])) {
                                args.IsValid = false;
                                sender.innerHTML = "Caracteres iguales no válidos, registre el primer apellido de representante legal";
                            }
                            else {
                                args.IsValid = true;
                            }
                        }
                        if (args.Value.length > 2) {
                            args.IsValid = true;
                        }
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Primer apellido de representante legal inválido";
                }
            }

            function CheckLengthSegundoApellidoRep(sender, args) {
                var control = $('#' + sender.controltovalidate + '');
                var strDatos = control.val();
                var strSplitDatos = strDatos.split("");
                var flagEspacios = false;
                if (args.Value.length >= 2) {
                    //----validar que no tenga espcaios al inicio y final del seg apellido
                    if (strSplitDatos[0] == ' ') {
                        args.IsValid = false;
                        sender.innerHTML = "No se permiten espacios en blanco al incio del segundo apellido del representante legal";
                        flagEspacios = true;
                    }
                    else if (strSplitDatos[strSplitDatos.length - 1] == ' ') {
                        args.IsValid = false;
                        sender.innerHTML = "No se permiten espacios en blanco al final segundo apellido del representante legal";
                        flagEspacios = true;
                    }
                    if (!flagEspacios) {
                        if (args.Value.length == 2) {
                            if (strSplitDatos[0] == (strSplitDatos[1])) {
                                args.IsValid = false;
                                sender.innerHTML = "Caracteres iguales no válidos, registre el segundo apellido de representante legal";
                            }
                            else {
                                args.IsValid = true;
                            }
                        }
                        if (args.Value.length > 2) {
                            args.IsValid = true;
                        }
                    }
                }
                else if (args.Value.length == 1) {
                    args.IsValid = false;
                    sender.innerHTML = "Segundo apellido de representante legal inválido";
                }
            }
            //-----------------------------------------

            function validatorTel() {
                //TxtIndicativo
                if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "1") {
                    if (document.getElementById('<%=TxtIndicativo.ClientID %>').value.length > 0) {
                        ValidatorEnable(document.getElementById('<%=rvTelefonoPerNat.ClientID %>'), true);
                        ValidatorValidate(document.getElementById('<%=rvTelefonoPerNat.ClientID %>'));
                    }
                    else if (document.getElementById('<%=TxtIndicativo.ClientID %>').value.length == 0) {
                        ValidatorEnable(document.getElementById('<%=rvTelefonoPerNat.ClientID %>'), false);
                    }
            }

            if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
                    if (document.getElementById('<%=TxtIndicativo.ClientID %>').value.length > 0) {
                    ValidatorEnable(document.getElementById('<%=rvTelefono.ClientID %>'), true);
                        ValidatorValidate(document.getElementById('<%=rvTelefono.ClientID %>'));
                    }
                    else if (document.getElementById('<%=TxtIndicativo.ClientID %>').value.length == 0) {
                        ValidatorEnable(document.getElementById('<%=rvTelefono.ClientID %>'), false);
                    }
            }
        }

        function validatorTelefono() {
            //TxtIndicativo
            if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "1" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
                if (document.getElementById('<%=TxtTelefono.ClientID %>').value.length > 0) {
                    ValidatorEnable(document.getElementById('<%=rvIndicativoPerNat.ClientID %>'), true);
                    ValidatorValidate(document.getElementById('<%=rvIndicativoPerNat.ClientID %>'));
                }
                else if (document.getElementById('<%=TxtTelefono.ClientID %>').value.length == 0) {
                    ValidatorEnable(document.getElementById('<%=rvIndicativoPerNat.ClientID %>'), false);
                }
        }
    }

    function validatorTelefonoRep() {
        //TxtIndicativo
        if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "2" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
            if (document.getElementById('<%=TxtTelefonoRepr.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rfvIndicativoRepr.ClientID %>'), true);
                ValidatorValidate(document.getElementById('<%=rfvIndicativoRepr.ClientID %>'));
            }
            else if (document.getElementById('<%=TxtTelefonoRepr.ClientID %>').value.length == 0) {
                ValidatorEnable(document.getElementById('<%=rfvIndicativoRepr.ClientID %>'), false);
            }
    }
}

function validatorIndicativo() {
    //TxtIndicativo
    if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "1") {
            if (document.getElementById('<%=TxtExtencion.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rvIndicativoPerNat.ClientID %>'), true);
                ValidatorValidate(document.getElementById('<%=rvIndicativoPerNat.ClientID %>'));
            }
            else if (document.getElementById('<%=TxtExtencion.ClientID %>').value.length == 0) {
                ValidatorEnable(document.getElementById('<%=rvIndicativoPerNat.ClientID %>'), false);
            }
    } if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
            if (document.getElementById('<%=TxtExtencion.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rvTelefono.ClientID %>'), true);
                ValidatorValidate(document.getElementById('<%=rvTelefono.ClientID %>'));
            }
            else {
                ValidatorEnable(document.getElementById('<%=rvTelefono.ClientID %>'), false);
            }
        }
    }


    function validatorTelRep() {
        //TxtIndicativo
        if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "2" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
            if (document.getElementById('<%=TxtIndicativoRepr.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'), true);
                    ValidatorValidate(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'));
                }
                else if (document.getElementById('<%=TxtIndicativoRepr.ClientID %>').value.length == 0) {
                    ValidatorEnable(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'), false);
                }
        }
        if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
            if (document.getElementById('<%=TxtIndicativoRepr.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'), true);
                    ValidatorValidate(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'));
                }
                else if (document.getElementById('<%=TxtIndicativoRepr.ClientID %>').value.length == 0) {
                    ValidatorEnable(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'), false);
                }
        }
    }


    function validatorIndicativoRep() {
        //TxtIndicativo
        if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "2") {
            if (document.getElementById('<%=TxtExtencionRepr.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rfvIndicativoRepr.ClientID %>'), true);
                    ValidatorValidate(document.getElementById('<%=rfvIndicativoRepr.ClientID %>'));
                }
                else if (document.getElementById('<%=TxtExtencionRepr.ClientID %>').value.length == 0) {
                    ValidatorEnable(document.getElementById('<%=rfvIndicativoRepr.ClientID %>'), false);
                }
        } if (document.getElementById('<%=hfTipoPersona.ClientID %>').value == "3" || document.getElementById('<%=hfTipoPersona.ClientID %>').value == "4") {
            if (document.getElementById('<%=TxtExtencionRepr.ClientID %>').value.length > 0) {
                ValidatorEnable(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'), true);
                    ValidatorValidate(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'));
                }
                else {
                    ValidatorEnable(document.getElementById('<%=rvTxtTelefonoRepr.ClientID %>'), false);
                }
            }
        }
        $(document).ready(function () {
            validatorTel();
            validatorTelRep();
            validatorIndicativo();
            validatorIndicativoRep();
        });

        function GetCodUNSPSC() {
            window_showModalDialog('../../../Page/Proveedor/InfoExperienciaEntidad/ConsultaUNSPSC.aspx', setUnspsc, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setUnspsc(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 0) {
                    document.getElementById('<%= hfIdTipoCodUNSPSC.ClientID %>').value = retLupa[0];
                    var objLimpiar = document.getElementById('linkLimpiar');
                    objLimpiar.style.visibility = 'visible';
                    DoPostBack();
                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }
        }

        function DoPostBack() {
            __doPostBack('<%= gvCodigoUNSPSC.ClientID %>', '');
        }

        function LimpiarUNSPSC() {
            var codigo = document.getElementById('<%= hfIdTipoCodUNSPSC.ClientID %>');

                if (confirm('¿Está seguro de eliminar esta clasificación?')) {
                    descripcion.value = "";
                    codigo.value = "";

                    var objLimpiar = document.getElementById('linkLimpiar');
                    objLimpiar.style.visibility = 'hidden';
                }
            }

            function validaNroDocumento(e) {

                tecla = (document.all) ? e.keyCode : e.which;
                //Tecla de retroceso para borrar, siempre la permite
                if (tecla == 8) {
                    return true;
                }

                var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDocumentoRepr").value;

                //9 - Tipo Documento Carne Diplomatico
                if (vTipoDocumentoText == 9) {
                    // Patron de entrada, en este caso solo acepta numeros y letras
                    patron = /[0-9A-Za-z]/;
                }
                else {
                    // Patron de entrada, en este caso solo acepta numeros
                    patron = /[0-9]/;
                }

                tecla_final = String.fromCharCode(tecla);
                tecla_final = tecla_final.toUpperCase();
                return patron.test(tecla_final);

            }

        </script>
        <%--<asp:UpdatePanel ID="updatos" runat="server">
         <ContentTemplate>--%>
        <style type="text/css">
            .ModalPopupBG {
                background-color: #666699;
                filter: alpha(opacity=50);
                opacity: 0.7;
            }
        </style>
        <h3 class="lbBloque">
            <asp:Label ID="LabelTitulo" runat="server" Text="Datos B&aacute;sicos"></asp:Label>
        </h3>
        <asp:Panel ID="UpdatePanel3" runat="server">
            <table width="80%" align="center">
                <tr class="rowA">
                    <td colspan="3">Si ha tenido v&iacute;nculo contractual con el ICBF, especifique la fecha de inicio.</td>
                </tr>
                <tr class="rowB">
                    <td>
                        <uc4:fechaICBF ID="FechaICBF" runat="server" Enabled="true" Requerid="False" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">Consecutivo interno
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">
                        <asp:TextBox runat="server" ID="TxtConsecutivoInterno" Enabled="False"></asp:TextBox>
                    </td>
                    <td width="50%" style="text-align: right">
                        <asp:LinkButton ID="lnkbtnAuditoria" runat="server" Text="Datos de Auditoria" OnClick="lnkbtnAuditoria_Click"></asp:LinkButton>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">Tipo de Persona o asociaci&oacute;n
                    </td>
                    <td width="50%">Correo electr&oacute;nico
                        <asp:Label ID="lblCorreoElectronicoAS" runat="server" Text="*"></asp:Label>
                        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                            ControlToValidate="TxtCorreoElectronico" SetFocusOnError="true" ValidationGroup="btnGuardar"
                            Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                        </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCorreoElectronico" ErrorMessage="Registre un correo electr&oacute;nico v&aacute;lido"
                            ControlToValidate="TxtCorreoElectronico" SetFocusOnError="true" ValidationGroup="btnGuardar"
                            ForeColor="Red" Display="Dynamic">Registre un correo electr&oacute;nico v&aacute;lido</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">
                        <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="True" OnSelectedIndexChanged="DdlTipoPersonaSelectedIndexChanged"
                            Width="80%" TabIndex="1" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td width="50%">
                        <asp:TextBox runat="server" ID="TxtCorreoElectronico" MaxLength="50" Enabled="true"
                            Width="80%" TabIndex="2"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">Tipo identificaci&oacute;n
                    </td>
                    <td width="50%">
                        <asp:Label runat="server" ID="lbIdentificacion" Text="N&uacute;mero de identificaci&oacute;n"
                            Visible="True"></asp:Label>
                        <asp:CustomValidator ID="customValidatxtNumIdentificacion" runat="server" ControlToValidate="txtIdentificacion"
                            SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                            ClientValidationFunction="funValidaNumIdentificacion">El n&uacute;mero de identificaci&oacute;n no es v&aacute;lido</asp:CustomValidator>
                        <asp:RegularExpressionValidator runat="server" ID="revNumDocCero" ErrorMessage="El documento no debe empezar con cero (0)"
                            ControlToValidate="txtIdentificacion" SetFocusOnError="true" ValidationGroup="btnGuardar"
                            ForeColor="Red" Display="Dynamic" ValidationExpression="^[A-Za-z1-9]+.*$"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblDV" Text="DV" Visible="True"></asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="False" Width="300px"
                            TabIndex="3">
                        </asp:DropDownList>
                        <asp:HiddenField runat="server" ID="hdIdTreceroProveedro"></asp:HiddenField>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtIdentificacion" MaxLength="11" Enabled="False"
                            TabIndex="4"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDv" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:Label runat="server" ID="LblRazonSocial" Text="Raz&oacute;n social" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">
                        <asp:TextBox runat="server" ID="TxtRazonSocial" Visible="False" Enabled="False" MaxLength="128"
                            Width="100%" TabIndex="6"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" width="100%">
                        <asp:Panel runat="server" ID="PnlNatural" Visible="False" Style="width: 100%">
                            <table style="width: 100%">
                                <tr class="rowA">
                                    <td width="50%">
                                        <asp:Label runat="server" ID="LblPrimerNombre" Text="Primer Nombre * "></asp:Label>
                                        <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="TxtPrimerNombre"
                                            FilterType="Custom,LowercaseLetters,UppercaseLetters" InvalidChars=" " ValidChars="Ã¡Ã©Ã­Ã³ÃºÃÃ‰ÃÃ“ÃšÃ±Ã‘ .,@_():;ÁÉÍÓÚÑ" />
                                        <asp:RequiredFieldValidator runat="server" ID="rqFvTxtPrimerNombre" SetFocusOnError="true"
                                            ErrorMessage="Registre su primer nombre" ValidationGroup="btnGuardar" ControlToValidate="TxtPrimerNombre"
                                            Display="Dynamic" ForeColor="Red" />
                                        <asp:CustomValidator runat="server" ID="CustomValidator2" SetFocusOnError="True"
                                            ClientValidationFunction="CheckLengthPrimerNombre" ErrorMessage="Registre su primer nombre"
                                            ValidationGroup="btnGuardar" ControlToValidate="TxtPrimerNombre" Display="Dynamic"
                                            ForeColor="Red" Enabled="false" />
                                    </td>
                                    <td width="50%">
                                        <asp:Label runat="server" ID="LblSegundoNombre" Text="Segundo Nombre "></asp:Label>
                                        <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                                            FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="Ã¡Ã©Ã­Ã³ÃºÃÃ‰ÃÃ“ÃšÃ±Ã‘ .,@_():;ÁÉÍÓÚÑ" />
                                        <asp:CustomValidator runat="server" ID="CustomValidator3" SetFocusOnError="True"
                                            ClientValidationFunction="CheckLengthSegundoNombre" ErrorMessage="Registre su segundo nombre"
                                            ValidationGroup="btnGuardar" ControlToValidate="TxtSegundoNombre" Display="Dynamic"
                                            ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell" width="50%;">
                                        <asp:TextBox runat="server" ID="TxtPrimerNombre" Enabled="False" Width="80%" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td class="Cell" width="50%;">
                                        <asp:TextBox runat="server" ID="TxtSegundoNombre" Enabled="False" MaxLength="50"
                                            Width="80%" TabIndex="8"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td width="50%">
                                        <asp:Label runat="server" ID="LblPrimerApellido" Text="Primer Apellido * "></asp:Label>
                                        <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="TxtPrimerApellido"
                                            FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="Ã¡Ã©Ã­Ã³ÃºÃÃ‰ÃÃ“ÃšÃ±Ã‘ .,@_():;ÁÉÍÓÚÑ" />
                                        <asp:RequiredFieldValidator runat="server" ID="rqFvTxtPrimerApellido" SetFocusOnError="true"
                                            ErrorMessage="Registre su primer apellido" ValidationGroup="btnGuardar" ControlToValidate="TxtPrimerApellido"
                                            Display="Dynamic" ForeColor="Red" />
                                        <asp:CustomValidator runat="server" ID="cvTxtPrimerApellido" SetFocusOnError="True"
                                            ClientValidationFunction="CheckLengthPrimerApellido" ErrorMessage="Registre su primer apellido"
                                            ValidationGroup="btnGuardar" ControlToValidate="TxtPrimerApellido" Display="Dynamic"
                                            ForeColor="Red" />
                                    </td>
                                    <td width="50%">
                                        <asp:Label runat="server" ID="LblSegundoApellido" Text="Segundo Apellido "></asp:Label>
                                        <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="TxtSegundoApellido"
                                            FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="Ã¡Ã©Ã­Ã³ÃºÃÃ‰ÃÃ“ÃšÃ±Ã‘ .,@_():;ÁÉÍÓÚÑ" />
                                        <asp:CustomValidator runat="server" ID="CustomValidator4" SetFocusOnError="True"
                                            ClientValidationFunction="CheckLengthSegundoApellido" ErrorMessage="Registre su segundo apellido"
                                            ValidationGroup="btnGuardar" ControlToValidate="TxtSegundoApellido" Display="Dynamic"
                                            ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell" width="50%;">
                                        <asp:TextBox runat="server" ID="TxtPrimerApellido" Enabled="False" MaxLength="50"
                                            Width="80%" TabIndex="9"></asp:TextBox>
                                    </td>
                                    <td class="Cell" width="50%;">
                                        <asp:TextBox runat="server" ID="TxtSegundoApellido" Enabled="False" MaxLength="50"
                                            Width="80%" TabIndex="10"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" align="left">
                            <tr class="rowA">
                                <td width="33%">Indicativo
                                    <asp:RequiredFieldValidator runat="server" ID="rvIndicativo" ControlToValidate="TxtIndicativo"
                                        ErrorMessage="Registre un indicativo v&aacute;lido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator runat="server" ID="rvIndicativoPerNat" ControlToValidate="TxtIndicativo"
                                        ErrorMessage="Registre un indicativo v&aacute;lido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator runat="server" ID="reIndicativo" ErrorMessage="Indicativo debe tener un valor diferente de cero"
                                        ControlToValidate="TxtIndicativo" ValidationGroup="btnGuardar" ForeColor="Red"
                                        Display="Dynamic" ValidationExpression="\d{1}"></asp:RegularExpressionValidator>
                                    <asp:RangeValidator ID="rgvIndicativo" runat="server" Display="Dynamic" ControlToValidate="TxtIndicativo"
                                        ErrorMessage="Indicativo debe tener un valor diferente de cero" Type="Integer"
                                        MinimumValue="1" MaximumValue="9" ForeColor="Red" ValidationGroup="btnGuardar"></asp:RangeValidator>
                                </td>
                                <td width="33%">Tel&eacute;fono
                                    <asp:Label ID="lblTelefonoOblig" runat="server" Text="*" Visible="false"></asp:Label>
                                    <asp:RequiredFieldValidator runat="server" ID="rvTelefono" ControlToValidate="TxtTelefono"
                                        ErrorMessage="Registre un n&uacute;mero de tel&eacute;fono v&aacute;lido" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator runat="server" ID="rvTelefonoPerNat" ControlToValidate="TxtTelefono"
                                        ErrorMessage="Registre un n&uacute;mero de tel&eacute;fono v&aacute;lido" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator runat="server" ID="reTelefono" ErrorMessage="La longitud del n&uacute;mero de tel&eacute;fono no es v&aacute;lida"
                                        ControlToValidate="TxtTelefono" ValidationGroup="btnGuardar" ForeColor="Red"
                                        Display="Dynamic" ValidationExpression="\d{7}"></asp:RegularExpressionValidator>
                                </td>
                                <td width="33%">Extensi&oacute;n
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td width="33%">
                                    <asp:TextBox runat="server" ID="TxtIndicativo" MaxLength="1" TabIndex="11" Width="40%"
                                        onblur="validatorTel();"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftIndicativo" runat="server" TargetControlID="TxtIndicativo"
                                        FilterType="Numbers" ValidChars="/123456789" InvalidChars="0" />
                                    <asp:HiddenField runat="server" ID="HdTelreProveedor"></asp:HiddenField>
                                </td>
                                <td width="33%">
                                    <asp:TextBox runat="server" ID="TxtTelefono" MaxLength="7" TabIndex="12" Width="90%"
                                        onblur="validatorTelefono()"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftTelefono" runat="server" TargetControlID="TxtTelefono"
                                        FilterType="Numbers" ValidChars="/1234567890" />
                                </td>
                                <td width="33%">
                                    <asp:TextBox runat="server" ID="TxtExtencion" MaxLength="10" TabIndex="13" Width="75%"
                                        onblur="validatorIndicativo()"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftExtencion" runat="server" TargetControlID="TxtExtencion"
                                        FilterType="Numbers" ValidChars="/1234567890" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">Celular
                        <asp:Label ID="lblCelularOblig" runat="server" Text="*" Visible="false"></asp:Label>
                        <asp:RequiredFieldValidator runat="server" ID="rvCelular" ControlToValidate="TxtCelular"
                            SetFocusOnError="true" ErrorMessage="Registre un n&uacute;mero de celular" Display="Dynamic"
                            ValidationGroup="btnGuardar" ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="revCelular" ErrorMessage="Registre un n&uacute;mero de celular"
                            ControlToValidate="TxtCelular" SetFocusOnError="True" ValidationGroup="btnGuardar"
                            ForeColor="Red" Display="Dynamic" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
                    </td>
                    <td width="50%">Sitio web
                        <asp:RegularExpressionValidator runat="server" ID="reSitioWeb" ErrorMessage="El sitio web no tiene una estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                            ControlToValidate="TxtSitioWeb" SetFocusOnError="True" ValidationGroup="btnGuardar"
                            ForeColor="Red" Display="Dynamic" ValidationExpression="^(|F)[a-zA-Z0-9\-\._]+(\.[a-zA-Z0-9\-\._]+){2,}(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:TextBox runat="server" ID="TxtCelular" MaxLength="10" Width="40%" TabIndex="14"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftCelular" runat="server" TargetControlID="TxtCelular"
                            FilterType="Numbers" ValidChars="/1234567890" />
                    </td>
                    <td width="50%">
                        <asp:TextBox runat="server" ID="TxtSitioWeb" MaxLength="50" TabIndex="15"></asp:TextBox>
                        <%-- <Ajax:FilteredTextBoxExtender ID="ftSitioWeb" runat="server" TargetControlID="TxtSitioWeb"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/.@_-:1234567890" />--%>
                        <Ajax:FilteredTextBoxExtender ID="ftSitioWeb" runat="server" TargetControlID="TxtSitioWeb"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/-._:" />
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">
                        <asp:Label ID="lblSector" runat="server" Text="Sector *"></asp:Label>
                        <asp:RequiredFieldValidator ID="rvSector" runat="server" ControlToValidate="ddlSector"
                            ErrorMessage="Debe seleccionar el sector" SetFocusOnError="True" Enabled="True"
                            InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td width="50%">
                        <asp:Label ID="lblRegTrib" runat="server" Text="R&eacute;gimen Tributario *"></asp:Label>
                        <asp:RequiredFieldValidator ID="rvRegimenTributario" runat="server" ControlToValidate="ddlRegimenTributario"
                            ErrorMessage="Debe seleccionar el r&eacute;gimen tributario" SetFocusOnError="True"
                            Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic"
                            ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:DropDownList ID="ddlSector" runat="server" Width="80%" AutoPostBack="True" OnSelectedIndexChanged="DdlSectorSelectedIndexChanged"
                            TabIndex="16">
                        </asp:DropDownList>
                    </td>
                    <td width="50%">
                        <asp:DropDownList ID="ddlRegimenTributario" runat="server" Width="80%" TabIndex="17" OnSelectedIndexChanged="ddlRegimenTributario_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td width="33%">Fecha constituci&oacute;n*
                        <asp:RequiredFieldValidator ID="rvFechaConstitucion" runat="server" ControlToValidate="CalFechaConstitucion$txtFecha"
                            ErrorMessage="Registre la Fecha constituci&oacute;n o de establecimiento de comercio"
                            SetFocusOnError="True" Enabled="True" ValidationGroup="btnGuardar" Display="Dynamic"
                            ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvFechaConstitucion" runat="server" Display="Dynamic" ControlToValidate="CalFechaConstitucion$txtFecha"
                            ValidationGroup="btnGuardar" ErrorMessage="La Fecha constituci&oacute;n o de establecimiento de comercio debe ser menor a la fecha actual"
                            Type="Date" Operator="LessThan" ForeColor="Red" ValueToCompare="">
                        </asp:CompareValidator>
                    </td>
                    <td style="width: 33%;">
                        <asp:Label ID="lblVigencia" runat="server" Text="Vigencia"></asp:Label>
                        <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblVigencia"
                            SetFocusOnError="true" ErrorMessage="Debe seleccionar si la vigencia es indefinida o vigencia definida"
                            Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 33%;">
                        <asp:Label ID="lbVigenciaHasta" runat="server" Visible="False" Text="Vigencia hasta"></asp:Label>
                        <asp:RequiredFieldValidator ID="rvFechaVigencia" runat="server" ControlToValidate="CalFechaVigencia$txtFecha"
                            ErrorMessage="Registre la fecha l&iacute;mite de la vigencia" SetFocusOnError="True"
                            Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvFechaVigencia" runat="server" Display="Dynamic" ControlToValidate="CalFechaVigencia$txtFecha"
                            ValidationGroup="btnGuardar" ErrorMessage="La fecha hasta de la vigencia debe ser mayor a la  Fecha constituci&oacute;n o de establecimiento de comercio"
                            Type="Date" Operator="GreaterThan" ForeColor="Red" ValueToCompare='<%# CalFechaConstitucion.Date.ToString("dd-MM-yyyy") %>'>
                        </asp:CompareValidator>
                        <%--   <asp:CustomValidator runat="server" ID="cusvFechaVigencia" SetFocusOnError="True"
                                    ClientValidationFunction="funValidaFechaVigencia" ErrorMessage="La fecha hasta de la vigencia debe ser mayor a la  Fecha constituci&oacute;n o de establecimiento de comercio"
                                    ValidationGroup="btnGuardar" ControlToValidate="CalFechaVigencia$txtFecha" Display="Dynamic"
                                    ForeColor="Red" Text=""></asp:CustomValidator>--%>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="33%">
                        <%--<UcFecha:fecha ID="CalFechaConstitucion" runat="server"  Enabled="True" Requerid="False"  TabIndex="18" />--%>
                        <uc3:fechaJS ID="CalFechaConstitucion" runat="server" Enabled="True" Requerid="False"
                            TabIndex="18" />
                    </td>
                    <td width="33%">
                        <asp:RadioButtonList runat="server" ID="rblVigencia" AutoPostBack="True" RepeatDirection="Vertical"
                            TabIndex="19" OnSelectedIndexChanged="RblVigenciaSelectedIndexChanged">
                        </asp:RadioButtonList>
                    </td>
                    <td width="33%">
                        <%--<UcFecha:fecha ID="CalFechaVigencia" runat="server" Enabled="True" Requerid="True"
                                    Visible="False" TabIndex="20" />--%>
                        <uc3:fechaJS ID="CalFechaVigencia" runat="server" Enabled="True" Requerid="True"
                            Visible="False" TabIndex="20" />
                    </td>
                </tr>
            </table>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td colspan="2" style="width: 100%">
                        <table width="100%">
                            <tr>
                                <td width="33%">
                                    <asp:Label ID="txtNombreComercial" runat="server" Text="Nombre comercial *"></asp:Label>
                                    <asp:RequiredFieldValidator runat="server" ID="RfNombreEstablecimiento" ControlToValidate="TxtNombreEstablecimiento"
                                        SetFocusOnError="true" ErrorMessage="Registre el nombre comercial" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                                <td width="20%"></td>
                                <td width="46%">
                                    <asp:Label ID="lblTipoEntidad" runat="server" Text="Tipo de entidad *"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rvTipoEntidad" runat="server" ControlToValidate="ddlTipoEntidad"
                                        ErrorMessage="Debe seleccionar  un Tipo de entidad" SetFocusOnError="True" Enabled="True"
                                        InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                                    </asp:RequiredFieldValidator>
                                    <asp:Label ID="lblNumInt" runat="server" Text="Cantidad de integrantes *" Visible="false"></asp:Label>
                                    <asp:CompareValidator runat="server" ID="compValidTxtNumInt" ValidationGroup="btnGuardar"
                                        Enabled="false" Display="Dynamic" ControlToValidate="txtNumInt" Type="Integer"
                                        Operator="GreaterThan" ValueToCompare="1" ErrorMessage="Registre el n&uacute;mero de integrantes, debe ser mayor a 1."
                                        ForeColor="Red"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator runat="server" ID="rfvTxtNumInt" ControlToValidate="txtNumInt"
                                        Enabled="false" ErrorMessage="Registre el n&uacute;mero de integrantes." ForeColor="Red"
                                        Display="Dynamic" ValidationGroup="btnGuardar" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2" style="width: 100%">
                        <table width="100%">
                            <tr>
                                <td width="33%">
                                    <asp:TextBox ID="TxtNombreEstablecimiento" runat="server" MaxLength="256" TextMode="MultiLine"
                                        TabIndex="21" Width="100%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftNombreEstablecimiento" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                        TargetControlID="TxtNombreEstablecimiento" ValidChars="áéíóúÁÉÍÓÚñÑ/ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂºÃƒÂÃƒâ€°ÃƒÂÃƒâ€œÃƒÅ¡ÃƒÂ±Ãƒâ€˜.,@_():; 1234567890" />
                                </td>
                                <td width="20%"></td>
                                <td width="46%">
                                    <asp:DropDownList ID="ddlTipoEntidad" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTipoEntidad_SelectedIndexChanged"
                                        TabIndex="22" Width="80%">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtNumInt" runat="server" MaxLength="2" Visible="false"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="FilteredTxtNumInt" runat="server" FilterType="Numbers"
                                        TargetControlID="txtNumInt" ValidChars="/1234567890" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td style="width: 50%">
                        <asp:Label ID="lbEstadoValidacion" runat="server" Visible="True" Text=" Estado Validaci&oacute;n Documental"></asp:Label>
                    </td>
                </tr>
                <tr class="rowB">
                    <td style="width: 50%">
                        <asp:DropDownList ID="ddlEstadoValidacion" runat="server" Width="80%" Enabled="False"
                            TabIndex="23">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowA">
                    <td style="width: 50%">Registrado Por
                    </td>
                    <td style="width: 50%">Fecha Registro
                    </td>
                </tr>
                <tr class="rowB">
                    <td style="width: 50%">
                        <asp:TextBox runat="server" ID="txtRegistradorPor" Enabled="false" Width="80%"></asp:TextBox>
                    </td>
                    <td style="width: 50%">
                        <asp:TextBox runat="server" ID="txtFechaRegistro" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PnActividadCiiu" runat="server">
            <h3 class="lbBloque">
                <asp:Label ID="LbActividadCiiu" runat="server" Text="Actividad CIIU - Bienes y Servicios UNSPSC"></asp:Label>
            </h3>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td width="50%">Actividad CIIU principal *
                        <asp:RequiredFieldValidator runat="server" ID="rvTxtActividadCiiuPrin" ControlToValidate="TxtActividadCiiuPrin"
                            SetFocusOnError="true" ErrorMessage="Debe seleccionar una actividad CIIU principal"
                            Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="CustomValidatorFechaActividadPri" runat="server" ControlToValidate="CalFechaActividadPrincipal$txtFecha"
                            SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                            ClientValidationFunction="funValidaFechaActividadPrincipal">Registre la actividad principal CIIU
                        </asp:CustomValidator>
                    </td>
                    <td width="50%">Actividad CIIU secundaria
                        <asp:CustomValidator ID="rvTxtActividadCiiuSecund" runat="server" ControlToValidate="TxtActividadCiiuSegu"
                            ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic" ClientValidationFunction="validaActividadesDiferentes">Si cuenta con una actividad CIIU secundaria, registre una diferente a la principal
                        </asp:CustomValidator>
                        <asp:CustomValidator ID="CustomValidatorFechaActividadSec" runat="server" ControlToValidate="CalFechaActividadSegundaria$txtFecha"
                            SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                            ClientValidationFunction="funValidaFechaActividadSecundaria">Registre la actividad secundaria CIIU
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td class="Cell" width="50%;">
                        <asp:TextBox runat="server" ID="TxtActividadCiiuPrin" Width="80%" Enabled="False"
                            TabIndex="25"></asp:TextBox>
                        <asp:ImageButton ID="imgActividadCiiuPrin" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                            OnClientClick="GetCiiuPrin(); return false;" Style="cursor: hand" ToolTip="Buscar" TabIndex="26" Visible="True" />

                        <%--<asp:LinkButton ID="btnLimpiarCiiuprin" runat="server" CssClass="vinculo" 
                                    OnClientClick="LimpiarCiiuPrin()">Limpiar</asp:LinkButton>--%>
                        <asp:LinkButton ID="btnLimpiarCiiuprin" runat="server" CssClass="vinculo" OnClick="btnLimpiarCiiuprin_Click">Limpiar</asp:LinkButton>
                        <asp:HiddenField runat="server" ID="hfIdActividadCiiuPrin"></asp:HiddenField>
                        <asp:HiddenField runat="server" ID="hfDesActividadCiiuPrin"></asp:HiddenField>
                    </td>
                    <td class="Cell" width="50%;">
                        <asp:TextBox runat="server" ID="TxtActividadCiiuSegu" Width="80%" Enabled="False"
                            TabIndex="27"></asp:TextBox>
                        <asp:Image ID="imgActividadCiiuSegu" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                            OnClick="GetCiiuSegun()" Style="cursor: hand" ToolTip="Buscar" TabIndex="28" Visible="True" />
                        <asp:LinkButton ID="btnLimpiarCiiuSegun" runat="server" CssClass="vinculo" OnClick="btnLimpiarCiiuSegun_Click">Limpiar</asp:LinkButton>
                        <%--<a href="javascript:LimpiarCiiuSegun();" class="vinculo">Limpiar</a>--%>
                        <asp:HiddenField runat="server" ID="HfActividadCiiuSegu"></asp:HiddenField>
                        <asp:HiddenField runat="server" ID="hfDesActividadCiiuSegu"></asp:HiddenField>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%;">Fecha inicio actividad econ&oacute;mica principal *
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CalFechaActividadPrincipal$txtFecha"
                            ErrorMessage="Registre la fecha de inicio de la actividad principal CIIU" SetFocusOnError="True"
                            Enabled="True" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvFechaActividadPrincipal" runat="server" Display="Dynamic"
                            ControlToValidate="CalFechaActividadPrincipal$txtFecha" ValidationGroup="btnGuardar"
                            ErrorMessage="La fecha de inicio de la actividad principal CIIU debe ser menor a la fecha actual"
                            Type="Date" Operator="LessThan" ForeColor="Red" ValueToCompare="">
                        </asp:CompareValidator>
                        <asp:Label ID="lbFechaActividadPrincipal" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </td>
                    <td width="50%;">Fecha inicio actividad econ&oacute;mica secundaria
                        <asp:RequiredFieldValidator ID="rfvCalFechaActividadSegundaria" runat="server" ControlToValidate="CalFechaActividadSegundaria$txtFecha"
                            ErrorMessage="Registre la fecha de inicio de la actividad secundaria CIIU" SetFocusOnError="True"
                            Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvFechaActividadSegundaria" runat="server" Display="Dynamic"
                            ControlToValidate="CalFechaActividadSegundaria$txtFecha" ValidationGroup="btnGuardar"
                            ErrorMessage="La fecha de inicio de la actividad secundaria CIIU debe ser menor a la fecha actual"
                            Type="Date" Operator="LessThan" ForeColor="Red" ValueToCompare="">
                        </asp:CompareValidator>
                        <asp:Label ID="lbCFechaActividadSegundaria" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%;">
                        <%--<UcFecha:fecha ID="CalFechaActividadPrincipal" runat="server" Width="80%" Enabled="True"
                                    Requerid="False" TabIndex="29" />--%>
                        <uc3:fechaJS ID="CalFechaActividadPrincipal" runat="server" Width="80%" Enabled="True"
                            Requerid="False" TabIndex="29" />
                    </td>
                    <td width="50%;">
                        <%--<UcFecha:fecha ID="CalFechaActividadSegundaria" runat="server" Width="80%" Enabled="True"
                                    Requerid="False" TabIndex="30" />--%>
                        <uc3:fechaJS ID="CalFechaActividadSegundaria" runat="server" Width="80%" Enabled="True"
                            Requerid="False" TabIndex="30" />
                    </td>
                </tr>
                <tr>
                    <td>Clasifique el producto o servicio según código UNSPSC *
                        <asp:Image ID="imgIdTipoCodUNSPSC" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                            OnClick="GetCodUNSPSC()" Style="cursor: hand" ToolTip="Buscar" />
                        <a href="javascript:LimpiarUNSPSC();" id="linkLimpiar" style="visibility: hidden">Limpiar</a>
                    </td>
                    <td class="Cell">
                        <asp:LinkButton ID="lnkQueRegistrar" Style="float: left" runat="server" OnClientClick="return false;">&iquest;Qu&eacute; debo registrar?</asp:LinkButton>
                        <asp:Label runat="server" Text="" ID="lbQueRegistrar" CssClass="popup"></asp:Label>
                        <Ajax:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="lnkQueRegistrar"
                            BalloonPopupControlID="lbQueRegistrar" Position="BottomLeft" BalloonStyle="Rectangle"
                            BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                            UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField runat="server" ID="hfIdTipoCodUNSPSC"></asp:HiddenField>
                        <asp:GridView runat="server" ID="gvCodigoUNSPSC" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" DataKeyNames="idTipoCodUNSPSC" CellPadding="0" Height="16px"
                            OnPageIndexChanging="gvCodigoUNSPSC_PageIndexChanging" OnSelectedIndexChanged="gvCodigoUNSPSC_SelectedIndexChanged"
                            OnRowCommand="gvCodigoUNSPSC_RowCommand" OnRowDeleting="gvCodigoUNSPSC_RowDeleting">
                            <Columns>
                                <asp:BoundField HeaderText="Codigo" DataField="Codigo" SortExpression="Codigo" />
                                <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" SortExpression="Descripcion" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblDocumentos" runat="server" CommandArgument='<%#Eval("idTipoCodUNSPSC")%>' ToolTip="Eliminar"
                                            CommandName="Delete" Visible='<%# !(bool)Eval("idTipoCodUNSPSC").Equals(string.Empty) %>' OnClientClick="return confirm('Está seguro que desea eliminar esta clasificación?');">
                                            <img src="../../../Image/btn/delete.gif" alt="Eliminar" height="16px" width="16px" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PnComplementoPrivada" runat="server" Visible="False">
            <h3 class="lbBloque">
                <asp:Label ID="Label1" runat="server" Text="Complemento de datos b&aacute;sicos entidades privadas"></asp:Label>
            </h3>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td width="50%">Tipo de actividad *
                        <asp:RequiredFieldValidator ID="rvddlTipoActividad" runat="server" ControlToValidate="ddlTipoActividad"
                            ErrorMessage="Debe seleccionar  un Tipo de actividad" SetFocusOnError="True"
                            Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic"
                            ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td width="50%">
                        <asp:Label ID="lblClaseEntidad" runat="server" Text="Clase de Entidad o Establecimiento * "></asp:Label>
                        <asp:RequiredFieldValidator ID="rvddlClaseEntidad" runat="server" ControlToValidate="ddlClaseEntidad"
                            ErrorMessage="Debe seleccionar una clase de entidad" SetFocusOnError="True" Enabled="True"
                            InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:DropDownList ID="ddlTipoActividad" runat="server" AutoPostBack="True" Width="80%"
                            OnSelectedIndexChanged="DdlTipoActividadSelectedIndexChanged" TabIndex="31">
                        </asp:DropDownList>
                    </td>
                    <td width="50%">
                        <asp:DropDownList ID="ddlClaseEntidad" runat="server" Width="80%" TabIndex="32">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">Exenci&oacute;n Matr&iacute;cula Mercantil *
                        <asp:RequiredFieldValidator ID="rvddlExtecionMatruculaMercantil" runat="server" ControlToValidate="ddlExtecionMatruculaMercantil"
                            ErrorMessage="Debe registrar si es o no Exento de Matr&iacute;cula Mercantil"
                            SetFocusOnError="True" Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar"
                            Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:DropDownList ID="ddlExtecionMatruculaMercantil" runat="server" AutoPostBack="True"
                            Width="80%" OnSelectedIndexChanged="DdlExtecionMatruculaMercantilSelectedIndexChanged"
                            TabIndex="33">
                            <asp:ListItem Selected="True" Value="-1">Seleccione..</asp:ListItem>
                            <asp:ListItem Value="1">SI</asp:ListItem>
                            <asp:ListItem Value="0">NO</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">
                        <asp:Label ID="LBNumeroMatriculaMercantil" runat="server" Text="N&uacute;mero de Matr&iacute;cula Mercantil *"></asp:Label>
                        <asp:RequiredFieldValidator runat="server" ID="rvTxtNumeroMatriculaMercantil" ControlToValidate="TxtNumeroMatriculaMercantil"
                            SetFocusOnError="true" ErrorMessage="Registre n&uacute;mero de Matr&iacute;cula mercantil"
                            Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                    </td>
                    <td width="50%">
                        <asp:Label ID="LbFechaMatriculaMercantil" runat="server" Text="Fecha de Matricula Mercantil *"></asp:Label>
                        <asp:CompareValidator ID="cvFechaMatriculaMercantil" runat="server" Display="Dynamic"
                            ControlToValidate="CalFechaMatriculaMercantil$txtFecha" ValidationGroup="btnGuardar"
                            ErrorMessage="La fecha de Matr&iacute;cula Mercantil debe ser menor a la fecha actual"
                            Type="Date" Operator="LessThan" ForeColor="Red" ValueToCompare="">
                        </asp:CompareValidator>
                        <asp:Label ID="lbFechaMatriculaMercan" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:TextBox runat="server" ID="TxtNumeroMatriculaMercantil" MaxLength="20" TabIndex="34"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtCelular"
                            FilterType="Numbers" ValidChars="/ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂºÃƒÂÃƒâ€°ÃƒÂÃƒâ€œÃƒÅ¡ÃƒÂ±Ãƒâ€˜ .,@_():;1234567890" />
                    </td>
                    <td width="50%">
                        <uc3:fechaJS ID="CalFechaMatriculaMercantil" runat="server" Width="80%" Enabled="True"
                            Requerid="True" TabIndex="35" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PnComplementoPublicas" runat="server" Visible="False">
            <h3 class="lbBloque">
                <asp:Label ID="Label2" runat="server" Text="Complemento de Datos b&aacute;sicos"></asp:Label>
            </h3>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td width="50%">Rama o estructura *
                        <asp:RequiredFieldValidator ID="rvddlRamaoestructura" runat="server" ControlToValidate="ddlRamaoestructura"
                            ErrorMessage="Clasifique la entidad en una rama o estructura" SetFocusOnError="True"
                            Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic"
                            ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td width="50%">Nivel de gobierno *
                        <asp:RequiredFieldValidator ID="rvddlNivelgobierno" runat="server" ControlToValidate="ddlNivelgobierno"
                            ErrorMessage="Clasifiquela entidad en un nivel de gobierno" SetFocusOnError="True"
                            Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic"
                            ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:DropDownList ID="ddlRamaoestructura" runat="server" Width="300px" AutoPostBack="True"
                            TabIndex="36" OnSelectedIndexChanged="ddlRamaoestructura_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td width="50%">
                        <asp:DropDownList ID="ddlNivelgobierno" runat="server" Width="300px" AutoPostBack="true"
                            TabIndex="37" OnSelectedIndexChanged="ddlNivelgobierno_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowA">
                    <td width="50%">Nivel organizacional *
                        <asp:RequiredFieldValidator ID="rvddlNivelOrganizacional" runat="server" ControlToValidate="ddlNivelOrganizacional"
                            ErrorMessage="Seleccione un nivel organizacional" SetFocusOnError="True" Enabled="True"
                            InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td width="50%">Tipo de entidad *
                        <asp:RequiredFieldValidator ID="rvddlTipoEntidadPublica" runat="server" ControlToValidate="ddlTipoEntidadPublica"
                            ErrorMessage="Seleccione un Tipo de entidad" SetFocusOnError="True" Enabled="True"
                            InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:DropDownList ID="ddlNivelOrganizacional" runat="server" Width="80%" AutoPostBack="true"
                            TabIndex="38" OnSelectedIndexChanged="ddlNivelOrganizacional_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td width="50%">
                        <asp:DropDownList ID="ddlTipoEntidadPublica" runat="server" Width="80%" TabIndex="39">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlDomicilioLegal" runat="server">
            <h3 class="lbBloque">
                <asp:Label ID="Label3" runat="server" Text="Domicilio legal en Colombia Donde est&eacute constituida"></asp:Label>
            </h3>
            <table width="80%" align="center">
                <tr class="rowA">
                    <td width="50%">Departamento *
                        <asp:RequiredFieldValidator ID="rvDepartamentoConstituida" runat="server" ControlToValidate="ddlDepartamentoConstituida"
                            ErrorMessage="Seleccione departamento de domicilio" SetFocusOnError="True" Enabled="True"
                            InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td width="50%">Municipio *
                        <asp:RequiredFieldValidator ID="rvMunicipioConstituida" runat="server" ControlToValidate="ddlMunicipioConstituida"
                            ErrorMessage="Seleccione municipio de domicilio" SetFocusOnError="True" Enabled="True"
                            InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowB">
                    <td width="50%">
                        <asp:DropDownList ID="ddlDepartamentoConstituida" runat="server" Width="80%" AutoPostBack="True"
                            OnSelectedIndexChanged="DdlDepartamentoConstituidaSelectedIndexChanged" TabIndex="40">
                        </asp:DropDownList>
                    </td>
                    <td width="50%">
                        <asp:DropDownList ID="ddlMunicipioConstituida" runat="server" Width="80%" TabIndex="41">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <h3 class="lbBloque">
            <asp:Label ID="Label4" runat="server" Text="Datos de Ubicaci&oacuten/Direcci&oacuten comercial o de gerencia"></asp:Label>
        </h3>
        <table width="80%" align="center">
            <tr class="rowA">
                <td width="50%">Departamento *
                    <asp:RequiredFieldValidator ID="rvDepartamentoDireccionComercial" runat="server"
                        ControlToValidate="ddlDepartamentoDireccionComercial" ErrorMessage="Seleccione departamento de ubicaci&oacute;n, de Direcci&oacute;n comercial o de gerencia"
                        SetFocusOnError="True" Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar"
                        Display="Dynamic" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                </td>
                <td width="50%">Municipio *
                    <asp:RequiredFieldValidator ID="rvMunicipioDireccionComercial" runat="server" ControlToValidate="ddlMunicipioDireccionComercial"
                        ErrorMessage="Seleccione municipio de ubicaci&oacute;n, de Direcci&oacute;n comercial o de gerencia"
                        SetFocusOnError="True" Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar"
                        Display="Dynamic" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowB">
                <td width="50%">
                    <asp:DropDownList ID="ddlDepartamentoDireccionComercial" runat="server" Width="80%"
                        AutoPostBack="True" OnSelectedIndexChanged="DdlDepartamentoDireccionComercialSelectedIndexChanged"
                        TabIndex="42">
                    </asp:DropDownList>
                </td>
                <td width="50%">
                    <asp:DropDownList ID="ddlMunicipioDireccionComercial" runat="server" Width="80%"
                        TabIndex="43">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table width="80%" align="center">
            <tr class="rowA">
                <td style="width: 90%">
                    <asp:Label ID="lbDireccionResidencia" runat="server" ForeColor="Red" Text=""></asp:Label>
                    <uc2:IcbfDireccion ID="txtDireccionResidencia" runat="server" Requerido="True" />
                </td>
            </tr>
        </table>
        <table width="80%" align="center">
            <tr class="rowA">
                <td width="100%">
                    <asp:Panel runat="server" ID="PnlReprLegal">
                        <h3 class="lbBloque">
                            <asp:Label ID="Label5" runat="server" Text="Representante Legal" Width="100%"></asp:Label>
                        </h3>
                        <table style="width: 100%">
                            <tr class="rowB">
                                <td style="width: 50%">Tipo de identificaci&oacuten *
                                    <asp:RequiredFieldValidator ID="rvddlTipoDocumentoRepr" runat="server" ControlToValidate="ddlTipoDocumentoRepr"
                                        ErrorMessage="Seleccione un tipo de identificaci&oacute;n" SetFocusOnError="True"
                                        Enabled="True" InitialValue="-1" ValidationGroup="btnGuardar" Display="Dynamic"
                                        ForeColor="Red">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 50%">N&uacute;mero de identificaci&oacute;n *
                                    <asp:RequiredFieldValidator runat="server" ID="rvtxtIdentificacionRepr" ControlToValidate="txtIdentificacionRepr"
                                        SetFocusOnError="true" ErrorMessage="Registre el n&uacute;mero de identificaci&oacute;n"
                                        Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CustomValidatortxtIdentificacionRepr" runat="server" ControlToValidate="txtIdentificacionRepr"
                                        SetFocusOnError="true" ValidationGroup="btnGuardar" ForeColor="Red" Display="Dynamic"
                                        ClientValidationFunction="funValidaNumIdentificacionRepr">El n&uacute;mero de identificaci&oacute;n no es v&aacute;lido</asp:CustomValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td style="width: 50%">
                                    <asp:DropDownList ID="ddlTipoDocumentoRepr" runat="server" Width="80%" TabIndex="50"
                                        OnSelectedIndexChanged="ddlTipoDocumentoRepr_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:HiddenField runat="server" ID="hfRepreLegal"></asp:HiddenField>
                                </td>
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtIdentificacionRepr" TabIndex="51"
                                        AutoPostBack="true" OnTextChanged="txtIdentificacionRepr_TextChanged" Width="60%" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                                    <%--<Ajax:FilteredTextBoxExtender ID="ftIdentificacionRepr" runat="server" TargetControlID="txtIdentificacionRepr"
                                        FilterType="Numbers" ValidChars="/ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂºÃƒÂÃƒâ€°ÃƒÂÃƒâ€œÃƒÅ¡ÃƒÂ±Ãƒâ€˜.,@_():;1234567890" />--%>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td width="50%">Primer nombre *
                                    <asp:RequiredFieldValidator runat="server" ID="rvTxtPrimerNombreRepr" ControlToValidate="TxtPrimerNombreRepr"
                                        SetFocusOnError="true" ErrorMessage="Registre el primer nombre" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator runat="server" ID="CustomValidator5" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLengthPrimerNombreRep" ErrorMessage="Registre su primer nombre"
                                        ValidationGroup="btnGuardar" ControlToValidate="TxtPrimerNombreRepr" Display="Dynamic"
                                        ForeColor="Red" />
                                </td>
                                <td width="50%">Segundo nombre
                                    <asp:CustomValidator runat="server" ID="cvTxtSegundoNombreRep" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLengthSegundoNombreRep" ErrorMessage="Registre el segundo nombre"
                                        ValidationGroup="btnGuardar" ControlToValidate="TxtSegundoNombreRepr" Display="Dynamic"
                                        ForeColor="Red" />
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td class="Cell" width="50%;">
                                    <asp:TextBox runat="server" ID="TxtPrimerNombreRepr" MaxLength="50" Width="80%" TabIndex="52"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftPrimerNombreRepr" runat="server" TargetControlID="TxtPrimerNombreRepr"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars=" ÁÉÍÓÚÑáéíóúñ" />
                                </td>
                                <td class="Cell" width="50%;">
                                    <asp:TextBox runat="server" ID="TxtSegundoNombreRepr" MaxLength="50" TabIndex="53"
                                        Width="80%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftSegundoNombreRepr" runat="server" TargetControlID="TxtSegundoNombreRepr"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars=" ÁÉÍÓÚÑáéíóúñ" />
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td width="50%">Primer apellido*
                                    <asp:RequiredFieldValidator runat="server" ID="rvTxtPrimerApellidoRepr" ControlToValidate="TxtPrimerApellidoRepr"
                                        SetFocusOnError="true" ErrorMessage="Registre el primer apellido" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator runat="server" ID="cvTxtPrimerApellidoRep" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLengthPrimerApellidoRep" ErrorMessage="Registre el primer apellido"
                                        ValidationGroup="btnGuardar" ControlToValidate="TxtPrimerApellidoRepr" Display="Dynamic"
                                        ForeColor="Red" />
                                </td>
                                <td width="50%">Segundo apellido
                                    <asp:CustomValidator runat="server" ID="cvTxtSegundoApellidoRep" SetFocusOnError="True"
                                        ClientValidationFunction="CheckLengthSegundoApellidoRep" ErrorMessage="Registre el segundo apellido"
                                        ValidationGroup="btnGuardar" ControlToValidate="TxtSegundoApellidoRepr" Display="Dynamic"
                                        ForeColor="Red" />
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td class="Cell" width="50%;">
                                    <asp:TextBox runat="server" ID="TxtPrimerApellidoRepr" MaxLength="50" TabIndex="54"
                                        Width="80%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftPrimerApellidoRepr" runat="server" TargetControlID="TxtPrimerApellidoRepr"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars=" ÁÉÍÓÚÑáéíóúñ" />
                                </td>
                                <td class="Cell" width="50%;">
                                    <asp:TextBox runat="server" ID="TxtSegundoApellidoRepr" MaxLength="50" TabIndex="55"
                                        Width="80%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftSegundoApellidoRepr" runat="server" TargetControlID="TxtSegundoApellidoRepr"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars=" ÁÉÍÓÚÑáéíóúñ" />
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>Sexo *
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="Seleccione un tipo de identificaciÃ³n"
                                        ControlToValidate="ddlSexoRepLegal" SetFocusOnError="true" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Display="Dynamic" InitialValue="-1">Seleccione un valor de la lista</asp:RequiredFieldValidator>
                                </td>
                                <td>Fecha de nacimiento *
                                    <asp:CompareValidator ID="cvFechaNacRepLegal" runat="server" Display="Dynamic" ControlToValidate="cuFechaNacRepLegal$txtFecha"
                                        ValidationGroup="btnGuardar" ErrorMessage="La edad m&iacute;nima del Representante Legal es de 18 años, int&eacute;ntelo de nuevo"
                                        Type="Date" Operator="LessThanEqual" ForeColor="Red">
                                    </asp:CompareValidator>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td class="Cell" width="50%;">
                                    <asp:DropDownList runat="server" ID="ddlSexoRepLegal" Width="80%">
                                    </asp:DropDownList>
                                </td>
                                <td class="Cell" width="50%;">
                                    <uc1:fecha ID="cuFechaNacRepLegal" runat="server" cssclass="calendario" />
                                </td>
                            </tr>
                            <tr>
                                <td width="50%">
                                    <table>
                                        <tr class="rowA">
                                            <td width="25%">Celular *
                                                <asp:RequiredFieldValidator runat="server" ID="rvTxtCelularRepr" ControlToValidate="TxtCelularRepr"
                                                    SetFocusOnError="true" ErrorMessage="Registre el n&uacute;mero de celular" Display="Dynamic"
                                                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator runat="server" ID="revCelularRepr" ErrorMessage="Registre un n&uacute;mero de celular"
                                                    ControlToValidate="TxtCelularRepr" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                                    ForeColor="Red" Display="Dynamic" ValidationExpression="\d{10}"></asp:RegularExpressionValidator>
                                            </td>
                                            <td width="25%">Indicativo
                                                <asp:CustomValidator runat="server" ID="cvIndicativoRep" SetFocusOnError="True" ClientValidationFunction="ValidaIndicativo1"
                                                    ErrorMessage="Registre un indicativo v&aacute;lido" ValidationGroup="btnGuardar"
                                                    ControlToValidate="TxtIndicativoRepr" Display="Dynamic" ForeColor="Red" Text=""></asp:CustomValidator>
                                                <asp:RequiredFieldValidator runat="server" ID="rfvIndicativoRepr" ControlToValidate="TxtIndicativoRepr"
                                                    SetFocusOnError="true" ErrorMessage="Registre un indicativo v&aacute;lido" Display="Dynamic"
                                                    ValidationGroup="btnGuardar" Enabled="False" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="rgvIndicativoRepr" runat="server" Display="Dynamic" ControlToValidate="TxtIndicativoRepr"
                                                    ErrorMessage="Indicativo debe tener un valor diferente de cero" Type="Integer"
                                                    MinimumValue="1" MaximumValue="9" ForeColor="Red" ValidationGroup="btnGuardar"></asp:RangeValidator>
                                                <asp:Label ID="lbIndicativoRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                            <td width="25%">Tel&eacute;fono
                                                <asp:Label ID="lblObligTelRep" runat="server" Text=" *"></asp:Label>
                                                <asp:CustomValidator runat="server" ID="cvIndicativoRepr" ClientValidationFunction="ValidaIndicativo"
                                                    ErrorMessage="Registre un indicativo v&aacute;lido" ValidationGroup="btnGuardar"
                                                    Enabled="True" ControlToValidate="TxtTelefonoRepr" Display="Dynamic" ForeColor="Red"
                                                    Text=""></asp:CustomValidator>
                                                <asp:RequiredFieldValidator runat="server" ID="rvTxtTelefonoRepr" ControlToValidate="TxtTelefonoRepr"
                                                    ErrorMessage="Registre un n&uacute;mero de tel&eacute;fono v&aacute;lido" Display="Dynamic"
                                                    ValidationGroup="btnGuardar" Enabled="False" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator runat="server" ID="reTelefonoRepr" ErrorMessage="La longitud del n&uacute;mero de tel&eacute;fono no es valida"
                                                    ControlToValidate="TxtTelefonoRepr" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                                    ForeColor="Red" Display="Dynamic" ValidationExpression="\d{7}"></asp:RegularExpressionValidator>
                                            </td>
                                            <td width="25%">Extensi&oacute;n
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td width="25%">
                                                <asp:TextBox runat="server" ID="TxtCelularRepr" MaxLength="10" TabIndex="56" Width="80%"></asp:TextBox>
                                                <Ajax:FilteredTextBoxExtender ID="ftCelularRepr" runat="server" TargetControlID="TxtCelularRepr"
                                                    FilterType="Numbers" ValidChars="/1234567890" />
                                                <asp:HiddenField runat="server" ID="hfTelTelRepreLega"></asp:HiddenField>
                                            </td>
                                            <td width="25%">
                                                <asp:TextBox runat="server" ID="TxtIndicativoRepr" MaxLength="1" TabIndex="57" Width="45%"
                                                    onblur="validatorTelRep();"></asp:TextBox>
                                                <Ajax:FilteredTextBoxExtender ID="ftIndicativoRepr" runat="server" TargetControlID="TxtIndicativoRepr"
                                                    FilterType="Numbers" ValidChars="/1234567890" />
                                            </td>
                                            <td width="25%">
                                                <asp:TextBox runat="server" ID="TxtTelefonoRepr" MaxLength="7" TabIndex="58" Width="95%"
                                                    onblur="validatorTelefonoRep()"></asp:TextBox>
                                                <Ajax:FilteredTextBoxExtender ID="ftTelefonoRepr" runat="server" TargetControlID="TxtTelefonoRepr"
                                                    FilterType="Numbers" ValidChars="/1234567890" />
                                            </td>
                                            <td width="25%">
                                                <asp:TextBox runat="server" ID="TxtExtencionRepr" MaxLength="10" TabIndex="59" Width="90%"
                                                    onblur="validatorIndicativoRep()"></asp:TextBox>
                                                <Ajax:FilteredTextBoxExtender ID="ftExtencionRepr" runat="server" TargetControlID="TxtExtencionRepr"
                                                    FilterType="Numbers" ValidChars="/1234567890" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>Correo electr&oacute;nico *
                                    <asp:RequiredFieldValidator runat="server" ID="rvTxtCorreoElectronicoRepr" ControlToValidate="TxtCorreoElectronicoRepr"
                                        SetFocusOnError="true" ErrorMessage="Registre un correo v&aacute;lido" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ErrorMessage="Correo electr&oacute;nico no tiene la estructura v&aacute;lida, int&eacute;ntelo de nuevo"
                                        ControlToValidate="TxtCorreoElectronicoRepr" SetFocusOnError="True" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    <asp:TextBox runat="server" ID="TxtCorreoElectronicoRepr" MaxLength="50" TabIndex="60"
                                        Width="80%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteCorreoElectronicoRepr" runat="server" TargetControlID="TxtCorreoElectronicoRepr"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-@._" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <h3 class="lbBloque">
            <asp:Label ID="Label6" runat="server" Text="Documento digital / certificados a adjuntar"></asp:Label>
        </h3>
        <table width="80%" align="center">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblDocumentosPersonal" Text="Documentos informaci&oacute;n de personal"
                        runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:Label ID="lblNota" Text="Nota: para eliminar el documento debe remplazarlo por el soporte legal"
                        runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocDatosBasicoProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto,IdTipoDocumento,MaxPermitidoKB,ExtensionesPermitidas"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocDatosBasicoProv_RowCommand"
                        OnRowEditing="gvDocDatosBasicoProv_RowEditing" OnRowCancelingEdit="gvDocDatosBasicoProv_RowCancelingEdit"
                        OnRowUpdating="gvDocDatosBasicoProv_RowUpdating" OnPageIndexChanging="gvDocDatosBasicoProv_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdDocAdjunto" runat="server" Text='<%# Bind("IdDocAdjunto") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre del documento" SortExpression="NombreDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link del documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'></asp:Label>--%>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--<asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'
                                        Visible="false"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'
                                        Visible="false"></asp:TextBox>
                                    <asp:FileUpload ID="fuDocumento" runat="server"></asp:FileUpload>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Observaciones" SortExpression="Observaciones">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                        TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtObservaciones" runat="server" Width="150px" Height="80px" CssClass="TextBoxGrande"
                                        TextMode="MultiLine" onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);"
                                        Text='<%# Bind("Observaciones") %>' MaxLength="128"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEditar" ImageUrl="~/Image/btn/attach.jpg"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Edit" ToolTip="Editar"></asp:ImageButton>
                                    &nbsp;
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar"></asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <table width="80%" align="center">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label7" Text="Documento(s) de registro de tercero" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView runat="server" ID="gvDocAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto,IdDocumento,MaxPermitidoKB,ExtensionesPermitidas"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocAdjuntos_RowCommand" OnPageIndexChanging="gvDocAdjuntos_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre del documento" SortExpression="NombreDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreTipoDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (Int16)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link del documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'></asp:Label>--%>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--<asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'
                                        Visible="false"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'
                                        Visible="false"></asp:TextBox>
                                    <asp:FileUpload ID="fuDocumento" runat="server"></asp:FileUpload>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Extensiones permitidas">
                                <ItemTemplate>
                                    <asp:Label ID="lblExtensionesPermitidas" runat="server" Text='<%# Bind("ExtensionesPermitidas") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Máximo permitido KB">
                                <ItemTemplate>
                                    <asp:Label ID="lblMaxPermitidoKB" runat="server" Text='<%# string.Format("{0}", (Int32)Eval("MaxPermitidoKB") )%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <%--display: none;--%>
                    <asp:Panel ID="pnlAuditoriaDatosBasicos" runat="server" Width="90%" Style="height: 95%; overflow: auto;">
                        <table width="90%" style="background-color: White">
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 95%">
                                    <label style="font-weight: bold">
                                        Auditor&iacute;a de Acciones en Datos B&aacute;sicos</label>
                                </td>
                                <td style="width: 5%">
                                    <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Image/btn/Cancel.png" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 100%">
                                    <asp:GridView ID="gridAuditoriaDatBAsicos" runat="server" AutoGenerateColumns="False" HeaderStyle-Font-Bold="true"
                                        AllowPaging="True" GridLines="None" Width="100%" CellPadding="0" Height="16px" EmptyDataText="No se han realizado acciones en Datos Básicos."
                                        PageSize="10" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gridAuditoriaDatBAsicos_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField HeaderText="Fecha" HeaderStyle-HorizontalAlign="Left" DataField="Fecha" />
                                            <asp:BoundField HeaderText="Usuario" HeaderStyle-HorizontalAlign="Left" DataField="Usuario" />
                                            <asp:BoundField HeaderText="IdSistema" HeaderStyle-HorizontalAlign="Left" DataField="IdSistema" />
                                            <asp:BoundField HeaderText="Sistema" HeaderStyle-HorizontalAlign="Left" DataField="Sistema" />
                                            <asp:BoundField HeaderText="Acci&oacute;n" HeaderStyle-HorizontalAlign="Left" DataField="Accion" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <asp:Panel runat="server" ID="pObservacionesCorreccion" Visible="True">
            <table width="80%" align="center">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblObservacionesCorreccion" runat="server" Text="Observaciones"></asp:Label>
                        <asp:Label ID="lblObservacionesCorreccionAS" runat="server" Text="*"></asp:Label>
                        <asp:RequiredFieldValidator runat="server" ID="rfvlblObservacionesCorreccion" ErrorMessage="Campo Obligatorio"
                            ControlToValidate="txtObservacionesCorreccion" SetFocusOnError="true" ValidationGroup="btnGuardar"
                            ForeColor="Red" Display="Dynamic">Campo Obligatorio</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox runat="server" ID="txtObservacionesCorreccion" Width="100%" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="fteObservacionesCorreccion" runat="server" TargetControlID="txtObservacionesCorreccion"
                            FilterType="Custom,LowercaseLetters,UppercaseLetters, Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <br />
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
    <Ajax:ModalPopupExtender ID="mdpDatosAuditoria" runat="server" PopupControlID="pnlAuditoriaDatosBasicos"
        TargetControlID="btnTarget" Drag="true" BackgroundCssClass="ModalPopupBG" CancelControlID="btnCancel">
    </Ajax:ModalPopupExtender>
    <asp:Button ID="btnTarget" runat="server" Text="target" Style="visibility: hidden" />
    <script type="text/javascript" language="javascript">
        function EjecutarJS() {

        }
    </script>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</div>
</div>
    </div>
    </div>
    </div>
</asp:Content>
