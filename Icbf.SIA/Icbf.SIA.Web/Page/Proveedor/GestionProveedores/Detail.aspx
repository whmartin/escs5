﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Proveedor_GestionProveedores_Add" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

    <link href="../../../Styles/TabContainer.css" rel="stylesheet" type="text/css" />
    <div>
        <Ajax:TabContainer ID="tcInfoProveedor" runat="server" Visible="true" 
            ActiveTabIndex="7" CssClass="ajax__tab_green-theme"
            Width="100%" OnActiveTabChanged="tcInfoProveedor_ActiveTabChanged" 
            AutoPostBack="True">
            <Ajax:TabPanel ID="tpDatosBasicos" runat="server" HeaderText="Datos Básicos"  >
                <HeaderTemplate>
                    Datos Básicos
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameDatosBasicos"  runat="server" name="frameDatosBasicos" overflow="scroll" Visible="True"
                        src="../GestionProveedores/DetailDatosBasicos.aspx" width="100%" height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel> 
            
            <Ajax:TabPanel ID="tpFormacion" runat="server" HeaderText="Formación Académica" Visible="False" >
                <HeaderTemplate>
                    Formación Académica
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameformacion" runat="server" name="frameformacion"
                        noresize="false" overflow="scroll" width="100%" src="../Formacion/List.aspx"
                        height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>            
            <Ajax:TabPanel ID="tpDatosFinancieros" runat="server" HeaderText="Datos Financieros" Visible="False">
                <HeaderTemplate>
                    Datos Financieros
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameInfoFinanciera" runat="server" name="frameInfoFinanciera"  
                        src="../InfoFinancieraEntidad/List.aspx" width="100%" height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpExperiencia" runat="server" HeaderText="Experiencia" Visible="False" >
                <HeaderTemplate>
                    Experiencia
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameProveedorExperiencia" runat="server" name="frameProveedorExperiencia"
                        noresize="false" overflow="scroll" width="100%" src="../InfoExperienciaEntidad/List.aspx"
                        height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpSucursal" runat="server" HeaderText="Sucursal" Visible="False">
                <HeaderTemplate>
                    Sucursal
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameSucursal" runat="server" name="frameSucursal" noresize="false" overflow="scroll"
                        src="../Sucursal/List.aspx" width="100%" autoposback="true" height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpContactos" runat="server" HeaderText="Contactos" Visible="False">
                <HeaderTemplate>
                    Contactos
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameProveedorContactos" runat="server" name="frameProveedorContactos"
                        noresize="false" overflow="scroll" width="100%" src="../ContactoEntidad/List.aspx"
                        height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
           <Ajax:TabPanel ID="tpNotificacionJudicial" runat="server" HeaderText=" Notificación Judicial" Visible="False">
                <HeaderTemplate>
                      Notificación Judicial
                </HeaderTemplate>
                <ContentTemplate>
                   <iframe id="frameNotificacionJudicial" runat="server" name="frameNotificacionJudicial"
                        noresize="false" overflow="scroll" width="100%" src="../NotificacionJudicial/List.aspx" 
                        height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpIntegrantes" runat="server" HeaderText="Integrantes" Visible="False" >
                <HeaderTemplate>
                    Integrantes
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameIntegrantes" runat="server" name="frameIntegrantes"
                        noresize="false" overflow="scroll" width="100%" src="../Integrantes/List.aspx"
                        height="700px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
        </Ajax:TabContainer>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPrincipalButton" Runat="Server">
</asp:Content>

