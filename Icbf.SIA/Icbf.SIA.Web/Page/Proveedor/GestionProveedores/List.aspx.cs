using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using EntidadProvOferente = Icbf.Proveedor.Entity.EntidadProvOferente;
using System.Text.RegularExpressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de gesti�n de proveedores
/// </summary>
public partial class Page_EntidadProvOferente_List : GeneralWeb
{
    masterPrincipal toolBar;
    bool _sorted = false;
    string PageName = "Proveedor/GestionProveedores";
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    OferenteService vOferenteService = new OferenteService();
    SIAService vRUBOService = new SIAService();
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                toolBar.LipiarMensajeError();
                toolBar.LimpiarOpcionesAdicionales();
                CargarDatosIniciales();
                if (vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
                {
                    pnlConsulta.Visible = false;


                }
                //Buscar("1");
                if (GetState(Page.Master, PageName))
                {
                    CargarTipoDocumentotipoPersona();
                    GetState(Page.Master, PageName);
                    Buscar();
                }
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void BtnBuscarTercerroClick(object sender, EventArgs e)
    {

        NavigateTo("~/Page/" + PageName + "/ListTerceros.aspx");
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        String vUsuarioCrea = null;
        vUsuarioCrea = GetSessionUser().NombreUsuario;
        Tercero ter = vOferenteService.ConsultarTerceroPorCorreo(vUsuarioCrea);
        if (ter.IdTercero != null && ter.IdTercero != 0)
        {
            int coubtProveedorAsociado = vProveedorService.ConsultarEntidadProvOferentes(null, null, null, null, null, vUsuarioCrea).Count;
            if (coubtProveedorAsociado == 0)
            {
                var terceroProveedor = vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario);
                if (terceroProveedor.Rol.Split(';').Contains("PROVEEDORES"))
                {

                    Tercero terceroProv = vProveedorService.ConsultarTerceroTipoNumeroIdentificacion(terceroProveedor.IdTipoDocumento, terceroProveedor.NumeroDocumento);

                    //SE han comentado las l�neas que preguntaban el Estado del Tercero... Anteriormente si no Ten�a un Estado Validado, no se 
                    //Permitia editar la informaci�n. Pero el COntrol de Cambios CO_016 lo ha solicitado.
                    //Se revisa si el estado del tercero asociado est� validado
                    //if (terceroProv.IdEstadoTercero == vProveedorService.ConsultarEstadoTercero("004").IdEstadoTercero)
                    //{

                    var vEntidadProvOferente = vOferenteService.ConsultarEntidadProvOferentes(terceroProv.IdTercero);
                    if (vEntidadProvOferente.Count == 0)
                    {
                        SetSessionParameter("Proveedor.IdTercero", terceroProv.IdTercero);
                        Response.Redirect("~/page/" + PageName + "/Add.aspx");
                    }
                    else
                    {
                        SetSessionParameter("EntidadProvOferente.IdEntidad", vEntidadProvOferente[0].IDENTIDAD);
                        Response.Redirect("~/page/" + PageName + "/DetailDatosBasicos.aspx");
                    }
                    //}
                    //else
                    //{

                    //    toolBar.MostrarMensajeGuardado("Aun no tiene un estado v&aacute;lido para registrarse como proveedor intente m&aacute;s tarde");
                    //}
                }
            }
            else if (coubtProveedorAsociado > 0)
            {
                toolBar.MostrarMensajeError("Este usuario ya tiene un Proveedor registrado, puede consultarlo.");
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Para registrarse como proveedor, antes debe realizar su registro como Tercero.");
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion, string entra)
    {
        try
        {
            toolBar.LipiarMensajeError();
            toolBar.LimpiarOpcionesAdicionales();
            String vTipoPersona = null;
            String vTipoidentificacion = null;
            String vIdentificacion = null;
            String vProveedor = null;
            String vEstado = null;
            String vUsuarioCrea = null;
            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vTipoPersona = Convert.ToString(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDocumento.SelectedValue != "-1" && ddlTipoDocumento.SelectedValue != "")
            {
                vTipoidentificacion = Convert.ToString(ddlTipoDocumento.SelectedValue);
            }
            if (txtIdentificacion.Text != "")
            {
                vIdentificacion = Convert.ToString(txtIdentificacion.Text.Trim());
            }
            if (TxtRazonSocial.Text != "")
            {
                vProveedor = Convert.ToString(TxtRazonSocial.Text.Trim());
            }
            if (ddlEstado.SelectedValue != "-1" && ddlEstado.SelectedValue != "")
            {
                vEstado = Convert.ToString(ddlEstado.SelectedValue);
            }
            if (entra == "1")
            {
                vUsuarioCrea = GetSessionUser().NombreUsuario;
            }
            else
            {
                if (vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
                {
                    vUsuarioCrea = GetSessionUser().NombreUsuario;

                }
                else
                {
                    vUsuarioCrea = "";
                }


            }
            var myGridResults = vProveedorService.ConsultarEntidadProvOferentes(vTipoPersona, vTipoidentificacion, vIdentificacion, vProveedor, vEstado, vUsuarioCrea);
            if (expresionOrdenamiento != null)
            {
                //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Proveedor.Entity.EntidadProvOferente), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecuci�n la expresi�n lambda
                    var sortExpression = Expression.Lambda<Func<Icbf.Proveedor.Entity.EntidadProvOferente, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvEntidadProvOferente, GridViewSortExpression, true, "2");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            gvEntidadProvOferente.PageSize = PageSize();
            gvEntidadProvOferente.EmptyDataText = EmptyDataText();
            if (vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES") || vRUBOService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("GESTOR PROVEEDOR"))
            {
                //toolBar.eventoBuscarTercero += new ToolBarDelegate(BtnBuscarTercerroClick);
                toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            }
            else
            {
                toolBar.eventoBuscarTercero += new ToolBarDelegate(BtnBuscarTercerroClick);
            }
            toolBar.EstablecerTitulos("Lista de Proveedores ", SolutionPage.List.ToString());

            //Se remueve la variable de sesion usada para mostrar el panel de edici�n de Observaciones
            RemoveSessionParameter("ValidarProveedor.NroRevision");
            RemoveSessionParameter("ValidarInfoFinancieraEntidad.NroRevision");
            RemoveSessionParameter("ValidarInfoExperienciaEntidad.NroRevision");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string strValue;
            int rowIndex = pRow.RowIndex;
            var dataKey = gvEntidadProvOferente.DataKeys[rowIndex];
            if (dataKey != null)
            {
                strValue = dataKey.Value.ToString();
                SetSessionParameter("EntidadProvOferente.IdEntidad", strValue);

                int vIdEntidad = Convert.ToInt32(strValue);
                vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
                SetSessionParameter("Proveedor.IdTercero", vEntidadProvOferente.IdTercero);
                if (vEntidadProvOferente.IdEstadoProveedor == 5) //SI estado del PROVEEDOR EN VALIDACI�N ....NO DEJA EDITAR NADA
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript", "<script>javascript: alert('Esta informaci\xf3n no puede ser editada en este momento, intente m\xe1s tarde');</script>");
                }
                else
                {
                    NavigateTo(SolutionPage.Detail);
                }
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void GvEntidadProvOferenteSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvEntidadProvOferente.SelectedRow);
    }
    protected void GvEntidadProvOferentePageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvEntidadProvOferente.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true, "2");
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            if (GetSessionParameter("EntidadProvOferente.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("EntidadProvOferente.Eliminado");
            RemoveSessionParameter("EntidadProvOferente.IdEntidad");
            // lista documento reprecentante legal
            ManejoControles.LlenarEstadoProveedor(ddlEstado, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarTipoDocumentotipoPersona();

    }
    private void ViewControlNatural(bool state)
    {
        //PnlNatural.Visible = state;
    }
    private void ViewControlJuridica(bool state)
    {
        LblRazonSocial.Visible = state;
        TxtRazonSocial.Visible = state;
    }

    /// <summary>
    /// Llena la lista desplegable correspondiende de acuerdo al tipo de personal
    /// </summary>
    protected void CargarTipoDocumentotipoPersona()
    {
        new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDocumento, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDocumento, "-1", true);
        }

    }

    #region ordenarGrilla
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }

    }
    private string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }

    }

    protected void GvEntidadProvOferentePage_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false, "2");
    }

    #endregion


    protected void ddlTipoDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDocumento.SelectedValue != "-1")
        {
            this.txtIdentificacion.Text = String.Empty;
            this.txtIdentificacion.Focus();
        }
    }
    private void CambiarValidadorDocumentoUsuario()
    {
        /*String varchar = @"[a-zA-Z0-9]"; //Pattern is Ok
        String integer = "[0-9]"; //Pattern is Ok*/


        if (this.ddlTipoDocumento.SelectedItem.Text == "CC" || this.ddlTipoDocumento.SelectedItem.Text == "NIT" || this.ddlTipoDocumento.SelectedItem.Text == "PA")
        {
            this.txtIdentificacion.MaxLength = 11;
            //Regex regex = new Regex(integer);
            

        }
        else if (this.ddlTipoDocumento.SelectedItem.Text == "CE")
        {
            this.txtIdentificacion.MaxLength = 6;
            //Regex regex = new Regex(integer);
        }
        else
        {
            this.txtIdentificacion.MaxLength = 17;
            //Regex regex = new Regex(varchar);
        }
    }
}
