<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_EntidadProvOferente_List" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDocumento").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Tipo de persona o asociaci&oacute;n
                </td>
                <td>
                    Tipo identificaci&oacute;n
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged"
                        Width="40%">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" Width="40%" OnSelectedIndexChanged="ddlTipoDocumento_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    N&uacute;mero de identificaci&oacute;n
                </td>
                <td>
                    <asp:Label runat="server" ID="LblRazonSocial" Text="Proveedor" Visible="True"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtIdentificacion" onkeypress="return validaNroDocumento(event)"></asp:TextBox>
                    <%-- %><Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtIdentificacion"
                                                FilterType="Numbers" ValidChars="/1234567890" />--%>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="TxtRazonSocial" Visible="True" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Estado                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList runat="server" ID="ddlEstado" AutoPostBack="true" Width="40%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvEntidadProvOferente" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdEntidad,Proveedor" 
                        AllowSorting="true" CellPadding="0"
                        Height="16px" OnPageIndexChanging="GvEntidadProvOferentePageIndexChanging" 
                        OnSorting="GvEntidadProvOferentePage_Sorting" 
                        OnSelectedIndexChanged="GvEntidadProvOferenteSelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona o asociación" DataField="TipoPersonaNombre" SortExpression="TipoPersonaNombre"  />
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                            <%--<asp:BoundField HeaderText="Proveedor" DataField="Proveedor" SortExpression="Proveedor"  />--%>
                            <asp:TemplateField HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center" SortExpression="Proveedor">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Proveedor")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Estado" DataField="EstadoProveedor" />
                            <asp:BoundField HeaderText="Observaciones" DataField="ObserValidador" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                        <EmptyDataTemplate>
                        No tiene proveedor registrado.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
