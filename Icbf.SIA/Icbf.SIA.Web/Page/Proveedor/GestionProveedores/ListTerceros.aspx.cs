﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;


/// <summary>
/// Página que despliega los terceros que pueden ser tomados como proveedores
/// </summary>
public partial class Page_Proveedor_GestionProveedores_ListTerceros : GeneralWeb
{
    #region Propiedades
    masterPrincipal toolBar;
    string PageName = "Proveedor/GestionProveedores";
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    ManejoControles ManejoControles = new ManejoControles();
    bool _sorted = false;
    #endregion

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();


    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        //NavigateTo(SolutionPage.Add);
        NavigateTo("~/Page/" + PageName + "/Add.aspx");
    }
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        string idTipoPersona = new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
        }
    }
    protected void gvProveedor_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProveedor.SelectedRow);
    }
    protected void gvProveedor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedor.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    #endregion

    #region Métodos

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int vIdTipoPersona = 0;
            int vIdTipoDocIdentifica = 0;
            string vNumeroIdentificacion = "";
            string vTercero = "";
            int vEstado = 0;

            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDoc.SelectedValue != "-1")
            {
                vIdTipoDocIdentifica = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (txtNumeroDoc.Text != "")
            {
                vNumeroIdentificacion = txtNumeroDoc.Text.Trim();
            }
            if (txtTercero.Text != "")
            {
                vTercero = txtTercero.Text;
            }
            if (ddlEstado.SelectedValue != "-1")
            {
                vEstado = int.Parse(ddlEstado.SelectedValue);
            }
            else
            {
                vEstado = 3;
            }
            //JValverde, he colocado de momento 0 para que cargue los Terceros REgistrados que esten en cualquier estado.
            //Toca preguntar a las analistas que Estados debe filtrar.
            vEstado = 0;
            string vUsuario = GetSessionUser().NombreUsuario;
            var myGridResults = vProveedorService.ConsultarTercerosNoProveedores(vIdTipoDocIdentifica, vEstado, vIdTipoPersona, vNumeroIdentificacion, vUsuario);
            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Tercero), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Tercero, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvProveedor, GridViewSortExpression, true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //Consultando si la persona que inició sesión es externo o interno
            string providerUserkey = new GeneralWeb().GetSessionUser().Providerkey;
            Tercero objtmp = vProveedorService.ConsultarTerceroProviderUserKey(providerUserkey);
            //Si  es externo entonces
            if (objtmp.IdTercero != 0)
            {

                mostrarParametrosBusqueda(false);
            }
            else
            {

                toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

                mostrarParametrosBusqueda(true);
            }

            gvProveedor.PageSize = PageSize();
            gvProveedor.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Listar Terceros", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/" + PageName + "/List.aspx");
    }

    private void mostrarParametrosBusqueda(bool p)
    {
        pnlConsulta.Visible = p;
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strCorreoValue = gvProveedor.DataKeys[rowIndex].Values[0].ToString();
            string strIdTerceroValue = gvProveedor.DataKeys[rowIndex].Values[1].ToString();
            SetSessionParameter("Proveedor.CorreoElectronico", strCorreoValue);
            SetSessionParameter("Proveedor.IdTercero", strIdTerceroValue);
            Response.Redirect("~/page/" + PageName + "/Add.aspx");
            //NavigateTo(SolutionPage.Add);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstado, "-1", true);
            if (GetSessionParameter("Proveedor.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Proveedor.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlEstado.SelectedValue = "6";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion


    #region ordenarGrilla
    private SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }


    }
    private string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }

    }


    protected void gvProveedor_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }


    protected void ddlTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        CambiarValidadorDocumentoUsuario();
        if (ddlTipoDoc.SelectedValue != "-1")
        {
            this.txtNumeroDoc.Text = String.Empty;
            this.txtNumeroDoc.Focus();
        }
    }
    private void CambiarValidadorDocumentoUsuario()
    {
       if (this.ddlTipoDoc.SelectedItem.Text == "CC" || this.ddlTipoDoc.SelectedItem.Text == "NIT" || this.ddlTipoDoc.SelectedItem.Text == "PA")
        {
            this.txtNumeroDoc.MaxLength = 11;
            
        }
        else if (this.ddlTipoDoc.SelectedItem.Text == "CE")
        {
            this.txtNumeroDoc.MaxLength = 6;
        }
        else
        {
            this.txtNumeroDoc.MaxLength = 17;  
        }
    }

    #endregion

}
