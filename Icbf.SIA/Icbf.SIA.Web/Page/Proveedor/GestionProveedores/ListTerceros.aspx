﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="ListTerceros.aspx.cs" Inherits="Page_Proveedor_GestionProveedores_ListTerceros" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript">

        function validaNroDocumento(e)
        {

            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            var vTipoDocumentoText = document.getElementById("cphCont_ddlTipoDoc").value;
            
            //9 - Tipo Documento Carne Diplomatico
            if (vTipoDocumentoText == 9) {
                // Patron de entrada, en este caso solo acepta numeros y letras
                patron = /[0-9A-Za-z]/;
            }
            else {
                // Patron de entrada, en este caso solo acepta numeros
                patron = /[0-9]/;
            }

            tecla_final = String.fromCharCode(tecla);
            tecla_final = tecla_final.toUpperCase();
            return patron.test(tecla_final);

        }

    </script>

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona"></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoPersona" Width="90%" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona"
                        ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="rfvTipoPersona"
                        WarningIconImageUrl="../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoDoc" Width="90%" OnSelectedIndexChanged="ddlTipoDoc_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Seleccione un tipo de identificación"
                        ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="rfvTipoDoc"
                        WarningIconImageUrl="../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de documento"></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTercero" runat="server" Text="Tercero"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumeroDoc" onkeypress="return validaNroDocumento(event)"
                        Width="89%"></asp:TextBox> <%-- onkeypress="return EsNumero(event)"  --%>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtTercero" Width="89%" AutoPostBack="True"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlEstado" Enabled="False" Width="70%" TabIndex="1">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedor" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Email,IdTercero" CellPadding="0"
                        Height="16px" AllowSorting="true" OnPageIndexChanging="gvProveedor_PageIndexChanging"
                        OnSelectedIndexChanged="gvProveedor_SelectedIndexChanged" OnSorting="gvProveedor_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Tipo de Identificaci&oacute;n" DataField="NombreListaTipoDocumento"
                                SortExpression="NombreListaTipoDocumento" />
                            <asp:BoundField HeaderText="N&uacute;mero Identificaci&oacute;n" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />

                           
                            
                            <%--<asp:BoundField HeaderText="Tercero" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial"/>--%>
                            
                            <asp:TemplateField HeaderText="Tercero" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre_Razonsocial">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre_Razonsocial")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Correo Electr&oacute;nico" DataField="Email" SortExpression="Email" />
                            <asp:BoundField HeaderText="Validaci&oacute;n" DataField="NombreEstadoTercero" SortExpression="NombreEstadoTercero" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
