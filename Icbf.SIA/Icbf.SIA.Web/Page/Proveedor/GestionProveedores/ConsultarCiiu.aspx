﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/VentanaPopup.master"
    AutoEventWireup="true" CodeFile="ConsultarCiiu.aspx.cs" Inherits="Page_Proveedor_GestionProveedores_ConsultarCiiu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="Panel1" style="width:300px">
        <div style="width:900px">
            <asp:Label ID="LbError" runat="server" Text=""></asp:Label>
        </div>
        <div style="width:990px">
            <asp:Panel runat="server" ID="pnlConsulta" style="width:100%">
                <table width="90%" align="center">
                    <tr class="rowB">
                        <td style="width:50%">
                            Código CIIU 
                        </td>
                        <td style="width:50%">
                            Descripción 
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td style="width:50%">
                            <asp:TextBox runat="server" ID="txtCodigoCiiu" Width="90%"></asp:TextBox>
                        </td>
                        <td style="width:50%">
                            <asp:TextBox runat="server" ID="txtDescripcion" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td colspan="2">
                           <%-- Estado --%>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2">
                            <asp:RadioButtonList runat="server" ID="rblEstado" Visible="False" RepeatDirection="Horizontal">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlLista" style="width:100%">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvTipoCiiu" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdTipoCiiu" AllowSorting="True" CellPadding="0" Height="16px"
                                OnPageIndexChanging="gvTipoCiiu_PageIndexChanging" OnSorting="gvTipoCiiuPage_Sorting"  OnSelectedIndexChanged="gvTipoCiiu_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                Height="16px" Width="16px" ToolTip="Detalle" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Código CIIU" DataField="CodigoCiiu" SortExpression="CodigoCiiu" />
                                    <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion" />
                                    <asp:TemplateField HeaderText="Estado" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("Estado") ? "Activo" : "Inactivo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </asp:Panel>
</asp:Content>
