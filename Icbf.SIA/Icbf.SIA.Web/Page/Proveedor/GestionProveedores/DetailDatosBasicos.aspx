<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="DetailDatosBasicos.aspx.cs" Inherits="Page_EntidadProvOferente_Detail"
    ValidateRequest="false" %>


<%@ Register Src="~/General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="UcFecha" %>
<%@ Register Src="../../../General/General/Control/IcbfDireccion.ascx" TagName="IcbfDireccion"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/ucHistorialRevisiones.ascx" TagName="ucHistorialRevisiones"
    TagPrefix="uc3" %>
<asp:Content ID="ContentPrincipal" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContratoTalentoHumano" runat="server" />
    <asp:HiddenField ID="hfIdEntidad" runat="server" />
    <asp:HiddenField ID="HfIdArchivo" runat="server" />
    <asp:HiddenField ID="HfUsuarioCrea" runat="server" />
    <asp:HiddenField ID="hfIdExpEntidad" runat="server" />

    <style type="text/css">
        .ModalPopupBG {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
    <div>
        <asp:UpdatePanel ID="updatos" runat="server">
            <ContentTemplate>
                <h3 class="lbBloque">
                    <asp:Label ID="LabelTitulo" runat="server" Text="Datos B&aacute;sicos"></asp:Label>
                </h3>
                <asp:Panel ID="UpdatePanel3" runat="server">
                    <table width="80%" align="center">
                        <tr class="rowA">
                            <td colspan="3"><span lang="ES">Si ha tenido v&iacute;nculo contractual con el ICBF, especifique la fecha de inicio.</span>&nbsp;</td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <UcFecha:fecha ID="FechaICBF" runat="server" Enabled="true" TabIndex="0" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td width="50%">Consecutivo interno </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox ID="TxtConsecutivoInterno" runat="server" Enabled="False"></asp:TextBox>
                            </td>
                            <td colspan="2" style="text-align: right" width="50%">
                                <asp:LinkButton ID="lnkbtnAuditoria" runat="server" OnClick="lnkbtnAuditoria_Click1" Text="Datos de Auditoria"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>Tipo de persona o asociaci&oacute;n </td>
                            <td>Correo electr&oacute;nico </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:DropDownList ID="ddlTipoPersona" runat="server" Enabled="False" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged1" TabIndex="1" Width="300px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="TxtCorreoElectronico" runat="server" Enabled="False" MaxLength="50" TabIndex="2" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>Tipo de identificaci&oacute;n </td>
                            <td>N&uacute;mero de identificaci&oacute;n </td>
                            <td>
                                <asp:Label ID="LbDv" runat="server" Text="DV" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="False" TabIndex="3" Width="300px">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdIdTreceroProveedro" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtIdentificacion" runat="server" AutoPostBack="True" Enabled="False" MaxLength="50" TabIndex="4"></asp:TextBox>
                                <%--<Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" FilterType="Numbers" TargetControlID="txtIdentificacion" ValidChars="/1234567890" />--%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDv" runat="server" Enabled="False" MaxLength="1" TabIndex="5" Visible="False"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftxtDv" runat="server" FilterType="Numbers" TargetControlID="txtDv" ValidChars="/1234567890" />
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:Label ID="LblRazonSocial" runat="server" Text="Razon social" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:TextBox ID="TxtRazonSocial" runat="server" Enabled="False" MaxLength="128" TabIndex="6" Visible="False" Width="100%"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="ftRazonSocial" runat="server" TargetControlID="TxtRazonSocial"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/������������.,@_():; " />--%></td>
                        </tr>
                        <tr class="rowB">
                            <td colspan="2">
                                <asp:Panel ID="PnlNatural" runat="server" Style="width: 100%" Visible="False">
                                    <table style="width: 100%">
                                        <tr class="rowA">
                                            <td>
                                                <asp:Label ID="LblPrimerNombre" runat="server" Text="Primer nombre:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="LblSegundoNombre" runat="server" Text="Segundo nombre:"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerNombre" runat="server" Enabled="False" MaxLength="50" TabIndex="7" Width="80%"></asp:TextBox>
                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftPrimerNombre" runat="server" TargetControlID="TxtPrimerNombre"
                                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers    " ValidChars="/������������.,@_():; " />--%></td>
                                            <td>
                                                <asp:TextBox ID="TxtSegundoNombre" runat="server" Enabled="False" MaxLength="50" TabIndex="8" Width="80%"></asp:TextBox>
                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftSegundoNombre" runat="server" TargetControlID="TxtSegundoNombre"
                                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/������������.,@_() :;" />--%></td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                <asp:Label ID="LblPrimerApellido" runat="server" Text="Primer apellido:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="LblSegundoApellido" runat="server" Text="Segundo apellido:"></asp:Label>
                                            </td>
                                            <%-- <td>
                                            </td>--%>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerApellido" runat="server" Enabled="False" MaxLength="50" TabIndex="9" Width="80%"></asp:TextBox>
                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="TxtPrimerApellido"
                                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/������������.,@_():; " />--%></td>
                                            <td>
                                                <asp:TextBox ID="TxtSegundoApellido" runat="server" Enabled="False" MaxLength="50" TabIndex="10" Width="80%"></asp:TextBox>
                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftSegundoApellido" runat="server" TargetControlID="TxtSegundoApellido"
                                                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/������������.,@_():; " />--%></td>
                                            <%-- <td>
                                            </td>--%>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table align="left" width="80%">
                                    <tr class="rowA">
                                        <td>Indicativo
                                                    <asp:Label ID="lbIndicativo" runat="server" ForeColor="Red" Text=""></asp:Label>
                                        </td>
                                        <td>Tel�fono
                                                    <asp:Label ID="lbTelefono" runat="server" ForeColor="Red" Text=""></asp:Label>
                                        </td>
                                        <td>Extensi&oacute;n </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <asp:TextBox ID="TxtIndicativo" runat="server" Enabled="False" MaxLength="1" TabIndex="11" Width="40%"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftIndicativo" runat="server" FilterType="Numbers" TargetControlID="TxtIndicativo" ValidChars="/1234567890" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtTelefono" runat="server" Enabled="False" MaxLength="7" TabIndex="12" Width="100"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftTelefono" runat="server" FilterType="Numbers" TargetControlID="TxtTelefono" ValidChars="/1234567890" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TxtExtencion" runat="server" Enabled="False" MaxLength="6" TabIndex="13" Width="100"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftExtencion" runat="server" FilterType="Numbers" TargetControlID="TxtExtencion" ValidChars="/1234567890" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Celular
                                        <asp:Label ID="lbCelular" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblSitioWeb" runat="server" Text="Sitio web"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:TextBox ID="TxtCelular" runat="server" Enabled="False" EnableTheming="True" MaxLength="10" TabIndex="14"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftCelular" runat="server" FilterType="Numbers" TargetControlID="TxtCelular" ValidChars="/1234567890" />
                            </td>
                            <td>
                                <asp:TextBox ID="TxtSitioWeb" runat="server" Enabled="False" MaxLength="50" TabIndex="15"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="ftSitioWeb" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters" TargetControlID="TxtSitioWeb" ValidChars="/������������.,@_():; 1234567890" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:Label ID="lblSector" runat="server" Text="Sector"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblRegimenTri" runat="server" Text="R&eacute;gimen tributario"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlSector" runat="server" Enabled="False" TabIndex="16" Width="300px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlRegimenTributario" runat="server" Enabled="False" OnSelectedIndexChanged="ddlRegimenTributario_SelectedIndexChanged" TabIndex="17" Width="300px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Fecha constituci&amp;oacuten
                                        <asp:Label ID="lbCalFechaConstitucion" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblVigencia" runat="server" Text="Vigencia"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbVigenciaHasta" runat="server" Text="Vigencia hasta"></asp:Label>
                                <asp:Label ID="lbFechaVigencia" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <UcFecha:fecha ID="CalFechaConstitucion" runat="server" Enabled="False" TabIndex="18" />
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblVigencia" runat="server" Enabled="False" RepeatDirection="Vertical">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <UcFecha:fecha ID="CalFechaVigencia" runat="server" Enabled="False" TabIndex="20" Visible="True" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Nombre comercial </td>
                            <td>
                                <asp:Label ID="lblTipoEnt" runat="server" Text="Tipo de entidad"></asp:Label>
                                <asp:Label ID="lblNumInt" runat="server" Enabled="false" Text="Cantidad de integrantes" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:TextBox ID="TxtNombreEstablecimiento" runat="server" Enabled="False" MaxLength="128" TabIndex="21" TextMode="MultiLine" Width="80%"></asp:TextBox>
                                <%-- <Ajax:FilteredTextBoxExtender ID="ftNombreEstablecimiento" runat="server" TargetControlID="TxtNombreEstablecimiento"
                                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������.,@_():; 1234567890" />--%></td>
                            <td>
                                <asp:DropDownList ID="ddlTipoEntidad" runat="server" Enabled="False" TabIndex="22" Width="300px">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtNumInt" runat="server" Enabled="false" MaxLength="2" Visible="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:Label ID="lbEstadoValidacion" runat="server" Text=" Estado validaci&oacute;n documental" Visible="True"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlEstadoValidacion" runat="server" Enabled="False" TabIndex="23" Width="300px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Registrado por </td>
                            <td>Fecha registro </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:TextBox ID="txtRegistradorPor" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaRegistro" runat="server" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        </tr>

                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PnActividadCiiu" runat="server">
                    <h3 class="lbBloque">
                        <asp:Label ID="LbActividadCiiu" runat="server" Text="Actividad CIIU - Bienes y Servicios UNSPSC"></asp:Label>
                    </h3>
                    <table width="80%" align="center">
                        <tr class="rowA" width="50%">
                            <td>Actividad CIIU principal
                            </td>
                            <td>Actividad CIIU secundaria
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="TxtActividadCiiuPrin" Width="80%" TabIndex="25" Enabled="False"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hfIdActividadCiiuPrin"></asp:HiddenField>
                                <asp:Image ID="imgActividadCiiuPrin" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                    OnClick="GetCiiuPrin()" Style="cursor: hand" ToolTip="Buscar" TabIndex="26" Visible="False" />
                            </td>
                            <td class="Cell" width="50%;">
                                <asp:TextBox runat="server" ID="TxtActividadCiiuSegu" Width="80%" TabIndex="27" Enabled="False"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="HfActividadCiiuSegu"></asp:HiddenField>
                                <asp:Image ID="imgActividadCiiuSegu" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                    OnClick="GetCiiuSegun()" Style="cursor: hand" ToolTip="Buscar" TabIndex="28"
                                    Visible="False" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Fecha inicio actividad econ&oacute;mica principal
                                <asp:Label ID="lbFechaActividadPrincipal" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>Fecha inicio actividad econ&oacute;mica secundaria
                                <asp:Label ID="lbCFechaActividadSegundaria" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <UcFecha:fecha ID="CalFechaActividadPrincipal" runat="server" Width="80%" Enabled="False"
                                    Requerid="False" TabIndex="29" />
                            </td>
                            <td>
                                <UcFecha:fecha ID="CalFechaActividadSegundaria" runat="server" Width="80%" Enabled="False"
                                    Requerid="False" TabIndex="30" />
                            </td>
                        </tr>
                        <tr>
                            <td>Clasifique el producto o servicio segun codigo UNSPSC&nbsp;
                            </td>
                            <td class="Cell">
                                <asp:LinkButton ID="lnkQueRegistrar" Style="float: left" runat="server" OnClientClick="return false;">&iquest;Qu&eacute; debo registrar?</asp:LinkButton>
                                <asp:Label runat="server" Text="" ID="lbQueRegistrar" CssClass="popup"></asp:Label>
                                <Ajax:BalloonPopupExtender ID="BalloonPopupExtender2" runat="server" TargetControlID="lnkQueRegistrar"
                                    BalloonPopupControlID="lbQueRegistrar" Position="BottomLeft" BalloonStyle="Rectangle"
                                    BalloonSize="Small" CustomCssUrl="CustomStyle/BalloonPopupOvalStyle.css" CustomClassName="oval"
                                    UseShadow="true" ScrollBars="Auto" DisplayOnFocus="True" DisplayOnClick="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField runat="server" ID="hfIdTipoCodUNSPSC"></asp:HiddenField>
                                <asp:GridView runat="server" ID="gvCodigoUNSPSC" AutoGenerateColumns="False" AllowPaging="True"
                                    GridLines="None" Width="100%" DataKeyNames="idTipoCodUNSPSC" CellPadding="0"
                                    Height="16px" OnPageIndexChanging="gvCodigoUNSPSC_PageIndexChanging" OnSelectedIndexChanged="gvCodigoUNSPSC_SelectedIndexChanged"
                                    OnRowCommand="gvCodigoUNSPSC_RowCommand">
                                    <Columns>
                                        <asp:BoundField HeaderText="Codigo" DataField="Codigo" SortExpression="Codigo" />
                                        <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" SortExpression="Descripcion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <script type="text/javascript" language="javascript">
                        function GetCiiuPrin() {
                            var prevReturnValue = window.returnValue; // Save the current returnValue
                            window.returnValue = undefined;
                            //\
                            var retLupa = window.showModalDialog('../../Proveedor/GestionProveedores/ConsultarCiiu.aspx', null, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                            if (retLupa == undefined) {
                                retLupa = window.returnValue;
                            }
                            if (retLupa != null) {
                                document.getElementById('cphCont_tcInfoProveedor_tpDatosBasicos_TxtActividadCiiuPrin').value = retLupa[2];
                                document.getElementById('cphCont_tcInfoProveedor_tpDatosBasicos_hfIdActividadCiiuPrin').value = retLupa[0];
                            }
                        }
                        function GetCiiuSegun() {
                            var prevReturnValue = window.returnValue; // Save the current returnValue
                            window.returnValue = undefined;
                            var retLupa = window.showModalDialog('../../Proveedor/GestionProveedores/ConsultarCiiu.aspx', null, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                            if (retLupa == undefined) {
                                retLupa = window.returnValue;
                            }
                            if (retLupa != null) {
                                document.getElementById('cphCont_tcInfoProveedor_tpDatosBasicos_TxtActividadCiiuSegu').value = retLupa[2];
                                document.getElementById('cphCont_tcInfoProveedor_tpDatosBasicos_HfActividadCiiuSegu').value = retLupa[0];
                            }
                        }
                    </script>
                </asp:Panel>
                <asp:Panel ID="PnComplementoPrivada" runat="server" Visible="False">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label1" runat="server" Text="Complemento de datos b&aacute;sicos entidades privadas"></asp:Label>
                    </h3>
                    <table width="80%" align="center">
                        <tr class="rowA">
                            <td width="50%">Tipo de actividad
                                <asp:Label ID="lbTipoActividad" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClaseEntidad" runat="server" Text="Clase de Entidad o Establecimiento"></asp:Label>
                                <asp:Label ID="lbClaseEntidad" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlTipoActividad" runat="server" Width="300px" Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlClaseEntidad" runat="server" Width="380px" TabIndex="32"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Exenci&oacute;n Matr&iacute;cula Mercantil
                                <asp:Label ID="lbExtecionMatruculaMercantil" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlExtecionMatruculaMercantil" runat="server" AutoPostBack="True"
                                    Width="300px" TabIndex="33" Enabled="False">
                                    <asp:ListItem Selected="True" Value="-1">Seleccione..</asp:ListItem>
                                    <asp:ListItem Value="1">SI</asp:ListItem>
                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                <asp:Label ID="LBNumeroMatriculaMercantil" runat="server" Text="N&uacute;mero de Matr&iacute;cula Mercantil"></asp:Label>
                                <asp:Label ID="lbNumeroMatriculaMercan" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LbFechaMatriculaMercantil" runat="server" Text="Fecha Matricula Mercantil"></asp:Label>
                                <asp:Label ID="lbFechaMatriculaMercan" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:TextBox runat="server" ID="TxtNumeroMatriculaMercantil" MaxLength="20" TabIndex="34"
                                    Enabled="False"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtCelular"
                                    FilterType="Numbers" ValidChars="/������������ .,@_():;1234567890" />
                            </td>
                            <td>
                                <UcFecha:fecha ID="CalFechaMatriculaMercantil" runat="server" Width="80%" Enabled="False"
                                    Requerid="False" TabIndex="35" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PnComplementoPublicas" runat="server" Visible="False">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label2" runat="server" Text="Complemento de Datos b&aacute;sicos"></asp:Label>
                    </h3>
                    <table width="80%" align="center">
                        <tr class="rowA">
                            <td width="50%">Rama o estructura
                                <asp:Label ID="lbRamaoestructura" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>Nivel de gobierno
                                <asp:Label ID="lbNivelgobierno" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlRamaoestructura" runat="server" Width="300px" TabIndex="36"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlNivelgobierno" runat="server" Width="300px" TabIndex="37"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>Nivel organizacional
                                <asp:Label ID="lbNivelOrganizacional" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                            <td>Tipo de entidad
                                <asp:Label ID="lbTipoEntidadPublica" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlNivelOrganizacional" runat="server" Width="300px" TabIndex="38"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipoEntidadPublica" runat="server" Width="300px" TabIndex="39"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlDomicilioLegal" runat="server">
                    <h3 class="lbBloque">
                        <asp:Label ID="Label3" runat="server" Text="Domicilio legal en Colombia Donde est&eacute; constituida"></asp:Label>
                    </h3>
                    <table width="80%" align="center">
                        <tr class="rowA">
                            <td width="50%">Departamento
                            </td>
                            <td>Municipio
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:DropDownList ID="ddlDepartamentoConstituida" runat="server" Width="300px" TabIndex="40"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMunicipioConstituida" runat="server" Width="300px" TabIndex="41"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <h3 class="lbBloque">
                    <asp:Label ID="Label4" runat="server" Text="Datos de Ubicaci&oacute;n/Direcci&oacute;n comercial o de gerencia"></asp:Label>
                </h3>
                <table width="80%" align="center">
                    <tr class="rowA">
                        <td width="50%">Departamento
                        </td>
                        <td>Municipio
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td>
                            <asp:DropDownList ID="ddlDepartamentoDireccionComercial" runat="server" Width="300px"
                                TabIndex="42" Enabled="False">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMunicipioDireccionComercial" runat="server" Width="300px"
                                TabIndex="43" Enabled="False">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table width="80%" align="center">
                    <tr class="rowA">
                        <td>
                            <asp:Label ID="lbDireccionResidencia" runat="server" ForeColor="Red" Text=""></asp:Label>
                            <uc2:IcbfDireccion ID="txtDireccionResidencia" runat="server" Requerido="True" Enabled="False"
                                ReadOnly="True" />
                        </td>
                    </tr>
                </table>
                <table width="80%" align="center">
                    <caption>
                        <tr class="rowA">
                            <td width="100%">
                                <asp:Panel ID="PnlReprLegal" runat="server">
                                    <h3 class="lbBloque">
                                        <asp:Label ID="Label5" runat="server" Text="Representante Legal"></asp:Label>
                                    </h3>
                                    <table style="width: 100%">
                                        <tr class="rowB">
                                            <td style="width: 50%">Tipo de identificaci&oacute;n
                                                <asp:Label ID="lbTipoDocumentoReprelegal" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                            <td style="width: 50%">N&uacute;mero de identificaci&oacute;n
                                                <asp:Label ID="lbidDocumentoRepre" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td style="width: 50%">
                                                <asp:DropDownList ID="ddlTipoDocumentoRepr" runat="server" Enabled="False" TabIndex="50"
                                                    Width="300px">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfRepreLegal" runat="server" />
                                            </td>
                                            <td style="width: 50%">
                                                <asp:TextBox ID="txtIdentificacionRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="51"></asp:TextBox>
                                                <Ajax:FilteredTextBoxExtender ID="ftIdentificacionRepr" runat="server" FilterType="Custom"
                                                    TargetControlID="txtIdentificacionRepr" ValidChars="abcdefghijklmn�opqrstuvwxyz ABCDEFGHIJKLMN�OPQRSTUVWXYZ.0123456789" />
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>Primer nombre
                                                <asp:Label ID="lbPrimerNombreRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                            <td>Segundo nombre
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerNombreRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="52" Width="80%"></asp:TextBox>
                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftPrimerNombreRepr" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                    TargetControlID="TxtPrimerNombreRepr" ValidChars="/������������1234567890" />--%>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtSegundoNombreRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="53" Width="80%"></asp:TextBox>
                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftSegundoNombreRepr" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                    TargetControlID="TxtSegundoNombreRepr" ValidChars="/������������.,@_(): ;1234567890" />--%>
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>Primer apellido
                                                <asp:Label ID="lbPrimerApellidoRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                            <td>Segundo apellido
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerApellidoRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="54" Width="80%"></asp:TextBox>
                                                <%--  <Ajax:FilteredTextBoxExtender ID="ftPrimerApellidoRepr" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                    TargetControlID="TxtPrimerApellidoRepr" ValidChars="/������������1234567890" />--%>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtSegundoApellidoRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="55" Width="80%"></asp:TextBox>
                                                <%-- <Ajax:FilteredTextBoxExtender ID="ftSegundoApellidoRepr" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                    TargetControlID="TxtSegundoApellidoRepr" ValidChars="/������������ 1234567890" />--%>
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>Sexo
                                            </td>
                                            <td>Fecha de nacimiento
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="rowB">
                                            <td class="Cell" width="50%;">
                                                <asp:DropDownList runat="server" ID="ddlSexoRepLegal" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Cell" width="50%;">
                                                <UcFecha:fecha ID="cuFechaNacRepLegal" runat="server" Enabled="false" Requerid="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr class="rowA">
                                                        <td width="25%">Celular
                                                            <asp:Label ID="lbCelularRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                        </td>
                                                        <td width="25%">Indicativo
                                                            <asp:Label ID="lbIndicativoRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                        </td>
                                                        <td width="25%">Tel&eacute;fono
                                                            <asp:Label ID="lbTelefonoRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                        </td>
                                                        <td width="25%">Extensi&oacute;n
                                                        </td>
                                                    </tr>
                                                    <tr class="rowB">
                                                        <td>
                                                            <asp:TextBox ID="TxtCelularRepr" runat="server" Enabled="False" MaxLength="10" TabIndex="56"
                                                                Width="100"></asp:TextBox>
                                                            <Ajax:FilteredTextBoxExtender ID="ftCelularRepr" runat="server" FilterType="Numbers"
                                                                TargetControlID="TxtCelularRepr" ValidChars="/1234567890" />
                                                            <asp:HiddenField ID="hfTelTelRepreLega" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TxtIndicativoRepr" runat="server" Enabled="False" MaxLength="1"
                                                                TabIndex="57" Width="40"></asp:TextBox>
                                                            <Ajax:FilteredTextBoxExtender ID="ftIndicativoRepr" runat="server" FilterType="Numbers"
                                                                TargetControlID="TxtIndicativoRepr" ValidChars="/1234567890" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TxtTelefonoRepr" runat="server" Enabled="False" MaxLength="7" TabIndex="58"
                                                                Width="100"></asp:TextBox>
                                                            <Ajax:FilteredTextBoxExtender ID="ftTelefonoRepr" runat="server" FilterType="Numbers"
                                                                TargetControlID="TxtTelefonoRepr" ValidChars="/1234567890" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TxtExtencionRepr" runat="server" Enabled="False" MaxLength="6" TabIndex="59"
                                                                Width="100"></asp:TextBox>
                                                            <Ajax:FilteredTextBoxExtender ID="ftExtencionRepr" runat="server" FilterType="Numbers"
                                                                TargetControlID="TxtExtencionRepr" ValidChars="/1234567890" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>Correo electr&oacute;nico
                                                <asp:Label ID="lbCorreoElectronicoRepr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                <asp:TextBox ID="TxtCorreoElectronicoRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="60" Width="80%"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </caption>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <h3 class="lbBloque">
            <asp:Label ID="Label6" runat="server" Text="Documento digital/ certificados a adjuntar"></asp:Label>
        </h3>
        <table width="80%" align="center">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblDocumentosPersonal" Text="Documentos informaci&oacute;n de personal"
                        runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocDatosBasicoProv" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocDatosBasicoProv_RowCommand"
                        OnPageIndexChanging="gvDocDatosBasicoProv_PageIndexChanging" OnSelectedIndexChanged="gvDocDatosBasicoProv_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre del documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (int)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Documento" DataField="LinkDocumento" />--%>
                            <asp:TemplateField HeaderText="Documento">
                                <ItemTemplate>
                                    <asp:Label ID="lblLnkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Observaciones">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblObservaciones" runat="server" Enabled="False" Width="200px" Height="80px"
                                        TextMode="MultiLine" MaxLength="128" Text='<%# Bind("Observaciones") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <%--   <ItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgEditar" ImageUrl="~/Image/btn/attach.jpg"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Edit" ToolTip="Editar">
                                    </asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgEliminar" ImageUrl="~/Image/btn/delete.gif"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Delete" ToolTip="Eliminar"
                                        Visible='<%# !(bool)(Request.QueryString["oP"] == "E") %>' OnClientClick="return confirm('�Est� seguro de eliminar este documento?');">
                                    </asp:ImageButton>
                                </ItemTemplate>--%>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" ID="imgUpdate" ImageUrl="~/Image/btn/apply.png" Width="18px"
                                        Height="18px" CausesValidation="True" CommandName="Update" ToolTip="Actualizar"></asp:ImageButton>
                                    &nbsp;
                                    <asp:ImageButton runat="server" ID="imgCancel" ImageUrl="~/Image/btn/Cancel.png"
                                        Width="18px" Height="18px" CausesValidation="True" CommandName="Cancel" ToolTip="Cancelar"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label7" Text="Documento(s) de registro de tercero" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvDocAdjuntos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdDocAdjunto,IdDocumento,MaxPermitidoKB,ExtensionesPermitidas"
                        CellPadding="0" Height="16px" OnRowCommand="gvDocAdjuntos_RowCommand" OnPageIndexChanging="gvDocAdjuntos_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Nombre del documento" SortExpression="NombreDocumento">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text='<%# Bind("NombreTipoDocumento") %>'></asp:Label>
                                    <asp:Label ID="lblObligatorio" runat="server" Text='<%# string.Format("{0}", (Int16)Eval("Obligatorio") == 1 ? " *" : "") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Link documento" SortExpression="LinkDocumento">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'></asp:Label>--%>
                                    <asp:Label ID="lblLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--<asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# Bind("LinkDocumento") %>'
                                        Visible="false"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtLinkDocumento" runat="server" Text='<%# (string)Eval("LinkDocumento") == "" ? "" : Eval("LinkDocumento").ToString().Substring(Eval("LinkDocumento").ToString().IndexOf("ProveedorDatosBasico_") + "ProveedorDatosBasico_".Length) %>'
                                        Visible="false"></asp:TextBox>
                                    <asp:FileUpload ID="fuDocumento" runat="server"></asp:FileUpload>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Extensiones permitidas">
                                <ItemTemplate>
                                    <asp:Label ID="lblExtensionesPermitidas" runat="server" Text='<%# Bind("ExtensionesPermitidas") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="M&aacute;ximo permitido KB">
                                <ItemTemplate>
                                    <asp:Label ID="lblMaxPermitidoKB" runat="server" Text='<%# string.Format("{0}", (Int32)Eval("MaxPermitidoKB") )%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" ImageUrl="~/Image/btn/list.png" Height="18px"
                                        Width="18px" ToolTip="Ver Documento" CommandName="VerDocumento" CommandArgument='<%# Eval("LinkDocumento") %>'
                                        Visible='<%# !(bool)Eval("LinkDocumento").Equals(string.Empty) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table width="80%" align="center">
            <tr>
                <td>
                    <uc3:ucHistorialRevisiones ID="ucHistorialRevisiones1" runat="server" style="width: 90%" />
                </td>
            </tr>
        </table>       
        <table width="80%" align="center">
            <tr>
                <td>
                    <asp:Panel ID="pnlAuditoriaDatosBasicos" runat="server" Width="90%" Style="height: 95%; overflow: auto; z-index: 20001;">
                        <table width="90%" style="background-color: White">
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 95%">
                                    <label style="font-weight: bold">
                                        Auditor&iacute;a de Acciones en Datos B&aacute;sicos</label>
                                </td>
                                <td style="width: 5%">
                                    <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Image/btn/Cancel.png" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width: 80%">
                                    <asp:GridView ID="gridAuditoriaDatBAsicos" runat="server" AutoGenerateColumns="False"
                                        HeaderStyle-Font-Bold="true" AllowPaging="True" GridLines="None" Width="100%"
                                        CellPadding="0" Height="16px" EmptyDataText="No se han realizado acciones en Datos B�sicos."
                                        PageSize="10" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gridAuditoriaDatBAsicos_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField HeaderText="Fecha" HeaderStyle-HorizontalAlign="Left" DataField="Fecha" />
                                            <asp:BoundField HeaderText="Usuario" HeaderStyle-HorizontalAlign="Left" DataField="Usuario" />
                                            <asp:BoundField HeaderText="IdSistema" HeaderStyle-HorizontalAlign="Left" DataField="IdSistema" />
                                            <asp:BoundField HeaderText="Sistema" HeaderStyle-HorizontalAlign="Left" DataField="Sistema" />
                                            <asp:BoundField HeaderText="Acci&oacute;n" HeaderStyle-HorizontalAlign="Left" DataField="Accion" />
                                        </Columns>
                                        <AlternatingRowStyle CssClass="rowBG" />
                                        <EmptyDataRowStyle CssClass="headerForm" />
                                        <HeaderStyle CssClass="headerForm" />
                                        <RowStyle CssClass="rowAG" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <Ajax:ModalPopupExtender ID="mdpDatosAuditoria" runat="server" PopupControlID="pnlAuditoriaDatosBasicos"
        TargetControlID="btnTarget" Drag="true" BackgroundCssClass="ModalPopupBG" CancelControlID="btnCancel">
    </Ajax:ModalPopupExtender>
    <asp:Button ID="btnTarget" runat="server" Text="target" Style="visibility: hidden" />
</asp:Content>
