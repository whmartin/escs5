﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;

/// <summary>
/// Página que permite consultar Ciuu a través de filtros
/// </summary>
public partial class Page_Proveedor_GestionProveedores_ConsultarCiiu : GeneralWeb
{
    Poveedor_VentanaPopup toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (Poveedor_VentanaPopup)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate_Poveedor_VentanaPopup(btnBuscar_Click);
            gvTipoCiiu.PageSize = PageSize();
            gvTipoCiiu.EmptyDataText = EmptyDataText();
            toolBar.EstablecerTitulos("Consultar Código CIIU", "ConsultarCiiu");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Establece tamaño de paginación
    /// </summary>
    /// <returns></returns>
    protected int PageSize()
    {
        Int32 vPageSize;
        if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
        {
            throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
        }
        return vPageSize;
    }
    protected String EmptyDataText()
    {
        String vEmptyDataText;
        if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
        {
            throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
        }
        vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
        return vEmptyDataText;
    }
    /// <summary>
    /// efectúa filtro en la búsqueda para despues asignar resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigoCiiu = null;
            String vDescripcion = null;
            Boolean? vEstado = null;
            if (txtCodigoCiiu.Text != "")
            {
                vCodigoCiiu = Convert.ToString(txtCodigoCiiu.Text);
            }
            if (txtDescripcion.Text != "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }
            //if (rblEstado.SelectedValue!= "-1")
            if (rblEstado.SelectedValue != "")
            {
                vEstado = Convert.ToBoolean(Convert.ToInt16(rblEstado.SelectedValue));
            }
            gvTipoCiiu.DataSource = vProveedorService.ConsultarTipoCiius(vCodigoCiiu, vDescripcion, vEstado);
            gvTipoCiiu.DataBind();
            SetSessionParameter("gvTipoCiiu.DataSource", gvTipoCiiu.DataSource);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }



    /// <summary>
    /// Selecciona registro de la grilla de actividad económica princiopal
    /// </summary>
    /// <param name="pRow"></param>
    /// 
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues = "";
            returnValues += HttpUtility.HtmlDecode(gvTipoCiiu.DataKeys[gvTipoCiiu.SelectedRow.RowIndex].Value.ToString()) + ";";
            for (int c = 1; c < 4; c++)
            {
                if (gvTipoCiiu.Rows[gvTipoCiiu.SelectedIndex].Cells[c].Text != "&nbsp;")
                    returnValues += HttpUtility.HtmlDecode(gvTipoCiiu.Rows[gvTipoCiiu.SelectedIndex].Cells[c].Text) + ";";
                else
                    returnValues += ";";
            }
            List<string> parametros = new List<string>();
            parametros = returnValues.Split(';').ToList();
            GetScriptCloseDialogCallback(string.Empty, parametros);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    

    protected void gvTipoCiiu_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoCiiu.SelectedRow);
    }
    protected void gvTipoCiiu_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoCiiu.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region ordenarGrilla
    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<TipoCiiu> SortList(List<TipoCiiu> data, bool isPageIndexChanging)
    {
        List<TipoCiiu> result = new List<TipoCiiu>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }

    protected void gvTipoCiiuPage_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvTipoCiiu.PageIndex;

        if (GetSessionParameter("gvTipoCiiu.DataSource") != null)
        {
            if (gvTipoCiiu.DataSource == null) { gvTipoCiiu.DataSource = (List<TipoCiiu>)GetSessionParameter("gvTipoCiiu.DataSource"); }
            gvTipoCiiu.DataSource = SortList((List<TipoCiiu>)gvTipoCiiu.DataSource, false);
            gvTipoCiiu.DataBind();
        }
        gvTipoCiiu.PageIndex = pageIndex;
    }
    #endregion
}

