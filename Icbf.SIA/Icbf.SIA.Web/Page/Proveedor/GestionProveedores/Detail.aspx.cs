﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;


using Icbf.SIA.Service;

/// <summary>
/// Página que despliega el detalle del registro de proveedor
/// </summary>
/// 
public partial class Page_Proveedor_GestionProveedores_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vRuboService = new SIAService();
    string PageName = "Proveedor/GestionProveedores";
    Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente = new Icbf.Proveedor.Entity.EntidadProvOferente();
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            var identida = GetSessionParameter("EntidadProvOferente.IdEntidad");
            SetSessionParameter("IdEntidad", identida); //prueba para integrantes
            int vIdEntidad = Convert.ToInt32(identida);
            vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            Tercero varTercero = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
            if (varTercero.IdTipoPersona == 3 || varTercero.IdTipoPersona == 4)
            {
                HabilitaParaConsorcioOUnion();
            }
            else
            {
                if (vEntidadProvOferente.IdTipoSector.ToString() == "2") // sectro publico
                {
                    DeshabilitarMenus1();
                }
                else
                {
                    DeshabilitarMenus();
                }
            }

            if (Request["Op"] != null)
            {
                string operacion = Request["Op"].ToString();
                if (operacion.Equals("Datos Básicos"))
                {
                    tcInfoProveedor.ActiveTab = tpDatosBasicos;
                }
                if (operacion.Equals("Financiera"))
                {
                    tcInfoProveedor.ActiveTab = tpDatosFinancieros;
                }
                if (operacion.Equals("Experiencias"))
                {
                    tcInfoProveedor.ActiveTab = tpExperiencia;
                }
                if (operacion.Equals("Integrantes"))
                {
                    tcInfoProveedor.ActiveTab = tpIntegrantes;
                }
                if (operacion.Equals("Formación Académica"))
                {
                    tcInfoProveedor.ActiveTab = tpFormacion;
                }
            }


        }
    }

    protected void tcInfoProveedor_ActiveTabChanged(object sender, EventArgs e)
    {
        toolBar = (masterPrincipal)this.Master;
        TabContainer tab = (TabContainer)sender;
        switch (tab.ActiveTabIndex)
        {
            case 0:
                frameDatosBasicos.Attributes.Add("src", "../GestionProveedores/DetailDatosBasicos.aspx");
                break;
            case 1:
                frameInfoFinanciera.Attributes.Add("src", "../InfoFinancieraEntidad/List.aspx");
                break;
            case 2:
                frameProveedorExperiencia.Attributes.Add("src", "../InfoExperienciaEntidad/List.aspx");
                break;
            case 3:
                frameSucursal.Attributes.Add("src", "../Sucursal/List.aspx");
                break;
            case 4:
                frameProveedorContactos.Attributes.Add("src", "../ContactoEntidad/List.aspx");
                break;
            case 5:
                frameNotificacionJudicial.Attributes.Add("src", "../NotificacionJudicial/List.aspx");
                break;
            case 6:
                frameIntegrantes.Attributes.Add("src", "../Integrantes/List.aspx");
                break;
            case 7:
                frameformacion.Attributes.Add("src", "../Formacion/List.aspx");
                break;

        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Información Proveedor");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(GetSessionParameter("ValidarProveedor.NroRevision").ToString()))
        {
            NavigateTo("~/Page/" + PageName + "/List.aspx");
        }
        else
        {
            //Retorna al List de Validar Proveedores
            NavigateTo("~/Page/Proveedor/ValidarProveedor/List.aspx");
        }
    }

    /// <summary>
    /// Deshabilita menus
    /// </summary>
    public void DeshabilitarMenus()
    {
        tpDatosFinancieros.Visible = true;
        tpExperiencia.Visible = true;
        tpSucursal.Visible = true;
        tpContactos.Visible = true;
        tpFormacion.Visible = true;
        //tpNotificacionJudicial.Visible = true;
        if (vRuboService.ConsultarUsuario(GetSessionUser().NombreUsuario).Rol.Split(';').Contains("PROVEEDORES"))
        {
            tpNotificacionJudicial.Visible = false;  // se  oculta el tab NotificacionJudicial para externos
        }
        else
        {
            tpNotificacionJudicial.Visible = true;
        }

       
    }

    public void DeshabilitarMenus1()
    {
        tpSucursal.Visible = true;
        tpContactos.Visible = true;
    }

    public void HabilitaParaConsorcioOUnion()
    {
        int vIdEntidad = Convert.ToInt32(GetSessionParameter("IdEntidad"));
        int vTercero = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad).IdTercero;
        Icbf.Proveedor.Entity.EntidadProvOferente entidadProveedor =
            vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
        Tercero tercero = vProveedorService.ConsultarTercero(entidadProveedor.IdTercero);

        if (tercero.IdTipoPersona == 3 || tercero.IdTipoPersona == 4)
        {
            tpIntegrantes.Visible = true;
        }

    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
}