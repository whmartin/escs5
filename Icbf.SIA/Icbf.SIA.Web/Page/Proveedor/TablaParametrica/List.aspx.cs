using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tablas param�tricas
/// </summary>
public partial class Page_Proveedor_TablaParametrica_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/TablaParametrica";
    ProveedorService vProveedorService = new ProveedorService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigoTablaParametrica = null;
            String vNombreTablaParametrica = null;
            Boolean? vEstado = null;
            if (txtCodigoTablaParametrica.Text!= "")
            {
                vCodigoTablaParametrica = Convert.ToString(txtCodigoTablaParametrica.Text);
            }
            if (txtNombreTablaParametrica.Text!= "")
            {
                vNombreTablaParametrica = Convert.ToString(txtNombreTablaParametrica.Text);
            }
            if (rblEstado.SelectedValue != "")
            {
                vEstado = Convert.ToBoolean(Convert.ToInt32(rblEstado.SelectedValue));
            }
            gvTablaParametrica.DataSource = vProveedorService.ConsultarTablaParametricas(vCodigoTablaParametrica, vNombreTablaParametrica, vEstado);
            SetSessionParameter("gvTablaParametrica.DataSource", gvTablaParametrica.DataSource);
            gvTablaParametrica.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvTablaParametrica.PageSize = PageSize();
            gvTablaParametrica.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Listas Param&eacute;tricas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTablaParametrica.DataKeys[rowIndex].Values[0].ToString();
            SetSessionParameter("TablaParametrica.IdTablaParametrica", strValue);
            
            Response.Redirect("~/page/" + strValue + "/List.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTablaParametrica_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTablaParametrica.SelectedRow);
    }
    protected void gvTablaParametrica_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTablaParametrica.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TablaParametrica.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TablaParametrica.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Ordenar Grilla (Revisar si se generaliza)

    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<TablaParametrica> SortList(List<TablaParametrica> data, bool isPageIndexChanging)
    {
        List<TablaParametrica> result = new List<TablaParametrica>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }
    protected void gvTablaParametrica_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvTablaParametrica.PageIndex;

        if (GetSessionParameter("gvTablaParametrica.DataSource") != null)
        {
            if (gvTablaParametrica.DataSource == null) { gvTablaParametrica.DataSource = (List<TablaParametrica>)GetSessionParameter("gvTablaParametrica.DataSource"); }
            gvTablaParametrica.DataSource = SortList((List<TablaParametrica>)gvTablaParametrica.DataSource, false);
            gvTablaParametrica.DataBind();
        }
        gvTablaParametrica.PageIndex = pageIndex;
    }

    #endregion
}
