using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de registro y edición de tablas paramétricas
/// </summary>
public partial class Page_Proveedor_TablaParametrica_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/TablaParametrica";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TablaParametrica vTablaParametrica = new TablaParametrica();

            vTablaParametrica.CodigoTablaParametrica = Convert.ToString(txtCodigoTablaParametrica.Text).Trim();
            vTablaParametrica.NombreTablaParametrica = Convert.ToString(txtNombreTablaParametrica.Text).Trim();
            vTablaParametrica.Url = Convert.ToString(txtUrl.Text);
            vTablaParametrica.Estado = Convert.ToBoolean(ddlEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vTablaParametrica.IdTablaParametrica = Convert.ToInt32(hfIdTablaParametrica.Value);
                vTablaParametrica.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTablaParametrica, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarTablaParametrica(vTablaParametrica);
            }
            else
            {
                vTablaParametrica.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTablaParametrica, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarTablaParametrica(vTablaParametrica);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TablaParametrica.IdTablaParametrica", vTablaParametrica.IdTablaParametrica);
                SetSessionParameter("TablaParametrica.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Listas Param&eacute;tricas", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdTablaParametrica = Convert.ToInt32(GetSessionParameter("TablaParametrica.IdTablaParametrica"));
            RemoveSessionParameter("TablaParametrica.Id");

            TablaParametrica vTablaParametrica = new TablaParametrica();
            vTablaParametrica = vProveedorService.ConsultarTablaParametrica(vIdTablaParametrica);
            hfIdTablaParametrica.Value = vTablaParametrica.IdTablaParametrica.ToString();
            txtCodigoTablaParametrica.Text = vTablaParametrica.CodigoTablaParametrica;
            txtNombreTablaParametrica.Text = vTablaParametrica.NombreTablaParametrica;
            txtUrl.Text = vTablaParametrica.Url;
            ddlEstado.SelectedValue = vTablaParametrica.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTablaParametrica.UsuarioCrea, vTablaParametrica.FechaCrea, vTablaParametrica.UsuarioModifica, vTablaParametrica.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
