<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Proveedor_TablaParametrica_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTablaParametrica" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código TablaParametrica *
            </td>
            <td>
                Nombre TablaParametrica *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoTablaParametrica"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreTablaParametrica"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Url
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUrl"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlEstado"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
