<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Proveedor_TablaParametrica_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre Lista Paramétrica
            </td>
            <td style="display: none">
                Código Lista Paramétrica
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreTablaParametrica" Width="50%" MaxLength = "50" ></asp:TextBox>
            </td>
            <td style="display: none">
                <asp:TextBox runat="server" ID="txtCodigoTablaParametrica"  MaxLength = "10" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2" style="display: none">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" style="display: none">
                <%--<asp:DropDownList runat="server" ID="ddlEstado"  ></asp:DropDownList>--%>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Visible="False"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTablaParametrica" AutoGenerateColumns="False" AllowPaging="True"
                    EmptyDataText="No existen listas parametrizadas<strong></strong>"
                        GridLines="None" Width="100%" DataKeyNames="Url" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTablaParametrica_PageIndexChanging" 
                        OnSelectedIndexChanged="gvTablaParametrica_SelectedIndexChanged" 
                        AllowSorting="true" OnSorting="gvTablaParametrica_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre lista param&eacute;trica" DataField="NombreTablaParametrica" SortExpression = "NombreTablaParametrica" />
                            <asp:BoundField HeaderText="Estado" DataField="DescripcionEstado"  SortExpression = "DescripcionEstado"/>
                        </Columns>
                         <EmptyDataTemplate>
                            <div >
                                <table cellpadding="0" cellspacing="0" summary="" width="90%" align="center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Nombre lista param&eacute;trica</th>
                                            <th scope="col">Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="rowB">
                                            <td colspan="2">
                                                No existen listas parametrizadas<strong></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </EmptyDataTemplate>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
