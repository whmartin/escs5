<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Proveedor_TablaParametrica_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTablaParametrica" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código TablaParametrica *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoTablaParametrica" ControlToValidate="txtCodigoTablaParametrica"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Nombre TablaParametrica *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreTablaParametrica" ControlToValidate="txtNombreTablaParametrica"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoTablaParametrica"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoTablaParametrica" runat="server" TargetControlID="txtCodigoTablaParametrica"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreTablaParametrica"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreTablaParametrica" runat="server" TargetControlID="txtNombreTablaParametrica"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Url
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="ddlEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvEstado" ControlToValidate="ddlEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUrl"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftUrl" runat="server" TargetControlID="txtUrl"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlEstado"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
