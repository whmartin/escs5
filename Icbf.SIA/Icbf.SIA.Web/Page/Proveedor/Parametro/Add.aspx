﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Parametro_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdParametro" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Nombre de Parámetro 
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreParametro" ControlToValidate="txtNombreParametro"
                    SetFocusOnError="true" ErrorMessage="Valor en blanco" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Valor de Parámetro *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorParametro" ControlToValidate="txtValorParametro"
                    SetFocusOnError="true" ErrorMessage="Valor en blanco" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revTexbox3" runat="server" ErrorMessage="Debe ingresar hasta un máximo de 512 caracteres"
                    ValidationExpression="^([\S\s]{0,512})$" 
                    ControlToValidate="txtValorParametro" Display="Dynamic"
                    ValidationGroup="btnGuardar" SetFocusOnError="true" ForeColor="Red"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreParametro" Enabled="False" Width="350px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreParametro" runat="server" TargetControlID="txtNombreParametro"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorParametro" Rows="4" TextMode="MultiLine"
                    MaxLength="512" onkeyDown="checkTextAreaMaxLength(this,event,'20');"  Width="350px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorParametro" runat="server" TargetControlID="txtValorParametro"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Funcionalidad
            </td>
            <td>
                <asp:label ID="lblImagenParametro" runat="server" Text ="Imagen de Parámetro" Visible="false"  />
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFuncionalidad" Width="350px" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:FileUpload ID="fuArchivoImagen" runat="server" Visible="true" />
            </td>
        </tr>
    </table>
</asp:Content>
