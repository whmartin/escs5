﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using System.IO;

/// Se habilita opcion de guardar y editar unn parámetro
/// <summary>
/// Página de registro y edición de parámetros
/// </summary>

public partial class Page_Parametro_Add : GeneralWeb
{

    masterPrincipal toolBar;
    SeguridadService vSeguridadService = new SeguridadService();
    string PageName = "Proveedor/Parametro";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }


      /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Parametro vParametro = new Parametro();

            vParametro.NombreParametro = Convert.ToString(txtNombreParametro.Text);
            vParametro.ValorParametro = Convert.ToString(txtValorParametro.Text);
            
            vParametro.Funcionalidad = Convert.ToString(txtFuncionalidad.Text);

            if (fuArchivoImagen.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(fuArchivoImagen.FileName);
                    string extension = Path.GetExtension(filename);

                    //
                    //Se modifica por solicitud de Bayron 2013/07/04
                    //

                    if (extension != ".jpg") //&& extension != ".jpeg" && extension != ".gif")
                    {
                        //throw new GenericException("Solo se permiten archivos JPG o GIF");
                        throw new GenericException("Solo se permiten archivos JPG");
                    }
                    if (fuArchivoImagen.PostedFile.ContentLength / 1024 > 4096)
                    {
                        throw new GenericException("Solo se permiten archivos inferiores a cuatro megas 4MB");
                    }

                    //Guardado del archivo en el servidor
                    HttpPostedFile vHttpPostedFile = fuArchivoImagen.PostedFile;
                    //Boolean ArchivoSubido = vRUBOService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream, NombreArchivoFtp, NombreArchivo);
                    string vPath = this.Page.Server.MapPath("~/Image/Proveedores");
                    string NombreArchivo = "imgLoginProveedores" + extension;
                    vHttpPostedFile.SaveAs(vPath + "/" + NombreArchivo);

                    
                    toolBar.MostrarMensajeGuardado("Registro almacenado en forma exitosa");
                    vParametro.ImagenParametro = NombreArchivo;
                    
                }
                catch (HttpException hex)
                {
                    toolBar.MostrarMensajeError("No es posible guardar el archivo");
                    return;
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                    return;
                }
            }
            else 
            {
                //if (fuArchivoImagen.Visible && Request.QueryString["oP"] != "E")
                //{
                //    toolBar.MostrarMensajeError("Para este mensaje es necesario seleccionar una imagen.");
                //    return; 
                //}
                //else 
                //{
                //    vParametro.ImagenParametro = null; 
                //}
            }


            if (Request.QueryString["oP"] == "E")
            {
                vParametro.IdParametro = Convert.ToInt32(hfIdParametro.Value);
                vParametro.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vParametro, this.PageName, vSolutionPage);
                vResultado = vSeguridadService.ModificarParametro(vParametro);
            }
            else
            {
                vParametro.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vParametro, this.PageName, vSolutionPage);
                vResultado = vSeguridadService.InsertarParametro(vParametro);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Parametro.IdParametro", vParametro.IdParametro);
                SetSessionParameter("Parametro.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            
            //quita la opcion de guardar en mayusculas
            toolBar.RemoveSaveConfirmation();

            toolBar.EstablecerTitulos("Par&aacute;metro o mensaje", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdParametro = Convert.ToInt32(GetSessionParameter("Parametro.IdParametro"));
            RemoveSessionParameter("Parametro.Id");

            Parametro vParametro = new Parametro();
            vParametro = vSeguridadService.ConsultarParametro(vIdParametro);
            hfIdParametro.Value = vParametro.IdParametro.ToString();
            txtNombreParametro.Text = vParametro.NombreParametro;
            txtValorParametro.Text = vParametro.ValorParametro;
            lblImagenParametro.Visible = (vParametro.ImagenParametro != null);
            fuArchivoImagen.Visible= (vParametro.ImagenParametro != null);
            txtFuncionalidad.Text = vParametro.Funcionalidad;
            
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vParametro.UsuarioCrea, vParametro.FechaCrea, vParametro.UsuarioModifica, vParametro.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
