<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Parametro_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td width="50%">
                    Nombre de Parámetro o mensaje 
                </td>
                <td width="50%">
                    Valor de Parámetro
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombreParametro" MaxLength = "50"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtValorParametro" MaxLength = "50"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Funcionalidad
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtFuncionalidad"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvParametro" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdParametro" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvParametro_PageIndexChanging" OnSelectedIndexChanged="gvParametro_SelectedIndexChanged"
                        OnRowDataBound="gvParametro_RowDataBound" OnSorting="gvParametro_Sorting" OnRowCommand="gvParametro_RowCommand"
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre de parámetro o mensaje" DataField="NombreParametro" SortExpression="NombreParametro" ItemStyle-Width="40%" />
                            <asp:BoundField HeaderText="Funcionalidad" DataField="Funcionalidad" SortExpression="Funcionalidad" ItemStyle-Width="20%" />
                            <asp:BoundField HeaderText="Valor de Parámetro" DataField="ValorParametro" SortExpression="ValorParametro" ItemStyle-Width="40%"  />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnLogo" runat="server" CommandName="viewLogo" CommandArgument='<%#Eval("ImagenParametro")%>'
                                        ImageUrl="~/Image/btn/icoPagBuscar.gif" Height="16px" Width="16px" ToolTip="Ver Logo"
                                        Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
