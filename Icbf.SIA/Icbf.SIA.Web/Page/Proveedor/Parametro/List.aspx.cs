using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Seguridad.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Seguridad.Entity;
using System.IO;
using Icbf.SIA.Service;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de par�metros
/// </summary>
public partial class Page_Parametro_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/Parametro";
    SeguridadService vSeguridadService = new SeguridadService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                SaveState(this.Master, PageName);
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreParametro = null;
            String vValorParametro = null;
            Boolean? vEstado = null;
            String vFuncionalidad = null;
            if (txtNombreParametro.Text != "")
            {
                vNombreParametro = Convert.ToString(txtNombreParametro.Text);
            }
            if (txtValorParametro.Text != "")
            {
                vValorParametro = Convert.ToString(txtValorParametro.Text);
            }
            
            if (txtFuncionalidad.Text != "")
            {
                vFuncionalidad = txtFuncionalidad.Text;
            }

            gvParametro.DataSource = vSeguridadService.ConsultarParametros(vNombreParametro, vValorParametro, vEstado, vFuncionalidad);

            gvParametro.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvParametro.PageSize = PageSize();
            gvParametro.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Par&aacute;metro o mensaje", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvParametro.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Parametro.IdParametro", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvParametro_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvParametro.SelectedRow);
    }

    protected void gvParametro_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvParametro.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Parametro.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Parametro.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvParametro_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Parametro objParametro = (Parametro)e.Row.DataItem;
            if (objParametro.ImagenParametro != null)
            {
                ImageButton objImagenLogo = (ImageButton)e.Row.FindControl("btnLogo");
                objImagenLogo.Visible = true;
            }
        }
    }

    /// <summary>
    /// comando de rengl�n para la grilla Parametro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvParametro_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "viewLogo":
                    if (e.CommandArgument.ToString() != null)
                    {
                        ManejoControles obj = new ManejoControles();
                        obj.OpenWindowPcUpdatePanel(this, "../../../Page/Proveedor/Parametro/MostrarImagen.htm", 200, 200);
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No se logro obtener la imagen desde el servidor.");
                        return;
                    }
                    break;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void gvParametro_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }

    /// <summary>
    /// Ordenamiento para una columna de la grilla
    /// </summary>
    /// <param name="e"></param>
    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNombreParametro = null;
        String vValorParametro = null;
        Boolean? vEstado = null;
        String vFuncionalidad = null;
        if (txtNombreParametro.Text != "")
        {
            vNombreParametro = Convert.ToString(txtNombreParametro.Text);
        }
        if (txtValorParametro.Text != "")
        {
            vValorParametro = Convert.ToString(txtValorParametro.Text);
        }
        
        if (txtFuncionalidad.Text != "")
        {
            vFuncionalidad = txtFuncionalidad.Text;
        }

        //Lleno una lista con los datos que uso para llenar la grilla
        var myGridResults = vSeguridadService.ConsultarParametros(vNombreParametro, vValorParametro, vEstado, vFuncionalidad);

        if (myGridResults != null)
        {
            //Parametro en "Parametro =>..."
            var param = Expression.Parameter(typeof(Parametro), e.SortExpression);

            //La propiedad de mi lista, esto es "Parametro.CualquierCampo"
            var prop = Expression.Property(param, e.SortExpression);

            //Creo en tiempo de ejecuci�n la expresi�n lambda
            var sortExpression = Expression.Lambda<Func<Parametro, object>>(Expression.Convert(prop, typeof(object)), param);

            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvParametro.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                gvParametro.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvParametro.DataBind();
        }
    }

}
