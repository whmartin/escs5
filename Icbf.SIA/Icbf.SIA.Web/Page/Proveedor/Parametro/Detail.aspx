<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Parametro_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdParametro" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Nombre de Parámetro 
            </td>
            <td width="50%">
                Valor de Parámetro 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreParametro"  Enabled="false"  Width="350px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorParametro"  Enabled="false"  Width="350px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Funcionalidad 
            </td>
            <td>
                <asp:label ID="lblImagenParametro" runat="server" Text ="Imagen de Parámetro" Visible="false"  />
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFuncionalidad" Enabled="False" Width="350px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtImagenParametro"  Enabled="false"  Width="350px"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
