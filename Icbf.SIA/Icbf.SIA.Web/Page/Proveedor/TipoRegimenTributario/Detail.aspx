<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoRegimenTributario_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdTipoRegimenTributario" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Codigo Regimen Tributario *
            </td>
            <td>
                Descripción Regimen Tributario *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoRegimenTributario" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Persona *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="true" Width="40%" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
