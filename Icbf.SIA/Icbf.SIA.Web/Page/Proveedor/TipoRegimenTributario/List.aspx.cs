using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tipos de r�gimen tributario
/// </summary>
public partial class Page_TipoRegimenTributario_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/TipoRegimenTributario";
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador del evento click para el bot�n retornar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Proveedor/TablaParametrica/List.aspx");
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigoRegimenTributario = null;
            String vDescripcion = null;
            int? vIdTipoPersona = null;
            Boolean? vEstado = null;
            if (txtCodigoRegimenTributario.Text != "")
            {
                vCodigoRegimenTributario = Convert.ToString(txtCodigoRegimenTributario.Text);
            }
            if (txtDescripcion.Text != "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }
            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = Convert.ToInt32(ddlTipoPersona.SelectedValue);
            }
            if (rblEstado.SelectedValue != "")
            {
                vEstado = Convert.ToBoolean(Convert.ToInt16(rblEstado.SelectedValue));
            }
            gvTipoRegimenTributario.DataSource = vProveedorService.ConsultarTipoRegimenTributarios(vCodigoRegimenTributario, vDescripcion, vIdTipoPersona, vEstado);
            SetSessionParameter("gvTipoRegimenTributario.DataSource", gvTipoRegimenTributario.DataSource);
            gvTipoRegimenTributario.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvTipoRegimenTributario.PageSize = PageSize();
            gvTipoRegimenTributario.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo R&eacute;gimen Tributario", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoRegimenTributario.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoRegimenTributario.IdTipoRegimenTributario", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoRegimenTributario_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoRegimenTributario.SelectedRow);
    }
    protected void gvTipoRegimenTributario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoRegimenTributario.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoRegimenTributario.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoRegimenTributario.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Ordenar Grilla (Revisar si se generaliza)

    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<TipoRegimenTributario> SortList(List<TipoRegimenTributario> data, bool isPageIndexChanging)
    {
        List<TipoRegimenTributario> result = new List<TipoRegimenTributario>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }
    protected void gvTipoRegimenTributario_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvTipoRegimenTributario.PageIndex;

        if (GetSessionParameter("gvTipoRegimenTributario.DataSource") != null)
        {
            if (gvTipoRegimenTributario.DataSource == null) { gvTipoRegimenTributario.DataSource = (List<TipoRegimenTributario>)GetSessionParameter("gvTipoRegimenTributario.DataSource"); }
            gvTipoRegimenTributario.DataSource = SortList((List<TipoRegimenTributario>)gvTipoRegimenTributario.DataSource, false);
            gvTipoRegimenTributario.DataBind();
        }
        gvTipoRegimenTributario.PageIndex = pageIndex;
    }

    #endregion
}
