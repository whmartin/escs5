<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_TipoRegimenTributario_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoRegimenTributario" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Codigo Regimen Tributario *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoRegimenTributario" ControlToValidate="txtCodigoRegimenTributario"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Descripción Regimen Tributario *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoRegimenTributario"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoRegimenTributario" runat="server" TargetControlID="txtCodigoRegimenTributario"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;1234567890" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Persona *
               </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="true" Width="40%">
                </asp:DropDownList>
               </td>
            <td>
               <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
