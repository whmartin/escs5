<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoRegimenTributario_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Codigo Regimen Tributario *
            </td>
            <td>
                Descripción Regimen Tributario *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoRegimenTributario"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Persona *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="true" Width="40%">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  ></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoRegimenTributario" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoRegimenTributario" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoRegimenTributario_PageIndexChanging" 
                        OnSelectedIndexChanged="gvTipoRegimenTributario_SelectedIndexChanged"
                        AllowSorting="true" OnSorting="gvTipoRegimenTributario_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Codigo Regimen Tributario" DataField="CodigoRegimenTributario" SortExpression="CodigoRegimenTributario" />
                            <asp:BoundField HeaderText="Descripción Regimen Tributario" DataField="Descripcion" SortExpression="Descripcion" />
                            <asp:BoundField HeaderText="Tipo Persona" DataField="DescripcionTipoPersona" SortExpression="DescripcionTipoPersona"  />
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado" >
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# string.Format("{0}", (bool)Eval("Estado") ? "Activo" : "Inactivo") %>'></asp:Label>
                                </ItemTemplate>                                
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
