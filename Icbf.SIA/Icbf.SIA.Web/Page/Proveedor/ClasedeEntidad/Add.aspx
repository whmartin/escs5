<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ClasedeEntidad_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdClasedeEntidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Codigo *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigo" ControlToValidate="txtCodigo"
                 SetFocusOnError="true" ErrorMessage="El código no puede estar en blanco" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Descripción Clase de Entidad *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="La descripción no puede estar en blanco" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            
        </tr>    
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigo" MaxLength = "10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigo" runat="server" TargetControlID="txtCodigo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;1234567890" />                
            </td>

            <td>
            <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;1234567890" />
                 
            </td>
        </tr>        
        <tr class="rowB">
            <td>
                Tipo de Actividad *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipodeActividad" ControlToValidate="ddlTipoActividad"
                 SetFocusOnError="true" ErrorMessage="Debe seleccionar un tipo de actividad" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>            
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Debe seleccionar un estado" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlTipoActividad" runat="server" Width="40%">
                    </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            
        </tr>
        <tr class="rowA">
            <td colspan="2">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
