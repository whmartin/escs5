<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ClasedeEntidad_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdClasedeEntidad" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Tipo de Actividad *
            </td>
            <td>
                Descripción Clase de Entidad *
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:DropDownList ID="ddlTipoActividad" runat="server" Width="40%">
                    </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
