<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_EstadoValidacionDocumental_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdEstadoValidacionDocumental" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Codigo Estado Validacion Documental *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoEstadoValidacionDocumental" ControlToValidate="txtCodigoEstadoValidacionDocumental"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Descripcion *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcion" ControlToValidate="txtDescripcion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoEstadoValidacionDocumental"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoEstadoValidacionDocumental" runat="server" TargetControlID="txtCodigoEstadoValidacionDocumental"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
