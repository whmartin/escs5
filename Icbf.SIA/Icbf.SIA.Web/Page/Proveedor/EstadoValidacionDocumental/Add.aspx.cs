using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;

/// <summary>
/// Página de registro y edición de estados de validación documental
/// </summary>
public partial class Page_EstadoValidacionDocumental_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Proveedor/EstadoValidacionDocumental";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            EstadoValidacionDocumental vEstadoValidacionDocumental = new EstadoValidacionDocumental();

            vEstadoValidacionDocumental.CodigoEstadoValidacionDocumental = Convert.ToString(txtCodigoEstadoValidacionDocumental.Text).Trim();
            vEstadoValidacionDocumental.Descripcion = Convert.ToString(txtDescripcion.Text).Trim();
            vEstadoValidacionDocumental.Estado = Convert.ToBoolean(rblEstado.SelectedValue);
            

            if (Request.QueryString["oP"] == "E")
            {
            vEstadoValidacionDocumental.IdEstadoValidacionDocumental = Convert.ToInt32(hfIdEstadoValidacionDocumental.Value);
                vEstadoValidacionDocumental.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vEstadoValidacionDocumental, this.PageName, vSolutionPage);
                vResultado = vProveedorService.ModificarEstadoValidacionDocumental(vEstadoValidacionDocumental);
            }
            else
            {
                vEstadoValidacionDocumental.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vEstadoValidacionDocumental, this.PageName, vSolutionPage);
                vResultado = vProveedorService.InsertarEstadoValidacionDocumental(vEstadoValidacionDocumental);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("EstadoValidacionDocumental.IdEstadoValidacionDocumental", vEstadoValidacionDocumental.IdEstadoValidacionDocumental);
                SetSessionParameter("EstadoValidacionDocumental.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Estado Validaci&oacute;n Documental", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdEstadoValidacionDocumental = Convert.ToInt32(GetSessionParameter("EstadoValidacionDocumental.IdEstadoValidacionDocumental"));
            RemoveSessionParameter("EstadoValidacionDocumental.Id");

            EstadoValidacionDocumental vEstadoValidacionDocumental = new EstadoValidacionDocumental();
            vEstadoValidacionDocumental = vProveedorService.ConsultarEstadoValidacionDocumental(vIdEstadoValidacionDocumental);
            hfIdEstadoValidacionDocumental.Value = vEstadoValidacionDocumental.IdEstadoValidacionDocumental.ToString();
            txtCodigoEstadoValidacionDocumental.Text = vEstadoValidacionDocumental.CodigoEstadoValidacionDocumental;
            txtDescripcion.Text = vEstadoValidacionDocumental.Descripcion;
            rblEstado.SelectedValue = vEstadoValidacionDocumental.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vEstadoValidacionDocumental.UsuarioCrea, vEstadoValidacionDocumental.FechaCrea, vEstadoValidacionDocumental.UsuarioModifica, vEstadoValidacionDocumental.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
