using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Entity;
using System.Reflection;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de estado de validaci�n documental
/// </summary>
public partial class Page_EstadoValidacionDocumental_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Proveedor/EstadoValidacionDocumental";
    ProveedorService vProveedorService = new ProveedorService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                SaveState(this.Master, PageName);
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Proveedor/TablaParametrica/List.aspx");
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigoEstadoValidacionDocumental = null;
            String vDescripcion = null;
            Boolean? vEstado = null;
            if (txtCodigoEstadoValidacionDocumental.Text!= "")
            {
                vCodigoEstadoValidacionDocumental = Convert.ToString(txtCodigoEstadoValidacionDocumental.Text);
            }
            if (txtDescripcion.Text!= "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }
            if (rblEstado.SelectedIndex != -1)
            {
                vEstado = Convert.ToBoolean(rblEstado.SelectedValue);
            }
            gvEstadoValidacionDocumental.DataSource = vProveedorService.ConsultarEstadoValidacionDocumentals( vCodigoEstadoValidacionDocumental, vDescripcion, vEstado);
            SetSessionParameter("gvEstadoValidacionDocumental.DataSource", gvEstadoValidacionDocumental.DataSource);
            gvEstadoValidacionDocumental.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvEstadoValidacionDocumental.PageSize = PageSize();
            gvEstadoValidacionDocumental.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Estado Validaci&oacute;n Documental", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvEstadoValidacionDocumental.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("EstadoValidacionDocumental.IdEstadoValidacionDocumental", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvEstadoValidacionDocumental_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvEstadoValidacionDocumental.SelectedRow);
    }
    protected void gvEstadoValidacionDocumental_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEstadoValidacionDocumental.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("EstadoValidacionDocumental.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("EstadoValidacionDocumental.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
            rblEstado.SelectedIndex  = -1;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Ordenar Grilla (Revisar si se generaliza)

    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<EstadoValidacionDocumental> SortList(List<EstadoValidacionDocumental> data, bool isPageIndexChanging)
    {
        List<EstadoValidacionDocumental> result = new List<EstadoValidacionDocumental>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }
    protected void gvEstadoValidacionDocumental_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvEstadoValidacionDocumental.PageIndex;

        if (GetSessionParameter("gvEstadoValidacionDocumental.DataSource") != null)
        {
            if (gvEstadoValidacionDocumental.DataSource == null) { gvEstadoValidacionDocumental.DataSource = (List<EstadoValidacionDocumental>)GetSessionParameter("gvEstadoValidacionDocumental.DataSource"); }
            gvEstadoValidacionDocumental.DataSource = SortList((List<EstadoValidacionDocumental>)gvEstadoValidacionDocumental.DataSource, false);
            gvEstadoValidacionDocumental.DataBind();
        }
        gvEstadoValidacionDocumental.PageIndex = pageIndex;
    }

    #endregion
}
