﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorLog.aspx.cs" Inherits="Page_Administracion_ErrorLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <link href="../../Styles/layout-default.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Oferentes.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: auto; padding: 10px">
        <table class="style1">
            <tr>
                <td colspan="2" style="background-color: #656646" align="center">
                    <br />
                    <h2>
                        Log de Errores
                    </h2>
                    <br />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    Estado del error:
                </td>
                <td align="left">
                    <asp:CheckBox ID="CheckBox1" runat="server" Text="Solo Solucionados" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    Seleccione un&nbsp; usuario:
                </td>
                <td align="left">
                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                        DataSourceID="SqlDataSourceUsuarios" DataTextField="LoweredUserName" DataValueField="LoweredUserName">
                        <asp:ListItem Value="" Text="Seleccione>>"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceUsuarios" runat="server" ConnectionString="<%$ ConnectionStrings:QoS %>"
                        SelectCommand="SELECT DISTINCT [LoweredUserName] FROM [aspnet_Users] au inner join aspnet_Applications aa on au.ApplicationId = aa.ApplicationId WHERE aa.ApplicationName = 'RUBO' Order by 1">
                    </asp:SqlDataSource>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;
                </td>
                <td align="left">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="EventId"
                        DataSourceID="SqlDataSourceErrors" AllowPaging="True" AllowSorting="True" PageSize="8">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:BoundField DataField="EventId" HeaderText="EventId" ReadOnly="True" SortExpression="EventId" />
                            <asp:BoundField DataField="EventTime" HeaderText="EventTime" SortExpression="EventTime" />
                            <asp:BoundField DataField="EventType" HeaderText="EventType" SortExpression="EventType" />
                            <asp:BoundField DataField="EventCode" HeaderText="EventCode" SortExpression="EventCode" />
                            <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
                            <asp:BoundField DataField="ExceptionType" HeaderText="ExceptionType" SortExpression="ExceptionType" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSourceErrors" runat="server" ConnectionString="<%$ ConnectionStrings:QoS %>"
                        SelectCommand="aspnet_WebEvent_HealMonitoring_Load_ByUser" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList1" DefaultValue="NULL" Name="User" PropertyName="SelectedValue"
                                Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSourceDetalleError"
                        Height="50px" Width="125px" DataKeyNames="EventId">
                        <Fields>
                            <asp:BoundField DataField="EventId" HeaderText="EventId" SortExpression="EventId"
                                ReadOnly="True" />
                            <asp:BoundField DataField="RequestUrl" HeaderText="RequestUrl" SortExpression="RequestUrl" />
                            <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
                            <asp:CheckBoxField DataField="Solucionado" HeaderText="Solucionado" SortExpression="Solucionado" />
                            <asp:BoundField DataField="SolutionDetails" HeaderText="SolutionDetails" SortExpression="SolutionDetails" />
                            <asp:CommandField ShowEditButton="True" />
                        </Fields>
                    </asp:DetailsView>
                    <asp:SqlDataSource ID="SqlDataSourceDetalleError" runat="server" ConnectionString="<%$ ConnectionStrings:QoS %>"
                        SelectCommand=" SELECT EventId,RequestUrl,Details FROM aspnet_WebEvent_Events WHERE (EventId = @EventId)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GridView1" DefaultValue="NULL" Name="EventId" PropertyName="SelectedValue"
                                Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
