﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_PreContractual_Solicitudes_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">

            function ConfirmarAccion(tipo) {

                var mensaje;

                if (tipo == 'Aprobar') 
                    mensaje = 'Esta seguro de que desea aprobar o Devolver la solicitud del contrato?';
                else if (tipo == 'Rechazar')
                    mensaje = 'Esta seguro de que desea rechazar la solicitud del contrato?';
                else if (tipo == 'Enviar')
                    mensaje = 'Esta seguro de que desea Enviar a validar la solicitud del contrato?';

                if (confirm(mensaje)) {
                    muestraImagenLoading();
                    var vIdSolicitudContrato = document.getElementById('<%= hfIdSolicitudContrato.ClientID %>').value;
                    var vIdOrigen = document.getElementById('<%= hfOrigen.ClientID %>').value;

                    var url = '../../../Page/PreContractual/Lupas/LupaAprobar.aspx?idSolicitudContrato=' + vIdSolicitudContrato + '&origen=' + vIdOrigen + '&tipo=' + tipo;
                    window_showModalDialog(url, setReturnCambioEstato, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                }

                return false;
            }


            function setReturnCambioEstato() {
                prePostbck(false);
                __doPostBack('ObtenerCambioEstado', '');
            }


            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }


            function ObtenerCDP() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaCDP.aspx?idContrato=' + vIdContrato;
                window_showModalDialog(url,ReturnCallBack,'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function ObtenerHistoricoSolicitud() {
                muestraImagenLoading();
                var vIdSolicitud = document.getElementById('<%= hfIdSolicitudContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaHistoricoSolicitud.aspx?idSolicitudContrato=' + vIdSolicitud;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function ObtenerVigenciasFuturas() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaVigenciasFuturas.aspx?idContrato=' + vIdContrato;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:400px;resizable:yes;');
            }

            function ObtenerLugarEjecucion() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaLugarEjecucion.aspx?idContrato=' + vIdContrato;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:400px;resizable:yes;');
            }

            function ObtenerPlanCompras() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaPlanCompras.aspx?idContrato=' + vIdContrato;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:400px;resizable:yes;');
            }

            function ObtenerAportesContrato() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaAportes.aspx?idContrato=' + vIdContrato;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:400px;resizable:yes;');
            }

            function ObtenerDocumentos() {
                muestraImagenLoading();
                var vIdSolicitud = document.getElementById('<%= hfIdSolicitudContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaDocumentos.aspx?idSolicitudContrato=' + vIdSolicitud;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:400px;resizable:yes;');
            }

            function ObtenerDocumentos() {
                muestraImagenLoading();
                var vIdSolicitud = document.getElementById('<%= hfIdSolicitudContrato.ClientID %>').value;
                var url = '../../../Page/PreContractual/LupasConsulta/LupaDocumentos.aspx?idSolicitudContrato=' + vIdSolicitud;
                window_showModalDialog(url, ReturnCallBack, 'dialogWidth:1250px;dialogHeight:400px;resizable:yes;');
            }

            function ReturnCallBack() {
                ocultaImagenLoading();
            }


        </script>

                    <table width="90%"  id="pnInfoUsuario" align="center">
                    <tr class="rowB">
                        <td>
                            Usuario Creador de la Solicitud
                        </td>
                        <td>
                            Regional de la Solicitud                                                     
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNombreUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtRegionalUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>                 
                    </table>
                    <table width="90%" id="pnInfoSolicitud" runat="server" align="center" style="display:none">
                    <tr class="rowB">
                        <td>
                            Número de Solicitud
                        </td>
                        <td>
                            Fecha de Registro en el Sistema                                                 
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtFechaRegistroSsistema" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="90%" id="pnInfoCategoriaContrato" align="center">
                    <tr class="rowB">
                        <td class="Cell">
                            Contrato Asociado
                        </td>
                        <td class="Cell">
                                Modalidad de Selecci&oacute;n 
                        </td>
                    </tr>
                    <tr class="rowA">
                    <td class="Cell">
                            <asp:CheckBox ID="chkConvenioMarco" runat="server" Text="Convenio Marco" Enabled="false" /><br />
                            <asp:CheckBox ID="chkContratoConvenioAd" runat="server" Text="Contrato/Convenio Adhesi&oacute;n" Enabled="false"  />
                        </td>
                        <td class="Cell">
                            <asp:DropDownList ID="ddlModalidadSeleccion" Enabled="false" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="Cell">
                        Categoría Contrato/Convenio                                                 
                    </td>
                        <td class="Cell">
                            Tipo de Contrato/Convenio 
                        </td>
                    </tr>
                    <tr class="rowA">
                            <td class="Cell">
                            <asp:DropDownList ID="ddlCategoriaContrato" Enabled="false" runat="server" >
                            </asp:DropDownList>
                            </td>
                            <td class="Cell">
                            <asp:DropDownList ID="ddlTipoContratoConvenio" Enabled="false" runat="server" />
                        </td>
                    </tr>
                    </table>
                    <table  width="90%" id="PnPrestacionServicios"  runat="server" style="display:none" align="center">
                        <tr class="rowB">
                                            <td class="Cell">
                                                Modalidad Académica 

                                            </td>
                                            <td class="Cell">
                                                Nombre de la Profesión 

                                            </td>
                                        </tr>
                        <tr class="rowA" >
                                            <td class="Cell">
                                                <asp:DropDownList ID="ddlModalidadAcademica" runat="server" Enabled="false" />
                                            </td>
                                            <td class="Cell">
                                                <asp:DropDownList ID="ddlNombreProfesion" runat="server" Enabled="false" />
                                            </td>
                                        </tr>
                    </table>
                    <table width="90%" id="PnInfoTipoContrato" align="center">
                        <tr class="rowB">
                            <td class="Cell">
                                Requiere Garant&iacute;a 
                            </td>
                            <td class="Cell">
                                Requiere Acta de Inicio 
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                    <asp:RadioButtonList runat="server" ID="rblRequiereGarantia" Enabled="false" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Si" Value="Si" />
                                        <asp:ListItem Text="No" Value="No" />
                                    </asp:RadioButtonList>  
                            </td>
                            <td class="Cell">
                                    <asp:RadioButtonList runat="server" ID="rblRequiereActaInicio" Enabled="false" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Si" Value="Si" />
                                        <asp:ListItem Text="No" Value="No" />
                                    </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                Maneja Recursos ICBF 
                            </td>
                            <td class="Cell">
                                Maneja Cofinanciaci&oacute;n 
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td>
                                    <asp:RadioButtonList runat="server" ID="rblManejaRecursosICBF" Enabled="false" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Si" Value="Si" />
                                        <asp:ListItem Text="No" Value="No" />
                                    </asp:RadioButtonList>  
                            </td>
                            <td class="Cell">
                                    <asp:RadioButtonList runat="server" ID="rblManejaCofinanciacion" Enabled="false" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Si" Value="Si" />
                                        <asp:ListItem Text="No" Value="No" />
                                    </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                Maneja Aportes en Especie ICBF 
                            </td>
                            <td class="Cell">
                                Requiere Vigencias Futuras 
                                <asp:ImageButton ID="imgVigenciasFuturas" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                OnClientClick="ObtenerVigenciasFuturas(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                    <asp:RadioButtonList runat="server" ID="rblManejaAportesEspecie" Enabled="false" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Si" Value="Si" />
                                        <asp:ListItem Text="No" Value="No" />
                                    </asp:RadioButtonList>  
                            </td>
                            <td class="Cell">
                                    <asp:RadioButtonList runat="server" ID="rblManejaVigenciasFuturas" Enabled="false" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Si" Value="Si" />
                                        <asp:ListItem Text="No" Value="No" />
                                    </asp:RadioButtonList>  
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td class="Cell">
                                Vigencia Incial del Contrato/Convenio *
                            </td>
                            <td class="Cell">
                               Vigencia Final del Contrato/Convenio *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                    <asp:TextBox ID="TxtFechaInicio" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                            <td class="Cell">
                                    <asp:TextBox ID="TxtFechaFin" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                        </tr>  
                    </table>
                    <table width="90%"  id="pnInfoSolcitantes" runat="server" align="center">
                        <tr class="rowB">
                            <td>
                                Nombre del Solicitante 
                            </td>
                            <td>
                                Regional del Contrato/Convenio 
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox ID="txtNombreSolicitante" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtRegionalContratoConvenio" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                Dependencia Solicitante
                            </td>
                            <td>
                                Cargo el Solicitante
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                Nombre del Ordenador del Gasto 
                            </td>
                            <td>
                                Regional Ordenador del Gasto
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox ID="txtNombreOrdenadorGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtRegionalOrdenadorGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                N&uacute;mero Identificaci&oacute;n Ordenador del Gasto
                            </td>
                            <td>
                                Cargo Ordenador del Gasto
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox ID="txtNumeroIdentOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtCargoOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                               <tr class="rowB">
                                        <td>
                                            Objeto del Contrato/Convenio 
                                        </td>
                                        <td>
                                            Alcance del Objeto del Contrato/Convenio 
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtObjeto" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                 AutoPostBack="true" 
                                                MaxLength="500" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtAlcance" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                MaxLength="4000" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);"></asp:TextBox>
                                        </td>
                                    </tr>
                               <tr class="rowB">
                            <td>
                               Valor del Contrato
                            </td>
                            <td>
                                                               Número de Radicado 
                                
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox ID="txtValorContrato" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                                                <asp:TextBox ID="txtNumeroRadicado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                         <tr class="rowB">
                            <td>
                               Historico de Solicitud
                                <asp:ImageButton ID="imgHistoricoSolicitud" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                OnClientClick="ObtenerHistoricoSolicitud(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                            <td>
                                Plan de Compras
                                <asp:ImageButton ID="imgePlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                OnClientClick="ObtenerPlanCompras(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                        </tr>
                         <tr class="rowA">
                            <td>
                               <b> Lugares de Ejecución </b>
                                <asp:ImageButton ID="imgLugarEjecucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                OnClientClick="ObtenerLugarEjecucion(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                            <td>
                               <b> Información CDP </b>
                              <asp:ImageButton ID="ImageButton1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                              OnClientClick="ObtenerCDP(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                        </tr>
                          <tr class="rowA">
                            <td>
                                 <b>Aportes Contrato</b>
                                <asp:ImageButton ID="imgAportesContrato" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                OnClientClick="ObtenerAportesContrato(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                            <td>
                               <b>Documentos Contrato</b>
                              <asp:ImageButton ID="ImageButton3" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                              OnClientClick="ObtenerDocumentos(); return false;" Style="cursor: hand" ToolTip="Ver Detalle" />
                            </td>
                        </tr>
                    </table>


                <asp:HiddenField ID="hfIdSolicitudContrato" runat="server" />
                    <asp:HiddenField ID="hfIdContrato" runat="server" />
                <asp:HiddenField ID="hfIdRegionalUsuarioCreacion" runat="server" />
                <asp:HiddenField ID="hfRegionalContConv" runat="server" />
                <asp:HiddenField ID="hfCodRegionalContConv" runat="server" />
                <asp:HiddenField ID="hfEsPrestacionServicios" runat="server" />

                <asp:HiddenField ID="hfNombreSolicitante" runat="server" />
                <asp:HiddenField ID="hfDependenciaSolicitante" runat="server" />
                <asp:HiddenField ID="hfCargoSolicitante" runat="server" />

                <asp:HiddenField ID="hfTipoIdentOrdenadorGasto" runat="server" />
                <asp:HiddenField ID="hfNombreOrdenadorGasto" runat="server" />
                <asp:HiddenField ID="hfNumeroIdentOrdenadoGasto" runat="server" />
                <asp:HiddenField ID="hfCargoOrdenadoGasto" runat="server" />

                <asp:HiddenField ID="hfOrigen" runat="server" />

        </table>

        </table>

</asp:Content>
