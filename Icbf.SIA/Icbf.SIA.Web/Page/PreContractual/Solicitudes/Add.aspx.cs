﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;

/// <summary>
/// Página que despliega el detalle del registro de una solicitud de contrato
/// </summary>
public partial class Page_PreContractual_Solicitudes_Add : GeneralWeb
{
    #region Constantes

    masterPrincipal toolBar;

    string PageName = "Precontractual/Solicitudes";


    //Contrato Asociado
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    //Categoria contrato
    private const string CodCategoriaContrato = "CTR"; //"Contrato";
    private const string CodCategoriaConvenio = "CVN"; //"Convenio";

    //Tipos contrato
    private const string CotTipoContConvPrestServApoyoGestion = "PREST_SERV_APO_GEST"; //"Prestación de Servicios de Apoyo a la gestión";
    private const string CodTipoContConvPrestServProfesionales = "PREST_SERV_PROF"; //"Prestación de Servicios Profesionales";
    private const string CodTipoContAporte = "APORTE"; //"Aporte";
    private const string CodMarcoInteradministrativo = "MC_INT_ADMIN"; //"Marco Interadministrativo";

    //Modalidad selección
    private const string CodContratacionDirecta = "CONT_DIR"; //"Contratación Directa";
    private const string CodContratacionDirectaAporte = "CDA"; //"Contratación Directa Aporte";
    #endregion

    #region Fachadas

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    SIAService vSIAService = new SIAService();

    #endregion

    #region Negocio y Acceso a Datos

    /// <summary>
    /// 
    /// </summary>
    private void Guardar()
    {
        try
        {
            int result = 0;

            SolicitudContrato solContrato = new SolicitudContrato();
            solContrato.IdModalidadSeleccion = int.Parse(ddlModalidadSeleccion.SelectedValue);
            solContrato.EsContratoMarco = chkConvenioMarco.Checked;
            solContrato.EsContratoConvenioAdhesion = chkContratoConvenioAd.Checked;
            solContrato.IdCategoriaContrato = int.Parse(ddlCategoriaContrato.SelectedValue);
            solContrato.IdTipoContrato = int.Parse(ddlTipoContratoConvenio.SelectedValue);

            if (hfEsPrestacionServicios.Value == "1")
	        {
                solContrato.IdModalidadAcademica = ddlModalidadAcademica.SelectedValue;
                solContrato.IdProfesion = ddlNombreProfesion.SelectedValue; 
	        }

            if (!string.IsNullOrEmpty(hfCodRegionalContConv.Value))
            {
                string codigo = hfCodRegionalContConv.Value.Length == 1 ? "0" + hfCodRegionalContConv.Value : hfCodRegionalContConv.Value;

                var idRegionalContrato = vSIAService.ConsultarRegionals(codigo, null);
                solContrato.IdRegional = idRegionalContrato.First().IdRegional;
            }

            solContrato.RequiereGarantia = rblRequiereGarantia.SelectedValue == "Si"? true:false;
            solContrato.ManejaCofinanciacion = rblManejaCofinanciacion.SelectedValue == "Si" ? true : false;
            solContrato.RequiereActaInicio = rblRequiereActaInicio.SelectedValue == "Si" ? true : false;
            solContrato.ManejaRecursosEspecieICBF = rblManejaAportesEspecie.SelectedValue == "Si" ? true : false;
            solContrato.ManejaRecursosICBF = rblManejaRecursosICBF.SelectedValue == "Si" ? true : false;
            solContrato.ManejaVigenciasFuturas = rblManejaVigenciasFuturas.SelectedValue == "Si" ? true : false;

            solContrato.FechaInicioContrato = null;
            solContrato.FechaFinalizaContrato = null;
            solContrato.IdVigenciaInicial = int.Parse(ddlVigenciaFiscalIni.SelectedValue);
            solContrato.IdVigenciaFinal = int.Parse(ddlVigenciaFiscalFin.SelectedValue);
 
            if (Request.QueryString["oP"] == "E")
            {
                solContrato.IdSolicitud = int.Parse(hfIdSolicitudContrato.Value);
                solContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                result = vPrecontractualService.ModificarSolicitudContrato(solContrato);
            }
            else
            {
                solContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                result = vPrecontractualService.InsertarSolicitudContrato(solContrato);
            }

            if (result > 0)
            {
                SetSessionParameter("SolicitudContrato.IdSolicitud", solContrato.IdSolicitud);
                SetSessionParameter("SolicitudContrato.Guardo", "1");

                if (Request.QueryString["oP"] == "E")
                    NavigateTo(SolutionPage.Detail);
                else
                    NavigateTo(SolutionPage.Edit);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            if (Request.QueryString["oP"] == "E")
                toolBar.SetSaveConfirmation("return Confirmar();");
            toolBar.EstablecerTitulos("Registro de Solicitudes de Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            txtNombreUsuario.Text = GetSessionUser().NombreUsuario;

            if (GetSessionUser().IdTipoUsuario == 1)
                txtRegionalUsuario.Text = "Sede Nacional";
            else
            {
                var miRegional = vSIAService.ConsultarRegional(GetSessionUser().IdUsuario);

                if (miRegional != null)
                    txtRegionalUsuario.Text = miRegional.NombreRegional;
            }
            
            CargarCategoriaContrato();
            CargarTipodeContrato();
            CargarModalidadSeleccion();
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalIni, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalFin, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (Request.QueryString["oP"] == "E")
            {
                pnInfoSolicitud.Style.Add("display", "");
                pnInfoSolcitantes.Style.Add("display","");
                AccordionPanelPlanCompras.Visible = true;
                AccordionPanelLugarEjecucion.Visible = true;
                ApContratista.Visible = true;
                AccordionPanelAportes.Visible = true;
                AccordionPaneDocumentos.Visible = true;
                var idSolicitudContrato = GetSessionParameter("SolicitudContrato.IdSolicitud").ToString();
                hfIdSolicitudContrato.Value = idSolicitudContrato.ToString();

                if (GetSessionParameter("SolicitudContrato.Guardo").ToString() == "1")
                {
                    toolBar.MostrarMensajeGuardado("Se creo la solicitud de contrato con Id" + hfIdSolicitudContrato.Value);
                    RemoveSessionParameter("SolicitudContrato.Guardo");
                }

                var itemSolicitud = vPrecontractualService.ConsultarSolicitudPorId(int.Parse(idSolicitudContrato));
                hfIdContrato.Value = itemSolicitud.IdContrato.ToString();
                txtFechaRegistroSsistema.Text = itemSolicitud.FechaCrea.ToShortDateString();
                txtNumeroSolicitud.Text = itemSolicitud.IdSolicitud.ToString();
                txtNombreUsuario.Text = itemSolicitud.UsuarioCrea.ToString();
                chkConvenioMarco.Checked = itemSolicitud.EsContratoMarco.HasValue? itemSolicitud.EsContratoMarco.Value : false;
                chkContratoConvenioAd.Checked = itemSolicitud.EsContratoConvenioAdhesion.HasValue ? itemSolicitud.EsContratoConvenioAdhesion.Value : false;
                ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
                ddlCategoriaContrato.SelectedValue = itemSolicitud.IdCategoriaContrato.ToString();
                hfRegionalContConv.Value = itemSolicitud.IdRegional.ToString();
                CargarTipodeContrato();
                ddlTipoContratoConvenio.SelectedValue = itemSolicitud.IdTipoContrato.ToString();

                if (itemSolicitud.CodigoTipoContrato == CotTipoContConvPrestServApoyoGestion || itemSolicitud.CodigoTipoContrato == CodTipoContConvPrestServProfesionales  )
                {
                    ActualizarTipoContratoConvenio(true);
                    cargarModalidadAcademica();
                    ddlModalidadAcademica.SelectedValue = itemSolicitud.IdModalidadAcademica.ToString();
                    cargarProfesion();
                    ddlNombreProfesion.SelectedValue = itemSolicitud.IdProfesion.ToString();
                }

                rblManejaAportesEspecie.SelectedValue = itemSolicitud.ManejaRecursosEspecieICBF == true ? "Si" : "No";
                
                rblManejaCofinanciacion.SelectedValue = itemSolicitud.ManejaCofinanciacion == true ? "Si" : "No";

                if (itemSolicitud.ManejaCofinanciacion)
                    imgValorApoContrat.Style.Add("display", "");

                rblManejaRecursosICBF.SelectedValue = itemSolicitud.ManejaRecursosICBF == true ? "Si" : "No";

                if (itemSolicitud.ManejaRecursosEspecieICBF)
                    imgValorApoICBF.Style.Add("display","");

                rblManejaVigenciasFuturas.SelectedValue = itemSolicitud.ManejaVigenciasFuturas == true ? "Si" : "No";

                if(itemSolicitud.ManejaVigenciasFuturas)
                {
                    ApVigenciasFuturas.Visible = true;
                    CargarVigenciasFuturas();
                }

                rblRequiereActaInicio.SelectedValue = itemSolicitud.RequiereActaInicio == true ? "Si" : "No";
                rblRequiereGarantia.SelectedValue = itemSolicitud.RequiereGarantia == true ? "Si" : "No";

                ddlVigenciaFiscalIni.SelectedValue = itemSolicitud.IdVigenciaInicial.ToString();
                hfVigenciaInicial.Value = itemSolicitud.IdVigenciaInicial.ToString();
                ddlVigenciaFiscalFin.SelectedValue = itemSolicitud.IdVigenciaFinal.ToString();
                hfVigenciaFinal.Value = itemSolicitud.IdVigenciaFinal.ToString();

                CargarInfoEmpleados(itemSolicitud);

                if(itemSolicitud.IdPlanCompras.HasValue)
                   hfPlanCompras.Value = itemSolicitud.IdPlanCompras.Value.ToString();
                
                CargarInfoPlanCompras(false);

                txtAlcance.Text = itemSolicitud.Alcance;
                txtObjeto.Text = itemSolicitud.Objeto;
                if(itemSolicitud.ValorContrato.HasValue)
                txtValorInicialContConv.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContrato.Value);

                gvAportesICBF.PageSize = PageSize();
                gvAportesICBF.EmptyDataText = EmptyDataText();

                gvLugaresEjecucion.PageSize = PageSize();
                gvLugaresEjecucion.EmptyDataText = EmptyDataText();

                CargarContratistas();
                CargarAportes();
                CargarLugarEjecucion();
                CargarDocumentos();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarCategoriaContrato()
    {
        ddlCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, true);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
        }
        ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTipodeContrato()
    {
        string idCategoriaSeleccionada = ddlCategoriaContrato.SelectedValue;
        PnPrestacionServicios.Style.Add("display", "none");

        if (idCategoriaSeleccionada != "-1")
        {
            ddlTipoContratoConvenio.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), true, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void VerificarTipoContratoConvenio()
    {
        string idTipoContratoConvenio = ddlTipoContratoConvenio.SelectedValue;
        string idCategoriaContrato = ddlCategoriaContrato.SelectedValue;

        if (idTipoContratoConvenio != "-1" && idCategoriaContrato != "-1")
        {
            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContratoConvenio);
            vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
            vTipoContrato = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato);

            if (vTipoContrato.CodigoTipoContrato == CodTipoContConvPrestServProfesionales || vTipoContrato.CodigoTipoContrato == CotTipoContConvPrestServApoyoGestion)
            {
                ActualizarTipoContratoConvenio(true);
                cargarModalidadAcademica();
                cargarProfesion();
            }
            else
                ActualizarTipoContratoConvenio(false);
        }
        else
            ActualizarTipoContratoConvenio(false);
    }

    /// <summary>
    /// 
    /// </summary>
    private void ActualizarTipoContratoConvenio(bool valido)
    {
        if (valido)
        {
            PnPrestacionServicios.Style.Add("display", "");
            hfEsPrestacionServicios.Value = "1";
        }
        else
        {
            PnPrestacionServicios.Style.Add("display", "none");
            hfEsPrestacionServicios.Value = "0";
        }
        cvModalidadAcademica.Enabled = valido;
        cvNombreProfesion.Enabled = valido;
    }

    /// <summary>
    /// 
    /// </summary>
    private void cargarModalidadAcademica()
    {
        ddlModalidadAcademica.Items.Clear();
        List<ModalidadAcademicaKactus> vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, null, null);
        foreach (ModalidadAcademicaKactus tD in vLModalidadesAcademicas)
            ddlModalidadAcademica.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
        
        ddlModalidadAcademica.Items.Insert(0, new ListItem("Seleccionar", "-1"));

        ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void cargarProfesion()
    {
        var idModalidadAcademicaSeleccionada = ddlModalidadAcademica.SelectedValue;

        if (idModalidadAcademicaSeleccionada != "-1")
        {
            ddlNombreProfesion.Items.Clear();
            List<ProfesionKactus> vLTiposContratos = vContratoService.ConsultarProfesionesKactus(idModalidadAcademicaSeleccionada, null, null, null);
            foreach (ProfesionKactus tD in vLTiposContratos)
                ddlNombreProfesion.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
            
            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlNombreProfesion.Items.Clear();
            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlNombreProfesion.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarInfoEmpleados(SolicitudContrato item)
    {
        if (item.IdentificacionSolicitante != null)
        {
            hfCodRegionalContConv.Value = item.IdRegionalSolicitante.ToString();

            txtNombreSolicitante.Text = item.NombreSolicitante;
            txtRegionalContratoConvenio.Text = item.RegionalSolicitante;
            txtDependenciaSolicitante.Text = item.DependenciaSolicitante;
            txtCargoSolicitante.Text = item.CargoSolicitante;
        }

        if (item.IdentificacionOrdenadorGasto != null)
        {
            txtNombreOrdenadorGasto.Text = item.NombreOrdenadorGasto;
            txtRegionalOrdenadorGasto.Text = item.RegionalOrdenadorGasto;
            txtNumeroIdentOrdenadoGasto.Text = item.IdentificacionOrdenadorGasto.ToString();
            txtCargoOrdenadoGasto.Text = item.CargoOrdenadorGasto;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarInfoPlanCompras(bool esCreacion)
    {
        if (!string.IsNullOrEmpty(GetSessionParameter("DetallePlanComprasProductos.Guardado").ToString()))
        {
            hfPlanCompras.Value = GetSessionParameter("DetallePlanComprasProductos.Guardado").ToString();
            RemoveSessionParameter("DetallePlanComprasProductos.Guardado");
        }
        else if (!string.IsNullOrEmpty(GetSessionParameter("DetallePlanComprasProductos.Elimino").ToString()))
        {
            hfPlanCompras.Value = string.Empty;
            RemoveSessionParameter("DetallePlanComprasProductos.Elimino");
        }

        if (! string.IsNullOrEmpty(hfPlanCompras.Value))
        {
            if(rblManejaRecursosICBF.SelectedValue == "Si")
            {
                CargarCDP();
                ApCDP.Visible = true;
            }
            else
                ApCDP.Visible = false;

            int idPlanCompras = int.Parse(hfPlanCompras.Value);
           
           var planCompras = vContratoService.ConsultarPlanComprasContratoss(null,int.Parse(hfIdContrato.Value),null,null);
           txtPlanCompras.Text = planCompras[0].NumeroConsecutivoPlanCompras.ToString();
           txtValorInicialContConv.Text = string.Format("{0:$#,##0}", planCompras[0].Valor);

           if (esCreacion)
           {
               SolicitudContrato solicitud = vPrecontractualService.ConsultarSolicitudPorId(int.Parse(hfIdSolicitudContrato.Value));
                txtObjeto.Text = solicitud.Objeto;
                txtAlcance.Text = solicitud.Alcance;
           }
               
           pnDetallePlanCompras.Style.Add("display", "");
           rfvObjeto.Enabled = true;
           rfvNumeroPlanCompras.Enabled = true;
           rfvValorInicialContConv.Enabled = true;

           WsContratosPacco.WSContratosPACCOSoapClient cliente = new WsContratosPacco.WSContratosPACCOSoapClient();
            var miPlanCompras = cliente.serv_ConsecutivoEncabezadInicial(planCompras[0].Vigencia, planCompras[0].NumeroConsecutivoPlanCompras);
           gvProductos.DataSource = miPlanCompras.DetalleProductos;
           gvProductos.DataBind();

           gvRubrosPlanCompras.DataSource = miPlanCompras.DetalleRubros;
           gvRubrosPlanCompras.DataBind();

           CargarAportes();
        }
        else
        {
            pnDetallePlanCompras.Style.Add("display", "none");
            rfvObjeto.Enabled = false;
            rfvNumeroPlanCompras.Enabled = false;
            rfvValorInicialContConv.Enabled = false;
            txtAlcance.Text = string.Empty;
            txtObjeto.Text = string.Empty;
            txtValorInicialContConv.Text = string.Empty;
            txtPlanCompras.Text = string.Empty;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarAportes()
    {
        var misaportes = vContratoService.ObtenerAportesContrato(int.Parse(hfIdContrato.Value), false);
        gvAportesICBF.DataSource = misaportes;
        gvAportesICBF.DataBind();

        var AportesICBF = misaportes.Where(e => e.AportanteICBF == true);
        var AportesCofi = misaportes.Where(e => e.AportanteICBF == false);

        if (AportesICBF != null)
        {
            var valor = AportesICBF.Sum(e => e.ValorAporte);
            txtValorApoICBF.Text = string.Format("{0:$#,##0}", valor);

            var valorEspecie = AportesICBF.FirstOrDefault(e => e.AporteEnDinero == false);

            hfAportes.Value = valorEspecie != null ? "1" : string.Empty;
        }
        else
            hfAportes.Value = string.Empty;

        if (AportesCofi != null)
        {
            var valor = AportesCofi.Sum(e => e.ValorAporte);
            txtValorApoContrat.Text = string.Format("{0:$#,##0}", valor);

            hfCofinanciacion.Value = valor > 0 ? "1" : string.Empty;
        }
        else
            hfCofinanciacion.Value = string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarAporteCofinanciacion()
    {
        int idContrato = int.Parse(hfIdContrato.Value);
        var result= vContratoService.EliminarAportesCofinanciacionContrato(idContrato);

        if (result > 0)
        {
            CargarAportes();
            imgValorApoContrat.Style.Add("display", "none");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarAporteEspecieICBF()
    {
        int idContrato = int.Parse(hfIdContrato.Value);
        var result = vContratoService.EliminarAportesEspecieICBFContrato(idContrato);

        if (result > 0)
        {
            CargarAportes();
            imgValorApoICBF.Style.Add("display", "none");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarLugarEjecucion()
    {
        gvLugaresEjecucion.EmptyDataText = EmptyDataText();
        gvLugaresEjecucion.PageSize = PageSize();
        int idContrato = int.Parse(hfIdContrato.Value);
        var lugarEjecucion = vContratoService.ConsultarLugaresEjecucionContrato(idContrato);

        if(lugarEjecucion.Any(e => e.EsNivelNacional))
        {
            dvLugaresEjecucion.Style.Remove("height");
            dvLugaresEjecucion.Style.Remove("overflow-x");
            dvLugaresEjecucion.Style.Remove("overflow-y");
            dvLugaresEjecucion.Style.Add("display", "none");
            chkNivelNacional.Checked = true;          
        }
        else
        {
            gvLugaresEjecucion.DataSource = lugarEjecucion;
            gvLugaresEjecucion.DataBind();
            chkNivelNacional.Checked = false;

            if(lugarEjecucion != null && lugarEjecucion.Count > 0)
                txtDatosAdicionalesLugarEjecucion.Text = lugarEjecucion.First().DatosAdicionales;

            if(lugarEjecucion.Count() > 2)
            {
                dvLugaresEjecucion.Style.Add("height", "100px");
                dvLugaresEjecucion.Style.Add("overflow-x", "hidden");
                dvLugaresEjecucion.Style.Add("overflow-y", "scroll");
            }
            else
            {
                dvLugaresEjecucion.Style.Remove("height");
                dvLugaresEjecucion.Style.Remove("overflow-x");
                dvLugaresEjecucion.Style.Remove("overflow-y");
            }

            dvLugaresEjecucion.Style.Add("display", "");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarCDP()
    {
        if(rblManejaRecursosICBF.SelectedValue == "Si")
          ApCDP.Visible = true;
        else
           ApCDP.Visible = false;

        gvCDP.EmptyDataText = EmptyDataText();
        gvCDP.PageSize = PageSize();
        gvRubrosCDP.EmptyDataText = EmptyDataText();
        gvRubrosCDP.PageSize = PageSize();

        List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDPSimple(Convert.ToInt32(hfIdContrato.Value));

        if(cdps.Count > 0)
            hfCDP.Value = "1";
        else
            hfCDP.Value = string.Empty;

        gvCDP.DataSource = cdps;
        gvCDP.DataBind();

        List<ContratosCDP> rubrosCdp = vContratoService.ConsultarContratosCDPRubroSimple(Convert.ToInt32(hfIdContrato.Value));
        gvRubrosCDP.DataSource = rubrosCdp;
        gvRubrosCDP.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarVigenciasFuturas()
    {
        if(rblManejaVigenciasFuturas.SelectedValue == "Si")
            ApVigenciasFuturas.Visible = true;
        else
            ApVigenciasFuturas.Visible = false;

        gvVigFuturas.EmptyDataText = EmptyDataText();
        gvVigFuturas.PageSize = PageSize();

        if(! string.IsNullOrEmpty(hfIdContrato.Value))
        {
            int idContrato = int.Parse(hfIdContrato.Value);
            var itemsVigencia = vContratoService.ConsultarVigenciaFuturass(null, null, idContrato, null);

            if(itemsVigencia.Count > 0)
                hfVigenciasFuturas.Value = "1";
            else
                hfVigenciasFuturas.Value = string.Empty;

            gvVigFuturas.DataSource = itemsVigencia;
            gvVigFuturas.DataBind();
        }
    }

    private void CargarContratistas()
    {
        List<Proveedores_Contratos> contratistas = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
        gvContratistas.DataSource = contratistas;
        gvContratistas.DataBind();

        List<Proveedores_Contratos> integrantesConsorcioUnionTemp = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(hfIdContrato.Value), 1);
        gvIntegrantesConsorcio_UnionTemp.DataSource = integrantesConsorcioUnionTemp;
        gvIntegrantesConsorcio_UnionTemp.DataBind();

        if(contratistas.Count() > 0)
            hfContratistas.Value = "1";
        else
            hfContratistas.Value = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarVigenciasFuturas()
    {
        var result = vContratoService.EliminarVigenciaFuturasContrato(int.Parse(hfIdContrato.Value));

        if (result > 0)
            CargarVigenciasFuturas();
    }

    private void EliminarCDPs()
    {

        ContratosCDP vContratoCPD = new ContratosCDP();
        vContratoCPD.IdContrato = Convert.ToInt32(hfIdContrato.Value);
        var result = vContratoService.EliminarContratosCDP(vContratoCPD);

        if(result > 0)
            CargarCDP();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDocumentos()
    {
        int idSolicitudContrato = int.Parse(hfIdSolicitudContrato.Value);
        gvDocumentos.EmptyDataText = EmptyDataText();
        gvDocumentos.PageSize = PageSize();
        var misitems = vPrecontractualService.ConsultarDocumentosPorIdSolicitud(idSolicitudContrato);

        if(misitems.Count > 0)
            ddlModalidadSeleccion.Enabled = false;

        gvDocumentos.DataSource = misitems;
        gvDocumentos.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dateTime"></param>
    /// <returns></returns>
    private string ValidacionesFechaFinalizacionInicialContConv(DateTime dateTime)
    {
        return string.Empty;
    }

    private void EliminarContratista(int rowIndex)
    {
        Proveedores_Contratos vProveedor_Contrato = new Proveedores_Contratos();
        vProveedor_Contrato.IdProveedoresContratos = Convert.ToInt32(gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"]);
        vProveedor_Contrato.IdContrato = Convert.ToInt32(hfIdContrato.Value);
        vContratoService.EliminarProveedores_Contratos(vProveedor_Contrato);
        CargarContratistas();
    }

    #endregion 

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                try
                {
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch (sControlName)
                    {
                        case "cphCont_txtPlanCompras":
                            CargarInfoPlanCompras(true);
                            break;
                        case "CargarAportes":
                            CargarAportes();
                            break;
                        case "EliminarAportesEspecieICBF":
                            EliminarAporteEspecieICBF();
                            break;
                        case "EliminarAportesCofinanciacion":
                            EliminarAporteCofinanciacion();
                            break;
                        case "ObtenerLugarEjecucion":
                            CargarLugarEjecucion();
                            break;
                        case "ObtenerVigenciasFuturas":
                            CargarVigenciasFuturas();
                            break;
                        case "EliminarVigenciasFuturas":
                            EliminarVigenciasFuturas();
                            break;
                        case "ObtenerCDP":
                            CargarCDP();
                            break;
                        case "EliminarCDP":
                        EliminarCDPs();
                        break;
                        case "ObtenerDocumentos":
                            CargarDocumentos();
                            break;
                        case "ObtenerContratistas":
                        CargarContratistas();
                        break;
                        default: break;
                    }
                }
                catch (UserInterfaceException ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfIdSolicitudContrato.Value))
        {
            SetSessionParameter("SolicitudContrato.IdSolicitudContrato", hfIdSolicitudContrato.Value);
            NavigateTo(SolutionPage.Detail);
        }
        else
        {
            NavigateTo(SolutionPage.List);
        }
    }

    protected void AccSolicitudContrato_ItemCommand(object sender, CommandEventArgs e)
    {

    }

    protected void ddlCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CargarTipodeContrato();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlTipoContratoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            VerificarTipoContratoConvenio();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlModalidadAcademica_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            cargarProfesion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void btnEliminarAporteClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            AporteContrato vAporteContrato = new AporteContrato();
            vAporteContrato.IdAporteContrato = Convert.ToInt32(gvAportesICBF.DataKeys[rowIndex]["IdAporteContrato"]);
             var result = vContratoService.EliminarAporteContrato(vAporteContrato);

             //if (result > 0)
             //{
                 CargarAportes();
             //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void chkNivelNacionalCheckedChanged(object sender, EventArgs e)
    {
        if (chkNivelNacional.Checked)
        {
            imgLugarEjecucion.Enabled = false;
            txtDatosAdicionalesLugarEjecucion.Enabled = true;
            dvLugaresEjecucion.Style.Add("display", "none");
        }
        else
        {
            imgLugarEjecucion.Enabled = true;
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            txtDatosAdicionalesLugarEjecucion.Text = string.Empty;
            dvLugaresEjecucion.Style.Add("display", "");
        }
    }

    protected void btnEliminarLugEjecucionClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
            vLugarEjecucionContrato.IdLugarEjecucionContratos = Convert.ToInt32(gvLugaresEjecucion.DataKeys[rowIndex]["IdLugarEjecucionContratos"]);
            var result =  vContratoService.EliminarLugarEjecucionContrato(vLugarEjecucionContrato);

            if (result > 0)
                CargarLugarEjecucion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAportesICBF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
                string aportante = gvAportesICBF.DataKeys[e.Row.RowIndex].Values[2].ToString();
                string tipoAporte = gvAportesICBF.DataKeys[e.Row.RowIndex].Values[1].ToString();

                if (aportante == "ICBF" && tipoAporte == "DINERO" )
                    ((LinkButton)e.Row.FindControl("btnEliminar")).Visible = false;
        }
    }

    protected void btnEliminarGvVigFuturasClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            VigenciaFuturas VVigenciaFutura = new VigenciaFuturas();
            VVigenciaFutura.IDVigenciaFuturas = Convert.ToInt32(gvVigFuturas.DataKeys[rowIndex]["IDVigenciaFuturas"]);
            int result = vContratoService.EliminarVigenciaFuturas(VVigenciaFutura);

            if (result > 0)
                CargarVigenciasFuturas();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarCDPClick(object sender, EventArgs e)
    {
        try
        { 
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            ContratosCDP vContratoCPD = new ContratosCDP();
            vContratoCPD.IdCDP = Convert.ToInt32(gvCDP.DataKeys[rowIndex]["IdContratosCDP"]);
            int result = vContratoService.EliminarContratosCDP(vContratoCPD);

            if (result > 0)
                CargarCDP();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarDocumentosClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            var idDocumento = Convert.ToInt32(gvDocumentos.DataKeys[rowIndex]["IdSolicitudContratoDocumento"]);
            int result = vPrecontractualService.EliminarSolicitudContratoDocumentos(idDocumento);

            if (result > 0)
                CargarDocumentos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlVigenciaSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void gvDocumentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool aprobado = bool.Parse(gvDocumentos.DataKeys[e.Row.RowIndex].Values[1].ToString());

            if (aprobado)
                ((LinkButton)e.Row.FindControl("btnEliminarDocumentos")).Visible = false;
        }
    }

    protected void btnOpcionGvContratistasClick(object sender, EventArgs e)
    {
        switch(((LinkButton)sender).CommandName)
        {
            case "Eliminar":
            EliminarContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
            break;
            case "Detalle":
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);

            string idProveedor = gvContratistas.DataKeys[rowIndex]["IdTercero"].ToString();
            SetSessionParameter("Contrato.IdProveedor", idProveedor);

            string idProveedoresContratos = gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"].ToString();
            SetSessionParameter("FormaPago.IDProveedoresContratos", idProveedoresContratos);

            //SetSessionParameter("Contrato.ContratosAPP", IdContrato);
            string returnValue = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                               "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                               "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                               "   <script language='javascript'> " +
                               "        GetFormaPago();" +
                               "   </script>";


            ClientScript.RegisterStartupScript(Page.GetType(), "formaPago", returnValue);

            break;
            default:
            break;
        }
    }

    #endregion

}

