﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;

/// <summary>
/// Página que despliega el detalle del registro de una solicitud de contrato
/// </summary>
public partial class Page_PreContractual_Solicitudes_Detail : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "Precontractual/Solicitudes";

    #region Constantes
    //Contrato Asociado
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    //Categoria contrato
    private const string CodCategoriaContrato = "CTR"; //"Contrato";
    private const string CodCategoriaConvenio = "CVN"; //"Convenio";

    //Tipos contrato
    private const string CotTipoContConvPrestServApoyoGestion = "PREST_SERV_APO_GEST"; //"Prestación de Servicios de Apoyo a la gestión";
    private const string CodTipoContConvPrestServProfesionales = "PREST_SERV_PROF"; //"Prestación de Servicios Profesionales";
    private const string CodTipoContAporte = "APORTE"; //"Aporte";
    private const string CodMarcoInteradministrativo = "MC_INT_ADMIN"; //"Marco Interadministrativo";

    //Modalidad selección
    private const string CodContratacionDirecta = "CONT_DIR"; //"Contratación Directa";
    private const string CodContratacionDirectaAporte = "CDA"; //"Contratación Directa Aporte";
    #endregion

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    SIAService vSIAService = new SIAService();

    private string[] estadosEdicion = new string[] { "RE", "DEVCL", "DEVCO" };

    private const string EstadoEnviado = "ENV";
    private const string EstadoDevuelto = "DEVCL";
    private const string EstadoAprobado = "APCL";
    private const string EstadoRechazado= "RECL";

    private const string DefaultCambioEstado = "No se puedo realizar el cambio de estado, verfique por favor";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                try
                {
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch (sControlName)
                    {
                        case "ObtenerCambioEstado":
                            CargarInfoCambioEstado();
                            break;
                        default: break;
                    }
                }
                catch (UserInterfaceException ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
            }
        }
    }

    private void CargarInfoCambioEstado()
    {
        if (! string.IsNullOrEmpty( GetSessionParameter("SolicitudContrato.CambioEstado").ToString()))
            NavigateTo();
        else
        {
            var item = vPrecontractualService.ConsultarSolicitudPorId(int.Parse(hfIdSolicitudContrato.Value));
            ValidarAcceso(item);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnRechazar_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btEditar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Registro de Solicitudes de Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            txtNombreUsuario.Text = GetSessionUser().NombreUsuario;

            if (GetSessionUser().IdTipoUsuario == 1)
                txtRegionalUsuario.Text = "Sede Nacional";
            else
            {
                var miRegional = vSIAService.ConsultarRegional(GetSessionUser().IdUsuario);

                if (miRegional != null)
                    txtRegionalUsuario.Text = miRegional.NombreRegional;
            }
            
            CargarCategoriaContrato();
            CargarTipodeContrato();
            CargarModalidadSeleccion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            pnInfoSolicitud.Style.Add("display", "");
            pnInfoSolcitantes.Style.Add("display", "");
            var idSolicitudContrato = GetSessionParameter("SolicitudContrato.IdSolicitud").ToString();
            hfIdSolicitudContrato.Value = idSolicitudContrato.ToString();

            if (GetSessionParameter("SolicitudContrato.Guardo").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado("Se actualizo la solicitud de contrato con Número de Solicitud: " + hfIdSolicitudContrato.Value);
                RemoveSessionParameter("SolicitudContrato.Guardo");
            }

            var itemSolicitud = vPrecontractualService.ConsultarSolicitudPorId(int.Parse(idSolicitudContrato));
            txtRegionalUsuario.Text = itemSolicitud.Regional.ToUpper();
            hfIdContrato.Value = itemSolicitud.IdContrato.ToString();
            ValidarAcceso(itemSolicitud);
            txtFechaRegistroSsistema.Text = itemSolicitud.FechaCrea.ToShortDateString();
            txtNombreUsuario.Text = itemSolicitud.UsuarioCrea.ToUpper();
            txtNumeroSolicitud.Text = itemSolicitud.IdSolicitud.ToString();
            chkConvenioMarco.Checked = itemSolicitud.EsContratoMarco.HasValue ? itemSolicitud.EsContratoMarco.Value : false;
            chkContratoConvenioAd.Checked = itemSolicitud.EsContratoConvenioAdhesion.HasValue ? itemSolicitud.EsContratoConvenioAdhesion.Value : false;
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
            ddlCategoriaContrato.SelectedValue = itemSolicitud.IdCategoriaContrato.ToString();
            CargarTipodeContrato();
            ddlTipoContratoConvenio.SelectedValue = itemSolicitud.IdTipoContrato.ToString();

            if (itemSolicitud.CodigoTipoContrato == CotTipoContConvPrestServApoyoGestion || itemSolicitud.CodigoTipoContrato == CodTipoContConvPrestServProfesionales)
            {
                ActualizarTipoContratoConvenio(true);
                cargarModalidadAcademica();
                ddlModalidadAcademica.SelectedValue = itemSolicitud.IdModalidadAcademica.ToString();
                cargarProfesion();
                ddlNombreProfesion.SelectedValue = itemSolicitud.IdProfesion.ToString();
            }

            rblManejaAportesEspecie.SelectedValue = itemSolicitud.ManejaRecursosEspecieICBF == true ? "Si" : "No";
            rblManejaCofinanciacion.SelectedValue = itemSolicitud.ManejaCofinanciacion == true ? "Si" : "No";
            rblManejaRecursosICBF.SelectedValue = itemSolicitud.ManejaRecursosICBF == true ? "Si" : "No";
            rblManejaVigenciasFuturas.SelectedValue = itemSolicitud.ManejaVigenciasFuturas == true ? "Si" : "No";
            imgVigenciasFuturas.Visible = itemSolicitud.ManejaVigenciasFuturas;
            rblRequiereActaInicio.SelectedValue = itemSolicitud.RequiereActaInicio == true ? "Si" : "No";
            rblRequiereGarantia.SelectedValue = itemSolicitud.RequiereGarantia == true ? "Si" : "No";
            CargarEmpleados(itemSolicitud);

            TxtFechaInicio.Text = itemSolicitud.VigenciaInicial.ToString();
            TxtFechaFin.Text = itemSolicitud.VigenciaFinal.ToString();

            txtObjeto.Text = itemSolicitud.Objeto;
            txtAlcance.Text = itemSolicitud.Alcance;

            if (itemSolicitud.ValorContrato.HasValue)
                txtValorContrato.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContrato.Value);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    private void ValidarAcceso(SolicitudContrato item)
    {
        hfOrigen.Value = Request.QueryString["origen"];

        if (hfOrigen.Value == "V")
        {
            if (item.CodigoEstadoSolicitud == "ENV" && (item.UsuarioRevision.ToUpper() == GetSessionUser().NombreUsuario.ToUpper() || (GetSessionUser().IdTipoUsuario == 1 && !string.IsNullOrEmpty(item.UsuarioRevision))))
            {
                toolBar.MostrarBotonAprobar(true);
                toolBar.SetAprobarConfirmation("return ConfirmarAccion('Aprobar');");
                toolBar.eventoRechazar += new ToolBarDelegate(btnRechazar_Click);
                toolBar.OcultarBotonRechazar(false);
                toolBar.SetRechazarConfirmation("return ConfirmarAccion('Rechazar');");
            }
            else
            {
                toolBar.OcultarBotonEliminar(true);
                toolBar.MostrarBotonAprobar(false);
            }

            toolBar.MostrarBotonEditar(false);
            toolBar.MostrarBotonNuevo(false);
        }
        else if (hfOrigen.Value == "R")
        {
            toolBar.MostrarBotonEditar(false);
            toolBar.MostrarBotonNuevo(false);
            toolBar.OcultarBotonEliminar(true);
            toolBar.MostrarBotonAprobar(false);
        }
        else
        {
            if (estadosEdicion.Any(e => e == item.CodigoEstadoSolicitud))
            {
                toolBar.MostrarBotonAprobar(true);
                toolBar.SetAprobarConfirmation("return ConfirmarAccion('Enviar');");
            }
            else
            {
                toolBar.MostrarBotonAprobar(false);
                toolBar.MostrarBotonEditar(false);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarCategoriaContrato()
    {
        ddlCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, true);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
        }
        ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTipodeContrato()
    {
        string idCategoriaSeleccionada = ddlCategoriaContrato.SelectedValue;
        PnPrestacionServicios.Style.Add("display", "none");

        if (idCategoriaSeleccionada != "-1")
        {
            ddlTipoContratoConvenio.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), true, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void VerificarTipoContratoConvenio()
    {
        string idTipoContratoConvenio = ddlTipoContratoConvenio.SelectedValue;
        string idCategoriaContrato = ddlCategoriaContrato.SelectedValue;

        if (idTipoContratoConvenio != "-1" && idCategoriaContrato != "-1")
        {
            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContratoConvenio);
            vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
            vTipoContrato = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato);

            if (vTipoContrato.CodigoTipoContrato == CodTipoContConvPrestServProfesionales || vTipoContrato.CodigoTipoContrato == CotTipoContConvPrestServApoyoGestion)
            {
                ActualizarTipoContratoConvenio(true);
                cargarModalidadAcademica();
                cargarProfesion();
            }
            else
                ActualizarTipoContratoConvenio(false);
        }
        else
            ActualizarTipoContratoConvenio(false);
    }

    /// <summary>
    /// 
    /// </summary>
    private void ActualizarTipoContratoConvenio(bool valido)
    {
        if (valido)
            PnPrestacionServicios.Style.Add("display", "");
        else
            PnPrestacionServicios.Style.Add("display", "none");           
    }

    /// <summary>
    /// 
    /// </summary>
    private void cargarModalidadAcademica()
    {
        ddlModalidadAcademica.Items.Clear();
        List<ModalidadAcademicaKactus> vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, null, null);
        foreach (ModalidadAcademicaKactus tD in vLModalidadesAcademicas)
            ddlModalidadAcademica.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
        
        ddlModalidadAcademica.Items.Insert(0, new ListItem("Seleccionar", "-1"));

        ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void cargarProfesion()
    {
        var idModalidadAcademicaSeleccionada = ddlModalidadAcademica.SelectedValue;

        if (idModalidadAcademicaSeleccionada != "-1")
        {
            ddlNombreProfesion.Items.Clear();
            List<ProfesionKactus> vLTiposContratos = vContratoService.ConsultarProfesionesKactus(idModalidadAcademicaSeleccionada, null, null, null);
            foreach (ProfesionKactus tD in vLTiposContratos)
                ddlNombreProfesion.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
            
            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlNombreProfesion.Items.Clear();
            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlNombreProfesion.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    private void CargarEmpleados(SolicitudContrato item)
    {
        txtCargoSolicitante.Text = item.CargoSolicitante;
        txtNombreSolicitante.Text = item.NombreSolicitante;
        txtRegionalContratoConvenio.Text = item.RegionalSolicitante;
        txtDependenciaSolicitante.Text = item.DependenciaSolicitante;

        txtNombreOrdenadorGasto.Text = item.NombreOrdenadorGasto;
        txtCargoOrdenadoGasto.Text = item.CargoOrdenadorGasto;
        txtNumeroIdentOrdenadoGasto.Text = item.IdentificacionOrdenadorGasto.HasValue ? item.IdentificacionOrdenadorGasto.Value.ToString(): string.Empty;
        txtRegionalOrdenadorGasto.Text = item.RegionalOrdenadorGasto;
    }

    /// <summary>
    /// 
    /// </summary>
    private  void NavigateTo()
    {
        if (hfOrigen.Value == "V")
            NavigateTo("~/Page/Precontractual/Validacion/List.aspx");
        else if (hfOrigen.Value == "R")
            NavigateTo("~/Page/Precontractual/Reparto/List.aspx");
        else
            NavigateTo(SolutionPage.List);
    }
}

