﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_PreContractual_Solicitudes_Add" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">


            $(document).ready(function () {

                $('#<%=ddlVigenciaFiscalIni.ClientID %>').change(function () {

                    var inicial =  $(this).val();
                    var final =  $('#<%=ddlVigenciaFiscalFin.ClientID %>').val();

                    if (final != '' && final != null) {

                        var finalInt = parseInt(final);
                        var inicialInt = parseInt(inicial);

                        if(finalInt < inicialInt)
                        {
                            $('#<%=ddlVigenciaFiscalFin.ClientID %>').val(inicial);
                        }
                    }

                    var existeSolicitud = $('#<%= hfIdSolicitudContrato.ClientID %>').val;
                    if (existeSolicitud != '' & existeSolicitud != null ) {
                        $('#<%= hfEsCambioVigencia.ClientID %>').val = '1';
                    }
                });

                $('#<%=ddlVigenciaFiscalFin.ClientID %>').change(function () {
                    
                    var final = $(this).val();
                    var inicial = $('#<%=ddlVigenciaFiscalIni.ClientID %>').val();

                    if (inicial != '' && inicial != null) {

                        var finalInt = parseInt(final);
                        var inicialInt = parseInt(inicial);

                        if (finalInt < inicialInt) {
                            $('#<%=ddlVigenciaFiscalFin.ClientID %>').val(inicial);
                        }
                    }
                    else {
                        $('#<%=ddlVigenciaFiscalIni.ClientID %>').val(inicial);
                    }

                    var existeSolicitud = $('#<%= hfIdSolicitudContrato.ClientID %>').val;
                    if (existeSolicitud != '' & existeSolicitud != null) {
                        $('#<%= hfEsCambioVigencia.ClientID %>').val = '1';
                    }
                });

                $('#<%=rblManejaRecursosICBF.ClientID %> input').change(function () {

                    if ($(this).val() == 'Si') {
                        __doPostBack('ObtenerCDP', '');
                    }
                    else {
   
                        var cdps = document.getElementById('<%= hfCDP.ClientID %>').value;

                        if (cdps != null && cdps != '') {

                                if (confirm('Esta seguro que desea eliminar, la información de los CDPs ?')) {
                                    __doPostBack('EliminarCDP', '');
                                }
                                else{
                                    $('#<%=rblManejaRecursosICBF.ClientID %>').find("input[value='Si']").prop("checked", true);
                                }
                        }
                    }
                });

                $('#<%=rblManejaVigenciasFuturas.ClientID %>  input').change(function () {
 

                    if ($(this).val() == 'Si') {
                        __doPostBack('ObtenerVigenciasFuturas', '');
                    }
                    else {
                            var VigenciasFututasExiste = document.getElementById('<%= hfVigenciasFuturas.ClientID %>').value;

                            if (VigenciasFututasExiste != null && VigenciasFututasExiste != '') {

                                if (confirm('Esta seguro que desea eliminar, la información de las vigencias futuras ?')) {
                                    __doPostBack('EliminarVigenciasFuturas', '');
                                }
                                else {
                                    $('#<%=rblManejaVigenciasFuturas.ClientID %>').find("input[value='Si']").prop("checked", true);
                                }
                            }
                            else {
                                __doPostBack('ObtenerVigenciasFuturas', '');
                            }
                    }
                });

                $('#<%=rblManejaCofinanciacion.ClientID %> input').change(function () {

                    if ($(this).val() == 'Si') {
                        $("[id*=imgValorApoContrat]").show();
                    }
                    else {
                        var aportesCofinanciacionExiste = document.getElementById('<%= hfCofinanciacion.ClientID %>').value;

                        if (aportesEspecieExiste != null && aportesEspecieExiste != '') {

                            if (confirm('Esta seguro que desea eliminar, la información de los aportes en confinaciación ?')) {
                                $("[id*=imgValorApoContrat]").hide();
                                __doPostBack('EliminarAportesCofinanciacion', '');
                            }
                            else {
                                $('#<%=rblManejaCofinanciacion.ClientID %>').find("input[value='Si']").prop("checked", true);
                            }
                        }
                        else 
                            $("[id*=imgValorApoContrat]").hide();
                    }
                });

                $('#<%=rblManejaAportesEspecie.ClientID%> input').change(function () {

                    if ($(this).val() == 'Si') {
                        $("[id*=imgValorApoICBF]").show();
                    }
                    else {
                        var aportesEspecieExiste = document.getElementById('<%= hfAportes.ClientID %>').value;

                        if (aportesEspecieExiste != null && aportesEspecieExiste != '') {

                            if (confirm('Esta seguro que desea eliminar, la información de los aportes en especie ?')) {
                                $("[id*=imgValorApoICBF]").hide();
                                __doPostBack('EliminarAportesEspecieICBF', '');
                            }
                            else {
                                $('#<%=rblManejaAportesEspecie.ClientID %>').find("input[value='Si']").prop("checked", true);
                            }
                        }
                        else 
                             $("[id*=imgValorApoICBF]").hide();
                    }
                });

            });


            function ValidaEliminacion(tipo) {

                if(tipo == 'Aportes')
                    return confirm('Esta seguro de que desea eliminar el aporte?');
                else if (tipo == 'LugarEjecuccion')
                    return confirm('Esta seguro de que desea eliminar el lugar de ejecucción?');
                else if (tipo == 'CDP')
                    return confirm('Esta seguro de que desea eliminar el CDP?');
                else if (tipo == 'VigenciasFuturas')
                    return confirm('Esta seguro de que desea eliminar la Vigencia Futura?');
                else if (tipo == 'Documento')
                    return confirm('Esta seguro de que desea eliminar el Documento?');
                else if (tipo == 'Contratista')
                    return confirm('Esta seguro de que desea eliminar el contratista?');
                
            }

            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }

            function GetEmpleado(valor) {
                muestraImagenLoading();
                var valorContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                window_showModalDialog('../../../Page/Precontractual/Lupas/LupaEmpleado.aspx?op=' + valor + '&idContrato=' + valorContrato, setReturnGetEmpleado, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetEmpleado(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 12)
                    {
                        if (retLupa[13] == 'Sol') {
                            document.getElementById('<%= hfCodRegionalContConv.ClientID %>').value = retLupa[12];

                            document.getElementById('<%= txtNombreSolicitante.ClientID  %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                            document.getElementById('<%= txtDependenciaSolicitante.ClientID %>').value = retLupa[8];
                            document.getElementById('<%= txtCargoSolicitante.ClientID %>').value = retLupa[9];
                            document.getElementById('<%= txtRegionalContratoConvenio.ClientID %>').value = retLupa[7];
                        }
                        else {
                            document.getElementById('<%= txtNombreOrdenadorGasto.ClientID  %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                            document.getElementById('<%= txtRegionalOrdenadorGasto.ClientID %>').value = retLupa[7];
                            document.getElementById('<%= txtNumeroIdentOrdenadoGasto.ClientID %>').value = retLupa[1];
                            document.getElementById('<%= txtCargoOrdenadoGasto.ClientID %>').value = retLupa[9];
                        }
                        prePostbck(false);
                    }
                    else
                    {
                        ocultaImagenLoading();
                    }
                } else {
                    ocultaImagenLoading();
                }
            }



            function GetPlanCompras() {
                muestraImagenLoading();

                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var vIdRegional = document.getElementById('<%= hfRegionalContConv.ClientID %>').value;
                var vIdPlanCompras = document.getElementById('<%= hfPlanCompras.ClientID %>').value;
                var vAnio = '<%= ddlVigenciaFiscalIni.SelectedItem.Text %>';

                var url = '';
                if(vIdPlanCompras == '')
                    url = '../../../Page/Contratos/Lupasv11/LupasBuscarPlanDeCompras.aspx?esPrecontractual=1&idContrato=' + vIdContrato +
                                                                                                     '&idRegional=' + vIdRegional +
                                                                                                     '&anio=' + vAnio;
                else
                    url = '../../../Page/Contratos/Lupasv11/LupasPlanCompras.aspx?esPrecontractual=1&idContrato=' + vIdContrato +
                                                                                             '&idRegional=' + vIdRegional +
                                                                                             '&idPlanCompras=' + vIdPlanCompras;

                window_showModalDialog(url, setReturnGetPlanCompras, 'dialogWidth:600px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetPlanCompras(dialog) {
                prePostbck(false);
                __doPostBack('<%= txtPlanCompras.ClientID %>', '');
            }


            function GetValorAportes(tipo,tipoAporte) {

                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                muestraImagenLoading();
                var url = '../../../Page/PreContractual/Lupas/LupaAportes.aspx?Aportante='+tipo+'&TipoAporte='+tipoAporte+'&idContrato='+vIdContrato;
                window_showModalDialog(url, setReturnGetValorAportes, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetValorAportes() {
                prePostbck(false);
                __doPostBack('CargarAportes', '');
            }


            function GetLugarEjecucion() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                    window_showModalDialog('../../../Page/PreContractual/Lupas/LupaConsultarLugarContrato.aspx?idContrato=' + vIdContrato, setRetrunGetLugarEjecucion, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                
            }

            function setRetrunGetLugarEjecucion(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    var chkNivelNacional = document.getElementById('<%= chkNivelNacional.ClientID %>');
                    prePostbck(false);
                    if (retLupa[0] == 'SI')
                        chkNivelNacional.checked = true;
                    else
                        chkNivelNacional.checked = false;
                    __doPostBack('ObtenerLugarEjecucion', retLupa[0]);
                }
            }

            function GetCDP() {
                muestraImagenLoading();
                var vIdRegional = document.getElementById('<%= hfRegionalContConv.ClientID  %>').value;
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var VigenciaInicial = document.getElementById('<%= ddlVigenciaFiscalIni.ClientID %>').value;
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaRegInfoPresupuestalEnLinea.aspx?vEsPrecontractual=1&IdRegContrato=' + vIdRegional + '&idContrato=' + vIdContrato + '&idVigencia=' + VigenciaInicial, setReturnGetCDP, 'dialogWidth:1050px;dialogHeight:600px;resizable:yes;');
            }
            function setReturnGetCDP(dialog) {
                    prePostbck(false);
                    __doPostBack('ObtenerCDP', '');
                } 
            
            function GetVigFuturas() {

                    var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                    var VigenciaInicial = document.getElementById('<%= ddlVigenciaFiscalIni.ClientID %>').value;
                    var VigenciaFinal = document.getElementById('<%= ddlVigenciaFiscalFin.ClientID %>').value;
                    var VigenciaInicialA = document.getElementById('<%= hfVigenciaInicial.ClientID %>').value;
                    var VigenciaFinalA = document.getElementById('<%= hfVigenciaFinal.ClientID %>').value;

                if (VigenciaFinal != null && VigenciaInicial != null && VigenciaFinal !='' && VigenciaInicial !='') {

                    if (VigenciaFinal == VigenciaFinalA && VigenciaInicial == VigenciaInicialA) {

                        var anioInicial = '<%= ddlVigenciaFiscalIni.SelectedItem.Text %>';
                        var anioFinal = '<%= ddlVigenciaFiscalFin.SelectedItem.Text %>';

                        var anioIntI = parseInt(anioInicial);
                        var anioIntF = parseInt(anioFinal);

                        if (anioIntF > anioIntI) {
                            muestraImagenLoading();
                            window_showModalDialog('../../../Page/PreContractual/Lupas/LupaVigenciasFuturas.aspx?idContrato=' + vIdContrato + '&idVigenciaInicial=' + anioInicial + '&idVigenciaFinal=' + anioFinal, setReturnGetVigFuturas, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                        }
                        else
                        {
                            alert('No puede asociar una vigencia futura, las vigencias son iguales.');
                        }
                    } else
                        alert('No puede asociar una vigencia futura, las vigencias fueron modificadas y no han sido actualizadas. Guarde cambios por favor.');
                }
                else
                    alert('La Vigencia Incial y/o Vigencia Final No estan asociadas.');
            }
            
            function setReturnGetVigFuturas() {
                prePostbck(false);
                __doPostBack('ObtenerVigenciasFuturas', '');
            }

            function GetDocumentos() {
                muestraImagenLoading();
                var vIdSolicitudContrato = document.getElementById('<%= hfIdSolicitudContrato.ClientID %>').value;
                var vModalidadSeleccion = '<%= ddlModalidadSeleccion.SelectedValue %>';

                window_showModalDialog('../../../Page/PreContractual/Lupas/LupaDocumentos.aspx?idSolicitudContrato=' + vIdSolicitudContrato + '&idModalidadSeleccion=' + vModalidadSeleccion, setReturnGetDocumentos, 'dialogWidth:1050px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetDocumentos(dialog) {
                prePostbck(false);
                __doPostBack('ObtenerDocumentos', '');
            }

                function ValidaContratistas(source, args) {
                    var hfContratistas = document.getElementById('<%= hfContratistas.ClientID %>');
                    if (parseInt(hfContratistas.value) > 0) {
                        args.IsValid = true;
                    } else {
                        args.IsValid = false;
                    }
                }

              function GetContratista() {
                    muestraImagenLoading();
                    var contrato = document.getElementById('<%= hfIdContrato.ClientID %>');
                    window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx?idContrato='+contrato.value, setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                }

                function setReturnGetContratista() {
                    prePostbck(false);
                    __doPostBack('ObtenerContratistas', '');
                }

                function juegoContratoAsociado(tipo) {

                    if (tipo == '1') {
                        var chkContratoConvenioAd = document.getElementById('<%= chkContratoConvenioAd.ClientID %>');
                        chkContratoConvenioAd.checked = false;
                    }
                    else {
                        var chkConvenioMarco = document.getElementById('<%= chkConvenioMarco.ClientID %>');
                        chkConvenioMarco.checked = false;
                    }
                }

        </script>

            <Ajax:Accordion ID="AccSolicitudContrato" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
            ContentCssClass="accordionContent" OnItemCommand="AccSolicitudContrato_ItemCommand"  runat="server"
            Width="98%" Height="100%">
            <Panes>
                 <Ajax:AccordionPane ID="ApDatosGeneralContrato" runat="server">
                         <Header>
                             Datos Generales
                        </Header>
                        <Content>
                              <table width="100%"  id="pnInfoUsuario" align="center">
                                <tr class="rowB">
                                    <td>
                                        Nombre del usuario Solicitud
                                    </td>
                                    <td>
                                        Regional del usuario Solicitud                                                     
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                            <asp:TextBox ID="txtNombreUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtRegionalUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                              </table>
                              <table width="100%" id="pnInfoSolicitud" runat="server" align="center" style="display:none">
                                <tr class="rowB">
                                    <td>
                                        Número de Solicitud
                                    </td>
                                    <td>
                                        Fecha de Registro en el Sistema                                                 
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td class="Cell">
                                            <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtFechaRegistroSsistema" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                              </table>
                              <table width="100%" id="pnInfoCategoriaContrato" align="center">
                                <tr class="rowB">
                                    <td class="Cell">
                                        Contrato Asociado
                                    </td>
                                    <td class="Cell">
                                            Modalidad de Selección *
                                            <asp:CompareValidator runat="server" ID="cvModalidadSeleccion" ControlToValidate="ddlModalidadSeleccion"
                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                            ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                <td class="Cell">
                                        <asp:CheckBox ID="chkConvenioMarco" runat="server" Text="Convenio Marco" onclick="juegoContratoAsociado('1')" /><br />
                                        <asp:CheckBox ID="chkContratoConvenioAd" runat="server" Text="Contrato/Convenio Adhesión" onclick="juegoContratoAsociado('0')" />
                                    </td>
                                    <td class="Cell">
                                        <asp:DropDownList ID="ddlModalidadSeleccion" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                    Categoría Contrato/Convenio *
                                    <asp:CompareValidator runat="server" ID="cvCategoriaContrato" ControlToValidate="ddlCategoriaContrato"
                                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>                                                 
                                </td>
                                    <td class="Cell">
                                        Tipo de Contrato/Convenio *
                                        <asp:CompareValidator runat="server" ID="cvTipoContratoConvenio" ControlToValidate="ddlTipoContratoConvenio"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                        <td class="Cell">
                                        <asp:DropDownList ID="ddlCategoriaContrato" runat="server" onchange="prePostbck(true)"
                                        OnSelectedIndexChanged="ddlCategoriaContrato_SelectedIndexChanged"   AutoPostBack="true">
                                        </asp:DropDownList>
                                        </td>
                                        <td class="Cell">
                                        <asp:DropDownList ID="ddlTipoContratoConvenio" runat="server"  onchange="prePostbck(true)"
                                            OnSelectedIndexChanged="ddlTipoContratoConvenio_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                               </table>
                              <table  width="100%" id="PnPrestacionServicios"  runat="server" style="display:none" align="center">
                                  <tr class="rowB">
                                                        <td class="Cell">
                                                            Modalidad Académica *
                                                            <asp:CompareValidator runat="server" ID="cvModalidadAcademica" ControlToValidate="ddlModalidadAcademica"
                                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                                            ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" ></asp:CompareValidator>
                                                        </td>
                                                        <td class="Cell">
                                                            Nombre de la Profesión *
                                                        <asp:CompareValidator runat="server" ID="cvNombreProfesion" ControlToValidate="ddlNombreProfesion"
                                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                  <tr class="rowA" >
                                                        <td class="Cell">
                                                            <asp:DropDownList ID="ddlModalidadAcademica" AutoPostBack="true" OnSelectedIndexChanged="ddlModalidadAcademica_SelectedIndexChanged" runat="server" Enabled="true" />
                                                        </td>
                                                        <td class="Cell">
                                                            <asp:DropDownList ID="ddlNombreProfesion" runat="server" />
                                                        </td>
                                                    </tr>
                                </table>
                              <table width="100%" id="PnInfoTipoContrato" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Requiere Garantía *
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="rblRequiereGarantia"
                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                            ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                        <td class="Cell">
                                            Requiere Acta de Inicio *
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                             <asp:RadioButtonList runat="server" ID="rblRequiereGarantia" RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>  
                                        </td>
                                        <td class="Cell">
                                             <asp:RadioButtonList runat="server" ID="rblRequiereActaInicio"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Maneja Recursos ICBF *
                                        </td>
                                        <td class="Cell">
                                            Maneja Cofinanciación *
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                             <asp:RadioButtonList runat="server" ID="rblManejaRecursosICBF"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>  
                                        </td>
                                        <td class="Cell">
                                             <asp:RadioButtonList runat="server" ID="rblManejaCofinanciacion"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Maneja Aportes en Especie ICBF *
                                        </td>
                                        <td class="Cell">
                                            Requiere Vigencias Futuras *
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                             <asp:RadioButtonList runat="server" ID="rblManejaAportesEspecie" RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>  
                                        </td>
                                        <td class="Cell">
                                             <asp:RadioButtonList runat="server" ID="rblManejaVigenciasFuturas" RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>  
                                        </td>
                                    </tr>

                                    <tr class="rowB">
                                        <td>
                                            Vigencia Fiscal Inicial del Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvVigenciaFiscalIni" ControlToValidate="ddlVigenciaFiscalIni"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                        <td colspan="2">
                                            Vigencia Fiscal Final del Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvVigenciaFiscalFin" ControlToValidate="ddlVigenciaFiscalFin"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlVigenciaFiscalIni"   runat="server"  />
                                        </td>
                                        <td class="Cell" colspan="2">
                                            <asp:DropDownList ID="ddlVigenciaFiscalFin"  runat="server"  />
                                        </td>
                                    </tr>
                                </table>
                              <table width="100%"  id="pnInfoSolcitantes" runat="server" align="center" style="display:none">
                                    <tr class="rowB">
                                        <td>
                                            Nombre del Solicitante *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombreSolicitante" ControlToValidate="txtNombreSolicitante"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Regional del Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvRegionalContConv" ControlToValidate="txtRegionalContratoConvenio"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                                Enabled="false" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtNombreSolicitante" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled" ></asp:TextBox>
                                            <asp:ImageButton ID="imgNombreSolicitante" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetEmpleado('Sol'); return false;" Style="cursor: hand" ToolTip="Buscar"
                                                 />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtRegionalContratoConvenio" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Dependencia Solicitante
                                        </td>
                                        <td>
                                            Cargo el Solicitante
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Nombre del Ordenador del Gasto *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombreOrdenadorGasto" ControlToValidate="txtNombreOrdenadorGasto"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" Enabled="false"
                                                ForeColor="Red" ></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Regional Ordenador del Gasto
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtNombreOrdenadorGasto" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled" ></asp:TextBox>
                                            <asp:ImageButton ID="imgNombreOrdenadorGasto" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetEmpleado('OrdG'); return false;" Style="cursor: hand" ToolTip="Buscar" />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtRegionalOrdenadorGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Número Identificación Ordenador del Gasto
                                        </td>
                                        <td>
                                            Cargo Ordenador del Gasto
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtNumeroIdentOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtCargoOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>

                              </table>
                        </Content>
                    </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="AccordionPanelPlanCompras"  Visible="false" runat="server">
                         <Header>
                             Plan de Compras
                        </Header>
                        <Content>
                            <table width="100%"  id="pnDetalleBasicoPlanCompras" align="center" >
                                   <tr class="rowB">
                                        <td>
                                           Plan de Compras
                                           <asp:RequiredFieldValidator runat="server" ID="rfvNumeroPlanCompras" ControlToValidate="txtPlanCompras"
                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                            ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                           Valor Inicial del Contrato *
                                           <asp:RequiredFieldValidator runat="server" ID="rfvValorInicialContConv" ControlToValidate="txtValorInicialContConv"
                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                            ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="false" Width="30%" ViewStateMode="Enabled"
                                                ></asp:TextBox>
                                            <asp:ImageButton ID="imgPlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetPlanCompras(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorInicialContConv" runat="server" Enabled="false" 
                                                MaxLength="25"
                                                Width="300px"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftValorInicialContConv" runat="server" TargetControlID="txtValorInicialContConv"
                                                FilterType="Custom,Numbers" ValidChars=".," />
                                        </td>
                                    </tr>
                            </table>
                            <table width="100%"  id="pnDetallePlanCompras" align="center" runat="server"  style="display:none"  >
                                    <tr class="rowB">
                                        <td>
                                            Objeto del Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvObjeto" ControlToValidate="txtObjeto"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="2">
                                            Alcance del Objeto del Contrato/Convenio 
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfObjeto" runat="server" />
                                            <asp:TextBox ID="txtObjeto" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                 AutoPostBack="true" 
                                                MaxLength="500" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfAlcance" runat="server" />
                                            <asp:TextBox ID="txtAlcance" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                MaxLength="4000" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr class="rowB">
                                        <td>
                                            Productos
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
<%--                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />--%>
                                                    <asp:BoundField HeaderText="Código del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
<%--                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />--%>
                                                    <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
<%--                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />--%>
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Rubros Plan de Compras
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" DataKeyNames="total_rubro" CellPadding="8" Height="16px">
                                                <Columns>
<%--                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />--%>
                                                    <asp:BoundField HeaderText="Código Rubro" DataField="codigo_rubro" />
<%--                                                    <asp:BoundField HeaderText="Descripción del Rubro" DataField="NombreRubro" />--%>
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" DataField="" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                             </table>
                        </Content>
                    </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="ApContratista" Visible="false" runat="server">
                            <Header>
                                Contratista</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Contratista 
<%--                                            <asp:CustomValidator ID="cvContratistas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnGuardar" ClientValidationFunction="ValidaContratistas"></asp:CustomValidator>--%>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfContratistas" runat="server" />
                                            <asp:ImageButton ID="imgContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetContratista(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Contratistas relacionados
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                OnClientClick="return ValidaEliminacion('Contratista');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
<%--                                                            <asp:LinkButton ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Medio Pago" />
                                                            </asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Integrantes relacionados al consorcio o unión temporal
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvIntegrantesConsorcio_UnionTemp" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Constratista" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacionIntegrante" />
                                                    <asp:BoundField HeaderText="Información Integrante" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Porcentaje Participación" DataField="PorcentajeParticipacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="AccordionPanelAportes" Visible="false" runat="server">
                            <Header>
                                Detalle Valor Inicial del Contrato</Header>
                            <Content>
                                <table width="100%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Valor ICBF 
                                            <asp:CustomValidator ID="cvAportes" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaAportes"></asp:CustomValidator>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Valor Cofinanciaci&#243;n Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoICBF" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                               ></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoICBF" runat="server"  CssClass="bN"  ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetValorAportes('ICBF','Especie'); return false;" Style="cursor:inherit; display:none"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoContrat" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                               ></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoContrat" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetValorAportes('Contratista','Mixto'); return false;" Style="cursor: hand; display:none"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes ICBF
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvAportesICBF_RowDataBound" DataKeyNames="IdAporteContrato,TipoAporte,TipoAportante"
                                                AllowSorting="false"  GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Aportante" DataField="TipoAportante"  />
                                                    <asp:BoundField HeaderText="Tipo de Aporte" DataField="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripci&oacute;n Cofinanciaci&oacute;n Especie" DataField="DescripcionAporte"/>
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarAporteClick"
                                                                OnClientClick="return ValidaEliminacion('Aportes');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="AccordionPanelLugarEjecucion" Visible="false" runat="server">
                            <Header>
                                Lugar de ejecución</Header>
                            <Content>
                                <table width="100%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Lugar de ejecución *
                                            <asp:CustomValidator ID="cvLugarEjecucion" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaLugarEjecucion"></asp:CustomValidator>
                                        </td>
                                        <td>
                                            Datos Adicionales lugar de ejecución
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtLugarEjecucion" runat="server" Enabled="false" ViewStateMode="Enabled"></asp:TextBox>
                                            <asp:ImageButton ID="imgLugarEjecucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetLugarEjecucion('LugarEjeccucion'); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtDatosAdicionalesLugarEjecucion" runat="server" Width="80%" TextMode="MultiLine" Enabled="false" ></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftDatosAdicionalesLugarEjecucion" runat="server"
                                                TargetControlID="txtDatosAdicionalesLugarEjecucion" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                ValidChars="áéíóúÁÉÍÓÚñÑ -" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkNivelNacional" runat="server" Enabled="false" AutoPostBack="true"
                                                OnCheckedChanged="chkNivelNacionalCheckedChanged" />
                                            Nivel Nacional
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <div id="dvLugaresEjecucion" runat="server">
                                                <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos"
                                                    AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                        <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                        <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarLugEjecucionClick"
                                                                    OnClientClick="return ValidaEliminacion('LugarEjecuccion');" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                 </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="ApCDP" runat="server" Visible="false">
                            <Header>
                                CDP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbCDP" runat="server" Text="CDP"></asp:Label>
                                            <asp:CustomValidator ID="cvCDP" runat="server" ErrorMessage="Campo Requerido" Enabled="false"
                                                ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCDP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCDP" runat="server" />
                                            <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" />
                                            <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetCDP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdContratosCDP,CodigoRubro,ValorCDP,ValorRubro"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion('CDP');"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Regional ICBF" DataField="Regional" />
                                                    <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorActualCDP" Visible="false" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                     <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosCDP" runat="server" AutoGenerateColumns="false" DataKeyNames=""
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                        <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                        <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DependenciaAfectacionGastos"/>
                                                        <asp:BoundField HeaderText="Tipo Fuente Financiamiento" DataField="TipoFuenteFinanciamiento"/>
                                                        <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                                                        <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
                                                        <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="TipoSituacionFondos"/>
                                                        <asp:BoundField HeaderText="Valor Rubro" DataField="ValorRubro"  DataFormatString="{0:C}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="ApVigenciasFuturas" Visible="false" runat="server">
                            <Header>
                                Vigencias Futuras</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbVigenciasFuturas" runat="server" Text="Vigencias Futuras"></asp:Label>
                                            <asp:CustomValidator ID="cvVigenciasFuturas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="False" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaVigenciasFuturas"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                            <asp:TextBox ID="txtVigFuturas" runat="server" Enabled="false" ViewStateMode="Enabled" />
                                            <asp:ImageButton ID="imgVigFuturas" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetVigFuturas(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvVigFuturas" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IDVigenciaFuturas" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número del Radicado Vigencia Futura" DataField="NumeroRadicado" />
                                                    <asp:BoundField HeaderText="Fecha de Expedición Vigencia Futura" DataField="FechaExpedicion"
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor Vigencia Futura" DataField="ValorVigenciaFutura"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Año Vigencia Futura" DataField="AnioVigencia" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarGvVigFuturasClick"
                                                                OnClientClick="return ValidaEliminacion('VigenciasFuturas');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                   <Ajax:AccordionPane ID="AccordionPaneDocumentos" Visible="false" runat="server">
                            <Header>
                                Documentos Solicitud Contrato
                            </Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Documentos.
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtDocumentos" runat="server" Enabled="false" ViewStateMode="Enabled" />
                                            <asp:ImageButton ID="ImgBtnDocumentos" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetDocumentos(); return false;" Style="cursor: hand" ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvDocumentos" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IdSolicitudContratoDocumento,Aprobado" OnRowDataBound="gvDocumentos_RowDataBound" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=SolicitudesContrato") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="Documento" DataField="TipoDocumento" />
                                                    <asp:BoundField HeaderText="Aprobado" DataField="AprobadoView" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminarDocumentos" runat="server" OnClick="btnEliminarDocumentosClick"
                                                                OnClientClick="return ValidaEliminacion('Documento');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                </Panes>
             </Ajax:Accordion>
    
                <asp:HiddenField ID="hfIdSolicitudContrato" runat="server" />
                <asp:HiddenField ID="hfIdContrato" runat="server" />
                <asp:HiddenField ID="hfIdRegionalUsuarioCreacion" runat="server" />
                <asp:HiddenField ID="hfEsPrestacionServicios" runat="server" />
                <asp:HiddenField ID="hfRegionalContConv" runat="server" />
                <asp:HiddenField ID="hfCodRegionalContConv" runat="server" />
                <asp:HiddenField ID="hfPlanCompras" runat="server" />
                <asp:HiddenField ID="hfAportes" runat="server" />
                <asp:HiddenField ID="hfCofinanciacion" runat="server" />
                <asp:HiddenField ID="hfVigenciasFuturas" runat="server" />
                <asp:HiddenField ID="hfLugarEjecucion" runat="server" />

                <asp:HiddenField ID="hfEsCambioVigencia" runat="server" />
                <asp:HiddenField ID="hfVigenciaInicial" runat="server" />
                <asp:HiddenField ID="hfVigenciaFinal" runat="server" />

</asp:Content>
