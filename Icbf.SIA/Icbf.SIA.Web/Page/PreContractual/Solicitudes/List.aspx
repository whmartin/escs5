<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_PreContractual_Solicitudes_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
 <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
                <tr class="rowB">
            <td class="Cell">
                Fecha Registro al Sistema Desde
            </td>
            <td class="Cell">
                Fecha Registro al Sistema hasta
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaDesde" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaHasta" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Solicitud</td>
            <td class="Cell">
                Vigencia Fiscal Inicial</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdSolicitud" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdSolicitud"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio</td>
            <td class="Cell">
                Modalidad de Selecci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDModalidadSeleccion"  ></asp:DropDownList>
            </td>
        </tr> 
        <tr class="rowB">
            <td class="Cell">
                Categoria del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged" 
                    AutoPostBack="True"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>     
          <tr class="rowB">
            <td class="Cell">
                Estado de la Solicitud</td>
            <td class="Cell">
                
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlEstadoSolicitud" 
                    AutoPostBack="True"  ></asp:DropDownList>
            </td>
            <td class="Cell">
            </td>
        </tr>     

    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSolicitudesContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdSolicitud" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvSolicitudesContrato_PageIndexChanging" OnSorting="gvSolicitudesContrato_Sorting" OnSelectedIndexChanged="gvSolicitudesContrato_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                                <asp:BoundField HeaderText="Fecha Registro al Sistema" DataField="FechaCrea"  SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText=" N&uacute;mero de Solicitud" DataField="IdSolicitud"  SortExpression="IdSolicitud"/>
                                <asp:BoundField HeaderText="Regional" DataField="Regional"  SortExpression="Regional"/>
                                <asp:BoundField HeaderText="Modalidad de Selecci&oacute;n" DataField="ModalidadSeleccion"  SortExpression="ModalidadSeleccion"/>
                                <asp:BoundField HeaderText="Dependencia" DataField="DependenciaSolicitante"  SortExpression="DependenciaSolicitante"/>
<%--                                <asp:BoundField HeaderText="Categoria del Contrato/Convenio" DataField="CategoriaContrato"  SortExpression="CategoriaContrato"/>--%>
                                <asp:BoundField HeaderText="Tipo de Contrato/Convenio" DataField="TipoContrato"  SortExpression="TipoContrato"/>
                                <asp:BoundField HeaderText="Estado" DataField="EstadoSolicitud"  SortExpression="EstadoSolicitud"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
