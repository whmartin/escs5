﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro de una solicitud de contrato
/// </summary>
public partial class Page_PreContractual_Validacion_Add : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "PreContractual/Validacion";

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    SIAService vSIAService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);

        if(! string.IsNullOrEmpty(hfIdActividadPadre.Value))
        NavigateTo("ListActividades.aspx?origen="+ hfIdActividadPadre.Value);
        else
        NavigateTo("ListActividades.aspx");
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Gest&oacute;n de Actividades de Solicitudes de Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            if(ValidarGuardar())
            {
                int result;

                SolicitudContratoActividadesDesarrolloDetalle item = new SolicitudContratoActividadesDesarrolloDetalle();

                if(!string.IsNullOrEmpty(txtFechaFin.Text))
                    item.FechaFinalizacion = DateTime.Parse(txtFechaFin.Text);
                if(!string.IsNullOrEmpty(txtFechaInicio.Text))
                    item.FechaInicial = DateTime.Parse(txtFechaInicio.Text);

                if(rblManejaAportesEspecie.SelectedValue == "1")
                {
                    item.AplicaPublicacion = true;

                    if(!string.IsNullOrEmpty(txtFechaPublicacion.Text))
                        item.FechaPublicacion = DateTime.Parse(txtFechaPublicacion.Text);
                }

                item.Observaciones = txtObservaciones.Text;

                if(string.IsNullOrEmpty(hfIdDesarrolloActividad.Value))
                {
                    item.UsuarioCrea = GetSessionUser().NombreUsuario;
                    item.IdActivdad = int.Parse(hfIdActividad.Value);
                    item.IdSolicitud = int.Parse(hfIdSolicitudContrato.Value);
                    result = vPrecontractualService.InsertarDesarrolloActividad(item);
                }
                else
                {
                    item.IdDesarrolloActividad = int.Parse(hfIdDesarrolloActividad.Value);
                    item.UsuarioModifica = GetSessionUser().NombreUsuario;
                    result = vPrecontractualService.ModificarDesarrolloActividad(item);
                }

                if(result > 0)
                {
                    SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);
                    SetSessionParameter("SolicitudContrato.GuardoDesarrollAct", "Se guardo y/o actualizo la gesti&oacute;n de la actividad.");

                    if(!string.IsNullOrEmpty(hfIdActividadPadre.Value))
                    {
                        string url = string.Format("Detail.aspx?idActividad={0}&idActividadPadre={1}", hfIdActividad.Value, hfIdActividadPadre.Value);
                        NavigateTo(url);
                    }
                    else
                    {
                        string url = string.Format("Detail.aspx?idActividad={0}", hfIdActividad.Value);
                        NavigateTo(url);
                    }
                }
                else
                    toolBar.MostrarMensajeError("Hubo un problema al intentar guardar, por favor intente nuevamente.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool ValidarGuardar()
    {
        bool isValid = true;

        StringBuilder mensajes = new StringBuilder();

        if(! String.IsNullOrEmpty(txtFechaInicio.Text))
        {
            DateTime fechaInicio;

            if(DateTime.TryParse(txtFechaInicio.Text, out fechaInicio))
            {
                if(!string.IsNullOrEmpty(txtFechaFin.Text))
                {
                    DateTime fechaFin;
                    if(DateTime.TryParse(txtFechaFin.Text, out fechaFin))
                    {
                        if(
                            rblManejaAportesEspecie.SelectedValue == "1" &&
                            !string.IsNullOrEmpty(txtFechaPublicacion.Text)
                          )
                        {
                            DateTime fechaPublicacion;
                            if(DateTime.TryParse(txtFechaPublicacion.Text, out fechaPublicacion))
                            {
                                if(fechaPublicacion < fechaInicio || fechaPublicacion > fechaFin)
                                    mensajes.AppendLine("Debe ingresar una fecha de publicaci&oacute;n mayor o igual a la fecha de inicio y menor o igual a la fecha de fin, verifique por favor.");
                            }
                            else
                                mensajes.AppendLine("Debe ingresar una fecha de publicaci&oacute;n valida, verifique por favor.");
                        }
                    }
                    else
                        mensajes.AppendLine("Debe ingresar una fecha de fin valida, verifique por favor.");
                }
            }
            else
                mensajes.AppendLine("Debe ingresar una fecha de inicio valida, verifique por favor.");
        }

        if(mensajes.Length > 0)
        {
            toolBar.MostrarMensajeError(mensajes.ToString().Replace("\n", "<br/>"));
            isValid = false;
        }

        return isValid;
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarModalidadSeleccion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            var idSolicitudContrato = int.Parse(GetSessionParameter("SolicitudContrato.IdSolicitud").ToString());
            hfIdSolicitudContrato.Value = idSolicitudContrato.ToString();
            RemoveSessionParameter("SolicitudContrato.IdSolicitud");

            var idActividad = int.Parse(Request.QueryString["idActividad"]);
            hfIdActividad.Value = idActividad.ToString();

            int? idActividadPadre = null;
            if(! string.IsNullOrEmpty(Request.QueryString["idActividadPadre"]))
            {
                idActividadPadre = int.Parse(Request.QueryString["idActividadPadre"]);
                hfIdActividadPadre.Value = idActividadPadre.Value.ToString();
                pnInfoSubActividad.Style.Add("display", "");
            }

            var itemSolicitud = vPrecontractualService.ConsultarDesarrolloActividadPorId(idSolicitudContrato,idActividad, idActividadPadre);

            if(itemSolicitud.IdDesarrolloActividad.HasValue)
            {
                hfIdDesarrolloActividad.Value = itemSolicitud.IdDesarrolloActividad.Value.ToString();
                pnDocumentos.Style.Add("display", "");
                gvDocumentos.EmptyDataText = EmptyDataText();
                gvDocumentos.PageSize = PageSize();
                gvDocumentos.DataSource = vPrecontractualService.ConsultarDocumentosPorIdDesarrolloActividad(itemSolicitud.IdDesarrolloActividad.Value);
                gvDocumentos.DataBind();
            }

            txtNumeroSolicitud.Text = itemSolicitud.IdSolicitud.ToString();
            txtNumeroRadicado.Text = itemSolicitud.NumeroRadico;
            txtEstado.Text = itemSolicitud.EstadoSolicitud.ToUpper();
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
            txtActividad.Text = itemSolicitud.NombreActividad.ToUpper();

            if(!string.IsNullOrEmpty(Request.QueryString["idActividadPadre"]))
                txtSubActividad.Text = itemSolicitud.NombreActividadPadre.ToUpper();

            cetFechaPublicacionI.EndDate = DateTime.Now;

            if(itemSolicitud.FechaInicial.HasValue)
            {
               txtFechaInicio.Text = itemSolicitud.FechaInicial.Value.ToShortDateString();
               cetFechaPublicacionF.StartDate = itemSolicitud.FechaInicial.Value;
               cetFechaPublicacionP.StartDate = itemSolicitud.FechaInicial.Value;
            }

            cetFechaPublicacionF.EndDate = DateTime.Now;

            if(itemSolicitud.FechaFinalizacion.HasValue)
            {
                txtFechaFin.Enabled = true;
                cetFechaPublicacionF.Enabled = true;
                txtFechaFin.Text = itemSolicitud.FechaFinalizacion.Value.ToShortDateString();
                cetFechaPublicacionP.Enabled = true;
                cetFechaPublicacionP.EndDate = itemSolicitud.FechaFinalizacion.Value;
                txtFechaPublicacion.Enabled = true;
            }

            if(itemSolicitud.AplicaPublicacion)
            {
                txtFechaPublicacion.Visible = true;
                rblManejaAportesEspecie.SelectedValue = "1";
                ImageFechaPublicacion.Visible = true;
                lblFechaPublicacion.Visible = true;

                if(itemSolicitud.FechaPublicacion.HasValue)
                {
                    txtFechaPublicacion.Text = itemSolicitud.FechaPublicacion.Value.ToShortDateString();
                    hfFechaPublicacion.Value = itemSolicitud.FechaPublicacion.Value.ToShortDateString();
                }
            }

            txtObservaciones.Text = itemSolicitud.Observaciones;
            txtEstado.Text = itemSolicitud.EstadoSolicitud.ToString().ToUpper();
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    protected void rblManejaAportesEspecie_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(rblManejaAportesEspecie.SelectedValue == "1")
        {
            txtFechaPublicacion.Visible = true;
            ImageFechaPublicacion.Visible = true;
            lblFechaPublicacion.Visible = true;
        }
        else
        {
            txtFechaPublicacion.Visible = false;
            ImageFechaPublicacion.Visible = false;
            lblFechaPublicacion.Visible = false;
        }
    }

    protected void txtFechaInicio_TextChanged(object sender, EventArgs e)
    {
        DateTime fechaInicio;
        bool isValidFechaInicio = DateTime.TryParse(txtFechaInicio.Text, out fechaInicio);

        if(isValidFechaInicio)
        {
            txtFechaFin.Enabled = true;
            cetFechaPublicacionF.Enabled = true;
            cetFechaPublicacionF.StartDate = fechaInicio;
            cetFechaPublicacionP.StartDate = fechaInicio;

            if(!string.IsNullOrEmpty(txtFechaFin.Text))
            {
                DateTime fechaFin = DateTime.MinValue;
                bool isValidFechaFin = DateTime.TryParse(txtFechaFin.Text, out fechaFin);

                if(isValidFechaFin && fechaFin >= fechaInicio)
                {
                    txtFechaPublicacion.Enabled = true;
                    cetFechaPublicacionP.Enabled = true;
                    cetFechaPublicacionP.EndDate = fechaFin;
                }
                else
                {
                    txtFechaPublicacion.Enabled = false;
                    txtFechaPublicacion.Text = String.Empty;
                    cetFechaPublicacionP.Enabled = false;
                }
            }
            else
            {
                txtFechaPublicacion.Enabled = false;
                txtFechaPublicacion.Text = String.Empty;
                cetFechaPublicacionP.Enabled = false;
            }
        }
        else
        {
            txtFechaFin.Enabled = false;
            txtFechaFin.Text = string.Empty;
            cetFechaPublicacionF.Enabled = false;

            txtFechaPublicacion.Enabled = false;
            txtFechaPublicacion.Text = string.Empty;
            cetFechaPublicacionP.Enabled = false;
            ClientScript.RegisterStartupScript(Page.GetType(), "ValidarFecha", "ValidarFecha('FechaInicio')");
        } 
    }

    protected void txtFechaFin_TextChanged(object sender, EventArgs e)
    {
        DateTime fechaFin;
        bool isValidFechaFin = DateTime.TryParse(txtFechaFin.Text, out fechaFin);

        if(isValidFechaFin)
        {
            txtFechaPublicacion.Enabled = true;
            cetFechaPublicacionP.Enabled = true;
            cetFechaPublicacionP.EndDate = fechaFin;
        }
        else
        {
            txtFechaPublicacion.Enabled = false;
            txtFechaPublicacion.Text = string.Empty;
            cetFechaPublicacionP.Enabled = false;
            ClientScript.RegisterStartupScript(Page.GetType(), "ValidarFecha", "ValidarFecha('FechaFinal')");
        }
    }

    protected void btnCargarDocumento_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int idDesarrollActividad = int.Parse(hfIdDesarrolloActividad.Value);

            ManejoControlesContratos controles = new ManejoControlesContratos();
            var guardo = controles.CargarArchivoFTPActividadSolicitudContrato
                (
                 FileUploadArchivoContrato,
                 hfIdSolicitudContrato.Value.ToString(),
                 idDesarrollActividad,
                 GetSessionUser().NombreUsuario
                 );

            if(guardo)
            {
                gvDocumentos.DataSource = vPrecontractualService.ConsultarDocumentosPorIdDesarrolloActividad(idDesarrollActividad);
                gvDocumentos.DataBind();
            }

        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void btnEliminarDocumentos_Click(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            var idDocumento = Convert.ToInt32(gvDocumentos.DataKeys[rowIndex]["IdDocumento"]);
            int result = vPrecontractualService.EliminarSDesarrolloActividadDocumentos(idDocumento);

            if(result > 0)
            {
                int idDesarrolloActividad = int.Parse(hfIdDesarrolloActividad.Value);
                gvDocumentos.DataSource = vPrecontractualService.ConsultarDocumentosPorIdDesarrolloActividad(idDesarrolloActividad);
                gvDocumentos.DataBind();
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}

