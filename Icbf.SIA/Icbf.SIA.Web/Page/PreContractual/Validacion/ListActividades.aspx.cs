﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;

/// <summary>
/// Página que despliega el detalle del registro de una solicitud de contrato
/// </summary>
public partial class Page_PreContractual_Validacion_ListActividades : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "PreContractual/Validacion";

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    SIAService vSIAService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if(! string.IsNullOrEmpty(hfIdActividadPadre.Value))
        {
            SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);
            NavigateTo("ListActividades.aspx");
        }
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Gest&oacute;n de Actividades de Solicitudes de Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarCategoriaContrato();
            CargarTipodeContrato();
            CargarModalidadSeleccion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            var idSolicitudContrato = GetSessionParameter("SolicitudContrato.IdSolicitud").ToString();
            hfIdSolicitudContrato.Value = idSolicitudContrato.ToString();
            RemoveSessionParameter("SolicitudContrato.IdSolicitud");

            var origen = Request.QueryString["origen"];

            List<SolicitudContratoActividadesDesarrollo> listaDesarrollo;

            if(!string.IsNullOrEmpty(GetSessionParameter("SolicitudContrato.FinalizoGestion").ToString()))
            {
                toolBar.MostrarMensajeGuardado(GetSessionParameter("SolicitudContrato.FinalizoGestion").ToString());
                RemoveSessionParameter("SolicitudContrato.FinalizoGestion");
            }

            if(!string.IsNullOrEmpty(origen))
            {
                 hfIdActividadPadre.Value = origen;
                gcDesarrolloActividades.Columns[6].Visible = false;
                toolBar.EstablecerTitulos("Gest&oacute;n de Sub Actividades de Solicitudes de Contrato", SolutionPage.Detail.ToString());
                listaDesarrollo = vPrecontractualService.ConsultarDesarrolloActividadPorIdActividad(int.Parse(hfIdSolicitudContrato.Value),int.Parse(hfIdActividadPadre.Value));
            }
            else
            {
                gcDesarrolloActividades.Columns[6].Visible = true;
                listaDesarrollo = vPrecontractualService.ConsultarDesarrolloActividadPorId(int.Parse(hfIdSolicitudContrato.Value));
            }

            var itemSolicitud = vPrecontractualService.ConsultarSolicitudPorId(int.Parse(idSolicitudContrato));
            txtFechaRegistroSsistema.Text = itemSolicitud.FechaCrea.ToShortDateString();
            txtNombreUsuario.Text = itemSolicitud.UsuarioCrea.ToString();
            txtNumeroSolicitud.Text = itemSolicitud.IdSolicitud.ToString();
            txtEstado.Text = itemSolicitud.EstadoSolicitud.ToString().ToUpper();
            txtRegionalUsuario.Text = itemSolicitud.Regional.ToUpper();
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
            ddlCategoriaContrato.SelectedValue = itemSolicitud.IdCategoriaContrato.ToString();
            CargarTipodeContrato();
            ddlTipoContratoConvenio.SelectedValue = itemSolicitud.IdTipoContrato.ToString();

            TxtFechaInicio.Text = itemSolicitud.VigenciaInicial.ToString();
            TxtFechaFin.Text = itemSolicitud.VigenciaFinal.ToString();

            txtObjeto.Text = itemSolicitud.Objeto;
            txtAlcance.Text = itemSolicitud.Alcance;

            if (itemSolicitud.ValorContrato.HasValue)
                txtValorContrato.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContrato.Value);

            gcDesarrolloActividades.DataSource = listaDesarrollo;
            gcDesarrolloActividades.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarCategoriaContrato()
    {
        ddlCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, true);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
        }
        ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTipodeContrato()
    {
        string idCategoriaSeleccionada = ddlCategoriaContrato.SelectedValue;

        if (idCategoriaSeleccionada != "-1")
        {
            ddlTipoContratoConvenio.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), true, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
        }
    }

    protected void gcDesarrolloActividades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gcDesarrolloActividades.PageIndex = e.NewPageIndex;

            List<SolicitudContratoActividadesDesarrollo> listaDesarrollo;

            if(!string.IsNullOrEmpty(hfIdActividadPadre.Value))
                listaDesarrollo = vPrecontractualService.ConsultarDesarrolloActividadPorIdActividad(int.Parse(hfIdSolicitudContrato.Value), int.Parse(hfIdActividadPadre.Value));
            else
                listaDesarrollo = vPrecontractualService.ConsultarDesarrolloActividadPorId(int.Parse(hfIdSolicitudContrato.Value));

            gcDesarrolloActividades.DataSource = listaDesarrollo;
            gcDesarrolloActividades.DataBind();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gcDesarrolloActividades_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            bool esPadre = bool.Parse(DataBinder.Eval(e.Row.DataItem, "EsPadre").ToString());

            if(esPadre)
            {
                int cantidad = int.Parse(DataBinder.Eval(e.Row.DataItem, "Cantidad").ToString());

                if(cantidad > 0)
                {
                    e.Row.FindControl("btnInfo").Visible = true;
                    e.Row.FindControl("btnDetalle").Visible = false;
                }
                else
                {
                    e.Row.FindControl("btnInfo").Visible = false;
                    e.Row.FindControl("btnDetalle").Visible = true;
                }
            }
            else
            {
                e.Row.FindControl("btnInfo").Visible = false;
                e.Row.FindControl("btnDetalle").Visible = true;
            }
        }
    }

    protected void btnDetalle_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((ImageButton)sender).CommandArgument);
            string idDesarrolloActividad = gcDesarrolloActividades.DataKeys[rowIndex].Values[0].ToString();
            string idActividad = gcDesarrolloActividades.DataKeys[rowIndex].Values[1].ToString();
            SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);

            if(idDesarrolloActividad == "0")
            {
                if(!string.IsNullOrEmpty(hfIdActividadPadre.Value))
                {
                    string url = string.Format("Add.aspx?idActividad={0}&idActividadPadre={1}", idActividad,hfIdActividadPadre.Value);
                    NavigateTo(url);
                }
                else
                    NavigateTo("Add.aspx?idActividad=" + idActividad);
            }
            else
            {
                if(!string.IsNullOrEmpty(hfIdActividadPadre.Value))
                {
                    string url = string.Format("Detail.aspx?idActividad={0}&idActividadPadre={1}", idActividad, hfIdActividadPadre.Value);
                    NavigateTo(url);
                }
                else
                {
                    string url = string.Format("Detail.aspx?idActividad={0}", idActividad);
                    NavigateTo(url);
                }
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((ImageButton)sender).CommandArgument);
            string idActividad = gcDesarrolloActividades.DataKeys[rowIndex].Values[1].ToString();
            SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);
            NavigateTo("~/Page/Precontractual/Validacion/ListActividades.aspx?origen=" + idActividad);
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}

