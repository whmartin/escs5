﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_PreContractual_Validacion_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">

            function ValidarFecha(valor) {

                if (valor == 'FechaInicio') {
                    alert('La fecha de inicio es invalida, revise por favor.');
                }
                else if (valor == 'FechaFinal') {
                    alert('La fecha final es invalida, revise por favor.');
                }
            }

            function ValidaEliminacion() {

                return confirm('¿Esta seguro que desea eliminar este documento ?');
            }

        </script>

                    <table width="90%"  id="pnInfoUsuario" align="center">
                     <tr class="rowB">
                        <td class="Cell">
                          N&uacute;mero de Solicitud   
                        </td>
                        <td class="Cell">
                          N&uacute;mero de Radicado                             
                        </td>
                    </tr>
                     <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                          </td>
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroRadicado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                          </td>
                    </tr>   
                    <tr class="rowB">
                        <td class="Cell">
                            Estado
                            Proceso</td>
                        <td class="Cell">
                                Modalidad de Selecci&oacute;n 
                        </td>
                    </tr>
                    <tr class="rowA">
                    <td class="Cell">
                         <asp:TextBox ID="txtEstado" runat="server" Enabled="false" Width="80%"></asp:TextBox>    
                        </td>
                        <td class="Cell">
                            <asp:DropDownList ID="ddlModalidadSeleccion" Enabled="false" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr class="rowB">
                        <td colspan="2">
                            Actividad
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2" class="Cell">
                                <asp:TextBox ID="txtActividad" runat="server" Enabled="false" Width="90%"></asp:TextBox>
                          </td>
                    </tr>       
                    </table>
                    <table width="90%"  id="pnInfoSubActividad" runat="server" style="display:none" align="center">
                        <tr class="rowB">
                            <td colspan="2">
                              Actividad Padre                                          
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2" class="Cell">
                           <asp:TextBox ID="txtSubActividad" runat="server" Enabled="false"  Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table  width="90%"  align="center">
                       <tr class="rowB">
                        <td class="Cell">
                          Fecha de Inicio de Gesti&oacute;n *
                        <%--<asp:RequiredFieldValidator runat="server" ID="rfvFechaInicio" ControlToValidate="txtFechaInicio"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido"  Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red" Enabled="true"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td class="Cell">
                          Fecha de Fin de Gesti&oacute;n
                        </td>
                    </tr>
                     <tr class="rowA">
                        <td class="Cell">
                       <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                                <asp:TextBox ID="txtFechaInicio" runat="server" Enabled="true" Width="80%" AutoPostBack="true" OnTextChanged="txtFechaInicio_TextChanged"></asp:TextBox>
                                <asp:Image ID="imgFechaPublicacionI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                                <Ajax:CalendarExtender ID="cetFechaPublicacionI" Enabled="true" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaPublicacionI" TargetControlID="txtFechaInicio"></Ajax:CalendarExtender>
<%--                                <Ajax:MaskedEditExtender ID="meeFechaPublicacionI"  runat="server" CultureAMPMPlaceholder="AM;PM"
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                </Ajax:MaskedEditExtender> --%>
                            </ContentTemplate>
                           <Triggers>
                               <asp:AsyncPostBackTrigger ControlID="txtFechaInicio" EventName="TextChanged" />
                           </Triggers>
                           </asp:UpdatePanel>
                        </td>
                        <td class="Cell">
                       <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                                <asp:TextBox ID="txtFechaFin" runat="server" Enabled="false" AutoPostBack="true" OnTextChanged="txtFechaFin_TextChanged" Width="80%"></asp:TextBox>
                                <asp:Image ID="ImageFechaFin" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                                <Ajax:CalendarExtender ID="cetFechaPublicacionF" Enabled="false" runat="server" Format="dd/MM/yyyy" PopupButtonID="ImageFechaFin" TargetControlID="txtFechaFin"></Ajax:CalendarExtender>
                                <Ajax:MaskedEditExtender ID="meeFechaPublicacionF"  runat="server" CultureAMPMPlaceholder="AM;PM"
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaFin">
                                </Ajax:MaskedEditExtender> 
                            </ContentTemplate>
                            <Triggers>
                               <asp:AsyncPostBackTrigger ControlID="txtFechaFin" EventName="TextChanged" />
                           </Triggers>
                        </asp:UpdatePanel>
                        </td>
                    </tr>   
                           <tr class="rowB">
                        <td class="Cell">
                            Aplica Publicaci&oacute;n</td>
                        <td class="Cell">
                         <asp:Label id="lblFechaPublicacion" Visible="false"  Text="Fecha de Fin de Gesti&oacute;n" runat="server" />
                        </td>
                    </tr>
                     <tr class="rowA">
                        <td class="Cell">
                       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                                <span>
                                    <asp:RadioButtonList runat="server" ID="rblManejaAportesEspecie" AutoPostBack="true" OnSelectedIndexChanged="rblManejaAportesEspecie_SelectedIndexChanged" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Si" Value="1" />
                                    <asp:ListItem Text="No" Value="0"  Selected="True" />
                                    </asp:RadioButtonList>  
                                </span>
                        </ContentTemplate>
                           <Triggers>
                               <asp:AsyncPostBackTrigger ControlID="rblManejaAportesEspecie" EventName="SelectedIndexChanged" />
                           </Triggers>
                      </asp:UpdatePanel>
                        </td>
                        <td class="Cell">
                                  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>     
                                                <asp:TextBox ID="txtFechaPublicacion" Visible="false" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                                <asp:Image ID="ImageFechaPublicacion" runat="server" CssClass="bN" Visible="false" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                                                <Ajax:CalendarExtender ID="cetFechaPublicacionP" Enabled="false"  runat="server" Format="dd/MM/yyyy" PopupButtonID="ImageFechaPublicacion" TargetControlID="txtFechaPublicacion"></Ajax:CalendarExtender>
                                                <Ajax:MaskedEditExtender ID="meeFechaPublicacionP" runat="server" CultureAMPMPlaceholder="AM;PM"
                                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaPublicacion">
                                                </Ajax:MaskedEditExtender> 
                                            </ContentTemplate>
                               </asp:UpdatePanel>
                        </td>
                    </tr> 
                    <tr class="rowB">
                        <td colspan="2">
                            Observaciones *
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtObservaciones"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                            ForeColor="Red" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2" class="Cell">
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" MaxLength="500" runat="server" Enabled="true" Width="90%" Height="46px"></asp:TextBox>
                          </td>
                    </tr>     
               </table> 
                    <table width="90%"  id="pnDocumentos" runat="server" style="display:none" align="center"> 
                           <tr class="rowB">
                            <td>
                               <asp:FileUpload Width="100%" ID="FileUploadArchivoContrato" runat="server" />
                            </td>
                       <td>
                           <asp:ImageButton ID="btnCargarDocumento" runat="server" CommandName="Select" Height="30px" ImageUrl="~/Image/btn/apply.png" OnClick="btnCargarDocumento_Click" ToolTip="Agregar" Width="30px" />
                       </td>
                        </tr> 
                           <tr class="rowB">
                                        <td colspan="2">
                                            <asp:GridView ID="gvDocumentos" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IdDocumento"  GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=SolicitudesContrato/"+hfIdSolicitudContrato.Value+"/") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="Documento" DataField="NombreOriginal" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminarDocumentos" runat="server" OnClick="btnEliminarDocumentos_Click"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                   </table>

                <asp:HiddenField ID="hfIdSolicitudContrato" runat="server" />
                <asp:HiddenField ID="hfIdActividad" runat="server" />
                <asp:HiddenField ID="hfIdActividadPadre" runat="server" />
                <asp:HiddenField ID="hfIdDesarrolloActividad" runat="server" />
                <asp:HiddenField ID="hfFechaPublicacion" runat="server" />
                </asp:Content>
