﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_PreContractual_Validacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">

              function ConfirmarFinalizar() {

                  return confirm('¿Esta seguro de que desea finalizar la gestión de la activdad?');
            }

        </script>

                    <table width="90%"  id="pnInfoUsuario" align="center">
                     <tr class="rowB">
                        <td class="Cell">
                          N&uacute;mero de Solicitud   
                        </td>
                        <td class="Cell">
                          N&uacute;mero de Radicado                             
                        </td>
                    </tr>
                     <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                          </td>
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroRadicado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                          </td>
                    </tr>   
                    <tr class="rowB">
                        <td class="Cell">
                            Estado
                            Proceso</td>
                        <td class="Cell">
                                Modalidad de Selecci&oacute;n 
                        </td>
                    </tr>
                    <tr class="rowA">
                    <td class="Cell">
                         <asp:TextBox ID="txtEstado" runat="server" Enabled="false" Width="80%"></asp:TextBox>    
                        </td>
                        <td class="Cell">
                            <asp:DropDownList ID="ddlModalidadSeleccion" Enabled="false" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr class="rowB">
                        <td colspan="2">
                            Actividad
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2" class="Cell">
                                <asp:TextBox ID="txtActividad" runat="server" Enabled="false" Width="90%"></asp:TextBox>
                          </td>
                    </tr>       
                    </table>
                    <table width="90%"  id="pnInfoSubActividad" runat="server" style="display:none" align="center">
                        <tr class="rowB">
                            <td colspan="2">
                              Actividad Padre                                           
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td colspan="2" class="Cell">
                           <asp:TextBox ID="txtSubActividad" runat="server" Enabled="false"  Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                   <table  width="90%"  align="center">
                       <tr class="rowB">
                        <td class="Cell">
                          Fecha de Inicio de Gesti&oacute;n
                        </td>
                        <td class="Cell">
                          Fecha de Fin de Gesti&oacute;n
                        </td>
                    </tr>
                     <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtFechaInicio" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                <asp:Image ID="imgFechaPublicacionI" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                        </td>
                        <td class="Cell">
                                <asp:TextBox ID="txtFechaFin" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                <asp:Image ID="ImageFechaFin" runat="server" CssClass="bN" Visible="true" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                        </td>
                    </tr>   
                           <tr class="rowB">
                        <td class="Cell">
                            Aplica Publicaci&oacute;n</td>
                        <td class="Cell">
                          Fecha de Fin de Gesti&oacute;n
                        </td>
                    </tr>
                     <tr class="rowA">
                        <td class="Cell">

                            <span>
                                             <asp:RadioButtonList runat="server" ID="rblManejaAportesEspecie" Enabled="false" RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="1" />
                                                 <asp:ListItem Text="No" Value="0"  Selected="True" />
                                             </asp:RadioButtonList>  
                                        </span>

                        </td>
                        <td class="Cell">
                                <asp:TextBox ID="txtFechaPublicacion" Visible="false" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                <asp:Image ID="ImageFechaPublicacion" runat="server" CssClass="bN" Visible="false" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                        </td>
                    </tr> 
                    <tr class="rowB">
                        <td colspan="2">
                            Observaciones
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td colspan="2" class="Cell">
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" MaxLength="250" runat="server" Enabled="false" Width="90%" Height="46px"></asp:TextBox>
                          </td>
                    </tr>         
                 </table>
                    <table width="90%"  id="pnDocumentos" runat="server" style="display:none" align="center"> 
                           <tr class="rowB">
                                        <td colspan="2">
                                            <asp:GridView ID="gvDocumentos" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IdDocumento"  GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=SolicitudesContrato/"+hfIdSolicitudContrato.Value+"/") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="Documento" DataField="NombreOriginal" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                   </table>

                <asp:HiddenField ID="hfIdSolicitudContrato" runat="server" />
                <asp:HiddenField ID="hfIdActividad" runat="server" />
                <asp:HiddenField ID="hfIdActividadPadre" runat="server" />
                <asp:HiddenField ID="hfIdDesarrolloActividad" runat="server" />
                </asp:Content>
