﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListActividades.aspx.cs" Inherits="Page_PreContractual_Validacion_ListActividades" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">


        </script>

                    <table width="90%"  id="pnInfoUsuario" align="center">
                    <tr class="rowB">
                        <td>
                            Nombre del usuario Solicitud
                        </td>
                        <td>
                            Regional del usuario Solicitud                                                     
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNombreUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtRegionalUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>                 
                    </table>
                    <table width="90%" id="pnInfoSolicitud" runat="server" align="center" style="display:none">
                    <tr class="rowB">
                        <td>
                            Número de Solicitud
                        </td>
                        <td>
                            Fecha de Registro en el Sistema                                                 
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtFechaRegistroSsistema" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="90%" id="pnInfoCategoriaContrato" align="center">
                    <tr class="rowB">
                        <td class="Cell">
                            Estado
                        </td>
                        <td class="Cell">
                                Modalidad de Selecci&oacute;n 
                        </td>
                    </tr>
                    <tr class="rowA">
                    <td class="Cell">
                         <asp:TextBox ID="txtEstado" runat="server" Enabled="false" Width="80%"></asp:TextBox>    
                        </td>
                        <td class="Cell">
                            <asp:DropDownList ID="ddlModalidadSeleccion" Enabled="false" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="Cell">
                        Categoría Contrato/Convenio                                                 
                    </td>
                        <td class="Cell">
                            Tipo de Contrato/Convenio 
                        </td>
                    </tr>
                    <tr class="rowA">
                            <td class="Cell">
                            <asp:DropDownList ID="ddlCategoriaContrato" Enabled="false" runat="server" >
                            </asp:DropDownList>
                            </td>
                            <td class="Cell">
                            <asp:DropDownList ID="ddlTipoContratoConvenio" Enabled="false" runat="server" />
                        </td>
                    </tr>
                    </table>
                    <table width="90%" id="PnInfoTipoContrato" align="center">
                        <tr class="rowB">
                            <td class="Cell">
                                Vigencia Incial del Contrato/Convenio
                            </td>
                            <td class="Cell">
                                Vigencia Final del Contrato/Convenio
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                    <asp:TextBox ID="TxtFechaInicio" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                            <td class="Cell">
                                    <asp:TextBox ID="TxtFechaFin" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                        </tr>  
                    </table>
                    <table width="90%"  id="pnInfoSolcitantes" runat="server" align="center">
                               <tr class="rowB">
                                        <td>
                                            Objeto del Contrato/Convenio 
                                        </td>
                                        <td>
                                            Alcance del Objeto del Contrato/Convenio 
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtObjeto" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                 AutoPostBack="true" 
                                                MaxLength="500" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtAlcance" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                MaxLength="4000" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);"></asp:TextBox>
                                        </td>
                                    </tr>
                               <tr class="rowB">
                            <td>
                               Valor del Contrato
                            </td>
                            <td>
                                                               Número de Radicado 
                                
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                <asp:TextBox ID="txtValorContrato" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                               <asp:TextBox ID="txtNumeroRadicado" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>        
                    </table>
                    <table  runat="server" width="90%" align="center">
                             <tr class="rowA">
                            <td colspan="2">
                             <asp:GridView runat="server" ID="gcDesarrolloActividades" Visible="true" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="90%" DataKeyNames="IdDesarrolloActividad,IdActivdad,EsPadre,Cantidad" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gcDesarrolloActividades_PageIndexChanging" OnRowDataBound="gcDesarrolloActividades_RowDataBound">
                        <Columns>  
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDetalle" runat="server" OnClick="btnDetalle_Click" ImageUrl="~/Image/btn/edit.gif" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                        Height="16px" Width="16px" ToolTip="Editar" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                                <asp:BoundField HeaderText="Actividad" DataField="Nombre"  SortExpression="Nombre"/>
                                <asp:BoundField HeaderText="Fecha Inicio" DataField="FechaInicial"  SortExpression="FechaInicial" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Fecha Finalizaci&oacute;n" DataField="FechaFinalizacion"  SortExpression="FechaFinalizacion" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Fecha Publicaci&oacute;n" DataField="FechaPublicacion"  SortExpression="FechaPublicacion" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Estado" DataField="FinalizadaView" SortExpression="FinalizadaView"/>
                                <asp:TemplateField HeaderText="Sub Actividades" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" OnClick="btnInfo_Click" ImageUrl="~/Image/btn/info.jpg" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView> 
                            
                            </td>
                        </tr>
                    </table>

                <asp:HiddenField ID="hfIdSolicitudContrato" runat="server" />
                <asp:HiddenField ID="hfIdActividadPadre" runat="server" />

</asp:Content>
