﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro de una solicitud de contrato
/// </summary>
public partial class Page_PreContractual_Validacion_Detail : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "PreContractual/Validacion";

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    SIAService vSIAService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);

        if(! string.IsNullOrEmpty(hfIdActividadPadre.Value))
        NavigateTo("ListActividades.aspx?origen="+ hfIdActividadPadre.Value);
        else
        NavigateTo("ListActividades.aspx");
    }

    private void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);

        if(!string.IsNullOrEmpty(hfIdActividadPadre.Value))
        {
            string url = string.Format("Add.aspx?idActividad={0}&idActividadPadre={1}", hfIdActividad.Value, hfIdActividadPadre.Value);
            NavigateTo(url);
        }
        else
        {
            string url = string.Format("Add.aspx?idActividad={0}", hfIdActividad.Value);
            NavigateTo(url);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.SetAprobarConfirmation("return ConfirmarFinalizar();");
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Gest&oacute;n de Actividades de Solicitudes de Contrato - Detalle", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
            if(ValidarAprobar())
            {
                int idDesarrolloActividad = int.Parse(hfIdDesarrolloActividad.Value);
                int result =  vPrecontractualService.FinalizarGestionActividad(idDesarrolloActividad);

                if(result > 0)
                {
                    SetSessionParameter("SolicitudContrato.IdSolicitud", hfIdSolicitudContrato.Value);
                    SetSessionParameter("SolicitudContrato.FinalizoGestion", string.Format("Se finalizo la gestión de la actividad: {0}", txtActividad.Text.ToUpper()));

                    if(!string.IsNullOrEmpty(hfIdActividadPadre.Value))
                        NavigateTo("ListActividades.aspx?origen=" + hfIdActividadPadre.Value);
                    else
                        NavigateTo("ListActividades.aspx");
                }
                else
                    toolBar.MostrarMensajeError("Hubo un problema al intentar finalizar la gestión, por favor intente nuevamente.");
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool ValidarAprobar()
    {
        bool isValid = true;

        StringBuilder mensajes = new StringBuilder();

        DateTime fechaInicio;
        if(DateTime.TryParse(txtFechaInicio.Text, out fechaInicio))
        {
                DateTime fechaFin;
                if(DateTime.TryParse(txtFechaFin.Text, out fechaFin))
                {
                    if(fechaInicio > fechaFin)
                    mensajes.AppendLine("Debe ingresar una fecha de fin mayor o igual a la fecha inicio, verifique por favor.");

                   if(rblManejaAportesEspecie.SelectedValue == "1" )
                    {
                        DateTime fechaPublicacion;
                        if(DateTime.TryParse(txtFechaPublicacion.Text, out fechaPublicacion))
                        {
                            if(fechaPublicacion < fechaInicio || fechaPublicacion > fechaFin)
                                mensajes.AppendLine("Debe ingresar una fecha de publicaci&oacute;n mayor o igual a la fecha de inicio y menor o igual a la fecha de fin, verifique por favor.");
                        }
                        else
                            mensajes.AppendLine("Debe ingresar una fecha de publicaci&oacute;n valida, verifique por favor.");
                    }
                }
                else
                    mensajes.AppendLine("Debe ingresar una fecha de fin valida, verifique por favor.");
        }
        else
            mensajes.AppendLine("Debe ingresar una fecha de inicio valida, verifique por favor.");

        if(mensajes.Length > 0)
        {
            toolBar.MostrarMensajeError(mensajes.ToString().Replace("\n", "<br/>"));
            isValid = false;
        }

        return isValid;
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarModalidadSeleccion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            var idSolicitudContrato = int.Parse(GetSessionParameter("SolicitudContrato.IdSolicitud").ToString());
            hfIdSolicitudContrato.Value = idSolicitudContrato.ToString();
            RemoveSessionParameter("SolicitudContrato.IdSolicitud");

            if(!string.IsNullOrEmpty(GetSessionParameter("SolicitudContrato.GuardoDesarrollAct").ToString()))
            {
                toolBar.MostrarMensajeGuardado(GetSessionParameter("SolicitudContrato.GuardoDesarrollAct").ToString());
                RemoveSessionParameter("SolicitudContrato.GuardoDesarrollAct");
            }

            var idActividad = int.Parse(Request.QueryString["idActividad"]);
            hfIdActividad.Value = idActividad.ToString();

            int? idActividadPadre = null;
            if(! string.IsNullOrEmpty(Request.QueryString["idActividadPadre"]))
            {
                idActividadPadre = int.Parse(Request.QueryString["idActividadPadre"]);
                hfIdActividadPadre.Value = idActividadPadre.Value.ToString();
                pnInfoSubActividad.Style.Add("display", "");
            }

            var itemSolicitud = vPrecontractualService.ConsultarDesarrolloActividadPorId(idSolicitudContrato,idActividad, idActividadPadre);

            if(itemSolicitud.IdDesarrolloActividad.HasValue)
            {
                hfIdDesarrolloActividad.Value = itemSolicitud.IdDesarrolloActividad.Value.ToString();
                pnDocumentos.Style.Add("display", "");
                gvDocumentos.EmptyDataText = EmptyDataText();
                gvDocumentos.PageSize = PageSize();
                gvDocumentos.DataSource = vPrecontractualService.ConsultarDocumentosPorIdDesarrolloActividad(itemSolicitud.IdDesarrolloActividad.Value);
                gvDocumentos.DataBind();
            }

            txtNumeroSolicitud.Text = itemSolicitud.IdSolicitud.ToString();
            txtNumeroRadicado.Text = itemSolicitud.NumeroRadico;
            txtEstado.Text = itemSolicitud.EstadoSolicitud.ToUpper();
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
            txtActividad.Text = itemSolicitud.NombreActividad.ToUpper();

            if(!string.IsNullOrEmpty(Request.QueryString["idActividadPadre"]))
                txtSubActividad.Text = itemSolicitud.NombreActividadPadre.ToUpper();

            if(itemSolicitud.FechaInicial.HasValue)
               txtFechaInicio.Text = itemSolicitud.FechaInicial.Value.ToShortDateString();

            if(itemSolicitud.FechaFinalizacion.HasValue)
                txtFechaFin.Text = itemSolicitud.FechaFinalizacion.Value.ToShortDateString();

            if(itemSolicitud.AplicaPublicacion)
            {
                txtFechaPublicacion.Visible = true;
                rblManejaAportesEspecie.SelectedValue = "1";
                ImageFechaPublicacion.Visible = true;

                if(itemSolicitud.FechaPublicacion.HasValue)
                    txtFechaPublicacion.Text = itemSolicitud.FechaPublicacion.Value.ToShortDateString();
            }

            txtObservaciones.Text = itemSolicitud.Observaciones;
            txtEstado.Text = itemSolicitud.EstadoSolicitud.ToString().ToUpper();
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();

            if(itemSolicitud.Finalizada.Value)
            {
                toolBar.MostrarBotonAprobar(false);
                toolBar.MostrarBotonEditar(false);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }
}

