<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"  AutoEventWireup="true" CodeFile="LupaPlanCompras.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaPlanCompras" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

                                 <table width="100%"  id="pnDetallePlanCompras" align="center" runat="server"  >
                                   <tr class="rowB">
                                        <td>
                                          No  Plan de Compras
                                        </td>
                                        <td>
                                           Valor Plan de Compras
                                        </td>
                                    </tr>

                                                                         <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="false" Width="30%" ViewStateMode="Enabled"
                                                ></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorInicialContConv" runat="server" Enabled="false" 
                                                MaxLength="25"
                                                Width="300px"></asp:TextBox>
                                        </td>
                                    </tr>

                                     <tr class="rowB">
                                        <td>
                                            Productos
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="N&uacute;mero Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="C&oacute;digo del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />
                                                    <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Rubros Plan de Compras
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" DataKeyNames="total_rubro" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="N&uacute;mero Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="C&oacute;digo Rubro" DataField="codigo_rubro" />
                                                    <asp:BoundField HeaderText="Descripci&oacute;n del Rubro" DataField="NombreRubro" />
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" DataField="" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                             </table>
</asp:Content>
