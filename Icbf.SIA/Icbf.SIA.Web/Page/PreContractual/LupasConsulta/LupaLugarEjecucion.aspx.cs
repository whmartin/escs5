using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.RUBO.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;
using Icbf.Contrato.Entity.PreContractual;
using System.Web.Security;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Precontractual_LupasConsulta_LupaLugarEjecucion : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    PreContractualService vPrecontractualService = new PreContractualService();

    ContratoService vContratoService = new ContratoService();

    SIAService vSIAService = new SIAService();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.EstablecerTitulos("Lugar de Ejecución", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int idContrato = int.Parse(Request.QueryString["idContrato"]);

            gvLugaresEjecucion.EmptyDataText = EmptyDataText();
            gvLugaresEjecucion.PageSize = PageSize();
            var lugarEjecucion = vContratoService.ConsultarLugaresEjecucionContrato(idContrato);
            gvLugaresEjecucion.DataSource = lugarEjecucion;
            gvLugaresEjecucion.DataBind();

            if (lugarEjecucion != null && lugarEjecucion.Count > 0)
                txtDatosAdicionalesLugarEjecucion.Text = lugarEjecucion.First().DatosAdicionales;

            if (lugarEjecucion.Count() > 2)
            {
                dvLugaresEjecucion.Style.Add("height", "100px");
                dvLugaresEjecucion.Style.Add("overflow-x", "hidden");
                dvLugaresEjecucion.Style.Add("overflow-y", "scroll");
            }
            else
            {
                dvLugaresEjecucion.Style.Remove("height");
                dvLugaresEjecucion.Style.Remove("overflow-x");
                dvLugaresEjecucion.Style.Remove("overflow-y");
            }

            if (lugarEjecucion.Count == 0)
                dvLugaresEjecucion.Style.Add("display", "none");
            else
                dvLugaresEjecucion.Style.Add("display", "");

            chkNivelNacional.Checked = lugarEjecucion.Count == 0 ? true : false;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion 

}
