<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaDocumentos.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaDocumentos" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <table width="90%" align="center">
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvDocumentos" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IdSolicitudContratoDocumento" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=SolicitudesContrato") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="Documento" DataField="TipoDocumento" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
        </table>
              
</asp:Content>
