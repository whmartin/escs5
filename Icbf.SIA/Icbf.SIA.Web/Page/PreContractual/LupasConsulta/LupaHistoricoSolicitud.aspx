<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaHistoricoSolicitud.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaHistoricoSolicitud" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <table width="90%" align="center">
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvHistoricoSolicitud" runat="server" DataKeyNames="IdSolicitudContratoGestion" AutoGenerateColumns="false" 
                                                AllowPaging="True"  GridLines="None" OnPageIndexChanging="gvHistoricoSolicitud_PageIndexChanging"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Usuario Gestiono" DataField="Usuario" />
                                                    <asp:BoundField HeaderText="Fecha Registro" DataField="FechaRegistro" DataFormatString="{0:dd/MM/yyyy}"  />
                                                    <asp:BoundField HeaderText="Observaciones" DataField="Observaciones"  />
                                                    <asp:BoundField HeaderText="Estado Solicitud" DataField="EstadoSolicitud"  />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
        </table>
              
</asp:Content>
