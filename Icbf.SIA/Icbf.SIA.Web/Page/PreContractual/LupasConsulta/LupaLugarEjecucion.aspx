<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaLugarEjecucion.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaLugarEjecucion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

              <table width="100%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Datos Adicionales lugar de ejecuci&oacute;n
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:TextBox ID="txtDatosAdicionalesLugarEjecucion" runat="server" Width="80%" TextMode="MultiLine" Enabled="false" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkNivelNacional" runat="server" Enabled="false" />
                                            Nivel Nacional
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <div id="dvLugaresEjecucion" runat="server">
                                                <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos"
                                                    AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                        <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>          


</asp:Content>
