<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaAportes.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaAportes" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
                                    <table width="100%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Valor ICBF 
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Valor Cofinanciaci&#243;n Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoICBF" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                               ></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoContrat" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                               ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes ICBF
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false"  DataKeyNames="IdAporteContrato,TipoAporte,TipoAportante"
                                                 GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Aportante" DataField="TipoAportante"  />
                                                    <asp:BoundField HeaderText="Tipo de Aporte" DataField="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" 
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripci&oacute;n Cofinanciaci&oacute;n Especie" DataField="DescripcionAporte"
                                                        SortExpression="DescripcionAporte" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
              
</asp:Content>
