<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaCDP.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaCDP" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
              <table width="90%" align="center">
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdContratosCDP,CodigoRubro,ValorCDP,ValorRubro"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Regional ICBF" DataField="Regional" />
                                                    <asp:BoundField HeaderText="N&uacute;mero CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorActualCDP" Visible="false" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                     <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosCDP" runat="server" AutoGenerateColumns="false" DataKeyNames=""
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                        <asp:BoundField HeaderText="N&uacute;mero CDP" DataField="NumeroCDP" />
                                                        <asp:BoundField HeaderText="Dependencia Afectaci&oacute;n Gastos" DataField="DependenciaAfectacionGastos"/>
                                                        <asp:BoundField HeaderText="Tipo Fuente Financiamiento" DataField="TipoFuenteFinanciamiento"/>
                                                        <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                                                        <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
                                                        <asp:BoundField HeaderText="Tipo Situaci&oacute;n Fondos" DataField="TipoSituacionFondos"/>
                                                        <asp:BoundField HeaderText="Valor Rubro" DataField="ValorRubro"  DataFormatString="{0:C}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
              
</asp:Content>
