<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaVigenciasFuturas.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaVigenciasFuturas" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
                                    <table width="90%" align="center">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvVigFuturas" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IDVigenciaFuturas" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="N&uacute;mero del Radicado Vigencia Futura" DataField="NumeroRadicado" />
                                                    <asp:BoundField HeaderText="Fecha de Expedici&oacute;n Vigencia Futura" DataField="FechaExpedicion"
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor Vigencia Futura" DataField="ValorVigenciaFutura"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="A&ntilde;o Vigencia Futura" DataField="AnioVigencia" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
              
</asp:Content>
