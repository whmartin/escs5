using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service; 
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;

public partial class Page_PreContractual_SubActividades_List : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "PreContractual/SubActividades";

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            string pNombre = string.Empty;
            int? pModalidadSeleccion = null;
            bool? estado = null;
            int? pTipoDocumento = null;

            if (!string.IsNullOrEmpty(txtNombre.Text))
                pNombre = txtNombre.Text.ToUpper();

            if (ddlIDModalidadSeleccion.SelectedValue != "-1")
                pModalidadSeleccion = int.Parse(ddlIDModalidadSeleccion.SelectedValue);

            if (!string.IsNullOrEmpty(ddlEstado.SelectedValue))
                estado = ddlEstado.SelectedValue == "1" ? true : false;

            var misItems = vPrecontractualService.ConsultarActividades(pModalidadSeleccion, pNombre, pTipoDocumento, estado, false);
            gvSubActividades.DataSource = misItems;
            gvSubActividades.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += Nuevo;
            toolBar.eventoRetornar += Retornar;

          gvSubActividades.PageSize = PageSize();
          gvSubActividades.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gesti&oacute;n de SubActividades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void Retornar(object sender, EventArgs e)
    {
        Response.Redirect("~/page/Precontractual/Parametricas/List.aspx");
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSubActividades.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SubActividad.IdSubActividad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            LlenarModalidadSeleccion();
            LlenarTiposDocumentos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSubActividades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      gvSubActividades.PageIndex = e.NewPageIndex;
        Buscar();
    }

    protected void gvSubActividades_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSubActividades.SelectedRow);
    }

    public void LlenarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void LlenarTiposDocumentos()
    {
    }
}
