﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_PreContractual_SubActividades_Detail : GeneralWeb
{
    masterPrincipal toolBar; 

    string PageName = "Precontractual/SubActividades";

    private ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {


        //EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SubActividad.IdSubActividad", hfIdSubActividad.Value);
        NavigateTo(SolutionPage.Edit);
    }


    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += btnEditar_Click;
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Gesti&oacute;n de SubActiviades", SolutionPage.Detail.ToString());
            gvModalidadesSeleccion.EmptyDataText = EmptyDataText();
            gvModalidadesSeleccion.PageSize = PageSize();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("SubActividad.Guardo").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado("Se Creó o actualizo correctamente la subActividad");
                RemoveSessionParameter("SubActividad.Guardo");
            }
            else if (GetSessionParameter("SubActividad.GuardoTiposDocumentos").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado("Se Creó o actualizo correctamente los tipos de documentos asociados a la subactividad");
                RemoveSessionParameter("SubActividad.GuardoTiposDocumentos");
            }

            hfIdSubActividad.Value = GetSessionParameter("SubActividad.IdSubActividad").ToString();
            RemoveSessionParameter("SubActividad.IdSubActividad");
            int idSubActividad = int.Parse(hfIdSubActividad.Value);
            var item = vPrecontractualService.ConsultarActividadPorId(idSubActividad);
            txtDescripción.Text = item.Descripcion;
            txtNombre.Text = item.Nombre;
            CargarModalidadesSeleccion();
            chklistActivo.Items.FindByText("SI").Selected = item.Estado ? true : false;
            chklistActivo.Items.FindByText("NO").Selected = item.Estado ? false : true;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarModalidadesSeleccion()
    {
        int subActividad = int.Parse(hfIdSubActividad.Value);
        var itemsModalidad = vPrecontractualService.ConsultarModalidadSeleccionPorIdActividad(subActividad);
        gvModalidadesSeleccion.DataSource = itemsModalidad;
        gvModalidadesSeleccion.DataBind();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvModalidadesSeleccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModalidadesSeleccion.PageIndex = e.NewPageIndex;
        CargarModalidadesSeleccion();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocumentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int subActividad = int.Parse(hfIdSubActividad.Value);

        var vLTiposDocumentos = vPrecontractualService.ConsultarTipoDocumentoPosiblesPorActividad(subActividad);

        if (vLTiposDocumentos != null && vLTiposDocumentos.Count > 0)
        {
            SetSessionParameter("SubActividad.IdSubActividad", subActividad);
            NavigateTo("DetailSubActividad.aspx");   
        }
        else
            toolBar.MostrarMensajeError("No existen tipos de documentos que se le puedan asociar a la sub-Actividad");
    }
}

