﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_PreContractual_SubActividades_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
         <asp:HiddenField ID="hfIdSubActividad" runat="server" /> 

        <script type="text/javascript" language="javascript">

            $(function () {
                $('[id*=ListModalidad]').multiselect({
                    includeSelectAllOption: true, maxHeight: 200, nonSelectedText: "Seleccione", numberDisplayed: 1,
                    nSelectedText: 'Seleccionado',
                });

            });



            function Confirmar() {

                return confirm('Esta seguro de que desea Realizar Cambios, estos pueden afectar las relaciones actuales con los tipos de documentos ?');
            }

        </script>

    <table width="90%" align="center">
        <tr class="rowB">
            <td >
                Nombre
                <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtNombre" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td >
                Descripci&oacute;n 
                <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ControlToValidate="txtDescripción" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre"  
                    MaxLength="100" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td >
                <asp:TextBox runat="server" ID="txtDescripción"  
                    MaxLength="100" Width="500px" Height="100px" TextMode="MultiLine" ></asp:TextBox>
            </td>
        </tr>
       <tr class="rowB">
            <td colspan="2">
                Modalidad de selecci&oacute;n
               <asp:RequiredFieldValidator ID="rfvModalidad" runat="server" ControlToValidate="ListModalidad" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>

            </td>

        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                <asp:ListBox ID="ListModalidad" EnableViewState="true"  Height="200px" SelectionMode="Multiple" runat="server">
                </asp:ListBox>
            </td>
        </tr>
               <tr class="rowB">
            <td colspan="2">
                Activo
                   <asp:RequiredFieldValidator ID="rfvActivo" runat="server" ControlToValidate="chklistActivo" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:RadioButtonList ID="chklistActivo" runat="server" >
                        <asp:ListItem Text="SI" Value="1" Selected="True" />
                        <asp:ListItem Text="NO" Value="0" />
                    </asp:RadioButtonList>
            </td>
        </tr>

        </table>

</asp:Content>
