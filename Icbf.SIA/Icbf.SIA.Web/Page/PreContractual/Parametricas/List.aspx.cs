using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web; 
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Service;

public partial class Page_PreContractual_Parametricas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Precontractual/Parametricas";
    ContratoService vContratoService = new ContratoService();


    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                Buscar();
                //if (GetState(Page.Master, PageName)) { Buscar(); }
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vCodigoTablaParametrica = null;
            String vNombreTablaParametrica = null;
            Boolean? vEstado = null;
            if (txtCodigoTablaParametrica.Text!= "")
            {
                vCodigoTablaParametrica = Convert.ToString(txtCodigoTablaParametrica.Text);
            }
            if (txtNombreTablaParametrica.Text!= "")
            {
                vNombreTablaParametrica = Convert.ToString(txtNombreTablaParametrica.Text);
            }
            if (rblEstado.SelectedValue != "")
            {
                vEstado = Convert.ToBoolean(Convert.ToInt32(rblEstado.SelectedValue));
            }
            gvTablaParametrica.DataSource = vContratoService.ConsultarTablaParametricas(vCodigoTablaParametrica, vNombreTablaParametrica, vEstado, true);
            gvTablaParametrica.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvTablaParametrica.PageSize = PageSize();
            gvTablaParametrica.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Listas Param&eacute;tricas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTablaParametrica.DataKeys[rowIndex].Values[0].ToString();
            SetSessionParameter("TablaParametrica.IdTablaParametrica", strValue);
            //NavigateTo(SolutionPage.List);
            Response.Redirect("~/page/" + strValue + "/List.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTablaParametrica_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTablaParametrica.SelectedRow);
    }

    protected void gvTablaParametrica_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTablaParametrica.PageIndex = e.NewPageIndex;
        //Buscar();

        CargarGrilla(gvTablaParametrica, GridViewSortExpression, true);
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TablaParametrica.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TablaParametrica.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "True"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "False"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvTablaParametrica_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el c�digo de llenado de datos para la grilla 
        ////////////////////////////////////////////////////////////////////////////////

        String vCodigoTablaParametrica = null;
        String vNombreTablaParametrica = null;
        Boolean? vEstado = null;
        if (txtCodigoTablaParametrica.Text != "")
        {
            vCodigoTablaParametrica = Convert.ToString(txtCodigoTablaParametrica.Text);
        }
        if (txtNombreTablaParametrica.Text != "")
        {
            vNombreTablaParametrica = Convert.ToString(txtNombreTablaParametrica.Text);
        }
        if (rblEstado.SelectedValue != "")
        {
            vEstado = Convert.ToBoolean(Convert.ToInt32(rblEstado.SelectedValue));
        }

        //Lleno una lista con los datos que uso para llenar la grilla
        var myGridResults = vContratoService.ConsultarTablaParametricas(vCodigoTablaParametrica, vNombreTablaParametrica, vEstado,true);

        //////////////////////////////////////////////////////////////////////////////////
        //////Fin del c�digo de llenado de datos para la grilla 
        ////////////////////////////////////////////////////////////////////////////////

        if (expresionOrdenamiento != null)
        {
            //Si la expresi�n de ordenamiento (columna) cambi�, entonces la direccion de ordenamiento es ascendente
            if (GridViewSortExpression != expresionOrdenamiento)
            {
                GridViewSortDirection = SortDirection.Ascending;
            }

            if (myGridResults != null)
            {
                //Financiero en "Entidad =>..."
                var param = Expression.Parameter(typeof(TablaParametrica), expresionOrdenamiento);

                //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                var prop = Expression.Property(param, expresionOrdenamiento);

                //Creo en tiempo de ejecuci�n la expresi�n lambda
                var sortExpression = Expression.Lambda<Func<TablaParametrica, object>>(Expression.Convert(prop, typeof(object)), param);

                //Dependiendo del modo de ordenamiento . . .
                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();

                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                    if (cambioPaginacion == false)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();

                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                    if (cambioPaginacion == false)
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                }

                GridViewSortExpression = expresionOrdenamiento;
            }
        }
        else
        {
            gridViewsender.DataSource = myGridResults;
        }

        gridViewsender.DataBind();
    }
}
