<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_PreContractual_TiposDocumento_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        
        .breakword
        {
        word-wrap:break-word;
        word-break:break-all;
        }

         .preformatted 
    {
        white-space: pre-line;
    }

    </style>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Nombre</td>
            <td class="Cell">
               Modalidad de Selecci&oacute;n</td>
        </tr>
        <tr class="rowA">
            <td> 
                <asp:TextBox ID="txtNombre" runat="server" Width="350px" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddlIDModalidadSeleccion" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Estado
            </td>

        </tr>
        <tr class="rowA">
            <td  class="Cell">
                <asp:DropDownList runat="server" ID="ddlEstado" Width="100px">
                   <asp:ListItem Text ="Activo" Value="1"></asp:ListItem>
                   <asp:ListItem Text ="Inactivo" Value="0"></asp:ListItem>
                </asp:DropDownList>
                
            
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTiposDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoDocumento" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTiposDocumentos_PageIndexChanging" OnSelectedIndexChanged="gvTiposDocumentos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:BoundField HeaderText="Nombre" DataField="Nombre"  />
<%--                            <asp:BoundField HeaderText="Descripci&oacute;n" DataField="Descripcion" />--%>
                            <asp:BoundField HeaderText="Modalidad Selecci&oacute;n" HtmlEncode="false"  ItemStyle-Wrap="true" DataField="ModalidadesSeleccionView" >
                                
                           </asp:BoundField>
                          
                            <asp:BoundField HeaderText="Fecha Creaci&oacute;n" DataField="FechaCrea"   DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Usuario Creaci&oacute;n" DataField="UsuarioCrea"  />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoView"  />

                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
