﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_PreContractual_TiposDocumento_Add : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "Precontractual/TiposdeDocumentos"; 

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();
    
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfIdTipoDocumento.Value))
        {
            SetSessionParameter("TipoDocumento.IdTipoDocumento", hfIdTipoDocumento.Value);
            NavigateTo(SolutionPage.Detail);
        }
        else
            NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    private void Guardar()
    {
        try
        {
            TiposDocumento pTipoDocumento = new TiposDocumento();
            pTipoDocumento.Descripcion = txtDescripción.Text.ToUpper();
            pTipoDocumento.Nombre = Utilidades.EliminarEspacios(txtNombre.Text.ToUpper());
            pTipoDocumento.UsuarioCrea = GetSessionUser().NombreUsuario;

            List<string> modalidades = new List<string>();

            foreach (ListItem itemModalidad in ListModalidad.Items)
                if (itemModalidad.Selected)
                    modalidades.Add(itemModalidad.Value);

          pTipoDocumento.ModalidadesSeleccionList = modalidades;

            int result;

             if(Request.QueryString["oP"] == "E")
             {
              pTipoDocumento.UsuarioModifica = GetSessionUser().NombreUsuario;
              pTipoDocumento.IdTipoDocumento = int.Parse(hfIdTipoDocumento.Value);
              pTipoDocumento.Estado = chklistActivo.Items.FindByText("SI").Selected ? true : false;
              result = vPrecontractualService.ModificarTipoDocumento(pTipoDocumento);
             }
            else
             result =  vPrecontractualService.InsertarTipoDocumento(pTipoDocumento);

          if(result > 0)
          {
              SetSessionParameter("TipoDocumento.IdTipoDocumento", pTipoDocumento.IdTipoDocumento);
              SetSessionParameter("TipoDocumento.Guardo", "1");
              NavigateTo(SolutionPage.Detail);
          }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Actividades", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            txtDescripción.Attributes.Add("maxlength", "100");
            LlenarModalidadSeleccion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (Request.QueryString["oP"] == "E")
            {
                hfIdTipoDocumento.Value = GetSessionParameter("TipoDocumento.IdTipoDocumento").ToString();
                RemoveSessionParameter("TipoDocumento.IdTipoDocumento");
                int idTipoDocumento = int.Parse(hfIdTipoDocumento.Value);
                var item = vPrecontractualService.ConsultarTipoDocumentoPorId(idTipoDocumento);
                txtDescripción.Text = item.Descripcion;
                txtNombre.Text = item.Nombre;
                var misModalidades = item.ModalidadesSeleccionList.ToList();
                foreach (ListItem itemModalidad in ListModalidad.Items)
                {
                    if (misModalidades.Any(e => e == itemModalidad.Value))
                        itemModalidad.Selected = true;
                }

                chklistActivo.Items.FindByText("SI").Selected = item.Estado ? true : false;
                chklistActivo.Items.FindByText("NO").Selected = item.Estado ? false : true;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void LlenarModalidadSeleccion()
    {
       ListModalidad.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ListModalidad.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
    }
}

