﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_PreContractual_GrupoActividades_Detail : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "Precontractual/TiposdeDocumentos";
 
    private ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
       SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {


        //EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoDocumento.IdTipoDocumento", hfIdTipoDocumento.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += btnEditar_Click;
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipos de Documentos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("TipoDocumento.Guardo").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado("Se Creó o actualizo correctamente el tipo de documento");
                RemoveSessionParameter("TipoDocumento.Guardo");
            }

            hfIdTipoDocumento.Value = GetSessionParameter("TipoDocumento.IdTipoDocumento").ToString();
            RemoveSessionParameter("TipoDocumento.IdTipoDocumento");
            int idTipoDocumento = int.Parse(hfIdTipoDocumento.Value);
            var item = vPrecontractualService.ConsultarTipoDocumentoPorId(idTipoDocumento);
            txtDescripción.Text = item.Descripcion;
            txtNombre.Text = item.Nombre;
            var misModalidades = item.ModalidadesSeleccionList.ToList();
            CargarModalidadesSeleccion();

            chklistActivo.Items.FindByText("SI").Selected = item.Estado ? true : false; 
            chklistActivo.Items.FindByText("NO").Selected = item.Estado ? false : true;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarModalidadesSeleccion()
    {
        int idTipoDocumento = int.Parse(hfIdTipoDocumento.Value);
        var itemsModalidad = vPrecontractualService.ConsultarModalidadSeleccionPorIdTipoDocumento(idTipoDocumento);
        gvModalidadesSeleccion.DataSource = itemsModalidad;
        gvModalidadesSeleccion.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvModalidadesSeleccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModalidadesSeleccion.PageIndex = e.NewPageIndex;
        CargarModalidadesSeleccion();
    }
}

