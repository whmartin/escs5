using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Service;

public partial class Page_PreContractual_TiposDocumento_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    string PageName = "PreContractual/TiposdeDocumentos";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            string pNombre = string.Empty;
            int? pModalidadSeleccion = null;
            bool ? estado = null;

            if (!string.IsNullOrEmpty(txtNombre.Text))
                pNombre = txtNombre.Text.ToUpper();

            if (ddlIDModalidadSeleccion.SelectedValue != "-1")
                pModalidadSeleccion = int.Parse(ddlIDModalidadSeleccion.SelectedValue);

            if (!string.IsNullOrEmpty(ddlEstado.SelectedValue))
                estado = ddlEstado.SelectedValue == "1" ? true : false;   

            var misItems = vPrecontractualService.ConsultarTiposDocumentos(pModalidadSeleccion, pNombre,estado);
            gvTiposDocumentos.DataSource = misItems;
            gvTiposDocumentos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += Nuevo;
            toolBar.eventoRetornar += Retornar;
            gvTiposDocumentos.PageSize = PageSize();
            gvTiposDocumentos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gesti&oacute;n de Tipos de Documentos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void Retornar(object sender, EventArgs e)
    {
        Response.Redirect("~/page/Precontractual/Parametricas/List.aspx");
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTiposDocumentos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoDocumento.IdTipoDocumento", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTiposDocumentos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTiposDocumentos.SelectedRow);
    }

    protected void gvTiposDocumentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTiposDocumentos.PageIndex = e.NewPageIndex;
        Buscar();
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            LlenarModalidadSeleccion();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void LlenarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }
}
