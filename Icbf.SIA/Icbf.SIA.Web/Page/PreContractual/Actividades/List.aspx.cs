using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;

public partial class Page_PreContractual_Actividades_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "PreContractual/Actividades";

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            string pNombre = string.Empty;
            int? pModalidadSeleccion = null;
            bool? estado = null;
            int? pTipoDocumento = null;

            if (!string.IsNullOrEmpty(txtNombre.Text))
                pNombre = txtNombre.Text.ToUpper();

            if (ddlIDModalidadSeleccion.SelectedValue != "-1")
                pModalidadSeleccion = int.Parse(ddlIDModalidadSeleccion.SelectedValue);

            if (!string.IsNullOrEmpty(ddlEstado.SelectedValue))
                estado = ddlEstado.SelectedValue == "1" ? true : false;

            var misItems = vPrecontractualService.ConsultarActividades(pModalidadSeleccion, pNombre, pTipoDocumento, estado, true);
            gvActividades.DataSource = misItems;
            gvActividades.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += Nuevo;
            toolBar.eventoRetornar += Retornar;
            gvActividades.PageSize = PageSize();
            gvActividades.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gesti&oacute;n  de Actividades", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void Retornar(object sender, EventArgs e)
    {
        Response.Redirect("~/page/Precontractual/Parametricas/List.aspx");
    }

    protected void Nuevo(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvActividades.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Actividad.IdActividad", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            CargarModalidadSeleccion();
            CargarTiposDocumentos();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvActividades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvActividades.PageIndex = e.NewPageIndex;
        Buscar();
    }

    protected void gvActividades_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvActividades.SelectedRow);
    }

    public void CargarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void CargarTiposDocumentos()
    {
       //ddltiposDocumentos.Items.Clear();
       //var vTiposDocumentos = vPrecontractualService.ConsultarTiposDocumentos(null, null, true);
       //foreach (TiposDocumento item in vTiposDocumentos)
       //    ddltiposDocumentos.Items.Add(new ListItem(item.Nombre, item.IdTipoDocumento.ToString()));
       
       //    ddltiposDocumentos.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }
}
