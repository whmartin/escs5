﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_PreContractual_Actividades_Detail : GeneralWeb
{ 
    masterPrincipal toolBar;

    string PageName = "Precontractual/Actividades";

    private ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {


        //EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Actividad.IdActividad", hfIdSubActividad.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += btnEditar_Click;
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Gesti&oacute;n de Actividades", SolutionPage.Detail.ToString());
            //gvDocumentos.EmptyDataText = EmptyDataText();
            //gvDocumentos.PageSize = PageSize();
            gvModalidadesSeleccion.EmptyDataText = EmptyDataText();
            gvModalidadesSeleccion.PageSize = PageSize();
            gvSubActiviades.EmptyDataText = EmptyDataText();
            gvSubActiviades.PageSize = PageSize();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("Actividad.Guardo").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado("Se Creó o actualizo correctamente la Actividad");
                RemoveSessionParameter("Actividad.Guardo");
            }
            else if (GetSessionParameter("Actividad.GuardoDetalle").ToString() == "1")
            {
                toolBar.MostrarMensajeGuardado("Se Creó o actualizo correctamente las sub actividades y/o los tipos de documentos  asociados a la actividad");
                RemoveSessionParameter("Actividad.GuardoDetalle");
            }

            hfIdSubActividad.Value = GetSessionParameter("Actividad.IdActividad").ToString();
            RemoveSessionParameter("Actividad.IdActividad");
            int idActividad = int.Parse(hfIdSubActividad.Value);
            var item = vPrecontractualService.ConsultarActividadPorId(idActividad);
            txtDescripción.Text = item.Descripcion;
            txtNombre.Text = item.Nombre;
            CargarModalidadesSeleccion();
            CargarTiposDocumentos();
            CargarSubActiviades();
            chklistActivo.Items.FindByText("SI").Selected = item.Estado ? true : false;
            chklistActivo.Items.FindByText("NO").Selected = item.Estado ? false : true;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarModalidadesSeleccion()
    {
        int actividad = int.Parse(hfIdSubActividad.Value);
        var itemsModalidad = vPrecontractualService.ConsultarModalidadSeleccionPorIdActividad(actividad);
        gvModalidadesSeleccion.DataSource = itemsModalidad;
        gvModalidadesSeleccion.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTiposDocumentos()
    {
        //int actividad = int.Parse(hfIdSubActividad.Value);
        //var itemsModalidad = vPrecontractualService.ConsultarTipoDocumentoPorActividad(actividad);
        //gvDocumentos.DataSource = itemsModalidad;
        //gvDocumentos.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarSubActiviades()
    {
        int actividad = int.Parse(hfIdSubActividad.Value);
        var itemsSubActividades = vPrecontractualService.ConsultarSubActividadesPorActividad(actividad);
        gvSubActiviades.DataSource = itemsSubActividades;
        gvSubActiviades.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvModalidadesSeleccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModalidadesSeleccion.PageIndex = e.NewPageIndex;
        CargarModalidadesSeleccion();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvDocumentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvDocumentos.PageIndex = e.NewPageIndex;
        //CargarTiposDocumentos();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSubActiviades_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDetalle_Click(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int actividad = int.Parse(hfIdSubActividad.Value);

        var vLTiposDocumentos = vPrecontractualService.ConsultarTipoDocumentoPosiblesPorActividad(actividad);

        var vLActividades = vPrecontractualService.ConsultarSubActividadesPosiblesPorActividad(actividad);

        if ((vLTiposDocumentos != null && vLTiposDocumentos.Count > 0 )|| (vLActividades != null  && vLActividades.Count > 0 ))
        {
            SetSessionParameter("Actividad.IdActividad", actividad);
            NavigateTo("DetailActividad.aspx");   
        }
        else
            toolBar.MostrarMensajeError("No existen tipos de documentos y sub Actividades que se le puedan asociar a la Actividad");
    }
}

