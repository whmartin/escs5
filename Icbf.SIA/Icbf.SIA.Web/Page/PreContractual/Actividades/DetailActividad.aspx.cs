﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_PreContractual_Actividades_DetailActividad : GeneralWeb
{
    masterPrincipal toolBar;

    string PageName = "Precontractual/Actividades";

    private ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {


        //EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfIdSubActividad.Value))
        {
            SetSessionParameter("Actividad.IdActividad", hfIdSubActividad.Value);
            NavigateTo(SolutionPage.Detail);
        }
        else
        {
            NavigateTo(SolutionPage.List);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            string tiposDocumentos = string.Empty;
            string subActiviades = string.Empty;

            //foreach (ListItem itemTipoDocumento in ListTiposDocumentos.Items)
            //    if (itemTipoDocumento.Selected)
            //        tiposDocumentos += itemTipoDocumento.Value + ",";

            //tiposDocumentos = tiposDocumentos.Substring(0, tiposDocumentos.Length - 1);

            foreach (ListItem itemSubActividad in ListSubActividades.Items)
                if (itemSubActividad.Selected)
                    subActiviades += itemSubActividad.Value + ",";

            if (subActiviades != string.Empty)
                subActiviades = subActiviades.Substring(0, subActiviades.Length - 1);

            int idActividad = int.Parse(hfIdSubActividad.Value);

            int result = vPrecontractualService.ModificarActividadSubActividadDocumentos(idActividad,tiposDocumentos,subActiviades);

            if (result > 0)
            {
                SetSessionParameter("Actividad.IdActividad", idActividad);
                SetSessionParameter("Actividad.GuardoDetalle","1");
                NavigateTo(SolutionPage.Detail);
            }
            else
                toolBar.MostrarMensajeError("No se actualizo correctamente, verifique por favor.");
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += btnGuardar_Click;
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Gesti&oacute;n de Activiades", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            hfIdSubActividad.Value = GetSessionParameter("Actividad.IdActividad").ToString();
            RemoveSessionParameter("Actividad.IdActividad");
            int idSubActividad = int.Parse(hfIdSubActividad.Value);
            var item = vPrecontractualService.ConsultarActividadPorId(idSubActividad);
            CargarModalidadesSeleccion();
            CargarTiposDocumentos();
            CargarSubActividades();
            txtDescripción.Text = item.Descripcion;
            txtNombre.Text = item.Nombre;
            chklistActivo.Items.FindByText("SI").Selected = item.Estado ? true : false;
            chklistActivo.Items.FindByText("NO").Selected = item.Estado ? false : true;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarSubActividades()
    {
        int actividad = int.Parse(hfIdSubActividad.Value);

        ListSubActividades.Items.Clear();
        var vLSubActiviades = vPrecontractualService.ConsultarSubActividadesPosiblesPorActividad(actividad);

        if (vLSubActiviades != null && vLSubActiviades.Count > 0)
        {
            foreach (SolicitudContratoActividad itemSubActividada in vLSubActiviades)
               ListSubActividades.Items.Add(new ListItem(itemSubActividada.Nombre, itemSubActividada.IdActividad.ToString()));

            var vLSubActividadesActuales = vPrecontractualService.ConsultarSubActividadesPorActividad(actividad);

            foreach (ListItem itemSubActividada in ListSubActividades.Items)
            {
                if (vLSubActividadesActuales.Any(e => e.IdActividad == int.Parse(itemSubActividada.Value)))
                    itemSubActividada.Selected = true;
            }
        }
        else
        {
            //rfvSubActividades.Enabled = false;
            ListSubActividades.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarModalidadesSeleccion()
    {
        int subActividad = int.Parse(hfIdSubActividad.Value);
        var itemsModalidad = vPrecontractualService.ConsultarModalidadSeleccionPorIdActividad(subActividad);
        gvModalidadesSeleccion.DataSource = itemsModalidad;
        gvModalidadesSeleccion.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTiposDocumentos()
    {
        //int actividad = int.Parse(hfIdSubActividad.Value);
        
        //ListTiposDocumentos.Items.Clear();

        //var vLTiposDocumentos = vPrecontractualService.ConsultarTipoDocumentoPosiblesPorActividad(actividad);

        //if (vLTiposDocumentos != null && vLTiposDocumentos.Count > 0)
        //{
        //    foreach (TiposDocumento tD in vLTiposDocumentos)
        //        ListTiposDocumentos.Items.Add(new ListItem(tD.Nombre, tD.IdTipoDocumento.ToString()));

        //    var vLTiposDocumentosActuales = vPrecontractualService.ConsultarTipoDocumentoPorActividad(actividad);
        //    foreach (ListItem itemTipoDocumento in ListTiposDocumentos.Items)
        //    {
        //        if (vLTiposDocumentosActuales.Any(e => e.IdTipoDocumento == int.Parse(itemTipoDocumento.Value)))
        //            itemTipoDocumento.Selected = true;
        //    }
        //}
        //else
        //{
        //   rfvTiposDocumentos.Enabled = false;
        //   ListTiposDocumentos.Visible = false;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvModalidadesSeleccion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModalidadesSeleccion.PageIndex = e.NewPageIndex;
        CargarModalidadesSeleccion();
    }
}

