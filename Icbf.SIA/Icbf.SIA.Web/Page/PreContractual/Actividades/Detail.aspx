﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_PreContractual_Actividades_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdSubActividad" runat="server" />
     
   <table width="90%" align="center">
        <tr class="rowB">
            <td >
                Nombre
            </td>
            <td >
                Descripci&oacute;n 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre"   Enabled="false"
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td >
                <asp:TextBox runat="server" ID="txtDescripción"  Enabled="false"
                    MaxLength="512" Width="500px" Height="100px" TextMode="MultiLine" ></asp:TextBox>
            </td>
        </tr>
       <tr class="rowB">
            <td colspan="2">
                Modalidad de selecci&oacute;n
                                                    
            </td>
        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:GridView runat="server" ID="gvModalidadesSeleccion" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Key" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvModalidadesSeleccion_PageIndexChanging" >
                        <Columns>
                            <asp:BoundField HeaderText="Nombre" DataField="Value"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
     <%--         <tr class="rowB">
            <td colspan="2">
                Tipos de
                Documentos 
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Editar" OnClick="btnDetalle_Click" />
            </td>
        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Nombre" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvDocumentos_PageIndexChanging" >
                        <Columns>
                            <asp:BoundField HeaderText="Nombre" DataField="Nombre"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>--%>
                     <tr class="rowB">
            <td colspan="2">
                SubActividades 
                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Editar" OnClick="btnDetalle_Click" />
            </td>
        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:GridView runat="server" ID="gvSubActiviades" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Nombre" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvSubActiviades_PageIndexChanging" >
                        <Columns>
                            <asp:BoundField HeaderText="Nombre" DataField="Nombre"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
       <tr class="rowB">
            <td colspan="2">
                Activo
            </td>

        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:RadioButtonList ID="chklistActivo" runat="server" Enabled="false">
                        <asp:ListItem Text="SI" Value="1" />
                        <asp:ListItem Text="NO" Value="0" />
                    </asp:RadioButtonList>
            </td>
        </tr>

        </table>
  
</asp:Content>



