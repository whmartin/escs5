﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="DetailActividad.aspx.cs" Inherits="Page_PreContractual_Actividades_DetailActividad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdSubActividad" runat="server" />
     
            <script type="text/javascript" language="javascript">

            $(function () {
                $('[id*=ListTiposDocumentos]').multiselect({
                    includeSelectAllOption: true, maxHeight: 200, nonSelectedText: "Seleccione", numberDisplayed: 1,
                    nSelectedText: 'Seleccionado',
                });


                $('[id*=ListSubActividades]').multiselect({
                    includeSelectAllOption: true, maxHeight: 200, nonSelectedText: "Seleccione", numberDisplayed: 1,
                    nSelectedText: 'Seleccionado',
                });
            });

        </script>


   <table width="90%" align="center">
        <tr class="rowB">
            <td >
                Nombre
            </td>
            <td >
                Descripci&oacute;n 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre"   Enabled="false"
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td >
                <asp:TextBox runat="server" ID="txtDescripción"  Enabled="false"
                    MaxLength="512" Width="500px" Height="100px" TextMode="MultiLine" ></asp:TextBox>
            </td>
        </tr>
              <tr class="rowB">
            <td colspan="2">
                Modalidad de selecci&oacute;n
                                                    
            </td>
        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:GridView runat="server" ID="gvModalidadesSeleccion" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="Key" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvModalidadesSeleccion_PageIndexChanging" >
                        <Columns>
                            <asp:BoundField HeaderText="Nombre" DataField="Value"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
            </tr>
                   <tr class="rowB">
            <td colspan="2">
                SubActividades
               <asp:RequiredFieldValidator ID="rfvSubActividades" runat="server" ControlToValidate="ListSubActividades" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                <asp:ListBox ID="ListSubActividades" EnableViewState="true"  Height="200px" SelectionMode="Multiple" runat="server">
                </asp:ListBox>
            </td>
            </tr>
          
     
              <tr class="rowB">
            <td colspan="2">
                Activo
            </td>

        </tr>
        <tr class="rowA">
            <td  class="Cell" colspan="2">
                    <asp:RadioButtonList ID="chklistActivo" runat="server" Enabled="false">
                        <asp:ListItem Text="SI" Value="1" />
                        <asp:ListItem Text="NO" Value="0" />
                    </asp:RadioButtonList>
            </td>
        </tr>
        </table>
  
</asp:Content>



