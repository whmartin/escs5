using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.RUBO.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;
using Icbf.Contrato.Entity.PreContractual;
using System.Web.Security;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Precontractual_Lupas_LupaRepartoSolicitudes : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    PreContractualService vPrecontractualService = new PreContractualService();

    ContratoService vContratoService = new ContratoService();

    SIAService vSIAService = new SIAService();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método de guardado para la entidad Garantia
    /// </summary>
    private void Guardar()
    {
        try
        {
            int idSolicitudContrato = int.Parse(hfIdSolicitudContrato.Value);
            var solMod = vPrecontractualService.ConsultarSolicitudPorId(idSolicitudContrato);
            solMod.UsuarioRevision = ddlUsuarioAsignar.SelectedValue.ToString();
            int result = vPrecontractualService.ModificarSolicitudContrato(solMod);

            if (result > 0)
            {
                string mensaje = string.Format("Se asigno el usuario {0}, al N&uacute;mero de Solicitud {1}", solMod.UsuarioRevision,idSolicitudContrato.ToString());
                SetSessionParameter("SolicitudContrato.AsignacionUsuario", mensaje);
                GetScriptCloseDialogCallback(string.Empty);               
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Reparto de Solicitudes de Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            CargarCategoriaContrato();
            CargarTipodeContrato();
            CargarModalidadSeleccion();
          
            pnInfoSolicitud.Style.Add("display", "");
            var idSolicitudContrato = Request.QueryString["idSolicitud"];
            hfIdSolicitudContrato.Value = idSolicitudContrato.ToString();

            var itemSolicitud = vPrecontractualService.ConsultarSolicitudPorId(int.Parse(idSolicitudContrato));
            hfIdRegional.Value = itemSolicitud.IdRegional.ToString();
            txtFechaRegistroSsistema.Text = itemSolicitud.FechaCrea.ToShortDateString();
            txtNombreUsuario.Text = itemSolicitud.UsuarioCrea.ToString();
            txtNumeroSolicitud.Text = itemSolicitud.IdSolicitud.ToString();
            chkConvenioMarco.Checked = itemSolicitud.EsContratoMarco.HasValue ? itemSolicitud.EsContratoMarco.Value : false;
            chkContratoConvenioAd.Checked = itemSolicitud.EsContratoConvenioAdhesion.HasValue ? itemSolicitud.EsContratoConvenioAdhesion.Value : false;
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
            ddlCategoriaContrato.SelectedValue = itemSolicitud.IdCategoriaContrato.ToString();
            CargarTipodeContrato();
            ddlTipoContratoConvenio.SelectedValue = itemSolicitud.IdTipoContrato.ToString();

            TxtFechaInicio.Text = itemSolicitud.VigenciaInicial.ToString();
            TxtFechaFin.Text = itemSolicitud.VigenciaFinal.ToString();

            if(itemSolicitud.CodigoEstadoSolicitud == "RECL" || itemSolicitud.CodigoEstadoSolicitud == "RECO" )
                ddlUsuarioAsignar.Enabled = false;

            CargarUsuariosRol();

            if(!string.IsNullOrEmpty(itemSolicitud.UsuarioRevision))
            {
                ddlUsuarioAsignar.SelectedValue = itemSolicitud.UsuarioRevision;
            }

            txtNombreUsuario.Text = itemSolicitud.UsuarioArea;
            txtRegionalUsuario.Text = itemSolicitud.Regional;
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarCategoriaContrato()
    {
        ddlCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, true);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
        }
        ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTipodeContrato()
    {
        string idCategoriaSeleccionada = ddlCategoriaContrato.SelectedValue;

        if (idCategoriaSeleccionada != "-1")
        {
            ddlTipoContratoConvenio.Items.Clear();
            List<Icbf.Contrato.Entity.TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), true, null, null, null, null, null);
            foreach (Icbf.Contrato.Entity.TipoContrato tD in vLTiposContratos)
            {
                ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarUsuariosRol()
    {    
       var rol =  vPrecontractualService.ConsultarParametro("RolReparto");

        var usuarios = Roles.GetUsersInRole(rol.Valor);

        List<string> usuariosList = new List<string>();

       if (usuarios != null)
       {
            foreach(var item in usuarios)
            {
                var miUsuario = Membership.GetUser(item);
                var itemUsuarioSIA = vSIAService.ConsultarUsuario(item);

                if(itemUsuarioSIA != null && ! string.IsNullOrEmpty(itemUsuarioSIA.Providerkey))
                {
                    int idRegional = int.Parse(hfIdRegional.Value);

                    if((itemUsuarioSIA.IdRegional.HasValue && itemUsuarioSIA.IdRegional.Value == idRegional) || itemUsuarioSIA.IdTipoUsuario == 1)
                        usuariosList.Add(item.ToUpper());
                }
            }
       }

       usuariosList.Insert(0, "Seleccione");
       ddlUsuarioAsignar.DataSource = usuariosList;
       ddlUsuarioAsignar.DataBind();
    }

    #endregion 

}
