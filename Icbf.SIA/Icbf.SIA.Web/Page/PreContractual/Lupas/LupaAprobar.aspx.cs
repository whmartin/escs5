using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.RUBO.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;
using Icbf.Contrato.Entity.PreContractual;
using System.Web.Security;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Precontractual_LupasConsulta_LupaDocumentos : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    PreContractualService vPrecontractualService = new PreContractualService();

    ContratoService vContratoService = new ContratoService();

    SIAService vSIAService = new SIAService();

    private string[] estadosEdicion = new string[] { "RE", "DEVCL", "DEVCO" };

    private const string EstadoEnviado = "ENV";
    private const string EstadoDevuelto = "DEVCL";
    private const string EstadoAprobado = "APCL";
    private const string EstadoRechazado = "RECL";
    
    private const string TemplateCambioEstado = "Se realizo el cambio de la solicitud número {1} al estado: {0}.";

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    private void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            int idSolicitud = int.Parse(Request.QueryString["idSolicitudContrato"]);
            SolicitudContratoCambioEstado solCambioEstado = new SolicitudContratoCambioEstado();
            solCambioEstado.IdSolicitudContrato = idSolicitud;
            solCambioEstado.Observaciones = txtObservacionesEstado.Text;
            solCambioEstado.Fecha = null;
            solCambioEstado.Usuario = GetSessionUser().NombreUsuario;

            string accion = Request.QueryString["tipo"];

            if (accion == "Rechazar")
            solCambioEstado.Codigo = EstadoRechazado;
            else if (accion == "Enviar")
            {
                solCambioEstado.Codigo = EstadoEnviado;

                if(!string.IsNullOrEmpty(txtNumeroRadicado.Text))
                    solCambioEstado.NumeroRadicado = txtNumeroRadicado.Text;

                //var misMensajes = vPrecontractualService.ValidarSolicitudContrato(idSolicitud);

                //if(misMensajes != null && misMensajes.Count > 0)
                //{
                //    toolBar.MostrarMensajeError(misMensajes);
                //    return;
                //}
            }
            else
            {
                var totaldocumentos = gvDocumentos.Rows.Count;
                string aprobados = string.Empty;
                int totalAprobados = 0;  
                RadioButtonList controlAprobar;

                if(!string.IsNullOrEmpty(txtNumeroRadicado.Text))
                    solCambioEstado.NumeroRadicado = txtNumeroRadicado.Text;

                foreach (GridViewRow fila in gvDocumentos.Rows)
                {
                    controlAprobar = fila.Cells[2].FindControl("rblAprobado") as RadioButtonList;

                    if (controlAprobar.Items.FindByValue("Si").Selected)
                    {
                        totalAprobados++;
                        aprobados += gvDocumentos.DataKeys[fila.RowIndex][0].ToString() +',';
                    }
                }

                solCambioEstado.IdsDocumentosAprobados = aprobados;
                if (totaldocumentos > totalAprobados)
                    solCambioEstado.Codigo = EstadoDevuelto;
                else
                    solCambioEstado.Codigo = EstadoAprobado;
            }
            
            int result = vPrecontractualService.CambiarEstadoSolicitud(solCambioEstado);

            if (result > 0)
            {
                switch(accion)
                {
                    case "Rechazar":
                    accion = "Rechazado";
                    break;
                    case "Enviar":
                    accion = "Enviado";
                    break;
                    default:
                    break;
                }

                string mensaje = string.Format(TemplateCambioEstado, accion, idSolicitud.ToString());
                SetSessionParameter("SolicitudContrato.CambioEstado", mensaje);
                GetScriptCloseDialogCallback(string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocumentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool aprobado = bool.Parse(gvDocumentos.DataKeys[e.Row.RowIndex].Values[1].ToString());

                if (aprobado)
                    ((RadioButtonList)e.Row.FindControl("rblAprobado")).Items.FindByValue("Si").Selected = true;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.EstablecerTitulos("Documentos Solicitud", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int idSolicitudContrato = int.Parse(Request.QueryString["idSolicitudContrato"]);
            var miSolicitud = vPrecontractualService.ConsultarSolicitudPorId(idSolicitudContrato);
            ValidarAcceso(miSolicitud);

            string accion = Request.QueryString["tipo"];

            if(accion == "Enviar")
            {
                var misMensajes = vPrecontractualService.ValidarSolicitudContrato(idSolicitudContrato);
                if(misMensajes != null && misMensajes.Count > 0)
                {
                    toolBar.MostrarMensajeError(misMensajes);
                    toolBar.OcultarBotonGuardar(true);
                    PnlObservacionesDocumentos.Style.Add("display", "none");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item">
    /// 
    /// </param>
    private void ValidarAcceso(SolicitudContrato item)
    {
        string origen = Request.QueryString["origen"];
        string accion = Request.QueryString["tipo"];

        if (origen == "V" && accion != "Rechazar")
        {
            if (item.CodigoEstadoSolicitud == "ENV" && (item.UsuarioRevision.ToUpper() == GetSessionUser().NombreUsuario.ToUpper() || (GetSessionUser().IdTipoUsuario == 1 && !string.IsNullOrEmpty(item.UsuarioRevision))))
            {
                pnlRadicado.Style.Add("display","");
                PnlAprobarDocumentos.Style.Add("display","");
                gvDocumentos.EmptyDataText = EmptyDataText();
                gvDocumentos.PageSize = PageSize();
                var misitems = vPrecontractualService.ConsultarDocumentosPorIdSolicitud(item.IdSolicitud);
                gvDocumentos.DataSource = misitems;
                gvDocumentos.DataBind();
            }
            else
            {
                pnlRadicado.Style.Add("display", "none");
                PnlAprobarDocumentos.Style.Add("display","none");
            }
        }
        else if(origen == "V" && accion == "Rechazar")
            RvObservaciones.Enabled = true;        
    }

    #endregion 
    
}
