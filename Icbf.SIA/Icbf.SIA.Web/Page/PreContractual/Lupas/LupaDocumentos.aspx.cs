using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro de los documentos de una solicitud de un contrato.
/// </summary>
public partial class Page_Precontractual_Lupas_LupaDocumentos : GeneralWeb
{
    General_General_Master_Lupa toolBar;

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack)        
            CargarDatosIniciales();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    
   /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad AporteContrato
        /// </summary>
    private void Guardar()
    {
        try
        {
            int idSolicitudContrato = int.Parse(Request.QueryString["idSolicitudContrato"]);
            ManejoControlesContratos controles = new ManejoControlesContratos();
            var guardo = controles.CargarArchivoFTPSolicitudContrato
                (
                 ddlTipoDocumentoCrear.SelectedItem.Text, 
                 FileUploadArchivoContrato, 
                 idSolicitudContrato,
                 int.Parse(ddlTipoDocumentoCrear.SelectedValue.ToString()),
                 GetSessionUser().NombreUsuario
                 );

            if (guardo)
                GetScriptCloseDialogCallback(string.Empty);
            else
                toolBar.MostrarMensajeError("La operación no se pudo realizar, intentelo nuevamente.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.EstablecerTitulos("Documentos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    /// <summary>
   /// Método de carga de listas y valores por defecto 
   /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarTiposDocumentosPosibles();
            int idSolicitudContrato = int.Parse(Request.QueryString["idSolicitudContrato"]);
            int idModalidadSeleccion = int.Parse(Request.QueryString["idModalidadSeleccion"]);

            var misitems = vPrecontractualService.ConsultarDocumentosFaltantesPorCrear(idSolicitudContrato,idModalidadSeleccion);
            ddlTipoDocumentoCrear.DataSource = misitems;
            ddlTipoDocumentoCrear.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga los posibles tipos de documentos.
    /// </summary>
    private void CargarTiposDocumentosPosibles()
    {

    }

    #region  Cargar Documentos

    //private void SeleccionarRegistro(GridViewRow pRow)
    //{
    //    try
    //    {
    //        int rowIndex = pRow.RowIndex;
    //        string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
    //        //vIdIndice = Convert.ToInt64(strIDCosModContractual);
    //        //hfIndice.Value = strIDCosModContractual;
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    //private void EliminarAnexo(GridViewRow pRow)
    //{
    //    try
    //    {
    //        int vResultado = 0;
    //        decimal indice = 0;
    //        int rowIndex = pRow.RowIndex;
    //        string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
    //        indice = Convert.ToInt64(strIDCosModContractual);

    //        if (indice != 0)
    //        {
    //            vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
    //            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
    //            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
    //            gvanexos.DataBind();
    //        }
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    //protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    //{
    //    int idContrato = Convert.ToInt32(hfIdContrato.Value);
    //    gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
    //    gvanexos.DataBind();
    //}

    //protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    EliminarAnexo(gvanexos.SelectedRow);
    //}

    //protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    //{
    //    toolBar.LipiarMensajeError();

    //    int idContrato = Convert.ToInt32(hfIdContrato.Value);

    //    FileUpload fuArchivo = FileUploadArchivoContrato;

    //    if (fuArchivo.HasFile)
    //    {
    //        try
    //        {
    //            ManejoControlesContratos controles = new ManejoControlesContratos();
    //            controles.CargarArchivoFTPContratos
    //                (
    //                 TIPO_ESTRUCTURA,
    //                 fuArchivo,
    //                 idContrato,
    //                 GetSessionUser().IdUsuario
    //                );

    //            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
    //            gvanexos.DataBind();
    //        }
    //        catch (Exception ex)
    //        {
    //            toolBar.MostrarMensajeError(ex.Message);
    //        }
    //    }
    //}

    #endregion
}
