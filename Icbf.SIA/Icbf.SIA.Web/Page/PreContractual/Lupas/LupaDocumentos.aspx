<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaDocumentos.aspx.cs" Inherits="Page_Precontractual_Lupas_LupaDocumentos" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfValor" runat="server" />
    <asp:HiddenField ID="HfAporte" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Tipo de Documento 
                                <asp:RequiredFieldValidator runat="server" ID="rvtTipoDocumento" ControlToValidate="ddlTipoDocumentoCrear"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList ID="ddlTipoDocumentoCrear" DataValueField="Key" DataTextField="Value"  runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Documento
            </td>
        </tr>
        <tr class="rowB">
            <td>
               <asp:FileUpload Width="100%" ID="FileUploadArchivoContrato" runat="server" /> 
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        
    </script>
</asp:Content>
