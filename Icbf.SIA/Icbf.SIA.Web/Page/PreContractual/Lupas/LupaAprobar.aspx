<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaAprobar.aspx.cs" Inherits="Page_Precontractual_LupasConsulta_LupaDocumentos" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

        <table width="90%" runat="server" id="pnlRadicado" align="center" style="display:none">
                       <tr class="rowB" >
                           <td class="Cell" colspan="2">
                                     N&uacute;mero del Radicado
                                    </td>
                            <td class="Cell">
                                          <asp:TextBox ID="txtNumeroRadicado" runat="server" MaxLength="25" Width="80%"></asp:TextBox>
<%--                                    <asp:RequiredFieldValidator runat="server" ID="RvNumeroRadicado" ControlToValidate="txtNumeroRadicado"
                                    SetFocusOnError="true" ErrorMessage="*" Enabled="false" Display="Dynamic" ValidationGroup="btnGuardar"
                                    ForeColor="Red"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
       </table>

    <table width="90%" runat="server" id="PnlObservacionesDocumentos" align="center">
                       <tr class="rowB" >
                            <td class="Cell" colspan="2">
                              Observaciones del Estado de la Solicitud
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell" colspan="2">
                                <asp:TextBox ID="txtObservacionesEstado" runat="server" TextMode="MultiLine" MaxLength="1000" Height="160px" Width="90%"></asp:TextBox>
                                 <asp:RequiredFieldValidator runat="server" ID="RvObservaciones" ControlToValidate="txtObservacionesEstado"
                                    SetFocusOnError="true" ErrorMessage="*" Enabled="false" Display="Dynamic" ValidationGroup="btnGuardar"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
    </table>


    <table runat="server" id="PnlAprobarDocumentos" width="90%" align="center" style="display:none">
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvDocumentos" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IdSolicitudContratoDocumento,Aprobado" GridLines="None" OnRowDataBound="gvDocumentos_RowDataBound" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/pdf.png" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}&tipo=SolicitudesContrato") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="Documento" DataField="TipoDocumento" />
                                                    <asp:TemplateField HeaderText="Devuelto Control de Legalidad">
                                                    <ItemTemplate>
                                                            <asp:RadioButtonList runat="server" ID="rblAprobado" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Si" Value="Si" Selected="True" />
                                                            <asp:ListItem Text="No" Value="No" />
                                                            </asp:RadioButtonList>  
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
        </table>
              
        <script type="text/javascript">

            $(document).ready(function () {

            $('input[type=radio],select').change(function () {

                var isValid = true;

                $("input[type='radio']").each(function () {

                    if ($(this).is(':checked') && $(this).val() == 'No') {
                        isValid = false;
                    }
                });

                if (isValid == true) {
                    document.getElementById("<%= RvObservaciones.ClientID  %>").enabled = false;
                }
                else {
                    document.getElementById("<%= RvObservaciones.ClientID  %>").enabled = true;
                }
            });
        });

    </script>

</asp:Content>



