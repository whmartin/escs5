<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaRepartoUsuario.aspx.cs" Inherits="Page_Precontractual_Lupas_LupaRepartoSolicitudes" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdRegional" runat="server" />
                    <table width="90%"  id="pnInfoUsuario" align="center">
                    <tr class="rowB">
                        <td>
                            Nombre del usuario Solicitud
                        </td>
                        <td>
                            Regional de la Solicitud                                                     
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNombreUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtRegionalUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="90%" id="pnInfoSolicitud" runat="server" align="center" style="display:none">
                    <tr class="rowB">
                        <td>
                            N&uacute;mero de Solicitud
                        </td>
                        <td>
                            Fecha de Registro en el Sistema                                                 
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                            <td class="Cell">
                                <asp:TextBox ID="txtFechaRegistroSsistema" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="90%" id="pnInfoCategoriaContrato" align="center">
                    <tr class="rowB">
                        <td class="Cell">
                            Contrato Asociado
                        </td>
                        <td class="Cell">
                                Modalidad de Selecci&oacute;n 
                        </td>
                    </tr>
                    <tr class="rowA">
                    <td class="Cell">
                            <asp:CheckBox ID="chkConvenioMarco" runat="server" Text="Convenio Marco" Enabled="false" /><br />
                            <asp:CheckBox ID="chkContratoConvenioAd" runat="server" Text="Contrato/Convenio Adhesi&oacute;n" Enabled="false"  />
                        </td>
                        <td class="Cell">
                            <asp:DropDownList ID="ddlModalidadSeleccion" Enabled="false" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="Cell">
                        Categor&iacute;a Contrato/Convenio                                                 
                    </td>
                        <td class="Cell">
                            Tipo de Contrato/Convenio 
                        </td>
                    </tr>
                    <tr class="rowA">
                            <td class="Cell">
                            <asp:DropDownList ID="ddlCategoriaContrato" Enabled="false" runat="server" >
                            </asp:DropDownList>
                            </td>
                            <td class="Cell">
                            <asp:DropDownList ID="ddlTipoContratoConvenio" Enabled="false" runat="server" />
                        </td>
                    </tr>

                      <tr class="rowB">
                            <td class="Cell">
                                Vigencia Incial del Contrato/Convenio *
                            </td>
                            <td class="Cell">
                               Vigencia Final del Contrato/Convenio *
                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                                    <asp:TextBox ID="TxtFechaInicio" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                            <td class="Cell">
                                    <asp:TextBox ID="TxtFechaFin" runat="server" Enabled="false" Width="80%" ></asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr class="rowB">
                            <td class="Cell">
                                Usuario Asignado
                                <asp:CompareValidator runat="server" ID="cvModalidadSeleccion" ControlToValidate="ddlUsuarioAsignar"
                                SetFocusOnError="true" ErrorMessage="*" ValidationGroup="btnGuardar"
                                ForeColor="Red" Operator="NotEqual" ValueToCompare="Seleccione" Display="Dynamic"></asp:CompareValidator>
                            </td>
                            <td class="Cell">

                            </td>
                        </tr>
                        <tr class="rowA">
                            <td class="Cell">
                            <asp:DropDownList ID="ddlUsuarioAsignar" ValidationGroup="btnGuardar" runat="server" />
                            </td>
                            <td class="Cell">
                            </td>
                        </tr>
                          
                    </table>
                    <asp:HiddenField ID="hfIdSolicitudContrato" runat="server" />
</asp:Content>
