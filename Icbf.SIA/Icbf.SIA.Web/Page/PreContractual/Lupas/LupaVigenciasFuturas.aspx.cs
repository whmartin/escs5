using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Precontractual_Lupas_LupaVigenciasFuturas : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;
    
    ContratoService vContratoService = new ContratoService();

    string PageName = "Contratos/VigenciasFuturas";
    
    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

            if (!Page.IsPostBack)
                CargarDatosIniciales();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método de guardado para la entidad Garantia
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado = 0;

            int anioFecha = txtFechaExpedicionVigenciaFutura.Date.Year;
            int anioVigencia = Convert.ToInt32(ddlAnio.SelectedValue);

            if(anioFecha != anioVigencia)
            {
                VigenciaFuturas vVigenciaFuturas = new VigenciaFuturas();
                vVigenciaFuturas.NumeroRadicado = txtNumeroRadicadoVigenciaFutura.Text;
                vVigenciaFuturas.FechaExpedicion = txtFechaExpedicionVigenciaFutura.Date;
                vVigenciaFuturas.ValorVigenciaFutura = decimal.Parse(txtValorVigenciaFutura.Text);
                vVigenciaFuturas.AnioVigencia = Convert.ToInt32(ddlAnio.SelectedValue);
                vVigenciaFuturas.Estado = 1;

                string strValidacion = ValidarReglasDeNegocio(vVigenciaFuturas);
                if(strValidacion != string.Empty)
                {
                    toolBar.MostrarMensajeError(strValidacion);
                    return;
                }

                vVigenciaFuturas.IdContrato = Convert.ToInt32(Request.QueryString["idContrato"]);
                vVigenciaFuturas.UsuarioCrea = GetSessionUser().NombreUsuario;
                vVigenciaFuturas.FechaCrea = DateTime.Now;
                InformacionAudioria(vVigenciaFuturas, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarVigenciaFuturas(vVigenciaFuturas);

                if(vResultado == 0)
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                else if(vResultado == 1)
                    GetScriptCloseDialogCallback(string.Empty);
            }
            else
                toolBar.MostrarMensajeError("La vigencía de la fecha es diferente a la seleccionada.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Vigencias Futuras", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        { 
            int idContrato = Convert.ToInt32(Request.QueryString["idContrato"]);
            int vigenciaInicial = Convert.ToInt32(Request.QueryString["idVigenciaInicial"]);
            int vigenciaFinal = Convert.ToInt32(Request.QueryString["idVigenciaFinal"]);

            SIAService vSIAService = new SIAService();            
            ddlAnio.DataSource = vSIAService.ConsultarVigencias(true).Where(x=> x.AcnoVigencia > vigenciaInicial && x.AcnoVigencia <= vigenciaFinal).OrderBy(x=> x.AcnoVigencia);
            ddlAnio.DataTextField = "AcnoVigencia";
            ddlAnio.DataValueField = "AcnoVigencia";            
            ddlAnio.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Validación de reglas de negocio
    /// </summary>
    /// <param name="vGarantia">Objeto garantía</param>
    /// <returns></returns>
    private string ValidarReglasDeNegocio(VigenciaFuturas vVigenciaFuturas)
    {
        return string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlAnioSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
    }

    #endregion 

}
