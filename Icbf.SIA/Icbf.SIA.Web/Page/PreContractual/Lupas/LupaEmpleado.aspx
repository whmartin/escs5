﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaEmpleado.aspx.cs" Inherits="Page_PreContractual_Lupas_LupaEmpleado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
<script type="text/javascript" language="javascript">
    function SoloNumero(e) {
        var charCode = (e.which) ? e.which : event.keyCode;
        
        if (charCode > 31 && (charCode < 48 || charCode > 57)) { //Solo Números
            return false;
        }

        return true;
    }

    function LoadingBuscar() {
        $("tr.headerForm").find("td").css("background-color", "white");

        var imgLoading = document.getElementById("imgLoading");
        imgLoading.style.visibility = "visible";
    }
</script>
<asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell" >
                Tipo Identificación
            </td>
            <td class="Cell" >
                Número Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList ID="ddlTipoIdentificacion" runat="server" ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:TextBox ID="txtNumeroIdentificacion" runat="server" MaxLength="11" onkeypress="return SoloNumero(event)" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" >
                Tipo Vinculación Contractual
            </td>
            <td class="Cell" >
                Regional
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList ID="ddlTipoVinculacionContractual" runat="server" ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlRegional" runat="server" ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" >
                Primer Nombre
            </td>
            <td class="Cell" >
                Segundo Nombre
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox ID="txtPrimerNombre" runat="server" MaxLength="50" ></asp:TextBox>
            </td>
            <td class="Cell">
                <asp:TextBox ID="txtSegundoNombre" runat="server" MaxLength="50" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" >
                Primer Apellido
            </td>
            <td class="Cell" >
                Segundo Apellido
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox ID="txtPrimerApellido" runat="server" MaxLength="50" ></asp:TextBox>
            </td>
            <td class="Cell">
                <asp:TextBox ID="txtSegundoApellido" runat="server" MaxLength="50" ></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<asp:Panel runat="server" ID="pnlLista">
    <table width="90%" align="center" id="tbResultados" runat="server" >
        <tr class="rowB">
            <td class="Cell" >CONSULTA EMPLEADO</td>
        </tr>
        <tr class="rowAG">
            <td>
                <asp:GridView runat="server" ID="gvEmpleados" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" CellPadding="8" 
                    DataKeyNames="PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,Direccion,Telefono,IdRegional,IdDependencia,IdCargo" 
                    Height="16px" AllowSorting="false" OnPageIndexChanging="gvEmpleados_PageIndexChanging"
                    OnSelectedIndexChanged="gvEmpleados_SelectedIndexChanged" OnSorting="gvEmpleados_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Seleccionar" >
                            <ItemTemplate>
                                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                    Height="16px" Width="16px" ToolTip="Seleccionar" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion"  SortExpression="TipoIdentificacion"/>
                        <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion"  SortExpression="NumeroIdentificacion"/>
                        <asp:BoundField HeaderText="Tipo Vinculación Contractual" DataField="TipoVinculacionContractual"  SortExpression="TipoVinculacionContractual"/>
                        <asp:BoundField HeaderText="Nombre Empleado" DataField="NombreEmpleado"  SortExpression="NombreEmpleado"/>
                        <asp:BoundField HeaderText="Regional" DataField="Regional"  SortExpression="Regional"/>
                        <asp:BoundField HeaderText="Dependencia" DataField="Dependencia"  SortExpression="Dependencia"/>
                        <asp:BoundField HeaderText="Cargo" DataField="Cargo"  SortExpression="Cargo"/>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:Content>