﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" 
CodeFile="LupaConsultarLugarContrato.aspx.cs" Inherits="Page_Precontractual_Lupas_ConsultarLugarContrato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

<asp:Panel runat="server" ID="Panel1">
         <div>
            <asp:Label ID="LbError" runat="server" Text=""></asp:Label>
        </div>
        <div>
        <asp:Panel runat="server" ID="pnlConsulta">
                   
            <table width="90%" align="center">
                 <tr class="rowB">
                    <td>
                       Nivel Nacional
                    </td>
                    <td>
                      <asp:Label ID="lblDescripcionNivelNacional" Text="Descripción" runat="server" />
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:CheckBox ID="chkNivelNacional" runat="server" AutoPostBack="True" 
                            oncheckedchanged="chkNivelNacional_CheckedChanged" />
                    </td>
                    <td><span>
                        <asp:TextBox ID="txtDatosAdicionalesLugarEjecucion" runat="server"  TextMode="MultiLine" Width="80%"></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftDatosAdicionalesLugarEjecucion" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters" TargetControlID="txtDatosAdicionalesLugarEjecucion" ValidChars="áéíóúÁÉÍÓÚñÑ -" />
                        </span></td>
                </tr>
                <tr class="rowB">
                    <td>
                        Departamento *
                        <asp:RequiredFieldValidator runat="server" ID="rfvddlDepartamento" ControlToValidate="ddlDepartamento"
                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="AgregarLugar"
                             ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        Municipio *
                        <asp:RequiredFieldValidator runat="server" ID="rfvMunicipio" ControlToValidate="lbxMunicipio"
                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="AgregarLugar"
                             ForeColor="Red" ></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td style="vertical-align:top;">
                        
                        <asp:DropDownList ID="ddlDepartamento" runat="server" OnSelectedIndexChanged="lbxDepartamento_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                        
                    </td>
                    <td >
                        
                       <asp:ListBox ID="lbxMunicipio" runat="server" SelectionMode="Multiple" Rows="8">
                       </asp:ListBox>
                    </td>
                </tr>
               
            </table>
            </asp:Panel>
           <%-- <asp:Panel runat="server" ID="pnlLista">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvLugarContrato" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="" CellPadding="0" Height="16px"
                                OnSorting="gvLugarContrato_Sorting" AllowSorting="True" 
                                OnPageIndexChanging="gvLugarContrato_PageIndexChanging" OnSelectedIndexChanged="gvLugarContrato_SelectedIndexChanged">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                Height="16px" Width="16px" ToolTip="Seleccionar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Departamento" DataField="Departamento"  SortExpression="Departamento"/>
                                    <asp:BoundField HeaderText="Municipio" DataField="Municipio"  SortExpression="Municipio"/>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>--%>
        </div>
    </asp:Panel>

</asp:Content>
