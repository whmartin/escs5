using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;
using System.Linq.Expressions;

public partial class Page_PreContractual_Reparto_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "PreContractual/Reparto";

    SIAService vRuboService = new SIAService();

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (Page.IsPostBack)
            {
                try
                {
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch(sControlName)
                    {
                        case "RepartoSolicitud":
                        if(!string.IsNullOrEmpty(GetSessionParameter("SolicitudContrato.AsignacionUsuario").ToString()))
                        {
                            toolBar.MostrarMensajeGuardado(GetSessionParameter("SolicitudContrato.AsignacionUsuario").ToString());
                            RemoveSessionParameter("SolicitudContrato.AsignacionUsuario");
                        }
                        Buscar();
                        break;
                        default:
                        break;
                    }
                }
                catch(UserInterfaceException ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
                catch(Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
            }
            else
                CargarDatosIniciales();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void gvSolicitudesContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSolicitudesContrato.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvSolicitudesContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSolicitudesContrato.SelectedRow);
    }

    protected void gvSolicitudesContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIDCategoriaContrato.SelectedValue != "-1")
        {
            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;
        }
        else
        {
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
        }

    }

    protected void gvSolicitudesContrato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            string estadoSol = DataBinder.Eval(e.Row.DataItem, "EstadoSolicitud").ToString();

            if(estadoSol == "Registro")
            {
               e.Row.FindControl("btnEditar").Visible = false ;
            }
            else
            {
                LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
                string id = DataBinder.Eval(e.Row.DataItem, "IdSolicitud").ToString();
                string script = string.Format("ShowRepartoSolicitud('{0}'); return false;", id);
                button.Attributes.Add("onclick", script);
            }
        }
    }

    private void Buscar()
    {
        try
        {
            CargarGrilla(gvSolicitudesContrato, GridViewSortExpression, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            gvSolicitudesContrato.PageSize = PageSize();
            gvSolicitudesContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Reparto de Solicitudes de Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        string controlFechaValidando = string.Empty;
        try
        {
            DateTime? vFechaRegistroSistemaDesde = null;
            DateTime? vFechaRegistroSistemaHasta = null;
            int? vIdSolicitudContrato = null;
            int? vVigenciaFiscalinicial = null;
            int? vIDRegional = null;
            int? vIDModalidadSeleccion = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            int? vIDEstadoSolicitud = null;

            controlFechaValidando = "Fecha Registro al Sistema Desde";
            if (txtFechaRegistroSistemaDesde.Date.Year.ToString() != "1900")
            {
                vFechaRegistroSistemaDesde = Convert.ToDateTime(txtFechaRegistroSistemaDesde.Date);
            }

            controlFechaValidando = string.Empty;

            controlFechaValidando = "Fecha Registro al Sistema Hasta";
            if (txtFechaRegistroSistemaHasta.Date.Year.ToString() != "1900")
            {
                vFechaRegistroSistemaHasta = Convert.ToDateTime(txtFechaRegistroSistemaHasta.Date);
            }
            controlFechaValidando = string.Empty;
            if (vFechaRegistroSistemaHasta != null)
            {
                TimeSpan ts = new TimeSpan(0, 23, 59, 59);
                vFechaRegistroSistemaHasta = Convert.ToDateTime(vFechaRegistroSistemaHasta) + ts;
            }

            if (txtIdSolicitud.Text != "")
                vIdSolicitudContrato = Convert.ToInt32(txtIdSolicitud.Text);
            
            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
                vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            
            if (ddlIDRegional.SelectedValue != "-1")
                vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            
            if (ddlIDModalidadSeleccion.SelectedValue != "-1")
                vIDModalidadSeleccion = Convert.ToInt32(ddlIDModalidadSeleccion.SelectedValue);
            
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
            
            if (ddlIDTipoContrato.SelectedValue != "-1" && ddlIDTipoContrato.SelectedValue != string.Empty)
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
            
            if (ddlEstadoSolicitud.SelectedValue != "-1")
                vIDEstadoSolicitud = Convert.ToInt32(ddlEstadoSolicitud.SelectedValue);

            string usuarioArea = null;

            if (GetSessionUser().IdTipoUsuario != 1)
                usuarioArea = GetSessionUser().NombreUsuario;

            var myGridResults = vPrecontractualService.ConsultarSolicitudContrato(vFechaRegistroSistemaDesde, vFechaRegistroSistemaHasta, vIdSolicitudContrato, vVigenciaFiscalinicial, vIDRegional, vIDModalidadSeleccion, vIDCategoriaContrato, vIDTipoContrato, vIDEstadoSolicitud,usuarioArea,null);

                int nRegistros = myGridResults.Count;
                int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

                if (nRegistros < NumRegConsultaGrilla)
                {
                    if (expresionOrdenamiento != null)
                    {
                        if (string.IsNullOrEmpty(GridViewSortExpression))
                            GridViewSortDirection = SortDirection.Ascending;
                        else if (GridViewSortExpression != expresionOrdenamiento)
                            GridViewSortDirection = SortDirection.Descending;

                        if (myGridResults != null)
                        {
                            var param = Expression.Parameter(typeof(SolicitudContrato), expresionOrdenamiento);

                            var prop = Expression.Property(param, expresionOrdenamiento);

                            var sortExpression = Expression.Lambda<Func<SolicitudContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                            if (GridViewSortDirection == SortDirection.Ascending)
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Descending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Ascending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }

                            GridViewSortExpression = expresionOrdenamiento;
                        }
                    }
                    else
                        gridViewsender.DataSource = myGridResults;
                    
                    gridViewsender.DataBind();
                }
                else
                    toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inv�lido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaVigencia();
            CargarListaRegional();
            CargarModalidadSeleccion();
            CargarCategoriaContrato();
            CargarEstados();

            if (! string.IsNullOrEmpty(GetSessionParameter("SolicitudContrato.CambioEstado").ToString()))
            {
                toolBar.MostrarMensajeGuardado(GetSessionParameter("SolicitudContrato.CambioEstado").ToString());
                RemoveSessionParameter("SolicitudContrato.CambioEstado");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {
        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {
            if (usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }
                }
            }
            else
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
        }
    }

    public void CargarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void CargarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void CargarEstados()
    {
        ddlEstadoSolicitud.Items.Clear();
        List<SolicitudContratoEstado> vLEstados = vPrecontractualService.ConsultarEstados();
        foreach (SolicitudContratoEstado tD in vLEstados)
            ddlEstadoSolicitud.Items.Add(new ListItem(tD.Nombre, tD.IdEstadoSolicitud.ToString()));
        
        ddlEstadoSolicitud.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSolicitudesContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SolicitudContrato.IdSolicitud", strValue);
            NavigateTo("~/Page/Precontractual/Solicitudes/Detail.aspx?origen=R");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
