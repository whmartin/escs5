﻿using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Contratos_Garantia_GenerarRptGarantia : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/CertificacionGarantia";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                string vIDCertificacion = GetSessionParameter("RptCertificacionesGarantia.IdGarantia").ToString();
                RemoveSessionParameter("RptCertificacionesGarantia.IdGarantia");
                HfIdGarantia.Value = vIDCertificacion;
            }
        }

    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.eventoReporte += ToolBar_eventoReporte;
            toolBar.EstablecerTitulos("Certificaci&oacute;n de las garant&iacute;as", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ToolBar_eventoReporte(object sender, EventArgs e)
    {
        GenerarReporte();
    }

    private void GenerarReporte()
    {

        try
        {
            #region Creacion reporte


            //Crea un objeto de tipo reporte
            Report objReport;
            objReport = new Report("CertificadoGarantia", true, PageName, "Generar Certificaci&oacute;n");


            //Adiciona los parametros al objeto reporte
            objReport.AddParameter("IDGarantia", HfIdGarantia.Value);
            objReport.AddParameter("NombreFirma", txtNombreUsuario.Text.ToUpper());
            objReport.AddParameter("CargoFirma", txtCargoUsuario.Text.ToUpper());
            objReport.AddParameter("UsuarioGenero", GetSessionUser().NombreUsuario);

            //

            //Crea un session con el objeto Reporte
            SetSessionParameter("Report", objReport);

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
            #endregion

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }


    }
}