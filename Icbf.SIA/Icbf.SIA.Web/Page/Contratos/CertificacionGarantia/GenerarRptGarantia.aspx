﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="GenerarRptGarantia.aspx.cs" Inherits="Page_Contratos_Garantia_GenerarRptGarantia" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
     <asp:HiddenField ID="HfIdGarantia" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
           <%-- <td>Nombre Usuario Firma</td>--%>
            <td class="style1" style="width: 50%">
                Nombre Usuario Firma *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreUsuario" ControlToValidate="txtNombreUsuario"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <%--<td>Cargo Usuario</td>--%>
            <td class="style1" style="width: 50%">
               Cargo Usuario Firma *
                <asp:RequiredFieldValidator runat="server" ID="rfvCargoUsuario" ControlToValidate="txtCargoUsuario"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreUsuario" Width="200px" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreUsuario" runat="server" TargetControlID="txtNombreUsuario"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCargoUsuario" Width="200px" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCargoUsuario" runat="server" TargetControlID="txtCargoUsuario"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
        </tr>
    </table>
</asp:Content>
