﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" 
    CodeFile="List.aspx.cs"
    Inherits="Page_Contratos_CertificacionGarantia_List" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>N&uacute;mero del Contrato / Convenio
                </td>
                <td>N&uacute;mero Garantia</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroContrato" Width="200px" MaxLength="50"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroGarantia" Width="200px" MaxLength="20"></asp:TextBox>
                </td>
            </tr>

        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                   <asp:GridView ID="gvGarantias" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CellPadding="0" DataKeyNames="IDGarantia,IDSucursalAseguradoraContrato,CodigoEstadoGarantia" 
                            GridLines="None" Height="16px" OnPageIndexChanging="gvGarantias_PageIndexChanging" OnSelectedIndexChanged="gvGarantias_SelectedIndexChanged"
                             Width="100%" AllowSorting="True" OnSorting="gvGarantias_Sorting" EmptyDataText="No se encontraron datos, por favor verifique.">
                        <Columns>
                            <asp:TemplateField HeaderText="Acciones">
                                <HeaderTemplate>
                                    Generar
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgRptGarantia" runat="server" CommandName="Select"
                                            Height="16px"
                                            Width="16px"
                                            ImageUrl="~/Image/btn/Report.png"
                                            ToolTip="Ver Reporte"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="IdContrato" HeaderText="Id Contrato" />
                            <asp:BoundField DataField="NumeroContrato" HeaderText="Número de Contrato" />
                            <asp:BoundField DataField="NumeroGarantia" HeaderText="Número de Garantía" />
                            <asp:BoundField DataField="NombreTipoGarantia" HeaderText="Tipo de Garantía" />
                            <asp:BoundField DataField="FechaInicioGarantiaView" HeaderText="Fecha Vigencia Desde" />
                            <asp:BoundField DataField="FechaVencimientoFinalGarantiaView" HeaderText="Fecha Vigencia Hasta" />
                            <asp:TemplateField HeaderText="Valor Asegurado" ItemStyle-HorizontalAlign="Center" SortExpression="ValorAsegurado">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 150px;">
                                        <%# Eval("ValorGarantia").ToString()== ""?"":Convert.ToDecimal(Eval("ValorGarantia")).ToString("$ #,###0.00##;($ #,###0.00##)")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionEstadoGarantia" HeaderText="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
