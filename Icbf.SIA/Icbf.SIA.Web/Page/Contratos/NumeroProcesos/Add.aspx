<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_NumeroProcesos_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIdNumeroProceso" runat="server" />
    <table width="90%" align="center">
        
        <tr class="rowB">
            <td>
                Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Nombre Modalidad de Selección *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdModalidadSeleccion" ControlToValidate="ddlIdModalidadSeleccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdModalidadSeleccion" ControlToValidate="ddlIdModalidadSeleccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
          <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional"></asp:DropDownList>
                <%--<img id="imghIdRegional" alt="help" title="Corresponde a la regional" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdRegional')" onmouseout="helpOut('imghIdRegional')"/>--%>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlIdModalidadSeleccion_SelectedIndexChanged"></asp:DropDownList>
                <%--<img id="imghIdModalidadSeleccion" alt="help" title="Corresponde a la modalidad de selecci&#243;n" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdModalidadSeleccion')" onmouseout="helpOut('imghIdModalidadSeleccion')"/>--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Sigla Modalidad de Selecci&oacute;n
            </td>
            <td>
                Vigencia *
                    <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>--%>
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtSigla" MaxLength="5" Enabled="False"></asp:TextBox>

            </td>
            <td>
                <%--<asp:TextBox runat="server" ID="txtVigencia" MaxLength="4"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtVigencia"
                    FilterType="Numbers" ValidChars="" />--%>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
                <%--<img id="imghIdVigencia" alt="help" title="Corresponde al a&#241;o de vigencia" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdVigencia')" onmouseout="helpOut('imghIdVigencia')"/>--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                N&uacute;mero de Proceso *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroProceso" ControlToValidate="txtNumeroProceso"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Fecha de Adjudicación *
                <asp:RequiredFieldValidator runat="server" ID="rvlFechaAdjudiacion" ControlToValidate="txtFecha"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProceso" MaxLength="5" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroProceso" runat="server" TargetControlID="txtNumeroProceso"
                    FilterType="Numbers" ValidChars="" />
                <%--<img id="imghNumeroProceso" alt="help" title="Es el n&#250;mero del proceso." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghNumeroProceso')" onmouseout="helpOut('imghNumeroProceso')"/>--%>
            </td>
            
                < <td class="auto-style1" colspan="2">
                <asp:TextBox runat="server" ID="txtFecha"  ></asp:TextBox>
                <asp:Image ID="imgCalendarioFin" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetxtFechaFin" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioFin" TargetControlID="txtFecha"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxtFechaFin" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
                    </Ajax:MaskedEditExtender>
            </td>
            
        </tr>
         <tr class="rowB">
            
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvInactivo" ControlToValidate="rblInactivo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal"></asp:RadioButtonList>
                <%--<img id="imghInactivo" alt="help" title="Corresponde al estado del registro." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghInactivo')" onmouseout="helpOut('imghInactivo')"/>--%>
            </td>
        </tr>

    </table>
</asp:Content>
