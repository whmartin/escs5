<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_NumeroProcesos_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdNumeroProceso" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Regional
            </td>
            <td>
                Nombre Modalidad de Selecci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidadSeleccion"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Numero 
                de Proceso (Generado)</td>
            <td>
                Vigencia
            </td>
            
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProcesoGenerado"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"  Enabled="false"></asp:DropDownList>
            </td>
            
        </tr>
        <tr class="rowB">
            <td>
                Número de Proceso
            </td>
            <td>
                Fecha de Adjudicación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProceso"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFecha"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
             <td>
                Estado
            </td>
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>

        
    </table>
</asp:Content>
