using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;


/// <summary>
/// Página de visualización detallada para la entidad NumeroProcesos
/// </summary>
public partial class Page_NumeroProcesos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/NumeroProcesos";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("NumeroProcesos.IdNumeroProceso", hfIdNumeroProceso.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdNumeroProceso = Convert.ToInt32(GetSessionParameter("NumeroProcesos.IdNumeroProceso"));
            RemoveSessionParameter("NumeroProcesos.IdNumeroProceso");

            if (GetSessionParameter("NumeroProcesos.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("NumeroProcesos.Guardado");


            NumeroProcesos vNumeroProcesos = new NumeroProcesos();
            vNumeroProcesos = vContratoService.ConsultarNumeroProcesos(vIdNumeroProceso);
            hfIdNumeroProceso.Value = vNumeroProcesos.IdNumeroProceso.ToString();
            txtNumeroProceso.Text = vNumeroProcesos.NumeroProceso.ToString();
            rblInactivo.SelectedValue = vNumeroProcesos.Inactivo.ToString().Trim().ToLower();
            txtNumeroProcesoGenerado.Text = vNumeroProcesos.NumeroProcesoGenerado;
            txtFecha.Text = vNumeroProcesos.FechaAdjudicacion.ToShortDateString();
            ddlIdModalidadSeleccion.SelectedValue = vNumeroProcesos.IdModalidadSeleccion.ToString();
            ddlIdRegional.SelectedValue = vNumeroProcesos.IdRegional.ToString();
            ddlIdVigencia.SelectedValue = vNumeroProcesos.IdVigencia.ToString();
            ObtenerAuditoria(PageName, hfIdNumeroProceso.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vNumeroProcesos.UsuarioCrea, vNumeroProcesos.FechaCrea, vNumeroProcesos.UsuarioModifica, vNumeroProcesos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdNumeroProceso = Convert.ToInt32(hfIdNumeroProceso.Value);

            NumeroProcesos vNumeroProcesos = new NumeroProcesos();
            vNumeroProcesos = vContratoService.ConsultarNumeroProcesos(vIdNumeroProceso);

            if (vContratoService.ConsultarNumeroProcesosPorContrato(vNumeroProcesos.IdNumeroProceso))
            {
                toolBar.MostrarMensajeError("El registro tiene elementos  que dependen de él, verifique por favor.");
                return;
            }
            InformacionAudioria(vNumeroProcesos, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarNumeroProcesos(vNumeroProcesos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("NumeroProcesos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Número de Proceso", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            CargarComboModalidad();
            CargarComboRegional();
            CargarComboVigencia();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarComboModalidad()
    {

        ManejoControlesContratos.LlenarComboLista(ddlIdModalidadSeleccion,
                                                  vContratoService.ConsultarModalidadSeleccions(null, null, true),
                                                  "IdModalidad", "Nombre");
    }

    public void CargarComboRegional()
    {
        ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vContratoService.ConsultarRegionals(null, null),
                                                  "IdRegional", "NombreRegional");
    }

    public void CargarComboVigencia()
    {
        List<Vigencia> lstVigencia = vRuboService.ConsultarVigencias(true);
        ddlIdVigencia.DataSource = lstVigencia;
        ddlIdVigencia.DataValueField = "IdVigencia";
        ddlIdVigencia.DataTextField = "AcnoVigencia";
        ddlIdVigencia.DataBind();
        ddlIdVigencia.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        ddlIdVigencia.SelectedIndex = -1;

    }

}
