<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_NumeroProcesos_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Regional
                </td>
                <td class="Cell">
                    Vigencia
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdRegional">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdVigencia">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Estado
                </td>
                <td class="Cell">
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td class="Cell">
                    <asp:ImageButton ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClientClick="GetRepLegal()" Style="cursor: hand" ToolTip="Buscar" OnClick="imgBcodigoUsuario_Click"
                        Height="20px" Width="20px" Visible="False" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvNumeroProcesos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdNumeroProceso" CellPadding="0"
                        Height="16px" OnSorting="gvNumeroProcesos_Sorting" AllowSorting="True" OnPageIndexChanging="gvNumeroProcesos_PageIndexChanging"
                        OnSelectedIndexChanged="gvNumeroProcesos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número Proceso" DataField="NumeroProceso" SortExpression="NumeroProceso"
                                Visible="False" />
                            <asp:BoundField HeaderText="Número de Proceso (Generado)" DataField="NumeroProcesoGenerado"
                                SortExpression="NumeroProcesoGenerado" />
                            <asp:TemplateField HeaderText="Estado" SortExpression="Inactivo">
                                <ItemTemplate>
                                    <asp:Label ID="lblInactivo" runat="server" Text='<%# (bool) Eval("Inactivo") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre modalidad selección" DataField="IdModalidadSeleccion"
                                SortExpression="IdModalidadSeleccion" Visible="False" />
                            <asp:BoundField HeaderText="Regional" DataField="IdRegional" SortExpression="IdRegional"
                                Visible="False" />
                            <asp:BoundField HeaderText="Vigencia" DataField="IdVigencia" SortExpression="IdVigencia"
                                Visible="False" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <script type="text/javascript" language="javascript">
                function limitText(limitField, limitNum) {
                    if (limitField.value.length > limitNum) {
                        limitField.value = limitField.value.substring(0, limitNum);
                    }
                }

                function GetRepLegal() {
                    window_showModalDialog('../../../Page/Contratos/Lupas/LupaRegInfoPresupuestal.aspx', "", 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                }
            </script>
        </table>
    </asp:Panel>
</asp:Content>
