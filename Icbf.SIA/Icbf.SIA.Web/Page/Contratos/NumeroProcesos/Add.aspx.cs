using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición para la entidad NumeroProcesos
/// </summary>
public partial class Page_NumeroProcesos_Add : GeneralWeb
{
    private masterPrincipal toolBar;
    private ContratoService vContratoService = new ContratoService();
    private string PageName = "Contratos/NumeroProcesos";
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad NumeroProcesos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            NumeroProcesos vNumeroProcesos = new NumeroProcesos();

            vNumeroProcesos.NumeroProceso = Convert.ToInt32(txtNumeroProceso.Text);
            vNumeroProcesos.Inactivo = Convert.ToBoolean(rblInactivo.SelectedValue);
            //vNumeroProcesos.NumeroProcesoGenerado = Convert.ToString(txtNumeroProcesoGenerado.Text);
            vNumeroProcesos.NumeroProcesoGenerado = GenerarNumeroProceso();
            vNumeroProcesos.IdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            vNumeroProcesos.IdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            vNumeroProcesos.FechaAdjudicacion = Convert.ToDateTime(txtFecha.Text);
            //vNumeroProcesos.IdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            vNumeroProcesos.IdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            if (Request.QueryString["oP"] == "E")
            {
                var Proceso = vContratoService.ConsultarNumeroProcesoss(Convert.ToInt32(hfIdNumeroProceso.Value),null,null, null, vNumeroProcesos.IdModalidadSeleccion, vNumeroProcesos.IdRegional, vNumeroProcesos.IdVigencia);
                if (Proceso.Count > 0)
                {
                    vNumeroProcesos.IdNumeroProceso = Convert.ToInt32(hfIdNumeroProceso.Value);
                    vNumeroProcesos.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vNumeroProcesos, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarNumeroProcesos(vNumeroProcesos);
                }
                else
                {
                    List<NumeroProcesos> lNumeroProcesos = vContratoService.ConsultarNumeroProcesoss(null,vNumeroProcesos.NumeroProceso, null,vNumeroProcesos.NumeroProcesoGenerado, vNumeroProcesos.IdModalidadSeleccion, vNumeroProcesos.IdRegional, vNumeroProcesos.IdVigencia);
                    if (lNumeroProcesos.Count > 0)
                    {
                        toolBar.MostrarMensajeError("Registro Duplicado, verifique por favor.");
                        return;
                    }

                    vNumeroProcesos.IdNumeroProceso = Convert.ToInt32(hfIdNumeroProceso.Value);
                    vNumeroProcesos.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vNumeroProcesos, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarNumeroProcesos(vNumeroProcesos);
                }
               
            }
            else
            {
                vNumeroProcesos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vNumeroProcesos, this.PageName, vSolutionPage);
                List<NumeroProcesos> lNumeroProcesos = vContratoService.ConsultarNumeroProcesoss(null, vNumeroProcesos.NumeroProceso, null, vNumeroProcesos.NumeroProcesoGenerado, vNumeroProcesos.IdModalidadSeleccion, vNumeroProcesos.IdRegional, vNumeroProcesos.IdVigencia);
                if (lNumeroProcesos.Count>0)
                {
                    toolBar.MostrarMensajeError("Registro Duplicado, verifique por favor.");
                    return;
                }
                vResultado = vContratoService.InsertarNumeroProcesos(vNumeroProcesos);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("NumeroProcesos.IdNumeroProceso", vNumeroProcesos.IdNumeroProceso);
                SetSessionParameter("NumeroProcesos.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError(
                    "La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal) this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Número de Proceso", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdNumeroProceso = Convert.ToInt32(GetSessionParameter("NumeroProcesos.IdNumeroProceso"));
            int vIdNumeroProceso = 0;

            if (int.TryParse(GetSessionParameter("NumeroProcesos.IdNumeroProceso").ToString(), out vIdNumeroProceso))
            {

                RemoveSessionParameter("NumeroProcesos.Id");

                NumeroProcesos vNumeroProcesos = new NumeroProcesos();
                vNumeroProcesos = vContratoService.ConsultarNumeroProcesos(vIdNumeroProceso);
                hfIdNumeroProceso.Value = vNumeroProcesos.IdNumeroProceso.ToString();
                txtNumeroProceso.Text = vNumeroProcesos.NumeroProceso.ToString();
                rblInactivo.SelectedValue = vNumeroProcesos.Inactivo.ToString();
                //txtNumeroProcesoGenerado.Text = vNumeroProcesos.NumeroProcesoGenerado;
                ddlIdModalidadSeleccion.SelectedValue = vNumeroProcesos.IdModalidadSeleccion.ToString();
                ddlIdRegional.SelectedValue = vNumeroProcesos.IdRegional.ToString();
                ddlIdVigencia.SelectedValue = vNumeroProcesos.IdVigencia.ToString();
                txtFecha.Text = vNumeroProcesos.FechaAdjudicacion.ToShortDateString();
                //ddlIdVigencia.SelectedValue = vNumeroProcesos.IdVigencia.ToString();
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vNumeroProcesos.UsuarioCrea,
                                                                                   vNumeroProcesos.FechaCrea,
                                                                                   vNumeroProcesos.UsuarioModifica,
                                                                                   vNumeroProcesos.FechaModifica);

                if (ddlIdModalidadSeleccion.SelectedValue != "-1")
                {
                    ModalidadSeleccion modalidadSeleccion = new ModalidadSeleccion();
                    modalidadSeleccion =
                        vContratoService.ConsultarModalidadSeleccion(Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue));
                    if (!string.IsNullOrEmpty(modalidadSeleccion.Sigla))
                    {
                        txtSigla.Enabled = true;
                        txtSigla.Text = modalidadSeleccion.Sigla.Trim();
                        txtSigla.Enabled = false;
                    }


                }
                if (vNumeroProcesos.Inactivo)
                {
                    rblInactivo.SelectedIndex = 1;
                }
                else
                {
                    rblInactivo.SelectedIndex = 0;

                }
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.SelectedValue = "true";
            CargarComboModalidad();
            CargarComboRegional();
            CargarComboVigencia();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarComboModalidad()
    {

        ManejoControlesContratos.LlenarComboLista(ddlIdModalidadSeleccion,
                                                  vContratoService.ConsultarModalidadSeleccions(null, null, true),
                                                  "IdModalidad", "Nombre");
    }

    public void CargarComboRegional()
    {
        ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vContratoService.ConsultarRegionals(null, null),
                                                  "IdRegional", "NombreRegional");
    }

    public void CargarComboVigencia()
    {
        List<Vigencia> lstVigencia = vRuboService.ConsultarVigencias(true);
        ddlIdVigencia.DataSource = lstVigencia;
        ddlIdVigencia.DataValueField = "IdVigencia";
        ddlIdVigencia.DataTextField = "AcnoVigencia";
        ddlIdVigencia.DataBind();
        ddlIdVigencia.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        ddlIdVigencia.SelectedIndex = -1;

    }

    public string GenerarNumeroProceso()
    {
        string numeroProcesoGenerado;

        if (ddlIdRegional.SelectedValue != "-1" && !string.IsNullOrEmpty(txtNumeroProceso.Text) && ddlIdModalidadSeleccion.SelectedValue != "-1")
        {
            //List <ModalidadSeleccion> lModalidad = new List<ModalidadSeleccion>();
            //ModalidadSeleccion modalidadSeleccion = new ModalidadSeleccion();
            //modalidadSeleccion =
            //    vContratoService.ConsultarModalidadSeleccion(Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue));

            string vConsecutivo = txtNumeroProceso.Text.Trim().PadLeft(5, '0');

            Regional vRegional = new Regional();
            vRegional = vRuboService.ConsultarRegional(Convert.ToInt32(ddlIdRegional.SelectedValue));

            numeroProcesoGenerado = vRegional.CodigoRegional.Trim() + "." + txtSigla.Text.Trim() + "." +
                                    vConsecutivo + "." + ddlIdVigencia.SelectedItem.Text.Substring(2);

            return numeroProcesoGenerado;


        }
        else
        {
            return string.Empty;
               //throw new Exception("No se pudo generar el cógigo, consunte con el administrador");
        }

        
           
    }

    protected void ddlIdModalidadSeleccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdModalidadSeleccion.SelectedValue != "-1")
        {
            ModalidadSeleccion modalidadSeleccion = new ModalidadSeleccion();
            modalidadSeleccion =
                vContratoService.ConsultarModalidadSeleccion(Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue));
            if (!string.IsNullOrEmpty(modalidadSeleccion.Sigla))
            {
                txtSigla.Enabled = true;
                txtSigla.Text = modalidadSeleccion.Sigla.Trim();
                txtSigla.Enabled = false;
            }
            

        }
        
        

    }
}
