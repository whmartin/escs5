<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contrato_Add" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

<asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlContrato">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Fecha Registro
            </td>
            <td>
                Tipo Contrato
                <asp:RequiredFieldValidator ID="rfvdllIdTipoContrato" runat="server" 
                    ControlToValidate="dllIdTipoContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvdllIdTipoContrato" runat="server" 
                    ControlToValidate="dllIdTipoContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                
                <uc1:fecha ID="fechaRegistro" runat="server" />
                
            </td>
            <td>
                <asp:DropDownList ID="dllIdTipoContrato" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
                Número Contrato Principal
                <asp:RequiredFieldValidator ID="rfvtxtNumeroContratoPrincipal" runat="server" 
                    ControlToValidate="txtNumeroContratoPrincipal" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td>
                Número de Contrato de Adhesión</td>
        </tr>
        <tr class="rowA">
            <td>
            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoPrincipal" />
                <asp:TextBox runat="server" ID="txtNumeroContratoPrincipal" MaxLength="25" 
                    Enabled="false" ClientIDMode="Static"></asp:TextBox><asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetContrato('hdIDContratoPrincipal,txtNumeroContratoPrincipal','0,3')" Style="cursor: hand" ToolTip="Buscar" />
            </td>
            <td>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoAdhesion" />
                <asp:TextBox ID="txtNumeroContratoAdhesion" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Requiere Acta Inicio
                <asp:RequiredFieldValidator runat="server" ID="rfvrblRequiereActaInicio" ControlToValidate="rblRequiereActaInicio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            <td>
                Maneja Aportes</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblRequiereActaInicio" runat="server">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList ID="rblManejaAportes" runat="server" 
                    RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional
                <asp:RequiredFieldValidator ID="rfvddlRegionalContrato" runat="server" 
                    ControlToValidate="ddlRegionalContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvddlRegionalContrato" runat="server" 
                    ControlToValidate="ddlRegionalContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlRegionalContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Código del Producto</td>
            <td>
                Nombre del Producto</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtCodigoProducto" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtNombreProducto" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad de Medida</td>
            <td>Cantidad</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtUnidadMedida" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtCantidad" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Unitario</td>
            <td>
                Valor Total</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtValorUnitario" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtValorTotal" runat="server"></asp:TextBox>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                Nombre del Solicitante</td>
            <td>
                  
                Dependencia Solicitante</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtNombreSolicitante" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato </td>
            <td>
                  
                Alcance Objeto Contrato</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtObjetoContrato" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px">
                    </asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtAlcanceObjetoContrato" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                &nbsp;</td>
            <td>
                  
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                &nbsp;</td>
            <td>
               
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Inicial Contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorInicialContrato" ControlToValidate="txtValorInicialContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtValorInicialContrato" MaxLength="50" 
                    Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Adiciones
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorTotalAdiciones" ControlToValidate="txtValorTotalAdiciones"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>   
            </td>
            <td>
                Valor Final Contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorFinalContrato" ControlToValidate="txtValorFinalContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalAdiciones" MaxLength="50" 
                    Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtValorTotalAdiciones" runat="server" TargetControlID="txtValorTotalAdiciones"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorFinalContrato" MaxLength="50" 
                    Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorFinalContrato" runat="server" TargetControlID="txtValorFinalContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Aportes ICBF
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorAportesICBF" ControlToValidate="txtValorAportesICBF"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
                </td>
            <td>
                Valor Aportes Operador 
                 <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorAportesOperador" ControlToValidate="txtValorAportesOperador"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesICBF" MaxLength="50" 
                    Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesICBF" runat="server" TargetControlID="txtValorAportesICBF"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesOperador" MaxLength="50" 
                    Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesOperador" runat="server" TargetControlID="txtValorAportesOperador"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Requiere Garantias</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblRequiereGarantias" runat="server">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Suscripciõn 
            </td>
            <td>Fecha Inicio Ejecución
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaSuscripcion" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaInicioEjecucion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Finalización Inicial
            </td>
            <td>Fecha Final Terminación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaFinalizacionInicial" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaFinalTerminacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Proyectada Liquidaciõn </td>
            <td>Fecha de Liquidacion</td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaProyectadaLiquidacion" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaLiquidacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Prorrogas
                <asp:RequiredFieldValidator ID="rfvtxtProrrogas" runat="server" 
                    ControlToValidate="txtProrrogas" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
             </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtProrrogas" runat="server" MaxLength="10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtProrrogas" runat="server" 
                    FilterType="Numbers" TargetControlID="txtProrrogas" ValidChars="" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
         <tr class="rowB">
            <td>
                Plazo Total de ejecucion
                <asp:RequiredFieldValidator ID="rfvtxtPlazoTotal" runat="server" 
                    ControlToValidate="txtPlazoTotal" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
             </td>
            <td>Fecha Firma Acta Inicio 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtPlazoTotal" runat="server" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtPlazoTotal_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtPlazoTotal" 
                    ValidChars="" />
            </td>
            <td>
                <uc1:fecha ID="fechaFirmaActaInicio" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad Ejecuciõn</td>
            <td>
                Valor Unidad Ejecución</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlUnidadEjecucion" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtValorUnidadEjecucion" runat="server" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftvtxtValorUnidadEjecucion" 
                    runat="server" FilterType="Numbers" TargetControlID="txtValorUnidadEjecucion" 
                    ValidChars="" /></td>
        </tr>
        <tr class="rowB">
            <td>
                Datos Adicionales lugar Ejecuciõn</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtDatosAdicionales" runat="server"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtDatosAdicionales_FilteredTextBoxExtender" 
                    runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters" 
                    TargetControlID="txtDatosAdicionales" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Abogado responsable del contrato
            </td>
            <td>
            </td>

        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtAbogadoResponsable" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Estado Contrato</td>
            <td>
                Forma Pago
                <asp:RequiredFieldValidator ID="rfvddlFormaPago" runat="server" 
                    ControlToValidate="ddlFormaPago" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvddlFormaPago" runat="server" 
                    ControlToValidate="ddlFormaPago" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlEstadoContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlFormaPago" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
