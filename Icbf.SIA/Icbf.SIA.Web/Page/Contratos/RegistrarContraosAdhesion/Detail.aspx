<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Contrato_Detail" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContrato" runat="server" />
     <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Fecha Registro
            </td>
            <td>
                Tipo Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                
                <asp:TextBox ID="txtFechaRegistro" runat="server" Enabled="False"></asp:TextBox>
                
            </td>
            <td>
                <asp:DropDownList ID="dllIdTipoContrato" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
                N�mero Contrato Principal</td>
            <td>
                N�mero de Contrato de Adhesi�n</td>
        </tr>
        <tr class="rowA">
            <td>
            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoPrincipal" />
                <asp:TextBox runat="server" ID="txtNumeroContratoPrincipal" MaxLength="25" 
                    Enabled="false" ClientIDMode="Static"></asp:TextBox>
            </td>
            <td>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdIDContratoAdhesion" />
                <asp:TextBox ID="txtNumeroContratoAdhesion" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Requiere Acta Inicio</td>
            <td>
                Maneja Aportes</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblRequiereActaInicio" runat="server" Enabled="False">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList ID="rblManejaAportes" runat="server" 
                    RepeatDirection="Horizontal" Enabled="False">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlRegionalContrato" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                C�digo del Producto</td>
            <td>
                Nombre del Producto</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtCodigoProducto" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtNombreProducto" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad de Medida</td>
            <td>Cantidad</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtUnidadMedida" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtCantidad" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Unitario</td>
            <td>
                Valor Total</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtValorUnitario" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtValorTotal" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                Nombre del Solicitante</td>
            <td>
                  
                Dependencia Solicitante</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtNombreSolicitante" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px" Enabled="False"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px" Enabled="False"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato </td>
            <td>
                  
                Alcance Objeto Contrato</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtObjetoContrato" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px" Enabled="False"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtAlcanceObjetoContrato" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px" Enabled="False"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                &nbsp;</td>
            <td>
                  
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                &nbsp;</td>
            <td>
               
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Inicial Contrato</td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtValorInicialContrato" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Adiciones
                </td>
            <td>
                Valor Final Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalAdiciones" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtValorTotalAdiciones" runat="server" TargetControlID="txtValorTotalAdiciones"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorFinalContrato" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorFinalContrato" runat="server" TargetControlID="txtValorFinalContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Aportes ICBF
                </td>
            <td>
                Valor Aportes Operador 
                 </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesICBF" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesICBF" runat="server" TargetControlID="txtValorAportesICBF"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesOperador" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesOperador" runat="server" TargetControlID="txtValorAportesOperador"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Requiere Garantias</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblRequiereGarantias" runat="server" Enabled="False">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Suscripci�n 
            </td>
            <td>Fecha Inicio Ejecuci�n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaSuscripcion" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaInicioEjecucion" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Finalizaci�n Inicial
            </td>
            <td>Fecha Final Terminaci�n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaFinalizacionInicial" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaFinalTerminacion" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Proyectada Liquidaci�n </td>
            <td>Fecha de Liquidacion</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaProyecradaLiquidacion" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaLiquidacion" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Prorrogas</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtProrrogas" runat="server" MaxLength="10" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtProrrogas" runat="server" 
                    FilterType="Numbers" TargetControlID="txtProrrogas" ValidChars="" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
         <tr class="rowB">
            <td>
                Plazo Total de ejecucion</td>
            <td>Fecha Firma Acta Inicio 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtPlazoTotal" runat="server" MaxLength="3" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtPlazoTotal_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtPlazoTotal" 
                    ValidChars="" />
            </td>
            <td>
                <asp:TextBox ID="txtFechaFirmaActaInicio" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad Ejecuci�n</td>
            <td>
                Valor Unidad Ejecuci�n</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlUnidadEjecucion" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtValorUnidadEjecucion" runat="server" MaxLength="3" 
                    Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftvtxtValorUnidadEjecucion" 
                    runat="server" FilterType="Numbers" TargetControlID="txtValorUnidadEjecucion" 
                    ValidChars="" /></td>
        </tr>
        <tr class="rowB">
            <td>
                Datos Adicionales lugar Ejecuci�n</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtDatosAdicionales" runat="server" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtDatosAdicionales_FilteredTextBoxExtender" 
                    runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters" 
                    TargetControlID="txtDatosAdicionales" ValidChars="/������������ .,@_():;" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Abogado responsable del contrato
            </td>
            <td>
            </td>

        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtAbogadoResponsable" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Estado Contrato</td>
            <td>
                Forma Pago</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlEstadoContrato" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlFormaPago" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
