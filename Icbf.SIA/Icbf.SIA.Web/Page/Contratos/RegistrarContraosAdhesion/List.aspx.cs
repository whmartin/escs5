using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de contratos de adhesi�n (registro)
/// </summary>
public partial class Page_Contrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RegistrarContraosAdhesion";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        //NavigateTo(SolutionPage.Add);
        SetSessionParameter("Contrato.Clase", "A");
        Response.Redirect("../GestionContratos/DetailA.aspx");
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            DateTime? vFechaRegistro = null;
            String vNumeroProceso = null;
            Decimal? vNumeroContrato = null;
            int? vIdModalidad = null;
            int? vIdCategoriaContrato = null;
            int? vIdTipoContrato = null;
            if (txtFechaRegistro.Date != null)      
            {
                if(txtFechaRegistro.Date.Year != 1900)
                {
                    vFechaRegistro = txtFechaRegistro.Date;
                }
            }
            if (txtNumeroProceso.Text!= "")
            {
                vNumeroProceso = Convert.ToString(txtNumeroProceso.Text);
            }
            if (txtNumeroContrato.Text!= "")
            {
                vNumeroContrato = Convert.ToDecimal(txtNumeroContrato.Text);
            }
            if (ddlIdTipoContrato.SelectedValue != "-1")
            {
                vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            }
            

            var result = from contrato in vContratoService.ConsultarContratos(vFechaRegistro, vNumeroProceso, vNumeroContrato, vIdModalidad, vIdCategoriaContrato, vIdTipoContrato,"A")
                         join tipoContrato in vContratoService.ConsultarTipoContratos(null, vIdCategoriaContrato, null,null, null, null, null, null)
                         on contrato.IdTipoContrato equals tipoContrato.IdTipoContrato
                         select new
                         {
                             contrato.IdContrato,
                             FechaRegistro = contrato.FechaRegistro.ToString("dd/MM/yyyy"),
                            contrato.NumeroProceso,
                            contrato.NumeroContrato,
                            TipoContrato = tipoContrato.NombreTipoContrato
                         };
            gvContrato.DataSource = result.ToList();
            gvContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvContrato.PageSize = PageSize();
            gvContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Contrato Adhesion", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContrato.SelectedRow);
    }
    protected void gvContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Contrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Contrato.Eliminado");
            ContratoService objContratoService = new ContratoService();
            /*Tipo Contrato*/
            ddlIdTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdTipoContrato.SelectedValue = "-1";
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
