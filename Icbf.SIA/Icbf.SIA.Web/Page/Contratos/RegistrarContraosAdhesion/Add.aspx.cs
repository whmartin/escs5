using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de contratos de adhesión
/// </summary>
public partial class Page_Contrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/RegistrarContraosAdhesion";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Contrato vContrato = new Contrato();
            vContrato.FechaRegistro = fechaRegistro.Date;
            vContrato.IdTipoContrato = Convert.ToInt32(dllIdTipoContrato.SelectedValue);
            vContrato.RequiereActa = Convert.ToBoolean(rblRequiereActaInicio.SelectedValue);
            vContrato.ManejaAportes = Convert.ToBoolean(rblManejaAportes.SelectedValue);
            vContrato.IdRegionalContrato = Convert.ToInt32(ddlRegionalContrato.SelectedValue);
            vContrato.NombreSolicitante = Convert.ToString(txtNombreSolicitante.Text);
            vContrato.DependenciaSolicitante = Convert.ToString(txtDependenciaSolicitante.Text);
            vContrato.ObjetoContrato = txtObjetoContrato.Text;
            vContrato.AlcanceObjetoContrato = txtAlcanceObjetoContrato.Text;
            vContrato.ValorInicialContrato = Convert.ToDecimal(txtValorInicialContrato.Text);
            vContrato.ValorTotalAdiciones = Convert.ToDecimal(txtValorTotalAdiciones.Text);
            vContrato.ValorFinalContrato = Convert.ToDecimal(txtValorFinalContrato.Text);
            vContrato.ValorAportesICBF = Convert.ToDecimal(txtValorAportesICBF.Text);
            vContrato.ValorAportesOperador = Convert.ToDecimal(txtValorAportesOperador.Text);
            vContrato.FechaSuscripcion = fechaSuscripcion.Date;
            vContrato.FechaInicioEjecucion = fechaInicioEjecucion.Date;
            vContrato.FechaFinalTerminacionContrato = fechaFinalTerminacion.Date;
            vContrato.FechaFinalizacionIniciaContrato = fechaFinalizacionInicial.Date;
            vContrato.FechaFinalTerminacionContrato = fechaFinalTerminacion.Date;
            vContrato.FechaProyectadaLiquidacion = fechaProyectadaLiquidacion.Date;
            vContrato.FechaLiquidacion = fechaLiquidacion.Date;
            vContrato.Prorrogas = Convert.ToInt32(txtProrrogas.Text);
            vContrato.PlazoTotal = Convert.ToInt32(txtPlazoTotal.Text);
            vContrato.FechaFirmaActaInicio = fechaFirmaActaInicio.Date;
            vContrato.DatosAdicionales = Convert.ToString(txtDatosAdicionales.Text);
            vContrato.IdEstadoContrato = Convert.ToInt32(1);
            vContrato.IdFormaPago = Convert.ToInt32(ddlFormaPago.SelectedValue);
            vContrato.RequiereGarantias = Convert.ToBoolean(rblRequiereGarantias.SelectedValue);
            vContrato.ClaseContrato = "A";
            if (Request.QueryString["oP"] == "E")
            {
            vContrato.IdContrato = Convert.ToInt32(hfIdContrato.Value);
                vContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarContrato(vContrato);
            }
            else
            {
                vContrato.NumeroContrato = vContratoService.GenerarNumeroContrato(new SecuenciaNumeroContrato() { CodigoRegional = Convert.ToInt32(ddlRegionalContrato.SelectedValue), AnoVigencia = DateTime.Now.Year, UsuarioCrea = GetSessionUser().NombreUsuario });
                vContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarContrato(vContrato);
                RelacionContrato vRelacionContrato = new RelacionContrato() { IdContratoMaestro = Convert.ToInt32(this.hdIDContratoPrincipal.Value), IdContratoAsociado = vContrato.IdContrato, FechaAsociacion = DateTime.Now, UsuarioCrea = vContrato.UsuarioCrea};
                vContratoService.InsertarRelacionContrato(vRelacionContrato);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Contrato.IdContrato", vContrato.IdContrato);
                SetSessionParameter("Contrato.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
           
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);

            toolBar.EstablecerTitulos("Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.Id");

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato(vIdContrato);
            hfIdContrato.Value = vContrato.IdContrato.ToString();
            fechaRegistro.Date = vContrato.FechaRegistro;
            txtNumeroContratoPrincipal.Text = vContrato.NumeroContrato.ToString();
            dllIdTipoContrato.SelectedValue = vContrato.IdTipoContrato.ToString();
            rblRequiereActaInicio.SelectedValue = vContrato.RequiereActa.ToString();
            rblManejaAportes.SelectedValue = vContrato.ManejaAportes.ToString();
            
            txtNombreSolicitante.Text = vContrato.NombreSolicitante;
            txtDependenciaSolicitante.Text = vContrato.DependenciaSolicitante;
            
            txtAlcanceObjetoContrato.Text = vContrato.AlcanceObjetoContrato;
            txtValorInicialContrato.Text = vContrato.ValorInicialContrato.ToString();
            txtValorTotalAdiciones.Text = vContrato.ValorTotalAdiciones.ToString();
            txtValorFinalContrato.Text = vContrato.ValorFinalContrato.ToString();
            txtValorAportesICBF.Text = vContrato.ValorAportesICBF.ToString();
            txtValorAportesOperador.Text = vContrato.ValorAportesOperador.ToString();
            rblRequiereGarantias.SelectedValue = vContrato.RequiereGarantias.ToString();
        
            fechaSuscripcion.Date = vContrato.FechaSuscripcion;

            if (vContrato.FechaInicioEjecucion != null)
                fechaInicioEjecucion.Date = (DateTime)vContrato.FechaInicioEjecucion;
            else
                fechaInicioEjecucion.InitNull = true;
            if (vContrato.FechaFinalizacionIniciaContrato != null)
                fechaFinalizacionInicial.Date = (DateTime)vContrato.FechaFinalizacionIniciaContrato;
            else
                fechaFinalizacionInicial.InitNull = true;
            if (vContrato.FechaLiquidacion != null)
                fechaLiquidacion.Date = vContrato.FechaLiquidacion.Value;
            if(vContrato.FechaFinalTerminacionContrato != null)
                fechaFinalTerminacion.Date = vContrato.FechaFinalTerminacionContrato.Value;
            fechaProyectadaLiquidacion.Date = vContrato.FechaProyectadaLiquidacion;
            txtProrrogas.Text = vContrato.Prorrogas.ToString();
            txtPlazoTotal.Text = vContrato.PlazoTotal.ToString();
            fechaFirmaActaInicio.Date = vContrato.FechaFirmaActaInicio;
            txtDatosAdicionales.Text = vContrato.DatosAdicionales;
            ddlEstadoContrato.SelectedValue = vContrato.IdEstadoContrato.ToString();
            ddlRegionalContrato.SelectedValue = vContrato.IdRegionalContrato.ToString();
            ddlFormaPago.SelectedValue = vContrato.IdFormaPago.ToString();
            
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            fechaRegistro.Date = DateTime.Now;
            fechaRegistro.HabilitarObligatoriedad(true);
            fechaSuscripcion.HabilitarObligatoriedad(true);
            fechaInicioEjecucion.HabilitarObligatoriedad(true);
            fechaFinalizacionInicial.HabilitarObligatoriedad(true);
            fechaFinalTerminacion.HabilitarObligatoriedad(true);
            fechaFirmaActaInicio.HabilitarObligatoriedad(true);
            fechaProyectadaLiquidacion.HabilitarObligatoriedad(true);
            fechaLiquidacion.HabilitarObligatoriedad(true);
            ManejoControlesContratos.LlenarComboLista(dllIdTipoContrato, vContratoService.ConsultarTipoContratos(null, null, true, null, null, null, null, null), "IdTipoContrato", "NombreTipoContrato");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblManejaAportes, "Si", "No");
            ManejoControlesContratos.LlenarComboLista(ddlRegionalContrato, vContratoService.ConsultarRegionals(null, null), "IdRegional", "NombreRegional");
            ManejoControlesContratos.InicializarCombo(ddlUnidadEjecucion);
            //ManejoControlesContratos.LlenarComboLista(ddlFormaPago, vContratoService.ConsultarFormaPagos(null,null, true), "IdFormapago", "NombreFormaPago");
            ManejoControlesContratos.InicializarCombo(ddlEstadoContrato);
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereActaInicio, "Si", "No");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereGarantias, "Si", "No");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
