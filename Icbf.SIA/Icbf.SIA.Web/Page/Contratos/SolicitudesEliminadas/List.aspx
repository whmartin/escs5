﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_SolicitudesEliminadas_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">    
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConsModContractual" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" OnPageIndexChanging="gvConsModContractual_PageIndexChanging"
                         DataKeyNames="IdConsModContractual,NumeroContrato" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:TemplateField>
                                <%--<ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>--%>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Regional del Contrato" DataField="NombreRegional" />
                            <asp:BoundField HeaderText="Dependencia Solicitante" DataField="NombreDependenciaSolicitante" />
                            <asp:BoundField HeaderText="No Contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="No Solicitud" DataField="IdConsModContractual" />
                            <asp:BoundField HeaderText="Tipo de Modificaci&oacute;n" DataField="TipoModificacion" />
                            <asp:BoundField HeaderText="Fecha Eliminaci&oacute;n" DataField="FechaEliminacion" SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField HeaderText="Usuario Responsable" DataField="UsuarioCrea" />
                            <asp:BoundField HeaderText="Justificaci&oacute;n de la Eliminaci&oacute;n" DataField="JustificacionEliminacion" />     
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>