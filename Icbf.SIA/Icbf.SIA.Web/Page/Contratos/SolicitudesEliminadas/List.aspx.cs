﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_SolicitudesEliminadas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesEliminadas";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvConsModContractual.PageSize = PageSize();
            gvConsModContractual.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Historico de Solicitudes de Modificaciones Eliminadas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControles Controles = new ManejoControles();

            List<HistoricoSolicitudesEliminadas> Historico = new List<HistoricoSolicitudesEliminadas>();
            Historico = vContratoService.ConsultarSolicitudesElimindasHistorico();

            gvConsModContractual.DataSource = Historico;
            gvConsModContractual.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvConsModContractual_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConsModContractual.PageIndex = e.NewPageIndex;
        CargarDatosIniciales();
    }
}


