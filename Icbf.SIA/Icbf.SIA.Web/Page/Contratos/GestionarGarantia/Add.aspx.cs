using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;
using System.Web.Security;
using Icbf.Seguridad.Service;


/// <summary>
/// Página de registro y edición para la entidad Garantia
/// </summary>
public partial class Page_GestionarGarantia_Add : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    SeguridadService vSeguridadFAC = new SeguridadService();

    string PageName = "Contratos/GestionarGarantia";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
        //SetSessionParameter("Garantia.IDGarantia", "2");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
            if (!Page.IsPostBack)
            {
                #region La gestión de la garantía solo la puede realizar el usuario que registro la garantía
                if (ValidarUsuario() == false)
                {
                    toolBar.MostrarMensajeError("La gestión de la garantía solo la puede realizar el usuario que registro la garantía");
                    return;
                }
                #endregion

                CargarDatosIniciales();
            }
        //}
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        CerrarLupa();
    }

    protected void rblAccion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.QuitarMensajeError();
        OcultarPaneles();
        switch (rblAccion.SelectedItem.Text)
        {
            case "Aprobar":
                pnlAprobarGarantia.Visible = true;
                break;
            case "Devolver":
                pnlDevolverGarantia.Visible = true;
                break;
            default:
                break;
        }
    }

    #endregion

    #region Métodos

    private void CerrarLupa()
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    /// <summary>
    /// Método de guardado para la entidad Garantia
    /// </summary>
    private void Guardar()
    {
        try
        {

            #region Crecion objeto garantia
            int idGarantia = Convert.ToInt32(GetSessionParameter("Garantia.IDGarantia"));
            Garantia vGarantia = vContratoService.ConsultarGarantia(idGarantia);

            #endregion


            #region Validaciones de negocio
            string strValidacion = ValidarReglasDeNegocio(vGarantia);
            if (strValidacion != string.Empty)
            {
                toolBar.MostrarMensajeError(strValidacion);
                return;
            }
            #endregion


            #region Grabar garantia
            int vResultado = 0;
            vGarantia.UsuarioModifica = GetSessionUser().NombreUsuario;
            vGarantia.FechaModifica = DateTime.Now;
            InformacionAudioria(vGarantia, this.PageName, vSolutionPage);

            if (rblAccion.SelectedItem.Text == "Aprobar")
            {
                vGarantia.FechaAprobacionGarantia = txtFechaAprobacionGarantia.Date;
                vGarantia.FechaCertificacionGarantia = txtFechaCertificacionPagoGarantia.Date;
                vGarantia.IDUsuarioAprobacion = GetSessionUser().IdUsuario;
            }
            else if (rblAccion.SelectedItem.Text == "Devolver")
            {
                vGarantia.FechaDevolucion = txtFechaDevolucionGarantia.Date;
                vGarantia.MotivoDevolucion = txtMotivoDevolucionGarantia.Text;
                vGarantia.IDUsuarioDevolucion = GetSessionUser().IdUsuario;
            }

            vResultado = vContratoService.ModificarGarantia(vGarantia);
            #endregion


            #region Mostrar al usuario el resultado de la transaccion
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                CerrarLupa();
            }
            #endregion

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Gestión Garantía", SolutionPage.Add.ToString());
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblAccion.Items.Insert(0, new ListItem("Aprobar", "true"));
            rblAccion.Items.Insert(1, new ListItem("Devolver", "false"));
            txtResponsableAprobacionGarantia.Text = ConcatenarNombresUsuario();
            txtResponsableDevolucionGarantia.Text = ConcatenarNombresUsuario();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Concatena primer nombre, segundo nombre, primer apellido, segundo apellido del usuario autenticado
    /// </summary>
    /// <returns></returns>
    private string ConcatenarNombresUsuario()
    {
        return string.Concat(GetSessionUser().PrimerNombre, " ", GetSessionUser().SegundoNombre, " ",
                             GetSessionUser().PrimerApellido, " ", GetSessionUser().SegundoApellido);
    }

    /// <summary>
    /// Oculta los paneles de aprobar y devolver
    /// </summary>
    private void OcultarPaneles()
    {
        pnlAprobarGarantia.Visible = false;
        pnlDevolverGarantia.Visible = false;
    }

    /// <summary>
    /// Validación de reglas de negocio
    /// </summary>
    /// <param name="vGarantia">Objeto garantía</param>
    /// <returns></returns>
    private string ValidarReglasDeNegocio(Garantia vGarantia)
    {
        if (rblAccion.SelectedItem.Text == "Aprobar")
        {
            if ((txtFechaAprobacionGarantia.Date < vGarantia.FechaReciboGarantia))
            {
                return "La Fecha Aprobación Garantía debe ser mayor o igual a la Fecha de Recibo Garantía";
            }

            if ((txtFechaCertificacionPagoGarantia.Date < vGarantia.FechaReciboGarantia))
            {
                return "La Fecha Certificación del Pago Garantía debe ser mayor o igual a la Fecha de Recibo Garantía";
            }
        }
        else if (rblAccion.SelectedItem.Text == "Devolver")
        {
            if ((txtFechaDevolucionGarantia.Date < vGarantia.FechaReciboGarantia))
            {
                return "La Fecha Devolución Garantía debe ser mayor o igual a la Fecha de Recibo Garantía";
            }
        }

        return string.Empty;
    }

    /// <summary>
    /// Valida si el usuario autenticado puede gestionar la garantia
    /// </summary>
    /// <returns>false cuando no debe gestionar la garantia</returns>
    private bool ValidarUsuario()
    {

        //Administrador puede gestionar
        string[] lRoles = Roles.GetRolesForUser(GetSessionUser().NombreUsuario);
        if (vSeguridadFAC.ConsultarRolesNombreEsAdmin(lRoles[0], true))
        {
            return true;
        }

        //Usuario que creo la garantia puede gestionar
        int idGarantia = Convert.ToInt32(GetSessionParameter("Garantia.IDGarantia"));
        Garantia vGarantia = vContratoService.ConsultarGarantia(idGarantia);
        if (vGarantia.UsuarioCrea.ToUpper() == GetSessionUser().NombreUsuario.ToUpper())
        {
            return true;
        }

        return false;
    }
    #endregion

}
