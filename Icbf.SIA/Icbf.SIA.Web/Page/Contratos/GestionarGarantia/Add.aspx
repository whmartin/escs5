<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_GestionarGarantia_Add" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Acci&oacute;n sobre la Garant&iacute;a *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblAccion" OnSelectedIndexChanged="rblAccion_OnSelectedIndexChanged"
                    AutoPostBack="True">
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" ID="rvtxtTipoIdentificacion" ControlToValidate="rblAccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlAprobarGarantia" runat="server" Visible="false">
        <fieldset>
            <legend>Aprobar Garantía</legend>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        Fecha Aprobaci&oacute;n Garant&iacute;a *
                    </td>
                    <td>
                        Fecha Certificaci&oacute;n del Pago Garant&iacute;a *
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <uc1:fecha ID="txtFechaAprobacionGarantia" runat="server" Width="80%" Enabled="True"
                            Requerid="True" />
                    </td>
                    <td>
                        <uc1:fecha ID="txtFechaCertificacionPagoGarantia" runat="server" Width="80%" Enabled="True"
                            Requerid="True" />
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        Responsable Aprobación Garantía
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtResponsableAprobacionGarantia" MaxLength="200"
                            Width="90%" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlDevolverGarantia" runat="server" Visible="false">
        <fieldset>
            <legend>Devolver Garant&iacute;a</legend>
            <table width="90%" align="center">
                <tr class="rowB">
                    <td>
                        Fecha Devoluci&oacute;n Garant&iacute;a *
                    </td>
                    <td>
                        Motivo Devoluci&oacute;n Garant&iacute;a *
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtMotivoDevolucionGarantia"
                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <uc1:fecha ID="txtFechaDevolucionGarantia" runat="server" Width="80%" Enabled="True"
                            Requerid="True" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMotivoDevolucionGarantia" Width="70%" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        Responsable Devoluci&oacute;n Garant&iacute;a
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox runat="server" ID="txtResponsableDevolucionGarantia" MaxLength="200"
                            Width="90%" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
</asp:Content>
