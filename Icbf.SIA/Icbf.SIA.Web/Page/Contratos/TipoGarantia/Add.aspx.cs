using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de tipos de garantía
/// </summary>
public partial class Page_TipoGarantia_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/TipoGarantia";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoGarantia vTipoGarantia = new TipoGarantia();

            vTipoGarantia.NombreTipoGarantia = Convert.ToString(txtNombreTipoGarantia.Text).ToUpper();
            vTipoGarantia.Estado = rblEstado.SelectedValue == "1"?true:false;

            if (Request.QueryString["oP"] == "E")
            {
            vTipoGarantia.IdTipoGarantia = Convert.ToInt32(hfIdTipoGarantia.Value);
                vTipoGarantia.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoGarantia, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarTipoGarantia(vTipoGarantia);
            }
            else
            {
                vTipoGarantia.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoGarantia, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarTipoGarantia(vTipoGarantia);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoGarantia.IdTipoGarantia", vTipoGarantia.IdTipoGarantia);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("TipoGarantia.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("TipoGarantia.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }

                
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Garant&#237;a", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
         
            //int vIdTipoGarantia = Convert.ToInt32(GetSessionParameter("TipoGarantia.IdTipoGarantia"));
            int vIdTipoGarantia = 0;

            if (int.TryParse(GetSessionParameter("TipoGarantia.IdTipoGarantia").ToString(), out vIdTipoGarantia))
            {
                RemoveSessionParameter("TipoGarantia.Id");

                TipoGarantia vTipoGarantia = new TipoGarantia();
                vTipoGarantia = vContratoService.ConsultarTipoGarantia(vIdTipoGarantia);
                hfIdTipoGarantia.Value = vTipoGarantia.IdTipoGarantia.ToString();
                txtNombreTipoGarantia.Text = vTipoGarantia.NombreTipoGarantia;
                rblEstado.SelectedValue = vTipoGarantia.Estado == true ? "1" : "0";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoGarantia.UsuarioCrea, vTipoGarantia.FechaCrea, vTipoGarantia.UsuarioModifica, vTipoGarantia.FechaModifica);    
            }
            else
            {
                NavigateTo(SolutionPage.List); 
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
