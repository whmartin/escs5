﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetalleProveedorMigracion.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master"
Inherits="Page_Contratos_LupasMigracion_DetalleProveedorMigracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

<asp:Panel runat="server" ID="Panel1">
    <asp:Panel runat="server" ID="pnlConsulta">
              <asp:HiddenField ID="hfIdTreceroProveedor" runat="server"/>     
            <table width="90%" align="center">
                 <tr class="rowB">
                    <td>
                       Tipo de Persona
                    </td>
                    <td>
                        Tipo de Identificación
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox ID="txtTipoPersona" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                        <asp:DropDownList ID="ddlTipoPersona" runat="server" Visible="false"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTipoIdentificacion" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                        <asp:DropDownList ID="ddlTipoIdentificacion" runat="server" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        Número de Identificación 
                    </td>
                    <td>
                        <asp:Label ID="lblNumIdentificacion" runat="server" Text="Razón Social"></asp:Label>
                        
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox ID="txtNúmeroIdentificacion" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                        
                    </td>
                    <td >
                        <asp:TextBox ID="txtRazonSocial" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                       
                    </td>
                </tr>
               <tr class="rowB">
                    <td>
                        <asp:Label ID="lblPrimerNombre" runat="server" Text="Nombres / Apellidos"></asp:Label> 
                    </td>                   
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox ID="txtPrimerNombre" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                        
                        
                    </td>                    
                </tr>                
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="PnlReprLegal" runat="server">
                                    <h3 class="lbBloque">
                                        <asp:Label ID="Label5" runat="server" Text="Representante Legal"></asp:Label>
                                    </h3>
                                    <table style="width: 100%">
                                        <tr class="rowB">
                                            <td style="width: 50%">
                                                Tipo de Identificaci&oacute;n
                                                
                                            </td>
                                            <td style="width: 50%">
                                                N&uacute;mero de Identificaci&oacute;n
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td style="width: 50%">
                                                <asp:TextBox ID="txtTipoIdentificacionRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="50"></asp:TextBox>
                                                <asp:DropDownList ID="ddlTipoIdentificacionRepr" runat="server" Visible="false"></asp:DropDownList>
                                            </td>
                                            <td style="width: 50%">
                                                <asp:TextBox ID="txtNumIdentificacionRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="51"></asp:TextBox>
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                Nombres / Apellidos</td>                                            
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerNombreRepr" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="52" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                         
                                        </tr>                               
                                        
                                    </table>
                                </asp:Panel>
                    </td>
                </tr>
            </table>

            </asp:Panel>
</asp:Panel>

</asp:Content>

