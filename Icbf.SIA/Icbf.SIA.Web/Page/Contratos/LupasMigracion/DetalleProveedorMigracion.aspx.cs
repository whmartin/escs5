﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
//using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;

public partial class Page_Contratos_LupasMigracion_DetalleProveedorMigracion : GeneralWeb
{
    General_General_Master_Lupa toolBar;

    string PageName = "Contratos/Contratos";
    ContratoService vContratoService = new ContratoService();

    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();

    ProveedorService vProveedorService = new ProveedorService();

    OferenteService vOferenteService = new OferenteService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //SolutionPage vSolutionPage = SolutionPage.List;
        //if (ValidateAccess(toolBar, PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {

            CargarRegistro();
        }
        //}
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        // Condición para redireccionar a la pagina Lupa info Contratista o a Lupa proveedores
        bool LLamadoPaginaInfoContratista = false;
        if (GetSessionParameter("Contratista.Retornar") != null)
        {
            if (bool.TryParse(GetSessionParameter("Contratista.Retornar").ToString(), out LLamadoPaginaInfoContratista))
            {
                if (LLamadoPaginaInfoContratista)
                {
                    string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                    string returnValues =
                            "<script language='javascript'> " +
                            "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

                    this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
                    return;

                }
            }
        }
        if (
               !string.IsNullOrEmpty(Request.QueryString["idContrato"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idContratista"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idCesion"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idDetConsmodcontractual"])
              )
        {
            string idContrato = Request.QueryString["idContrato"];
            string idContratista = Request.QueryString["idContratista"];
            string idCesion = Request.QueryString["idCesion"];
            string iddetConsmodContractual = Request.QueryString["idDetConsmodcontractual"];

            var url = string.Format(
                                    "LupaProveedores.aspx?idContrato={0}&idContratista={1}&idCesion={2}&idDetConsmodcontractual={3}",
                                    idContrato,
                                    idContratista,
                                    idCesion,
                                    iddetConsmodContractual
                                   );
            NavigateTo(url);
        }
        else
            NavigateTo("LupaProveedores.aspx");
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {

        Guardar();
    }

    private void Guardar()
    {
        try
        {
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";

        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
        return;
    }

    

   

    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            if (string.IsNullOrEmpty("Prooveedor.btnretornar"))
                toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            else
                RemoveSessionParameter("Prooveedor.btnretornar");

            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            // Condición para redireccionar a la pagina Lupa info Contratista o a Lupa proveedores
            bool LLamadoPaginaInfoContratista = false;
            if (GetSessionParameter("Contratista.Retornar") != null)
            {
                if (bool.TryParse(GetSessionParameter("Contratista.Retornar").ToString(), out LLamadoPaginaInfoContratista))
                {
                    if (LLamadoPaginaInfoContratista)
                    {
                        toolBar.OcultarBotonGuardar(true);

                    }

                }
            }
            RemoveSessionParameter("Contratista.Retornar");
            toolBar.EstablecerTitulos("Detalle del proveedor");
            CargarDatosTipoPersona();
        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
        catch (Exception ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    private void habilitarcamposnarutal(bool estado)
    {
        PnlReprLegal.Visible = !estado;
        txtRazonSocial.Visible = !estado;
        lblNumIdentificacion.Visible = !estado;
        txtPrimerNombre.Visible = estado;
        lblPrimerNombre.Visible = estado;
        //txtSegundoNombre.Visible = estado;
        //lblSegundoNombre.Visible = estado;
        //txtPrimerApellido.Visible = estado;
        //lblPrimerApellido.Visible = estado;
        //txtSegundoApellido.Visible = estado;
        //lblSegundoApellido.Visible = estado;
    }

    private void CargarRegistro()
    {
        try
        {
            if (GetSessionParameter("EntidadProvOferentes.IdEntidad") == null)
            {
                toolBar.MostrarMensajeError("Seleccione un proveedor");
                return;
            }
            //CargarDatos 
            var identida = GetSessionParameter("EntidadProvOferentes.IdEntidad");
            RemoveSessionParameter("EntidadProvOferentes.IdEntidad");
            if (identida == null)
                throw new Exception("El valor de la entidad es nula, verifique la selección del proveedor");
            int vIdEntidad = Convert.ToInt32(identida);

            ContratistaMigrados vContratista = new ContratistaMigrados();

            vContratista = vContratoService.ConsultarContratistaMigracion(vIdEntidad);

            if (vContratista != null)
            {//Proveedor

                if (!string.IsNullOrEmpty(vContratista.TipoPersona))
                {
                    txtTipoIdentificacion.Text = vContratista.TipoIdentificacion;
                    txtTipoPersona.Text = vContratista.TipoPersona;

                    switch (txtTipoPersona.Text)
                    {
                        case "UNIÓN TEMPORAL":
                            habilitarcamposnarutal(false);
                            break;
                        case "JURIDICA":
                            habilitarcamposnarutal(false);
                            break;
                        case "CONSORCIO":
                            habilitarcamposnarutal(false);
                            break;
                        case "NATURAL":
                            habilitarcamposnarutal(true);
                            break;
                    }
                }


                if (!string.IsNullOrEmpty(vContratista.NumeroIdentificacion))
                {
                    txtNúmeroIdentificacion.Text = vContratista.NumeroIdentificacion;
                }
                if (!string.IsNullOrEmpty(vContratista.NombreRazonSocial))
                {
                    txtRazonSocial.Text = vContratista.NombreRazonSocial;
                }
                if (!string.IsNullOrEmpty(vContratista.NombreRazonSocial))
                {
                    txtPrimerNombre.Text = vContratista.NombreRazonSocial;
                }
                //if (vEntidadProvOferente.TerceroProveedor.SegundoNombre != null)
                //{
                //    txtSegundoNombre.Text = vEntidadProvOferente.TerceroProveedor.SegundoNombre;
                //}
                //if (vEntidadProvOferente.TerceroProveedor.PrimerApellido != null)
                //{
                //    txtPrimerApellido.Text = vEntidadProvOferente.TerceroProveedor.PrimerApellido;
                //}
                //if (vEntidadProvOferente.TerceroProveedor.SegundoApellido != null)
                //{
                //    txtSegundoApellido.Text = vEntidadProvOferente.TerceroProveedor.SegundoApellido;
                //}
            }
            else {
                throw new Exception("El proveedor presenta inconsistencia, por favor comuníquese con el administrador."); }
                //Representante legal
                
                if (!string.IsNullOrEmpty(vContratista.NombreRepresentanteLegal))
                {
                     if (!string.IsNullOrEmpty(vContratista.IdentificacionRepresentanteLegal))
                        {
                            
                            if (!string.IsNullOrEmpty(vContratista.IdentificacionRepresentanteLegal))
                            {
                                txtNumIdentificacionRepr.Text = vContratista.IdentificacionRepresentanteLegal;
                            }
                            if (!string.IsNullOrEmpty(vContratista.NombreRepresentanteLegal))
                            {
                                TxtPrimerNombreRepr.Text = vContratista.NombreRepresentanteLegal;
                            }
                            //if (vEntidadProvOferente.RepresentanteLegal.SegundoNombre != null)
                            //{
                            //    TxtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                            //}
                            //if (vEntidadProvOferente.RepresentanteLegal.PrimerApellido != null)
                            //{
                            //    TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                            //}
                            //if (vEntidadProvOferente.RepresentanteLegal.SegundoApellido != null)
                            //{
                            //    TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                            //}
                         }                       
                    }      
            
        }
        catch (UserInterfaceException ex)
        {
            if (ex.Message.IndexOf("Object reference not set to an instance of an object") >= 0)
            {
                toolBar.MostrarMensajeError("El proveedor presenta inconsistencia, por favor comuníquese con el administrador.");
            }
            else
            { toolBar.MostrarMensajeError(ex.Message); }
        }
        catch (Exception ex)
        {
            if (ex.Message.IndexOf("Object reference not set to an instance of an object") >= 0)
            {
                toolBar.MostrarMensajeError("El proveedor presenta inconsistencia, por favor comuníquese con el administrador.");
            }
            else
            { toolBar.MostrarMensajeError(ex.Message); }
        }
    }

    private void CargarDatosTipoPersona()
    {
        ManejoControles vManejoControles = new ManejoControles();
        vManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
    }

    private void CargarDatosTipoIdentificacion()
    {
        ManejoControles vManejoControles = new ManejoControles();
        if (ddlTipoPersona.SelectedValue == "1") //NATURAL
        {
            vManejoControles.LlenarTipoDocumentoRL(ddlTipoIdentificacion, "-1", true);
        }
        else
        {
            vManejoControles.LlenarTipoDocumentoN(ddlTipoIdentificacion, "-1", true);
        }
    }

    private void CargarDatosTipoIdentificacionRepr()
    {
        ManejoControles vManejoControles = new ManejoControles();

        vManejoControles.LlenarTipoDocumentoRL(ddlTipoIdentificacionRepr, "-1", true);

    }

    private KeyValuePair<int,string> ExisteContratoEnProveedor(int pIdContrato, int pIdProveedor)
    {
        Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
        vProveedores_Contratos.IdContrato = pIdContrato;
        vProveedores_Contratos.IdProveedores = pIdProveedor;
        return vContratoService.ExisteProveedores_Contratos(vProveedores_Contratos);
    }
}