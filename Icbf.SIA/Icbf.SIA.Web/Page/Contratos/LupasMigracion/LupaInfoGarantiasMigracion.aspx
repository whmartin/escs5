﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master"
    CodeFile="LupaInfoGarantiasMigracion.aspx.cs" Inherits="Page_Contratos_LupasMigracion_LupaInfoGarantiasMigracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlLista" CssClass="width: 100%">
        <table width="100%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvGarantia" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContratoEnContratos,FechaAprobacionGarantia"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvGarantia_PageIndexChanging"
                        OnSelectedIndexChanged="gvGarantia_SelectedIndexChanged" OnRowCommand="gvGarantia_RowCommand">
                        <Columns>
                            
                           
                            <asp:BoundField HeaderText="Tipo Garantía" DataField="TipoGarantia" SortExpression="TipoGarantia" />
                            <asp:BoundField HeaderText="Nombre Aseguradora" DataField="NombreEntidadAseguradora" SortExpression="NombreEntidadAseguradora" />
                            <asp:BoundField HeaderText="Nit Aseguradora" DataField="NitEntidadAseguradora" SortExpression="NitEntidadAseguradora" />
                            <asp:BoundField HeaderText="Fecha de Aprobación" DataField="FechaAprobacionGarantia"
                                SortExpression="FechaAprobacionGarantia" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField HeaderText="Valor Asegurado" DataField="ValorTotalAsegurado" SortExpression="ValorTotalAsegurado"
                                DataFormatString="{0:C}"  />
                            <asp:BoundField HeaderText="Porecentaje Asegurado" DataField="PorcentajeAsegurado" SortExpression="PorcentajeAsegurado"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>

            <tr>
                <td>
                    Valor Garantías
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtValorTotalGarantias" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            
        </table>
        <asp:HiddenField ID="hfPostbk" runat="server" />
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function GestionarGarantias() {
            window_showModalDialogLupa('../../../Page/Contratos/GestionarGarantia/Add.aspx', setReturnGestionarGarantias, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGestionarGarantias() {
            __doPostBack('<%= hfPostbk.ClientID %>', ''); // EC-08-08-2014 Se realiza postbk cada vez que se regresa de la lupa para validar cambios en las garantias
        }

        function GetDetalleGarantias() {
            window_showModalDialog('../../../Page/Contratos/lupas/LupaGarantias.aspx', "", 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
    </script>
</asp:Content>
