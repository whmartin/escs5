﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master"
    CodeFile="LupaSupervisorInterventorMigracion.aspx.cs" Inherits="Page_Contratos_LupasMigracion_LupaSupervisorInterventorMigracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<%--    <asp:Panel runat="server" ID="pnlLista">--%>
<%--        <div id="dvSupervInterv" runat="server">--%>
    <br />

            <table width="100%" align="center">
                <tr class="rowB">
                    <td colspan="2">
                        Supervisores y/o Interventores
                    </td>
                </tr>
                <tr class="rowAG">
                    <td colspan="2">
                        <asp:GridView ID="gvSupervInterv" runat="server" AutoGenerateColumns="false" DataKeyNames="IDSupervisorIntervContrato,Identificacion"
                            GridLines="None" Width="80%" CellPadding="8" Height="16px" OnPageIndexChanging="gvSupervInterv_PageIndexChanging"
                            OnSelectedIndexChanged="gvSupervInterv_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Tipo Supervisor y/o Interventor" DataField="EtQSupervisorInterventor" />
                                <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                <asp:BoundField HeaderText="Número Identificación" DataField="Identificacion" />
                                <asp:BoundField HeaderText="Supervisor y/o Interventor" DataField="NombreCompletoSuperInterventor" />
                                <%--<asp:BoundField HeaderText="Fecha de Inicio" DataField="FechaInicioString" />--%>
                                <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        <br />
                           Hist&oacute;rico de Supervisores
                    </td>
                </tr>
                                <tr class="rowAG">
                    <td colspan="2">
                        <asp:GridView ID="gvSupervHistorico" runat="server" AutoGenerateColumns="false" DataKeyNames="IDSupervisorIntervContrato,Identificacion"
                            GridLines="None" Width="80%" CellPadding="8" Height="16px" OnPageIndexChanging="gvSupervInterv_PageIndexChanging"
                            OnSelectedIndexChanged="gvSupervInterv_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                <asp:BoundField HeaderText="Número Identificación" DataField="Identificacion" />
                                <asp:BoundField HeaderText="Supervisor" DataField="SupervisorInterventor.NombreCompleto" />
                                <asp:BoundField HeaderText="Fecha de Inicio" DataField="FechaInicioString" />
                                <asp:BoundField HeaderText="Fecha de Finalización" DataField="FechaFinString" />
                                <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
<%--        </div> 
    </asp:Panel>--%>
    <script type="text/javascript" language="javascript">
        function GetDetalleSupervisorInterventor() {
            window_showModalDialog('../../../Page/Contratos/lupas/LupaDetalleSupervisorInterventor.aspx', "", 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
    </script>
</asp:Content>
