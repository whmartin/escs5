using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de visualización detallada para la entidad ConsecutivoContratoRegionales
/// </summary>
public partial class Page_ConsecutivoContratoRegionales_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ConsecutivoContratoRegionales";
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();

    public bool vEsAnioActual
    {
        set { ViewState["EsAnoActual"] = value; }
        get
        {
            if (ViewState["EsAnoActual"] != null)
                return (bool)ViewState["EsAnoActual"];
            else
                return false;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ConsecutivoContratoRegionales.IdContrato", hfIdContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    
    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        if (GetSessionParameter("ConsecutivoContratoRegionales.Guardo").ToString() =="1")
        {
            toolBar.MostrarMensajeGuardado("Se actualizo el número de contrato correctamente");
            RemoveSessionParameter("ConsecutivoContratoRegionales.Guardo");
        }

        int vIdCntrato = Convert.ToInt32(GetSessionParameter("ConsecutivoContratoRegionales.IdContrato"));
        RemoveSessionParameter("ConsecutivoContratoRegionales.IdContrato");
        hfIdContrato.Value = vIdCntrato.ToString();

        var itemContrato = vContratoService.ConsultarContratoss(null, null, vIdCntrato, null, null, null, null,
                                                                 null, null, null, null, null, null, null, null, null,null);
        txtContrato.Text = itemContrato[0].NumeroContrato;
        txtRegional.Text = itemContrato[0].NombreRegional;

        txtobjeto.Text = itemContrato[0].ObjetoContrato;
        txtalcance.Text = itemContrato[0].AlcanceObjetoContrato;
        txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorInicialContrato);
        txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorFinalContrato);

        DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato[0].FechaInicioEjecucion);
        DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato[0].FechaFinalTerminacionContrato);

        txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
        txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);            
            toolBar.EstablecerTitulos("Información Consecutivo De Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //CargarIdRegional();
            //CargarIdVigencia();
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    //private void CargarDatos()
    //{
    //    try
    //    {
    //        int vIDConsecutivoContratoRegional = Convert.ToInt32(GetSessionParameter("ConsecutivoContratoRegionales.IDConsecutivoContratoRegional"));
    //        RemoveSessionParameter("ConsecutivoContratoRegionales.IDConsecutivoContratoRegional");

    //        if (GetSessionParameter("ConsecutivoContratoRegionales.Guardado").ToString() == "1")
    //            toolBar.MostrarMensajeGuardado();
    //        RemoveSessionParameter("ConsecutivoContratoRegionales.Guardado");


    //        ConsecutivoContratoRegionales vConsecutivoContratoRegionales = new ConsecutivoContratoRegionales();
    //        vConsecutivoContratoRegionales = vContratoService.ConsultarConsecutivoContratoRegionales(vIDConsecutivoContratoRegional);
    //        hfIDConsecutivoContratoRegional.Value = vConsecutivoContratoRegionales.IDConsecutivoContratoRegional.ToString();
    //        ddlIdRegional.SelectedValue = vConsecutivoContratoRegionales.IdRegional.ToString();
    //        ddlIdVigencia.SelectedValue = vConsecutivoContratoRegionales.IdVigencia.ToString();
    //        txtConsecutivo.Text = vConsecutivoContratoRegionales.Consecutivo.ToString();
    //        VisibleEditar();
    //        ObtenerAuditoria(PageName, hfIDConsecutivoContratoRegional.Value);
    //        ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vConsecutivoContratoRegionales.UsuarioCrea, vConsecutivoContratoRegionales.FechaCrea, vConsecutivoContratoRegionales.UsuarioModifica, vConsecutivoContratoRegionales.FechaModifica);
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    //protected void btnEliminar_Click(object sender, EventArgs e)
    //{
    //    EliminarRegistro();
    //}
    //private void EliminarRegistro()
    //{
    //    try
    //    {
    //        int vIDConsecutivoContratoRegional = Convert.ToInt32(hfIDConsecutivoContratoRegional.Value);

    //        ConsecutivoContratoRegionales vConsecutivoContratoRegionales = new ConsecutivoContratoRegionales();
    //        vConsecutivoContratoRegionales = vContratoService.ConsultarConsecutivoContratoRegionales(vIDConsecutivoContratoRegional);
    //        InformacionAudioria(vConsecutivoContratoRegionales, this.PageName, vSolutionPage);
    //        int vResultado = vContratoService.EliminarConsecutivoContratoRegionales(vConsecutivoContratoRegionales);
    //        if (vResultado == 0)
    //        {
    //            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
    //        }
    //        else if (vResultado == 1)
    //        {
    //            toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
    //            SetSessionParameter("ConsecutivoContratoRegionales.Eliminado", "1");
    //            NavigateTo(SolutionPage.List);
    //        }
    //        else
    //        {
    //            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
    //        }
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}
    ///// <summary>
    ///// Método para cargar los datos de IdRegional
    ///// </summary>
    //private void CargarIdRegional()
    //{
    //    var vResult = vSIAService.ConsultarVigencias(true);
    //    ManejoControlesContratos.LlenarComboLista(ddlIdVigencia, vResult, "IdVigencia", "AcnoVigencia");
    //}

    ///// <summary>
    ///// Método para cargar los datos de IdVigencia
    ///// </summary>
    //private void CargarIdVigencia()
    //{

    //    var vResult = vSIAService.ConsultarRegionals(null, null);
    //    ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vResult, "IdRegional", "NombreRegional");
    //}

    //private void VisibleEditar()
    //{
    //    toolBar.VerBtnEditar(false);
    //    toolBar = (masterPrincipal)this.Master;
    //    if (ddlIdVigencia.SelectedIndex > 0 && Convert.ToInt32(ddlIdVigencia.SelectedItem.Text) == DateTime.Now.Year)
    //    {
    //        toolBar.VerBtnEditar(true);
    //        //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
    //    }
    //}
}
