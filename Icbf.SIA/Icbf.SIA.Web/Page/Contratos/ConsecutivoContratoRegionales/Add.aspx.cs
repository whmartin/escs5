using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad ConsecutivoContratoRegionales
/// </summary>
public partial class Page_ConsecutivoContratoRegionales_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();
    string PageName = "Contratos/ConsecutivoContratoRegionales";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private  void Guardar()
    {
        try
        {
            toolBar.LipiarMensajeError();

            int consecutivoOut;

            
            if (int.TryParse(txtConsecutivoContrato.Text, out consecutivoOut))
            {
                if (consecutivoOut >= 1 && consecutivoOut <= 99999)
                {
                    var vNumeroContrato = consecutivoOut.ToString().PadLeft(5, '0');

                    var vConsecutivo = hfIdRegional.Value.Trim() + "" + vNumeroContrato + "" + hfIdVigencia.Value.ToString().Trim();

                    if (vConsecutivo != hfConsecutivoActual.Value)
                    {

                        var lNumeroContrato = vContratoService.ConsultarContratoss(
                                                                 null, null, null, vConsecutivo, null, null, null,
                                                                 null, null, null, null, null, null, null, null, null,null
                                                                );
                        if (lNumeroContrato.Count == 0)
                        {
                            int vidContrato = int.Parse(hfIdContrato.Value);
                            var result = vContratoService.ActualizarNumeroContrato(vidContrato, GetSessionUser().NombreUsuario, vConsecutivo, consecutivoOut.ToString());

                            if (result >= 1)
                            {
                                SetSessionParameter("ConsecutivoContratoRegionales.IdContrato", hfIdContrato.Value);
                                SetSessionParameter("ConsecutivoContratoRegionales.Guardo", "1");
                                NavigateTo(SolutionPage.Detail);
                            }
                            else
                                toolBar.MostrarMensajeError("Se produjo un Error en la actualización");
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("El consecutivo ya se encuentra asociado al id de contrato " + lNumeroContrato[0].IdContrato + ", verifique por favor.");
                            return;
                        }
                    }
                    else
                        toolBar.MostrarMensajeError("El consecutivo actual es igual al consecutivo propuesto.");
                }
                else
                toolBar.MostrarMensajeError("el número del contrato se encuenta por fuera de los rangos habilitados, verifique por favor.");
            }
            else
                toolBar.MostrarMensajeError("El Consecutivo no tiene un formato correcto.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.EstablecerTitulos("Información Consecutivo De Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdCntrato = Convert.ToInt32(GetSessionParameter("ConsecutivoContratoRegionales.IdContrato"));
            RemoveSessionParameter("ConsecutivoContratoRegionales.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();

            var itemContrato = vContratoService.ConsultarContratoss(null, null, vIdCntrato, null, null, null, null,
                                                                     null, null, null, null, null, null, null, null, null,null);

            txtContrato.Text = itemContrato[0].NumeroContrato;
            txtRegional.Text = itemContrato[0].NombreRegional;

            var vIdVigencia = Convert.ToInt32(itemContrato[0].IdVigenciaInicial);
            var vIdRegional = Convert.ToInt32(itemContrato[0].IdRegionalContrato);

            Vigencia vVigencia = vSIAService.ConsultarVigencia(vIdVigencia);
            Regional vRegional = vSIAService.ConsultarRegional(vIdRegional);

            hfIdRegional.Value = vRegional.CodigoRegional.Trim(); 
            hfIdVigencia.Value = vVigencia.AcnoVigencia.ToString().Trim();
            hfConsecutivoActual.Value = itemContrato[0].NumeroContrato;

            txtobjeto.Text = itemContrato[0].ObjetoContrato;
            txtalcance.Text = itemContrato[0].AlcanceObjetoContrato;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorFinalContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato[0].FechaInicioEjecucion);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato[0].FechaFinalTerminacionContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad ConsecutivoContratoRegionales
    /// </summary>
    //private void GuardarOld()
    //{
    //    try
    //    {
    //        //int vResultado;
    //        //ConsecutivoContratoRegionales vConsecutivoContratoRegionales = new ConsecutivoContratoRegionales();

    //        //vConsecutivoContratoRegionales.IdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
    //        //vConsecutivoContratoRegionales.IdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
    //        //vConsecutivoContratoRegionales.Consecutivo = Convert.ToDecimal(txtConsecutivo.Text);

    //        //if (vContratoService.ExisteConsecutivoContratoRegionales(
    //        //            vConsecutivoContratoRegionales.Consecutivo, vConsecutivoContratoRegionales.IdVigencia,
    //        //            vConsecutivoContratoRegionales.IdRegional))
    //        //    throw new Exception("Este consecutivo ya se encuentra asignado");

    //        //if (!vContratoService.EsMayorConsecutivoContratoRegionales(
    //        //        vConsecutivoContratoRegionales.Consecutivo, vConsecutivoContratoRegionales.IdVigencia,
    //        //        vConsecutivoContratoRegionales.IdRegional))
    //        //    throw new Exception("Consecutivo ya ha sido asignado");

    //        //if (Request.QueryString["oP"] == "E")
    //        //{
    //        //vConsecutivoContratoRegionales.IDConsecutivoContratoRegional = Convert.ToInt32(hfIDConsecutivoContratoRegional.Value);
    //        //    vConsecutivoContratoRegionales.UsuarioModifica = GetSessionUser().NombreUsuario;
    //        //    InformacionAudioria(vConsecutivoContratoRegionales, this.PageName, vSolutionPage);
    //        //    vResultado = vContratoService.ModificarConsecutivoContratoRegionales(vConsecutivoContratoRegionales);
    //        //}
    //        //else
    //        //{
    //        //    vConsecutivoContratoRegionales.UsuarioCrea = GetSessionUser().NombreUsuario;
    //        //    InformacionAudioria(vConsecutivoContratoRegionales, this.PageName, vSolutionPage);
    //        //    vResultado = vContratoService.InsertarConsecutivoContratoRegionales(vConsecutivoContratoRegionales);
    //        //}
    //        //if (vResultado == 0)
    //        //{
    //        //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
    //        //}
    //        //else if (vResultado == 1)
    //        //{
    //        //    SetSessionParameter("ConsecutivoContratoRegionales.IDConsecutivoContratoRegional", vConsecutivoContratoRegionales.IDConsecutivoContratoRegional);
    //        //    SetSessionParameter("ConsecutivoContratoRegionales.Guardado", "1");
    //        //    NavigateTo(SolutionPage.Detail);
    //        //}
    //        //else
    //        //{
    //        //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
    //        //}
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}
    ///// <summary>
    ///// Método para cargar los datos de IdRegional
    ///// </summary>
    //private void CargarIdRegional()
    //{
    //    var vResult = vSIAService.ConsultarVigencias(true);
    //    ManejoControlesContratos.LlenarComboLista(ddlIdVigencia, vResult, "IdVigencia", "AcnoVigencia");
    //}

    ///// <summary>
    ///// Método para cargar los datos de IdVigencia
    ///// </summary>
    //private void CargarIdVigencia()
    //{

    //    var vResult = vSIAService.ConsultarRegionals(null, null);
    //    ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vResult, "IdRegional", "NombreRegional");
    //}

    //private void HabilitarControlesCombo(bool pEsHabilitado)
    //{
    //    ddlIdRegional.Enabled = pEsHabilitado;
    //    ddlIdVigencia.Enabled = pEsHabilitado;
    //}
}
