using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de consulta a través de filtros para la entidad ConsecutivoContratoRegionales
/// </summary>
public partial class Page_ConsecutivoContratoRegionales_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ConsecutivoContratoRegionales";
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
        /// Método que realiza la búsqueda filtrada con múltiples criterios 
        /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrillaContrato(gvContratos);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="gvContratos"></param>
    private void CargarGrillaContrato(GridView gvContratos)
    {
        try
        {
            int? vIdRegional = null;
            int? vIdVigencia = null;
            int? vIdContrato = null;
            int idContratoOut = 0;
            string vNumeroContrato = null;

            if (!string.IsNullOrEmpty(txtNumeroContrato.Text))
                vNumeroContrato = txtNumeroContrato.Text;

            if (ddlIdRegional.SelectedValue != "-1")
            {
                vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            }
            if (ddlIdVigencia.SelectedValue != "-1")
            {
                vIdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            }

            if (int.TryParse(txtIdContrato.Text, out idContratoOut) || !string.IsNullOrEmpty(vNumeroContrato))
            {
                if (idContratoOut >= 0)
                    vIdContrato = idContratoOut;

                var myGridResults = vContratoService.ConsultarContratosSimple
                    (null,
                     null,
                     vIdContrato,
                     vNumeroContrato,
                     null,
                     vIdVigencia,
                     vIdRegional,
                     null,
                     null,
                     null,
                     null, null, null);

                gvContratos.DataSource = myGridResults;
                gvContratos.DataBind();
            }
            else
                toolBar.MostrarMensajeError("Debre ingresar el número y/o el Id del contrato");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvContratos.PageSize = PageSize();
            gvContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Información Consecutivo De Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Retorna a la principal de Contratos.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ConsecutivoContratoRegionales.Eliminado").ToString() == "1")
            toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ConsecutivoContratoRegionales.Eliminado");
            CargarIdRegional();
            CargarIdVigencia();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos de IdRegional
    /// </summary>
    private void CargarIdRegional()
    {
        var vResult = vSIAService.ConsultarVigencias(true);
        ManejoControlesContratos.LlenarComboLista(ddlIdVigencia, vResult, "IdVigencia", "AcnoVigencia");
    }

    /// <summary>
    /// Método para cargar los datos de IdVigencia
    /// </summary>
    private void CargarIdVigencia()
    {
       
        var vResult = vSIAService.ConsultarRegionals(null, null);
        ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vResult, "IdRegional", "NombreRegional");
    }

    /// <summary>
    /// Selecciona el Registro del Contrato.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvContratos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = gvContratos.SelectedRow.RowIndex;
            string strValue = gvContratos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ConsecutivoContratoRegionales.IdContrato", strValue);
            NavigateTo(SolutionPage.Edit);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    #region Cosnecutivo Contratos

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    //private void SeleccionarRegistro(GridViewRow pRow)
    //{
    //    try
    //    {
    //        int rowIndex = pRow.RowIndex;
    //        string strValue = gvConsecutivoContratoRegionales.DataKeys[rowIndex].Value.ToString();
    //        SetSessionParameter("ConsecutivoContratoRegionales.IDConsecutivoContratoRegional", strValue);
    //        NavigateTo(SolutionPage.Detail);
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}


    //protected void gvConsecutivoContratoRegionales_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    SeleccionarRegistro(gvConsecutivoContratoRegionales.SelectedRow);
    //}
    //protected void gvConsecutivoContratoRegionales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    gvConsecutivoContratoRegionales.PageIndex = e.NewPageIndex;
    //    CargarGrilla((GridView)sender, GridViewSortExpression, true);
    //}
    ///// <summary>
    ///// Guarda la dirección de ordenamiento del gridview
    ///// </summary>
    //public SortDirection GridViewSortDirection
    //{
    //    get
    //    {
    //        if (ViewState["sortDirection"] == null)
    //            ViewState["sortDirection"] = SortDirection.Ascending;

    //        return (SortDirection)ViewState["sortDirection"];
    //    }
    //    set { ViewState["sortDirection"] = value; }
    //}

    ///// <summary>
    ///// Guarda el criterio de ordenamiento de la grilla
    ///// </summary>
    //public string GridViewSortExpression
    //{
    //    get { return (string)ViewState["sortExpression"]; }
    //    set { ViewState["sortExpression"] = value; }
    //}

    //protected void gvConsecutivoContratoRegionales_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    CargarGrilla((GridView)sender, e.SortExpression, false);
    //}

    ///// <summary>
    ///// Cargar una grilla con ordenamiento
    ///// </summary>
    ///// <param name="gridViewsender">Grilla a ordenar</param>
    ///// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    ///// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    //private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    //{
    //    //////////////////////////////////////////////////////////////////////////////////
    //    //////Aqui va el código de llenado de datos para la grilla 
    //    //////////////////////////////////////////////////////////////////////////////////

    //    //Lleno una lista con los datos que uso para llenar la grilla
    //    try
    //    {
    //        int? vIdRegional = null;
    //        int? vIdVigencia = null;
    //        Decimal? vConsecutivo = null;
    //        if (ddlIdRegional.SelectedValue!= "-1")
    //        {
    //            vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
    //        }
    //        if (ddlIdVigencia.SelectedValue!= "-1")
    //        {
    //            vIdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
    //        }
    //        //if (txtConsecutivo.Text!= "")
    //        //{
    //        //    vConsecutivo = Convert.ToDecimal(txtConsecutivo.Text);
    //        //}
    //        var myGridResults = vContratoService.ConsultarConsecutivoContratoRegionaless( vIdRegional, vIdVigencia, null);
    //        //////////////////////////////////////////////////////////////////////////////////
    //        //////Fin del código de llenado de datos para la grilla 
    //        //////////////////////////////////////////////////////////////////////////////////

    //        if (expresionOrdenamiento != null)
    //        {
    //            //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
    //            if (string.IsNullOrEmpty(GridViewSortExpression))
    //            {
    //                GridViewSortDirection = SortDirection.Ascending;
    //            }
    //            else if (GridViewSortExpression != expresionOrdenamiento)
    //            {
    //                GridViewSortDirection = SortDirection.Descending;
    //            }
    //            if (myGridResults != null)
    //            {
    //                var param = Expression.Parameter(typeof(ConsecutivoContratoRegionales), expresionOrdenamiento);

    //                //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
    //                var prop = Expression.Property(param, expresionOrdenamiento);

    //                //Creo en tiempo de ejecución la expresión lambda
    //                var sortExpression = Expression.Lambda<Func<ConsecutivoContratoRegionales, object>>(Expression.Convert(prop, typeof(object)), param);

    //                //Dependiendo del modo de ordenamiento . . .
    //                if (GridViewSortDirection == SortDirection.Ascending)
    //                {

    //                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
    //                    if (cambioPaginacion == false)
    //                    {
    //                        GridViewSortDirection = SortDirection.Descending;
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
    //                    }
    //                    else
    //                    {
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList(); 
    //                    }
    //                }
    //                else
    //                {

    //                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
    //                    if (cambioPaginacion == false)
    //                    {
    //                        GridViewSortDirection = SortDirection.Ascending;
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
    //                    }
    //                    else
    //                    {
    //                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
    //                    }
    //                }

    //                GridViewSortExpression = expresionOrdenamiento;
    //            }
    //        }
    //        else
    //        {
    //            gridViewsender.DataSource = myGridResults;
    //        }

    //        gridViewsender.DataBind();
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}
    #endregion
}
