<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ConsecutivoContratoRegionales_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<%--<asp:HiddenField ID="hfIDConsecutivoContratoRegional" runat="server" />--%>
            <asp:HiddenField ID="hfIdContrato" runat="server" />
            <asp:HiddenField ID="hfIdRegional" runat="server" />
            <asp:HiddenField ID="hfIdVigencia" runat="server" />
            <asp:HiddenField ID="hfConsecutivoActual" runat="server" />

    <table width="90%" align="center">
    <%--    <tr class="rowB">
            <td>
                Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Vigencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdVigencia" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Consecutivo *
                <asp:RequiredFieldValidator runat="server" ID="rfvConsecutivo" ControlToValidate="txtConsecutivo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtConsecutivo" MaxLength="10" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftConsecutivo" runat="server" TargetControlID="txtConsecutivo"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>--%>
           <tr class="rowB">
            <td class="style1" style="width: 50%">
                N&uacute;mero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Consecutivo.
            </td>
            <td style="width: 50%">

            </td>
        </tr>
         <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtConsecutivoContrato"  
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
