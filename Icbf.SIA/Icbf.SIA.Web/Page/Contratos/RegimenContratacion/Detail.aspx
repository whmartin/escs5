<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_RegimenContratacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdRegimenContratacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre del R&#233;gimen de Contrataci&#243;n *
            </td>
            <td>
                Descripci&#243;n R&#233;gimen De Contrataci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreRegimenContratacion"  Enabled="false" 
                    Height="22px" MaxLength="50" Width="320px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px"  onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
