<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RegimenContratacion_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdRegimenContratacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                 Nombre del R&#233;gimen de Contrataci&#243;n*
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreRegimenContratacion" ControlToValidate="txtNombreRegimenContratacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Descripci&#243;n R&#233;gimen de Contrataci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreRegimenContratacion" Height="22px" 
                    MaxLength="50" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreRegimenContratacion" runat="server" TargetControlID="txtNombreRegimenContratacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="128" 
                    TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
