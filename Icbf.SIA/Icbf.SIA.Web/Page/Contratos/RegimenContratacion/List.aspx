<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RegimenContratacion_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Nombre del R&#233;gimen de Contrataci&#243;n
            </td>
            <td width="50%">
                Descripci&#243;n R&#233;gimen de Contrataci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtNombreRegimenContratacion" Height="22px" 
                    MaxLength="50" Width="320px"  ></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreRegimenContratacion" runat="server" TargetControlID="txtNombreRegimenContratacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " InvalidChars="&#63;"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="128" 
                    TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @" InvalidChars="&#63;"/>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegimenContratacion" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRegimenContratacion" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvRegimenContratacion_PageIndexChanging" 
                        OnSelectedIndexChanged="gvRegimenContratacion_SelectedIndexChanged" 
                        AllowSorting="True" onsorting="gvRegimenContratacion_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="R&#233;gimen" DataField="NombreRegimenContratacion" SortExpression="NombreRegimenContratacion" />--%>
                            <asp:TemplateField HeaderText="Nombre del R&#233;gimen de Contrataci&#243;n" ItemStyle-HorizontalAlign="Center" SortExpression="NombreRegimenContratacion">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreRegimenContratacion")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Descripci&#243;n" DataField="Descripcion" SortExpression="Descripcion" />--%>
                            <asp:TemplateField HeaderText="Descripci&#243;n R&#233;gimen de Contrataci&#243;n" ItemStyle-HorizontalAlign="Center" SortExpression="Descripcion">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Descripcion")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
