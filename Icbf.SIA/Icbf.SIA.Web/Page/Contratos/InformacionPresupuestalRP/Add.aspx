<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_InformacionPresupuestalRP_Add" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
<asp:HiddenField ID="hfIdInformacionPresupuestalRP" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Fecha de Solicitud RP *</td>
            <td width="50%">
                Número del RP *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroRP" ControlToValidate="txtNumeroRP"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td valign="bottom">
                <uc1:fecha runat="server" ID="txtFechaSolicitudRP" MaxLength="0" ></uc1:fecha>
            </td>
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtNumeroRP" MaxLength="50" Height="22px" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroRP" runat="server" TargetControlID="txtNumeroRP"
                    FilterType="Numbers" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor del RP *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorRP" ControlToValidate="txtValorRP"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Fecha expedición RP *                
            </td>
        </tr>
        <tr class="rowA">
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtValorRP" MaxLength="18" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorRP" runat="server" TargetControlID="txtValorRP"
                    FilterType="Numbers, Custom" />
            </td>
            <td valign="top">
                <uc1:fecha runat="server" ID="txtFechaExpedicionRP" MaxLength="0" ></uc1:fecha>
            </td>
        </tr>
    </table>
</asp:Content>
