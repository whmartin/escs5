<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_InformacionPresupuestalRP_List" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha de Solicitud RP
            </td>
            <td>
                Número del RP
            </td>
        </tr>
        <tr class="rowA">
            <td valign="bottom">
                <uc1:fecha runat="server" ID="txtFechaSolicitudRP" ></uc1:fecha>
            </td>
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtNumeroRP" MaxLength="50" ></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftNumeroRP" runat="server" TargetControlID="txtNumeroRP"
                    FilterType="Numbers" />

            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvInformacionPresupuestalRP" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdInformacionPresupuestalRP" CellPadding="0" Height="16px"
                        OnSorting="gvInformacionPresupuestalRP_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvInformacionPresupuestalRP_PageIndexChanging" OnSelectedIndexChanged="gvInformacionPresupuestalRP_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha de Solicitud RP" 
                                DataField="FechaSolicitudRP"  SortExpression="FechaSolicitudRP"  
                                DataFormatString="{0:dd/MM/yyyy} "/>
                            <asp:BoundField HeaderText="Número del RP" DataField="NumeroRP"  SortExpression="NumeroRP"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
