<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_InformacionPresupuestalRP_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdInformacionPresupuestalRP" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha de Solicitud RP *
            </td>
            <td>
                Número del RP *
            </td>
        </tr>
        <tr class="rowA">
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtFechaSolicitudRP"  Enabled="false"></asp:TextBox>
            </td>
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtNumeroRP"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor del RP *
            </td>
            <td>
                Fecha expedición RP *
            </td>
        </tr>
        <tr class="rowA">
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtValorRP"  Enabled="false"></asp:TextBox>
            </td>
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtFechaExpedicionRP"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
