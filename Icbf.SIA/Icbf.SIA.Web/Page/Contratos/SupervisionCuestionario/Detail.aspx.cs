using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionCuestionario_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionCuestionario";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionCuestionario.IdCuestionario", hfIdCuestionario.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdCuestionario = Convert.ToInt32(GetSessionParameter("SupervisionCuestionario.IdCuestionario"));
            RemoveSessionParameter("SupervisionCuestionario.IdCuestionario");

            if (GetSessionParameter("SupervisionCuestionario.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionCuestionario.Guardado");


            SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
            vSupervisionCuestionario = vSupervisionService.ConsultarSupervisionCuestionario(vIdCuestionario);
            hfIdCuestionario.Value = vSupervisionCuestionario.IdCuestionario.ToString();
            txtNombre.Text = vSupervisionCuestionario.Nombre;
            ddlIdDireccion.SelectedValue = vSupervisionCuestionario.IdDireccion.ToString();
            ddlIdVigenciaServicio.SelectedValue = vSupervisionCuestionario.IdVigenciaServicio.ToString();
            ddlIdModalidadServicio.SelectedValue = vSupervisionCuestionario.IdModalidadServicio.ToString();
            ddlIdSubComponente.SelectedValue = vSupervisionCuestionario.IdSubComponente.ToString();
            rblEstado.SelectedValue = vSupervisionCuestionario.EstadoPregunta.ToString();
            ddlIdComponente.SelectedValue = vSupervisionCuestionario.IdComponente.ToString();

            //Llear listBox
            lstSelect.Items.Clear();
            List<SupervisionCuestionario> Preguntas = vSupervisionService.ConsultarSupervisionCuestionarioPreguntas(vSupervisionCuestionario.IdCuestionario);
            foreach (SupervisionCuestionario vCuestionario in Preguntas)
            {
                lstSelect.Items.Insert(0,new ListItem(vCuestionario.NombrePregunta,vCuestionario.IdPregunta.ToString()));
                lstMain.Items.Remove( new ListItem( vCuestionario.NombrePregunta,vCuestionario.IdPregunta.ToString()));
            }


            ObtenerAuditoria(PageName, hfIdCuestionario.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionCuestionario.UsuarioCrea, vSupervisionCuestionario.FechaCrea, vSupervisionCuestionario.UsuarioModifica, vSupervisionCuestionario.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdCuestionario = Convert.ToInt32(hfIdCuestionario.Value);

            SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
            vSupervisionCuestionario = vSupervisionService.ConsultarSupervisionCuestionario(vIdCuestionario);
            InformacionAudioria(vSupervisionCuestionario, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionCuestionario(vSupervisionCuestionario);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionCuestionario.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Cuestionario", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControlesSupervision.ValoresEstadoCuestionario(rblEstado, "En Construccion", "Activo", "Inactivo");
            ManejoControlesSupervision.LlenarAplicable(rblAplicable, "1");
            ManejoControlesSupervision.LlenarModalidad(ddlIdModalidadServicio, "-1", true);
            ManejoControlesSupervision.LLenarVigencia(ddlIdVigenciaServicio, "-1", true);
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarSubComponente(ddlIdSubComponente, "-1", true,null);
            Controles.LlenarPreguntasLB(this.lstMain, true);
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);
            Controles.LlenarComponente(ddlIdComponente, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIdComponente_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelecionarSubcomponente();
    }

    protected void SelecionarSubcomponente()
    {
        if (ddlIdDireccion.SelectedValue != "5" && ddlIdComponente.SelectedValue == "5")
        {
            lblSubcomponente.Enabled = true;
            ddlIdSubComponente.Visible = true;
            ddlIdSubComponente.Enabled = true;
            lblSubcomponente.Visible = true;

        }
        else
        {
            lblSubcomponente.Enabled = false;
            ddlIdSubComponente.Visible = false;
            ddlIdSubComponente.Enabled = false;
            lblSubcomponente.Visible = false;
        }
    }

    protected void ddlIdDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelecionarSubcomponente();
    }
}
