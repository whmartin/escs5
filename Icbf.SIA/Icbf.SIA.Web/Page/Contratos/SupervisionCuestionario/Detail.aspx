<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SupervisionCuestionario_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdCuestionario" runat="server" />
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="upC" runat="server">
                    <ContentTemplate>
                        <table width="100%" align="center">
                            <tr class="rowB">
                                <td  style="width:50%">
                                    Nombre del cuestionario *
                                </td>
                                <td>
                                    Direccion *
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtNombre" Enabled="false" Width="80%"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdDireccion" Enabled="false" 
                                        Width="80%" AutoPostBack="True" 
                                        onselectedindexchanged="ddlIdDireccion_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    Vigencia del Servicio *
                                </td>
                                <td>
                                    Modalidad/Servicio *
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdVigenciaServicio" Enabled="false" Width="80%">
                                        <asp:ListItem Value="2015">2015</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdModalidadServicio" Enabled="false" Width="80%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    Componente *
                                </td>
                                <td>
                                    Estado *
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdComponente" Enabled="false" Width="80%"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlIdComponente_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    Aplicable a
                                </td>
                                <td>
                                    <asp:Label ID="lblSubcomponente" runat="server" Text="Subcomponente*"></asp:Label>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblAplicable" RepeatDirection="Horizontal">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdSubComponente" Enabled="false" Width="80%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" align="center">
                    <tr class="rowB">
                        <td>
                            Banco de Preguntas
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Preguntas del Cuestionario *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:ListBox ID="lstMain" runat="server" SelectionMode="Multiple" Width="100%" Height="380px"
                                ClientIDMode="Static"></asp:ListBox>
                        </td>
                        <td>
                        </td>
                        <td>
                            <input id="Agregar" type="button" value="->" onclick=" moverOptions('lstMain','lstSelect','lstSelect');" />
                            <br />
                            <br />
                            <input id="Eliminar" type="button" value="<-" onclick=" moverOptions('lstSelect','lstMain','lstSelect');" />
                        </td>
                        <td>
                            <asp:ListBox ID="lstSelect" runat="server" SelectionMode="Multiple" Width="85%" Height="380px"
                                ClientIDMode="Static"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
