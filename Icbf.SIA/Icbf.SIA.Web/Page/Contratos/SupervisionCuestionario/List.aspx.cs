using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionCuestionario_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionCuestionario";
    SupervisionService vSupervisionService = new SupervisionService();
    List<SupervisionDireccion> lstDireccion;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            String vNombre = null;
            int? vIdDireccion = null;
            int? vIdVigenciaServicio = null;
            int? vIdModalidadServicio = null;
            int? vIdSubComponente = null;
            int? vIdComponente = null;
            int? vEstado = null;
            if (txtNombre.Text!= "")
            {
                vNombre = Convert.ToString(txtNombre.Text);
            }
            if (ddlIdDireccion.SelectedValue!= "-1")
            {
                vIdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            }
            if (ddlIdVigenciaServicio.SelectedValue!= "-1")
            {
                vIdVigenciaServicio = Convert.ToInt32(ddlIdVigenciaServicio.SelectedValue);
            }
            if (ddlIdModalidadServicio.SelectedValue!= "-1")
            {
                vIdModalidadServicio = Convert.ToInt32(ddlIdModalidadServicio.SelectedValue);
            }
            if (ddlIdSubComponente.SelectedValue!= "-1")
            {
                vIdSubComponente = Convert.ToInt32(ddlIdSubComponente.SelectedValue);
            }
            if (ddlIdComponente.SelectedValue != "-1")
            {
                vIdComponente = Convert.ToInt32(ddlIdComponente.SelectedValue);
            }
            if (rblEstado.SelectedValue!= "-1" && rblEstado.SelectedValue !="")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            lstDireccion = vSupervisionService.ConsultarSupervisionDireccions(null, "", "", 1);
            gvSupervisionCuestionario.DataSource = vSupervisionService.ConsultarSupervisionCuestionarios(vNombre, vIdDireccion, vIdVigenciaServicio, vIdModalidadServicio, vIdSubComponente, vIdComponente, vEstado);
            gvSupervisionCuestionario.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionCuestionario.PageSize = PageSize();
            gvSupervisionCuestionario.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Cuestionario", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionCuestionario.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionCuestionario.IdCuestionario", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionCuestionario_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionCuestionario.SelectedRow);
    }
    protected void gvSupervisionCuestionario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionCuestionario.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionCuestionario.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionCuestionario.Eliminado");

            ManejoControlesSupervision.ValoresEstadoCuestionario(rblEstado, "En Construccion", "Activo", "Inactivo");
            ManejoControlesSupervision.LlenarAplicable(rblAplicable, "1");
            ManejoControlesSupervision.LlenarModalidad(ddlIdModalidadServicio, "-1", true);
            ManejoControlesSupervision.LLenarVigencia(ddlIdVigenciaServicio, "-1", true);
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarSubComponente(ddlIdSubComponente, "-1", true,null);
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);
            Controles.LlenarComponente(ddlIdComponente, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIdComponente_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelecionarSubcomponente();
    }

    protected void SelecionarSubcomponente()
    {
        if (ddlIdDireccion.SelectedValue != "5" && ddlIdComponente.SelectedValue == "5")
        {
            lblSubcomponente.Enabled = true;
            ddlIdSubComponente.Visible = true;
            ddlIdSubComponente.Enabled = true;
            lblSubcomponente.Visible = true;

        }
        else
        {
            lblSubcomponente.Enabled = false;
            ddlIdSubComponente.Visible = false;
            ddlIdSubComponente.Enabled = false;
            lblSubcomponente.Visible = false;
        }
    }
    protected void ddlIdDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelecionarSubcomponente();

    }

    protected void gvSupervisionCuestionario_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Se busca el nombre de la direccion
        var vRegistrosDireccion = from resp in lstDireccion
                                  where resp.IdDireccionesICBF == Convert.ToInt16(e.Row.Cells[1].Text)
                                  select resp;
        e.Row.Cells[1].Text = vRegistrosDireccion.ToList()[0].NombreDireccion;
    }
}
