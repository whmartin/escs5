<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SupervisionCuestionario_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width:50%">
                    Nombre del cuestionario *
                </td>
                <td>
                    Direccion *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombre" Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdDireccion" Width="80%" 
                        AutoPostBack="True" 
                        onselectedindexchanged="ddlIdDireccion_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Vigencia del Servicio *
                </td>
                <td>
                    Modalidad/Servicio *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdVigenciaServicio" Width="80%">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdModalidadServicio" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Componente *
                </td>
                <td>
                    Estado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdComponente" Width="80%" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlIdComponente_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Aplicable a
                </td>
                <td>
                    <asp:Label ID="lblSubcomponente" runat="server" Text="Subcomponente*"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblAplicable" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdSubComponente" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSupervisionCuestionario" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdCuestionario"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvSupervisionCuestionario_PageIndexChanging"
                        OnSelectedIndexChanged="gvSupervisionCuestionario_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre del cuestionario" DataField="Nombre" />
                            <asp:BoundField HeaderText="Direccion" DataField="Direccion" />
                            <asp:BoundField HeaderText="Vigencia del Servicio" DataField="IdVigenciaServicio" />
                            <asp:BoundField HeaderText="Modalidad/Servicio" DataField="IdModalidadServicio" />
                            <asp:BoundField HeaderText="Subcomponente" DataField="IdSubComponente" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoPreguntaStr" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
