using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionCuestionario_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionCuestionario";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
          

            vSupervisionCuestionario.Nombre = Convert.ToString(txtNombre.Text);
            vSupervisionCuestionario.IdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            vSupervisionCuestionario.IdVigenciaServicio = Convert.ToInt32(ddlIdVigenciaServicio.SelectedValue);
            vSupervisionCuestionario.IdModalidadServicio = Convert.ToInt32(ddlIdModalidadServicio.SelectedValue);
            vSupervisionCuestionario.IdSubComponente = Convert.ToInt32(ddlIdSubComponente.SelectedValue);
            vSupervisionCuestionario.IdComponente = Convert.ToInt32(ddlIdComponente.SelectedValue);
            vSupervisionCuestionario.EstadoPregunta = Convert.ToInt32(rblEstado.SelectedValue);

            vSupervisionCuestionario.Preguntas = new System.Data.DataTable();
            vSupervisionCuestionario.Preguntas.Columns.Add("Id");
            System.Data.DataRow row = vSupervisionCuestionario.Preguntas.NewRow();

            String[] vp = listPre.Value.Split('-');
            if (vp.Length==1)
            {
                toolBar.MostrarMensajeError("No hay ninguna pregunta asociada al cuestionario, verifique por favor.");

            }
            for (int i = 0; i < vp.Length ; i++)
            {
                row = vSupervisionCuestionario.Preguntas.NewRow();
                row["Id"] = vp[i];
                vSupervisionCuestionario.Preguntas.Rows.Add(row);
            }
            
            if (Request.QueryString["oP"] == "E")
            {
                if (hfIdCuestionario.Value != "")
                {
                    vSupervisionCuestionario.IdCuestionario = Convert.ToInt32(hfIdCuestionario.Value);

                }

                vSupervisionCuestionario.UsuarioModifica = GetSessionUser().NombreUsuario;
                vSupervisionCuestionario.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSupervisionCuestionario, this.PageName, vSolutionPage);
                vResultado = vSupervisionService.InsertarSupervisionCuestionario(vSupervisionCuestionario);
            }
            else
            {
                vSupervisionCuestionario.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSupervisionCuestionario, this.PageName, vSolutionPage);
                vResultado = vSupervisionService.InsertarSupervisionCuestionario(vSupervisionCuestionario);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)
            {
                SetSessionParameter("SupervisionCuestionario.IdCuestionario", vSupervisionCuestionario.IdCuestionario);
                SetSessionParameter("SupervisionCuestionario.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Cuestionario", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdCuestionario = Convert.ToInt32(GetSessionParameter("SupervisionCuestionario.IdCuestionario"));
            RemoveSessionParameter("SupervisionCuestionario.IdCuestionario");
            SupervisionCuestionario vSupervisionCuestionario = new SupervisionCuestionario();
            vSupervisionCuestionario = vSupervisionService.ConsultarSupervisionCuestionario(vIdCuestionario);
            hfIdCuestionario.Value = vSupervisionCuestionario.IdCuestionario.ToString();
            txtNombre.Text = vSupervisionCuestionario.Nombre;
            ddlIdDireccion.SelectedValue = vSupervisionCuestionario.IdDireccion.ToString();
            ddlIdVigenciaServicio.SelectedValue = vSupervisionCuestionario.IdVigenciaServicio.ToString();
            ddlIdModalidadServicio.SelectedValue = vSupervisionCuestionario.IdModalidadServicio.ToString();
            rblEstado.SelectedValue = vSupervisionCuestionario.EstadoPregunta.ToString();
            ddlIdComponente.SelectedValue = vSupervisionCuestionario.IdComponente.ToString();
            SelecionarSubcomponente();
            ddlIdSubComponente.SelectedValue = vSupervisionCuestionario.IdSubComponente.ToString();

            //Llear listBox
            lstSelect.Items.Clear();
            List<SupervisionCuestionario> Preguntas = vSupervisionService.ConsultarSupervisionCuestionarioPreguntas(vSupervisionCuestionario.IdCuestionario);
            foreach (SupervisionCuestionario vCuestionario in Preguntas)
            {
                lstSelect.Items.Insert(0, new ListItem(vCuestionario.NombrePregunta, vCuestionario.IdPregunta.ToString()));
                lstMain.Items.Remove(new ListItem(vCuestionario.NombrePregunta, vCuestionario.IdPregunta.ToString()));
            }
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionCuestionario.UsuarioCrea, vSupervisionCuestionario.FechaCrea, vSupervisionCuestionario.UsuarioModifica, vSupervisionCuestionario.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesSupervision.ValoresEstadoCuestionario(rblEstado, "En Construccion", "Activo", "Inactivo");
            ManejoControlesSupervision.LlenarAplicable(rblAplicable, "1");
            ManejoControlesSupervision.LlenarModalidad(ddlIdModalidadServicio, "-1", true);
            ManejoControlesSupervision.LLenarVigencia(ddlIdVigenciaServicio, "-1", true);
            ManejoControlesSupervision vControles = new ManejoControlesSupervision();
            vControles.LlenarPreguntasLB(this.lstMain, true);
            vControles.LlenarSubComponente(ddlIdSubComponente, "-1", true,null);
            vControles.LlenarDireccion(ddlIdDireccion, "-1", true);
            vControles.LlenarComponente(ddlIdComponente, "-1", true);
            SelecionarSubcomponente();
            


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIdComponente_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelecionarSubcomponente();
    }

    protected void SelecionarSubcomponente()
    {
        if (ddlIdDireccion.SelectedValue != "5" && ddlIdComponente.SelectedValue == "5")
        {
            lblSubcomponente.Enabled = true;
            ddlIdSubComponente.Visible = true;
            ddlIdSubComponente.Enabled = true;
            lblSubcomponente.Visible = true;

        }
        else
        {
            lblSubcomponente.Enabled = false;
            ddlIdSubComponente.Visible = false;
            ddlIdSubComponente.Enabled = false;
            lblSubcomponente.Visible = false;
        }
    }
    protected void ddlIdDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelecionarSubcomponente();
    }
}
