<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SupervisionCuestionario_Add"
    EnableEventValidation="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/SupervisionContratos-Cuestionario.js" type="text/javascript"></script>
    <asp:HiddenField ID="hfIdCuestionario" runat="server" />
    <asp:HiddenField ID="listPre" runat="server" />
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="upC" runat="server">
                    <ContentTemplate>
                        <table width="100%" align="center">
                            <tr class="rowB">
                                <td  style="width:50%">
                                    Nombre del cuestionario *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvNombre" ControlToValidate="txtNombre"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Direccion *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccion" ControlToValidate="ddlIdDireccion"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator runat="server" ID="cvIdDireccion" ControlToValidate="ddlIdDireccion"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:TextBox runat="server" ID="txtNombre" Width="80%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdDireccion" Width="80%" 
                                        AutoPostBack="True" 
                                        onselectedindexchanged="ddlIdDireccion_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    Vigencia del Servicio *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvIdVigenciaServicio" ControlToValidate="ddlIdVigenciaServicio"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator runat="server" ID="cvIdVigenciaServicio" ControlToValidate="ddlIdVigenciaServicio"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                </td>
                                <td>
                                    Modalidad/Servicio *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvIdModalidadServicio" ControlToValidate="ddlIdModalidadServicio"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator runat="server" ID="cvIdModalidadServicio" ControlToValidate="ddlIdModalidadServicio"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdVigenciaServicio" Width="80%">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdModalidadServicio" Width="80%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    Componente *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvIdComponente" ControlToValidate="ddlIdComponente"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator runat="server" ID="cvIdComponente" ControlToValidate="ddlIdComponente"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                </td>
                                <td>
                                    Estado *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlIdComponente" Width="80%" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlIdComponente_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td>
                                    Aplicable a
                                </td>
                                <td>
                                    <asp:Label ID="lblSubcomponente" runat="server" Text="Subcomponente*">
                                        <asp:RequiredFieldValidator ID="rfvIdSubComponente" runat="server" ErrorMessage="Campo Requerido"
                                            ForeColor="Red" ControlToValidate="ddlIdSubComponente" SetFocusOnError="true"
                                            Display="Dynamic" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvIdSubComponente" runat="server" ErrorMessage="Campo Requerido"></asp:CustomValidator>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rblAplicable" RepeatDirection="Horizontal">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlIdSubComponente" runat="server" Width="80%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" align="center">
                    <tr class="rowB">
                        <td>
                            Banco de Preguntas
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Preguntas del Cuestionario *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:ListBox ID="lstMain" runat="server" SelectionMode="Multiple" Width="100%" Height="380px"
                                ClientIDMode="Static"></asp:ListBox>
                        </td>
                        <td>
                        </td>
                        <td>
                            <input id="Agregar" type="button" value="->" onclick=" moverOptions('lstMain','lstSelect','lstSelect');" />
                            <br />
                            <br />
                            <input id="Eliminar" type="button" value="<-" onclick=" moverOptions('lstSelect','lstMain','lstSelect');" />
                        </td>
                        <td>
                            <asp:ListBox ID="lstSelect" runat="server" SelectionMode="Multiple" Width="85%" Height="380px"
                                ClientIDMode="Static"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
