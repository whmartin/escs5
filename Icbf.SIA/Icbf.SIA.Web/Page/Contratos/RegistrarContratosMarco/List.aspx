<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contrato_List" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha Registro *
            </td>
            <td>
                N&#250;mero Proceso *
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="txtFechaRegistro" runat="server" /></td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProceso"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                N&#250;mero Contrato *
            </td>
            <td>
                Modalidad *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato"  ></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidad"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categor&#237;a Contrato *
            </td>
            <td>
                Tipo Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlIdCategoriaContrato_SelectedIndexChanged"  ></asp:DropDownList>
            </td>
            <td>
                 <asp:DropDownList runat="server" ID="ddlIdTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContrato_PageIndexChanging" OnSelectedIndexChanged="gvContrato_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro" DataField="FechaRegistro" />
                            <asp:BoundField HeaderText="N&#250;mero Proceso" DataField="NumeroProceso" />
                            <asp:BoundField HeaderText="N&#250;mero Contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="Modalidad" DataField="Modalidad" />
                            <asp:BoundField HeaderText="Categor&#237;a Contrato" DataField="CategoriaContrato" />
                            <asp:BoundField HeaderText="Tipo Contrato" DataField="TipoContrato" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
