<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contrato_Add" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

<asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlContrato">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Fecha Registro
            </td>
            <td>
                Consecutivo<asp:RequiredFieldValidator ID="rfvtxtConsecutivo" 
                    runat="server" ControlToValidate="txtConsecutivo" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                
                <uc1:fecha ID="fechaRegistro" runat="server" />
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivo" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtConsecutivo" 
                    runat="server" FilterType="Numbers" TargetControlID="txtConsecutivo" 
                    ValidChars="" />
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
                N&#250;mero Contrato
            </td>
            <td>
                Modalidad Selecci&#243;n
                <asp:RequiredFieldValidator runat="server" ID="rfvddlModalidad" ControlToValidate="ddlModalidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvddlModalidad" ControlToValidate="ddlModalidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidad"  ></asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Requiere Acta Inicio
                <asp:RequiredFieldValidator runat="server" ID="rfvrblRequiereActaInicio" ControlToValidate="rblRequiereActaInicio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            <td>
                Regimen de Contratación
                <asp:RequiredFieldValidator runat="server" ID="rfvddlRegimenContratacion" ControlToValidate="ddlRegimenContratacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvddlRegimenContratacion" ControlToValidate="ddlRegimenContratacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblRequiereActaInicio" runat="server">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList ID="ddlRegimenContratacion" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categor&#237;a Contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvdllIdCategoriaContrato" ControlToValidate="dllIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvdllIdCategoriaContrato" ControlToValidate="dllIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Tipo Contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvdllIdTipoContrato" ControlToValidate="dllIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvdllIdTipoContrato" ControlToValidate="dllIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="dllIdCategoriaContrato"  ></asp:DropDownList>                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="dllIdTipoContrato"  ></asp:DropDownList>               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Afecta Plan de Compras</td>
            <td>
                Maneja Aportes</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblAfectaPlanCompras" runat="server">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList ID="rblManejaAportes" runat="server" 
                    RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional
                </td>
            <td>Nombre Solicitante</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlRegionalContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreSolicitante" Height="50px" 
                    MaxLength="50" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);" onfocus=blur(); class="enable"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Dependencia Solicitante</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDependenciaSolicitante" Height="50px" 
                    MaxLength="50" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);" onfocus=blur(); class="enable">
                    </asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato
            </td>
            <td>Alcance Objeto Contrato</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" Height="50px" 
                    MaxLength="300" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);">
                    </asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtAlcanceObjetoContrato" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                Código del Producto</td>
            <td>
                  
                Nombre del Producto</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtCodigoProducto" runat="server" onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtNombreProducto" runat="server" onfocus=blur(); class="enable"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad de Medida</td>
            <td>
                  
                Cantidad</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtUnidadMedida" runat="server" onfocus=blur(); class="enable"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtCantidad" runat="server" onfocus=blur(); class="enable"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Unitario</td>
            <td>
                  
                Valor Total</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtValorUnitario" runat="server" onfocus=blur(); class="enable">0</asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtValorTotal" runat="server" onfocus=blur(); class="enable">0</asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Inicial Contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorInicialContrato" ControlToValidate="txtValorInicialContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtValorInicialContrato" MaxLength="50" 
                    Width="200px">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Adiciones
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorTotalAdiciones" ControlToValidate="txtValorTotalAdiciones"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>   
            </td>
            <td>
                Valor Final Contrato
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorFinalContrato" ControlToValidate="txtValorFinalContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalAdiciones" MaxLength="50" 
                    Width="200px" onfocus=blur(); class="enable">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtValorTotalAdiciones" runat="server" TargetControlID="txtValorTotalAdiciones"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorFinalContrato" MaxLength="50" 
                    Width="200px" onfocus=blur(); class="enable">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorFinalContrato" runat="server" TargetControlID="txtValorFinalContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Aportes ICBF
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorAportesICBF" ControlToValidate="txtValorAportesICBF"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
                </td>
            <td>
                Valor Aportes Operador 
                 <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorAportesOperador" ControlToValidate="txtValorAportesOperador"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesICBF" MaxLength="50" 
                    Width="200px" onfocus=blur(); class="enable" Text="0"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesICBF" runat="server" TargetControlID="txtValorAportesICBF"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesOperador" MaxLength="50" 
                    Width="200px" onfocus=blur(); class="enable">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesOperador" runat="server" TargetControlID="txtValorAportesOperador"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Justificar Adici&#243;n Superior al 50%
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicionSuperior" MaxLength="50" ></asp:TextBox>
      <Ajax:FilteredTextBoxExtender ID="FtJustificacionAdicionSuperior" runat="server" TargetControlID="txtJustificacionAdicionSuperior"
                    FilterType="Custom, Numbers" ValidChars="0123456789" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Suscripci&#243;n 
            </td>
            <td>Fecha Inicio Ejecuci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaSuscripcion" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaInicioEjecucion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Finalizaci&#243;n Inicial
            </td>
            <td>Fecha Final Terminaci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaFinalizacionInicial" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaFinalTerminacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Proyectada Liquidaci&#243;n </td>
            <td>Fecha de Liquidaci&#243;n</td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaProyectadaLiquidacion" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaLiquidacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Prorrogas
                <asp:RequiredFieldValidator ID="rfvtxtProrrogas" runat="server" 
                    ControlToValidate="txtProrrogas" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
             </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtProrrogas" runat="server" MaxLength="10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtProrrogas" runat="server" 
                    FilterType="Numbers" TargetControlID="txtProrrogas" ValidChars="" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
         <tr class="rowB">
            <td>
                Plazo Total de ejecuci&#243;n
                <asp:RequiredFieldValidator ID="rfvtxtPlazoTotal" runat="server" 
                    ControlToValidate="txtPlazoTotal" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
             </td>
            <td>Fecha Firma Acta Inicio 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtPlazoTotal" runat="server" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtPlazoTotal_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtPlazoTotal" 
                    ValidChars="" />
            </td>
            <td>
                <uc1:fecha ID="fechaFirmaActaInicio" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia Fiscal Inicial
                <asp:RequiredFieldValidator ID="rfvtxtVigenciaFiscalInicial" runat="server" 
                    ControlToValidate="txtVigenciaFiscalInicial" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td>
                Vigencia Fiscal Final
                <asp:RequiredFieldValidator ID="rfvtxtVigenciaFiscalFinal" runat="server" 
                    ControlToValidate="txtVigenciaFiscalFinal" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtVigenciaFiscalInicial" runat="server" MaxLength="4" 
                    Width="80px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtVigenciaFiscalInicial_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtVigenciaFiscalInicial" 
                    ValidChars="" />
            </td>
            <td>
                <asp:TextBox ID="txtVigenciaFiscalFinal" runat="server" MaxLength="4" 
                    Width="80px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtVigenciaFiscalFinal_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtVigenciaFiscalFinal" 
                    ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                No. Documento Vigencia Futura</td>
            <td>
                Unidad Ejecuci&#243;n 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtDocumentoVigFutura" runat="server"></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftetxtDocumentoVigFutura" 
                    runat="server" FilterType="Numbers" TargetControlID="txtDocumentoVigFutura" 
                    ValidChars="" />
            </td>
            <td>
                <asp:DropDownList ID="ddlUnidadEjecucion" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Datos Adicionales lugar Ejecuci&#243;n</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtDatosAdicionales" runat="server" MaxLength="128"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtDatosAdicionales_FilteredTextBoxExtender" 
                    runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters" 
                    TargetControlID="txtDatosAdicionales" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Estado Contrato</td>
            <td>
                Forma Pago
                <asp:RequiredFieldValidator ID="rfvddlFormaPago" runat="server" 
                    ControlToValidate="ddlFormaPago" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvddlFormaPago" runat="server" 
                    ControlToValidate="ddlFormaPago" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                    SetFocusOnError="true" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlEstadoContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlFormaPago" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
