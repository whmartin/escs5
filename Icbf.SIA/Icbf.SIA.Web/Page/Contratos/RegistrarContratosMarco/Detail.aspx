<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Contrato_Detail" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdContrato" runat="server" />
     <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Fecha Registro
            </td>
            <td>
                Consecutivo</td>
        </tr>
        <tr class="rowA">
            <td>
                
                <asp:TextBox ID="txtFechaRegistro" runat="server" Enabled="False"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtConsecutivo" MaxLength="50" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtConsecutivo" 
                    runat="server" FilterType="Numbers" TargetControlID="txtConsecutivo" 
                    ValidChars="" />
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
                N&#250;mero Contrato
            </td>
            <td>
                Modalidad Selecci&#243;n
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidad" Enabled="False"  ></asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Requiere Acta Inicio
                </td>
            <td>
                Regimen de Contrataci&#243;n
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblRequiereActaInicio" runat="server" Enabled="False">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList ID="ddlRegimenContratacion" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categor&#237;a Contrato
                </td>
            <td>
                Tipo Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="dllIdCategoriaContrato" Enabled="False"  ></asp:DropDownList>                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="dllIdTipoContrato" Enabled="False"  ></asp:DropDownList>               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Afecta Plan de Compras</td>
            <td>
                Maneja Aportes</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblAfectaPlanCompras" runat="server" Enabled="False">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList ID="rblManejaAportes" runat="server" 
                    RepeatDirection="Horizontal" Enabled="False">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional
                </td>
            <td>Nombre Solicitante</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlRegionalContrato" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreSolicitante" Height="50px" 
                    MaxLength="50" TextMode="MultiLine" Width="320px" 
                    onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Dependencia Solicitante</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDependenciaSolicitante" Height="50px" 
                    MaxLength="50" TextMode="MultiLine" Width="320px" 
                    onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);" Enabled="False"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato
            </td>
            <td>Alcance Objeto Contrato</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" Height="50px" 
                    MaxLength="300" TextMode="MultiLine" Width="320px" 
                    onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtAlcanceObjetoContrato" runat="server" Height="50px" 
                    MaxLength="300" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);" 
                    TextMode="MultiLine" Width="320px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                C�digo del Producto</td>
            <td>
                  
                Nombre del Producto</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtCodigoProducto" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtNombreProducto" runat="server" Enabled="False"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad de Medida</td>
            <td>
                  
                Cantidad</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtUnidadMedida" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtCantidad" runat="server" Enabled="False"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Unitario</td>
            <td>
                  
                Valor Total</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtValorUnitario" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
               
                <asp:TextBox ID="txtValorTotal" runat="server" Enabled="False"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Inicial Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtValorInicialContrato" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Adiciones
                </td>
            <td>
                Valor Final Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalAdiciones" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtValorTotalAdiciones" runat="server" TargetControlID="txtValorTotalAdiciones"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorFinalContrato" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorFinalContrato" runat="server" TargetControlID="txtValorFinalContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Aportes ICBF
                </td>
            <td>
                Valor Aportes Operador 
                 </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesICBF" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesICBF" runat="server" TargetControlID="txtValorAportesICBF"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesOperador" MaxLength="50" 
                    Width="200px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesOperador" runat="server" TargetControlID="txtValorAportesOperador"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Justificar Adici&#245;n Superior al 50%
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicionSuperior" MaxLength="50" 
                    Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Suscripci�n 
            </td>
            <td>Fecha Inicio Ejecuci�n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaSuscripcion" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaInicioEjecucion" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Finalizaci&#243;n Inicial
            </td>
            <td>Fecha Final Terminaci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaFinalizacionInicial" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaFinalTerminacion" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Proyectada Liquidaci&#243;n </td>
            <td>Fecha de Liquidaci&#243;n</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaProyecradaLiquidacion" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaLiquidacion" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Prorrogas
                </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtProrrogas" runat="server" MaxLength="10" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtProrrogas" runat="server" 
                    FilterType="Numbers" TargetControlID="txtProrrogas" ValidChars="" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
         <tr class="rowB">
            <td>
                Plazo Total de ejecuci&#243;n
                </td>
            <td>Fecha Firma Acta Inicio 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtPlazoTotal" runat="server" MaxLength="3" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtPlazoTotal_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtPlazoTotal" 
                    ValidChars="" />
            </td>
            <td>
                <asp:TextBox ID="txtFechaFirmaActaInicio" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia Fiscal Inicial
                </td>
            <td>
                Vigencia Fiscal Final
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtVigenciaFiscalInicial" runat="server" MaxLength="4" 
                    Width="80px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtVigenciaFiscalInicial_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtVigenciaFiscalInicial" 
                    ValidChars="" />
            </td>
            <td>
                <asp:TextBox ID="txtVigenciaFiscalFinal" runat="server" MaxLength="4" 
                    Width="80px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtVigenciaFiscalFinal_FilteredTextBoxExtender" 
                    runat="server" FilterType="Numbers" TargetControlID="txtVigenciaFiscalFinal" 
                    ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                No. Documento Vigencia Futura</td>
            <td>
                Unidad Ejecuci&#243;n 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtDocumentoVigFutura" runat="server" Enabled="False"></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftetxtDocumentoVigFutura" 
                    runat="server" FilterType="Numbers" TargetControlID="txtDocumentoVigFutura" 
                    ValidChars="" />
            </td>
            <td>
                <asp:DropDownList ID="ddlUnidadEjecucion" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Datos Adicionales lugar Ejecuci&#243;n</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtDatosAdicionales" runat="server" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtDatosAdicionales_FilteredTextBoxExtender" 
                    runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters" 
                    TargetControlID="txtDatosAdicionales" ValidChars="/������������ .,@_():;" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Estado Contrato</td>
            <td>
                Forma Pago
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlEstadoContrato" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlFormaPago" runat="server" Enabled="False">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
