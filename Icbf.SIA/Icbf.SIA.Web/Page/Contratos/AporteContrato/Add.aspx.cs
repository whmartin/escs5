using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición para la entidad AporteContrato
/// </summary>
public partial class Page_AporteContrato_Add : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/AporteContrato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            CargarDatosIniciales();
        }
        AsociarInfoCajas();
    }

    protected void rblTipoAporte_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        txtValorAporte.Enabled = true;
        rfvValorAporte.Enabled = true;
        txtFechaRP.Requerid = false;
        rfvnumerorp.Enabled = false;
        

        if (rblTipoAporte.SelectedItem.Text.Equals("Especie"))
        {
            txtDescripcionAporte.Enabled = true;
            lblDescripcionAporteEspecieCon.Visible = true;
            lblDescripcionAporteEspecieSin.Visible = false;
            rfvDescripcionAporte.Enabled = true;
            txtFechaRP.Visible = false;
            txtNumeroRP.Visible = false;
            lblFecharp.Visible = false;
            lblNumerorp.Visible = false;
            txtValorAporte.Text = string.Empty;

        }
        else
        {
            txtDescripcionAporte.Text = string.Empty;
            rfvDescripcionAporte.Enabled = false;
            txtDescripcionAporte.Enabled = false;
            lblDescripcionAporteEspecieCon.Visible = false;
            lblDescripcionAporteEspecieSin.Visible = true;
            rfvDescripcionAporte.Enabled = false;
            if (HfAporte.Value == "true")
            {
                decimal valor = Convert.ToDecimal(hfValor.Value);
                txtValorAporte.Text = string.Format("{0:$#,##0}", valor);
                txtValorAporte.Enabled = false;
            }
            
            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
            {
               
                txtFechaRP.Visible = true;
                txtNumeroRP.Visible = true;
                lblFecharp.Visible = true;
                lblNumerorp.Visible = true;
            }
        }

        activarRP();
        //txtValorAporte.Text = string.Empty;
        txtValorAporte.Focus();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    
   /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad AporteContrato
        /// </summary>
    private void Guardar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vResultado;
            int idContrato;
            int? idTercero = null;
            DateTime fecha = new DateTime(1900,1,1);
            
            if (string.IsNullOrEmpty(txtValorAporte.Text) || decimal.Parse(txtValorAporte.Text, NumberStyles.Currency) <= 0)
            {
                toolBar.MostrarMensajeError("Debe ingresar un valor mayor a cero (0)");
                return;
            }

            //decimal number;
            //bool result = decimal.Parse(txtValorAporte.Text, NumberStyles.Currency);
            //if (!result)
            //{
            //    toolBar.MostrarMensajeError("El valor del aporte es demasiado largo");
            //    return;
            //}
           
            if (GetSessionParameter("Contrato.ContratosAPP") != null)
            {
                idContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
                bool esIcbf = ViewState["Aporte"].ToString() == "Contratista" ? false : true;

                if (ExisteContratoAporte(idContrato,Convert.ToBoolean(rblTipoAporte.SelectedValue), decimal.Parse(txtValorAporte.Text, NumberStyles.Currency),esIcbf))
                {
                    toolBar.MostrarMensajeError("El contrato, tipo de aporte y valor del aporte ingresado ya se encuentra asociado al contratista");
                    return;
                }
            }
            else
            {
                toolBar.MostrarMensajeError("No se encuentra número de contrato para validar");
                return;
            }

            //decimal valorinicicialcontrato = (decimal)vContratoService.ConsultarContrato(idContrato).ValorInicialContrato;
            decimal acomuladoaportes = vContratoService.ConsultarAporteContratos(null, null, null, null, null, idContrato, null, null, null,null).Sum(d => d.ValorAporte);
            acomuladoaportes += decimal.Parse(txtValorAporte.Text, NumberStyles.Currency);

            //if (acomuladoaportes > valorinicicialcontrato)
            //{
            //    toolBar.MostrarMensajeError("El Valor de los Aportes supera el Valor Final del Contrato/Convenio"); 
            //    return;
            //}

            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
                 idTercero = Convert.ToInt32(hdIdEntidad.Value);

            AporteContrato vAporteContrato = new AporteContrato();
            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
                vAporteContrato.AportanteICBF = false;
            else
                vAporteContrato.AportanteICBF = true;
            vAporteContrato.NumeroIdentificacionICBF = Convert.ToString(txtNumeroIdentificacion.Text).Trim();
            vAporteContrato.ValorAporte = decimal.Parse(txtValorAporte.Text, NumberStyles.Currency);
            vAporteContrato.DescripcionAporte = Convert.ToString(txtDescripcionAporte.Text).Trim();
            if (rblTipoAporte.SelectedItem.Text.Equals("Dinero"))
                vAporteContrato.AporteEnDinero = true;
            else
                vAporteContrato.AporteEnDinero = false;
            vAporteContrato.IdContrato = idContrato;
            vAporteContrato.Estado = Convert.ToBoolean(true);
            vAporteContrato.IDEntidadProvOferente = idTercero;
            if (txtFechaRP.Date != fecha.Date)
                vAporteContrato.FechaRP = txtFechaRP.Date;
            else
                vAporteContrato.FechaRP = null;
            if (txtNumeroRP.Text != String.Empty)
                vAporteContrato.NumeroRP = txtNumeroRP.Text;
            else
                vAporteContrato.NumeroRP = null;

            vAporteContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
            InformacionAudioria(vAporteContrato, this.PageName, vSolutionPage);
            vResultado = vContratoService.InsertarAporteContrato(vAporteContrato);
            
            if (vResultado == 0)
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            else if(vResultado >= 1)
            {
                toolBar.MostrarMensajeGuardado();
                return;

            }

            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //    return;
            //}
                
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            String valor = Request.QueryString["Aporte"];
            if (valor == null)
            {
                toolBar.MostrarMensajeError("No ha enviado la información suficiente para el proceso");
                return;
            }

            ViewState["Aporte"] = valor;

            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            String Titulo = null;
            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
                Titulo = "Aporte Contratista";
            else
                Titulo = "Aporte ICBF";

            toolBar.EstablecerTitulos(Titulo, SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        Retornar();
    }

    /// <summary>
   /// Método de carga de listas y valores por defecto 
   /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
         ftValorAporte.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();

            rblTipoAporte.Items.Insert(0, new ListItem("Dinero", "true"));

            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
            {
                imgTipoIdentificacionContratista.Visible = true;
                txttipoAportante.Text = "Contratista";
                rblTipoAporte.Enabled = false;
                rfvTipoAporte.Enabled = false;
                cvrbltipoaporte.Enabled = false;
            }
            else
            {
                CargarInfoICBF();

                txtDescripcionAporte.Text = string.Empty;
                rfvDescripcionAporte.Enabled = false;
                txtDescripcionAporte.Enabled = false;
                lblDescripcionAporteEspecieCon.Visible = false;
                lblDescripcionAporteEspecieSin.Visible = true;
                rfvDescripcionAporte.Enabled = false;                
                decimal valor = Convert.ToDecimal(hfValor.Value);
                txtValorAporte.Text = string.Format("{0:$#,##0}", valor);
                txtValorAporte.Enabled = false;
                rblTipoAporte.Items.FindByText("Dinero").Selected = true;
            }


            rblTipoAporte.Items.Insert(1, new ListItem("Especie", "false"));


         //rblEstado.Items.Insert(0, new ListItem("Activo", "true"));
         //rblEstado.Items.Insert(0, new ListItem("Inactivo", "false"));
         //rblEstado.SelectedValue = "true";
        
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Métodoque carga los datos cuando el aporte es ICBF
    /// </summary>
    private void CargarInfoICBF()
    {
        imgTipoIdentificacionContratista.Visible = false;
        hdTipoIdentificacion.Value = "NIT";
        hdNumeroIdentificacion.Value = "899999239";
        hdInformacionAportante.Value = "ICBF";
        txttipoAportante.Text = "ICBF";
    }


    private bool ExisteContratoAporte(int pIdContrato, Boolean pAporteEnDinero, Decimal pValorAporte, bool pEsICBF)
    {
        return vContratoService.ExisteContratoAporte(pIdContrato,pAporteEnDinero,pValorAporte,pEsICBF);
    }

    private void AsociarInfoCajas()
    {
        txtNumeroIdentificacion.Text = hdNumeroIdentificacion.Value;
        txtTipoIdentificacion.Text = hdTipoIdentificacion.Value;
        txtInformacionAportante.Text = hdInformacionAportante.Value;
    }

    private void Retornar()
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }


    protected void btnCargarTipoAporte_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        rblTipoAporte.Enabled = true;
        rfvTipoAporte.Enabled = true;
        cvrbltipoaporte.Enabled = true;
    }

    protected void txtDescripcionAporte_OnTextChanged(object sender, EventArgs e)
    {
        activarRP();
    }

    protected void txtValorAporte_OnTextChanged(object sender, EventArgs e)
    {
        activarRP();
    }

    private void activarRP()
    {
        toolBar.LipiarMensajeError();
        if (rblTipoAporte.SelectedItem.Text.Equals("Dinero"))
        {
            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
            {
                txtFechaRP.Requerid = false;
                rfvnumerorp.Enabled = false;
            }
        }
    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["vValorInicialContrato"]))
             hfValor.Value = Request.QueryString["vValorInicialContrato"];

        if (!string.IsNullOrEmpty(Request.QueryString["AporteIcbf"]))
            HfAporte.Value = Request.QueryString["AporteIcbf"];

    }
}
