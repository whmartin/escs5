﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_Contratos_ProcesoImposicionMulta_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesModificacion";
    string PageName1 = "Contratos/ConsModContractual";
    string TIPO_ESTRUCTURA = "ProcesoImposicionMultas";
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
       SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage) || ValidateAccess(toolBar, PageName1, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Contrato.IdImposicionMultas", hfIdImposicionMultas.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {


        //EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx?oP=E", false);
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());
            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Proceso de Imposición de multas", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ImposicionMultas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ImposicionMultas.Guardado");

            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();
            var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            int vIdConModContratual;

            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);

                if (detalleSolicitud.Estado == "Registro" || detalleSolicitud.Estado == "Devuelta")
                    toolBar.MostrarBotonEditar(true);
                else
                    toolBar.MostrarBotonEditar(false);
            }
            else
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();

                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                {
                    hfEsSubscripcion.Value = "1";
                    RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                {
                    hfEsSubscripcion.Value = "2";
                    RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                }

                toolBar.MostrarBotonEditar(false);
            }

            int vIdImposicionMultas = Convert.ToInt32(GetSessionParameter("Contrato.IdImposicionMultas"));
            RemoveSessionParameter("Contrato.IdImposicionMultas");
            hfIdImposicionMultas.Value = vIdImposicionMultas.ToString();
            ImposicionMulta vImposicionMulta = vContratoService.ConsultarImposicionMultaId(vIdImposicionMultas).First();
            txtConsideraciones.Text = vImposicionMulta.Razones;
            toolBar.MostrarBotonNuevo(false);

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalizacionInicialContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdCntrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdCntrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
}

