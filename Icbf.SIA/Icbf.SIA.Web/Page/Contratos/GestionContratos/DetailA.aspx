﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="DetailA.aspx.cs" Inherits="Page_Contratos_GestionContratos_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <link href="../../../Styles/TabContainer.css" rel="stylesheet" type="text/css" />
    <div>
        <Ajax:TabContainer ID="tcInfoContraro" runat="server" Visible="true" 
            ActiveTabIndex="0" CssClass="ajax__tab_green-theme"
            Width="100%" OnActiveTabChanged="tcInfoContraro_ActiveTabChanged" 
            AutoPostBack="false">
            <Ajax:TabPanel ID="tpRegistrarContrato" runat="server" HeaderText="Registrar Contrato">
                <HeaderTemplate>
                    Registrar Contrato Adhesión
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameRegistrarContratos"  runat="server" name="frameRegistrarContratos" noresize="false" overflow="scroll" 
                        src="../RegistrarContraosAdhesion/Add.aspx" width="100%" height="440px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpSupervisorInterventor" runat="server" HeaderText="Supervisor Interventor" Enabled="True">
                <HeaderTemplate>
                    Supervisor Interventor
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameSupervisorInterventor" runat="server" name="frameSupervisorInterventor" noresize="false" overflow="scroll"
                        src="../RelacionarSupervisorInterventor/List.aspx" width="100%" height="440px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpGarantias" runat="server" HeaderText="Garantias" Enabled="True">
                <HeaderTemplate>
                    Garantias
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameGarantias" runat="server" name="frameGarantias" noresize="false" overflow="scroll"
                    src="../Garantia/List.aspx" height="440px" width="100%"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpAmparos" runat="server" HeaderText="Amparos" Enabled="true">
                <HeaderTemplate>
                    Amparos
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameAmparos" runat="server" name="frameSucursal" noresize="true" overflow="scroll"
                        src="../AmparosAsociados/List.aspx" width="100%" height="440px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="tpClausulas" runat="server" HeaderText="Cláusulas" Enabled="True">
                <HeaderTemplate>
                    Cláusulas
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameClausulas" runat="server" name="frameClausulas"
                        noresize="true" overflow="scroll" src="../GestionarClausulasContrato/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
                        <Ajax:TabPanel ID="TbObliciones" runat="server" HeaderText="Obligaiones" Enabled="True">
                <HeaderTemplate>
                    Obligaciones
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="IframeObligaciones" runat="server" name="frameObliciones"
                        noresize="true" overflow="scroll" src="../GestionarObligacion/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
            </Ajax:TabPanel>
            <Ajax:TabPanel ID="Tpunidadmedidad" runat="server" HeaderText="Unidad de Medida" Enabled="True">
                <HeaderTemplate>
                    Unidad de Medida
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameUnidadMedida" runat="server" name="frameUnidadMedida"
                        noresize="true" overflow="scroll" src="../UnidadMedida/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
              </Ajax:TabPanel>

              <Ajax:TabPanel ID="Tpaportecontrato" runat="server" HeaderText="Aporte Contrato" Enabled="True">
                <HeaderTemplate>
                    Aporte Contratista
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameAporteContrato" runat="server" name="frameAporteContrato"
                        noresize="true" overflow="scroll" src="../AporteContrato/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
              </Ajax:TabPanel>

              <Ajax:TabPanel ID="TpAporteICBF" runat="server" HeaderText="Aporte ICBF" Enabled="True">
                <HeaderTemplate>
                    Aporte ICBF
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameAporteICBF" runat="server" name="frameAporteICBF"
                        noresize="true" overflow="scroll" src="../AporteContratoICBF/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
              </Ajax:TabPanel>

               <Ajax:TabPanel ID="TpLugarEjecucion" runat="server" HeaderText="Lugar Ejecución" Enabled="True">
                <HeaderTemplate>
                    Lugar Ejecución
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameLugarEjecucion" runat="server" name="frameLugarEjecucion"
                        noresize="true" overflow="scroll" src="../LugarEjecucionContrato/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
              </Ajax:TabPanel>

               <Ajax:TabPanel ID="TpInformacionCDP" runat="server" HeaderText="Información de CDP" Enabled="True">
                <HeaderTemplate>
                    Información de CDP
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameInformacionCDP" runat="server" name="frameInformacionCDP"
                        noresize="true" overflow="scroll" src="../InformacionPresupuestal/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
              </Ajax:TabPanel>

               <Ajax:TabPanel ID="TpInformacionRP" runat="server" HeaderText="Información RP" Enabled="True">
                <HeaderTemplate>
                    Información RP
                </HeaderTemplate>
                <ContentTemplate>
                    <iframe id="frameInformacionRP" runat="server" name="frameInformacionRP"
                        noresize="true" overflow="scroll" src="../InformacionPresupuestalRP/List.aspx" width="100%"
                        height="440px"></iframe>
                </ContentTemplate>
              </Ajax:TabPanel>
        </Ajax:TabContainer>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPrincipalButton" Runat="Server">
</asp:Content>

