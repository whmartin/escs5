using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionSubcomponentes_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionSubcomponentes";
    SupervisionService vSupervisionService = new SupervisionService();
    System.Drawing.Color vColorDesactivado = System.Drawing.Color.FromArgb(235, 235, 228);

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionSubcomponentes.IdSubcomponente", hfIdSubcomponente.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {

            ddlIdDireccion.BackColor = vColorDesactivado;
            txtNombre.BackColor = vColorDesactivado;
            txtNombre.Enabled = false;

            int vIdSubcomponente = Convert.ToInt32(GetSessionParameter("SupervisionSubcomponentes.IdSubcomponente"));
            RemoveSessionParameter("SupervisionSubcomponentes.IdSubcomponente");

            if (GetSessionParameter("SupervisionSubcomponentes.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionSubcomponentes.Guardado");

            SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();
            vSupervisionSubcomponentes = vSupervisionService.ConsultarSupervisionSubcomponentes(vIdSubcomponente);
            hfIdSubcomponente.Value = vSupervisionSubcomponentes.IdSubcomponente.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionSubcomponentes.IdDireccion.ToString();
            txtNombre.Text = vSupervisionSubcomponentes.Nombre;
            rblEstado.SelectedValue = vSupervisionSubcomponentes.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdSubcomponente.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionSubcomponentes.UsuarioCrea, vSupervisionSubcomponentes.FechaCrea, vSupervisionSubcomponentes.UsuarioModifica, vSupervisionSubcomponentes.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdSubcomponente = Convert.ToInt32(hfIdSubcomponente.Value);

            SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();
            vSupervisionSubcomponentes = vSupervisionService.ConsultarSupervisionSubcomponentes(vIdSubcomponente);
            InformacionAudioria(vSupervisionSubcomponentes, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionSubcomponentes(vSupervisionSubcomponentes);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionSubcomponentes.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Subcomponentes", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
