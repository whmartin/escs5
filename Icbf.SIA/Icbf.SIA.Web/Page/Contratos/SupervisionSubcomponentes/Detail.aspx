<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SupervisionSubcomponentes_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdSubcomponente" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Direccion *
            </td>
            <td>
                Subcomponente *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccion"  Enabled="false" Width="80%"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombre" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;0123456789" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
