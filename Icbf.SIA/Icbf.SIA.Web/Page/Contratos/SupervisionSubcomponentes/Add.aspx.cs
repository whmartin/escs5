using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionSubcomponentes_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionSubcomponentes";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();

            vSupervisionSubcomponentes.IdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            vSupervisionSubcomponentes.Nombre = Convert.ToString(txtNombre.Text);
            vSupervisionSubcomponentes.Estado = Convert.ToInt32(rblEstado.SelectedValue);
            int vResultExis = vSupervisionService.ConsultarSupervisionRespuestas(vSupervisionSubcomponentes.Nombre);

            if (Request.QueryString["oP"] == "E")
            {
                if (vResultExis == 0)
                {
                    vSupervisionSubcomponentes.IdSubcomponente = Convert.ToInt32(hfIdSubcomponente.Value);
                    vSupervisionSubcomponentes.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionSubcomponentes, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.ModificarSupervisionSubcomponentes(vSupervisionSubcomponentes);
                }
                else
                {
                    toolBar.MostrarMensajeError("El subcomponente ingresado ya existe");
                    vResultado = 3;
                }
            }
            else
            {
                if (vResultExis == 0)
                {
                    vSupervisionSubcomponentes.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionSubcomponentes, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.InsertarSupervisionSubcomponentes(vSupervisionSubcomponentes);
                }
                else
                {
                    toolBar.MostrarMensajeError("El subcomponente ingresado ya existe");
                    vResultado = 3;
                }
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SupervisionSubcomponentes.IdSubcomponente", vSupervisionSubcomponentes.IdSubcomponente);
                SetSessionParameter("SupervisionSubcomponentes.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Subcomponentes", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdSubcomponente = Convert.ToInt32(GetSessionParameter("SupervisionSubcomponentes.IdSubcomponente"));
            RemoveSessionParameter("SupervisionSubcomponentes.Id");

            SupervisionSubcomponentes vSupervisionSubcomponentes = new SupervisionSubcomponentes();
            vSupervisionSubcomponentes = vSupervisionService.ConsultarSupervisionSubcomponentes(vIdSubcomponente);
            hfIdSubcomponente.Value = vSupervisionSubcomponentes.IdSubcomponente.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionSubcomponentes.IdDireccion.ToString();
            txtNombre.Text = vSupervisionSubcomponentes.Nombre;
            rblEstado.SelectedValue = vSupervisionSubcomponentes.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionSubcomponentes.UsuarioCrea, vSupervisionSubcomponentes.FechaCrea, vSupervisionSubcomponentes.UsuarioModifica, vSupervisionSubcomponentes.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
