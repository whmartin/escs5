using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_SuscripcionConsModContractual_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SubscripcionModificacion";
    string TIPO_ESTRUCTURA = "ModificacionContractual";
    ContratoService vTipoSolicitudService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.MostrarBotonNuevo(false);
            toolBar.EstablecerTitulos("Suscripci&oacute;n de Modificaciones Contractuales", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            //ArchivosContrato.LoadInfoContratos(vIdContrato);

            if(!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualSubscripcion.Guardo").ToString()))
            toolBar.MostrarMensajeGuardado("La Solicitud ha sido suscrita correctamente.");

            RemoveSessionParameter("ConsModContractualSubscripcion.Guardo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfEstado.Value == "Aceptada")
            {
                if (IsValido())
                {
                    DateTime valor = DateTime.Parse(TxtFechaAprobacion.Text);
                    vTipoSolicitudService.SubscribirModificacionContractualContrato(Convert.ToInt32(hfIDCosModContractual.Value), GetSessionUser().NombreUsuario, valor);
                    SetSessionParameter("ConsModContractualSubscripcion.Guardo", "1");
                    SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value);
                    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value);
                    NavigateTo(SolutionPage.Detail);
                }
            }
            else
            {
                toolBar.MostrarMensajeError("No se puede Subscribir la Solicitud cuando su estado es diferente de Aceptada.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool IsValido()
    {
        bool isvalid = true;

        try
        {
            DateTime fechaAprobacion;

            if (DateTime.TryParse(TxtFechaAprobacion.Text, out fechaAprobacion))
            {
                if (fechaAprobacion.Year > 1900)
                {
                    var contrato = vTipoSolicitudService.ContratoObtener(int.Parse(hfIdTContrato.Value));

                    if (contrato.FechaFinalTerminacionContrato < fechaAprobacion && hfCodigoModificacion.Value != "TM7" && hfCodigoModificacion.Value != "TM10")
                    {
                        isvalid = false;
                        toolBar.MostrarMensajeError("La fecha de suscripción de la modificación es mayor a la fecha de terminación de contrato");
                    }
                    else if (contrato.FechaFinalTerminacionContrato > fechaAprobacion && hfCodigoModificacion.Value == "TM7")
                    {
                        isvalid = false;
                        toolBar.MostrarMensajeError("La fecha de suscripción de la Liquidación es menor a la fecha de terminación de contrato");
                    }
                    else if (hfCodigoModificacion.Value == "TM10" && DateTime.Parse(hfFechaSolicitud.Value) > fechaAprobacion)
                    {
                        isvalid = false;
                        toolBar.MostrarMensajeError("La fecha de suscripción del cambio de supervisor, debe ser mayor a la fecha de solicitud.");
                    }
                }
                else
                {
                    isvalid = false;
                    toolBar.MostrarMensajeError("La fecha de suscripción no tiene un formato correcto");
                }
            }
            else
            {
                isvalid = false;
                toolBar.MostrarMensajeError("La fecha de suscripción no tiene un formato correcto");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        return isvalid;
    }

    private void CargarDatos()
    {
        try
        {
            int vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");

            if (GetSessionParameter("ConsModContractualGestion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ConsModContractualGestion.Guardado");

            hfIDCosModContractual.Value = vIdConsModContractual.ToString();

            SolModContractual vSolContractual = new SolModContractual();
            vSolContractual = vTipoSolicitudService.ConsultarSolitud(vIdConsModContractual);

            txtJustificacion.Text = vSolContractual.Justificacion;

            hfIdTContrato.Value = vSolContractual.IdContrato.ToString();
            hfEstado.Value = vSolContractual.Estado;
            hfFechaSolicitud.Value = vSolContractual.FechaSolicitud.ToShortDateString();

            if (vSolContractual.Estado == "Aceptada")
            {
                    MultiViewAprobacion.ActiveViewIndex = 0;
                    toolBar.MostrarBotonAprobar(true);
            }
            else
            {
                toolBar.MostrarBotonAprobar(false);
                MultiViewAprobacion.ActiveViewIndex = -1;
            }
            
            var detalleSolicitud = vTipoSolicitudService.ConsultarSolicitudesDetalle(vIdConsModContractual);

            txtNumeroDoc.Text = vSolContractual.NumeroDoc;
            txtNumeroContrato.Text = vSolContractual.NumeroContrato ?? String.Empty;
            txtSolicitudMod.Text = vSolContractual.IDCosModContractual.ToString();
            txtTipoMod.Text = vSolContractual.TipoModificacion;
            hfCodigoModificacion.Value = vSolContractual.Codigo;
           
            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
            vTipoModificacionBasica = vTipoSolicitudService.consultartipomodificacion(vSolContractual.IDTipoModificacionContractual);

            CalendarExtenderFechaAprobacion.EndDate = DateTime.Now;
           
            var contrato = vTipoSolicitudService.ContratoObtener(vSolContractual.IdContrato);

            if ( vSolContractual.TipoModificacion != "Cambio de Supervisor" && vSolContractual.Codigo != "TM7")
                CalendarExtenderFechaAprobacion.EndDate = contrato.FechaFinalTerminacionContrato.Value;
           
            if (vSolContractual.Codigo == "TM7")
                CalendarExtenderFechaAprobacion.StartDate = contrato.FechaFinalTerminacionContrato.Value;
            else if (vSolContractual.TipoModificacion == "Cambio de Supervisor")
                CalendarExtenderFechaAprobacion.StartDate = vSolContractual.FechaSolicitud;
            else
                CalendarExtenderFechaAprobacion.StartDate = contrato.FechaInicioEjecucion;

            foreach (var itemDetalle in detalleSolicitud)
            {
                switch (itemDetalle.TipoModificacion)
                {
                    case "Adición":
                        AccTiposModificacion.Panes[0].Visible = true;
                        gvAdicionales.DataSource = vTipoSolicitudService.ConsultarAdicioness(null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvAdicionales.DataBind();
                        break;
                    case "Prorroga":
                        AccTiposModificacion.Panes[1].Visible = true;
                        gvProrrogas.DataSource = vTipoSolicitudService.ConsultarProrrogass(null, null, null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvProrrogas.DataBind();
                        break;
                    case "Modificación":
                        AccTiposModificacion.Panes[2].Visible = true;
                        gvModificaciones.DataSource = vTipoSolicitudService.ConsultarModificacionObligacionXIDDetalleConsModContractual(itemDetalle.IDDetalleConsModContractual);
                        gvModificaciones.DataBind();
                        break;
                    case "Suspensión":
                        AccTiposModificacion.Panes[5].Visible = true;
                        gvSuspensiones.DataSource = vTipoSolicitudService.ConsultarSuspensionesPorDetalleModificacion(itemDetalle.IDDetalleConsModContractual);
                        gvSuspensiones.DataBind();
                        break;
                    case "Reducción de Valor":
                        AccTiposModificacion.Panes[3].Visible = true;
                        gvReducciones.DataSource = vTipoSolicitudService.ConsultarReduccioness(null, null, itemDetalle.IDDetalleConsModContractual);
                        gvReducciones.DataBind();
                        break;
                    case "Cesión":
                        AccTiposModificacion.Panes[4].Visible = true;
                        gvCesiones.DataSource = vTipoSolicitudService.ConsultarCesioness(null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvCesiones.DataBind();
                        break;
                    case "Liquidación de Contrato":
                        AccTiposModificacion.Panes[6].Visible = true;
                        gvLiquidacionContrato.DataSource = vTipoSolicitudService.ConsultarLiquidacionIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvLiquidacionContrato.EmptyDataText = EmptyDataText();
                        gvLiquidacionContrato.PageSize = PageSize();
                        gvLiquidacionContrato.DataBind();
                        break;
                    case "Imposición de Multas":
                        AccTiposModificacion.Panes[7].Visible = true;
                        gvImposicionMultas.DataSource = vTipoSolicitudService.ConsultarImposicionMultaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvImposicionMultas.EmptyDataText = EmptyDataText();
                        gvImposicionMultas.PageSize = PageSize();
                        gvImposicionMultas.DataBind();
                        break;
                    case "Terminación Anticipada":
                        AccTiposModificacion.Panes[8].Visible = true;
                        gvTerminacionAnticipada.DataSource = vTipoSolicitudService.ConsultarTerminacionAnticipadaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvTerminacionAnticipada.EmptyDataText = EmptyDataText();
                        gvTerminacionAnticipada.PageSize = PageSize();
                        gvTerminacionAnticipada.DataBind();
                        break;
                    case "Cambio de Supervisor":
                        AccTiposModificacion.Panes[9].Visible = true;
                        List<SupervisorInterContrato> supervisoresInterventores = vTipoSolicitudService.ObtenerSupervisoresInterventoresContrato(vSolContractual.IdContrato, null);
                        if (supervisoresInterventores != null)
                        {
                            var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                            gvSupervisoresActuales.DataSource = querySupervisores;
                            gvSupervisoresActuales.DataBind();
                        }
                        List<SupervisorInterContrato> supervisoresTemporales = vTipoSolicitudService.ObtenerSupervisoresTemporalesContrato(vSolContractual.IdContrato,null);
                            gvSupervInterv.DataSource = supervisoresTemporales;
                            gvSupervInterv.DataBind();

                        break;
                }
            }

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vTipoSolicitudService.ConsultarArchivoTipoEstructurayContrato(vSolContractual.IdContrato, TIPO_ESTRUCTURA, vIdConsModContractual);
            gvanexos.DataBind();

            ObtenerAuditoria(PageName, hfIDCosModContractual.Value);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnCreateDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void btnSelectDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void btnDeleteDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void btnDeleteOtroDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void gvAdicionales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvAdicionales.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvAdicionales.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Adiciones.IdAdiccion", strValue);
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            NavigateTo("~/Page/Contratos/Adiciones/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvProrrogas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvProrrogas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvProrrogas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            NavigateTo("~/Page/Contratos/Prorrogas/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModificaciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvModificaciones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvModificaciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("ModificacionObligacion.IdModObligacion", strValue);
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            NavigateTo("~/Page/Contratos/ModificacionObligacion/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvReducciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReducciones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReducciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Reducciones.IdReduccion", strValue);
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            NavigateTo("~/Page/Contratos/ReduccionesValor/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCesiones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvCesiones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvCesiones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Cesiones.IdCesion", strValue);
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            NavigateTo("~/Page/Contratos/Cesiones/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSuspensiones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvSuspensiones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvSuspensiones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Suspensiones.IdSuspension", strValue);
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            NavigateTo("~/Page/Contratos/Suspenciones/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvLiquidacionContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvLiquidacionContrato.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvLiquidacionContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdLiquidacionContrato", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            Response.Redirect("../LiquidacionContrato/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvImpsicionMultas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvImposicionMultas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvImposicionMultas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdImposicionMultas", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            Response.Redirect("../ProcesoImposicionMulta/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTerminacionAnticipada_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvTerminacionAnticipada.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvTerminacionAnticipada.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdTerminacionAnticipada", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            Response.Redirect("../TerminacionAnticipada/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsSubscripcion", true);
            Response.Redirect("../CambioSupervision/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            toolBar.LipiarMensajeError();
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        toolBar.LipiarMensajeError();
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            string strTipoDocumento = gvanexos.DataKeys[rowIndex].Values[1] != null ? gvanexos.DataKeys[rowIndex].Values[1].ToString() : string.Empty;
            indice = Convert.ToInt64(strIDCosModContractual);
            int idConsMod = Convert.ToInt32(hfIDCosModContractual.Value);
            if (indice != 0)
            {
                int vIdContrato = Convert.ToInt32(hfIdTContrato.Value);
                    vResultado = vTipoSolicitudService.EliminarDocumentoAnexoContrato(indice);
                    gvanexos.DataSource = vTipoSolicitudService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA,idConsMod);
                    gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        int idContrato = Convert.ToInt32(hfIdTContrato.Value);
        int idConsMod = Convert.ToInt32(hfIDCosModContractual.Value);
        gvanexos.DataSource = vTipoSolicitudService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA, idConsMod);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdTContrato.Value);
        int idConsMod = Convert.ToInt32(hfIDCosModContractual.Value);

            FileUpload fuArchivo = FileUploadArchivoContrato;

            if (fuArchivo.HasFile)
            {
                try
                {
                        ManejoControlesContratos controles = new ManejoControlesContratos();
                        controles.CargarArchivoFTPModificacionContractual
                            (
                             TIPO_ESTRUCTURA,
                             fuArchivo,
                             idContrato,
                             idConsMod,
                             GetSessionUser().IdUsuario
                            );

                        gvanexos.DataSource = vTipoSolicitudService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA, idConsMod);
                        gvanexos.DataBind();
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
            }
            else
                toolBar.MostrarMensajeError("Debe Seleccionar un Documento a Cargar");
    }

    #endregion
}
