<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SuscripcionConsModContractual_Detail" %>
<%@ Register Src="~/General/General/Control/ArchivosContrato.ascx" TagPrefix="uc1" TagName="ArchivosContrato" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDCosModContractual" runat="server" />
    <asp:HiddenField ID="hfEstado" runat="server" />
    <asp:HiddenField ID="hfIdTContrato" runat="server" />
    <asp:HiddenField ID="hfCodigoModificacion" runat="server" />
    <asp:HiddenField ID="hfFechaSolicitud" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="3">
            <asp:MultiView  ID="MultiViewAprobacion" runat="server">
                <asp:View ID="ViewAprobacion" runat="server">
                    <table width="100%">
                        <tr class="rowAG">
                            <td>Fecha Suscripción

                                <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="TxtFechaAprobacion" Display="Dynamic" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" style="font-weight: 700" ValidationGroup="btnAprobar"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="rowB">
                            <td>
                                <asp:TextBox ID="TxtFechaAprobacion" Width="150px" runat="server"></asp:TextBox>
                                                <Ajax:CalendarExtender ID="CalendarExtenderFechaAprobacion" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaAprobacion" TargetControlID="TxtFechaAprobacion">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="TxtFechaAprobacion">
                </Ajax:MaskedEditExtender>
                                &nbsp;<asp:Image ID="imgFechaAprobacion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número del Contrato/Convenio
            </td>
            <td>
                N&uacute;mero de Documento
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style2">
                <asp:TextBox runat="server" ID="txtNumeroContrato"  Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style2">
                <asp:TextBox runat="server" ID="txtNumeroDoc"  Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Modificaci&oacute;n
            </td>
            <td>
                Solicitud de Modificaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtTipoMod" Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtSolicitudMod" Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Justificaci&oacute;n
            </td>
            <td class="auto-style2">
               </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                <asp:TextBox runat="server" ID="txtJustificacion" Width="90%" TextMode="MultiLine" Enabled="false" Height="79px"></asp:TextBox>
            </td>
            <td class="auto-style2">
                &nbsp;</td>
        </tr>
        </table>
    <table width="90%" align="center">
         <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                Subir</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
            </td>
            <td>
                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"  ValidationGroup="CargarArchivoFTP"  OnClick="CargarArchivoFTP" />
                </td>
        </tr>
            <tr class="rowAG">
                    <td colspan="2">
                        <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo,NombreTabla" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" Visible="false" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:BoundField HeaderText="Tipo del Documento" DataField="NombreTabla" />                                                                                                        
                                                   
                                                    <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" OnClientClick="if ( ! ConfirmacionEliminar()) return false;" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <Ajax:Accordion ID="AccTiposModificacion" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                        ContentCssClass="accordionContent" runat="server"
                        Width="100%" Height="100%">
                        <Panes>
                            <Ajax:AccordionPane ID="ApAdiccion" Visible="false" runat="server">
                                <Header>
                                    Adici&oacute;n
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel1">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvAdicionales" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdAdicion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvAdicionales_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor de la Adci&#243;n" DataField="ValorAdicion" />
<%--                                                            <asp:BoundField HeaderText="Justificaci&#243;n de la Adici&#243;n Superior al 50%" DataField="JustificacionAdicion" />--%>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApProrroga" Visible="false" runat="server">
                                <Header>
                                    Pr&oacute;rroga
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel2">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvProrrogas" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdProrroga" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvProrrogas_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Inicio Pr&#243;rroga" DataField="FechaInicio" DataFormatString="{0:dd-MM-yyyy}"/>
                                                            <asp:BoundField HeaderText="Fecha de Finalizaci&#243;n Pr&#243;rroga" DataField="FechaFin" DataFormatString="{0:dd-MM-yyyy}"/>
                                                            <asp:BoundField HeaderText="Pr&#243;rroga Dias" DataField="Dias" />
                                                            <asp:BoundField HeaderText="Pr&#243;rroga Meses" DataField="Meses" />
                                                            <asp:BoundField HeaderText="Pr&#243;rroga Años" DataField="Años" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApModificacionesContractuales" Visible="false" runat="server">
                                <Header>
                                    Modificaciones Contractuales
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel3">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvModificaciones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdModObligacion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvModificaciones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Clausula del Contrato" DataField="ClausulaContrato" />
                                                            <asp:BoundField HeaderText="Descripción de Modificaci&#243;n" DataField="DescripcionModificacion" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApReduccionesValor" Visible="false" runat="server">
                                <Header>
                                    Reducci&oacute;n en Valor
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel4">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvReducciones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdReduccion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvReducciones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Reducci&#243;n" DataField="FechaReduccion" />
                                                            <asp:BoundField HeaderText="Valor Total de Reducci&#243;n" DataField="ValorReduccion" />
                                                            <asp:BoundField HeaderText="Justificaci&#243;n de la Reducci&#243;n" DataField="Justificacion" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApCesion" Visible="false" runat="server">
                                <Header>
                                    Cesi&oacute;n
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel5">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvCesiones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdCesion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvCesiones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha de Cesi&#243;n" DataField="FechaCesion" />
                                                                    <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor Cesi&#243;n" DataField="ValorCesion" />
                                                                    <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor Ejecutado" DataField="ValorEjecutado" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApSuspencion" Visible="false" runat="server">
                                <Header>
                                    Suspenci&oacute;n
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel6">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvSuspensiones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdSuspension" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvSuspensiones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Inicio Suspensión" DataField="FechaInicio" />
                                                            <asp:BoundField HeaderText="Fecha Terminación Suspensión" DataField="FechaFin" />
                                                            <asp:BoundField HeaderText="Valor Descuento" DataField="ValorSuspencion" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                                                    <Ajax:AccordionPane ID="ApLiquidacionContrato" Visible="false" runat="server">
                            <Header>
                                Liquidac&oacute;n de contrato
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel7">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                <asp:GridView runat="server" ID="gvLiquidacionContrato" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvLiquidacionContrato_SelectedIndexChanged" DataKeyNames="IdLiquidacionContrato" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Radicaci&oacute;n" DataField="FechaRadicacionView" />
                                                                 <asp:BoundField HeaderText="Dependencia" DataField="DependenciaSueprvisor" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApImposicionMultas" Visible="false" runat="server">
                            <Header>
                                Imposici&oacute;n de multas
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel8">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                 <asp:GridView runat="server" ID="gvImposicionMultas" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvImpsicionMultas_SelectedIndexChanged" DataKeyNames="IdImposicionMulta" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Razón" DataField="Razones" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApTerminacionAnticipada" Visible="false" runat="server">
                            <Header>
                                Terminaci&oacute;n anticipada
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel9">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                  <asp:GridView runat="server" ID="gvTerminacionAnticipada" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvTerminacionAnticipada_SelectedIndexChanged" DataKeyNames="IdTerminacionAnticipada" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Radicación" DataField="FechaRadicacionView" />
                                                                <asp:BoundField HeaderText="No de Radicación" DataField="NumeroRadicacion" />
                                                                <asp:BoundField HeaderText="Fecha de Terminación Anticipada" DataField="FechaTerminacionAnticipadaView" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>   
                                                                        <Ajax:AccordionPane ID="ApCambioSupervisor" Visible="false" runat="server">
                            <Header>
                                Cambio de Supervisor de Contrato
                            </Header>
                            <Content>
                                <asp:Panel runat="server" ID="Panel10">
                                    <table width="90%" align="center">
                                     <tr class="rowAG">
                                         <td>
                                          Detalle:
                                         </td>
                                         <td>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" OnClick="btnInfo_Click" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />      
                                         </td>
                                     </tr>
                                      <table width="90%" align="center">
                                            <tr class="rowBG">
            <td colspan="4">
            <table align="center" width="100%">
                <tr class="rowB">
                    <td class="Cell">Supervisores relacionados Actuales </td>
                </tr>
                  <tr class="rowA">
                        <td class="Cell">
                            <div id="Div1" runat="server">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                                        <asp:BoundField DataField="FechaInicioString" HeaderText="Fecha Inicio" />   
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
            </table>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="4">
                                <asp:MultiView ID="MultiViewSupervisores" ActiveViewIndex="0" runat="server">
                    <asp:View ID="ViewSupervisores" runat="server">
             <table align="center" width="100%">
                    <tr class="rowB">
                        <td class="Cell">Supervisores relacionados Modificación</td>
                        <td class="Cell">
                            &nbsp;</td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell" colspan="2">
                            <div id="dvSupervInterv" runat="server">
                                <asp:GridView ID="gvSupervInterv" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px">
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                                        <asp:BoundField DataField="FechaInicioString" HeaderText="Fecha Inicio" />   
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                        </asp:View>
                                    </asp:MultiView>
            </td>
        </tr> 
                                    </table>
                                </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>                           
                        </Panes>
                </Ajax:Accordion>     
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 38px;
        }
        .auto-style2 {
            height: 26px;
        }
        </style>
</asp:Content>




