using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_ModificacionGarantia_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ModificacionGarantia";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdContrato = null;
            int? vNumeroDocumento = null;
            DateTime? vFechaRegistro = null;
            String vTipoModificacion = null;
            String vNumeroGarantia = null;
            int? vIdAmparo = null;
            String vTipoAmparo = null;
            DateTime? vFechaVigenciaDesde = null;
            DateTime? vFechaVigenciaHasta = null;
            int? vCalculoValorAsegurado = null;
            String vTipoCalculo = null;
            int? vValorAdicion = null;
            int? vValorTotalReduccion = null;
            int? vValorAsegurado = null;
            String vObservacionesModificacion = null;
            if (txtIdContrato.Text!= "")
            {
                vIdContrato = Convert.ToInt32(txtIdContrato.Text);
            }
            if (txtNumeroDocumento.Text!= "")
            {
                vNumeroDocumento = Convert.ToInt32(txtNumeroDocumento.Text);
            }
            if (txtFechaRegistro.Text!= "")
            {
                vFechaRegistro = Convert.ToDateTime(txtFechaRegistro.Text);
            }
            if (txtTipoModificacion.Text!= "")
            {
                vTipoModificacion = Convert.ToString(txtTipoModificacion.Text);
            }
            
            gvModificacionGarantia.DataSource = vContratoService.ConsultarModificacionGarantias( vIdContrato, vNumeroDocumento, vFechaRegistro, vTipoModificacion, vNumeroGarantia, vIdAmparo, vTipoAmparo, vFechaVigenciaDesde, vFechaVigenciaHasta, vCalculoValorAsegurado, vTipoCalculo, vValorAdicion, vValorTotalReduccion, vValorAsegurado, vObservacionesModificacion);
            gvModificacionGarantia.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvModificacionGarantia.PageSize = PageSize();
            gvModificacionGarantia.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("ModificacionGarantia", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvModificacionGarantia.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ModificacionGarantia.IdModificacionGarantia", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModificacionGarantia_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvModificacionGarantia.SelectedRow);
    }
    protected void gvModificacionGarantia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModificacionGarantia.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ModificacionGarantia.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ModificacionGarantia.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
