using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_ModificacionGarantia_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ModificacionGarantia";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ModificacionGarantia.IdModificacionGarantia", hfIdModificacionGarantia.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdModificacionGarantia = Convert.ToInt32(GetSessionParameter("ModificacionGarantia.IdModificacionGarantia"));
            RemoveSessionParameter("ModificacionGarantia.IdModificacionGarantia");

            if (GetSessionParameter("ModificacionGarantia.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ModificacionGarantia");


            ModificacionGarantia vModificacionGarantia = new ModificacionGarantia();
            vModificacionGarantia = vContratoService.ConsultarModificacionGarantia(vIdModificacionGarantia);
            hfIdModificacionGarantia.Value = vModificacionGarantia.IdModificacionGarantia.ToString();
            txtIdContrato.Text = vModificacionGarantia.IdContrato.ToString();
            txtNumeroDocumento.Text = vModificacionGarantia.NumeroDocumento.ToString();
            txtFechaRegistro.Text = vModificacionGarantia.FechaRegistro.ToString();
            txtTipoModificacion.Text = vModificacionGarantia.TipoModificacion;
            txtNumeroGarantia.Text = vModificacionGarantia.NumeroGarantia;
            txtIdAmparo.Text = vModificacionGarantia.IdAmparo.ToString();
            txtTipoAmparo.Text = vModificacionGarantia.TipoAmparo;
            txtFechaVigenciaDesde.Text = vModificacionGarantia.FechaVigenciaDesde.ToString();
            txtFechaVigenciaHasta.Text = vModificacionGarantia.FechaVigenciaHasta.ToString();
            txtCalculoValorAsegurado.Text = vModificacionGarantia.CalculoValorAsegurado.ToString();
            txtTipoCalculo.Text = vModificacionGarantia.TipoCalculo;
            txtValorAdicion.Text = vModificacionGarantia.ValorAdicion.ToString();
            txtValorTotalReduccion.Text = vModificacionGarantia.ValorTotalReduccion.ToString();
            txtValorAsegurado.Text = vModificacionGarantia.ValorAsegurado.ToString();
            txtObservacionesModificacion.Text = vModificacionGarantia.ObservacionesModificacion;
            ObtenerAuditoria(PageName, hfIdModificacionGarantia.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModificacionGarantia.UsuarioCrea, vModificacionGarantia.FechaCrea, vModificacionGarantia.UsuarioModifica, vModificacionGarantia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdModificacionGarantia = Convert.ToInt32(hfIdModificacionGarantia.Value);

            ModificacionGarantia vModificacionGarantia = new ModificacionGarantia();
            vModificacionGarantia = vContratoService.ConsultarModificacionGarantia(vIdModificacionGarantia);
            InformacionAudioria(vModificacionGarantia, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarModificacionGarantia(vModificacionGarantia);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ModificacionGarantia.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("ModificacionGarantia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
