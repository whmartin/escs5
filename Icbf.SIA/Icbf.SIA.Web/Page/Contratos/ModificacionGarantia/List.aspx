<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ModificacionGarantia_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número del Contrato *
            </td>
            <td>
                Número del Documento *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroDocumento"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Registro Modificación *
            </td>
            <td>
                Tipo Modificación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistro"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoModificacion"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel1">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="GridView1" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdModificacionGarantia" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvModificacionGarantia_PageIndexChanging" OnSelectedIndexChanged="gvModificacionGarantia_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número de Garantía" DataField="NumeroGarantia" />
                            <asp:BoundField HeaderText="Tipo Amparo" DataField="TipoAmparo" />
                            <asp:BoundField HeaderText="Fecha Vigencia Desde" DataField="FechaVigenciaDesde" />
                            <asp:BoundField HeaderText="Valor Asegurado" DataField="ValorAsegurado" />
                            <asp:TemplateField HeaderText="Acciones">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/list.png"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnAdd" runat="server" CommandName="Add" ImageUrl="~/Image/btn/add.gif"
                                        Height="16px" Width="16px" ToolTip="Adicionar"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="~/Image/btn/edit.gif"
                                        Height="16px" Width="16px" ToolTip="Editar"/>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Opciones
                                </HeaderTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvModificacionGarantia" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdModificacionGarantia" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvModificacionGarantia_PageIndexChanging" OnSelectedIndexChanged="gvModificacionGarantia_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número de Garantía" DataField="NumeroGarantia" />
                            <asp:BoundField HeaderText="Id Amparo" DataField="IdAmparo" />
                            <asp:BoundField HeaderText="Tipo Amparo" DataField="TipoAmparo" />
                            <asp:BoundField HeaderText="Fecha Vigencia Desde" DataField="FechaVigenciaDesde" />
                            <asp:BoundField HeaderText="Vigencia Hasta" DataField="FechaVigenciaHasta" />
                            <asp:BoundField HeaderText="Valor Asegurado" DataField="ValorAsegurado" />
                            <asp:TemplateField HeaderText="Acciones">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="True"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
