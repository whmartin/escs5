<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ModificacionGarantia_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdModificacionGarantia" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número del Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtIdContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Número del Documento *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroDocumento" ControlToValidate="txtNumeroDocumento"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroDocumento"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroDocumento" runat="server" TargetControlID="txtNumeroDocumento"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Registro Modificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaRegistro" ControlToValidate="txtFechaRegistro"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo Modificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoModificacion" ControlToValidate="txtTipoModificacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistro"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoModificacion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoModificacion" runat="server" TargetControlID="txtTipoModificacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Garantía
            </td>
            <td>
                Id Amparo
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroGarantia"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroGarantia" runat="server" TargetControlID="txtNumeroGarantia"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdAmparo"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdAmparo" runat="server" TargetControlID="txtIdAmparo"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Amparo *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoAmparo" ControlToValidate="txtTipoAmparo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Fecha Vigencia Desde *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaVigenciaDesde" ControlToValidate="txtFechaVigenciaDesde"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoAmparo"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoAmparo" runat="server" TargetControlID="txtTipoAmparo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaVigenciaDesde"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia Hasta *
                <asp:RequiredFieldValidator runat="server" ID="rfvFechaVigenciaHasta" ControlToValidate="txtFechaVigenciaHasta"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Valor Para Calculo Asegurado *
                <asp:RequiredFieldValidator runat="server" ID="rfvCalculoValorAsegurado" ControlToValidate="txtCalculoValorAsegurado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaVigenciaHasta"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCalculoValorAsegurado"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCalculoValorAsegurado" runat="server" TargetControlID="txtCalculoValorAsegurado"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo de Calculo *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoCalculo" ControlToValidate="txtTipoCalculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Valor Adición (Contrato) *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorAdicion" ControlToValidate="txtValorAdicion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoCalculo"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoCalculo" runat="server" TargetControlID="txtTipoCalculo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAdicion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAdicion" runat="server" TargetControlID="txtValorAdicion"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total de Reducción (Contrato) *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorTotalReduccion" ControlToValidate="txtValorTotalReduccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Valor Asegurado *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorAsegurado" ControlToValidate="txtValorAsegurado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalReduccion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorTotalReduccion" runat="server" TargetControlID="txtValorTotalReduccion"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAsegurado"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAsegurado" runat="server" TargetControlID="txtValorAsegurado"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Observaciones Modificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvObservacionesModificacion" ControlToValidate="txtObservacionesModificacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtObservacionesModificacion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObservacionesModificacion" runat="server" TargetControlID="txtObservacionesModificacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
    </table>
</asp:Content>
