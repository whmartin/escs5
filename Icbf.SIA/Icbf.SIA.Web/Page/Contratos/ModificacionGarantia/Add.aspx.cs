using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_ModificacionGarantia_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/ModificacionGarantia";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            ModificacionGarantia vModificacionGarantia = new ModificacionGarantia();

            vModificacionGarantia.IdContrato = Convert.ToInt32(txtIdContrato.Text);
            vModificacionGarantia.NumeroDocumento = Convert.ToInt32(txtNumeroDocumento.Text);
            vModificacionGarantia.FechaRegistro = Convert.ToDateTime(txtFechaRegistro.Text);
            vModificacionGarantia.TipoModificacion = Convert.ToString(txtTipoModificacion.Text);
            vModificacionGarantia.NumeroGarantia = Convert.ToString(txtNumeroGarantia.Text);
            vModificacionGarantia.IdAmparo = Convert.ToInt32(txtIdAmparo.Text);
            vModificacionGarantia.TipoAmparo = Convert.ToString(txtTipoAmparo.Text);
            vModificacionGarantia.FechaVigenciaDesde = Convert.ToDateTime(txtFechaVigenciaDesde.Text);
            vModificacionGarantia.FechaVigenciaHasta = Convert.ToDateTime(txtFechaVigenciaHasta.Text);
            vModificacionGarantia.CalculoValorAsegurado = Convert.ToInt32(txtCalculoValorAsegurado.Text);
            vModificacionGarantia.TipoCalculo = Convert.ToString(txtTipoCalculo.Text);
            vModificacionGarantia.ValorAdicion = Convert.ToInt32(txtValorAdicion.Text);
            vModificacionGarantia.ValorTotalReduccion = Convert.ToInt32(txtValorTotalReduccion.Text);
            vModificacionGarantia.ValorAsegurado = Convert.ToInt32(txtValorAsegurado.Text);
            vModificacionGarantia.ObservacionesModificacion = Convert.ToString(txtObservacionesModificacion.Text);

            if (Request.QueryString["oP"] == "E")
            {
            vModificacionGarantia.IdModificacionGarantia = Convert.ToInt32(hfIdModificacionGarantia.Value);
                vModificacionGarantia.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vModificacionGarantia, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarModificacionGarantia(vModificacionGarantia);
            }
            else
            {
                vModificacionGarantia.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vModificacionGarantia, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarModificacionGarantia(vModificacionGarantia);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ModificacionGarantia.IdModificacionGarantia", vModificacionGarantia.IdModificacionGarantia);
                SetSessionParameter("ModificacionGarantia.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("ModificacionGarantia", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdModificacionGarantia = Convert.ToInt32(GetSessionParameter("ModificacionGarantia.IdModificacionGarantia"));
            RemoveSessionParameter("ModificacionGarantia.Id");

            ModificacionGarantia vModificacionGarantia = new ModificacionGarantia();
            vModificacionGarantia = vContratoService.ConsultarModificacionGarantia(vIdModificacionGarantia);
            hfIdModificacionGarantia.Value = vModificacionGarantia.IdModificacionGarantia.ToString();
            txtIdContrato.Text = vModificacionGarantia.IdContrato.ToString();
            txtNumeroDocumento.Text = vModificacionGarantia.NumeroDocumento.ToString();
            txtFechaRegistro.Text = vModificacionGarantia.FechaRegistro.ToString();
            txtTipoModificacion.Text = vModificacionGarantia.TipoModificacion;
            txtNumeroGarantia.Text = vModificacionGarantia.NumeroGarantia;
            txtIdAmparo.Text = vModificacionGarantia.IdAmparo.ToString();
            txtTipoAmparo.Text = vModificacionGarantia.TipoAmparo;
            txtFechaVigenciaDesde.Text = vModificacionGarantia.FechaVigenciaDesde.ToString();
            txtFechaVigenciaHasta.Text = vModificacionGarantia.FechaVigenciaHasta.ToString();
            txtCalculoValorAsegurado.Text = vModificacionGarantia.CalculoValorAsegurado.ToString();
            txtTipoCalculo.Text = vModificacionGarantia.TipoCalculo;
            txtValorAdicion.Text = vModificacionGarantia.ValorAdicion.ToString();
            txtValorTotalReduccion.Text = vModificacionGarantia.ValorTotalReduccion.ToString();
            txtValorAsegurado.Text = vModificacionGarantia.ValorAsegurado.ToString();
            txtObservacionesModificacion.Text = vModificacionGarantia.ObservacionesModificacion;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModificacionGarantia.UsuarioCrea, vModificacionGarantia.FechaCrea, vModificacionGarantia.UsuarioModifica, vModificacionGarantia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
