using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de consulta a través de filtros para la entidad AsociarRPContrato
/// </summary>
public partial class Page_AsociarRPContrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/AsociarRPContrato";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvAsociarRPContrato, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            gvAsociarRPContrato.PageSize = PageSize();
            gvAsociarRPContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Asociar el Registro presupuestal a un Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../General/Inicio/Default.aspx");
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvAsociarRPContrato.DataKeys[rowIndex].Values["IdContrato"].ToString();
            string strVigencia = gvAsociarRPContrato.DataKeys[rowIndex].Values["IdVigenciaInicial"].ToString();
            string strIdRegional = gvAsociarRPContrato.DataKeys[rowIndex].Values["IdRegionalContrato"].ToString();
            SetSessionParameter("Contrato.IdContrato", strValue);
            SetSessionParameter("ContratoRP.IdVigenciaFiscal", strVigencia);
            SetSessionParameter("ContratoRP.IdRegionalContrato", strIdRegional);
            SetSessionParameter("EsContratoRP", "1");
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAsociarRPContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvAsociarRPContrato.SelectedRow);
    }
    protected void gvAsociarRPContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAsociarRPContrato.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvAsociarRPContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int? vIDRegional = null;
            int? vVigenciaFiscalinicial = null;
            String vNumeroContrato = null;
            String vDependenciaSolicitante = null;
            int? vIdDependenciaSolicitante = null;
            int? vIdContrato = null;
            int? vIDTipoContrato = null;
            if (ddlIDRegional.SelectedValue != "-1")
            {
                vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            }
            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
            {
                vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            }
            if (txtNumeroContrato.Text != "")
            {
                vNumeroContrato = txtNumeroContrato.Text;
            }
            //if (txtDependenciaSolicitante.Text!= "")
            //{
            //    vDependenciaSolicitante = Convert.ToString(txtDependenciaSolicitante.Text);
            //}
            if (ddlDependenciaSolicitante.SelectedValue != "-1")
            {
                vIdDependenciaSolicitante = Convert.ToInt32(ddlDependenciaSolicitante.SelectedValue);
            }
            if (ddlIDTipoContrato.SelectedValue != "-1")
            {
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
            }

            List<EstadoContrato> vEstadoContrato = new List<EstadoContrato>();

            vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "SUS", null, true);
            if (vEstadoContrato.Count == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }


            var myGridResults = vContratoService.ConsultarContratoss(null, null, null, vNumeroContrato, null, vVigenciaFiscalinicial, vIDRegional, null, null, vIDTipoContrato, vEstadoContrato[0].IdEstadoContrato, vIdDependenciaSolicitante, true,null,null,null,null);
            //var myGridResults = vContratoService.ConsultarAsociarRPContratos( vIDRegional, vVigenciaFiscalinicial, vNumeroContrato, vDependenciaSolicitante, vIDTipoContrato);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Contrato), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Contrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("AsociarRPContrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("AsociarRPContrato.Eliminado");

            CargarListaVigencia();
            CargarListaRegional();
            CargarListaTipoContrato();
            CargarListaDependenciaSolicitante();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }
    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                Regional vRegional = new Regional();
                vRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(vRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
            }

        }

    }

    public void CargarListaTipoContrato()
    {
        ddlIDTipoContrato.Items.Clear();
        List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, null, null, null, null, null, null, null);
        foreach (TipoContrato tD in vLTiposContratos)
        {
            ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
        }
        ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        ddlIDTipoContrato.Enabled = true;

    }

    public void CargarListaDependenciaSolicitante()
    {

        ddlDependenciaSolicitante.Items.Clear();
        List<DependenciaSolicitante> vLDependenciaSolicitante = vContratoService.ConsultarDependenciaSolicitante(null, null,null);
        foreach (DependenciaSolicitante tD in vLDependenciaSolicitante)
        {
            ddlDependenciaSolicitante.Items.Add(new ListItem(tD.NombreDependenciaSolicitante, tD.IdDependenciaSolicitante.ToString()));
        }
        ddlDependenciaSolicitante.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        ddlDependenciaSolicitante.Enabled = true;

    }
}
