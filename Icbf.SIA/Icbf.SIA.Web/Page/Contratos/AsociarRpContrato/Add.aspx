<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_AsociarRPContrato_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Regional del Contrato/Convenio
            </td>
            <td>
                Vigencia Fiscal Inicial
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDRegional"></asp:DropDownList>
                <img id="imghIDRegional" alt="help" title="&gt;Corresponde al listado de regionales asociadas al contrato." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIDRegional')" onmouseout="helpOut('imghIDRegional')"/>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"></asp:DropDownList>
                <img id="imghVigenciaFiscalinicial" alt="help" title="Es el id de la vigencia fiscal inicial." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghVigenciaFiscalinicial')" onmouseout="helpOut('imghVigenciaFiscalinicial')"/>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Contrato
            </td>
            <td>
                Dependencia Solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <img id="imghNumeroContrato" alt="help" title="Es el n&#250;mero del contrato." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghNumeroContrato')" onmouseout="helpOut('imghNumeroContrato')"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDependenciaSolicitante" MaxLength="256" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDependenciaSolicitante" runat="server" TargetControlID="txtDependenciaSolicitante"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <img id="imghDependenciaSolicitante" alt="help" title="Es la dependencia del solicitante." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghDependenciaSolicitante')" onmouseout="helpOut('imghDependenciaSolicitante')"/>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Tipo de Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"></asp:DropDownList>
                <img id="imghIDTipoContrato" alt="help" title="Corresponde al tipo de contrato." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIDTipoContrato')" onmouseout="helpOut('imghIDTipoContrato')"/>
            </td>
        </tr>
    </table>
</asp:Content>
