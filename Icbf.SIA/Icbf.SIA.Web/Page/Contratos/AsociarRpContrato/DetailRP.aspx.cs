﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using EstadoContrato = Icbf.Contrato.Entity.EstadoContrato;

public partial class Page_Contratos_AsociarRpContrato_DetailRP : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/AsociarRPContrato";
    ContratoService vContratoService = new ContratoService();
    private SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            CargarRegistro();
            VerficarQueryStrings();
        }

    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            if(GetSessionParameter("Contrato.RPEnLinea").ToString() != "")
            {
                ddlRegionalICBF.SelectedValue = GetSessionParameter("Contrato.RPRegional").ToString();
                decimal valorRP = decimal.Parse(GetSessionParameter("Contrato.RPvalorInicial").ToString());
                txtValorRP.Text = valorRP.ToString("$ #,###0.00##;($ #,###0.00##)");
                txtValorActualRP.Text = valorRP.ToString("$ #,###0.00##;($ #,###0.00##)");
                DateTime fechaRP = Convert.ToDateTime(GetSessionParameter("Contrato.RPfechaRP"));
                txtFechaRp.Text = fechaRP.ToShortDateString();
                ddlVigenciaFiscal.SelectedValue = ddlVigenciaFiscal.Items.FindByText(fechaRP.Year.ToString()).Value;
                txtNumeroRP.Text =GetSessionParameter("Contrato.RP").ToString();
                hfValorActualRP.Value = valorRP.ToString();
                hfValorInicialRP.Value = valorRP.ToString();
                hfNumeroRP.Value = "-1";
                RemoveSessionParameter("Contrato.RPEnLinea");
                RemoveSessionParameter("Contrato.RPRegional");
                RemoveSessionParameter("Contrato.RPvalorInicial");
                RemoveSessionParameter("Contrato.RPfechaRP");
                RemoveSessionParameter("Contrato.RP");
            }
            else
            {
                int vIdNumeroRP = Convert.ToInt32(GetSessionParameter("Contrato.NumeroRP"));
                RemoveSessionParameter("Contrato.NumeroRP");

                RPContrato lRPcontato = new RPContrato();
                lRPcontato = vContratoService.ConsultarRPPorId(vIdNumeroRP);
                if(lRPcontato != null)
                {
                    ddlRegionalICBF.SelectedValue = lRPcontato.IdRegional.ToString();
                    ddlVigenciaFiscal.SelectedValue = ddlVigenciaFiscal.Items.FindByText(lRPcontato.VigenciaFiscal.ToString()).Value;
                    txtValorRP.Text = lRPcontato.ValorInicialRP.ToString("$ #,###0.00##;($ #,###0.00##)");
                    txtValorActualRP.Text = lRPcontato.ValorActualRP.ToString("$ #,###0.00##;($ #,###0.00##)");
                    txtFechaRp.Text = Convert.ToDateTime(lRPcontato.FechaRP).ToShortDateString();
                    hfNumeroRP.Value = lRPcontato.IdRP.ToString();
                    txtNumeroRP.Text = lRPcontato.NumeroRP.ToString();
                    hfValorActualRP.Value = lRPcontato.ValorActualRP.ToString();
                    hfValorInicialRP.Value = lRPcontato.ValorInicialRP.ToString();
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            if (Request.QueryString["esAdicion"] != null)
            {
                hfEsAdicion.Value = Request.QueryString["esAdicion"].ToString();
                toolBar.EstablecerTitulos("Asociar el Registro presupuestal a una adición", SolutionPage.Detail.ToString());
            }
            else if (Request.QueryString["esCesion"] != null)
            {
                hfEsCesion.Value = Request.QueryString["esCesion"].ToString();
                toolBar.EstablecerTitulos("Asociar el Registro presupuestal a una cesión", SolutionPage.Detail.ToString());
            }
            else if (Request.QueryString["vesVigenciaFutura"] != null)
            {
                hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"].ToString();
                toolBar.EstablecerTitulos("Asociar el Registro presupuestal a una Vigencia Futura", SolutionPage.Detail.ToString());
            }
            else
            {
                toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
                toolBar.EstablecerTitulos("Asociar el Registro presupuestal a un Contrato", SolutionPage.Detail.ToString());
            }

            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        //EliminarRegistro();
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (hfEsAdicion.Value == "1")
            Response.Redirect("../Lupas/LupaListRP.aspx");
        else if(! string.IsNullOrEmpty(hfEsCesion.Value))
        {
            string uri = string.Format("../Lupas/LupaListRP.aspx?esCesion={0}&IdRegContrato={1}&IdVigenciaI={2}&IdVigenciaF={3}", hfEsCesion.Value,hfIdRegContrato.Value,hIdVigenciaI.Value,hIdVigenciaF.Value);
            Response.Redirect(uri);
        }
        else if(!string.IsNullOrEmpty(hfesVigenciaFutura.Value))
        {
            string uri = string.Format("../../../Page/Contratos/Lupas/LupaListRP.aspx?idContrato={0}", hfIdContrato.Value + "&vesVigenciaFutura=" + hfesVigenciaFutura.Value
                + "&vidVigenciaFutura=" + hfidVigenciaFutura.Value + "&IdRegContrato=" + hfIdRegContrato.Value + "&vValorVigenciaFutura=" + hfValorVigencia.Value);

            //NavigateTo("../../../Page/Contratos/AsociarRpContrato/DetailRP.aspx?vesVigenciaFutura=" + hfesVigenciaFutura.Value + "&idContrato=" + hfIdContrato.Value + "&vidVigenciaFutura=" + hfidVigenciaFutura.Value + "&idRegContrato=" + hfIdRegContrato.Value);
            Response.Redirect(uri);
        }
        else
            Response.Redirect("ListRP.aspx");
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Response.Redirect("ListRP.aspx");
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (hfesVigenciaFutura.Value == "1")
            GuardarRPVigenciasFuturas();
        else
        Guardar();
    }
    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaVigencia();
            CargarListaRegional();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad AsociarRPContrato
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            int vIdContrato;
            decimal vValorRP;
            int vIdRegionalContrato;
            int vIdRegionalRP = int.Parse(ddlRegionalICBF.SelectedValue);

            RPContrato vRPContrato = new RPContrato();

            vRPContrato.IdRP = Convert.ToInt32(hfNumeroRP.Value);
            vRPContrato.NumeroRP = txtNumeroRP.Text;

            if(!decimal.TryParse(hfValorInicialRP.Value.Trim(), out vValorRP))
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }

            var rps = vContratoService.ConsultarRPContratosExiste(Convert.ToInt32(vRPContrato.NumeroRP), vIdRegionalRP);

            if(rps != null && rps.Count() > 0)
            {
                var idContrato = rps[0].IdContrato;
                toolBar.MostrarMensajeError("El RP del contrato ya se encuentra asociado al Id de contrato:" + idContrato + ", verifique por favor.");
                return;
            }

            if(vRPContrato.IdRP == -1)
            {
                vRPContrato.EsEnLinea = true;
                vRPContrato.FechaRP = DateTime.Parse(txtFechaRp.Text);
                vRPContrato.ValorRP = decimal.Parse(hfValorActualRP.Value.ToString());
            }

            if (string.IsNullOrEmpty(hfEsAdicion.Value) && string.IsNullOrEmpty(hfEsCesion.Value))
            {
                if (!int.TryParse(GetSessionParameter("Contrato.IdContrato").ToString().Trim(), out vIdContrato))
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                var vValorContrato = vContratoService.ConsultarValorsinAportes(vIdContrato);

                if (!int.TryParse(GetSessionParameter("Contrato.IdRegionalContrato").ToString().Trim(), out vIdRegionalContrato))
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                var valorActualContrato = Math.Round(vValorContrato, 0);
                var valorRp = Math.Round(decimal.Parse(hfValorInicialRP.Value.Trim()), 0);

                if(vValorContrato != valorRp)
                {
                    toolBar.MostrarMensajeError("El valor del RP es diferente al valor de los aportes de dinero del ICBF, verifique por favor.");
                    return;
                }

                var result = vContratoService.ContratoObtener(vRPContrato.IdContrato);

                if (vIdRegionalRP != vIdRegionalContrato)
                {
                    toolBar.MostrarMensajeError("El RP no corresponde a la regional en la cual se encuentra registrado el contrato");
                    return;
                }

                vRPContrato.IdContrato = vIdContrato;
                vRPContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                vRPContrato.FechaRP = DateTime.Parse(txtFechaRp.Text);

                DateTime? fechaFinalTerminacion = null;

                if (result.EsFechaFinalCalculada.HasValue && result.EsFechaFinalCalculada.Value)
                {
                    fechaFinalTerminacion = ContratoService.CalcularFechaFinalContrato
                        (
                         vRPContrato.FechaRP.Value,
                         result.DiasFechaFinalCalculada.Value,
                         result.MesesFechaFinalCalculada.Value
                        );
                }

                vRPContrato.FechaFinalizacion = fechaFinalTerminacion;

                InformacionAudioria(vRPContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarRPContratos(vRPContrato);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado >= 1)
                {
                    List<EstadoContrato> vEstadoContrato = new List<EstadoContrato>();

                    vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "CRP", null, true);

                    if (vEstadoContrato.Count == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        return;
                    }

                    vResultado = vContratoService.ActualizarContratoRP
                                                (
                                                vIdContrato, 
                                                GetSessionUser().NombreUsuario, 
                                                vEstadoContrato[0].IdEstadoContrato
                                                );
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        return;
                    }

                    if (vResultado == 1)
                    {
                        SetSessionParameter("Contrato.GuardadoRP", "1");
                        RemoveSessionParameter("ContratoRP.IdVigenciaFiscal");
                        RemoveSessionParameter("ContratoRP.IdRegionalContrato");
                        NavigateTo(SolutionPage.Detail);
                    }
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else if (!string.IsNullOrEmpty(hfEsCesion.Value))
            {
                var idCesion = int.Parse(hfEsCesion.Value);
                var usuario = GetSessionUser().NombreUsuario;
                vContratoService.ActualizarCesionRP(idCesion, usuario, vRPContrato.IdRP, vValorRP, vIdRegionalRP);

                string dialog = "Page_Contratos_Lupas_LupaListRP"; //Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                string returnValues =
                   "<script language='javascript'> " +
                   "var pObj = Array();" +
                   "pObj[" + (0) + "] = '1,1';" +
                        " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                        " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                    "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rvProductos", returnValues);                
            }
            else if (!string.IsNullOrEmpty(hfEsAdicion.Value))
            {
                var idAdicion = int.Parse(hfEsAdicion.Value);
                var usuario = GetSessionUser().NombreUsuario;
                vContratoService.ActualizarAdicionRP(idAdicion, usuario, vRPContrato.IdRP, vValorRP, vIdRegionalRP);

                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                string returnValues =
                   "<script language='javascript'> " +
                   "var pObj = Array();" +
                   "pObj[" + (0) + "] = '1,1';" +
                        " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                        " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                    "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rvProductos", returnValues);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    private void GuardarETL()
    {

    }

    private void GuardarRPVigenciasFuturas()
    {
        try
        {
            int vResultado;
            int vIdContrato;
            decimal vValorRP;
            int vIdRegionalContrato;
            int vIdRegionalRP;
            decimal valorVigenciaFutura = decimal.Parse(hfValorVigencia.Value);

            RPContrato vRPContrato = new RPContrato();

            vRPContrato.IdRP = Convert.ToInt32(hfNumeroRP.Value);

            if (!decimal.TryParse(hfValorInicialRP.Value.Trim(), out vValorRP))
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }

            if (!int.TryParse(ddlRegionalICBF.SelectedValue.Trim(), out vIdRegionalRP))
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }

            var rps = vContratoService.ConsultarRPContratosExiste(vRPContrato.IdRP);

            if (rps != null && rps.Count() > 0)
            {
                var idContrato = rps[0].IdContrato;
                toolBar.MostrarMensajeError("El RP del contrato ya se encuentra asociado al Id de contrato:" + idContrato + ", verifique por favor.");
                return;
            }

            
                if (!int.TryParse(hfIdContrato.Value, out vIdContrato))
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                //vValorContrato = vContratoService.ConsultarValorsinAportes(vIdContrato);

                if (!int.TryParse(hfIdRegContrato.Value, out vIdRegionalContrato))
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                if (vIdRegionalRP != vIdRegionalContrato)
                {
                    toolBar.MostrarMensajeError("El RP no corresponde a la regional en la cual se encuentra registrado el contrato");
                    return;
                }           

            
                //if (vValorRP != valorVigenciaFutura)
                //{
                //    toolBar.MostrarMensajeError("El Valor del RP no es igual al valor de la Vigencia Futura.");
                //    return;
                //}

            vRPContrato.IdContrato = vIdContrato;
                vRPContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                vRPContrato.FechaRP = DateTime.Parse(txtFechaRp.Text);
                vRPContrato.ValorRP = Convert.ToDecimal(hfValorActualRP.Value);
                vRPContrato.NumeroRP = txtNumeroRP.Text;
                vRPContrato.IdVigenciaFutura = Convert.ToInt32(hfidVigenciaFutura.Value);
                vRPContrato.EsVigenciaFutura = true;

                var result = vContratoService.ContratoObtener(vRPContrato.IdContrato);

                DateTime? fechaFinalTerminacion = null;

                if (result.EsFechaFinalCalculada.HasValue && result.EsFechaFinalCalculada.Value)
                {
                    fechaFinalTerminacion = ContratoService.CalcularFechaFinalContrato
                        (
                         vRPContrato.FechaRP.Value,
                         result.DiasFechaFinalCalculada.Value,
                         result.MesesFechaFinalCalculada.Value
                        );
                }

                vRPContrato.FechaFinalizacion = fechaFinalTerminacion;

                InformacionAudioria(vRPContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarRPContratos(vRPContrato);

                
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        return;
                    }

                    if (vResultado >= 1)
                    {                    
                    string returnValues =
                            "<script language='javascript'> " +
                            "   window.parent.window_closeModalDialog('dialogPage_Contratos_Lupas_LupaListRP');" +
                            "</script>";
                    this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
            }                       
            
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {
        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscal, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                String codigoRegional = vRuboService.ConsultarRegional(usuario.IdRegional).CodigoRegional;
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF, vRuboService.ConsultarRegionalPCIs(codigoRegional, null), "IdRegional", "CodigoNombreRegional");
                if (ddlRegionalICBF.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlRegionalICBF.SelectedValue = usuario.IdRegional.ToString();
                        ddlRegionalICBF.Enabled = false;
                    }
                    else
                    {
                        ddlRegionalICBF.SelectedValue = "-1";
                        ddlRegionalICBF.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "CodigoNombreRegional");
            }

        }

    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            hfIdContrato.Value = Request.QueryString["idContrato"];
        
        if (!string.IsNullOrEmpty(Request.QueryString["vesVigenciaFutura"]))
            hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vidVigenciaFutura"]))
            hfidVigenciaFutura.Value = Request.QueryString["vidVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdRegContrato"]))
            hfIdRegContrato.Value = Request.QueryString["IdRegContrato"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdVigenciaI"]))
           hIdVigenciaI.Value = Request.QueryString["IdVigenciaI"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdVigenciaF"]))
            hIdVigenciaF.Value = Request.QueryString["IdVigenciaF"];

        if (!string.IsNullOrEmpty(Request.QueryString["vValorVigenciaFutura"]))
            hfValorVigencia.Value = Request.QueryString["vValorVigenciaFutura"];
    }

}