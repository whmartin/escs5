<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_AsociarRPContrato_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Regional del contrato/Convenio*
                <asp:RequiredFieldValidator runat="server" ID="rfvEntidadFinanciera" ControlToValidate="ddlIDRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Vigencia Fiscal Inicial
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Número de Contrato
            </td>
            <td class="Cell">
                Dependencia solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Numbers" />
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlDependenciaSolicitante"  ></asp:DropDownList>
                <%--<asp:TextBox runat="server" ID="txtDependenciaSolicitante" MaxLength="256" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDependenciaSolicitante" runat="server" TargetControlID="txtDependenciaSolicitante"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />--%>
            </td>
        </tr>
        <tr class="rowB">
            <td  class="Cell">
                Tipo de contrato/convenio
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
            <td></td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAsociarRPContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,IdVigenciaInicial,IdRegionalContrato" CellPadding="0" Height="16px"
                        OnSorting="gvAsociarRPContrato_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvAsociarRPContrato_PageIndexChanging" OnSelectedIndexChanged="gvAsociarRPContrato_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Regional del contrato / convenio" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Número Contrato" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Dependencia solicitante" DataField="DependenciaSolicitante"  SortExpression="DependenciaSolicitante"/>
                            <asp:BoundField HeaderText="tipo de contrato / convenio" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>

                </td>
            </tr>
        </table>
        
    </asp:Panel>
</asp:Content>
