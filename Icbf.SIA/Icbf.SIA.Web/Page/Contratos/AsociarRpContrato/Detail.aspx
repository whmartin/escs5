<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_AsociarRPContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%@ Register src="../UserControl/ucContratoDetail.ascx" tagname="ucContratoDetail" tagprefix="uc1" %>
    
    <link href="../../../Styles/TabContainer.css" rel="stylesheet" type="text/css" />
    
    <div>
        <Ajax:TabContainer ID="tcInfoContraro" runat="server" Visible="true" 
            ActiveTabIndex="0" CssClass="ajax__tab_green-theme"
            Width="97%" OnActiveTabChanged="tcInfoContraro_ActiveTabChanged" 
            AutoPostBack="false" xmlns:ajax="ajaxcontroltoolkit">
                    <Ajax:TabPanel ID="tpContrato" runat="server" HeaderText="Contrato"> <HeaderTemplate> Contrato        
                </HeaderTemplate> <ContentTemplate> <uc1:ucContratoDetail ID="ucContratoDetail1" runat="server" />
                            <%--<iframe id="frameRegistrarContratos"  runat="server" name="frameRegistrarContratos" noresize="false" overflow="scroll" 
                        src="../Contratos/Detail.aspx" width="100%" height="440px"></iframe>--%>        
                </ContentTemplate> </Ajax:TabPanel> <Ajax:TabPanel ID="TpRp" runat="server" HeaderText="RP"> <HeaderTemplate> RP        
                </HeaderTemplate> <ContentTemplate> <div> <table width="100%"> <tr> <td id="right" colspan="3"> </td> </tr> <tr class="rowB"> <td class="style1"> <asp:Label ID="lblRegional" runat="server" Text="Regional"></asp:Label> </td> <td class="style1"><asp:Label ID="lblNumProceso" runat="server" Text="Número de Proceso"></asp:Label> </td> <td class="style1"> <asp:Label ID="lblFechaAdjProc" runat="server" Text="Fecha Adjudicación Proceso"></asp:Label> </td> </tr> <tr class="rowA"> <td> <asp:TextBox runat="server" ID="txtRegional" Enabled="False"></asp:TextBox> </td> <td> <asp:TextBox runat="server" ID="txtNumProceso" Enabled="False"></asp:TextBox> </td> <td> <asp:TextBox runat="server" ID="txtFechaAdjProc" Enabled="False"></asp:TextBox> </td> </tr> <tr> <td colspan="3"> </td> </tr> </table> <asp:GridView ID="gvRegistroPresupuestalContrato" runat="server"
                      AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px"
                        OnSorting="gvRegistroPresupuestalContrato_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvRegistroPresupuestalContrato_PageIndexChanging" 
                                      OnSelectedIndexChanged="gvRegistroPresupuestalContrato_SelectedIndexChanged"><Columns><asp:BoundField HeaderText="Regional ICBF" DataField="NombreRegional"  SortExpression="NombreRegional" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"/>
                            <asp:BoundField HeaderText="Número RP" DataField="NumeroRP"  SortExpression="IdRP" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"/>
                           <%-- <asp:BoundField HeaderText="Valor RP" DataField="ValorRP"  SortExpression="ValorRP" DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"/>--%>
                            <asp:BoundField HeaderText="Valor Inicial RP" DataField="ValorInicialRP"  SortExpression="ValorIncialRP" DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"/>
                            <asp:BoundField HeaderText="Valor Actual  RP" DataField="ValorActualRP"  SortExpression="ValorActualRP" DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center"/>
                       
                                           </Columns> 
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                      </asp:GridView>
                      

                                    <%--<asp:GridView runat="server" ID="gvRegistroPresupuestal" 
                                      AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" CellPadding="0" Height="16px"
                        OnSorting="gvRegistroPresupuestal_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvRegistroPresupuestal_PageIndexChanging" 
                                      OnSelectedIndexChanged="gvRegistroPresupuestal_SelectedIndexChanged" Visible="True">
                        <Columns>
                            
                            <asp:BoundField HeaderText="Regional ICBF" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Número RP" DataField="NumeroRP"  SortExpression="NumeroRP"/>
                            <asp:BoundField HeaderText="Valor RP" DataField="ValorRP"  SortExpression="ValorRP" DataFormatString="{0:C}" />
                                
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>--%> </div>        
                </ContentTemplate> </Ajax:TabPanel> </Ajax:TabContainer>

    </div>
    
    
    <%--<table width="90%" align="center">
        <tr class="rowB">
            <td>
                Regional del Contrato/Convenio
            </td>
            <td>
                Vigencia Fiscal Inicial
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDRegional"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Contrato
            </td>
            <td>
                Dependencia Solicitante
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDependenciaSolicitante"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Tipo de Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>--%>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
    </style>
</asp:Content>

