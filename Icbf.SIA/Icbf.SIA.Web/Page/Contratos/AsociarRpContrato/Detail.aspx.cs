using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de visualización detallada para la entidad AsociarRPContrato
/// </summary>
public partial class Page_AsociarRPContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/AsociarRPContrato";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        //RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo("ListRP.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        EliminarRegistro();
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("Contrato.GuardadoRP").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La información ha sido registrada en forma exitosa");
            RemoveSessionParameter("Contrato.GuardadoRP");

            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            //RemoveSessionParameter("Contrato.IdContrato");

            List<Contrato> lContrato = new List<Contrato>();

            lContrato = vContratoService.ConsultarContratoss(null, null, vIdContrato, null, null, null, null, null, null, null, null, null,null,null,null,null,null);

            if (lContrato.Count > 0)
            {
                DateTime vFechaAdjProc;

                txtRegional.Text = lContrato[0].NombreRegional;
                txtNumProceso.Text = lContrato[0].NumeroProceso;
                if (DateTime.TryParse(lContrato[0].FechaAdjudicacionProceso.ToString(), out vFechaAdjProc))
                {
                    if (vFechaAdjProc.Year.ToString() != "1900")
                    {
                        txtFechaAdjProc.Text = vFechaAdjProc.ToShortDateString();
                    }

                }

            }

            SetSessionParameter("Contrato.ValorFinalContrato", lContrato[0].ValorFinalContrato);
            SetSessionParameter("Contrato.ValorInicialContrato", lContrato[0].ValorInicialContrato);
            SetSessionParameter("Contrato.IdRegionalContrato", lContrato[0].IdRegionalContrato);
            gvRegistroPresupuestalContrato.PageSize = PageSize();
            gvRegistroPresupuestalContrato.EmptyDataText = EmptyDataText();
            CargarGrilla(gvRegistroPresupuestalContrato, GridViewSortExpression, true);


            if (gvRegistroPresupuestalContrato.Rows.Count > 0)
            {
                toolBar.MostrarBotonNuevo(false);
            }
            else
            {
                List<Icbf.Contrato.Entity.EstadoContrato> vEstadoContrato = new List<Icbf.Contrato.Entity.EstadoContrato>();
                vEstadoContrato = vContratoService.ConsultarEstadoContrato(lContrato[0].IdEstadoContrato, null, null, true);
                if (vEstadoContrato.Count > 0)
                {
                    if (vEstadoContrato[0].CodEstado == "SUS")
                    {
                        toolBar.MostrarBotonNuevo(true);
                    }
                }

            }



        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
        var myGridResults = vContratoService.ConsultarRPContratosAsociados(vIdContrato, null);


        //////////////////////////////////////////////////////////////////////////////////
        //////Fin del código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        gridViewsender.DataSource = myGridResults;
        if (expresionOrdenamiento != null)
        {
            //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
            if (string.IsNullOrEmpty(GridViewSortExpression))
            {
                GridViewSortDirection = SortDirection.Ascending;
            }
            else if (GridViewSortExpression != expresionOrdenamiento)
            {
                GridViewSortDirection = SortDirection.Descending;
            }
            if (myGridResults != null)
            {
                var param = Expression.Parameter(typeof(RPContrato), expresionOrdenamiento);

                //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                var prop = Expression.Property(param, expresionOrdenamiento);

                //Creo en tiempo de ejecución la expresión lambda
                var sortExpression = Expression.Lambda<Func<RPContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                //Dependiendo del modo de ordenamiento . . .
                if (GridViewSortDirection == SortDirection.Ascending)
                {

                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                    if (cambioPaginacion == false)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                    }
                    else
                    {
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                    }
                }
                else
                {

                    //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                    if (cambioPaginacion == false)
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                    }
                    else
                    {
                        gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                    }
                }

                GridViewSortExpression = expresionOrdenamiento;
            }
        }
        else
        {
            gridViewsender.DataSource = myGridResults;
        }

        gridViewsender.DataBind();

    }

    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

            //AsociarRPContrato vAsociarRPContrato = new AsociarRPContrato();
            //vAsociarRPContrato = vContratoService.ConsultarAsociarRPContrato();
            //InformacionAudioria(vAsociarRPContrato, this.PageName, vSolutionPage);
            //int vResultado = vContratoService.EliminarAsociarRPContrato(vAsociarRPContrato);
            //if (vResultado == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            //    SetSessionParameter("AsociarRPContrato.Eliminado", "1");
            //    NavigateTo(SolutionPage.List);
            //}
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {

            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);


            toolBar.EstablecerTitulos("Asociar el Registro presupuestal a un Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRegistroPresupuestalContrato_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvRegistroPresupuestalContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvRegistroPresupuestalContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroPresupuestalContrato.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    protected void tcInfoContraro_ActiveTabChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }



}
