﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListRP.aspx.cs" Inherits="Page_Contratos_AsociarRpContrato_ListRP" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
<%--        $(function () {

            $("#<%= txtValorTotalDesde.ClientID%>").change(function () {
                $("#<%= txtValorTotalDesde.ClientID%>").formatCurrency();
            }).blur(function () {
                $("#<%= txtValorTotalDesde.ClientID%>").formatCurrency();
            });

            $("#<%= txtValorTotalHasta.ClientID%>").change(function () {
                $("#<%= txtValorTotalHasta.ClientID%>").formatCurrency();
            }).blur(function () {
                $("#<%= txtValorTotalHasta.ClientID%>").formatCurrency();
            });


        });--%>

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <asp:HiddenField ID="hfIdRegional" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Vigencia Fiscal*
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlVigenciaFiscal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Regional ICBF*
                <asp:RequiredFieldValidator runat="server" ID="rfvEntidadFinanciera" ControlToValidate="ddlRegionalICBF"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscal"></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlRegionalICBF" AutoPostBack="True" ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Número del RP
            </td>
            <td class="Cell">
                
                Fuente</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroRP" MaxLength="50" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroRP" runat="server" TargetControlID="txtNumeroRP"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                
                <asp:RadioButtonList ID="RbtTipoBusqueda" runat="server"  RepeatDirection="Horizontal">
                    <asp:ListItem Text="ETL" Value="1" />
                    <asp:ListItem Selected="True" Text="Linea" Value="0" />
                </asp:RadioButtonList>
                
            </td>
        </tr>
<%--        <tr class="rowB">
            <td class="Cell">
                Valor Total desde
              
            </td>
            <td class="Cell">
                Valor Total Hasta
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtValorTotalDesde" MaxLength="20" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorTotalDesde" runat="server" TargetControlID="txtValorTotalDesde"
                    FilterType="Custom,Numbers" ValidChars="$,." />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtValorTotalHasta" MaxLength="20" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorTotalHasta" runat="server" TargetControlID="txtValorTotalHasta"
                    FilterType="Custom,Numbers" ValidChars="$,." />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" style="height: 26px">
                Fecha RP Desde
                <asp:CompareValidator ID="cvFechaVigenciaHasta" runat="server" ControlToValidate="txtFechaCDPDesde" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnBuscar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>
            <td class="Cell" style="height: 26px">
                Fecha RP Hasta
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFechaCDPHasta" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnBuscar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                 <asp:TextBox runat="server" ID="txtFechaCDPDesde" ></asp:TextBox>
                <asp:Image ID="imgCalendarioDesde" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="True"/>
                <Ajax:CalendarExtender ID="ceFechaVigenciaDesde" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="imgCalendarioDesde" TargetControlID="txtFechaCDPDesde"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meFechaVigenciaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaCDPDesde">
                </Ajax:MaskedEditExtender>
                
            </td>
            <td class="Cell">
                 <asp:TextBox runat="server" ID="txtFechaCDPHasta"></asp:TextBox>
                <asp:Image ID="imgCalendarioHasta" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="True"/>
                <Ajax:CalendarExtender ID="ceFechaVigenciaHasta" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="imgCalendarioHasta" TargetControlID="txtFechaCDPHasta"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meFechaVigenciaHasta" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaCDPHasta">
                </Ajax:MaskedEditExtender>
                
            </td>
        </tr>--%>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegistroInformacionPresupuestal" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRP,IdRegional,ValorInicialRP,NumeroRP,EsEnLinea,FechaRP" CellPadding="0" Height="16px"
                        OnSorting="gvRegistroInformacionPresupuestal_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvRegistroInformacionPresupuestal_PageIndexChanging" OnSelectedIndexChanged="gvRegistroInformacionPresupuestal_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Id RP" DataField="IdRP" Visible="false"  SortExpression="IdRP"/>
                            <asp:BoundField HeaderText="Valor Inicial RP" DataField="ValorInicialRP"  SortExpression="ValorInicialRP" DataFormatString="{0:C}" />
                            <asp:BoundField HeaderText="Valor Actual  RP" DataField="ValorActualRP"  SortExpression="ValorActualRP" DataFormatString="{0:C}" />
                            <asp:BoundField HeaderText="Número RP" DataField="NumeroRP" SortExpression="NumeroRP"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>


</asp:Content>