﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="DetailRP.aspx.cs" Inherits="Page_Contratos_AsociarRpContrato_DetailRP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfEsAdicion" runat="server" />
    <asp:HiddenField ID="hfEsCesion" runat="server" />
    <asp:HiddenField ID="hfValorInicialRP" runat="server" />
    <asp:HiddenField ID="hfValorActualRP" runat="server" />
    <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfidVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfValorVigencia" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
     <asp:HiddenField ID="hfIdRegContrato" runat="server" />
     <asp:HiddenField ID="hIdVigenciaI" runat="server" />
     <asp:HiddenField ID="hIdVigenciaF" runat="server" />
    <asp:HiddenField ID="hfNumeroRP" runat="server" />
    <asp:HiddenField ID="hfEsEnLinea" runat="server" />

    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Vigencia Fiscal
            </td>
            <td class="Cell">
                Regional
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscal" Enabled="False" ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlRegionalICBF" AutoPostBack="True" Enabled="False" ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Número RP
            </td>
            <td class="Cell">
                
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtNumeroRP" MaxLength="50" Enabled="False"></asp:TextBox>
                
            </td>
            <td class="auto-style1">
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Valor Inicial RP
                
            </td>
            <td class="Cell">
                Fecha RP
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtValorRP" MaxLength="20" Enabled="False"></asp:TextBox>
                
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtFechaRp" MaxLength="20" Enabled="False"></asp:TextBox>
                
            </td>
        </tr>
         <tr class="rowB">
            <td class="Cell">
                Valor Actual RP
                
            </td>
            <td class="Cell">

            </td>
        </tr>
                <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtValorActualRP" MaxLength="20" Enabled="False"></asp:TextBox>
                
            </td>
            <td class="Cell">
                
            </td>
        </tr>
        
    </table>
    </asp:Panel>

    </asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 50%;
            height: 54px;
        }
    </style>
</asp:Content>

