using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición para la entidad AsociarRPContrato
/// </summary>
public partial class Page_AsociarRPContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/AsociarRPContrato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

        /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad AsociarRPContrato
        /// </summary>
    private void Guardar()
    {
        try
        {
            //int vResultado;
            //AsociarRPContrato vAsociarRPContrato = new AsociarRPContrato();

            //vAsociarRPContrato.IDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            //vAsociarRPContrato.VigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            //vAsociarRPContrato.NumeroContrato = Convert.ToString(txtNumeroContrato.Text);
            //vAsociarRPContrato.DependenciaSolicitante = Convert.ToString(txtDependenciaSolicitante.Text);
            //vAsociarRPContrato.IDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);

            //if (Request.QueryString["oP"] == "E")
            //{
            //    vAsociarRPContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
            //    InformacionAudioria(vAsociarRPContrato, this.PageName, vSolutionPage);
            //    vResultado = vContratoService.ModificarAsociarRPContrato(vAsociarRPContrato);
            //}
            //else
            //{
            //    vAsociarRPContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
            //    InformacionAudioria(vAsociarRPContrato, this.PageName, vSolutionPage);
            //    vResultado = vContratoService.InsertarAsociarRPContrato(vAsociarRPContrato);
            //}
            //if (vResultado == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    SetSessionParameter("AsociarRPContrato.Guardado", "1");
            //    NavigateTo(SolutionPage.Detail);
            //}
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Asociar el Registro presupuestal a un Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {

            //AsociarRPContrato vAsociarRPContrato = new AsociarRPContrato();
            //vAsociarRPContrato = vContratoService.ConsultarAsociarRPContrato();
            //ddlIDRegional.SelectedValue = vAsociarRPContrato.IDRegional.ToString();
            //ddlVigenciaFiscalinicial.SelectedValue = vAsociarRPContrato.VigenciaFiscalinicial.ToString();
            //txtNumeroContrato.Text = vAsociarRPContrato.NumeroContrato;
            //txtDependenciaSolicitante.Text = vAsociarRPContrato.DependenciaSolicitante;
            //ddlIDTipoContrato.SelectedValue = vAsociarRPContrato.IDTipoContrato.ToString();
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vAsociarRPContrato.UsuarioCrea, vAsociarRPContrato.FechaCrea, vAsociarRPContrato.UsuarioModifica, vAsociarRPContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
