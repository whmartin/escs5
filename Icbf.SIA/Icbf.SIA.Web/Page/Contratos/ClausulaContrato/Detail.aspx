<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ClausulaContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdClausulaContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre de Cl&#225;usula *
            </td>
            <td style="width: 50%">
                Tipo Cl&#225;usula *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreClausulaContrato"  Enabled="false" 
                    Height="50px" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdTipoClausula"  Enabled="false" 
                    Height="25px" Width="394px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Contenido de la Cl&#225;usula
            </td>
            <td>
                Tipo de Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContenido"  Enabled="false" Height="50px" 
                    TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdTipoContrato"  Enabled="false" 
                    Height="25px" Width="392px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 379px;
        }
    </style>
</asp:Content>

