using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de cláusula contrato
/// </summary>
public partial class Page_ClausulaContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ClausulaContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("ClausulaContrato.Guardado");
            }

        }
    }

    /// <summary>
    /// Manejador de eventos click para el botón Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ClausulaContrato.IdClausulaContrato", hfIdClausulaContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botón Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdClausulaContrato = Convert.ToInt32(GetSessionParameter("ClausulaContrato.IdClausulaContrato"));
            RemoveSessionParameter("ClausulaContrato.IdClausulaContrato");

            if (GetSessionParameter("ClausulaContrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ClausulaContrato.Guardado");

            if (GetSessionParameter("ClausulaContrato.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
            RemoveSessionParameter("ClausulaContrato.Modificado");

            ClausulaContrato vClausulaContrato = new ClausulaContrato();
            vClausulaContrato = vContratoService.ConsultarClausulaContrato(vIdClausulaContrato);
            hfIdClausulaContrato.Value = vClausulaContrato.IdClausulaContrato.ToString();
            txtNombreClausulaContrato.Text = vClausulaContrato.NombreClausulaContrato;
            ddlIdTipoClausula.SelectedValue = vClausulaContrato.IdTipoClausula.ToString();
            txtContenido.Text = vClausulaContrato.Contenido;
            ddlIdTipoContrato.SelectedValue = vClausulaContrato.IdTipoContrato.ToString();
            rblEstado.SelectedValue = vClausulaContrato.Estado == true ? "True" : "False";
            ObtenerAuditoria(PageName, hfIdClausulaContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vClausulaContrato.UsuarioCrea, vClausulaContrato.FechaCrea, vClausulaContrato.UsuarioModifica, vClausulaContrato.FechaModifica);
            //toolBar.OcultarBotonNuevo(true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdClausulaContrato = Convert.ToInt32(hfIdClausulaContrato.Value);

            ClausulaContrato vClausulaContrato = new ClausulaContrato();
            vClausulaContrato = vContratoService.ConsultarClausulaContrato(vIdClausulaContrato);
            InformacionAudioria(vClausulaContrato, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarClausulaContrato(vClausulaContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ClausulaContrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Cl&#225;usula Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService objContratoServices = new ContratoService();
            ddlIdTipoClausula.DataSource = objContratoServices.ConsultarTipoClausulas(null,null,true);
            ddlIdTipoClausula.DataTextField = "NombreTipoClausula";
            ddlIdTipoClausula.DataValueField = "IdTipoClausula";
            ddlIdTipoClausula.DataBind();
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdTipoContrato.DataSource = objContratoServices.ConsultarTipoContratos(null, null,true,null,null,null,null,null);
            ddlIdTipoContrato.DataTextField = "NombreTipoContrato";
            ddlIdTipoContrato.DataValueField = "IdTipoContrato";
            ddlIdTipoContrato.DataBind();
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
