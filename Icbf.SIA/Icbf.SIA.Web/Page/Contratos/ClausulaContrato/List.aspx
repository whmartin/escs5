<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ClausulaContrato_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre de Cl&#225;usula</td>
            <td style="width: 50%">
                Tipo Cl&#225;usula</td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreClausulaContrato" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"
                    Height="50px" TextMode="MultiLine" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreClausulaContrato" runat="server" TargetControlID="txtNombreClausulaContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " InvalidChars="&#63;"/>
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdTipoClausula" Height="25px" 
                    Width="381px"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Tipo de Contrato
                
            </td>
            <td colspan="2">
                
                <asp:Label runat="server" ID="lblContClausula" Text="Contenido de la Cl&#225;usula" Visible="False"></asp:Label> 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
               <asp:DropDownList runat="server" ID="ddlIdTipoContrato" Height="25px" 
                    Width="383px"  ></asp:DropDownList>
            </td>
            <td colspan="2">
                 <asp:TextBox runat="server" ID="txtContenido" MaxLength="128" Height="50px" 
                    TextMode="MultiLine" Width="320px"
                     onKeyDown="limitText(this,128);" Visible="False" onKeyUp="limitText(this,128);">
                    </asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftContenido" runat="server" TargetControlID="txtContenido"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " InvalidChars="&#63;"/>
                
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvClausulaContrato" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdClausulaContrato" 
                        CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvClausulaContrato_PageIndexChanging" 
                        OnSelectedIndexChanged="gvClausulaContrato_SelectedIndexChanged" 
                        AllowSorting="True" onsorting="gvClausulaContrato_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Nombre de Cl&#225;usula" DataField="NombreClausulaContrato" SortExpression="NombreClausulaContrato"/>--%>
                            <asp:TemplateField HeaderText="Nombre de Cl&#225;usula" ItemStyle-HorizontalAlign="Center" SortExpression="NombreClausulaContrato">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 250px;">
                                        <%#Eval("NombreClausulaContrato")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Cl&#225;usula" DataField="NombreTipoClausula" SortExpression="NombreTipoClausula"/>
                            <asp:BoundField HeaderText="Tipo De Contrato" DataField="NombreTipoContrato" SortExpression="NombreTipoContrato" />
                            <%--<asp:BoundField HeaderText="Contenido de la Cl&#225;usula" DataField="Contenido" SortExpression="Contenido" />--%>
                            <asp:TemplateField HeaderText="Contenido de la Cl&#225;usula" ItemStyle-HorizontalAlign="Center" SortExpression="Contenido">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 250px;">
                                        <%#Eval("Contenido")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                            <%--<asp:CheckBoxField DataField="Estado" HeaderText="Estado" />--%>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 364px;
        }
    </style>
</asp:Content>

