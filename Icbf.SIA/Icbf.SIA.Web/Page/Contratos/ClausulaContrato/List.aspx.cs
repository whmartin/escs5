using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;
using System.Data;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de cl�usula contrato
/// </summary>
public partial class Page_ClausulaContrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ClausulaContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreClausulaContrato = null;
            String vContenido = null;
            int? vIdTipoClausula = null;
            int? vIdTipoContrato = null;
            Boolean? vEstado = null;
            if (txtNombreClausulaContrato.Text != "")
            {
                vNombreClausulaContrato = Convert.ToString(txtNombreClausulaContrato.Text);
            }
            if (ddlIdTipoClausula.SelectedValue != "-1")
            {
                vIdTipoClausula = Convert.ToInt32(ddlIdTipoClausula.SelectedValue);
            }
            if (ddlIdTipoContrato.SelectedValue != "-1")
            {
                vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            }
            if (txtContenido.Text != "")
            {
                vContenido = Convert.ToString(txtContenido.Text);
            }
            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }

            var result = from clausulaContrato in vContratoService.ConsultarClausulaContratos(vNombreClausulaContrato, vContenido, vIdTipoClausula, vIdTipoContrato, vEstado)
                         join tipoContrato in vContratoService.ConsultarTipoContratos(null, null, true, null, null, null, null, null)
                         on clausulaContrato.IdTipoContrato equals tipoContrato.IdTipoContrato
                         join tipoClausula in vContratoService.ConsultarTipoClausulas(null, null, true)
                         on clausulaContrato.IdTipoClausula equals tipoClausula.IdTipoClausula
                         select new
                         {
                             clausulaContrato.IdClausulaContrato,
                             clausulaContrato.NombreClausulaContrato,
                             tipoClausula.NombreTipoClausula,
                             tipoContrato.NombreTipoContrato,
                             clausulaContrato.Estado,
                             clausulaContrato.Contenido
                         };


            List<datosGridView> listaClausulaContrato = new List<datosGridView>();

            foreach (var datosResult in result)
            {
                datosGridView objDatosGridView = new datosGridView();
                objDatosGridView.IdClausulaContrato = datosResult.IdClausulaContrato;
                objDatosGridView.NombreClausulaContrato = datosResult.NombreClausulaContrato;
                objDatosGridView.NombreTipoClausula = datosResult.NombreTipoClausula;
                objDatosGridView.NombreTipoContrato = datosResult.NombreTipoContrato;
                objDatosGridView.Contenido = datosResult.Contenido;
                if (datosResult.Estado)
                {
                    objDatosGridView.EstadoString = "Activo";
                }
                else
                {
                    objDatosGridView.EstadoString = "Inactivo";
                }
                listaClausulaContrato.Add(objDatosGridView);
            }

            gvClausulaContrato.DataSource = listaClausulaContrato.ToList();
            
            gvClausulaContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvClausulaContrato.PageSize = PageSize();
            gvClausulaContrato.EmptyDataText = EmptyDataText();
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Cl&#225;usula Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvClausulaContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ClausulaContrato.IdClausulaContrato", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvClausulaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvClausulaContrato.SelectedRow);
    }
    protected void gvClausulaContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvClausulaContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ClausulaContrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("ClausulaContrato.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService objContratoServices = new ContratoService();
            ddlIdTipoClausula.DataSource = objContratoServices.ConsultarTipoClausulas(null, null, true);
            ddlIdTipoClausula.DataTextField = "NombreTipoClausula";
            ddlIdTipoClausula.DataValueField = "IdTipoClausula";
            ddlIdTipoClausula.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoClausula);

            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdTipoContrato.DataSource = objContratoServices.ConsultarTipoContratos(null, null, true, null, null, null, null, null);
            ddlIdTipoContrato.DataTextField = "NombreTipoContrato";
            ddlIdTipoContrato.DataValueField = "IdTipoContrato";
            ddlIdTipoContrato.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoContrato);

            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            rblEstado.SelectedIndex = -1;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvClausulaContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }

    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNombreClausulaContrato = null;
        String vContenido = null;
        int? vIdTipoClausula = null;
        int? vIdTipoContrato = null;
        Boolean? vEstado = null;
        if (txtNombreClausulaContrato.Text != "")
        {
            vNombreClausulaContrato = Convert.ToString(txtNombreClausulaContrato.Text);
        }
        if (ddlIdTipoClausula.SelectedValue != "-1")
        {
            vIdTipoClausula = Convert.ToInt32(ddlIdTipoClausula.SelectedValue);
        }
        if (ddlIdTipoContrato.SelectedValue != "-1")
        {
            vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
        }
        if (txtContenido.Text != "")
        {
            vContenido = Convert.ToString(txtContenido.Text);
        }

        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }

        var myGridResults = from clausulaContrato in vContratoService.ConsultarClausulaContratos(vNombreClausulaContrato, vContenido, vIdTipoClausula, vIdTipoContrato, vEstado)
                            join tipoContrato in vContratoService.ConsultarTipoContratos(null, null, true, null, null, null, null, null)
                            on clausulaContrato.IdTipoContrato equals tipoContrato.IdTipoContrato
                            join tipoClausula in vContratoService.ConsultarTipoClausulas(null, null, true)
                            on clausulaContrato.IdTipoClausula equals tipoClausula.IdTipoClausula
                            select new
                            {
                                clausulaContrato.IdClausulaContrato,
                                clausulaContrato.NombreClausulaContrato,
                                tipoClausula.NombreTipoClausula,
                                tipoContrato.NombreTipoContrato,
                                clausulaContrato.Estado,
                                clausulaContrato.Contenido
                            };

        List<datosGridView> listaClausulaContrato = new List<datosGridView>();

        foreach (var datosResult in myGridResults)
        {
            datosGridView objDatosGridView = new datosGridView();
            objDatosGridView.IdClausulaContrato = datosResult.IdClausulaContrato;
            objDatosGridView.NombreClausulaContrato = datosResult.NombreClausulaContrato;
            objDatosGridView.NombreTipoClausula = datosResult.NombreTipoClausula;
            objDatosGridView.NombreTipoContrato = datosResult.NombreTipoContrato;
            objDatosGridView.Contenido = datosResult.Contenido;
            if (datosResult.Estado)
            {
                objDatosGridView.EstadoString = "Activo";
            }
            else
            {
                objDatosGridView.EstadoString = "Inactivo";
            }
            listaClausulaContrato.Add(objDatosGridView);
        }

        if (myGridResults != null)
        {
            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                switch (e.SortExpression)
                {
                    case ("NombreClausulaContrato"):
                        listaClausulaContrato = listaClausulaContrato.OrderBy(x => x.NombreClausulaContrato).ToList();
                        break;
                    case ("NombreTipoClausula"):
                        listaClausulaContrato = listaClausulaContrato.OrderBy(x => x.NombreTipoClausula).ToList();
                        break;
                    case ("NombreTipoContrato"):
                        listaClausulaContrato = listaClausulaContrato.OrderBy(x => x.NombreTipoContrato).ToList();
                        break;
                    case ("EstadoString"):
                        listaClausulaContrato = listaClausulaContrato.OrderByDescending(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvClausulaContrato.DataSource = listaClausulaContrato;
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                switch (e.SortExpression)
                {
                    case ("NombreClausulaContrato"):
                        listaClausulaContrato = listaClausulaContrato.OrderByDescending(x => x.NombreClausulaContrato).ToList();
                        break;
                    case ("NombreTipoClausula"):
                        listaClausulaContrato = listaClausulaContrato.OrderByDescending(x => x.NombreTipoClausula).ToList();
                        break;
                    case ("NombreTipoContrato"):
                        listaClausulaContrato = listaClausulaContrato.OrderByDescending(x => x.NombreTipoContrato).ToList();
                        break;
                    case ("Contenido"):
                        listaClausulaContrato = listaClausulaContrato.OrderByDescending(x => x.Contenido).ToList();
                        break;
                    case ("EstadoString"):
                        listaClausulaContrato = listaClausulaContrato.OrderByDescending(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvClausulaContrato.DataSource = listaClausulaContrato;
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvClausulaContrato.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}

public class datosGridView
{
    public int IdClausulaContrato { get; set; }
    public String NombreClausulaContrato { get; set; }
    public String NombreTipoClausula { get; set; }
    public String NombreTipoContrato { get; set; }
    public String EstadoString { get; set; }
    public String Contenido { get; set; }

    public datosGridView()
    {

    }
}
