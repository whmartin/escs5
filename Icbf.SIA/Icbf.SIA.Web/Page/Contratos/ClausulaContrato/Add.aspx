<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_ClausulaContrato_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdClausulaContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre de
                Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreClausulaContrato" ControlToValidate="txtNombreClausulaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 50%">
                Tipo Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoClausula" ControlToValidate="ddlIdTipoClausula"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoClausula" ControlToValidate="ddlIdTipoClausula"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreClausulaContrato" MaxLength="128" 
                    Height="50px" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreClausulaContrato" runat="server" TargetControlID="txtNombreClausulaContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdTipoClausula" Height="25px" 
                    Width="384px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Contenido de la Cl&#225;usula *
                <asp:RequiredFieldValidator runat="server" ID="rfvContClausula" ControlToValidate="txtContenido"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo de Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoContrato" ControlToValidate="ddlIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoContrato" ControlToValidate="ddlIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContenido" Height="50px" 
                    TextMode="MultiLine" Width="320px"
                     onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftContenido" runat="server" TargetControlID="txtContenido"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdTipoContrato" Height="25px" 
                    Width="383px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 361px;
        }
    </style>
</asp:Content>

