using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de aporte contratos para clausula contratos
/// </summary>
public partial class Page_ClausulaContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/ClausulaContrato";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botoòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botón Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Información del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ClausulaContrato vClausulaContrato = new ClausulaContrato();

            vClausulaContrato.NombreClausulaContrato = Convert.ToString(txtNombreClausulaContrato.Text);
            vClausulaContrato.IdTipoClausula = Convert.ToInt32(ddlIdTipoClausula.SelectedValue);
            vClausulaContrato.Contenido = Convert.ToString(txtContenido.Text);
            vClausulaContrato.IdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            vClausulaContrato.Estado = rblEstado.SelectedValue == "1"? true : false;

            if (Request.QueryString["oP"] == "E")
            {
            vClausulaContrato.IdClausulaContrato = Convert.ToInt32(hfIdClausulaContrato.Value);
                vClausulaContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vClausulaContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarClausulaContrato(vClausulaContrato);
            }
            else
            {
                vClausulaContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vClausulaContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarClausulaContrato(vClausulaContrato);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("ClausulaContrato.IdClausulaContrato", vClausulaContrato.IdClausulaContrato);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("ClausulaContrato.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("ClausulaContrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
               
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Cl&#225;usula Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos provenientes de la entidad a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdClausulaContrato = Convert.ToInt32(GetSessionParameter("ClausulaContrato.IdClausulaContrato"));
            int vIdClausulaContrato = 0;

            if (int.TryParse(GetSessionParameter("ClausulaContrato.IdClausulaContrato").ToString(), out vIdClausulaContrato))
            {
                RemoveSessionParameter("ClausulaContrato.Id");

                ClausulaContrato vClausulaContrato = new ClausulaContrato();
                vClausulaContrato = vContratoService.ConsultarClausulaContrato(vIdClausulaContrato);
                hfIdClausulaContrato.Value = vClausulaContrato.IdClausulaContrato.ToString();
                txtNombreClausulaContrato.Text = vClausulaContrato.NombreClausulaContrato;
                ddlIdTipoClausula.SelectedValue = vClausulaContrato.IdTipoClausula.ToString();
                txtContenido.Text = vClausulaContrato.Contenido;
                ddlIdTipoContrato.SelectedValue = vClausulaContrato.IdTipoContrato.ToString();
                rblEstado.SelectedValue = vClausulaContrato.Estado == true ? "1" : "0";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vClausulaContrato.UsuarioCrea, vClausulaContrato.FechaCrea, vClausulaContrato.UsuarioModifica, vClausulaContrato.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService objContratoServices = new ContratoService();
            ddlIdTipoClausula.DataSource = objContratoServices.ConsultarTipoClausulas(null, null, true);
            ddlIdTipoClausula.DataTextField = "NombreTipoClausula";
            ddlIdTipoClausula.DataValueField = "IdTipoClausula";
            ddlIdTipoClausula.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoClausula);

            
            ddlIdTipoContrato.DataSource = objContratoServices.ConsultarTipoContratos(null, null, true, null, null, null, null, null);
            ddlIdTipoContrato.DataTextField = "NombreTipoContrato";
            ddlIdTipoContrato.DataValueField = "IdTipoContrato";
            ddlIdTipoContrato.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoContrato);



            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
