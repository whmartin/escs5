﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página que despliega la información y permite la selección de garantías
/// </summary>
public partial class Page_Contratos_Lupas_LupaContratos : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    /// <summary>
    /// 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            //toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            //toolBar.HabilitarBotonGuardar(false);

            
            toolBar.EstablecerTitulos("Garantías", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}