﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaGarantia.aspx.cs" Inherits="Page_Contratos_Lupas_LupaGarantia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <asp:HiddenField ID="hfIdGarantia" runat="server" />
    <asp:HiddenField ID="hfFechaInicio" runat="server" />
<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            
             <tr class="rowB">
                <td>Número de Garantía
                    <asp:RequiredFieldValidator runat="server" ID="rfvNumGarantia" ControlToValidate="txtNumeroGarantia"
                    SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" ></asp:RequiredFieldValidator>
                </td>
                <td>
                     <asp:TextBox ID="txtNumeroGarantia" runat="server"  />

                      <Ajax:FilteredTextBoxExtender ID="fttxtNumGarantia" runat="server" TargetControlID="txtNumeroGarantia"
                                                FilterType="Numbers"  />
                </td>
            </tr>
            <tr class="rowB">
                <td>Fecha Aprobación Garantía
                                        <asp:RequiredFieldValidator runat="server" ID="rfvFechaAprobacion" ControlToValidate="txtFechaAprobacion"
                    SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" ></asp:RequiredFieldValidator>
                </td>
                <td>
                                    <asp:TextBox ID="txtFechaAprobacion" runat="server"></asp:TextBox>
                                    <asp:Image ID="imgFechaSolicitud" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                    <Ajax:CalendarExtender ID="CalendarExtenderFechaAprobacion" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaSolicitud" TargetControlID="txtFechaAprobacion">
                                    </Ajax:CalendarExtender>
                                    <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaAprobacion">
                                    </Ajax:MaskedEditExtender> 
                                    <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaAprobacion" runat="server" />   
                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>