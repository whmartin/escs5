﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaContratistaContrato.aspx.cs" Inherits="Page_Contratos_Lupas_LupaContratistaContrato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratista" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContratistaContrato" CellPadding="0" 
                        Height="16px" 
                        OnSelectedIndexChanged="gvSucursal_SelectedIndexChanged"
                        OnPageIndexChanging="gvSucursal_PageIndexChanging" 
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                    <asp:Label ID="labelIdContratistaContrato" Text='<%# Bind ("IdContratistaContrato") %>' runat="server" Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Numero Identificacion" DataField="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Razon Social" DataField="RazonSocial" />
                            <asp:BoundField HeaderText="Clase Entidad" DataField="ClaseEntidad" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>

