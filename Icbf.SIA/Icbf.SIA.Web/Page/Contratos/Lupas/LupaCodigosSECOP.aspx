﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaCodigosSECOP.aspx.cs" Inherits="Page_Contratos_Lupas_LupaCodigoSECOP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="HfIdContrato" runat="server" />

    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                                            <asp:GridView ID="gvCodigosSECOP" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="key" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Código SECOP" DataField="Value" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
