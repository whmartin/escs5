﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaCodigoSECOP : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }
    
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoGuardar += toolBar_eventoGuardar;
            toolBar.EstablecerTitulos("Información Código SECOP", SolutionPage.Edit.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnBuscar_Click(object sender, EventArgs e)
    {
        var codigo = txtCodigosSECOP.Text;

        if (! string.IsNullOrEmpty(codigo))
        {
            gvCodigosSECOP.DataSource = vContratoService.ConsultarVariosCodigosSECOP(true, codigo, 10);
            gvCodigosSECOP.DataBind();
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if(! string.IsNullOrEmpty(Request.QueryString["idContrato"]))
                hfIdContrato.Value = Request.QueryString["idContrato"];

            gvCodigosSECOP.DataSource = vContratoService.ConsultarVariosCodigosSECOP(true, null,10);
            gvCodigosSECOP.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void toolBar_eventoGuardar(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();

            int count = 0;
            List<int> codigos = new List<int>();

            foreach (GridViewRow fila in gvCodigosSECOP.Rows)
            {
                if ((fila.Cells[0].FindControl("chbContiene") as CheckBox).Checked)
                {
                    count++;
                    codigos.Add(int.Parse(gvCodigosSECOP.DataKeys[fila.RowIndex].Value.ToString()));
                }
            }

            if (count > 0)
            {
                int result = 0;

                foreach (var item in codigos)
                result += vContratoService.AsociarCodigoSECOPContrato(int.Parse(hfIdContrato.Value), item);

                if (result > 0)
                {
                    string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                    string returnValues =
                            "<script language='javascript'> " +
                            "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";
                    this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
                }
                else
                    toolBar.MostrarMensajeError("Sucedio un Error al asociar los codigos.");
            }
            else
                toolBar.MostrarMensajeError("Debe seleccionar al menos un código SECOP");
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }


    }
    #endregion

}