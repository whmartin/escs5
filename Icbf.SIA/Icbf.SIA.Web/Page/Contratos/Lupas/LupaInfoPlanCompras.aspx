﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoPlanCompras.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoPlanCompras" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="gvConsecutivos" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%"
                                                DataKeyNames="IDPlanDeComprasContratos" CellPadding="8" Height="16px" Visible="true">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número consecutivo plan de compras" DataField="IDPlanDeCompras" />
                                                    <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                            </asp:GridView>
                </td>
            </tr>
            
        </table>
    </asp:Panel>
    
   </asp:Content>