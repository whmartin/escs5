﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaCalcularFecha : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/LupaInfoGarantias";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }
    
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoGuardar += toolBar_eventoGuardar;
            toolBar.EstablecerTitulos("Información Cálculo de Fechas", SolutionPage.Edit.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            string anio, mes, dia;
            anio = mes = dia = string.Empty;

            if(! string.IsNullOrEmpty(Request.QueryString["anio"]))
             anio = Request.QueryString["anio"];
            if(! string.IsNullOrEmpty(Request.QueryString["mes"]))
             mes = Request.QueryString["mes"];
            if(! string.IsNullOrEmpty(Request.QueryString["dia"]))
             dia = Request.QueryString["dia"];

            if (!string.IsNullOrEmpty(Request.QueryString["diasCalculados"]))
                txtDias.Text = Request.QueryString["diasCalculados"];

            if (!string.IsNullOrEmpty(Request.QueryString["mesesCalculados"]))
                txtMeses.Text = Request.QueryString["mesesCalculados"];

            if (! string.IsNullOrEmpty(Request.QueryString["tipoContrato"]))
                hfIdTipoContrato.Value = Request.QueryString["tipoContrato"];
           
          lblFechaInicioContrato.Text = string.Format("{0}/{1}/{2}",anio,mes,dia);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoGuardar(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        if (string.IsNullOrEmpty(txtDias.Text) || string.IsNullOrEmpty(txtMeses.Text))
        {
            toolBar.MostrarMensajeError("Debe ingresar un valor para el campo días y/o meses");
            return;
        }

        int dias = int.Parse(txtDias.Text);
        int meses = int.Parse(txtMeses.Text);

        if (dias < 0 || dias > 29)
        {
            toolBar.MostrarMensajeError("El valor  del campo  Días  debe estar entre 0 y 29");
            return;
        }

        if (meses < 0 || meses > 999)
        {
            toolBar.MostrarMensajeError("El valor  del campo  meses debe estar entre 0 y 999");
            return;
        }


        int diasIniciales = int.Parse(lblFechaInicioContrato.Text.Split('/')[2]);
        int mesesIniciales = int.Parse(lblFechaInicioContrato.Text.Split('/')[1]);
        int aniosIniciales = int.Parse(lblFechaInicioContrato.Text.Split('/')[0]);

        DateTime fechaInicial = new DateTime(aniosIniciales, mesesIniciales, diasIniciales);
        fechaInicial = fechaInicial.AddDays(dias);
        fechaInicial = fechaInicial.AddMonths(meses);
        fechaInicial = fechaInicial.AddDays(-1);

        if (! string.IsNullOrEmpty(hfIdTipoContrato.Value))
        {
            var tipoContrato = vContratoService.ConsultarTipoContrato(int.Parse(hfIdTipoContrato.Value));

            if ((tipoContrato.CodigoTipoContrato == "PREST_SERV_APO_GEST" || tipoContrato.CodigoTipoContrato == "PREST_SERV_PROF") && fechaInicial.Year > aniosIniciales )
            {
                toolBar.MostrarMensajeError("La vigencía Final es diferente a la vigencía inicial y el tipo de contrato es de prestación de servicios");
                return;
            }
        }

        SetSessionParameter("CalculoFecha.FechaFinal", fechaInicial.ToShortDateString());
        SetSessionParameter("CalculoFecha.Dias", dias.ToString());
        SetSessionParameter("CalculoFecha.Meses", meses.ToString() );

        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }
    #endregion

    protected void txtMeses_TextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        if (string.IsNullOrEmpty(txtDias.Text) || string.IsNullOrEmpty(txtMeses.Text))
        {
            toolBar.MostrarMensajeError("Debe ingresar un valor para el campo días y/o meses");
            return;
        }

        int dias = int.Parse(txtDias.Text);
        int meses = int.Parse(txtMeses.Text);

        if (dias < 0 || dias > 29)
        {
            toolBar.MostrarMensajeError("El valor  del campo  Días  debe estar entre 0 y 29");
            return;                        
        }

        if (meses < 0 || meses > 999)
        {
            toolBar.MostrarMensajeError("El valor  del campo  meses debe estar entre 0 y 999");
            return;            
        }

        int diasIniciales =  int.Parse(lblFechaInicioContrato.Text.Split('/')[2]);
        int mesesIniciales = int.Parse(lblFechaInicioContrato.Text.Split('/')[1]);
        int aniosIniciales = int.Parse(lblFechaInicioContrato.Text.Split('/')[0]);

        DateTime fechaInicial = new DateTime(aniosIniciales, mesesIniciales, diasIniciales);
        fechaInicial = fechaInicial.AddDays(dias);
        fechaInicial = fechaInicial.AddMonths(meses);
        fechaInicial = fechaInicial.AddDays(-1);
        lblFechaFinalContrato.Text = string.Format("{0}/{1}/{2}",fechaInicial.Year, fechaInicial.Month, fechaInicial.Day);
    }
}