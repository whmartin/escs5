using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de consulta a través de filtros para la entidad NumeroProcesos
/// </summary>
public partial class Page_NumeroProcesos_List : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/NumeroProcesos";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

        /// <summary>
        /// Método que realiza la búsqueda filtrada con múltiples criterios 
        /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvNumeroProcesos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegateLupa(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            //toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            gvNumeroProcesos.PageSize = PageSize();
            gvNumeroProcesos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Número de Proceso", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método para redirigir a la página detalle del registro seleccionado 
        /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            var dataKey = gvNumeroProcesos.DataKeys[rowIndex];
            if (dataKey != null)
            {
                string returnValues =
                    "<script language='javascript'> " +
                    "var pObj = Array();";

                string strValue = dataKey["IdNumeroProceso"].ToString();
                returnValues += "pObj[0] = '" + HttpUtility.HtmlDecode(strValue) + "';";
                strValue = dataKey["NumeroProcesoGenerado"].ToString();
                returnValues += "pObj[1] = '" + HttpUtility.HtmlDecode(strValue) + "';";
                strValue = dataKey["FechaAdjudicacion"].ToString();
                returnValues += "pObj[2] = '" + HttpUtility.HtmlDecode(strValue) + "';";
                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                               " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvNumeroProcesos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvNumeroProcesos.SelectedRow);
    }
    protected void gvNumeroProcesos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvNumeroProcesos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvNumeroProcesos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int? vNumeroProceso = null;
            Boolean? vInactivo = null;
            String vNumeroProcesoGenerado = null;
            int? vIdModalidadSeleccion = null;
            int? vIdRegional = null;
            int? vIdVigencia = null;
            //if (txtNumeroProceso.Text!= "")
            //{
            //    vNumeroProceso = Convert.ToInt32(txtNumeroProceso.Text);
            //}
            if (rblInactivo.SelectedValue!= "-1")
            {
                vInactivo = Convert.ToBoolean(rblInactivo.SelectedValue);
            }
            //if (txtNumeroProcesoGenerado.Text!= "")
            //{
            //    vNumeroProcesoGenerado = Convert.ToString(txtNumeroProcesoGenerado.Text);
            //}
            //if (ddlIdModalidadSeleccion.SelectedValue!= "-1")
            //{
            //    vIdModalidadSeleccion = Convert.ToInt32(ddlIdModalidadSeleccion.SelectedValue);
            //}
            if (ddlIdRegional.SelectedValue!= "-1")
            {
                vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            }
            if (ddlIdVigencia.SelectedValue!= "-1")
            {
                vIdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            }
            var myGridResults = vContratoService.ConsultarNumeroProcesoss(null,null, vInactivo, null, null, vIdRegional, vIdVigencia);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(NumeroProcesos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<NumeroProcesos, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList(); 
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("NumeroProcesos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("NumeroProcesos.Eliminado");
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.Items.Insert(0, new ListItem("Todos", "-1"));
            rblInactivo.SelectedValue = "-1";
            CargarComboRegional();
            CargarComboVigencia();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void CargarComboRegional()
    {
        ManejoControlesContratos.LlenarComboLista(ddlIdRegional, vContratoService.ConsultarRegionals(null, null), "IdRegional", "NombreRegional");
    }
    public void CargarComboVigencia()
    {
        List<Vigencia> lstVigencia = vRuboService.ConsultarVigencias(true);
        ddlIdVigencia.DataSource = lstVigencia;
        ddlIdVigencia.DataValueField = "IdVigencia";
        ddlIdVigencia.DataTextField = "AcnoVigencia";
        ddlIdVigencia.DataBind();
        ddlIdVigencia.Items.Insert(0, new ListItem("Seleccione", "-1"));
        ddlIdVigencia.SelectedIndex = -1;

    }

    protected void imgBcodigoUsuario_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}
