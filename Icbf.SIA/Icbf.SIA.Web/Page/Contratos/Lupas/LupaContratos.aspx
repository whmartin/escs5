﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaContratos.aspx.cs" Inherits="Page_Contratos_Lupas_LupaContratos" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
<div style="display: none">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </div>
<asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha Registro *
            </td>
            <td>
                N&#250;mero Proceso *
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fecha1" runat="server" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProceso" MaxLength="50"  ></asp:TextBox>
               <Ajax:FilteredTextBoxExtender ID="ftNumeroProceso" runat="server" TargetControlID="txtNumeroProceso"
                    FilterType="Custom, Numbers" ValidChars="0123456789" />

            </td>
        </tr>
        <tr class="rowB">
            <td>
                N&#250;mero Contrato *
            </td>
            <td>
                Modalidad *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25"  ></asp:TextBox>
               <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdModalidad"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categor&#237;a Contrato *
            </td>
            <td>
                Tipo Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlIdCategoriaContrato_SelectedIndexChanged"  ></asp:DropDownList>
            </td>
            <td>
                 <asp:DropDownList runat="server" ID="ddlIdTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px" OnPageIndexChanging="gvContrato_PageIndexChanging" OnSelectedIndexChanged="gvContrato_SelectedIndexChanged"
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                    <asp:Label ID="labelIdTipoContrato" Text='<%# Bind ("IdTipoContrato") %>' runat="server" Visible="false" />
                                    <asp:Label ID="labelTipoContrato" Text='<%# Bind ("TipoContrato") %>' runat="server" Visible="false" />
                                    <asp:Label ID="labelIdContrato" Text='<%# Bind ("IdContrato") %>' runat="server" Visible="false" /> 
                                    <asp:Label ID="labelRequiereActa" Text='<%# Bind ("RequiereActa") %>' runat="server" Visible="false" />
                                    <asp:Label ID="labelFechaInicioEjecucion" Text='<%# Bind ("FechaInicioEjecucion") %>' runat="server" Visible="false" />
                                    <asp:Label ID="labelFechaSuscripcion" Text='<%# Bind ("FechaSuscripcion") %>' runat="server" Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro" DataField="FechaRegistro" DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="N&#250;mero Proceso" DataField="NumeroProceso" />
                            <asp:BoundField HeaderText="N&#250;mero Contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="Modalidad" DataField="Modalidad" />
                            <asp:BoundField HeaderText="Categor&#237;a Contrato" DataField="CategoriaContrato" />

                             <asp:TemplateField HeaderText="Tipo Contrato" ItemStyle-HorizontalAlign="Center">
                             <ItemTemplate>
                                 <div style="word-wrap: break-word; width: 100px;">
                                        <%#Eval("TipoContrato")%>
                                 </div>
                             </ItemTemplate>
                             </asp:TemplateField>
<%--                            <asp:BoundField HeaderText="Tipo Contrato" DataField="TipoContrato" />--%>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

