﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaInfoProductos : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/LupaInfoProductos";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }
    protected void gvInfoRP_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInfoRP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }
    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            //toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.HabilitarBotonGuardar(false);

            gvProductos.PageSize = PageSize();
            gvProductos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Productos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarGrillaRP();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void CargarGrillaRP()
    {
        decimal vValorTotalRP = 0;
        CargarGrilla(gvProductos, GridViewSortExpression, true);
        

    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdContrato = 0;

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }
            //////////////////////////////////////////////////////////////////////////////////
            //////Inicio Procedimiento Obtener Productos 
            //////////////////////////////////////////////////////////////////////////////////
            List<PlanDeComprasContratos> consecutivosPlanCompras = vContratoService.ConsultarPlanDeComprasContratoss(vIdContrato, null);
            //List<WsContratosPacco.GetDetalleProductosServicio_Result> productos = new List<WsContratosPacco.GetDetalleProductosServicio_Result>();
            List<GetDetalleProductosServicioWSPacco> productos = new List<GetDetalleProductosServicioWSPacco>();
            if (consecutivosPlanCompras.Count() > 0)
            {
                var client = new WsContratosPacco.WSContratosPACCOSoapClient();
                

                // Carga grilla Plan de Compras y Rubros con webservice, filtrar por productos registrados en ProductoPlanComprasContrato
                

                decimal? sumatoriaProductos = 0;

                //#region ProductosDelPlanComprasPaso1
                List<PlanComprasProductos> productosContratos = new List<PlanComprasProductos>();
                List<PlanComprasRubrosCDP> rubrosContratos = new List<PlanComprasRubrosCDP>();
                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras)
                {
                    foreach (PlanComprasProductos p in vContratoService.ObtenerProductosPlanCompras(pc.IDPlanDeComprasContratos))
                    {
                        productosContratos.Add(p);
                    }

                    foreach (PlanComprasRubrosCDP r in vContratoService.ObtenerRubrosPlanCompras(pc.IDPlanDeComprasContratos))
                    {
                        rubrosContratos.Add(r);
                    }
                }
                //#endregion
                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras) //Recorro las vigencias y consecutivos asociados al plan de compras del contrato
                {
                    //#region FiltradosProductosWebService
                    foreach (WsContratosPacco.GetDetalleProductosServicio_Result pWs in client.GetListaDetalleProductoPACCO(pc.Vigencia, pc.IDPlanDeCompras)) //Recorro y obtengo los productos obtenidos del web service
                    {
                        foreach (PlanComprasProductos p in productosContratos) // Recorro los productos obtenidos en ProductosDelPlanComprasPaso1
                        {
                            GetDetalleProductosServicioWSPacco itemToAdd = new GetDetalleProductosServicioWSPacco();

                            if ((p.CodigoProducto == pWs.codigo_producto) && (p.NumeroConsecutivoPlanCompras == pWs.consecutivo)
                            && (p.IdDetalleObjeto == pWs.iddetalleobjetocontractual.ToString())) // Verifico que los productosContratos coincidan con los productos del webservice
                            {
                            itemToAdd.nombre_producto = pWs.nombre_producto;
                            itemToAdd.tipoProductoView = pWs.tipoProductoView;
                            itemToAdd.codigo_producto = pWs.codigo_producto;
                            itemToAdd.unidad_medida = pWs.unidad_medida;
                            itemToAdd.consecutivo = pWs.consecutivo;

                            if (p.IsReduccion == true)
                                itemToAdd.Modificacion = "Reducción";
                            else if (p.EsAdicion == true)
                                itemToAdd.Modificacion = "Adición";
                            else
                                itemToAdd.Modificacion = string.Empty;

                            if (p.ValorUnitario != null)
                                itemToAdd.valor_unitario = p.ValorUnitario;
                            else
                                itemToAdd.valor_unitario = pWs.valor_unitario;

                            if (p.CantidadCupos != null)
                                itemToAdd.cantidad = p.CantidadCupos;
                            else
                                itemToAdd.cantidad = pWs.cantidad;

                            if (p.Tiempo != null)
                                itemToAdd.tiempo = p.Tiempo;
                            else
                                itemToAdd.tiempo = pWs.tiempo;


                            if (!string.IsNullOrEmpty(p.UnidadTiempo))
                                itemToAdd.tipotiempoView = p.UnidadTiempo;
                            else
                                itemToAdd.tipoProductoView = pWs.tipoProductoView;

                            if (pWs.tipoProductoView.Equals("SERVICIO"))
                            {
                                itemToAdd.valor_total = itemToAdd.cantidad * itemToAdd.valor_unitario * itemToAdd.tiempo;
                                //pWs.valor_total = p.CantidadCupos * p.ValorUnitario * p.Tiempo;
                            }
                            else
                            {
                                itemToAdd.valor_total = itemToAdd.cantidad * itemToAdd.valor_unitario;
                                //pWs.valor_total = p.CantidadCupos * p.ValorUnitario;
                            }
                            sumatoriaProductos += itemToAdd.valor_total;
                            productos.Add(itemToAdd);
                        }
                        }
                    }
                    ////#endregion

                    
                }
            }

            

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin Procedimiento Obtener Productos 
            //////////////////////////////////////////////////////////////////////////////////
            var myGridResults = productos;
            gridViewsender.DataSource = myGridResults;
            gridViewsender.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected int PageSize()
    {
        Int32 vPageSize;
        if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
        {
            throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
        }
        return vPageSize;
    }

    protected String EmptyDataText()
    {
        String vEmptyDataText;
        if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
        {
            throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
        }
        vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
        return vEmptyDataText;
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    #endregion
}