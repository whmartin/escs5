﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaInfoGarantias : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/LupaInfoGarantias";
    #endregion

    #region Eventos
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
        else
        { 
            string sControlName = Request.Params.Get("__EVENTTARGET");
            switch (sControlName)
            {
                case "cphCont_hfPostbk":
                    CargarDatosIniciales();
                    break;
                default:
                    break;
            }
        }
    }
    
    protected void gvGarantia_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGarantia.PageIndex = e.NewPageIndex;
        CargarGrillaGarantias();
    }
    
    protected void gvGarantia_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
  
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    protected void gvGarantia_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Seleccionar")
        {
            int vIDGarantia = 0;

            if (!int.TryParse(gvGarantia.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IDGarantia"].ToString(), out vIDGarantia))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;

            }
            SetSessionParameter("Garantia.IDGarantia", vIDGarantia);
            ScriptManager.RegisterStartupScript(this, GetType(), "GestionarGarantias001", "GestionarGarantias()", true);
            //Response.Redirect("../../Contratos/GestionarGarantia/Add.aspx");
        }
        if (e.CommandName == "Visualizar")
        {

            int vIDGarantia = 0;
            int vIdSucursalAseguradoraContrato = 0;
            //int vIDContratoConsultado = 0;

            SetSessionParameter("Garantia.IDContratoConsultado", GetSessionParameter("Contrato.ContratosAPP"));

            if (!int.TryParse(gvGarantia.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IdSucursalAseguradoraContrato"].ToString(), out vIdSucursalAseguradoraContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;

            }

            if (!int.TryParse(gvGarantia.DataKeys[Convert.ToInt32(e.CommandArgument)].Values["IDGarantia"].ToString(), out vIDGarantia))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;

            }

            SetSessionParameter("Garantia.IDGarantia", vIDGarantia);
            SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", vIdSucursalAseguradoraContrato);

            ScriptManager.RegisterStartupScript(this, GetType(), "GetDetalleGarantias001", "GetDetalleGarantias()", true);
            //Response.Redirect("LupaGarantias.aspx");
        }

    }
    
    #endregion

    #region Métodos
    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            //toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.HabilitarBotonGuardar(false);

            gvGarantia.PageSize = PageSize();
            gvGarantia.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Garantía", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarGrillaGarantias();
            CargarGrillaHistorico(gvGarantiaHistorico, GridViewSortExpressionHistorico, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarGrillaGarantias()
    {
        CargarGrilla(gvGarantia, GridViewSortExpression, true);

        decimal vValorTotalGarantia = 0;

        foreach (GridViewRow fila in gvGarantia.Rows)
        {
            if (gvGarantia.DataKeys[fila.RowIndex].Values["FechaAprobacionGarantia"] == null || gvGarantia.DataKeys[fila.RowIndex].Values["FechaCertificacionGarantia"] == null)
            {
                if (fila.Cells[0].FindControl("btnSeleccionar") !=null)
	            {
		            ImageButton ibtnSeleccionar = new ImageButton();
	                ibtnSeleccionar = ((ImageButton) fila.Cells[0].FindControl("btnSeleccionar"));
	                ibtnSeleccionar.Visible = true;
	            }

            }
            if (!string.IsNullOrEmpty(fila.Cells[4].Text))
            {
                decimal vValorGarantia = 0;
                string valor = fila.Cells[4].Text.Replace("$", string.Empty);
                if (decimal.TryParse(valor, out vValorGarantia))
                {
                    fila.Cells[4].Text = vValorGarantia.ToString("$ #,###0.00##;($ #,###0.00##)");
                    vValorTotalGarantia += vValorGarantia;
                }
                
            }
        }

        txtValorTotalGarantias.Text = vValorTotalGarantia.ToString("$ #,###0.00##;($ #,###0.00##)");

    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdContrato = 0;

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            var myGridResults = vContratoService.ConsultarInfoGarantias(vIdContrato);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////
            gridViewsender.DataSource = myGridResults;
            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Garantia), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Garantia, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaHistorico(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int vIdContrato = 0;

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            var myGridResults = vContratoService.ConsultarInfoGarantiasHistoricoPorContrato(vIdContrato);
            gridViewsender.DataSource = myGridResults;

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpressionHistorico))
                {
                    GridViewSortDirectionHistorico = SortDirection.Ascending;
                }
                else if (GridViewSortExpressionHistorico != expresionOrdenamiento)
                {
                    GridViewSortDirectionHistorico = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Garantia), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<GarantiaHistorico, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirectionHistorico == SortDirection.Ascending)
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirectionHistorico = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirectionHistorico = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                    }

                    GridViewSortExpressionHistorico = expresionOrdenamiento;
                }
            }
            else
                gridViewsender.DataSource = myGridResults;

            gridViewsender.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected int PageSize()
    {
        Int32 vPageSize;
        if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
        {
            throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
        }
        return vPageSize;
    }

    protected String EmptyDataText()
    {
        String vEmptyDataText;
        if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
        {
            throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
        }
        vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
        return vEmptyDataText;
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirectionHistorico
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirectionH"];
        }
        set { ViewState["sortDirectionH"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpressionHistorico
    {
        get { return (string)ViewState["sortExpressionH"]; }
        set { ViewState["sortExpressionH"] = value; }
    }

    #endregion

}