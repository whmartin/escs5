﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master"
    CodeFile="LupaInfoContratista.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoContratista" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Contratistas Actuales
                </td>
            </tr>
             <tr class="rowAG">
                <td class="Cell">
                    <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero,IdEntidad"
                        GridLines="None" Width="100%" CellPadding="8" Height="16px" OnSorting="gvContratistas_Sorting"
                        AllowSorting="True" OnPageIndexChanging="gvContratistas_PageIndexChanging" OnSelectedIndexChanged="gvContratistas_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                    <ItemTemplate>
                                                <asp:LinkButton  ID="LinkButton1" runat="server" OnClick="btnOpcionGvContratistasClick"
                                            CommandName="VerSupervisor" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                <img alt="Detalle" src="../../../Image/btn/list.png" title="Historíco Representantes" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                            <asp:BoundField HeaderText="Identificación representante Legal" DataField="NumeroIDRepresentanteLegal" />
                            <asp:BoundField HeaderText="Información Representante" DataField="RepresentanteLegal" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Integrantes relacionados al consorcio o unión temporal
                </td>
            </tr>
            <tr class="rowAG">
                <td class="Cell">
                    <asp:GridView ID="gvIntegrantesConsorcio_UnionTemp" runat="server" AutoGenerateColumns="false"
                        GridLines="None" Width="100%" CellPadding="8" Height="16px">
                        <Columns>
                            <asp:BoundField HeaderText="Identificación Contratista" DataField="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Tipo de Persona" DataField="TipoPersonaNombre" />
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacionIntegrante" />
                            <asp:BoundField HeaderText="Información Integrante" DataField="Proveedor" />
                            <asp:BoundField HeaderText="Porcentaje Participación" DataField="PorcentajeParticipacion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
             <tr class="rowB">
                <td class="Cell">
                    Contratistas Historicos
                </td>
            </tr>
            <tr class="rowAG">
                <td class="Cell">
                    <asp:GridView ID="gvContratistasHistorico" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero,IdEntidad"
                        GridLines="None" Width="100%" CellPadding="8" Height="16px" OnSorting="gvContratistasHistorico_Sorting"
                        AllowSorting="True" OnPageIndexChanging="gvContratistasHistorico_PageIndexChanging" OnSelectedIndexChanged="gvContratistasHistorico_SelectedIndexChanged">
                        <Columns>
<%--                            <asp:TemplateField>
                                    <ItemTemplate>
                                                <asp:LinkButton  ID="LinkButton1" runat="server" OnClick="btnOpcionGvContratistasClick"
                                            CommandName="VerSupervisor" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                <img alt="Detalle" src="../../../Image/btn/list.png" title="Historíco Representantes" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                            <asp:BoundField HeaderText="Identificación representante Legal" DataField="NumeroIDRepresentanteLegal" />
                            <asp:BoundField HeaderText="Información Representante" DataField="RepresentanteLegal" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function GetDetalleContratistas() {
            window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/DetalleProveedor.aspx', "", 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetHistoricoSupervisores(idSupervisor) {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaRepresentanteLegal.aspx?idproveedor=' + idSupervisor, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

    </script>
</asp:Content>
