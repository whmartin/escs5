﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoAportesICBF.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoAportesICBF" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false" 
                                                DataKeyNames="IdAporteContrato" AllowSorting="true" OnSorting="gvAportesICBF_Sorting"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo de Aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Total Aporte" DataField="ValorAporte" SortExpression="ValorAporte" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripción Aporte Especie" DataField="DescripcionAporte" SortExpression="DescripcionAporte" />
                                                    <%--<asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center" >  
                                                        <ItemTemplate> 
                                                            <asp:LinkButton ID="btnEliminar" runat="server" Enabled="False" OnClick="btnEliminarAporteICBFClick" OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" >
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate> 
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                </td>
            </tr>
           
        </table>
    </asp:Panel>
    
   </asp:Content>
