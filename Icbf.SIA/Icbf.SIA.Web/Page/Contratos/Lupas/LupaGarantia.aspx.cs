﻿using System;
using System.Web.UI;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;

public partial class Page_Contratos_Lupas_LupaGarantia : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/LupaInfoGarantia";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }
    
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoGuardar += toolBar_eventoGuardar;
            toolBar.EstablecerTitulos("Información Garantías", SolutionPage.Edit.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString["idGarantia"]))
                hfIdGarantia.Value = Request.QueryString["idGarantia"];

            if (!string.IsNullOrEmpty(Request.QueryString["numeroGarantia"]))
                txtNumeroGarantia.Text = Request.QueryString["numeroGarantia"];

            if (! string.IsNullOrEmpty(Request.QueryString["fechaGarantia"]))
              txtFechaAprobacion.Text = Request.QueryString["fechaGarantia"];

            if(! string.IsNullOrEmpty(Request.QueryString["fechaInicio"]))
            {
                DateTime fechaInicio;

                if(DateTime.TryParse(Request.QueryString["fechaInicio"],out fechaInicio))
                {
                    hfFechaInicio.Value = fechaInicio.ToShortDateString();
                    CalendarExtenderFechaAprobacion.EndDate = fechaInicio;
                } 
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoGuardar(object sender, EventArgs e)
    {
        try
        {
            if(Guardar())
            {
                Garantia item = new Garantia();
                item.IDGarantia = int.Parse(hfIdGarantia.Value);
                item.FechaAprobacionGarantia = DateTime.Parse(txtFechaAprobacion.Text);
                item.NumeroGarantia = txtNumeroGarantia.Text;
                var result = vContratoService.ActualizarGarantia(item);

                if(result > 0)
                {
                    toolBar.LipiarMensajeError();

                    string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');

                    string returnValues =
                            "<script language='javascript'> " +
                            "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

                    this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
                }
                else
                    toolBar.MostrarMensajeError("Error en actualizar el registro, por favor intente nuevamente");
            }
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool Guardar()
    {
        bool isValid = true;

        DateTime fechaInicio;

        DateTime fechaAprobacion;

        if(DateTime.TryParse(txtFechaAprobacion.Text, out fechaAprobacion) && DateTime.TryParse(hfFechaInicio.Value, out fechaInicio))
        {
            if(fechaAprobacion > fechaInicio)
            {
                toolBar.MostrarMensajeError("La fecha de aprobación es mayor que la fecha de inicio del contrato");
                isValid = false;
            }
        }
        else
        {
            toolBar.MostrarMensajeError("El formato de la fecha es incorrecto");
            isValid = false;
        }

        return isValid;
    }
    #endregion
}