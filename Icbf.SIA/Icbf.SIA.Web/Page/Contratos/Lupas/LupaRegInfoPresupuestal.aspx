<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaRegInfoPresupuestal.aspx.cs" Inherits="Page_RegistroInformacionPresupuestal_List" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfidVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdVigencia" runat="server" />
    <asp:HiddenField ID="hfIdRegional" runat="server" />
    <asp:HiddenField ID="hfAnioVigencia" runat="server" />

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Vigencia Fiscal
                </td>
                <td class="Cell">
                    Regional ICBF*
                    <asp:RequiredFieldValidator runat="server" ID="rfvEntidadFinanciera" ControlToValidate="ddlRegionalICBF"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlVigenciaFiscal">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlRegionalICBF" AutoPostBack="False" OnSelectedIndexChanged="ddlRegionalICBF_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Número CDP
                </td>
                <td class="Cell">
                    <asp:Label ID="Label1" runat="server" Text="Área" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style="height: 27px">
                    <asp:TextBox runat="server" ID="txtNumeroCDP" MaxLength="25"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroCDP" runat="server" TargetControlID="txtNumeroCDP"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td class="Cell" style="height: 27px">
                    <asp:DropDownList runat="server" ID="ddlArea" Visible="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Valor Total Desde
                </td>
                <td class="Cell">
                    Valor Total Hasta
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtValorTotalDesde" MaxLength="26" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftValorTotalDesde" runat="server" TargetControlID="txtValorTotalDesde"
                        FilterType="Custom,Numbers" ValidChars="$,." />
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtValorTotalHasta" MaxLength="26" Width="250px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftValorTotalHasta" runat="server" TargetControlID="txtValorTotalHasta"
                        FilterType="Custom,Numbers" ValidChars="$,." />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" style="height: 26px">
                    Fecha CDP Desde
                    <asp:CompareValidator runat="server" ID="cvFechaDesdeValida" ControlToValidate="txtFechaCDPDesde$txtFecha"
                        Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha Desde Válida"
                        ForeColor="red" ValidationGroup="btnBuscar"></asp:CompareValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtFechaCDPDesde$txtFecha"
                        ControlToValidate="txtFechaCDPHasta$txtFecha" Display="Dynamic" Type="Date" ErrorMessage=" La Fecha CDP Hasta debe ser mayor o igual que la Fecha CDP Desde"
                        ForeColor="Red" Operator="GreaterThanEqual" SetFocusOnError="True" ValidationGroup="btnBuscar"></asp:CompareValidator>
                </td>
                <td class="Cell" style="height: 26px" valign="Top">
                    Fecha CDP Hasta
                    <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtFechaCDPHasta$txtFecha"
                        Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha Hasta Válida"
                        ForeColor="red" ValidationGroup="btnBuscar"></asp:CompareValidator>
                    <br />
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <uc1:fecha ID="txtFechaCDPDesde" runat="server" Width="80%" Enabled="True" Requerid="False" />
                </td>
                <td class="Cell">
                    <uc1:fecha ID="txtFechaCDPHasta" runat="server" Width="80%" Enabled="True" Requerid="False" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" style="height: 26px">
                    Cantidad de Registros a Mostrar en la Grilla
                </td>
                <td class="Cell" style="height: 26px">
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style="height: 26px">
                    <asp:DropDownList runat="server" ID="ddlNumeroRegistros">
                    </asp:DropDownList>
                </td>
                <td class="Cell" style="height: 26px">
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell" style="height: 26px">
                    <asp:Label runat="server" ID="lblResultadosCDP" Text="Resultados de CDP" Visible="False"></asp:Label>
                </td>
                <td class="Cell" style="height: 26px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegistroInformacionPresupuestal" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdEtlCDP,ValorCDP,RegionalICBF,VigenciaFiscal,NumeroCDP" CellPadding="0"
                        Height="16px" OnSorting="gvRegistroInformacionPresupuestal_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvRegistroInformacionPresupuestal_PageIndexChanging" OnSelectedIndexChanged="gvRegistroInformacionPresupuestal_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chbContiene" runat="server" OnCheckedChanged="chbContiene_CheckedChanged"
                                        AutoPostBack="true" />
                                </ItemTemplate>
                                <ControlStyle Width="25px" />
                                <FooterStyle Width="25px" />
                                <HeaderStyle Width="25px" />
                                <ItemStyle Width="25px" />
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Seleccionar" >
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                    Height="16px" Width="16px" ToolTip="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Vigencia Fiscal" DataField="VigenciaFiscal" SortExpression="VigenciaFiscal"
                                Visible="False" />
                            <asp:BoundField HeaderText="Regional ICBF" DataField="NombreRegionalICBF" SortExpression="NombreRegionalICBF" />
                            <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" SortExpression="NumeroCDP" />
                            <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" SortExpression="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP" SortExpression="ValorCDP" DataFormatString="{0:C}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
         <table width="90%" align="center" runat="server" id="PnCDPRubro" style="display:none">
            <tr class="rowB">
                        <td class="Cell" style="height: 26px">
                                                        <asp:Label ID="lblNumeroCDP" runat="server" />
                        </td>
                        <td class="Cell" style="height: 26px">
                                                       <asp:Label ID="lblVigenciaCDP" runat="server" />
                        </td>
                    </tr>
            <tr class="rowAG">
                <td colspan="2"> 
                    <asp:GridView runat="server" ID="gvRegistroInformacionPresupuestalRubros" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdEtlCDP" CellPadding="0"
                        Height="16px"  AllowSorting="True"
                        >
                        <Columns>
                            <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DependenciaAfectacionGastos"/>
                            <asp:BoundField HeaderText="Tipo Fuente Financiamiento" DataField="TipoFuenteFinanciamiento"/>
                            <asp:BoundField HeaderText="Posición Catalogo Gastos" DataField="PosicionCatalogoGastos"/>
                            <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                            <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
<%--                            <asp:BoundField HeaderText="Tipo Documento Soporte" DataField="TipoDocumentoSoporte"  />--%>
                            <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="TipoSituacionFondos"/>
                            <asp:BoundField HeaderText="Valor Rubro" DataField="ValorCDP"  DataFormatString="{0:C}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(function () {
            $("#<%= txtValorTotalDesde.ClientID%>").blur(function () {
                if ($("#<%= txtValorTotalDesde.ClientID%>").val().length > 17) {
                    $("#<%= txtValorTotalDesde.ClientID%>").val($("#<%= txtValorTotalDesde.ClientID%>").val().substring(0, 17));
                    $("#<%= txtValorTotalDesde.ClientID%>").formatCurrency();
                } else {
                    $("#<%= txtValorTotalDesde.ClientID%>").formatCurrency();
                }
            }).focus(function () {
                $("#<%= txtValorTotalDesde.ClientID%>").val($("#<%= txtValorTotalDesde.ClientID%>").formatCurrency().asNumber());
            });

            $("#<%= txtValorTotalHasta.ClientID%>").blur(function () {
                if ($("#<%= txtValorTotalHasta.ClientID%>").val().length > 17) {
                    $("#<%= txtValorTotalHasta.ClientID%>").val($("#<%= txtValorTotalHasta.ClientID%>").val().substring(0, 17));
                    $("#<%= txtValorTotalHasta.ClientID%>").formatCurrency();
                } else {
                    $("#<%= txtValorTotalHasta.ClientID%>").formatCurrency();
                }
            }).focus(function () {
                $("#<%= txtValorTotalHasta.ClientID%>").val($("#<%= txtValorTotalHasta.ClientID%>").formatCurrency().asNumber());
            });
        });

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
</asp:Content>
