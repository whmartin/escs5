using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición para la entidad AporteContrato
/// </summary>
public partial class Page_Contratos_Lupas_LupaAporteContrato : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            CargarDatosIniciales();
        }

        AsociarInfoCajas();
    }

    protected void rblTipoAporte_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        txtValorAporte.Enabled = true;
        rfvValorAporte.Enabled = true;
        txtFechaRP.Requerid = false;
        rfvnumerorp.Enabled = false;
        

        if (rblTipoAporte.SelectedItem.Text.Equals("Especie"))
        {
            txtDescripcionAporte.Enabled = true;
            lblDescripcionAporteEspecieCon.Visible = true;
            lblDescripcionAporteEspecieSin.Visible = false;
            rfvDescripcionAporte.Enabled = true;
            txtFechaRP.Visible = false;
            txtNumeroRP.Visible = false;
            lblFecharp.Visible = false;
            lblNumerorp.Visible = false;
            txtValorAporte.Text = string.Empty;

        }
        else
        {
            txtDescripcionAporte.Text = string.Empty;
            rfvDescripcionAporte.Enabled = false;
            txtDescripcionAporte.Enabled = false;
            lblDescripcionAporteEspecieCon.Visible = false;
            lblDescripcionAporteEspecieSin.Visible = true;
            rfvDescripcionAporte.Enabled = false;
            if (HfAporte.Value == "true")
            {
                decimal valor = Convert.ToDecimal(hfValor.Value);
                txtValorAporte.Text = string.Format("{0:$#,##0}", valor);
                txtValorAporte.Enabled = false;
            }
            
            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
            {
               
                txtFechaRP.Visible = true;
                txtNumeroRP.Visible = true;
                lblFecharp.Visible = true;
                lblNumerorp.Visible = true;
            }
        }

        activarRP();
        //txtValorAporte.Text = string.Empty;
        txtValorAporte.Focus();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    
   /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad AporteContrato
        /// </summary>
    private void Guardar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vResultado;
            int idContrato;
            int? idTercero = null;
            DateTime fecha = new DateTime(1900,1,1);
            
            if (string.IsNullOrEmpty(txtValorAporte.Text) || decimal.Parse(txtValorAporte.Text, NumberStyles.Currency) <= 0)
            {
                toolBar.MostrarMensajeError("Debe ingresar un valor mayor a cero (0)");
                return;
            }

            if (Request.QueryString["idContrato"] != null)
            {
                bool esIcbf = ViewState["Aportante"].ToString() == "Contratista"?false:true;
                idContrato = Convert.ToInt32(Request.QueryString["idContrato"]);

                if (ExisteContratoAporte(idContrato,Convert.ToBoolean(rblTipoAporte.SelectedValue),-1,esIcbf))
                {
                    toolBar.MostrarMensajeError("El contrato ya contiene este tipo de aporte asociado al contratista");
                    return;
                }
            }
            else
            {
                toolBar.MostrarMensajeError("No se encuentra número de contrato para validar");
                return;
            }

            decimal acomuladoaportes = vContratoService.ConsultarAporteContratos(null, null, null, null, null, idContrato, null, null, null,null).Sum(d => d.ValorAporte);
            acomuladoaportes += decimal.Parse(txtValorAporte.Text, NumberStyles.Currency);

            if (Convert.ToString(ViewState["Aportante"]) == "Contratista")
                 idTercero = Convert.ToInt32(hdIdEntidad.Value);

            AporteContrato vAporteContrato = new AporteContrato();

            if (Convert.ToString(ViewState["Aportante"]) == "Contratista")
                vAporteContrato.AportanteICBF = false;
            else
                vAporteContrato.AportanteICBF = true;
            vAporteContrato.NumeroIdentificacionICBF = Convert.ToString(txtNumeroIdentificacion.Text).Trim();
            vAporteContrato.ValorAporte = decimal.Parse(txtValorAporte.Text, NumberStyles.Currency);
            vAporteContrato.DescripcionAporte = Convert.ToString(txtDescripcionAporte.Text).Trim();
            if (rblTipoAporte.SelectedItem.Text.Equals("Dinero"))
                vAporteContrato.AporteEnDinero = true;
            else
                vAporteContrato.AporteEnDinero = false;
            vAporteContrato.IdContrato = idContrato;
            vAporteContrato.Estado = Convert.ToBoolean(true);
            vAporteContrato.IDEntidadProvOferente = idTercero;
            if (txtFechaRP.Date != fecha.Date)
                vAporteContrato.FechaRP = txtFechaRP.Date;
            else
                vAporteContrato.FechaRP = null;
            if (txtNumeroRP.Text != String.Empty)
                vAporteContrato.NumeroRP = txtNumeroRP.Text;
            else
                vAporteContrato.NumeroRP = null;

            vAporteContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
            vResultado = vContratoService.InsertarAporteContrato(vAporteContrato);
            
            if (vResultado == 0)
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            else if (vResultado >= 1)
            {
                GetScriptCloseDialogCallback(string.Empty);
            }    
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            String valor = Request.QueryString["Aportante"];
            if (valor == null)
            {
                toolBar.MostrarMensajeError("No ha enviado la información suficiente para el proceso");
                return;
            }

            ViewState["Aportante"] = valor;

            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            String Titulo = null;
            if (Convert.ToString(ViewState["Aporte"]) == "Contratista")
                Titulo = "Aporte Contratista";
            else
                Titulo = "Aporte ICBF";

            toolBar.EstablecerTitulos(Titulo, SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        Retornar();
    }

    /// <summary>
   /// Método de carga de listas y valores por defecto 
   /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
         ftValorAporte.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();

            rblTipoAporte.Items.Insert(0, new ListItem("Dinero", "true"));
            rblTipoAporte.Items.Insert(1, new ListItem("Especie", "false")); 

            if (Convert.ToString(ViewState["Aportante"]) == "Contratista")
            {
                imgTipoIdentificacionContratista.Visible = true;
                txttipoAportante.Text = "Contratista";
                rblTipoAporte.Enabled = false;
                rfvTipoAporte.Enabled = false;
                cvrbltipoaporte.Enabled = false;
            }
            else
            {
                CargarInfoICBF();
                txtDescripcionAporte.Text = string.Empty;
                rfvDescripcionAporte.Enabled = true;
                txtDescripcionAporte.Enabled = true;
                lblDescripcionAporteEspecieCon.Visible = true;
                lblDescripcionAporteEspecieSin.Visible = false;
                rfvDescripcionAporte.Enabled = true;
                txtValorAporte.Enabled = true;
                rblTipoAporte.Items.FindByText("Especie").Selected = true;
                rblTipoAporte.Enabled = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Métodoque carga los datos cuando el aporte es ICBF
    /// </summary>
    private void CargarInfoICBF()
    {
        imgTipoIdentificacionContratista.Visible = false;
        hdTipoIdentificacion.Value = "NIT";
        hdNumeroIdentificacion.Value = "899999239";
        hdInformacionAportante.Value = "ICBF";
        txttipoAportante.Text = "ICBF";
    }


    private bool ExisteContratoAporte(int pIdContrato, Boolean pAporteEnDinero, Decimal pValorAporte, bool pAportanteICBF)
    {
        return vContratoService.ExisteContratoAporte(pIdContrato,pAporteEnDinero,pValorAporte, pAportanteICBF);
    }

    private void AsociarInfoCajas()
    {
        txtNumeroIdentificacion.Text = hdNumeroIdentificacion.Value;
        txtTipoIdentificacion.Text = hdTipoIdentificacion.Value;
        txtInformacionAportante.Text = hdInformacionAportante.Value;
    }

    private void Retornar()
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    protected void btnCargarTipoAporte_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        rblTipoAporte.Enabled = true;
        rfvTipoAporte.Enabled = true;
        cvrbltipoaporte.Enabled = true;
    }

    protected void txtDescripcionAporte_OnTextChanged(object sender, EventArgs e)
    {
        activarRP();
    }

    protected void txtValorAporte_OnTextChanged(object sender, EventArgs e)
    {
        activarRP();
    }

    private void activarRP()
    {
        toolBar.LipiarMensajeError();
        if (rblTipoAporte.SelectedItem.Text.Equals("Dinero"))
        {
            if (Convert.ToString(ViewState["Aportante"]) == "Contratista")
            {
                txtFechaRP.Requerid = false;
                rfvnumerorp.Enabled = false;
            }
        }
    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["AporteIcbf"]))
            HfAporte.Value = Request.QueryString["AporteIcbf"];
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            hfIdContrato.Value = Request.QueryString["idContrato"];

    }
}
