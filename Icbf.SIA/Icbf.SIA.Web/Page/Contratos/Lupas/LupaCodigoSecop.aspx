﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaCodigoSecop.aspx.cs" Inherits="Page_Contratos_Lupas_LupaCodigoSECOP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
    <asp:HiddenField ID="hfIdContrato" runat="server" />
        <table width="90%" align="center">
            
              <tr class="rowB">
                <td>
                    Código SECOP
                    </td>
                <td>
                 <asp:TextBox Width="600px" ID="txtCodigosSECOP" runat="server" />
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:GridView ID="gvCodigosSECOP" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IdCodigoSECOP" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Código SECOP" DataField="CodigoSECOP" />
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                        <asp:CheckBox ID="chbContiene" runat="server"  />
                                                        </ItemTemplate>
                                                        <ControlStyle Width="25px" />
                                                        <FooterStyle Width="25px" />
                                                        <HeaderStyle Width="25px" />
                                                        <ItemStyle Width="25px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>

                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>