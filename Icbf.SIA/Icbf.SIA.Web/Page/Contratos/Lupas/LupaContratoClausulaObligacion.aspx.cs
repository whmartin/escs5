﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega la información y permite la selección de una cláusula de obligación
/// </summary>
public partial class Page_Contratos_Lupas_LupaContratoClausulaObligacion : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            BuscarClasulasPorIdTipoContrato();
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            gvClausulaContrato.PageSize = PageSize();
            gvClausulaContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Clausulas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvClausulaContrato.DataKeys[gvClausulaContrato.SelectedRow.RowIndex].Value.ToString()) + "';";
            for (int c = 1; c < 3; c++)
            {
                if (gvClausulaContrato.Rows[gvClausulaContrato.SelectedIndex].Cells[c].Text != "&nbsp;")
                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvClausulaContrato.Rows[gvClausulaContrato.SelectedIndex].Cells[c].Text) + "';";
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            
            String vIdContrato = null;
            String vNombreClausula = null;
            int? vTipoClausula = null;
            String vOrden = null;
            String vDescripcionClausula = null;
            if (txtIdContrato.Text != "")
            {
                vIdContrato = Convert.ToString(txtIdContrato.Text);
            }
            if (txtNombreClausula.Text != "")
            {
                vNombreClausula = Convert.ToString(txtNombreClausula.Text);
            }
            if (txtTipoClausula.Text != "")
            {
                vTipoClausula = Convert.ToInt32(txtTipoClausula.Text);
            }
            if (txtOrden.Text != "")
            {
                vOrden = Convert.ToString(txtOrden.Text);
            }
            if (txtDescripcionClausula.Text != "")
            {
                vDescripcionClausula = Convert.ToString(txtDescripcionClausula.Text);
            }
            gvClausulaContrato.DataSource = vContratoService.ConsultarGestionarClausulasContratos(null, vIdContrato, vNombreClausula, vTipoClausula, vOrden, null, vDescripcionClausula);
            gvClausulaContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvClausulaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvClausulaContrato.SelectedRow);
    }

    protected void gvClausulaContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvClausulaContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("GestionarClausulasContrato.Eliminado").ToString() == "1")
                RemoveSessionParameter("GestionarClausulasContrato.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void BuscarClasulasPorIdTipoContrato()
    {
        ContratoService objContratoService = new ContratoService();

        string stringIdContrato = GetSessionParameter("GestionarClausulaContrato.IdContrato").ToString();
        var result = from gestionarClausulasContrato in objContratoService.ConsultarGestionarClausulasContratos(null,
                        stringIdContrato, null, null, null, null, null)
                 orderby gestionarClausulasContrato.OrdenNumero
                 select new
                 {
                         gestionarClausulasContrato.IdGestionClausula,
                         gestionarClausulasContrato.Orden,
                         gestionarClausulasContrato.DescripcionClausula,
                         gestionarClausulasContrato.OrdenNumero
                     };

        if (result != null)
        {
            gvClausulaContrato.DataSource = result.ToList();
            gvClausulaContrato.DataBind();
            gvClausulaContrato.Visible = true;
        }
    }
}