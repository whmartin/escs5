﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using System.Data;

/// <summary>
/// Página que despliega la información y permite la selección de un contrato
/// </summary>
public partial class Page_Contratos_Lupas_LupaContratos : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            gvContrato.PageSize = PageSize();
            gvContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Contratos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            DateTime? vFechaRegistro = null;
            String vNumeroProceso = null;
            string vNumeroContrato = null;
            int? vIdModalidad = null;
            int? vIdCategoriaContrato = null;
            int? vIdTipoContrato = null;
            string vClaseContrato = "N";
            if (txtNumeroProceso.Text != "")
            {
                vNumeroProceso = Convert.ToString(txtNumeroProceso.Text);
            }
            if (txtNumeroContrato.Text != "")
            {
                vNumeroContrato = txtNumeroContrato.Text;
            }
            if (ddlIdModalidad.SelectedValue != "-1")
            {
                vIdModalidad = Convert.ToInt32(ddlIdModalidad.SelectedValue);
            }
            if (ddlIdCategoriaContrato.SelectedValue != "-1")
            {
                vIdCategoriaContrato = Convert.ToInt32(ddlIdCategoriaContrato.SelectedValue);
            }
            if (ddlIdTipoContrato.SelectedValue != "-1")
            {
                vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            }
            if (this.fecha1.Date.ToShortDateString() != "01/01/1900")
                vFechaRegistro = fecha1.Date;
            if (Request.QueryString["cc"] != null)
                vClaseContrato = Request.QueryString["cc"];
            DataTable dtContratos = vContratoService.ConsultarContratosLupa(vFechaRegistro, vNumeroProceso, vNumeroContrato, vIdModalidad, vIdCategoriaContrato, vIdTipoContrato, vClaseContrato);
            gvContrato.DataSource = dtContratos;
            gvContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ContratoService objContratoService = new ContratoService();
            /* Modalidad */
            ManejoControlesContratos.LlenarComboLista(ddlIdModalidad,objContratoService.ConsultarModalidadSeleccions(null, null, true),"IdModalidad","Nombre");
            /*Categoria Contrato */
            ManejoControlesContratos.LlenarComboLista(ddlIdCategoriaContrato,objContratoService.ConsultarCategoriaContratos(null,null,true),"IdCategoriaContrato","NombreCategoriaContrato");
            /*Tipo Contrato*/
            ManejoControlesContratos.InicializarCombo(ddlIdTipoContrato);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlIdCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdCategoriaContrato.SelectedValue != "-1")
        {
            ContratoService objContratoService = new ContratoService();
            ManejoControlesContratos.LlenarComboLista(ddlIdTipoContrato,objContratoService.ConsultarTipoContratos(null, int.Parse(ddlIdCategoriaContrato.SelectedValue), true,null,null,null,null,null),"IdTipoContrato","NombreTipoContrato");
        }
        else
        {
            ManejoControlesContratos.InicializarCombo(ddlIdTipoContrato);
        }
    }
    protected void gvContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContrato.SelectedRow);
    }
    protected void gvContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvContrato.DataKeys[gvContrato.SelectedRow.RowIndex].Value.ToString()) + "';";
            SetSessionParameter("GestionarClausulaContrato.IdContrato", ((Label)pRow.FindControl("labelIdContrato")).Text);

            for (int c = 1; c < 7; c++)
            {
                if (gvContrato.Rows[gvContrato.SelectedIndex].Cells[c].Text != "&nbsp;")
                    {
                        if(c == 3)
                        {
                            SetSessionParameter("GestionarClausulaContrato.NumeroContrato", HttpUtility.HtmlDecode(gvContrato.Rows[gvContrato.SelectedIndex].Cells[c].Text));
                        }
                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvContrato.Rows[gvContrato.SelectedIndex].Cells[c].Text) + "';";
                    }
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            SetSessionParameter("GestionarClausulaContrato.IdTipoContrato", ((Label)pRow.FindControl("labelIdTipoContrato")).Text);
            returnValues += "pObj[" + (7) + "] = '"+ ((Label)pRow.FindControl("labelIdTipoContrato")).Text+"';";

            SetSessionParameter("GestionarClausulaContrato.RequiereActa", ((Label)pRow.FindControl("labelRequiereActa")).Text);
            returnValues += "pObj[" + (8) + "] = '" + ((Label)pRow.FindControl("labelRequiereActa")).Text + "';";

            SetSessionParameter("UnidadMedida.FechaInicioEjecucion", ((Label)pRow.FindControl("labelFechaInicioEjecucion")).Text);
            returnValues += "pObj[" + (9) + "] = '" + ((Label)pRow.FindControl("labelFechaInicioEjecucion")).Text + "';";

            SetSessionParameter("Clausula.FechaSuscripcion", ((Label)pRow.FindControl("labelFechaSuscripcion")).Text);
            returnValues += "pObj[" + (10) + "] = '" + ((Label)pRow.FindControl("labelFechaSuscripcion")).Text + "';";

            SetSessionParameter("Clausula.TipoContrato", ((Label)pRow.FindControl("labelTipoContrato")).Text);
            returnValues += "pObj[" + (11) + "] = '" + ((Label)pRow.FindControl("labelTipoContrato")).Text + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}