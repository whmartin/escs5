﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;

/// <summary>
/// Página que despliega la información y permite la selección de un tercero 
/// </summary>
public partial class Page_Contratos_Lupas_LupaRepresentanteLegal : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    OferenteService vOferenteService = new OferenteService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.OcultarBotonBuscar(true);
            gvProveedor.PageSize = PageSize();
            gvProveedor.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos(" Histórico de Representante Legal", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int idproveedorContrato = Convert.ToInt32(Request.QueryString["idproveedor"].ToString());
            string vUsuario = GetSessionUser().NombreUsuario;
            List<Tercero> lista = vProveedorService.ConsultarHistoricoRepresentanteLegal(idproveedorContrato);
            if (lista.Count > 0)
            {
                gvProveedor.DataSource = lista;
                SetSessionParameter("gvProveedor.DataSource", gvProveedor.DataSource);
                gvProveedor.DataBind();
            }
            else
            {
                gvProveedor.DataSource = null;
                gvProveedor.DataBind();
                toolBar.MostrarMensajeError("No existe histórico asociado al representate legal");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProveedor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedor.PageIndex = e.NewPageIndex;
        CargarDatosIniciales();
    }
    
    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    
    protected List<Tercero> SortList(List<Tercero> data, bool isPageIndexChanging)
    {
        List<Tercero> result = new List<Tercero>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }

    protected void gvProveedor_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvProveedor.PageIndex;

        if (GetSessionParameter("gvContactoEntidad.DataSource") != null)
        {
            if (gvProveedor.DataSource == null) { gvProveedor.DataSource = (List<Tercero>)GetSessionParameter("gvProveedor.DataSource"); }
            gvProveedor.DataSource = SortList((List<Tercero>)gvProveedor.DataSource, false);
            gvProveedor.DataBind();
        }
        gvProveedor.PageIndex = pageIndex;
    }
}