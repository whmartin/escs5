﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;

/// <summary>
/// Página que despliega la información y permite la selección de un tercero 
/// </summary>
public partial class Page_Contratos_Lupas_LupaTercero : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    OferenteService vOferenteService = new OferenteService();
    bool _sorted = false;

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            gvProveedor.PageSize = PageSize();
            gvProveedor.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Terceros", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int vIdTipoPersona = 0;
            int vIdTipoDocIdentifica = 0;
            string vNumeroIdentificacion = "";
            string vTercero = "";
            int vEstado = 0;

            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (ddlTipoDoc.SelectedValue != "-1")
            {
                vIdTipoDocIdentifica = int.Parse(ddlTipoPersona.SelectedValue);
            }
            if (txtNumeroDoc.Text != "")
            {
                vNumeroIdentificacion = txtNumeroDoc.Text.Trim();
            }
            if (txtTercero.Text != "")
            {
                vTercero = txtTercero.Text;
            }
            if (ddlEstado.SelectedValue != "-1")
            {
                vEstado = int.Parse(ddlEstado.SelectedValue);
            }
            else
            {
                vEstado = 3;
            }

            string vUsuario = GetSessionUser().NombreUsuario;
            List<Tercero> lista = vProveedorService.ConsultarTerceros(vIdTipoDocIdentifica, vEstado, vIdTipoPersona, vNumeroIdentificacion, vUsuario);
            if (lista.Count > 0)
            {
                gvProveedor.DataSource = lista;
                SetSessionParameter("gvProveedor.DataSource", gvProveedor.DataSource);
                gvProveedor.DataBind();
            }
            else
            {
                gvProveedor.DataSource = null;
                gvProveedor.DataBind();
                toolBar.MostrarMensajeError("No se encuentran terceros registrados por este usuario");
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
            ManejoControles.LlenarEstadosTercero(ddlEstado, "-1", true);
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlEstado.SelectedValue = "6";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        string idTipoPersona = new GeneralWeb().GetSessionUser().IdTipoPersona.ToString();
        TipoPersona tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlTipoPersona.SelectedValue));
        if (tipoper.CodigoTipoPersona == "001")
        {
            ManejoControles.LlenarTipoDocumentoRL(ddlTipoDoc, "-1", true);
        }
        else
        {
            ManejoControles.LlenarTipoDocumentoN(ddlTipoDoc, "-1", true);
        }
    }
    protected void gvProveedor_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProveedor.SelectedRow);
    }
    protected void gvProveedor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedor.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvProveedor.DataKeys[gvProveedor.SelectedRow.RowIndex].Value.ToString()) + "';";

            for (int c = 1; c < 7; c++)
            {
                if (gvProveedor.Rows[gvProveedor.SelectedIndex].Cells[c].Text != "&nbsp;")
                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvProveedor.Rows[gvProveedor.SelectedIndex].Cells[c].Text) + "';";
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            SetSessionParameter("RelacionarContratistas.PrimerNombre", ((Label)pRow.FindControl("labelPrimerNombre")).Text);
            returnValues += "pObj[" + (7) + "] = '" + ((Label)pRow.FindControl("labelPrimerNombre")).Text + "';";

            SetSessionParameter("RelacionarContratistas.SegundoNombre", ((Label)pRow.FindControl("labelSegundoNombre")).Text);
            returnValues += "pObj[" + (8) + "] = '" + ((Label)pRow.FindControl("labelSegundoNombre")).Text + "';";

            SetSessionParameter("RelacionarContratistas.PrimerApellido", ((Label)pRow.FindControl("labelPrimerApellido")).Text);
            returnValues += "pObj[" + (9) + "] = '" + ((Label)pRow.FindControl("labelPrimerApellido")).Text + "';";

            SetSessionParameter("RelacionarContratistas.SegundoApellido", ((Label)pRow.FindControl("labelSegundoApellido")).Text);
            returnValues += "pObj[" + (10) + "] = '" + ((Label)pRow.FindControl("labelSegundoApellido")).Text + "';";

            SetSessionParameter("RelacionarContratistas.SegundoApellido", ((Label)pRow.FindControl("labelSegundoApellido")).Text);
            returnValues += "pObj[" + (10) + "] = '" + ((Label)pRow.FindControl("labelSegundoApellido")).Text + "';";

            SetSessionParameter("Garantia.IdTercero", ((Label)pRow.FindControl("labelIdTercero")).Text);
            returnValues += "pObj[" + (11) + "] = '" + ((Label)pRow.FindControl("labelIdTercero")).Text + "';";

            SetSessionParameter("Garantia.RazonSocial", ((Label)pRow.FindControl("labelRazonSocial")).Text);
            returnValues += "pObj[" + (12) + "] = '" + ((Label)pRow.FindControl("labelRazonSocial")).Text + "';";

            SetSessionParameter("Garantia.NumeroIdentificacion", ((Label)pRow.FindControl("labelNumeroIdentificacion")).Text);
            returnValues += "pObj[" + (13) + "] = '" + ((Label)pRow.FindControl("labelNumeroIdentificacion")).Text + "';";

            returnValues += "pObj[" + (14) + "] = '" + ((Label)pRow.FindControl("labelEmail")).Text + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";
            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private string GridViewSortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = "ASC";
            }
            return ViewState["SortDirection"].ToString();
        }

        set
        {
            ViewState["SortDirection"] = value;
        }

    }
    private string GridViewSortExpression
    {
        get
        {
            return ViewState["SortExpression"] as string ?? string.Empty;
        }

        set
        {
            ViewState["SortExpression"] = value;
        }

    }
    protected List<Tercero> SortList(List<Tercero> data, bool isPageIndexChanging)
    {
        List<Tercero> result = new List<Tercero>();
        if (data != null)
        {
            if (GridViewSortExpression != string.Empty)
            {
                if (data.Count > 0)
                {
                    PropertyInfo[] propertys = data[0].GetType().GetProperties();
                    foreach (PropertyInfo p in propertys)
                    {
                        if (p.Name == GridViewSortExpression)
                        {
                            if (GridViewSortDirection == "ASC")
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "DESC";
                                }
                            }
                            else
                            {
                                if (isPageIndexChanging)
                                {
                                    result = data.OrderBy(key =>
                                    p.GetValue(key, null)).ToList();
                                }
                                else
                                {
                                    result = data.OrderByDescending(key => p.GetValue(key, null)).ToList();
                                    GridViewSortDirection = "ASC";
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        _sorted = true;
        return result;
    }

    protected void gvProveedor_Sorting(object sender, GridViewSortEventArgs e)
    {
        GridViewSortExpression = e.SortExpression;
        int pageIndex = gvProveedor.PageIndex;

        if (GetSessionParameter("gvContactoEntidad.DataSource") != null)
        {
            if (gvProveedor.DataSource == null) { gvProveedor.DataSource = (List<Tercero>)GetSessionParameter("gvProveedor.DataSource"); }
            gvProveedor.DataSource = SortList((List<Tercero>)gvProveedor.DataSource, false);
            gvProveedor.DataBind();
        }
        gvProveedor.PageIndex = pageIndex;
    }
}