using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using ICBF.MasterData.Entity;
using System.Data;
using System.Globalization;


/// <summary>
/// Página de consulta a través de filtros para la entidad RegistroInformacionPresupuestal
/// </summary>
public partial class Page_RegistroInformacionPresupuestalEnLinea_List : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/RegistroInformacionPresupuestal";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    IntegrationService vIntegracionService = new IntegrationService();
    public const int vNumeroRegistros = 10;

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            CargarDatosIniciales();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
            Guardar();
    }

    protected void gvRegistroInformacionPresupuestal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroInformacionPresupuestal.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, false);
    }

    protected void gvRegistroInformacionPresupuestal_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vVigenciaFiscal=0;
            int vRegionalICBF=0;
            string vNumeroCDP = null;
            int? vArea = null;
            Decimal? vValorTotalDesde = null;
            Decimal? vValorTotalHasta = null;
            DateTime? vFechaCDPDesde = null;
            DateTime? vFechaCDPHasta = null;
            string error = string.Empty;
            int vRegistros = 10;
            bool flag = false;

            toolBar.QuitarMensajeError();

            if (ddlVigenciaFiscal.SelectedValue != "-1")
                vVigenciaFiscal = Convert.ToInt32(ddlVigenciaFiscal.SelectedItem.Text);
            
            if (ddlRegionalICBF.SelectedValue != "-1")
                vRegionalICBF = Convert.ToInt32(ddlRegionalICBF.SelectedValue);
            
            if (txtNumeroCDP.Text != "")
                vNumeroCDP = txtNumeroCDP.Text;

            if (flag)
            {
                if (!string.IsNullOrEmpty(error))
                {
                    toolBar.MostrarMensajeError(error);
                    gridViewsender.Visible = false;
                    return;

                }

            }

            if (vNumeroCDP.Length >= 3)
            {
                string vigenciaCDP = vNumeroCDP.Substring(vNumeroCDP.Length - 2, 2);
                string vigencia = ddlVigenciaFiscal.SelectedItem.Text.Substring(ddlVigenciaFiscal.SelectedItem.Text.Length - 2, 2);

                if (vigencia != vigenciaCDP)
                {
                    toolBar.MostrarMensajeError("La vigencia del CDP a buscar es diferente a la vigencia asociada al contrato.");
                    return;
                }
            }
            else
            {
                toolBar.MostrarMensajeError("El formato del número del CDP es incorrecto");
                return;
            }

            ConsultaCDPMaestro  myGridResults;

            if (RbtTipoBusqueda.SelectedValue == "1")
            {
                var myGridResultsAll = vContratoService.ConsultarCDPConRubros(vVigenciaFiscal, vRegionalICBF, vNumeroCDP, vArea, vValorTotalDesde, vValorTotalHasta, vFechaCDPDesde, vFechaCDPHasta, vRegistros);
                myGridResults = myGridResultsAll.Value;
                hfIdContratoCDP.Value = myGridResultsAll.Key.ToString();
            }
            else
            {
                    myGridResults = vIntegracionService.ObtenerCDP(vNumeroCDP, vRegionalICBF);   
            }

            if (myGridResults != null && !string.IsNullOrEmpty(myGridResults.NumeroCDP))
            {
                List<ConsultaCDPMaestro> items = new List<ConsultaCDPMaestro>();
                items.Add(myGridResults);

                gridViewsender.Visible = true;
                gridViewsender.DataSource = items;

                toolBar.HabilitarBotonGuardar(true);

                if (expresionOrdenamiento != null)
                {
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                        GridViewSortDirection = SortDirection.Ascending;
                    else if (GridViewSortExpression != expresionOrdenamiento)
                        GridViewSortDirection = SortDirection.Descending;

                    if (items != null)
                    {
                        var param = Expression.Parameter(typeof(ConsultaCDPMaestro), expresionOrdenamiento);

                        var prop = Expression.Property(param, expresionOrdenamiento);

                        var sortExpression = Expression.Lambda<Func<ConsultaCDPMaestro, object>>(Expression.Convert(prop, typeof(object)), param);

                        if (GridViewSortDirection == SortDirection.Ascending)
                        {
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = items.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                                gridViewsender.DataSource = items.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = items.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                                gridViewsender.DataSource = items.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }

                }
                else
                {
                    gridViewsender.DataSource = items;
                }

                gvRegistroInformacionPresupuestalRubros.DataSource = items.First().detalle;

                gridViewsender.DataBind();
                gvRegistroInformacionPresupuestalRubros.DataBind();
            }
            else
            {
                toolBar.MostrarMensajeError("La busqueda no arrojo resultados, por favor intente nuevamente.");
                List<ConsultaCDPMaestro> items = new List<ConsultaCDPMaestro>();
                gvRegistroInformacionPresupuestal.DataSource = items;
                gvRegistroInformacionPresupuestal.DataBind();
                List<ConsultaCDPDetalle> itemsDetalle = new List<ConsultaCDPDetalle>();
                gvRegistroInformacionPresupuestalRubros.DataSource = itemsDetalle;
                gvRegistroInformacionPresupuestalRubros.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvRegistroInformacionPresupuestal, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.HabilitarBotonGuardar(false);
            gvRegistroInformacionPresupuestal.PageSize = PageSize();
            gvRegistroInformacionPresupuestal.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registro Información Presupuestal", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaVigencia();
            CargarListaRegional();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {
        var misVigencias = vRuboService.ConsultarVigencias(true);

        if(! string.IsNullOrEmpty(hfIdVigencia.Value))
        {
          int idVigencia = int.Parse(hfIdVigencia.Value);
          misVigencias = misVigencias.Where(e => e.IdVigencia == idVigencia).ToList();
        }
        else if (! string.IsNullOrEmpty(hfAnioVigencia.Value))
        {
            int anioVigencia = int.Parse(hfAnioVigencia.Value);
            misVigencias = misVigencias.Where(e => e.AcnoVigencia == anioVigencia).ToList();
        }

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscal, misVigencias, "IdVigencia", "AcnoVigencia");
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();

        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {
            int? idRegional;

            if (!string.IsNullOrEmpty(hfIdRegional.Value))
                idRegional = Convert.ToInt32(hfIdRegional.Value);
            else
                idRegional = usuario.IdRegional;

                Regional vRegional = new Regional();
                
                vRegional = vRuboService.ConsultarRegional(idRegional);
                
                List<Regional> vListRegional = new List<Regional>();

                vListRegional = vRuboService.ConsultarRegionalPCIs(vRegional.CodigoRegional, null);
                
                if(vListRegional != null && vListRegional.Count > 0)
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF,vListRegional, "IdRegional", "CodigoNombreRegional");
                else
                {
                    vListRegional = vRuboService.ConsultarRegionalPCIs(null, null);
                    ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF,vListRegional, "IdRegional", "CodigoNombreRegional");
                }

                if (ddlRegionalICBF.Items.Count > 0)
                {
                    if (idRegional != null)
                    {
                        ddlRegionalICBF.SelectedValue = idRegional.Value.ToString();
                        ddlRegionalICBF.Enabled = false;
                    }
                    else
                    {
                        ddlRegionalICBF.SelectedValue = usuario.IdRegional.ToString();
                        ddlRegionalICBF.Enabled = false;
                    }
                }
        }

    }

    private void Guardar()
    {
        if (gvRegistroInformacionPresupuestal.Rows.Count > 0)
        {
            bool flag = false;

            if (gvRegistroInformacionPresupuestal.Rows.Count > 0)
            {
                List<ContratosCDP> lContratosCDP = new List<ContratosCDP>();

                string idContrato = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
                    idContrato = Request.QueryString["idContrato"];
                else
                   idContrato = GetSessionParameter("Contrato.ContratosAPP").ToString();

                foreach (GridViewRow fila in gvRegistroInformacionPresupuestal.Rows)
                {
                    if ((fila.Cells[0].FindControl("chbContiene") as CheckBox).Checked)
                    {                       
                        int numeroCDP = Convert.ToInt32(fila.Cells[2].Text);
                        DateTime fechaCDP = DateTime.Parse(fila.Cells[3].Text);
                        decimal valorCDP = decimal.Parse( gvRegistroInformacionPresupuestal.DataKeys[fila.RowIndex].Values[0].ToString().Replace("$",string.Empty) );
                        int idRegional = int.Parse(ddlRegionalICBF.SelectedValue.ToString());
                        
                        var itemExisteCDP = vContratoService.ConsultarExisteCPDAsociadoContrato(numeroCDP, fechaCDP,idRegional);

                        if (itemExisteCDP != 0 )
                        {
                            if(string.IsNullOrEmpty(hfEsPrecontractual.Value))
                            toolBar.MostrarMensajeError("El Número de CDP " + numeroCDP.ToString() + " ya esta asociado al Id de Contrato " +  itemExisteCDP);
                            else
                                toolBar.MostrarMensajeError("El Número de CDP " + numeroCDP.ToString() + " ya esta asociado al Id de Solicitud " + itemExisteCDP);
                            return;                            
                        }

                        ContratosCDP contratosCDP = new ContratosCDP();
                        contratosCDP.IdContrato = Convert.ToInt32(idContrato);
                        contratosCDP.UsuarioCrea = GetSessionUser().NombreUsuario;
                        contratosCDP.FechaCDP = fechaCDP;
                        contratosCDP.ValorCDP = valorCDP;
                        contratosCDP.NumeroCDP = numeroCDP.ToString();

                        if (hfesVigenciaFutura.Value == "1")
                        {
                            contratosCDP.EsVigenciaFutura = true;
                            contratosCDP.IdVigenciaFutura = Convert.ToInt32(hfidVigenciaFutura.Value);
                        }


                        if (!string.IsNullOrEmpty(hfIdContratoCDP.Value))
                            contratosCDP.IdCDP = int.Parse(hfIdContratoCDP.Value);
                        else
                            contratosCDP.IdCDP = 0;

                        if (gvRegistroInformacionPresupuestalRubros.Rows.Count > 0)
                        {

                            var vDtRubros = new DataTable();
                            vDtRubros.Columns.Add(new DataColumn("DescripcionDependenciaAfectacionGasto", Type.GetType("System.String")));
                            vDtRubros.Columns.Add(new DataColumn("DescripcionTipoFuenteFinanciamiento", Type.GetType("System.String")));
                            vDtRubros.Columns.Add(new DataColumn("PosicionCatalogoGastos", Type.GetType("System.String")));
                            vDtRubros.Columns.Add(new DataColumn("RubroPresupuestal", Type.GetType("System.String")));
                            vDtRubros.Columns.Add(new DataColumn("DescripcionTipoRecursoPresupuestal", Type.GetType("System.String")));
                            vDtRubros.Columns.Add(new DataColumn("DescripcionTipoSituacionFondos", Type.GetType("System.String")));
                            vDtRubros.Columns.Add(new DataColumn("ValorDetalle", Type.GetType("System.Decimal")));
                            vDtRubros.Columns.Add(new DataColumn("CodigoRubro", Type.GetType("System.String")));

                            foreach (GridViewRow vResultRow in gvRegistroInformacionPresupuestalRubros.Rows)
                            {
                                var filaDatatable = vDtRubros.NewRow();
                                filaDatatable[0] = vResultRow.Cells[0].Text;
                                filaDatatable[1] = vResultRow.Cells[1].Text;
                                filaDatatable[2] =  string.Empty;
                                filaDatatable[3] = vResultRow.Cells[2].Text;
                                filaDatatable[4] = vResultRow.Cells[3].Text;
                                filaDatatable[5] = vResultRow.Cells[4].Text;
                                filaDatatable[6] = decimal.Parse(gvRegistroInformacionPresupuestalRubros.DataKeys[vResultRow.RowIndex].Values[1].ToString().Replace("$", string.Empty)); 
                                filaDatatable[7] = gvRegistroInformacionPresupuestalRubros.DataKeys[vResultRow.RowIndex].Value.ToString();
                                vDtRubros.Rows.Add(filaDatatable);
                            }

                            int vResultado = vContratoService.InsertarContratosCDP(contratosCDP,vDtRubros);

                            if (vResultado == 0)
                            {
                                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                                flag = false;
                            }
                            else if (vResultado >= 1)
                            {
                                flag = true;
                                toolBar.MostrarMensajeGuardado();
                                SeleccionarRegistros();
                            } 
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("El CDP no cuenta con información de rubros presupuestales.");
                            return;
                        }
                    }
                }
            }
            else
                toolBar.MostrarMensajeError("La grilla no arrojó registros asociados.");

            if(!flag)
                toolBar.MostrarMensajeError("No se selecciono ningún registro o se prodrujo un error al guardar.");
        }
    }

    private void SeleccionarRegistros()
    {
        try
        {
            this.GetScriptCloseDialogScriptsCallback(string.Empty, null);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void VerficarQueryStrings()
    {    
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            hfIdContrato.Value = Request.QueryString["idContrato"];
        
        if (!string.IsNullOrEmpty(Request.QueryString["vesVigenciaFutura"]))
            hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vidVigenciaFutura"]))
            hfidVigenciaFutura.Value = Request.QueryString["vidVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["idVigencia"]))
            hfIdVigencia.Value = Request.QueryString["idVigencia"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdRegContrato"]))
            hfIdRegional.Value = Request.QueryString["IdRegContrato"];

        if (!string.IsNullOrEmpty(Request.QueryString["vAnioVigencia"]))
            hfAnioVigencia.Value = Request.QueryString["vAnioVigencia"];

        if(!string.IsNullOrEmpty(Request.QueryString["vEsPrecontractual"]))
            hfEsPrecontractual.Value = Request.QueryString["vEsPrecontractual"];

    }

    protected void RbtTipoBusqueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();

            gvRegistroInformacionPresupuestal.EmptyDataText = EmptyDataText();
            List<ConsultaCDPMaestro> items = new List<ConsultaCDPMaestro>();
            gvRegistroInformacionPresupuestal.DataSource = items;
            gvRegistroInformacionPresupuestal.DataBind();

            gvRegistroInformacionPresupuestalRubros.EmptyDataText = EmptyDataText();
            List<ConsultaCDPDetalle> itemsDetalle = new List<ConsultaCDPDetalle>();
            gvRegistroInformacionPresupuestalRubros.DataSource = itemsDetalle;
            gvRegistroInformacionPresupuestalRubros.DataBind();
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
