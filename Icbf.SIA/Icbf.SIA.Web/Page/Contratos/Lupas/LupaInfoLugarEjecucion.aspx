﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoLugarEjecucion.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoLugarEjecucion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Lugar de Ejecución Actual
            </td>
            
        </tr>
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos" AutoGenerateColumns="false" 
                                                    GridLines="None" Width="98%" CellPadding="8" Height="16px" Visible="True" >
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                        <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                        <%--<asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center" >  
                                                            <ItemTemplate> 
                                                                <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarLugEjecucionClick" OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" >
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate> 
                                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                </td>
            </tr>
            
        </table>
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Historico Lugar de Ejecución 
            </td>
            </tr>
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="gvLugaresEjecucionHistorico" runat="server" DataKeyNames="IdLugarEjecucionContratos" AutoGenerateColumns="false" 
                                                    GridLines="None" Width="98%" CellPadding="8" Height="16px" Visible="True" >
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                        <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                        <%--<asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center" >  
                                                            <ItemTemplate> 
                                                                <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarLugEjecucionClick" OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" >
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate> 
                                                        </asp:TemplateField>--%>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                </td>
            </tr>
            
        </table>

    </asp:Panel>
    
   </asp:Content>
