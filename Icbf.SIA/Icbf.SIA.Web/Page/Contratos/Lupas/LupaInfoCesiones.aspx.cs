﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaInfoCesiones : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/LupaInfoCesiones";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            CargarDatosIniciales();

        }
    }
    protected void gvInfoRP_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInfoRP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }
    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            //toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.HabilitarBotonGuardar(false);

            gvCesiones.PageSize = PageSize();
            gvCesiones.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Cesiones", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarGrillaRP();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void CargarGrillaRP()
    {
        
        CargarGrilla(gvCesiones, GridViewSortExpression, true);


    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdContrato = 0;
            
            if (!int.TryParse(hfIdContrato.Value, out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            var cesiones = vContratoService.ConsultarInfoCesionesSuscritas(vIdContrato);

            var myGridResults = cesiones;
            gridViewsender.DataSource = myGridResults;
            gridViewsender.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected int PageSize()
    {
        Int32 vPageSize;
        if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
        {
            throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
        }
        return vPageSize;
    }

    protected String EmptyDataText()
    {
        String vEmptyDataText;
        if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
        {
            throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
        }
        vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
        return vEmptyDataText;
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }
    private void VerficarQueryStrings()
    {
       
        if (!string.IsNullOrEmpty(Request.QueryString["vIdContrato"]))
            hfIdContrato.Value = Request.QueryString["vIdContrato"];
    }

    #endregion
}