﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaInfoCDP : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/LupaInfoGarantias";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }
    protected void gvInfoRP_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInfoRP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.HabilitarBotonGuardar(false);

            gvCDPDetalle.PageSize = PageSize();
            gvCDPDetalle.EmptyDataText = EmptyDataText();

            gvCDPEncabezado.PageSize = PageSize();
            gvCDPEncabezado.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Información CDP", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarGrillasCDP();
            CargarGrillaHistorico();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public void CargarGrillasCDP()
    {
        try
        {
            decimal vValorTotalCDP = 0;
            CargarGrillas(gvCDPEncabezado, gvCDPDetalle, GridViewSortExpression, true);
            List<string> numerosCDP = new List<string>();
            foreach (GridViewRow fila in gvCDPEncabezado.Rows)
            {
                var itemNumeroCDP = gvCDPEncabezado.DataKeys[fila.RowIndex].Values["ValorCDP"].ToString();
                if (!string.IsNullOrEmpty(gvCDPEncabezado.DataKeys[fila.RowIndex].Values["ValorCDP"].ToString()) && ! numerosCDP.Any(itemCD => itemCD == itemNumeroCDP))
                {
                    numerosCDP.Add(itemNumeroCDP);
                    vValorTotalCDP += Convert.ToDecimal(gvCDPEncabezado.DataKeys[fila.RowIndex].Values["ValorCDP"].ToString());
                }
            }

            txtTotalCDP.Text = vValorTotalCDP.ToString("$ #,###0.00##;($ #,###0.00##)");

            //CargarGrillas(gvCDPDetalle, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        

    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender1">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillas(BaseDataBoundControl gridViewsender1, BaseDataBoundControl gridViewsender2, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdContrato = 0;

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }
            
            var myGridResults = vContratoService.ConsultarContratosCDP(vIdContrato);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            List<ContratosCDP> List = new List<ContratosCDP>();

            if (myGridResults != null)
            {
                foreach (var item in myGridResults)
                    if (!List.Any(itemCDP => itemCDP.NumeroCDP == item.NumeroCDP))
                        List.Add(item);
            }

            gridViewsender1.DataSource = List;
            gridViewsender2.DataSource = myGridResults;
            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(ContratosCDP), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ContratosCDP, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender1.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            gridViewsender2.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender1.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            gridViewsender2.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender1.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            gridViewsender2.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender1.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            gridViewsender2.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender1.DataSource = List;
                gridViewsender2.DataSource = myGridResults;
            }

            gridViewsender1.DataBind();
            gridViewsender2.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected int PageSize()
    {
        Int32 vPageSize;
        if (!Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["PageSize"], out vPageSize))
        {
            throw new UserInterfaceException("El valor del parametro 'PageSize', no es valido.");
        }
        return vPageSize;
    }

    protected String EmptyDataText()
    {
        String vEmptyDataText;
        if (System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"] == null)
        {
            throw new UserInterfaceException("El valor del parametro 'EmptyDataText', no es valido.");
        }
        vEmptyDataText = System.Configuration.ConfigurationManager.AppSettings["EmptyDataText"];
        return vEmptyDataText;
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    public void CargarGrillaHistorico()
    {
        CargarGrillaHistorico(gvInfoModificaciones, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaHistorico(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int vIdContrato = 0;

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            var myGridResults = vContratoService.ConsultarAdicionesAprobadasContrato(vIdContrato);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////
            gridViewsender.DataSource = myGridResults;
            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Adiciones), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Adiciones, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvInfoModificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    #endregion

    
}