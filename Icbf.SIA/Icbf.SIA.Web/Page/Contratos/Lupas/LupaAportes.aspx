<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaAportes.aspx.cs" Inherits="Page_Contratos_Lupas_LupaAporteContrato" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfValor" runat="server" />
    <asp:HiddenField ID="HfAporte" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Tipo de Aportante
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txttipoAportante" Enabled="False" Width="37%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Identificación *
                <asp:RequiredFieldValidator runat="server" ID="rvtxtTipoIdentificacion" ControlToValidate="txtTipoIdentificacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:HiddenField runat="server" ID="hdIdEntidad" ClientIDMode="Static" />
            </td>
            <td>
                Número de Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoIdentificacion" MaxLength="56" Width="80%"
                    onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTipoIdentificacion"
                    FilterType="Numbers,UppercaseLetters,LowercaseLetters" ValidChars=" " />
                <asp:Image ID="imgTipoIdentificacionContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContratista()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField runat="server" ID="hdTipoIdentificacion" ClientIDMode="Static" />
                <asp:Button runat="server" ID="btnhabilitartipoAporte" Style="display: none" OnClick="btnCargarTipoAporte_Click" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="56" Width="80%"
                    onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                    FilterType="Numbers" ValidChars=" " />
                <asp:HiddenField runat="server" ID="hdNumeroIdentificacion" ClientIDMode="Static" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Información Aportante
            </td>
            <td>
                Tipo de aporte *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoAporte" ControlToValidate="rblTipoAporte"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido
                " Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvrbltipoaporte" ControlToValidate="rblTipoAporte"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare=" " Display="Dynamic" Enabled="True"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtInformacionAportante" MaxLength="56" Width="80%"
                    onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtInformacionAportante"
                    FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" .áéíóúÁÉÍÓÚñÑ" />
                <asp:HiddenField runat="server" ID="hdInformacionAportante" ClientIDMode="Static" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblTipoAporte" RepeatDirection="Horizontal"
                    AutoPostBack="True" OnSelectedIndexChanged="rblTipoAporte_OnSelectedIndexChanged"
                    Enabled="True">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor aporte *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorAporte" ControlToValidate="txtValorAporte"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label runat="server" ID="lblDescripcionAporteEspecieCon" Text="Descripción Aporte Especie *"
                    Visible="False"></asp:Label>
                <asp:Label runat="server" ID="lblDescripcionAporteEspecieSin" Text="Descripción Aporte Especie"
                    Visible="True"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvDescripcionAporte" ControlToValidate="txtDescripcionAporte"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAporte" MaxLength="30" Width="80%" onkeypress="return EsEspacio(event,this)"
                    Enabled="False" AutoPostBack="True" OnTextChanged="txtValorAporte_OnTextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAporte" runat="server" TargetControlID="txtValorAporte"
                    FilterType="Custom,Numbers" ValidChars=",.$" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionAporte" MaxLength="200" Width="80%"
                    Height="80px" TextMode="MultiLine" onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);"
                    CssClass="TextBoxGrande" onkeypress="return EsEspacio(event,this)" Enabled="False"
                    AutoPostBack="True" OnTextChanged="txtDescripcionAporte_OnTextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionAporte" runat="server" TargetControlID="txtDescripcionAporte"
                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars=" ñÑ" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:Label runat="server" ID="lblFecharp" Text="Fecha RP " Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Label runat="server" ID="lblNumerorp" Text="Número RP " Visible="False"></asp:Label>
                <asp:RequiredFieldValidator runat="server" ID="rfvnumerorp" ControlToValidate="txtNumeroRP"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="txtFechaRP" runat="server"  Visible="False" Requerid="False" Width="80%" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroRP" MaxLength="30" Visible="False" Width="80%"
                    onkeypress="return EsEspacio(event,this)"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtNumeroRP"
                    FilterType="Custom,Numbers" ValidChars=" " />
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }

        function GetContratista() {

            var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
            window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaAportesContratistas.aspx?idContrato=' + vIdContrato, setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContratista(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 4) {
                    $('#<%=hdIdEntidad.ClientID %>').val(retLupa[0]);
                    $('#<%=txtTipoIdentificacion.ClientID %>').val(retLupa[2]);
                    $('#<%=txtNumeroIdentificacion.ClientID %>').val(retLupa[3]);
                    $('#<%=txtInformacionAportante.ClientID %>').val(retLupa[4]);

                    $('#<%=hdTipoIdentificacion.ClientID %>').val(retLupa[2]);
                    $('#<%=hdNumeroIdentificacion.ClientID %>').val(retLupa[3]);
                    $('#<%=hdInformacionAportante.ClientID %>').val(retLupa[4]);
                    $("#<%= btnhabilitartipoAporte.ClientID%>").click();
                }
            }
        }
    </script>
</asp:Content>
