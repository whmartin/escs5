﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoCDPLinea.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoCDPLinea" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
                        <tr class="rowB">
                <td>CDPs</td>
            </tr>
            <tr class="rowAG">
                <td>                    
                    <asp:GridView ID="gvCDP" runat="server" AllowPaging="True" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IdContratosCDP" GridLines="None" Height="16px"  Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NumeroCDP" HeaderText="Número CDP" />
                            <asp:BoundField DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha CDP" />
                            <asp:BoundField DataField="ValorCDP" DataFormatString="{0:c}" HeaderText="Valor CDP" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td>Rubros CDP</td>
            </tr>
            <tr>
                <td>
                    <span>
                    <asp:GridView ID="gvRubrosCDP" runat="server" AllowPaging="True" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="NumeroCDP" GridLines="None" Height="16px" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NumeroCDP" HeaderText="Número CDP" />
                            <asp:BoundField DataField="DependenciaAfectacionGastos" HeaderText="Dependencia Afectación Gastos" />
                            <asp:BoundField DataField="CodigoRubro" HeaderText="Cod Rubro Presupuestal" />
                            <asp:BoundField DataField="RubroPresupuestal" HeaderText="Rubro Presupuestal" />
                            <asp:BoundField DataField="RecursoPresupuestal" HeaderText="Recurso Presupuestal" />
                            <asp:BoundField DataField="TipoSituacionFondos" HeaderText="Tipo Situación Fondos" />
                            <asp:BoundField DataField="ValorRubro" DataFormatString="{0:C}" HeaderText="Valor Rubro" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                    </span>

                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>