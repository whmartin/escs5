﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoRP.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoRP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvInfoRP" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%"  CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvInfoRP_PageIndexChanging" 
                        OnSelectedIndexChanged="gvInfoRP_SelectedIndexChanged" DataKeyNames="ValorRP">
                        <Columns>
                        <asp:BoundField HeaderText="Número del RP" DataField="NumeroRP"  SortExpression="IdRP"/>
                        <asp:BoundField HeaderText="Fecha de Expedición" DataField="FechaExpedicionRP" SortExpression="FechaExpedicionRP" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField HeaderText="Valor del RP Actual" DataField="ValorActualRP"  SortExpression="ValorActualRP" DataFormatString="{0:C}" />   
                        <asp:BoundField HeaderText="Valor del RP Inicial" DataField="ValorInicialRP"  SortExpression="ValorInicialRP" DataFormatString="{0:C}" />   
                        <asp:BoundField HeaderText="Es Cesión" DataField="EsCesionString"  SortExpression="EsCesionString" />   
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    Valor Total RP
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtTotalRP" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>Historico Adiciones</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:GridView ID="gvInfoModificaciones" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="ValorRP" GridLines="None" Height="16px" OnPageIndexChanging="gvInfoModificaciones_PageIndexChanging"  Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ValorRP" HeaderText="Valor del RP" SortExpression="ValorRP" DataFormatString="{0:C}" />
                            <asp:BoundField DataField="FechaSubscripcion" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha de Subscripción" SortExpression="FechaSubscripcion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>