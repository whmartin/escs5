﻿using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using WsContratosPacco;

public partial class Page_Contratos_Lupas_LupaCodigoSECOP : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa _toolBar;
    readonly ContratoService _vContratoService = new ContratoService();
    public string PageName = "Contratos/Contratos";

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(_toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                var idContrato =  Request.QueryString["IdContrato"];

                if (!string.IsNullOrEmpty(idContrato))
                {
                    gvCodigosSECOP.EmptyDataText = EmptyDataText();
                    gvCodigosSECOP.PageSize = PageSize();
                    gvCodigosSECOP.DataSource = _vContratoService.ObtenerCodigosSECOPContrato(int.Parse(idContrato));
                    gvCodigosSECOP.DataBind();
                }
            }
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rvDetallePlan", returnValues);
    }

    #endregion

    #region Funciones y Procedmientos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)Master;
            if (_toolBar != null)
            {
                _toolBar.eventoRetornar += btnRetornar_Click;
                _toolBar.EstablecerTitulos("Códigos SECOP", SolutionPage.Add.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}