﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master"
    CodeFile="LupaInfoGarantias.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoGarantias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlLista" CssClass="width: 100%">
        <table width="100%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvGarantia" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDGarantia,FechaAprobacionGarantia,FechaCertificacionGarantia,IdSucursalAseguradoraContrato"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvGarantia_PageIndexChanging"
                        OnSelectedIndexChanged="gvGarantia_SelectedIndexChanged" OnRowCommand="gvGarantia_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Visualizar" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                    <%--<asp:ImageButton ID="btnSeleccionar" runat="server" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" Visible="False" ToolTip="Gestionar Garantía" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'
                                        CommandName="Seleccionar" />--%>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número Garantía" DataField="NumeroGarantia" SortExpression="NumeroGarantia" />
                            <asp:BoundField HeaderText="Tipo Garantía" DataField="NombreTipoGarantia" SortExpression="NombreTipoGarantia" />
                            <asp:BoundField HeaderText="Fecha de Inicio Garantía" DataField="FechaInicioGarantia"
                                SortExpression="FechaInicioGarantia" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField HeaderText="Valor Garantía" DataField="ValorGarantia" SortExpression="ValorGarantia"
                                DataFormatString="{0:C}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>

            <tr>
                <td>
                    Valor Garantías
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtValorTotalGarantias" Enabled="False"></asp:TextBox>
                </td>
            </tr>

            <tr class="rowB">
                <td>
                    Historico de Garantías
                </td>
            </tr>
                        <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView runat="server" ID="gvGarantiaHistorico" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDGarantia,FechaAprobacionGarantia,FechaCertificacionGarantia,IdSucursalAseguradoraContrato"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvGarantia_PageIndexChanging"
                        OnSelectedIndexChanged="gvGarantia_SelectedIndexChanged" OnRowCommand="gvGarantia_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderText="Número Garantía" DataField="NumeroGarantia" SortExpression="NumeroGarantia" />
                            <asp:BoundField HeaderText="Tipo Garantía" DataField="NombreTipoGarantia" SortExpression="NombreTipoGarantia" />
                            <asp:BoundField HeaderText="Fecha de Inicio Garantía" DataField="FechaInicioGarantia"
                                SortExpression="FechaInicioGarantia" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField HeaderText="Valor Garantía" DataField="ValorGarantia" SortExpression="ValorGarantia"
                                DataFormatString="{0:C}" />
                            <asp:BoundField HeaderText="Fecha Modificación Garantía" DataFormatString="{0:dd/MM/yyyy}" DataField="FechaModificacionContractual" />
                            <asp:BoundField HeaderText="Modificación Contractual" DataField="TipoModificacionContractual" />
                            <asp:BoundField HeaderText="Fecha Aprobación Garantía" DataFormatString="{0:dd/MM/yyyy}" DataField="FechaAprobacionGarantia" />

                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hfPostbk" runat="server" />
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function GestionarGarantias() {
            window_showModalDialogLupa('../../../Page/Contratos/GestionarGarantia/Add.aspx', setReturnGestionarGarantias, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGestionarGarantias() {
            __doPostBack('<%= hfPostbk.ClientID %>', ''); // EC-08-08-2014 Se realiza postbk cada vez que se regresa de la lupa para validar cambios en las garantias
        }

        function GetDetalleGarantias() {
            window_showModalDialog('../../../Page/Contratos/lupas/LupaGarantias.aspx', "", 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
    </script>
</asp:Content>
