﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaRepresentanteLegal.aspx.cs" Inherits="Page_Contratos_Lupas_LupaRepresentanteLegal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <div style="display: none">
 <%--       <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>--%>
    </div>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedor" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTercero" CellPadding="0"
                        Height="16px" AllowSorting="true" OnPageIndexChanging="gvProveedor_PageIndexChanging"
                         OnSorting="gvProveedor_Sorting">
                        <Columns>
<%--                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Tipo de Identificaci&oacute;n" DataField="NombreListaTipoDocumento"
                                SortExpression="NombreListaTipoDocumento" />--%>
                            <asp:BoundField HeaderText="N&uacute;mero Identificaci&oacute;n" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Tercero" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial" />
                            <asp:BoundField HeaderText="Correo Electr&oacute;nico" DataField="Email" SortExpression="Email" />
                            <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaCrea" SortExpression="FechaCrea" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

