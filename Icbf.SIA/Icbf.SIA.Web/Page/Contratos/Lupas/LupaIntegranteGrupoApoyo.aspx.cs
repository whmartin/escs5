﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;



public partial class Page_LupaIntegrantesGupoApoyo_List : GeneralWeb
{

    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionIntegrantes";
    public string bandera = "0";
    List<SupervisionGruposLupaPersona> listaIntegrantes;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            if (bandera == "1")
            {
                Buscar();
            }
        }

    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }
    private void Iniciar()
    {
        try
        {

            if (Request.Params["mostrar"] != null)
            {
                bandera = Request.Params["mostrar"].ToString();

            }

            //gvLupaIntegrantesGupoApoyo.Page.

            gvLupaIntegrantesGupoApoyo.PageSize = 8;// PageSize();
            gvLupaIntegrantesGupoApoyo.EmptyDataText = EmptyDataText();

        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Buscar()
    {
        try
        {
            String vTipoDoc = Request.Params["tipoDoc"] != null ? Request.Params["tipoDoc"].ToString() : null;
            String vNombre1 = Request.Params["nombre1"] != null ? Request.Params["nombre1"].ToString() : null;
            String vApellido1 = Request.Params["apellido1"] != null ? Request.Params["apellido1"].ToString() : null;
            String vIndice = Request.Params["indice"] != null ? Request.Params["indice"].ToString() : null;
            int? vIdentificacion = null;

            if (vIndice != "")
            {
                gvLupaIntegrantesGupoApoyo.PageIndex = Convert.ToInt32(vIndice);
            }

            if (Request.Params["numDoc"] != null && Request.Params["numDoc"].ToString() != "")
            { vIdentificacion = Convert.ToInt32(Request.Params["numDoc"].ToString()); }

            String vNombre2 = Request.Params["nombre2"] != null ? Request.Params["nombre2"].ToString() : null;
            String vApellido2 = Request.Params["apellido2"] != null ? Request.Params["apellido2"].ToString() : null;

            listaIntegrantes = vSupervisionService.ConsultarPersonaLupa(vTipoDoc, vIdentificacion, vNombre1, vNombre2, vApellido1, vApellido2);
            gvLupaIntegrantesGupoApoyo.DataSource = listaIntegrantes;
            gvLupaIntegrantesGupoApoyo.DataBind();
            SetSessionParameter("gvLupaIntegrantesGupoApoyo.DataSource", gvLupaIntegrantesGupoApoyo.DataSource);

        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
    }


    protected void gvLupaIntegrantesGupoApoyo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvLupaIntegrantesGupoApoyo.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {


            ManejoControlesSupervision vControles = new ManejoControlesSupervision();
            vControles.LlenarTipoDocumento(ddlTipoDocTercero, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
        }
    }


    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // No obliga a a la página a tener un form incluido
    }


    protected void gvLupaIntegrantesGupoApoyo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            e.Row.Cells[0].Text = e.Row.Cells[0].Text + "<a href=\"javascript:asignarNombres('" + ((SupervisionGruposLupaPersona)e.Row.DataItem).Identificacion + "','" + ((SupervisionGruposLupaPersona)e.Row.DataItem).Nombre1 + ' ' + ((SupervisionGruposLupaPersona)e.Row.DataItem).Nombre2 + "','" + ((SupervisionGruposLupaPersona)e.Row.DataItem).Apellido1 + ' ' + ((SupervisionGruposLupaPersona)e.Row.DataItem).Apellido2 + "');\"><img src='../../../Image/btn/info.jpg'><a/>";

        }
        else if (e.Row.RowType == DataControlRowType.Pager)
        {             
            Int32 pValorPagina = 0;
            Table tb = (Table)e.Row.Cells[0].Controls[0];
            Int32 indice = 0;
            foreach (TableCell pageCell in tb.Rows[0].Cells)//Se recorren todas las celdas de los campos de paginación
            {
                if (pageCell.Controls[0] is LinkButton)
                {
                    Label lbl1;
                    LinkButton lbt1;
                    LinkButton lbt = (LinkButton)pageCell.Controls[0];
                    if (lbt.Text == "...")//Se valida si la celda actual es alguno de los extremos de paginación
                    {

                        if (indice == 0) //Se valida si es la primera o la última celda de la tabla
                        {
                          

                            if (tb.Rows[0].Cells[1].Controls[0].GetType() == typeof(Label)) //Se valida si es label para los casos en que esa es la página seleccionada actualmente
                            {
                                lbl1 = (Label)tb.Rows[0].Cells[1].Controls[0];
                                pValorPagina = Convert.ToInt32(lbl1.Text) - 2;
                            }
                            else if ((tb.Rows[0].Cells[1].Controls[0].GetType() == typeof(LinkButton)) || tb.Rows[0].Cells[tb.Rows[0].Cells.Count - 2].Controls[0].GetType().BaseType.BaseType == typeof(LinkButton))
                            {
                                lbt1 = (LinkButton)tb.Rows[0].Cells[1].Controls[0];
                                pValorPagina = Convert.ToInt32(lbt1.Text) - 2;
                            }

                        }
                        else if (indice == (tb.Rows[0].Cells.Count - 1))
                        {

                            if (tb.Rows[0].Cells[tb.Rows[0].Cells.Count - 2].Controls[0].GetType() == typeof(Label)) //Se valida si es label para los casos en que esa es la página seleccionada actualmente
                            {
                                lbl1 = (Label)tb.Rows[0].Cells[tb.Rows[0].Cells.Count - 2].Controls[0];
                                pValorPagina = Convert.ToInt32(lbl1.Text);
                            }
                            else if ((tb.Rows[0].Cells[tb.Rows[0].Cells.Count - 2].Controls[0].GetType() == typeof(LinkButton)) || tb.Rows[0].Cells[tb.Rows[0].Cells.Count - 2].Controls[0].GetType().BaseType.BaseType == typeof(LinkButton))
                            {
                                lbt1 = (LinkButton)tb.Rows[0].Cells[tb.Rows[0].Cells.Count - 2].Controls[0];
                                pValorPagina = Convert.ToInt32(lbt1.Text);
                            }
                        }

                    }
                    else { pValorPagina = Convert.ToInt32(lbt.Text) - 1; } // se asigna el valor de la paginación -1 a cada uno de los numeros intermedios

                    lbt.Attributes.Add("onclick", "javascript:cargaGridTerceros(" + pValorPagina + ");return false; ");//Se le asigna la función de llamado del número de página a cada opción.
                }
                indice = indice + 1;
            }

        }
    }
}
