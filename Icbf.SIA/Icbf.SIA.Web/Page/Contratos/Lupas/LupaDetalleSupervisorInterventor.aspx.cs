﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Contratos_Lupas_LupaDetalleSupervisorInterventor : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/DetalleSupervisorInterventor";
    ContratoService vContratoService = new ContratoService();
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //vSolutionPage = SolutionPage.Detail;
        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            CargarDatos();
        }
        //}
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }
    #endregion

    #region Métodos
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
           //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            
            toolBar.EstablecerTitulos("Supervisor/Interventor", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            //rblListEstadoSuperInterv.Items.Insert(0, new ListItem("Activo", "true"));
            //rblListEstadoSuperInterv.Items.Insert(1, new ListItem("Inactivo", "false"));
            //rblListEstadoSuperInterv.SelectedValue = "true";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatos()
    {
        

        try
        {

            int vIdentificacion = 0;

            if (!int.TryParse(GetSessionParameter("ConsultarSuperInterContrato.Identificacion").ToString(), out vIdentificacion))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }
            
            SupervisorInterContrato vConsultarSuperInter = new SupervisorInterContrato();
            vConsultarSuperInter = vContratoService.ObtenerSupervisorInterventoresContratoDetalle(vIdentificacion, null);
            if (!string.IsNullOrEmpty( vConsultarSuperInter.EtQSupervisorInterventor))
            {
            txtSupervisorInterventor.Text = vConsultarSuperInter.EtQSupervisorInterventor.ToUpper();
            txtFechaInicio.Text = string.Format("{0:dd/MM/yyyy}", vConsultarSuperInter.FechaInicio);
            txtTipoPersona.Text = vConsultarSuperInter.TipoPersona.ToUpper();
            txtTipoIdentificacion.Text = vConsultarSuperInter.TipoIdentificacion;
            txtNumeroIdentificacion.Text = vConsultarSuperInter.Identificacion;
            txtTipoSupervisorInterventor.Text = vConsultarSuperInter.EtQInternoExterno.ToUpper();
            txtEmail.Text = vConsultarSuperInter.SupervisorInterventor.CorreoElectronico;
            }
            if (vConsultarSuperInter.EtQSupervisorInterventor == "Supervisor")
            {
                lblRazonSocial.Visible = false;
                txtRazonSocial.Visible = false;
                lblEmail.Visible = true;
                txtEmail.Visible = true;
                lblTipoVincContract.Visible = true;
                txtTipoVinContrac.Visible = true;
                lblPrimerApellido.Visible = true;
                lblPrimerNombre.Visible = true;
                lblSegundoApellido.Visible = true;
                lblSegundoNombre.Visible = true;
                lblRegional.Visible = true;
                lblDependencia.Visible = true;
                lblCargo.Visible = true;
                lblDireccion.Visible = true;
                lblTelefono.Visible = true;
                txtPrimerApellido.Visible = true;
                txtPrimerNombre.Visible = true;
                txtSegundoApellido.Visible = true;
                txtSegundoNombre.Visible = true;
                txtRegional.Visible = true;
                txtDependencia.Visible = true;
                txtCargo.Visible = true;
                txtDireccion.Visible = true;
                txtTelefono.Visible = true;
                pnPersonaNatral.Visible = true;
                txtTipoVinContrac.Text = vConsultarSuperInter.SupervisorInterventor.TipoVinculacionContractual.ToUpper();
                txtPrimerApellido.Text = vConsultarSuperInter.SupervisorInterventor.PrimerApellido.ToUpper();
                txtPrimerNombre.Text = vConsultarSuperInter.SupervisorInterventor.PrimerNombre.ToUpper();
                txtSegundoApellido.Text = vConsultarSuperInter.SupervisorInterventor.SegundoApellido.ToUpper();
                txtSegundoNombre.Text = vConsultarSuperInter.SupervisorInterventor.SegundoNombre.ToUpper();
                txtRegional.Text = vConsultarSuperInter.SupervisorInterventor.Regional.ToUpper();
                txtDependencia.Text = vConsultarSuperInter.SupervisorInterventor.Dependencia.ToUpper();
                txtCargo.Text = vConsultarSuperInter.SupervisorInterventor.Cargo.ToUpper();
                txtDireccion.Text = vConsultarSuperInter.SupervisorInterventor.Direccion.ToUpper();
                txtTelefono.Text = vConsultarSuperInter.SupervisorInterventor.Telefono.ToUpper();
            }
            else
            {
                if (vConsultarSuperInter.IdTipoPersona == 1)
                {
                    pnPersonaNatral.Visible = true;
                    lblRazonSocial.Visible = false;
                    txtRazonSocial.Visible = false;
                    lblEmail.Visible = true;
                    txtEmail.Visible = true;
                    lblPrimerApellido.Visible = true;
                    lblPrimerNombre.Visible = true;
                    lblSegundoApellido.Visible = true;
                    lblSegundoNombre.Visible = true;
                    txtPrimerApellido.Visible = true;
                    txtPrimerNombre.Visible = true;
                    txtSegundoApellido.Visible = true;
                    txtSegundoNombre.Visible = true;
                    txtPrimerApellido.Text = vConsultarSuperInter.SupervisorInterventor.PrimerApellido.ToUpper();
                    txtPrimerNombre.Text = vConsultarSuperInter.SupervisorInterventor.PrimerNombre.ToUpper();
                    txtSegundoApellido.Text = vConsultarSuperInter.SupervisorInterventor.SegundoApellido.ToUpper();
                    txtSegundoNombre.Text = vConsultarSuperInter.SupervisorInterventor.SegundoNombre.ToUpper();
                }
                else if (vConsultarSuperInter.IdTipoPersona == 2)
                {
                    pnPersonaNatral.Visible = false;
                    lblPrimerApellido.Visible = false;
                    lblPrimerNombre.Visible = false;
                    lblSegundoApellido.Visible = false;
                    lblSegundoNombre.Visible = false;
                    txtPrimerApellido.Visible = false;
                    txtPrimerNombre.Visible = false;
                    txtSegundoApellido.Visible = false;
                    txtSegundoNombre.Visible = false;
                    lblEmail.Visible = true;
                    txtEmail.Visible = true;
                    lblRazonSocial.Visible = true;
                    txtRazonSocial.Visible = true;
                    PnlReprLegal.Visible = true;
                    txtRazonSocial.Text = vConsultarSuperInter.SupervisorInterventor.NombreCompleto.ToUpper();
                    SupervisorInterContrato vConsultarDirinter = new SupervisorInterContrato();
                    vConsultarDirinter = vContratoService.ObtenerSupervisorInterventoresContratoDetalle(vIdentificacion, true);
                    txtTipoIdentificacionDirInt.Text = vConsultarDirinter.TipoIdentificacion.ToUpper();
                    txtNumIdentificacionDirInt.Text = vConsultarDirinter.Identificacion;
                    TxtPrimerNombreDirInt.Text = vConsultarDirinter.SupervisorInterventor.PrimerNombre.ToUpper();
                    TxtSegundoNombreDirInt.Text = vConsultarDirinter.SupervisorInterventor.SegundoNombre.ToUpper();
                    TxtPrimerApellidoDirInt.Text = vConsultarDirinter.SupervisorInterventor.PrimerApellido.ToUpper();
                    TxtSegundoApellidoDirInt.Text = vConsultarDirinter.SupervisorInterventor.SegundoApellido.ToUpper();
                    txtTelefonoDirInt.Text = vConsultarDirinter.SupervisorInterventor.Telefono.ToUpper();
                    txtEmailDirInt.Text = vConsultarDirinter.SupervisorInterventor.CorreoElectronico.ToUpper();
                    txtNumeroContratoInter.Text = vConsultarDirinter.NumeroContratoInterventoria;
                }
                
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    
}