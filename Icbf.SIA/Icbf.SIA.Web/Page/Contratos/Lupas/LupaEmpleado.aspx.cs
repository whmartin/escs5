﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página que despliega la información y permite la selección de un tercero 
/// </summary>
public partial class Page_Contratos_Lupas_LupaEmpleado : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ManejoControles ManejoControles = new ManejoControles();
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.SetBtnBuscarOnClientClick("LoadingBuscar();");

            gvEmpleados.PageSize = PageSize();
            gvEmpleados.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Empleados ICBF", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            tbResultados.Visible = false;

            #region LlenarComboTipoIdentificacion
            ddlTipoIdentificacion.Items.Clear();
            List<TipoDocumento> vLTipoDoc = vSIAService.ConsultarTodosTipoDocumento();
            foreach (TipoDocumento tD in vLTipoDoc)
            {
                if ((tD.Codigo == "CC") || (tD.Codigo == "CE"))
                { ddlTipoIdentificacion.Items.Add(new ListItem(tD.Codigo, tD.Codigo.ToString())); }
            }
            ddlTipoIdentificacion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            #region LlenarTipoVinculacionContractual
            ddlTipoVinculacionContractual.Items.Clear();
            List<Empleado> vLTiposVincContrac = vContratoService.ConsultarTiposVinculacionContractual(null, null);
            foreach (Empleado tD in vLTiposVincContrac)
            {
                ddlTipoVinculacionContractual.Items.Add(new ListItem(tD.TipoVinculacionContractual, tD.IdTipoVinculacionContractual));
            }
            ddlTipoVinculacionContractual.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            #region LlenarComboRegional
            ddlRegional.Items.Clear();
            List<Regional> vLRegional = vSIAService.ConsultarRegionals(null, null);
            foreach (Regional tD in vLRegional)
            {
                ddlRegional.Items.Add(new ListItem(tD.NombreRegional, tD.CodigoRegional.ToString()));
            }
            ddlRegional.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            //ManejoControlesContratos.LlenarComboLista(ddlRegional, vSIAService.ConsultarRegionals(null, null), "CodigoRegional", "NombreRegional");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            //toolBar.LipiarMensajeError();
            CargarGrilla(gvEmpleados, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vTipoIdentificacion = null;
            String vNumeroIdentificacion = null;
            String vTipoVinculacionContractual = null;
            String vRegional = null;
            String vPrimerNombre = null;
            String vSegundoNombre = null;
            String vPrimerApellido = null;
            String vSegundoApellido = null;

            if (!ddlTipoIdentificacion.SelectedValue.Equals("-1") && !ddlTipoIdentificacion.SelectedValue.Equals(string.Empty))
            {
                vTipoIdentificacion = ddlTipoIdentificacion.SelectedValue;
            }
            if (!txtNumeroIdentificacion.Text.Equals(string.Empty))
            {
                vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            }
            if (!ddlTipoVinculacionContractual.SelectedValue.Equals("-1") && !ddlTipoVinculacionContractual.SelectedValue.Equals(string.Empty))
            {
                vTipoVinculacionContractual = ddlTipoVinculacionContractual.SelectedItem.Text;
            }
            if (!ddlRegional.SelectedValue.Equals("-1") && !ddlRegional.SelectedValue.Equals(string.Empty))
            {
                vRegional = ddlRegional.SelectedValue;
            }
            if (txtPrimerNombre.Text != "")
            {
                vPrimerNombre = txtPrimerNombre.Text;
            }
            if (txtSegundoNombre.Text != "")
            {
                vSegundoNombre = txtSegundoNombre.Text;
            }
            if (txtPrimerApellido.Text != "")
            {
                vPrimerApellido = txtPrimerApellido.Text;
            }
            if (txtSegundoApellido.Text != "")
            {
                vSegundoApellido = txtSegundoApellido.Text;
            }

            var myGridResults = vContratoService.ConsultarEmpleados(vTipoIdentificacion, vNumeroIdentificacion, vTipoVinculacionContractual, vRegional, vPrimerNombre, vSegundoNombre, vPrimerApellido, vSegundoApellido);

            int nRegistros = myGridResults.Count;
            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (nRegistros < NumRegConsultaGrilla)
            {
                #region LlenadoGrilla
                //////////////////////////////////////////////////////////////////////////////////
                //////Fin del código de llenado de datos para la grilla 
                //////////////////////////////////////////////////////////////////////////////////
                gridViewsender.DataSource = myGridResults;
                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(Empleado), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression = Expression.Lambda<Func<Empleado, object>>(Expression.Convert(prop, typeof(object)), param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults;
                }
                #endregion
            
                gridViewsender.DataBind();
                tbResultados.Visible = true;
            }
            else
            {
                tbResultados.Visible = false;
                toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvEmpleados_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvEmpleados.SelectedRow);
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        { 
            #region OperacionRegistroContratos
            if (Request.QueryString["oP"] != null)
            {
                Contrato vContrato = new Contrato();
                vContrato.IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP").ToString());

                switch (Request.QueryString["oP"].ToString())
                {
                    case "Sol":
                        if (gvEmpleados.DataKeys[pRow.RowIndex]["IdRegional"] != null)
                            vContrato.IdRegionalEmpSol = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdRegional"].ToString()));
                        if (gvEmpleados.DataKeys[pRow.RowIndex]["IdDependencia"] != null)
                            vContrato.IdDependenciaEmpSol = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdDependencia"].ToString()));
                        if (gvEmpleados.DataKeys[pRow.RowIndex]["IdCargo"] != null)
                            vContrato.IdCargoEmpSol = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdCargo"].ToString()));
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[2].Text).Equals(string.Empty))
                            vContrato.IdEmpleadoSolicitante = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[2].Text));
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[3].Text).Equals(string.Empty))
                            vContrato.NombreEmpleadoSolicitante = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[3].Text);
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[4].Text).Equals(string.Empty))
                            vContrato.RegionalEmpleadoSolicitante = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[4].Text);
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[5].Text).Equals(string.Empty))
                            vContrato.DependenciaSolicitante = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[5].Text);
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[6].Text).Equals(string.Empty))
                            vContrato.CargoSolicitante = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[6].Text);
                        vContratoService.LupaEmpleadoInsertarSolicitanteContrato(vContrato);
                        break;
                    case "OrdG":
                        if (gvEmpleados.DataKeys[pRow.RowIndex]["IdRegional"] != null)
                            vContrato.IdRegionalEmpOrdG = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdRegional"].ToString()));
                        if (gvEmpleados.DataKeys[pRow.RowIndex]["IdDependencia"] != null)
                            vContrato.IdDependenciaEmpOrdG = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdDependencia"].ToString()));
                        if (gvEmpleados.DataKeys[pRow.RowIndex]["IdCargo"] != null)
                            vContrato.IdCargoEmpOrdG = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdCargo"].ToString()));
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[2].Text).Equals(string.Empty))
                            vContrato.IdEmpleadoOrdenadorGasto = Convert.ToInt32(HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[2].Text));

                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[3].Text).Equals(string.Empty))
                            vContrato.NombreEmpleadoOrdenadorGasto = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[3].Text);
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[4].Text).Equals(string.Empty))
                            vContrato.RegionalEmpOrdG = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[4].Text);
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[5].Text).Equals(string.Empty))
                            vContrato.DependenciaEmpOrdG = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[5].Text);
                        if (!HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[6].Text).Equals(string.Empty))
                            vContrato.CargoEmpOrdG = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[6].Text);

                        vContratoService.LupaEmpleadoInsertarOrdenadoGasto(vContrato);
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region DevolucionJavascript
            string TipoId, NumIden, TipoVinCon, PrimerNom, SegundoNom, PrimeApe, SegundoApe, Regional,
            Dependencia, Cargo, Direccion, Telefono, IdRegional;

            TipoId = NumIden = TipoVinCon = PrimerNom = SegundoNom = PrimeApe = SegundoApe = Regional =
            Dependencia = Cargo = Direccion = Telefono = IdRegional = string.Empty;

            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            //SetSessionParameter("RelacionarContratistas.PrimerNombre", ((Label)pRow.FindControl("labelPrimerNombre")).Text);
            
            TipoId = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[1].Text);
            returnValues += "pObj[" + (0) + "] = '" + TipoId.TrimEnd() + "';";

            NumIden = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[2].Text);
            returnValues += "pObj[" + (1) + "] = '" + NumIden.TrimEnd() + "';";

            TipoVinCon = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[3].Text);
            returnValues += "pObj[" + (2) + "] = '" + TipoVinCon.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["PrimerNombre"] != null)
                PrimerNom = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["PrimerNombre"].ToString());
            returnValues += "pObj[" + (3) + "] = '" + PrimerNom.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["SegundoNombre"] != null)
                SegundoNom = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["SegundoNombre"].ToString());
            returnValues += "pObj[" + (4) + "] = '" + SegundoNom.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["PrimerApellido"] != null)
                PrimeApe = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["PrimerApellido"].ToString());
            returnValues += "pObj[" + (5) + "] = '" + PrimeApe.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["SegundoApellido"] != null)
                SegundoApe = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["SegundoApellido"].ToString());
            returnValues += "pObj[" + (6) + "] = '" + SegundoApe.TrimEnd() + "';";

            Regional = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[5].Text);
            returnValues += "pObj[" + (7) + "] = '" + Regional.TrimEnd() + "';";

            Dependencia = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[6].Text);
            returnValues += "pObj[" + (8) + "] = '" + Dependencia.TrimEnd() + "';";

            Cargo = HttpUtility.HtmlDecode(gvEmpleados.Rows[pRow.RowIndex].Cells[7].Text);
            returnValues += "pObj[" + (9) + "] = '" + Cargo.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["Direccion"] != null)
                Direccion = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["Direccion"].ToString());
            returnValues += "pObj[" + (10) + "] = '" + Direccion.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["Telefono"] != null)
                Telefono = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["Telefono"].ToString());
            returnValues += "pObj[" + (11) + "] = '" + Telefono.TrimEnd() + "';";

            if (gvEmpleados.DataKeys[pRow.RowIndex]["IdRegional"] != null)
                IdRegional = HttpUtility.HtmlDecode(gvEmpleados.DataKeys[pRow.RowIndex]["IdRegional"].ToString());
            returnValues += "pObj[" + (12) + "] = '" + IdRegional.TrimEnd() + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
            #endregion
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvEmpleados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmpleados.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvEmpleados_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

}