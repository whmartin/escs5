﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;

/// <summary>
/// Página de registro de una vigencia futura
/// </summary>
public partial class Page_Contratos_Lupas_LupaEliminarModificacion : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;

    ContratoService vContratoService = new ContratoService();

    string PageName = "Contratos/LupaEliminarModificacion";
   // ContratoService vTipoSolicitudService = new ContratoService();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            CargarDatosIniciales();
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        GetScriptCloseDialogCallback(string.Empty);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método de guardado para la entidad Garantia
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado = 0;

            HistoricoSolicitudesEliminadas vSolModificacion = new HistoricoSolicitudesEliminadas();

            vSolModificacion.NombreRegional = txtRegional.Text;
            vSolModificacion.NumeroContrato = txtNumeroContrato.Text;
            vSolModificacion.JustificacionEliminacion = txtJustificacion.Text;
            vSolModificacion.TipoModificacion = txtTipoModificacion.Text;
            vSolModificacion.UsuarioCrea = GetSessionUser().NombreUsuario;
            vSolModificacion.IdConsModContractual = Convert.ToInt32(txtNumeroSolicitud.Text);
            vSolModificacion.IdContrato = Convert.ToInt32(hfIdContrato.Value);
            vSolModificacion.IdConsModContractualesEstado = Convert.ToInt32(hfidEstadoModificacion.Value);

            vResultado = vContratoService.InsertarSolModContractualHistorico(vSolModificacion);

            if (vResultado == 0)
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            else if (vResultado >= 1)
            {
                GetScriptCloseDialogCallback(string.Empty);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoEliminar+= new ToolBarDelegateLupa(btnEliminar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Eliminar Modificaciones Contractuales", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdConsModContractual = Convert.ToInt32(hfIdConsModContrato.Value);
            HistoricoSolicitudesEliminadas detalleSolicitud = new HistoricoSolicitudesEliminadas();
            detalleSolicitud = vContratoService.ConsultarSolicitudesEliminacion(vIdConsModContractual);

            txtRegional.Text = detalleSolicitud.NombreRegional;
            txtTipoModificacion.Text = detalleSolicitud.TipoModificacion;
            txtNumeroSolicitud.Text = detalleSolicitud.IdConsModContractual.ToString();
            txtNumeroContrato.Text = detalleSolicitud.NumeroContrato;
            hfidEstadoModificacion.Value = detalleSolicitud.IdConsModContractualesEstado.ToString();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["vIdContrato"]))
            hfIdContrato.Value = Request.QueryString["vIdContrato"];

        if (!string.IsNullOrEmpty(Request.QueryString["vIDCosModContractual"]))
            hfIdConsModContrato.Value = Request.QueryString["vIDCosModContractual"];
    }


    #endregion

}
