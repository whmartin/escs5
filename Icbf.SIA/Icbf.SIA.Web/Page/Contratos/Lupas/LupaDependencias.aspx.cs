﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_LupaDependencias : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa toolBar;
    
    ContratoService vContratoService = new ContratoService();
    
    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

        }
    }
    
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    protected void gvDependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDependencias.SelectedRow);
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            var idDependencia = gvDependencias.DataKeys[pRow.RowIndex].Values[0].ToString();
            var dependencia = gvDependencias.DataKeys[pRow.RowIndex].Values[1].ToString();

            string [] parametros = new string [] {idDependencia, dependencia};

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');

            this.GetScriptCloseDialogScriptsCallback(dialog,parametros.ToList());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.OcultarBotonNuevo(true);
            toolBar.OcultarBotonGuardar(true);
            toolBar.EstablecerTitulos("Información Dependencías", SolutionPage.Edit.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnBuscar_Click(object sender, EventArgs e)
    {
        var codigo = txtDependencia.Text;

        if (! string.IsNullOrEmpty(codigo))
        {
            gvDependencias.DataSource = vContratoService.ConsultarDependencias(codigo, 10);
           gvDependencias.DataBind();
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

}