﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaDependencias.aspx.cs" Inherits="Page_Contratos_Lupas_LupaDependencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
    <asp:HiddenField ID="hfIdContrato" runat="server" />
        <table width="90%" align="center">
            
              <tr class="rowB">
                <td>
                   Dependencia
                    </td>
                <td>
                 <asp:TextBox Width="600px" ID="txtDependencia" runat="server" />
                    
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:GridView ID="gvDependencias" OnSelectedIndexChanged="gvDependencias_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="Key,Value" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                            <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                    
                                                    <asp:BoundField HeaderText="Dependencía" DataField="Value" />

                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>

                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>