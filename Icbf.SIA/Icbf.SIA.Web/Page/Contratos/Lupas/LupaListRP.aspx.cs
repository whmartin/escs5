﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;

using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_Contratos_Lupas_ListRP : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/AsociarRPContrato";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    IntegrationService vIntegracionService = new IntegrationService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            CargarDatosIniciales();
        }

    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaVigencia();
            CargarListaRegional();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {

            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            if (Request.QueryString["esAdicion"] != null)
            {
                hfEsAdicion.Value = Request.QueryString["esAdicion"].ToString();
                toolBar.EstablecerTitulos("Asociar el Registro presupuestal a una adición", SolutionPage.Detail.ToString());
            }
            if (Request.QueryString["esCesion"] != null)
            {
                hfEsCesion.Value = Request.QueryString["esCesion"].ToString();
                toolBar.EstablecerTitulos("Asociar el Registro presupuestal a una cesión", SolutionPage.Detail.ToString());
            }
            gvRegistroInformacionPresupuestal.PageSize = PageSize();
            gvRegistroInformacionPresupuestal.EmptyDataText = EmptyDataText();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        //EliminarRegistro();
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfEsAdicion.Value) || !string.IsNullOrEmpty(hfEsCesion.Value))
        {
            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            string returnValues =
                    "<script language='javascript'> " +
                    "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                    "</script>";
            this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
        }
        else
        NavigateTo(SolutionPage.List);
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        SaveState(this.Master, PageName);
        Buscar();
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvRegistroInformacionPresupuestal, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            toolBar.LipiarMensajeError();
            int? vVigenciaFiscal = null;
            int? vRegionalICBF = null;
            int? vIdRP = null;
            Decimal? vValorTotalDesde = null;
            Decimal? vValorTotalHasta = null;
            DateTime? vFechaRPDesde = null;
            DateTime? vFechaRPHasta = null;
            DateTime fechavalidacion = new DateTime(1900,1,1);

            //if (!String.IsNullOrEmpty(txtFechaCDPDesde.Text))
            //{
            //    if (!DateTime.TryParse(txtFechaCDPDesde.Text, out fechavalidacion))
            //    {
            //        toolBar.MostrarMensajeError("Fecha RP desde no es Válida");
            //        return;
            //    }

            //    if (txtFechaCDPDesde.Text.StartsWith("_"))
            //    {
            //        toolBar.MostrarMensajeError("Fecha RP desde no es Válida");
            //        return;
            //    } 
            //}

            //if (!String.IsNullOrEmpty(txtFechaCDPHasta.Text))
            //{
            //    fechavalidacion = new DateTime(1900, 1, 1);
            //    if (!DateTime.TryParse(txtFechaCDPHasta.Text, out fechavalidacion))
            //    {
            //        toolBar.MostrarMensajeError("Fecha RP hasta no es Válida");
            //        return;
            //    }

            //    if (txtFechaCDPHasta.Text.StartsWith("_"))
            //    {
            //        toolBar.MostrarMensajeError("Fecha RP hasta no es Válida");
            //        return;
            //    }
            //}

            //if (!String.IsNullOrEmpty(txtFechaCDPDesde.Text) && String.IsNullOrEmpty(txtFechaCDPHasta.Text))
            //{
            //    toolBar.MostrarMensajeError("Debe registrar una fecha desde y una fecha hasta");
            //    return;
            //}

            //if (String.IsNullOrEmpty(txtFechaCDPDesde.Text) && !String.IsNullOrEmpty(txtFechaCDPHasta.Text))
            //{
            //    toolBar.MostrarMensajeError("Debe registrar una fecha desde y una fecha hasta");
            //    return;
            //}

            //if (hfesVigenciaFutura.Value == "1" && ddlVigenciaFiscal.SelectedItem.Text != hfAnioVigencia.Value)
            //{
            //    toolBar.MostrarMensajeError("El RP  debe  corresponder   a la misma vigencia   relacionada  en la   Vigencia Futura.");
            //    return;
            //}

            //if (!String.IsNullOrEmpty(txtFechaCDPDesde.Text) && !String.IsNullOrEmpty(txtFechaCDPHasta.Text))
            //{
            //    if (Convert.ToDateTime(txtFechaCDPHasta.Text).Date < Convert.ToDateTime(txtFechaCDPDesde.Text).Date)
            //    {
            //        toolBar.MostrarMensajeError("La Fecha RP hasta debe ser mayor que la fecha RP desde");
            //        return;
            //    }
            //}
           

            //if (txtValorTotalDesde.Text == string.Empty && txtValorTotalHasta.Text != string.Empty)
            //{
            //    toolBar.MostrarMensajeError("Debe registrar el valor  total hasta y valor Total Desde");
            //    return;
            //}

            //if (txtValorTotalDesde.Text != string.Empty && txtValorTotalHasta.Text == string.Empty)
            //{
            //    toolBar.MostrarMensajeError("Debe registrar el valor  total hasta y valor Total Desde"); 
            //    return;
            //}

            //if (txtValorTotalDesde.Text != string.Empty && txtValorTotalHasta.Text != string.Empty)
            //{
            //    string valorTotalDesde = txtValorTotalDesde.Text.Replace(@"$", "").Replace(",", "").Replace(".00", "");
            //    string valorTotalHasta = txtValorTotalHasta.Text.Replace(@"$", "").Replace(",", "").Replace(".00", "");
            //    decimal vDTotalDesde = 0;
            //    decimal vDTotalHasta = 0;

            //    if (!decimal.TryParse(valorTotalDesde, out vDTotalDesde))
            //    {
            //        toolBar.MostrarMensajeError("Ingrese un valor válido.");
            //        return;
            //    }

            //    if (!decimal.TryParse(valorTotalHasta, out vDTotalHasta))
            //    {
            //        toolBar.MostrarMensajeError("Ingrese un valor válido.");
            //        return;
            //    }
            //    vValorTotalDesde = vDTotalDesde;
            //    vValorTotalHasta = vDTotalHasta;

            //    if (vValorTotalHasta < vValorTotalDesde)
            //    {
            //        toolBar.MostrarMensajeError("El valor total  hasta debe ser mayor o igual que el valor total Desde");
            //        return;
            //    }
            //}

            //if (!String.IsNullOrEmpty(txtFechaCDPDesde.Text) && !String.IsNullOrEmpty(txtFechaCDPHasta.Text))
            //{
            //    if (ddlVigenciaFiscal.SelectedValue != "-1")
            //    {
            //        int vigencia = Convert.ToInt32(ddlVigenciaFiscal.SelectedItem.Text);
            //        if (vigencia != Convert.ToInt32(Convert.ToDateTime(txtFechaCDPDesde.Text).Year.ToString()) && vigencia != Convert.ToInt32(Convert.ToDateTime(txtFechaCDPHasta.Text).Year.ToString()))
            //        {
            //            toolBar.MostrarMensajeError("La fecha debe corresponder a la vigencia registrada.");
            //            return;
            //        }
            //    }
            //}

            if (txtNumeroRP.Text != string.Empty)
            {
                vIdRP = Convert.ToInt32(txtNumeroRP.Text);
            }

            if (ddlVigenciaFiscal.SelectedValue != "-1")
            {
                vVigenciaFiscal = Convert.ToInt32(ddlVigenciaFiscal.SelectedItem.Text);
            }
            if (ddlRegionalICBF.SelectedValue != "-1")
            {
                vRegionalICBF = Convert.ToInt32(ddlRegionalICBF.SelectedValue);
            }

            //if (!String.IsNullOrEmpty(txtFechaCDPDesde.Text))
            //{
            //    vFechaRPDesde = Convert.ToDateTime(txtFechaCDPDesde.Text);
            //}
            //if (!String.IsNullOrEmpty(txtFechaCDPHasta.Text))
            //{
            //    vFechaRPHasta = Convert.ToDateTime(txtFechaCDPHasta.Text);
            //}

            List<RPContrato> myGridResults = new List<RPContrato>();

            if(RbtTipoBusqueda.SelectedValue == "1")
            {
                myGridResults = vContratoService.ConsultarRPContratoss
                   (vIdRP, vRegionalICBF, vVigenciaFiscal, vValorTotalDesde, vValorTotalHasta, vFechaRPDesde, vFechaRPHasta);
            }
            else
                myGridResults = vIntegracionService.ObtenerRP(vIdRP.Value, vRegionalICBF.Value, 1, 1);

            int nRegistros = myGridResults.Count;
            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (nRegistros < NumRegConsultaGrilla)
            {
                //////////////////////////////////////////////////////////////////////////////////
                //////Fin del código de llenado de datos para la grilla 
                //////////////////////////////////////////////////////////////////////////////////
                gridViewsender.DataSource = myGridResults;
                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(RPContrato), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression = Expression.Lambda<Func<RPContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults;
                }

                gridViewsender.DataBind();
            }
            else
            {
                toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
            }



        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvRegistroInformacionPresupuestal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroInformacionPresupuestal.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    protected void gvRegistroInformacionPresupuestal_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvRegistroInformacionPresupuestal_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRegistroInformacionPresupuestal.SelectedRow);
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            bool esEnlinea = bool.Parse(gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Values["EsEnLinea"].ToString());

            if(esEnlinea)
            {
                string fechaRP = gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Values["FechaRP"].ToString();
                string idRegional = gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Values["IdRegional"].ToString();
                string valorInicial = gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Values["ValorInicialRP"].ToString();
                string numeroRP = gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Values["NumeroRP"].ToString();

                SetSessionParameter("Contrato.RPEnLinea", esEnlinea);
                SetSessionParameter("Contrato.RPRegional", idRegional);
                SetSessionParameter("Contrato.RPvalorInicial", valorInicial);
                SetSessionParameter("Contrato.RPfechaRP", fechaRP);
                SetSessionParameter("Contrato.RP", numeroRP);
            }
            else
            {
                string strValRP = gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Values["IdRP"].ToString();
                SetSessionParameter("Contrato.NumeroRP", strValRP);
            }

            if(!string.IsNullOrEmpty(hfEsAdicion.Value))
                NavigateTo("../../../Page/Contratos/AsociarRpContrato/DetailRP.aspx?esAdicion=" + hfEsAdicion.Value);
            else if (!string.IsNullOrEmpty(hfEsCesion.Value))
            {
                string uri = string.Format("../../../Page/Contratos/AsociarRpContrato/DetailRP.aspx?esCesion={0}&IdRegContrato={1}&IdVigenciaI={2}&IdVigenciaF={3}", hfEsCesion.Value, hfIdRegContrato.Value, hIdVigenciaI.Value, hIdVigenciaF.Value);
                NavigateTo(uri);
            }
            else if (!string.IsNullOrEmpty(hfesVigenciaFutura.Value))
                NavigateTo("../../../Page/Contratos/AsociarRpContrato/DetailRP.aspx?vesVigenciaFutura=" + hfesVigenciaFutura.Value
               + "&idContrato=" + hfIdContrato.Value + "&vidVigenciaFutura=" + hfidVigenciaFutura.Value + "&IdRegContrato=" + hfIdRegContrato.Value + "&vValorVigenciaFutura=" + hfValorVigencia.Value);
            else
                NavigateTo("../../../Page/Contratos/AsociarRpContrato/DetailRP.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {
        List<Vigencia> items = vRuboService.ConsultarVigencias(true);

        if (!string.IsNullOrEmpty(hIdVigenciaI.Value) || !string.IsNullOrEmpty(hIdVigenciaF.Value))
        {
            int idVigenciaInicial = string.IsNullOrEmpty(hIdVigenciaI.Value) ? 0 : int.Parse(hIdVigenciaI.Value);
            int idVigenciaFinal = string.IsNullOrEmpty(hIdVigenciaF.Value) ? 0 : int.Parse(hIdVigenciaF.Value);
            items = items.Where(e => e.IdVigencia == idVigenciaInicial || e.IdVigencia == idVigenciaFinal).ToList();
        }

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscal, items, "IdVigencia", "AcnoVigencia");
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {
            int idRegional = 0;

            if (! string.IsNullOrEmpty(hfIdRegContrato.Value))
                idRegional = Convert.ToInt32(hfIdRegContrato.Value);
            else
                idRegional = usuario.IdRegional.Value;

            if (idRegional != 0)
            {
                String codigoRegional = vRuboService.ConsultarRegional(idRegional).CodigoRegional;
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF, vRuboService.ConsultarRegionalPCIs(codigoRegional, null), "IdRegional", "CodigoNombreRegional");
            }
            else
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "CodigoNombreRegional");
        }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
        {
            hfIdContrato.Value = Request.QueryString["idContrato"];
        }
        if (!string.IsNullOrEmpty(Request.QueryString["IdRegContrato"]))
        {
            hfIdRegContrato.Value = Request.QueryString["IdRegContrato"];
        }
        if (!string.IsNullOrEmpty(Request.QueryString["vesVigenciaFutura"]))
            hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vidVigenciaFutura"]))
            hfidVigenciaFutura.Value = Request.QueryString["vidVigenciaFutura"];
        if (!string.IsNullOrEmpty(Request.QueryString["vValorVigenciaFutura"]))
            hfValorVigencia.Value = Request.QueryString["vValorVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdVigenciaI"]))
            hIdVigenciaI.Value = Request.QueryString["IdVigenciaI"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdVigenciaF"]))
            hIdVigenciaF.Value = Request.QueryString["IdVigenciaF"];

        if (!string.IsNullOrEmpty(Request.QueryString["vAnioVigencia"]))
            hfAnioVigencia.Value = Request.QueryString["vAnioVigencia"];
    }
}