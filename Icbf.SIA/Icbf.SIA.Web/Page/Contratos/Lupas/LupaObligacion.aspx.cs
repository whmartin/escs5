﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega la información y permite la selección de una obligación
/// </summary>
public partial class Page_Contratos_Lupas_LupaObligacion : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                BuscarObligacionesPorIdTipoContrato();
            }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            gvGestionarObligacion.PageSize = PageSize();
            gvGestionarObligacion.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Obligaciones", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvGestionarObligacion.DataKeys[gvGestionarObligacion.SelectedRow.RowIndex].Value.ToString()) + "';";
            for (int c = 1; c < 3; c++)
            {
                if (gvGestionarObligacion.Rows[gvGestionarObligacion.SelectedIndex].Cells[c].Text != "&nbsp;")
                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvGestionarObligacion.Rows[gvGestionarObligacion.SelectedIndex].Cells[c].Text) + "';";
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            SetSessionParameter("GestionarObligacion.IdTipoObligacion", ((HiddenField)pRow.FindControl("HiddenFieldIdTipoObligacion")).Value);
            returnValues += "pObj[" + (3) + "] = '" + ((HiddenField)pRow.FindControl("HiddenFieldIdTipoObligacion")).Value + "';";

            SetSessionParameter("GestionarObligacion.NombreTipoObligacion", ((HiddenField)pRow.FindControl("HiddenFieldNombreTipoObligacion")).Value);
            returnValues += "pObj[" + (4) + "] = '" + ((HiddenField)pRow.FindControl("HiddenFieldNombreTipoObligacion")).Value + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdGestionObligacion = null;
            int? vIdGestionClausula = null;
            String vNombreObligacion = null;
            int? vIdTipoObligacion = null;
            String vOrden = null;
            String vDescripcionObligacion = null;
            
            if (txtIdGestionClausula.Text != "")
            {
                vIdGestionClausula = Convert.ToInt32(txtIdGestionClausula.Text);
            }
            if (txtNombreObligacion.Text != "")
            {
                vNombreObligacion = Convert.ToString(txtNombreObligacion.Text);
            }
            if (txtIdTipoObligacion.Text != "")
            {
                vIdTipoObligacion = Convert.ToInt32(txtIdTipoObligacion.Text);
            }
            if (txtOrden.Text != "")
            {
                vOrden = Convert.ToString(txtOrden.Text);
            }
            if (txtDescripcionObligacion.Text != "")
            {
                vDescripcionObligacion = Convert.ToString(txtDescripcionObligacion.Text);
            }
            gvGestionarObligacion.DataSource = vContratoService.ConsultarGestionarObligacions(vIdGestionObligacion, vIdGestionClausula, vNombreObligacion, vIdTipoObligacion, vOrden, vDescripcionObligacion);
            gvGestionarObligacion.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvGestionarObligacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvGestionarObligacion.SelectedRow);
    }

    protected void gvGestionarObligacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGestionarObligacion.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("GestionarObligacion.Eliminado").ToString() == "1")
               RemoveSessionParameter("GestionarObligacion.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void BuscarObligacionesPorIdTipoContrato()
    {
        ContratoService objContratoService = new ContratoService();

        int idTipoContrato = Convert.ToInt32(GetSessionParameter("GestionarClausulaContrato.IdTipoContrato"));

        var result = from obligacion in objContratoService.ConsultarObligacions(null,null, idTipoContrato, true)
                     join tipoObligacion in vContratoService.ConsultarTipoObligacions(null,null,true)
                     on obligacion.IdTipoObligacion equals tipoObligacion.IdTipoObligacion
                     select new
                     {
                         obligacion.IdTipoObligacion,
                         obligacion.Descripcion,
                         obligacion.IdObligacion,
                         tipoObligacion.NombreTipoObligacion
                     };

        if (result != null)
        {
            gvGestionarObligacion.DataSource = result.ToList();
            gvGestionarObligacion.DataBind();
            gvGestionarObligacion.Visible = true;
        }
    }
}