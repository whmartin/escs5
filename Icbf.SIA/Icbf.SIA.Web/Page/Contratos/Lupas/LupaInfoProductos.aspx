﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoProductos.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoProductos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px" >
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo"  />
                                                    <asp:BoundField HeaderText="Código del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />
                                                    <asp:BoundField HeaderText="Cantidad / Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                    <asp:BoundField HeaderText="Valor total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tipo de Modificación" DataField="Modificacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                </td>
            </tr>
            
        </table>
    </asp:Panel>
    
   </asp:Content>
