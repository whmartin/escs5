<%@ Page Title="" Language="C#"MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaAseguradora.aspx.cs" Inherits="Page_Contratos_Contratos_LupaAseguradora" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        
         <tr class="rowB">
            <td class="Cell" colspan="2">
                Tipo de Persona 
            </td>
            <td class="Cell">
                Tipo Identificaci&oacute;n
            </td>
        </tr>
        
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdTipoPersona" Enabled="False">
                <%--<asp:ListItem Text="Juridica" Value="2" Selected="true"></asp:ListItem>--%>
                </asp:DropDownList>
            </td>
            <td class="Cell">
                 <asp:DropDownList runat="server" ID="ddlIdDListaTipoDocumento"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
                 <td class="Cell" style="width: 25%">
                <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de identificaci&oacute;n"></asp:Label>
                                <Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                                    FilterType="Numbers" ValidChars="/1234567890" />
                                <asp:RegularExpressionValidator runat="server" ID="revNumDocCeroNIT" ErrorMessage="El documento no debe empezar con cero (0)"
                                    ControlToValidate="txtNumeroIdentificacion" SetFocusOnError="true" ValidationGroup="btnBuscar"
                                    ForeColor="Red" Display="Dynamic" ValidationExpression="^[A-Za-z1-9]+.*$"></asp:RegularExpressionValidator>
                              <%-- <asp:CustomValidator ID="cvNIT" runat="server" ControlToValidate="txtNumeroIdentificacion"
                                    SetFocusOnError="true" ValidationGroup="btnBuscar" ForeColor="Red" Display="Dynamic"
                                    ClientValidationFunction="funValidaNumIdentificacion">El n�mero de identificaci�n no es v�lido</asp:CustomValidator>--%> 
                                     
            </td>
            <td>
                DV
            </td>
            <td class="Cell">
                <asp:Label ID="lblRazonSocial" runat="server" Text="Raz&oacute;n Social Aseguradora"></asp:Label>
            </td>
        </tr>
       <tr class="rowA">
            <td class="Cell" style="width: 25%">
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="11" Width="200px" AutoPostBack="True" OnTextChanged="txtNumeroIdentificacion_OnTextChanged"></asp:TextBox>
               <asp:Label runat="server" ID="lblSepDigVerif" Text="-"></asp:Label>
            </td>
            <td>
                 <asp:TextBox runat="server" ID="txtDV" Style="text-align: center;" Width="20" Enabled="false"></asp:TextBox>
            </td>
             <td>
                <asp:TextBox runat="server" ID="txtRazonSocial" MaxLength="50" Width="80%"></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTercero" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTercero" CellPadding="0" Height="16px"
                        OnSorting="gvTercero_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvTercero_PageIndexChanging" 
                        OnSelectedIndexChanged="gvTercero_SelectedIndexChanged" 
                        onrowdatabound="gvTercero_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona"  SortExpression="NombreTipoPersona"/>
                            <asp:BoundField HeaderText="Tipo Identificaci&oacute;n" DataField="NombreListaTipoDocumento"  SortExpression="NombreListaTipoDocumento"/>
                            <asp:BoundField HeaderText="Numero Identificaci&oacute;n" DataField="NumeroIdentificacion"  SortExpression="NumeroIdentificacion"/>
                            <asp:BoundField HeaderText="Raz&oacute;n Social Aseguradora" DataField="RazonSocial"  SortExpression="RazonSocial"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
