﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaContratoClausulaObligacion.aspx.cs" Inherits="Page_Contratos_Lupas_LupaContratoClausulaObligacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Gestión Clausula
            </td>
            <td>
                Id Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="hfIdGestionClausula"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nombre Clausula *
            </td>
            <td>
                Tipo Clausula *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreClausula"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoClausula"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Orden *
            </td>
            <td>
                Descripción Clausula *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtOrden"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionClausula"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <%--<asp:GridView runat="server" ID="gvGestionarClausulasContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdGestionClausula" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvGestionarClausulasContrato_PageIndexChanging" OnSelectedIndexChanged="gvGestionarClausulasContrato_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Id Gestión Clausula" DataField="IdGestionClausula" />
                            <asp:BoundField HeaderText="Id Contrato" DataField="IdContrato" />
                            <asp:BoundField HeaderText="Nombre Clausula" DataField="NombreClausula" />
                            <asp:BoundField HeaderText="Tipo Clausula" DataField="TipoClausula" />
                            <asp:BoundField HeaderText="Orden" DataField="Orden" />
                            <asp:BoundField HeaderText="Descripción Clausula" DataField="DescripcionClausula" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>--%>
                    <asp:GridView runat="server" ID="gvClausulaContrato" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdGestionClausula" 
                        CellPadding="0" Height="16px"  
                        SelectedRowStyle-CssClass="selected"
                        OnPageIndexChanging="gvClausulaContrato_PageIndexChanging" 
                        OnSelectedIndexChanged="gvClausulaContrato_SelectedIndexChanged"
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Cl&#225;usula" DataField="Orden"/>
                            <asp:BoundField HeaderText="Nombre Cl&#225;usula Contrato" DataField="DescripcionClausula"/>
                            <asp:BoundField HeaderText="Id Tipo Clausula" DataField="IdGestionClausula" Visible="false"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>

