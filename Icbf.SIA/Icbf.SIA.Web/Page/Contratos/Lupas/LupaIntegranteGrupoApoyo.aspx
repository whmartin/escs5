<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="LupaIntegranteGrupoApoyo.aspx.cs"
    Inherits="Page_LupaIntegrantesGupoApoyo_List" %>

<html>
<% if (bandera == "0")
   { %>
<asp:panel runat="server" id="pnlConsulta">
       <link href="../../../Styles/global.css" rel="Stylesheet" type="text/css" />
        <table width="90%" align="center" style="padding:20px; border-collapse:collapse;" >
            <tr style=" background-color: #F6F6F6;border-top: solid 1px #DFDFDF;border-bottom: solid 1px #DFDFDF;">
                <td style="width:50%">
                    B&uacute;squeda Integrantes
                </td>
                <td id="fTb">
                    <img src="../../../Image/btn/list.png" onclick="cargaGridTerceros();" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Tipo Documento *
                </td>
                <td>
                    Numero de documento *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlTipoDocTercero" width="230px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIdentificacionTercero" width="230px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer nombre *
                </td>
                <td>
                    Segundo Nombre *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombre1Tercero" width="230px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombre2Tercero" width="230px"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer apellido *
                </td>
                <td>
                    Segundo Apellido *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtApellido1Tercero" width="230px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtApellido2Tercero" width="230px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:panel>
<div id="dvGridIntegrantes">
</div>
<% }
   if (bandera == "1")
   { %>
<asp:panel runat="server" id="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvLupaIntegrantesGupoApoyo" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IDENTIFICACION"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvLupaIntegrantesGupoApoyo_PageIndexChanging" 
                        onrowdatabound="gvLupaIntegrantesGupoApoyo_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                    <asp:HiddenField ID="HiddenFieldNumerDocIntegrante" runat="server" Value='<%# Eval("Identificacion") %>' />
                                    <asp:HiddenField ID="HiddenFieldNombre1Integrante" runat="server" Value='<%# Eval("NOMBRE1")%> ' />
                                    <asp:HiddenField ID="HiddenFieldNombre2Integrante" runat="server" Value='<%#Eval("NOMBRE2")%> ' />
                                    <asp:HiddenField ID="HiddenFieldApellido1Integrante" runat="server" Value='<%# Eval("APELLIDO1")%> ' />
                                    <asp:HiddenField ID="HiddenFieldApellido2Integrante" runat="server" Value='<%#Eval("APELLIDO2")%> ' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Documento" DataField="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Numero de documento" DataField="Identificacion" />
                            <asp:BoundField HeaderText="Primer nombre" DataField="Nombre1" />
                            <asp:BoundField HeaderText="Segundo Nombre" DataField="Nombre2" />
                            <asp:BoundField HeaderText="Primer apellido" DataField="Apellido1" />
                            <asp:BoundField HeaderText="Segundo Apellido" DataField="Apellido2" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
</asp:panel>
<% } %>
</html>
