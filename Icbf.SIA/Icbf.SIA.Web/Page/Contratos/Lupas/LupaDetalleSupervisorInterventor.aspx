﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master"  CodeFile="LupaDetalleSupervisorInterventor.aspx.cs" Inherits="Page_Contratos_Lupas_LupaDetalleSupervisorInterventor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDSupervisorIntervContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td style="width: 408px">
                Supervisor y/o Interventor
            </td>
            <td>
                Tipo Supervisor y/o Interventor
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 408px">
                <asp:TextBox runat="server" ID="txtSupervisorInterventor" Width="350px" 
                    Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoSupervisorInterventor" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 408px">
                Fecha de Inicio
            </td>
            <td>
                Tipo de Persona
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 408px">
                <asp:TextBox runat="server" ID="txtFechaInicio" Width="350px" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoPersona" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 408px">
                Tipo de Identificaci&oacute;n
            </td>
            <td>
                N&uacute;mero de Identificaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 408px">
                <asp:TextBox runat="server" ID="txtTipoIdentificacion" Width="350px" 
                    Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 408px">
                <asp:Label runat="server" ID="lblRazonSocial" Text="Raz&oacute;n Social" Visible="False"></asp:Label>
                
            </td>
            <td>
                <asp:Label runat="server" ID="lblTipoVincContract" Text="Tipo Vinculaci&oacute;n Contractual" Visible="False"></asp:Label>
                
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 408px">
                <asp:TextBox runat="server" ID="txtRazonSocial" Rows="4" Width="350px" 
                    Enabled="false" Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoVinContrac" Width="85%" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
        </table>
        <asp:Panel runat="server" ID="pnPersonaNatral" Visible="False">
        <table width="90%" align="center">
        <tr class="rowB">
            <td style="width: 409px">
                <asp:Label runat="server" ID="lblPrimerApellido" Text="Primer Apellido" 
                    Visible="False"></asp:Label>
                
            </td>
            <td>
                <asp:Label runat="server" ID="lblSegundoApellido" Text="Segundo Apellido" 
                    Visible="False"></asp:Label>
                
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 409px">
                <asp:TextBox runat="server" ID="txtPrimerApellido" Width="350px" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" Width="85%" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 409px">
                <asp:Label runat="server" ID="lblPrimerNombre" Text="Primer Nombre" 
                    Visible="False"></asp:Label>
                
            </td>
            <td>
                <asp:Label runat="server" ID="lblSegundoNombre" Text="Segundo Nombre" 
                    Visible="False"></asp:Label>
                
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 409px">
                <asp:TextBox runat="server" ID="txtPrimerNombre" Width="350px" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" Width="85%" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 409px">
                <asp:Label runat="server" ID="lblRegional" Text="Regional" Visible="False"></asp:Label> 
            </td>
            <td>
                <asp:Label runat="server" ID="lblDependencia" Text="Dependencia" 
                    Visible="False"></asp:Label>
                
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 409px">
                <asp:TextBox runat="server" ID="txtRegional" Width="350px" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDependencia" Width="85%" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td style="width: 409px">
                <asp:Label runat="server" ID="lblCargo" Text="Cargo" Visible="False"></asp:Label>
                
                 
            </td>
            <td>
                <asp:Label runat="server" ID="lblDireccion" Text="Direcci&oacute;n" 
                    Visible="False" />
                
                 
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 409px">
                <asp:TextBox runat="server" ID="txtCargo" Width="350px" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDireccion" Width="85%" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 409px">
                <asp:Label runat="server" ID="lblTelefono" Text="Tel&eacute;fono" 
                    Visible="False"></asp:Label>
                
               
            </td>
            <td>
                <asp:Label runat="server" ID="lblEmail" Text="Correo Electr&oacute;nico" 
                    Visible="False"></asp:Label>
                 
                
                 
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 409px">
                <asp:TextBox runat="server" ID="txtTelefono" Width="350px" Enabled="false" 
                    Visible="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEmail" Width="85%" Enabled="false" Visible="False" 
                   ></asp:TextBox>
            </td>
        </tr>
        </table>
        </asp:Panel>
        <table width="90%" align="center">
        <tr>
                    <td colspan="2">
                        <asp:Panel ID="PnlReprLegal" runat="server" Visible="False">
                                    <h3 class="lbBloque">
                                        <asp:Label ID="Label5" runat="server" Text="Director de Interventoria"></asp:Label>
                                    </h3>
                                    <table style="width: 100%">
                                        <tr class="rowB">
                                            <td style="width: 50%">
                                                Número Contrato Interventoría
                                                
                                            </td>
                                            <td style="width: 50%">
                                                
                                                
                                            </td>
                                        </tr>
                                         <tr class="rowA">
                                            <td style="width: 50%">
                                                <asp:TextBox ID="txtNumeroContratoInter" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="50"></asp:TextBox>
                                                
                                            </td>
                                            <td style="width: 50%">
                                                
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td style="width: 50%">
                                                Tipo de Identificación
                                                
                                            </td>
                                            <td style="width: 50%">
                                                N&uacute;mero de Identificaci&oacute;n
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td style="width: 50%">
                                                <asp:TextBox runat="server" ID="txtTipoIdentificacionDirInt"></asp:TextBox>
                                              <%--<asp:DropDownList ID="ddlTipoIdentificacionDirInt" runat="server" Visible="false"></asp:DropDownList>--%>
                                            </td>
                                            <td style="width: 50%">
                                                <asp:TextBox ID="txtNumIdentificacionDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="51"></asp:TextBox>
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                Primer Nombre
                                                
                                            </td>
                                            <td>
                                                Segundo Nombre
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerNombreDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="52" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtSegundoNombreDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="53" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                Primer Apellido
                                                
                                            </td>
                                            <td>
                                                Segundo Apellido
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                <asp:TextBox ID="TxtPrimerApellidoDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="54" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtSegundoApellidoDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="55" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                        </tr>
                                        <tr class="rowB">
                                            <td>
                                                Tel&eacute;fono
                                                
                                            </td>
                                            <td>
                                                Correo Electr&oacute;nico
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="rowA">
                                            <td>
                                                <asp:TextBox ID="txtTelefonoDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="54" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmailDirInt" runat="server" Enabled="False" MaxLength="50"
                                                    TabIndex="55" Width="80%"></asp:TextBox>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                    </td>
                </tr>

        
        
    </table>
        
</asp:Content>

