﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaEliminarModificacion.aspx.cs" Inherits="Page_Contratos_Lupas_LupaEliminarModificacion" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdConsModContrato" runat="server" />
    <asp:HiddenField ID="hfidEstadoModificacion" runat="server" />    

    <table width="90%" align="center">
        <tr class="rowB">
            <td style="width: 487px">
                Regional               
            </td>
            <td>
                N&#250;mero de Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 487px">
                <asp:TextBox runat="server" ID="txtRegional" MaxLength="26" Width="50%" Enabled="False" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="26" Width="50%" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td style="width: 487px">
                No. de Solicitud
               
            </td>
            <td>
                Tipo Modificaci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 487px">
                <asp:TextBox runat="server" ID="txtNumeroSolicitud" MaxLength="26" Width="50%" Enabled="False"></asp:TextBox>
            </td>
            <td>                
                <asp:TextBox runat="server" ID="txtTipoModificacion" MaxLength="26" Width="50%" Enabled="False"></asp:TextBox>
            </td>
        </tr>        
         <tr class="rowB">
            <td style="width: 487px">
               Justificaci&#243;n de la Eliminaci&#243;n
                <asp:RequiredFieldValidator runat="server" ID="rvJustificacion" ControlToValidate="txtJustificacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnEliminar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            
        </tr>
        <tr class="rowA">
            <td style="width: 487px">
                 <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Width="400px" MaxLength2="400"
                    onKeyDown="limitText(this,500);" Rows="4" Style="resize: none" onKeyUp="limitText(this,500);"></asp:TextBox>
            </td>
            
        </tr>

        <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }        
    </script>
    </table>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
</asp:Content>
