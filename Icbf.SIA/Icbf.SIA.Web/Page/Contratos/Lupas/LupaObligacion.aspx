﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaObligacion.aspx.cs" Inherits="Page_Contratos_Lupas_LupaObligacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Gestión Obligación
            </td>
            <td>
                Id Gestión Clausula *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="hfIdGestionObligacion"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdGestionClausula"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nombre Obligacion *
            </td>
            <td>
                Tipo Obligación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreObligacion"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdTipoObligacion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Orden *
            </td>
            <td>
                Descripción Obligación *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtOrden"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionObligacion"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
<%--                    <asp:GridView runat="server" ID="gvGestionarObligacion" 
                        AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdGestionObligacion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvGestionarObligacion_PageIndexChanging" 
                        OnSelectedIndexChanged="gvGestionarObligacion_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                    <asp:HiddenField ID="HiddenFieldIdTipoObligacion" runat="server" 
                                        Value='<%# Eval("IdTipoObligacion") %>' />
                                    <asp:HiddenField ID="HiddenFieldNombreTipoObligacion" runat="server" 
                                        Value='<%# Eval("NombreTipoObligacion") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Id Gestión Obligación" DataField="IdGestionObligacion" />
                            <asp:BoundField HeaderText="Id Gestión Clausula" DataField="IdGestionClausula" />
                            <asp:BoundField HeaderText="Nombre Obligacion" DataField="NombreObligacion" />
                            <asp:BoundField HeaderText="Tipo Obligación" DataField="IdTipoObligacion" />
                            <asp:BoundField HeaderText="Orden" DataField="Orden" />
                            <asp:BoundField HeaderText="Descripción Obligación" DataField="DescripcionObligacion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>--%>

                    <asp:GridView runat="server" ID="gvGestionarObligacion" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdObligacion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvGestionarObligacion_PageIndexChanging" 
                        OnSelectedIndexChanged="gvGestionarObligacion_SelectedIndexChanged" AllowSorting="True" 
                        SelectedRowStyle-CssClass="selected">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                    <asp:HiddenField ID="HiddenFieldIdTipoObligacion" runat="server" 
                                        Value='<%# Eval("IdTipoObligacion") %>' />
                                    <asp:HiddenField ID="HiddenFieldNombreTipoObligacion" runat="server" 
                                        Value='<%# Eval("NombreTipoObligacion") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Obligación" DataField="NombreTipoObligacion" SortExpression="NombreTipoObligacion" />

                         <%--   <asp:BoundField HeaderText="Tipo Contrato" DataField="NombreTipoContrato" SortExpression="NombreTipoContrato"/>--%>

                           <asp:BoundField HeaderText="Descripción" DataField="Descripcion" SortExpression="Descripcion"/>

                            <%--<asp:CheckBoxField HeaderText="Estado" DataField="Estado" />--%>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>

                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>

