﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoVigenciasFuturas.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoVigenciasFuturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                   <asp:GridView ID="gvVigFuturas" runat="server" AutoGenerateColumns="false" 
                                                DataKeyNames="IDVigenciaFuturas" GridLines="None" Width="100%" 
                                                CellPadding="8" Height="16px" Visible="True" >
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número del Radicado Vigencia Futura" DataField="NumeroRadicado" />
                                                    <asp:BoundField HeaderText="Fecha de Expedición Vigencia Futura" DataField="FechaExpedicion" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor Vigencia Futura" DataField="ValorVigenciaFutura" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Año Vigencia Futura" DataField="AnioVigencia"  />
                                                    <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" >  
                                                        <ItemTemplate> 
                                                            <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarGvVigFuturasClick" OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" >
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate> 
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                </td>
            </tr>
            
        </table>
    </asp:Panel>
    
   </asp:Content>
