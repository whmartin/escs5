<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaVigenciasFuturas.aspx.cs" Inherits="Page_Contratos_Lupas_LupaVigenciasFuturas" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&#250;mero del Radicado Vigencia Futura *
                <asp:RequiredFieldValidator runat="server" ID="rvtxtNumeroRadicadoVigenciaFutura"
                    ControlToValidate="txtNumeroRadicadoVigenciaFutura" SetFocusOnError="true" ErrorMessage="Campo Requerido"
                    Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroGarantia" runat="server" TargetControlID="txtNumeroRadicadoVigenciaFutura"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                Fecha de Expedici&#243;n Vigencia Futura *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroRadicadoVigenciaFutura" MaxLength="26" Width="45%"></asp:TextBox>
            </td>
            <td>
                <uc1:fecha ID="txtFechaExpedicionVigenciaFutura" runat="server" Enabled="True" Requerid="True" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Vigencia Futura *
                <asp:RequiredFieldValidator runat="server" ID="rvtxtValorVigenciaFutura" ControlToValidate="txtValorVigenciaFutura"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <Ajax:FilteredTextBoxExtender ID="ftValorVigenciaFutura" runat="server" TargetControlID="txtValorVigenciaFutura"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                A&#241;o Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorVigenciaFutura" MaxLength="26" Width="45%" ></asp:TextBox>
            </td>
            <td>                
                <asp:DropDownList ID="ddlAnio" runat="server" Enabled="true"  AutoPostBack="true" OnSelectedIndexChanged="ddlAnioSelectedIndexChanged">  </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
</asp:Content>
