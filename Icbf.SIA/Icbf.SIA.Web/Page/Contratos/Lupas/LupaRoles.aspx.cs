﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Linq.Expressions;
using System.Reflection;




/// <summary>
/// Página de consulta a través de filtros para la entidad NumeroProcesos
/// </summary>
public partial class Page_Contratos_Lupas_LupaRoles  : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    OferenteService vOferenteService = new OferenteService();
    ContratoService vContratoService = new ContratoService();
    bool _sorted = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);     
                  
            gvRoles.PageSize = PageSize();
            gvRoles.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Roles", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvRoles, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString["idSupervisor"]))
            {
              int idRol = int.Parse(HttpUtility.HtmlDecode(gvRoles.DataKeys[gvRoles.SelectedRow.RowIndex].Values["IdRol"].ToString()));
              int idSupervisorInter = int.Parse(Request.QueryString["idSupervisor"]);

                var result = vContratoService.ActualizarRolSuerpvisor(idSupervisorInter,idRol);

                if (result == 1)
	                GetScriptCloseDialogCallback(string.Empty);
                else
                    toolBar.MostrarMensajeError("Ocurrio un error al asociar el rol al supervisor.");
            }
            else
            {
                string returnValues =
               "<script language='javascript'> " +
               "var pObj = Array();";

                returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvRoles.DataKeys[gvRoles.SelectedRow.RowIndex].Values["IdRol"].ToString()) + "';";

                returnValues += "pObj[" + (1) + "] = '" + HttpUtility.HtmlDecode(gvRoles.Rows[gvRoles.SelectedIndex].Cells[1].Text) + "';";

                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                    "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRoles_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRoles.SelectedRow);
    }
    protected void gvRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRoles.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvRoles_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {

            Boolean? vEstado = null;
            String vCodigo = null;

            vCodigo = txtCodigo.Text;

            if (rblInactivo.SelectedValue != "-1")
            {
                vEstado = Convert.ToBoolean(rblInactivo.SelectedValue);
            }


            var myGridResults = vContratoService.ConsultarVariosRoles(vEstado, vCodigo);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(RolesContrato), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<RolesContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    private void CargarDatosIniciales()
    {
        try
        {      
         
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            //rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            //rblInactivo.Items.Insert(0, new ListItem("Todos", "-1"));
            rblInactivo.SelectedValue = "true";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


}
