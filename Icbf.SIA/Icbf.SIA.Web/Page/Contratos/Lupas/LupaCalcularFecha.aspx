﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaCalcularFecha.aspx.cs" Inherits="Page_Contratos_Lupas_LupaCalcularFecha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <asp:HiddenField ID="hfIdTipoContrato" runat="server" />
<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            
              <tr class="rowB">
                <td>
                    Fecha Inicio Contrato
                </td>
                <td>
                    <asp:Label ID="lblFechaInicioContrato" runat="server" />

                </td>
            </tr>
             <tr class="rowB">
                <td>Meses
                    <asp:RequiredFieldValidator runat="server" ID="rfvMeses" ControlToValidate="txtMeses"
                    SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" ></asp:RequiredFieldValidator>
                </td>
                <td>
                     <asp:TextBox ID="txtMeses" Text="0" runat="server" AutoPostBack="true" OnTextChanged="txtMeses_TextChanged" />

                      <Ajax:FilteredTextBoxExtender ID="fttxtMeses" runat="server" TargetControlID="txtMeses"
                                                FilterType="Numbers"  />
                </td>
            </tr>
            <tr class="rowB">
                <td>Dias
                                        <asp:RequiredFieldValidator runat="server" ID="rfvDias" ControlToValidate="txtDias"
                    SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" ></asp:RequiredFieldValidator>
                </td>
                <td>
                     <asp:TextBox ID="txtDias" Text="0" runat="server" AutoPostBack="true" OnTextChanged="txtMeses_TextChanged" />
                                          <Ajax:FilteredTextBoxExtender ID="fttxtDias" runat="server" TargetControlID="txtDias"
                                                FilterType="Numbers"  />
                </td>
            </tr>
                <tr class="rowB">
                <td>
                    Fecha Final Contrato
                </td>
                <td>
                    <asp:Label ID="lblFechaFinalContrato" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>