﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoCDP.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoCDP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <%--<asp:GridView runat="server" ID="gvInfoCDP" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDGarantia,FechaAprobacionGarantia,FechaCertificacionGarantia,IdSucursalAseguradoraContrato" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvInfoCDP_PageIndexChanging" 
                        OnSelectedIndexChanged="gvInfoCDP_SelectedIndexChanged">
                        <Columns>
                        
                        <asp:BoundField HeaderText="Número CDP" DataField="IdCDP"  SortExpression="IdRP"/>
                        <asp:BoundField HeaderText="Fecha de Expedicion del CDP" DataField="FechaExpedicionRP" SortExpression="FechaExpedicionRP" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP"  SortExpression="ValorCDP" DataFormatString="{0:C}" />   
                        </Columns>

                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>--%>
                    
                    <asp:GridView ID="gvCDPEncabezado" runat="server" AutoGenerateColumns="false" 
                                                DataKeyNames="IdContratosCDP,ValorCDP,NumeroCDP" 
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <%--<asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center" >  
                                                            <ItemTemplate> 
                                                            <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" >
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                            </ItemTemplate> 
                                                    </asp:TemplateField>--%>
                                                    <%--<asp:BoundField HeaderText="Regional" DataField="Regional" />--%>
                                                    <%--<asp:BoundField HeaderText="Área" DataField="Area" />--%>
                                                    <asp:BoundField HeaderText="Número del CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha de Expedición" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor del CDP" DataField="ValorCDP" DataFormatString="{0:c}" />
                                                    <%--<asp:BoundField HeaderText="Rubro presupuestal" DataField="RubroPresupuestal" />--%>
                                                    <%--<asp:BoundField HeaderText="Tipo fuente financiamiento" DataField="TipofuenteFinanciamiento" />--%>
                                                    <%--<asp:BoundField HeaderText="Recurso presupuestal" DataField="RecursoPresupuestal" />--%>
                                                    <%--<asp:BoundField HeaderText="Dependencia afectación gastos" DataField="DependenciaAfectacionGastos" />--%>
                                                    <%--<asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumentoSoporte" />--%>
                                                    <%--<asp:BoundField HeaderText="Tipo situación fondos" DataField="TipoSituacionFondos" />--%>
                                                    <%--<asp:BoundField HeaderText="Consecutivo plan de compras" DataField="ConsecutivoPlanCompras" />--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>

                </td>
            </tr>
            <tr class="rowA">
                <td>
                    Valor Total CDP
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtTotalCDP" Enabled="False"></asp:TextBox>
                </td>
            </tr>
                       <tr class="rowB">
                <td>Historico Adiciones</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    <asp:GridView ID="gvInfoModificaciones" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="ValorRP" GridLines="None" Height="16px" OnPageIndexChanging="gvInfoModificaciones_PageIndexChanging"  Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ValorCDP" HeaderText="Valor del CDP" SortExpression="ValorCDP" DataFormatString="{0:C}" />
                            <asp:BoundField DataField="FechaSubscripcion" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha de Subscripción" SortExpression="FechaSubscripcion" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                <td>Rubros CDP</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvCDPDetalle" runat="server" AutoGenerateColumns="false" 
                                                DataKeyNames="IdContratosCDP,ValorCDP" 
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <%--<asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center" >  
                                                            <ItemTemplate> 
                                                            <asp:LinkButton ID="btnEliminar" Enabled="False" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" >
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                            </ItemTemplate> 
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="ConsecutivoPlanCompras" />
                                                    <%--<asp:BoundField HeaderText="Regional" DataField="Regional" />--%>
                                                    <%--<asp:BoundField HeaderText="Área" DataField="Area" />--%>
                                                    <asp:BoundField HeaderText="Código Rubro" DataField="CodigoRubro" />
                                                    <asp:BoundField HeaderText="Descripción Rubro" DataField="RubroPresupuestal" />
                                                    <%--<asp:BoundField HeaderText="Tipo fuente financiamiento" DataField="TipofuenteFinanciamiento" />--%>
                                                    <asp:BoundField HeaderText="Recurso" DataField="RecursoPresupuestal" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="ValorRubro" DataFormatString="{0:c}"  />
                                                    <%--<asp:BoundField HeaderText="Dependencia afectación gastos" DataField="DependenciaAfectacionGastos" />--%>
                                                    <%--<asp:BoundField HeaderText="Tipo documento" DataField="TipoDocumentoSoporte" />--%>
                                                    <%--<asp:BoundField HeaderText="Tipo situación fondos" DataField="TipoSituacionFondos" />--%>
                                                    
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>

                </td>
            </tr>
        </table>
    </asp:Panel>
    
   </asp:Content>