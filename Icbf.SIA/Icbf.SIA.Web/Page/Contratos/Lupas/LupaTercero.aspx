﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaTercero.aspx.cs" Inherits="Page_Contratos_Lupas_LupaTercero" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    <div style="display: none">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </div>
<asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblTipoPersona" runat="server" Text="Tipo de persona"></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTipoDocumento" runat="server" Text="Tipo de identificaci&oacute;n"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoPersona" Width="90%" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ErrorMessage="Seleccione un tipo de persona"
                        ControlToValidate="ddlTipoPersona" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="rfvTipoPersona"
                        WarningIconImageUrl="../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlTipoDoc" Width="90%">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoDoc" ErrorMessage="Seleccione un tipo de identificación"
                        ControlToValidate="ddlTipoDoc" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" InitialValue="-1">*</asp:RequiredFieldValidator>
                    <Ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="rfvTipoDoc"
                        WarningIconImageUrl="../Image/Oferente/warning.png" CssClass="CustomValidatorCalloutStyle" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblNumeroDoc" runat="server" Text="N&uacute;mero de documento"></asp:Label>
                </td>
                <td class="Cell">
                    <asp:Label ID="lblTercero" runat="server" Text="Tercero"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumeroDoc" onkeypress="return EsNumero(event)"
                        Width="89%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtTercero" Width="89%" AutoPostBack="True"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlEstado" Width="70%" TabIndex="1">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedor" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTercero" CellPadding="0"
                        Height="16px" AllowSorting="true" OnPageIndexChanging="gvProveedor_PageIndexChanging"
                        OnSelectedIndexChanged="gvProveedor_SelectedIndexChanged" OnSorting="gvProveedor_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                    <asp:Label ID="labelPrimerNombre" runat="server" 
                                        Text='<%# Bind("PrimerNombre") %>' Visible="false" />
                                    <asp:Label ID="labelSegundoNombre" runat="server" 
                                        Text='<%# Bind("SegundoNombre") %>' Visible="false" />
                                    <asp:Label ID="labelPrimerApellido" runat="server" 
                                        Text='<%# Bind("PrimerApellido") %>' Visible="false" />
                                    <asp:Label ID="labelSegundoApellido" runat="server" 
                                        Text='<%# Bind("SegundoApellido") %>' Visible="false" />
                                    <asp:Label ID="labelIdTercero" runat="server" 
                                        Text='<%# Bind("IdTercero") %>' Visible="false" />
                                    <asp:Label ID="labelRazonSocial" runat="server" 
                                        Text='<%# Bind("RazonSocial") %>' Visible="false" />
                                    <asp:Label ID="labelNumeroIdentificacion" runat="server" 
                                        Text='<%# Bind("NumeroIdentificacion") %>' Visible="false" />
                                    <asp:Label ID="labelEmail" runat="server" Text='<%# Bind("Email") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona" />
                            <asp:BoundField HeaderText="Tipo de Identificaci&oacute;n" DataField="NombreListaTipoDocumento"
                                SortExpression="NombreListaTipoDocumento" />
                            <asp:BoundField HeaderText="N&uacute;mero Identificaci&oacute;n" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Tercero" DataField="Nombre_Razonsocial" SortExpression="Nombre_Razonsocial" />
                            <asp:BoundField HeaderText="Correo Electr&oacute;nico" DataField="Email" SortExpression="Email" />
                            <asp:BoundField HeaderText="Validaci&oacute;n" DataField="NombreEstadoTercero" SortExpression="NombreEstadoTercero" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

