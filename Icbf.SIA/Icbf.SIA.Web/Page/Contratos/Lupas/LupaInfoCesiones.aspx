﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General/General/Master/Lupa.master" CodeFile="LupaInfoCesiones.aspx.cs" Inherits="Page_Contratos_Lupas_LupaInfoCesiones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
     <asp:HiddenField ID="hfIdContrato" runat="server" />

<asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td colspan="2">
                    <asp:GridView ID="gvCesiones" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px" >
                                                <Columns>
                                                    <asp:BoundField HeaderText="N&#250;mero de Solicitud Cesi&#243;n" DataField="IDCosModContractual"  />
                                                    <asp:BoundField HeaderText="Valor Ejecutado" DataField="ValorEjecutado" SortExpression="ValorEjecutado" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Cesi&#243;n" DataField="ValorCesion" SortExpression="ValorCesion" DataFormatString="{0:c}" />                                                    
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                </td>
            </tr>
            
        </table>
    </asp:Panel>
    
   </asp:Content>
