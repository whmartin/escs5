﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Página que despliega la información y permite la selección de un contratista
/// </summary>
public partial class Page_Contratos_Lupas_LupaContratistaContrato : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
        Buscar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            gvContratista.PageSize = PageSize();
            gvContratista.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Lista Contratistas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            List<datosContratista> listaContratistas = new List<datosContratista>();

            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DataBaseConnectionString"].ConnectionString);
                conn.Open();

                SqlCommand Query = new SqlCommand("usp_RubOnline_Contrato_Consulta_ContratistaContrato", conn);
                Query.CommandType = CommandType.StoredProcedure;
                Query.Parameters.Add("@IDCONTRATO", SqlDbType.Int);
                String tmp = GetSessionParameter("GestionarClausulaContrato.IdContrato").ToString();
                Query.Parameters["@IDCONTRATO"].Value = Convert.ToInt32(tmp);

                SqlDataReader reader = Query.ExecuteReader();

                

                while (reader.Read())
                {
                    datosContratista objDatosGridView = new datosContratista();
                    objDatosGridView.IdContratistaContrato = Convert.ToInt32(reader["IdContratistaContrato"].ToString());
                    objDatosGridView.NumeroIdentificacion = reader["NumeroIdentificacion"].ToString();
                    objDatosGridView.RazonSocial = reader["RAZONSOCIAL"].ToString();
                    listaContratistas.Add(objDatosGridView);
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }

            gvContratista.DataSource = listaContratistas.ToList();
            gvContratista.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        
    }
    protected void ddlIdCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvContratista.DataKeys[gvContratista.SelectedRow.RowIndex].Value.ToString()) + "';";
            

            for (int c = 1; c < 4; c++)
            {
                if (gvContratista.Rows[gvContratista.SelectedIndex].Cells[c].Text != "&nbsp;")
                {
                    
                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvContratista.Rows[gvContratista.SelectedIndex].Cells[c].Text) + "';";
                }
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvSucursal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratista.PageIndex = e.NewPageIndex;
        Buscar();
    }
    protected void gvSucursal_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContratista.SelectedRow);
    }
}

public class datosContratista
{
    public int IdContratistaContrato { get; set; }
    public String NumeroIdentificacion { get; set; }
    public String RazonSocial { get; set; }
    public String ClaseEntidad { get; set; }

    public datosContratista()
    {

    }
}