﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaRoles.aspx.cs" Inherits="Page_Contratos_Lupas_LupaRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">   

<asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
             <tr class="rowB">
                <td class="Cell">
                    Nombre del Rol
                </td>
                <td class="Cell">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtCodigo" Width="400px"  ></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>       
            
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
             <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRoles" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdRol" CellPadding="0"
                        Height="16px" OnSorting="gvRoles_Sorting" AllowSorting="True" OnPageIndexChanging="gvRoles_PageIndexChanging"
                        OnSelectedIndexChanged="gvRoles_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre del Rol" DataField="NombreRol" SortExpression="NumeroProcesoGenerado"/>
                            <asp:TemplateField HeaderText="Estado" SortExpression="Inactivo">
                                <ItemTemplate>
                                    <asp:Label ID="lblInactivo" runat="server" Text='<%# (bool) Eval("Estado") ? "Activo" : "Inactivo" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>