<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaRegInfoPresupuestalEnLinea.aspx.cs" Inherits="Page_RegistroInformacionPresupuestalEnLinea_List" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfidVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdVigencia" runat="server" />
    <asp:HiddenField ID="hfIdRegional" runat="server" />
    <asp:HiddenField ID="hfAnioVigencia" runat="server" />
    <asp:HiddenField ID="hfIdContratoCDP" runat="server" />
    <asp:HiddenField ID="hfEsPrecontractual" runat="server" />


    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Vigencia Fiscal
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlVigenciaFiscal"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                    ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    Regional ICBF*
                    <asp:RequiredFieldValidator runat="server" ID="rfvEntidadFinanciera" ControlToValidate="ddlRegionalICBF"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlVigenciaFiscal">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlRegionalICBF" AutoPostBack="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Número CDP
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtNumeroCDP"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    Fuente</td>
            </tr>
            <tr class="rowA">
                <td class="Cell" style="height: 27px">
                    <asp:TextBox runat="server" ID="txtNumeroCDP" MaxLength="25"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroCDP" runat="server" TargetControlID="txtNumeroCDP"
                        FilterType="Numbers" ValidChars="" />
                </td>
                <td class="Cell" style="height: 27px">
                    <asp:RadioButtonList ID="RbtTipoBusqueda" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RbtTipoBusqueda_SelectedIndexChanged" RepeatDirection="Horizontal">
                        <asp:ListItem  Text="ETL" Value="1" />
                        <asp:ListItem Selected="True" Text="Linea" Value="0" />
                    </asp:RadioButtonList>
                </td>
                </tr>
        </table>
                <table width="90%" id="pnBusquedaEtl" runat="server" style="display:none" align="center" >
                    <tr class="rowB">
                        <td class="Cell">Valor Total Desde </td>
                        <td class="Cell">Valor Total Hasta </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                            <asp:TextBox ID="txtValorTotalDesde" runat="server" MaxLength="26" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftValorTotalDesde" runat="server" FilterType="Custom,Numbers" TargetControlID="txtValorTotalDesde" ValidChars="$,." />
                        </td>
                        <td class="Cell">
                            <asp:TextBox ID="txtValorTotalHasta" runat="server" MaxLength="26" Width="250px"></asp:TextBox>
                            <Ajax:FilteredTextBoxExtender ID="ftValorTotalHasta" runat="server" FilterType="Custom,Numbers" TargetControlID="txtValorTotalHasta" ValidChars="$,." />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="Cell" style="height: 26px">Fecha CDP Desde
                            <asp:CompareValidator ID="cvFechaDesdeValida" runat="server" ControlToValidate="txtFechaCDPDesde$txtFecha" ErrorMessage=" Ingrese Fecha Desde Válida" ForeColor="red" Operator="DataTypeCheck" Type="Date" ValidationGroup="btnBuscar"></asp:CompareValidator>
                            <br />
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtFechaCDPDesde$txtFecha" ControlToValidate="txtFechaCDPHasta$txtFecha" Display="Dynamic" ErrorMessage=" La Fecha CDP Hasta debe ser mayor o igual que la Fecha CDP Desde" ForeColor="Red" Operator="GreaterThanEqual" SetFocusOnError="True" Type="Date" ValidationGroup="btnBuscar"></asp:CompareValidator>
                        </td>
                        <td class="Cell" style="height: 26px" valign="Top">Fecha CDP Hasta
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFechaCDPHasta$txtFecha" ErrorMessage=" Ingrese Fecha Hasta Válida" ForeColor="red" Operator="DataTypeCheck" Type="Date" ValidationGroup="btnBuscar"></asp:CompareValidator>
                            <br />
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell">
                            <uc1:fecha ID="txtFechaCDPDesde" runat="server" Enabled="True" Requerid="False" Width="80%" />
                        </td>
                        <td class="Cell">
                            <uc1:fecha ID="txtFechaCDPHasta" runat="server" Enabled="True" Requerid="False" Width="80%" />
                        </td>
                    </tr>
                    <tr class="rowB">
                        <td class="Cell" style="height: 26px">Cantidad de Registros a Mostrar en la Grilla </td>
                        <td class="Cell" style="height: 26px">
                            <asp:Label ID="Label2" runat="server" Text="Área" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell" style="height: 26px">
                            <asp:DropDownList ID="ddlNumeroRegistros" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Cell" style="height: 26px">
                            <asp:DropDownList ID="ddlArea" runat="server" Visible="False">
                            </asp:DropDownList>
                        </td>
                    </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
           <tr class="rowB">
                <td class="Cell" style="height: 26px">
                    CDP
                </td>
            </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvRegistroInformacionPresupuestal" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="ValorCDP,NumeroCDP" CellPadding="0"
                        Height="16px" OnSorting="gvRegistroInformacionPresupuestal_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvRegistroInformacionPresupuestal_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chbContiene" runat="server" />
                                </ItemTemplate>
                                <ControlStyle Width="25px" />
                                <FooterStyle Width="25px" />
                                <HeaderStyle Width="25px" />
                                <ItemStyle Width="25px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Vigencia Fiscal" DataField="VigenciaFiscal" SortExpression="VigenciaFiscal" Visible="False" />
                            <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" SortExpression="NumeroCDP" />
                            <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" SortExpression="FechaCDP"  />
                            <asp:TemplateField HeaderText="Valor CDP" SortExpression="ValorCDP">
                                <ItemTemplate>
                                    <asp:Label Text='<%# string.Format("{0:C}", Convert.ToDecimal(Eval("ValorCDP"))) %>' ID="lblValorCDP" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="rowB">
                        <td class="Cell" style="height: 26px">
                            Rubros Presupuestales.
                        </td>
                    </tr>
            <tr class="rowAG">
                <td colspan="2"> 
                    <asp:GridView runat="server" ID="gvRegistroInformacionPresupuestalRubros" DataKeyNames="CodigoRuproPresupuestal,ValorDetalle" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" CellPadding="0"
                        Height="16px"  AllowSorting="True">
                        <Columns>
                            <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DescripcionDependenciaAfectacionGasto"/>
                            <asp:BoundField HeaderText="Tipo Fuente Financiamiento" DataField="DescripcionTipoFuenteFinanciamiento"/>
                            <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                            <asp:BoundField HeaderText="Recurso Presupuestal" DataField="DescripcionTipoRecursoPresupuestal" />
                            <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="DescripcionTipoSituacionFondos"/>
                            <asp:TemplateField HeaderText="Valor Rubro" SortExpression="ValorDetalle">
                                <ItemTemplate>
                                    <asp:Label Text='<%# string.Format("{0:C}", Convert.ToDecimal(Eval("ValorDetalle"))) %>' ID="lblValorDetalle" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(function () {
            $("#<%= txtValorTotalDesde.ClientID%>").blur(function () {
                if ($("#<%= txtValorTotalDesde.ClientID%>").val().length > 17) {
                    $("#<%= txtValorTotalDesde.ClientID%>").val($("#<%= txtValorTotalDesde.ClientID%>").val().substring(0, 17));
                    $("#<%= txtValorTotalDesde.ClientID%>").formatCurrency();
                } else {
                    $("#<%= txtValorTotalDesde.ClientID%>").formatCurrency();
                }
            }).focus(function () {
                $("#<%= txtValorTotalDesde.ClientID%>").val($("#<%= txtValorTotalDesde.ClientID%>").formatCurrency().asNumber());
            });

            $("#<%= txtValorTotalHasta.ClientID%>").blur(function () {
                if ($("#<%= txtValorTotalHasta.ClientID%>").val().length > 17) {
                    $("#<%= txtValorTotalHasta.ClientID%>").val($("#<%= txtValorTotalHasta.ClientID%>").val().substring(0, 17));
                    $("#<%= txtValorTotalHasta.ClientID%>").formatCurrency();
                } else {
                    $("#<%= txtValorTotalHasta.ClientID%>").formatCurrency();
                }
            }).focus(function () {
                $("#<%= txtValorTotalHasta.ClientID%>").val($("#<%= txtValorTotalHasta.ClientID%>").formatCurrency().asNumber());
            });
        });

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
</asp:Content>
