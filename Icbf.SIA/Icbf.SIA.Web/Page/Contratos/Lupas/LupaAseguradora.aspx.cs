using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;


/// <summary>
/// Página de consulta de terceros
/// </summary>
public partial class Page_Contratos_Contratos_LupaAseguradora : GeneralWeb
{
    #region variables
    string PageName = "Contratos/LupaAseguradora";
    OferenteService vOferenteService = new OferenteService();
    ProveedorService vProveedorService = new ProveedorService();
    ManejoControles ManejoControles = new ManejoControles();
    General_General_Master_Lupa toolBar;
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        //if (ValidateAccess(toolBar, PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
            //if (GetState(Page.Master, PageName)) { Buscar(); }
        }
        //  }


    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void gvTercero_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.Cells.Count > 8)
        {
            TipoPersona tipoper = new TipoPersona();
            tipoper = vOferenteService.ConsultarTipoPersona(Convert.ToInt32(ddlIdTipoPersona.SelectedValue));

            if (tipoper.CodigoTipoPersona == "001")
            {
                e.Row.Cells[3].Visible = false;

                e.Row.Cells[5].Visible = true;
                e.Row.Cells[6].Visible = true;
                e.Row.Cells[7].Visible = true;
                e.Row.Cells[8].Visible = true;
            }
            else if (tipoper.CodigoTipoPersona == "002")
            {
                e.Row.Cells[3].Visible = true;

                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
            }
            else
            {
                e.Row.Cells[3].Visible = true;

                e.Row.Cells[5].Visible = true;
                e.Row.Cells[6].Visible = true;
                e.Row.Cells[7].Visible = true;
                e.Row.Cells[8].Visible = true;
            }
        }
    }

    protected void gvTercero_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTercero.SelectedRow);
    }

    protected void gvTercero_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTercero.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvTercero_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void txtNumeroIdentificacion_OnTextChanged(object sender, EventArgs e)
    {
        revNumDocCeroNIT.ValidationGroup = "vgRevNIT";
        Validate("vgRevNIT");

        if (txtNumeroIdentificacion.Text == string.Empty)
        {
            txtDV.Text = string.Empty;
            return;
        }

        if (IsValid)
        {

            CalcularDiV();
            txtRazonSocial.Focus();
            revNumDocCeroNIT.ValidationGroup = "btnBuscar";

        }
        else
        {
            txtDV.Text = "";
            revNumDocCeroNIT.ValidationGroup = "btnGuardar";
        }
    }
    #endregion

    #region Métodos
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvTercero, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            gvTercero.PageSize = PageSize();
            gvTercero.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Aseguradora", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            string departamentos = string.Empty;
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvTercero.DataKeys[gvTercero.SelectedRow.RowIndex].Values["IdTercero"].ToString()) + "';";
            //returnValues += "pObj[" + (1) + "] = '" + HttpUtility.HtmlDecode(gvTercero.DataKeys[gvTercero.SelectedRow.RowIndex].Values["IdEntidad"].ToString()) + "';";

            for (int c = 3; c <= 4; c++)
            {
                if (gvTercero.Rows[gvTercero.SelectedIndex].Cells[c].Text != "&nbsp;")
                {

                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvTercero.Rows[gvTercero.SelectedIndex].Cells[c].Text) + "';";
                }
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            List<Icbf.Proveedor.Entity.EntidadProvOferente> vEntidadProvOferentes = vProveedorService.ConsultarSucursales_EntidadProvOferentes(Convert.ToInt32(gvTercero.DataKeys[gvTercero.SelectedRow.RowIndex].Value.ToString()),null,null);

            if (vEntidadProvOferentes.Count > 0)
            {
                foreach (var vEntidadProvOferente in vEntidadProvOferentes)
                {
                    departamentos += Convert.ToString(vEntidadProvOferente.IdDepartamento) + ';' + vEntidadProvOferente.NombreDepartamento + ';';
                }
                returnValues += "pObj[" + (5) + "] = '" + HttpUtility.HtmlDecode(departamentos) + "';";
            }
            else
                returnValues += "pObj[" + (5) + "] = '" + HttpUtility.HtmlDecode("-1") + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vIdDListaTipoDocumento = null;
            String vIdTipoPersona = null;
            String vRazonSocial = null;
            String vNumeroIdentificacion = null;

            if (ddlIdDListaTipoDocumento.SelectedValue != "-1")
            {
                vIdDListaTipoDocumento = Convert.ToString(ddlIdDListaTipoDocumento.SelectedValue);
            }
            if (ddlIdTipoPersona.SelectedValue != "-1")
            {
                vIdTipoPersona = Convert.ToString(ddlIdTipoPersona.SelectedValue);
            }
            if (txtRazonSocial.Text != "")
            {
                vRazonSocial = Convert.ToString(txtRazonSocial.Text);
            }
            if (txtNumeroIdentificacion.Text != "")
            {
                vNumeroIdentificacion = Convert.ToString(txtNumeroIdentificacion.Text);
            }

            List<Tercero> myGridResults = vOferenteService.ConsultarTerceroFuente(vIdTipoPersona, vIdDListaTipoDocumento, vNumeroIdentificacion, vRazonSocial);
            
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Tercero), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Tercero, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarIdDListaTipoDocumento();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarIdDListaTipoDocumento()
    {
        ManejoControlesContratos ManejoControlesContrato = new ManejoControlesContratos();
        ManejoControlesContrato.LlenarTipoPersonaParameter(ddlIdTipoPersona, "-1", false);
        ManejoControles.LlenarTipoDocumentoN(ddlIdDListaTipoDocumento, "-1", false);
        limpiar();
    }

    private void limpiar()
    {
        txtRazonSocial.Text = string.Empty;
    }

    private void CalcularDiV()
    {
        int lenTxtNumeroDoc = 15 - txtNumeroIdentificacion.Text.Length;

        if (lenTxtNumeroDoc >= 0)
        {
            var strDato = Left("000000000000000", lenTxtNumeroDoc) + txtNumeroIdentificacion.Text;
            char[] strArray = (strDato.ToCharArray());

            int result = Convert.ToInt32(Convert.ToString(strArray[14])) * 3 +
                         Convert.ToInt32(Convert.ToString(strArray[13])) * 7 +
                         Convert.ToInt32(Convert.ToString(strArray[12])) * 13 +
                         Convert.ToInt32(Convert.ToString(strArray[11])) * 17 +
                         Convert.ToInt32(Convert.ToString(strArray[10])) * 19 +
                         Convert.ToInt32(Convert.ToString(strArray[9])) * 23 +
                         Convert.ToInt32(Convert.ToString(strArray[8])) * 29 +
                         Convert.ToInt32(Convert.ToString(strArray[7])) * 37 +
                         Convert.ToInt32(Convert.ToString(strArray[6])) * 41 +
                         Convert.ToInt32(Convert.ToString(strArray[5])) * 43 +
                         Convert.ToInt32(Convert.ToString(strArray[4])) * 47 +
                         Convert.ToInt32(Convert.ToString(strArray[3])) * 53 +
                         Convert.ToInt32(Convert.ToString(strArray[2])) * 59 +
                         Convert.ToInt32(Convert.ToString(strArray[1])) * 67 +
                         Convert.ToInt32(Convert.ToString(strArray[0])) * 71;

            int residuo = result % 11;

            if (residuo == 0)
            {
                txtDV.Text = "0";
            }
            else
            {
                if (residuo == 1)
                {
                    txtDV.Text = "1";
                }
                else
                {
                    residuo = 11 - residuo;
                    txtDV.Text = Convert.ToString(residuo);
                }
            }
        } //Fin if (lenTxtNumeroDoc >= 0)
        else
        {
            txtDV.Text = "";
        } // Fin else if (lenTxtNumeroDoc >= 0)
    }

    private static string Left(string param, int length)
    {
        string result = param.Substring(0, length);
        return result;
    }

    private static string Right(string param, int length)
    {

        int value = param.Length - length;
        string result = param.Substring(value, length);
        return result;
    }
    #endregion
}
