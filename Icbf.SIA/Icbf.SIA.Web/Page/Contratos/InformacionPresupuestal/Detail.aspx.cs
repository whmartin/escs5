using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de información presupuestal
/// </summary>
public partial class Page_InformacionPresupuestal_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/InformacionPresupuestal";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("InformacionPresupuestal.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("InformacionPresupuestal.IdInformacionPresupuestal", hfIdInformacionPresupuestal.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdInformacionPresupuestal = Convert.ToInt32(GetSessionParameter("InformacionPresupuestal.IdInformacionPresupuestal"));
            RemoveSessionParameter("InformacionPresupuestal.IdInformacionPresupuestal");

            if (GetSessionParameter("InformacionPresupuestal.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("InformacionPresupuestal.Guardado");


            InformacionPresupuestal vInformacionPresupuestal = new InformacionPresupuestal();
            vInformacionPresupuestal = vContratoService.ConsultarInformacionPresupuestal(vIdInformacionPresupuestal);
            hfIdInformacionPresupuestal.Value = vInformacionPresupuestal.IdInformacionPresupuestal.ToString();
            txtNumeroCDP.Text = vInformacionPresupuestal.NumeroCDP.ToString();
            txtValorCDP.Text = vInformacionPresupuestal.ValorCDP.ToString();
            txtFechaExpedicionCDP.Text = vInformacionPresupuestal.FechaExpedicionCDP.ToString("dd/MM/yyyy");
            ObtenerAuditoria(PageName, hfIdInformacionPresupuestal.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInformacionPresupuestal.UsuarioCrea, vInformacionPresupuestal.FechaCrea, vInformacionPresupuestal.UsuarioModifica, vInformacionPresupuestal.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdInformacionPresupuestal = Convert.ToInt32(hfIdInformacionPresupuestal.Value);

            InformacionPresupuestal vInformacionPresupuestal = new InformacionPresupuestal();
            vInformacionPresupuestal = vContratoService.ConsultarInformacionPresupuestal(vIdInformacionPresupuestal);
            InformacionAudioria(vInformacionPresupuestal, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarInformacionPresupuestal(vInformacionPresupuestal);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("InformacionPresupuestal.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Informaci&#243;n CDP", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
