<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_InformacionPresupuestal_Add" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
<asp:HiddenField ID="hfIdInformacionPresupuestal" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                N&#250;mero del CDP *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroCDP" ControlToValidate="txtNumeroCDP"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Valor del CDP *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorCDP" ControlToValidate="txtValorCDP"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50">
                <asp:TextBox runat="server" ID="txtNumeroCDP" MaxLength="50" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroCDP" runat="server" TargetControlID="txtNumeroCDP"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td width="50">
                <asp:TextBox runat="server" ID="txtValorCDP" MaxLength="18" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorCDP" runat="server" TargetControlID="txtValorCDP"
                    FilterType="Numbers, Custom" ValidChars="0123456789" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Fecha expedici&#243;n CDP *
            </td>
        </tr>
        <tr class="rowA">
            <td >
                <uc1:fecha ID="txtFechaExpedicionCDP"  runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
