<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_InformacionPresupuestal_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdInformacionPresupuestal" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&#250;mero del CDP
            </td>
            <td>
                Valor del CDP
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroCDP"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorCDP"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Fecha expedici&#243;n CDP
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtFechaExpedicionCDP"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
