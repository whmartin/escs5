<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_InformacionPresupuestal_List" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&#250;mero del CDP 
            </td>
            <td>
                Fecha expedici&#243;n CDP 
            </td>
        </tr>
        <tr class="rowA">
            <td valign="bottom">
                <asp:TextBox runat="server" ID="txtNumeroCDP" MaxLength="50" ></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftNumeroCDP" runat="server" TargetControlID="txtNumeroCDP"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td valign="bottom">
                <uc1:fecha runat="server" ID="txtFechaExpedicionCDP" ></uc1:fecha>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvInformacionPresupuestal" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdInformacionPresupuestal" CellPadding="0" Height="16px"
                        OnSorting="gvInformacionPresupuestal_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvInformacionPresupuestal_PageIndexChanging" OnSelectedIndexChanged="gvInformacionPresupuestal_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número del CDP" DataField="NumeroCDP"  SortExpression="NumeroCDP"/>
                            <asp:BoundField HeaderText="Fecha expedición CDP" 
                                DataField="FechaExpedicionCDP"  SortExpression="FechaExpedicionCDP" 
                                DataFormatString="{0:dd/MM/yyyy} "/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
