using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de información presupuestal
/// </summary>
public partial class Page_InformacionPresupuestal_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/InformacionPresupuestal";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            InformacionPresupuestal vInformacionPresupuestal = new InformacionPresupuestal();

            vInformacionPresupuestal.NumeroCDP = Convert.ToString(txtNumeroCDP.Text);
            vInformacionPresupuestal.ValorCDP = Convert.ToDecimal(txtValorCDP.Text);
            vInformacionPresupuestal.FechaExpedicionCDP = txtFechaExpedicionCDP.Date;

            if (Request.QueryString["oP"] == "E")
            {
                vInformacionPresupuestal.IdInformacionPresupuestal = Convert.ToInt32(hfIdInformacionPresupuestal.Value);
                vInformacionPresupuestal.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vInformacionPresupuestal, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarInformacionPresupuestal(vInformacionPresupuestal);
            }
            else
            {
                vInformacionPresupuestal.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vInformacionPresupuestal, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarInformacionPresupuestal(vInformacionPresupuestal);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("InformacionPresupuestal.IdInformacionPresupuestal", vInformacionPresupuestal.IdInformacionPresupuestal);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("InformacionPresupuestal.Modificado", "1");
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                }
                else
                {
                    SetSessionParameter("InformacionPresupuestal.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
           
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Informaci&#243;n CDP", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos provenientes de la entidad a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdInformacionPresupuestal = Convert.ToInt32(GetSessionParameter("InformacionPresupuestal.IdInformacionPresupuestal"));
            RemoveSessionParameter("InformacionPresupuestal.Id");

            InformacionPresupuestal vInformacionPresupuestal = new InformacionPresupuestal();
            vInformacionPresupuestal = vContratoService.ConsultarInformacionPresupuestal(vIdInformacionPresupuestal);
            hfIdInformacionPresupuestal.Value = vInformacionPresupuestal.IdInformacionPresupuestal.ToString();
            txtNumeroCDP.Text = vInformacionPresupuestal.NumeroCDP.ToString();
            txtValorCDP.Text = vInformacionPresupuestal.ValorCDP.ToString();
            txtFechaExpedicionCDP.Date = vInformacionPresupuestal.FechaExpedicionCDP;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vInformacionPresupuestal.UsuarioCrea, vInformacionPresupuestal.FechaCrea, vInformacionPresupuestal.UsuarioModifica, vInformacionPresupuestal.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            txtNumeroCDP.Text = "";
            txtFechaExpedicionCDP.HabilitarObligatoriedad(true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
