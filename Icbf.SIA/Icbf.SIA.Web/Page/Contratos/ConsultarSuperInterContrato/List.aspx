<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConsultarSuperInterContrato_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Supervisor y/o Interventor
            </td>
            <td class="Cell">
                Número del Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDSupervisorInterventor"  AutoPostBack="True"
                    onselectedindexchanged="ddlIDSupervisorInterventor_SelectedIndexChanged"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Nombre Supervisor y/o Interventor
            </td>
            <td class="Cell">
                Razón social Supervisor y/o Interventor
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
              <asp:TextBox runat="server" ID="txtNombreSuperInterv" MaxLength="50" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreSuperInterv" runat="server" TargetControlID="txtNombreSuperInterv"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtRazonSocial" MaxLength="50" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoIdentificacion" runat="server" TargetControlID="txtRazonSocial"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB" id="datosupervisor" runat="server">
             <td class="Cell">
                Nombre Director Interventoría
            </td>
            <td class="Cell">
                Número Identificación Director Interventoría
            </td>
        </tr>
        <tr class="rowA" id="datosupervisorText" runat="server">
             <td class="Cell">
                 <asp:TextBox runat="server" ID="txtNombreDirInterventoria" MaxLength="100" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreDirInterventoria" runat="server" TargetControlID="txtNombreDirInterventoria"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumIdentifDirInterventoria" MaxLength="16" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumIdentifDirInterventoria" runat="server" TargetControlID="txtNumIdentifDirInterventoria"
                    FilterType="Numbers" />
            </td>
        </tr>
        <tr class="rowB">
           <td class="Cell">
                <asp:Label runat="server" ID="LblIdentificacionsp" Text="Número Identificación Supervisor y/o Interventor"></asp:Label>
            </td>
            <td class="Cell">
                
            </td>
        </tr>
        <tr class="rowA" >
           <td class="Cell">
                  <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="16" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroIdentificacion" runat="server" TargetControlID="txtNumeroIdentificacion"
                    FilterType="Numbers" />

               
            </td>
            <td class="Cell">
               
            </td>
        </tr>
        
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConsultarSuperInterContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDSupervisorIntervContrato" CellPadding="0" Height="16px"
                        OnSorting="gvConsultarSuperInterContrato_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvConsultarSuperInterContrato_PageIndexChanging" OnSelectedIndexChanged="gvConsultarSuperInterContrato_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Supervisor y/o Interventor" DataField="SupervisorInterventor"  SortExpression="SupervisorInterventor"/>
                            <asp:BoundField HeaderText="Número del contrato/convenio" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Tipo del contrato/convenio" DataField="TipoContrato"  SortExpression="TipoContrato"/>
                            <asp:BoundField HeaderText="Regional del contrato/convenio" DataField="RegionalContrato"  SortExpression="RegionalContrato"/>
                            <asp:BoundField HeaderText="Fecha de Suscripción" DataField="FechaSuscripcion"  SortExpression="FechaSuscripcion" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Fecha de Terminación" DataField="FechaTerminacion"  SortExpression="FechaTerminacion" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Fecha Inicio Supervisión y/o Interventoria" DataField="FechaInicioSuperInterv"  SortExpression="FechaInicioSuperInterv" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Fecha Finalización Supervisión y/o Interventoria" DataField="FechaFinalSuperInterv"  SortExpression="FechaFinalSuperInterv" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="fecha Modificación supervisión y/o Interventoria" DataField="FechaModSuperInterv"  SortExpression="FechaModSuperInterv" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion"  SortExpression="TipoIdentificacion"/>
                            <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion"  SortExpression="NumeroIdentificacion"/>
                            <asp:BoundField HeaderText="Nombre y/o Razón Social" DataField="NombreRazonSocialSuperInterv"  SortExpression="NombreRazonSocialSuperInterv"/>
                            <asp:BoundField HeaderText="Número Contrato Interventoria" DataField="NumeroContratoInterventoria"  SortExpression="NumeroContratoInterventoria"/>
                            
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
