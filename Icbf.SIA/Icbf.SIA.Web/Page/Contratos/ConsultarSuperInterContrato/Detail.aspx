<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ConsultarSuperInterContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIDSupervisorIntervContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Supervisor y/o Interventor
            </td>
            <td>
                Número del Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtSupervisorInterventor" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo del Contrato/Convenio
            </td>
            <td>
                Regional del Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoContrato" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegionalContrato" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha De Suscripción
            </td>
            <td>
                Fecha De Terminación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaSuscripcion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaTerminacion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto del Contrato
            </td>
            <td>
                Estado Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" TextMode="MultiLine" Rows="4" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEstadoContrato" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicio Supervisión y/o Interventoria
            </td>
            <td>
                Fecha Finalización Supervisión y/o Interventoria
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicioSuperInterv" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinalSuperInterv" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Modificación Supervisor y/o Interventor
            </td>
            <td>
                Tipo Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaModSuperInterv" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTipoIdentificacion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número Identificación
            </td>
            <td>
                Nombre y/o razón social
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocial" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td>
                 <asp:Label runat="server" ID="lblestadoSupervisorInterventor" Text="Estado Supervisor/Interventor" Visible="True"></asp:Label>  
            </td>
            <td>
                 <asp:Label runat="server" ID="NumeroContratoInterventoria" Text="Número Contrato interventoría" Visible="True"></asp:Label>  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblListEstadoSuperInterv"  Enabled="false" Visible="True"></asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContratoInterventoria" Width="85%" Enabled="false" Visible="True"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               <asp:Label runat="server" ID="Numident" Text="Número Identificación" Visible="True"></asp:Label>   
               <br />
               <asp:Label runat="server" ID="DirectorInter" Text=" Director de la Interventoría" Visible="True"></asp:Label>
               
            </td>
            <td>
                 
                
                 <asp:Label runat="server" ID="lblnombredir" Text="Nombre Director de  la" Visible="True"></asp:Label>   
                   <br />
                   <asp:Label runat="server" ID="lblinterventoria" Text="interventoría" Visible="True"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumIdentifDirInterventoria" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreDirInterventoria" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               <asp:Label runat="server" ID="lbltelefono" Text="Teléfono  Director de la Interventoría"></asp:Label> 
            </td>
            <td>
                <asp:Label runat="server" ID="lblCelular" Text="Celular  Director de la Interventoría"></asp:Label> 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTelefonoInterventoria" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTelCelularInterventoria" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        
    </table>
</asp:Content>
