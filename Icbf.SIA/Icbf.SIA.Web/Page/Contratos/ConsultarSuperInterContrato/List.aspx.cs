using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de consulta a través de filtros para la entidad ConsultarSuperInterContrato
/// </summary>
public partial class Page_ConsultarSuperInterContrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ConsultarSuperInterContrato";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
               // if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

        /// <summary>
        /// Método que realiza la búsqueda filtrada con múltiples criterios 
        /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvConsultarSuperInterContrato, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoLimpiar += new ToolBarDelegate(btnLimpiar_Click);
            gvConsultarSuperInterContrato.PageSize = PageSize();
            gvConsultarSuperInterContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Supervisor/Interventor contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        ddlIDSupervisorInterventor.SelectedIndex = 0;
        txtNumeroContrato.Text = string.Empty;
        txtNombreSuperInterv.Text = string.Empty;
        txtRazonSocial.Text = string.Empty;
        txtNombreDirInterventoria.Text = string.Empty;
        txtNumIdentifDirInterventoria.Text = string.Empty;
        LblIdentificacionsp.Text = "Número Identificación Supervisor y/o Interventor";
        txtNumeroIdentificacion.Text = string.Empty;
    }

        /// <summary>
        /// Método para redirigir a la página detalle del registro seleccionado 
        /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvConsultarSuperInterContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ConsultarSuperInterContrato.IDSupervisorIntervContrato", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvConsultarSuperInterContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConsultarSuperInterContrato.SelectedRow);
    }
    protected void gvConsultarSuperInterContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConsultarSuperInterContrato.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvConsultarSuperInterContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vCodSupervisorInterventor = null;
            String vNumeroContrato = null;
            String vNumeroIdentificacion = null;
            String vRazonSocial = null;
            
            String vNombreSuperInterv = null;

            String vNumIdentifDirInterventoria = null;
            String vNombreDirInterventoria = null;
            
            
            if (ddlIDSupervisorInterventor.SelectedValue!= "-1")
            {
                vCodSupervisorInterventor = Convert.ToString(ddlIDSupervisorInterventor.SelectedValue);
            }
            if (txtNumeroContrato.Text!= "")
            {
                vNumeroContrato = Convert.ToString(txtNumeroContrato.Text);
            }
            if (txtNumeroIdentificacion.Text!= "")
            {
                vNumeroIdentificacion = Convert.ToString(txtNumeroIdentificacion.Text);
            }
            
            if (txtRazonSocial.Text!= "")
            {
                vRazonSocial = Convert.ToString(txtRazonSocial.Text);
            }
            if (txtNombreSuperInterv.Text!= "")
            {
                vNombreSuperInterv = Convert.ToString(txtNombreSuperInterv.Text);
            }

            if (txtNumIdentifDirInterventoria.Text!= "")
            {
                vNumIdentifDirInterventoria = Convert.ToString(txtNumIdentifDirInterventoria.Text);
            }
            if (txtNombreDirInterventoria.Text!= "")
            {
                vNombreDirInterventoria = Convert.ToString(txtNombreDirInterventoria.Text);
            }

            if (vCodSupervisorInterventor == null)
                throw new Exception("Seleccione el tipo de Supervisor / Interventor");

            if (vCodSupervisorInterventor == null && vNumeroContrato == null && vNumeroIdentificacion == null && vRazonSocial == null && vNombreSuperInterv == null && vNumIdentifDirInterventoria == null && vNombreDirInterventoria == null)
                throw new Exception("Se debe ingresar un paràmetro para realizar la consulta");

            var myGridResults = vContratoService.ConsultarConsultarSuperInterContratos(vCodSupervisorInterventor, vNumeroContrato, vNumeroIdentificacion, vRazonSocial, vNombreSuperInterv, vNumIdentifDirInterventoria, vNombreDirInterventoria);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(ConsultarSuperInterContrato), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<ConsultarSuperInterContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList(); 
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();

            toolBar.LipiarMensajeError();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("ConsultarSuperInterContrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeError("Registro Eliminado Con Exito");
            RemoveSessionParameter("ConsultarSuperInterContrato.Eliminado");
            CargarIDSupervisorInterventor();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar los datos de IDSupervisorInterventor
    /// </summary>
    private void CargarIDSupervisorInterventor()
    {
        var Vresultado = vContratoService.ConsultarSupervisorInterventors(null, null);
        ManejoControlesContratos.LlenarComboLista(ddlIDSupervisorInterventor, Vresultado, "Codigo", "Descripcion");
        
    }

    protected void ddlIDSupervisorInterventor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIDSupervisorInterventor.SelectedValue == "01")
        {
            //datosupervisor.Visible = false;
            //datosupervisorText.Visible = false;
            txtNombreDirInterventoria.Enabled = false;
            txtNumIdentifDirInterventoria.Enabled = false;
        }
        else
        {
            //datosupervisor.Visible = true;
            //datosupervisorText.Visible = true;
            txtNombreDirInterventoria.Enabled = true;
            txtNumIdentifDirInterventoria.Enabled = true;
        }
    }
}
