using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad ConsultarSuperInterContrato
/// </summary>
public partial class Page_ConsultarSuperInterContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ConsultarSuperInterContrato";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ConsultarSuperInterContrato.IDSupervisorIntervContrato", hfIDSupervisorIntervContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        // Condición para redireccionar a la pagina Lupa info Contratista o a Lupa proveedores
        bool LLamadoPaginaInfoSupervisorInter = false;
        if (GetSessionParameter("SupervisorInterventor.Retornar") != null)
        {
            if (bool.TryParse(GetSessionParameter("SupervisorInterventor.Retornar").ToString(), out LLamadoPaginaInfoSupervisorInter))
            {
                if (LLamadoPaginaInfoSupervisorInter)
                {
                    RemoveSessionParameter("SupervisorInterventor.Retornar");
                    string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                    string returnValues =
                            "<script language='javascript'> " +
                            "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

                    this.ClientScript.RegisterStartupScript(this.GetType(), "CerrarVentanaSupInt", returnValues);
                    return;
                }
                
            }
        }

        NavigateTo(SolutionPage.List);

    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIDSupervisorIntervContrato = Convert.ToInt32(GetSessionParameter("ConsultarSuperInterContrato.IDSupervisorIntervContrato"));
            RemoveSessionParameter("ConsultarSuperInterContrato.IDSupervisorIntervContrato");

            if (GetSessionParameter("ConsultarSuperInterContrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ConsultarSuperInterContrato.Guardado");


            ConsultarSuperInterContrato vConsultarSuperInterContrato = new ConsultarSuperInterContrato();
            vConsultarSuperInterContrato = vContratoService.ConsultarConsultarSuperInterContrato(vIDSupervisorIntervContrato);
            hfIDSupervisorIntervContrato.Value = vConsultarSuperInterContrato.IDSupervisorIntervContrato.ToString();
            txtSupervisorInterventor.Text = vConsultarSuperInterContrato.SupervisorInterventor.ToString();
            txtNumeroContrato.Text = vConsultarSuperInterContrato.NumeroContrato;
            txtNumeroIdentificacion.Text = vConsultarSuperInterContrato.NumeroIdentificacion;
            txtTipoIdentificacion.Text = vConsultarSuperInterContrato.TipoIdentificacion;
            txtNombreRazonSocial.Text = vConsultarSuperInterContrato.NombreRazonSocialSuperInterv;

            txtNumIdentifDirInterventoria.Text = vConsultarSuperInterContrato.NumIdentifDirInterventoria;
            txtNombreDirInterventoria.Text = vConsultarSuperInterContrato.NombreDirInterventoria;
            txtTelefonoInterventoria.Text = vConsultarSuperInterContrato.TelefonoInterventoria;
            txtTelCelularInterventoria.Text = vConsultarSuperInterContrato.TelCelularInterventoria;
            
            txtTipoContrato.Text = vConsultarSuperInterContrato.TipoContrato;
            txtRegionalContrato.Text = vConsultarSuperInterContrato.RegionalContrato;
            txtObjetoContrato.Text = vConsultarSuperInterContrato.ObjetoContrato;
            rblListEstadoSuperInterv.SelectedValue = vConsultarSuperInterContrato.Inactivo.ToString().Trim().ToLower();
            txtEstadoContrato.Text = vConsultarSuperInterContrato.EstadoContrato;
            txtFechaSuscripcion.Text = string.Format("{0:dd/MM/yyyy}",vConsultarSuperInterContrato.FechaSuscripcion);
            txtFechaTerminacion.Text = string.Format("{0:dd/MM/yyyy}", vConsultarSuperInterContrato.FechaTerminacion);
            txtFechaInicioSuperInterv.Text = string.Format("{0:dd/MM/yyyy}",vConsultarSuperInterContrato.FechaInicioSuperInterv);
            txtFechaFinalSuperInterv.Text = string.Format("{0:dd/MM/yyyy}",vConsultarSuperInterContrato.FechaFinalSuperInterv);
            txtFechaModSuperInterv.Text = string.Format("{0:dd/MM/yyyy}",vConsultarSuperInterContrato.FechaModSuperInterv);
            txtNumeroContratoInterventoria.Text = vConsultarSuperInterContrato.NumeroContratoInterventoria;
            txtTelefonoInterventoria.Text = vConsultarSuperInterContrato.TelefonoInterventoria;
            txtTelCelularInterventoria.Text = vConsultarSuperInterContrato.TelCelularInterventoria;

            if (vConsultarSuperInterContrato.CodTipoSuperInter.Equals("01"))
                VisualizarInformacionInterventor(false);
            
            else if (vConsultarSuperInterContrato.CodTipoSuperInter.Equals("02"))
                VisualizarInformacionInterventor(true);


            bool LLamadoPaginaInfoSupervisorInter = false;
            if (GetSessionParameter("SupervisorInterventor.Retornar") != null)
            {
                if (bool.TryParse(GetSessionParameter("SupervisorInterventor.Retornar").ToString(), out LLamadoPaginaInfoSupervisorInter))
                {
                    if (LLamadoPaginaInfoSupervisorInter)
                    {
                        toolBar.OcultarBotonBuscar(true);
                    }
                    else
                    {
                        toolBar.OcultarBotonBuscar(false);
                    }

                }
                else
                {
                    toolBar.OcultarBotonBuscar(false);
                }
            }
            

            ObtenerAuditoria(PageName, hfIDSupervisorIntervContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vConsultarSuperInterContrato.UsuarioCrea, vConsultarSuperInterContrato.FechaCrea, vConsultarSuperInterContrato.UsuarioModifica, vConsultarSuperInterContrato.FechaModifica == null ? DateTime.Now : Convert.ToDateTime(vConsultarSuperInterContrato.FechaModifica));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIDSupervisorIntervContrato = Convert.ToInt32(hfIDSupervisorIntervContrato.Value);

            ConsultarSuperInterContrato vConsultarSuperInterContrato = new ConsultarSuperInterContrato();
            vConsultarSuperInterContrato = vContratoService.ConsultarConsultarSuperInterContrato(vIDSupervisorIntervContrato);
            InformacionAudioria(vConsultarSuperInterContrato, this.PageName, vSolutionPage);
            int vResultado = 0;// vContratoService.EliminarConsultarSuperInterContrato(vConsultarSuperInterContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ConsultarSuperInterContrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            //toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Supervisor/Interventor Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblListEstadoSuperInterv.Items.Insert(0, new ListItem("Activo", "true"));
            rblListEstadoSuperInterv.Items.Insert(1, new ListItem("Inactivo", "false"));
            rblListEstadoSuperInterv.SelectedValue = "true";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void VisualizarInformacionInterventor(bool dato)
    {
        NumeroContratoInterventoria.Visible = dato;
        txtNumeroContratoInterventoria.Visible = dato;

        Numident.Visible = dato;
        DirectorInter.Visible = dato;
        txtNumIdentifDirInterventoria.Visible = dato;

        lblnombredir.Visible = dato;
        lblinterventoria.Visible = dato;
        txtNombreDirInterventoria.Visible = dato;

        lbltelefono.Visible = dato;
        lblCelular.Visible = dato;
        txtTelefonoInterventoria.Visible = dato;
        txtTelCelularInterventoria.Visible = dato;
    }
}
