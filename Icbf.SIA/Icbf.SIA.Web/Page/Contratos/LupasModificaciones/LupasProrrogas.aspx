﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasProrrogas.aspx.cs" Inherits="Page_Contratos_LupasModificaciones_LupasProrrogas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField id="HfIdContrato" runat="server" />
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }
    </script>
    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Prorrogas</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvProrrogas" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="IdProrroga"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvProrrogas_Sorting_Sorting"
                            OnPageIndexChanging="gvProrrogas_Sorting_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Fecha de Inicio" DataField="FechaInicioView" SortExpression="FechaInicio" />
                                <asp:BoundField HeaderText="Fecha de Fin" DataField="FechaFinView" SortExpression="FechaFin" />
                                <asp:BoundField HeaderText="Años" DataField="Anios" SortExpression="Anios" />
                                <asp:BoundField HeaderText="Meses" DataField="Meses" SortExpression="Meses" />
                                <asp:BoundField HeaderText="D&iacute;as" DataField="dias" SortExpression="Dias" />

                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
