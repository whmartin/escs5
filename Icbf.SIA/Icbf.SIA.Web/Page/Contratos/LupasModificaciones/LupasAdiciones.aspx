﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasAdiciones.aspx.cs" Inherits="Page_Contratos_LupasModificaciones_LupasAdiciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="HfIdContrato" runat="server" />
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }
    </script>
    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Adiciones</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvAdiciones" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="IdAdicion"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvAdiciones_Sorting"
                            OnPageIndexChanging="gvAdiciones_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Fecha de Adici&oacute;n" DataField="FechaAdicionView" SortExpression="FechaAdicion" />
                                <asp:BoundField HeaderText="Valor Adici&oacute;n" DataField="ValorAdicion" SortExpression="ValorAdicion" DataFormatString="{0:C}" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
