﻿using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using WsContratosPacco;

public partial class Page_Contratos_LupasModificaciones_LupaHistoricoModificaciones : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa _toolBar;
    readonly ContratoService _vContratoService = new ContratoService();
    public string PageName = "Contratos/Contratos";

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;

        if (ValidateAccess(_toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                var idContrato = Request.QueryString["IdContrato"];

                if (!string.IsNullOrEmpty(idContrato))
                {
                    gvModificaciones.EmptyDataText = EmptyDataText();
                    gvModificaciones.PageSize = PageSize();
                    HfIdContrato.Value = idContrato;
                    var itemsAdiciones = _vContratoService.ConsultarModificacionesHistorico(int.Parse(idContrato));
                    gvModificaciones.DataSource = itemsAdiciones;
                    gvModificaciones.DataBind();
                }
            }
        }
    }

    protected void gvModificaciones_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaConsolidado((GridView)sender, e.SortExpression, false);
    }

    protected void gvModificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvModificaciones.PageIndex = e.NewPageIndex;
        CargarGrillaConsolidado((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rvDetallePlan", returnValues);
    }

    #endregion

    #region Funciones y Procedmientos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)Master;
            if (_toolBar != null)
            {
                _toolBar.eventoRetornar += btnRetornar_Click;
                _toolBar.EstablecerTitulos("Historico de Modificaciones", SolutionPage.Add.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrillaConsolidado(gvModificaciones, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaConsolidado(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        try
        {
            int idContrato = int.Parse(HfIdContrato.Value);

            var myGridResults = _vContratoService.ConsultarModificacionesHistorico(idContrato);

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                {
                    var param = Expression.Parameter(typeof(Func<HistoricoSolicitudesContrato, object>), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<HistoricoSolicitudesContrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}