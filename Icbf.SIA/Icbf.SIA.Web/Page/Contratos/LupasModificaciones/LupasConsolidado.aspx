﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasConsolidado.aspx.cs" Inherits="Page_Contratos_LupasModificaciones_LupasConsolidado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="HfIdContrato" runat="server" />
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }
    </script>
    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Consolidado de Modificaciones</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvAdiciones" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="key"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvAdiciones_Sorting"
                            OnPageIndexChanging="gvAdiciones_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Modificaci&oacute;n" DataField="key" SortExpression="key" />
                                <asp:BoundField HeaderText="Cantidad" DataField="value" SortExpression="value" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
