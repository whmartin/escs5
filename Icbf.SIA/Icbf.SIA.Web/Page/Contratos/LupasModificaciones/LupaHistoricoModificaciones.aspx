﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaHistoricoModificaciones.aspx.cs" Inherits="Page_Contratos_LupasModificaciones_LupaHistoricoModificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="HfIdContrato" runat="server" />
    <script language="javascript" type="text/javascript">
       
    </script>
    <div align="center">
        <asp:Panel runat="server" ID="pnlModificaciones">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Historico de Modificaciones</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvModificaciones" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="IDContrato"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvModificaciones_Sorting"
                            OnPageIndexChanging="gvModificaciones_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Tipo de Modificaci&oacute;n" DataField="TipoModificacion" SortExpression="TipoModificacion" />
                                <asp:BoundField HeaderText="Numero Solicitud" DataField="NumeroModificacion" SortExpression="NumeroModificacion" />
                                <asp:BoundField HeaderText="Estado" DataField="EstadoModificacion" SortExpression="EstadoModificacion" />                                
                                <asp:BoundField HeaderText="Fecha de Solicitud" DataField="FechaSolicitud"  SortExpression="FechaSolicitud" dataformatstring="{0:d}"/>
                                <asp:BoundField HeaderText="Fecha de Suscripci&oacute;n" DataField="FechaSuscripcion"  SortExpression="FechaSuscripcion" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Fecha de Modificaci&oacute;n" DataField="FechaModificacion"  SortExpression="FechaModificacion" dataformatstring="{0:d}"/>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
