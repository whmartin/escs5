﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasProcesoImpMultas.aspx.cs" Inherits="Page_Contratos_LupasModificaciones_LupasProcesoImpMultas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="HfIdProcesoImplMulta" runat="server" />
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }
    </script>
    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Histórico de Audicencias</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvHistoricoProcesoImpMultas" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="IdHistoricoProcesoImpMultas"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvHistoricoProcesoImpMultas_Sorting"
                            OnPageIndexChanging="gvHistoricoProcesoImpMultas_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Fecha" DataField="Fecha" DataFormatString="{0:dd/MM/yyyy}" SortExpression="Fecha" />
                                <asp:BoundField HeaderText="Motivo" DataField="Motivo" SortExpression="Motivo"  />
                                <asp:BoundField HeaderText="Usuario Creación" DataField="UsuarioCrea" SortExpression="UsuarioCrea" />
                                <asp:BoundField HeaderText="Fecha Creación" DataFormatString="{0:dd/MM/yyyy}" DataField="FechaCrea" SortExpression="FechaCrea"  />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
