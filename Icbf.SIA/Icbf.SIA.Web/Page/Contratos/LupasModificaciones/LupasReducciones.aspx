﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasReducciones.aspx.cs" Inherits="Page_Contratos_LupasModificaciones_LupasReducciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField id="HfIdContrato" runat="server" />
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }
    </script>
    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Reducciones</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView runat="server" ID="gvReducciones" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="IdReduccion"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvReducciones_Sorting_Sorting"
                            OnPageIndexChanging="gvReducciones_Sorting_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Fecha de Reducci&oacute;n" DataField="FechaReduccionView" SortExpression="FechaReduccion" />
                                <asp:BoundField HeaderText="Valor de Reducci&oacute;n" DataField="ValorReduccion" SortExpression="ValorReduccion" DataFormatString="{0:C}" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
