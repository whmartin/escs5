﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;


public partial class Page_Contratos_SupervisorInterContrato_RelacionarSupervisorInterventor : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    ManejoControles vManejoControles = new ManejoControles();
    string PageName = "Contratos/SupervisorInterContrato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        if (HFIdContratoInterventor.Value != string.Empty)
        {
            txtNumeroContratoIntermediario.Text = HFIdContratoInterventor.Value;
        }
        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();

            CargarRegistro();
        }
        //}
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void ValidacionesPrincipal(int IdContrato)
    {
        DateTime? fechaejecucionContrato = vContratoService.ConsultarContrato(IdContrato).FechaInicioEjecucion;

        if (fechaejecucionContrato == null)
        {
            throw new Exception("El Contrato aun no cuenta con una fecha de ejecucion");
        }
        else
        {
            if (txtFechaInicio.Date < fechaejecucionContrato)
            {
                throw new Exception("La fecha de Inicio debe ser mayor  o igual a la Fecha de Ejecución  del Contrato/Convenio"); 
            }
        }
        DateTime? FechaFinalizacionIniciaContratoContrato = vContratoService.ConsultarContrato(IdContrato).FechaFinalizacionIniciaContrato;

        if (FechaFinalizacionIniciaContratoContrato == null)
        {
            throw new Exception("El Contrato aun no cuenta con una fecha de Finalizacion");
        }
        else
        {
            if (txtFechaInicio.Date > FechaFinalizacionIniciaContratoContrato)
            {
                throw new Exception("La Fecha de inicio debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio");
            }
        }

        

    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad SupervisorInterContrato
    /// </summary>
    private void Guardar()
    {
        try
        {
            //if (!Page.IsValid)
            //    return;// HfTipoIdentifica
            SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
            int vResultado = 0;

            if (hfEsModificacion.Value == "1")
            {
                vSupervisorInterContrato.IdContrato = int.Parse(hfIdContrato.Value);
                vSupervisorInterContrato.FechaInicio = txtFechaInicio.Date;
                vSupervisorInterContrato.IDSupervisorAnterior = int.Parse(hfIDSupervisorIntervContrato.Value);
                vSupervisorInterContrato.Inactivo = !Convert.ToBoolean(chkEstado.SelectedValue);
                vSupervisorInterContrato.Identificacion = Convert.ToString(txtNumeroIdentificacion.Text);
                if (HfTipoIdentifica.Value != string.Empty)
                vSupervisorInterContrato.TipoIdentificacion = Convert.ToString(HfTipoIdentifica.Value);
                vSupervisorInterContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                vSupervisorInterContrato.IDTipoSuperInter = Convert.ToInt32(ddlSuperInter.SelectedValue);
                if (!string.IsNullOrEmpty(hfIdRol.Value))
                    vSupervisorInterContrato.IdRol = Convert.ToInt32(hfIdRol.Value);
                
                if (hfIdEmpleado.Value != string.Empty)
                    vSupervisorInterContrato.IDEmpleadosSupervisor = Convert.ToInt32(hfIdEmpleado.Value);

                vResultado = vContratoService.InsertarSupervisorTemporal(vSupervisorInterContrato);

                if (vResultado == 0)
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                else if (vResultado == 1)
                {
                    SetSessionParameter("SupervisorInterContrato.IDSupervisorIntervContrato", vSupervisorInterContrato.IDSupervisorIntervContrato);
                    SetSessionParameter("SupervisorInterContrato.Guardado", "1");
                    NavigateTo("DetalleSupervisor.aspx?esModificacion=1");
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            else
            {
                string codtipoSuperInter = vContratoService.ConsultarSupervisorInterventorsID(Convert.ToInt32(ddlSuperInter.SelectedValue)).Codigo;

                if (GetSessionParameter("Contrato.ContratosAPP") != null)
                {
                    vSupervisorInterContrato.IdContrato = int.Parse(hfIdContrato.Value); //Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
                }
                else
                {
                    throw new Exception("Faltan datos para completar la operacion");
                }

                ValidacionesPrincipal(Convert.ToInt32(vSupervisorInterContrato.IdContrato));


                vSupervisorInterContrato.FechaInicio = txtFechaInicio.Date;


                vSupervisorInterContrato.Inactivo = !Convert.ToBoolean(chkEstado.SelectedValue); //!chkEstado.Checked;
                vSupervisorInterContrato.Identificacion = Convert.ToString(txtNumeroIdentificacion.Text);
                if (HfTipoIdentifica.Value != string.Empty)
                    vSupervisorInterContrato.TipoIdentificacion = Convert.ToString(HfTipoIdentifica.Value); //Convert.ToString(txtTipoIdentificacion.Text);
                vSupervisorInterContrato.IDTipoSuperInter = Convert.ToInt32(ddlSuperInter.SelectedValue); //Convert.ToInt32(chkTipoSuperInter.SelectedValue);
                vSupervisorInterContrato.IdNumeroContratoInterventoria = null;
                if (hfIdProveedor.Value != string.Empty)
                    vSupervisorInterContrato.IDProveedoresInterventor = Convert.ToInt32(hfIdProveedor.Value);
                if (hfIdEmpleado.Value != string.Empty)
                    vSupervisorInterContrato.IDEmpleadosSupervisor = Convert.ToInt32(hfIdEmpleado.Value);
                if (HFIdContratoInterventor.Value != string.Empty)
                {
                    vSupervisorInterContrato.IdNumeroContratoInterventoria = Convert.ToInt32(HFIdContratoInterventor.Value);
                }
                if (!string.IsNullOrEmpty(hfIdRol.Value))
                    vSupervisorInterContrato.IdRol = Convert.ToInt32(hfIdRol.Value);

                //if (hfIdContrato.Value != string.Empty)
                //    vSupervisorInterContrato.IdContrato = Convert.ToInt32(hfIdContrato.Value);
                //vSupervisorInterContrato.IdContrato = null;

                if (Request.QueryString["oP"] == "E")
                {
                    vSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(hfIDSupervisorIntervContrato.Value);
                    vSupervisorInterContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisorInterContrato, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarSupervisorInterContrato(vSupervisorInterContrato);
                }
                else
                {
                    vSupervisorInterContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisorInterContrato, this.PageName, vSolutionPage);

                    //Interventoria
                    if (codtipoSuperInter == "02")
                    {
                        DirectorInterventoria vDirectorInterventoria = new DirectorInterventoria();
                        vDirectorInterventoria.IdTipoIdentificacion = Convert.ToInt32(ddlTipoIdentificacionDirectorInter.SelectedValue);
                        vDirectorInterventoria.NumeroIdentificacion = txtNumeroIdentificacionDirectorInter.Text;
                        vDirectorInterventoria.PrimerNombre = txtPrimerNombreDirectorInter.Text;
                        vDirectorInterventoria.SegundoNombre = txtSegundoNombreDirectorInter.Text;
                        vDirectorInterventoria.PrimerApellido = txtPrimerApellidoDirectorInter.Text;
                        vDirectorInterventoria.SegundoApellido = txtSegundoApellidoDirectorInter.Text;
                        vDirectorInterventoria.Celular = txtCelularDirectorInter.Text;
                        vDirectorInterventoria.Telefono = txtTelefonoDirectorInter.Text;
                        vDirectorInterventoria.CorreoElectronico = txtCorreoElectronicoDirectorInter.Text;
                        vDirectorInterventoria.UsuarioCrea = GetSessionUser().NombreUsuario;
                        if (HFIdContratoInterventor.Value != string.Empty)
                        {
                            vDirectorInterventoria.IDContratoInterventoria = Convert.ToInt32(HFIdContratoInterventor.Value);
                        }
                        else
                        {
                            throw new Exception("El contrato seleccionado debe corresponder a una interventoría");
                        }

                        InformacionAudioria(vDirectorInterventoria, this.PageName, vSolutionPage);
                        vResultado = vContratoService.InsertarSupervisorInterContrato(vSupervisorInterContrato, vDirectorInterventoria);
                    }
                    else
                    {
                        vResultado = vContratoService.InsertarSupervisorInterContrato(vSupervisorInterContrato);
                    }
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    SetSessionParameter("SupervisorInterContrato.IDSupervisorIntervContrato", vSupervisorInterContrato.IDSupervisorIntervContrato);
                    SetSessionParameter("SupervisorInterContrato.Guardado", "1");
                    if (codtipoSuperInter == "01")//Supervisor
                    {
                        NavigateTo("DetalleSupervisor.aspx");
                    }
                    else if (codtipoSuperInter == "02")//Interventor
                    {
                        NavigateTo("DetalleInterventor.aspx");
                    }

                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.EstablecerTitulos("Relacionar Supervisor/Interventor Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
             hfIdContrato.Value = null;            

            if (Request.QueryString["esModificacion"] != null && Request.QueryString["idContrato"] != null)
             {
                 hfIdContrato.Value = Request.QueryString["idContrato"].ToString();
                 hfEsModificacion.Value = Request.QueryString["esModificacion"].ToString();
                 hfIDSupervisorIntervContrato.Value = Request.QueryString["idSupervisor"].ToString();
                 hfFechaInicioSupervisorAnterior.Value = Request.QueryString["fechaInicio"].ToString();
             }
            else if(! string.IsNullOrEmpty(Request.QueryString["idContrato"]))
                hfIdContrato.Value = Request.QueryString["idContrato"].ToString();
            else if (GetSessionParameter("Contrato.ContratosAPP") != null && Convert.ToString(GetSessionParameter("Contrato.ContratosAPP")) != "")
                 hfIdContrato.Value = GetSessionParameter("Contrato.ContratosAPP").ToString();  

            if (hfIdContrato.Value != null)
            {
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                                
                DateTime? fechaejecucionContrato = vContratoService.ConsultarContrato(vIdContrato).FechaInicioEjecucion;

                //if (! string.IsNullOrEmpty(hfFechaInicioSupervisorAnterior.Value))
                //{
                //    DateTime vFechaInicionSupervisorAnterior = Convert.ToDateTime(hfFechaInicioSupervisorAnterior.Value);
                //    txtFechaInicio.Date = vFechaInicionSupervisorAnterior.AddDays(1);
                //}
                //else 
                if (fechaejecucionContrato.HasValue && string.IsNullOrEmpty(hfFechaInicioSupervisorAnterior.Value))
                    txtFechaInicio.Date = fechaejecucionContrato.Value;

                List<Contrato> vlistContrato = vContratoService.ConsultarContratosLupa(vIdContrato, null);
                if (vlistContrato.Count > 0)
                {
                    hfIdContrato.Value = vlistContrato.First<Contrato>().IdContrato.ToString();
                    if (vlistContrato.First<Contrato>().FechaInicioEjecucion != null)
                    {
                        DateTime vFechaInicioEjecucion = Convert.ToDateTime(vlistContrato.First<Contrato>().FechaInicioEjecucion);
                        hfFechaContratoInicial.Value = vFechaInicioEjecucion.ToString("dd/MM/yyyy HH:mm:ss");
                    }

                    if (vlistContrato.First<Contrato>().FechaFinalTerminacionContrato != null)
                    {
                        DateTime vFechaTerminacion = Convert.ToDateTime(vlistContrato.First<Contrato>().FechaFinalTerminacionContrato);
                        hfFechaContratoFinal.Value = vFechaTerminacion.ToString("dd/MM/yyyy HH:mm:ss"); ;
                    }
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            InhabilitarControlesIniciales();
            CargarSupervisorInterventor();
            CargarTipoSupervisorInterventor();
            CargarTipoPersona();
            InicializaTipoIdentificacion();
            chkEstado.Items.Insert(0, new ListItem("Activo", "true"));
            chkEstado.Items.Insert(1, new ListItem("Inactivo", "false"));
            chkEstado.SelectedValue = "true";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarSupervisorInterventor()
    {
        var vResultado = vContratoService.ConsultarSupervisorInterventors(null, null);
        ManejoControlesContratos.LlenarComboLista(ddlSuperInter, vResultado, "IDSupervisorInterventor", "Descripcion");

        if ( ! string.IsNullOrEmpty(Request.QueryString["esModificacion"]))
        {
            ddlSuperInter.SelectedValue = "1";
            ddlSuperInter.Enabled = false;

            chkTipoSuperInter.SelectedValue = "1"; 
            txtFechaInicio.Enabled = false;
            txtTipoPersona.Text = string.Empty;
            imgSupervisor.Visible = true;
            imgInterventor.Visible = false;
            LimpiarCamposDetalle();
            toolBar.QuitarMensajeError();
            pnlExterno.Visible = false;
            cvFechaInicioInicial.Enabled = true;
            cvFechaInicioFinal.Enabled = true;
            rfvTipoPersona.Enabled = true;
            //txtFechaInicio.Requerid = true;

            rfvTipoIdentificacionDirectorInter.Enabled = false;
            rfvNumeroIdentificacionInter.Enabled = false;
            rfvPrimerNombreDirectorInter.Enabled = false;
            rfvPrimerApellidoDirectorInter.Enabled = false;
            rfvTelefonoDirectorInter.Enabled = false;
            rfvCelularDirectorInter.Enabled = false;
            rfvCorreoElectronicoDirectorInter.Enabled = false;
        }
    }

    private void CargarTipoSupervisorInterventor()
    {
        var VResultado = vContratoService.ConsultarTipoSuperInters(null, null).Where(x => x.Estado.Equals(true));
        ManejoControlesContratos.LlenarCheckBoxLista(chkTipoSuperInter, VResultado, "IDTipoSuperInter", "Descripcion");
    }

    private void CargarTipoPersona()
    {
        //ManejoControles vManejoControles = new ManejoControles();
        //vManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
    }

    private void CargarTipoIdentificacion()
    {
        //ManejoControles vManejoControles = new ManejoControles();
        //if (ddlTipoPersona.SelectedValue == "1") //NATURAL
        //{
        //    vManejoControles.LlenarTipoDocumentoRL(ddlTipoIdentificacion, "-1", true);
        //}
        //else if (ddlTipoPersona.SelectedValue == "2") //JURIDICA
        //{
        //    vManejoControles.LlenarTipoDocumentoN(ddlTipoIdentificacion, "-1", true);
        //}
        //else 
        //{
        //    ddlTipoIdentificacion.Items.Clear();
        //    ddlTipoIdentificacion.Items.Add(new ListItem() { Value="-1", Text="Seleccione" });
        //}
    }

    private void InicializaTipoIdentificacion()
    {
        //ddlTipoIdentificacion.Items.Clear();
        //ddlTipoIdentificacion.Items.Add(new ListItem() { Value = "-1", Text = "Seleccione" });
    }

    private void InhabilitarControlesIniciales()
    {
        chkTipoSuperInter.Enabled = false;
        txtFechaInicio.Enabled = false;
        txtTipoPersona.Enabled = false;
    }

    protected void ddlSuperInter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSuperInter.SelectedValue == "1")//Supervisor
        {
            chkTipoSuperInter.SelectedValue = "1"; //Interno
            txtFechaInicio.Enabled = false;
            //txtTipoPersona.Text = "Seleccionar Supervisor";
            txtTipoPersona.Text = string.Empty;
            imgSupervisor.Visible = true;
            imgInterventor.Visible = false;
            //imgIdContrato.Attributes.Add("OnClick", "GetEmpleado();");
            LimpiarCamposDetalle();
            toolBar.QuitarMensajeError();
            pnlExterno.Visible = false;
            cvFechaInicioInicial.Enabled = true;
            cvFechaInicioFinal.Enabled = true;
            rfvTipoPersona.Enabled = true;
            txtFechaInicio.Requerid = true;

            rfvTipoIdentificacionDirectorInter.Enabled = false;
            rfvNumeroIdentificacionInter.Enabled = false;
            rfvPrimerNombreDirectorInter.Enabled = false;
            rfvPrimerApellidoDirectorInter.Enabled = false;
            rfvTelefonoDirectorInter.Enabled = false;
            rfvCelularDirectorInter.Enabled = false;
            rfvCorreoElectronicoDirectorInter.Enabled = false;
        }
        else if (ddlSuperInter.SelectedValue == "2")//Interventor
        {
            chkTipoSuperInter.SelectedValue = "2";//Externo
            txtFechaInicio.Enabled = false;
            //txtTipoPersona.Text = "Seleccionar Interventor";
            txtTipoPersona.Text = string.Empty;
            imgInterventor.Visible = true;
            imgSupervisor.Visible = false;
            //imgIdContrato.Attributes.Add("OnClick", "GetProveedor();");
            LimpiarCamposDetalle();
            toolBar.QuitarMensajeError();
            CargarDatosDirectorInterventoria();
            pnlExterno.Visible = true;
            cvFechaInicioInicial.Enabled = true;
            cvFechaInicioFinal.Enabled = true;
            rfvTipoPersona.Enabled = true;
            txtFechaInicio.Requerid = true;

            rfvTipoIdentificacionDirectorInter.Enabled = true;
            rfvNumeroIdentificacionInter.Enabled = true;
            rfvPrimerNombreDirectorInter.Enabled = true;
            rfvPrimerApellidoDirectorInter.Enabled = true;
            rfvTelefonoDirectorInter.Enabled = true;
            rfvCelularDirectorInter.Enabled = true;
            rfvCorreoElectronicoDirectorInter.Enabled = true;
        }
        else
        {
            pnlExterno.Visible = false;
            for (int item = 0; item < chkTipoSuperInter.Items.Count; item++)
            {
                chkTipoSuperInter.Items[item].Selected = false;
            }
            txtFechaInicio.Enabled = false;
            LimpiarCamposDetalle();
            txtTipoPersona.Text = "";
            txtTipoPersona.Enabled = false;
            imgSupervisor.Visible = false;
            imgInterventor.Visible = false;
            //imgIdContrato.Attributes.Add("OnClick", "");
            InHabilitaControles();
            toolBar.QuitarMensajeError();
            cvFechaInicioInicial.Enabled = false;
            cvFechaInicioFinal.Enabled = false;
            rfvTipoPersona.Enabled = false;
            txtFechaInicio.Requerid = false;

            rfvTipoIdentificacionDirectorInter.Enabled = false;
            rfvNumeroIdentificacionInter.Enabled = false;
            rfvPrimerNombreDirectorInter.Enabled = false;
            rfvPrimerApellidoDirectorInter.Enabled = false;
            rfvTelefonoDirectorInter.Enabled = false;
            rfvCelularDirectorInter.Enabled = false;
            rfvCorreoElectronicoDirectorInter.Enabled = false;
        }
    }

    private void CargarDatosDirectorInterventoria()
    {
        ddlTipoIdentificacionDirectorInter.Items.Clear();
        vManejoControles.LlenarTipoDocumento(ddlTipoIdentificacionDirectorInter, "-1", true);
    }

    private void LimpiarCamposDetalle()
    {

        txtTipoIdentificacion.Text = string.Empty;
        txtNumeroIdentificacion.Text = string.Empty;
        txtRazonSocial.Text = string.Empty;
        txtTipoVinculacionContractual.Text = string.Empty;
        txtPrimerNombre.Text = string.Empty;
        txtSegundoNombre.Text = string.Empty;
        txtPrimerApellido.Text = string.Empty;
        txtSegundoApellido.Text = string.Empty;
        txtRegional.Text = string.Empty;
        txtDependencia.Text = string.Empty;
        txtCargo.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtCorreoElectronico.Text = string.Empty;
        txtCorreoInterventor.Text = string.Empty;
        hfIdEmpleado.Value = string.Empty;
        hfIdProveedor.Value = string.Empty;
    }

    private void HabilitaControlesSupervisor()
    {
        lblRazonSocial.Visible = false;
        txtRazonSocial.Visible = false;
        lblTipoVnculacion.Visible = true;
        txtTipoVinculacionContractual.Visible = true;
        PnlDatos1.Visible = true;
        PnlDatos2.Visible = true;
        PnlDatos3.Visible = false;
    }

    private void HabilitaControlesInterventor(String TipoPersona)
    {
        if (TipoPersona.Equals("NATURAL"))
        {
            lblRazonSocial.Visible = false;
            txtRazonSocial.Visible = false;
            lblTipoVnculacion.Visible = false;
            txtTipoVinculacionContractual.Visible = false;
            PnlDatos1.Visible = true;
            PnlDatos2.Visible = false;
        }
        else
        {
            lblRazonSocial.Visible = true;
            lblTipoVnculacion.Visible = true;
            txtRazonSocial.Visible = true;
            txtTipoVinculacionContractual.Visible = true;
            PnlDatos1.Visible = false;
            PnlDatos2.Visible = false;
        }

        PnlDatos3.Visible = true;
    }

    private void InHabilitaControles()
    {
        txtTipoIdentificacion.Enabled = false;
        txtNumeroIdentificacion.Enabled = false;
        txtTipoVinculacionContractual.Enabled = false;
        txtPrimerNombre.Enabled = false;
        txtSegundoNombre.Enabled = false;
        txtPrimerApellido.Enabled = false;
        txtSegundoApellido.Enabled = false;
        txtRegional.Enabled = false;
        txtDependencia.Enabled = false;
        txtCargo.Enabled = false;
        txtDireccion.Enabled = false;
        txtTelefono.Enabled = false;
        txtCorreoElectronico.Enabled = false;
        txtCorreoInterventor.Enabled = false;
        txtRazonSocial.Enabled = false;
    }

    protected void cvTipoSupervisorInterventor_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        for (int item = 0; item < chkTipoSuperInter.Items.Count; item++)
        {
            if (chkTipoSuperInter.Items[item].Selected == true)
            {
                args.IsValid = true;
                return;
            }
        }

    }

    protected void cvFechaInicioInicial_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            args.IsValid = false;
            if (hfFechaContratoInicial.Value != string.Empty)
            {
                DateTime vFechaInicioEjecucion = Convert.ToDateTime(hfFechaContratoInicial.Value);
                if (txtFechaInicio.Date >= vFechaInicioEjecucion)
                {
                    if (hfEsModificacion.Value == "1")
                    {
                        DateTime vFechaInicionSupervisorAnterior = Convert.ToDateTime(hfFechaInicioSupervisorAnterior.Value);

                        if (txtFechaInicio.Date > vFechaInicionSupervisorAnterior)
                            args.IsValid = true;
                        else
                            ((CustomValidator)source).ErrorMessage = "La fecha de inicio debe ser mayor a la fecha de Inicio del anterior supervisor";
                    }
                    else
                        args.IsValid = true;
                }
                else
                {
                    ((CustomValidator)source).ErrorMessage = "La fecha de inicio debe ser mayor o igual a la fecha de ejecuciòn del Contrato";

                }
            }
            else
            {

                //((CustomValidator)source).ErrorMessage = "El contrato no tiene fecha de inicio de ejecuciòn";
                throw new Exception("El contrato no tiene fecha de inicio de ejecuciòn");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void cvFechaInicioFinal_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            args.IsValid = false;
            if (hfFechaContratoFinal.Value != string.Empty)
            {
                DateTime vFechaFinalTerminacion = Convert.ToDateTime(hfFechaContratoFinal.Value);
                if (txtFechaInicio.Date <= vFechaFinalTerminacion)
                {
                    args.IsValid = true;
                }
                else
                {
                    ((CustomValidator)source).ErrorMessage = "La fecha de inicio debe ser menor o igual a la fecha Final de terminaciòn Contrato/Convenio";

                }
            }
            else
            {
                //((CustomValidator)source).ErrorMessage = "El contrato no tiene fecha Final de terminaciòn";
                throw new Exception("El contrato no tiene fecha Final de terminaciòn");
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void btnVisualizarDatos_Click(object sender, EventArgs e)
    {
        String TipoPersona = Convert.ToString(hfTipoPersona.Value);
        AsignarInformacionControles();

        if (ddlSuperInter.SelectedValue == "2") //Interventor
        {
            txtTipoPersona.Text = "Interventor";
            HabilitaControlesInterventor(TipoPersona);
        }

        if (ddlSuperInter.SelectedValue == "1") //Supervisor
        {
            txtTipoPersona.Text = "Supervisor";
            HabilitaControlesSupervisor();
        }

        rfvnumerocontrato.Enabled = true;
        ImageContrato.Visible = true;
    }

    private void AsignarInformacionControles()
    {
        txtTipoIdentificacion.Text = Convert.ToString( hfTipoIdentificacion.Value);
        txtNumeroIdentificacion.Text = Convert.ToString( hfNumeroIdentificacion.Value);
        txtRazonSocial.Text = Convert.ToString( hfRazonSocial.Value);
        txtTipoVinculacionContractual.Text = Convert.ToString( hfTipoVinculacionContractual.Value);
        txtPrimerNombre.Text = Convert.ToString( hfPrimerNombre.Value);
        txtSegundoNombre.Text = Convert.ToString( hfSegundoNombre.Value);
        txtPrimerApellido.Text = Convert.ToString( hfPrimerApellido.Value);
        txtSegundoApellido.Text = Convert.ToString( hfSegundoApellido.Value);
        txtRegional.Text = Convert.ToString( hfRegional.Value);
        txtDependencia.Text = Convert.ToString( hfDependencia.Value);
        txtCargo.Text = Convert.ToString( hfCargo.Value);
        txtDireccion.Text = Convert.ToString( hfDireccion.Value);
        txtTelefono.Text = Convert.ToString( hfTelefono.Value);
        txtCorreoElectronico.Text = Convert.ToString(hfCorreoElectronico.Value);
        txtCorreoInterventor.Text = Convert.ToString(hfCorreoElectronico.Value);
    }

    protected void btnCompararContratos_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (GetSessionParameter("Contrato.ContratosAPP") != null && Convert.ToString(GetSessionParameter("Contrato.ContratosAPP")) != "")
        {
            int idsessionContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
            if (Convert.ToInt32(HFIdContratoInterventor.Value).Equals(idsessionContrato))
            {
                toolBar.MostrarMensajeError("El contrato del interventor no puede ser el mismo contrato");
                return;
            }
            else
            {
                int vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "EJE", null, true)[0].IdEstadoContrato;
                int? IdEstadoContrato = vContratoService.ConsultarContrato(Convert.ToInt32(HFIdContratoInterventor.Value)).IdEstadoContrato;
                if (IdEstadoContrato != vEstadoContrato)
                {
                    toolBar.MostrarMensajeError("El contrato seleccionado de interventoría debe estar en ejecución");
                    return; 
                }

            }
        }
        else
        {
            toolBar.MostrarMensajeError("No se tiene el número del contrato a comparar");
        }

       
    }
}