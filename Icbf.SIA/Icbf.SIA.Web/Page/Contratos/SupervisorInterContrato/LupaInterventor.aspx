﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LupaInterventor.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master" Inherits="Page_Contratos_SupervisorInterContrato_LupaInterventor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlConsulta" >

<table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo de Persona
                
            </td>
            <td>
                Tipo Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoPersona" AutoPostBack="True" OnTextChanged="ddlTipoPersona_OnTextChanged" >
                </asp:DropDownList>
                
                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoIdentificacion">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Identificación
                
            </td>
            <td>
                Nombre y/o Razón Social Interventor
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroDocumento" MaxLength="11" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocial" MaxLength="50" ></asp:TextBox>
                
            </td>
        </tr>
    </table>

</asp:Panel>
<asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvInterventor" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdInterventor" CellPadding="0" Height="16px"
                                OnSorting="gvInterventor_Sorting" AllowSorting="True" 
                                OnPageIndexChanging="gvInterventor_PageIndexChanging" OnSelectedIndexChanged="gvInterventor_SelectedIndexChanged">
                                <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo persona" DataField="TipoPersona" SortExpression="TipoPersona"  />
                            <asp:BoundField HeaderText="Tipo identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número Identificación" DataField="NumeroDocumento" SortExpression="NumeroDocumento"  />
                            
                            <asp:TemplateField HeaderText="Nombre y/o Razón Social Interventor" ItemStyle-HorizontalAlign="Center" SortExpression="NombreRazonSocial">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreRazonSocial")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfPrimerNombre" Value='<%#Eval("PrimerNombre")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfSegundoNombre" Value='<%#Eval("SegundoNombre")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfPrimerApellido" Value='<%#Eval("PrimerApellido")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfSegundoApellido" Value='<%#Eval("SegundoApellido")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfRazonSocial" Value='<%#Eval("RazonSocial")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfCorreoElectronico" Value='<%#Eval("CorreoElectronico")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfDireccion" Value='<%#Eval("Direccion")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfTelefono" Value='<%#Eval("Telefono")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
</asp:Panel>
</asp:Content>
