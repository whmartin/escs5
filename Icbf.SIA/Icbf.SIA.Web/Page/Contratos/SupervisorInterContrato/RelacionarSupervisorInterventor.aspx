﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RelacionarSupervisorInterventor.aspx.cs"
    MasterPageFile="~/General/General/Master/Lupa.master" Inherits="Page_Contratos_SupervisorInterContrato_RelacionarSupervisorInterventor" %>

<%@ Register Src="../../../General/General/Control/fechaJS.ascx" TagName="fechaJS"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDSupervisorIntervContrato" runat="server" />
    <asp:HiddenField runat="server" ID="hfIdContrato" />
    <asp:HiddenField runat="server" ID="hfFechaContratoInicial" />
    <asp:HiddenField runat="server" ID="hfFechaContratoFinal" />
    <asp:HiddenField runat="server" ID="HFIdContratoInterventor" />
    <asp:HiddenField runat="server" ID="hfEsModificacion" />
    <asp:HiddenField runat="server" ID="hfFechaInicioSupervisorAnterior" />

    <table width="90%" align="center">
        <tr>
            <td colspan="2">
                <asp:Panel ID="PnlIden" runat="server" Visible="True">
                    <table width="100%" align="center">
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Supervisor y/o Interventor *
                                <asp:CompareValidator runat="server" ID="cvSupervisorInterventor" ControlToValidate="ddlSuperInter"
                                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                            </td>
                            <td>
                                Tipo Supervisor y/o interventor
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:DropDownList runat="server" ID="ddlSuperInter" AutoPostBack="True" OnSelectedIndexChanged="ddlSuperInter_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBoxList runat="server" ID="chkTipoSuperInter" AutoPostBack="true">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Fecha de inicio *
                                <asp:CustomValidator runat="server" ID="cvFechaInicioInicial" SetFocusOnError="true"
                                    ErrorMessage="" Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"
                                    OnServerValidate="cvFechaInicioInicial_ServerValidate" Enabled="False"></asp:CustomValidator>
                                <br />
                                <asp:CustomValidator runat="server" ID="cvFechaInicioFinal" SetFocusOnError="true"
                                    ErrorMessage="" Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red"
                                    OnServerValidate="cvFechaInicioFinal_ServerValidate" Enabled="False"></asp:CustomValidator>
                            </td>
                            <td>
                                Tipo de Persona *
                                <asp:RequiredFieldValidator runat="server" ID="rfvTipoPersona" ControlToValidate="txtTipoPersona"
                                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <uc1:fechaJS ID="txtFechaInicio" Requerid="False" runat="server"/>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTipoPersona" Width="85%" Enabled="false"></asp:TextBox>
                                <asp:Image ID="imgInterventor" runat="server" Visible="False" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                    OnClick="GetProveedor()" Style="cursor: hand" ToolTip="Buscar" />
                                <asp:Image ID="imgSupervisor" runat="server" Visible="False" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                    OnClick="GetEmpleado()" Style="cursor: hand" ToolTip="Buscar" />
                                <asp:HiddenField runat="server" ID="hfIdProveedor" />
                                <asp:HiddenField runat="server" ID="hfIdEmpleado" />
                                <asp:HiddenField runat="server" ID="HfTipoIdentifica" />
                                <asp:HiddenField runat="server" ID="hfTipoPersona" />
                                <asp:Button runat="server" ID="btnValidarDatosVisualizar" Style="display: none" OnClick="btnVisualizarDatos_Click" />
                                <asp:HiddenField runat="server" ID="hfTipoIdentificacion" />
                                <asp:HiddenField runat="server" ID="hfNumeroIdentificacion" />
                                <asp:HiddenField runat="server" ID="hfRazonSocial" />
                                <asp:HiddenField runat="server" ID="hfTipoVinculacionContractual" />
                                <asp:HiddenField runat="server" ID="hfPrimerNombre" />
                                <asp:HiddenField runat="server" ID="hfSegundoNombre" />
                                <asp:HiddenField runat="server" ID="hfPrimerApellido" />
                                <asp:HiddenField runat="server" ID="hfSegundoApellido" />
                                <asp:HiddenField runat="server" ID="hfRegional" />
                                <asp:HiddenField runat="server" ID="hfDependencia" />
                                <asp:HiddenField runat="server" ID="hfCargo" />
                                <asp:HiddenField runat="server" ID="hfDireccion" />
                                <asp:HiddenField runat="server" ID="hfTelefono" />
                                <asp:HiddenField runat="server" ID="hfCorreoElectronico" />
                                <asp:HiddenField runat="server" ID="hfIdRol" />
                                <asp:HiddenField runat="server" ID="hfNombreRol" />
                            </td>
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Tipo de Identificación
                            </td>
                            <td>
                                Número de Identificación
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtTipoIdentificacion" Width="85%" Enabled="False"
                                    MaxLength="16"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="16" Width="85%"
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                <asp:Label runat="server" ID="lblRazonSocial" Text="Razón Social" Visible="False"></asp:Label>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblTipoVnculacion" Text="Tipo de Vinculación Contractual"
                                    Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtRazonSocial" MaxLength="50" Width="85%" Enabled="false"
                                    Visible="False"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTipoVinculacionContractual" MaxLength="50" Width="85%"
                                    Enabled="false" Visible="False"></asp:TextBox>
                            </td>
                        </tr>                      
                        
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="PnlDatos1" runat="server" Visible="False">
                    <table width="100%" align="center">
                        <tr class="rowB" style="width: 50%">
                            <td colspan="2">
                                <asp:Label runat="server" ID="lblRol" Text="Rol Supervisor *" Visible="true"></asp:Label>

                                <asp:RequiredFieldValidator runat="server" ID="rfvRoles" ControlToValidate="txtRol"
                                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Enabled="true"></asp:RequiredFieldValidator>

                            </td>                                                      
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td colspan="2">
                                 
                                            <asp:TextBox ID="txtRol" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                 AutoPostBack="true" Width="70%"></asp:TextBox>
                                                                                                            <asp:ImageButton ID="ImgRol" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="true" OnClientClick="GetRol(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />

                            </td> 
                            <td>

                            </td>                                                   
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Primer Nombre
                            </td>
                            <td>
                                Segundo Nombre
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtPrimerNombre" MaxLength="50" Width="85%" Enabled="False"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoNombre" MaxLength="50" Width="85%" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Primer Apellido
                            </td>
                            <td>
                                Segundo Apellido
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtPrimerApellido" MaxLength="50" Width="85%" Enabled="False"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSegundoApellido" MaxLength="50" Width="85%" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="PnlDatos2" runat="server" Visible="False">
                    <table width="100%" align="center">
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Regional
                            </td>
                            <td>
                                Dependencia
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtRegional" Width="85%" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDependencia" MaxLength="50" Width="85%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Cargo
                            </td>
                            <td>
                                Correo Electrónico
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtCargo" MaxLength="50" Width="85%" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtCorreoElectronico" MaxLength="50" Width="85%"
                                    Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="PnlDatos3" runat="server" Visible="False">
                    <table width="100%" align="center">
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Dirección
                            </td>
                            <td>
                                Teléfono
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtDireccion" MaxLength="50" Width="85%" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtTelefono" MaxLength="10" Width="85%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="rowB" style="width: 50%">
                            <td>
                                Correo Electrónico
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="rowA" style="width: 50%">
                            <td>
                                <asp:TextBox runat="server" ID="txtCorreoInterventor" MaxLength="50" Width="85%"
                                    Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="chkEstado">
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlExterno" runat="server" Visible="false">
                    <fieldset>
                        <legend>Información Director de la Interventoría</legend>
                        <table width="100%" align="center">
                            <tr class="rowB">
                                <td>
                                    Número Contrato Interventoría *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvnumerocontrato" ControlToValidate="txtNumeroContratoIntermediario"
                                        SetFocusOnError="true" ErrorMessage="Seleccione un tipo de identificación" Display="Dynamic"
                                        ValidationGroup="btnGuardar" ForeColor="Red" InitialValue="" Enabled="False">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Tipo Identificación *
                                    <asp:RequiredFieldValidator runat="server" ID="rfvTipoIdentificacionDirectorInter"
                                        ControlToValidate="ddlTipoIdentificacionDirectorInter" SetFocusOnError="true"
                                        ErrorMessage="Seleccione un tipo de identificación" Display="Dynamic" ValidationGroup="btnGuardar"
                                        ForeColor="Red" InitialValue="-1">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td style="width: 50%">
                                    <asp:HiddenField ID="hdIDContratoInterventoria" ClientIDMode="Static" runat="server" />
                                    <asp:TextBox runat="server" ID="txtNumeroContratoIntermediario" ClientIDMode="Static"
                                        MaxLength="50" Enabled="False" Width="85%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteNumeroContratoIntermediario" runat="server"
                                        TargetControlID="txtNumeroContratoIntermediario" FilterType="Numbers" ValidChars="" />
                                    <asp:Image ID="ImageContrato" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                        OnClick="GetContratoInter()" Style="cursor: hand" ToolTip="Buscar" Visible="False" />
                                    <asp:Button runat="server" ID="btnCompararContratos" Style="display: none" OnClick="btnCompararContratos_Click" />
                                </td>
                                <td style="width: 50%">
                                    <asp:DropDownList runat="server" ID="ddlTipoIdentificacionDirectorInter" Width="85%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td style="width: 50%">
                                    Número Identificación *
                                    <asp:RequiredFieldValidator ID="rfvNumeroIdentificacionInter" runat="server" ControlToValidate="txtNumeroIdentificacionDirectorInter"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 50%">
                                    Primer Nombre *
                                    <asp:RequiredFieldValidator ID="rfvPrimerNombreDirectorInter" runat="server" ControlToValidate="txtPrimerNombreDirectorInter"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionDirectorInter" ClientIDMode="Static"
                                        MaxLength="16" Width="85%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fteNumeroIdentificacionDirectorInter" runat="server"
                                        TargetControlID="txtNumeroIdentificacionDirectorInter" FilterType="Numbers" ValidChars="" />
                                </td>
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtPrimerNombreDirectorInter" ClientIDMode="Static"
                                        MaxLength="50" Width="85%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td style="width: 50%">
                                    Segundo Nombre
                                </td>
                                <td style="width: 50%">
                                    Primer Apellido *
                                    <asp:RequiredFieldValidator ID="rfvPrimerApellidoDirectorInter" runat="server" ControlToValidate="txtPrimerApellidoDirectorInter"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtSegundoNombreDirectorInter" ClientIDMode="Static"
                                        MaxLength="50" Width="85%"></asp:TextBox>
                                </td>
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtPrimerApellidoDirectorInter" ClientIDMode="Static"
                                        MaxLength="50" Width="85%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td style="width: 50%">
                                    Segundo Apellido
                                </td>
                                <td style="width: 50%">
                                    Teléfono *
                                    <asp:RequiredFieldValidator ID="rfvTelefonoDirectorInter" runat="server" ControlToValidate="txtTelefonoDirectorInter"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtSegundoApellidoDirectorInter" ClientIDMode="Static"
                                        MaxLength="50" Width="85%"></asp:TextBox>
                                </td>
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtTelefonoDirectorInter" ClientIDMode="Static" MaxLength="10"
                                        Width="85%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fttxtTelefonoDirectorInter" runat="server" TargetControlID="txtTelefonoDirectorInter"
                                        FilterType="Numbers" />
                                </td>
                            </tr>
                            <tr class="rowB">
                                <td style="width: 50%">
                                    Celular *
                                    <asp:RequiredFieldValidator ID="rfvCelularDirectorInter" runat="server" ControlToValidate="txtCelularDirectorInter"
                                        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 50%">
                                    Correo Electrónico *
                                    <asp:RequiredFieldValidator ID="rfvCorreoElectronicoDirectorInter" runat="server"
                                        ControlToValidate="txtCorreoElectronicoDirectorInter" Display="Dynamic" ErrorMessage="RequiredFieldValidator"
                                        ForeColor="Red" ValidationGroup="btnGuardar">Campo Requerido</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revCorreoElectronicoDirectorInter" runat="server"
                                        ControlToValidate="txtCorreoElectronicoDirectorInter" Display="Dynamic" ErrorMessage="RegularExpressionValidator"
                                        ForeColor="Red" ValidationGroup="btnGuardar" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Correo Electrónico no válido</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr class="rowA">
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtCelularDirectorInter" ClientIDMode="Static" MaxLength="10"
                                        Width="85%"></asp:TextBox>
                                    <Ajax:FilteredTextBoxExtender ID="fttxtCelularDirectorInter" runat="server" TargetControlID="txtCelularDirectorInter"
                                        FilterType="Numbers" />
                                </td>
                                <td style="width: 50%">
                                    <asp:TextBox runat="server" ID="txtCorreoElectronicoDirectorInter" ClientIDMode="Static"
                                        MaxLength="50" Width="85%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function EjecutarJS() {
            return true;
        }
        function GetRol() {
            //muestraImagenLoading();

            window_showModalDialog('../../../Page/Contratos/Lupas/LupaRoles.aspx',setReturnGetRol, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetRol(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                
                if (retLupa.length > 1) {
                    $('#<%=hfIdRol.ClientID %>').val(retLupa[0]);
                    console.log($('<%=hfIdRol.ClientID %>').val());

                    $('#<%=hfNombreRol.ClientID %>').val(retLupa[1]);
                    console.log( $('<%=hfNombreRol.ClientID %>').val());

                    $('#<%=txtRol.ClientID %>').val(retLupa[1]);
                    console.log( $('<%=txtRol.ClientID %>').val());
                }
            }
        }

        function GetProveedor() {
            window_showModalDialog('../../../Page/Contratos/SupervisorInterContrato/LupaInterventor.aspx',setReturnGetProveedor, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetProveedor(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 12) {
                    $('#<%=hfIdProveedor.ClientID %>').val(retLupa[0]);
                    $('#<%=hfTipoPersona.ClientID %>').val(retLupa[1]);
                    $('#<%=hfTipoIdentificacion.ClientID %>').val(retLupa[2]);
                    $('#<%=hfNumeroIdentificacion.ClientID %>').val(retLupa[3]);
                    $('#<%=hfPrimerNombre.ClientID %>').val(retLupa[5]);
                    $('#<%=hfSegundoNombre.ClientID %>').val(retLupa[6]);
                    $('#<%=hfPrimerApellido.ClientID %>').val(retLupa[7]);
                    $('#<%=hfSegundoApellido.ClientID %>').val(retLupa[8]);
                    $('#<%=hfRazonSocial.ClientID %>').val(retLupa[9]);
                    $('#<%=hfCorreoElectronico.ClientID %>').val(retLupa[10]);
                    $('#<%=hfDireccion.ClientID %>').val(retLupa[11]);
                    $('#<%=hfTelefono.ClientID %>').val(retLupa[12]);
                    $("#<%=btnValidarDatosVisualizar.ClientID%>").click();
                }
            }
        }

        function GetEmpleado() {
            window_showModalDialog('../../../Page/Contratos/SupervisorInterContrato/LupaSupervisor.aspx', setReturnGetEmpleado, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetEmpleado(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 15) {
                    $('#<%=hfIdEmpleado.ClientID %>').val(retLupa[0]);
                    $('#<%=hfTipoPersona.ClientID %>').val('NATURAL');
                    $('#<%=hfTipoIdentificacion.ClientID %>').val(retLupa[1]);
                    $('#<%=hfNumeroIdentificacion.ClientID %>').val(retLupa[2]);
                    $('#<%=hfTipoVinculacionContractual.ClientID %>').val(retLupa[3]);
                    $('#<%=hfRegional.ClientID %>').val(retLupa[5]);
                    $('#<%=hfDependencia.ClientID %>').val(retLupa[6]);
                    $('#<%=hfCargo.ClientID %>').val(retLupa[7]);
                    $('#<%=hfPrimerNombre.ClientID %>').val(retLupa[8]);
                    $('#<%=hfSegundoNombre.ClientID %>').val(retLupa[9]);
                    $('#<%=hfPrimerApellido.ClientID %>').val(retLupa[10]);
                    $('#<%=hfSegundoApellido.ClientID %>').val(retLupa[11]);
                    $('#<%=hfDireccion.ClientID %>').val(retLupa[12]);
                    $('#<%=hfTelefono.ClientID %>').val(retLupa[13]);
                    $('#<%=hfCorreoElectronico.ClientID %>').val(retLupa[14]);
                    $('#<%=HfTipoIdentifica.ClientID %>').val(retLupa[15]);
                    $("#<%=btnValidarDatosVisualizar.ClientID%>").click();
                }
            }
        }

        function GetContratoInter() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratoNew.aspx', setReturnGetContratoInter, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContratoInter(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 0) {
                    $('#<%=txtNumeroContratoIntermediario.ClientID %>').val(retLupa[0]);
                    $('#<%=HFIdContratoInterventor.ClientID %>').val(retLupa[0]);
                    $("#<%=btnCompararContratos.ClientID%>").click();
                }
            }
        }
    </script>
</asp:Content>
