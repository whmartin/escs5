﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

public partial class Page_Contratos_SupervisorInterContrato_DetalleSupervisor : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarRegistro();
        }

    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
           
            toolBar.EstablecerTitulos("Supervisor");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    private void CargarRegistro()
    {
        try
        {
            int vIDSupervisorIntervContrato = Convert.ToInt32(GetSessionParameter("SupervisorInterContrato.IDSupervisorIntervContrato"));
            RemoveSessionParameter("SupervisorInterContrato.IDSupervisorIntervContrato");

            if (GetSessionParameter("SupervisorInterContrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisorInterContrato.Guardado");

            Supervisor vSupervisor = null;

            if(Request.QueryString["esModificacion"] == null)
             vSupervisor = vContratoService.ConsultarSupervisor(vIDSupervisorIntervContrato);
            else
             vSupervisor = vContratoService.ConsultarSupervisorTemporal(vIDSupervisorIntervContrato);

            txtTipoIdentificacion.Text = vSupervisor.TipoIdentificacion;
            txtNumeroIdentificacion.Text = vSupervisor.NumeroIdentificacion;
            txtTipoVinculacionContractual.Text = vSupervisor.TipoVinculacionContractual;
            txtPrimerNombre.Text = vSupervisor.PrimerNombre;
            txtSegundoNombre.Text = vSupervisor.SegundoNombre;
            txtPrimerApellido.Text = vSupervisor.PrimerApellido;
            txtSegundoApellido.Text = vSupervisor.SegundoApellido;
            txtRegional.Text = vSupervisor.Regional;
            txtDependencia.Text = vSupervisor.Cargo;
            txtRol.Text = vSupervisor.NombreRol;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}