﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LupaSupervisor.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master" Inherits="Page_Contratos_SupervisorInterContrato_LupaSupervisor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

<asp:Panel runat="server" ID="pnlConsulta" >

<table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo Identificación
                
            </td>
            <td>
                Número Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoIdentificacion" >
                </asp:DropDownList>
                
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="11" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo de Vinculación
                
            </td>
            <td>
                Regional
                
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 50%">
                <asp:DropDownList runat="server" ID="ddlTipoVinculacion" >
                </asp:DropDownList>
            </td>
            <td>
               <asp:DropDownList runat="server" ID="ddlRegional" >
                </asp:DropDownList>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Nombre
                
            </td>
            <td>
                Segundo Nombre
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" MaxLength="50" ></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Apellido
                
            </td>
            <td>
                Segundo Apellido
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" MaxLength="50" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" MaxLength="50" ></asp:TextBox>
                
            </td>
        </tr>
    </table>

</asp:Panel>

<asp:Panel runat="server" ID="pnlLista">
            <table width="90%" align="center">
                    <tr class="rowAG">
                        <td>
                            <asp:GridView runat="server" ID="gvSupervisor" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdSupervisor,ID_Ident" CellPadding="0" Height="16px"
                                OnSorting="gvSupervisor_Sorting" AllowSorting="True" 
                                OnPageIndexChanging="gvSupervisor_PageIndexChanging" OnSelectedIndexChanged="gvSupervisor_SelectedIndexChanged"> 
                                <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion"  />
                            <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                            <asp:BoundField HeaderText="Tipo Vinculación Contractual" DataField="TipoVinculacionContractual" SortExpression="TipoVinculacionContractual"  />
                            <asp:BoundField HeaderText="Nombre Empleado" DataField="NombreCompleto" SortExpression="NombreCompleto"  />
                            <asp:BoundField HeaderText="Regional" DataField="Regional" SortExpression="Regional"  />
                            <asp:BoundField HeaderText="Dependencia" DataField="Dependencia" SortExpression="Dependencia"  />
                            <asp:BoundField HeaderText="Cargo" DataField="Cargo" SortExpression="Cargo"  />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfPrimerNombre" Value='<%#Eval("PrimerNombre")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfSegundoNombre" Value='<%#Eval("SegundoNombre")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfPrimerApellido" Value='<%#Eval("PrimerApellido")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfSegundoApellido" Value='<%#Eval("SegundoApellido")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfDireccion" Value='<%#Eval("Direccion")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfTelefono" Value='<%#Eval("Telefono")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hfCorreoElectronico" Value='<%#Eval("CorreoElectronico")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
</asp:Panel>

</asp:Content>
