﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetalleInterventor.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master" Inherits="Page_Contratos_SupervisorInterContrato_DetalleInterventor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
 <table width="90%" align="center">
     <tr class="rowB">
            <td>
                Tipo de Persona
                
            </td>
            <td>
                 
                Tipo Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoPersona" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                
                <asp:TextBox runat="server" ID="txtTipoIdentificacion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nùmero de Identificación
                
            </td>
            <td>
                 
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer nombre
            </td>
            <td>
                Segundo nombre
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
               
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer apellido
            </td>
            <td>
                Segundo apellido
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
         <tr class="rowB">
            <td>
                Razón social
                
            </td>
            <td>
                 
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRazonSocial" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Dirección 
            </td>
            <td>
                Teléfono
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDireccion" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTelefono" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
        </table>
</asp:Content>