﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using AjaxControlToolkit;
using System.Linq.Expressions;
using Icbf.Proveedor.Service;

public partial class Page_Contratos_SupervisorInterContrato_LupaSupervisor : System.Web.UI.Page
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/Contratos";
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }

    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvSupervisor, GridViewSortExpression, true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.EstablecerTitulos("Seleccionar Supervisor");

            gvSupervisor.EmptyDataText = "No se encontraron datos, verifique por favor";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }



    protected void gvSupervisor_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisor.SelectedRow);
    }
    protected void gvSupervisor_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvSupervisor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisor.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////


        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vIdTipoidentificacion = null;
            String vNumeroIdentificacion = null;
            String vIdTipoVinculacion = null;
            String vIdRegional = null;
            String vPrimerNombre = null;
            String vSegundoNombre = null;
            String vPrimerApellido = null;
            String vSegundoApellido = null;

            if (ddlTipoIdentificacion.SelectedValue != "-1")
            {
                vIdTipoidentificacion = Convert.ToString(ddlTipoIdentificacion.SelectedValue);
            }
            if (txtNumeroIdentificacion.Text.Trim() != string.Empty)
            {
                vNumeroIdentificacion = txtNumeroIdentificacion.Text;
            }
            if (ddlTipoVinculacion.SelectedValue != "-1")
            {
                vIdTipoVinculacion = ddlTipoVinculacion.SelectedValue;
            }
            if (ddlRegional.SelectedValue != "-1")
            {
                vIdRegional = ddlRegional.SelectedValue;
            }
            if(txtPrimerNombre.Text.Trim() != string.Empty)
            {
                vPrimerNombre = txtPrimerNombre.Text;
            }
            if(txtSegundoNombre.Text.Trim() != string.Empty)
            {
                vSegundoNombre = txtSegundoNombre.Text;
            }
            if(txtPrimerApellido.Text.Trim() != string.Empty)
            {
                vPrimerApellido = txtPrimerApellido.Text;
            }
            if(txtSegundoApellido.Text.Trim() != string.Empty)
            {
                vSegundoApellido = txtSegundoApellido.Text;
            }


            var myGridResults = vContratoService.ConsultarSupervisors(vIdTipoidentificacion, vNumeroIdentificacion, vIdTipoVinculacion, vIdRegional,vPrimerNombre, vSegundoNombre, vPrimerApellido, vSegundoApellido);

            if (myGridResults.Count > Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NumRegConsultaGrilla"]))
            {
                throw new Exception("Esta consulta es demasiado grande, ingrese un criterio de consulta");
            }

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Supervisor), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Supervisor, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {

            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvSupervisor.DataKeys[gvSupervisor.SelectedRow.RowIndex].Values["IdSupervisor"].ToString()) + "';";

            for (int c = 1; c <= 7; c++)
            {
                if (gvSupervisor.Rows[gvSupervisor.SelectedIndex].Cells[c].Text != "&nbsp;")
                {

                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvSupervisor.Rows[gvSupervisor.SelectedIndex].Cells[c].Text) + "';";
                }
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }

            returnValues += "pObj[" + (8) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfPrimerNombre")).Value) + "';";
            returnValues += "pObj[" + (9) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfSegundoNombre")).Value) + "';";
            returnValues += "pObj[" + (10) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfPrimerApellido")).Value) + "';";
            returnValues += "pObj[" + (11) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfSegundoApellido")).Value) + "';";
            returnValues += "pObj[" + (12) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfDireccion")).Value) + "';";
            returnValues += "pObj[" + (13) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfTelefono")).Value) + "';";
            returnValues += "pObj[" + (14) + "] = '" + HttpUtility.HtmlDecode(((HiddenField)gvSupervisor.Rows[gvSupervisor.SelectedIndex].FindControl("hfCorreoElectronico")).Value) + "';";
            returnValues += "pObj[" + (15) + "] = '" + HttpUtility.HtmlDecode(gvSupervisor.DataKeys[gvSupervisor.SelectedRow.RowIndex].Values["ID_Ident"].ToString()) + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                                       " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                "</script>";


            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarTipoIdentificacion();
            CargarTipoVinculacion();
            CargarRegionales();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarTipoIdentificacion()
    { 
        //var vResultado = vContratoService.ConsultarSupervisorTipoIdentificacions();
        //ManejoControlesContratos.LlenarComboLista(ddlTipoIdentificacion, vResultado, "ID_Ident", "desc_ide");
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarComboTipoIdentificacion(ddlTipoIdentificacion, "-1", true);
    }

    private void CargarTipoVinculacion()
    {
        var vResultado = vContratoService.ConsultarSupervisorTipoVinculacions().OrderBy(x => x.desc_vin);
        ManejoControlesContratos.LlenarComboLista(ddlTipoVinculacion, vResultado, "desc_vin", "desc_vin");
    }

    private void CargarRegionales()
    {
        //var vResultado = vContratoService.ConsultarSupervisorRegionals();
        //ManejoControlesContratos.LlenarComboLista(ddlRegional, vResultado, "ID_regio", "regional");
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarComboRegional(ddlRegional, "-1", true);
    }
}