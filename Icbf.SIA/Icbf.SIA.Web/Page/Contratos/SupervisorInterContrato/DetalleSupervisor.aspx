﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetalleSupervisor.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master" Inherits="Page_Contratos_SupervisorInterContrato_DetalleSupervisor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">
    
    <asp:Panel runat="server" ID="pnlConsulta" >

    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo Identificación
            </td>
            <td>
                Número Identificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                
                <asp:TextBox runat="server" ID="txtTipoIdentificacion" Width="85%"  Enabled="false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" MaxLength="11" Width="85%"  Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Vinculación Contractual
            </td>
            <td>
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoVinculacionContractual" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Nombre
            </td>
            <td>
                Segundo Nombre
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
               
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Apellido
            </td>
            <td>
                Segundo Apellido
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional
            </td>
            <td>
                Dependencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDependencia" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Cargo 
            </td>
            <td>
                Rol Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCargo" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRol" MaxLength="50"  Width="85%" Enabled="false"></asp:TextBox>
                
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
