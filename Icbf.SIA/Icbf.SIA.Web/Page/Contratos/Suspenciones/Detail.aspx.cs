using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

public partial class Page_Suspensiones_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    
    string PageName = "Contratos/Suspensiones";
    const string TIPO_ESTRUCTURA = "Suspension"; 
    ContratoService vContratoService = new ContratoService();

    private SIAService vSiaService = new SIAService();

    int vIdContrato;
    int vIdDetalleConsModContractual;
    int vSuspensionesIdSuspension;
    int vIdSuspension;
    int vIdConsModContractual;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatos();
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        vIdSuspension = Convert.ToInt32(hfIdSuspensiones.Value);
        vIdContrato = Convert.ToInt32(hfIdContrato.Value);
        vIdDetalleConsModContractual = Convert.ToInt32(hfIdDetConsModContractual.Value);
        vIdConsModContractual = Convert.ToInt32(hfIdConsModContractual.Value);

        SetSessionParameter("Suspensiones.IdSuspension", vIdSuspension);
        SetSessionParameter("Contrato.IdContrato", vIdContrato);
        SetSessionParameter("ConsModContractual.IDCosModContractual", vIdConsModContractual);
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", vIdDetalleConsModContractual);
        Response.Redirect("~/Page/Contratos/Suspenciones/Add.aspx?oP=E", false);
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());

            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    private void CargarDatos()
    {
        try
        {
            if (
                 !string.IsNullOrEmpty(GetSessionParameter("Contrato.IdContrato").ToString()) &&
                 !string.IsNullOrEmpty(GetSessionParameter("Suspensiones.IdSuspension").ToString())
               )
            {
                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
                {
                    var vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                    RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                    hfIdConsModContractual.Value = vIdConModContratual.ToString();

                    var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);

                    if (detalleSolicitud.Estado == "Registro" || detalleSolicitud.Estado == "Devuelta")
                    {
                        toolBar.MostrarBotonEditar(true);
                        toolBar.OcultarBotonEliminar(false);
                    }
                    else
                    {
                        toolBar.MostrarBotonEditar(false);
                        toolBar.OcultarBotonEliminar(true);
                    }
                }
                else
                {
                    var vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                    RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                    hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();

                    if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                    {
                        hfEsSubscripcion.Value = "1";
                        RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                    }
                    else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                    {
                        hfEsSubscripcion.Value = "2";
                        RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                    }

                    toolBar.MostrarBotonEditar(false);
                }

                hfIdContrato.Value = Convert.ToString(GetSessionParameter("Contrato.IdContrato"));
                vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                hfIdSuspensiones.Value = Convert.ToString(GetSessionParameter("Suspensiones.IdSuspension"));
                vIdSuspension = Convert.ToInt32(hfIdSuspensiones.Value);

                var vSuspensiones = vContratoService.ConsultarSuspensiones(vIdSuspension);
                Textfechainisuspension.Text = vSuspensiones.FechaInicio.ToString();
                Textfechafinsuspension.Text = vSuspensiones.FechaFin.ToString();

                string calculo = string.Empty;

                ContratoService.ObtenerDiferenciaFechas(vSuspensiones.FechaInicio, vSuspensiones.FechaFin, out calculo);

                var itemsFecha = calculo.Split('|');
                TextDiasSuspension.Text = itemsFecha[2];
                TextMesesSuspension.Text = itemsFecha[1];
                TextAnnosSuspension.Text = itemsFecha[0];

                TextFechaReinicioCon.Text = vSuspensiones.ReinicioContrato.ToString();
                //txtFechaSolicitud.Text = vSuspensiones.FechaSolicitud.ToString();
                TextValorDescuentoSus.Text = string.Format("{0:$#,##0}", vSuspensiones.ValorSuspencion);
                vSuspensionesIdSuspension = vIdSuspension;
                hfIdDetConsModContractual.Value = Convert.ToString(vSuspensiones.IDDetalleConsModContractual);
                hfIdSuspensiones.Value = Convert.ToString(vIdSuspension);
                rblTiempoConmutable.SelectedValue = vSuspensiones.ValorConmutado.ToString();
                vIdDetalleConsModContractual = Convert.ToInt32(vSuspensiones.IDDetalleConsModContractual);

                var vConsModContractual = vContratoService.ConsModContractualPorDetalleConsModContractual(vIdDetalleConsModContractual);
                txtFechaSolicitud.Text = vConsModContractual.FechaSolicitud.ToString();
                txtJustificacion.Text = vConsModContractual.Justificacion;

                var vContrato = vContratoService.ConsultarContrato(vIdContrato);
                txtNumeroContrato.Text = vContrato.NumeroContrato;
                txtRegional.Text = vContrato.NombreRegional;
                TextFechaInicio.Text = vContrato.FechaInicioEjecucion.HasValue ? vContrato.FechaInicioEjecucion.Value.ToString() : string.Empty;
                TextFechaFinalTerminacion.Text = vContrato.FechaFinalTerminacionContrato.HasValue ? vContrato.FechaFinalTerminacionContrato.Value.ToString() : string.Empty;
                TextObjeto.Text = vContrato.ObjetoContrato;
                TextAlcance.Text = vContrato.AlcanceObjetoContrato;
                txtvalorini.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
                txtvalorfinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

                gvanexos.EmptyDataText = EmptyDataText();
                gvanexos.PageSize = PageSize();
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();

                if (GetSessionParameter("Suspensiones.Guardado").ToString() == "1")
                {
                    toolBar.MostrarMensajeGuardado();
                    RemoveSessionParameter("Suspensiones.Guardado");
                }
                RemoveSessionParameter("Suspensiones.IdSuspension");
                RemoveSessionParameter("Contrato.IdContrato");

                List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
                if (supervisoresInterventores != null)
                {
                    var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                    gvSupervisoresActuales.DataSource = querySupervisores;
                    gvSupervisoresActuales.DataBind();
                }

            }
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSuspensiones.UsuarioCrea, vSuspensiones.FechaCrea, vSuspensiones.UsuarioModifica, vSuspensiones.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarRegistro()
    {
        try
        {

            Suspensiones vSuspensiones = new Suspensiones();
            vIdSuspension = Convert.ToInt32(hfIdSuspensiones.Value);
            vSuspensiones = vContratoService.ConsultarSuspensiones(vIdSuspension);
            //InformacionAudioria(vSuspensiones, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarSuspensiones(vSuspensiones);
            /*if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {*/
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                RemoveSessionParameter("Suspensiones.IdSuspension");
                RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                RemoveSessionParameter("Contrato.IdContrato");
                SetSessionParameter("Suspensiones.Eliminado", "1");
                NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");

            /*}
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.OcultarBotonGuardar(true);

            toolBar.EstablecerTitulos("Suspensiones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }
}
