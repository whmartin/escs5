using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using Icbf.SIA.Service;

public partial class Page_Suspensiones_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    string PageName = "Contratos/Suspensiones";
    const string TIPO_ESTRUCTURA = "Suspension"; 
    int vIdContrato;
    decimal vIdIndice=0;
    int vIdDetalleConsModContractual ;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                //gvSupervicion.PageSize = PageSize();
                //gvSupervicion.EmptyDataText = EmptyDataText();
                gvanexos.PageSize = PageSize();
                gvanexos.EmptyDataText = EmptyDataText();
                CargarDatosIniciales();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
  
    protected void EliminarAnexo_Click(object sender, EventArgs e)
    {
        //EliminarAnexo(vIdIndice);
    }

    protected void btnbtnView_Click(object sender, ImageClickEventArgs e)
    {
        int rowIndex = 0;
        GridViewRow pRow = gvanexos.SelectedRow;        
        rowIndex = pRow.RowIndex;
        toolBar.MostrarMensajeError(Convert.ToString(rowIndex));
       
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }    
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIDDetalleConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());
            Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    private void Guardar()
    {
        try
        {

         int vResultado;

         if (ValidarFechas())
         {
            vIdContrato  = Convert.ToInt32(hfIdContrato.Value);
            var contrato = vContratoService.ConsultarContrato(int .Parse(hfIdContrato.Value));
            
            var fechaFinal = Convert.ToDateTime(Textfechafinsuspension.Text.ToString());
            var fechaInico = Convert.ToDateTime(Textfechainisuspension.Text.ToString());

            vIdDetalleConsModContractual = Convert.ToInt32(hfIDDetalleConsModContractual.Value);

            Suspensiones vSuspensiones = new Suspensiones();
            vSuspensiones.FechaInicio = fechaInico;
            vSuspensiones.FechaFin = fechaFinal;
            vSuspensiones.IDDetalleConsModContractual = vIdDetalleConsModContractual;
            vSuspensiones.ValorConmutado = Convert.ToInt32(rblTiempoConmutable.SelectedValue);
            string calculo = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(vSuspensiones.FechaInicio, vSuspensiones.FechaFin, out calculo);

            var itemsFecha = calculo.Split('|');
            vSuspensiones.SuspensionDias = int.Parse(itemsFecha[2]);
            vSuspensiones.SuspensionMeses = int.Parse(itemsFecha[1]);
            vSuspensiones.Suspensionyear = int.Parse(itemsFecha[0]);

            if (Request.QueryString["oP"] == "E")
            {
                vSuspensiones.IdSuspension = Convert.ToInt32(hfIdSuspension.Value);
                vSuspensiones.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSuspensiones, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarSuspensiones(vSuspensiones);
            }
            else
            {
                vSuspensiones.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSuspensiones, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarSuspensiones(vSuspensiones);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", vIdDetalleConsModContractual);
                SetSessionParameter("Contrato.IdContrato", vIdContrato);
                SetSessionParameter("Suspensiones.IdSuspension", vSuspensiones.IdSuspension);
                SetSessionParameter("Suspensiones.Guardado", "1");

                if (hfIdConsModContractual.Value != "")
                    SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
                else
                    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());

                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
         }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool ValidarFechas()
    {
        bool isValid = true;

        DateTime fechaInicioSuspension;
        DateTime fechaFinSuspension;

        try
        {
            if (!DateTime.TryParse(Textfechainisuspension.Text, out fechaInicioSuspension))
            {
                toolBar.MostrarMensajeError("el formato de la fecha de inicio de la suspensión es invalido");
                isValid = false;
            }
            else if (!DateTime.TryParse(Textfechafinsuspension.Text, out fechaFinSuspension))
            {
                toolBar.MostrarMensajeError("el formato de la fecha de fin de la suspensión es invalido");
                isValid = false;
            }
            else
            {
                var contrato = vContratoService.ConsultarContrato(int.Parse(hfIdContrato.Value));

                if (fechaInicioSuspension <= contrato.FechaInicioEjecucion.Value)
                {
                    toolBar.MostrarMensajeError("la fecha de inicio de la suspensión es menor o igual a la fecha de Inicio del contrato ");
                    isValid = false;
                }
                else if (fechaFinSuspension <= contrato.FechaInicioEjecucion.Value)
                {
                    toolBar.MostrarMensajeError("la fecha de terminación de la suspensión es menor o igual fecha de Inicio del contrato ");
                    isValid = false;
                }
                else if (fechaInicioSuspension > contrato.FechaFinalTerminacionContrato.Value)
                {
                    toolBar.MostrarMensajeError("la fecha de inicio de la suspensión es mayor a la fecha de finalización del contrato ");
                    isValid = false;
                }
                else if (fechaFinSuspension > contrato.FechaFinalTerminacionContrato.Value)
	            {
                    toolBar.MostrarMensajeError("la fecha de terminación de la suspensión es mayor a la fecha de finalización del contrato ");
                    isValid = false;
	            }
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        return isValid;
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Suspensiones", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdSuspension = Convert.ToInt32(GetSessionParameter("Suspensiones.IdSuspension"));
            RemoveSessionParameter("Suspensiones.IdSuspension");

            Suspensiones vSuspensiones = new Suspensiones();
            vSuspensiones = vContratoService.ConsultarSuspensiones(vIdSuspension);
            hfIdSuspension.Value = vSuspensiones.IdSuspension.ToString();
            Textfechainisuspension.Text = vSuspensiones.FechaInicio.ToString();
            Textfechafinsuspension.Text = vSuspensiones.FechaFin.ToString();

            string calculo = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(vSuspensiones.FechaInicio, vSuspensiones.FechaFin, out calculo);

            var itemsFecha = calculo.Split('|');
            vSuspensiones.SuspensionDias = int.Parse(itemsFecha[2]);
            vSuspensiones.SuspensionMeses = int.Parse(itemsFecha[1]);
            vSuspensiones.Suspensionyear = int.Parse(itemsFecha[0]);

            TextDiasSuspension.Text = itemsFecha[2];
            TextMesesSuspension.Text = itemsFecha[1];
            TextAnnosSuspension.Text = itemsFecha[0];

            vIdDetalleConsModContractual = Convert.ToInt32(vSuspensiones.IDDetalleConsModContractual.ToString());
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSuspensiones.UsuarioCrea, vSuspensiones.FechaCrea, vSuspensiones.UsuarioModifica, vSuspensiones.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {         
            hfIdContrato.Value = Convert.ToString(GetSessionParameter("Contrato.IdContrato"));
            vIdContrato = Convert.ToInt32(hfIdContrato.Value);
            RemoveSessionParameter("Contrato.IdContrato");

            hfIDDetalleConsModContractual.Value = Convert.ToString(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
            vIdDetalleConsModContractual = Convert.ToInt32(hfIDDetalleConsModContractual.Value);
            RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
            

            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                var vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();
            }
            else
            {
                var vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();
            }

            if (vIdContrato == 0) 
            {
               var vConsModContractualCon = vContratoService.ConsModContractualPorDetalleConsModContractual(vIdDetalleConsModContractual);
                vIdContrato = Convert.ToInt32(vConsModContractualCon.IDContrato.ToString());
            }           

            //rblTiempoConmutable.SelectedValue = "1";

            var vContrato = vContratoService.ConsultarContrato(vIdContrato);
            txtNumeroContrato.Text = vContrato.NumeroContrato;
            txtRegional.Text = vContrato.NombreRegional;
            TextFechaInicio.Text = vContrato.FechaInicioEjecucion.ToString();
            TextFechaFinalTerminacion.Text = vContrato.FechaFinalTerminacionContrato.ToString();
            TextObjeto.Text = vContrato.ObjetoContrato.ToString();
            TextAlcance.Text = vContrato.AlcanceObjetoContrato.ToString();
            txtvalorini.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            var  vConsModContractual = vContratoService.ConsModContractualPorDetalleConsModContractual(vIdDetalleConsModContractual);
            txtFechaSolicitud.Text = vConsModContractual.FechaSolicitud.ToString();
            txtJustificacion.Text = vConsModContractual.Justificacion.ToString();

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            CalendarExtender2.StartDate = vContrato.FechaInicioEjecucion;
            if (!GetSessionParameter("Suspensiones.IdSuspension").Equals(""))
            {
                toolBar.MostrarBotonNuevo(false);
                hfIdSuspension.Value = Convert.ToString(GetSessionParameter("Suspensiones.IdSuspension"));
                int vIdSuspensionINI = Convert.ToInt32(hfIdSuspension.Value);
                
                var vSuspensiones = vContratoService.ConsultarSuspensiones(vIdSuspensionINI);
                hfIdSuspension.Value = vSuspensiones.IdSuspension.ToString();
                Textfechainisuspension.Text = vSuspensiones.FechaInicio.ToString();
                Textfechafinsuspension.Text = vSuspensiones.FechaFin.ToString();
                TextFechaReinicioCon.Text = vSuspensiones.ReinicioContrato.ToString();
                TextDiasSuspension.Text = vSuspensiones.SuspensionDias.ToString();
                TextMesesSuspension.Text = vSuspensiones.SuspensionMeses.ToString();
                TextValorDescuentoSus.Text = string.Format("{0:$#,##0}", vSuspensiones.ValorSuspencion);
                TextAnnosSuspension.Text = vSuspensiones.Suspensionyear.ToString();
                rblTiempoConmutable.SelectedValue = vSuspensiones.ValorConmutado.ToString();
                RemoveSessionParameter("Suspensiones.IdSuspension");
            }
            else
            {
                toolBar.MostrarBotonNuevo(true);
            }
        } 
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void txtFechaFin_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        DateTime fechavalidacion = new DateTime(1900, 1, 1);
        if (!DateTime.TryParse(Textfechafinsuspension.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de Terminacion de la Suspención no es Válida");
            return;
        }
        if (Convert.ToDateTime(Textfechafinsuspension.Text) < Convert.ToDateTime(Textfechainisuspension.Text))
        {
            toolBar.MostrarMensajeError("Fecha de Final de la Suspención no es Válida");
            return;
        }
        DateTime caTxtFechaFinalContrato = Convert.ToDateTime(TextFechaFinalTerminacion.Text);
        DateTime caTxtFechaInicioContrato = Convert.ToDateTime(TextFechaInicio.Text);
        DateTime caFechaInicioSuspencion = Convert.ToDateTime(Textfechainisuspension.Text);
        DateTime caFechaFinalizacionSuspencion = Convert.ToDateTime(Textfechafinsuspension.Text);


        if (!DateTime.TryParse(TextFechaInicio.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de Inicio de la Suspención no es Válida");
            return;
        }

        if (!DateTime.TryParse(TextFechaFinalTerminacion.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha Final de la Suspención no es Válida");
            return;
        }
       

        string calculo = string.Empty;

        ContratoService.ObtenerDiferenciaFechas(caFechaInicioSuspencion, caFechaFinalizacionSuspencion, out calculo);

        var itemsFecha = calculo.Split('|');

        TextDiasSuspension.Text = itemsFecha[2];
        TextMesesSuspension.Text = itemsFecha[1];
        TextAnnosSuspension.Text = itemsFecha[0];
        DateTime caFechaCalculada = caFechaFinalizacionSuspencion.AddDays(1);
        TextFechaReinicioCon.Text = caFechaCalculada.ToShortDateString();

        calculo = string.Empty;        
    }

    protected void txtFechaIni_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vIdContrato = Convert.ToInt32(hfIdContrato.Value);
        var vContrato = vContratoService.ConsultarContrato(vIdContrato);

        DateTime fechavalidacion = new DateTime(1900, 1, 1);
        DateTime fechaInicio = Convert.ToDateTime(Textfechainisuspension.Text);

        bool isvalid =   fechaInicio < vContrato.FechaInicioEjecucion;

        if (isvalid)
        {
            toolBar.MostrarMensajeError("Fecha de Inicio de la Suspención no es Válida");
            return;
        }

        
        if (!DateTime.TryParse(Textfechainisuspension.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de Inicio de la Suspención no es Válida");
            return;
        }

        CalendarExtender4.Enabled = true;
        DateTime caFechaInicioSuspencion = Convert.ToDateTime(Textfechainisuspension.Text);
        CalendarExtender4.StartDate = caFechaInicioSuspencion.AddDays(1);
        Textfechafinsuspension.Enabled = true;
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            vIdIndice = Convert.ToInt64(strIDCosModContractual);
            hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }   

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    private ContratoService vTipoSolicitudService = new ContratoService();

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();
        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }
}
