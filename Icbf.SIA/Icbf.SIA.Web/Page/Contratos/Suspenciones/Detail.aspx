<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Suspensiones_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
<asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
<asp:HiddenField ID="hfIdSuspensiones" runat="server" />
<asp:HiddenField ID="hfIdContrato" runat="server" />
<asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />
<asp:HiddenField ID="hfIdConsModContractual" runat="server" />
<asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
<asp:HiddenField runat="server" ID="hfEsSubscripcion" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Numero Contrato
            </td>
            <td>
                Regional
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Width="316px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               Fecha Inicio de Contrato / Convenio
            </td>
            <td>
               Fecha Final de Terminaci&oacute;n / Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="TextFechaInicio" Enabled="False" Width="242px"></asp:TextBox> 
                <Ajax:MaskedEditExtender ID="MaskedEditExtender3" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TextFechaInicio">
                    </Ajax:MaskedEditExtender>            
            </td>
             <td>
                <asp:TextBox runat="server" ID="TextFechaFinalTerminacion" Width="259px" Enabled="False"></asp:TextBox>
                 <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TextFechaFinalTerminacion">
                    </Ajax:MaskedEditExtender> 
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style1">
               Objeto del Contrato
            </td>
            <td class="auto-style1">
               Alcance del Contrato
            </td>
        </tr>
         <tr class="rowA" >
            <td>
                <asp:TextBox runat="server" ID="TextObjeto" Width="320px" Height="88px" Enabled="False" TextMode="MultiLine" MaxLength="1024" ></asp:TextBox>
            </td>
             <td>
                <asp:TextBox runat="server" ID="TextAlcance" Width="320px" Height="88px" TextMode ="MultiLine" Enabled="False" MaxLength="1024" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorini"  Enabled="False" 
                     MaxLength="128" Width="234px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="False" 
                     MaxLength="128" Width="227px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>    
    <asp:Panel runat="server" ID="Panel1">
         <table width="90%" align="center">
          <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Solicitud de Modificaci&oacute;n
            </td>
            <td style="width: 50%">
                Justificaci&oacute;n de la Novedad Contractual
            </td>
          </tr>
           <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaSolicitud"  Enabled="False" 
                    MaxLength="128" Width="185px" Height="22px"></asp:TextBox> 
                <Ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaSolicitud">
                    </Ajax:MaskedEditExtender> 
            </td>
            <td>
                 <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Rows="4"  Enabled="False" 
                     MaxLength="128" Width="320px" Height="83px" ></asp:TextBox>
            </td>
        </tr>
        </table>  
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Supervisores</td>
            <td>
                &nbsp;</td>
        </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:GridView ID="gvSupervisoresActuales" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                            <%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                            <asp:TemplateField HeaderText="Fecha de Inicio">
                                <ItemTemplate>
                                    <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                    <%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                            <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
      
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel2">
         <table width="90%" align="center">
          <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio Suspensi&oacute;n
            </td>
            <td style="width: 50%">
                Fecha Terminaci&oacute;n Suspensi&oacute;n
            </td>
          </tr>
           <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="Textfechainisuspension"  Enabled="False" 
                    MaxLength="128" Width="182px" Height="22px"></asp:TextBox>
                <Ajax:MaskedEditExtender ID="MaskedEditExtender4" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="Textfechainisuspension">
                    </Ajax:MaskedEditExtender> 
            </td>
            <td>
                 <asp:TextBox runat="server" ID="Textfechafinsuspension"  Enabled="False" 
                     MaxLength="128" Width="211px" Height="22px"></asp:TextBox>
                <Ajax:MaskedEditExtender ID="MaskedEditExtender5" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="Textfechafinsuspension">
                    </Ajax:MaskedEditExtender> 
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Reinicio Contrato
            </td>
            <td style="width: 50%">
                Tiempo de Suspens&oacute;n
            </td>
          </tr>
          <tr class="rowB">
            <td class="style1">
                <asp:TextBox runat="server" ID="TextFechaReinicioCon"  Enabled="False" 
                    MaxLength="128" Width="187px" Height="22px"></asp:TextBox>
                <Ajax:MaskedEditExtender ID="MaskedEditExtender6" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TextFechaReinicioCon">
                    </Ajax:MaskedEditExtender> 
            </td>
            <td>
                Dias:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  <asp:TextBox runat="server" ID="TextDiasSuspension"  Enabled="False" 
                     MaxLength="128" Width="97px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor descuento por Suspensi&oacute;n
            </td>
            <td>
                  Meses:&nbsp;&nbsp;&nbsp;  <asp:TextBox runat="server" ID="TextMesesSuspension"  Enabled="False" 
                     MaxLength="128" Width="97px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                <asp:TextBox runat="server" ID="TextValorDescuentoSus"  Enabled="False" 
                    MaxLength="128" Width="187px" Height="22px"></asp:TextBox>
            </td>
            <td>
                A&ntilde;os:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <asp:TextBox runat="server" ID="TextAnnosSuspension"  Enabled="False" 
                     MaxLength="128" Width="97px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Tiempo de Suspensi&oacute;n conmutable?
            </td>
            <td>
                  
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
               <asp:RadioButtonList runat="server" ID="rblTiempoConmutable" RepeatDirection="Horizontal" style="margin-left: 161px" Enabled="False"  >
               <asp:ListItem Text="SI" Value="1"></asp:ListItem>
               <asp:ListItem Text="NO" Value="0"></asp:ListItem>     
               </asp:RadioButtonList>     
            </td>
            <td>
               
            </td>
        </tr>
        </table>  
    </asp:Panel>
    <asp:Panel runat="server" Width="90%" ID="Panel4">
        <table width="90%" align="center">
        <tr class="rowA">
            <td>
               
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
