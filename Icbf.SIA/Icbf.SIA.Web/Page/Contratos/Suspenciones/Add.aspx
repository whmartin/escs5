<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Suspensiones_Add" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdSuspension" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />
     <asp:HiddenField ID="hfIndice" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
     <asp:HiddenField ID="hfIdConsModContractual" runat="server" />

    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Numero Contrato
            </td>
            <td>
                Regional
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Width="316px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               Fecha Inicio de Contrato / Convenio
            </td>
            <td>
               Fecha Final de Terminaci&oacute;n / Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="TextFechaInicio" Enabled="False" Width="238px"></asp:TextBox> 
                 <Ajax:MaskedEditExtender ID="meetxtFechaInicio" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TextFechaInicio">
                    </Ajax:MaskedEditExtender>  
            </td>
             <td>
                <asp:TextBox runat="server" ID="TextFechaFinalTerminacion" Width="255px" Enabled="False"></asp:TextBox>
                 <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TextFechaFinalTerminacion">
                    </Ajax:MaskedEditExtender> 
            </td>
        </tr>
        <tr class="rowB">
            <td>
               Objeto del Contrato
            </td>
            <td>
               Alcance del Contrato
            </td>
        </tr>
         <tr class="rowA" >
            <td>
                <asp:TextBox runat="server" ID="TextObjeto" TextMode="MultiLine" Width="320px" Height="88px" Enabled="False" ></asp:TextBox>
            </td>
             <td>
                <asp:TextBox runat="server" ID="TextAlcance" TextMode="MultiLine" Width="320px" Height="88px" Enabled="False" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td>
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td>
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtvalorini"  Enabled="False" 
                     MaxLength="128" Width="234px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="False" 
                     MaxLength="128" Width="227px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>Supervisores&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
                <tr class="rowA">
            <td colspan="2">
                <asp:GridView ID="gvSupervisoresActuales" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                        <%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                        <asp:TemplateField HeaderText="Fecha de Inicio">
                            <ItemTemplate>
                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                <%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                       <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                    </td>
        </tr>
    </table>
    </asp:Panel>
<%--    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td><asp:GridView ID="gvSupervicion" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Supervisor y/o Interventor" DataField="EtQSupervisorInterventor" />
                                                    <asp:BoundField HeaderText="Tipo Supervisor y/o Interventor" DataField="EtQInternoExterno" />
                                                    <asp:BoundField HeaderText="Fecha de Inicio" DataField="FechaInicio" />                                                    
                                                    <asp:BoundField HeaderText="Tipo de Identificacion" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Numero de Identificacion" DataField="Identificacion" />  
                                                    <asp:BoundField HeaderText="Tipo de vinculacion" DataField="TipoVinculacionContractual" />                                                      
                                                    <asp:BoundField HeaderText="Nombre Supervisor y/o Interventor" DataField="NombreCompleto" />
                                                    <asp:BoundField HeaderText="Numero de Contrato Interventoria" DataField="IdNumeroContratoInterventoria" />                                                    
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
    <asp:Panel runat="server" ID="Panel1">
         <table width="90%" align="center">
          <tr class="rowB">
            <td>
                Fecha Solicitud de Modificaci&oacute;n
            </td>
            <td>
                Justificaci&oacute;n de la Novedad Contractual
            </td>
          </tr>
           <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaSolicitud"  Enabled="false" 
                    MaxLength="128" Width="185px" Height="22px"></asp:TextBox> 
                <Ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaSolicitud">
                    </Ajax:MaskedEditExtender>  
            </td>
            <td>
                 <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Rows="4" Enabled="false" 
                     MaxLength="128" Width="320px" Height="83px" ></asp:TextBox>
            </td>
        </tr>
        </table>  
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td><asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="Panel2">
         <table width="90%" align="center">
          <tr class="rowB">
            <td >
                Fecha Inicio Suspensi&oacute;n
            </td>
            <td>
                Fecha Terminacion Suspensi&oacute;n
            </td>
          </tr>
           <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="Textfechainisuspension" 
                    MaxLength="128" Width="182px" Height="22px" OnTextChanged="txtFechaIni_OnTextChanged" AutoPostBack="True" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" 
                                                    Format="dd/MM/yyyy" PopupButtonID="ImageCalendar2" 
                                                    TargetControlID="Textfechainisuspension">
                                                </asp:CalendarExtender>
                <asp:Image ID="ImageCalendar2" runat="server" CssClass="manito" 
                                                    ImageUrl="~/Image/btn/Calendar.gif" ToolTip="Seleccione la fecha Inicial" />
                 <Ajax:MaskedEditExtender ID="MaskedEditExtender4" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="Textfechainisuspension">
                    </Ajax:MaskedEditExtender>

                
            </td>
            <td>
                 <asp:TextBox runat="server" ID="Textfechafinsuspension" 
                     MaxLength="128" Width="198px" Height="22px" OnTextChanged="txtFechaFin_OnTextChanged" AutoPostBack="True" Enabled="false" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="false" 
                Format="dd/MM/yyyy" PopupButtonID="ImageCalendar3" 
                TargetControlID="Textfechafinsuspension">
                </asp:CalendarExtender>                
                <asp:Image ID="ImageCalendar3" runat="server" CssClass="manito" 
                ImageUrl="~/Image/btn/Calendar.gif" ToolTip="Seleccione la fecha Final" />
                <Ajax:MaskedEditExtender ID="MaskedEditExtender5" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="Textfechafinsuspension">
                    </Ajax:MaskedEditExtender>

                 
            </td>
        </tr>
        
         <tr class="rowB">
            <td>
                Fecha Reinicio Contrato</td>
            <td>
                Tiempo de Suspensi&oacute;n
            </td>
          </tr>
          <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="TextFechaReinicioCon" 
                    MaxLength="128" Width="187px" Height="22px" Enabled="False"></asp:TextBox> 
                <Ajax:MaskedEditExtender ID="MaskedEditExtender3" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TextFechaReinicioCon">
                    </Ajax:MaskedEditExtender>                 
            </td>
            <td>
                Dias:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  <asp:TextBox runat="server" ID="TextDiasSuspension"  Enabled="false" 
                     MaxLength="128" Width="97px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor descuento por Suspensi&oacute;n
            </td>
            <td>
                  Meses:&nbsp;&nbsp;&nbsp;  <asp:TextBox runat="server" ID="TextMesesSuspension"  Enabled="false" 
                     MaxLength="128" Width="97px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:TextBox runat="server" ID="TextValorDescuentoSus" 
                    MaxLength="128" Width="187px" Height="22px" Enabled="False"></asp:TextBox>               
            </td>
            <td>
                A&ntilde;os:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <asp:TextBox runat="server" ID="TextAnnosSuspension"  Enabled="false" 
                     MaxLength="128" Width="97px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tiempo de Suspensi&oacute;n conmutable *
                <asp:RequiredFieldValidator runat="server" ID="RVTiempoConmutable" ControlToValidate="rblTiempoConmutable"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                  
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:RadioButtonList runat="server" ID="rblTiempoConmutable" RepeatDirection="Horizontal" style="margin-left: 161px" >
               <asp:ListItem Text="SI" Value="1"></asp:ListItem>
               <asp:ListItem Text="NO" Value="0"></asp:ListItem>     
               </asp:RadioButtonList>     
            </td>
            <td>
               
            </td>
        </tr>
        </table>  
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    </asp:Content>

