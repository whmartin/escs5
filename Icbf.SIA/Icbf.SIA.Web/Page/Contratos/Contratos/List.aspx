<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function juegoManejaHistorico(control) {                     
            var chkSiHistorico = document.getElementById('<%= chkSiHistorico.ClientID %>');
            var chkNoHistorico = document.getElementById('<%= chkNoHistorico.ClientID %>');

            if (chkSiHistorico.checked || chkNoHistorico.checked) {
                if (control.id == chkNoHistorico.id) {
                    chkSiHistorico.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkSiHistorico.ClientID %>', '');
                }
                else {
                    if (chkNoHistorico.checked) {
                        if (confirm('La busqueda se realizara al Historico de los contratos')) {
                            chkSiHistorico.checked = true;
                            chkNoHistorico.checked = false;
                            prePostbck(false);
                            __doPostBack('<%= chkSiHistorico.ClientID %>', '');
                        } else {
                            chkNoHistorico.checked = true;
                            control.checked = false;
                            ocultaImagenLoading();
                        }
                    } else {
                        prePostbck(false);
                        __doPostBack('<%= chkSiHistorico.ClientID %>', '');
                    }
                }
            }
           
        }

    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Fecha Registro al Sistema Desde
            </td>
            <td class="Cell">
                Fecha Registro al Sistema hasta
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaDesde" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaHasta" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Id Contrato/Convenio</td>
            <td class="Cell">
                Número del Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;-" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Proceso
            </td>
            <td class="Cell">
                Vigencia Fiscal Inicial
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroProceso" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroProceso" runat="server" TargetControlID="txtNumeroProceso"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;-" />
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio</td>
            <td class="Cell">
                Modalidad de Selecci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDModalidadSeleccion"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Categoria del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged" 
                    AutoPostBack="True"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td  class="Cell" >
                Estado del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Persona</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDEstadoContraro"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoPersona" AutoPostBack="True"  onselectedindexchanged="ddlIDTipoPersona_SelectedIndexChanged" ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td  class="auto-style1" >
                Tipo de Identificación</td>
            <td class="auto-style1">
                Numero de Identificación</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                 <asp:DropDownList runat="server" ID="ddlIDTipoIdentificacion"  ></asp:DropDownList>                
            </td>
            <td class="Cell">
               <asp:TextBox runat="server" ID="txtNumIdentificacion" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtNumIdentificacio" runat="server" TargetControlID="txtNumIdentificacion"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td  class="Cell" >
                 Nombre o Razón Social del Contratista  </td>    
            <td  class="Cell" >
                 Historico de Contratación   </td>       
        </tr>
        <tr class="rowA">
            <td class="Cell" rowspan="3">
                <asp:TextBox runat="server" ID="txtNombreContratista" MaxLength="50" Width="80%" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtNombreContratista" runat="server" TargetControlID="txtNombreContratista"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():-&;" />
            </td>             
         </tr>  
           <tr class="rowA">
                <td colspan="2">
                    <asp:CheckBox ID="chkSiHistorico" runat="server"  Enabled="true" onclick="juegoManejaHistorico(this)" />
                    Si
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:CheckBox ID="chkNoHistorico" runat="server"  Enabled="true" onclick="juegoManejaHistorico(this)" />
                    No
                </td>
            </tr>       
             <tr class="rowB">
            <td  class="Cell" >
                 Usuario Creación</td>    
            <td  class="Cell" >
                 &nbsp;</td>       
        </tr>
                    <tr class="rowA">
                <td >
                <asp:TextBox runat="server" ID="txtUsuarioCreacion" MaxLength="50" Width="80%" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtUsuarioCreacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():-&;" />
                </td>
                        <td>

                        </td>
            </tr>      

    </table>
        
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,IdEstadoContrato,UsuarioCrea" CellPadding="0" Height="16px"
                        OnSorting="gvContratos_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContratos_PageIndexChanging" OnSelectedIndexChanged="gvContratos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro al Sistema" DataField="FechaCrea"  SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Id Contrato   " DataField="IdContrato"  SortExpression="IdContrato"/>
                            <asp:BoundField HeaderText="Número del Contrato" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional del contrato" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Número de Proceso" DataField="NumeroProceso"  SortExpression="NumeroProceso"/>
                            <asp:BoundField HeaderText="Modalidad de Selección" DataField="NombreModalidadSeleccion"  SortExpression="NombreModalidadSeleccion"/>
                            <asp:BoundField HeaderText="Nombre del Contratista" DataField="NombreContratista"  SortExpression="NombreContratista"/>
                            <asp:BoundField HeaderText="Numero de Identificacion" DataField="IdentificacionContratista"  SortExpression="IdentificacionContratista"/>
                            <asp:BoundField HeaderText="Categoria del Contrato" DataField="NombreCategoriaContrato"  SortExpression="NombreCategoriaContrato"/>
                            <asp:BoundField HeaderText="Tipo de Contrato" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                            <asp:BoundField HeaderText="Usuario Creación" DataField="UsuarioCrea"  SortExpression="UsuarioCrea"/>
                            <asp:BoundField HeaderText="Estado de Contrato" DataField="NombreEstadoContrato"  SortExpression="NombreEstadoContrato"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 50%;
            height: 27px;
        }
    </style>
</asp:Content>

