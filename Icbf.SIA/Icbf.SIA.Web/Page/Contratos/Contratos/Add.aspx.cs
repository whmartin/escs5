using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System.Web.Security;
using Icbf.Seguridad.Service;
using WsContratosPacco;


/// <summary>
/// Página de registro y edición para la entidad Contrato
/// </summary>
public partial class Page_Contrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SIAService vSIAService = new SIAService();
    ContratoService vContratoService = new ContratoService();
    SeguridadService vSeguridadFAC = new SeguridadService();
    Contrato vContrato = new Contrato();
    string PageName = "Contratos/Contratos";
    string PageNameAprobar = "Contratos/ValidarRegistroContrato";

    private bool scriptEnCola;
    private int NoPrecargarInformacion; // Entero que sirve como bandera para no precargar información usualmente activada al momento de abrir la pagina la primera vez para un contrato ya registrado
    private const string CodEstadoEnRegistro = "REE";
    private const string CodEstadoRegistrado = "REC";
    private const string CodEstadoSuscrito = "SUS";
    private const string CodEstadoEjecucion = "EJE";
    #region ConstantesConfiguracionControles
    //Contrato Asociado
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    //Categoria contrato
    private const string CodCategoriaContrato = "CTR"; //"Contrato";
    private const string CodCategoriaConvenio = "CVN"; //"Convenio";

    //Tipos contrato
    private const string CotTipoContConvPrestServApoyoGestion = "PREST_SERV_APO_GEST"; //"Prestación de Servicios de Apoyo a la gestión";
    private const string CodTipoContConvPrestServProfesionales = "PREST_SERV_PROF"; //"Prestación de Servicios Profesionales";
    private const string CodTipoContAporte = "APORTE"; //"Aporte";
    private const string CodMarcoInteradministrativo = "MC_INT_ADMIN"; //"Marco Interadministrativo";

    //Modalidad selección
    private const string CodContratacionDirecta = "CONT_DIR"; //"Contratación Directa";
    private const string CodContratacionDirectaAporte = "CDA";
    #endregion
    
    /// <summary>
    /// Los identificadores fijos corresponden a los id de tabla que corresponden a los códigos
    /// establecidos como constantes y usados para temas de configuración de controles dentro de esta pantalla
    /// </summary>
    #region IdentificadoresFijos
    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }
    private string IdRegional
    {
        get
        {
            return hfRegional.Value;
        }
        set
        {
            hfRegional.Value = value;
        }
    }
    private string IdEstadoContrato
    {
        get
        {
            return hfIdEstadoContrato.Value;
        }
        set
        {
            hfIdEstadoContrato.Value = value;
        }
    }

    private string IdCategoriaContrato
    {
        get
        {
            return hfIdCategoriaContrato.Value;
        }
        set
        {
            hfIdCategoriaContrato.Value = value;
        }
    }
    private string IdCategoriaConvenio
    {
        get
        {
            return hfIdCategoriaConvenio.Value;
        }
        set
        {
            hfIdCategoriaConvenio.Value = value;
        }
    }

    private string IdTipoContConvPrestServApoyoGestion
    {
        get
        {
            return hfIdTipoContConvPrestServApoyoGestion.Value;
        }
        set
        {
            hfIdTipoContConvPrestServApoyoGestion.Value = value;
        }
    }
    private string IdTipoContConvPrestServProfesionales
    {
        get
        {
            return hfIdTipoContConvPrestServProfesionales.Value;
        }
        set
        {
            hfIdTipoContConvPrestServProfesionales.Value = value;
        }
    }
    //private string IdTipoContAporte
    //{
    //    get
    //    {
    //        return hfIdTipoContAporte.Value;
    //    }
    //    set
    //    {
    //        hfIdTipoContAporte.Value = value;
    //    }
    //}
    private string IdMarcoInteradministrativo
    {
        get
        {
            return hfIdMarcoInteradministrativo.Value;
        }
        set
        {
            hfIdMarcoInteradministrativo.Value = value;
        }
    }

    private string IdContratacionDirecta
    {
        get
        {
            return hfIdContratacionDirecta.Value;
        }
        set
        {
            hfIdContratacionDirecta.Value = value;
        }
    }
    private string IdContratacionDirectaAporte
    {
        get
        {
            return hfIdContratacionDirectaAporte.Value;
        }
        set
        {
            hfIdContratacionDirectaAporte.Value = value;
        }
    }
    #endregion

    private string CodContratoAsociadoSeleccionado
    {
        get
        {
            return hfCodContratoAsociadoSel.Value;
        }
        set
        {
            hfCodContratoAsociadoSel.Value = value;
        }
    }
    private string TotalProductos
    {
        get
        {
            return hfTotalProductos.Value;
        }
        set
        {
            hfTotalProductos.Value = value;
        }
    }
    private string TotalProductosCDP
    {
        get
        {
            return hfTotalProductosCDP.Value;
        }
        set
        {
            hfTotalProductosCDP.Value = value;
        }
    }
    private string IdContratoAsociado
    {
        get
        {
            return hfIdConvenioContratoAsociado.Value;
        }
        set
        {
            hfIdConvenioContratoAsociado.Value = value;
        }
    }
    private string ValorInicial
    {
        get
        {
            return hfValorInicialContConv.Value;
        }
        set
        {
            hfValorInicialContConv.Value = value;
        }
    }
    private string FechaInicio
    {
        get
        {
            return hfFechaInicioEjecucion.Value;
        }
        set
        {
            hfFechaInicioEjecucion.Value = value;
        }
    }
    private string FechaFinalizacion
    {
        get 
        {
            return hfFechaFinalizacion.Value;
        }
        set 
        {
            hfFechaFinalizacion.Value = value;
        }
    }
    private string AcordeonActivo
    {
        get 
        {
            if (hfAcordeonActivo.Value.Equals(string.Empty))
                hfAcordeonActivo.Value = "1";
            return hfAcordeonActivo.Value;
        }
        set 
        {
            hfAcordeonActivo.Value = value;
        }
    }
    private string ObjetoPlanCompras
    {
        get
        {
            return hfObjPlan.Value;
        }
        set
        {
            hfObjPlan.Value = value;
        }
    }
    private string AlcancePlanCompras
    {
        get
        {
            return hfAlcPlan.Value;
        }
        set
        {
            hfAlcPlan.Value = value;
        }
    }

    private string nConsecutivosPlanCompras
    { set { hfConsecutivosPlanCompras.Value = value; } }
    private string nProductos
    { set { hfProductos.Value = value; } }
    private string nAportes
    { set { hfAportes.Value = value; } }
    private string nLugarEjecucion
    { set { hfLugarEjecucion.Value = value; } }
    private string nContratistas
    { set { hfContratistas.Value = value; } }
    private string nSupervInterv
    { set { hfSupervInterv.Value = value; } }
    private string nCDP
    { set { hfCDP.Value = value; } }
    private string nVigenciasFuturas
    {
        get
        {
            return hfVigenciasFuturas.Value;
        }
        set { hfVigenciasFuturas.Value = value; } }
    
    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad Contrato
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            ObtenerIdContratoConvenio(); // Si al guardar no existe un identificador de contrato se crea uno
            
            vContrato.IdContrato = Convert.ToInt32(IdContrato);
            vContrato.IdEstadoContrato = Convert.ToInt32(IdEstadoContrato);

            #region DatosGeneralesContrato
            #region ObtieneIdTipoContratoAsociado
            TipoContratoAsociado vContratoAsociado = new TipoContratoAsociado();
            vContratoAsociado.CodContratoAsociado = CodContratoAsociadoSeleccionado;
            vContratoAsociado = vContratoService.IdentificadorCodigoTipoContratoAsociado(vContratoAsociado);
            #endregion
            if (vContratoAsociado.IdContratoAsociado != 0)
                vContrato.IdContratoAsociado = vContratoAsociado.IdContratoAsociado; // TipoContratoAsociado



            vContrato.ConvenioMarco = chkConvenioMarco.Checked;
            vContrato.ConvenioAdhesion = chkContratoConvenioAd.Checked;

            if (!IdContratoAsociado.Equals(string.Empty))
                vContrato.FK_IdContrato = Convert.ToInt32(IdContratoAsociado); // no lo hace la lupa

            if (!ddlCategoriaContrato.SelectedValue.Equals("-1"))
                vContrato.IdCategoriaContrato = Convert.ToInt32(ddlCategoriaContrato.SelectedValue);

            if (!ddlTipoContratoConvenio.SelectedValue.Equals("-1"))
                vContrato.IdTipoContrato = Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue);

            if (!ddlModalidadAcademica.SelectedValue.Equals("-1"))
                vContrato.IdModalidadAcademica = ddlModalidadAcademica.SelectedValue;

            if (!ddlNombreProfesion.SelectedValue.Equals("-1"))
                vContrato.IdProfesion = ddlNombreProfesion.SelectedValue;

            if (!ddlModalidadSeleccion.SelectedValue.Equals("-1"))
                vContrato.IdModalidadSeleccion = Convert.ToInt32(ddlModalidadSeleccion.SelectedValue);

            //if (!ddlCodigoSECOP.SelectedValue.Equals("-1"))
            //    vContrato.IdCodigoSECOP = Convert.ToInt32(ddlCodigoSECOP.SelectedValue);

            if (!hfIdNumeroProceso.Value.Equals(string.Empty))
                vContrato.IdNumeroProceso = Convert.ToInt32(hfIdNumeroProceso.Value); // no lo hace la lupa

            if (! string.IsNullOrEmpty(hfCodRegionalContConv.Value))
            {
                var idRegionalContrato = vSIAService.ConsultarRegionals(hfCodRegionalContConv.Value, null);
                vContrato.IdRegionalContrato = idRegionalContrato.First().IdRegional;
            }

            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (!caFechaAdjudicaProceso.Date.ToShortDateString().Equals(string.Empty) && !caFechaAdjudicaProceso.Date.ToShortDateString().Equals("01/01/1900"))
            if (!caFechaAdjudicaProceso.Date.ToShortDateString().Equals(string.Empty) && Convert.ToDateTime(caFechaAdjudicaProceso.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
                vContrato.FechaAdjudicacionProceso = caFechaAdjudicaProceso.Date;

            vContrato.ActaDeInicio = null;
            if (chkSiReqActa.Checked)
                vContrato.ActaDeInicio = true;
            if (chkNoReqActa.Checked)
                vContrato.ActaDeInicio = false;

            vContrato.ManejaAporte = null;
            if (chkSiManejaAportes.Checked)
                vContrato.ManejaAporte = true;
            if (chkNoManejaAportes.Checked)
                vContrato.ManejaAporte = false;

            vContrato.ManejaRecurso = null;
            if (chkSiManejaRecursos.Checked)
                vContrato.ManejaRecurso = true;
            if (chkNoManejaRecursos.Checked)
                vContrato.ManejaRecurso = false;

            if (!ddlRegimenContratacion.SelectedValue.Equals("-1"))
                vContrato.IdRegimenContratacion = Convert.ToInt32(ddlRegimenContratacion.SelectedValue);

            if(!rblRequiereGarantia.SelectedValue.Equals(string.Empty))
               vContrato.RequiereGarantia = Convert.ToBoolean(rblRequiereGarantia.SelectedValue);

            if (!rblAportesEspecie.SelectedValue.Equals(string.Empty))
                vContrato.AportesEspecie = Convert.ToBoolean(rblAportesEspecie.SelectedValue);

            if(!string.IsNullOrEmpty(txtPorcentajeAnticipo.Text))
               vContrato.PorcentajeAnticipo = Convert.ToInt32(txtPorcentajeAnticipo.Text);

            if (!string.IsNullOrEmpty(txtNumContMigrado.Text))
                vContrato.NumeroContratoMigrado = txtNumContMigrado.Text;

            //vContrato.PorcentajeAnticipo = Convert.ToInt32(txtPorcentajeAnticipo.Text);
            //vContrato.IdEmpleadoSolicitante // lo hace la lupa
            //vContrato.IdEmpleadoOrdenadorGasto // lo hace la lupa
            #endregion

            #region PlanCompras

            #region ConsecutivoPlanCompras
            if (!DdlConsecutivoPlanCompras.SelectedValue.Equals("-1"))
                vContrato.ConsecutivoPlanComprasAsociado = Convert.ToInt32(DdlConsecutivoPlanCompras.SelectedValue);
            #endregion

            if (!string.IsNullOrEmpty(TotalProductos))
                vContrato.ValorInicialContrato = decimal.Parse(TotalProductos.Replace("$",string.Empty));

            #endregion

            #region VigenciaValorContrato
            #region Objeto
            if (!txtObjeto.Text.Equals(string.Empty))
                vContrato.ObjetoContrato = txtObjeto.Text;//HttpUtility.HtmlDecode(txtObjeto.Text);
            #endregion

            #region Alcance
            if (!txtAlcance.Text.Equals(string.Empty))
                vContrato.AlcanceObjetoContrato = txtAlcance.Text;//HttpUtility.HtmlDecode(txtAlcance.Text);
            #endregion

            #region ValorInicial
            if (!txtValorInicialContConv.Text.Equals(string.Empty) && ! string.IsNullOrEmpty(hfTotalProductos.Value))
            {
                //string valorInicial = txtValorInicialContConv.Text.Replace(",", "");
                //string valorInicial = txtValorInicialContConv.Text; //.Replace("$", "");    //Modificacion de guardar el valor inicial se calcula en la funcion  ActualizarValorInicialContrato
                //vContrato.ValorInicialContrato = Convert.ToDecimal(valorInicial);
                Decimal vValorProductos = decimal.Parse(hfTotalProductos.Value, NumberStyles.Currency);

                //vContrato.ValorInicialContrato = vContratoService.ActualizarValorInicialContrato(vContrato.IdContrato, vValorProductos);    
                vContrato.ValorInicialContrato = vValorProductos;
            }
            #endregion

            #region FechaInicioEjec
            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (!caFechaInicioEjecucion.Date.ToShortDateString().Equals(string.Empty) && !caFechaInicioEjecucion.Date.ToShortDateString().Equals("01/01/1900"))
            if (!caFechaInicioEjecucion.Date.ToShortDateString().Equals(string.Empty) && Convert.ToDateTime(caFechaInicioEjecucion.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
                vContrato.FechaInicioEjecucion = caFechaInicioEjecucion.Date;
            #endregion

            #region FechaFinalCalculada

            vContrato.EsFechaFinalCalculada = chkCalculaFecha.Checked;

            if (chkCalculaFecha.Checked)
            {
                vContrato.DiasFechaFinalCalculada =  !string.IsNullOrEmpty(txtDiasPiEj.Text) ? int.Parse(txtDiasPiEj.Text) : 0;
                vContrato.MesesFechaFinalCalculada = !string.IsNullOrEmpty(txtMesesPiEj.Text) ? int.Parse(txtMesesPiEj.Text) : 0;
            }

            #endregion  

            #region FechaFinalizacionInicial
            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (!caFechaFinalizacionInicial.Date.ToShortDateString().Equals(string.Empty) && !caFechaFinalizacionInicial.Date.ToShortDateString().Equals("01/01/1900"))
            if (!caFechaFinalizacionInicial.Date.ToShortDateString().Equals(string.Empty) && Convert.ToDateTime(caFechaFinalizacionInicial.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
                vContrato.FechaFinalizacionIniciaContrato = caFechaFinalizacionInicial.Date;
            #endregion

            #region FechaFinalTerminacion
            if (!txtFechaFinalTerminacion.Text.Equals(string.Empty))
                vContrato.FechaFinalTerminacionContrato = Convert.ToDateTime(txtFechaFinalTerminacion.Text);
            #endregion

            #region ManejaVigenciasFuturas
            vContrato.ManejaVigenciaFuturas = null;
            if (chkSiManejaVigFuturas.Checked)
                vContrato.ManejaVigenciaFuturas = true;
            if (chkNoManejaVigFuturas.Checked)
                vContrato.ManejaVigenciaFuturas = false;
            #endregion

            #region VigenciaFiscalInicial
            if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1"))
                vContrato.IdVigenciaInicial = Convert.ToInt32(ddlVigenciaFiscalIni.SelectedValue);
            #endregion

            #region VigenciaFiscalFinal
            if (!ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
                vContrato.IdVigenciaFinal = Convert.ToInt32(ddlVigenciaFiscalFin.SelectedValue);
            #endregion

            #region FormaPago
            if (!ddlFormaPago.SelectedValue.Equals("-1"))
                vContrato.IdFormaPago = Convert.ToInt32(ddlFormaPago.SelectedValue);
            #endregion

            #region Tipo Forma de Pago
            if (!ddlTipoFormaPago.SelectedValue.Equals("-1"))
                vContrato.IdTipoPago = Convert.ToInt32(ddlTipoFormaPago.SelectedValue);
            #endregion

            #endregion

            #region LugarEjecucion

            #region DatosAdicionales

            if (!txtDatosAdicionalesLugarEjecucion.Text.Equals(string.Empty))
                vContrato.DatosAdicionaleslugarEjecucion = txtDatosAdicionalesLugarEjecucion.Text;

            #endregion

            #endregion

            #region Aportes ICBF
            


            #endregion

            #region Supervisores
            if (chkContratoConvenioAd.Checked)
            {
                foreach(GridViewRow gvr in gvSupervInterv.Rows)
                {
                    TextBox txtFechaInicio = ((TextBox)gvr.FindControl("txtFechaInicio"));
                    if (txtFechaInicio.Visible)
                    {
                        if (DateTime.Parse(txtFechaInicio.Text) < caFechaInicioEjecucion.Date)
                        {
                            throw new UserInterfaceException("La Fecha de Inicio de Supervisor y/o Interventor debe ser mayor o igual a la Fecha de Ejecución de Contrato/Convenio");
                        }

                        SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
                        vSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(gvSupervInterv.DataKeys[gvr.RowIndex]["IDSupervisorIntervContrato"]);
                        vSupervisorInterContrato.FechaInicio = Convert.ToDateTime(((TextBox)gvr.FindControl("txtFechaInicio")).Text);
                        vSupervisorInterContrato.UsuarioModifica = GetSessionUser().Usuario;
                        vContratoService.ActualizarFechaInicioSupervisorInterContrato(vSupervisorInterContrato);
                    }
                }
            }
            
           if(hfContratoSV.Value == "true")
            {
                vContrato.EsContSinValor = true;
            }
            else
            {
                vContrato.EsContSinValor = false;
            }
            
            #endregion

            //if (Request.QueryString["oP"] == "E")
            //{
            //    vContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
            //}
            //else
            //{
            //    vContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
            //}
            if (vContrato.ManejaVigenciaFuturas == true )
            {
                var isValid = ValidarVigencias();

                if (!isValid)
                    return;
            }
            vContrato.UsuarioModifica = GetSessionUser().NombreUsuario;

            if (vContrato.PorcentajeAnticipo > 50)
            {
                toolBar.MostrarMensajeError("El Valor de la forma de pago no debe ser mayor al 50% del valor del contrato");
            }
            else
            {
                
                InformacionAudioria(vContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ContratoActualizacion(vContrato);

                

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado >= 1)
                {
                    SetSessionParameter("Contrato.EsUsuarioAutenticado", true);
                    SetSessionParameter("Contrato.EstadoContrato", IdEstadoContrato);
                    SetSessionParameter("Contrato.IdContrato", vContrato.IdContrato);
                    RemoveSessionParameter("Contrato.ContratosAPP");
                    SetSessionParameter("Contrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo es inválido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);

            toolBar.SetSaveConfirmation("return PreGuardado()");
            toolBar.SetAprobarConfirmation("return Aprobacion()");

            toolBar.EstablecerTitulos("Contrato", SolutionPage.Add.ToString());

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    { 
        try
        {
            bool aprueba = ValidateAccessEdit(toolBar, PageNameAprobar, SolutionPage.Edit);

           if (aprueba)
               toolBar.MostrarBotonAprobar(true);
            else
               toolBar.MostrarBotonAprobar(false);

            TotalProductosCDP = "0";

            #region CargaEncabezado
            
                dvUsuarioCreacion.Style.Add("visibility", ""); 

            txtNombreUsuario.Text = GetSessionUser().NombreUsuario;

            #region Regional
            
            Usuario vUsuario = vSIAService.ConsultarUsuarioPorproviderKey(GetSessionUser().Providerkey);
            int? idRegional = vUsuario.IdRegional;
            if (idRegional != null)
            {
                txtRegionalUsuario.Text = ObtenerRegionalUsuario(idRegional);
                IdRegional = Convert.ToString(idRegional);
            }
            else
            {
                int tipoUsuario = vUsuario.IdTipoUsuario;
                if (tipoUsuario == 1) 
                {
                    txtRegionalUsuario.Text = "Todas las Regionales";
                    //txtRegionalUsuario.Visible = false;
                    //chkContratoConvenioAd.Enabled = false;
                    //chkConvenioMarco.Enabled = false;
                    //ddlCategoriaContrato.Enabled = false;
                }
                else
                {
                    toolBar.MostrarMensajeError("El usuario no tiene registrada una regional. Debe asignarle una regional para poder continuar");
                    rfvRegionalUsuario.Enabled = true;
                    chkContratoConvenioAd.Enabled = false;
                    chkConvenioMarco.Enabled = false;
                }
            }
            #endregion
            txtFechaRegistro.Text = DateTime.Today.ToString().Substring(0, 10);

            #endregion

            #region Radio Button 
            rblRequiereGarantia.Items.Insert(0, new ListItem("No", "False"));
            rblRequiereGarantia.Items.Insert(0, new ListItem("Si", "True"));

            ListItem itemNo = new ListItem("No", "False");
            itemNo.Attributes.Add("onclik", "javascript:ValidarAportesEspecie()");
            rblAportesEspecie.Items.Insert(0, itemNo);
            rblAportesEspecie.Items.Insert(0, new ListItem("Si", "True"));
          
            #endregion

            #region CargarIdentificadores
            List<EstadoContrato> estadoContrato = vContratoService.ConsultarEstadoContrato(null, CodEstadoEnRegistro, null, true);
            if (estadoContrato.Count > 0)
                IdEstadoContrato = estadoContrato[0].IdEstadoContrato.ToString();

            CategoriaContrato vCategoriaContrato = new CategoriaContrato();
            vCategoriaContrato.CodigoCategoriaContrato = CodCategoriaContrato;
            IdCategoriaContrato = vContratoService.IdentificadorCodigoCategoriaContrato(vCategoriaContrato).IdCategoriaContrato.ToString();
            vCategoriaContrato = new CategoriaContrato();
            vCategoriaContrato.CodigoCategoriaContrato = CodCategoriaConvenio;
            IdCategoriaConvenio = vContratoService.IdentificadorCodigoCategoriaContrato(vCategoriaContrato).IdCategoriaContrato.ToString();

            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato.CodigoTipoContrato = CotTipoContConvPrestServApoyoGestion;
            IdTipoContConvPrestServApoyoGestion = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
            vTipoContrato = new TipoContrato();
            vTipoContrato.CodigoTipoContrato = CodTipoContConvPrestServProfesionales;
            IdTipoContConvPrestServProfesionales = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
            vTipoContrato = new TipoContrato();
            vTipoContrato.CodigoTipoContrato = CodMarcoInteradministrativo;
            IdMarcoInteradministrativo = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
            
            //vTipoContrato = new TipoContrato();
            //vTipoContrato.CodigoTipoContrato = CodTipoContAporte;
            //IdTipoContAporte = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();

            ModalidadSeleccion vModalidad = new ModalidadSeleccion();
            vModalidad.CodigoModalidad = CodContratacionDirecta;
            IdContratacionDirecta = vContratoService.IdentificadorCodigoModalidadSeleccion(vModalidad).IdModalidad.ToString();
            vModalidad = new ModalidadSeleccion();
            vModalidad.CodigoModalidad = IdContratacionDirectaAporte;
            IdContratacionDirectaAporte = vContratoService.IdentificadorCodigoModalidadSeleccion(vModalidad).IdModalidad.ToString();
            #endregion

            #region LlenarComboCategoriaContrato
            ddlCategoriaContrato.Items.Clear();
            List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, true);
            foreach (CategoriaContrato tD in vLContratos)
            {
                ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
            }
            ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #region LlenarComboModalidadAcademicaKACTUS
            ddlModalidadAcademica.Items.Clear();
            List<ModalidadAcademicaKactus> vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, null, null);
            foreach (ModalidadAcademicaKactus tD in vLModalidadesAcademicas)
            {
                ddlModalidadAcademica.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));
            }
            ddlModalidadAcademica.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #region LlenarComboModalidadSeleccion
            CargarModalidadSeleccion();
            #endregion

            #region LlenarComboCodigoSECOP
            //ddlCodigoSECOP.Items.Clear();
            //List<CodigosSECOP> vLCodigoSECOP = vContratoService.ConsultarVariosCodigosSECOP(true, null);
            //foreach (CodigosSECOP tD in vLCodigoSECOP)
            //{
            //    ddlCodigoSECOP.Items.Add(new ListItem(tD.CodigoSECOP, tD.IdCodigoSECOP.ToString()));
            //}
            //ddlCodigoSECOP.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            #region LlenarComboRegimenContratacion
            ddlRegimenContratacion.Items.Clear();
            List<RegimenContratacion> vLRegimensContratacion = vContratoService.ConsultarRegimenContratacions(null, null, true);
            foreach (RegimenContratacion tD in vLRegimensContratacion)
            {
                ddlRegimenContratacion.Items.Add(new ListItem(tD.NombreRegimenContratacion, tD.IdRegimenContratacion.ToString()));
            }
            ddlRegimenContratacion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            #endregion

            DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #region LlenarCombosVigencias
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalIni, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalFin, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            #endregion

            #region LlenarComboFormaPago
            ddlFormaPago.Items.Clear();
            List<TipoFormaPago> vLFormasPago = vContratoService.ConsultarTipoMedioPagos(null, null, null,true);
            foreach (TipoFormaPago tD in vLFormasPago)
            {
                ddlFormaPago.Items.Add(new ListItem(tD.NombreTipoFormaPago, Convert.ToString(tD.IdTipoFormaPago)));
            }
            ddlFormaPago.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            ddlTipoFormaPago.Items.Clear();
            List<TipoPago> vLTipoPago = vContratoService.ConsultarVariosTipoPago(true,null);
            foreach (TipoPago tP in vLTipoPago)
            {
                ddlTipoFormaPago.Items.Add(new ListItem(tP.NombreTipoPago, Convert.ToString(tP.IdTipoPago)));
            }
            ddlTipoFormaPago.Items.Insert(0, new ListItem("Seleccionar", "-1"));

            #endregion            
            AcordeonActivo = "1";


            caFechaInicioEjecucion.ToolTip = "Favor registre la posible fecha de suscripción del contrato,  ya que esta fecha es dinámica y una vez se aprueben las garantías o se registe la fecha de firma del acta de inicio se actualizara con la fecha correspondiente";
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    #region Encabezado

    private string ObtenerRegionalUsuario(int? IdRegional)
    {
        Regional usuarioRegional = vSIAService.ConsultarRegional(IdRegional);
        return usuarioRegional.CodigoRegional + " - " + usuarioRegional.NombreRegional; // Pagina 6 Descripcion adicional Regional del Usuario
    }

    private void SeleccionaRegional(string idRegionalSeleccionada)
    {
        if (idRegionalSeleccionada.Equals("-1"))
        {
            chkContratoConvenioAd.Enabled = false;
            chkConvenioMarco.Enabled = false;
            HabilitaCategoriaContrato(false);
            SeleccionadoContratoAsociado(string.Empty);
        }
        else
        {
            IdRegional = idRegionalSeleccionada;
            chkContratoConvenioAd.Enabled = true;
            chkConvenioMarco.Enabled = true;
            HabilitaCategoriaContrato(true);
        }
    }

    #endregion

    private void SeleccionarPanel(string commandNamePanel)
    {
        switch (commandNamePanel)
        {
            case "DatosGeneralContrato":
                break;
            case "PlanComprasProductos":
                break;
            case "VigenciaValorContrato":
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            NoPrecargarInformacion = 1;
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
            //Contrato vContrato = new Contrato();
            vContrato = vContratoService.ContratoObtener(vIdContrato);
            txtFechaRegistro.Text = vContrato.FechaCrea.ToString("dd/MM/yyyy");//.Substring(0, 10);
            IdContrato = vContrato.IdContrato.ToString();
            IdRegional = vContrato.IdRegionalContrato.ToString();
            rblRequiereGarantia.SelectedValue = vContrato.RequiereGarantia.ToString();
            rblAportesEspecie.SelectedValue = vContrato.AportesEspecie.ToString();
            if (!string.IsNullOrEmpty(vContrato.NumeroContratoMigrado))
                txtNumContMigrado.Text = vContrato.NumeroContratoMigrado;
            else
            {
                txtNumContMigrado.Visible = false;
                lblContratoMigrado.Visible = false;
            }

            txtIdContrato.Text = IdContrato;
            //ddlRegionalUsuarioContrato.SelectedValue = vContrato.IdRegionalContrato.ToString();
            txtPorcentajeAnticipo.Text = vContrato.PorcentajeAnticipo.ToString();
            string[] lRoles = Roles.GetRolesForUser(GetSessionUser().NombreUsuario);

            if (!vSeguridadFAC.ConsultarRolesNombreEsAdmin(lRoles[0], true) && 
                !vContrato.UsuarioCrea.ToUpper().Equals(GetSessionUser().NombreUsuario.ToUpper()))
            {
                string mensajeUsuario = "El usuario no puede realizar la edición de este contrato";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "myScript",
                "<script>javascript: alert('" + mensajeUsuario + "');window.location ='Detail.aspx' ;</script>");
            }
            else
            { 
                string CodContratoAsociado = string.Empty;
                if (vContrato.IdContratoAsociado != null)
                {
                    TipoContratoAsociado vTipoContratoAsociado = new TipoContratoAsociado();
                    vTipoContratoAsociado.IdContratoAsociado = (int)vContrato.IdContratoAsociado;
                    vTipoContratoAsociado = vContratoService.IdentificadorCodigoTipoContratoAsociado(vTipoContratoAsociado);
                    CodContratoAsociado = vTipoContratoAsociado.CodContratoAsociado;
                    chkConvenioMarco.Checked = vContrato.ConvenioMarco;
                    chkContratoConvenioAd.Checked = vContrato.ConvenioAdhesion;
                    SeleccionadoContratoAsociado(CodContratoAsociado);
                    NumeroContrato();
                    CategoriaContrato();
                }
                else
                {
                    SeleccionadoContratoAsociado(CodContratoAsociado);
                    CategoriaContrato();
                }

                CargarCodigosSECOP();

                ObtenerAuditoria(PageName, IdContrato);
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
            }
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }


    #region FuncionesCarga

    public void NumeroContrato()
    {
        #region NumeroContrato
        if (vContrato.FK_IdContrato != null)
        {
            IdContratoAsociado = Convert.ToString(vContrato.FK_IdContrato);
        }
        SeleccionaNumeroConvenioContrato(IdContratoAsociado); // Aqui se debe cargar toda la informacion del contrato asociado en hiddens
        #endregion
    }

    public void CategoriaContrato()
    {
        #region Categoria
        string IdCategoriaContrato = "-1";
        if (vContrato.IdCategoriaContrato != null)
        {
            IdCategoriaContrato = vContrato.IdCategoriaContrato.ToString();
            SeleccionadoCategoriaContrato(IdCategoriaContrato, true);
            ddlCategoriaContrato.SelectedValue = IdCategoriaContrato;
        }
        TipoContrato();
        #endregion
    }

    public void TipoContrato()
    {
        #region TipoContrato
        string IdTipoContrato = "-1";
        if (vContrato.IdTipoContrato != null)
        {
            IdTipoContrato = vContrato.IdTipoContrato.ToString();
            SeleccionadoTipoContratoConvenio(IdCategoriaContrato, IdTipoContrato);
            ddlTipoContratoConvenio.SelectedValue = IdTipoContrato;            
            
        }
        ModalidadAcademica();
        ModalidadSeleccion();
        #endregion
    }

    public void ModalidadAcademica()
    {
        #region ModalidadAcademica
        string IdModalidadAcademica = "-1";
        if (vContrato.IdModalidadAcademica != null)
        {
            IdModalidadAcademica = vContrato.IdModalidadAcademica;
            SeleccionarModalidadAcademica(IdModalidadAcademica);
            ddlModalidadAcademica.SelectedValue = IdModalidadAcademica;    
        }
        Profesion();
        #endregion
    }

    public void Profesion()
    {
        #region Profesion
        string IdProfesion = "-1";
        if (vContrato.IdProfesion != null)
            IdProfesion = vContrato.IdProfesion;
        SeleccionaProfesion(IdProfesion);
        ddlNombreProfesion.SelectedValue = IdProfesion.Trim();
        #endregion
    }

    public void ModalidadSeleccion()
    {
        #region ModalidadSeleccion
        string IdModalidadSeleccion = "-1";
        if (vContrato.IdModalidadSeleccion != null)
        {
            IdModalidadSeleccion = vContrato.IdModalidadSeleccion.ToString();
            SeleccionarModalidadSeleccion(IdModalidadSeleccion);
            ddlModalidadSeleccion.SelectedValue = IdModalidadSeleccion;
            
        }
        NumeroProceso();
        ActaInicio();
        CodigoSECOP();
        #endregion
    }

    public void CodigoSECOP()
    {
        //string IdCodigoSECOP = "-1";
        //if (vContrato.IdCodigoSECOP != null)
        //{
        //    ddlCodigoSECOP.SelectedValue = vContrato.IdCodigoSECOP.ToString();
        //}
    }

    public void NumeroProceso()
    {
        #region NumeroProceso
        if (vContrato.IdNumeroProceso != null)
        {
            hfIdNumeroProceso.Value = Convert.ToString(vContrato.IdNumeroProceso);
            hfNumeroProceso.Value = Convert.ToString(vContrato.NumeroProceso); 
            txtNumeroProceso.Text = Convert.ToString(vContrato.NumeroProceso);
            HabilitarNumeroProceso(true);
            
        }
        FechaAdjudicacionProceso();
        SeleccionarNumeroProceso(hfNumeroProceso.Value);
        #endregion
    }

    public void FechaAdjudicacionProceso()
    {
        #region FechaAdjudicacionProceso
        string FechaAdjudicacionProceso = string.Empty;
        if (vContrato.FechaAdjudicacionProceso != null)
        {
            FechaAdjudicacionProceso = vContrato.FechaAdjudicacionProceso.ToString();
            caFechaAdjudicaProceso.Date = (DateTime)vContrato.FechaAdjudicacionProceso;
            hfFechaAdjudicacionProceso.Value = vContrato.FechaAdjudicacionProceso.ToString();
            
        }
        SeleccionaFechaAdjudicacion(FechaAdjudicacionProceso);
        #endregion
    }

    public void ActaInicio()
    {
        #region ActaInicio
        // (SE REPITE)
        //RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Se comenta para que no se repita el llamado
        if (vContrato.ActaDeInicio != null)
        {
            HabilitarManejaAportes(true);
            RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
            if (vContrato.ActaDeInicio == true)
            {
                chkSiReqActa.Checked = true;
                chkNoReqActa.Checked = false;
            }
            else
            {
                chkSiReqActa.Checked = false;
                chkNoReqActa.Checked = true;
            }
        }
        else
        {
            HabilitarManejaAportes(false);
        }
        
        ManejaAportes();
        #endregion
    }

    public void ManejaAportes()
    {
        #region ManejaAportes
        //RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue);
        if (vContrato.ManejaAporte != null)
        {
            HabilitarManejaRecursos(true);
            RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
            if (vContrato.ManejaAporte == true)
            {
                chkSiManejaAportes.Checked = true;
                chkNoManejaAportes.Checked = false;
            }
            else
            {
                chkSiManejaAportes.Checked = false;
                chkNoManejaAportes.Checked = true;
            }
        }
        else
        {
            HabilitarManejaRecursos(false);
        }
        
        ManejaRecursos();
        #endregion
    }

    public void ManejaRecursos()
    {
        #region ManejaRecursos
        //RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue);
        if (vContrato.ManejaRecurso != null)
        {
            HabilitarRegimenContratacion(true);
            if (vContrato.ManejaRecurso == true)
            {
                chkSiManejaRecursos.Checked = true;
                chkNoManejaRecursos.Checked = false;
            }
            else
            {
                chkSiManejaRecursos.Checked = false;
                chkNoManejaRecursos.Checked = true;
            }
        }
        else
        {
            HabilitarRegimenContratacion(false);
        }

        RegimenContratacion();
        #endregion
    }

    public void RegimenContratacion()
    {
        #region RegimenContratacion
        string IdRegimenContratacion = "-1";
        if (vContrato.IdRegimenContratacion != null)
        {
            IdRegimenContratacion = Convert.ToString(vContrato.IdRegimenContratacion);
            SeleccionaRegimenContratacion(IdRegimenContratacion);
            ddlRegimenContratacion.SelectedValue = IdRegimenContratacion;
        }
        Solicitante();
        #endregion
    }

    public void Solicitante()
    {
        #region Solicitante
        if (vContrato.IdEmpleadoSolicitante != null)
        {
            Empleado vEmpleadoSol = new Empleado();
            vEmpleadoSol = vContratoService.ConsultarEmpleado(vContrato.IdEmpleadoSolicitante.ToString(), vContrato.IdRegionalEmpSol.ToString(),
                vContrato.IdDependenciaEmpSol.ToString(), vContrato.IdCargoEmpSol.ToString());
            hfNombreSolicitante.Value = vEmpleadoSol.PrimerNombre + " " + vEmpleadoSol.SegundoNombre + " " + vEmpleadoSol.PrimerApellido + " " + vEmpleadoSol.SegundoApellido;
            hfRegionalContConv.Value = vEmpleadoSol.Regional;
            hfDependenciaSolicitante.Value = vEmpleadoSol.Dependencia;
            hfCargoSolicitante.Value = vEmpleadoSol.Cargo;
            hfCodRegionalContConv.Value = vEmpleadoSol.IdRegional;

            txtNombreSolicitante.Text = hfNombreSolicitante.Value;
            txtRegionalContratoConvenio.Text = hfRegionalContConv.Value;
            txtDependenciaSolicitante.Text = hfDependenciaSolicitante.Value;
            txtCargoSolicitante.Text = hfCargoSolicitante.Value;
            SeleccionarSolicitante(txtNombreSolicitante.Text);
        }
        OrdenadorGasto();
        #endregion
    }

    public void OrdenadorGasto()
    {
        #region OrdenadoGasto
        if (vContrato.IdEmpleadoOrdenadorGasto != null)
        {
            Empleado vEmpleadoOrdG = new Empleado();
            vEmpleadoOrdG = vContratoService.ConsultarEmpleado(vContrato.IdEmpleadoOrdenadorGasto.ToString(), vContrato.IdRegionalEmpOrdG.ToString(),
                vContrato.IdDependenciaEmpOrdG.ToString(), vContrato.IdCargoEmpOrdG.ToString());

            hfNombreOrdenadorGasto.Value = vEmpleadoOrdG.PrimerNombre + " " + vEmpleadoOrdG.SegundoNombre + " " + vEmpleadoOrdG.PrimerApellido + " " + vEmpleadoOrdG.SegundoApellido;
            hfTipoIdentOrdenadorGasto.Value = vEmpleadoOrdG.TipoIdentificacion;
            hfNumeroIdentOrdenadoGasto.Value = vEmpleadoOrdG.NumeroIdentificacion;
            hfCargoOrdenadoGasto.Value = vEmpleadoOrdG.Cargo;

            txtNombreOrdenadorGasto.Text = hfNombreOrdenadorGasto.Value;
            txtTipoIdentOrdenadorGasto.Text = hfTipoIdentOrdenadorGasto.Value;
            txtNumeroIdentOrdenadoGasto.Text = hfNumeroIdentOrdenadoGasto.Value;
            txtCargoOrdenadoGasto.Text = hfCargoOrdenadoGasto.Value;

            SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value);

            //if (chkSiManejaRecursos.Checked)
            //{
                SeleccionarPlanCompras("1");
                ConsecutivoPlanCompras();
                AcordeonActivo = "2";
           // }
        }
        ObjetoContrato();
        #endregion
    }

    public void ConsecutivoPlanCompras()
    {
        #region ConsecutivoPlanCompras

        string IdConsecutivoPlanCompras = "-1";
        if (vContrato.ConsecutivoPlanComprasAsociado != null)
        {
            IdConsecutivoPlanCompras = vContrato.ConsecutivoPlanComprasAsociado.ToString();
            //SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);
        }
        DdlConsecutivoPlanCompras.SelectedValue = IdConsecutivoPlanCompras;
        SeleccionaConsecutivoPlanDeCompras(IdConsecutivoPlanCompras);

        #endregion
    }

    public void ObjetoContrato()
    {
        if (vContrato.ObjetoContrato != null)
        {
            AcordeonActivo = "3";
            txtObjeto.Text = vContrato.ObjetoContrato;

            if (chkContratoConvenioAd.Checked) 
            {
                txtObjeto.Enabled = false;
            }
            else
            {
                if (chkSiManejaRecursos.Checked)
                {
                    if (gvConsecutivos.Rows.Count > 0)
                        txtObjeto.Enabled = false;
                    else
                        txtObjeto.Enabled = false;
                }
                else
                    txtObjeto.Enabled = true;
            }

            IngresaObjetoContratoConvenio(txtObjeto.Text);
        }

        AlcanceObjetoContrato();
    }

    public void AlcanceObjetoContrato()
    {
        if (vContrato.AlcanceObjetoContrato != null)
        {
            txtAlcance.Text = vContrato.AlcanceObjetoContrato;
            IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
        }
        ValorInicialContrato();
    }

    public void ValorInicialContrato()
    {
        if (vContrato.ValorInicialContrato != null)
        {
            txtValorInicialContConv.Text = Convert.ToDecimal(vContrato.ValorInicialContrato).ToString("#,###0.00##;($ #,###0.00##)");
            ValorInicial = txtValorInicialContConv.Text;
            IngresaValorInicialContConv(txtValorInicialContConv.Text);
        }

        FechaInicioEjecucion();

    }

    public void FechaInicioEjecucion()
    {
        string FechaInicioEjecucion = string.Empty;
        if (vContrato.FechaInicioEjecucion != null)
        {
            FechaInicioEjecucion = vContrato.FechaInicioEjecucion.ToString();
            caFechaInicioEjecucion.Date = (DateTime)vContrato.FechaInicioEjecucion;
            FechaInicio = vContrato.FechaInicioEjecucion.ToString().Substring(0, 10);
        }
        EsFechaFinalizacionCalculada();
        SeleccionaFechaInicioEjecucion(FechaInicioEjecucion);
    }

    public void EsFechaFinalizacionCalculada()
    {
        if (vContrato.EsFechaFinalCalculada != null)
        {
            chkCalculaFecha.Checked = vContrato.EsFechaFinalCalculada.Value ? true : false;
            chkCalculaFecha.Enabled = true;

            if (chkCalculaFecha.Checked)
            {
                txtDiasPiEj.Text = vContrato.DiasFechaFinalCalculada.HasValue ? vContrato.DiasFechaFinalCalculada.Value.ToString() : "0";
                txtMesesPiEj.Text = vContrato.MesesFechaFinalCalculada.HasValue ? vContrato.MesesFechaFinalCalculada.Value.ToString() : "0";
                ImgBtnFechaFinal.Visible = true;
            }
            else
            {
                ImgBtnFechaFinal.Visible = false;
            }
        }
        else
        {
            chkCalculaFecha.Checked = false;
            chkCalculaFecha.Enabled = false;
            ImgBtnFechaFinal.Visible = false;
        }
        
        FechaFinalizacionInicial();
    }

    public void FechaFinalizacionInicial()
    {
        string FechaFinalizacionInicial = string.Empty;
        if (vContrato.FechaFinalizacionIniciaContrato != null)
        {
            FechaFinalizacionInicial = vContrato.FechaFinalizacionIniciaContrato.ToString();
            caFechaFinalizacionInicial.Date = (DateTime)vContrato.FechaFinalizacionIniciaContrato;
            FechaFinalizacion = vContrato.FechaFinalizacionIniciaContrato.ToString().Substring(0, 10);
            SeleccionaFechaFinalizacionInicialContConv(FechaFinalizacionInicial);
        }
        FechaFinalTerminacion();

    }

    public void FechaFinalTerminacion()
    {
        if (vContrato.FechaFinalTerminacionContrato != null)
        {
            txtFechaFinalTerminacion.Text = vContrato.FechaFinalTerminacionContrato.ToString().Substring(0, 10);
        }
        else if (vContrato.FechaFinalizacionIniciaContrato != null)
        {
            txtFechaFinalTerminacion.Text = vContrato.FechaFinalizacionIniciaContrato.ToString().Substring(0, 10);
        }

        ManejaVigenciasFuturas();
    }

    public void ManejaVigenciasFuturas()
    {
        if (vContrato.ManejaVigenciaFuturas != null)
        {
            HabilitarVigenciaFiscal(true);
            if (vContrato.ManejaVigenciaFuturas == true)
            {
                chkSiManejaVigFuturas.Checked = true;
                chkNoManejaVigFuturas.Checked = false;
                //HabilitarSeccionVigenciaFuturas(true);
            }
            else
            {
                chkSiManejaVigFuturas.Checked = false;
                chkNoManejaVigFuturas.Checked = true;
                //HabilitarSeccionVigenciaFuturas(false);
            }    
        }
        else
        {
            HabilitarVigenciaFiscal(false);
        }
        VigenciaFiscal();
    }

    public void VigenciaFiscal()
    {
        #region VigenciaFiscalInicial
        string IdVigenciaFiscalInicial = "-1";
        if (vContrato.IdVigenciaInicial != null)
            IdVigenciaFiscalInicial = Convert.ToString(vContrato.IdVigenciaInicial);
        ddlVigenciaFiscalIni.SelectedValue = IdVigenciaFiscalInicial;
        SeleccionaVigenciaFiscalInicial(IdVigenciaFiscalInicial);

        //string VigenciaFiscalInicial = string.Empty;
        //if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1"))
        //{
        //    VigenciaFiscalInicial = ddlVigenciaFiscalIni.SelectedItem.Text;
        //}
        //txtVigenciaFiscalIni.Text = VigenciaFiscalInicial;
        #endregion

        #region VigenciaFiscalFinal
        string IdVigenciaFiscalFinal = "-1";
        if (vContrato.IdVigenciaFinal != null)
            IdVigenciaFiscalFinal = Convert.ToString(vContrato.IdVigenciaFinal);
        ddlVigenciaFiscalFin.SelectedValue = IdVigenciaFiscalFinal;
        SeleccionaVigenciaFiscalFinal(IdVigenciaFiscalFinal);

        //string VigenciaFiscalFinal = string.Empty;
        //if (!ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
        //{
        //    VigenciaFiscalFinal = ddlVigenciaFiscalFin.SelectedItem.Text;
        //}
        //txtVigenciaFiscalFin.Text = VigenciaFiscalFinal;
        #endregion

        if (vContrato.IdVigenciaInicial != null && vContrato.IdVigenciaFinal != null)
        {
            
        }
        else
        {
            HabilitarFormaPago(false);
        }
        
        FormaPago();
        TipoFormaPago();
    }

    public void FormaPago()
    {
        string IdFormaPago = "-1";
        if (vContrato.IdFormaPago != null)
        { 
            IdFormaPago = Convert.ToString(vContrato.IdFormaPago);
            SeleccionaLugarEjecucion("1");    
        }
        LugarEjecucion();
        SeleccionaFormaPago(IdFormaPago);
        ddlFormaPago.SelectedValue = IdFormaPago;

        if (ddlFormaPago.SelectedValue == "1")
        {
            //txtValorAnticipo.Enabled = false;
            txtPorcentajeAnticipo.Enabled = false;
        }

    }

    public void TipoFormaPago()
    {
        string IdTipoPago = "-1";
        if (vContrato.IdTipoPago != null)
        {
            IdTipoPago = Convert.ToString(vContrato.IdTipoPago);
            SeleccionaLugarEjecucion("1");
        }
        LugarEjecucion();
        SeleccionarTipoFormaPago(IdTipoPago);
        ddlTipoFormaPago.SelectedValue = IdTipoPago;
    }

    public void LugarEjecucion()
    {
        #region LugarEjecucion

        if (vContrato.DatosAdicionaleslugarEjecucion != null)
        {
            txtDatosAdicionalesLugarEjecucion.Text = vContrato.DatosAdicionaleslugarEjecucion;
        }
        //IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);

        #endregion
    }

    public  void CargarCodigosSECOP()
    {
        imgbtnCodigosSECOP.Enabled = true;
        int idContrato = int.Parse(hfIdContrato.Value);
        gvCodigosSECOP.EmptyDataText = EmptyDataText();
        gvCodigosSECOP.PageSize = PageSize();
        gvCodigosSECOP.DataSource = vContratoService.ObtenerCodigosSECOPContrato(idContrato);
        gvCodigosSECOP.DataBind();
        if (gvCodigosSECOP.Rows != null && gvCodigosSECOP.Rows.Count > 0)
            hfCodigoSECOP.Value = "1";
        else
            hfCodigoSECOP.Value = "0";
    }

    public void CargarModalidadSeleccion()
    {
        #region LlenarComboModalidadSeleccion
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        #endregion

    }

    public void CargarModalidadSeleccionTipoContrato(int IdTipoContrato)
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccionTipoContrato(IdTipoContrato, true);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }


    #endregion

    #region DatosGeneralesContrato

    #region ContratoAsociado
    private void SeleccionadoContratoAsociado(string codContratoAsociado)
    {
        switch (codContratoAsociado)
        {
            case CodConvenioMarco:
                //chkContratoConvenioAd.Checked = false;
                lbNumConvenioContrato.Text = "Número Convenio Marco Asociado *"; // Pagina 7/11 paso 4.1
                CodContratoAsociadoSeleccionado = CodConvenioMarco;
                HabilitaCategoriaContrato(false); //Se deshabilita la eleccion de la categoria Pagina 11 paso 4.3
                SeleccionadoContratoAsociado(true);
                break;
            case CodContratoConvenioAdhesion:
                //chkConvenioMarco.Checked = false;
                lbNumConvenioContrato.Text = "Número Contrato/Convenio Adhesión *"; // Pagina 7/11 paso 4.2
                CodContratoAsociadoSeleccionado = CodContratoConvenioAdhesion;
                HabilitaCategoriaContrato(false); //Se deshabilita la eleccion de la categoria Pagina 11 paso 4.3
                SeleccionadoContratoAsociado(true);
                break;
            default:
                CodContratoAsociadoSeleccionado = string.Empty;
                HabilitaCategoriaContrato(true); // Pagina 11 Paso 6 Se habilita la eleccion de la categoria 
                SeleccionadoContratoAsociado(false); // 
                break;
        }
    }
    /// <summary>
    /// Pagina 11 Paso 6 Habilita Categoria por seleccion de contrato asociado
    /// </summary>
    /// <param name="seleccionado"></param>
    private void SeleccionadoContratoAsociado(bool seleccionado)
    {
        imgNumConvenioContrato.Enabled = seleccionado;
        if (!seleccionado)
        {
            PnValorConvenio.Style.Add("visibility", "hidden");

            hfFechaInicioEjecConvMarco.Value = string.Empty;
            hfFechaFinalTerminContConvMarco.Value = string.Empty;
            hfFechaInicioEjecContConvAd.Value = string.Empty;
            hfFechaFinalTerminContConvAd.Value = string.Empty;

            IdContratoAsociado = string.Empty;
            hfNumConvenioContratoAsociado.Value = string.Empty;
            txtNumConvenioContrato.Text = string.Empty;
            //rfvNumConvenioContrato.Enabled = false;
        }
        else
        {
            PnValorConvenio.Style.Add("visibility", "");
            //rfvNumConvenioContrato.Enabled = true;
        }

        SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue, false); // Se comenta ya que la deseleccion no requiere que se verifique el valor de la categoria y actue ademas recarga datos de tipo de contrato
    }
    #endregion

    /// <summary>
    /// AQUI SE CARGA INFO DEL CONTRATO ASOCIADO
    /// </summary>
    /// <param name="numeroContratoSeleccionado"></param>
    public void SeleccionaNumeroConvenioContrato(string numeroContratoSeleccionado)
    {
        if (!IdContratoAsociado.Equals(string.Empty))
        {
            Contrato vContratoAsociado = new Contrato();
            vContratoAsociado = vContratoService.ContratoObtener(Convert.ToInt32(IdContratoAsociado));

            #region RecuperacionFechasContraAsociadoParaValidaciones
            if (chkConvenioMarco.Checked)
            {
                hfObjeto.Value = vContratoAsociado.ObjetoContrato;
                hfAlcance.Value = vContratoAsociado.AlcanceObjetoContrato;

                hfFechaInicioEjecConvMarco.Value = vContratoAsociado.FechaInicioEjecucion.ToString();
                hfFechaFinalTerminContConvMarco.Value = vContratoAsociado.FechaFinalTerminacionContrato.ToString();
                hfFechaSuscripcionContConvMarco.Value = vContratoAsociado.FechaSuscripcionContrato.ToString();

                hfFechaInicioEjecContConvAd.Value = string.Empty;
                hfFechaFinalTerminContConvAd.Value = string.Empty;

                //HabilitaCategoriaContrato(true); // ?????? no lo dice en el caso de uso
            }
            else if (chkContratoConvenioAd.Checked)
            {
                hfObjeto.Value = vContratoAsociado.ObjetoContrato;
                hfAlcance.Value = vContratoAsociado.AlcanceObjetoContrato;

                hfFechaInicioEjecContConvAd.Value = vContratoAsociado.FechaInicioEjecucion.ToString();
                hfFechaFinalTerminContConvAd.Value = vContratoAsociado.FechaFinalTerminacionContrato.ToString();

                hfFechaInicioEjecConvMarco.Value = string.Empty;
                hfFechaFinalTerminContConvMarco.Value = string.Empty;
                hfFechaSuscripcionContConvMarco.Value = string.Empty;
            }
            #endregion

            int idEstadoSuscrito = vContratoService.ConsultarEstadoContrato(null, CodEstadoSuscrito, null, true)[0].IdEstadoContrato;
            int idEstadoEjecucion = vContratoService.ConsultarEstadoContrato(null, CodEstadoEjecucion , null, true)[0].IdEstadoContrato;
            string NumeroContratoAsociado = vContratoAsociado.NumeroContrato;
            if (vContratoAsociado.IdEstadoContrato != idEstadoSuscrito && vContratoAsociado.IdEstadoContrato != idEstadoEjecucion)
            {
                if (!scriptEnCola)
                {
                    txtNumConvenioContrato.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this, GetType(), "valEstadoContConvAsociado", "alert('El contrato/convenio asociado debe estar suscrito o en ejecución')", true); // RNF 20 y 24
                    scriptEnCola = true;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(NumeroContratoAsociado))
                {
                    hfNumConvenioContratoAsociado.Value = vContratoAsociado.NumeroContrato;
                    txtNumConvenioContrato.Text = vContratoAsociado.NumeroContrato;
                    HabilitaCategoriaContrato(true); // ?????? no lo dice en el caso de uso
                }
                else
                {
                    txtNumConvenioContrato.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this, GetType(), "valNumeroContConvAsociado", "alert('Es necesario que el contrato/convenio asociado tenga un número de proceso asociado')", true);
                    scriptEnCola = true;
                }
            }
            
        }
        else
        {
            hfNumConvenioContratoAsociado.Value = string.Empty;
            txtNumConvenioContrato.Text = string.Empty;
            hfFechaInicioEjecContConvAd.Value = string.Empty;
            hfFechaFinalTerminContConvAd.Value = string.Empty;
            hfFechaInicioEjecConvMarco.Value = string.Empty;
            hfFechaFinalTerminContConvMarco.Value = string.Empty;
            HabilitaCategoriaContrato(false);
        }
    }

    #region CategoriaContrato

    private void HabilitaCategoriaContrato(bool? habilitar)
    {
        if (habilitar == null)
        { 
            ddlCategoriaContrato.SelectedIndex = 0;
            SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue, true);
            //cvCategoriaContrato.Enabled = false;
        } 
        else if (habilitar == true)
        {
            ddlCategoriaContrato.Enabled = true;
            //cvCategoriaContrato.Enabled = false;
        }
        else
        {
            if (IdContratoAsociado.Equals(string.Empty))
                ddlCategoriaContrato.Enabled = false;
            //cvCategoriaContrato.Enabled = false;
            //SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue);
        }
    }

    private void ObtenerIdContratoConvenio()
    {
        try
        {
            if (IdContrato.Equals(string.Empty))
            {
                int vResultado;
                
                Contrato vContratoRegistroInicial = new Contrato();
                vContratoRegistroInicial.IdEstadoContrato = Convert.ToInt32(IdEstadoContrato);
                vContratoRegistroInicial.UsuarioCrea = GetSessionUser().NombreUsuario;
                vContratoRegistroInicial.IdRegionalContrato = Convert.ToInt32(IdRegional);
                vResultado = vContratoService.ContratoRegistroInicial(vContratoRegistroInicial);

                if (vResultado == 1)
                {
                    IdContrato = Convert.ToString(vContratoRegistroInicial.IdContrato);
                    SetSessionParameter("Contrato.ContratosAPP", IdContrato);
                    toolBar.LipiarMensajeError();
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada, bool cargaListaTipoContrato)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            ObtenerIdContratoConvenio();

            // Página 1 Descripción adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio
            if (cargaListaTipoContrato)
            { 
                ddlTipoContratoConvenio.Items.Clear();
                List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), true, null, null, null, null, null);
                foreach (TipoContrato tD in vLTiposContratos)
                {
                    ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
                }
                ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            }
            #endregion

            switch (CodContratoAsociadoSeleccionado) // Se asigna previamente en SeleccionadoContratoAsociado 
            {
                case CodContratoConvenioAdhesion:
                    #region IdTipoContAporte_CategoriaSeleccionada
                    TipoContrato vTipoContrato = new TipoContrato();
                    vTipoContrato.CodigoTipoContrato = CodTipoContAporte;
                    vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaSeleccionada);
                    String IdTipoContAporte = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).IdTipoContrato.ToString();
                    #endregion
                    HabilitarTipoContratoConvenio(false);
                    if (ddlTipoContratoConvenio.Items.FindByValue(IdTipoContAporte) != null)
                        ddlTipoContratoConvenio.SelectedValue = IdTipoContAporte; // Página 1 Descripción adicional Tipo Contrato Convenio
                    break;
                default:
                    HabilitarTipoContratoConvenio(true); //Página 11 Paso 9 
                    break;
            }
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            HabilitarTipoContratoConvenio(false);
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
            #endregion
        }

        if (NoPrecargarInformacion == 0)
            ConfigurarModalidad(idCategoriaSeleccionada, ddlTipoContratoConvenio.SelectedValue);
    }

    private void HabilitarTipoContratoConvenio(bool habilitar)
    {
        ddlTipoContratoConvenio.Enabled = habilitar;
        if (habilitar)
        {
            //cvTipoContratoConvenio.Enabled = true;
        }
        else
        {
            //cvTipoContratoConvenio.Enabled = true;
            ddlTipoContratoConvenio.SelectedIndex = 0;
            SeleccionadoTipoContratoConvenio(ddlCategoriaContrato.SelectedValue, ddlTipoContratoConvenio.SelectedValue);
        }
    }

    private void SeleccionadoTipoContratoConvenio(string idCategoriaSeleccionada, string idTipoContratoConvenioSeleccionado)
    {
        ConfigurarModalidad(idCategoriaSeleccionada, idTipoContratoConvenioSeleccionado);

        if (ddlTipoContratoConvenio.SelectedValue == "10" || ddlTipoContratoConvenio.SelectedValue == "9")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "1";
            rblRequiereGarantia.SelectedValue = "True";
            rblAportesEspecie.SelectedValue = "False";
            chkNoManejaAportes.Checked = true;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = true;

            chkSiManejaRecursos.Checked = true;
            chkSiManejaRecursos.Enabled = true;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else if (ddlTipoContratoConvenio.SelectedValue == "15" || ddlTipoContratoConvenio.SelectedValue == "16" || ddlTipoContratoConvenio.SelectedValue == "68" || ddlTipoContratoConvenio.SelectedValue == "39" || ddlTipoContratoConvenio.SelectedValue == "45"
                 || ddlTipoContratoConvenio.SelectedValue == "49" || ddlTipoContratoConvenio.SelectedValue == "64" || ddlTipoContratoConvenio.SelectedValue == "51" || ddlTipoContratoConvenio.SelectedValue == "72"
                 || ddlTipoContratoConvenio.SelectedValue == "52")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "1";
            
            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else if (ddlTipoContratoConvenio.SelectedValue == "17")
        {
            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "18")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "4";
            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else if (ddlTipoContratoConvenio.SelectedValue == "19")
        {
            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "38")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "9";
            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "2";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else if (ddlTipoContratoConvenio.SelectedValue == "43")
        {
            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "4";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "24")
        {
            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "26")
        {
            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "69"|| ddlTipoContratoConvenio.SelectedValue == "55" || ddlTipoContratoConvenio.SelectedValue == "70"
             || ddlTipoContratoConvenio.SelectedValue == "71")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "14";
            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else if (ddlTipoContratoConvenio.SelectedValue == "27")
        {

            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "28")
        {

            CargarModalidadSeleccionTipoContrato(Convert.ToInt32(ddlTipoContratoConvenio.SelectedValue));

            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";

        }
        else if (ddlTipoContratoConvenio.SelectedValue == "67")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "15";
            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else if (ddlTipoContratoConvenio.SelectedValue == "34")
        {
            CargarModalidadSeleccion();
            ddlModalidadSeleccion.SelectedValue = "3";
            chkNoManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "1";
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else
        {

            CargarModalidadSeleccion();
            chkSiManejaAportes.Checked = false;
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;
            

            chkSiManejaRecursos.Checked = false;
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            ddlRegimenContratacion.SelectedValue = "-1";
        }

        #region PrecargaChecksDependientesTipoContratoConvenio
        if (!idTipoContratoConvenioSeleccionado.Equals("-1"))
        {
            TipoContrato vTipoContrato = new TipoContrato();
             vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContratoConvenioSeleccionado);
            vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaSeleccionada);
            vTipoContrato = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato);
            
            bool requiereActaInicio = vTipoContrato.ActaInicio; // Pagina 13 Paso 26.2
            bool requiereManejoAportes = vTipoContrato.AporteCofinaciacion; // Pagina 13 Paso 29.2
            bool requiereManejoRecursos = vTipoContrato.RecursoFinanciero; // Pagina 13 Paso 32.2

            if (PnRequiereActaInicio.Style["visibility"].Equals(string.Empty))
            {
                if (requiereActaInicio)
                {
                    chkSiReqActa.Checked = true; //.SelectedIndex = 0; // Pagina 13 Paso 26.3
                    chkSiReqActa.Enabled = false;
                    chkNoReqActa.Enabled = false;

                    chkNoReqActa.Checked = false;
                }
                else
                {
                    chkSiReqActa.Enabled = true;
                    chkNoReqActa.Enabled = true;
                }
            }

            if (PnManejaAportes.Style["visibility"].Equals(string.Empty))
            {
                if (requiereManejoAportes)
                {
                    chkSiManejaAportes.Checked = true; // Pagina 13 Paso 29.3
                    chkSiManejaAportes.Enabled = false;
                    chkNoManejaAportes.Enabled = false;

                    chkNoManejaAportes.Checked = false;
                }
                else
                {
                    chkSiManejaAportes.Enabled = true;
                    chkNoManejaAportes.Enabled = true;
                }
            }

            if (PnManejaRecursos.Style["visibility"].Equals(string.Empty))
            {
                if (requiereManejoRecursos)
                {
                    chkSiManejaRecursos.Checked = true; // Pagina 13 Paso 32.3
                    chkSiManejaRecursos.Enabled = false;
                    chkNoManejaRecursos.Enabled = false;

                    chkNoManejaRecursos.Checked = false;
                }
                else
                {
                    chkSiManejaRecursos.Enabled = true;
                    chkNoManejaRecursos.Enabled = true;
                }
            }
        }
        #endregion
    }
    #endregion

    #region Modalidad
    private void ConfigurarModalidad(string idCategoriaSeleccionada, string idTipoContratoConvenioSeleccionado)
    {
        if (idCategoriaSeleccionada.Equals("-1"))
        {
            ddlTipoContratoConvenio.Enabled = false;
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(false);
        }
        else if (idTipoContratoConvenioSeleccionado.Equals("-1"))
        {
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(false);
        }
        else if (idCategoriaSeleccionada.Equals(IdCategoriaContrato) &&
            idTipoContratoConvenioSeleccionado.Equals(IdTipoContConvPrestServApoyoGestion)) // Pagina 2 Descripción adicional modalidad academica
        {
            HabilitarModalidadAcademica(true); // Pagina 11 Paso 12
            HabilitarModalidadSeleccion(true);            
        }
        else if (idCategoriaSeleccionada.Equals(IdCategoriaContrato) &&
            idTipoContratoConvenioSeleccionado.Equals(IdTipoContConvPrestServProfesionales)) // Pagina 2 Descripción adicional modalidad academica
        {

            HabilitarModalidadAcademica(true); // Pagina 11 Paso 12
            HabilitarModalidadSeleccion(true);
        }
        // PAgina 12 Paso 12 Si corresponde a otra categoria(Diferente a Contrato) y tipo contrato(Diferente a prestación de serv)
        else if (idCategoriaSeleccionada.Equals(IdCategoriaConvenio) &&
            idTipoContratoConvenioSeleccionado.Equals(IdMarcoInteradministrativo))
        {
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(false);
            ddlModalidadSeleccion.Enabled = false;
            ddlModalidadSeleccion.SelectedValue = IdContratacionDirecta; //Pagina 12 Paso 18.2 Preseleccionar Contratacion Directa
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
        }
        else
        {
            HabilitarModalidadAcademica(false);
            HabilitarModalidadSeleccion(true); // Pagina 12 Paso 18.1 
        }        
    }

    private void HabilitarModalidadAcademica(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlModalidadAcademica.SelectedIndex = 0;
            ddlModalidadAcademica.Enabled = false;
            HabilitarNombreProfesion(habilitar);
            //cvModalidadAcademica.Enabled = false;
            SeleccionarModalidadAcademica(ddlModalidadAcademica.SelectedValue);
        } 
        else if (habilitar == true)
        {
            ddlModalidadAcademica.Enabled = true;
            //cvModalidadAcademica.Enabled = true;
        } 
        else 
        {
            ddlModalidadAcademica.Enabled = false;
            ddlModalidadAcademica.SelectedIndex = 0; //Se coloca para que limpie en los casos en que se cambia de marco a adhesion, o cambio de categoria, que afecta el tipo de contrato y por ende este campo
            //cvModalidadAcademica.Enabled = false;
            HabilitarNombreProfesion(habilitar);
            SeleccionarModalidadAcademica(ddlModalidadAcademica.SelectedValue);
        }
    }

    private void HabilitarModalidadSeleccion(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlModalidadSeleccion.Enabled = false;
            ddlModalidadSeleccion.SelectedIndex = 0;
            SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
            //cvModalidadSeleccion.Enabled = true;
        } 
        else if (habilitar == true)
        {
            ddlModalidadSeleccion.Enabled = true;
            //cvModalidadSeleccion.Enabled = true;
        } 
        else 
        {
            ddlModalidadSeleccion.Enabled = false;
            //cvModalidadSeleccion.Enabled = true;
        }
    }

    private void HabilitarNombreProfesion(bool? habilitar)
    {
        if (habilitar == null)
        {
            lbNombreProfesion.Text = "Nombre de la profesión";
            ddlNombreProfesion.SelectedIndex = 0;
            SeleccionaProfesion(ddlNombreProfesion.SelectedValue);
            ddlNombreProfesion.Enabled = false;
            //cvNombreProfesion.Enabled = false;
        }
        else if (habilitar == true)
        {
            lbNombreProfesion.Text = "Nombre de la profesión *";
            ddlNombreProfesion.Enabled = true;
            //cvNombreProfesion.Enabled = true;
        } 
        else 
        {
            lbNombreProfesion.Text = "Nombre de la profesión";
            ddlNombreProfesion.Enabled = false;
            ddlNombreProfesion.SelectedIndex = 0; //Se coloca para que limpie en los casos en que se cambia de marco a adhesion, o cambio de categoria, que afecta el tipo de contrato y por ende este campo
            //cvNombreProfesion.Enabled = false;
        }
    }

    private void SeleccionarModalidadAcademica(string idModalidadAcademicaSeleccionada)
    {
        #region LlenarComboNombreProfesion
        ddlNombreProfesion.Items.Clear();
        List<ProfesionKactus> vLTiposContratos = vContratoService.ConsultarProfesionesKactus(idModalidadAcademicaSeleccionada, null, null, null);
        foreach (ProfesionKactus tD in vLTiposContratos)
        {
            ddlNombreProfesion.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString().Trim()));
        }
        ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        #endregion

        if (idModalidadAcademicaSeleccionada.Equals("-1"))
        {
            HabilitarNombreProfesion(false);
        }
        else
        {
            HabilitarNombreProfesion(true); // Pagina 2 Descripción adicional Nombre de la profesión. Pagina 12 Paso 15 
        }

        //HabilitarModalidadSeleccion(false);
    }

    private void SeleccionaProfesion(string idProfesionSeleccionada)
    {
        if (idProfesionSeleccionada.Equals("-1"))
        {
            if (ddlNombreProfesion.Enabled) //Permite que se de el Paso 18.2 cuando se carga el contrato
                HabilitarModalidadSeleccion(false);
        }
        else
        {
            HabilitarModalidadSeleccion(true);
        }
    }

    /// <summary>
    /// Pagina 3 Descripción adicional Número de Proceso
    /// </summary>
    /// <param name="idModalidadSeleccionSeleccionada"></param>
    private void SeleccionarModalidadSeleccion(string idModalidadSeleccionSeleccionada)
    {
        if (idModalidadSeleccionSeleccionada.Equals("-1"))
        {
            HabilitarNumeroProceso(false);
            HabilitarActaInicio(null);
        }
        else
        {
            ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
            vModalidadSeleccion.IdModalidad = Convert.ToInt32(idModalidadSeleccionSeleccionada);
            string CodModalidadSeleccionSeleccionada = vContratoService.IdentificadorCodigoModalidadSeleccion(vModalidadSeleccion).CodigoModalidad;
            switch (CodModalidadSeleccionSeleccionada)
            {
                case CodContratacionDirecta:
                    HabilitarNumeroProceso(false); // Pagina 12 Paso 20.1
                    HabilitarActaInicio(true); // Pagina 12 Paso 20.2
                    RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
                    break;
                case CodContratacionDirectaAporte:
                    HabilitarNumeroProceso(false); // Pagina 12 Paso 20.1
                    HabilitarActaInicio(true); // Pagina 12 Paso 20.2
                    RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
                    break;
                default:
                    HabilitarNumeroProceso(true); // Página 12 Paso 20.1 
                    //Jorge Vizcaino: Se cambiala forma de comparar la fecha
                    //if (NoPrecargarInformacion == 0 && 
                    //    (caFechaAdjudicaProceso.Date.ToShortDateString().Equals(string.Empty) || caFechaAdjudicaProceso.Date.ToShortDateString().Equals("01/01/1900")))
                    if (NoPrecargarInformacion == 0 &&
                        (caFechaAdjudicaProceso.Date.ToShortDateString().Equals(string.Empty) || Convert.ToDateTime(caFechaAdjudicaProceso.Date.ToShortDateString()) == Convert.ToDateTime("01/01/1900")))
                        HabilitarActaInicio(false); // Pagina 12 Paso 20.2 // Se anula porque elimina informacion en la edicion.
                    break;
            }
        }
    }
    #endregion

    private void HabilitarNumeroProceso(bool habilitar)
    {
        if (habilitar)
        {
            PnNumeroProceso.Style.Add("visibility", "");
            PnFechaAdjudicaProceso.Style.Add("visibility", "");
            //rfvNumeroProceso.Enabled = true;
            //Se deberia cargar el numero de proceso??
        }
        else
        {
            PnNumeroProceso.Style.Add("visibility", "hidden");
            PnFechaAdjudicaProceso.Style.Add("visibility", "hidden");
            hfNumeroProceso.Value = string.Empty;
            hfIdNumeroProceso.Value = string.Empty;
            txtNumeroProceso.Text = string.Empty;
            //rfvNumeroProceso.Enabled = false;
            SeleccionarNumeroProceso(txtNumeroProceso.Text);
        }
    }

    private void SeleccionarNumeroProceso(string numeroProcesoSeleccionado)
    {
        if (numeroProcesoSeleccionado.Equals(string.Empty))
        {
            HabilitarFechaAdjudicacion(false);
        }
        else
        {
            HabilitarFechaAdjudicacion(true); // Pagina 12 Paso 22
        }
    }

    private void HabilitarFechaAdjudicacion(bool habilitar)
    {
        if (habilitar)
        {
            caFechaAdjudicaProceso.Enabled = false;
            caFechaAdjudicaProceso.Requerid = true;
            SeleccionaFechaAdjudicacion(caFechaAdjudicaProceso.Date.ToShortDateString());
        }
        else
        {
            caFechaAdjudicaProceso.InitNull = true;
            caFechaAdjudicaProceso.Enabled = false;
            caFechaAdjudicaProceso.Requerid = false;
            SeleccionaFechaAdjudicacion(caFechaAdjudicaProceso.Date.ToShortDateString());
        }
    }

    private void SeleccionaFechaAdjudicacion(string fechaAdjudicacion)
    {
        if (PnNumeroProceso.Style["visibility"].Equals(string.Empty))
        {
            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (fechaAdjudicacion.Equals(string.Empty) || fechaAdjudicacion.Substring(0, 10).Equals("01/01/1900"))
            if (fechaAdjudicacion.Equals(string.Empty) || Convert.ToDateTime(fechaAdjudicacion)== Convert.ToDateTime("01/01/1900"))
            {
                HabilitarActaInicio(false);
            }
            else
            {
                string textValFechaAdjuProc = ValidacionFechaAdjudicacionProceso(caFechaAdjudicaProceso.Date); // Pagina 12 Paso 25 
                if (textValFechaAdjuProc.Equals(string.Empty))
                {
                    HabilitarActaInicio(true); // Pagina 13 Paso 26.1
                    RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
                }
                else
                {
                    HabilitarActaInicio(false);
                    caFechaAdjudicaProceso.InitNull = true;
                    if (!scriptEnCola)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "valFechaAdjuProc", "alert('" + textValFechaAdjuProc + "')", true);
                        scriptEnCola = true;
                    }
                }
            }
        }
    }

    private string ValidacionFechaAdjudicacionProceso(DateTime? fechaAdjudicacionSeleccionada)
    {
        string textValFechaAdjuProc = string.Empty;

        if (((DateTime)fechaAdjudicacionSeleccionada).Year < (DateTime.Today.Year - 1) || ((DateTime)fechaAdjudicacionSeleccionada).Year > DateTime.Today.Year)
        {
            textValFechaAdjuProc = "El año de la fecha de adjudicación del proceso debe ser menor un año o igual al año de la Fecha actual";
        }

        return textValFechaAdjuProc;
    }

    private void HabilitarActaInicio(bool? habilitar)
    {
        if (habilitar == null)
        {
            PnRequiereActaInicio.Style.Add("visibility", "hidden");
            chkSiReqActa.Checked = false; //.Items[0].Selected = false;
            chkNoReqActa.Checked = false; //chkSiReqActa.Items[1].Selected = false; //
            cuvReqActa.Enabled = false;
            HabilitarManejaAportes(null);
        }
        else if (habilitar == true)
        {
            PnRequiereActaInicio.Style.Add("visibility", "");
            cuvReqActa.Enabled = true;
            // (SE REPITE) 
            //RequiereActaDeInicio(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 26
        }
        else
        {
            chkSiReqActa.Enabled = false;
            chkNoReqActa.Enabled = false;
            if (PnRequiereActaInicio.Style["visibility"].Equals(string.Empty))
            {
                cuvReqActa.Enabled = true;
            }
            else
            {
                cuvReqActa.Enabled = false;
            }
            HabilitarManejaAportes(false);
        }        
    }


    protected void txtFechaActa_OnTextChanged(object sender, EventArgs e)
    {
        
    }

    private void RequiereActaDeInicio(string idTipoContrato, string idCategoriaContrato)
    {
        TipoContrato vTipoContrato = new TipoContrato();
        vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContrato);
        vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
        bool requiereActaInicio = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).ActaInicio; // Pagina 13 Paso 26.2

        if (requiereActaInicio)
        {
            chkSiReqActa.Checked = true; //.SelectedIndex = 0; // Pagina 13 Paso 26.3
            chkSiReqActa.Enabled = false;
            chkNoReqActa.Enabled = false;

            chkNoReqActa.Checked = false;

            PnManejaAportes.Style.Add("visibility", "");
            RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
        }
        else
        {
            chkSiReqActa.Enabled = true; // Pagina 13 Paso 26.4
            chkNoReqActa.Enabled = true;
            if (chkSiReqActa.Checked || chkNoReqActa.Checked)
            {
                HabilitarManejaAportes(true);
                RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29                
            }
        }
    }

    private void HabilitarManejaAportes(bool? habilitar)
    {
        if (habilitar == null)
        {
            PnManejaAportes.Style.Add("visibility", "hidden");
            chkSiManejaAportes.Checked = false;
            chkNoManejaAportes.Checked = false;
            cuvManejaAportes.Enabled = false;
            HabilitarManejaRecursos(null);
        }
        else if (habilitar == true)
        {
            PnManejaAportes.Style.Add("visibility", "");
            cuvManejaAportes.Enabled = true;
            // (SE REPITE) 
            //RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
        }
        else
        {
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;
            if (PnManejaAportes.Style["visibility"].Equals(string.Empty))
            {
                cuvManejaAportes.Enabled = true;
            }
            else
            {
                cuvManejaAportes.Enabled = false;
            }
            HabilitarManejaRecursos(false);
        }
    }

    private void RequiereManejoAportes(string idTipoContrato, string idCategoriaContrato)
    {
        TipoContrato vTipoContrato = new TipoContrato();
        vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContrato);
        vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
        bool requiereManejoAportes = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).AporteCofinaciacion; // Pagina 13 Paso 29.2

        if (requiereManejoAportes)
        {
            chkSiManejaAportes.Checked = true; // Pagina 13 Paso 29.3
            chkSiManejaAportes.Enabled = false;
            chkNoManejaAportes.Enabled = false;

            chkNoManejaAportes.Checked = false;

            PnManejaRecursos.Style.Add("visibility", "");
            RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
        }
        else
        {
            chkSiManejaAportes.Enabled = true; // Pagina 13 Paso 29.4
            chkNoManejaAportes.Enabled = true;
            if (chkSiManejaAportes.Checked || chkNoManejaAportes.Checked)
            {
                HabilitarManejaRecursos(true);
                RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
            }
        }
    }

    private void HabilitarManejaRecursos(bool? habilitar)
    {
        if (habilitar == null)
        {
            PnManejaRecursos.Style.Add("visibility", "hidden");
            chkSiManejaRecursos.Checked = false;
            chkNoManejaRecursos.Checked = false;
            cuvManejaRecursos.Enabled = false;
            HabilitarRegimenContratacion(null);
        } else if (habilitar == true)
        {
            PnManejaRecursos.Style.Add("visibility", "");
            cuvManejaRecursos.Enabled = true;
            // (SE REPITE) 
            //RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
        }
        else
        {
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;
            if (PnManejaRecursos.Style["visibility"].Equals(string.Empty))
            {
                cuvManejaRecursos.Enabled = true;
            }
            else
            {
                cuvManejaRecursos.Enabled = false;
            }
            HabilitarRegimenContratacion(false);
        }
    }

    private void RequiereManejaRecursos(string idTipoContrato, string idCategoriaContrato)
    {
        TipoContrato vTipoContrato = new TipoContrato();
        vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContrato);
        vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
        bool requiereManejoRecursos = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato).RecursoFinanciero; // Pagina 13 Paso 32.2

        if (requiereManejoRecursos)
        {
            chkSiManejaRecursos.Checked = true; // Pagina 13 Paso 32.3
            chkSiManejaRecursos.Enabled = false;
            chkNoManejaRecursos.Enabled = false;

            chkNoManejaRecursos.Checked = false;

            HabilitarRegimenContratacion(true);
        }
        else
        {
            chkSiManejaRecursos.Enabled = true; // Pagina 13 Paso 32.4
            chkNoManejaRecursos.Enabled = true;
            if (chkSiManejaRecursos.Checked || chkNoManejaRecursos.Checked)
                HabilitarRegimenContratacion(true);
        }        
    }

    private void HabilitarRegimenContratacion(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlRegimenContratacion.Enabled = false;
            ddlRegimenContratacion.SelectedIndex = 0;
            //cvRegimenContratacion.Enabled = true;
            HabilitarBusquedaSolicitante(null);
        } else if (habilitar == true)
        {
            ddlRegimenContratacion.Enabled = true;
            //cvRegimenContratacion.Enabled = true;
            // (SE REPITE) 
            if (!ddlRegimenContratacion.SelectedValue.Equals("-1"))
                SeleccionaRegimenContratacion(ddlRegimenContratacion.SelectedValue);
        }
        else
        {
            ddlRegimenContratacion.Enabled = false;
            //cvRegimenContratacion.Enabled = true;
            HabilitarBusquedaSolicitante(false);
        }
    }

    private void SeleccionaRegimenContratacion(string idRegimenContratacion)
    {
        if (idRegimenContratacion.Equals("-1"))
        {
            HabilitarBusquedaSolicitante(false);
        }
        else
        {
            HabilitarBusquedaSolicitante(true);
        }
    }

    private void HabilitarBusquedaSolicitante(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgNombreSolicitante.Enabled = false;

            hfNombreSolicitante.Value = string.Empty;
            hfRegionalContConv.Value = string.Empty;
            hfDependenciaSolicitante.Value = string.Empty;
            hfCargoSolicitante.Value = string.Empty;
            hfCodRegionalContConv.Value = string.Empty;

            txtNombreSolicitante.Text = string.Empty;
            txtRegionalContratoConvenio.Text = string.Empty;
            txtDependenciaSolicitante.Text = string.Empty;
            txtCargoSolicitante.Text = string.Empty;

            //rfvNombreSolicitante.Enabled = true;
            //rfvRegionalContConv.Enabled = true;

            HabilitarOrdenadorGasto(null);
            SeleccionarSolicitante(string.Empty);
        } 
        else if (habilitar == true)
        {
            imgNombreSolicitante.Enabled = true;
            //rfvNombreSolicitante.Enabled = true;
            //rfvRegionalContConv.Enabled = true;
            if (!txtNombreSolicitante.Text.Equals(string.Empty))
                SeleccionarSolicitante(txtNombreSolicitante.Text); // Puede que no sea necesaria esta linea
        }
        else
        {
            imgNombreSolicitante.Enabled = false;
            //rfvNombreSolicitante.Enabled = true;
            //rfvRegionalContConv.Enabled = true;
            HabilitarOrdenadorGasto(false);

            //SeleccionarSolicitante(string.Empty);
        }

        VerficarContratoSinValor();
    }

    private void VerficarContratoSinValor()
    {
        //if (chkNoManejaRecursos.Checked && chkNoManejaAportes.Checked && !string.IsNullOrEmpty(rblAportesEspecie.SelectedValue) && Convert.ToBoolean(rblAportesEspecie.SelectedValue) == false && Convert.ToBoolean(rblRequiereGarantia.SelectedValue) == false)
        if(chkNoManejaRecursos.Checked)
        {
            hfContratoSV.Value = "true";
        }
        else
        {
            hfContratoSV.Value = "False";
        }
    }

    private void SeleccionarSolicitante(string solicitanteSeleccionado)
    {
        if (solicitanteSeleccionado.Equals(string.Empty))
        {
            HabilitarOrdenadorGasto(false);
        }
        else
        {
            HabilitarOrdenadorGasto(true); // Pagina 14 Paso 42
        }
    }

    private void HabilitarOrdenadorGasto(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgNombreOrdenadorGasto.Enabled = false;

            hfNombreOrdenadorGasto.Value = string.Empty;
            hfTipoIdentOrdenadorGasto.Value = string.Empty;
            hfNumeroIdentOrdenadoGasto.Value = string.Empty;
            hfCargoOrdenadoGasto.Value = string.Empty;

            txtNombreOrdenadorGasto.Text = string.Empty;
            txtTipoIdentOrdenadorGasto.Text = string.Empty;
            txtNumeroIdentOrdenadoGasto.Text = string.Empty;
            txtCargoOrdenadoGasto.Text = string.Empty;

            //rfvNombreOrdenadorGasto.Enabled = true;

            SeleccionarOrdenadorGasto(string.Empty);
        } 
        else if (habilitar == true)
        {
            imgNombreOrdenadorGasto.Enabled = true;
            //rfvNombreOrdenadorGasto.Enabled = true;
            if (!txtNombreOrdenadorGasto.Text.Equals(string.Empty))
                SeleccionarOrdenadorGasto(txtNombreOrdenadorGasto.Text); // Puede que no sea necesaria esta linea
        }
        else
        {
            imgNombreOrdenadorGasto.Enabled = false;
            //rfvNombreOrdenadorGasto.Enabled = true;
            SeleccionarOrdenadorGasto(string.Empty);
        }
    }

    private void SeleccionarOrdenadorGasto(string ordenadorGastoSeleccionado)
    {
        if (ordenadorGastoSeleccionado.Equals(string.Empty))
        {
            HabilitarPlanDeCompras(false);
            HabilitarObjetoContratoConvenio(false);
        }
        else
        {
            if (chkSiManejaRecursos.Checked)
            {
                HabilitarPlanDeCompras(true); // Pagina 14 Paso 46
                //SeleccionarPlanCompras("1"); // Carga la información de la sección Plan de Compras
                AcordeonActivo = "2";
                // Muestra Plan de compras acordeon
                //AccContratos.SelectedIndex = 1;
            }
            else
            {
                AcordeonActivo = "3";
                if (hfContratoSV.Value == "true")
                {
                    HabilitarPlanDeCompras(true);
                }
                else
                {
                    HabilitarPlanDeCompras(false);
                }

                
                HabilitarObjetoContratoConvenio(true); // Se continua con el paso 55, despues 66 y despues 62
                if (!ddlFormaPago.SelectedValue.Equals("-1"))
                {
                    SeleccionaFormaPago(ddlFormaPago.SelectedValue);
                    SeleccionaLugarEjecucion(string.Empty);
                }
            }
        }
    }

    #endregion

    #region PlanDeCompras

    private void HabilitarPlanDeCompras(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgPlanCompras.Enabled = false;
            gvConsecutivos.DataSource = null;
            gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = "0";
            gvProductos.DataSource = null;
            gvProductos.DataBind();
            nProductos = "0";
            gvRubrosPlanCompras.DataSource = null;
            gvRubrosPlanCompras.DataBind();
            HabilitarConsecutivoPlanDeCompras(null);
            //cvPlanCompras.Enabled = false;
            //cvProductos.Enabled = false;
            lbPlanCompras.Text = "Plan de compras";
            LbProductos.Text = "Productos";
        } else if (habilitar == true)
        {
            imgPlanCompras.Enabled = true;
            //cvPlanCompras.Enabled = true;
            //cvProductos.Enabled = true;
            lbPlanCompras.Text = "Plan de compras *";
            LbProductos.Text = "Productos *";
        }
        else
        {
            imgPlanCompras.Enabled = false;
            //cvPlanCompras.Enabled = false;
            //cvProductos.Enabled = false;
            HabilitarConsecutivoPlanDeCompras(false);
            lbPlanCompras.Text = "Plan de compras";
            LbProductos.Text = "Productos";
        }
    }

    private void SeleccionarPlanCompras(string idPlanDeComprasSeleccionado)
    {
        if (!string.IsNullOrEmpty(idPlanDeComprasSeleccionado) && idPlanDeComprasSeleccionado !=  "-1")
        {
            int idContrato = Convert.ToInt32(IdContrato);

            List<PlanDeComprasContratos> consecutivosPlanCompras = vContratoService.ConsultarPlanDeComprasContratoss(idContrato, null);
            gvConsecutivos.DataSource = consecutivosPlanCompras;
            gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = gvConsecutivos.Rows.Count.ToString();

            if (consecutivosPlanCompras.Count() > 0)
            {
                decimal ? valorTotal = null;
                decimal valorTotalItem = 0;
 
                foreach (var item in consecutivosPlanCompras)
                {
                    if (item.ValorTotal.HasValue)
                        valorTotalItem += item.ValorTotal.Value;
                }

                if (valorTotalItem > 0)
                    valorTotal = valorTotalItem;

                decimal? sumatoriaProductos = 0;
 
                var client = new WSContratosPACCOSoapClient();

                List<GetDetalleProductosServicio_Result> productos = new List<GetDetalleProductosServicio_Result>();
                
                List<GetDetalleRubrosServicio_Result> rubros = new List<GetDetalleRubrosServicio_Result>();

                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras) 
                {
                    var itemsDetallePACCO = client.GetListaDetalleProductoPACCO(pc.Vigencia, pc.IDPlanDeCompras);

                    foreach (GetDetalleProductosServicio_Result pWs in itemsDetallePACCO) 
                    { 
                        sumatoriaProductos += pWs.valor_total;
                        productos.Add(pWs);                            
                    }

                    var itemsRubrosPACCO =  client.GetListaDetalleRubroPACCO(pc.Vigencia, pc.IDPlanDeCompras);

                    if (itemsRubrosPACCO != null)
                        rubros = itemsRubrosPACCO.ToList();
                }

                decimal valorTotalD = valorTotal.HasValue ? valorTotal.Value : sumatoriaProductos.Value;
                TotalProductos = valorTotalD.ToString();
                PrecargaValorInicialValorProductosPlanCompras();
                txtValorPlanComprasContratos.Text = valorTotalD.ToString("#,###0.00##;($ #,###0.00##)"); 
 
                gvProductos.DataSource = productos;
                gvProductos.DataBind();
                nProductos = gvProductos.Rows.Count.ToString();

                gvRubrosPlanCompras.DataSource = rubros;
                gvRubrosPlanCompras.DataBind();

                #region ListaConsecutivoPlanCompras
                HabilitarConsecutivoPlanDeCompras(true);

                string ConsecutivoPlanComprasSeleccionado = DdlConsecutivoPlanCompras.SelectedValue;
                DdlConsecutivoPlanCompras.Items.Clear();

                foreach (PlanDeComprasContratos tD in consecutivosPlanCompras)
                {
                    DdlConsecutivoPlanCompras.Items.Add(new ListItem(tD.IDPlanDeCompras.ToString(), tD.IDPlanDeComprasContratos.ToString()));
                }

                if (DdlConsecutivoPlanCompras.Items.FindByValue(ConsecutivoPlanComprasSeleccionado) != null)
                    DdlConsecutivoPlanCompras.SelectedValue = ConsecutivoPlanComprasSeleccionado;

                #endregion

                vContratoService.ModificarAporteContratoValor(idContrato, Convert.ToDecimal(TotalProductos), GetSessionUser().NombreUsuario);
                txtValorApoICBFTextChanged(null, EventArgs.Empty);
            }
            else
            {
                gvProductos.DataSource = null;
                gvProductos.DataBind();

                gvRubrosPlanCompras.DataSource = null;
                gvRubrosPlanCompras.DataBind();

                TotalProductos = string.Empty;
                HabilitarConsecutivoPlanDeCompras(null);
                DdlConsecutivoPlanCompras.Items.Clear();
                DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));

                vContratoService.EliminarAporteContratoValor(int.Parse(hfIdContrato.Value));
                txtValorApoICBFTextChanged(null, EventArgs.Empty);
            }
        }
        else
        {
            TotalProductos = string.Empty;
            HabilitarConsecutivoPlanDeCompras(false);
        }
    }

    private void SeleccionaConsecutivoPlanDeCompras(string idConsecutivoPlanDeCompras)
    {
        if (idConsecutivoPlanDeCompras == "-1")
        {
            if (chkSiManejaRecursos.Checked)
                txtObjeto.Text = string.Empty;

            HabilitarObjetoContratoConvenio(false);
        }
        else
        {
            string textoAlertavalSeleccionaConsecPlanComp = string.Empty;

            if (!vContratoService.ValidacionCoincideRegionalesContratoPlanComprasContrato(Convert.ToInt32(IdContrato), hfCodRegionalContConv.Value))
                textoAlertavalSeleccionaConsecPlanComp += "Alerta: La Regional del Contrato/Convenio (Solicitante) no corresponde con el Plan de Compras seleccionado, Verifique";

            if (!string.IsNullOrEmpty(textoAlertavalSeleccionaConsecPlanComp))
            {
                DdlConsecutivoPlanCompras.SelectedIndex = 0;

                textoAlertavalSeleccionaConsecPlanComp = "alert('" + textoAlertavalSeleccionaConsecPlanComp + "')";

                if (!scriptEnCola)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "valSeleccionaConsecPlanComp", textoAlertavalSeleccionaConsecPlanComp, true);
                    scriptEnCola = true;
                }
            }


            if (!DdlConsecutivoPlanCompras.SelectedValue.Equals("-1"))
            {
                int vVigencia = 0;

                foreach (GridViewRow gvr in gvConsecutivos.Rows)
                {
                    string idPlanCompras = gvConsecutivos.DataKeys[gvr.RowIndex]["IDPlanDeCompras"].ToString();

                    if (idPlanCompras.Equals(DdlConsecutivoPlanCompras.SelectedItem.Text))
                    {
                        vVigencia = Convert.ToInt32(gvConsecutivos.DataKeys[gvr.RowIndex]["Vigencia"].ToString());
                        break;
                    }
                }

                var vCodRegional = vSIAService.ConsultarRegional(Convert.ToInt32(IdRegional)).CodigoRegional + "00";
                WsContratosPacco.WSContratosPACCOSoap _wsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
                var myGridResults = _wsContratosPacco.GetListaConsecutivosEncabezadosPACCO(vVigencia,
                                                                                           vCodRegional,
                                                                                           0,
                                                                                           0,
                                                                                           Convert.ToInt32(DdlConsecutivoPlanCompras.SelectedItem.Text),
                                                                                           null,
                                                                                           0,
                                                                                           0,
                                                                                           null);
                if (myGridResults.Count() > 0)
                {
                    AlcancePlanCompras = myGridResults[0].alcance;
                    ObjetoPlanCompras = myGridResults[0].objeto_contractual;
                }
            }

            AcordeonActivo = "3";
            HabilitarObjetoContratoConvenio(true);
        }
    }

    private void HabilitarConsecutivoPlanDeCompras(bool? habilitar)
    {
        if (habilitar == null)
        { 
            DdlConsecutivoPlanCompras.SelectedIndex = 0;
            DdlConsecutivoPlanCompras.Enabled = false;
            SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);
        }
        else if (habilitar == true)
        {
            DdlConsecutivoPlanCompras.Enabled = true;
        }
        else
        {
            DdlConsecutivoPlanCompras.Enabled = false;
        }
    }

    private void SeleccionarPlanComprasOld(string idPlanDeComprasSeleccionado)
    {
        if (!idPlanDeComprasSeleccionado.Equals(string.Empty) && !idPlanDeComprasSeleccionado.Equals("-1"))
        {
            List<PlanDeComprasContratos> consecutivosPlanCompras = vContratoService.ConsultarPlanDeComprasContratoss(Convert.ToInt32(IdContrato), null);
            gvConsecutivos.DataSource = consecutivosPlanCompras;
            gvConsecutivos.DataBind();
            nConsecutivosPlanCompras = gvConsecutivos.Rows.Count.ToString();

            if (consecutivosPlanCompras.Count() > 0)
            {
                // Carga grilla Plan de Compras y Rubros con webservice, filtrar por productos registrados en ProductoPlanComprasContrato
                #region CargaGrillaProductosRubros

                decimal? sumatoriaProductos = 0;

                #region ProductosDelPlanComprasPaso1
                List<PlanComprasProductos> productosContratos = new List<PlanComprasProductos>();
                List<PlanComprasRubrosCDP> rubrosContratos = new List<PlanComprasRubrosCDP>();
                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras)
                {
                    foreach (PlanComprasProductos p in vContratoService.ObtenerProductosPlanCompras(pc.IDPlanDeComprasContratos))
                    {
                        productosContratos.Add(p);
                    }

                    foreach (PlanComprasRubrosCDP r in vContratoService.ObtenerRubrosPlanCompras(pc.IDPlanDeComprasContratos))
                    {
                        rubrosContratos.Add(r);
                    }
                }
                #endregion

                var client = new WsContratosPacco.WSContratosPACCOSoapClient();
                List<WsContratosPacco.GetDetalleProductosServicio_Result> productos = new List<WsContratosPacco.GetDetalleProductosServicio_Result>();
                List<WsContratosPacco.GetDetalleRubrosServicio_Result> rubros = new List<WsContratosPacco.GetDetalleRubrosServicio_Result>();

                foreach (PlanDeComprasContratos pc in consecutivosPlanCompras) //Recorro las vigencias y consecutivos asociados al plan de compras del contrato
                {
                    #region FiltradosProductosWebService
                    foreach (WsContratosPacco.GetDetalleProductosServicio_Result pWs in client.GetListaDetalleProductoPACCO(pc.Vigencia, pc.IDPlanDeCompras)) //Recorro y obtengo los productos obtenidos del web service
                    {
                        foreach (PlanComprasProductos p in productosContratos) // Recorro los productos obtenidos en ProductosDelPlanComprasPaso1
                        {
                            if ((p.CodigoProducto == pWs.codigo_producto) && (p.NumeroConsecutivoPlanCompras == pWs.consecutivo)
                                && (p.IdDetalleObjeto == pWs.iddetalleobjetocontractual.ToString()) && p.IsReduccion != true) // Verifico que los productosContratos coincidan con los productos del webservice
                            {
                                if (p.ValorUnitario != null)
                                    pWs.valor_unitario = p.ValorUnitario;
                                if (p.CantidadCupos != null)
                                    pWs.cantidad = p.CantidadCupos;
                                if (p.Tiempo != null)
                                    pWs.tiempo = p.Tiempo;
                                if (!string.IsNullOrEmpty(p.UnidadTiempo))
                                    pWs.tipotiempoView = p.UnidadTiempo;

                                //if (pWs.tipoProductoView.Equals("SERVICIO", StringComparison.CurrentCultureIgnoreCase))
                                //{
                                //    pWs.valor_total = pWs.cantidad * pWs.valor_unitario * pWs.tiempo;
                                //    //pWs.valor_total = p.CantidadCupos * p.ValorUnitario * p.Tiempo;
                                //}
                                //else
                                //{
                                //    pWs.valor_total = pWs.cantidad * pWs.valor_unitario;
                                //    //pWs.valor_total = p.CantidadCupos * p.ValorUnitario;
                                //}
                                //pWs.valor_total = 

                                sumatoriaProductos += pWs.valor_total;
                                productos.Add(pWs);
                            }
                        }
                    }
                    #endregion

                    #region FiltradosRubrosWebService
                    foreach (WsContratosPacco.GetDetalleRubrosServicio_Result rWs in client.GetListaDetalleRubroPACCO(pc.Vigencia, pc.IDPlanDeCompras))
                    {
                        foreach (PlanComprasRubrosCDP r in rubrosContratos)
                        {
                            if ((r.CodigoRubro == rWs.codigo_rubro) && (r.NumeroConsecutivoPlanCompras == rWs.consecutivo)
                                && (r.IdPagosDetalle == rWs.idpagosdetalle.ToString()) && r.IsReduccion != true)
                            {
                                if (r.ValorRubro != null)
                                    rWs.total_rubro = r.ValorRubro;
                                rubros.Add(rWs);
                            }
                        }
                    }
                    #endregion
                }

                TotalProductos = sumatoriaProductos.ToString();
                PrecargaValorInicialValorProductosPlanCompras();

                gvProductos.DataSource = productos;
                gvProductos.DataBind();
                nProductos = gvProductos.Rows.Count.ToString();

                gvRubrosPlanCompras.DataSource = rubros;
                gvRubrosPlanCompras.DataBind();
                #endregion

                #region ListaConsecutivoPlanCompras
                HabilitarConsecutivoPlanDeCompras(true);

                string ConsecutivoPlanComprasSeleccionado = DdlConsecutivoPlanCompras.SelectedValue;
                DdlConsecutivoPlanCompras.Items.Clear();
                foreach (PlanDeComprasContratos tD in consecutivosPlanCompras)
                {
                    DdlConsecutivoPlanCompras.Items.Add(new ListItem(tD.IDPlanDeCompras.ToString(), tD.IDPlanDeComprasContratos.ToString()));
                }
                //DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));

                if (DdlConsecutivoPlanCompras.Items.FindByValue(ConsecutivoPlanComprasSeleccionado) != null)
                    DdlConsecutivoPlanCompras.SelectedValue = ConsecutivoPlanComprasSeleccionado;
                #endregion

                // Actualizar valor actual de los aportes.
                vContratoService.ModificarAporteContratoValor(int.Parse(hfIdContrato.Value), Convert.ToDecimal(hfTotalProductos.Value), GetSessionUser().NombreUsuario);
                txtValorApoICBFTextChanged(null, EventArgs.Empty);

            }
            else
            {
                gvProductos.DataSource = null;
                gvProductos.DataBind();

                gvRubrosPlanCompras.DataSource = null;
                gvRubrosPlanCompras.DataBind();

                TotalProductos = string.Empty;
                HabilitarConsecutivoPlanDeCompras(null);
                DdlConsecutivoPlanCompras.Items.Clear();
                DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));

                // Eliminar aporte ICBF.
                vContratoService.EliminarAporteContratoValor(int.Parse(hfIdContrato.Value));
                txtValorApoICBFTextChanged(null, EventArgs.Empty);


            }
        }
        else
        {
            TotalProductos = string.Empty;
            HabilitarConsecutivoPlanDeCompras(false);
        }
    }

    private void SeleccionaConsecutivoPlanDeComprasOld(string idConsecutivoPlanDeCompras)
    {
        if (idConsecutivoPlanDeCompras.Equals("-1"))
        {
            if (chkSiManejaRecursos.Checked) 
                txtObjeto.Text = string.Empty;
            
            HabilitarObjetoContratoConvenio(false); 
        }
        else
        {
            string textoAlertavalSeleccionaConsecPlanComp = string.Empty;

            if (!vContratoService.ValidacionCoincideRegionalesContratoPlanComprasContrato(Convert.ToInt32(IdContrato), hfCodRegionalContConv.Value))
            {
                textoAlertavalSeleccionaConsecPlanComp += "Alerta: La Regional del Contrato/Convenio (Solicitante) no corresponde con el Plan de Compras seleccionado, Verifique";
            }

            if (!textoAlertavalSeleccionaConsecPlanComp.Equals(string.Empty))
            {
                DdlConsecutivoPlanCompras.SelectedIndex = 0;
                textoAlertavalSeleccionaConsecPlanComp = "alert('" + textoAlertavalSeleccionaConsecPlanComp + "')";
                if (!scriptEnCola)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "valSeleccionaConsecPlanComp", textoAlertavalSeleccionaConsecPlanComp, true);
                    scriptEnCola = true;
                }
            }

            if (!DdlConsecutivoPlanCompras.SelectedValue.Equals("-1"))
            {
                int vVigencia = 0;
                foreach (GridViewRow gvr in gvConsecutivos.Rows)
                {
                    string idPlanCompras = gvConsecutivos.DataKeys[gvr.RowIndex]["IDPlanDeCompras"].ToString();
                    if (idPlanCompras.Equals(DdlConsecutivoPlanCompras.SelectedItem.Text))
                    {
                        vVigencia = Convert.ToInt32(gvConsecutivos.DataKeys[gvr.RowIndex]["Vigencia"].ToString());
                        break;
                    }
                }

                var vCodRegional = vSIAService.ConsultarRegional(Convert.ToInt32(IdRegional)).CodigoRegional + "00";
                WsContratosPacco.WSContratosPACCOSoap _wsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
                var myGridResults = _wsContratosPacco.GetListaConsecutivosEncabezadosPACCO(vVigencia,
                                                                                           vCodRegional,
                                                                                           0,
                                                                                           0,
                                                                                           Convert.ToInt32(DdlConsecutivoPlanCompras.SelectedItem.Text),
                                                                                           null,
                                                                                           0,
                                                                                           0,
                                                                                           null);
                if (myGridResults.Count() > 0)
                {
                    AlcancePlanCompras = myGridResults[0].alcance;
                    ObjetoPlanCompras = myGridResults[0].objeto_contractual;

                }
            }

            AcordeonActivo = "3";
            HabilitarObjetoContratoConvenio(true);
        }
    }

    #endregion

    #region VigenciaValorContrato

    private void HabilitarObjetoContratoConvenio(bool habilitar)
    {
        if (habilitar)
        {
            if (chkContratoConvenioAd.Checked) // Pagina 15 Paso 62
            {
                txtObjeto.Text = ObjetoPlanCompras; //hfObjeto.Value;// Precarga info del num contrato convenio adhesion????
                txtObjeto.Enabled = false;
                IngresaObjetoContratoConvenio(txtObjeto.Text);
            }
            else
            {
                //if (chkSiManejaRecursos.Checked) Se comentarea el codigo para el contrato sin valor el if and else
                //{
                    if (gvConsecutivos.Rows.Count > 0)
                    {
                        //if (chkConvenioMarco.Checked)
                        //{
                            // Pagina 15 Paso 63.1
                            txtObjeto.Text = ObjetoPlanCompras; // hfObjeto.Value;// Pregarga info del objeto del contrato/convenio del consecutivo plan de compras contratado seleccionado
                            txtObjeto.Enabled = false;
                            IngresaObjetoContratoConvenio(txtObjeto.Text);
                        //}
                        //else
                        //{
                        //    txtObjeto.Enabled = true;
                        //}
                    }
                    else
                    {
                        txtObjeto.Text = string.Empty; //Limpieza
                        txtObjeto.Enabled = false;
                    }
                //}
                //else
                //{
                //    // Pagina 16 Paso 63.2
                //    txtObjeto.Enabled = true;
                //    if (!txtObjeto.Text.Equals(string.Empty))
                //        IngresaObjetoContratoConvenio(txtObjeto.Text);
                //}
            }
        }
        else
        {
            //txtObjeto.Text = string.Empty;
            txtObjeto.Enabled = false;
            IngresaObjetoContratoConvenio(string.Empty);
        }
    }

    private void IngresaObjetoContratoConvenio(string objetoContratoConvenio)
    {
        if (objetoContratoConvenio.Equals(string.Empty))
        {
            HabilitaAlcanceObjetoContratoConvenio(false);
        }
        else
        {
            HabilitaAlcanceObjetoContratoConvenio(true);
        }
    }

    private void HabilitaAlcanceObjetoContratoConvenio(bool habilitar)
    {
        if (habilitar)
        {
            if (chkContratoConvenioAd.Checked) // Pagina 16 Paso 66
            {
                //if (txtAlcance.Text.Equals(string.Empty) 
                if (NoPrecargarInformacion != 1)
                    txtAlcance.Text = hfAlcance.Value;// Precarga info del num contrato convenio adhesion????
                txtAlcance.Enabled = true;
                //IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
            }

            //if (chkSiManejaRecursos.Checked)Se comentarea el codigo para el contrato sin valor el if 
            //{
            //if (chkConvenioMarco.Checked)
            //{
            // Pagina 16 Paso 67.1
            txtAlcance.Text = AlcancePlanCompras; //hfAlcance.Value; // Pregarga info del alcance objeto del contrato/convenio del consecutivo plan de compras contratado seleccionado
                    //IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
                //}
            //}

            // Pagina 16 Paso 67.2
            txtAlcance.Enabled = true;
            if (!txtAlcance.Text.Equals(string.Empty) && NoPrecargarInformacion == 0)
                IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
        }
        else
        {
            txtAlcance.Text = string.Empty; //Se limpia campo de valor inicial para cuando se elimina el plan de compra, 2015-11-21 pruebas calidad
            txtAlcance.Enabled = false;
            IngresaAlcanceObjetoContratoConvenio(string.Empty);
        }
    }

    private void PrecargaValorInicialValorProductosPlanCompras()
    {
        if ((txtValorInicialContConv.Text.Equals(string.Empty) || !DdlConsecutivoPlanCompras.SelectedValue.Equals("-1")) && !string.IsNullOrEmpty(TotalProductos))
        {
            //try
            //{
            NumberFormatInfo nfi = new CultureInfo("en-US", true).NumberFormat;
            nfi.NumberDecimalDigits = 2;
            nfi.NumberDecimalSeparator = ",";
            nfi.NumberGroupSeparator = "";
            decimal totalProductos = decimal.Parse(TotalProductos);
            txtValorInicialContConv.Text = totalProductos.ToString("#,###0.00##;($ #,###0.00##)");//totalProductos.ToString("n", nfi); // Pagina 16 paso 70.1
            txtValorInicialContConv.Enabled = false;
            ValorInicial = txtValorInicialContConv.Text;
            hddValorInicial.Value = ValorInicial;
            //} catch (Exception e)
            //{

            //}
        }
    }

    private void IngresaAlcanceObjetoContratoConvenio(string alcanceObjetoContratoConvenio)
    {
        //if (alcanceObjetoContratoConvenio.Equals(string.Empty))
        //{
        //    HabilitaValorInicialContConv(false);
        //}
        //else
        //{
                PrecargaValorInicialValorProductosPlanCompras();
                
                decimal sumatoriaProductos = 0;
                if (!TotalProductos.Equals(string.Empty))
                    sumatoriaProductos = Convert.ToDecimal(TotalProductos);


                if (NoPrecargarInformacion != 1) //SE COMENTA YA QUE AL PRECARGAR VALOR INICIAL CON VALOR DE LOS PRODUCTOS NO HABILITA EL SIGUIENTE CAMPO
                {
                    if (ValidacionValorInicialContConv(sumatoriaProductos)) // Pagina 16 paso 73 valor inicial no puede superar el 0.0025 del total de los productos
                    {
                        HabilitaFechaInicioEjecucionContConv(true);
                    }
                    else
                    {
                        string textoAlertavalInicialContConv = string.Empty;
                    }
                }
        //}
    }

    private void HabilitaValorInicialContConv(bool? habilitar)
    {
        if (habilitar == null)
        {
            txtValorInicialContConv.Text = string.Empty;
            txtValorInicialContConv.Enabled = false;
            IngresaValorInicialContConv(string.Empty);
        }
        else if (habilitar == true)
        {
            txtValorInicialContConv.Enabled = true;
            if (NoPrecargarInformacion == 0 && !txtValorInicialContConv.Text.Equals(string.Empty))
                IngresaValorInicialContConv(txtValorInicialContConv.Text);
        }
        else
        {
            txtValorInicialContConv.Text = string.Empty;//Se limpia campo de valor inicial para cuando se elimina el plan de compra, 2015-11-21 pruebas calidad            
            txtValorInicialContConv.Enabled = false;
            IngresaValorInicialContConv(string.Empty);
        }
    }

    private bool ValidacionValorInicialContConv(decimal sumatoriaProductos)
    {
        // desarrollo validacion Paso 73 el valor inicial no puede superar el 0.0025 del total de los productos
        decimal tope = sumatoriaProductos + (sumatoriaProductos * Convert.ToDecimal(0.000025));
        decimal valorInicial = 0;
        if (!txtValorInicialContConv.Text.Equals(string.Empty))
        {
            //NumberFormatInfo nfi = new CultureInfo("en-US", true).NumberFormat;
            //nfi.NumberDecimalDigits = 2;
            //nfi.NumberDecimalSeparator = ",";
            //nfi.NumberGroupSeparator = "";
            valorInicial = decimal.Parse(txtValorInicialContConv.Text);
            //valorInicial = Convert.ToDecimal(txtValorInicialContConv.Text, new CultureInfo("en-US"));
        }
            
        if (valorInicial > tope)
            return false;
        else
            return true;
    }

    private void IngresaValorInicialContConv(string valorInicialContConv)
    {
        if (valorInicialContConv.Equals(string.Empty))
        {
            HabilitaFechaInicioEjecucionContConv(false);
        }
        else
        {
            string textoAlertavalInicialContConv = string.Empty;

            //if (chkSiManejaRecursos.Checked) Se comentarea el codigo para el contrato sin valor el if 
            //{
                decimal sumatoriaProductos = 0;
                if (!TotalProductos.Equals(string.Empty))
                    sumatoriaProductos = Convert.ToDecimal(TotalProductos);

                //if (!ValidacionValorInicialContConv(sumatoriaProductos)) // Pagina 16 paso 73
                //{
                //    textoAlertavalInicialContConv = "El Valor Inicial del Contrato/Convenio no puede superar el 0.0025% del Valor Total de los Productos\\n";
                //}
            //}

            if (chkConvenioMarco.Checked) // Pagina 17 Paso 74
            {
                if (!ValidacionValorInicialSuperaValorFinal(decimal.Parse(valorInicialContConv)))
                {
                    textoAlertavalInicialContConv += "El Valor Inicial Contrato/Convenio supera el Valor Final del Contrato/Convenio del Convenio Marco Asociado\\n";
                }
            }

            //if (chkSiManejaAportes.Checked)
            //{
            //    decimal valorinicicialcontrato = decimal.Parse(valorInicialContConv);
            //    decimal acomuladoaportes = vContratoService.ConsultarAporteContratos(null, null, null, null, null, Convert.ToInt32(IdContrato), null, null, null, null).Sum(d => d.ValorAporte);
            //    if (acomuladoaportes > valorinicicialcontrato)
            //    {
            //        textoAlertavalInicialContConv += "El Valor de los Aportes supera el Valor Final del Contrato/Convenio \\n";
            //    }
            //}

            if (textoAlertavalInicialContConv.Equals(string.Empty))
            {
                HabilitaFechaInicioEjecucionContConv(true);
            }
            else
            {
                //txtValorInicialContConv.Text = string.Empty;
                ///textoAlertavalInicialContConv = "alert('" + textoAlertavalInicialContConv + "')";
                textoAlertavalInicialContConv = "mensajeAlerta('" + txtValorInicialContConv.ClientID + "', '" + textoAlertavalInicialContConv + "')";
                if (!scriptEnCola)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "valInicialContConv", textoAlertavalInicialContConv, true);
                    scriptEnCola = true;
                }
                HabilitaFechaInicioEjecucionContConv(false);
                txtValorInicialContConv.Focus();
            }

        }
    }

    private bool ValidacionValorInicialSuperaValorFinal(decimal valorInicialContConv)
    {
        if (!IdContratoAsociado.Equals(string.Empty))
        {
            int idContratoConvMarco = Convert.ToInt32(IdContratoAsociado);
            return vContratoService.ValidacionValorInicialSuperaValorFinal(idContratoConvMarco, valorInicialContConv, Convert.ToInt32(IdContrato)); // validación paso 74 
        }
        else
        {
            return false;
        }
    }

    private void HabilitaFechaInicioEjecucionContConv(bool? habilitar)
    {
        if (habilitar == null)
        {
            caFechaInicioEjecucion.Enabled = false;
            caFechaInicioEjecucion.InitNull = true;
        }
        else if (habilitar == true)
        {
            caFechaInicioEjecucion.Enabled = true;

            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (!caFechaInicioEjecucion.Date.ToShortDateString().Equals(string.Empty) && !caFechaInicioEjecucion.Date.ToShortDateString().Substring(0, 10).Equals("01/01/1900"))
            if (!caFechaInicioEjecucion.Date.ToShortDateString().Equals(string.Empty) && Convert.ToDateTime(caFechaInicioEjecucion.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
                SeleccionaFechaInicioEjecucion(caFechaInicioEjecucion.Date.ToShortDateString());
        }
        else
        {
            caFechaInicioEjecucion.Enabled = false;
            SeleccionaFechaInicioEjecucion(string.Empty);
        }
    }

    private void SeleccionaFechaInicioEjecucion(string fechaInicioEjecucionSeleccionada)
    {
        //Jorge Vizcaino: Se cambiala forma de comparar la fecha
        //if (fechaInicioEjecucionSeleccionada.Equals(string.Empty) || fechaInicioEjecucionSeleccionada.Substring(0, 10).Equals("01/01/1900"))
        if (fechaInicioEjecucionSeleccionada.Equals(string.Empty) || Convert.ToDateTime(fechaInicioEjecucionSeleccionada) == Convert.ToDateTime("01/01/1900"))
        {
            HabilitaFechaFinalizacionInicial(false);
            chkCalculaFecha.Enabled = false;
            ddlVigenciaFiscalIni.SelectedValue = null;
        }
        else
        {
            string textValFechaInicioEjec = ValidacionesFechaInicioEjecucion(caFechaInicioEjecucion.Date);
            if (textValFechaInicioEjec.Equals(string.Empty))
            {
                HabilitaFechaFinalizacionInicial(true);
                chkCalculaFecha.Enabled = true;// Pagina 18 Paso 84
                ddlVigenciaFiscalIni.SelectedValue = null;
                if (ddlVigenciaFiscalIni.Items.FindByText(caFechaInicioEjecucion.Date.Year.ToString()) != null)
                {
                    ddlVigenciaFiscalIni.Items.FindByText(caFechaInicioEjecucion.Date.Year.ToString()).Selected = true;
                    SeleccionaVigenciaFiscalInicial(ddlVigenciaFiscalIni.SelectedValue);
                }
            }
            else
            {
                HabilitaFechaFinalizacionInicial(false);
                chkCalculaFecha.Enabled = false;
                caFechaInicioEjecucion.InitNull = true;
                txtAcnosPiEj.Text = string.Empty;
                txtMesesPiEj.Text = string.Empty;
                txtDiasPiEj.Text = string.Empty;
                ddlVigenciaFiscalIni.SelectedValue = null;
                if (!scriptEnCola)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "valFechaIniEjec", "alert('" + textValFechaInicioEjec + "')", true);
                    scriptEnCola = true;
                }
            }
        }
    }

    private string ValidacionesFechaInicioEjecucion(DateTime? fechaInicioEjecucionSeleccionada)
    {
        string textValFechaInicioEjec = string.Empty;
        DateTime unAcnooMayorHoy = new DateTime(DateTime.Today.Year + 1, DateTime.Today.Month, DateTime.Today.Day);
        DateTime unAcnoMenorHoy = new DateTime(DateTime.Today.Year - 1, DateTime.Today.Month, DateTime.Today.Day);

        if (fechaInicioEjecucionSeleccionada < unAcnoMenorHoy || fechaInicioEjecucionSeleccionada > unAcnooMayorHoy) // Paso 78
        {
            textValFechaInicioEjec = "La fecha ingresada debe estar dentro del rango de un año menos y un año más del año actual\\n";
        }

        if (!txtNumeroProceso.Text.Equals(string.Empty)) // Paso 79
        {
            DateTime fechaAdjudicacionProceso = caFechaAdjudicaProceso.Date;
            if (fechaInicioEjecucionSeleccionada < fechaAdjudicacionProceso)
            {
                textValFechaInicioEjec += "La fecha ingresada debe ser mayor o igual a la Fecha de Adjudicación del Proceso\\n";
            }
        }

        if (chkConvenioMarco.Checked) // Paso 80 y 81
        {
            if (!hfFechaInicioEjecConvMarco.Value.Equals(string.Empty))
            {
                DateTime fechaInicioEjecConvMarco = Convert.ToDateTime(hfFechaInicioEjecConvMarco.Value);
                if (fechaInicioEjecucionSeleccionada < fechaInicioEjecConvMarco)
                {
                    textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser mayor o igual a la Fecha de Inicio de Ejecución del Convenio Marco\\n";
                }
            }

            //try
            //{
            if (!hfFechaSuscripcionContConvMarco.Value.Equals(string.Empty))
            {
                DateTime fechaSuscripcionContConvMarco = Convert.ToDateTime(hfFechaSuscripcionContConvMarco.Value);
                if (fechaInicioEjecucionSeleccionada < fechaSuscripcionContConvMarco)
                {
                    textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser mayor o igual a la Fecha de suscripción del Convenio Marco\\n"; // Requerimiento especial 23
                }
            }
            //}
            //catch (Exception e)
            //{
            //    throw new Exception("El Convenio Marco no tiene una fecha de suscripción");
            //}

            if (!hfFechaFinalTerminContConvMarco.Value.Equals(string.Empty))
            {
                DateTime fechaFinalTermContConvMarco = Convert.ToDateTime(hfFechaFinalTerminContConvMarco.Value);
                if (fechaInicioEjecucionSeleccionada > fechaFinalTermContConvMarco)
                {
                    textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Convenio Marco\\n";
                }
            }
        }
        else if (chkContratoConvenioAd.Checked) // Paso 82 y 83
        {
            if (!hfFechaInicioEjecContConvAd.Value.Equals(string.Empty))
            {
                DateTime fechaInicioEjecContConvAd = Convert.ToDateTime(hfFechaInicioEjecContConvAd.Value);
                if (fechaInicioEjecucionSeleccionada < fechaInicioEjecContConvAd)
                {
                    textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser mayor o igual a la Fecha de Inicio de Ejecución del Contrato/Convenio Adhesión\\n";
                }
            }

            if (!hfFechaFinalTerminContConvAd.Value.Equals(string.Empty))
            {
                DateTime fechaFinalTerminContConvAd = Convert.ToDateTime(hfFechaFinalTerminContConvAd.Value);
                if (fechaInicioEjecucionSeleccionada > fechaFinalTerminContConvAd)
                {
                    textValFechaInicioEjec += "La fecha de inicio de Ejecución debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Contrato/Convenio Adhesión\\n";
                }
            }
        }

        return textValFechaInicioEjec;
    }

    private void HabilitaFechaFinalizacionInicial(bool? habilitar)
    {
        if (habilitar == null)
        {
            txtFechaFinalTerminacion.Text = string.Empty;
            caFechaFinalizacionInicial.Enabled = false;
            caFechaFinalizacionInicial.InitNull = true;
            SeleccionaFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date.ToShortDateString());
        }
        else if (habilitar == true)
        {
            caFechaFinalizacionInicial.Enabled = true;
            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (NoPrecargarInformacion == 0 &&
            //    !caFechaFinalizacionInicial.Date.ToShortDateString().Equals(string.Empty) &&
            //    !caFechaFinalizacionInicial.Date.ToShortDateString().Substring(0, 10).Equals("01/01/1900"))
            if (NoPrecargarInformacion == 0 &&
                !caFechaFinalizacionInicial.Date.ToShortDateString().Equals(string.Empty) &&
                Convert.ToDateTime(caFechaFinalizacionInicial.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
            {
                SeleccionaFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date.ToShortDateString());
            }
        }
        else
        {
            caFechaFinalizacionInicial.Enabled = false;
            LimpiarPlazoInicialEjecucion();
            SeleccionaFechaFinalizacionInicialContConv(string.Empty);
        }
    }

    private void SeleccionaFechaFinalizacionInicialContConv(string fechaFinalizacionInicialContConvSeleccionada)
    {
        if (fechaFinalizacionInicialContConvSeleccionada.Equals(string.Empty))
        {
            HabilitarVigenciasFuturas(false);
        }
        else
        {
            if (Convert.ToDateTime(fechaFinalizacionInicialContConvSeleccionada) == Convert.ToDateTime("01/01/1900"))
            {
                HabilitarVigenciasFuturas(false);
            }
            else
            {
                string textValFechaFinalIniciContConv = ValidacionesFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date);

                if (textValFechaFinalIniciContConv.Equals(string.Empty))
                {
                    if (!chkCalculaFecha.Checked)
                    {
                    string diferenciaFechas = string.Empty;
                    ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion.Date, caFechaFinalizacionInicial.Date, out diferenciaFechas);

                    var items = diferenciaFechas.Split('|');

                    txtDiasPiEj.Text = items[2];
                    txtMesesPiEj.Text = items[1];
                    txtAcnosPiEj.Text = items[0];
                    }
                    else
                    {
                        txtAcnosPiEj.Text = "0";
                        if (!string.IsNullOrEmpty(GetSessionParameter("CalculoFecha.Dias").ToString()))
                        {
                            txtDiasPiEj.Text = GetSessionParameter("CalculoFecha.Dias").ToString();
                            RemoveSessionParameter("CalculoFecha.Dias");
                        }
                        if (!string.IsNullOrEmpty( GetSessionParameter("CalculoFecha.Meses").ToString()))
                        {
                            txtMesesPiEj.Text = GetSessionParameter("CalculoFecha.Meses").ToString();
                            RemoveSessionParameter("CalculoFecha.Meses");
                        }
                    }

                    txtFechaFinalTerminacion.Text = caFechaFinalizacionInicial.Date.ToShortDateString();
                    HabilitarVigenciasFuturas(true);
                }
                else
                {
                    LimpiarPlazoInicialEjecucion();
                    HabilitarVigenciasFuturas(false);
                    caFechaFinalizacionInicial.InitNull = true;
                    if (!scriptEnCola)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "valFechaFinIniContConv",
                            "alert('" + textValFechaFinalIniciContConv + "')", true);
                        scriptEnCola = true;
                    }
                }
            }
        }
    }

    private string ValidacionesFechaFinalizacionInicialContConv(DateTime fechaFinalizacionInicialContConvSeleccionada)
    {
        string textValFechaFinalIniciContConv = string.Empty;

        if (fechaFinalizacionInicialContConvSeleccionada < caFechaInicioEjecucion.Date)
        {
            textValFechaFinalIniciContConv += "La fecha de Finalización Inicial debe ser mayor o igual a la Fecha de Inicio de Ejecución\\n";
        }

        string idTipoContratoConvenio = ddlTipoContratoConvenio.SelectedValue.ToString();

        if ( 
            (idTipoContratoConvenio == IdTipoContConvPrestServProfesionales || idTipoContratoConvenio == IdTipoContConvPrestServApoyoGestion) && 
            fechaFinalizacionInicialContConvSeleccionada.Year > caFechaInicioEjecucion.Date.Year
           )
        {
            textValFechaFinalIniciContConv += "La Fecha final del contrato debe estar dentro de la misma vigencía de la fecha inicial\\n";
        }

        if (chkConvenioMarco.Checked) // Paso 88
        {
            if (!hfFechaFinalTerminContConvMarco.Value.Equals(string.Empty))
            {
                DateTime fechaFinalTermContConvMarco = Convert.ToDateTime(hfFechaFinalTerminContConvMarco.Value);
                if (fechaFinalizacionInicialContConvSeleccionada > fechaFinalTermContConvMarco)
                {
                    textValFechaFinalIniciContConv += "La Fecha de Finalización Inicial del contrato/convenio debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Convenio Marco\\n";
                }
            }
        }
        else if (chkContratoConvenioAd.Checked) // Paso 89
        {
            if (!hfFechaFinalTerminContConvAd.Value.Equals(string.Empty))
            {
                DateTime fechaFinalTerminContConvAd = Convert.ToDateTime(hfFechaFinalTerminContConvAd.Value);
                if (fechaFinalizacionInicialContConvSeleccionada > fechaFinalTerminContConvAd)
                {
                    textValFechaFinalIniciContConv += "La fecha de Finalización Inicial del contrato/convenio debe ser menor o igual a la Fecha Final de Terminación Contrato/Convenio del Contrato/Convenio Adhesión\\n";
                }
            }
        }




        return textValFechaFinalIniciContConv;
    }

    private void LimpiarPlazoInicialEjecucion()
    {
        txtFechaFinalTerminacion.Text = string.Empty;
        txtDiasPiEj.Text = string.Empty;
        txtMesesPiEj.Text = string.Empty;
        txtAcnosPiEj.Text = string.Empty;
    }

    private void HabilitarVigenciasFuturas(bool? habilitar)
    {
        if (habilitar == null)
        {
            chkSiManejaVigFuturas.Checked = false;
            chkSiManejaVigFuturas.Enabled = false;
            chkNoManejaVigFuturas.Checked = false;
            chkNoManejaVigFuturas.Enabled = false;
            //cvManejaVigFut.Enabled = true;
            HabilitarVigenciaFiscal(false);
            HabilitarSeccionVigenciaFuturas(false);
            LimpiarPlazoInicialEjecucion();
        }
        else if (habilitar == true)
        {
            chkSiManejaVigFuturas.Enabled = true;
            chkNoManejaVigFuturas.Enabled = true;
            //cvManejaVigFut.Enabled = true;
            if (chkSiManejaVigFuturas.Checked || chkNoManejaVigFuturas.Checked)
            {
                HabilitarVigenciaFiscal(true);
                if (int.Parse(AcordeonActivo) == 9)
                    HabilitarSeccionVigenciaFuturas(true);
            }
        }
        else
        {
            chkSiManejaVigFuturas.Enabled = false;
            chkNoManejaVigFuturas.Enabled = false;
            //cvManejaVigFut.Enabled = true;
            HabilitarVigenciaFiscal(false);
            HabilitarSeccionVigenciaFuturas(false);
        }
    }

    private void HabilitarVigenciaFiscal(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlVigenciaFiscalIni.Enabled = false;
            ddlVigenciaFiscalFin.Enabled = false;
            ddlVigenciaFiscalIni.SelectedIndex = 0;
            ddlVigenciaFiscalFin.SelectedIndex = 0;
            //txtVigenciaFiscalIni.Text = string.Empty;
            //txtVigenciaFiscalFin.Text = string.Empty;
            //cvVigenciaFiscalIni.Enabled = false;
            //cvVigenciaFiscalFin.Enabled = false;
            SeleccionaVigenciaFiscalInicial(ddlVigenciaFiscalIni.SelectedValue);
            SeleccionaVigenciaFiscalFinal(ddlVigenciaFiscalFin.SelectedValue);
            HabilitarFormaPago(false);
        }
        else if (habilitar == true)
        {
            //ddlVigenciaFiscalIni.Enabled = true;
            //ddlVigenciaFiscalFin.Enabled = true;
            //cvVigenciaFiscalIni.Enabled = true;
            //cvVigenciaFiscalFin.Enabled = true;
            //txtVigenciaFiscalIni.Text = caFechaInicioEjecucion.Date.Year.ToString();
            //txtVigenciaFiscalFin.Text = caFechaFinalizacionInicial.Date.Year.ToString();
            ddlVigenciaFiscalIni.SelectedValue = null;
            if (ddlVigenciaFiscalIni.Items.FindByText(caFechaFinalizacionInicial.Date.Year.ToString()) != null)
                ddlVigenciaFiscalIni.Items.FindByText(caFechaInicioEjecucion.Date.Year.ToString()).Selected = true;
            ddlVigenciaFiscalFin.SelectedValue = null;
            if (ddlVigenciaFiscalFin.Items.FindByText(caFechaFinalizacionInicial.Date.Year.ToString()) != null)
                ddlVigenciaFiscalFin.Items.FindByText(caFechaFinalizacionInicial.Date.Year.ToString()).Selected = true;
            if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1") && !ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
                HabilitarFormaPago(true);
        }
        else
        {
            ddlVigenciaFiscalIni.Enabled = false;
            ddlVigenciaFiscalFin.Enabled = false;
            //cvVigenciaFiscalIni.Enabled = true;
            //cvVigenciaFiscalFin.Enabled = true;
            HabilitarFormaPago(false);
        }
    }

    private void SeleccionaVigenciaFiscalInicial(string vigenciaFiscalInicialSeleccionada)
    {
        if (!vigenciaFiscalInicialSeleccionada.Equals("-1"))
        {
            if (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) < caFechaInicioEjecucion.Date.Year ||
                (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) > (caFechaInicioEjecucion.Date.Year + 1))) // Paso 98
            {
                ddlVigenciaFiscalIni.SelectedIndex = 0;
                string textValVigeFiscIni = "La Vigencia Fiscal Inicial del Contrato/Convenio debe ser Mayor o Igual al año de la Fecha de Inicio de Ejecución del contrato/Convenio";
                textValVigeFiscIni = "mensajeAlerta('" + ddlVigenciaFiscalIni.ClientID + "', '" + textValVigeFiscIni + "')";
                if (!scriptEnCola)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "valVigeFiscIni", textValVigeFiscIni, true);
                    scriptEnCola = true;
                }
            }
            else
            {
                //if (chkSiManejaVigFuturas.Checked && !ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
                //{
                    ValidacionVigenciasFiscales(ddlVigenciaFiscalIni);
                //}
            }
        }
        else
        {
            HabilitarFormaPago(false);
        }
    }

    private void SeleccionaVigenciaFiscalFinal(string vigenciaFiscalFinalSeleccionada)
    {
        if (!vigenciaFiscalFinalSeleccionada.Equals("-1") && !ddlVigenciaFiscalIni.SelectedValue.Equals("-1"))
        {
            //if (chkSiManejaVigFuturas.Checked)
            //{
                ValidacionVigenciasFiscales(ddlVigenciaFiscalFin);
                //if (int.Parse(ddlVigenciaFiscalFin.SelectedItem.Text) <= int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text)) // Paso 101
                //{
                //    HabilitarFormaPago(false);
                //    ddlVigenciaFiscalFin.SelectedIndex = 0;
                //    string textValVigeFiscFin = "La Vigencia Fiscal Final del Contrato/Convenio debe ser Mayor a la Vigencia Fiscal Inicial del Contrato/Convenio";
                //    textValVigeFiscFin = "mensajeAlerta('" + ddlVigenciaFiscalIni.ClientID + "', '" + textValVigeFiscFin + "')";
                //    if (!scriptEnCola)
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "valVigeFiscFin", textValVigeFiscFin, true);
                //        scriptEnCola = true;
                //    }
                //}
                //else
                //{
                //    HabilitarFormaPago(true); // Paso 103
                //}
            //}
            //else
            //{
            //    HabilitarFormaPago(true); // ??????
            //}
        }
        else
        {
            HabilitarFormaPago(false);
        }
    }

    private void ValidacionVigenciasFiscales(DropDownList ddlVigencia)
    {
        if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1") && !ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
        {
            if (chkSiManejaVigFuturas.Checked)
            {
                if (int.Parse(ddlVigenciaFiscalFin.SelectedItem.Text) <= int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text)) // Paso 101
                {
                    HabilitarFormaPago(false);
                    if (ddlVigencia != null)
                        ddlVigencia.SelectedIndex = 0;
                    string textValVigeFiscFin = "La Vigencia Fiscal Final del Contrato/Convenio debe ser Mayor a la Vigencia Fiscal Inicial del Contrato/Convenio";
                    textValVigeFiscFin = "mensajeAlerta('" + ddlVigenciaFiscalIni.ClientID + "', '" + textValVigeFiscFin + "')";
                    if (!scriptEnCola)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "valVigeFiscIni", textValVigeFiscFin, true);
                        scriptEnCola = true;
                    }
                }
                else
                {
                    HabilitarFormaPago(true); // Paso 103
                    HabilitarContratista(true);                    
                }
            }
            else
            {
                if (int.Parse(ddlVigenciaFiscalFin.SelectedItem.Text) < int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text)) // Paso 102
                {
                    HabilitarFormaPago(false);
                    if (ddlVigencia != null)
                        ddlVigencia.SelectedIndex = 0;
                    string textValVigeFiscFin = "La Vigencia Fiscal Final del Contrato/Convenio debe ser Mayor o Igual a la Vigencia Fiscal Inicial del Contrato/Convenio";
                    textValVigeFiscFin = "mensajeAlerta('" + ddlVigenciaFiscalIni.ClientID + "', '" + textValVigeFiscFin + "')";
                    if (!scriptEnCola)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "valVigeFiscIni", textValVigeFiscFin, true);
                        scriptEnCola = true;
                    }
                }
                else
                {
                    HabilitarFormaPago(true); // Paso 103
                }
            }
        }
        else
        {
            HabilitarFormaPago(false);
        }
    }

    private void HabilitarFormaPago(bool? habilitar)
    {
        if (habilitar == null)
        {
            ddlFormaPago.Enabled = false;
            //cvFormaPago.Enabled = true;
            ddlFormaPago.SelectedIndex = 0;
            SeleccionaFormaPago(ddlFormaPago.SelectedValue);
        }
        else if (habilitar == true)
        {
            ddlFormaPago.Enabled = true;
            ddlTipoFormaPago.Enabled = true;
            //cvFormaPago.Enabled = true;
            if (!ddlFormaPago.SelectedValue.Equals("-1") && NoPrecargarInformacion == 0)
                SeleccionaFormaPago(ddlFormaPago.SelectedValue);
        }
        else
        {
            ddlFormaPago.Enabled = false;
            //cvFormaPago.Enabled = true;
            SeleccionaFormaPago("-1");
        }

        if(chkNoManejaRecursos.Checked && hfContratoSV.Value == "true")
        {
            ddlFormaPago.Enabled = false;
            ddlTipoFormaPago.Enabled = false;
            AcordeonActivo = "4";
            SeleccionarContratistas(string.Empty);
            HabilitarContratista(true);
            SeleccionaLugarEjecucion(string.Empty);
        }
    }

    private  void HabilitarTipoFormaPapago(bool habilitar)
    {
        ddlTipoFormaPago.Enabled = true;

        if (ddlFormaPago.SelectedValue == "1")
        {
            //txtValorAnticipo.Enabled = false;
            txtPorcentajeAnticipo.Enabled = false;
        }
        else
        {
            //txtValorAnticipo.Enabled = true;
            txtPorcentajeAnticipo.Enabled = true;
            // txtValorAnticipo.Text = string.Empty;
        }

        if (chkNoManejaRecursos.Checked && hfContratoSV.Value == "true")
        {
            txtPorcentajeAnticipo.Enabled = false;
            HabilitarContratista(true);
            AcordeonActivo = "4";
        }
    }

    private void HabilitarValorAnticipo()
    {
        AcordeonActivo = "4";
        HabilitarContratista(true);
        SeleccionarContratistas(string.Empty);
    }

    private void SeleccionaFormaPago(string formaPagoSeleccionada)
    {
        if (formaPagoSeleccionada.Equals("-1"))
        {
            HabilitarContratista(false);
            HabilitarAportes(false);
            HabilitarLugarEjecucion(false);
            ddlTipoFormaPago.Enabled = false;
            txtPorcentajeAnticipo.Enabled = false;
            //txtValorAnticipo.Text = string.Empty;
            txtPorcentajeAnticipo.Text = string.Empty;    
        }
        else
        {
            HabilitarTipoFormaPapago(true);
            //HabilitarValorAnticipo();
        }
    }

    private void SeleccionarTipoFormaPago( string tipoFormaPagoSeleccionada)
    {
        if (tipoFormaPagoSeleccionada.Equals("-1"))
        {
            HabilitarContratista(false);
            HabilitarAportes(false);
            HabilitarLugarEjecucion(false);
        }
        else
        {
            if (ddlFormaPago.SelectedValue == "1")
            {
                AcordeonActivo = "4";
                HabilitarContratista(true);
                SeleccionarContratistas(string.Empty);
                //txtValorAnticipo.Text = string.Empty;
                txtPorcentajeAnticipo.Text = string.Empty;
            }

            HabilitarValorAnticipo();
            

            //if (chkSiManejaAportes.Checked) // Página 21 Paso 122
            //{
            //    AcordeonActivo = "4";
            //    HabilitarAportes(true);
            //    SeleccionarAportes(string.Empty);
            //}
            //else //if (chkNoManejaAportes.Checked)
            //{
            //    AcordeonActivo = "5";
            //    HabilitarAportes(false);
            //    HabilitarLugarEjecucion(true);// Pagina 20 Paso 106
            //}
        }
    }

    #endregion

    #region Contratista

    private void HabilitarContratista(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgContratista.Enabled = false;
            gvContratistas.DataSource = null;
            gvContratistas.DataBind();
            nContratistas = "0";
            //HabilitarSupervisorInterventor(false);
            HabilitarAportes(false);
            gvIntegrantesConsorcio_UnionTemp.DataSource = null;
            gvIntegrantesConsorcio_UnionTemp.DataBind();
            //cvContratistas.Enabled = false;
        }
        else if (habilitar == true)
        {
            imgContratista.Enabled = true;
            gvContratistas.Enabled = true;
            //cvContratistas.Enabled = true;
            //if (int.Parse(AcordeonActivo) > 6)
                //HabilitarSupervisorInterventor(true);
            if (int.Parse(AcordeonActivo) > 4)
            {
                HabilitarAportes(true);
                if (chkSiManejaAportes.Checked) // Página 21 Paso 122
                {
                    AcordeonActivo = "5";
                    HabilitarAportes(true);
                    SeleccionarAportes(string.Empty);
                }
                else //if (chkNoManejaAportes.Checked)
                {
                    AcordeonActivo = "6";
                    HabilitarAportes(false);
                    HabilitarLugarEjecucion(true);// Pagina 20 Paso 106
                }
                
            }
        }
        else
        {
            imgContratista.Enabled = false;
            gvContratistas.Enabled = false;
            gvIntegrantesConsorcio_UnionTemp.Enabled = false;
            //cvContratistas.Enabled = true;
            //HabilitarSupervisorInterventor(false);
            HabilitarAportes(false);
        }
    }

    private void SeleccionarContratistas(string contratistaSeleccionado)
    {
        List<Proveedores_Contratos> contratistas = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(IdContrato), null);
        gvContratistas.DataSource = contratistas;
        gvContratistas.DataBind();
        nContratistas = gvContratistas.Rows.Count.ToString();

        List<Proveedores_Contratos> integrantesConsorcioUnionTemp = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(IdContrato), 1);
        gvIntegrantesConsorcio_UnionTemp.DataSource = integrantesConsorcioUnionTemp;
        gvIntegrantesConsorcio_UnionTemp.DataBind();

        if (contratistas.Count() == 0)
        {
            //HabilitarSupervisorInterventor(false);
            HabilitarAportes(false);
            HabilitarLugarEjecucion(false);
        }
        else
        {
            //AcordeonActivo = "7";
            //HabilitarSupervisorInterventor(true);
            //SeleccionarSupervisorInterventor(string.Empty);

            if (chkSiManejaAportes.Checked) // Página 21 Paso 122
            {
                AcordeonActivo = "5";
                HabilitarAportes(true);
                SeleccionarAportes(string.Empty);
            }
            else //if (chkNoManejaAportes.Checked)
            {
                AcordeonActivo = "6";
                HabilitarAportes(false);
                HabilitarLugarEjecucion(true);// Pagina 20 Paso 106
            }

        }
    }

    private void EliminarContratista(int rowIndex)
    {
        Proveedores_Contratos vProveedor_Contrato = new Proveedores_Contratos();
        vProveedor_Contrato.IdProveedoresContratos = Convert.ToInt32(gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"]);
        vProveedor_Contrato.IdContrato = Convert.ToInt32(IdContrato);
        vContratoService.EliminarProveedores_Contratos(vProveedor_Contrato);
        SeleccionarContratistas(string.Empty);
        CargarAportes(IdContrato);
    }

    #endregion

    #region Aportes

    private void HabilitarAportes(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgValorApoICBF.Enabled = false;
            //imgValorApoContrat.Enabled = false;
            txtValorApoICBF.Text = string.Empty;
            txtValorApoContrat.Text = string.Empty;
            //gvAportesICBF.DataSource = null;
            //gvAportesICBF.DataBind();
            gvAportesContratista.DataSource = null;
            gvAportesContratista.DataBind();
            nAportes = "0";
            //cvAportes.Enabled = false;
        }
        else if (habilitar == true)
        {
            
            
            gvAportesICBF.Enabled = true;
            gvAportesContratista.Enabled = true;

            if (Convert.ToBoolean(rblAportesEspecie.SelectedValue) == true)
            {
                //imgValorApoContrat.Enabled = false;
                imgValorApoICBF.Enabled = true;
                // imgValorApoContrat.Visible = true;
            }
            else
            {
                //imgValorApoContrat.Enabled = true;
                imgValorApoICBF.Enabled = false;
            }

            if (chkSiManejaAportes.Checked)
            {
                imgValorApoContrat.Enabled = true;
            }
            else
            {
                imgValorApoContrat.Enabled = false;
            }
            //cvAportes.Enabled = true;
        }
        else
        {
            //imgValorApoICBF.Enabled = false;
            //imgValorApoContrat.Enabled = false;
            gvAportesICBF.Enabled = false;
            gvAportesContratista.Enabled = false;

            if (!string.IsNullOrEmpty(rblAportesEspecie.SelectedValue))
            {
                if (Convert.ToBoolean(rblAportesEspecie.SelectedValue) == true)
                {
                    //imgValorApoContrat.Enabled = false;
                    imgValorApoICBF.Enabled = true;
                    // imgValorApoContrat.Visible = true;
                }
                else
                {
                    //imgValorApoContrat.Enabled = true;
                    imgValorApoICBF.Enabled = false;
                }
            }

            if (chkSiManejaAportes.Checked)
            {
                imgValorApoContrat.Enabled = true;
            }
            else
            {
                imgValorApoContrat.Enabled = false;
            }
            //if (chkSiManejaAportes.Checked)
            //{
            //    cvAportes.Enabled = true;
            //}
            //else
            //{
            //    cvAportes.Enabled = false;
            //}
        }
    }

    private void SeleccionarAportes(string idAporteSeleccionado)
    {
        CargarAportes(IdContrato);
    }

    private void CargarAportes(string IdContrato)
    {
        int numAportes = 0;
        int numAportesContratista = 0;
        
            List<AporteContrato> aportes = vContratoService.ObtenerAportesContrato(true, Convert.ToInt32(IdContrato), null, null);

        if (rblAportesEspecie.SelectedValue.ToString() == "False" && aportes != null)
            aportes = aportes.Where(e => e.AportanteICBF == true && e.AporteEnDinero == true).ToList();

            gvAportesICBF.DataSource = aportes;
            gvAportesICBF.DataBind();

            txtValorApoICBF.Text = string.Format("{0:$#,##0}", aportes.Sum(a => a.ValorAporte));

            numAportes += aportes.Count();
        

        if (chkSiManejaAportes.Checked)
        {
            List<AporteContrato> aportesContratista = vContratoService.ObtenerAportesContrato(false, Convert.ToInt32(IdContrato), null, null);
            gvAportesContratista.DataSource = aportesContratista;
            gvAportesContratista.DataBind();

            txtValorApoContrat.Text = string.Format("{0:$#,##0}", aportesContratista.Sum(a => a.ValorAporte));

            numAportes += aportesContratista.Count();
            numAportesContratista += aportesContratista.Count();
            nAportes = numAportes.ToString();
        }
        
        if (numAportesContratista > 0)
        {
            HabilitarLugarEjecucion(true);
            if (NoPrecargarInformacion == 0)
                SeleccionaLugarEjecucion(string.Empty);
        }
        else
        {
            HabilitarLugarEjecucion(false);
        }
    }

    private void EliminarAporteICBF(int rowIndex)
    {
        AporteContrato vAporteContrato = new AporteContrato();
        vAporteContrato.IdAporteContrato = Convert.ToInt32(gvAportesICBF.DataKeys[rowIndex]["IdAporteContrato"]);
        vContratoService.EliminarAporteContrato(vAporteContrato);
        SeleccionarAportes(string.Empty);
    }

    private void EliminarAporteContratista(int rowIndex)
    {
        AporteContrato vAporteContrato = new AporteContrato();
        vAporteContrato.IdAporteContrato = Convert.ToInt32(gvAportesContratista.DataKeys[rowIndex]["IdAporteContrato"]);
        vContratoService.EliminarAporteContrato(vAporteContrato);
        SeleccionarAportes(string.Empty);
    }

    #endregion

    #region LugarEjecucion

    private void HabilitarLugarEjecucion(bool habilitar)
    {
        if (String.IsNullOrEmpty(habilitar.ToString()))
        {
            imgLugarEjecucion.Enabled = false;
            gvLugaresEjecucion.DataSource = null;
            gvLugaresEjecucion.DataBind();
            nLugarEjecucion = "0";
            dvLugaresEjecucion.Style.Remove("height");
            dvLugaresEjecucion.Style.Remove("overflow-x");
            dvLugaresEjecucion.Style.Remove("overflow-y");
            HabilitarNivelNacional(false);
            HabilitarDatosAdicionalesLugarEjecucion(false);
            //cvLugarEjecucion.Enabled = false;
        }
        else if (habilitar == true)
        {
            imgLugarEjecucion.Enabled = true;
            gvLugaresEjecucion.Enabled = true;
            //cvLugarEjecucion.Enabled = true;
            HabilitarDatosAdicionalesLugarEjecucion(true);
            //if (int.Parse(AcordeonActivo) > 5)
            //    HabilitarContratista(true);
            //if (int.Parse(AcordeonActivo) > 6)
                HabilitarSupervisorInterventor(true);
        }
        else
        {
            imgLugarEjecucion.Enabled = false;
            gvLugaresEjecucion.Enabled = false;
            HabilitarNivelNacional(false);
            HabilitarDatosAdicionalesLugarEjecucion(false);
            //HabilitarContratista(false);
            //cvLugarEjecucion.Enabled = false;
            HabilitarSupervisorInterventor(false);
        }
    }

    private void EliminarLugarEjecucion(int rowIndex)
    {
        LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
        vLugarEjecucionContrato.IdLugarEjecucionContratos = Convert.ToInt32(gvLugaresEjecucion.DataKeys[rowIndex]["IdLugarEjecucionContratos"]);
        vContratoService.EliminarLugarEjecucionContrato(vLugarEjecucionContrato);
        SeleccionaLugarEjecucion(string.Empty);
    }

    private void SeleccionaLugarEjecucion(string LugarSeleccionado)
    {
        List<LugarEjecucionContrato> lugaresContrato = vContratoService.ConsultarLugaresEjecucionContrato(Convert.ToInt32(IdContrato));
        gvLugaresEjecucion.DataSource = lugaresContrato;
        gvLugaresEjecucion.DataBind();
        nLugarEjecucion = gvLugaresEjecucion.Rows.Count.ToString();

        dvLugaresEjecucion.Style.Remove("height");
        dvLugaresEjecucion.Style.Remove("overflow-x");
        dvLugaresEjecucion.Style.Remove("overflow-y");

        if (lugaresContrato.Count() == 0)
        {
            SeleccionarNivelNacional(true);
            HabilitarNivelNacional(true);
            AcordeonActivo = "7";
            HabilitarSupervisorInterventor(true);
            SeleccionarSupervisorInterventor(string.Empty);
            //HabilitarContratista(true);
            //SeleccionarContratistas(string.Empty);
        }
        else if(lugaresContrato.Any(x => x.EsNivelNacional == true))
        {
            SeleccionarNivelNacional(true);
            HabilitarNivelNacional(true);
            AcordeonActivo = "7";
            HabilitarSupervisorInterventor(true);
            SeleccionarSupervisorInterventor(string.Empty);
            gvLugaresEjecucion.Visible = false;
        }
        else
        {
            if (lugaresContrato.Count() > 2)
            {
                dvLugaresEjecucion.Style.Add("height", "100px");
                dvLugaresEjecucion.Style.Add("overflow-x", "hidden");
                dvLugaresEjecucion.Style.Add("overflow-y", "scroll");
            }

            chkNivelNacional.Checked = false;
            HabilitarNivelNacional(false);
            HabilitarDatosAdicionalesLugarEjecucion(true);
            //SeleccionarNivelNacional(false); // Se comenta por que sobra
            //AcordeonActivo = "6";
            //HabilitarContratista(true);
            //SeleccionarContratistas(string.Empty);
            AcordeonActivo = "7";
            HabilitarSupervisorInterventor(true);
            SeleccionarSupervisorInterventor(string.Empty);
            gvLugaresEjecucion.Visible = true;
        }
    }

    private void HabilitarNivelNacional(bool habilitar)
    {
        if (habilitar)
        {
            chkNivelNacional.Enabled = true;
        }
        else
        {
            chkNivelNacional.Enabled = false;
        }
    }

    private void SeleccionarNivelNacional(bool seleccionaNivelNacional)
    {
        if ((bool)seleccionaNivelNacional)
        {
            chkNivelNacional.Checked = true;
            imgLugarEjecucion.Enabled = false;
            //cvLugarEjecucion.Enabled = false;
            //HabilitarLugarEjecucion(false);
            HabilitarDatosAdicionalesLugarEjecucion(true);
            //
            AcordeonActivo = "7";
            //HabilitarContratista(true);
            //SeleccionarContratistas(string.Empty);
            HabilitarSupervisorInterventor(true);
            SeleccionarSupervisorInterventor(string.Empty);
        }
        else
        {
            HabilitarLugarEjecucion(true);
            AcordeonActivo = "6";
            //HabilitarContratista(false);
            HabilitarSupervisorInterventor(false);
            HabilitarDatosAdicionalesLugarEjecucion(false);
        }
    }

    private void HabilitarDatosAdicionalesLugarEjecucion(bool? habilitar)
    {
        if (habilitar == null)
        {
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            txtDatosAdicionalesLugarEjecucion.Text = string.Empty;
            //rfvDatosAdicionales.Enabled = false;
            //IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
        }
        else if (habilitar == true)
        {
            txtDatosAdicionalesLugarEjecucion.Enabled = true;
            //rfvDatosAdicionales.Enabled = true;
        }
        else
        {
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            //rfvDatosAdicionales.Enabled = false;
            //IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
        }
    }

    //private void IngresaDatosAdicionalesLugarEjecucion(string datosAdicionalesLugarEjecucionIngresados)
    //{
    //    if (gvLugaresEjecucion.Rows.Count > 0 || chkNivelNacional.Checked)
    //    {
    //        AcordeonActivo = "6";
    //        HabilitarContratista(true);
    //        SeleccionarContratistas(string.Empty);
    //    }
    //    else
    //    {
    //        HabilitarContratista(false);
    //    }
    //}

    #endregion

    #region SupervisorInterventor

    private void HabilitarSupervisorInterventor(bool habilitar)
    {
        if (habilitar == null)
        {
            imgSuperInterv.Enabled = false;
            gvSupervInterv.DataSource = null;
            gvSupervInterv.DataBind();
            nSupervInterv = "0";
            HabilitarCDP(false);
            gvDirectoresInterv.DataSource = null;
            gvDirectoresInterv.DataBind();
            //cvSupervInterv.Enabled = false;
        }
        else if (habilitar == true)
        {
            imgSuperInterv.Enabled = true;
            gvSupervInterv.Enabled = true;
            //cvSupervInterv.Enabled = true;
            if (int.Parse(AcordeonActivo) > 7 && chkSiManejaRecursos.Checked)
            {
                HabilitarCDP(true);
            }
            else if (int.Parse(AcordeonActivo) > 8 && chkSiManejaVigFuturas.Checked)
            {
                HabilitarVigenciasFuturas(true);
            }
        }
        else
        {
            imgSuperInterv.Enabled = false;
            gvSupervInterv.Enabled = false;
            //cvSupervInterv.Enabled = true;
            HabilitarCDP(false);
        }
    }

    private void SeleccionarSupervisorInterventor(string supervisorInterventorSeleccionado)
    {
        List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContrato), null);
        gvSupervInterv.DataSource = supervisoresInterventores;
        gvSupervInterv.DataBind();

        if (chkContratoConvenioAd.Checked)
        {
            if ((NoPrecargarInformacion == 0) && (supervisoresInterventores.Count() == 0))
            {
                PrecargaSupervInterContraConvAdhesion();// Pagina 21 Paso 128
            }
        }

        nSupervInterv = gvSupervInterv.Rows.Count.ToString();
        
        List<SupervisorInterContrato> directoresInterventoria = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContrato), true);
        gvDirectoresInterv.DataSource = directoresInterventoria;
        gvDirectoresInterv.DataBind();

        FlujoPosteriorCargaSupervInterv(gvSupervInterv.Rows.Count);
    }

    private void PrecargaSupervInterContraConvAdhesion()
    {
        List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContratoAsociado), null);
        foreach (SupervisorInterContrato sic in supervisoresInterventores)
        {
            sic.IDSupervisorIntervContrato = 0;
            sic.IdContrato = Convert.ToInt32(IdContrato);
            sic.FechaInicio = caFechaInicioEjecucion.Date;
            vContratoService.InsertarSupervisorInterContrato(sic);
        }

        supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(IdContrato), null);
        gvSupervInterv.DataSource = supervisoresInterventores;
        gvSupervInterv.DataBind();
    }

    protected void gvSupervIntervOnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (chkContratoConvenioAd.Checked)
            {
                ((Label)e.Row.FindControl("lbFechaInicio")).Visible = false;
                ((TextBox)e.Row.FindControl("txtFechaInicio")).Visible = true;

                string fechaInicio = gvSupervInterv.DataKeys[e.Row.RowIndex]["FechaInicio"].ToString();
                if (!fechaInicio.Equals(string.Empty))
                    fechaInicio = fechaInicio.Substring(0, 10);
                
                ((TextBox)e.Row.FindControl("txtFechaInicio")).Attributes.Add("onchange", "EjecutarJSFechaInicialSupervisores(this, '" + fechaInicio + "')");     
                //onchange="EjecutarJSFechaInicialSupervisores(this);"
                //if (NoPrecargarInformacion == 0)
                //    ((TextBox)e.Row.FindControl("txtFechaInicio")).Text = string.Empty; // No se puede dejar vacia ya que es obligatoria por el cu 19
            }
        }
    }

    private void FlujoPosteriorCargaSupervInterv(int numeroSupervisoresInterventores)
    {
        //dvSupervInterv.Style.Remove("overflow-x");
        //dvSupervInterv.Style.Remove("width");
        dvSupervInterv.Style.Remove("height");
        dvSupervInterv.Style.Remove("overflow-y");

        if (numeroSupervisoresInterventores == 0)
        {
            HabilitarCDP(false);
        }
        else
        {
            if (numeroSupervisoresInterventores > 2)
            {
                //dvSupervInterv.Style.Add("overflow-x", "scroll");
                //dvSupervInterv.Style.Add("width", "80%");
                dvSupervInterv.Style.Add("height", "200px");
                dvSupervInterv.Style.Add("overflow-y", "scroll");
            }

            if (chkSiManejaRecursos.Checked)
            {
                AcordeonActivo = "8";
                HabilitarCDP(true);
                SeleccionarCDP(string.Empty);
            }
            else
            {
                HabilitarCDP(false);

                if (chkSiManejaVigFuturas.Checked)
                {
                    AcordeonActivo = "9";
                    HabilitarSeccionVigenciaFuturas(true);
                    SeleccionarVigenciaFutura(string.Empty);
                }
                else
                {
                    HabilitarSeccionVigenciaFuturas(false);
                    //toolBar.MostrarBotonAprobar(true);
                    //// FIN HABILITAR BOTON GUARDAR FINAL
                }
            }
        }
    }

    private void EliminarSupervInterv(int rowIndex)
    {
        SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
        vSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(gvSupervInterv.DataKeys[rowIndex]["IDSupervisorIntervContrato"]);
        vContratoService.EliminarSupervisorInterContrato(vSupervisorInterContrato);
        NoPrecargarInformacion = 1;
        SeleccionarSupervisorInterventor(string.Empty);
    }

    #endregion

    #region CDP

    private void HabilitarCDP(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgCDP.Enabled = false;
            gvCDP.DataSource = null;
            gvCDP.DataBind();
            nCDP = "0";
            HabilitarSeccionVigenciaFuturas(false);
            //cvCDP.Enabled = false;
            lbCDP.Text = "CDP";
        } 
        else if (habilitar == true)
        {
            imgCDP.Enabled = true;
            gvCDP.Enabled = true;
            //cvCDP.Enabled = true;
            lbCDP.Text = "CDP *";
        }
        else
        {
            imgCDP.Enabled = false;
            gvCDP.Enabled = false;
            if (NoPrecargarInformacion == 0)
                HabilitarSeccionVigenciaFuturas(false);
            if (chkSiManejaRecursos.Checked)
            {
                lbCDP.Text = "CDP *";
                //cvCDP.Enabled = true;
            }
            else
            {
                lbCDP.Text = "CDP";
                //cvCDP.Enabled = false;
            }
        }
    }

    private void SeleccionarCDP(string CDPseleccionado)
    {
        CargarGrillaCDP();
        
        nCDP = gvCDP.Rows.Count.ToString();

        if (gvCDP.Rows.Count == 0)
        {
            HabilitarSeccionVigenciaFuturas(false);
        }
        else
        {
            string textoAlertavalValidacionesCDP = string.Empty;
            textoAlertavalValidacionesCDP = ValidacionesCDP();

            if (textoAlertavalValidacionesCDP.Equals(string.Empty))
            {
                if (chkSiManejaVigFuturas.Checked)
                {
                    AcordeonActivo = "9";
                    HabilitarSeccionVigenciaFuturas(true);
                    SeleccionarVigenciaFutura(string.Empty);
                }
                else
                {
                    HabilitarSeccionVigenciaFuturas(false);
                    //// FIN HABILITAR BOTON GUARDAR FINAL
                }
            }
            else
            {
                textoAlertavalValidacionesCDP = "alert('" + textoAlertavalValidacionesCDP + "')";
                if (!scriptEnCola)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "valValidacionesCDP", textoAlertavalValidacionesCDP, true);
                    scriptEnCola = true;
                }
                HabilitarSeccionVigenciaFuturas(false);
            }

        }
    }

    private void CargarGrillaCDP()
    {
        gvCDP.EmptyDataText = EmptyDataText();
        gvCDP.PageSize = PageSize();
        gvRubrosCDP.EmptyDataText = EmptyDataText();
        gvRubrosCDP.PageSize = PageSize();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDPLinea(idContrato);
        gvCDP.DataSource = cdps;
        gvCDP.DataBind();

        List<ContratosCDP> rubrosCdp = vContratoService.ConsultarContratosCDPRubroLinea(idContrato);
        gvRubrosCDP.DataSource = rubrosCdp;
        gvRubrosCDP.DataBind();
    }

    private string ValidacionesCDP()
    {
        string result = string.Empty;

        bool isValid = true;

        foreach (GridViewRow gvrCDP in gvRubrosCDP.Rows)
        {
            isValid = false;

            string codigoRubroCDP = HttpUtility.HtmlDecode(gvRubrosCDP.DataKeys[gvrCDP.RowIndex]["CodigoRubro"].ToString()).Trim();

            foreach (GridViewRow gvrPlan in gvRubrosPlanCompras.Rows)
            {
                string codigoRubroPlan = HttpUtility.HtmlDecode(gvrPlan.Cells[1].Text).Trim();

                if (codigoRubroCDP == codigoRubroPlan)
                {
                    isValid = true;
                    break;
                }
            }

            if (!isValid)
            {
                result = "Verifique los rubros del plan de compras vs el CDP\\n";
                break;
            }
        }

        if (result == string.Empty && !string.IsNullOrEmpty(txtValorPlanComprasContratos.Text))
        {
            var valorPlanCompras = decimal.Parse(txtValorPlanComprasContratos.Text);

            decimal valorCDP = 0;

            foreach (GridViewRow gvrCDP in gvCDP.Rows)
                valorCDP += decimal.Parse(HttpUtility.HtmlDecode(gvCDP.DataKeys[gvrCDP.RowIndex]["ValorCDP"].ToString()).Trim());

            if (valorCDP < valorPlanCompras)
                result = "Verifique el valor del CDP, debe ser mayor o igual al valor del Plan de Compras."; 
        }

        return result;
    }

    private void EliminarCDP(int rowIndex)
    {
        ContratosCDP vContratoCPD = new ContratosCDP();
        vContratoCPD.IdCDP = Convert.ToInt32(gvCDP.DataKeys[rowIndex]["IdContratosCDP"]);
        vContratoService.EliminarContratosCDP(vContratoCPD);
        SeleccionarCDP(string.Empty);
    }

    #endregion

    #region VigenciasFuturas

    public void HabilitarSeccionVigenciaFuturas(bool? habilitar)
    {
        if (habilitar == null)
        {
            imgVigFuturas.Enabled = false;
            gvVigFuturas.DataSource = null;
            gvVigFuturas.DataBind();
            nVigenciasFuturas = "0";
            //cvVigenciasFuturas.Enabled = false;
            lbVigenciasFuturas.Text = "Vigencias Futuras";
        }
        else if (habilitar == true)
        {
            imgVigFuturas.Enabled = true;
            //cvVigenciasFuturas.Enabled = true;
            gvVigFuturas.Enabled = true;
            lbVigenciasFuturas.Text = "Vigencias Futuras *";
        }
        else
        {
            imgVigFuturas.Enabled = false;
            gvVigFuturas.Enabled = false;
            //nVigenciasFuturas = "0";
            if (chkSiManejaVigFuturas.Checked)
            {
                lbVigenciasFuturas.Text = "Vigencias Futuras *";
                //cvVigenciasFuturas.Enabled = true;
            }
            else
            {
                lbVigenciasFuturas.Text = "Vigencias Futuras";
                //cvVigenciasFuturas.Enabled = false;
            }
        }
    }

    public void SeleccionarVigenciaFutura(string VigenciaFuturaSeleccionada)
    {
        List<VigenciaFuturas> vigencias = vContratoService.ConsultarVigenciaFuturass(null, null, Convert.ToInt32(IdContrato), null);
        gvVigFuturas.DataSource = vigencias;
        gvVigFuturas.DataBind();
        nVigenciasFuturas = gvVigFuturas.Rows.Count.ToString();

        if (gvVigFuturas.Rows.Count == 0)
        {
            //toolBar.MostrarBotonAprobar(false);
        }
        else
        { 
        
        }
    }

    private void EliminarVigenciaFutura(int rowIndex)
    {
        VigenciaFuturas vVigenciaFutura = new VigenciaFuturas();
        vVigenciaFutura.IDVigenciaFuturas = Convert.ToInt32(gvVigFuturas.DataKeys[rowIndex]["IDVigenciaFuturas"]);
        vContratoService.EliminarVigenciaFuturas(vVigenciaFutura);
        SeleccionarVigenciaFutura(string.Empty);
    }

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string controlFechaValidando = string.Empty;
        //Response.HeaderEncoding = System.Text.Encoding.UTF32;
        scriptEnCola = false;
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        
        try
        {
            toolBar.LipiarMensajeError();
            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {
                if (!Page.IsPostBack)
                {
                    CargarDatosIniciales();
                    if (Request.QueryString["oP"] == "E")
                        CargarRegistro();
                }
                else
                {
                    #region CargaValoresContratoAsociado
                    if (hfNumConvenioContratoAsociado.Value != "")
                        txtNumConvenioContrato.Text = hfNumConvenioContratoAsociado.Value;
                    #endregion

                    #region CargaValoresSolicitante
                    if (hfNombreSolicitante.Value != "")
                        txtNombreSolicitante.Text = hfNombreSolicitante.Value;

                    if (hfRegionalContConv.Value != "")
                        txtRegionalContratoConvenio.Text = hfRegionalContConv.Value;

                    if (hfDependenciaSolicitante.Value != "")
                        txtDependenciaSolicitante.Text = hfDependenciaSolicitante.Value;

                    if (hfCargoSolicitante.Value != "")
                        txtCargoSolicitante.Text = hfCargoSolicitante.Value;
                    #endregion

                    #region CagaValoresOrdenadorGasto
                    if (hfNombreOrdenadorGasto.Value != "")
                        txtNombreOrdenadorGasto.Text = hfNombreOrdenadorGasto.Value;

                    if (hfTipoIdentOrdenadorGasto.Value != "")
                        txtTipoIdentOrdenadorGasto.Text = hfTipoIdentOrdenadorGasto.Value;

                    if (hfNumeroIdentOrdenadoGasto.Value != "")
                        txtNumeroIdentOrdenadoGasto.Text = hfNumeroIdentOrdenadoGasto.Value;

                    if (hfCargoOrdenadoGasto.Value != "")
                        txtCargoOrdenadoGasto.Text = hfCargoOrdenadoGasto.Value;
                    #endregion

                    #region NumeroProceso
                    if (hfNumeroProceso.Value != "")
                        txtNumeroProceso.Text = hfNumeroProceso.Value;
                    #endregion

                    #region AdministraPostBack
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch (sControlName)
                    {
                        case "cphCont_chkConvenioMarco":
                            chkContratoAsociadoCheckedChanged(chkConvenioMarco, e);
                            break;
                        case "cphCont_chkContratoConvenioAd":
                            chkContratoAsociadoCheckedChanged(chkContratoConvenioAd, e);
                            break;
                        case "cphCont_txtNumConvenioContrato":
                            txtNumConvenioContratoTextChanged(sender, e);
                            break;
                        case "cphCont_txtNumeroProceso":
                            if (! string.IsNullOrEmpty(hfFechaAdjudicacionProceso.Value))
                            {
                                DateTime fecha = DateTime.Parse(hfFechaAdjudicacionProceso.Value);
                                caFechaAdjudicaProceso.Date = fecha;
                            }
                            txtNumeroProcesoTextChanged(sender, e);
                            break;
                        case "cphCont_caFechaAdjudicaProceso":
                            controlFechaValidando = "Fecha Adjudicación del Proceso";
                            string fechaAdjudicaProceso =caFechaAdjudicaProceso.Date.ToString();
                            toolBar.LipiarMensajeError();
                            SeleccionaFechaAdjudicacion(fechaAdjudicaProceso);
                            controlFechaValidando = string.Empty;
                            break;
                        case "cphCont_chkSiReqActa":
                            chkReqActaCheckedChanged(chkSiReqActa, e);
                            break;
                        case "cphCont_chkNoReqActa":
                            chkReqActaCheckedChanged(chkNoReqActa, e);
                            break;
                        case "cphCont_chkSiManejaAportes":
                            chkManejaAportesCheckedChanged(chkSiManejaAportes, e);
                            break;
                        case "cphCont_chkNoManejaAportes":
                            chkManejaAportesCheckedChanged(chkNoManejaAportes, e);
                            break;
                        case "cphCont_chkSiManejaRecursos":
                            chkManejaRecursosCheckedChanged(chkSiManejaRecursos, e);
                            break;
                        case "cphCont_chkNoManejaRecursos":
                            chkManejaRecursosCheckedChanged(chkNoManejaRecursos, e);
                            break;
                        case "cphCont_txtNombreSolicitante":
                            txtNombreSolicitanteTextChanged(sender, e);
                            break;
                        case "cphCont_txtNombreOrdenadorGasto":
                            txtNombreOrdenadorGastoTextChanged(sender, e);
                            break;
                        case "cphCont_txtPlanCompras":
                            txtPlanComprasTextChanged(sender, e);
                            break;
                        case "cphCont_caFechaInicioEjecucion":
                            controlFechaValidando = "Fecha de Inicio de ejecución del Contrato/Convenio";
                            string fechaInicioEjecucion = caFechaInicioEjecucion.Date.ToString();
                            toolBar.LipiarMensajeError();
                            SeleccionaFechaInicioEjecucion(fechaInicioEjecucion);
                            controlFechaValidando = string.Empty;
                            break;
                        case "cphCont_caFechaFinalizacionInicial":
                            controlFechaValidando = "Fecha de Finalización Inicial del Contrato/Convenio";
                            string fechaFinalizacionInicial = string.Empty;
                            if(chkCalculaFecha.Checked)
                            {
                                if (GetSessionParameter("CalculoFecha.FechaFinal") != null)
                                {
                                    DateTime fechafinalizacionInicialD = DateTime.Parse(GetSessionParameter("CalculoFecha.FechaFinal").ToString());
                                    caFechaFinalizacionInicial.Date = fechafinalizacionInicialD;
                                    RemoveSessionParameter("CalculoFecha.FechaFinal");
                                    fechaFinalizacionInicial = caFechaFinalizacionInicial.Date.ToString();
                                }
                                else if(caFechaFinalizacionInicial.Date != DateTime.MinValue)
                                    fechaFinalizacionInicial = caFechaFinalizacionInicial.Date.ToString();
                            }
                            else
                            fechaFinalizacionInicial = caFechaFinalizacionInicial.Date.ToString();

                            toolBar.LipiarMensajeError();
                            SeleccionaFechaFinalizacionInicialContConv(fechaFinalizacionInicial);
                            controlFechaValidando = string.Empty;
                            break;
                        case "cphCont_chkSiManejaVigFuturas":
                            chkManejaVigFuturasCheckedChanged(chkSiManejaVigFuturas, e);
                            break;
                        case "cphCont_chkNoManejaVigFuturas":
                            chkManejaVigFuturasCheckedChanged(chkNoManejaVigFuturas, e);
                            break;
                        case "cphCont_txtValorApoICBF":
                            txtValorApoICBFTextChanged(sender, e);
                            break;
                        case "cphCont_txtValorApoContrat":
                            txtValorApoContratTextChanged(sender, e);
                            break;
                        case "cphCont_txtLugarEjecucion":
                            txtLugarEjecucionTextChanged(sender, e);
                            break;
                        case "cphCont_txtContratista":
                            txtContratistaTextChanged(sender, e);
                            break;
                        case "cphCont_txtSuperInterv":
                            txtSuperIntervTextChanged(sender, e);
                            break;
                        case "cphCont_txtCDP":
                            txtCDPTextChanged(sender, e);
                            break;
                        case "cphCont_txtVigFuturas":
                            txtVigFuturasTextChanged(sender, e);
                            break;
                        case "cphCont_rblAportesEspecie":
                            rblAportesEspecieClick(sender, e);
                            break;
                        case "GetCodigosSECOP":
                            txtCodigosSECOP_TextChanged(sender, e);
                            break;
                        default:
                            break;
                    }
                    #endregion
                }
            }
        }
        catch (FormatException ex)
        {
            HabilitarVigenciasFuturas(false);
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inválido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        string texto = string.Empty;
        string controlFechaValidando = string.Empty;
        try
        {
            controlFechaValidando = "Fecha Adjudicación del Proceso";
            //if (PnNumeroProceso.Style["visibility"].Equals(string.Empty) && !caFechaAdjudicaProceso.Date.ToShortDateString().Substring(0, 10).Equals("01/01/1900"))
            if (PnNumeroProceso.Style["visibility"].Equals(string.Empty) && (Convert.ToDateTime(caFechaAdjudicaProceso.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900")))
            {
                texto = ValidacionFechaAdjudicacionProceso(caFechaAdjudicaProceso.Date); // Pagina 12 Paso 25 
            }
            controlFechaValidando = string.Empty;

            //Si maneja recursos
            //if (chkSiManejaRecursos.Checked)
            //{
            //    decimal sumatoriaProductos = 0;
            //    if (!TotalProductos.Equals(string.Empty))
            //        sumatoriaProductos = Convert.ToDecimal(TotalProductos);

            //    if (!ValidacionValorInicialContConv(sumatoriaProductos)) // Pagina 16 paso 73
            //    {
            //        texto += "El Valor Inicial del Contrato/Convenio no puede superar el 0.0025% del Valor Total de los Productos\\n";
            //    }
            //}


            //Si es convenio marco
            if (chkConvenioMarco.Checked && !txtValorInicialContConv.Text.Equals(string.Empty)) // Pagina 17 Paso 74
            {
                if (!ValidacionValorInicialSuperaValorFinal(decimal.Parse(txtValorInicialContConv.Text)))
                {
                    texto += "El Valor Inicial Contrato/Convenio supera el Valor Final del Contrato/Convenio del Convenio Marco Asociado\\n";
                }
            }

            //if (chkSiManejaAportes.Checked && !txtValorInicialContConv.Text.Equals(string.Empty))
            //{
            //    decimal valorinicicialcontrato = decimal.Parse(txtValorInicialContConv.Text);
            //    decimal acomuladoaportes = vContratoService.ConsultarAporteContratos(null, null, null, null, null, Convert.ToInt32(IdContrato), null, null, null, null).Sum(d => d.ValorAporte);
            //    if (acomuladoaportes > valorinicicialcontrato)
            //    {
            //        texto += "El Valor de los Aportes supera el Valor Final del Contrato/Convenio\\n";
            //    }
            //}

            controlFechaValidando = "Fecha de Inicio de ejecución del Contrato/Convenio";
            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (!caFechaInicioEjecucion.Date.ToShortDateString().Substring(0, 10).Equals("01/01/1900"))
            if (Convert.ToDateTime(caFechaInicioEjecucion.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
            {
                texto += ValidacionesFechaInicioEjecucion(caFechaInicioEjecucion.Date);
            }
            controlFechaValidando = string.Empty;

            controlFechaValidando = "Fecha de Finalización Inicial del Contrato/Convenio";
            //Jorge Vizcaino: Se cambiala forma de comparar la fecha
            //if (!caFechaFinalizacionInicial.Date.ToShortDateString().Substring(0, 10).Equals("01/01/1900"))
            if (Convert.ToDateTime(caFechaFinalizacionInicial.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
            {
                texto += ValidacionesFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date);
            }
            controlFechaValidando = string.Empty;

            if (chkSiManejaVigFuturas.Checked)
            {
                //controlFechaValidando = "Fecha de Inicio de ejecución del Contrato/Convenio";
                //Jorge Vizcaino: Se cambiala forma de comparar la fecha
                //if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1") && !ddlVigenciaFiscalIni.SelectedValue.Equals("-1") &&
                //    !caFechaInicioEjecucion.Date.ToShortDateString().Substring(0, 10).Equals("01/01/1900"))
                if (!ddlVigenciaFiscalIni.SelectedValue.Equals("-1") && !ddlVigenciaFiscalIni.SelectedValue.Equals("-1") &&
                    Convert.ToDateTime(caFechaInicioEjecucion.Date.ToShortDateString()) != Convert.ToDateTime("01/01/1900"))
                {
                    if (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) < caFechaInicioEjecucion.Date.Year ||
                            (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) > (caFechaInicioEjecucion.Date.Year + 1))) // Paso 98
                    {
                        texto = "La Vigencia Fiscal Inicial del Contrato/Convenio debe ser Mayor o Igual al año de la Fecha de Inicio de Ejecución del contrato/Convenio";
                    }

                    if (!ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
                    {
                        if (int.Parse(ddlVigenciaFiscalFin.SelectedItem.Text) <= int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text)) // Paso 101
                        {
                            texto = "La Vigencia Fiscal Final del Contrato/Convenio debe ser Mayor a la Vigencia Fiscal Inicial del Contrato/Convenio";
                        }
                    }
                }
                //controlFechaValidando = string.Empty;
            }


            if (texto.Equals(string.Empty))
            {
                Guardar();
            }
            else
            {
                texto = "alert('" + texto + "')";
                ScriptManager.RegisterStartupScript(this, GetType(), "valGuardarContConv", texto, true);
            }
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inválido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        string texto = string.Empty;

        if (PnNumeroProceso.Style["visibility"].Equals(string.Empty))
        {
            texto = ValidacionFechaAdjudicacionProceso(caFechaAdjudicaProceso.Date); // Pagina 12 Paso 25 
        }

        //Si maneja recursos
        if (chkSiManejaRecursos.Checked)
        {
            if (!ValidacionSumatoriaProductosVsRubros()) // Se controla la validacion del cu 40 negociada por SM 03/09/2014 para no generar un cambio mayor en el CU40
            {
                texto += "La suma de los productos no es igua a la suma de los rubros por cada Consecutivo Plan de Compras verifique por favor\\n";
            }

            decimal sumatoriaProductos = 0;
            if (!TotalProductos.Equals(string.Empty))
                sumatoriaProductos = Convert.ToDecimal(TotalProductos);

            //if (!ValidacionValorInicialContConv(sumatoriaProductos)) // Pagina 16 paso 73
            //{
            //    texto += "El Valor Inicial del Contrato/Convenio no puede superar el 0.0025% del Valor Total de los Productos\\n";
            //}
        }


        //Si es convenio marco
        if (chkConvenioMarco.Checked) // Pagina 17 Paso 74
        {
            if (!ValidacionValorInicialSuperaValorFinal(decimal.Parse(txtValorInicialContConv.Text)))
            {
                texto += "El Valor Inicial Contrato/Convenio supera el Valor Final del Contrato/Convenio del Convenio Marco Asociado\\n";
            }
        }

        //if (chkSiManejaAportes.Checked)
        //{
        //    decimal valorinicicialcontrato = decimal.Parse(txtValorInicialContConv.Text);
        //    decimal acomuladoaportes = vContratoService.ConsultarAporteContratos(null, null, null, null, null, Convert.ToInt32(IdContrato), null, null, null, null).Sum(d => d.ValorAporte);
        //    if (acomuladoaportes != valorinicicialcontrato)
        //    {
        //        texto += "El Valor de los Aportes debe ser igual al Valor Final del Contrato/Convenio\\n";
        //    }
        //}

        texto += ValidacionesFechaInicioEjecucion(caFechaInicioEjecucion.Date);

        texto += ValidacionesFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date);

        if (chkSiManejaVigFuturas.Checked)
        {
            if (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) < caFechaInicioEjecucion.Date.Year ||
                    (int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text) > (caFechaInicioEjecucion.Date.Year + 1))) // Paso 98
            {
                texto = "La Vigencia Fiscal Inicial del Contrato/Convenio debe ser Mayor o Igual al año de la Fecha de Inicio de Ejecución del contrato/Convenio\\n";
            }

            if (!ddlVigenciaFiscalFin.SelectedValue.Equals("-1"))
            {
                if (int.Parse(ddlVigenciaFiscalFin.SelectedItem.Text) <= int.Parse(ddlVigenciaFiscalIni.SelectedItem.Text)) // Paso 101
                {
                    texto = "La Vigencia Fiscal Final del Contrato/Convenio debe ser Mayor a la Vigencia Fiscal Inicial del Contrato/Convenio\\n";
                }
            }
        }

        texto += ValidacionesCDP();


        if (texto.Equals(string.Empty))
        {
            List<EstadoContrato> estadoContrato = vContratoService.ConsultarEstadoContrato(null, CodEstadoRegistrado, null, true);
            if (estadoContrato.Count > 0)
                IdEstadoContrato = estadoContrato[0].IdEstadoContrato.ToString();

            
            Guardar();
            
        }
        else
        {
            texto = "alert('" + texto + "')";
            ScriptManager.RegisterStartupScript(this, GetType(), "valFinalesContConv", texto, true);
        }
    }
    
    private bool ValidacionSumatoriaProductosVsRubros()
    {
        foreach (GridViewRow gvr in gvConsecutivos.Rows)
        {
            string numeroConsecutivo = gvConsecutivos.DataKeys[gvr.RowIndex]["IDPlanDeCompras"].ToString();

            var vTotalProductosPorConsecutivo = (from vListRows in gvProductos.Rows.Cast<GridViewRow>()
                                                 let vTipoProd = vListRows.Cells[3].Text
                                                 where vListRows.Cells[0].Text == numeroConsecutivo//GetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo").ToString()
                                                 select new
                                                 {
                                                     vTotalProductos = Convert.ToDecimal(decimal.Parse(vListRows.Cells[6].Text, NumberStyles.Currency))
                                                     //vTotalProductos =  vTipoProd.ToUpper().Trim()  == "SERVICIO" ?
                                                     //                  Convert.ToDecimal(decimal.Parse(vListRows.Cells[4].Text, NumberStyles.Currency)) *
                                                     //                  Convert.ToDecimal(decimal.Parse(vListRows.Cells[5].Text, NumberStyles.Currency)) *
                                                     //                  Convert.ToDecimal(decimal.Parse(vListRows.Cells[7].Text, NumberStyles.Currency)) :
                                                     //                  Convert.ToDecimal(decimal.Parse(vListRows.Cells[4].Text, NumberStyles.Currency)) *
                                                     //                  Convert.ToDecimal(decimal.Parse(vListRows.Cells[5].Text, NumberStyles.Currency))
                                                 }).Sum(d => d.vTotalProductos);

            var vTotalRubrosPorConsecutivo = (from vListRows in gvRubrosPlanCompras.Rows.Cast<GridViewRow>()
                                              where vListRows.Cells[0].Text == numeroConsecutivo//GetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo").ToString()
                                              select new
                                              {
                                                  vTotalRubros = Convert.ToDecimal(decimal.Parse(vListRows.Cells[4].Text, NumberStyles.Currency))
                                              }).Sum(d => d.vTotalRubros);
            
            if (vTotalProductosPorConsecutivo != vTotalRubrosPorConsecutivo)
            {
                return false;
            }
        }

        return true;
    }

    protected void AccContratos_ItemCommand(object sender, CommandEventArgs e)
    {
        SeleccionarPanel(e.CommandName);
    }

    protected void ddlRegionalUsuarioContratoSelectedIndexChanged(object sender, EventArgs e)
    {
        //SeleccionaRegional(ddlRegionalUsuarioContrato.SelectedValue);
    }

    protected void chkContratoAsociadoCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            switch (((CheckBox)sender).ID)
            {
                case "chkConvenioMarco":
                    SeleccionadoContratoAsociado(CodConvenioMarco);
                    break;
                case "chkContratoConvenioAd":
                    SeleccionadoContratoAsociado(CodContratoConvenioAdhesion);
                    break;
                default:
                    break;
            }

            ObtenerIdContratoConvenio();
        }
        else
        {
            SeleccionadoContratoAsociado(string.Empty);
        }
    }

    protected void txtNumConvenioContratoTextChanged(object sender, EventArgs e)
    {
        SeleccionaNumeroConvenioContrato(IdContratoAsociado);
        //SeleccionaNumeroConvenioContrato(txtNumConvenioContrato.Text);
    }

    protected void ddlCategoriaContratoSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlCategoriaContrato.SelectedValue, true);
    }

    protected void ddlTipoContratoConvenioSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoTipoContratoConvenio(ddlCategoriaContrato.SelectedValue, ddlTipoContratoConvenio.SelectedValue);
        
    }

    protected void ddlModalidadAcademicaSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarModalidadAcademica(ddlModalidadAcademica.SelectedValue);
    }

    protected void ddlNombreProfesionSeleccionSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaProfesion(ddlNombreProfesion.SelectedValue);
    }

    protected void ddlModalidadSeleccionSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarModalidadSeleccion(ddlModalidadSeleccion.SelectedValue);
    }
    

    protected void txtNumeroProcesoTextChanged(object sender, EventArgs e)
    {
        SeleccionarNumeroProceso(txtNumeroProceso.Text);
    }

    protected void chkReqActaCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            //#region UnicaSeleccionCheck
            //switch (((CheckBox)sender).ID)
            //{
            //    case "chkSiReqActa":
            //        chkNoReqActa.Checked = false;
            //        break;
            //    case "chkNoReqActa":
            //        chkSiReqActa.Checked = false;
            //        break;
            //    default:
            //        break;
            //}
            //#endregion

            HabilitarManejaAportes(true); // Pagina 13 Paso 29.1
            RequiereManejoAportes(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 29
        }
        else
        {
            HabilitarManejaAportes(null);
        }
    }

    protected void chkManejaAportesCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            //#region UnicaSeleccionCheck
            //switch (((CheckBox)sender).ID)
            //{
            //    case "chkSiManejaAportes":
            //        chkNoManejaAportes.Checked = false;
            //        break;
            //    case "chkNoManejaAportes":
            //        chkSiManejaAportes.Checked = false;
            //        break;
            //    default:
            //        break;
            //}
            //#endregion

            if ( chkNoManejaAportes.Checked )
            {
                HabilitarAportes(null);   
            }

            HabilitarManejaRecursos(true); // Pagina 13 Paso 32.1
            RequiereManejaRecursos(ddlTipoContratoConvenio.SelectedValue, ddlCategoriaContrato.SelectedValue); // Pagina 13 Paso 32
            SeleccionaFormaPago(ddlFormaPago.SelectedValue);
        }
        else
        {
            HabilitarManejaRecursos(null);
        }
    }

    protected void chkManejaRecursosCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            //#region UnicaSeleccionCheck
            //switch (((CheckBox)sender).ID)
            //{
            //    case "chkSiManejaRecursos":
            //        chkNoManejaRecursos.Checked = false;
            //        break;
            //    case "chkNoManejaRecursos":
            //        chkSiManejaRecursos.Checked = false;
            //        break;
            //    default:
            //        break;
            //}
            //#endregion

            HabilitarRegimenContratacion(true); // Pagina 13 Paso 35
            //SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value);

            if (chkSiManejaRecursos.Checked)
            {
                if (int.Parse(AcordeonActivo) >= 2)
                {
                    HabilitarPlanDeCompras(true); // Pagina 14 Paso 46
                    SeleccionarPlanCompras("1"); // Carga la información de la sección Plan de Compras
                }

                if (int.Parse(AcordeonActivo) >= 8)
                {
                    HabilitarCDP(true);
                    SeleccionarCDP(string.Empty);
                }
            }
            else
            {
                AcordeonActivo = "3";

                if (hfContratoSV.Value == "true")
                {
                    HabilitarPlanDeCompras(true);//Cambio realizado por caso de uso contrato sin valor 
                }
                else
                {
                    HabilitarPlanDeCompras(false);
                }
                HabilitarCDP(null);
                if (!hfNombreOrdenadorGasto.Value.Equals(string.Empty))
                    HabilitarObjetoContratoConvenio(true); // Se continua con el paso 55, despues 66 y despues 62
            }
        }
        else
        {
            HabilitarRegimenContratacion(null);
        }
    }

    protected void ddlRegimenContratacionSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaRegimenContratacion(ddlRegimenContratacion.SelectedValue); // Pagina 14 Paso 38
    }

    protected void txtNombreSolicitanteTextChanged(object sender, EventArgs e)
    {
        SeleccionarSolicitante(txtNombreSolicitante.Text); // Pagina 14 Paso 42
    }

    protected void txtNombreOrdenadorGastoTextChanged(object sender, EventArgs e)
    {
        SeleccionarOrdenadorGasto(hfNombreOrdenadorGasto.Value); // Pagina 14 Paso 46
    }

    protected void txtPlanComprasTextChanged(object sender, EventArgs e)
    {
        SeleccionarPlanCompras("1"); //txtPlanCompras.Text);
        SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);

        if (
            ddlTipoContratoConvenio.SelectedItem.Text == "PRESTACIÓN DE SERVICIOS DE APOYO A LA GESTIÓN" 
           // || ddlTipoContratoConvenio.SelectedItem.Text == "PRESTACIÓN DE SERVICIOS PROFESIONALES"
           )
        {
            List<Proveedores_Contratos> contratistas = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(IdContrato), null);
            gvContratistas.DataSource = contratistas;
            gvContratistas.DataBind();
            nContratistas = gvContratistas.Rows.Count.ToString();

            var contrato = vContratoService.ContratoObtener(Convert.ToInt32(IdContrato));
            string IdModalidadAcademica = "-1";
            if (contrato.IdModalidadAcademica != null)
            {
                IdModalidadAcademica = contrato.IdModalidadAcademica;
                ddlModalidadAcademica.SelectedValue = IdModalidadAcademica;
                SeleccionarModalidadAcademica(IdModalidadAcademica);
            }
            string IdProfesion = "-1";
            if (contrato.IdProfesion != null)
            {
                IdProfesion = contrato.IdProfesion;
                SeleccionaProfesion(IdProfesion);
                ddlNombreProfesion.SelectedValue = IdProfesion.Trim();
            }
        }
    }

    protected void DdlConsecutivoPlanComprasSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);
    }

    protected void txtObjetoTextChanged(object sender, EventArgs e)
    {
        IngresaObjetoContratoConvenio(txtObjeto.Text);
    }

    protected void txtAlcanceTextChanged(object sender, EventArgs e)
    {
        NoPrecargarInformacion = 0;
        IngresaAlcanceObjetoContratoConvenio(txtAlcance.Text);
    }

    protected void txtValorInicialContConvTextChanged(object sender, EventArgs e)
    {
        if (txtValorInicialContConv.Text.Equals(string.Empty))
        {
            IngresaValorInicialContConv(string.Empty);
        }
        else
        { 
            bool error = false;
            string mensajeValorInicial = string.Empty;
            string s = txtValorInicialContConv.Text;
            string sPattern = @"^(\$|)([0-9]\d{0,2}(\.\d{3})*|([1-9]\d*))(\,\d{2})?$";
            //
            if (System.Text.RegularExpressions.Regex.IsMatch(s, sPattern))
            {
                decimal number;
                bool result = decimal.TryParse(s, out number);
                if (!result)
                {
                    mensajeValorInicial = "El formato del campo es inválido";
                    IngresaValorInicialContConv(string.Empty);
                    error = true;
                }
                else
                {
                    // Tope recibido por la base de datos
                    // 9999999999999999999999,99
                    if (decimal.Parse(s) < decimal.Parse("10000000000000000000000"))
                    { 
                        IngresaValorInicialContConv(txtValorInicialContConv.Text);
                    }
                    else
                    {
                        //txtValorInicialContConv.Text = string.Empty;
                        mensajeValorInicial = "El valor inicial es demasiado alto";
                        IngresaValorInicialContConv(string.Empty);
                        error = true;
                    }
                }
            }
            else
            {
                //txtValorInicialContConv.Text = string.Empty;
                mensajeValorInicial = "El formato del campo es inválido";
                IngresaValorInicialContConv(string.Empty);
                error = true;
            }

            if (error)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "valValorInicial", "mensajeAlerta('" + txtValorInicialContConv.ClientID + "', '" + mensajeValorInicial + "')", true);
            }
        }
        
    }

    protected void chkManejaVigFuturasCheckedChanged(object sender, EventArgs e)
    {
        if (((CheckBox)sender).Checked)
        {
            #region UnicaSeleccionCheck
            switch (((CheckBox)sender).ID)
            {
                case "chkSiManejaVigFuturas":
                    HabilitarSeccionVigenciaFuturas(true);
                    ValidacionVigenciasFiscales(ddlVigenciaFiscalIni);
                    break;
                case "chkNoManejaVigFuturas":
                    HabilitarSeccionVigenciaFuturas(false);
                    //toolBar.MostrarBotonAprobar(true);
                    break;
                default:
                    break;
            }
            #endregion

            HabilitarVigenciaFiscal(true); // Pagina 19 Paso 95
        }
        else
        {
            HabilitarVigenciaFiscal(null);
        }
    }

    protected void ddlVigenciaFiscalIniSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaVigenciaFiscalInicial(ddlVigenciaFiscalIni.SelectedValue);
    }

    protected void ddlVigenciaFiscalFinSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaVigenciaFiscalFinal(ddlVigenciaFiscalFin.SelectedValue);
    }

    protected void ddlFormaPagoSelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionaFormaPago(ddlFormaPago.SelectedValue);
    }
    
    protected void ddlTipoFormaPagoSelectedIndexChanged(object sender, EventArgs e)
    {
       SeleccionarTipoFormaPago(ddlFormaPago.SelectedValue);
    }

    protected void txtValorApoICBFTextChanged(object sender, EventArgs e)
    {
        SeleccionarAportes(string.Empty);
    }

    protected void txtValorApoContratTextChanged(object sender, EventArgs e)
    {
        SeleccionarAportes(string.Empty);
    }

    #region gvAportesICBF

    protected void gvAportesICBF_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void btnEliminarAporteICBFClick(object sender, EventArgs e)
    {
        EliminarAporteICBF(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    #endregion

    #region gvAportesContratista

    protected void gvAportesContratista_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    protected void btnEliminarAporteContratistaClick(object sender, EventArgs e)
    {
        EliminarAporteContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    #endregion

    protected void txtLugarEjecucionTextChanged(object sender, EventArgs e)
    {
        SeleccionaLugarEjecucion(string.Empty);
    }

    protected void chkNivelNacionalCheckedChanged(object sender, EventArgs e)
    {
        SeleccionarNivelNacional(((CheckBox)sender).Checked);
    }

    protected void btnEliminarLugEjecucionClick(object sender, EventArgs e)
    {
        EliminarLugarEjecucion(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtDatosAdicionalesLugarEjecucionTextChanged(object sender, EventArgs e)
    {
        //IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
    }

    protected void txtContratistaTextChanged(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(GetSessionParameter("Proveedor.Duplicado").ToString()))
        {
            toolBar.MostrarMensajeError(GetSessionParameter("Proveedor.Duplicado").ToString());
            RemoveSessionParameter("Proveedor.Duplicado");
        }

        SeleccionarContratistas(string.Empty);
    }

    protected void btnOpcionGvContratistasClick(object sender, EventArgs e)
    {
        switch (((LinkButton)sender).CommandName)
        {
            case "Eliminar":
                EliminarContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
                break;
            case "Detalle":
                int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);

                string idProveedor = gvContratistas.DataKeys[rowIndex]["IdTercero"].ToString();
                SetSessionParameter("Contrato.IdProveedor", idProveedor);

                string idProveedoresContratos = gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"].ToString();
                SetSessionParameter("FormaPago.IDProveedoresContratos", idProveedoresContratos);

                //SetSessionParameter("Contrato.ContratosAPP", IdContrato);
                    string returnValue = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                                       "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                                       "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                                       "   <script language='javascript'> " +
                                       "        GetFormaPago();" +
                                       "   </script>";


                    ClientScript.RegisterStartupScript(Page.GetType(), "formaPago", returnValue);


                //ScriptManager.RegisterStartupScript(this, GetType(), "formaPago", "GetFormaPago()", true);
                break;
            default:
                break;
        }
    }

    protected void txtSuperIntervTextChanged(object sender, EventArgs e)
    {
        SeleccionarSupervisorInterventor(string.Empty);
    }

    protected void btnEliminarGvSupervIntervClick(object sender, EventArgs e)
    {
        EliminarSupervInterv(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtCDPTextChanged(object sender, EventArgs e)
    {
        SeleccionarCDP(string.Empty); //activar la Validacion si se carga la información
    }

    protected void gvCDP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCDP.PageIndex = e.NewPageIndex;
        CargarGrillaCDP();
    }

    protected void btnEliminarCDPClick(object sender, EventArgs e)
    {
        EliminarCDP(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtVigFuturasTextChanged(object sender, EventArgs e)
    {
        SeleccionarVigenciaFutura(string.Empty);
    }

    protected void btnEliminarGvVigFuturasClick(object sender, EventArgs e)
    {
        EliminarVigenciaFutura(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void rblAportesEspecieClick(object sender, EventArgs e)
    {
        if (rblAportesEspecie.SelectedValue == "False")
        {
            gvAportesICBF.DataSource = null;
            gvAportesICBF.DataBind();
        }
        else
        {


        }
    }

    protected void rblAportesEspecie_SelectedIndexChanged(object sender, EventArgs e)
    {
        HabilitarAportes(true);
    }

    private bool ValidarVigencias()
    {
        bool IsValid = true;
        try
        {

            if (!string.IsNullOrEmpty(nVigenciasFuturas) && nVigenciasFuturas !="0")
            {
                bool result = false;
                SIAService vSIAService = new SIAService();
                List<Vigencia> AniosVigenciaContrato = new List<Vigencia>();
                Icbf.Contrato.Entity.Contrato vContrato = new Icbf.Contrato.Entity.Contrato();
                vContrato = vContratoService.ConsultarContrato(Convert.ToInt32(hfIdContrato.Value));

                AniosVigenciaContrato = vSIAService.ConsultarVigencias(true).Where(x => x.AcnoVigencia > vContrato.FechaInicioEjecucion.Value.Year && x.AcnoVigencia <= vContrato.FechaFinalTerminacionContrato.Value.Year).OrderBy(x => x.AcnoVigencia).ToList();
                foreach (Vigencia anio in AniosVigenciaContrato)
                {
                    result = vSIAService.ConsultarVigenciasExistenteAnio(Convert.ToInt32(hfIdContrato.Value), anio.AcnoVigencia);
                }
            }



        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            IsValid = false;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        return IsValid;
    }

    protected void chkCalculaFecha_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCalculaFecha.Checked)
        {
            ImgBtnFechaFinal.Visible = true;
            caFechaFinalizacionInicial.Enabled = false;
        }
        else
        {
            ImgBtnFechaFinal.Visible = false;
            caFechaFinalizacionInicial.Enabled = true;
        }
    }

    protected void txtCodigosSECOP_TextChanged(object sender, EventArgs e)
    {
        try
        {
            CargarCodigosSECOP();
        }
        catch (Exception ex)
        {
         toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarCodigoSECOP_Click(object sender, EventArgs e)
    {
        try 
        {	        
            int idContrato = int.Parse(hfIdContrato.Value);
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            int idCodigoSECOP  = Convert.ToInt32(gvCodigosSECOP.DataKeys[rowIndex]["key"]);
            vContratoService.EliminarCodigoSECOPContrato(idContrato,idCodigoSECOP);
            CargarCodigosSECOP(); 
        }
        catch (Exception ex)
        {
         toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion





}
