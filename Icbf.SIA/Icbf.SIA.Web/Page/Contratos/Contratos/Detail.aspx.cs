using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad Contrato
/// </summary>
public partial class Page_Contrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Contratos";
    string PageNameAprobar = "Contratos/ValidarRegistroContrato";
    Contrato vContrato = new Contrato();
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        SetSessionParameter("Contrato.ContratosAPP", hfIdContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        RemoveSessionParameter("Contrato.IdContrato");
        RemoveSessionParameter("Contrato.ContratosAPP");
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            //RemoveSessionParameter("Contrato.ContratosAPP");

            if (GetSessionParameter("Contrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Contrato.Guardado");


            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ContratoObtener(vIdContrato);
            hfIdContrato.Value =  vContrato.IdContrato.ToString();
            ObtenerAuditoria(PageName, hfIdContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            Contrato vContrato = new Contrato();
            //vContrato = vContrato.ConsultarContrato(vIdContrato);
            InformacionAudioria(vContrato, this.PageName, vSolutionPage);
            int vResultado = 0;//vContrato.EliminarContrato(vContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Contrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            #region CargarBotones
            //Valida si el contrato seleccionado esta en registro o registrado y si el usuario autenticado es el mismo al que pertenece el contrato
            int vIdestadoContrato = 0;
            if (!int.TryParse(GetSessionParameter("Contrato.EstadoContrato").ToString(), out vIdestadoContrato))
            {
                toolBar.MostrarBotonEditar(false);
                
            }
            else
            {
                bool flag = false;
                List<EstadoContrato> vEstadoContrato = new List<EstadoContrato>();

                vEstadoContrato = vContratoService.ConsultarEstadoContrato(vIdestadoContrato, null, null, true);
                if (vEstadoContrato != null)
                {
                    if (vEstadoContrato.Count > 0)
                    {
                        if (vEstadoContrato[0].CodEstado.Trim() == "REC" || vEstadoContrato[0].CodEstado.Trim() == "REE")
                        {
                            if (GetSessionParameter("Contrato.EsUsuarioAutenticado") !=null)
                            {
                                bool vEsAutenticado = Convert.ToBoolean(GetSessionParameter("Contrato.EsUsuarioAutenticado"));
                                if (vEsAutenticado || ValidateAccessEdit(toolBar, PageNameAprobar, SolutionPage.Edit))
                                {
                                    toolBar.MostrarBotonEditar(true);
                                    return;
                                }
                                
                            }

                        }
                        
                    }
                }
                if (GetSessionParameter("Contrato.Guardado").ToString() != "1") // EC-06-08-2014 SE AGREGA PARA QUE AL GUARDAR UN CONTRATO POR PRIMERA VEZ NO OCULTE EL BOTON EDITAR
                    toolBar.MostrarBotonEditar(false);
                
            }

            #endregion
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public override void MostrarMensajeErrorMasterPrincipal(string mensaje)
    {
        toolBar.MostrarMensajeError(mensaje);
    }

    public override void MostrarMensajeGuardadoMasterPrincipal(string mensaje)
    {
        toolBar.MostrarMensajeGuardado(mensaje);
    }

    public override void LimpiarMensajeMasterPrincipal()
    {
        toolBar.LipiarMensajeError();
    }


}
