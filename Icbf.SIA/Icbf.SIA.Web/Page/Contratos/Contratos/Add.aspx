<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contrato_Add" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <style type="text/css">
        table .grillaCentral tr.rowAG td, table .grillaCentral tr.rowBG td
        {
            text-align: center;
            padding: 5px;
            padding-right: 0px;
        }
        .auto-style1 {
            margin-left: 0px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function CalculoPorcentaje(anticipo) {           
            document.getElementById('<%= txtValorAnticipo.ClientID %>').value = '0';             
            if (anticipo.value.length > 0) {
                var vic = document.getElementById('<%=hddValorInicial.ClientID %>').value;                
                if (vic.length > 0 && vic != 0) {
                    vic = vic.split('.').join('');
                    vic = vic.replace(',', '.');                                       
                    var porcentaje = (vic / 100 * anticipo.value);                    
                    document.getElementById('<%=txtValorAnticipo.ClientID %>').value = number_format(porcentaje,0);
                }
                if(anticipo.value > 50)
                {
                   
                    alert('El Valor de la forma de pago no debe ser mayor al 50% del valor del contrato');
                }
            }
        }
        
        
        function number_format(amount, decimals) {

            amount += ''; // por si pasan un numero en vez de un string
            amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

            decimals = decimals || 0; // por si la variable no fue fue pasada

            // si no es un numero o es igual a cero retorno el mismo cero
            if (isNaN(amount) || amount === 0)
                return parseFloat(0).toFixed(decimals);

            // si es mayor o menor que cero retorno el valor formateado como numero
            amount = '' + amount.toFixed(decimals);

            var amount_parts = amount.split('.'),
                regexp = /(\d+)(\d{3})/;

            while (regexp.test(amount_parts[0]))
                amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

            return amount_parts.join('.');
        }

        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function juegoContratoAsociado(control) {
            muestraImagenLoading();
            var chkConvenioMarco = document.getElementById('<%= chkConvenioMarco.ClientID %>');
            var chkContratoConvenioAd = document.getElementById('<%= chkContratoConvenioAd.ClientID %>');

            if (chkConvenioMarco.checked || chkContratoConvenioAd.checked) {
                if (control.id == chkConvenioMarco.id) {
                    chkContratoConvenioAd.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkConvenioMarco.ClientID %>', '');
                }
                else {
                    if (confirm('Esta cambio afecta valores relacionados con el contrato por lo que perderá información registrada de aquí en adelante. Desea continuar?')) {
                        chkConvenioMarco.checked = false;
                        prePostbck(false);
                        __doPostBack('<%= chkContratoConvenioAd.ClientID %>', '');
                    }
                    else {
                        chkContratoConvenioAd.checked = false //Reversa
                        ocultaImagenLoading();
                    }
                }
            }
            else {
                if (confirm('Esta cambio afecta valores relacionados con el contrato por lo que perderá información registrada de aquí en adelante. Desea continuar?')) {
                    if (control.id == chkConvenioMarco.id) {
                        prePostbck(false);
                        __doPostBack('<%= chkConvenioMarco.ClientID %>', '');
                    }
                    else {
                        prePostbck(false);
                        __doPostBack('<%= chkContratoConvenioAd.ClientID %>', '');
                    }
                }
                else {
                    control.checked = true;
                    ocultaImagenLoading();
                }
            }
        }

        function juegoReqActa(control) {
            muestraImagenLoading();
            var chkSiReqActa = document.getElementById('<%= chkSiReqActa.ClientID %>');
            var chkNoReqActa = document.getElementById('<%= chkNoReqActa.ClientID %>');

            if (chkSiReqActa.checked || chkNoReqActa.checked) {
                if (control.id == chkSiReqActa.id) {
                    chkNoReqActa.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkSiReqActa.ClientID %>', '');
                }
                else {
                    chkSiReqActa.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkNoReqActa.ClientID %>', '');
                }
            }
            else {
                if (confirm('Si no elige un valor se perderá la información registrada de aquí en adelante. Desea continuar?')) {
                    if (control.id == chkSiReqActa.id) {
                        prePostbck(false);
                        __doPostBack('<%= chkSiReqActa.ClientID %>', '');
                    }
                    else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoReqActa.ClientID %>', '');
                    }
                } else {
                    control.checked = true;
                    ocultaImagenLoading();
                }
            }
        }

        function juegoManejaAportes(control) {
            muestraImagenLoading();
            var chkSiManejaAportes = document.getElementById('<%= chkSiManejaAportes.ClientID %>');
            var chkNoManejaAportes = document.getElementById('<%= chkNoManejaAportes.ClientID %>');

            if (chkSiManejaAportes.checked || chkNoManejaAportes.checked) {
                if (control.id == chkSiManejaAportes.id) {
                    chkNoManejaAportes.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkSiManejaAportes.ClientID %>', '');
                }
                else {
                    if (chkSiManejaAportes.checked) {
                        if (confirm('Si elige que NO requiere aportes se perderá la información registrada en el acordeón "Aportes Contratistas e ICBF". Desea continuar?')) {
                            chkSiManejaAportes.checked = false;
                            prePostbck(false);
                            __doPostBack('<%= chkNoManejaAportes.ClientID %>', '');
                        } else {
                            chkSiManejaAportes.checked = true;
                            control.checked = false;
                            ocultaImagenLoading();
                        }
                    } else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoManejaAportes.ClientID %>', '');
                    }
                }
            }
            else {
                if (confirm('Si no elige un valor se perderá la información registrada de aquí en adelante. Desea continuar?')) {
                    if (control.id == chkSiManejaAportes.id) {
                        prePostbck(false);
                        __doPostBack('<%= chkSiManejaAportes.ClientID %>', '');
                    }
                    else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoManejaAportes.ClientID %>', '');
                    }
                } else {
                    control.checked = true;
                    ocultaImagenLoading();
                }
            }
        }

        function juegoManejaRecursos(control) {
            muestraImagenLoading();
            var chkSiManejaRecursos = document.getElementById('<%= chkSiManejaRecursos.ClientID %>');
            var chkNoManejaRecursos = document.getElementById('<%= chkNoManejaRecursos.ClientID %>');

            if (chkSiManejaRecursos.checked || chkNoManejaRecursos.checked) {
                if (control.id == chkSiManejaRecursos.id) {
                    chkNoManejaRecursos.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkSiManejaRecursos.ClientID %>', '');
                }
                else {
                    if (chkSiManejaRecursos.checked) {
                        if (confirm('Si elige que NO requiere aportes se perderá la información registrada en los acordeones "Plan de compras asociado / Productos" y "CDP". Desea continuar?')) {
                            chkSiManejaRecursos.checked = false;
                            prePostbck(false);
                            __doPostBack('<%= chkNoManejaRecursos.ClientID %>', '');
                        } else {
                            chkSiManejaRecursos.checked = true;
                            control.checked = false;
                            ocultaImagenLoading();
                        }
                    } else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoManejaRecursos.ClientID %>', '');
                    }
                }
            }
            else {
                if (confirm('Si no elige un valor se perderá la información registrada de aquí en adelante. Desea continuar?')) {
                    if (control.id == chkSiManejaRecursos.id) {
                        prePostbck(false);
                        __doPostBack('<%= chkSiManejaRecursos.ClientID %>', '');
                    }
                    else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoManejaRecursos.ClientID %>', '');
                    }
                } else {
                    control.checked = true;
                    ocultaImagenLoading();
                }
            }
        }


        function ValidarAportesEspecie() {

            var chkSiManejaVigFuturas = document.getElementById('<%= rblAportesEspecie.ClientID %>');

            if (confirm('Si elige que NO requiere aportes se perderá la información registrada en el acordeón "Aportes en especie ICBF". Desea continuar?')) {

                        prePostbck(false);
                        __doPostBack('<%= rblAportesEspecie.ClientID %>', '');
                }
        }

        function juegoManejaVigFuturas(control) {
            muestraImagenLoading();
            var chkSiManejaVigFuturas = document.getElementById('<%= chkSiManejaVigFuturas.ClientID %>');
            var chkNoManejaVigFuturas = document.getElementById('<%= chkNoManejaVigFuturas.ClientID %>');

            if (chkSiManejaVigFuturas.checked || chkNoManejaVigFuturas.checked) {
                if (control.id == chkSiManejaVigFuturas.id) {
                    chkNoManejaVigFuturas.checked = false;
                    prePostbck(false);
                    __doPostBack('<%= chkSiManejaVigFuturas.ClientID %>', '');
                }
                else {
                    if (chkSiManejaVigFuturas.checked) {
                        if (confirm('Si elige que NO maneja vigencias futuras se perderá la información registrada en el acordeón "Vigencias Futuras". Desea continuar?')) {
                            chkSiManejaVigFuturas.checked = false;
                            prePostbck(false);
                            __doPostBack('<%= chkNoManejaVigFuturas.ClientID %>', '');
                        } else {
                            chkSiManejaVigFuturas.checked = true;
                            control.checked = false;
                            ocultaImagenLoading();
                        }
                    } else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoManejaVigFuturas.ClientID %>', '');
                    }
                }
            }
            else {
                if (confirm('Si no elige un valor se perderá la información registrada de aquí en adelante. Desea continuar?')) {
                    if (control.id == chkSiManejaVigFuturas.id) {
                        prePostbck(false);
                        __doPostBack('<%= chkSiManejaVigFuturas.ClientID %>', '');
                    }
                    else {
                        prePostbck(false);
                        __doPostBack('<%= chkNoManejaVigFuturas.ClientID %>', '');
                    }
                } else {
                    control.checked = true;
                    ocultaImagenLoading();
                }
            }
        }


        function ValidaCheckReqActa(source, args) {
            var chkSiReqActa = document.getElementById('<%= chkSiReqActa.ClientID %>');
            var chkNoReqActa = document.getElementById('<%= chkNoReqActa.ClientID %>');

            if (chkSiReqActa.checked || chkNoReqActa.checked) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaCheckManejaAportes(source, args) {
            var chkSiManejaAportes = document.getElementById('<%= chkSiManejaAportes.ClientID %>');
            var chkNoManejaAportes = document.getElementById('<%= chkNoManejaAportes.ClientID %>');

            if (chkSiManejaAportes.checked || chkNoManejaAportes.checked) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaCheckManejaRecursos(source, args) {
            var chkSiManejaRecursos = document.getElementById('<%= chkSiManejaRecursos.ClientID %>');
            var chkNoManejaRecursos = document.getElementById('<%= chkNoManejaRecursos.ClientID %>');

            if (chkSiManejaRecursos.checked || chkNoManejaRecursos.checked) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaCheckManejaVigFut(source, args) {
            var chkSiManejaVigFuturas = document.getElementById('<%= chkSiManejaVigFuturas.ClientID %>');
            var chkNoManejaVigFuturas = document.getElementById('<%= chkNoManejaVigFuturas.ClientID %>');

            if (chkSiManejaVigFuturas.checked || chkNoManejaVigFuturas.checked) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }
         

        function ValidaPlanCompras(source, args) {
            var hfConsecutivosPlanCompras = document.getElementById('<%= hfConsecutivosPlanCompras.ClientID %>');
            if (parseInt(hfConsecutivosPlanCompras.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaProductos(source, args) {
            var hfProductos = document.getElementById('<%= hfProductos.ClientID %>');
            if (parseInt(hfProductos.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaAportes(source, args) {
            var hfAportes = document.getElementById('<%= hfAportes.ClientID %>');
            if (parseInt(hfAportes.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaLugarEjecucion(source, args) {
            var chkNivelNacional = document.getElementById('<%= chkNivelNacional.ClientID %>');
            var hfLugarEjecucion = document.getElementById('<%= hfLugarEjecucion.ClientID %>');
            if (chkNivelNacional.checked) {
                args.IsValid = true;
            } else {
                if (parseInt(hfLugarEjecucion.value) > 0) {
                    args.IsValid = true;
                } else {
                    args.IsValid = false;
                }
            }
        }

        function ValidaContratistas(source, args) {
            var hfContratistas = document.getElementById('<%= hfContratistas.ClientID %>');
            if (parseInt(hfContratistas.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaSupervInterv(source, args) {
            var hfSupervInterv = document.getElementById('<%= hfSupervInterv.ClientID %>');
            if (parseInt(hfSupervInterv.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaCDP(source, args) {
            var hfCDP = document.getElementById('<%= hfCDP.ClientID %>');
            if (parseInt(hfCDP.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaVigenciasFuturas(source, args) {
            var hfVigenciasFuturas = document.getElementById('<%= hfVigenciasFuturas.ClientID %>');              
            if (parseInt(hfVigenciasFuturas.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaCodigoSECOP(source, args) {
            var hfCodigoSECOP = document.getElementById('<%= hfCodigoSECOP.ClientID %>');              
            if (parseInt(hfCodigoSECOP.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function EjecutarJS(control) {
            //ConfiguraValidadores(false);
            switch (control.id) {
            case "cphCont_caFechaAdjudicaProceso_txtFecha":
                prePostbck(false);
                __doPostBack('<%= caFechaAdjudicaProceso.ClientID %>', '');
                break;
            case "cphCont_caFechaInicioEjecucion_txtFecha":
                prePostbck(false);
                __doPostBack('<%= caFechaInicioEjecucion.ClientID %>', '');
                break;
            case "cphCont_caFechaFinalizacionInicial_txtFecha":
                prePostbck(false);
                __doPostBack('<%= caFechaFinalizacionInicial.ClientID %>', '');
                break;
            default:
                break;
            }
        }

        function mensajeAlerta(control, texto) {
            alert(texto);
            var creference = document.getElementById(control);
            creference.focus();
        }

        function ValidaEliminacion() {
            return confirm('Esta seguro de que desea eliminar el registro?');
        }

        function PreGuardado() {
            ConfiguraValidadores(false);
            muestraImagenLoading();
        }

        function Aprobacion() {
            ConfiguraValidadores(true);
            window.Page_ClientValidate("btnAprobar");
            if (!window.Page_IsValid) {
                alert('Debe diligenciar los campos obligatorios');
                return false;
            } else {
                return confirm('Está seguro que desea Finalizar el registro del Contrato/Convenio?');
            }
        }

        function ConfiguraValidadores(habilitar) {

            var chkConvenioMarco = document.getElementById('<%= chkConvenioMarco.ClientID %>');
            var chkContratoConvenioAd = document.getElementById('<%= chkContratoConvenioAd.ClientID %>');
            if (chkConvenioMarco.checked || chkContratoConvenioAd.checked)
                ValidatorEnable($("#cphCont_rfvNumConvenioContrato")[0], habilitar);

            ValidatorEnable($("#cphCont_cvCategoriaContrato")[0], habilitar);
            ValidatorEnable($("#cphCont_cvTipoContratoConvenio")[0], habilitar);

            <%--var ddlModalidadAcademica = document.getElementById('<%= ddlModalidadAcademica.ClientID %>');
            if (!ddlModalidadAcademica.disabled)
                ValidatorEnable($("#cphCont_cvModalidadAcademica")[0], habilitar);

            var ddlNombreProfesion = document.getElementById('<%= ddlNombreProfesion.ClientID %>');
            if (!ddlNombreProfesion.disabled)
                ValidatorEnable($("#cphCont_cvNombreProfesion")[0], habilitar);--%>

            ValidatorEnable($("#cphCont_cvModalidadSeleccion")[0], habilitar);

            var pnNumeroProceso = document.getElementById('cphCont_PnNumeroProceso');
            if (pnNumeroProceso.style.visibility != 'hidden')
                ValidatorEnable($("#cphCont_rfvNumeroProceso")[0], habilitar);

            var chkSiReqActa = document.getElementById('<%= chkSiReqActa.ClientID %>');
            if (chkSiReqActa.checked)
                ValidatorEnable($("#cphCont_cuvReqActa")[0], habilitar);

            var chkSiManejaAportes = document.getElementById('<%= chkSiManejaAportes.ClientID %>');
            if (chkSiManejaAportes.checked) {
                ValidatorEnable($("#cphCont_cuvManejaAportes")[0], habilitar);
                ValidatorEnable($("#cphCont_cvAportes")[0], habilitar);
            }

            var chkSiManejaRecursos = document.getElementById('<%= chkSiManejaRecursos.ClientID %>');
            if (chkSiManejaRecursos.checked) {
                ValidatorEnable($("#cphCont_cvPlanCompras")[0], habilitar);
                ValidatorEnable($("#cphCont_cvProductos")[0], habilitar);
                ValidatorEnable($("#cphCont_cvConsecutivoPlanCompras")[0], habilitar);
                ValidatorEnable($("#cphCont_cvCDP")[0], habilitar);
            }

            var esContratoSinValor = document.getElementById('<%= hfContratoSV.ClientID %>');

            if (esContratoSinValor.value == "true") {
                ValidatorEnable($("#cphCont_cvCDP")[0], false);
            }

            ValidatorEnable($("#cphCont_cvRegimenContratacion")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvNombreSolicitante")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvRegionalContConv")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvNombreOrdenadorGasto")[0], habilitar);

            ValidatorEnable($("#cphCont_rfvObjeto")[0], habilitar);
            //ValidatorEnable($("#cphCont_rfvAlcance")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvValorInicialContConv")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvFechaFinalTerminacion")[0], habilitar);

            ValidatorEnable($("#cphCont_cvManejaVigFut")[0], habilitar);
            var chkSiManejaVigFuturas = document.getElementById('<%= chkSiManejaVigFuturas.ClientID %>');            
            if (chkSiManejaVigFuturas.checked) {
                ValidatorEnable($("#cphCont_cvVigenciasFuturas")[0], habilitar);
            }

            ValidatorEnable($("#cphCont_cvVigenciaFiscalIni")[0], habilitar);
            ValidatorEnable($("#cphCont_cvVigenciaFiscalFin")[0], habilitar);
            ValidatorEnable($("#cphCont_cvFormaPago")[0], habilitar);

            ValidatorEnable($("#cphCont_cvLugarEjecucion")[0], habilitar);

            ValidatorEnable($("#cphCont_cvContratistas")[0], habilitar);

            ValidatorEnable($("#cphCont_cvSupervInterv")[0], habilitar);
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostbck.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }

        function EjecutarJSFechaInicialSupervisores(control, fechaOriginal) {
            $("#lblError")[0].innerHTML = '';
            $("#lblError")[0].className = '';
            var fechaInicioEjecucion = document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value;
            var fechaInicioSupervisor = document.getElementById(control.id).value;

            if (fechaInicioEjecucion != '') {
                var splitfechainicio = fechaInicioEjecucion.split('/');
                var jfechaInicioEjecucion = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                splitfechainicio = fechaInicioSupervisor.split('/');
                var jfechaInicioSupervisor = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                if (jfechaInicioSupervisor < jfechaInicioEjecucion) {
                    document.getElementById(control.id).value = fechaOriginal;
                    alert('La Fecha de Inicio de Supervisor y/o Interventor debe ser mayor o igual a la Fecha de Ejecución de Contrato/Convenio');
                }
            }
        }

         function pageLoad(){
            var accordion = $find('<%= AccContratos.ClientID %>'+'_AccordionExtender');
            accordion.add_selectedIndexChanged(onACESelectedIndexChanged);
        }
        function onACESelectedIndexChanged(sender, eventArgs) {

            if (sender.get_SelectedIndex() == '2') {

                var porcentaje = document.getElementById('<%= txtPorcentajeAnticipo.ClientID %>');

                if (porcentaje != null) {
                    CalculoPorcentaje(porcentaje);
                }

            }
 
         }



    </script>
    <script type="text/javascript" language="javascript">
        /*****************************************************************************
        Código para colocar los indicadores de miles  y decimales mientras se escribe
        Script creado por Tunait!
        Si quieres usar este script en tu sitio eres libre de hacerlo con la condición de que permanezcan intactas estas líneas, osea, los créditos.

        http://javascript.tunait.com
        tunait@yahoo.com  27/Julio/03
        ******************************************************************************/
        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

        function onBlurTxtValorInicialContConv() {
            prePostbck(true)
            __doPostBack('<%= txtValorInicialContConv.ClientID %>', '');
        }
    </script>
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfContratoSV" runat="server" />
    <asp:HiddenField ID="hddValorInicial" runat="server" />
    <asp:HiddenField ID="hdfRol" runat="server" />
    <asp:HiddenField ID="hfConsecutivosPlanCompras" runat="server" />
    <asp:HiddenField ID="hfFechaAdjudicacionProceso" runat="server" />

    <table width="90%" align="center">
        <tr>
            <td>
                <Ajax:Accordion ID="AccContratos" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                    ContentCssClass="accordionContent" OnItemCommand="AccContratos_ItemCommand" runat="server"
                    Width="100%" Height="100%">
                    <Panes>
                        <Ajax:AccordionPane ID="ApDatosGeneralContrato" runat="server">
                            <Header>
                                Datos Generales del contrato
                            </Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td colspan="2">
                                            <div id="dvUsuarioCreacion" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr class="rowB">
                                                        <td>
                                                            Nombre del usuario *
                                                        </td>
                                                        <td>
                                                            Regional del usuario *
                                                            <asp:RequiredFieldValidator runat="server" ID="rfvRegionalUsuario" ControlToValidate="txtRegionalUsuario"
                                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
<%--                                                            <asp:CompareValidator runat="server" ID="cvRegionalUsuarioContrato" ControlToValidate="ddlRegionalUsuarioContrato"
                                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td class="Cell">
                                                            <asp:TextBox ID="txtNombreUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                                        </td>
                                                        <td class="Cell">
                                                            <asp:HiddenField ID="hfRegional" runat="server" />
                                                            <asp:TextBox ID="txtRegionalUsuario" runat="server" Enabled="false" Width="80%"></asp:TextBox>
<%--                                                            <asp:DropDownList ID="ddlRegionalUsuarioContrato" runat="server" Visible="false"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlRegionalUsuarioContratoSelectedIndexChanged">
                                                            </asp:DropDownList>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Fecha de Registro al Sistema *
                                        </td>
                                        <td>
                                            Id Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtIdContrato"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtFechaRegistro" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtIdContrato" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="lblContratoMigrado" runat="server" Text="Número de Contrato/contrato Migrado" />      
                                        </td>
                                        <td>
                                            Contrato Asociado
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                                                              
                                        <td>
                                             <asp:TextBox ID="txtNumContMigrado" runat="server" Enabled="false"></asp:TextBox>                                          
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hfFechaInicioEjecConvMarco" runat="server" />
                                            <asp:HiddenField ID="hfFechaFinalTerminContConvMarco" runat="server" />
                                            <asp:HiddenField ID="hfFechaSuscripcionContConvMarco" runat="server" />
                                            <asp:CheckBox ID="chkConvenioMarco" runat="server" onclick="" />
                                            Convenio Marco
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                            
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hfFechaInicioEjecContConvAd" runat="server" />
                                            <asp:HiddenField ID="hfFechaFinalTerminContConvAd" runat="server" />
                                            <asp:CheckBox ID="chkContratoConvenioAd" runat="server" onclick="juegoContratoAsociado(this)" />
                                            Contrato/Convenio Adhesión
                                        </td>
                                    </tr>                                                                        
                                        
                                    </tr>                                    
                                    
                                    <tr class="rowB">
                                        <td rowspan="2" style="padding: 0px">
                                            <div id="PnValorConvenio" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbNumConvenioContrato" runat="server"></asp:Label>
                                                            <asp:RequiredFieldValidator runat="server" ID="rfvNumConvenioContrato" ControlToValidate="txtNumConvenioContrato"
                                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td class="Cell">
                                                            <asp:HiddenField ID="hfNumConvenioContratoAsociado" runat="server" />
                                                            <asp:HiddenField ID="hfIdConvenioContratoAsociado" runat="server" />
                                                            <asp:TextBox ID="txtNumConvenioContrato" runat="server" Enabled="false" Width="80%"
                                                                ViewStateMode="Enabled" OnTextChanged="txtNumConvenioContratoTextChanged" AutoPostBack="true"></asp:TextBox>
                                                            <asp:ImageButton ID="imgNumConvenioContrato" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                                Enabled="false" OnClientClick="GetNumConvenioContrato(); return false;" Style="cursor: hand"
                                                                ToolTip="Buscar" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td style="padding: 0px">
                                            Categoría de Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvCategoriaContrato" ControlToValidate="ddlCategoriaContrato"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" style="padding-bottom: 4px">
                                            <asp:DropDownList ID="ddlCategoriaContrato" runat="server" onchange="prePostbck(true)"
                                                OnSelectedIndexChanged="ddlCategoriaContratoSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Tipo de Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvTipoContratoConvenio" ControlToValidate="ddlTipoContratoConvenio"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="false"></asp:CompareValidator>
                                        </td>
                                        <td>
                                            Modalidad Académica
                                            <%--<asp:CompareValidator runat="server" ID="cvModalidadAcademica" ControlToValidate="ddlModalidadAcademica"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="false"></asp:CompareValidator>--%>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlTipoContratoConvenio" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                OnSelectedIndexChanged="ddlTipoContratoConvenioSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlModalidadAcademica" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                OnSelectedIndexChanged="ddlModalidadAcademicaSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="lbNombreProfesion" runat="server" Text="Nombre de la profesión"></asp:Label>
                                            <%--<asp:CompareValidator runat="server" ID="cvNombreProfesion" ControlToValidate="ddlNombreProfesion"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
                                        </td>
                                        <td>
                                            Modalidad de Selección *
                                            <asp:CompareValidator runat="server" ID="cvModalidadSeleccion" ControlToValidate="ddlModalidadSeleccion"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlNombreProfesion" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                OnSelectedIndexChanged="ddlNombreProfesionSeleccionSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlModalidadSeleccion" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                OnSelectedIndexChanged="ddlModalidadSeleccionSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <div id="PnNumeroProceso" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr class="rowB">
                                                        <td>
                                                            Número de proceso
                                                            <asp:RequiredFieldValidator runat="server" ID="rfvNumeroProceso" ControlToValidate="txtNumeroProceso"
                                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td class="Cell">
                                                            <asp:HiddenField ID="hfNumeroProceso" runat="server" />
                                                            <asp:HiddenField ID="hfIdNumeroProceso" runat="server" />
                                                            <asp:TextBox ID="txtNumeroProceso" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                                                OnTextChanged="txtNumeroProcesoTextChanged" AutoPostBack="true"></asp:TextBox>
                                                            <asp:ImageButton ID="imgNumeroProceso" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                                OnClientClick="GetNumeroProceso(); return false;" Style="cursor: hand" ToolTip="Buscar" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="PnFechaAdjudicaProceso" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr class="rowB">
                                                        <td>
                                                            Fecha Adjudicación del Proceso
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td class="Cell">
                                                            <uc1:fecha ID="caFechaAdjudicaProceso" runat="server" Enabled="false" Requerid="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
<%--                                     <tr class="rowB">
                                    <td>
                                        
                                        Codigo SECOP *
                                            <asp:CompareValidator runat="server" ID="cvCodigoSECOP" ControlToValidate="ddlCodigoSECOP"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                    </td>                                    
                                   
                                </tr>
                                    <tr class="rowA">
                                    <td>
                                         <asp:DropDownList ID="ddlCodigoSECOP" runat="server" Enabled="true" onchange="prePostbck(true)"
                                                 AutoPostBack="true">
                                            </asp:DropDownList>
                                    </td>                                                                    
                                     
                                </tr>--%> 
                                     <tr class="rowB">
                                    <td>
                                        
                                        Requiere Garantia *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvRequiereGarantia" ControlToValidate="rblRequiereGarantia"
                                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                             ForeColor="Red"></asp:RequiredFieldValidator>    
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="rblRequiereGarantia"
                                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                             ForeColor="Red"></asp:RequiredFieldValidator>
                                    </td>                                    
                                    <td> 
                                          Aportes en Especie ICBF *
                                        <asp:RequiredFieldValidator runat="server" ID="rfvAportesEspecie" ControlToValidate="rblAportesEspecie"
                                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                             ForeColor="Red"></asp:RequiredFieldValidator>    
                                            <asp:RequiredFieldValidator runat="server" ID="rfvAportesEspecie2" ControlToValidate="rblAportesEspecie"
                                             SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                             ForeColor="Red"></asp:RequiredFieldValidator>                                                                                
                                    </td>
                                </tr>
                                    <tr class="rowA">
                                    <td>
                                         <asp:RadioButtonList runat="server" ID="rblRequiereGarantia" RepeatDirection="Horizontal"></asp:RadioButtonList>  &nbsp;
                                    </td>
                                    <td>                                         
                                        <asp:RadioButtonList runat="server" ID="rblAportesEspecie" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblAportesEspecie_SelectedIndexChanged" ></asp:RadioButtonList> 
                                    </td>                                   
                                     
                                </tr> 
                                    <tr class="rowB">
                                        <td>
                                            <div id="PnRequiereActaInicio" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr class="rowB">
                                                        <td>
                                                            Requiere Acta de Inicio *
                                                            <asp:CustomValidator ID="cuvReqActa" runat="server" ErrorMessage="Campo Requerido"
                                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCheckReqActa"></asp:CustomValidator>
                                                        </td>
                                                        <%--<td colspan="2">  
                                                            <asp:Label runat="server" ID="lblFeActaInicio" Text="Fecha Acta de Inicio" Visible="true"></asp:Label>                                                      
                                                        &nbsp;</td> --%>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td>
                                                            <asp:CheckBox ID="chkSiReqActa" runat="server" onclick="juegoReqActa(this)" />
                                                            Si
                                                        </td>
                                                        <%--<td colspan="2">
                                                           <asp:TextBox runat="server" ID="txtFechaActa" visible ="true" Enabled="true" OnTextChanged="txtFechaActa_OnTextChanged" AutoPostBack="True"></asp:TextBox>
                                                                <asp:Image ID="ImageFechaActa" runat="server" Visible="false" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                                                                 <Ajax:CalendarExtender ID="CalendarExtenderFechaActa" runat="server" Format="dd/MM/yyyy" 
                                                                        PopupButtonID="ImageFechaActa" TargetControlID="txtFechaActa"></Ajax:CalendarExtender>
                                                                 <Ajax:MaskedEditExtender ID="meetxtFechaActa" runat="server" CultureAMPMPlaceholder="AM;PM"
                                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaActa">
                                                                 </Ajax:MaskedEditExtender>
                                                        &nbsp;</td> --%>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td>
                                                            <asp:CheckBox ID="chkNoReqActa" runat="server" onclick="juegoReqActa(this)" />
                                                            No
                                                        </td>
                                                        <%--<td colspan="2">                                                        
                                                        &nbsp;</td>--%> 
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="PnManejaAportes" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr class="rowB">
                                                        <td>
                                                            Maneja Cofinanciaci&#243;n *
                                                            <asp:CustomValidator ID="cuvManejaAportes" runat="server" ErrorMessage="Campo Requerido"
                                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCheckManejaAportes"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td>
                                                            <asp:CheckBox ID="chkSiManejaAportes" runat="server" onclick="juegoManejaAportes(this)" />
                                                            Si
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td>
                                                            <asp:CheckBox ID="chkNoManejaAportes" runat="server" onclick="juegoManejaAportes(this)" />
                                                            No
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td rowspan="2" style="padding: 0px">
                                            <div id="PnManejaRecursos" runat="server" style="visibility: hidden">
                                                <table width="100%">
                                                    <tr class="rowB">
                                                        <td>
                                                            Maneja Recursos ICBF*
                                                            <asp:CustomValidator ID="cuvManejaRecursos" runat="server" ErrorMessage="Campo Requerido"
                                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCheckManejaRecursos"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td>
                                                            <asp:CheckBox ID="chkSiManejaRecursos" runat="server" onclick="juegoManejaRecursos(this)" />
                                                            Si
                                                        </td>
                                                    </tr>
                                                    <tr class="rowA">
                                                        <td>
                                                            <asp:CheckBox ID="chkNoManejaRecursos" runat="server" onclick="juegoManejaRecursos(this)" />
                                                            No
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td style="padding: 0px">
                                            Régimen de Contratación *
                                            <asp:CompareValidator runat="server" ID="cvRegimenContratacion" ControlToValidate="ddlRegimenContratacion"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" style="padding-bottom: 15px">
                                            <asp:DropDownList ID="ddlRegimenContratacion" runat="server" onchange="prePostbck(true)"
                                                OnSelectedIndexChanged="ddlRegimenContratacionSelectedIndexChanged" AutoPostBack="true"
                                                Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Nombre del Solicitante *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombreSolicitante" ControlToValidate="txtNombreSolicitante"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Regional del Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvRegionalContConv" ControlToValidate="txtRegionalContratoConvenio"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfNombreSolicitante" runat="server" />
                                            <asp:TextBox ID="txtNombreSolicitante" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled" OnTextChanged="txtNombreSolicitanteTextChanged"></asp:TextBox>
                                            <asp:ImageButton ID="imgNombreSolicitante" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetNombreSolicitante(); return false;" Style="cursor: hand" ToolTip="Buscar"
                                                Enabled="false" />
                                        </td>
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfRegionalContConv" runat="server" />
                                            <asp:HiddenField ID="hfCodRegionalContConv" runat="server" />
                                            <asp:TextBox ID="txtRegionalContratoConvenio" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Dependencia Solicitante
                                        </td>
                                        <td>
                                            Cargo el Solicitante
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfDependenciaSolicitante" runat="server" />
                                            <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCargoSolicitante" runat="server" />
                                            <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Nombre del Ordenador del Gasto *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombreOrdenadorGasto" ControlToValidate="txtNombreOrdenadorGasto"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Tipo Identificación Ordenador del Gasto
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfNombreOrdenadorGasto" runat="server" />
                                            <asp:TextBox ID="txtNombreOrdenadorGasto" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled" OnTextChanged="txtNombreOrdenadorGastoTextChanged"></asp:TextBox>
                                            <asp:ImageButton ID="imgNombreOrdenadorGasto" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetNombreOrdenadorGasto(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" Enabled="false" />
                                        </td>
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfTipoIdentOrdenadorGasto" runat="server" />
                                            <asp:TextBox ID="txtTipoIdentOrdenadorGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Número Identificación Ordenador del Gasto
                                        </td>
                                        <td>
                                            Cargo Ordenador del Gasto
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfNumeroIdentOrdenadoGasto" runat="server" />
                                            <asp:TextBox ID="txtNumeroIdentOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCargoOrdenadoGasto" runat="server" />
                                            <asp:TextBox ID="txtCargoOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApPlanComprasProductos" runat="server">
                            <Header>
                                Plan de compras asociado / Productos
                            </Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="lbPlanCompras" runat="server" Text="Plan de compras"></asp:Label>
                                            <asp:CustomValidator ID="cvPlanCompras" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaPlanCompras"></asp:CustomValidator>
                                        
                                        </td>
                                        <td colspan="2">
                                           <asp:Label ID="lblValorPlanComprasContratos" runat="server" Text="Valor Plan de compras contrato"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            
                                            <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="false" Width="30%" ViewStateMode="Enabled"
                                                OnTextChanged="txtPlanComprasTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgPlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetPlanCompras(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtValorPlanComprasContratos"  
                                                runat="server" 
                                                
                                                onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')" Enabled="false" Width="250px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="gvConsecutivos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" DataKeyNames="IDPlanDeComprasContratos,IDPlanDeCompras,Vigencia"
                                                CellPadding="8" Height="16px" CssClass="grillaCentral">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="IDPlanDeCompras" />
                                                    <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                                <RowStyle CssClass="rowBG" HorizontalAlign="Center" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="LbProductos" runat="server" Text="Productos"></asp:Label>
                                            <asp:HiddenField ID="hfProductos" runat="server" />
                                            <asp:CustomValidator ID="cvProductos" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaProductos"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />
                                                    <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Rubros Plan de Compras
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" DataKeyNames="total_rubro" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código Rubro" DataField="codigo_rubro" />
                                                    <asp:BoundField HeaderText="Descripción del Rubro" DataField="NombreRubro" />
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" DataField="" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Consecutivo Plan de Compras
                                            <asp:CompareValidator runat="server" ID="cvConsecutivoPlanCompras" ControlToValidate="DdlConsecutivoPlanCompras"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="DdlConsecutivoPlanCompras" runat="server" Width="30%" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true" OnSelectedIndexChanged="DdlConsecutivoPlanComprasSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApVigenciaValorContrato"  runat="server">
                            <Header>
                                Vigencia y valor contrato
                            </Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Objeto del Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvObjeto" ControlToValidate="txtObjeto"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="2">
                                            Alcance del Objeto del Contrato/Convenio 
                                            <%--<asp:RequiredFieldValidator runat="server" ID="rfvAlcance" ControlToValidate="txtAlcance"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfObjeto" runat="server" />
                                            <asp:TextBox ID="txtObjeto" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true" OnTextChanged="txtObjetoTextChanged"
                                                MaxLength="500" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);"></asp:TextBox>
                                        </td>
                                        <td class="Cell" colspan="2">
                                            <asp:HiddenField ID="hfAlcance" runat="server" />
                                            <asp:TextBox ID="txtAlcance" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true" OnTextChanged="txtAlcanceTextChanged"
                                                MaxLength="4000" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Valor Inicial del Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvValorInicialContConv" ControlToValidate="txtValorInicialContConv"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="2">
                                                   <label id="lblFechaInicioEjecucion" 
                                                   runat="server" 
                                                   title="Favor registre la posible fecha de suscripción del contrato,  ya que esta fecha es dinámica y una vez se aprueben las garantías o se registe la fecha de firma del acta de inicio se actualizara con la fecha correspondiente" >
                                                Fecha de Inicio de ejecución del Contrato/Convenio * 
                                            </label>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfValorInicialContConv" runat="server" />
                                            <asp:TextBox ID="txtValorInicialContConv" runat="server" Enabled="false" onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')"
                                                onblur="onBlurTxtValorInicialContConv()" onchange="prePostbck(true)" MaxLength="25"
                                                Width="300px" AutoPostBack="true" OnTextChanged="txtValorInicialContConvTextChanged"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftValorInicialContConv" runat="server" TargetControlID="txtValorInicialContConv"
                                                FilterType="Custom,Numbers" ValidChars=".," />
                                        </td>
                                        <td class="Cell" colspan="2" >
                                            <table>
                                                <tr class="rowA">
                                                    <td class="Cell">
                                                        <asp:HiddenField ID="hfFechaInicioEjecucion" runat="server" />
                                                        <uc1:fecha ID="caFechaInicioEjecucion" runat="server" Enabled="false" Requerid="true" />
                                                    </td>
                                                </tr>
                                                <tr class="rowA">
                                                    <td class="Cell">
                                                        <asp:CheckBox runat="server" AutoPostBack="true" ID="chkCalculaFecha" Text="PLAZO DE EJECUCION EN MM Y DD" OnCheckedChanged="chkCalculaFecha_CheckedChanged"   Enabled="false" /> <br />
                                                        <asp:ImageButton ID="ImgBtnFechaFinal" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetCalculoFechaFinal(); return false;" Visible="false" Style="cursor: hand" ToolTip="Buscar"
                                                 />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Fecha de Finalización Inicial del Contrato/Convenio *
                                        </td>
                                        <td colspan="2">
                                            Plazo Inicial de Ejecución
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" rowspan="4">
                                            <asp:HiddenField ID="hfFechaFinalizacion" runat="server" />
                                            <uc1:fecha ID="caFechaFinalizacionInicial" runat="server" Enabled="false" Requerid="true" />
                                            
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Días
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDiasPiEj" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Meses
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMesesPiEj" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Años
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAcnosPiEj" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Fecha Final de Terminación contrato/convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvFechaFinalTerminacion" ControlToValidate="txtFechaFinalTerminacion"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="2">
                                            Maneja Vigencia Futuras *
                                            <asp:CustomValidator ID="cvManejaVigFut" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCheckManejaVigFut"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" rowspan="3">
                                            <asp:TextBox ID="txtFechaFinalTerminacion" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkSiManejaVigFuturas" runat="server" Enabled="false" onclick="juegoManejaVigFuturas(this)" />
                                            Si
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkNoManejaVigFuturas" runat="server" Enabled="false" onclick="juegoManejaVigFuturas(this)" />
                                            No
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Vigencia Fiscal Inicial del Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvVigenciaFiscalIni" ControlToValidate="ddlVigenciaFiscalIni"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                        <td colspan="2">
                                            Vigencia Fiscal Final del Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvVigenciaFiscalFin" ControlToValidate="ddlVigenciaFiscalFin"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlVigenciaFiscalIni" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlVigenciaFiscalIniSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Cell" colspan="2">
                                            <asp:DropDownList ID="ddlVigenciaFiscalFin" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlVigenciaFiscalFinSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                           Anticipos  y/o Pago Anticipado  
                                            <%--<asp:CompareValidator runat="server" ID="cvFormaPago" ControlToValidate="ddlFormaPago"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
                                        </td>
                                        <td>
                                            Tipos de Pago
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlFormaPago" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlFormaPagoSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Cell"colspan="2">
                                            <asp:DropDownList ID="ddlTipoFormaPago" runat="server" Enabled="false" onchange="prePostbck(true)"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlTipoFormaPagoSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                     <tr class="rowB">
                                        <td>
                                             % Anticipo/Anticipado
                                        </td>
                                        <td>
                                            Valor del Anticipo/Anticipado 
                                            <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="TxtValorAnticipo"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="true" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>                                           
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">                                                 
                                            <asp:TextBox ID="txtPorcentajeAnticipo" runat="server" Enabled="true" MaxLength="2" onKeyup="CalculoPorcentaje(this);" onblur="CalculoPorcentaje(this);"></asp:TextBox>
                                             <Ajax:FilteredTextBoxExtender ID="FtPorcentajeAnticipo" runat="server" TargetControlID="txtPorcentajeAnticipo"
                                               FilterType="Custom,Numbers" ValidChars=""  />                                      
                                        </td>
                                        <td class="Cell"colspan="2">
                                            <asp:TextBox ID="txtValorAnticipo" runat="server" Enabled="false" MaxLength="17"
                                            ></asp:TextBox>
                                             <Ajax:FilteredTextBoxExtender ID="ftValorAnticipo" runat="server" TargetControlID="txtValorAnticipo"
                                               FilterType="Custom,Numbers" ValidChars=".,"  /> 
                                        </td>
                                    </tr>                                    
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApContratista" runat="server">
                            <Header>
                                Contratista</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Contratista *
                                            <asp:CustomValidator ID="cvContratistas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaContratistas"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfContratistas" runat="server" />
                                            <asp:TextBox ID="txtContratista" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                OnTextChanged="txtContratistaTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetContratista(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Contratistas relacionados
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Medio Pago" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Integrantes relacionados al consorcio o unión temporal
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvIntegrantesConsorcio_UnionTemp" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Constratista" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacionIntegrante" />
                                                    <asp:BoundField HeaderText="Información Integrante" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Porcentaje Participación" DataField="PorcentajeParticipacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApAportes" runat="server">
                            <Header>
                                Detalle Valor Inicial del Contrato</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Valor ICBF 
                                            <asp:HiddenField ID="hfAportes" runat="server" />
                                            <asp:CustomValidator ID="cvAportes" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaAportes"></asp:CustomValidator>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Valor Cofinanciaci&#243;n Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoICBF" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                                OnTextChanged="txtValorApoICBFTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoICBF" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetValorApoICBF(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoContrat" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                                OnTextChanged="txtValorApoContratTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoContrat" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" Visible ="true" OnClientClick="GetValorApoContrat(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes ICBF
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false" DataKeyNames="IdAporteContrato"
                                                AllowSorting="true" OnSorting="gvAportesICBF_Sorting" GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo de Aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripci&oacute;n Cofinanciaci&oacute;n Especie" DataField="DescripcionAporte"
                                                        SortExpression="DescripcionAporte" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarAporteICBFClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesContratista" runat="server" AutoGenerateColumns="false"
                                                DataKeyNames="IdAporteContrato" AllowSorting="true" OnSorting="gvAportesContratista_Sorting"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Contratista" DataField="NumeroIdentificacionContratista"
                                                        SortExpression="NumeroIdentificacionContratista" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="InformacionAportante"
                                                        SortExpression="InformacionAportante" />
                                                    <asp:BoundField HeaderText="Tipo Aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripción Aporte Especie" DataField="DescripcionAporte"
                                                        SortExpression="DescripcionAporte" />
                                                    <asp:BoundField HeaderText="Fecha RP" DataField="FechaRP" SortExpression="FechaRP"
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Número RP" DataField="NumeroRP" SortExpression="NumeroRP" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarAporteContratistaClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApLugarEjecucion" runat="server">
                            <Header>
                                Lugar de ejecución</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Lugar de ejecución *
                                            <asp:CustomValidator ID="cvLugarEjecucion" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaLugarEjecucion"></asp:CustomValidator>
                                        </td>
                                        <td>
                                            Datos Adicionales lugar de ejecución
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfLugarEjecucion" runat="server" />
                                            <asp:TextBox ID="txtLugarEjecucion" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                OnTextChanged="txtLugarEjecucionTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgLugarEjecucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetLugarEjecucion(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtDatosAdicionalesLugarEjecucion" runat="server" Width="80%" TextMode="MultiLine"
                                                Enabled="false" AutoPostBack="true" OnTextChanged="txtDatosAdicionalesLugarEjecucionTextChanged"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftDatosAdicionalesLugarEjecucion" runat="server"
                                                TargetControlID="txtDatosAdicionalesLugarEjecucion" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                ValidChars="áéíóúÁÉÍÓÚñÑ -" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkNivelNacional" runat="server" Enabled="false" AutoPostBack="true"
                                                OnCheckedChanged="chkNivelNacionalCheckedChanged" />
                                            Nivel Nacional
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <div id="dvLugaresEjecucion" runat="server">
                                                <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos"
                                                    AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                        <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                        <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarLugEjecucionClick"
                                                                    OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApSupervisorInterventor" runat="server">
                            <Header>
                                Supervisor y/o Interventor</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Supervisor y/o Interventor relacionados *
                                            <asp:CustomValidator ID="cvSupervInterv" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaSupervInterv"></asp:CustomValidator>
                                        </td>                                        
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfSupervInterv" runat="server" />
                                            <asp:TextBox ID="txtSuperInterv" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                OnTextChanged="txtSuperIntervTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgSuperInterv" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetSuperInterv(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>                                       
                                    </tr>                                   
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Supervisor y/o Interventor relacionados
                                        </td>                                        
                                    </tr>                                    
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <div id="dvSupervInterv" runat="server">
                                                <asp:GridView ID="gvSupervInterv" runat="server" AutoGenerateColumns="false" DataKeyNames="IDSupervisorIntervContrato,FechaInicio"
                                                    OnRowDataBound="gvSupervIntervOnRowDataBound" GridLines="None" CellPadding="8"
                                                    Height="16px">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Supervisor y/o Interventor" DataField="EtQSupervisorInterventor" />
                                                        <asp:BoundField HeaderText="Tipo Supervisor y/o Interventor" DataField="EtQInternoExterno" />
                                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                <asp:TextBox ID="txtFechaInicio" runat="server" Visible="false" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'
                                                                    Width="73px" MaxLength="10"></asp:TextBox>
                                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio"
                                                                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                                                                    SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar" Display="Dynamic"></asp:CompareValidator>
                                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
                                                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                                </Ajax:MaskedEditExtender>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                        <asp:BoundField HeaderText="Número Identificación" DataField="Identificacion" />
                                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                                       <%-- <asp:BoundField HeaderText="Tipo Vinculación Contractual" DataField="SupervisorInterventor.TipoVinculacionContractual" />
                                                        <asp:BoundField HeaderText="Información Supervisor y/o Interventor" DataField="SupervisorInterventor.NombreCompleto" />--%>
                                                       <%-- <asp:BoundField HeaderText="Regional" DataField="SupervisorInterventor.Regional" />
                                                        <asp:BoundField HeaderText="Dependencia" DataField="SupervisorInterventor.Dependencia" />
                                                        <asp:BoundField HeaderText="Cargo" DataField="SupervisorInterventor.Cargo" />
                                                        <asp:BoundField HeaderText="Dirección" DataField="SupervisorInterventor.Direccion" />
                                                        <asp:BoundField HeaderText="Teléfono" DataField="SupervisorInterventor.Telefono" />--%>
                                                        <asp:BoundField HeaderText="Número Contrato Interventoría" DataField="NumeroContratoInterventoria" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarGvSupervIntervClick"
                                                                    OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Director Interventoria
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvDirectoresInterv" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="80%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="Identificacion" />
                                                    <asp:BoundField HeaderText="Información Director Interventoría" DataField="SupervisorInterventor.NombreCompleto" />
                                                    <asp:BoundField HeaderText="Teléfono" DataField="SupervisorInterventor.Telefono" />
                                                    <asp:BoundField HeaderText="Celular" DataField="SupervisorInterventor.Celular" />
                                                    <asp:BoundField HeaderText="Correo Electrónico" DataField="SupervisorInterventor.CorreoElectronico" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApCDP" runat="server">
                            <Header>
                                CDP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbCDP" runat="server" Text="CDP"></asp:Label>
                                            <asp:CustomValidator ID="cvCDP" runat="server" ErrorMessage="Campo Requerido" Enabled="true"
                                                ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCDP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCDP" runat="server" />
                                            <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" OnTextChanged="txtCDPTextChanged"
                                                AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetCDP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                         <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="gvCDP_PageIndexChanging" DataKeyNames="IdContratosCDP,ValorCDP"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion();"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP"  DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                     <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="CodigoRubro"
                                                AllowPaging="True"  GridLines="None" 
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                        <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                        <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DependenciaAfectacionGastos"/>
                                                        <asp:BoundField HeaderText="Cod Rubro Presupuestal" DataField="CodigoRubro" />
                                                        <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                                                        <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
                                                        <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="TipoSituacionFondos"/>
                                                        <asp:BoundField HeaderText="Valor Rubro" DataField="ValorRubro"  DataFormatString="{0:C}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApVigenciasFuturas" runat="server">
                            <Header>
                                Vigencias Futuras</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbVigenciasFuturas" runat="server" Text="Vigencias Futuras"></asp:Label>
                                            <asp:CustomValidator ID="cvVigenciasFuturas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="False" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaVigenciasFuturas"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfVigenciasFuturas" runat="server" />
                                            <asp:TextBox ID="txtVigFuturas" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                OnTextChanged="txtVigFuturasTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgVigFuturas" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetVigFuturas(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvVigFuturas" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IDVigenciaFuturas" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número del Radicado Vigencia Futura" DataField="NumeroRadicado" />
                                                    <asp:BoundField HeaderText="Fecha de Expedición Vigencia Futura" DataField="FechaExpedicion"
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor Vigencia Futura" DataField="ValorVigenciaFutura"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Año Vigencia Futura" DataField="AnioVigencia" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarGvVigFuturasClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApCodigosSECOP" runat="server">
                            <Header>
                               Código SECOP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="Label1" runat="server" Text="Códigos SECOP"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCodigoSECOP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCodigoSECOP" runat="server" Value="0" />
                                            <asp:TextBox ID="txtCodigosSECOP" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                OnTextChanged="txtCodigosSECOP_TextChanged"  AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgbtnCodigosSECOP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetCodigosSECOP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvCodigosSECOP" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="key" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Código SECOP" DataField="Value" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminarCodigoSECOP" runat="server" OnClick="btnEliminarCodigoSECOP_Click"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                    </Panes>
                </Ajax:Accordion>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfPostbck" runat="server" />
    <asp:HiddenField ID="hfIdEstadoContrato" runat="server" />
    <asp:HiddenField ID="hfCodContratoAsociadoSel" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaConvenio" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServApoyoGestion" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServProfesionales" runat="server" />
    <asp:HiddenField ID="hfIdTipoContAporte" runat="server" />
    <asp:HiddenField ID="hfIdMarcoInteradministrativo" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirecta" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirectaAporte" runat="server" />
    <asp:HiddenField ID="hfTotalProductos" runat="server" />
    <asp:HiddenField ID="hfTotalProductosCDP" runat="server" />
    <asp:HiddenField ID="hfAcordeonActivo" runat="server" />
    <asp:HiddenField ID="hfObjPlan" runat="server" />
    <asp:HiddenField ID="hfAlcPlan" runat="server" />
    <script type="text/javascript" language="javascript">
        
        function GetFormaPago() {
            window_showModalDialog('../../../Page/Contratos/LupaFormapago/List.aspx', setReturnGetFormaPagos, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetFormaPagos(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 1) {
                  //  document.getElementById('<%= hfIdNumeroProceso.ClientID %>').value = retLupa[0];
                   // document.getElementById('<%= hfNumeroProceso.ClientID %>').value = retLupa[1];

                 <%--   prePostbck(false);
                    __doPostBack('<%= txtNumeroProceso.ClientID %>', '');--%>
                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }
        }

       function GetCalculoFechaFinal() {
           muestraImagenLoading();

           var anio = '<%= caFechaInicioEjecucion.Date.Year  %>';
           var mes = '<%= caFechaInicioEjecucion.Date.Month  %>';
           var dia = '<%= caFechaInicioEjecucion.Date.Day  %>';
           
           var diasCalculados = '<%= txtDiasPiEj.Text  %>';
           var mesesCalculados = '<%= txtMesesPiEj.Text  %>';

           var tipoContrato = '<%= ddlTipoContratoConvenio.SelectedValue %>';

           window_showModalDialog('../../../Page/Contratos/Lupas/LupaCalcularFecha.aspx?anio=' + anio + '&mes=' + mes + '&dia=' + dia + '&diasCalculados=' + diasCalculados + '&mesesCalculados=' + mesesCalculados + '&tipoContrato=' + tipoContrato, setReturnCalculoFechaFinal, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
       }

       function setReturnCalculoFechaFinal() {
           prePostbck(false);
           __doPostBack('<%= caFechaFinalizacionInicial.ClientID %>', '');
       }

        function GetNumConvenioContrato() {
            muestraImagenLoading();
            //CU-46-CONT-CONS-CONT
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratoNew.aspx', setReturnGetNumConvenioContrato, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetNumConvenioContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 0) {
                    document.getElementById('<%= hfIdConvenioContratoAsociado.ClientID %>').value = retLupa[0];  //'1';

                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }
        }

        function GetNumeroProceso() {
            muestraImagenLoading();
            //CU-033-CONT-NUM_PRO
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaNumeroProceso.aspx', setReturnGetNumeroProceso, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetNumeroProceso(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 1) {
                    document.getElementById('<%= hfIdNumeroProceso.ClientID %>').value = retLupa[0];
                    document.getElementById('<%= hfNumeroProceso.ClientID %>').value = retLupa[1];
                    document.getElementById('<%= hfFechaAdjudicacionProceso.ClientID %>').value = retLupa[2];

                    prePostbck(false);
                    __doPostBack('<%= txtNumeroProceso.ClientID %>', '');
                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }
        }

        function GetNombreSolicitante() {
            muestraImagenLoading();
            //CU-47-CONT-SOLI
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaEmpleado.aspx?op=Sol', setReturnGetNombreSolicitante, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetNombreSolicitante(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 12) {
                    document.getElementById('<%= hfNombreSolicitante.ClientID %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                    document.getElementById('<%= hfRegionalContConv.ClientID %>').value = retLupa[7];
                    document.getElementById('<%= hfDependenciaSolicitante.ClientID %>').value = retLupa[8];
                    document.getElementById('<%= hfCargoSolicitante.ClientID %>').value = retLupa[9];
                    document.getElementById('<%= hfCodRegionalContConv.ClientID %>').value = retLupa[12];

                    prePostbck(false);
                    __doPostBack('<%= txtNombreSolicitante.ClientID %>', '');
                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }
        }

        function GetNombreOrdenadorGasto() {
            muestraImagenLoading();
            //CU-47-CONT-SOLI
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaEmpleado.aspx?op=OrdG', setReturnGetNombreOrdenadorGasto, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetNombreOrdenadorGasto(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 9) {
                    document.getElementById('<%= hfNombreOrdenadorGasto.ClientID %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                    document.getElementById('<%= hfTipoIdentOrdenadorGasto.ClientID %>').value = retLupa[0];
                    document.getElementById('<%= hfNumeroIdentOrdenadoGasto.ClientID %>').value = retLupa[1];
                    document.getElementById('<%= hfCargoOrdenadoGasto.ClientID %>').value = retLupa[9];

                    prePostbck(false);
                    __doPostBack('<%= txtNombreOrdenadorGasto.ClientID %>', '');
                } else {
                    ocultaImagenLoading();
                }
            } else {
                ocultaImagenLoading();
            }
        }

        function GetPlanCompras() {
            muestraImagenLoading();
            var vContratoSV = document.getElementById('<%= hfContratoSV.ClientID %>');   
            var idRegional = document.getElementById('<%= hfRegional.ClientID %>').value;
            var url = '../../../Page/Contratos/LupasV11/LupasPlanCompras.aspx?vContratoSV=' + vContratoSV.value + '&vIdRegContrato=' + idRegional;
            window_showModalDialog(url, setReturnGetPlanCompras, 'dialogWidth:800px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetPlanCompras(dialog) {
            prePostbck(false);
            __doPostBack('<%= txtPlanCompras.ClientID %>', '');
        }

        function GetValorApoICBF() {
            //CU-21-CONT-APORTESCONTRATO
            if (document.getElementById('<%= hfValorInicialContConv.ClientID %>').value != '' && document.getElementById('<%= txtValorInicialContConv.ClientID %>').value != '') {
                muestraImagenLoading();
                var vValorInicialContrato = document.getElementById('<%= hfTotalProductos.ClientID %>');

                //window_showModalDialog('../../../Page/Contratos/AporteContrato/Add.aspx?Aporte=ICBF', setReturnGetValorApoICBF, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                var url = '../../../Page/Contratos/AporteContrato/Add.aspx?Aporte=ICBF&vValorInicialContrato='+vValorInicialContrato.value + '&AporteIcbf='+ true;

                window_showModalDialog(url, setReturnGetValorApoICBF, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }
            else {
                if (document.getElementById('<%= txtValorInicialContConv.ClientID %>').value != '') {
                    alert('Para continuar debe guardar el avance del contrato registrado.')
                }
                else {
                    alert('Debe completar el campo Valor Inicial del Contrato/Convenio y guardar el contrato para poder continuar.')
                }
            }
        }
        function setReturnGetValorApoICBF() {
            prePostbck(false);
            __doPostBack('<%= txtValorApoICBF.ClientID %>', '');
        }

        function GetValorApoContrat() {
            //CU-21-CONT-APORTESCONTRATO
            if (document.getElementById('<%= txtValorInicialContConv.ClientID %>').value != '' &&
                document.getElementById('<%= hfValorInicialContConv.ClientID %>').value == document.getElementById('<%= txtValorInicialContConv.ClientID %>').value) {

                muestraImagenLoading();
                
                var vValorInicialContrato = document.getElementById('<%= hfTotalProductos.ClientID %>');
            
                //alert(vValorInicialContrato.value);     


                var url = '../../../Page/Contratos/AporteContrato/Add.aspx?Aporte=Contratista&AporteIcbf=' + false;

                window_showModalDialog(url, setReturnGetValorApoContrat, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }
            else {
                if (document.getElementById('<%= txtValorInicialContConv.ClientID %>').value != '') {
                    alert('Para continuar debe guardar el avance del contrato registrado.')
                }
                else {
                    alert('Debe completar el campo Valor Inicial del Contrato/Convenio y guardar el contrato para poder continuar.')
                }
            }
        }
        function setReturnGetValorApoContrat() {
            prePostbck(false);
            __doPostBack('<%= txtValorApoContrat.ClientID %>', '');
        }

        function GetLugarEjecucion() {
            muestraImagenLoading();
            //CU-028-CONT-RELAC-LUGEJEC
            window_showModalDialog('../../../Page/Contratos/LugarEjecucionContrato/LupaConsultarLugarContrato.aspx', setRetrunGetLugarEjecucion, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setRetrunGetLugarEjecucion(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                prePostbck(false);
                __doPostBack('<%= txtLugarEjecucion.ClientID %>', '');
            } else {
                ocultaImagenLoading();
            }
        }

        function GetContratista() {
            muestraImagenLoading();
            //CU-022-CONT-RELAC-CONTRATISTA
            window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx', setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContratista() {
            prePostbck(false);
            __doPostBack('<%= txtContratista.ClientID %>', '');
        }

        function GetSuperInterv() {
            if (document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value != '' &&
                document.getElementById('cphCont_caFechaFinalizacionInicial_txtFecha').value != '' &&
                document.getElementById('<%= hfFechaInicioEjecucion.ClientID %>').value == document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value &&
                document.getElementById('<%= hfFechaFinalizacion.ClientID %>').value == document.getElementById('cphCont_caFechaFinalizacionInicial_txtFecha').value) {
                muestraImagenLoading();
                //CU-019-CONT-ASIG-INTER/SUP
                window_showModalDialog('../../../Page/Contratos/SupervisorInterContrato/RelacionarSupervisorInterventor.aspx', setReturnGetSuperInterv, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }
            else {
                if (document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value == '') {
                    alert('Debe completar el campo Fecha de Inicio de ejecución del Contrato/Convenio y guardar el contrato para poder continuar.')
                } else {
                    if (document.getElementById('cphCont_caFechaFinalizacionInicial_txtFecha').value == '') {
                        alert('Debe completar el campo Fecha de Finalización Inicial del Contrato/Convenio y guardar el contrato para poder continuar.')
                    }
                    else {
                        alert('Para continuar debe guardar el avance del contrato registrado.')
                    }
                }
            }
        }
        function setReturnGetSuperInterv() {
            prePostbck(false);
            __doPostBack('<%= txtSuperInterv.ClientID %>', '');        }

       
        function GetCDP() {
            muestraImagenLoading();
            var idRegional = document.getElementById('<%= hfRegional.ClientID %>').value;
            var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
            var idVigencia = document.getElementById('<%= ddlVigenciaFiscalIni.ClientID %>').value;
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaRegInfoPresupuestalEnLinea.aspx?IdRegContrato=' + idRegional + '&idVigencia=' + idVigencia + '&idContrato=' + idContrato, setReturnGetCDP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetCDP() {
                __doPostBack('<%= txtCDP.ClientID %>', '');
        }

        function GetVigFuturas() {
            //CU-12
            if (document.getElementById('<%= txtValorInicialContConv.ClientID %>').value != '' &&
                document.getElementById('<%= hfValorInicialContConv.ClientID %>').value == document.getElementById('<%= txtValorInicialContConv.ClientID %>').value) {

                muestraImagenLoading();

                window_showModalDialog('../../../Page/Contratos/VigenciasFuturas/Add.aspx', setReturnGetVigFuturas, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            } else {
                alert('Debe completar el campo Valor Inicial del Contrato/Convenio y guardar el contrato para poder continuar.')
            }
        }
        function setReturnGetVigFuturas() {
            prePostbck(false);
            __doPostBack('<%= txtVigFuturas.ClientID %>', '');
        }


        function GetCodigosSECOP() {
            muestraImagenLoading();
            var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaCodigoSecop.aspx?idContrato='+idContrato.value, setReturnGetCodigosSECOP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');         
        }

        function setReturnGetCodigosSECOP() {
            prePostbck(false);
            __doPostBack('GetCodigosSECOP', '');
        }



    </script>
</asp:Content>
