<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ConsModContractual_Detail" %>
<%@ Register Src="~/General/General/Control/ArchivosContrato.ascx" TagPrefix="uc1" TagName="ArchivosContrato" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDCosModContractual" runat="server" />
    <asp:HiddenField ID="hfEstado" runat="server" />
    <asp:HiddenField ID="hfIdTContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número del Contrato/Convenio
            </td>
            <td>
                N&uacute;mero de Documento
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style2">
                <asp:TextBox runat="server" ID="txtNumeroContrato"  Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style2">
                <asp:TextBox runat="server" ID="txtNumeroDoc"  Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Modificaci&oacute;n
            </td>
            <td>
                Solicitud de Modificaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtTipoMod" Width="90%" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtSolicitudMod" Width="90%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Justificaci&oacute;n
            </td>
            <td class="auto-style2">
               </td>
        </tr>
        <tr class="rowA">
                <td class="auto-style1">
                    <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Height="73px" Width="90%" MaxLength="500" TabIndex="23" Enabled="false"></asp:TextBox>
                </td>
                <td class="auto-style1">
                    <div style="text-align:left">
                          <asp:Label ID="lblCambiarEstado" Font-Bold="true" runat="server" Text="Cambiar de Estado" />                   
                    </div>
                    <div style="text-align:center">

                        <asp:RadioButtonList ID="RbtnListTiposModifcacion" AutoPostBack="true" OnSelectedIndexChanged="RbtnListTiposModifcacion_SelectedIndexChanged"
                             runat="server">
                            <%--<asp:ListItem Value="Eliminar" Text="Eliminar" />--%>
                            <asp:ListItem Value="Rechazar" Text="Rechazar" />
                            <asp:ListItem Value="Devolver" Text="Devolver" />
                            <asp:ListItem Value="Aprobar" Text="Aprobar" />
                        </asp:RadioButtonList> <br />
                        <asp:ImageButton ID="btnAprobar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png" ToolTip="Aprobar" OnClick="btnAprobar_Click" />
                                           
                        </div>
                </td>
            </tr>
        <tr class="rowB">
            <td colspan="2">
            <asp:MultiView ID="MultiViewDevolucionRechazo" runat="server">
                <asp:View ID="ViewDevolucionRechazo" runat="server">
                <table  width="100%" align="center">
                     <tr class="rowB">
                        <td colspan="2">
                            Justificación devoluci&oacute;n o Rechazo 
                        </td>
                     </tr>
                     <tr class="rowA">
                        <td colspan="2">
                                <asp:TextBox runat="server" ID="txtJustificacionDevolucionRechazo" TextMode="MultiLine" Height="73px" Width="90%" MaxLength="500" TabIndex="23"></asp:TextBox>
                            </td>
                         <td>
                            <div style="text-align:left">
                            &nbsp;&nbsp&nbsp;&nbsp;
                            <asp:ImageButton ID="btnDevolver" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Return.png" ToolTip="Devolver" OnClick="btnDevolver_Click" />
                            &nbsp;&nbsp&nbsp;&nbsp;
                            <asp:ImageButton ID="btnRechazar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png" ToolTip="Rechazar" OnClick="btnRechazar_Click" />
                            <%--&nbsp;&nbsp&nbsp;&nbsp;
                            <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Delete.gif" ToolTip="Rechazar" OnClick="btnRechazar_Click" />--%>
                            </div>
                         </td>
                    </tr>     
                </table>
                </asp:View>
                <%--<asp:View ID="ViewAprobacion" runat="server">
                <table  width="100%" align="center">
                            <tr class="rowA">
                                <td colspan="4">

                                </td>
                                <td>
                                    <div style="text-align:right">
                                                  
                                        <asp:ImageButton ID="btnAprobar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png" ToolTip="Aprobar" OnClick="btnAprobar_Click" />
                                             &nbsp;&nbsp&nbsp;&nbsp;                 &nbsp;&nbsp&nbsp;&nbsp;          &nbsp;&nbsp&nbsp;&nbsp;   &nbsp;&nbsp&nbsp;&nbsp;
                                         </div>
                                </td>
                            </tr>     
                </table>
                </asp:View>--%>
                <asp:View ID="ViewDefault" runat="server">

                </asp:View>
            </asp:MultiView>
                </td>
        </tr>                
    </table>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                <Ajax:Accordion ID="AccTiposModificacion" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                        ContentCssClass="accordionContent" runat="server"
                        Width="100%" Height="100%">
                        <Panes>
                            <Ajax:AccordionPane ID="ApAdiccion" Visible="false" runat="server">
                                <Header>
                                    Adici&oacute;n
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel1">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvAdicionales" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdAdicion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvAdicionales_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor de la Adci&#243;n" DataField="ValorAdicion" />
<%--                                                            <asp:BoundField HeaderText="Justificaci&#243;n de la Adici&#243;n Superior al 50%" DataField="JustificacionAdicion" />--%>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApProrroga" Visible="false" runat="server">
                                <Header>
                                    Pr&oacute;rroga
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel2">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvProrrogas" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdProrroga" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvProrrogas_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Inicio Pr&#243;rroga" DataField="FechaInicio" DataFormatString="{0:dd-MM-yyyy}"/>
                                                            <asp:BoundField HeaderText="Fecha de Finalizaci&#243;n Pr&#243;rroga" DataField="FechaFin" DataFormatString="{0:dd-MM-yyyy}"/>
                                                            <asp:BoundField HeaderText="Pr&#243;rroga Dias" DataField="Dias" />
                                                            <asp:BoundField HeaderText="Pr&#243;rroga Meses" DataField="Meses" />
                                                            <asp:BoundField HeaderText="Pr&#243;rroga Años" DataField="Años" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApModificacionesContractuales" Visible="false" runat="server">
                                <Header>
                                    Modificaciones Contractuales
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel3">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvModificaciones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdModObligacion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvModificaciones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Clausula del Contrato" DataField="ClausulaContrato" />
                                                            <asp:BoundField HeaderText="Descripción de Modificaci&#243;n" DataField="DescripcionModificacion" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApReduccionesValor" Visible="false" runat="server">
                                <Header>
                                    Reducci&oacute;n en Valor
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel4">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvReducciones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdReduccion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvReducciones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Reducci&#243;n" DataField="FechaReduccion" />
                                                            <asp:BoundField HeaderText="Valor Total de Reducci&#243;n" DataField="ValorReduccion" />
                                                            <asp:BoundField HeaderText="Justificaci&#243;n de la Reducci&#243;n" DataField="Justificacion" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApCesion" Visible="false" runat="server">
                                <Header>
                                    Cesi&oacute;n
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel5">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvCesiones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdCesion" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvCesiones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Cesi&#243;n" DataField="FechaCesion" DataFormatString="{0:dd-MM-yyyy}" />
                                                                    <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor Cesi&#243;n" DataField="ValorCesion" />
                                                                    <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor Ejecutado" DataField="ValorEjecutado" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                            <Ajax:AccordionPane ID="ApSuspencion" Visible="false" runat="server">
                                <Header>
                                    Suspenci&oacute;n
                                </Header>
                                <Content>
                                    <asp:Panel runat="server" ID="Panel6">
                                        <table width="90%" align="center">
                                            <tr class="rowAG">
                                                <td>
                                                    <asp:GridView runat="server" ID="gvSuspensiones" AutoGenerateColumns="False" AllowPaging="True"
                                                        GridLines="None" Width="100%" DataKeyNames="IdSuspension" CellPadding="0" Height="16px" OnSelectedIndexChanged="gvSuspensiones_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                        Height="16px" Width="16px" ToolTip="Ver Detalle"/>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Ver Detalle
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Fecha de Inicio Suspensión" DataField="FechaInicio" />
                                                            <asp:BoundField HeaderText="Fecha Terminación Suspensión" DataField="FechaFin" />
                                                            <asp:BoundField HeaderText="Valor Descuento" DataField="ValorSuspencion" />
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="rowBG" />
                                                        <EmptyDataRowStyle CssClass="headerForm" />
                                                        <HeaderStyle CssClass="headerForm" />
                                                        <RowStyle CssClass="rowAG" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </Content>
                            </Ajax:AccordionPane>
                                                    <Ajax:AccordionPane ID="ApLiquidacionContrato" Visible="false" runat="server">
                            <Header>
                                Liquidac&oacute;n de contrato
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel7">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                <asp:GridView runat="server" ID="gvLiquidacionContrato" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvLiquidacionContrato_SelectedIndexChanged" DataKeyNames="IdLiquidacionContrato" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Radicaci&oacute;n" DataField="FechaRadicacionView" />
                                                                 <asp:BoundField HeaderText="Dependencia" DataField="DependenciaSueprvisor" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApImposicionMultas" Visible="false" runat="server">
                            <Header>
                                Imposici&oacute;n de multas
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel8">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                 <asp:GridView runat="server" ID="gvImposicionMultas" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvImpsicionMultas_SelectedIndexChanged" DataKeyNames="IdImposicionMulta" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Razón" DataField="Razones" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApTerminacionAnticipada" Visible="false" runat="server">
                            <Header>
                                Terminaci&oacute;n anticipada
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel9">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                  <asp:GridView runat="server" ID="gvTerminacionAnticipada" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvTerminacionAnticipada_SelectedIndexChanged" DataKeyNames="IdTerminacionAnticipada" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Radicación" DataField="FechaRadicacionView" />
                                                                <asp:BoundField HeaderText="No de Radicación" DataField="NumeroRadicacion" />
                                                                <asp:BoundField HeaderText="Fecha de Terminación Anticipada" DataField="FechaTerminacionAnticipadaView" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane> 
                                                    <Ajax:AccordionPane ID="ApCambioSupervisor" Visible="false" runat="server">
                            <Header>
                                Cambio de Supervisor de Contrato
                            </Header>
                            <Content>
                                <asp:Panel runat="server" ID="Panel10">
                                    <table width="90%" align="center">
                                     <tr class="rowAG">
                                         <td>
                                          Detalle:
                                         </td>
                                         <td>
                                        <asp:ImageButton ID="btnInfo" runat="server" OnClick="btnInfo_Click" CommandName="Select" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />      
                                         </td>
                                     </tr>
                                      <table width="90%" align="center">
                                            <tr class="rowBG">
            <td colspan="4">
            <table align="center" width="90%">
                <tr class="rowB">
                    <td class="Cell">Supervisores relacionados Actuales </td>
                </tr>
                  <tr class="rowA">
                        <td class="Cell">
                            <div id="Div1" runat="server">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                                        <asp:BoundField DataField="FechaInicioString" HeaderText="Fecha Inicio" />   
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                       <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
            </table>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="4">
                <asp:MultiView ID="MultiViewSupervisores" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewSupervisores" runat="server">
             <table align="center" width="90%">
                    <tr class="rowB">
                        <td class="Cell">Supervisores relacionados Modificaci&oacute;n</td>
                        <td class="Cell">
                            &nbsp;</td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell" colspan="2">
                            <div id="dvSupervInterv" runat="server">
                                <asp:GridView ID="gvSupervInterv" runat="server" Width="100%" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px">
                                    <Columns>
                                       <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                                        <asp:BoundField DataField="FechaInicioString" HeaderText="Fecha Inicio" />   
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                       <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                        </asp:View>
                    </asp:MultiView>
            </td>
        </tr> 
                                    </table>
                                </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>                                            
                        </Panes>
                </Ajax:Accordion>     
            </td>
        </tr>
    </table>
     <asp:Panel runat="server" ID="pnlLista">
<%--        <table width="90%" align="center">
            <tr class="rowA">
                <td colspan="2">
                </td>
            </tr>  
           <tr class="rowB">
                <td>
                    ¿Requiere Modificar la garantía?
                </td>
                <td>
                    Fecha de notificación a la aseguradora *
                    <asp:RequiredFieldValidator ID="rfvFechaNotificacion" runat="server" ControlToValidate="txtFechaNotificacion" ErrorMessage="Campo Requerido" SetFocusOnError="True" 
                    Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="cvFechaNotificacion" runat="server" ControlToValidate="txtFechaNotificacion" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
                </td>
            </tr>         
            <tr class="rowA">
                <td class="auto-style2">
                <asp:RadioButtonList runat="server" ID="rblModificarGarantia" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
                </td>
                <td class="auto-style2">
                    <asp:TextBox runat="server" ID="txtFechaNotificacion" AutoPostBack="True" Enabled="False"></asp:TextBox>
                    <asp:Image ID="imgCalendarioDesde" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
                    <Ajax:CalendarExtender ID="cetxtFechaNotificacion" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioDesde" TargetControlID="txtFechaNotificacion"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaNotificacion">
                    </Ajax:MaskedEditExtender>
                </td>
            </tr>
           <tr class="rowB">
                <td class="auto-style1">
                    Observaciones de la Notificación
                </td>
                <td class="auto-style1">
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2" class="auto-style1">
                    <asp:TextBox runat="server" ID="txtObservacionNotificacion" TextMode="MultiLine" Height="73px" Width="90%" MaxLength="500" TabIndex="23" Enabled="false"></asp:TextBox>
                </td>
                <td class="auto-style1">
                </td>
            </tr>
        </table>--%>
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvOtrosDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDDetalleConsModContractual" CellPadding="0" Height="16px">
                        <Columns>
                            <asp:BoundField HeaderText="Nombre del Documento" DataField="ValorAdicion" />
                            <asp:BoundField HeaderText="NombreArchivo" DataField="ValorAdicion" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="Create" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Adicionar" OnClick="btnDeleteOtroDocumento_Click" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Opciones
                                </HeaderTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Información Documentos
            </td>
        </tr>           
    </table>
    <uc1:ArchivosContrato runat="server" ID="ArchivosContrato" ShowCreate="false" />
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 38px;
        }
        .auto-style2 {
            height: 26px;
        }
        .auto-style3 {
            width: 268436000px;
        }
    </style>
</asp:Content>




