using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_ConsModContractual_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ConsModContractual";
    ContratoService vTipoSolicitudService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.MostrarBotonNuevo(false);
            toolBar.EstablecerTitulos("Gesti&oacute;n de Modificaciones Contractuales", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            ArchivosContrato.LoadInfoContratos(vIdContrato);
            MultiViewDevolucionRechazo.ActiveViewIndex = -1;

            var guardo = GetSessionParameter("ConsModContractualGestion.Guardo").ToString();
                
            if (!string.IsNullOrEmpty(guardo))
            {
                if(guardo == "1")                    
                toolBar.MostrarMensajeGuardado("La Solicitud ha sido Aprobada correctamente.");
                else if(guardo == "2")
                toolBar.MostrarMensajeGuardado("La Solicitud ha sido Devuelta correctamente.");
                else if (guardo == "3")
                toolBar.MostrarMensajeGuardado("La Solicitud ha sido Rechazada correctamente.");

                RemoveSessionParameter("ConsModContractualGestion.Guardo");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        if (hfEstado.Value == "Enviada")
        {
            vTipoSolicitudService.ModificacionContractualContrato(Convert.ToInt32(hfIDCosModContractual.Value),GetSessionUser().NombreUsuario);
            SetSessionParameter("Contrato.IdContrato",hfIdTContrato.Value);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value);
            SetSessionParameter("ConsModContractualGestion.Guardo", "1");
            NavigateTo(SolutionPage.Detail);
        }
        else
        {
            toolBar.MostrarMensajeError("No se puede Aprobar la Solicitud cuando su estado es diferente de Enviada");
        }
    }

    protected void btnDevolver_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtJustificacionDevolucionRechazo.Text))
        {
            int idConsModContractual = int.Parse(hfIDCosModContractual.Value);
            string usuario = GetSessionUser().NombreUsuario;
            int result = vTipoSolicitudService.ModificacionContractualContratoRechazoDevolucion(idConsModContractual,
                                                                                   false,
                                                                                   usuario,
                                                                                   txtJustificacionDevolucionRechazo.Text);


            if (result > 0)
            {
                SetSessionParameter("Contrato.IdContrato",hfIdTContrato.Value);
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value);
                SetSessionParameter("ConsModContractualGestion.Guardo", "2");
                NavigateTo(SolutionPage.Detail);
            }
        }
        else
            toolBar.MostrarMensajeError("debe Ingresar la Justificación de la devolución o Rechazo");

        //if (hfEstado.Value == "Enviada")
        //{
        //SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value);
        //SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
        //SetSessionParameter("ConsModContractualGestion.IdEstado", (int)ConsModContractualesEstado.DEVUELTA);
        //NavigateTo(SolutionPage.Edit);
        //}
        //else
        //{
        //    toolBar.MostrarMensajeError("No se puede Devolver la Solicitud cuando su estado es diferente de Enviada");
        //}
    }

    protected void btnRechazar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtJustificacionDevolucionRechazo.Text))
        {
            int idConsModContractual = int.Parse(hfIDCosModContractual.Value);
            string usuario = GetSessionUser().NombreUsuario;
            int result = vTipoSolicitudService.ModificacionContractualContratoRechazoDevolucion(idConsModContractual,
                                                                                   true,
                                                                                   usuario,
                                                                                   txtJustificacionDevolucionRechazo.Text);


            if (result > 0)
            {
                SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value);
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value);
                SetSessionParameter("ConsModContractualGestion.Guardo", "3");
                NavigateTo(SolutionPage.Detail);
            }
        }
        else
            toolBar.MostrarMensajeError("debe Ingresar la Justificación de la devolución o Rechazo");

        //if (hfEstado.Value == "Enviada")
        //{
        //    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value);
        //    SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
        //    SetSessionParameter("ConsModContractualGestion.IdEstado", (int)ConsModContractualesEstado.RECHAZADA);
        //    NavigateTo(SolutionPage.Edit);
        //}
        //else
        //{
        //    toolBar.MostrarMensajeError("No se puede Rechazar la Solicitud cuando su estado es diferente de Enviada");
        //}
    }

    private void CargarDatos()
    {
        try
        {
            int vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");

            if (GetSessionParameter("ConsModContractualGestion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ConsModContractualGestion.Guardado");

            hfIDCosModContractual.Value = vIdConsModContractual.ToString();

            SolModContractual vSolContractual = new SolModContractual();
            vSolContractual = vTipoSolicitudService.ConsultarSolitud(vIdConsModContractual);

            txtJustificacion.Text = vSolContractual.Justificacion;

            hfIdTContrato.Value = vSolContractual.IdContrato.ToString();
            hfEstado.Value = vSolContractual.Estado;

            if (vSolContractual.Estado == "Enviada")
            {
                btnAprobar.Visible = true;
                btnDevolver.Visible = true;
                btnRechazar.Visible = true;
            }
            else
            {
                //toolBar.MostrarBotonEditar(false);
                btnAprobar.Visible = false;
                btnDevolver.Visible = false;
                btnRechazar.Visible = false;
                RbtnListTiposModifcacion.Visible = false;
                lblCambiarEstado.Visible = false;

                if (vSolContractual.Estado == "Aceptada" && vSolContractual.TipoModificacion == "Cambio de Supervisor")
                    MultiViewSupervisores.ActiveViewIndex = -1;
            }
            
            var detalleSolicitud = vTipoSolicitudService.ConsultarSolicitudesDetalle(vIdConsModContractual);

            txtNumeroDoc.Text = vSolContractual.NumeroDoc;
            txtNumeroContrato.Text = vSolContractual.NumeroContrato ?? String.Empty;
            txtSolicitudMod.Text = vSolContractual.IDCosModContractual.ToString();
            txtTipoMod.Text = vSolContractual.TipoModificacion;


            TipoModificacion vTipoModificacionBasica = new TipoModificacion();
            vTipoModificacionBasica = vTipoSolicitudService.consultartipomodificacion(vSolContractual.IDTipoModificacionContractual);

            //rblModificarGarantia.SelectedValue = vTipoModificacionBasica.RequiereModificacion ? "SI" : "NO";

            foreach (var itemDetalle in detalleSolicitud)
            {
                switch (itemDetalle.TipoModificacion)
                {
                    case "Adición":
                        AccTiposModificacion.Panes[0].Visible = true;
                        gvAdicionales.DataSource = vTipoSolicitudService.ConsultarAdicioness(null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvAdicionales.DataBind();
                        break;
                    case "Prorroga":
                        AccTiposModificacion.Panes[1].Visible = true;
                        gvProrrogas.DataSource = vTipoSolicitudService.ConsultarProrrogass(null, null, null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvProrrogas.DataBind();
                        break;
                    case "Modificación":
                        AccTiposModificacion.Panes[2].Visible = true;
                        gvModificaciones.DataSource = vTipoSolicitudService.ConsultarModificacionObligacionXIDDetalleConsModContractual(itemDetalle.IDDetalleConsModContractual);
                        gvModificaciones.DataBind();
                        break;
                    case "Suspensión":
                        AccTiposModificacion.Panes[5].Visible = true;
                        gvSuspensiones.DataSource = vTipoSolicitudService.ConsultarSuspensionesPorDetalleModificacion(itemDetalle.IDDetalleConsModContractual);
                        gvSuspensiones.DataBind();
                        break;
                    case "Reducción de Valor":
                        AccTiposModificacion.Panes[3].Visible = true;
                        gvReducciones.DataSource = vTipoSolicitudService.ConsultarReduccioness(null, null, itemDetalle.IDDetalleConsModContractual);
                        gvReducciones.DataBind();
                        break;
                    case "Cesión":
                        AccTiposModificacion.Panes[4].Visible = true;
                        gvCesiones.DataSource = vTipoSolicitudService.ConsultarCesioness(null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvCesiones.DataBind();
                        break;
                    case "Liquidación de Contrato":
                        AccTiposModificacion.Panes[6].Visible = true;
                        gvLiquidacionContrato.DataSource = vTipoSolicitudService.ConsultarLiquidacionIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvLiquidacionContrato.EmptyDataText = EmptyDataText();
                        gvLiquidacionContrato.PageSize = PageSize();
                        gvLiquidacionContrato.DataBind();
                        break;
                    case "Imposición de Multas":
                        AccTiposModificacion.Panes[7].Visible = true;
                        gvImposicionMultas.DataSource = vTipoSolicitudService.ConsultarImposicionMultaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvImposicionMultas.EmptyDataText = EmptyDataText();
                        gvImposicionMultas.PageSize = PageSize();
                        gvImposicionMultas.DataBind();
                        break;
                    case "Terminación Anticipada":
                        AccTiposModificacion.Panes[8].Visible = true;
                        gvTerminacionAnticipada.DataSource = vTipoSolicitudService.ConsultarTerminacionAnticipadaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvTerminacionAnticipada.EmptyDataText = EmptyDataText();
                        gvTerminacionAnticipada.PageSize = PageSize();
                        gvTerminacionAnticipada.DataBind();
                        break;
                    case "Cambio de Supervisor":
                        AccTiposModificacion.Panes[9].Visible = true;
                        List<SupervisorInterContrato> supervisoresInterventores = vTipoSolicitudService.ObtenerSupervisoresInterventoresContrato(vSolContractual.IdContrato, null);
                        if (supervisoresInterventores != null)
                        {
                            var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                            gvSupervisoresActuales.DataSource = querySupervisores;
                            gvSupervisoresActuales.DataBind();
                        }
                        List<SupervisorInterContrato> supervisoresTemporales = vTipoSolicitudService.ObtenerSupervisoresTemporalesContrato(vSolContractual.IdContrato, null);
                            gvSupervInterv.DataSource = supervisoresTemporales;
                            gvSupervInterv.DataBind();

                        break;
                }
            }

            ObtenerAuditoria(PageName, hfIDCosModContractual.Value);
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vConsModContractual.UsuarioCrea, vConsModContractual.FechaCrea, vConsModContractual.UsuarioModifica, vConsModContractual.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnCreateDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void btnSelectDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void btnDeleteDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void btnDeleteOtroDocumento_Click(object sender, ImageClickEventArgs e)
    {
    }

    private void EliminarRegistro()
    {
        try
        {
            int vIDCosModContractual = Convert.ToInt32(hfIDCosModContractual.Value);

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vTipoSolicitudService.ConsultarConsModContractual(vIDCosModContractual);
            InformacionAudioria(vConsModContractual, this.PageName, vSolutionPage);
            int vResultado = vTipoSolicitudService.EliminarConsModContractual(vConsModContractual);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ConsModContractualGestion.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAdicionales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvAdicionales.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvAdicionales.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Adiciones.IdAdiccion", strValue);
            NavigateTo("~/Page/Contratos/Adiciones/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvProrrogas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvProrrogas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvProrrogas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            NavigateTo("~/Page/Contratos/Prorrogas/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModificaciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvModificaciones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvModificaciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("ModificacionObligacion.IdModObligacion", strValue);
            NavigateTo("~/Page/Contratos/ModificacionObligacion/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvReducciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReducciones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReducciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Reducciones.IdReduccion", strValue);
            NavigateTo("~/Page/Contratos/ReduccionesValor/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCesiones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvCesiones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvCesiones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Cesiones.IdCesion", strValue);
            NavigateTo("~/Page/Contratos/Cesiones/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSuspensiones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvSuspensiones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvSuspensiones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            SetSessionParameter("Suspensiones.IdSuspension", strValue);
            NavigateTo("~/Page/Contratos/Suspenciones/Detail.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void RbtnListTiposModifcacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        if (RbtnListTiposModifcacion.SelectedValue == "Rechazar")
        {
            MultiViewDevolucionRechazo.ActiveViewIndex = 0;
            btnDevolver.Visible = false;
            btnRechazar.Visible = true;
            btnAprobar.Visible = false;
            txtJustificacionDevolucionRechazo.Text = string.Empty;
        }
        else if (RbtnListTiposModifcacion.SelectedValue == "Devolver")
        {
            MultiViewDevolucionRechazo.ActiveViewIndex = 0;
            btnDevolver.Visible = true;
            btnRechazar.Visible = false;
            btnAprobar.Visible = false;
            txtJustificacionDevolucionRechazo.Text = string.Empty;
        }
        else if (RbtnListTiposModifcacion.SelectedValue == "Aprobar")
        {
            MultiViewDevolucionRechazo.ActiveViewIndex = -1;
            btnAprobar.Visible = true;
        }
    }

    protected void gvLiquidacionContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvLiquidacionContrato.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvLiquidacionContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdLiquidacionContrato", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            Response.Redirect("../LiquidacionContrato/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvImpsicionMultas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvImposicionMultas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvImposicionMultas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdImposicionMultas", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            Response.Redirect("../ProcesoImposicionMulta/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTerminacionAnticipada_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvTerminacionAnticipada.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvTerminacionAnticipada.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdTerminacionAnticipada", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            Response.Redirect("../TerminacionAnticipada/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            Response.Redirect("../CambioSupervision/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
