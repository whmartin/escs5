<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_ConsultaDependencia_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">


    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function GetDependencias() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaDependencias.aspx', setReturnGetDependencias, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetDependencias(dialog) {

            var pObj = window_returnModalDialog(dialog);

            if (pObj != undefined && pObj != '' && pObj != null) {

                var retLupa = pObj.split(",");
                var idDependencia = retLupa[0];
                var valorDependencia = retLupa[1];
                var payload = idDependencia + '|||' + valorDependencia;

                __doPostBack('ActualizarDependencias',payload);
            }
            else {

            }
        }

        function ValidaExistencia(source, args) {

            var ids = document.getElementById('<%= hfIdDependencia.ClientID %>').value;

            if (ids != '' && ids != null) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }

    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
         <tr class="rowB">
            <td class="Cell">
                Vigencia Fiscal 
               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVigenciaFiscalinicial" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="True" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Regional del Contrato
                <asp:RequiredFieldValidator ID="rfvTipoBusqueda0" runat="server" ControlToValidate="ddlIDRegional" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="True" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2">
                Dependencia 
                <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif" OnClientClick="GetDependencias(); return false;" Style="cursor: hand" ToolTip="Buscar" />
                <asp:CustomValidator ID="cuvReqCDP" runat="server" ErrorMessage="Campo Requerido"
                Enabled="true" ForeColor="Red" ValidationGroup="btnBuscar" ClientValidationFunction="ValidaExistencia"></asp:CustomValidator>
            </td>
        </tr>
         <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:GridView ID="gvDependencias" runat="server" AllowPaging="True" PageSize="4" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="Key,Value" GridLines="None" Height="16px" OnPageIndexChanging="gvDependencias_PageIndexChanging"  Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Key" HeaderText="Id Dependencía" />
                        <asp:BoundField DataField="Value" HeaderText="Dependencía" />
                        <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click"
                                 CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                            </asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
             </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Fecha Suscripción del Contrato
                <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtFechaRegistroSistema$txtFecha" Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha Suscripción al Sistema Válida" ForeColor="red"  ValidationGroup="btnBuscar"></asp:CompareValidator>
            </td>
            <td class="Cell">
                Número del Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistema" runat="server" Width="80%" Enabled="True" Requerid="False" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="15" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato" FilterType="Numbers" ValidChars="" />
            </td>
        </tr>

        <tr class="rowB">
            <td class="Cell">
                Categoría del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,FechaInicioEjecucion" CellPadding="0" Height="16px"
                        OnSorting="gvContratos_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContratos_PageIndexChanging">
                        <Columns>
                            <asp:BoundField HeaderText="Id Contrato" DataField="IdContrato"  SortExpression="IdContrato"/>
                            <asp:BoundField HeaderText="No Contrato" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal Inicial" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Contratista" DataField="NombreContratista"  SortExpression="NombreContratista"/>
                            <asp:BoundField HeaderText="Documento" DataField="NumeroDocumentoIdentificacion"  SortExpression="NumeroDocumentoIdentificacion" />
                            <asp:BoundField HeaderText="Tipo Documento" DataField="TipoDocumentoIdentificacion"  SortExpression="TipoDocumentoIdentificacion" />
                            <asp:BoundField HeaderText="Tipo de Contrato/Convenio" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                            <asp:BoundField HeaderText="Fecha de Suscripción" DataField="FechasSuscripcionContratoView"  SortExpression="FechasSuscripcionContratoView" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

                    <asp:HiddenField ID="hfIdDependencia" runat="server" />
                        <asp:HiddenField ID="hfDependencia" runat="server" />
</asp:Content>
