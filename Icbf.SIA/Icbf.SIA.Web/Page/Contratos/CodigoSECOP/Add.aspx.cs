﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición para la entidad NumeroProcesos
/// </summary>
public partial class Page_Contratos_CodigoSECOP_Add : GeneralWeb
{
    private masterPrincipal toolBar;
    private ContratoService vContratoService = new ContratoService();
    private string PageName = "Contratos/CodigoSECOP";
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad NumeroProcesos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            CodigosSECOP vCodigosSECOP = new CodigosSECOP();
            

            vCodigosSECOP.CodigoSECOP = txtCodigo.Text.Trim();
            vCodigosSECOP.Estado = Convert.ToBoolean(rblInactivo.SelectedValue);
            vCodigosSECOP.Descripcion = txtDescripcion.Text.Trim();


            if (Request.QueryString["oP"] == "E")
            {

                vCodigosSECOP.IdCodigoSECOP = Convert.ToInt32(hfIdCodigoSECOP.Value);
                vCodigosSECOP.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCodigosSECOP, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarCodigoSECOP(vCodigosSECOP);
            }
            else
            {
                vCodigosSECOP.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCodigosSECOP, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarCodigoSECOP(vCodigosSECOP);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("CodigoSECOP.IdCodigoSECOP", vCodigosSECOP.IdCodigoSECOP);
                SetSessionParameter("CodigoSECOP.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError(
                    "La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Códigos SECOP", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            
            int vIdCodigoSECOP = 0;

            if (int.TryParse(GetSessionParameter("CodigoSECOP.IdCodigoSECOP").ToString(), out vIdCodigoSECOP))
            {
                RemoveSessionParameter("CodigoSECOP.IdCodigoSECOP");
               
                CodigosSECOP vCodigoSSECOP = new CodigosSECOP();
                vCodigoSSECOP = vContratoService.ConsultarCodigoSECOP(vIdCodigoSECOP);
                hfIdCodigoSECOP.Value = vCodigoSSECOP.IdCodigoSECOP.ToString();
                txtCodigo.Text = vCodigoSSECOP.CodigoSECOP;
                txtDescripcion.Text = vCodigoSSECOP.Descripcion;
                rblInactivo.SelectedValue = vCodigoSSECOP.Estado.ToString();

            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.SelectedValue = "true";


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
