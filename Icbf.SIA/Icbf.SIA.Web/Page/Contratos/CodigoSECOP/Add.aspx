﻿

<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_CodigoSECOP_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdCodigoSECOP" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="auto-style1">
                Código SECOP *
                <asp:RequiredFieldValidator runat="server" ID="rvlCodigo" ControlToValidate="txtCodigo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
             <td>
                Descripción               
            </td>
            
        </tr>
        <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="txtCodigo" TextMode="MultiLine" Width="400px" MaxLength2="200"
                    onKeyDown="limitText(this,200);" Rows="2" Style="resize: none" onKeyUp="limitText(this,200);"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" Width="400px" Height="42px" MaxLength="250" 
                    onKeyDown="limitText(this,250);" Rows="3" Style="resize: none" onKeyUp="limitText(this,250);"></asp:TextBox>
                
            </td>           
        </tr>  
          <tr class="rowB">
             <td class="auto-style1">
                
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvInactivo" ControlToValidate="rblInactivo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>            
                
            </td>
        </tr>
        <tr class="rowA">
            
            <td class="auto-style1">
                <asp:RadioButtonList runat="server" ID="rblInactivo" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>      
        
    </table>

     <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }        
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
    .auto-style1 {
        width: 500px;
    }
</style>
</asp:Content>



