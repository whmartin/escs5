using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de visualización detallada para la entidad ConsecutivoContratoRegionales
/// </summary>
public partial class Page_ArchivosContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ArchivosContrato";
    string TIPO_ESTRUCTURA = "Todas";
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();

    public bool vEsAnioActual
    {
        set { ViewState["EsAnoActual"] = value; }
        get
        {
            if (ViewState["EsAnoActual"] != null)
                return (bool)ViewState["EsAnoActual"];
            else
                return false;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ArchivosContrato.IdContrato", hfIdContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    
    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        if (GetSessionParameter("ArchivosContrato.Guardo").ToString() == "1")
        {
            toolBar.MostrarMensajeGuardado("Se actualizarón los documentos del contrato correctamente");
            RemoveSessionParameter("ArchivosContrato.Guardo");
        }

        int vIdCntrato = Convert.ToInt32(GetSessionParameter("ArchivosContrato.IdContrato"));
        RemoveSessionParameter("ArchivosContrato.IdContrato");
        hfIdContrato.Value = vIdCntrato.ToString();

        var itemContrato = vContratoService.ConsultarContratosSimple(null, null, vIdCntrato, null, null, null, null,
                                                                 null, null, null, null, null, null);
        txtContrato.Text = itemContrato[0].NumeroContrato;
        txtRegional.Text = itemContrato[0].NombreRegional;

        txtobjeto.Text = itemContrato[0].ObjetoContrato;
        txtalcance.Text = itemContrato[0].AlcanceObjetoContrato;
        txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorInicialContrato);
        txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorFinalContrato);

        DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato[0].FechaInicioEjecucion);
        DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato[0].FechaFinalTerminacionContrato);

        txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
        txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

        gvanexos.EmptyDataText = EmptyDataText();
        gvanexos.PageSize = PageSize();
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdCntrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();

    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnEditar_Click);
            toolBar.EstablecerTitulos("Documentos Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //CargarIdRegional();
            //CargarIdVigencia();
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
}
