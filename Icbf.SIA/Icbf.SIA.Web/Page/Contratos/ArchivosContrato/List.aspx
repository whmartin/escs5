<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ArchivosContrato_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Regional *
                                <asp:RequiredFieldValidator runat="server" ID="rfvEntidadFinanciera" ControlToValidate="ddlIdRegional"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Vigencia *
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlIdVigencia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red" InitialValue="-1" Enabled="True"></asp:RequiredFieldValidator>

            </td>

        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdVigencia"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Id Contrato/Convenio</td>
            <td class="Cell">
                N&uacute;mero del Contrato/Convenio</td>
        </tr>
                <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/������������ .,@_():;-" />
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <%--<asp:GridView runat="server" ID="gvConsecutivoContratoRegionales" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDConsecutivoContratoRegional" CellPadding="0" Height="16px"
                        OnSorting="gvConsecutivoContratoRegionales_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvConsecutivoContratoRegionales_PageIndexChanging" OnSelectedIndexChanged="gvConsecutivoContratoRegionales_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Regional" DataField="Regional"  SortExpression="Regional"/>
                            <asp:BoundField HeaderText="Vigencia" DataField="Vigencia"  SortExpression="Vigencia"/>
                            <asp:BoundField HeaderText="Consecutivo" DataField="Consecutivo"  SortExpression="Consecutivo"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>--%>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px" AllowSorting="True" OnSelectedIndexChanged="gvContratos_SelectedIndexChanged" OnSorting="gvContratos_Sorting"   OnPageIndexChanging="gvContratos_PageIndexChanging"  >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro al Sistema" DataField="FechaCrea"  SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Id Contrato   " DataField="IdContrato"  SortExpression="IdContrato"/>
                            <asp:BoundField HeaderText="N&uacute;mero del Contrato" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional del contrato" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Modalidad de Selecci&oacute;n" DataField="NombreModalidadSeleccion"  SortExpression="NombreModalidadSeleccion"/>
                            <asp:BoundField HeaderText="Categoria del Contrato" DataField="NombreCategoriaContrato"  SortExpression="NombreCategoriaContrato"/>
                            <asp:BoundField HeaderText="Tipo de Contrato" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                            <asp:BoundField HeaderText="Estado de Contrato" DataField="NombreEstadoContrato"  SortExpression="NombreEstadoContrato"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
