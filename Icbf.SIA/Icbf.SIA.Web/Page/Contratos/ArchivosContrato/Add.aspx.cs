using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad ConsecutivoContratoRegionales
/// </summary>
public partial class Page_ArchivosContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();
    string PageName = "Contratos/ArchivosContrato";
    string PageNameEliminar = "Contratos/ArchivosContratoEliminar";
    string TIPO_ESTRUCTURA = "Todas";
    string TIPO_ESTRUCTURA_MINUTA = "Minuta";
    string TIPO_ESTRUCTURA_OTROS = "DocumentoContrato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    ///// <summary>
    //    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    //    /// </summary>            
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.EstablecerTitulos("Documentos Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        SetSessionParameter("ArchivosContrato.IdContrato",hfIdContrato.Value);
        NavigateTo(SolutionPage.Detail);
    }

    /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdCntrato = Convert.ToInt32(GetSessionParameter("ArchivosContrato.IdContrato"));
            RemoveSessionParameter("ArchivosContrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();

            var itemContrato = vContratoService.ConsultarContratosSimple(null, null, vIdCntrato, null, null, null, null,
                                                                     null, null,  null, null, null, null);

            txtContrato.Text = itemContrato[0].NumeroContrato;
            txtRegional.Text = itemContrato[0].NombreRegional;

            var vIdVigencia = Convert.ToInt32(itemContrato[0].IdVigenciaInicial);
            var vIdRegional = Convert.ToInt32(itemContrato[0].IdRegionalContrato);

            Vigencia vVigencia = vSIAService.ConsultarVigencia(vIdVigencia);
            Regional vRegional = vSIAService.ConsultarRegional(vIdRegional);

            hfIdRegional.Value = vRegional.CodigoRegional.Trim(); 
            hfIdVigencia.Value = vVigencia.AcnoVigencia.ToString().Trim();
            hfConsecutivoActual.Value = itemContrato[0].NumeroContrato;

            txtobjeto.Text = itemContrato[0].ObjetoContrato;
            txtalcance.Text = itemContrato[0].AlcanceObjetoContrato;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato[0].ValorFinalContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato[0].FechaInicioEjecucion);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato[0].FechaFinalTerminacionContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdCntrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            toolBar.LipiarMensajeError();
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        toolBar.LipiarMensajeError();
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            string strTipoDocumento = gvanexos.DataKeys[rowIndex].Values[1] != null ? gvanexos.DataKeys[rowIndex].Values[1].ToString(): string.Empty;
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                if (strTipoDocumento == TIPO_ESTRUCTURA_MINUTA || strTipoDocumento == TIPO_ESTRUCTURA_OTROS || strTipoDocumento == string.Empty)
                {
                    int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                    var creador = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA).First(e1 => e1.IdArchivo == indice).UsuarioCrea;

                    if (GetSessionUser().NombreUsuario == creador || ValidateAccessDelete(toolBar, PageNameEliminar, SolutionPage.Add))
                    {
                        vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                        gvanexos.DataBind();
                    }
                    else
                        toolBar.MostrarMensajeError("No tiene los permisos suficientes para eliminar el documento.");
                }
                else
                    toolBar.MostrarMensajeError("No puede eliminar este tipo de archivo, se integro mediante un proceso de modificación.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        if (ddlTipoDocumento.SelectedValue != "-1")
        {
            int idContrato = Convert.ToInt32(hfIdContrato.Value);

            FileUpload fuArchivo = FileUploadArchivoContrato;

            if (fuArchivo.HasFile)
            {
                try
                {
                    bool isValid = true;

                    if (ddlTipoDocumento.SelectedValue.ToString() == TIPO_ESTRUCTURA_MINUTA)
                    {
                        var existeMinuta = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA_MINUTA);

                        if (existeMinuta != null && existeMinuta.Count > 0)
                            isValid = false;
                    }

                    if (isValid)
                    {

                        ManejoControlesContratos controles = new ManejoControlesContratos();
                        controles.CargarArchivoFTPContratos
                            (
                             ddlTipoDocumento.SelectedValue.ToString(),
                             fuArchivo,
                             idContrato,
                             GetSessionUser().IdUsuario
                            );

                        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                        gvanexos.DataBind();
                    }
                    else
                        toolBar.MostrarMensajeError("Ya existe un tipo de documento Minuta, Si desea adjuntarlo Por favor elimine el documento actual.");
                }
                catch (Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
            }
            else
                toolBar.MostrarMensajeError("Debe Seleccionar un Documento a Cargar");
        }
        else
            toolBar.MostrarMensajeError("Debe Seleccionar un Tipo de Documento");
    }

    #endregion

}
