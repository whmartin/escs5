<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ArchivosContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <%--<asp:HiddenField ID="hfIDConsecutivoContratoRegional" runat="server" />--%>
                <asp:HiddenField ID="hfIdContrato" runat="server" />
    <table width="90%" align="center">
<%--        <tr class="rowB">
            <td>
                Regional
            </td>
            <td>
                Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdVigencia"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Consecutivo
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtConsecutivo"  Enabled="false"></asp:TextBox>
            </td>
        </tr>--%>
             <tr class="rowB">
            <td class="style1" style="width: 50%">
                N&uacute;mero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminaci&oacute;n de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
             <td colspan="2">

                  <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
<%--                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />   --%> 
                                                    <asp:BoundField HeaderText="Tipo del Documento" DataField="NombreTabla" />                                                                                                        
                                                                                                    
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>

             </td>

         </tr>
    </table>
</asp:Content>
