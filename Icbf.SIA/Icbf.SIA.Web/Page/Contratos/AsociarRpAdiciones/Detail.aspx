<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_AsociarRpAdiciones_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

            <script type="text/javascript">


                function AprobacionRP() {

                    return confirm("Esta segúro que desea aprobar el RP?");
                }

    </script>

    <asp:HiddenField runat="server" ID="hfEsSubscripcion" />
    <asp:HiddenField runat="server" ID="hfEstado" />


   <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td style="width: 50%">
                Regional
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px"></asp:TextBox>
               
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminaci&oacute;n de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjetoContrato" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcanceContrato"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorini"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor de la adición *
            </td>
            <td>
                 <asp:Label ID="lblJustificacion" runat="server" Text="Justificación adición superior al 50%" />
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" DataFormatString="{0:c}" Enabled="false" ID="txtValorAdicion"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicion" Width="320px" Height="65px" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftJustificacionAdicion" runat="server" TargetControlID="txtJustificacionAdicion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
            <asp:Label ID="lblRP" Text="Registro Presupuestal"  runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblCDPAdicion" Text="CDP"  runat="server" />
               &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtRP" runat="server" Enabled="false" ViewStateMode="Enabled"
                     ></asp:TextBox>
                </td>
            <td>
            <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" 
                ></asp:TextBox>
            </td>
        </tr>
               <tr class="rowB">
            <td>
            <asp:Label ID="Label1" Text="Valor Actual Registro Presupuestal"  runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label2" Text="Valor Actual CDP"  runat="server" />
               &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtValorRP" runat="server" Enabled="false"  ViewStateMode="Enabled"
                     ></asp:TextBox>
                </td>
            <td>
            <asp:TextBox ID="txtValorCDP" runat="server" Enabled="false"  ViewStateMode="Enabled" 
                AutoPostBack="true"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Supervisores&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </td>
        </tr>
    </table>
    <style type="text/css">
        table .grillaCentral tr.rowAG td, table .grillaCentral tr.rowBG td
        {
            text-align: center;
            padding: 5px;
            padding-right: 0px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function GetDetallePlanCompras() {

            var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
            var idAdiccion = document.getElementById('<%= hfIdAdicion.ClientID %>');

            var url = '../../../Page/Contratos/LupaAdiciones/LupaDetallePlanCompras.aspx?idContrato=' + idContrato.value + '&IdEsAdicion=' + idAdiccion.value;

            window_showModalDialog(url, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');

            return false;
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function ValidaPlanCompras(source, args) {
            var hfConsecutivosPlanCompras = document.getElementById('<%= hfConsecutivosPlanCompras.ClientID %>');
            if (parseInt(hfConsecutivosPlanCompras.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaProductos(source, args) {
<%--            var hfProductos = document.getElementById('<%= hfProductos.ClientID %>');
            if (parseInt(hfProductos.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }--%>
        }

        function mensajeAlerta(control, texto) {
            alert(texto);
            var creference = document.getElementById(control);
            creference.focus();
        }

        function ValidaEliminacion() {
            return confirm('Esta seguro de que desea eliminar el registro?');
        }

        function PreGuardado() {
            ConfiguraValidadores(false);
            muestraImagenLoading();
        }

        function Aprobacion() {
            ConfiguraValidadores(true);
            window.Page_ClientValidate("btnAprobar");
            if (!window.Page_IsValid) {
                alert('Debe diligenciar los campos obligatorios');
                return false;
            } else {
                return confirm('Está seguro que desea Finalizar el registro del Contrato/Convenio?');
            }
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostbck.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }

        function EjecutarJSFechaInicialSupervisores(control, fechaOriginal) {
            $("#lblError")[0].innerHTML = '';
            $("#lblError")[0].className = '';
            var fechaInicioEjecucion = document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value;
            var fechaInicioSupervisor = document.getElementById(control.id).value;

            if (fechaInicioEjecucion != '') {
                var splitfechainicio = fechaInicioEjecucion.split('/');
                var jfechaInicioEjecucion = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                splitfechainicio = fechaInicioSupervisor.split('/');
                var jfechaInicioSupervisor = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                if (jfechaInicioSupervisor < jfechaInicioEjecucion) {
                    document.getElementById(control.id).value = fechaOriginal;
                    alert('La Fecha de Inicio de Supervisor y/o Interventor debe ser mayor o igual a la Fecha de Ejecución de Contrato/Convenio');
                }
            }
        }

    </script>
    <script type="text/javascript" language="javascript">
        /*****************************************************************************
        Código para colocar los indicadores de miles  y decimales mientras se escribe
        Script creado por Tunait!
        Si quieres usar este script en tu sitio eres libre de hacerlo con la condición de que permanezcan intactas estas líneas, osea, los créditos.

        http://javascript.tunait.com
        tunait@yahoo.com  27/Julio/03
        ******************************************************************************/
        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

    </script>
    
    <table width="90%" align="center">
         <%--    <tr>
            <td>
           <Ajax:Accordion ID="AccContratos" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                    ContentCssClass="accordionContent"  runat="server"
                    Width="100%" Height="100%">
                    <Panes>
                        <Ajax:AccordionPane ID="ApPlanComprasProductos" runat="server">
                            <Header>
                                Plan de compras asociado / Productos
                            </Header>
                            <Content>
                                <table width="90%" align="center">
<%--                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="lbPlanCompras" runat="server" Text="Plan de compras"></asp:Label>
                                            <asp:CustomValidator ID="cvPlanCompras" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaPlanCompras"></asp:CustomValidator>
                                        </td>
                                    </tr>--%>
<%--                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfConsecutivosPlanCompras" runat="server" />
                                            <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="false" Width="30%" ViewStateMode="Enabled"
                                                OnTextChanged="txtPlanComprasTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgPlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="true" OnClientClick="GetPlanCompras(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>--%>
  <%--                                  <tr>
                                        <td>
                                            <asp:GridView ID="gvConsecutivos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" DataKeyNames="IDPlanDeComprasContratos,IDPlanDeCompras,Vigencia"
                                                CellPadding="8" Height="16px" CssClass="grillaCentral">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="IDPlanDeCompras" />
                                                    <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                                <RowStyle CssClass="rowBG" HorizontalAlign="Center" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="LbProductos" runat="server" Text="Productos"></asp:Label>
                                            <asp:HiddenField ID="hfProductos" runat="server" />
                                            <asp:CustomValidator ID="cvProductos" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaProductos"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />
                                                    <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                    <asp:BoundField HeaderText="Tipo de Modificacón" DataField="Modificacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Rubros Plan de Compras
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" DataKeyNames="total_rubro" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código Rubro" DataField="codigo_rubro" />
                                                    <asp:BoundField HeaderText="Descripción del Rubro" DataField="NombreRubro" />
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" DataField="" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tipo de Modificacón" DataField="Modificacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Consecutivo Plan de Compras
                                            <asp:CompareValidator runat="server" ID="cvConsecutivoPlanCompras" ControlToValidate="DdlConsecutivoPlanCompras"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="DdlConsecutivoPlanCompras" runat="server" Width="30%" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true" OnSelectedIndexChanged="DdlConsecutivoPlanComprasSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                    </Panes>
                </Ajax:Accordion>
                 
               </td>
        </tr> --%>

        <tr class="rowB">
            <td>
             Valores Actuales de Contrato
                </td>
        </tr>

        <tr class="rowB">
            <td>
                 
                <asp:GridView ID="gvAportesActuales" runat="server" AllowSorting="true"  AutoGenerateColumns="false" CellPadding="8" 
                              DataKeyNames ="IdAporteContrato,EsAdicion,AporteEnDinero,AportanteICBF" GridLines="None" Height="16px"  Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Procedencia" HeaderText="Procedencia" SortExpression="Procedencia" />
                        <asp:BoundField DataField="TipoAporte" HeaderText="Tipo de Aporte" SortExpression="TipoAporte" />
                        <asp:BoundField DataField="ValorAporte" DataFormatString="{0:c}" HeaderText="Valor Total Aporte" SortExpression="ValorAporte" />
                                                                        <asp:BoundField DataField="TipoModificacion" HeaderText="Tipo de Modificaci&oacute;n" SortExpression="TipoModificacion" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                 
                </td>
        </tr>
        <tr class="rowB">
            <td>

                Valores de la adición</td>
        </tr>
        <tr class="rowB">
            <td>

                <asp:GridView ID="gvAportesAdicion" runat="server" AllowSorting="true" OnRowDataBound="gvAportesAdicion_RowDataBound"  AutoGenerateColumns="false" CellPadding="8" 
                              DataKeyNames ="IdAporteContrato,EsAdicion,AporteEnDinero,AportanteICBF" GridLines="None" Height="16px"  Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" Visible="false"  runat="server" AutoPostBack="false"  >
                                <img alt="Editar" src="../../../Image/btn/info.jpg"   title="Editar" />
                            </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Procedencia" HeaderText="Procedencia" SortExpression="Procedencia" />
                        <asp:BoundField DataField="TipoAporte" HeaderText="Tipo de Aporte" SortExpression="TipoAporte" />
                        <asp:BoundField DataField="ValorAporte" DataFormatString="{0:c}" HeaderText="Valor Total Aporte" SortExpression="ValorAporte" />

                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                 
            </td>
        </tr>
    </table>

       <table width="90%" align="center">
        <tr class="rowB">
            <td>
              Documentos
            </td>
            <td colspan="2">
                &nbsp;</td>            
        </tr>
            <tr class="rowAG">
                              <td>
                                  <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    <asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIDCosModContractual" runat="server" />
    <asp:HiddenField ID="hfPostbck" runat="server" />
               <asp:HiddenField ID="hfCDP" runat="server" />
    <asp:HiddenField ID="hfIdEstadoContrato" runat="server" />
    <asp:HiddenField ID="hfCodContratoAsociadoSel" runat="server" />
    <asp:HiddenField ID="hfIdConvenioContratoAsociado" runat="server" />
    <asp:HiddenField ID="hfValorInicialContConv" runat="server" />
    <asp:HiddenField ID="hfFechaFinalizacion" runat="server" />
    <asp:HiddenField ID="hfFechaInicioEjecucion" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaConvenio" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServApoyoGestion" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServProfesionales" runat="server" />
    <asp:HiddenField ID="hfIdTipoContAporte" runat="server" />
    <asp:HiddenField ID="hfIdMarcoInteradministrativo" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirecta" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirectaAporte" runat="server" />
    <asp:HiddenField ID="hfTotalProductos" runat="server" />
    <asp:HiddenField ID="hfTotalProductosCDP" runat="server" />
    <asp:HiddenField ID="hfAcordeonActivo" runat="server" />
    <asp:HiddenField ID="hfObjPlan" runat="server" />
    <asp:HiddenField ID="hfAlcPlan" runat="server" />
    <asp:HiddenField ID="hfRegional" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdAdiciones" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="hfIndice" runat="server" />
    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
                                                <asp:HiddenField ID="hfConsecutivosPlanCompras" runat="server" />
    <script type="text/javascript" language="javascript">

     

    </script>
</asp:Content>