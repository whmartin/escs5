<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_FormaPago_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdFormaPago" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Medio de Pago
            </td>
            <td>
                Entidad Financiera
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdMedioPago"  Enabled="false" 
                    Width="350px"></asp:DropDownList>
            </td>
             <td>
                <asp:DropDownList runat="server" ID="ddlIdEntidadFinanciera"  Enabled="false" 
                    Width="350px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            
            <td>
                Tipo Cuenta Bancaria
            </td>
            <td>
                Cuenta Bancaria
            </td>
        </tr>
        <tr class="rowA">
           
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoCuentaBancaria"  Enabled="false" 
                    Width="350px"></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroCuentaBancaria"  Enabled="false" 
                    Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            
            <td>
                <asp:Label runat="server" Text="Id Proveedor" Visible="False"></asp:Label> 
            </td>
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:TextBox runat="server" ID="txtIdProveedores"  Enabled="false" 
                    Width="300px" Visible="False"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
