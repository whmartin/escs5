<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_FormaPago_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
<asp:HiddenField ID="hfIdFormaPago" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <%--<td>
                Id Proveedor *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdProveedores" ControlToValidate="txtIdProveedores"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>--%>
            <td>
                <asp:Label runat="server" ID="lblMedioPago" Text="Medio de Pago *"></asp:Label>
                
                <asp:RequiredFieldValidator runat="server" ID="rfvIdMedioPago" ControlToValidate="ddlIdMedioPago"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdMedioPago" ControlToValidate="ddlIdMedioPago"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                <asp:Label runat="server" ID="lblEntidadFinanciera" Text="Entidad Financiera"></asp:Label>
                
                 <asp:RequiredFieldValidator runat="server" ID="rfvEntidadFinanciera" ControlToValidate="ddlIdEntidadFinanciera"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" InitialValue="-1" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <%--<td>
                <asp:TextBox runat="server" ID="txtIdProveedores" MaxLength="20" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdProveedores" runat="server" TargetControlID="txtIdProveedores"
                    FilterType="Numbers" ValidChars="" />
                <img id="imghIdProveedores" alt="help" title="Corresponde al Id del proveedor asociado a la forma de pago." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdProveedores')" onmouseout="helpOut('imghIdProveedores')"/>
            </td>--%>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdMedioPago" 
                    onselectedindexchanged="ddlIdMedioPago_SelectedIndexChanged" 
                    AutoPostBack="True" Width="350px"></asp:DropDownList>
                <%--<img id="imghIdMedioPago" alt="help" title="Es el medio de pago." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdMedioPago')" onmouseout="helpOut('imghIdMedioPago')"/>--%>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdEntidadFinanciera" Width="570px"></asp:DropDownList>
                <%--<img id="imghIdEntidadFinanciera" alt="help" title="Corresponde a la entidad financiera." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdEntidadFinanciera')" onmouseout="helpOut('imghIdEntidadFinanciera')"/>--%>
            </td>
        </tr>
        <tr class="rowB">
            
            <td>
                <asp:Label runat="server" ID="lblTipoCuentaBancaria" Text="Tipo Cuenta Bancaria"></asp:Label>
                
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoCuentaBancaria" ControlToValidate="ddlTipoCuentaBancaria"
                 SetFocusOnError="true" ErrorMessage=" Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" InitialValue="-1" Enabled="False"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label runat="server" ID="lblCuentaBancaria" Text="Cuenta Bancaria"></asp:Label>
                
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroCuentaBancaria" ControlToValidate="txtNumeroCuentaBancaria"
                 SetFocusOnError="true" ErrorMessage=" Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoCuentaBancaria" Width="350px"></asp:DropDownList>
                <%--<img id="imghTipoCuentaBancaria" alt="help" title="&gt;Corresponde al tipo de cuenta bancaria." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghTipoCuentaBancaria')" onmouseout="helpOut('imghTipoCuentaBancaria')"/>--%>
            </td>
             <td>
                <asp:TextBox runat="server" ID="txtNumeroCuentaBancaria" MaxLength="80" 
                     Width="295px" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroCuentaBancaria" runat="server" TargetControlID="txtNumeroCuentaBancaria"
                    FilterType="Custom,Numbers" ValidChars="-" />
                <%--<img id="imghNumeroCuentaBancaria" alt="help" title="&gt;Corresponde al n&#250;mero de cuenta bancaria." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghNumeroCuentaBancaria')" onmouseout="helpOut('imghNumeroCuentaBancaria')"/>--%>
            </td>
        </tr>
        <tr class="rowA">
           <asp:HiddenField ID="hfIdProveedor" runat="server"/>
        </tr>
    </table>
</asp:Content>
