using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición para la entidad FormaPago
/// </summary>
public partial class Page_FormaPago_Add : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contrato/FormaPago";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        
    }

    #region Eventos
    
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List); 
    }

    protected void ddlIdMedioPago_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdMedioPago.SelectedValue != "-1")
        {
            if (ddlIdMedioPago.SelectedValue == "14")
            {
                rfvEntidadFinanciera.Enabled = true;
                rfvNumeroCuentaBancaria.Enabled = true;
                rfvTipoCuentaBancaria.Enabled = true;

                ddlIdEntidadFinanciera.Enabled = true;
                ddlTipoCuentaBancaria.Enabled = true;
                txtNumeroCuentaBancaria.Enabled = true;
                lblCuentaBancaria.Text += @"*";
                lblEntidadFinanciera.Text += @"*";
                lblTipoCuentaBancaria.Text += @"*";


            }
            else /*if (ddlIdMedioPago.SelectedValue == "4")*/
            {
                rfvEntidadFinanciera.Enabled = false;
                rfvNumeroCuentaBancaria.Enabled = false;
                rfvTipoCuentaBancaria.Enabled = false;
                ddlIdEntidadFinanciera.Enabled = false;
                ddlTipoCuentaBancaria.Enabled = false;
                txtNumeroCuentaBancaria.Enabled = false;
                lblCuentaBancaria.Text = lblCuentaBancaria.Text.Replace(@"*", "");
                lblEntidadFinanciera.Text = lblEntidadFinanciera.Text.Replace(@"*", "");
                lblTipoCuentaBancaria.Text = lblTipoCuentaBancaria.Text.Replace(@"*", "");
                ddlIdEntidadFinanciera.SelectedIndex = 0;
                ddlTipoCuentaBancaria.SelectedIndex = 0;
                txtNumeroCuentaBancaria.Text = string.Empty;
            }


        }
        else
        {
            rfvEntidadFinanciera.Enabled = false;
            rfvNumeroCuentaBancaria.Enabled = false;
            rfvTipoCuentaBancaria.Enabled = false;
            ddlIdEntidadFinanciera.Enabled = false;
            ddlTipoCuentaBancaria.Enabled = false;
            txtNumeroCuentaBancaria.Enabled = false;
            lblCuentaBancaria.Text = lblCuentaBancaria.Text.Replace(@"*", "");
            lblEntidadFinanciera.Text = lblEntidadFinanciera.Text.Replace(@"*", "");
            lblTipoCuentaBancaria.Text = lblTipoCuentaBancaria.Text.Replace(@"*", "");
            ddlIdEntidadFinanciera.SelectedIndex = 0;
            ddlTipoCuentaBancaria.SelectedIndex = 0;
            txtNumeroCuentaBancaria.Text = string.Empty;
        }
    }
    #endregion

    #region Métodos

    
    /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad FormaPago
        /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            FormaPago vFormaPago = new FormaPago();
                vFormaPago.IdProveedores = Convert.ToInt32(GetSessionParameter("Contrato.IdProveedor"));

            int idProveedoresContratos = 0;
                    if (!int.TryParse(GetSessionParameter("FormaPago.IDProveedoresContratos").ToString(), out idProveedoresContratos))
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor");
                        return;
                    }
                if (ddlIdMedioPago.SelectedValue != "-1")
                {
                    vFormaPago.IdMedioPago = Convert.ToInt32(ddlIdMedioPago.SelectedValue);
                }
                if (ddlIdEntidadFinanciera.SelectedValue != "-1")
                {
                    vFormaPago.IdEntidadFinanciera = Convert.ToInt32(ddlIdEntidadFinanciera.SelectedValue);
                }
                if (ddlTipoCuentaBancaria.SelectedValue != "-1")
                {
                    vFormaPago.TipoCuentaBancaria = Convert.ToInt32(ddlTipoCuentaBancaria.SelectedValue);
                }

                if (txtNumeroCuentaBancaria.Text != string.Empty)
                {
                    vFormaPago.NumeroCuentaBancaria = Convert.ToString(txtNumeroCuentaBancaria.Text);
                }
                if (Request.QueryString["oP"] == "E")
                {

                    vFormaPago.IdFormaPago = Convert.ToInt32(hfIdFormaPago.Value);
                    vFormaPago.UsuarioModifica = GetSessionUser().NombreUsuario;
                    List<FormaPago> lFormaPago = vContratoService.ConsultarFormaPagos(vFormaPago.IdProveedores, vFormaPago.IdMedioPago, vFormaPago.IdEntidadFinanciera, vFormaPago.TipoCuentaBancaria, vFormaPago.NumeroCuentaBancaria);
                    if (lFormaPago.Count > 0)
                    {
                        if (ddlIdMedioPago.SelectedValue == "14")
                        {
                            toolBar.MostrarMensajeError("Esta cuenta ya esta registrada.");

                        }
                        else
                        {
                            toolBar.MostrarMensajeError("Este medio de pago ya se encuentra seleccionado.");

                        }
                        return;
                    }

                    if (vContratoService.ConsultarFormaPagoContrato(vFormaPago.IdFormaPago, idProveedoresContratos))
                    {
                        toolBar.MostrarMensajeError("Esta cuenta se encuentra asociada a otro contrato y no puede ser modificada.");
                        return;
                    }

                    InformacionAudioria(vFormaPago, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarFormaPago(vFormaPago);
                }
                else
                {
                    List<FormaPago> lFormaPago = vContratoService.ConsultarFormaPagos(vFormaPago.IdProveedores, vFormaPago.IdMedioPago, vFormaPago.IdEntidadFinanciera, vFormaPago.TipoCuentaBancaria, vFormaPago.NumeroCuentaBancaria);
                    if (lFormaPago.Count > 0)
                    {
                        if (ddlIdMedioPago.SelectedValue == "14")
                        {
                            toolBar.MostrarMensajeError("Esta cuenta ya esta registrada.");
                            
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("Este medio de pago ya se encuentra seleccionado.");
                            
                        }
                        return;
                    }
                    vFormaPago.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vFormaPago, this.PageName, vSolutionPage);
                    vResultado = vContratoService.InsertarFormaPago(vFormaPago);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("Problemas para almacenar la información, ponerse en contacto con el administrador del sistema.");
                    return;
                }
                else if (vResultado == 1)
                {
                    if (vFormaPago.IdFormaPago != 0)
                    {
                        
                        vResultado = vContratoService.ModificarFormaPagoContrato(idProveedoresContratos, vFormaPago.IdFormaPago, GetSessionUser().NombreUsuario);
                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor. ");
                            return;
                        }
                        if (vResultado==1)
                        {
                            SetSessionParameter("FormaPago.IdFormaPago", vFormaPago.IdFormaPago);
                            SetSessionParameter("FormaPago.Guardado", "1");
                            NavigateTo(SolutionPage.Detail);
                            SeleccionarRegistros(vFormaPago.IdFormaPago.ToString());
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("Problemas para almacenar la información, ponerse en contacto con el administrador del sistema.");
                        }
                        
                    }
                    
                }
                else
                {
                    toolBar.MostrarMensajeError("Problemas para almacenar la información, ponerse en contacto con el administrador del sistema.");
                }
                
                

            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
    }

        /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegateLupa(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.EstablecerTitulos("Forma de Pago", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que carga los datos del registro a editar
        /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdFormaPago = Convert.ToInt32(GetSessionParameter("FormaPago.IdFormaPago"));
            RemoveSessionParameter("FormaPago.Id");

            FormaPago vFormaPago = new FormaPago();
            vFormaPago = vContratoService.ConsultarFormaPago(vIdFormaPago);
            hfIdFormaPago.Value = vFormaPago.IdFormaPago.ToString();
            ddlIdMedioPago.SelectedValue = vFormaPago.IdMedioPago.ToString();

            hfIdProveedor.Value = vFormaPago.IdProveedores.ToString();

            if (ddlIdMedioPago.SelectedValue == "14")
            {
                rfvEntidadFinanciera.Enabled = true;
                rfvNumeroCuentaBancaria.Enabled = true;
                rfvTipoCuentaBancaria.Enabled = true;

                ddlIdEntidadFinanciera.Enabled = true;
                ddlTipoCuentaBancaria.Enabled = true;
                txtNumeroCuentaBancaria.Enabled = true;
                lblCuentaBancaria.Text += @"*";
                lblEntidadFinanciera.Text += @"*";
                lblTipoCuentaBancaria.Text += @"*";


            }
            else
            {
                rfvEntidadFinanciera.Enabled = false;
                rfvNumeroCuentaBancaria.Enabled = false;
                rfvTipoCuentaBancaria.Enabled = false;
                ddlIdEntidadFinanciera.Enabled = false;
                ddlTipoCuentaBancaria.Enabled = false;
                txtNumeroCuentaBancaria.Enabled = false;
                lblCuentaBancaria.Text = lblCuentaBancaria.Text.Replace(@"*", "");
                lblEntidadFinanciera.Text = lblEntidadFinanciera.Text.Replace(@"*", "");
                lblTipoCuentaBancaria.Text = lblTipoCuentaBancaria.Text.Replace(@"*", "");
                ddlIdEntidadFinanciera.SelectedIndex = 0;
                ddlTipoCuentaBancaria.SelectedIndex = 0;
                txtNumeroCuentaBancaria.Text = string.Empty; 
            }
            
            ddlIdEntidadFinanciera.SelectedValue = vFormaPago.IdEntidadFinanciera.ToString();
            ddlTipoCuentaBancaria.SelectedValue = vFormaPago.TipoCuentaBancaria.ToString();
            txtNumeroCuentaBancaria.Text = vFormaPago.NumeroCuentaBancaria;
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vFormaPago.UsuarioCrea, vFormaPago.FechaCrea, vFormaPago.UsuarioModifica, vFormaPago.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas y valores por defecto 
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaMedioPago();
            CargarListaEntidadFinanciera();
            CargarListaTipoCuentaBancaria();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    

    public void CargarListaMedioPago()
    {

        LlenarTipoMedioPagos(ddlIdMedioPago, null, null, true, true, "DescTipoMedioPago", "IdTipoMedioPago");

        //ManejoControlesContratos.LlenarComboLista(ddlIdMedioPago, vContratoService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }
    public void CargarListaEntidadFinanciera()
    {

        LlenarEntidadFinanciera(ddlIdEntidadFinanciera, "-1", true, null);


    }
    public void CargarListaTipoCuentaBancaria()
    {

        //ManejoControlesContratos.LlenarComboLista(ddlTipoCuentaBancaria, vContratoService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
        LlenarTipoCuentaEntFins(ddlTipoCuentaBancaria, null, null, true, true, "Descripcion", "IdTipoCta");

    }

    public void LlenarTipoMedioPagos(DropDownList pDropDownList,
                                     String pCodMedioPago,
                                     String pDescTipoMedioPago,
                                     Boolean? pEstado,
                                     Boolean pMostrarSeleccionar,
                                     String pDataTextField,
                                     String pDataValueField)
    {
        pDropDownList.Items.Clear();
        var vlstTipoMedioPago = new List<TipoMedioPago>();
        var vlstConsultaTipoMedioPago = vContratoService.ConsultarTipoMedioPagos(pCodMedioPago, pDescTipoMedioPago, pEstado);
        if (!vlstConsultaTipoMedioPago.Count.Equals(0))
        {
            vlstTipoMedioPago.AddRange(from lst in vlstConsultaTipoMedioPago
                                       orderby lst.CodMedioPago ascending
                                       select new TipoMedioPago
                                       {
                                           DescTipoMedioPago = lst.CodMedioPago + " - " + lst.DescTipoMedioPago,
                                           IdTipoMedioPago = lst.IdTipoMedioPago,
                                           CodMedioPago = lst.CodMedioPago
                                       });

            pDropDownList.DataTextField = pDataTextField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataSource = vlstTipoMedioPago;
            pDropDownList.DataBind();
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
    }

    public void LlenarEntidadFinanciera(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, Boolean? estado)
    {
        pDropDownList.Items.Clear();
        List<EntidadFinanciera> vLTipoArchivoPlano = vContratoService.ConsultarEntidadFinancieras(null, null, estado, null, null);
        foreach (EntidadFinanciera ef in vLTipoArchivoPlano)
        {

            pDropDownList.Items.Add(new ListItem(ef.NombreEntFin.ToString(), ef.IdEntidadFinanciera.ToString()));
        }
        pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }

    }

    public void LlenarTipoCuentaEntFins(DropDownList pDropDownList,
                                        String pCodTipoCta,
                                        String pDescripcion,
                                        Boolean? pEstado,
                                        Boolean pMostrarSeleccionar,
                                        String pDataTextField,
                                        String pDataValueField)
    {
        pDropDownList.Items.Clear();
        var vlstTipoCuentaEntFin = new List<TipoCuentaEntFin>();
        var vlstConsultaTipoCuentaEntFin = vContratoService.ConsultarTipoCuentaEntFins(pCodTipoCta, pDescripcion, pEstado);
        if (!vlstConsultaTipoCuentaEntFin.Count.Equals(0))
        {
            vlstTipoCuentaEntFin.AddRange(from lst in vlstConsultaTipoCuentaEntFin
                                          orderby lst.CodTipoCta ascending
                                          select new TipoCuentaEntFin
                                          {
                                              Descripcion = lst.CodTipoCta + " - " + lst.Descripcion,
                                              IdTipoCta = lst.IdTipoCta,
                                              CodTipoCta = lst.CodTipoCta
                                          });

            pDropDownList.DataTextField = pDataTextField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataSource = vlstTipoCuentaEntFin;
            pDropDownList.DataBind();
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
    }


    private void SeleccionarRegistros(string IdFormaPago)
    {
        try
        {
            string returnValues =
           "<script language='javascript'> " +
           "var pObj = Array();";
            returnValues += "pObj[0] = '" + HttpUtility.HtmlDecode(IdFormaPago) + "';";
            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    #endregion

    

    
}
