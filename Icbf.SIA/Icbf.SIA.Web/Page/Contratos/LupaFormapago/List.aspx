<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_FormaPago_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        
        <tr class="rowB">
            <td class="Cell" colspan="2">
                Tipo Persona
            </td>
            <td class="Cell">
                Tipo Identificaci&oacute;n
            </td>
        </tr>
        
         <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:TextBox runat="server" ID="txtTipoPersona" Enabled="False" Width="300px"></asp:TextBox>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtTipoIdentificacion" Enabled="False" 
                    Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" style="width: 25%">
                N&uacute;mero identificaci&oacute;n
            </td>
            <td>
                <asp:Label runat="server" ID="lblEtiqDigVerif" Text="DV" Visible="False"></asp:Label>
            </td>
            <td class="Cell">
                Contratista
            </td>
        </tr>
        
         <tr class="rowA">
            <td class="Cell" style="width: 25%">
                <asp:TextBox runat="server" ID="txtNumeroIdent" Enabled="False" Width="200px"></asp:TextBox> <asp:Label runat="server" ID="lblSepDigVerif" Text="-"></asp:Label>
                </td>
                <td>
                <asp:TextBox runat="server" ID="txtDigVerificacion" Width="20" Visible="False" 
                    Enabled="False"></asp:TextBox>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtContratista" Enabled="False" Width="450px"></asp:TextBox>
            </td>
        </tr>

        <tr class="rowB">
            <td class="Cell" colspan="2">
                <asp:Label ID="Label3" runat="server" Text="Medio de Pago" Visible="False"></asp:Label> 
            </td>
            <td class="Cell">
               <asp:Label ID="Label2" runat="server" Text="Entidad Financiera" Visible="False"></asp:Label> 
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdMedioPago" Visible="False" 
                    Width="350px"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIdEntidadFinanciera" Visible="False" 
                    Width="350px" ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell" colspan="2">
                <asp:Label ID="Label1" runat="server" Text="Tipo Cuenta Bancaria" Visible="False"></asp:Label> 
            </td>
            <td class="Cell">
                <asp:Label runat="server" Text="Cuenta Bancaria" Visible="False"></asp:Label> 
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell" colspan="2">
                <asp:DropDownList runat="server" ID="ddlTipoCuentaBancaria" Visible="False" 
                    Width="350px" ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroCuentaBancaria" MaxLength="80" 
                    Visible="False" Width="300px" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroCuentaBancaria" runat="server" TargetControlID="txtNumeroCuentaBancaria"
                    FilterType="Custom,Numbers" ValidChars="-" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="3">
                <asp:Label runat="server" ID="lblFormaPago" Text="Informaci&oacute;n Forma de Pago" Visible="False"></asp:Label>
                
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvFormaPago" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdFormaPago" CellPadding="0" Height="16px"
                        OnSorting="gvFormaPago_Sorting" AllowSorting="True"  
                        OnPageIndexChanging="gvFormaPago_PageIndexChanging" 
                        OnSelectedIndexChanged="gvFormaPago_SelectedIndexChanged" 
                        onrowcommand="gvFormaPago_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Medio de Pago" DataField="IdMedioPago"  SortExpression="IdMedioPago" Visible="False"/>
                            <asp:BoundField HeaderText="Entidad Financiera" DataField="IdEntidadFinanciera"  SortExpression="IdEntidadFinanciera" Visible="False"/>
                            <asp:BoundField HeaderText="Tipo de Cuenta Bancaria" DataField="TipoCuentaBancaria"  SortExpression="TipoCuentaBancaria" Visible="False"/>
                            
                            
                            <asp:BoundField HeaderText="Medio de Pago" DataField="NombreTipoMedioPago"  SortExpression="NombreTipoMedioPago"/>
                            <asp:BoundField HeaderText="Entidad Financiera" DataField="NombreEntidadFinanciera"  SortExpression="NombreEntidadFinanciera"/>
                            <asp:BoundField HeaderText="Tipo Cuenta Bancaria" DataField="NombreTipoCuenta"  SortExpression="NombreTipoCuenta"/>
                            <asp:BoundField HeaderText="Cuenta Bancaria" DataField="NumeroCuentaBancaria"  SortExpression="NumeroCuentaBancaria" />
                            <asp:TemplateField HeaderText="Cuenta Seleccionada">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chbSeleccionada" runat="server"
                                            AutoPostBack="true" Enabled="False"/>
                                    </ItemTemplate>
                                    <ControlStyle Width="25px" />
                                    <FooterStyle Width="25px" />
                                    <HeaderStyle Width="25px" />
                                    <ItemStyle Width="25px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnSeleccionar" runat="server" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Seleccionar forma de pago"  CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
