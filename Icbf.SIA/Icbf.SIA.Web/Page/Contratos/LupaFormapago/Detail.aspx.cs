using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad FormaPago
/// </summary>
public partial class Page_FormaPago_Detail : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contrato/FormaPago";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        int vIdFormaPago = Convert.ToInt32(hfIdFormaPago.Value);
        FormaPago vFormaPago = new FormaPago();
        vFormaPago = vContratoService.ConsultarFormaPago(vIdFormaPago);
        int idContrato = 0;
        if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out idContrato))
        {
            toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
            return;
        }

        if (vContratoService.ConsultarProveedoresContratosFormaPago(vFormaPago.IdProveedores, vFormaPago.IdFormaPago, idContrato))
        {
            toolBar.MostrarMensajeError("Esta cuenta se encuenta asociada a otro contrato y no puede ser modificada.");
            return;
        }

        int idProveedoresContratos = 0;
        if (!int.TryParse(GetSessionParameter("FormaPago.IDProveedoresContratos").ToString(), out idProveedoresContratos))
        {
            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor");
            return;
        }

        if (vContratoService.ConsultarFormaPagoContrato(vFormaPago.IdFormaPago, idProveedoresContratos))
        {
            toolBar.MostrarMensajeError("Esta cuenta se encuentra asociada a otro contrato y no puede ser modificada.");
            return;
        }
        SetSessionParameter("FormaPago.IdFormaPago", hfIdFormaPago.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdFormaPago = Convert.ToInt32(GetSessionParameter("FormaPago.IdFormaPago"));
            RemoveSessionParameter("FormaPago.IdFormaPago");

            //if (GetSessionParameter("FormaPago.Guardado").ToString() == "1")
            //    toolBar.MostrarMensajeGuardado();
            //RemoveSessionParameter("FormaPago.Guardado");


            FormaPago vFormaPago = new FormaPago();
            vFormaPago = vContratoService.ConsultarFormaPago(vIdFormaPago);
            hfIdFormaPago.Value = vFormaPago.IdFormaPago.ToString();
            txtIdProveedores.Text = vFormaPago.IdProveedores.ToString();
            ddlIdMedioPago.SelectedValue = vFormaPago.IdMedioPago.ToString();
            ddlIdEntidadFinanciera.SelectedValue = vFormaPago.IdEntidadFinanciera.ToString();
            ddlTipoCuentaBancaria.SelectedValue = vFormaPago.TipoCuentaBancaria.ToString();
            txtNumeroCuentaBancaria.Text = vFormaPago.NumeroCuentaBancaria;
            ObtenerAuditoria(PageName, hfIdFormaPago.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vFormaPago.UsuarioCrea, vFormaPago.FechaCrea, vFormaPago.UsuarioModifica, vFormaPago.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de eliminación del registro seleccionado 
        /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdFormaPago = Convert.ToInt32(hfIdFormaPago.Value);
            int idContrato = 0;
            FormaPago vFormaPago = new FormaPago();
            vFormaPago = vContratoService.ConsultarFormaPago(vIdFormaPago);
            InformacionAudioria(vFormaPago, this.PageName, vSolutionPage);

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out idContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            if (vContratoService.ConsultarProveedoresContratosFormaPago(vFormaPago.IdProveedores, vFormaPago.IdFormaPago, idContrato))
            {
                toolBar.MostrarMensajeError("Esta cuenta se encuenta asociada a otro contrato y no puede ser eliminada.");
                return;
            }
            int idProveedoresContratos = 0;
            if (!int.TryParse(GetSessionParameter("FormaPago.IDProveedoresContratos").ToString(), out idProveedoresContratos))
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor");
                return;
            }

            if (vContratoService.ConsultarFormaPagoContrato(vFormaPago.IdFormaPago, idProveedoresContratos))
            {
                toolBar.MostrarMensajeError("Esta cuenta se encuentra asociada a otro contrato y no puede ser eliminada.");
                return;
            }

            int vResultado = vContratoService.EliminarFormaPago(vFormaPago);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeEliminado();
                SetSessionParameter("FormaPago.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa) this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnBuscar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegateLupa(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegateLupa(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegateLupa(btnEliminar_Click);

            toolBar.EstablecerTitulos("Forma de Pago", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
        /// <summary>
        /// Método de carga de listas desplegables y valores por defecto
        /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaMedioPago();
            CargarListaEntidadFinanciera();
            CargarListaTipoCuentaBancaria();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaMedioPago()
    {

        LlenarTipoMedioPagos(ddlIdMedioPago, null, null, true, true, "DescTipoMedioPago", "IdTipoMedioPago");

        //ManejoControlesContratos.LlenarComboLista(ddlIdMedioPago, vContratoService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }
    public void CargarListaEntidadFinanciera()
    {

        LlenarEntidadFinanciera(ddlIdEntidadFinanciera, "-1", true, null);


    }
    public void CargarListaTipoCuentaBancaria()
    {

        //ManejoControlesContratos.LlenarComboLista(ddlTipoCuentaBancaria, vContratoService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
        LlenarTipoCuentaEntFins(ddlTipoCuentaBancaria, null, null, true, true, "Descripcion", "IdTipoCta");

    }

    public void LlenarTipoMedioPagos(DropDownList pDropDownList,
                                     String pCodMedioPago,
                                     String pDescTipoMedioPago,
                                     Boolean? pEstado,
                                     Boolean pMostrarSeleccionar,
                                     String pDataTextField,
                                     String pDataValueField)
    {
        pDropDownList.Items.Clear();
        var vlstTipoMedioPago = new List<TipoMedioPago>();
        var vlstConsultaTipoMedioPago = vContratoService.ConsultarTipoMedioPagos(pCodMedioPago, pDescTipoMedioPago, pEstado);
        if (!vlstConsultaTipoMedioPago.Count.Equals(0))
        {
            vlstTipoMedioPago.AddRange(from lst in vlstConsultaTipoMedioPago
                                       orderby lst.CodMedioPago ascending
                                       select new TipoMedioPago
                                       {
                                           DescTipoMedioPago = lst.CodMedioPago + " - " + lst.DescTipoMedioPago,
                                           IdTipoMedioPago = lst.IdTipoMedioPago,
                                           CodMedioPago = lst.CodMedioPago
                                       });

            pDropDownList.DataTextField = pDataTextField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataSource = vlstTipoMedioPago;
            pDropDownList.DataBind();
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
    }

    public void LlenarEntidadFinanciera(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, Boolean? estado)
    {
        pDropDownList.Items.Clear();
        List<EntidadFinanciera> vLTipoArchivoPlano = vContratoService.ConsultarEntidadFinancieras(null, null, estado, null, null);
        foreach (EntidadFinanciera ef in vLTipoArchivoPlano)
        {

            pDropDownList.Items.Add(new ListItem(ef.NombreEntFin.ToString(), ef.IdEntidadFinanciera.ToString()));
        }
        pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }

    }

    public void LlenarTipoCuentaEntFins(DropDownList pDropDownList,
                                        String pCodTipoCta,
                                        String pDescripcion,
                                        Boolean? pEstado,
                                        Boolean pMostrarSeleccionar,
                                        String pDataTextField,
                                        String pDataValueField)
    {
        pDropDownList.Items.Clear();
        var vlstTipoCuentaEntFin = new List<TipoCuentaEntFin>();
        var vlstConsultaTipoCuentaEntFin = vContratoService.ConsultarTipoCuentaEntFins(pCodTipoCta, pDescripcion, pEstado);
        if (!vlstConsultaTipoCuentaEntFin.Count.Equals(0))
        {
            vlstTipoCuentaEntFin.AddRange(from lst in vlstConsultaTipoCuentaEntFin
                                          orderby lst.CodTipoCta ascending
                                          select new TipoCuentaEntFin
                                          {
                                              Descripcion = lst.CodTipoCta + " - " + lst.Descripcion,
                                              IdTipoCta = lst.IdTipoCta,
                                              CodTipoCta = lst.CodTipoCta
                                          });

            pDropDownList.DataTextField = pDataTextField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataSource = vlstTipoCuentaEntFin;
            pDropDownList.DataBind();
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccionar", "-1")); }
    }

}
