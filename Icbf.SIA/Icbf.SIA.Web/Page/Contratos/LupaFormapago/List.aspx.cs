using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using System.Web.Security;
using Icbf.Seguridad.Service;

/// <summary>
/// Página de consulta a través de filtros para la entidad FormaPago
/// </summary>
public partial class Page_FormaPago_List : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contrato/FormaPago";
    ContratoService vContratoService = new ContratoService();
    SeguridadService vSeguridadFAC = new SeguridadService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

        /// <summary>
        /// Método que realiza la búsqueda filtrada con múltiples criterios 
        /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvFormaPago, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegateLupa(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            gvFormaPago.PageSize = PageSize();
            gvFormaPago.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Forma de Pago", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método para redirigir a la página detalle del registro seleccionado 
        /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvFormaPago.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("FormaPago.IdFormaPago", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvFormaPago_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvFormaPago.SelectedRow);
    }
    protected void gvFormaPago_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFormaPago.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvFormaPago_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int? vIdMedioPago = null;
            int? vIdEntidadFinanciera = null;
            int? vTipoCuentaBancaria = null;
            int vIdProveedor = 0;
            String vNumeroCuentaBancaria = null;
            if (ddlIdMedioPago.SelectedValue!= "-1")
            {
                vIdMedioPago = Convert.ToInt32(ddlIdMedioPago.SelectedValue);
            }
            if (ddlIdEntidadFinanciera.SelectedValue!= "-1")
            {
                vIdEntidadFinanciera = Convert.ToInt32(ddlIdEntidadFinanciera.SelectedValue);
            }
            if (ddlTipoCuentaBancaria.SelectedValue!= "-1")
            {
                vTipoCuentaBancaria = Convert.ToInt32(ddlTipoCuentaBancaria.SelectedValue);
            }
            if (txtNumeroCuentaBancaria.Text!= "")
            {
                vNumeroCuentaBancaria = Convert.ToString(txtNumeroCuentaBancaria.Text);
            }

            if (!int.TryParse(GetSessionParameter("Contrato.IdProveedor").ToString(), out vIdProveedor))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            var myGridResults = vContratoService.ConsultarFormaPagos(vIdProveedor, null, vIdEntidadFinanciera, vTipoCuentaBancaria, vNumeroCuentaBancaria);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(FormaPago), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<FormaPago, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList(); 
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }
            lblFormaPago.Visible = true;
            gridViewsender.DataBind();
            int idContrato = 0;

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out idContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }
            List<ProveedoresContratos> lProveedoresContratos = new List<ProveedoresContratos>();
            lProveedoresContratos = vContratoService.ConsultarProveedoresContratoss(vIdProveedor, idContrato);
            if (gvFormaPago.Rows.Count >0)
            {
                if (lProveedoresContratos.Count > 0)
                {
                    foreach (GridViewRow fila in gvFormaPago.Rows)
                    {
                        int idFormaPago = Convert.ToInt32(gvFormaPago.DataKeys[fila.RowIndex]["IdFormaPago"]);

                        if (idFormaPago == lProveedoresContratos[0].IdFormaPago)
                        {
                            CheckBox chkSeleccionado = ((CheckBox)fila.Cells[0].FindControl("chbSeleccionada"));
                            chkSeleccionado.Enabled = true;
                            chkSeleccionado.Checked = true;
                            chkSeleccionado.Enabled = false;
                        }
                    } 
                }
                
            }
            

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int idProveedores = 0;
            int idContratos = 0;
            //Valor de prueba para el campo traido por session de id del proveedor
            //string valorTempSessionIdProveedor = "3";
            //SetSessionParameter("Contrato.IdProveedor", valorTempSessionIdProveedor);
            // Valor de prueba para el campo traido por session de id del proveedor

            //Valor de prueba para el campo traido por session de id del contrato
            //string valorTempSessionIdContrato = "2";
            //SetSessionParameter("Contrato.IdContrato", valorTempSessionIdContrato);
            //GetSessionParameter("Contrato.ContratosAPP")
            // Valor de prueba para el campo traido por session de id del contrato
        
            
            string[] lRoles = Roles.GetRolesForUser(GetSessionUser().NombreUsuario);

            if (!vContratoService.ConsultarContratoUsuarioCrea(GetSessionUser().NombreUsuario, Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"))))
            {
                if (!vSeguridadFAC.ConsultarRolesNombreEsAdmin(lRoles[0], true))
                {
                    toolBar.MostrarMensajeError("El usuario ingresado no puede modificar la forma de pago para este contrato.");
                    return;


                }
                
            }
            
            
            if (!int.TryParse(GetSessionParameter("Contrato.IdProveedor").ToString(), out idProveedores))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out idContratos))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }
            Tercero vTercero = new Tercero();

            vTercero = vContratoService.ConsultarTercero(idProveedores);

            txtTipoPersona.Text =  vTercero.NombreTipoPersona;
            txtTipoIdentificacion.Text = vTercero.NombreListaTipoDocumento;
            txtNumeroIdent.Text = vTercero.NumeroIdentificacion;
            if (vTercero.IdDListaTipoDocumento.ToString() == "7")
            {
                txtContratista.Text = vTercero.RazonSocial;
                lblSepDigVerif.Visible = true;
                txtDigVerificacion.Visible = true;
                txtDigVerificacion.Enabled = true;
                txtDigVerificacion.Text = vTercero.DigitoVerificacion.ToString();
                txtDigVerificacion.Enabled = false;
                lblEtiqDigVerif.Visible = true;

            }
            else
            {
                txtContratista.Text = vTercero.Nombre_Razonsocial;
                txtDigVerificacion.Enabled = false;
                lblSepDigVerif.Visible = false;
                txtDigVerificacion.Visible = false;
                txtDigVerificacion.Enabled = false;
                lblEtiqDigVerif.Visible = false;

            }

            vSeguridadFAC.ConsultarRoles(lRoles[0], true);
            if (GetSessionParameter("FormaPago.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("FormaPago.Eliminado");
            CargarListaMedioPago();
            CargarListaEntidadFinanciera();
            CargarListaTipoCuentaBancaria();
            //RemoveSessionParameter("Contrato.IdProveedor");
            Buscar();


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaMedioPago()
    {

        LlenarTipoMedioPagos(ddlIdMedioPago, null, null, true, true, "DescTipoMedioPago", "IdTipoMedioPago");

        //ManejoControlesContratos.LlenarComboLista(ddlIdMedioPago, vContratoService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }
    public void CargarListaEntidadFinanciera()
    {

        LlenarEntidadFinanciera(ddlIdEntidadFinanciera, "-1", true, null);


    }
    public void CargarListaTipoCuentaBancaria()
    {

        //ManejoControlesContratos.LlenarComboLista(ddlTipoCuentaBancaria, vContratoService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
        LlenarTipoCuentaEntFins(ddlTipoCuentaBancaria, null, null, true, true, "Descripcion", "IdTipoCta");

    }

    public void LlenarTipoMedioPagos(DropDownList pDropDownList,
                                     String pCodMedioPago,
                                     String pDescTipoMedioPago,
                                     Boolean? pEstado,
                                     Boolean pMostrarSeleccionar,
                                     String pDataTextField,
                                     String pDataValueField)
    {
        pDropDownList.Items.Clear();
        var vlstTipoMedioPago = new List<TipoMedioPago>();
        var vlstConsultaTipoMedioPago = vContratoService.ConsultarTipoMedioPagos(pCodMedioPago, pDescTipoMedioPago, pEstado);
        if (!vlstConsultaTipoMedioPago.Count.Equals(0))
        {
            vlstTipoMedioPago.AddRange(from lst in vlstConsultaTipoMedioPago
                                       orderby lst.CodMedioPago ascending
                                       select new TipoMedioPago
                                       {
                                           DescTipoMedioPago = lst.CodMedioPago + " - " + lst.DescTipoMedioPago,
                                           IdTipoMedioPago = lst.IdTipoMedioPago,
                                           CodMedioPago = lst.CodMedioPago
                                       });

            pDropDownList.DataTextField = pDataTextField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataSource = vlstTipoMedioPago;
            pDropDownList.DataBind();
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
    }

    public void LlenarEntidadFinanciera(DropDownList pDropDownList, string pPredeterminado, Boolean pMostrarSeleccionar, Boolean? estado)
    {
        pDropDownList.Items.Clear();
        List<EntidadFinanciera> vLTipoArchivoPlano = vContratoService.ConsultarEntidadFinancieras(null, null, estado, null, null);
        foreach (EntidadFinanciera ef in vLTipoArchivoPlano)
        {

            pDropDownList.Items.Add(new ListItem(ef.NombreEntFin.ToString(), ef.IdEntidadFinanciera.ToString()));
        }
        pDropDownList.DataBind();
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }

    }

    public void LlenarTipoCuentaEntFins(DropDownList pDropDownList,
                                        String pCodTipoCta,
                                        String pDescripcion,
                                        Boolean? pEstado,
                                        Boolean pMostrarSeleccionar,
                                        String pDataTextField,
                                        String pDataValueField)
    {
        pDropDownList.Items.Clear();
        var vlstTipoCuentaEntFin = new List<TipoCuentaEntFin>();
        var vlstConsultaTipoCuentaEntFin = vContratoService.ConsultarTipoCuentaEntFins(pCodTipoCta, pDescripcion, pEstado);
        if (!vlstConsultaTipoCuentaEntFin.Count.Equals(0))
        {
            vlstTipoCuentaEntFin.AddRange(from lst in vlstConsultaTipoCuentaEntFin
                                          orderby lst.CodTipoCta ascending
                                          select new TipoCuentaEntFin
                                          {
                                              Descripcion = lst.CodTipoCta + " - " + lst.Descripcion,
                                              IdTipoCta = lst.IdTipoCta,
                                              CodTipoCta = lst.CodTipoCta
                                          });

            pDropDownList.DataTextField = pDataTextField;
            pDropDownList.DataValueField = pDataValueField;
            pDropDownList.DataSource = vlstTipoCuentaEntFin;
            pDropDownList.DataBind();
        }
        if (pMostrarSeleccionar) { pDropDownList.Items.Insert(0, new ListItem("Seleccione", "-1")); }
    }

    protected void gvFormaPago_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int vResultado;
        if (e.CommandName == "Seleccionar")
        {
            
            string strValue = gvFormaPago.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                int idProveedoresContratos = 0;
                if (!int.TryParse(GetSessionParameter("FormaPago.IDProveedoresContratos").ToString(), out idProveedoresContratos))
                {
                    toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                    return;
                }

                vResultado = vContratoService.ModificarFormaPagoContrato(idProveedoresContratos, Convert.ToInt32(strValue), GetSessionUser().NombreUsuario);
                if (vResultado == 0)
                {
                    int vIdProveedor = 0;
                    int idContratos = 0;

                    if (!int.TryParse(GetSessionParameter("Contrato.IdProveedor").ToString(), out vIdProveedor))
                    {
                        toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                        return;
                    }

                    if (!int.TryParse(GetSessionParameter("Contrato.ContratosAPP").ToString(), out idContratos))
                    {
                        toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                        return;
                    }

                    ProveedoresContratos vProveedoresContratos = new ProveedoresContratos();
                    vProveedoresContratos.IdContrato = idContratos;
                    vProveedoresContratos.IdProveedores = vIdProveedor;
                    vProveedoresContratos.IdFormaPago = Convert.ToInt32(strValue);
                    vProveedoresContratos.UsuarioCrea = GetSessionUser().NombreUsuario;

                    vResultado = vContratoService.InsertarProveedoresContratos(vProveedoresContratos);
                    if (vResultado ==0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        return;
                        
                    }
                    if (vResultado == 1)
                    {
                        RemoveSessionParameter("FormaPago.IDProveedoresContratos");
                        SetSessionParameter("FormaPago.IDProveedoresContratos", vProveedoresContratos.IdProveedoresContratos);
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        return;
                    }

                    
                }

            }
            SeleccionarRegistros(strValue);
        }
    }


    private void SeleccionarRegistros(string IdFormaPago)
    {
        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";
            returnValues += "pObj[0] = '" + HttpUtility.HtmlDecode(IdFormaPago) + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "'); " +
                "</script>";

        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }
}
