using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de contrato
/// </summary>
public partial class Page_Contrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RegistrarContratos";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("Contrato.Guardado");
                RemoveSessionParameter("Contrato.Modificado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");

            if (GetSessionParameter("Contrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            if (GetSessionParameter("Contrato.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&oacute;n ha sido modificada exitosamente");
            RemoveSessionParameter("Contrato");


            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato(vIdContrato);
            hfIdContrato.Value = vContrato.IdContrato.ToString();
            txtFechaRegistro.Text = vContrato.FechaRegistro.ToShortDateString();
            txtNumeroProceso.Text = vContrato.NumeroProceso;
            txtNumeroContrato.Text = vContrato.NumeroContrato;
            ddlModalidad.SelectedValue = vContrato.IdModalidad.ToString();
            dllIdCategoriaContrato.SelectedValue = vContrato.IdCategoriaContrato.ToString();
            dllIdTipoContrato.SelectedValue = vContrato.IdTipoContrato.ToString();
            rblRequiereActa.SelectedValue = vContrato.RequiereActa.ToString();
            rblManejaAportes.SelectedValue = vContrato.ManejaAportes.ToString();
            rblManejaRecursos.SelectedValue = vContrato.ManejaRecursos.ToString();
            ddlRegimenContratacion.SelectedValue = vContrato.IdRegimenContratacion.ToString();
            //vContrato.CodigoRegional = Convert.ToInt32(txtCodigoRegional.Text);
            txtNombreSolicitante.Text = vContrato.NombreSolicitante;
            txtDependenciaSolicitante.Text = vContrato.DependenciaSolicitante;
            txtCargoSolicitante.Text = vContrato.ObjetoContrato;
            txtAlcanceObjetoContrato.Text = vContrato.AlcanceObjetoContrato;
            txtValorInicialContrato.Text = vContrato.ValorInicialContrato.ToString();
            txtValorTotalAdiciones.Text = vContrato.ValorTotalAdiciones.ToString();
            txtValorFinalContrato.Text = vContrato.ValorFinalContrato.ToString();
            txtValorAportesICBF.Text = vContrato.ValorAportesICBF.ToString();
            txtValorAportesOperador.Text = vContrato.ValorAportesOperador.ToString();
            txtValorTotalReduccion.Text = vContrato.ValorTotalReduccion.ToString();
            txtFechaAdjudicacion.Text = vContrato.FechaAdjudicacion.Value.ToShortDateString();
            txtFechaSuscripcion.Text = vContrato.FechaSuscripcion.ToShortDateString();
            if (vContrato.FechaInicioEjecucion != null)
                txtFechaInicioEjecucion.Text = vContrato.FechaInicioEjecucion.ToString();
            else
                txtFechaInicioEjecucion.Text = string.Empty;
            txtFechaInicialTerminacion.Text = vContrato.FechaInicialTerminacion.Value.ToShortDateString();
            txtFechaAnulacion.Text = vContrato.FechaAnulacion.Value.ToShortDateString();
            if (vContrato.FechaFinalizacionIniciaContrato != null)
                txtFechaFinalizacionInicial.Text = vContrato.FechaFinalizacionIniciaContrato.ToString();
            else
                txtFechaFinalizacionInicial.Text = string.Empty;
            txtPlazoInicial.Text = vContrato.PlazoInicial.ToString();
            txtFechaFinalTerminacion.Text = vContrato.FechaFinalTerminacionContrato.Value.ToShortDateString();
            txtFechaProyecradaLiquidacion.Text = vContrato.FechaProyectadaLiquidacion.ToShortDateString();
            txtProrrogas.Text = vContrato.Prorrogas.ToString();
            txtPlazoTotal.Text = vContrato.PlazoTotal.ToString();
            txtFechaFirmaActaInicio.Text = vContrato.FechaFirmaActaInicio.ToShortDateString();
            txtVigenciaFiscalInicial.Text = vContrato.VigenciaFiscalInicial.ToString();
            txtVigenciaFiscalFinal.Text = vContrato.VigenciaFiscalFinal.ToString();
            txtJustificacionAdicionSuperior.Text = vContrato.JustificacionAdicionSuperior50porc;
            txtCargoSolicitante.Text = vContrato.CargoSolicitante;
            ddlRegionalContrato.SelectedValue = vContrato.IdRegionalContrato.ToString();
            txtObjetoContrato.Text = vContrato.ObjetoContrato;
            
            txtDatosAdicionales.Text = vContrato.DatosAdicionales;
            
            ddlFormaPago.SelectedValue = vContrato.IdFormaPago.ToString();
            ddlClaseEntidad.SelectedValue = vContrato.IdTipoEntidad.ToString();
            ObtenerAuditoria(PageName, hfIdContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato(vIdContrato);
            InformacionAudioria(vContrato, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarContrato(vContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Contrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesContratos.LlenarComboLista(ddlModalidad, vContratoService.ConsultarModalidadSeleccions(null, null, true), "IdModalidad", "Nombre");
            ManejoControlesContratos.InicializarCombo(ddlModalidadAcademica);
            ManejoControlesContratos.LlenarComboLista(dllIdCategoriaContrato, vContratoService.ConsultarCategoriaContratos(null, null,true), "IdCategoriaContrato", "NombreCategoriaContrato");
            ManejoControlesContratos.LlenarComboLista(dllIdTipoContrato, vContratoService.ConsultarTipoContratos(null, null, true, null, null, null,null,null), "IdTipoContrato", "NombreTipoContrato");
            ManejoControlesContratos.InicializarCombo(ddlCodigoProfesion);
            ManejoControlesContratos.InicializarCombo(ddlNombreProfesion);
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereActa, "Activo", "Inactivo");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblManejaAportes, "Activo", "Inactivo");
            ManejoControlesContratos.LlenarComboLista(ddlRegimenContratacion, vContratoService.ConsultarRegimenContratacions(null,null, true), "IdRegimenContratacion", "NombreRegimenContratacion");
            ManejoControlesContratos.LlenarComboLista(ddlRegionalContrato, vContratoService.ConsultarRegionals(null, null), "IdRegional", "NombreRegional");
            ManejoControlesContratos.InicializarCombo(ddlUnidadEjecucion);
            ManejoControlesContratos.InicializarCombo(ddlLugarEjecucion);
            //ManejoControlesContratos.LlenarComboLista(ddlFormaPago, vContratoService.ConsultarFormaPagos(null,null, true), "IdFormapago", "NombreFormaPago");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblManjaVigenciasFuturas, "Activo", "Inactivo");
            ManejoControlesContratos.InicializarCombo(ddlEstadoContrato);
            ManejoControlesContratos.InicializarCombo(ddlClaseEntidad);
            ddlClaseEntidad.Items.Add(new ListItem("Contratista", "1"));
            ddlClaseEntidad.Items.Add(new ListItem("Consorcio", "2"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
