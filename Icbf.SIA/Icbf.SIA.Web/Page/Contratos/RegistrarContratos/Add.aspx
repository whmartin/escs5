<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contrato_Add" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

<asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlContrato">
        <table width="90%" align="center">
            <tr class="rowB">
            <td style="width: 50%">
                Fecha Registro Al Sistema *
            </td>
            <td style="width: 50%">
                N&#250;mero Proceso *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                
                <uc1:fecha ID="fechaRegistro" runat="server"/>
                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProceso" MaxLength="50" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
                N&#250;mero del Contrato/Convenio * 
            </td>
            <td>
                Modalidad Selecci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvddlModalidad" ControlToValidate="ddlModalidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvddlModalidad" ControlToValidate="ddlModalidad"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidad"  ></asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Adjudicaci&#243;n del Proceso *</td>
            <td>
                Modalidad Acad&#233;mica *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaAdjudicacion" runat="server" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidadAcademica"  ></asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categor&#237;a Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvdllIdCategoriaContrato" ControlToValidate="dllIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvdllIdCategoriaContrato" ControlToValidate="dllIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Tipo de Contrato/Convenio *
                <asp:RequiredFieldValidator runat="server" ID="rfvdllIdTipoContrato" ControlToValidate="dllIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvdllIdTipoContrato" ControlToValidate="dllIdTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="dllIdCategoriaContrato"  ></asp:DropDownList>                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="dllIdTipoContrato"  ></asp:DropDownList>               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                C&#243;digo Profesi&#243;n *
            </td>
            <td>
                Nombre Profesi&#243;n *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlCodigoProfesion"  ></asp:DropDownList>                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlNombreProfesion"  ></asp:DropDownList>               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Maneja Recursos *</td>
                <td>
                    &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblManejaRecursos" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="rblManejaRecursos_SelectedIndexChanged" 
                    RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>Requiere Acta de Inicio *</td>
            <td>
                Fecha Firma Acta Inicio * </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblRequiereActa" 
                    RepeatDirection="Horizontal" AutoPostBack="True" 
                    onselectedindexchanged="rblRequiereActa_SelectedIndexChanged"></asp:RadioButtonList>
            </td>
            <td>
                <uc1:fecha ID="fechaFirmaActaInicio" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Maneja Aportes *
            </td>
            <td>
                R&#233;gimen Contrataci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvddlRegimenContratacion" ControlToValidate="ddlRegimenContratacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvddlRegimenContratacion" ControlToValidate="ddlRegimenContratacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblManejaAportes" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList ID="ddlRegimenContratacion" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional del Contrato/Convenio *
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="ddlRegionalContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToValidate="ddlRegionalContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" Operator="NotEqual" 
                    SetFocusOnError="True" ValidationGroup="btnGuardar" ValueToCompare="-1"></asp:CompareValidator>
                </td>
            <td>Nombre Solicitante *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlRegionalContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreSolicitante" Height="50px" 
                    MaxLength="50" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);">
                    </asp:TextBox>
           <Ajax:FilteredTextBoxExtender ID="FtNombreSolicitante" runat="server" TargetControlID="txtNombreSolicitante"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
        </tr>
        <tr class="rowB">
            <td>
                Dependencia Solicitante *</td>
            <td>
                Cargo Solicitante *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDependenciaSolicitante" Height="50px" 
                    MaxLength="50" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);">
                    </asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCargoSolicitante" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto del Contrato/Convenio *
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtObjetoContrato" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" Height="50px" 
                    MaxLength="300" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,300);" onKeyUp="limitText(this,300);">
                    </asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftObjetoContrato" runat="server" TargetControlID="txtObjetoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                Alcance del Objeto del Contrato/Convenio *</td>
            <td>
                Valor Inicial del Contrato/Convenio *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorInicialContrato" ControlToValidate="txtValorInicialContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>    
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAlcanceObjetoContrato" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px" 
                    onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);" 
                    Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftAlcanceObjetoContrato" runat="server" TargetControlID="txtAlcanceObjetoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorInicialContrato" MaxLength="50" 
                    Width="200px" Enabled="False">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Adiciones *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorTotalAdiciones" ControlToValidate="txtValorTotalAdiciones"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>   
            </td>
            <td>
                Valor Final del Contrato/Convenio *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorFinalContrato" ControlToValidate="txtValorFinalContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalAdiciones" MaxLength="50" 
                    Width="200px" Enabled="False">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fttxtValorTotalAdiciones" runat="server" TargetControlID="txtValorTotalAdiciones"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorFinalContrato" MaxLength="50" 
                    Width="200px"  onfocus=blur(); class="enable">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorFinalContrato" runat="server" TargetControlID="txtValorFinalContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Aportes ICBF *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorAportesICBF" ControlToValidate="txtValorAportesICBF"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
                </td>
            <td>
                Valor Aportes Operador *
                 <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorAportesOperador" ControlToValidate="txtValorAportesOperador"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesICBF" MaxLength="50" 
                    Width="200px"  onfocus=blur(); class="enable">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesICBF" runat="server" TargetControlID="txtValorAportesICBF"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesOperador" MaxLength="50" 
                    Width="200px"  onfocus=blur(); class="enable">0</asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAportesOperador" runat="server" TargetControlID="txtValorAportesOperador"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Justificar Adici&#243;n Superior al 50% 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicionSuperior" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Reducci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtValorTotalReduccion" ControlToValidate="txtValorTotalReduccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
            </td>
            <td>Fecha Suscripci&#243;n *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalReduccion" MaxLength="50" 
                    Width="200px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorTotalReduccion" runat="server" TargetControlID="txtValorTotalReduccion"
                    FilterType="Custom, Numbers" ValidChars="0123456789,." />
            </td>
            <td>
                <uc1:fecha ID="fechaSuscripcion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha de Inicio de Ejecuci&#243;n del Contrato/Convenio *
            </td>
            <td>
                Fecha de Finalización Inicial del Contrato/Convenio *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:UpdatePanel ID="pnlFechaInicioEjecucion" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <uc1:fecha ID="fechaInicioEjecucion" runat="server" />        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <uc1:fecha ID="fechaFinalizacionInicial" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicial Terminaci&#243;n *</td>
            <td>Fecha Final Terminaci&#243;n *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaInicialTerminacion" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="fechaFinalTerminacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Plazo Inicial *<asp:RequiredFieldValidator ID="rfvtxtPlazoInicial0" runat="server" 
                    ControlToValidate="txtPlazoInicial" Display="Dynamic" 
                    ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="true" 
                    ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
             </td>
            <td>Fecha Proyectada Liquidaci&#243;n *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtPlazoInicial" runat="server" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtPlazoInicial" 
                    runat="server" FilterType="Numbers" TargetControlID="txtPlazoInicial" 
                    ValidChars="" />
            </td>
            <td>
                <uc1:fecha ID="fechaProyectadaLiquidacion" runat="server" />
            </td>
        </tr>
         <tr class="rowB">
            <td>
                Fecha de Anulaci&#243;n *</td>
            <td>Prorrogas
            <asp:RequiredFieldValidator runat="server" ID="rfvtxtProrrogas" ControlToValidate="txtProrrogas"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="fechaAnulacion" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtProrrogas" runat="server" MaxLength="10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftetxtProrrogas" runat="server" TargetControlID="txtProrrogas"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Plazo Total de Ejecuci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtPlazoTotal" ControlToValidate="txtPlazoTotal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPlazoTotal" MaxLength="3"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPlazoTotal" runat="server" TargetControlID="txtPlazoTotal"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia Fiscal Inicial del Contrato/Convenio *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtVigenciaFiscalInicial" ControlToValidate="txtVigenciaFiscalInicial"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
             </td>
            <td>
                Vigencia Fiscal Final del Contrato/Convenio *
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtVigenciaFiscalFinal" ControlToValidate="txtVigenciaFiscalFinal"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator> 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtVigenciaFiscalInicial" Width="80px" MaxLength="4"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftVigenciaFiscalInicial" runat="server" TargetControlID="txtVigenciaFiscalInicial"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVigenciaFiscalFinal" Width="80px" MaxLength="4"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftVigenciaFiscalFinal" runat="server" TargetControlID="txtVigenciaFiscalFinal"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Maneja Vigencias Futuras *
            </td>
            <td>
                Unidad Ejecuci&#243;n *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblManjaVigenciasFuturas" runat="server">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList ID="ddlUnidadEjecucion" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Lugar Ejecuci&#243;n *
            </td>
            <td>
                Datos Adicionales lugar Ejecuci&#243;n *</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlLugarEjecucion" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDatosAdicionales" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDatosAdicionales" runat="server" TargetControlID="txtDatosAdicionales"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado del Contrato/Convenio </td>
            <td>Forma Pago *
             <asp:RequiredFieldValidator runat="server" ID="rfvddlFormaPago" ControlToValidate="ddlFormaPago"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvddlFormaPago" ControlToValidate="ddlFormaPago"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlEstadoContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlFormaPago" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Clase de Entidad *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlClaseEntidad" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
