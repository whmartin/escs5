using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de contratos
/// </summary>
public partial class Page_Contrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/RegistrarContratos";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Contrato vContrato = new Contrato();
            vContrato.FechaRegistro = fechaRegistro.Date;
            vContrato.NumeroProceso = txtNumeroProceso.Text;
            vContrato.NumeroContrato = txtNumeroContrato.Text;
            vContrato.IdModalidad = Convert.ToInt32(ddlModalidad.SelectedValue);
            vContrato.IdCategoriaContrato = Convert.ToInt32(dllIdCategoriaContrato.SelectedValue);
            vContrato.IdTipoContrato = Convert.ToInt32(dllIdTipoContrato.SelectedValue);
            vContrato.RequiereActa = Convert.ToBoolean(rblRequiereActa.SelectedValue);
            vContrato.ManejaAportes = Convert.ToBoolean(rblManejaAportes.SelectedValue);
            vContrato.ManejaRecursos = Convert.ToBoolean(rblManejaRecursos.SelectedValue);
            vContrato.IdRegimenContratacion = Convert.ToInt32(ddlRegimenContratacion.SelectedValue);
            vContrato.IdRegionalContrato = Convert.ToInt32(ddlRegionalContrato.SelectedValue);
            vContrato.NombreSolicitante = Convert.ToString(txtNombreSolicitante.Text);
            vContrato.DependenciaSolicitante = Convert.ToString(txtDependenciaSolicitante.Text);
            vContrato.ObjetoContrato = Convert.ToString(txtObjetoContrato.Text);
            vContrato.AlcanceObjetoContrato = Convert.ToString(txtAlcanceObjetoContrato.Text);
            vContrato.ValorInicialContrato = Convert.ToDecimal(txtValorInicialContrato.Text);
            vContrato.ValorTotalAdiciones = Convert.ToDecimal(txtValorTotalAdiciones.Text);
            vContrato.ValorFinalContrato = Convert.ToDecimal(txtValorFinalContrato.Text);
            vContrato.ValorAportesICBF = Convert.ToDecimal(txtValorAportesICBF.Text);
            vContrato.ValorAportesOperador = Convert.ToDecimal(txtValorAportesOperador.Text);
            vContrato.ValorTotalReduccion = Convert.ToDecimal(txtValorTotalReduccion.Text);
            vContrato.FechaAdjudicacion = fechaAdjudicacion.Date;
            vContrato.FechaSuscripcion = fechaSuscripcion.Date;
            vContrato.FechaInicioEjecucion = fechaInicioEjecucion.Date;
            vContrato.FechaInicialTerminacion = fechaInicialTerminacion.Date;
            vContrato.FechaFinalTerminacionContrato = fechaFinalTerminacion.Date;
            vContrato.FechaAnulacion = fechaAnulacion.Date;
            vContrato.FechaFinalizacionIniciaContrato = fechaFinalizacionInicial.Date;
            vContrato.PlazoInicial = Convert.ToInt32(txtPlazoInicial.Text);
            vContrato.FechaFinalTerminacionContrato = fechaFinalTerminacion.Date;
            vContrato.FechaProyectadaLiquidacion = fechaProyectadaLiquidacion.Date;
            vContrato.Prorrogas = Convert.ToInt32(txtProrrogas.Text);
            vContrato.PlazoTotal = Convert.ToInt32(txtPlazoTotal.Text);
            vContrato.FechaFirmaActaInicio = fechaFirmaActaInicio.Date;
            vContrato.VigenciaFiscalInicial = Convert.ToInt32(txtVigenciaFiscalInicial.Text);
            vContrato.VigenciaFiscalFinal = Convert.ToInt32(txtVigenciaFiscalFinal.Text);
            vContrato.JustificacionAdicionSuperior50porc = txtJustificacionAdicionSuperior.Text;
            vContrato.CargoSolicitante = txtCargoSolicitante.Text;
            //vContrato.IdUnidadEjecucion = Convert.ToInt32(ddlUnidadEjecucion.SelectedValue);
            //vContrato.IdLugarEjecucion = Convert.ToInt32(ddlLugarEjecucion.SelectedValue);
            vContrato.DatosAdicionales = Convert.ToString(txtDatosAdicionales.Text);
            vContrato.IdEstadoContrato = Convert.ToInt32(1);
            //vContrato.IdTipoDocumentoContratista = Convert.ToString(txtIdTipoDocumentoContratista.Text);
            //vContrato.IdentificacionContratista = Convert.ToString(txtIdentificacionContratista.Text);
            //vContrato.NombreContratista = Convert.ToString(txtNombreContratista.Text);
            vContrato.IdFormaPago = Convert.ToInt32(ddlFormaPago.SelectedValue);
            vContrato.IdTipoEntidad = Convert.ToInt32(ddlClaseEntidad.SelectedValue);
            vContrato.ClaseContrato = "N";
            // validamos si el contrato requiere acta de inicio
            if (this.rblRequiereActa.SelectedValue == "True")
            {
                vContrato.FechaInicioEjecucion = this.fechaFirmaActaInicio.Date;
            }
            // se adicionan 4 meses a la fecha final terminacion para calcular la fecha proyectada de liquidacion
            vContrato.FechaProyectadaLiquidacion = vContrato.FechaFinalTerminacionContrato.Value.AddMonths(4);

            if (Request.QueryString["oP"] == "E")
            {
            vContrato.IdContrato = Convert.ToInt32(hfIdContrato.Value);
                vContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarContrato(vContrato);
            }
            else
            {
                vContrato.NumeroContrato = vContratoService.GenerarNumeroContrato(new SecuenciaNumeroContrato() { CodigoRegional = Convert.ToInt32(ddlRegionalContrato.SelectedValue), AnoVigencia = Convert.ToInt32(this.txtVigenciaFiscalInicial.Text), UsuarioCrea = GetSessionUser().NombreUsuario });
                var modalidad = vContratoService.ConsultarModalidadSeleccion(Convert.ToInt32(this.ddlModalidad.SelectedValue));
                //vContrato.NumeroProceso = vContratoService.GenerarNumeroProceso(new SecuenciaNumeroProceso() { CodigoRegional = Convert.ToInt32(ddlRegionalContrato.SelectedValue), AnoVigencia = Convert.ToInt32(this.txtVigenciaFiscalInicial.Text), Sigla = modalidad.Sigla, UsuarioCrea = GetSessionUser().NombreUsuario });
                vContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarContrato(vContrato);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Contrato.IdContrato", vContrato.IdContrato);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("Contrato.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("Contrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
           
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);

            toolBar.EstablecerTitulos("Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.Id");

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato(vIdContrato);
            hfIdContrato.Value = vContrato.IdContrato.ToString();
            fechaRegistro.Date = vContrato.FechaRegistro;
            txtNumeroProceso.Text = vContrato.NumeroProceso;
            txtNumeroContrato.Text = vContrato.NumeroContrato.ToString();
            ddlModalidad.SelectedValue = vContrato.IdModalidad.ToString();
            ddlRegimenContratacion.SelectedValue = vContrato.IdRegimenContratacion.ToString();
            dllIdCategoriaContrato.SelectedValue = vContrato.IdCategoriaContrato.ToString();
            dllIdTipoContrato.SelectedValue = vContrato.IdTipoContrato.ToString();
            rblRequiereActa.SelectedValue = vContrato.RequiereActa.ToString();
            rblManejaAportes.SelectedValue = vContrato.ManejaAportes.ToString();
            rblManejaRecursos.SelectedValue = vContrato.ManejaRecursos.ToString();
            txtNombreSolicitante.Text = vContrato.NombreSolicitante;
            txtDependenciaSolicitante.Text = vContrato.DependenciaSolicitante;
            txtCargoSolicitante.Text = vContrato.ObjetoContrato;
            txtAlcanceObjetoContrato.Text = vContrato.AlcanceObjetoContrato;
            txtValorInicialContrato.Text = vContrato.ValorInicialContrato.ToString();
            txtValorTotalAdiciones.Text = vContrato.ValorTotalAdiciones.ToString();
            txtValorFinalContrato.Text = vContrato.ValorFinalContrato.ToString();
            txtValorAportesICBF.Text = vContrato.ValorAportesICBF.ToString();
            txtValorAportesOperador.Text = vContrato.ValorAportesOperador.ToString();
            txtValorTotalReduccion.Text = vContrato.ValorTotalReduccion.ToString();
            fechaAdjudicacion.Date = vContrato.FechaAdjudicacion.Value;
            fechaSuscripcion.Date = vContrato.FechaSuscripcion;
            if (vContrato.FechaInicioEjecucion != null)
                fechaInicioEjecucion.Date = (DateTime)vContrato.FechaInicioEjecucion;
            else
                fechaInicioEjecucion.InitNull = true;
            if (vContrato.FechaFinalizacionIniciaContrato != null)
                fechaFinalizacionInicial.Date = (DateTime)vContrato.FechaFinalizacionIniciaContrato;
            else
                fechaFinalizacionInicial.InitNull = true;
            txtPlazoInicial.Text = vContrato.PlazoInicial.ToString();
            if (vContrato.FechaFinalTerminacionContrato != null)
                fechaFinalTerminacion.Date = vContrato.FechaFinalTerminacionContrato.Value;
            fechaProyectadaLiquidacion.Date = vContrato.FechaProyectadaLiquidacion;
            txtProrrogas.Text = vContrato.Prorrogas.ToString();
            txtPlazoTotal.Text = vContrato.PlazoTotal.ToString();
            fechaFirmaActaInicio.Date = vContrato.FechaFirmaActaInicio;
            txtVigenciaFiscalInicial.Text = vContrato.VigenciaFiscalInicial.ToString();
            txtVigenciaFiscalFinal.Text = vContrato.VigenciaFiscalFinal.ToString();
            txtJustificacionAdicionSuperior.Text = vContrato.JustificacionAdicionSuperior50porc;
            
            txtDatosAdicionales.Text = vContrato.DatosAdicionales;
            ddlEstadoContrato.SelectedValue = vContrato.IdEstadoContrato.ToString();
            fechaInicialTerminacion.Date = vContrato.FechaInicialTerminacion.Value;
            fechaAnulacion.Date = vContrato.FechaAnulacion.Value;
            ddlRegionalContrato.SelectedValue = vContrato.IdRegionalContrato.ToString();
            if (rblManejaRecursos.SelectedValue == "True")
                this.txtAlcanceObjetoContrato.Enabled = false;
            if (rblRequiereActa.SelectedValue == "False")
            {
                this.fechaInicioEjecucion.Enabled = true;
                this.fechaInicioEjecucion.HabilitarObligatoriedad(true);
                this.fechaFirmaActaInicio.HabilitarObligatoriedad(false);
                this.fechaFirmaActaInicio.Enabled = false;
            }
            else
            {
                this.fechaFirmaActaInicio.Enabled = true;
                this.fechaFirmaActaInicio.HabilitarObligatoriedad(true);
                this.fechaInicioEjecucion.Enabled = false;
                this.fechaInicioEjecucion.HabilitarObligatoriedad(false);
            }
            
            ddlFormaPago.SelectedValue = vContrato.IdFormaPago.ToString();
            ddlClaseEntidad.SelectedValue = vContrato.IdTipoEntidad.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContrato.UsuarioCrea, vContrato.FechaCrea, vContrato.UsuarioModifica, vContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            fechaRegistro.Date = DateTime.Now;
            
            fechaRegistro.HabilitarObligatoriedad(true);
            fechaAdjudicacion.HabilitarObligatoriedad(true);
            fechaSuscripcion.HabilitarObligatoriedad(true);
            fechaInicioEjecucion.HabilitarObligatoriedad(false);
            fechaInicioEjecucion.Enabled = false;
            fechaFinalizacionInicial.HabilitarObligatoriedad(true);
            fechaFinalTerminacion.HabilitarObligatoriedad(true);
            fechaAnulacion.HabilitarObligatoriedad(true);
            fechaFirmaActaInicio.HabilitarObligatoriedad(true);
            fechaProyectadaLiquidacion.HabilitarObligatoriedad(true);
            fechaInicialTerminacion.HabilitarObligatoriedad(true);
            ManejoControlesContratos.LlenarComboLista(ddlModalidad, vContratoService.ConsultarModalidadSeleccions(null, null, true), "IdModalidad", "Nombre");
            ManejoControlesContratos.InicializarCombo(ddlModalidadAcademica);
            ManejoControlesContratos.LlenarComboLista(dllIdCategoriaContrato, vContratoService.ConsultarCategoriaContratos(null,null, true), "IdCategoriaContrato", "NombreCategoriaContrato");
            ManejoControlesContratos.LlenarComboLista(dllIdTipoContrato, vContratoService.ConsultarTipoContratos(null, null, true, null, null, null, null, null), "IdTipoContrato", "NombreTipoContrato");
            ManejoControlesContratos.InicializarCombo(ddlCodigoProfesion);
            ManejoControlesContratos.InicializarCombo(ddlNombreProfesion);
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereActa, "Si", "No");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblManejaAportes, "Si", "No");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblManejaRecursos, "Si", "No");
            ManejoControlesContratos.LlenarComboLista(ddlRegimenContratacion, vContratoService.ConsultarRegimenContratacions(null,null, true), "IdRegimenContratacion", "NombreRegimenContratacion");
            ManejoControlesContratos.LlenarComboLista(ddlRegionalContrato, vContratoService.ConsultarRegionals(null, null), "IdRegional", "NombreRegional");
            ManejoControlesContratos.InicializarCombo(ddlUnidadEjecucion);
            ManejoControlesContratos.InicializarCombo(ddlLugarEjecucion);
            //ManejoControlesContratos.LlenarComboLista(ddlFormaPago, vContratoService.ConsultarFormaPagos(null,null, true), "IdFormapago", "NombreFormaPago");
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblManjaVigenciasFuturas, "Si", "No");
            ManejoControlesContratos.InicializarCombo(ddlEstadoContrato);
            ManejoControlesContratos.InicializarCombo(ddlClaseEntidad);

            ddlClaseEntidad.Items.Add(new ListItem("Contratista","1"));
            ddlClaseEntidad.Items.Add(new ListItem("Consorcio", "2"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void rblRequiereActa_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblRequiereActa.SelectedValue == "False")
        {
            this.fechaInicioEjecucion.Enabled = true;
            this.fechaInicioEjecucion.HabilitarObligatoriedad(true);
            this.fechaFirmaActaInicio.HabilitarObligatoriedad(false);
            this.fechaFirmaActaInicio.Enabled = false;
        }
        else 
        {
            this.fechaFirmaActaInicio.Enabled = true;
            this.fechaFirmaActaInicio.HabilitarObligatoriedad(true);
            this.fechaInicioEjecucion.Enabled = false;
            this.fechaInicioEjecucion.HabilitarObligatoriedad(false);
        }
        pnlFechaInicioEjecucion.Update();
    }
    protected void rblManejaRecursos_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.rblManejaRecursos.SelectedValue == "True")
        {
            this.txtAlcanceObjetoContrato.Text = string.Empty;
            this.txtAlcanceObjetoContrato.Enabled = false;
        }
        else
        {
            this.txtAlcanceObjetoContrato.Enabled = true;
        }
    }
}
