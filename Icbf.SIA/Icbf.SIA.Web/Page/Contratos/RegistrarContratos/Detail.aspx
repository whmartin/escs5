<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Contrato_Detail" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContrato" runat="server" />
     <table width="90%" align="center">
            <tr class="rowB">
            <td style="width: 50%">
                Fecha Registro
            </td>
            <td style="width: 50%">
                N&#250;mero Proceso
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaRegistro" runat="server" Enabled="false"></asp:TextBox>    
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroProceso" Enabled="false" 
                    MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        
        <tr class="rowB">
            <td>
                N&#250;mero Contrato
                </td>
            <td>
                Modalidad Selecci&oacute;n</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" Enabled="false" 
                    MaxLength="25"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidad"  Enabled="false"></asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Adjudicaci&oacute;n</td>
            <td>
                Modalidad Acad&#233;mica
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaAdjudicacion" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlModalidadAcademica"  Enabled="false"></asp:DropDownList>                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categor&#237;a Contrato
                </td>
            <td>
                Tipo Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="dllIdCategoriaContrato"  Enabled="false"></asp:DropDownList>                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="dllIdTipoContrato"  Enabled="false"></asp:DropDownList>               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                C&oacute;digo Profesi&oacute;n
            </td>
            <td>
                Nombre Profesi&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlCodigoProfesion"  Enabled="false"></asp:DropDownList>                
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlNombreProfesion" Enabled="false" ></asp:DropDownList>               
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Maneja Recursos</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblManejaRecursos" 
                    RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>Requiere Acta</td>
            <td>
                Fecha Firma Acta Inicio</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblRequiereActa" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox ID="txtFechaFirmaActaInicio" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Maneja Aportes
            </td>
            <td>
                R&#233;gimen Contrataci&oacute;n
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblManejaAportes" RepeatDirection="Horizontal" Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList ID="ddlRegimenContratacion" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional
                </td>
            <td>Nombre Solicitante</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlRegionalContrato" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreSolicitante" Enabled="false" 
                    Height="50px" MaxLength="50" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Dependencia Solicitante</td>
            <td>
                Cargo Solicitante</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDependenciaSolicitante" Enabled="false" 
                    Height="50px" MaxLength="50" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCargoSolicitante" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto Contrato
            </td>
            <td></td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtObjetoContrato" Enabled="false" 
                    Height="50px" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
       <tr class="rowB">
            <td>
                Alcance Objeto Contrato</td>
            <td>
                Valor Inicial Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAlcanceObjetoContrato" Enabled="false" 
                    Height="50px" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorInicialContrato" Enabled="false" 
                    Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Adiciones
                </td>
            <td>
                Valor Final Contrato
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalAdiciones" Enabled="false" 
                    Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorFinalContrato" Enabled="false" 
                    Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Aportes ICBF
                </td>
            <td>
                Valor Aportes Operador 
                 </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesICBF" Enabled="false" 
                    Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorAportesOperador" Enabled="false" 
                    Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Justificar Adici&oacute;n Superior al 50%
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicionSuperior" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Total Reducci&oacute;n
                </td>
            <td>Fecha Suscripci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorTotalReduccion" Enabled="false" 
                    Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtFechaSuscripcion" runat="server" Enabled="false" 
                    Width="200px"></asp:TextBox></td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicio Ejecuci&oacute;n
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaInicioEjecucion" runat="server" Enabled="false"></asp:TextBox>
                </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Finalizaci&oacute;n Inicial
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaFinalizacionInicial" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                &nbsp;Fecha Inicial Terminaci&oacute;n</td>
            <td>Fecha Final Terminaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaInicialTerminacion" runat="server" Enabled="false"></asp:TextBox>
                </td>
            <td>
                <asp:TextBox ID="txtFechaFinalTerminacion" runat="server" Enabled="false"></asp:TextBox></td>
        </tr>
        <tr class="rowB">
            <td>
                Plazo Inicial</td>
            <td>Fecha Proyectada Liquidaci&oacute;n </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtPlazoInicial" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
               <asp:TextBox ID="txtFechaProyecradaLiquidacion" runat="server" Enabled="false"></asp:TextBox></td>
        </tr>
         <tr class="rowB">
            <td>
                Fecha de Anulaci&oacute;n</td>
            <td>Prorrogas
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtFechaAnulacion" runat="server" Enabled="false"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtProrrogas" runat="server" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Plazo Total
                </td>
            <td>
            &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPlazoTotal" Enabled="false"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia Fiscal Inicial
                </td>
            <td>
                Vigencia Fiscal Final
                </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtVigenciaFiscalInicial" Width="80px" MaxLength="4" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVigenciaFiscalFinal" Width="80px" MaxLength="4" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Maneja Vigencias Futuras
            </td>
            <td>
                Unidad Ejecuci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList ID="rblManjaVigenciasFuturas" runat="server" Enabled="false">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList ID="ddlUnidadEjecucion" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Lugar Ejecuci&oacute;n
            </td>
            <td>
                Datos Adicionales
                lugar Ejecuci&oacute;n</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlLugarEjecucion" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDatosAdicionales" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado Contrato</td>
            <td>Forma Pago
             </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlEstadoContrato" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlFormaPago" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Clase de Entidad  Clase de Entidad
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlClaseEntidad" runat="server" Enabled="false">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
