using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.IO;

/// <summary>
/// Página de consulta a través de filtros para la entidad Contratos
/// </summary>
public partial class Page_ContraloriaReporte_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ContraloriaReporte";
    SIAService vRuboService = new SIAService();
    ContratoService vContratoService = new ContratoService();
  
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {

        ddlIDRegional.SelectedIndex = 0;
        ddlVigenciaFiscalinicial.SelectedIndex = 0;
        toolBar.LipiarMensajeError();
    }

    /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            toolBar.eventoLimpiar += new ToolBarDelegate(btnLimpiar_Click);
            toolBar.MostrarBotonEditar(false);
            toolBar.MostrarBotonNuevo(false);
            toolBar.OcultarBotonBuscar(true);
            toolBar.EstablecerTitulos("Reporte de Contraloría", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnReporte_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        GenerarReporte(true);
    }

    private void GenerarReporte(bool pPrevisualizar = false)
    {
        try
        {
            var reportePath = Server.MapPath("~/General/General/Report/PlantillaSIRECI.xlsx");
            int vigencia = int.Parse(ddlVigenciaFiscalinicial.SelectedItem.Text);
            int periodo = int.Parse(ddlPeriodo.SelectedItem.Value);

            string regional = string.Empty;

            foreach (ListItem item in ddlIDRegional.Items)
            {
                if (item.Selected)
                    regional += item.Value+",";
            }

            var result = vContratoService.GenerarReporteContraloria(vigencia,periodo,regional,reportePath);

            if (result != string.Empty)
            {
                string archivoLocal = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"], result);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(archivoLocal);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + result);
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(archivoLocal);
                Response.End();
                toolBar.MostrarMensajeGuardado("Se genero el reporte correctamente");
            }
            else
            {
                toolBar.MostrarMensajeError("Los parametros seleccionados no generarón información, Por favor verifique.");
            }
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaRegional();

            var vigencia = vRuboService.ConsultarVigencias(true);

            rfvVigencia.InitialValue = "-1";

            var miVigencia = vigencia.Where(e1 => e1.AcnoVigencia >= 2016);
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, miVigencia, "IdVigencia", "AcnoVigencia");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
            }

        }

    }

}
