<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ContraloriaReporte_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        $(document).ready(function () {

            ocultaImagenLoading();
        });



        $(function () {
            $('[id*=ddlIDRegional]').multiselect({
                includeSelectAllOption: true, maxHeight: 200, nonSelectedText: "Seleccione", numberDisplayed: 1,
                nSelectedText: 'Seleccionado(s)',
            });
        });



    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Periodo * <asp:RequiredFieldValidator ID="rfvTipoBusqueda" runat="server" ControlToValidate="ddlPeriodo" InitialValue="-1" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Vigencia Fiscal Inicial * <asp:RequiredFieldValidator ID="rfvVigencia" runat="server" ControlToValidate="ddlVigenciaFiscalinicial"  Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
        </tr>
                <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList ID="ddlPeriodo" runat="server" >
                    <asp:ListItem Text="Seleccione" Value="-1" />
                    <asp:ListItem Text="PRIMERO" Value="1"/>
                    <asp:ListItem Text="SEGUNDO" Value="2"/>
                    <asp:ListItem Text="TERCERO" Value="3"/>
                    <asp:ListItem Text="CUARTO" Value="4"/>
                </asp:DropDownList>
                    </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlVigenciaFiscalinicial" runat="server" >
                </asp:DropDownList>
                    </td>
        </tr>

        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio *
                <asp:RequiredFieldValidator ID="rfvTipoBusqueda0" runat="server" ControlToValidate="ddlIDRegional" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="" SetFocusOnError="True" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:ListBox ID="ddlIDRegional" SelectionMode="Multiple" runat="server" />
               <%-- <asp:DropDownList ID="ddlIDRegional" runat="server">
                </asp:DropDownList>--%>
            </td>
            <td class="Cell">
                &nbsp;</td>
        </tr>
      
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    </asp:Content>

