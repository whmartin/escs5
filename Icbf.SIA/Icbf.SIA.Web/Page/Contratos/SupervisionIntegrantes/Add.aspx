<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SupervisionGruposApoyoIntegrantes_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/SupervisionContratos-ConfirmaGuardar.js" type="text/javascript"></script>
    <asp:HiddenField ID="hfIdIntegranteGrupo" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="hfIdGrupo" runat="server" ClientIDMode="Static"/>
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre del Grupo *
                <asp:RequiredFieldValidator runat="server" ID="rfvGrupo" ControlToValidate="txtGrupo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Dirección *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccion" ControlToValidate="ddlIdDireccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtGrupo" Width="80%" ClientIDMode="Static" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftGrupo" runat="server" TargetControlID="txtGrupo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccion" ClientIDMode="Static" Width="80%">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Numero de documento *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdentificacion" ControlToValidate="txtIdentificacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Nombre Integrante *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombres" ControlToValidate="txtNombres"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdentificacion" ClientIDMode="Static" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdentificacion" runat="server" TargetControlID="txtIdentificacion"
                    FilterType="Numbers" ValidChars="" />
                <asp:Image ID="imgDocumentoIdenIntegrante" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="consultaTerceros()" Style="cursor: hand" ToolTip="Buscar" Visible="True" />
                <asp:HiddenField runat="server" ID="hfDocumentoIdenIntegrante" ClientIDMode="Static">
                </asp:HiddenField>
                <asp:HiddenField runat="server" ID="hfNombreIntegrante" ClientIDMode="Static"></asp:HiddenField>
                <asp:HiddenField runat="server" ID="hfApellidoIntegrante" ClientIDMode="Static">
                </asp:HiddenField>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombres" ClientIDMode="Static" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombres" runat="server" TargetControlID="txtNombres"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0.123456789" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Apellidos Integrante *
                <asp:RequiredFieldValidator runat="server" ID="rfvApellidos" ControlToValidate="txtApellidos"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtApellidos" ClientIDMode="Static" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftApellidos" runat="server" TargetControlID="txtApellidos"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" ClientIDMode="Static">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <div id="dvBloqueo" style="position: absolute; z-index: 1; width: 100%; height: 100%;
        background-color: #000000; top: 0px; left: 0px; display: none; opacity: 0.25;
        filter: alpha(opacity=25);">
    </div>
    <div id="dvConfirmacion" class="modalDialog">
        <table style="text-align: center; padding: 9px">
            <tr>
                <td colspan="2">
                    ¿Está seguro de guardar el Integrante? Recuerde que después de guardado no puede
                    ser modificada toda la información
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnAceptar" type="button" value="Aceptar" onclick="guardarIntegrante();" />
                </td>
                <td>
                    <input id="btnCancelar" type="button" value="Cancelar" onclick="ocultarDiv();" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dvConsultaIntegrantes" style="background-color:#fffeff; display:none;z-index:10px;position:absolute; width:600px;height:500px; border:1px solid red;">
    </div>
</asp:Content>
