using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionGruposApoyoIntegrantes_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionIntegrantes";
    SupervisionService vSupervisionService = new SupervisionService();
    System.Drawing.Color vColorDesactivado = System.Drawing.Color.FromArgb(235, 235, 228);

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            String vGrupo = null;
            int? vIdDireccion = null;
            int? vIdentificacion = null;
            String vNombres = null;
            String vApellidos = null;
            int? vEstado = null;
            if (txtGrupo.Text != "")
            {
                vGrupo = Convert.ToString(txtGrupo.Text);
            }
            if (ddlIdDireccion.SelectedValue != "-1")
            {
                vIdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            }
            if (txtIdentificacion.Text != "")
            {
                vIdentificacion = Convert.ToInt32(txtIdentificacion.Text);
            }
            if (txtNombres.Text != "")
            {
                vNombres = Convert.ToString(txtNombres.Text);
            }
            if (txtApellidos.Text != "")
            {
                vApellidos = Convert.ToString(txtApellidos.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            gvSupervisionGruposApoyoIntegrantes.DataSource = vSupervisionService.ConsultarSupervisionGruposApoyoIntegrantess(null, null, vIdentificacion, vNombres, vApellidos, vEstado);
            gvSupervisionGruposApoyoIntegrantes.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionGruposApoyoIntegrantes.PageSize = PageSize();
            gvSupervisionGruposApoyoIntegrantes.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Integrantes", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionGruposApoyoIntegrantes.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo", strValue);
            SetSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionGruposApoyoIntegrantes_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionGruposApoyoIntegrantes.SelectedRow);
    }
    
    protected void gvSupervisionGruposApoyoIntegrantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionGruposApoyoIntegrantes.PageIndex = e.NewPageIndex;
        Buscar();
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionGruposApoyoIntegrantes.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionGruposApoyoIntegrantes.Eliminado");

            ddlIdDireccion.BackColor = vColorDesactivado;

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);
            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            ddlIdDireccion.SelectedValue = GruposSupervision[2];
            ddlIdDireccion.Enabled = false;
            txtGrupo.Enabled = false;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
