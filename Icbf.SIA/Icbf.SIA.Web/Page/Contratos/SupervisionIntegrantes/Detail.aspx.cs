using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionGruposApoyoIntegrantes_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionIntegrantes";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo", hfIdIntegranteGrupo.Value);
        SetSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo", hfIdGrupo.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdIntegranteGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo"));
            RemoveSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo");
            int vIdGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo"));
            RemoveSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo");

            if (GetSessionParameter("SupervisionGruposApoyoIntegrantes.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionGruposApoyoIntegrantes.Guardado");


            SupervisionGruposApoyoIntegrantes vSupervisionGruposApoyoIntegrantes = new SupervisionGruposApoyoIntegrantes();
            vSupervisionGruposApoyoIntegrantes = vSupervisionService.ConsultarSupervisionGruposApoyoIntegrantes(vIdIntegranteGrupo,vIdGrupo);
            hfIdIntegranteGrupo.Value = vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo.ToString();
            hfIdGrupo.Value = vSupervisionGruposApoyoIntegrantes.IdGrupo.ToString();
            txtIdentificacion.Text = vSupervisionGruposApoyoIntegrantes.Identificacion.ToString();
            txtNombres.Text = vSupervisionGruposApoyoIntegrantes.Nombres;
            rblEstado.SelectedValue = vSupervisionGruposApoyoIntegrantes.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdIntegranteGrupo.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionGruposApoyoIntegrantes.UsuarioCrea, vSupervisionGruposApoyoIntegrantes.FechaCrea, vSupervisionGruposApoyoIntegrantes.UsuarioModifica, vSupervisionGruposApoyoIntegrantes.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdIntegranteGrupo = Convert.ToInt32(hfIdIntegranteGrupo.Value);
            int vIdGrupo = Convert.ToInt32(hfIdGrupo.Value);

            SupervisionGruposApoyoIntegrantes vSupervisionGruposApoyoIntegrantes = new SupervisionGruposApoyoIntegrantes();
            vSupervisionGruposApoyoIntegrantes = vSupervisionService.ConsultarSupervisionGruposApoyoIntegrantes(vIdIntegranteGrupo,vIdGrupo);
            InformacionAudioria(vSupervisionGruposApoyoIntegrantes, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionGruposApoyoIntegrantes(vSupervisionGruposApoyoIntegrantes);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionGruposApoyoIntegrantes.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Integrantes", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            hfIdGrupo.Value = GruposSupervision[1];


            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarTipoDocumento(ddlTipoDoc, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
