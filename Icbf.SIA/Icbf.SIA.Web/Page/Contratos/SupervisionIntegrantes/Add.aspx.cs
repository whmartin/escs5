using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionGruposApoyoIntegrantes_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionIntegrantes";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }



    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Integrantes", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdIntegranteGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo"));
            RemoveSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo");
            int vIdGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo"));
            RemoveSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo");

            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            hfIdGrupo.Value = GruposSupervision[1];
            ddlIdDireccion.SelectedValue = GruposSupervision[2];
            ddlIdDireccion.Enabled = false;
            txtGrupo.Enabled = false;

            SupervisionGruposApoyoIntegrantes vSupervisionGruposApoyoIntegrantes = new SupervisionGruposApoyoIntegrantes();
            vSupervisionGruposApoyoIntegrantes = vSupervisionService.ConsultarSupervisionGruposApoyoIntegrantes(vIdIntegranteGrupo, vIdGrupo);
            hfIdIntegranteGrupo.Value = vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo.ToString();
            hfIdGrupo.Value = vSupervisionGruposApoyoIntegrantes.IdGrupo.ToString();
            txtIdentificacion.Text = vSupervisionGruposApoyoIntegrantes.Identificacion.ToString();
            txtNombres.Text = vSupervisionGruposApoyoIntegrantes.Nombres;
            txtApellidos.Text = vSupervisionGruposApoyoIntegrantes.Apellidos;
            rblEstado.SelectedValue = vSupervisionGruposApoyoIntegrantes.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionGruposApoyoIntegrantes.UsuarioCrea, vSupervisionGruposApoyoIntegrantes.FechaCrea, vSupervisionGruposApoyoIntegrantes.UsuarioModifica, vSupervisionGruposApoyoIntegrantes.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);
            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            hfIdGrupo.Value = GruposSupervision[1];
            ddlIdDireccion.SelectedValue = GruposSupervision[2];
            ddlIdDireccion.Enabled = false;
            txtGrupo.Enabled = false;


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    [System.Web.Services.WebMethod]
    public static String Guardar(string pNombreGrupo, string pIdgrupo, string pIdDireccion, string pNdocIntegrante, string pNombIntegrante, string pApeIntegrante, string pEstado)
    {
        string respuesta = "@OK";

        try
        {

            int vResultado;
            SupervisionGruposApoyoIntegrantes vSupervisionGruposApoyoIntegrantes = new SupervisionGruposApoyoIntegrantes();
            SupervisionService vSupervisionService = new SupervisionService();
            //GeneralWeb objGeneralweb = new GeneralWeb(); 

            vSupervisionGruposApoyoIntegrantes.Grupo = pNombreGrupo;
            vSupervisionGruposApoyoIntegrantes.IdGrupo = Convert.ToInt32(pIdgrupo);
            vSupervisionGruposApoyoIntegrantes.IdDireccion = Convert.ToInt32(pIdDireccion);
            vSupervisionGruposApoyoIntegrantes.Identificacion = Convert.ToInt32(pNdocIntegrante);
            vSupervisionGruposApoyoIntegrantes.Nombres = pNombIntegrante;
            vSupervisionGruposApoyoIntegrantes.Apellidos = pApeIntegrante;
            vSupervisionGruposApoyoIntegrantes.Estado = Convert.ToInt32(pEstado);

            //if (Request.QueryString["oP"] == "E")
            //{
            //vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo = Convert.ToInt32(hfIdIntegranteGrupo.Value);
            //vSupervisionGruposApoyoIntegrantes.IdGrupo = Convert.ToInt32(hfIdGrupo.Value);
            //vSupervisionGruposApoyoIntegrantes.UsuarioModifica = GetSessionUser().NombreUsuario;
            //InformacionAudioria(vSupervisionGruposApoyoIntegrantes, this.PageName, vSolutionPage);
            //vResultado = vSupervisionService.ModificarSupervisionGruposApoyoIntegrantes(vSupervisionGruposApoyoIntegrantes);
            //}
            //else
            //{
            GeneralWeb vObjGeneralWeb = new GeneralWeb();

            vSupervisionGruposApoyoIntegrantes.UsuarioCrea = vObjGeneralWeb.GetSessionUser().NombreUsuario;
            // objGeneralweb.InformacionAudioria(vSupervisionGruposApoyoIntegrantes, this.PageName, vSolutionPage);
            vResultado = vSupervisionService.InsertarSupervisionGruposApoyoIntegrantes(vSupervisionGruposApoyoIntegrantes);
            //}
            if (vResultado == 0)
            {
                respuesta = "La operación no se completo satisfactoriamente, verifique por favor.";
            }
            //else if (vResultado == 1)
            //{
            //    SetSessionParameter("SupervisionGruposApoyoIntegrantes.IdIntegranteGrupo", vSupervisionGruposApoyoIntegrantes.IdIntegranteGrupo);
            //    SetSessionParameter("SupervisionGruposApoyoIntegrantes.IdGrupo", vSupervisionGruposApoyoIntegrantes.IdGrupo);
            //    SetSessionParameter("SupervisionGruposApoyoIntegrantes.Guardado", "1");
            //    NavigateTo(SolutionPage.Detail);
            //}
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}



        }
        catch (UserInterfaceException ex)
        {
            respuesta = ex.Message.ToString();
        }
        catch (Exception ex)
        {
            respuesta = ex.Message.ToString();
        }

        return respuesta;
    }


}
