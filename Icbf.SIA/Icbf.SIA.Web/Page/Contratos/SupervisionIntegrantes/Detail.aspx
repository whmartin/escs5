<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SupervisionGruposApoyoIntegrantes_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdIntegranteGrupo" runat="server" />
    <asp:HiddenField ID="hfIdGrupo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo Documento *
            </td>
            <td>
                Numero de documento *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoDoc" Width="80%">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdentificacion" Enabled="false" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Apellidos *
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombres" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
