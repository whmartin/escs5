<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SupervisionGruposApoyoIntegrantes_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Nombre del Grupo *
                </td>
                <td>
                    Dirección *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtGrupo"  Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdDireccion" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Numero de documento *
                </td>
                <td>
                    Nombre Integrante *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtIdentificacion"  Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombres"  Width="80%"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Apellidos *
                </td>
                <td>
                    Estado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtApellidos"  Width="80%"></asp:TextBox>
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSupervisionGruposApoyoIntegrantes" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdIntegranteGrupo,IdGrupo"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvSupervisionGruposApoyoIntegrantes_PageIndexChanging"
                        OnSelectedIndexChanged="gvSupervisionGruposApoyoIntegrantes_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre del Grupo" DataField="IdGrupo" />
                            <asp:BoundField HeaderText="Numero de documento" DataField="Identificacion" />
                            <asp:BoundField HeaderText="Nombre Integrante" DataField="Nombres" />
                            <asp:BoundField HeaderText="Apellidos" DataField="Apellidos" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
