<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Adiciones_Add" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<%--<asp:HiddenField ID="hfIdAdicion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td style="width: 50%">
                Regional
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px"></asp:TextBox>
               
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjetoContrato"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcanceContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorini"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor de la adición *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorAdicion" ControlToValidate="txtValorAdicion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Justificación de la adición *
                <asp:RequiredFieldValidator runat="server" ID="rfvJustificacionAdicion" ControlToValidate="txtJustificacionAdicion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAdicion"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAdicion" runat="server" TargetControlID="txtValorAdicion"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicion" Width="320px" Height="65px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftJustificacionAdicion" runat="server" TargetControlID="txtJustificacionAdicion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
    </table>
    <style type="text/css">
        table .grillaCentral tr.rowAG td, table .grillaCentral tr.rowBG td
        {
            text-align: center;
            padding: 5px;
            padding-right: 0px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function ValidaPlanCompras(source, args) {
            var hfConsecutivosPlanCompras = document.getElementById('<%= hfConsecutivosPlanCompras.ClientID %>');
            if (parseInt(hfConsecutivosPlanCompras.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaProductos(source, args) {
            var hfProductos = document.getElementById('<%= hfProductos.ClientID %>');
            if (parseInt(hfProductos.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function ValidaAportes(source, args) {
            var hfAportes = document.getElementById('<%= hfAportes.ClientID %>');
            if (parseInt(hfAportes.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }


        function ValidaCDP(source, args) {
            var hfCDP = document.getElementById('<%= hfCDP.ClientID %>');
            if (parseInt(hfCDP.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function mensajeAlerta(control, texto) {
            alert(texto);
            var creference = document.getElementById(control);
            creference.focus();
        }

        function ValidaEliminacion() {
            return confirm('Esta seguro de que desea eliminar el registro?');
        }

        function PreGuardado() {
            ConfiguraValidadores(false);
            muestraImagenLoading();
        }

        function Aprobacion() {
            ConfiguraValidadores(true);
            window.Page_ClientValidate("btnAprobar");
            if (!window.Page_IsValid) {
                alert('Debe diligenciar los campos obligatorios');
                return false;
            } else {
                return confirm('Está seguro que desea Finalizar el registro del Contrato/Convenio?');
            }
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostbck.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }

        function EjecutarJSFechaInicialSupervisores(control, fechaOriginal) {
            $("#lblError")[0].innerHTML = '';
            $("#lblError")[0].className = '';
            var fechaInicioEjecucion = document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value;
            var fechaInicioSupervisor = document.getElementById(control.id).value;

            if (fechaInicioEjecucion != '') {
                var splitfechainicio = fechaInicioEjecucion.split('/');
                var jfechaInicioEjecucion = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                splitfechainicio = fechaInicioSupervisor.split('/');
                var jfechaInicioSupervisor = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                if (jfechaInicioSupervisor < jfechaInicioEjecucion) {
                    document.getElementById(control.id).value = fechaOriginal;
                    alert('La Fecha de Inicio de Supervisor y/o Interventor debe ser mayor o igual a la Fecha de Ejecución de Contrato/Convenio');
                }
            }
        }

    </script>
    <script type="text/javascript" language="javascript">
        /*****************************************************************************
        Código para colocar los indicadores de miles  y decimales mientras se escribe
        Script creado por Tunait!
        Si quieres usar este script en tu sitio eres libre de hacerlo con la condición de que permanezcan intactas estas líneas, osea, los créditos.

        http://javascript.tunait.com
        tunait@yahoo.com  27/Julio/03
        ******************************************************************************/
        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

    </script>
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <table width="90%" align="center">
        <tr>
            <td>
                <Ajax:Accordion ID="AccContratos" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                    ContentCssClass="accordionContent" OnItemCommand="AccContratos_ItemCommand" runat="server"
                    Width="100%" Height="100%">
                    <Panes>
                        <Ajax:AccordionPane ID="ApPlanComprasProductos" runat="server">
                            <Header>
                                Plan de compras asociado / Productos
                            </Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="lbPlanCompras" runat="server" Text="Plan de compras"></asp:Label>
                                            <asp:CustomValidator ID="cvPlanCompras" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaPlanCompras"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfConsecutivosPlanCompras" runat="server" />
                                            <asp:TextBox ID="txtPlanCompras" runat="server" Enabled="false" Width="30%" ViewStateMode="Enabled"
                                                OnTextChanged="txtPlanComprasTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgPlanCompras" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetPlanCompras(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvConsecutivos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" DataKeyNames="IDPlanDeComprasContratos,IDPlanDeCompras,Vigencia"
                                                CellPadding="8" Height="16px" CssClass="grillaCentral">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="IDPlanDeCompras" />
                                                    <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" />
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowAG" HorizontalAlign="Center" />
                                                <RowStyle CssClass="rowBG" HorizontalAlign="Center" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            <asp:Label ID="LbProductos" runat="server" Text="Productos"></asp:Label>
                                            <asp:HiddenField ID="hfProductos" runat="server" />
                                            <asp:CustomValidator ID="cvProductos" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaProductos"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvProductos" runat="server" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código del Producto" DataField="codigo_producto" />
                                                    <asp:BoundField HeaderText="Nombre del Producto" DataField="nombre_producto" />
                                                    <asp:BoundField HeaderText="Tipo Producto" DataField="tipoProductoView" />
                                                    <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" DataFormatString="{0:N0}" />
                                                    <asp:BoundField HeaderText="Valor Unitario" DataField="valor_unitario" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Total" DataField="valor_total" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Tiempo" DataField="tiempo" />
                                                    <asp:BoundField HeaderText="Unidad de Tiempo" DataField="tipotiempoView" />
                                                    <asp:BoundField HeaderText="Unidad de Medida" DataField="unidad_medida" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Rubros Plan de Compras
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosPlanCompras" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" DataKeyNames="total_rubro" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número Consecutivo Plan de Compras" DataField="consecutivo" />
                                                    <asp:BoundField HeaderText="Código Rubro" DataField="codigo_rubro" />
                                                    <asp:BoundField HeaderText="Descripción del Rubro" DataField="NombreRubro" />
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" DataField="" />
                                                    <asp:BoundField HeaderText="Valor del Rubro" DataField="total_rubro" DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Consecutivo Plan de Compras
                                            <asp:CompareValidator runat="server" ID="cvConsecutivoPlanCompras" ControlToValidate="DdlConsecutivoPlanCompras"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="DdlConsecutivoPlanCompras" runat="server" Width="30%" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true" OnSelectedIndexChanged="DdlConsecutivoPlanComprasSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApCDP" runat="server">
                            <Header>
                                CDP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbCDP" runat="server" Text="CDP"></asp:Label>
                                            <asp:CustomValidator ID="cvCDP" runat="server" ErrorMessage="Campo Requerido" Enabled="false"
                                                ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCDP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCDP" runat="server" />
                                            <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" OnTextChanged="txtCDPTextChanged"
                                                AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetCDP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdContratosCDP,CodigoRubro,ValorCDP,ValorRubro"
                                                AllowPaging="True" OnPageIndexChanging="gvCDP_PageIndexChanging" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion();"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Regional ICBF" DataField="Regional" />
                                                    <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorRubro" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Rubros Presupuestal" DataField="RubroPresupuestal" />
                                                    <asp:BoundField HeaderText="Tipo fuente Financiamiento" DataField="TipofuenteFinanciamiento" />
                                                    <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
                                                    <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DependenciaAfectacionGastos" />
                                                    <asp:BoundField HeaderText="Tipo Documento Soporte" DataField="TipoDocumentoSoporte" />
                                                    <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="TipoSituacionFondos" />
                                                    <asp:BoundField HeaderText="Consecutivo Plan de Compras" DataField="ConsecutivoPlanCompras" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApAportes" runat="server">
                            <Header>
                                Aportes Contratistas e ICBF</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Valor Aportes ICBF
                                            <asp:HiddenField ID="hfAportes" runat="server" />
                                            <asp:CustomValidator ID="cvAportes" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaAportes"></asp:CustomValidator>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Valor Aportes Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoICBF" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                                OnTextChanged="txtValorApoICBFTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoICBF" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetValorApoICBF(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoContrat" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                                OnTextChanged="txtValorApoContratTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoContrat" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetValorApoContrat(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes ICBF
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false" DataKeyNames="IdAporteContrato"
                                                AllowSorting="true" OnSorting="gvAportesICBF_Sorting" GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo de Aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripción Aporte Especie" DataField="DescripcionAporte"
                                                        SortExpression="DescripcionAporte" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarAporteICBFClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesContratista" runat="server" AutoGenerateColumns="false"
                                                DataKeyNames="IdAporteContrato" AllowSorting="true" OnSorting="gvAportesContratista_Sorting"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Contratista" DataField="NumeroIdentificacionContratista"
                                                        SortExpression="NumeroIdentificacionContratista" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="InformacionAportante"
                                                        SortExpression="InformacionAportante" />
                                                    <asp:BoundField HeaderText="Tipo Aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripción Aporte Especie" DataField="DescripcionAporte"
                                                        SortExpression="DescripcionAporte" />
                                                    <asp:BoundField HeaderText="Fecha RP" DataField="FechaRP" SortExpression="FechaRP"
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Número RP" DataField="NumeroRP" SortExpression="NumeroRP" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarAporteContratistaClick"
                                                                OnClientClick="return ValidaEliminacion();" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                    </Panes>
                </Ajax:Accordion>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfPostbck" runat="server" />
    <asp:HiddenField ID="hfIdEstadoContrato" runat="server" />
    <asp:HiddenField ID="hfCodContratoAsociadoSel" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaConvenio" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServApoyoGestion" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServProfesionales" runat="server" />
    <asp:HiddenField ID="hfIdTipoContAporte" runat="server" />
    <asp:HiddenField ID="hfIdMarcoInteradministrativo" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirecta" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirectaAporte" runat="server" />
    <asp:HiddenField ID="hfTotalProductos" runat="server" />
    <asp:HiddenField ID="hfTotalProductosCDP" runat="server" />
    <asp:HiddenField ID="hfAcordeonActivo" runat="server" />
    <asp:HiddenField ID="hfObjPlan" runat="server" />
    <asp:HiddenField ID="hfAlcPlan" runat="server" />
    <asp:HiddenField ID="hfRegional" runat="server" />
    
    <script type="text/javascript" language="javascript">
        function GetNumConvenioContrato() {
            muestraImagenLoading();
            //CU-46-CONT-CONS-CONT
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratoNew.aspx', setReturnGetNumConvenioContrato, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetNumeroProceso() {
            muestraImagenLoading();
            //CU-033-CONT-NUM_PRO
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaNumeroProceso.aspx', setReturnGetNumeroProceso, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetNombreSolicitante() {
            muestraImagenLoading();
            //CU-47-CONT-SOLI
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaEmpleado.aspx?op=Sol', setReturnGetNombreSolicitante, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetNombreOrdenadorGasto() {
            muestraImagenLoading();
            //CU-47-CONT-SOLI
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaEmpleado.aspx?op=OrdG', setReturnGetNombreOrdenadorGasto, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetPlanCompras() {
            muestraImagenLoading();
            //CU-40-CONT-SOLI
            window_showModalDialog('../../../Page/Contratos/LupasFormaPagos/LupasRelacionarPlanCompras.aspx', setReturnGetPlanCompras, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetPlanCompras(dialog) {
            prePostbck(false);
            __doPostBack('<%= txtPlanCompras.ClientID %>', '');
        }

        function setReturnGetValorApoICBF() {
            prePostbck(false);
            __doPostBack('<%= txtValorApoICBF.ClientID %>', '');
        }

        function setReturnGetValorApoContrat() {
            prePostbck(false);
            __doPostBack('<%= txtValorApoContrat.ClientID %>', '');
        }

        function GetLugarEjecucion() {
            muestraImagenLoading();
            //CU-028-CONT-RELAC-LUGEJEC
            window_showModalDialog('../../../Page/Contratos/LugarEjecucionContrato/LupaConsultarLugarContrato.aspx', setRetrunGetLugarEjecucion, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetContratista() {
            muestraImagenLoading();
            //CU-022-CONT-RELAC-CONTRATISTA
            window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx', setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function GetCDP() {
            muestraImagenLoading();
            //CU-031-CONT-REG_INF_CD-RP
            var idRegional = document.getElementById('<%= hfRegional.ClientID %>').value;
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaRegInfoPresupuestal.aspx?IdRegContrato=' + idRegional, setReturnGetCDP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetCDP(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                prePostbck(false);
                __doPostBack('<%= txtCDP.ClientID %>', '');
            } else {
                ocultaImagenLoading();
            }
        }
    </script>--%>
</asp:Content>
