using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Adiciones_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/Adiciones";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vValorAdicion = null;
            String vJustificacionAdicion = null;
            int? vEstado = null;
            DateTime? vFechaAdicion = null;
            int? vIDDetalleConsModContractual = null;
            if (txtValorAdicion.Text!= "")
            {
                vValorAdicion = Convert.ToInt32(txtValorAdicion.Text);
            }
            if (txtJustificacionAdicion.Text!= "")
            {
                vJustificacionAdicion = Convert.ToString(txtJustificacionAdicion.Text);
            }
            if (txtEstado.Text!= "")
            {
                vEstado = Convert.ToInt32(txtEstado.Text);
            }
            if (txtFechaAdicion.Text!= "")
            {
                vFechaAdicion = Convert.ToDateTime(txtFechaAdicion.Text);
            }
            if (ddlIDDetalleConsModContractual.SelectedValue!= "-1")
            {
                vIDDetalleConsModContractual = Convert.ToInt32(ddlIDDetalleConsModContractual.SelectedValue);
            }
            gvAdiciones.DataSource = vContratoService.ConsultarAdicioness( vValorAdicion, vJustificacionAdicion, vEstado, vFechaAdicion, vIDDetalleConsModContractual);
            gvAdiciones.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvAdiciones.PageSize = PageSize();
            gvAdiciones.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Adiciones", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvAdiciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Adiciones.IdAdicion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAdiciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvAdiciones.SelectedRow);
    }
    protected void gvAdiciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAdiciones.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Adiciones.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Adiciones.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIDDetalleConsModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDDetalleConsModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
