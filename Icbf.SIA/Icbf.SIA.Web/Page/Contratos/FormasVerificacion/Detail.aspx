<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Adiciones_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdAdicion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Valor de la adición *
            </td>
            <td>
                Justificación de la adición *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAdicion"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>
                Fecha de la Adición *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEstado"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaAdicion"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Detalle adicion *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIDDetalleConsModContractual"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
