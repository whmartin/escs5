using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Adiciones_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/Adiciones";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Adiciones.IdAdicion", hfIdAdicion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdAdicion = Convert.ToInt32(GetSessionParameter("Adiciones.IdAdicion"));
            RemoveSessionParameter("Adiciones.IdAdicion");

            if (GetSessionParameter("Adiciones.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Adiciones");


            Adiciones vAdiciones = new Adiciones();
            vAdiciones = vContratoService.ConsultarAdiciones(vIdAdicion);
            hfIdAdicion.Value = vAdiciones.IdAdicion.ToString();
            txtValorAdicion.Text = vAdiciones.ValorAdicion.ToString();
            txtJustificacionAdicion.Text = vAdiciones.JustificacionAdicion;
            txtEstado.Text = vAdiciones.Estado.ToString();
            txtFechaAdicion.Text = vAdiciones.FechaAdicion.ToString();
            ddlIDDetalleConsModContractual.SelectedValue = vAdiciones.IDDetalleConsModContractual.ToString();
            ObtenerAuditoria(PageName, hfIdAdicion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vAdiciones.UsuarioCrea, vAdiciones.FechaCrea, vAdiciones.UsuarioModifica, vAdiciones.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdAdicion = Convert.ToInt32(hfIdAdicion.Value);

            Adiciones vAdiciones = new Adiciones();
            vAdiciones = vContratoService.ConsultarAdiciones(vIdAdicion);
            InformacionAudioria(vAdiciones, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarAdiciones(vAdiciones);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Adiciones.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Adiciones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIDDetalleConsModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDDetalleConsModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
