<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Adiciones_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Valor de la adición *
            </td>
            <td>
                Justificación de la adición *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorAdicion"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacionAdicion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>
                Fecha de la Adición *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEstado"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaAdicion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Detalle adicion *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIDDetalleConsModContractual"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAdiciones" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdAdicion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvAdiciones_PageIndexChanging" OnSelectedIndexChanged="gvAdiciones_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Valor de la adición" DataField="ValorAdicion" />
                            <asp:BoundField HeaderText="Justificación de la adición" DataField="JustificacionAdicion" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />
                            <asp:BoundField HeaderText="Fecha de la Adición" DataField="FechaAdicion" />
                            <asp:BoundField HeaderText="Detalle adicion" DataField="IDDetalleConsModContractual" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
