using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using System.Net;
using Icbf.SIA.Service;

public partial class Page_CambioLugarEjecucion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/CambioLugarEjecucion";
    string TIPO_ESTRUCTURA = "Prorrogas";
    ContratoService vContratoService = new ContratoService();
    private ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    private string nLugarEjecucion
    { set { hfLugarEjecucion.Value = value; } }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                VerficarQueryStrings();
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;            
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Cambio Lugar de Ejecuci&#243;n", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        bool Nuevo = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                Nuevo = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                Nuevo = true;
            }
        }

        if (Nuevo)
        {
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetalleConsModContractual.Value.ToString());
            NavigateTo(SolutionPage.Add);
        }
        else
        {
            toolBar.MostrarMensajeError("No se puede Registrar una nueva prorroga, Por favor validar el estado!");
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        bool Editar = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                Editar = true;
            }
        }
        else
        {
            //if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            //{
            //    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            //    Editar = true;
            //}
            Editar = false;
        }

        if (Editar)
        {
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetalleConsModContractual.Value);
            NavigateTo(SolutionPage.Edit);
        }
        else
        {
            toolBar.MostrarMensajeError("No se puede Editar el Cambio de Lugar de Ejecucion, Por favor validar el estado!");
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        bool Eliminar = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
                Eliminar = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
                Eliminar = true;
            }
        }

        if (!Eliminar)
        {
            toolBar.MostrarMensajeError("No se puede Eliminar la prorroga, Por favor validar el estado!");
        }       
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());

            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    private void CargarDatos()
    {
        try
        {
            int vIdConsModContractual;

            if (! string.IsNullOrEmpty( GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConsModContractual);

                if (detalleSolicitud.Estado == "Registro" || detalleSolicitud.Estado == "Devuelta")
                    toolBar.MostrarBotonEditar(true);
                else
                    toolBar.MostrarBotonEditar(false);
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();

                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                {
                    hfEsSubscripcion.Value = "1";
                    RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                {
                    hfEsSubscripcion.Value = "2";
                    RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                }

                toolBar.MostrarBotonEditar(false);
                toolBar.OcultarBotonEliminar(true);
                toolBar.OcultarBotonNuevo(true);
            }

            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));

           

            hfIdTContrato.Value = vIdContrato.ToString();

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(vIdContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinalTerminacionContrato = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);
            //DateTime caFechaFinalTerminacionContrato = Convert.ToDateTime(vContrato.FechaFinalizacionIniciaContrato);

            TxtContrato.Text = vContrato.NumeroContrato.ToString();
            TxtRegional.Text = vContrato.NombreRegional;
            TxtFechaInicioContrato.Text = caFechaInicioEjecucion.ToShortDateString();
            TxtFechaFinalContrato.Text = caFechaFinalTerminacionContrato.ToShortDateString();
            TxtObjetoContrato.Text = vContrato.ObjetoContrato;
            TxtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            TxtValorInicial.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            TxtValorFinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

           
            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            txtJustificacion.Text = vConsModContractual.Justificacion;
            hfIDCosModContractualEstado.Value = vConsModContractual.IdConsModContractualesEstado.ToString();
                                

            ObtenerAuditoria(PageName, hfIdProrroga.Value);

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            SeleccionaLugarEjecucion(string.Empty);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    
   
    private void EliminarRegistro()
    {
        try
        {
            int vIdProrroga = Convert.ToInt32(hfIdProrroga.Value);

            ReduccionesTiempo vReduccion = new ReduccionesTiempo();
            vReduccion = vContratoService.ConsultarReduccionTiempo(vIdProrroga);
            InformacionAudioria(vReduccion, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarReduccionesTiempo(vReduccion);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Prorrogas.Eliminado", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region LugarEjecucion

    private void HabilitarLugarEjecucion(bool habilitar)
    {
        if (String.IsNullOrEmpty(habilitar.ToString()))
        {
            gvLugarEjecucion.DataSource = null;
            gvLugarEjecucion.DataBind();
            nLugarEjecucion = "0";
            dvLugarEjecucion.Style.Remove("height");
            dvLugarEjecucion.Style.Remove("overflow-x");
            dvLugarEjecucion.Style.Remove("overflow-y");     
        }
        else if (habilitar == true)
        {            
            gvLugarEjecucion.Enabled = true;       
        }
        else
        {
            gvLugarEjecucion.Enabled = false;           
            
        }
    }

    
    private void SeleccionaLugarEjecucion(string LugarSeleccionado)
    {
        List<CambioLugarEjecucion> lugaresContrato = vContratoService.ConsultarCambiosLugaresEjecucion(Convert.ToInt32(hfIdDetalleConsModContractual.Value));
        gvLugarEjecucion.DataSource = lugaresContrato;
        gvLugarEjecucion.DataBind();
        nLugarEjecucion = gvLugarEjecucion.Rows.Count.ToString();

        dvLugarEjecucion.Style.Remove("height");
        dvLugarEjecucion.Style.Remove("overflow-x");
        dvLugarEjecucion.Style.Remove("overflow-y");

        if (lugaresContrato.Any(x => x.EsNivelNacional == true))
        {
            chkNivelNal.Checked = true;

        }
        else
        {
            if (lugaresContrato.Count() > 2)
            {
                dvLugarEjecucion.Style.Add("height", "100px");
                dvLugarEjecucion.Style.Add("overflow-x", "hidden");
                dvLugarEjecucion.Style.Add("overflow-y", "scroll");
            }

            chkNivelNal.Checked = false;                     

        }
    }

   

    private void VerficarQueryStrings()
    {     

        if (!string.IsNullOrEmpty(Request.QueryString["IDDetalleConsModContractual"]))
            hfIdDetalleConsModContractual.Value = Request.QueryString["IDDetalleConsModContractual"];

        if (!string.IsNullOrEmpty(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString()))
            hfIdDetalleConsModContractual.Value = GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString();
    }



    #endregion

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdTContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
}
