using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using Icbf.SIA.Service;

public partial class Page_CambioLugarEjecucion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    string PageName = "Contratos/CambioLugarEjecucion";
    string TIPO_ESTRUCTURA = "Prorrogas";
    int vIdContrato;
    decimal vIdIndice = 0;

    private string nLugarEjecucion
    { set { hfLugarEjecucion.Value = value; } }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                VerficarQueryStrings();
                ObtenerLugaresEjecucionContrato();
                CargarDatosIniciales();
                CargarRegistro();
            }
            else
            {
                string sControlName = Request.Params.Get("__EVENTTARGET");

                if (sControlName == "cphCont_txtLugarEjecucion")
                    txtLugarEjecucionTextChanged(sender, e);
            }
        }
    }

    protected void chkNivelNacionalCheckedChanged(object sender, EventArgs e)
    {
        
    }

    protected void txtLugarEjecucionTextChanged(object sender, EventArgs e)
    {
        SeleccionaLugarEjecucion(string.Empty);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            //toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            

            toolBar.EstablecerTitulos("Cambio Lugar de Ejecuci&#243;n", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
        }
    }
    

    private void CargarRegistro()
    {
        try
        {
            //int IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            //hfIdContrato.Value = IdContrato.ToString();           

            int vIdConsModContractual;

            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(Convert.ToInt32(hfIdContrato.Value));

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinContrato = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);
            DateTime caFechaCalculada = caFechaFinContrato.AddDays(1);            

            TxtContrato.Text = vContrato.NumeroContrato.ToString();
            TxtRegional.Text = vContrato.NombreRegional;
            TxtFechaInicioContrato.Text = caFechaInicioEjecucion.ToShortDateString();
            TxtFechaFinalContrato.Text = caFechaFinContrato.ToShortDateString();
            TxtObjetoContrato.Text = vContrato.ObjetoContrato;
            TxtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            TxtValorInicial.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            TxtValorFinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);
            // txtFechaInicio.Text = caFechaCalculada.ToShortDateString();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }
            

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            txtJustificacion.Text = vConsModContractual.Justificacion;

           
            SeleccionaLugarEjecucion(string.Empty);
            SeleccionaLugarEjecucionActual(string.Empty);
            HabilitarLugarEjecucion(true);

          

            if (Request.QueryString["oP"] == "E")
            {
                
                             

                PanelArchivos.Visible = true;
            }

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region LugarEjecucion

    protected void btnEliminarLugEjecucionClick(object sender, EventArgs e)
    {
        EliminarLugarEjecucion(Convert.ToInt32(((LinkButton)sender).CommandArgument));
    }

    protected void txtDatosAdicionalesLugarEjecucionTextChanged(object sender, EventArgs e)
    {
        //IngresaDatosAdicionalesLugarEjecucion(txtDatosAdicionalesLugarEjecucion.Text);
    }
   

    private void HabilitarLugarEjecucion(bool habilitar)
    {
        if (String.IsNullOrEmpty(habilitar.ToString()))
        {
            imgLugarEjecucion.Enabled = false;
            gvLugaresEjecucion.DataSource = null;
            gvLugaresEjecucion.DataBind();
            nLugarEjecucion = "0";
            dvLugaresEjecucion.Style.Remove("height");
            dvLugaresEjecucion.Style.Remove("overflow-x");
            dvLugaresEjecucion.Style.Remove("overflow-y");
            
            
        }
        else if (habilitar == true)
        {
            imgLugarEjecucion.Enabled = true;
            gvLugaresEjecucion.Enabled = true;
            
            //HabilitarDatosAdicionalesLugarEjecucion(true);
            
        }
        else
        {
            imgLugarEjecucion.Enabled = false;
            gvLugaresEjecucion.Enabled = false;
                     
        }
    }

    private void EliminarLugarEjecucion(int rowIndex)
    {
        CambioLugarEjecucion vLugarEjecucionContrato = new CambioLugarEjecucion();
        vLugarEjecucionContrato.IdCambioLugarEjecucion = Convert.ToInt32(gvLugaresEjecucion.DataKeys[rowIndex]["IdCambioLugarEjecucion"]);
        vContratoService.EliminarCambioLugarEjecucion(vLugarEjecucionContrato);
        SeleccionaLugarEjecucion(string.Empty);
    }

    private void SeleccionaLugarEjecucion(string LugarSeleccionado)
    {
       
        List<CambioLugarEjecucion> lugaresContrato = vContratoService.ConsultarCambiosLugaresEjecucion(Convert.ToInt32(hfIDDetalleConsModContractual.Value));
        gvLugaresEjecucion.DataSource = lugaresContrato;
        gvLugaresEjecucion.DataBind();
        nLugarEjecucion = gvLugaresEjecucion.Rows.Count.ToString();

        dvLugaresEjecucion.Style.Remove("height");
        dvLugaresEjecucion.Style.Remove("overflow-x");
        dvLugaresEjecucion.Style.Remove("overflow-y");

        
        if (lugaresContrato.Any(x => x.EsNivelNacional == true))
        {
            chkNivelNacional.Checked = true;
            gvLugaresEjecucion.Visible = false;
        }
        else
        {
            if (lugaresContrato.Count() > 2)
            {
                dvLugaresEjecucion.Style.Add("height", "100px");
                dvLugaresEjecucion.Style.Add("overflow-x", "hidden");
                dvLugaresEjecucion.Style.Add("overflow-y", "scroll");
            }

            chkNivelNacional.Checked = false;
            gvLugaresEjecucion.Visible = true;

        }
    }

    private void SeleccionaLugarEjecucionActual(string LugarSeleccionado)
    {
        List<LugarEjecucionContrato> lugaresContrato = vContratoService.ConsultarLugaresEjecucionContrato(Convert.ToInt32(hfIdContrato.Value)).Where(X=>X.Historico != true).ToList();
        gvLugarEjecucion.DataSource = lugaresContrato;
        gvLugarEjecucion.DataBind();
        nLugarEjecucion = gvLugaresEjecucion.Rows.Count.ToString();

        dvLugarEjecucion.Style.Remove("height");
        dvLugarEjecucion.Style.Remove("overflow-x");
        dvLugarEjecucion.Style.Remove("overflow-y");

        
        if (lugaresContrato.Any(x => x.EsNivelNacional == true) || lugaresContrato.Any(x => x.NivelNacional == true))
        {
            chkNivelNal.Checked = true;
        }
        else
        {
            if (lugaresContrato.Count() > 2)
            {
                dvLugarEjecucion.Style.Add("height", "100px");
                dvLugarEjecucion.Style.Add("overflow-x", "hidden");
                dvLugarEjecucion.Style.Add("overflow-y", "scroll");
            }

            chkNivelNal.Checked = false;
           

        }
    }       

    private void VerficarQueryStrings()
    {
        if (GetSessionParameter("Contrato.IdContrato") != null)
            hfIdContrato.Value = GetSessionParameter("Contrato.IdContrato").ToString();        

        if (!string.IsNullOrEmpty(Request.QueryString["IDDetalleConsModContractual"]))
            hfIDDetalleConsModContractual.Value = Request.QueryString["IDDetalleConsModContractual"];

        if (! string.IsNullOrEmpty(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString()))
            hfIDDetalleConsModContractual.Value = GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString();
    }

    private void ObtenerLugaresEjecucionContrato()
    {
        
        if(!string.IsNullOrEmpty(hfIDDetalleConsModContractual.Value) && !string.IsNullOrEmpty(hfIdContrato.Value))
        {
            vContratoService.InsertarLugaresEjecucionActuales(Convert.ToInt32(hfIdContrato.Value), Convert.ToInt32(hfIDDetalleConsModContractual.Value), GetSessionUser().NombreUsuario);
        }
    }

    #endregion


    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion


}
