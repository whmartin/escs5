<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_CambioLugarEjecucion_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdProrroga" runat="server" />
    <asp:HiddenField ID="hfIDCosModContractual" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIndice" runat="server" />
    <asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />

    
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de Contrato
            </td>
            <td>
                Regional
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="TxtContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TxtRegional"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicio Contrato/Convenio
            </td>
            <td>
                Fecha Final de Terminación Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="TxtFechaInicioContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TxtFechaFinalContrato"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Objeto del Contrato
            </td>
            <td>
                Alcance del Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="TxtObjetoContrato"  TextMode="MultiLine" Height="73px" Width="90%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TxtAlcanceContrato" TextMode="MultiLine" Height="73px" Width="90%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Valor Inicial del Contrato/Convenio
            </td>
            <td>
                Valor Final del Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="TxtValorInicial"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TxtValorFinal"  Enabled="false"></asp:TextBox>
            </td>
        </tr>   
        
        <tr class="rowA">
            <td class="auto-style3">
            </td>
            <td class="auto-style3">
            </td>
        </tr>   
        
        <tr class="rowB">
                <td colspan="2">
                    Lugar de Ejecución Actual</td>
            </tr>
        
        <tr class="rowB">
                <td colspan="2">
                    <asp:CheckBox ID="chkNivelNal" runat="server" Enabled="false" AutoPostBack="true"/>
                    Nivel Nacional
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" colspan="2">
                    <div id="dvLugarEjecucion" runat="server">
                        <asp:GridView ID="gvLugarEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos"
                            AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px" >
                            <Columns>
                                <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                <asp:BoundField HeaderText="Municipio" DataField="Municipio" />                                
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>   
        
        <tr class="rowB">
            <td>
                 &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>   
             
        <tr class="rowB">
            <td>
                 Lugar de ejecución Nuevo *
                <asp:CustomValidator ID="cvLugarEjecucion" runat="server" ErrorMessage="Campo Requerido"
                 Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaLugarEjecucion"></asp:CustomValidator>
            </td>
            <td>
                <%--Datos Adicionales lugar de ejecución--%>
            </td>
        </tr>   
             
        <tr class="rowA">
            <td>
                <asp:HiddenField ID="hfLugarEjecucion" runat="server" />
                <asp:TextBox ID="txtLugarEjecucion" runat="server" Enabled="false" ViewStateMode="Enabled"
                    OnTextChanged="txtLugarEjecucionTextChanged" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgLugarEjecucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    Enabled="true" OnClientClick="GetLugarEjecucion(); return false;" Style="cursor: hand"
                    ToolTip="Buscar" />
             &nbsp;</td>
            <td>
               
            </td>
            
                      </tr>   
        <tr class="rowB">
                <td colspan="2">
                    <asp:CheckBox ID="chkNivelNacional" runat="server" Enabled="false" AutoPostBack="true"
                        OnCheckedChanged="chkNivelNacionalCheckedChanged" />
                    Nivel Nacional
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell" colspan="2">
                    <div id="dvLugaresEjecucion" runat="server">
                        <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdCambioLugarEjecucion" OnTextChanged ="cambioLugarEjeTextChanged"
                            AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px">
                            <Columns>
                                <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarLugEjecucionClick" 
                                            OnClientClick="return ValidaEliminacion();" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                            <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        <tr class="rowAB">
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Justificación de la novedad contractual 
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Height="73px" Width="95%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowAB">
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" Visible="true" ID="PanelArchivos">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td>
                                            <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
   

    <script type="text/javascript" language="javascript">

        function GetLugarEjecucion() {
            
        //muestraImagenLoading();
        
            //CU-028-CONT-RELAC-LUGEJEC

            var idDetalleConsModContractual = document.getElementById('<%= hfIDDetalleConsModContractual.ClientID %>');
            var url = '../../../Page/Contratos/LugarEjecucionContrato/LupaConsultarLugarContrato.aspx? idDetalleConsModContractual =' + idDetalleConsModContractual.value;
            window_showModalDialog(url, setRetrunGetLugarEjecucion, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                        
        }
    function setRetrunGetLugarEjecucion(dialog) {            
        prePostbck(false);
            __doPostBack('<%= txtLugarEjecucion.ClientID %>', '');
    }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function prePostbck(imagenLoading) {
<%--            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostbck.ClientID %>', '');
            }--%>

            if (imagenLoading == true)
                muestraImagenLoading();
            else
                ocultaImagenLoading();
        }
   </script>

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style3 {
            height: 22px;
        }
    </style>
</asp:Content>

