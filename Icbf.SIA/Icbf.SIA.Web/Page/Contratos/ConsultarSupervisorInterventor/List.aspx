<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_ConsultarSupervisorInterventor_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Tipo Supervisor y/o Interventor
            </td>
            <td>
                Número de contrato
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="hfTipoSupervisorInterventor"  ></asp:DropDownList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de Identificación
            </td>
            <td>
                Nombre y/o razón social del Supervisor e Interventor</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocialSupervisorInterventor"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número de identificación Director de la Interventoría</td>
            <td>
                Nombre o Razón social del Director de la Interventoría
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroIdentificacionDirectorInterventoria"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreRazonSocialDirectorInterventoria"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConsultarSupervisorInterventor" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="TipoSupervisorInterventor" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvConsultarSupervisorInterventor_PageIndexChanging" OnSelectedIndexChanged="gvConsultarSupervisorInterventor_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Supervisor y/o Interventor" DataField="TipoSupervisorInterventor" />
                            <asp:BoundField HeaderText="Número de contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Nombre y/o razón social del Supervisor e Interventor" DataField="NombreRazonSocialSupervisorInterventor" />
                            <asp:BoundField HeaderText="Número de identificación Director de la Interventoría" DataField="NumeroIdentificacionDirectorInterventoria" />
                            <asp:BoundField HeaderText="Nombre o Razón social del Director de la Interventoría" DataField="NombreRazonSocialDirectorInterventoria" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
