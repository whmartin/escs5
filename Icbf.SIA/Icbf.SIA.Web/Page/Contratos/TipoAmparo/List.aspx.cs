using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tipos de amparo
/// </summary>
public partial class Page_TipoAmparo_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoAmparo";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreTipoAmparo = null;
            Boolean? vEstado = null;
            int? vIdTipoGarantia = null;
            if (txtNombreTipoAmparo.Text!= "")
            {
                vNombreTipoAmparo = Convert.ToString(txtNombreTipoAmparo.Text);
            }
            if (ddlIdTipoGarantia.SelectedValue!= "-1")
            {
                vIdTipoGarantia = Convert.ToInt32(ddlIdTipoGarantia.SelectedValue);
            }
            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }
            var result = from tipoAmparo in vContratoService.ConsultarTipoAmparos(vNombreTipoAmparo, vIdTipoGarantia, vEstado)
                         join tipoGarantia in vContratoService.ConsultarTipoGarantias(null, true)
                         on tipoAmparo.IdTipoGarantia equals tipoGarantia.IdTipoGarantia
                         orderby tipoAmparo.NombreTipoAmparo
                         select new
                         {
                             tipoAmparo.IdTipoAmparo,
                             tipoAmparo.NombreTipoAmparo,
                             tipoGarantia.NombreTipoGarantia,
                             tipoAmparo.Estado};

            List<datosGridView> listaTipoAmparo = new List<datosGridView>();

            foreach (var datosResult in result)
            {
                datosGridView objDatosGridView = new datosGridView();
                objDatosGridView.IdTipoAmparo = datosResult.IdTipoAmparo;
                objDatosGridView.NombreTipoAmparo = datosResult.NombreTipoAmparo;
                objDatosGridView.NombreTipoGarantia = datosResult.NombreTipoGarantia;

                if (datosResult.Estado)
                {
                    objDatosGridView.EstadoString = "Activo";
                }
                else
                {
                    objDatosGridView.EstadoString = "Inactivo";
                }
                listaTipoAmparo.Add(objDatosGridView);
            }

            gvTipoAmparo.DataSource = listaTipoAmparo.ToList();
            gvTipoAmparo.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.LipiarMensajeError();
            gvTipoAmparo.PageSize = PageSize();
            gvTipoAmparo.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo Amparo", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoAmparo.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoAmparo.IdTipoAmparo", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoAmparo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoAmparo.SelectedRow);
    }
    protected void gvTipoAmparo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoAmparo.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoAmparo.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoAmparo.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService objContratoServices = new ContratoService();
            ddlIdTipoGarantia.DataSource = objContratoServices.ConsultarTipoGarantias(null, true);
            ddlIdTipoGarantia.DataTextField = "NombreTipoGarantia";
            ddlIdTipoGarantia.DataValueField = "IdTipoGarantia";
            ddlIdTipoGarantia.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoGarantia);
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            rblEstado.SelectedIndex = -1;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvTipoAmparo_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNombreTipoAmparo = null;
        int? vIdTipoGarantia = null;
        Boolean? vEstado = null;
        if (txtNombreTipoAmparo.Text != "")
        {
            vNombreTipoAmparo = Convert.ToString(txtNombreTipoAmparo.Text);
        }
        if (ddlIdTipoGarantia.SelectedValue != "-1")
        {
            vIdTipoGarantia = Convert.ToInt32(ddlIdTipoGarantia.SelectedValue);
        }

        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }

        var myGridResults = from tipoAmparo in vContratoService.ConsultarTipoAmparos(vNombreTipoAmparo, vIdTipoGarantia, vEstado)
                            join tipoGarantia in vContratoService.ConsultarTipoGarantias(null, true)
                     on tipoAmparo.IdTipoGarantia equals tipoGarantia.IdTipoGarantia
                     select new
                     {
                         tipoAmparo.IdTipoAmparo,
                         tipoAmparo.NombreTipoAmparo,
                         tipoGarantia.NombreTipoGarantia,
                         tipoAmparo.Estado
                     };

        List<datosGridView> listaTipoAmparo = new List<datosGridView>();

        foreach (var datosResult in myGridResults)
        {
            datosGridView objDatosGridView = new datosGridView();
            objDatosGridView.IdTipoAmparo = datosResult.IdTipoAmparo;
            objDatosGridView.NombreTipoAmparo = datosResult.NombreTipoAmparo;
            objDatosGridView.NombreTipoGarantia = datosResult.NombreTipoGarantia;

            if (datosResult.Estado)
            {
                objDatosGridView.EstadoString = "Activo";
            }
            else
            {
                objDatosGridView.EstadoString = "Inactivo";
            }
            listaTipoAmparo.Add(objDatosGridView);
        }

        if (myGridResults != null)
        {
            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                switch (e.SortExpression)
                {
                    case ("NombreTipoAmparo"):
                        listaTipoAmparo = listaTipoAmparo.OrderBy(x => x.NombreTipoAmparo).ToList();
                        break;
                    case ("NombreTipoGarantia"):
                        listaTipoAmparo = listaTipoAmparo.OrderBy(x => x.NombreTipoGarantia).ToList();
                        break;
                    case ("EstadoString"):
                        listaTipoAmparo = listaTipoAmparo.OrderBy(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvTipoAmparo.DataSource = listaTipoAmparo;
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                switch (e.SortExpression)
                {
                    case ("NombreTipoAmparo"):
                        listaTipoAmparo = listaTipoAmparo.OrderByDescending(x => x.NombreTipoAmparo).ToList();
                        break;
                    case ("NombreTipoGarantia"):
                        listaTipoAmparo = listaTipoAmparo.OrderByDescending(x => x.NombreTipoGarantia).ToList();
                        break;
                    case ("EstadoString"):
                        listaTipoAmparo = listaTipoAmparo.OrderByDescending(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvTipoAmparo.DataSource = listaTipoAmparo;
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvTipoAmparo.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}

public class datosGridView
{
    public int IdTipoAmparo { get; set; }
    public String NombreTipoAmparo { get; set; }
    public String NombreTipoGarantia { get; set; }
    public String EstadoString { get; set; }

    public datosGridView()
    {

    }
}
