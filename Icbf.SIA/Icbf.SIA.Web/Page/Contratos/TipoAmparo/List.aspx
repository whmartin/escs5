<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoAmparo_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="style1" style="width: 50%">
                    Nombre del tipo de amparo
                </td>
                <td style="width: 50%">
                    Nombre del tipo de garant&#237;a
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:TextBox runat="server" ID="txtNombreTipoAmparo" Height="25px" MaxLength="128"
                        Width="320px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreTipoAmparo" runat="server" TargetControlID="txtNombreTipoAmparo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters,numbers" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209;+/.,@_():;- "
                        InvalidChars="&#63;" />
                </td>
                <td valign="top">
                    <asp:DropDownList runat="server" ID="ddlIdTipoGarantia" Height="25px" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoAmparo" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoAmparo" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoAmparo_PageIndexChanging" OnSelectedIndexChanged="gvTipoAmparo_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvTipoAmparo_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Tipo de amparo" DataField="NombreTipoAmparo" SortExpression="NombreTipoAmparo" />--%>
                            <asp:TemplateField HeaderText="Nombre Del Tipo De Amparo" ItemStyle-HorizontalAlign="Center"
                                SortExpression="NombreTipoAmparo">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreTipoAmparo")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nombre Del Tipo De Garant&#237;a" DataField="NombreTipoGarantia"
                                SortExpression="NombreTipoGarantia" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 391px;
        }
    </style>
</asp:Content>
