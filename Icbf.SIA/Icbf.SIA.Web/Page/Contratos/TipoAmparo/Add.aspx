<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_TipoAmparo_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdTipoAmparo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" width="50%">
                Nombre del
                Tipo de amparo *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreTipoAmparo" ControlToValidate="txtNombreTipoAmparo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Nombre del
                Tipo de garant&#237;a *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoGarantia" ControlToValidate="ddlIdTipoGarantia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoGarantia" ControlToValidate="ddlIdTipoGarantia"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreTipoAmparo" Height="22px" 
                    MaxLength="128" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreTipoAmparo" runat="server" TargetControlID="txtNombreTipoAmparo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,numbers" ValidChars="áéíóúÁÉÍÓÚñÑ+/.,@_():;- "/>
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdTipoGarantia" Height="25px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 339px;
        }
    </style>
</asp:Content>

