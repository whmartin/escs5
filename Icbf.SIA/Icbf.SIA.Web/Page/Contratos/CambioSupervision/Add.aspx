﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_CambioSupervisor_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfIdConsModContractual" runat="server"  />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfSupervInterv" runat="server" />
    <script type="text/javascript">

        function GetSuperInterv(idSupervisorActual, fechaInicio, puedeEditar) {

            if (puedeEditar == '0') {
                muestraImagenLoading();
                var contrato = document.getElementById('<%= hfIdContrato.ClientID %>');
                window_showModalDialog('../../../Page/Contratos/SupervisorInterContrato/RelacionarSupervisorInterventor.aspx?esModificacion=1&idContrato=' + contrato.value +
                                                                                                                                            '&idSupervisor=' + idSupervisorActual +
                                                                                                                                            '&fechaInicio=' + fechaInicio, setReturnGetSupervisor, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }
            else
            {
                alert('No puede editar el supervisor, porque ya existe una edición anterior.')
            }
        }

        function setReturnGetSupervisor() {
            prePostbck(false);
            __doPostBack('<%= txtSuperInterv.ClientID %>', '');
        }


        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function prePostbck(imagenLoading) {
<%--            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostbck.ClientID %>', '');
            }--%>

            if (imagenLoading == true)
                muestraImagenLoading();
            else
                ocultaImagenLoading();
        }

    </script>


    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowBG">
            <td colspan="2">
            <table align="center" width="90%">
                <tr class="rowB">
                    <td class="Cell">Supervisores relacionados Actuales </td>
                </tr>
                  <tr class="rowA">
                        <td class="Cell">
                            <div id="Div1" runat="server">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,IdRol" OnRowDataBound="gvSupervisoresActuales_RowDataBound" GridLines="None" Height="16px" >
                                    <Columns>
                                       <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEditar"  runat="server" AutoPostBack="false"  >
                                                                    <img alt="Eliminar" src="../../../Image/btn/edit.gif"   title="Editar" />
                                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
            </table>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="2">
                <table align="center" width="90%">
<%--                    <tr class="rowB">
                        <td class="Cell">Supervisores relacionados *
                            <asp:CustomValidator ID="cvSupervInterv" runat="server" ClientValidationFunction="ValidaSupervInterv" Enabled="false" ErrorMessage="Campo Requerido" ForeColor="Red" ValidationGroup="btnAprobar"></asp:CustomValidator>
                        </td>
                    </tr>--%>
<%--                    <tr class="rowA">
                        <td class="Cell" colspan="2">
                            &nbsp;</td>
                    </tr>--%>
                    <tr class="rowB">
                        <td class="Cell">Supervisores relacionados Modificación</td>
                        <td class="Cell">
                            <asp:TextBox ID="txtSuperInterv" Visible="false" runat="server" AutoPostBack="true"  OnTextChanged="txtSuperIntervTextChanged" ViewStateMode="Enabled"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell" colspan="2">
                            <div id="dvSupervInterv" runat="server">
                                <asp:GridView ID="gvSupervInterv" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
           
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="Eliminar" OnClick="btnEliminar_Click" >
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td><asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    </asp:Content>
