﻿using System;
using System.IO;
using System.Net;

/// <summary>
/// Página genérica para descarga de archivos del FTP
/// </summary>
public partial class Page_OAC_DescargarArchivo_DescargarArchivo : System.Web.UI.Page
{
    masterPrincipal toolBar;

    protected void Page_Load(object sender, EventArgs e)
    {

        string nombreArchivo = Page.Request.QueryString["fname"];
        string subfolder = string.Empty;

        if (! string.IsNullOrEmpty(Request.QueryString["tipo"]))
            subfolder = Page.Request.QueryString["tipo"]; 

        string archivoLocal = DownloadFromFTP(nombreArchivo,subfolder);
        if (archivoLocal != string.Empty)
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(archivoLocal);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo);
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(archivoLocal);
            Response.End();
        }
        else
        {
            throw new System.Exception("Error al descargar el archivo");
        }


    }
    private string DownloadFromFTP(string ftpFileName, string subFolder = "")
    {
        string ftpServerIP = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" + System.Configuration.ConfigurationManager.AppSettings["FtpPort"];
        string ftpUserID = System.Configuration.ConfigurationManager.AppSettings["FtpUser"];
        string ftpPassword = System.Configuration.ConfigurationManager.AppSettings["FtpPass"];
        string filePath = System.Configuration.ConfigurationManager.AppSettings["LocalPath"];

        if (!Directory.Exists(filePath))
        {
            string errMsg = "No existe al ruta Temporal en el equipo ";
            return errMsg;
        }

        FtpWebRequest reqFTP;
        try
        {
            FileStream outputStream = new FileStream(filePath + ftpFileName, FileMode.Create);
            if(string.IsNullOrEmpty(subFolder))
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpServerIP + "/" + ftpFileName));
            else
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpServerIP + "/"+subFolder + "/" + ftpFileName));

            reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            Stream ftpStream = response.GetResponseStream();
            long cl = response.ContentLength;
            int bufferSize = 2048;
            int readCount;
            byte[] buffer = new byte[bufferSize];
            readCount = ftpStream.Read(buffer, 0, bufferSize);
            while (readCount > 0)
            {
                outputStream.Write(buffer, 0, readCount);
                readCount = ftpStream.Read(buffer, 0, bufferSize);
            }
            ftpStream.Close();
            outputStream.Close();
            response.Close();
            return filePath + ftpFileName;
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

        return string.Empty;
    }
}