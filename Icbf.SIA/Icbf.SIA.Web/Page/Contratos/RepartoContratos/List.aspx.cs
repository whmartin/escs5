using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de consulta a través de filtros para la entidad Contratos
/// </summary>
public partial class Page_RepartoContratos_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RepartoContratos";
    SIAService vRuboService = new SIAService();
    ContratoService vContratoService = new ContratoService();
    private string CodContratoAsociadoSeleccionado;
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../General/Inicio/Default.aspx");
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        txtNumeroProceso.Text = string.Empty;
        txtNumeroContrato.Text = string.Empty;
        txtIdContrato.Text = string.Empty;
        ((TextBox) txtFechaRegistroSistemaDesde.FindControl("txtFecha")).Text = string.Empty;
        ((TextBox)txtFechaRegistroSistemaHasta.FindControl("txtFecha")).Text = string.Empty;
        ddlIDCategoriaContrato.SelectedIndex = 0;
        ddlIDEstadoContraro.SelectedIndex = 0;
        ddlIDModalidadSeleccion.SelectedIndex = 0;
        ddlIDRegional.SelectedIndex = 0;
        ddlIDTipoContrato.SelectedIndex = 0;
        ddlVigenciaFiscalinicial.SelectedIndex = 0;
        ddlIDTipoContrato.Enabled = false;

    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

        /// <summary>
        /// Método que realiza la búsqueda filtrada con múltiples criterios 
        /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvContratos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoLimpiar += new ToolBarDelegate(btnLimpiar_Click);
            gvContratos.PageSize = PageSize();
            gvContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Contratos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

        /// <summary>
        /// Método para redirigir a la página detalle del registro seleccionado 
        /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValueIdContrato = string.Empty;
            string strValueEstadoContrato = string.Empty;
            string strValueUsuarioCrea = string.Empty;

            if (gvContratos.DataKeys[rowIndex].Values["IdContrato"]!= null)
            {
                strValueIdContrato = gvContratos.DataKeys[rowIndex].Values["IdContrato"].ToString();    
            }
            if (gvContratos.DataKeys[rowIndex].Values["IdEstadoContrato"] !=null)
            {
                strValueEstadoContrato = gvContratos.DataKeys[rowIndex].Values["IdEstadoContrato"].ToString();  
            }

            if (gvContratos.DataKeys[rowIndex].Values["UsuarioCrea"] != null)
            {
                strValueUsuarioCrea = gvContratos.DataKeys[rowIndex].Values["UsuarioCrea"].ToString();
            }
           
            SetSessionParameter("Contrato.IdContrato", strValueIdContrato);
            
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContratos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContratos.SelectedRow);
    }

    protected void gvContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;
            
            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvContratos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        string controlFechaValidando = string.Empty;
        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            DateTime? vFechaRegistroSistemaDesde = null;
            DateTime? vFechaRegistroSistemaHasta = null;
            int? vIdContrato = null;
            String vNumeroContrato = null;
            String vNumeroProceso = null;
            int? vVigenciaFiscalinicial = null;
            int? vIDRegional = null;
            int? vIDModalidadSeleccion = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            int? vIDEstadoContraro = null;

            controlFechaValidando = "Fecha Registro al Sistema Desde";
            if (txtFechaRegistroSistemaDesde.Date.Year.ToString() != "1900")
            {
                vFechaRegistroSistemaDesde = Convert.ToDateTime(txtFechaRegistroSistemaDesde.Date);
            }
            controlFechaValidando = string.Empty;

            controlFechaValidando = "Fecha Registro al Sistema Hasta";
            if (txtFechaRegistroSistemaHasta.Date.Year.ToString() != "1900")
            {
                vFechaRegistroSistemaHasta = Convert.ToDateTime(txtFechaRegistroSistemaHasta.Date);
            }
            controlFechaValidando = string.Empty;
            if (vFechaRegistroSistemaHasta != null)
            {

                TimeSpan ts = new TimeSpan(0, 23, 59, 59);

                vFechaRegistroSistemaHasta = Convert.ToDateTime(vFechaRegistroSistemaHasta) + ts;
            }
            
            if (txtIdContrato.Text!= "")
            {
                vIdContrato = Convert.ToInt32(txtIdContrato.Text);
            }
            if (txtNumeroContrato.Text!= "")
            {
                vNumeroContrato = Convert.ToString(txtNumeroContrato.Text);
            }
            if (txtNumeroProceso.Text!= "")
            {
                vNumeroProceso = Convert.ToString(txtNumeroProceso.Text);
            }
            if (ddlVigenciaFiscalinicial.SelectedValue!= "-1")
            {
                vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            }
            if (ddlIDRegional.SelectedValue!= "-1")
            {
                vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            }
            if (ddlIDModalidadSeleccion.SelectedValue!= "-1")
            {
                vIDModalidadSeleccion = Convert.ToInt32(ddlIDModalidadSeleccion.SelectedValue);
            }
            if (ddlIDCategoriaContrato.SelectedValue!= "-1")
            {
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
            }
            if (ddlIDTipoContrato.SelectedValue!= "-1")
            {
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
            }
            if (ddlIDEstadoContraro.SelectedValue!= "-1")
            {
                vIDEstadoContraro = Convert.ToInt32(ddlIDEstadoContraro.SelectedValue);
            }
            var myGridResults = vContratoService.ConsultarContratoss(vFechaRegistroSistemaDesde, vFechaRegistroSistemaHasta, vIdContrato, vNumeroContrato, vNumeroProceso, vVigenciaFiscalinicial, vIDRegional, vIDModalidadSeleccion, vIDCategoriaContrato, vIDTipoContrato, vIDEstadoContraro,null,null,null,null,null,null);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////
            int nRegistros = myGridResults.Count;
            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (nRegistros < NumRegConsultaGrilla)
            {
                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(Contrato), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression = Expression.Lambda<Func<Contrato, object>>(Expression.Convert(prop, typeof(object)), param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults;
                }
                gridViewsender.DataBind();
            }
            else
            {
                toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
            }

            
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inválido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Contratos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Contratos.Eliminado");
            CargarListaVigencia();
            CargarListaRegional();
            LlenarModalidadSeleccion();
            CargarListaEstadoContrato();
            LlenarCategoriaContrato();
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaEstadoContrato()
    {
        ManejoControlesContratos.LlenarComboLista(ddlIDEstadoContraro, vContratoService.ConsultarEstadoContrato(null,null,null,true), "IdEstadoContrato", "Descripcion");
    }

    public void CargarListaVigencia()
    {

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
            }

        }

    }

    public void LlenarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            // Página 1 Descripción adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion

            //switch (CodContratoAsociadoSeleccionado) // Se asigna previamente en SeleccionadoContratoAsociado 
            //{
            //    case CodContratoConvenioAdhesion:
            //        ddlIDTipoContrato.Enabled = false;
            //        ddlIDTipoContrato.SelectedValue = IdTipoContAporte; // Página 1 Descripción adicional Tipo Contrato Convenio
            //        break;
            //    default:
            //        ddlIDTipoContrato.Enabled = true; //Página 11 Paso 9 
            //        break;
            //}
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }

        
    }
}
