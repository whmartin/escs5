<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_RepartoContratos_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Fecha Registro al Sistema Desde
            </td>
            <td class="Cell">
                Fecha Registro al Sistema hasta
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaDesde" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaHasta" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Id Contrato/Convenio</td>
            <td class="Cell">
                Número del Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                N&uacute;mero de Proceso
            </td>
            <td class="Cell">
                Vigencia Fiscal Inicial
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtNumeroProceso" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroProceso" runat="server" TargetControlID="txtNumeroProceso"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio</td>
            <td class="Cell">
                Modalidad de Selecci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDModalidadSeleccion"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Categoria del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged" 
                    AutoPostBack="True"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td  class="Cell" >
                Estado del Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDEstadoContraro"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,IdEstadoContrato,UsuarioCrea" CellPadding="0" Height="16px"
                        OnSorting="gvContratos_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContratos_PageIndexChanging" OnSelectedIndexChanged="gvContratos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro al Sistema" DataField="FechaCrea"  SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Id Contrato/Convenio" DataField="IdContrato"  SortExpression="IdContrato"/>
                            <asp:BoundField HeaderText="Número del Contrato/Convenio" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional del contrato/convenio" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Número de Proceso" DataField="NumeroProceso"  SortExpression="NumeroProceso"/>
                            <asp:BoundField HeaderText="Modalidad de Selección" DataField="NombreModalidadSeleccion"  SortExpression="NombreModalidadSeleccion"/>
                            <asp:BoundField HeaderText="Categoria del Contrato/Convenio" DataField="NombreCategoriaContrato"  SortExpression="NombreCategoriaContrato"/>
                            <asp:BoundField HeaderText="Tipo de Contrato/Convenio" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                            <asp:BoundField HeaderText="Estado de Contrato/Convenio" DataField="NombreEstadoContrato"  SortExpression="NombreEstadoContrato"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
