using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_SolicitudesModificacion_ListDetalle : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesModificacion";
    ContratoService vTipoSolicitudService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                gvTipoSolicitud.EmptyDataText = EmptyDataText();
                gvTipoSolicitud.PageSize = PageSize();
                gvSolRechazadas.EmptyDataText = EmptyDataText();
                gvSolRechazadas.PageSize = PageSize();
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            NavigateTo(SolutionPage.List);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        var strValue = hfIdTContrato.Value.ToString();
        SetSessionParameter("Contrato.IdContrato", strValue);
        NavigateTo(SolutionPage.Add);
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonBuscar(true);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);


            toolBar.EstablecerTitulos("Solicitudes de Modificaciones del Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoSolicitud.DataKeys[rowIndex].Value.ToString();
            //string idContrato = hfIdTContrato.Value.ToString();
            //SetSessionParameter("Contrato.IdContrato", idContrato);
            SetSessionParameter("ConsModContractual.IDCosModContractual", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void SeleccionarRegistrosRechazados(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSolRechazadas.DataKeys[rowIndex].Value.ToString();
            //string idContrato = hfIdTContrato.Value.ToString();
            //SetSessionParameter("Contrato.IdContrato", idContrato);
            SetSessionParameter("ConsModContractual.IDCosModContractual", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoSolicitud_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoSolicitud.SelectedRow);
    }

    protected void gvTipoSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoSolicitud.PageIndex = e.NewPageIndex;
        ObtenerSolicitudes();
    }

    protected void gvSolRechazadas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistrosRechazados(gvSolRechazadas.SelectedRow);
    }

    protected void gvSolRechazadas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSolRechazadas.PageIndex = e.NewPageIndex;
        ObtenerSolicitudesRechazadas();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SolModContratual.CambioEstado") == "1")
            {
              RemoveSessionParameter("SolModContratual.CambioEstado");
              toolBar.MostrarMensajeGuardado("Se realizo el cambio de estado satisfactoriamente.");
            }

            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");

            var detalleContrato = vTipoSolicitudService.ConsultarContratoSolicitud(vIdContrato);

            txtCategoriaContratoConvenio.Text = detalleContrato.CategoriaContrato;
            txtIdContrato.Text = detalleContrato.NumeroContrato;
            txtRegional.Text = detalleContrato.NombreRegional;
            txtTipodeContratoConvenio.Text = detalleContrato.NombreTipoContrato;
            txtVigenciaFiscal.Text = detalleContrato.VigenciaFiscalInicial == 0 ? string.Empty : detalleContrato.VigenciaFiscalInicial.ToString();

            var solicitudesModificacionContrato = vTipoSolicitudService.ConsultarSolicitudesPorContrato(vIdContrato);
            var solModificacionesRechazadas = vTipoSolicitudService.ConsultarSolicitudesRechazadasPorContrato(vIdContrato);
            //solicitudesModificacionContrato = solicitudesModificacionContrato.Where(items => items.UsuarioCrea == GetSessionUser().NombreUsuario).ToList();

            hfIdTContrato.Value = vIdContrato.ToString();

            ArchivosContrato.LoadInfoContratos(vIdContrato);

            gvTipoSolicitud.PageSize = PageSize();
            gvTipoSolicitud.EmptyDataText = EmptyDataText();
            gvTipoSolicitud.DataSource = solicitudesModificacionContrato;
            gvTipoSolicitud.DataBind();


            gvSolRechazadas.PageSize = PageSize();
            gvSolRechazadas.EmptyDataText = EmptyDataText();
            gvSolRechazadas.DataSource = solModificacionesRechazadas;
            gvSolRechazadas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ObtenerSolicitudes()
    {
         try
        {
            int idOut = int.Parse(hfIdTContrato.Value.ToString()); 

            var solicitudesModificacionContrato = vTipoSolicitudService.ConsultarSolicitudesPorContrato(idOut);

            gvTipoSolicitud.DataSource = solicitudesModificacionContrato;
            gvTipoSolicitud.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ObtenerSolicitudesRechazadas()
    {
        try
        {
            int idOut = int.Parse(hfIdTContrato.Value.ToString());

            var solicitudesModificacionContrato = vTipoSolicitudService.ConsultarSolicitudesRechazadasPorContrato(idOut);

            gvSolRechazadas.DataSource = solicitudesModificacionContrato;
            gvSolRechazadas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
