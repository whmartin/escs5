using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;

public partial class Page_SolicitudesModificacion_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesModificacion";
    ContratoService vTipoSolicitudService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            //int? vIdContrato = null;
            String vNumeroContrato = null;
            int? vVigenciaFiscal = null;
            int? vRegional = null;
            String vDependenciaSolicitante = null;
            String vTipoIdentificacion = null;
            String vNumeroIdentificacion = null;
            int? vValorContratoDesde = null;
            int? vValorContratoHasta = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;


            if (txtNumeroContrato.Text!= "")
            {
                vNumeroContrato = Convert.ToString(txtNumeroContrato.Text);
            }
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
            {
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
            }
            if (ddlIDTipoContrato.SelectedValue != "-1" && ddlIDTipoContrato.SelectedValue != "")
            {
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
            }
            if (ddlRegional.SelectedValue != "-1")
            {
                vRegional = Convert.ToInt32(ddlRegional.SelectedValue);
            }
            if (txtNumeroIdentificacion.Text!= "")
            {
                vNumeroIdentificacion = Convert.ToString(txtNumeroIdentificacion.Text);
            }            
            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
            {
                vVigenciaFiscal = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            }

            if (vValorContratoDesde > vValorContratoHasta)
            {
                toolBar.MostrarMensajeError("Valores de contrato no v�lidos");
            }
            else
            {
                gvTipoSolicitud.DataSource = vTipoSolicitudService.ConsultarContratosSolicitud(vNumeroContrato, vVigenciaFiscal, vRegional, vIDCategoriaContrato, vIDTipoContrato, vNumeroIdentificacion, vValorContratoDesde, vValorContratoHasta);
                gvTipoSolicitud.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.MostrarBotonNuevo(false);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoBuscarModificacion += toolBar_eventoBuscarTercero;
            gvTipoSolicitud.PageSize = PageSize();
            gvTipoSolicitud.EmptyDataText = EmptyDataText();
            
            toolBar.EstablecerTitulos("Gesti&oacute;n de Solicitudes de modificaciones Contractuales", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoBuscarTercero(object sender, EventArgs e)
    {
        NavigateTo("ListSolicitudes.aspx");
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoSolicitud.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", strValue);
            NavigateTo("ListDetalle.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoSolicitud_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoSolicitud.SelectedRow);
    }

    protected void gvTipoSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoSolicitud.PageIndex = e.NewPageIndex;
        Buscar();
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoSolicitud.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoSolicitud.Eliminado");

            ManejoControles Controles = new ManejoControles();
            Controles.LlenarNombreRegional(ddlRegional, string.Empty, true);
            LlenarCategoriaContrato();
            CargarListaVigencia();
            gvTipoSolicitud.PageSize = PageSize();
            gvTipoSolicitud.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vTipoSolicitudService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void CargarListaVigencia()
    {
        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            // P�gina 1 Descripci�n adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vTipoSolicitudService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion

            //switch (CodContratoAsociadoSeleccionado) // Se asigna previamente en SeleccionadoContratoAsociado 
            //{
            //    case CodContratoConvenioAdhesion:
            //        ddlIDTipoContrato.Enabled = false;
            //        ddlIDTipoContrato.SelectedValue = IdTipoContAporte; // P�gina 1 Descripci�n adicional Tipo Contrato Convenio
            //        break;
            //    default:
            //        ddlIDTipoContrato.Enabled = true; //P�gina 11 Paso 9 
            //        break;
            //}
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }


    }
}
