using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Text;

public partial class Page_SolicitudesModificacion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesModificacion";
    ContratoService vTipoSolicitudService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private string nLugarEjecucion
    { set { hfLugarEjecucion.Value = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                string sControlName = Request.Params.Get("__EVENTTARGET");
                switch (sControlName)
                {

                    case "ActualizarEliminarProducto":
                        btnBuscar_Click(sender,e);
                        break;
                    
                }
            }
        }


    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        var strValue = hfIdTContrato.Value.ToString();
        SetSessionParameter("Contrato.IdContrato", strValue);
        NavigateTo(SolutionPage.Add);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        //var strValue = hfIdTContrato.Value.ToString();
        //SetSessionParameter("Contrato.IdContrato", strValue);

        var strValue1 = hfIdConsModContrato.Value.ToString();
        SetSessionParameter("ConsModContractual.IDCosModContractual", strValue1);

        var strValue2 = hfIdTContrato.Value.ToString();
        SetSessionParameter("Contrato.IdContrato", strValue2);

        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        var strValue = hfIdTContrato.Value.ToString();
        SetSessionParameter("Contrato.IdContrato", strValue);
        NavigateTo("ListDetalle.aspx");
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
            var idConsModContractual = int.Parse( hfIdConsModContrato.Value.ToString());

            var itemsMsjs = vTipoSolicitudService.ValidarCambiodeEstadoaEnviada(idConsModContractual);

            if (itemsMsjs == null || itemsMsjs.Count == 0)
            {
               var result =  vTipoSolicitudService.CambiarEstadoRegistroaEnviada(idConsModContractual);

               if (result > 0)
               {
                   var strValue = hfIdTContrato.Value.ToString();
                   SetSessionParameter("Contrato.IdContrato", strValue);
                   SetSessionParameter("SolModContratual.CambioEstado", "1");
                   NavigateTo("ListDetalle.aspx");
               }
            }
            else
            {
                StringBuilder texto = new StringBuilder();

                foreach (var item in itemsMsjs)
                    texto.AppendLine(item.Tipo + " - " + item.Descripcion);

                toolBar.MostrarMensajeError(texto.ToString().Replace("\n","<br/>"));
            } 
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        try
        {
            string vParametros =  "&vIdContrato=" + hfIdTContrato.Value + "&vIDCosModContractual=" + hfIdConsModContrato.Value;

            string returnValues = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                                    "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                                    "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                                    "   <script language='javascript'> " +
                                    "   function rebtnEliminar(url) { " +
                                    "       window_showModalDialog('' + url + '', setReturnGetbtnEliminar, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');" +
                                    "   }" +
                                    "   rebtnEliminar('../../../Page/Contratos/Lupas/LupaEliminarModificacion.aspx?" + vParametros + "')" +
                                    "   </script>";
            ClientScript.RegisterStartupScript(Page.GetType(), "rvRetornarbtnEliminar", returnValues);    


            }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatos()
    {
        try
        {
            if (GetSessionParameter("SolicitudesModificacion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SolicitudesModificacion.Guardado");

            int vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractual.IDCosModContractual");

            hfIdConsModContrato.Value = vIdConsModContractual.ToString();

            SolModContractual vSolContractual = new SolModContractual();
            vSolContractual = vTipoSolicitudService.ConsultarSolitud(vIdConsModContractual);

            hfIdTContrato.Value = vSolContractual.IdContrato.ToString();

            var detalleSolicitud = vTipoSolicitudService.ConsultarSolicitudesDetalle(vIdConsModContractual);

            var historicosSolicitud = vTipoSolicitudService.ConsultarSolicitudesHistorico(vIdConsModContractual);

            if (vSolContractual.Estado == "Registro" || vSolContractual.Estado == "Devuelta")
            {
                toolBar.MostrarBotonEditar(true);
                toolBar.MostrarBotonAprobar(true);
            }
            else
            {
                toolBar.MostrarBotonEditar(false);
                toolBar.MostrarBotonAprobar(false);

                if (vSolContractual.Estado == "Aceptada" && vSolContractual.TipoModificacion == "Cambio de Supervisor")
                    MultiViewSupervisores.ActiveViewIndex = -1;
            }

                       
            if(((ValidateAccessDelete(toolBar, "Contratos/SolicitudesEliminacion", SolutionPage.Add))== true)&&
                (vSolContractual.Estado == "Registro" || vSolContractual.Estado == "Enviada" || vSolContractual.Estado == "Aceptada" || vSolContractual.Estado == "Devuelta"))
               toolBar.MostrarBotonEliminar(true);              
            else
                toolBar.MostrarBotonEliminar(false);


            txtJustificacion.Text = vSolContractual.Justificacion;
            txtNumeroDocumento.Text = vSolContractual.NumeroDoc;
            txtFechaSolicitud.Text = vSolContractual.FechaSolicitud.ToShortDateString();
            txtIdContrato.Text = vSolContractual.NumeroContrato ?? String.Empty;
            txtNumeroSolicitud.Text = vSolContractual.IDCosModContractual.ToString();
            txtTiposModificaciones.Text = vSolContractual.TipoModificacion;

            lblEstado.Text = vSolContractual.Estado;

            gvDetalleSolicitud.DataSource = detalleSolicitud;
            gvDetalleSolicitud.EmptyDataText = EmptyDataText();
            gvDetalleSolicitud.PageSize = PageSize();
            gvDetalleSolicitud.DataBind();

            gvHistorico.DataSource = historicosSolicitud;
            gvHistorico.EmptyDataText = EmptyDataText();
            gvHistorico.PageSize = PageSize();
            gvHistorico.DataBind();

            foreach (var itemDetalle in detalleSolicitud)
            {
                switch (itemDetalle.TipoModificacion)
                {
                    case "Adición":
                        AccTiposModificacion.Panes[0].Visible = true;
                        gvAdicciones.DataSource = vTipoSolicitudService.ConsultarAdicioness(null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvAdicciones.EmptyDataText = EmptyDataText();
                        gvAdicciones.PageSize = PageSize();
                        gvAdicciones.DataBind();
                        break;
                    case "Prorroga":
                        AccTiposModificacion.Panes[1].Visible = true;
                        gvProrroga.DataSource = vTipoSolicitudService.ConsultarProrrogass(null, null, null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvProrroga.EmptyDataText = EmptyDataText();
                        gvProrroga.PageSize = PageSize();
                        gvProrroga.DataBind();
                        break;
                    case "Modificación":
                        AccTiposModificacion.Panes[2].Visible = true;
                        gvModificacionesContractuales.DataSource = vTipoSolicitudService.ConsultarModificacionObligacionXIDDetalleConsModContractual(itemDetalle.IDDetalleConsModContractual);
                        gvModificacionesContractuales.EmptyDataText = EmptyDataText();
                        gvModificacionesContractuales.PageSize = PageSize();
                        gvModificacionesContractuales.DataBind();
                        break;
                    case "Reducción de Valor":
                        AccTiposModificacion.Panes[3].Visible = true;
                        gvReduccionesValor.DataSource = vTipoSolicitudService.ConsultarReduccioness(null, null, itemDetalle.IDDetalleConsModContractual);
                        gvReduccionesValor.EmptyDataText = EmptyDataText();
                        gvReduccionesValor.PageSize = PageSize();
                        gvReduccionesValor.DataBind();
                        break;
                    case "Cesión":
                        AccTiposModificacion.Panes[4].Visible = true;
                        gvCesion.DataSource = vTipoSolicitudService.ConsultarCesioness(null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvCesion.EmptyDataText = EmptyDataText();
                        gvCesion.PageSize = PageSize();
                        gvCesion.DataBind();
                        break;
                    case "Suspensión":
                        AccTiposModificacion.Panes[5].Visible = true;
                        gvSuspencion.DataSource = vTipoSolicitudService.ConsultarSuspensionesPorDetalleModificacion(itemDetalle.IDDetalleConsModContractual);
                        gvSuspencion.EmptyDataText = EmptyDataText();
                        gvSuspencion.PageSize = PageSize();
                        gvSuspencion.DataBind();
                        break;
                    case "Liquidación de Contrato":
                        AccTiposModificacion.Panes[6].Visible = true;
                        gvLiquidacionContrato.DataSource = vTipoSolicitudService.ConsultarLiquidacionIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvLiquidacionContrato.EmptyDataText = EmptyDataText();
                        gvLiquidacionContrato.PageSize = PageSize();
                        gvLiquidacionContrato.DataBind();
                        break;
                    case "Imposición de Multas":
                        AccTiposModificacion.Panes[7].Visible = true;
                        gvImposicionMultas.DataSource = vTipoSolicitudService.ConsultarImposicionMultaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvImposicionMultas.EmptyDataText = EmptyDataText();
                        gvImposicionMultas.PageSize = PageSize();
                        gvImposicionMultas.DataBind();
                        break;
                    case "Terminación Anticipada":
                        AccTiposModificacion.Panes[8].Visible = true;
                        gvTerminacionAnticipada.DataSource = vTipoSolicitudService.ConsultarTerminacionAnticipadaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvTerminacionAnticipada.EmptyDataText = EmptyDataText();
                        gvTerminacionAnticipada.PageSize = PageSize();
                        gvTerminacionAnticipada.DataBind();
                        break;
                    case "Reducción de Tiempo":
                        AccTiposModificacion.Panes[9].Visible = true;
                        gvReduccion.DataSource = vTipoSolicitudService.ConsultarReduccionesTiempo(null, itemDetalle.IDDetalleConsModContractual);
                        gvReduccion.EmptyDataText = EmptyDataText();
                        gvReduccion.PageSize = PageSize();
                        gvReduccion.DataBind();
                        break;
                    case "Cambio Lugar de Ejecución":
                        AccTiposModificacion.Panes[10].Visible = true;

                        // gvReduccion.DataSource = vTipoSolicitudService.ConsultarReduccionesTiempo(null, itemDetalle.IDDetalleConsModContractual);
                        hfIdDetalleConsMod.Value = itemDetalle.IDDetalleConsModContractual.ToString();
                        List<CambioLugarEjecucion> lugaresContrato = vTipoSolicitudService.ConsultarCambiosLugaresEjecucion(itemDetalle.IDDetalleConsModContractual);
                        gvLugarEjecucion.DataSource = lugaresContrato;
                        gvLugarEjecucion.DataBind();
                        nLugarEjecucion = gvLugarEjecucion.Rows.Count.ToString();

                        dvLugarEjecucion.Style.Remove("height");
                        dvLugarEjecucion.Style.Remove("overflow-x");
                        dvLugarEjecucion.Style.Remove("overflow-y");

                        if (lugaresContrato.Any(x=>x.EsNivelNacional == true))
                        {

                            chkNivelNacional.Checked = true;                            
                            btnInfoLuEjecucion.Visible = true;

                        }
                        else
                        {
                            if (lugaresContrato.Count() > 2)
                            {
                                dvLugarEjecucion.Style.Add("height", "100px");
                                dvLugarEjecucion.Style.Add("overflow-x", "hidden");
                                dvLugarEjecucion.Style.Add("overflow-y", "scroll");
                                
                            }

                            if (lugaresContrato.Count() > 0)
                            {
                                btnInfoLuEjecucion.Visible = true;
                            }
                            chkNivelNacional.Checked = false;
                            chkNivelNacional.Enabled = false;
                        }
                       ;
                        break;
                    case "Cambio de Supervisor":
                        AccTiposModificacion.Panes[11].Visible = true;
                        List<SupervisorInterContrato> supervisoresInterventores = vTipoSolicitudService.ObtenerSupervisoresInterventoresContrato(vSolContractual.IdContrato, null);
                        if (supervisoresInterventores != null)
                        {
                            var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                            gvSupervisoresActuales.DataSource = querySupervisores;
                            gvSupervisoresActuales.DataBind();
                        }
                        List<SupervisorInterContrato> supervisoresTemporales = vTipoSolicitudService.ObtenerSupervisoresTemporalesContrato(vSolContractual.IdContrato, null);
                        gvSupervInterv.DataSource = supervisoresTemporales;
                        gvSupervInterv.DataBind();

                    break;
                }
            }

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSolContractual.UsuarioCrea, vSolContractual.FechaCrea, vSolContractual.UsuarioModifica, vSolContractual.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonBuscar(true);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Detalle de la Solicitud Modificación", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            //rblEstado.Items.Insert(0, new ListItem("Activa", "Activa"));
            //rblEstado.Items.Insert(1, new ListItem("Inactiva", "Inactiva"));

            //    var itemContrato = vTipoSolicitudService.ConsultarContratoSolicitud(vIdCntrato);
            //    txtIdContrato.Text = itemContrato.NumeroContrato.ToString();
            //    gvDetalleSolicitud.Visible = false;
            //    AccTiposModificacion.Visible = false;
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAdicciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvAdicciones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvAdicciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Adiciones.IdAdiccion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Adiciones/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        //var btn = sender as ImageButton;

        //string comando = btn.CommandName;

        //if (comando == "Add")
        //{

        //}
        //else if (comando == "Select")
        //{

        //}
    }

    protected void gvProrroga_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvProrroga.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvProrroga.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Prorrogas/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvReduccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReduccion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReduccion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ReduccionTiempo/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModificacionesContractuales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvModificacionesContractuales.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvModificacionesContractuales.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ModificacionObligacion.IdModObligacion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ModificacionObligacion/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvReduccionesValor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReduccionesValor.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReduccionesValor.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Reducciones.IdReduccion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ReduccionesValor/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvCesion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvCesion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvCesion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Cesiones.IdCesion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Cesiones/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSuspencion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvSuspencion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvSuspencion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Suspensiones.IdSuspension", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Suspenciones/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvLiquidacionContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvLiquidacionContrato.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvLiquidacionContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdLiquidacionContrato", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../LiquidacionContrato/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvImpsicionMultas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvImposicionMultas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvImposicionMultas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdImposicionMultas", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ProcesoImposicionMulta/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTerminacionAnticipada_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvTerminacionAnticipada.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvTerminacionAnticipada.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdTerminacionAnticipada", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../TerminacionAnticipada/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../CambioSupervision/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfoLuEjecucion_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetalleConsMod.Value.ToString());
            Response.Redirect("../CambioLugarEjecucion/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
