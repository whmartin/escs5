using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Text;

public partial class Page_SolicitudesModificacion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vTipoSolicitudService = new ContratoService();
    string PageName = "Contratos/SolicitudesModificacion";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    private string nLugarEjecucion
    { set { hfLugarEjecucion.Value = value; } }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        var strValue = hfIdTContrato.Value.ToString();
        SetSessionParameter("Contrato.IdContrato", strValue);
        NavigateTo("ListDetalle.aspx"); ;
    }

    private void Guardar()
    {
        try
        {
            DateTime fechavalidacion = new DateTime(1900, 1, 1);            
           
            var itemContrato = vTipoSolicitudService.ConsultarContrato(int.Parse(hfIdTContrato.Value));

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual.IDTipoModificacionContractual = int.Parse(ddlTiposModificaciones.SelectedValue.ToString());
            vConsModContractual.Justificacion = txtJustificacion.Text;
            vConsModContractual.Suscrito = false;
            vConsModContractual.ConsecutivoSuscrito = string.Empty;
            vConsModContractual.NumeroDoc = string.Empty;
            int idContrato = int.Parse(hfIdTContrato.Value.ToString());

            int vResultado = -1;

            if ( !string.IsNullOrEmpty(txtFechaSolicitud.Text))
            {                                       
                        
                vConsModContractual.FechaSolicitud = Convert.ToDateTime(txtFechaSolicitud.Text);

                if (Request.QueryString["oP"] == "E")
                {
                    int idConsModContractual = int.Parse(hfIdConsModContrato.Value);
                    var actual = vTipoSolicitudService.ConsultarConsModContractual(idConsModContractual);

                    if (actual.IDTipoModificacionContractual != vConsModContractual.IDTipoModificacionContractual)
                    {
                        bool existeTipoModificacion1 = vTipoSolicitudService.ConsultarExisteSolicitudModificacion(idContrato, vConsModContractual.IDTipoModificacionContractual);

                        if (!existeTipoModificacion1)
                        {
                            var itemsMsjs = vTipoSolicitudService.PuedeCrearSolicitud(idContrato, vConsModContractual.IDTipoModificacionContractual, idConsModContractual);

                            if (itemsMsjs == null || itemsMsjs.Count == 0)
                            {
                                vConsModContractual.UsuarioModifica = GetSessionUser().NombreUsuario;
                                vConsModContractual.IdConsModContractualesEstado = rblEstado.SelectedValue.ToString() == "Activa" ? 1 : 0;
                                vConsModContractual.IDCosModContractual = int.Parse(hfIdConsModContrato.Value);
                                InformacionAudioria(vConsModContractual, this.PageName, vSolutionPage);
                                vResultado = vTipoSolicitudService.ModificarSolModContractual(vConsModContractual);
                            }
                            else
                            {
                                StringBuilder texto = new StringBuilder();

                                foreach (var item in itemsMsjs)
                                    texto.AppendLine(item);

                                toolBar.MostrarMensajeError(texto.ToString().Replace("\n", "<br/>"));
                            }
                        }
                        else
                            toolBar.MostrarMensajeError("Actualmente ya existe un tipo de modificación: " + ddlTiposModificaciones.SelectedItem.Text + " en proceso, verifique por favor.");
                    }
                    else
                    {
                        vConsModContractual.UsuarioModifica = GetSessionUser().NombreUsuario;
                        vConsModContractual.IdConsModContractualesEstado = rblEstado.SelectedValue.ToString() == "Activa" ? 1 : 0;
                        vConsModContractual.IDCosModContractual = int.Parse(hfIdConsModContrato.Value);
                        InformacionAudioria(vConsModContractual, this.PageName, vSolutionPage);
                        vResultado = vTipoSolicitudService.ModificarSolModContractual(vConsModContractual);
                    }
                }
                else
                {
                    bool existeTipoModificacion = vTipoSolicitudService.ConsultarExisteSolicitudModificacion(idContrato, vConsModContractual.IDTipoModificacionContractual);

                    if (!existeTipoModificacion)
                    {
                        var itemsMsjs = vTipoSolicitudService.PuedeCrearSolicitud(idContrato, vConsModContractual.IDTipoModificacionContractual, 0);

                        if (itemsMsjs == null || itemsMsjs.Count == 0)
                        {
                            vConsModContractual.IDContrato = idContrato;
                            vConsModContractual.UsuarioCrea = GetSessionUser().NombreUsuario;
                            InformacionAudioria(vConsModContractual, this.PageName, vSolutionPage);
                            vResultado = vTipoSolicitudService.InsertarSolModContractual(vConsModContractual);
                        }
                        else
                        {
                            StringBuilder texto = new StringBuilder();

                            foreach (var item in itemsMsjs)
                                texto.AppendLine(item);

                            toolBar.MostrarMensajeError(texto.ToString().Replace("\n", "<br/>"));
                        }
                    }
                    else
                        toolBar.MostrarMensajeError("Actualmente ya existe un tipo de modificación: " + ddlTiposModificaciones.SelectedItem.Text + " en proceso, verifique por favor.");
                }                
               

            }
            else
            {
                toolBar.MostrarMensajeError("La Fecha de Solicitud no es Válida");
            }                   
            

            
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado >= 1)
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", vConsModContractual.IDCosModContractual);
                SetSessionParameter("SolicitudesModificacion.Guardado", "1");
                Response.Redirect("Detail.aspx",false);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.EstablecerTitulos( HttpUtility.HtmlEncode("Detalle Solicitud de Modificación"), SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractual.IDCosModContractual");

             hfIdConsModContrato.Value = vIdConsModContractual.ToString();
         
            SolModContractual vSolContractual = new SolModContractual();
             vSolContractual = vTipoSolicitudService.ConsultarSolitud(vIdConsModContractual);

             hfIdTContrato.Value = vSolContractual.IdContrato.ToString();
            

             var contrato = vTipoSolicitudService.ConsultarContrato(int.Parse(hfIdTContrato.Value));

             var detalleSolicitud = vTipoSolicitudService.ConsultarSolicitudesDetalle(vIdConsModContractual);

             var historicosSolicitud = vTipoSolicitudService.ConsultarSolicitudesHistorico(vIdConsModContractual);

             if (vSolContractual.Estado == "Inactiva")
                 rblEstado.SelectedValue = rblEstado.Items.FindByText(vSolContractual.Estado).Value;
             else
                 rblEstado.SelectedValue = rblEstado.Items.FindByText("Activa").Value;

             txtJustificacion.Text = vSolContractual.Justificacion;
             txtFechaSolicitud.Text = vSolContractual.FechaSolicitud.ToShortDateString();
             txtIdContrato.Text = vSolContractual.NumeroContrato ?? string.Empty;
             txtNumeroSolicitud.Text = vSolContractual.IDCosModContractual.ToString();

             ddlTiposModificaciones.SelectedValue = vSolContractual.IDTipoModificacionContractual.ToString();

             bool puedeModificar = vTipoSolicitudService.ConsultarPuedeModificarTipoSolicitud(vIdConsModContractual);

             if (!puedeModificar)
             {
                 ddlTiposModificaciones.Enabled = false;
                 //rblEstado.Enabled = false;
             }

             gvDetalleSolicitud.DataSource = detalleSolicitud;
             gvDetalleSolicitud.EmptyDataText = EmptyDataText();
             gvDetalleSolicitud.PageSize = PageSize();
             gvDetalleSolicitud.DataBind();

             gvHistorico.DataSource = historicosSolicitud;
             gvHistorico.EmptyDataText = EmptyDataText();
             gvHistorico.PageSize = PageSize();
             gvHistorico.DataBind();


             foreach (var itemDetalle in detalleSolicitud)
             {
                 switch (itemDetalle.TipoModificacion)
                 {
                     case "Adición":
                     AccTiposModificacion.Panes[0].Visible = true;
                     gvAdicciones.DataSource = vTipoSolicitudService.ConsultarAdicioness(null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                     gvAdicciones.EmptyDataText = EmptyDataText();
                     gvAdicciones.PageSize = PageSize();
                     gvAdicciones.DataBind();
                     break;
                     case "Prorroga":
                     AccTiposModificacion.Panes[1].Visible = true;
                     gvProrroga.DataSource = vTipoSolicitudService.ConsultarProrrogass(null, null, null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                     gvProrroga.EmptyDataText = EmptyDataText();
                     gvProrroga.PageSize = PageSize();
                     gvProrroga.DataBind();
                     break;
                     case "Modificación":
                     AccTiposModificacion.Panes[2].Visible = true;
                     gvModificacionesContractuales.DataSource = vTipoSolicitudService.ConsultarModificacionObligacionXIDDetalleConsModContractual(itemDetalle.IDDetalleConsModContractual);
                     gvModificacionesContractuales.EmptyDataText = EmptyDataText();
                     gvModificacionesContractuales.PageSize = PageSize();
                     gvModificacionesContractuales.DataBind();
                     break;
                     case "Reducción de Valor":
                     AccTiposModificacion.Panes[3].Visible = true;
                     gvReduccionesValor.DataSource = vTipoSolicitudService.ConsultarReduccioness(null, null, itemDetalle.IDDetalleConsModContractual);
                     gvReduccionesValor.EmptyDataText = EmptyDataText();
                     gvReduccionesValor.PageSize = PageSize();
                     gvReduccionesValor.DataBind();
                     break;
                     case "Cesión":
                     AccTiposModificacion.Panes[4].Visible = true;
                     gvCesion.DataSource = vTipoSolicitudService.ConsultarCesioness(null, null, null, itemDetalle.IDDetalleConsModContractual);
                     gvCesion.EmptyDataText = EmptyDataText();
                     gvCesion.PageSize = PageSize();
                     gvCesion.DataBind();
                     break;
                     case "Suspensión":
                     AccTiposModificacion.Panes[5].Visible = true;
                     gvSuspencion.DataSource = vTipoSolicitudService.ConsultarSuspensionesPorDetalleModificacion(itemDetalle.IDDetalleConsModContractual);
                     gvSuspencion.EmptyDataText = EmptyDataText();
                     gvSuspencion.PageSize = PageSize();
                     gvSuspencion.DataBind();
                     break;
                     case "Liquidación de Contrato":
                     AccTiposModificacion.Panes[6].Visible = true;
                     gvLiquidacionContrato.DataSource = vTipoSolicitudService.ConsultarLiquidacionIdDetalle(itemDetalle.IDDetalleConsModContractual);
                     gvLiquidacionContrato.EmptyDataText = EmptyDataText();
                     gvLiquidacionContrato.PageSize = PageSize();
                     gvLiquidacionContrato.DataBind();
                     break;
                     case "Imposición de Multas":
                     AccTiposModificacion.Panes[7].Visible = true;
                     gvImposicionMultas.DataSource = vTipoSolicitudService.ConsultarImposicionMultaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                     gvImposicionMultas.EmptyDataText = EmptyDataText();
                     gvImposicionMultas.PageSize = PageSize();
                     gvImposicionMultas.DataBind();
                     break;
                     case "Terminación Anticipada":
                     AccTiposModificacion.Panes[8].Visible = true;
                     gvTerminacionAnticipada.DataSource = vTipoSolicitudService.ConsultarTerminacionAnticipadaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                     gvTerminacionAnticipada.EmptyDataText = EmptyDataText();
                     gvTerminacionAnticipada.PageSize = PageSize();
                     gvTerminacionAnticipada.DataBind();
                     break;
                     case "Reducción de Tiempo":
                     AccTiposModificacion.Panes[9].Visible = true;
                     gvReduccion.DataSource = vTipoSolicitudService.ConsultarReduccionesTiempo(null, itemDetalle.IDDetalleConsModContractual);
                     gvReduccion.EmptyDataText = EmptyDataText();
                     gvReduccion.PageSize = PageSize();
                     gvReduccion.DataBind();
                     break;
                    case "Cambio Lugar de Ejecución":
                    AccTiposModificacion.Panes[10].Visible = true;
                        hfIdDetalleConsMod.Value = itemDetalle.IDDetalleConsModContractual.ToString();
                        List<CambioLugarEjecucion> lugaresContrato = vTipoSolicitudService.ConsultarCambiosLugaresEjecucion(itemDetalle.IDDetalleConsModContractual);
                        gvLugarEjecucion.DataSource = lugaresContrato;
                        gvLugarEjecucion.DataBind();
                        nLugarEjecucion = gvLugarEjecucion.Rows.Count.ToString();

                        dvLugarEjecucion.Style.Remove("height");
                        dvLugarEjecucion.Style.Remove("overflow-x");
                        dvLugarEjecucion.Style.Remove("overflow-y");

                        if (lugaresContrato.Any(x => x.EsNivelNacional == true))
                        {

                            chkNivelNacional.Checked = true;
                            btnInfoLuEjecucion.Visible = true;

                        }
                        else
                        {
                            if (lugaresContrato.Count() > 2)
                            {
                                dvLugarEjecucion.Style.Add("height", "100px");
                                dvLugarEjecucion.Style.Add("overflow-x", "hidden");
                                dvLugarEjecucion.Style.Add("overflow-y", "scroll");

                            }

                            if (lugaresContrato.Count() > 0)
                            {
                                btnInfoLuEjecucion.Visible = true;
                            }
                            chkNivelNacional.Checked = false;
                            chkNivelNacional.Enabled = false;
                        }
                       ;
                    break;
                    case "Cambio de Supervisor":
                     AccTiposModificacion.Panes[11].Visible = true;
                     List<SupervisorInterContrato> supervisoresInterventores = vTipoSolicitudService.ObtenerSupervisoresInterventoresContrato(vSolContractual.IdContrato, null);
                     if (supervisoresInterventores != null)
                     {
                         var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                         gvSupervisoresActuales.EmptyDataText = EmptyDataText();
                         gvSupervisoresActuales.PageSize = PageSize();
                         gvSupervisoresActuales.DataSource = querySupervisores;
                         gvSupervisoresActuales.DataBind();
                     }
                     List<SupervisorInterContrato> supervisoresTemporales = vTipoSolicitudService.ObtenerSupervisoresTemporalesContrato(vSolContractual.IdContrato,null);
                     if (supervisoresTemporales != null && supervisoresTemporales.Count() > 0)
                     {
                         gvSupervInterv.EmptyDataText = EmptyDataText();
                         gvSupervInterv.PageSize = PageSize();
                         gvSupervInterv.DataSource = supervisoresTemporales;
                         gvSupervInterv.DataBind();
                         btnInfo.Visible = true;
                     }
                     else
                         btnInfo.Visible = false;
                     break;
                 }
             }

             ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSolContractual.UsuarioCrea, vSolContractual.FechaCrea, vSolContractual.UsuarioModifica, vSolContractual.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {

            if (GetSessionParameter("DetConsModContractual.Guardado") == "1")
                toolBar.MostrarMensajeGuardado("Se guardo el registro correctamente");

            RemoveSessionParameter("DetConsModContractual.Guardado");
            rblEstado.Items.Insert(0, new ListItem("Activa", "Activa"));
            rblEstado.Items.Insert(1, new ListItem("Inactiva", "Inactiva"));

            rblEstado.SelectedValue = rblEstado.Items.FindByText("Activa").Value;

            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdTContrato.Value = vIdCntrato.ToString();

            if (Request.QueryString["oP"] != "E")
            {
                rblEstado.Enabled = false;
                var itemContrato = vTipoSolicitudService.ConsultarContratoSolicitud(vIdCntrato);
                txtIdContrato.Text = itemContrato.NumeroContrato ?? string.Empty;
                gvDetalleSolicitud.Visible = false;
                AccTiposModificacion.Visible = false;
            }

            var contrato = vTipoSolicitudService.ConsultarContrato(int.Parse(hfIdTContrato.Value));

            var solicitud = vTipoSolicitudService.ConsultarUltimaSolitud(int.Parse(hfIdTContrato.Value));

            bool esContratoSinValor = vTipoSolicitudService.ConsultarSiesContratoSV(int.Parse(hfIdTContrato.Value));


            var misTiposModificaciones = vTipoSolicitudService.ConsultarTipoModificaciones(null, null, null, true, null);

            if(esContratoSinValor == true)
            {
                ddlTiposModificaciones.DataSource = misTiposModificaciones.Where(e => e.Codigo != "TM1" && e.Codigo != "TM5"); //misTiposModificaciones.Where(e => e.Codigo != "TM1" && e.Codigo != "TM5" && e.Codigo != "TM7");
                ddlTiposModificaciones.DataTextField = "Descripcion";
                ddlTiposModificaciones.DataValueField = "IdTipoModificacionBasica";
                ddlTiposModificaciones.DataBind();

            }
            else
            {
                ddlTiposModificaciones.DataSource = misTiposModificaciones; //misTiposModificaciones.Where(e => e.Codigo != "TM7");
                ddlTiposModificaciones.DataTextField = "Descripcion";
                ddlTiposModificaciones.DataValueField = "IdTipoModificacionBasica";
                ddlTiposModificaciones.DataBind();
            }                     

            if (string.IsNullOrEmpty(solicitud.FechaSolicitud.ToString()))
            {
                if (solicitud.FechaSolicitud > contrato.FechaInicioEjecucion)
                    CalendarExtenderFechaSolicitud.StartDate = solicitud.FechaSolicitud;
                else
                    CalendarExtenderFechaSolicitud.StartDate = contrato.FechaInicioEjecucion;
            }
            else
                CalendarExtenderFechaSolicitud.StartDate = contrato.FechaInicioEjecucion;

            if (contrato.FechaFinalTerminacionContrato.HasValue)
            {
                if (contrato.FechaFinalTerminacionContrato.Value.Date < DateTime.Now.Date)
                {
                    //var modificaciones = misTiposModificaciones.Where(e => e.Codigo == "TM7");
                    //ddlTiposModificaciones.DataSource = modificaciones;
                    //ddlTiposModificaciones.DataTextField = "Descripcion";
                    //ddlTiposModificaciones.DataValueField = "IdTipoModificacionBasica";
                    //ddlTiposModificaciones.DataBind();
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAdicciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        { 
           GridViewRow row = gvAdicciones.SelectedRow;
           int rowIndex = row.RowIndex;
           string strValue = gvAdicciones.DataKeys[rowIndex].Value.ToString();
           SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
           SetSessionParameter("Adiciones.IdAdiccion", strValue);
           SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
           Response.Redirect("../Adiciones/Detail.aspx",false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
   
    protected void gvProrroga_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvProrroga.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvProrroga.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Prorrogas/Detail.aspx",false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvReduccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReduccion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReduccion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ReduccionTiempo/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModificacionesContractuales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvModificacionesContractuales.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvModificacionesContractuales.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ModificacionObligacion.IdModObligacion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ModificacionObligacion/Detail.aspx",false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvReduccionesValor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReduccionesValor.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReduccionesValor.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Reducciones.IdReduccion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ReduccionesValor/Detail.aspx",false);    
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvCesion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvCesion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvCesion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Cesiones.IdCesion", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Cesiones/Detail.aspx", false);     
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvSuspencion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvSuspencion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvSuspencion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Suspensiones.IdSuspension", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../Suspenciones/Detail.aspx",false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvLiquidacionContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvLiquidacionContrato.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvLiquidacionContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdLiquidacionContrato", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../LiquidacionContrato/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvImpsicionMultas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvImposicionMultas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvImposicionMultas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdImposicionMultas", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../ProcesoImposicionMulta/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTerminacionAnticipada_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvTerminacionAnticipada.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvTerminacionAnticipada.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdTerminacionAnticipada", strValue);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../TerminacionAnticipada/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDetalleSolicitud_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvDetalleSolicitud.SelectedRow);
    }

    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            Response.Redirect("../CambioSupervision/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfoLuEjecucion_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetalleConsMod.Value.ToString());
            Response.Redirect("../CambioLugarEjecucion/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;

            string strValue = gvDetalleSolicitud.DataKeys[rowIndex].Value.ToString();

            var tipoModificacion = gvDetalleSolicitud.Rows[rowIndex].Cells[1].Text;

            tipoModificacion = HttpUtility.HtmlDecode(tipoModificacion);

            bool isValid = true;
            string paginaNavegacion = string.Empty;

            switch (tipoModificacion)
            {
                case "Adición":
                if(gvAdicciones.Rows == null || gvAdicciones.Rows.Count == 0 )
                   paginaNavegacion = "../Adiciones/Add.aspx";
                else
                {
                    toolBar.MostrarMensajeError("Ya existe una Adición asociada a la solicitud.");
                    isValid = false;
                }
                break;
                case "Prorroga":
                if (gvProrroga.Rows == null || gvProrroga.Rows.Count == 0)
                   paginaNavegacion = "../Prorrogas/Add.aspx";
                else
                {
                    toolBar.MostrarMensajeError("Ya existe una prorroga asociada a la solicitud.");
                    isValid = false;
                }
                break;
                case "Reducción de Tiempo":
                    if (gvReduccion.Rows == null || gvReduccion.Rows.Count == 0)
                        paginaNavegacion = "../ReduccionTiempo/Add.aspx";
                    else
                    {
                        toolBar.MostrarMensajeError("Ya existe una prorroga asociada a la solicitud.");
                        isValid = false;
                    }
                    break;
                case "Modificación":
                      paginaNavegacion = "../ModificacionObligacion/Add.aspx";
                break;
                case "Suspensión":
                if (gvSuspencion.Rows == null || gvSuspencion.Rows.Count == 0)
                   paginaNavegacion = "../Suspenciones/Add.aspx";
                else
                {
                    toolBar.MostrarMensajeError("Ya existe una Suspención asociada a la solicitud.");
                    isValid = false;
                }
                break;
                case "Reducción de Valor":
                    if (gvReduccionesValor.Rows == null || gvReduccionesValor.Rows.Count == 0)
                        paginaNavegacion = "../ReduccionesValor/Add.aspx";
                    else
                    {
                        toolBar.MostrarMensajeError("Ya existe una reducción de valor asociada a la solicitud.");
                        isValid = false;
                    }
                    break;                 
                case "Cesión":
                if (gvCesion.Rows == null || gvCesion.Rows.Count == 0)
                   paginaNavegacion = "../Cesiones/Add.aspx";   
                else
                {
                    toolBar.MostrarMensajeError("Ya existe una Cesión asociada a la solicitud.");
                    isValid = false;
                }
                break;
                case "Imposición de Multas":
                if(gvImposicionMultas.Rows == null || gvImposicionMultas.Rows.Count == 0)
                    paginaNavegacion = "../ProcesoImposicionMulta/Add.aspx";   
                else
                {
                    toolBar.MostrarMensajeError("Ya existe un proceso de imposición de multas asociado a la solicitud.");
                    isValid = false;
                }
                break;
                case "Liquidación de Contrato":
                if (gvLiquidacionContrato.Rows == null || gvLiquidacionContrato.Rows.Count == 0)
                    paginaNavegacion = "../LiquidacionContrato/Add.aspx";
                else
                {
                    toolBar.MostrarMensajeError("Ya existe una liquidación de contrato asociada a la solicitud.");
                    isValid = false;
                }
                break;
                case "Terminación Anticipada":
                if (gvImposicionMultas.Rows == null || gvImposicionMultas.Rows.Count == 0)
                    paginaNavegacion = "../TerminacionAnticipada/Add.aspx";
                else
                {
                    toolBar.MostrarMensajeError("Ya existe una terminación anticipada asociada a la solicitud.");
                    isValid = false;
                }
                break;
                case "Cambio Lugar de Ejecución":
                    if ((gvLugarEjecucion.Rows == null || gvLugarEjecucion.Rows.Count == 0) && chkNivelNacional.Checked != true)
                        paginaNavegacion = "../CambioLugarEjecucion/Add.aspx";
                    else
                    {
                        toolBar.MostrarMensajeError("Ya existe un Cambio de Lugar de Ejecución asociada a la solicitud.");
                        isValid = false;
                    }
                break;
                case "Cambio de Supervisor":
                if (gvSupervInterv.Rows == null || gvSupervInterv.Rows.Count == 0)
                    paginaNavegacion = "../CambioSupervision/Add.aspx";
                else
                {
                    toolBar.MostrarMensajeError("Ya existe un cambio de supervisor asociado a la solicitud.");
                    isValid = false;
                }
                break;
                default:
                break;
            }

            if (isValid)
            {
                SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
                SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", strValue);
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContrato.Value.ToString());
                Response.Redirect(paginaNavegacion, false);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void txtFechaSolicitud_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        
        DateTime fechavalidacion = new DateTime(1900, 1, 1);    
        

        var itemContrato = vTipoSolicitudService.ConsultarContrato(int.Parse(hfIdTContrato.Value));            

        if (!txtFechaSolicitud.Text.StartsWith("_") && !string.IsNullOrEmpty(txtFechaSolicitud.Text) && DateTime.TryParse(txtFechaSolicitud.Text, out fechavalidacion) )
        {
            DateTime FechaSolicitud = Convert.ToDateTime(txtFechaSolicitud.Text);

            if (FechaSolicitud >= itemContrato.FechaInicioEjecucion)
            {
                if (
                     ddlTiposModificaciones.SelectedItem.Text != "Liquidación de Contrato" && 
                     ddlTiposModificaciones.SelectedItem.Text != "Imposición de Multas" &&
                     ddlTiposModificaciones.SelectedItem.Text != "Cambio de Supervisor" &&
                     FechaSolicitud > itemContrato.FechaFinalTerminacionContrato
                   )
                {
                    txtFechaSolicitud.Text = string.Empty;
                    toolBar.MostrarMensajeError("Fecha de Solicitud de la modificación no es Válida debe ser menor a la fecha de terminación del Contrato");
                }
            }
            else
            {
                txtFechaSolicitud.Text = string.Empty;
                toolBar.MostrarMensajeError("La Fecha de Solicitud no es Válida debe ser mayor a la fecha de Inicio de Ejecución");

            }

        }
        else
        {
            txtFechaSolicitud.Text = string.Empty;
            toolBar.MostrarMensajeError("La Fecha de Solicitud no es Válida");

        }


    }
}

