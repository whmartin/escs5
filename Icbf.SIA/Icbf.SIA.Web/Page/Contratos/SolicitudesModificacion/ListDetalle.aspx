<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListDetalle.aspx.cs" Inherits="Page_SolicitudesModificacion_ListDetalle" %>

<%@ Register Src="~/General/General/Control/ArchivosContrato.ascx" TagPrefix="uc1" TagName="ArchivosContrato" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

        <link href="../../../Styles/TabContainer.css" rel="stylesheet" type="text/css" />



    <asp:HiddenField ID="hfIdTContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&uacute;mero de Contrato/Convenio
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato" Enabled="False"  ></asp:TextBox>
            </td>
            <td>

            </td>
        </tr>
                <tr class="rowB">
            <td>
                Vigenc&iacute;a Fiscal Incial
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowA">
            <td>
                                <asp:TextBox runat="server" ID="txtVigenciaFiscal" Enabled="False"> </asp:TextBox>
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Enabled="False" Width="500px"  ></asp:TextBox>
            </td>
            <td>

            </td>
        </tr>
                <tr class="rowB">
            <td>
                Categor&iacute;a de Contrato/Convenio
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCategoriaContratoConvenio" Width="500px" Enabled="False"  ></asp:TextBox>
            </td>
            <td>

            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Tipo de Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtTipodeContratoConvenio" Width="500px" Enabled="False"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <br />

        <table width="90%" align="center">
        <tr>
            <td>
        <Ajax:TabContainer runat="server"  CssClass="ajax__tab_green-theme" Width="100%"  ActiveTabIndex="0">
        <Ajax:TabPanel ID="TabDetalle" runat="server">
            <HeaderTemplate>
                 Modificaciones Realizadas
            
</HeaderTemplate>
            
<ContentTemplate>
                    <asp:Panel runat="server" ID="pnlLista">
                    <asp:GridView runat="server" ID="gvTipoSolicitud" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDCosModContractual" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoSolicitud_PageIndexChanging" OnSelectedIndexChanged="gvTipoSolicitud_SelectedIndexChanged">
<AlternatingRowStyle CssClass="rowBG" />
<Columns>
<asp:TemplateField><ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField HeaderText="No Solicitud" DataField="IDCosModContractual" />
<asp:BoundField HeaderText="Tipo de Modificaci&oacute;n" DataField="TipoModificacion" />
<asp:BoundField HeaderText="Estado" DataField="Estado" />
</Columns>

<EmptyDataRowStyle CssClass="headerForm" />

<HeaderStyle CssClass="headerForm" />

<RowStyle CssClass="rowAG" />
</asp:GridView>

    </asp:Panel>

                
</ContentTemplate>
            
</Ajax:TabPanel>

<Ajax:TabPanel ID="TabPanel1" runat="server">
            <HeaderTemplate>
                 Modificaciones Rechazadas 
            
</HeaderTemplate>
            
<ContentTemplate>
                    <asp:Panel runat="server" ID="pnlListaRechazados">
                    <asp:GridView runat="server" ID="gvSolRechazadas" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDCosModContractual" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvSolRechazadas_PageIndexChanging" OnSelectedIndexChanged="gvSolRechazadas_SelectedIndexChanged">
<AlternatingRowStyle CssClass="rowBG" />
<Columns>
<asp:TemplateField><ItemTemplate>
                                    <asp:ImageButton ID="btnSolRechazadas" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField HeaderText="No Solicitud" DataField="IDCosModContractual" />
<asp:BoundField HeaderText="Tipo de Modificaci&oacute;n" DataField="TipoModificacion" />
<asp:BoundField HeaderText="Estado" DataField="Estado" />
<asp:BoundField HeaderText="Fecha de Rechazo" DataField="FechaModifica" dataformatstring="{0:d}" />
</Columns>

<EmptyDataRowStyle CssClass="headerForm" />

<HeaderStyle CssClass="headerForm" />

<RowStyle CssClass="rowAG" />
</asp:GridView>

    </asp:Panel>

                
</ContentTemplate>
            
</Ajax:TabPanel>
              <Ajax:TabPanel ID="TabAdjuntoContrato" runat="server">
            <HeaderTemplate>
                Adjuntos al Contrato
            
</HeaderTemplate>
                
<ContentTemplate>
                      <uc1:ArchivosContrato runat="server" ID="ArchivosContrato" ShowCreate="false" />
                
</ContentTemplate>
             
</Ajax:TabPanel>
        </Ajax:TabContainer>

             </td>
            </tr>
            </table>



</asp:Content>
