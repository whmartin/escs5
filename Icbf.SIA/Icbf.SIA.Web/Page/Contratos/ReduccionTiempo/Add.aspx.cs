using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using Icbf.SIA.Service;

public partial class Page_ReduccionTiempo_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    string PageName = "Contratos/ReduccionTiempo";
    string TIPO_ESTRUCTURA = "Prorrogas";
    int vIdContrato;
    decimal vIdIndice = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();
            }
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            

            toolBar.EstablecerTitulos("Reducci&#243;n de Tiempo", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
        }
    }

    private void Guardar()
    {
        try
        {
            if (!ValidarCampos())
            {
                int vResultado;
                ReduccionesTiempo vReduccion = new ReduccionesTiempo();

                //vProrrogas.FechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
                vReduccion.FechaReduccion = Convert.ToDateTime(txtFechaFin.Text);
                vReduccion.Dias = Convert.ToInt32(txtDias.Text);
                vReduccion.Meses = Convert.ToInt32(txtMeses.Text);
                vReduccion.Anios = Convert.ToInt32(txtAños.Text);
                vReduccion.Justificacion = Convert.ToString(txtJustificacion.Text);                

                if (Request.QueryString["oP"] == "E")
                {
                    vReduccion.IdReduccionTiempo = Convert.ToInt32(hfIdProrroga.Value);
                    vReduccion.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vReduccion, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarReduccionesTiempo(vReduccion);
                }
                else
                {
                    vReduccion.IdDetalleConsModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                    vReduccion.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vReduccion, this.PageName, vSolutionPage);
                    vResultado = vContratoService.InsertarReduccionesTiempo(vReduccion);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    if (hfIDCosModContractual.Value != "")
                    {
                        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                    }
                    else
                    {
                        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                    }

                    SetSessionParameter("Prorrogas.IdProrroga", vReduccion.IdReduccionTiempo);
                    SetSessionParameter("Prorrogas.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private Boolean ValidarCampos()
    {
        DateTime fechavalidacion = new DateTime(1900, 1, 1);

        DateTime caTxtFechaFinalContrato = Convert.ToDateTime(TxtFechaFinalContrato.Text);
        //DateTime caFechaInicioEjecucion = Convert.ToDateTime(txtFechaInicio.Text);
        DateTime caFechaReduccion = Convert.ToDateTime(txtFechaFin.Text);
        DateTime caFechaInicioEjecucion = Convert.ToDateTime(TxtFechaInicioContrato.Text);

        bool sw = false;
       

        if (!DateTime.TryParse(txtFechaFin.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de la Reducci&#243;n no es Válida");
            sw = true;
        }
        
        if (caFechaReduccion.Date > caTxtFechaFinalContrato.Date)
        {
            toolBar.MostrarMensajeError(
            "La Fecha de la Reducci&#243;n no puede ser mayor a la Fecha Final de Terminación Contrato");
            sw = true;
        }

        if (caFechaReduccion.Date < caFechaInicioEjecucion.Date)
        {
            toolBar.MostrarMensajeError(
            "La Fecha de la Reducci&#243;n no puede ser menor a la Fecha de inicio de Ejecuci&#243;n");
            sw = true;
        }



        return sw;
    }

    private void CargarRegistro()
    {
        try
        {
            int IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            hfIdContrato.Value = IdContrato.ToString();

            int vIdConsModContractual;

            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(IdContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinContrato = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);
            DateTime caFechaCalculada = caFechaFinContrato.AddDays(1);
            cetxtFechaFin.StartDate = caFechaInicioEjecucion;
            cetxtFechaFin.EndDate = caFechaFinContrato;

            TxtContrato.Text = vContrato.NumeroContrato.ToString();
            TxtRegional.Text = vContrato.NombreRegional;
            TxtFechaInicioContrato.Text = caFechaInicioEjecucion.ToShortDateString();
            TxtFechaFinalContrato.Text = caFechaFinContrato.ToShortDateString();
            TxtObjetoContrato.Text = vContrato.ObjetoContrato;
            TxtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            TxtValorInicial.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            TxtValorFinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);
            // txtFechaInicio.Text = caFechaCalculada.ToShortDateString();

            string calculo1 = string.Empty;
            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion, caFechaFinContrato, out calculo1);

            var itemsFecha = calculo1.Split('|');

            TxtDiasPlazoInicial.Text = itemsFecha[2];
            TxtMesesPlazoInicial.Text = itemsFecha[1];
            TxtAñosPlazoInicial.Text = itemsFecha[0];

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(IdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }
            

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            txtJustificacion.Text = vConsModContractual.Justificacion;

            ReduccionesTiempo vReduccion = new ReduccionesTiempo();

            if (Request.QueryString["oP"] == "E")
            {
                int vIdProrroga = Convert.ToInt32(GetSessionParameter("Prorrogas.IdProrroga"));
                RemoveSessionParameter("Prorrogas.IdProrroga");

                vReduccion = vContratoService.ConsultarReduccionTiempo(vIdProrroga);
                hfIdProrroga.Value = vReduccion.IdReduccionTiempo.ToString();
                //txtFechaInicio.Text = vProrrogas.FechaInicio.ToShortDateString();
                txtFechaFin.Text = vReduccion.FechaReduccion.ToString();
                txtDias.Text = vReduccion.Dias.ToString();
                txtMeses.Text = vReduccion.Meses.ToString();
                txtAños.Text = vReduccion.Anios.ToString();

                string calculo = string.Empty;

                DateTime caFechaFinalTerminacionContrato = Convert.ToDateTime(vReduccion.FechaReduccion);
                ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion, caFechaFinalTerminacionContrato, out calculo);

                var itemsFecha1 = calculo.Split('|');

                txtDiasAcumulados.Text = itemsFecha1[2];
                txtMesesAcumulado.Text = itemsFecha1[1];
                txtAñosAcumulados.Text = itemsFecha1[0];

                PanelArchivos.Visible = true;
            }

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vReduccion.UsuarioCrea, vReduccion.FechaCrea, vReduccion.UsuarioModifica, vReduccion.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void txtFechaFin_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        DateTime fechavalidacion = new DateTime(1900, 1, 1);       

        if (!DateTime.TryParse(txtFechaFin.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de la Reducci&#243;n no es Válida");
            return;
        }          

       // DateTime caTxtFechaFinalContrato = Convert.ToDateTime(TxtFechaFinalContrato.Text);
        DateTime caTxtFechaInicioContrato = Convert.ToDateTime(TxtFechaInicioContrato.Text);
        DateTime caFechaReduccion = Convert.ToDateTime(txtFechaFin.Text);

        DateTime caFechaFinalizacionInicial = Convert.ToDateTime(TxtFechaFinalContrato.Text);
        
        
       
        if (!DateTime.TryParse(txtFechaFin.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de la Reducci&#243;n no es Válida");
            return;
        }

        string calculo = string.Empty;

        ContratoService.ObtenerDiferenciaFechas(caFechaReduccion, caFechaFinalizacionInicial, out calculo);

        var itemsFecha = calculo.Split('|');

        txtDias.Text = itemsFecha[2];
        txtMeses.Text = itemsFecha[1];
        txtAños.Text = itemsFecha[0];

        calculo = string.Empty;

        ContratoService.ObtenerDiferenciaFechas(caTxtFechaInicioContrato, caFechaReduccion, out calculo);

        itemsFecha = calculo.Split('|');

        txtDiasAcumulados.Text = itemsFecha[2];
        txtMesesAcumulado.Text = itemsFecha[1];
        txtAñosAcumulados.Text = itemsFecha[0];         
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion


}
