using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Service;
using System.Data;

/// <summary>
/// Página de consulta a través de filtros para la entidad Contratos
/// </summary>
public partial class Page_Contratos_GeneracionArchivoObligaciones_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GeneracionArchivosObligaciones";
    ContratoService vContratoService = new ContratoService();
    SeguridadService vSeguridadFAC = new SeguridadService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvContratos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            gvContratos.PageSize = PageSize();
            gvContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos(@"Generación de Archivo de Obligaciones", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnReporte_Click(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();

            if (gvContratos.Rows.Count > 0)
            {
                bool existeSeleccionado = false;
                string ids = string.Empty;

                foreach (GridViewRow fila in gvContratos.Rows)
                {
                    if ((fila.Cells[0].FindControl("chbContiene") as CheckBox).Checked)
                    {
                        ids += string.Format("{0},",fila.Cells[1].Text);
                       existeSeleccionado = true;
                    }
                }

                if (existeSeleccionado)
                {
                    string idregional = gvContratos.DataKeys[0].Values[1].ToString();

                    string mes =string.Empty;
                    if (DateTime.Now.Month < 10)
                        mes = string.Format("0{0}",DateTime.Now.Month);

                    if (idregional.Length == 1)
                        idregional = string.Format("0{0}",idregional);

                    string nombreArchivo = string.Format("{0}{1}{2}_{3}_ZipArchivosRP",DateTime.Now.Year,mes,DateTime.Now.Day,idregional); 

                    string rutaBase = System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"];

                    string rutaZip = System.IO.Path.Combine(rutaBase, nombreArchivo);

                    if (!System.IO.Directory.Exists(rutaZip))
                        System.IO.Directory.CreateDirectory(rutaZip);

                    string rol = string.Empty;

                    var itemsRoles = Roles.GetRolesForUser(GetSessionUser().NombreUsuario);

                    if(itemsRoles.Count() > 0)
                        rol = itemsRoles[0];

                    DataSet setArchivos = vContratoService.ConsultarContratosSuscritosObligacionesPresupuestales(ids, GetSessionUser().NombreUsuario,rol);

                    Utilidades.ExportToExcel(setArchivos.Tables[0], "Maestro", rutaZip + "\\",false);
                    Utilidades.ExportToExcel(setArchivos.Tables[1], "Archivo_Detalle1", rutaZip + "\\",false);
                    Utilidades.ExportToExcel(setArchivos.Tables[2], "Archivo_Detalle2", rutaZip + "\\",false);

                    Utilidades.Compress(rutaBase, nombreArchivo);

                    string nombreArchivoZip = string.Format("{0}.zip", rutaZip);

                    bool isValid = DescargarZip(nombreArchivoZip, nombreArchivo);

                    if (isValid)
                    {
                        System.IO.Directory.Delete(rutaZip, true);
                        System.IO.File.Delete(nombreArchivoZip);
                        int result =  vContratoService.ActualizarContratosSuscritosObligacionesPresupuestales(ids, GetSessionUser().NombreUsuario);

                        if (result > 0)
                            Buscar();
                        else
                            toolBar.MostrarMensajeError("Sucedio un error al actualizar el cargue");
                    }
                }
                else
                    toolBar.MostrarMensajeError("No existen registros asociados");
            }
            else
                toolBar.MostrarMensajeError("La grilla no arrojó registros asociados.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private bool DescargarZip(string rutaArchivo, string nombreArchivo)
    {
        try
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(rutaArchivo);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nombreArchivo + ".zip");
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(rutaArchivo);
            Response.Flush();
            return true;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        return false;
    }

    protected void gvContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvContratos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            DateTime? vFechaSuscripcion = null;
            string vNumContrato = null;
            int vVigenciaFiscalinicial;
            int vIDRegional;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            string vUsuarioCrea = null;
            string[] lRoles = Roles.GetRolesForUser(GetSessionUser().NombreUsuario);

            if (!vSeguridadFAC.ConsultarRolesNombreEsAdmin(lRoles[0], true))
                vUsuarioCrea = GetSessionUser().NombreUsuario;

            if (txtFechaRegistroSistema.Date.Year.ToString() != "1900")
                vFechaSuscripcion = Convert.ToDateTime(txtFechaRegistroSistema.Date);

            if (! string.IsNullOrEmpty(txtIdContrato.Text))
                vNumContrato = txtIdContrato.Text;

            vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            
            vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
            
            if (ddlIDTipoContrato.SelectedValue != "-1")
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);

            List<EstadoContrato> vEstadoContrato = new List<EstadoContrato>();

            vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "SUS", null, true);
            if (vEstadoContrato.Count == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }

            var myGridResults = vContratoService.ConsultarContratosGenerarObligaciones(vFechaSuscripcion, vNumContrato, vVigenciaFiscalinicial, vIDRegional, vIDCategoriaContrato, vIDTipoContrato);

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Contrato), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Contrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
                else
                    gridViewsender.DataSource = myGridResults;
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Contratos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Contratos.Eliminado");
            CargarListaVigencia();
            CargarListaRegional();
            LlenarCategoriaContrato();
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
            }

        }

    }
    
    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }
    
    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            // Página 1 Descripción adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion

            //switch (CodContratoAsociadoSeleccionado) // Se asigna previamente en SeleccionadoContratoAsociado 
            //{
            //    case CodContratoConvenioAdhesion:
            //        ddlIDTipoContrato.Enabled = false;
            //        ddlIDTipoContrato.SelectedValue = IdTipoContAporte; // Página 1 Descripción adicional Tipo Contrato Convenio
            //        break;
            //    default:
            //        ddlIDTipoContrato.Enabled = true; //Página 11 Paso 9 
            //        break;
            //}
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }


    }
}
