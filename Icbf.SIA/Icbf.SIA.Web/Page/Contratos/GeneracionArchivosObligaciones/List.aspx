<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_GeneracionArchivoObligaciones_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Fecha Suscripción del Contrato
                <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtFechaRegistroSistema$txtFecha" Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha Registro al Sistema Válida" ForeColor="red"  ValidationGroup="btnBuscar"></asp:CompareValidator>
            </td>
            <td class="Cell">
                Número del Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistema" runat="server" Width="80%" Enabled="True" Requerid="False" />
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="9" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato" FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Vigencia Fiscal 
               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVigenciaFiscalinicial" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="True" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Regional del Contrato
                <asp:RequiredFieldValidator ID="rfvTipoBusqueda0" runat="server" ControlToValidate="ddlIDRegional" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="-1" SetFocusOnError="True" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Categoría del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,CodigoRegional" CellPadding="0" Height="16px"
                        OnSorting="gvContratos_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContratos_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chbContiene" runat="server"  />
                                </ItemTemplate>
                                <ControlStyle Width="25px" />
                                <FooterStyle Width="25px" />
                                <HeaderStyle Width="25px" />
                                <ItemStyle Width="25px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Id Contrato" DataField="IdContrato"  SortExpression="IdContrato"/>
                            <asp:BoundField HeaderText="No Contrato" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal Inicial" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Contratista" DataField="NombreContratista"  SortExpression="NombreContratista"/>
                            <asp:BoundField HeaderText="Documento" DataField="NumeroDocumentoIdentificacion"  SortExpression="NumeroDocumentoIdentificacion" />
                            <asp:BoundField HeaderText="Tipo Documento" DataField="TipoDocumentoIdentificacion"  SortExpression="TipoDocumentoIdentificacion" />
                            <asp:BoundField HeaderText="Tipo de Contrato" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                            <asp:BoundField HeaderText="Fecha de Suscripción" DataField="FechasSuscripcionContratoView"  SortExpression="FechasSuscripcionContratoView" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
