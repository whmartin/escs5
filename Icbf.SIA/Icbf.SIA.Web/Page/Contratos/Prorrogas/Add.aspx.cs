using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using Icbf.SIA.Service;

public partial class Page_Prorrogas_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    string PageName = "Contratos/Prorrogas";
    string TIPO_ESTRUCTURA = "Prorrogas";
    int vIdContrato;
    decimal vIdIndice = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();
            }
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            

            toolBar.EstablecerTitulos("Prorrogas", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
        }
    }

    private void Guardar()
    {
        try
        {
            if (!ValidarCampos())
            {
                int vResultado;
                Prorrogas vProrrogas = new Prorrogas();

                vProrrogas.FechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
                vProrrogas.FechaFin = Convert.ToDateTime(txtFechaFin.Text);
                vProrrogas.Dias = Convert.ToInt32(txtDias.Text);
                vProrrogas.Meses = Convert.ToInt32(txtMeses.Text);
                vProrrogas.Años = Convert.ToInt32(txtAños.Text);
                vProrrogas.Justificacion = Convert.ToString(txtJustificacion.Text);                

                if (Request.QueryString["oP"] == "E")
                {
                    vProrrogas.IdProrroga = Convert.ToInt32(hfIdProrroga.Value);
                    vProrrogas.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vProrrogas, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarProrrogas(vProrrogas);
                }
                else
                {
                    vProrrogas.IdDetalleConsModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                    vProrrogas.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vProrrogas, this.PageName, vSolutionPage);
                    vResultado = vContratoService.InsertarProrrogas(vProrrogas);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    if (hfIDCosModContractual.Value != "")
                    {
                        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                    }
                    else
                    {
                        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                    }

                    SetSessionParameter("Prorrogas.IdProrroga", vProrrogas.IdProrroga);
                    SetSessionParameter("Prorrogas.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private Boolean ValidarCampos()
    {
        DateTime fechavalidacion = new DateTime(1900, 1, 1);

        DateTime caTxtFechaFinalContrato = Convert.ToDateTime(TxtFechaFinalContrato.Text);
        DateTime caFechaInicioEjecucion = Convert.ToDateTime(txtFechaInicio.Text);
        DateTime caFechaFinalizacionInicial = Convert.ToDateTime(txtFechaFin.Text);
        
        bool sw = false;

        if (!DateTime.TryParse(txtFechaInicio.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de Inicio de la Prórroga no es Válida");
            sw = true;
        }

        if (!DateTime.TryParse(txtFechaFin.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha Final de la Prórroga no es Válida");
            sw = true;
        }
        
        if (caFechaInicioEjecucion.Date <= caTxtFechaFinalContrato.Date || caFechaFinalizacionInicial.Date < caTxtFechaFinalContrato.Date)
        {
            toolBar.MostrarMensajeError(
            "La Fecha de Inicio y Final de la Prórroga no pueden ser menores a la Fecha Final de Terminación Contrato");
            sw = true;
        }

        if (caFechaInicioEjecucion.Date > caFechaFinalizacionInicial.Date)
        {
            toolBar.MostrarMensajeError(
                        "La Fecha Final de la Prórroga no puede ser menor a la Fecha de Inicio de la Prórroga");
            sw = true;
        }

        return sw;
    }

    private void CargarRegistro()
    {
        try
        {
            int IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            hfIdContrato.Value = IdContrato.ToString();

            int vIdConsModContractual;

            if (! string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(IdContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinalTerminacionContrato = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);
            DateTime caFechaFinContrato = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);
            DateTime caFechaCalculada = caFechaFinContrato.AddDays(1);
            cetxtFechaFin.StartDate = caFechaFinContrato.AddDays(2);

            TxtContrato.Text = vContrato.NumeroContrato.ToString();
            TxtRegional.Text = vContrato.NombreRegional;
            TxtFechaInicioContrato.Text = caFechaInicioEjecucion.ToShortDateString();
            TxtFechaFinalContrato.Text = caFechaFinContrato.ToShortDateString();
            TxtObjetoContrato.Text = vContrato.ObjetoContrato;
            TxtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            TxtValorInicial.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            TxtValorFinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);
            txtFechaInicio.Text = caFechaCalculada.ToShortDateString();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(IdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            string calculo = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion, caFechaFinalTerminacionContrato, out calculo);

            var itemsFecha = calculo.Split('|');

            TxtDiasPlazoInicial.Text = itemsFecha[2];
            TxtMesesPlazoInicial.Text = itemsFecha[1];
            TxtAñosPlazoInicial.Text = itemsFecha[0];

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            txtJustificacion.Text = vConsModContractual.Justificacion;

            Prorrogas vProrrogas = new Prorrogas();

            if (Request.QueryString["oP"] == "E")
            {
                int vIdProrroga = Convert.ToInt32(GetSessionParameter("Prorrogas.IdProrroga"));
                RemoveSessionParameter("Prorrogas.IdProrroga");

                vProrrogas = vContratoService.ConsultarProrrogas(vIdProrroga);
                hfIdProrroga.Value = vProrrogas.IdProrroga.ToString();
                txtFechaInicio.Text = vProrrogas.FechaInicio.ToShortDateString();
                txtFechaFin.Text = vProrrogas.FechaFin.ToString();
                txtDias.Text = vProrrogas.Dias.ToString();
                txtMeses.Text = vProrrogas.Meses.ToString();
                txtAños.Text = vProrrogas.Años.ToString();
                PanelArchivos.Visible = true;
            }

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProrrogas.UsuarioCrea, vProrrogas.FechaCrea, vProrrogas.UsuarioModifica, vProrrogas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void txtFechaFin_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        DateTime fechavalidacion = new DateTime(1900, 1, 1);       

        if (!DateTime.TryParse(txtFechaFin.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de Fin de la Prórroga no es Válida");
            return;
        }       

        if (Convert.ToDateTime(txtFechaFin.Text)< Convert.ToDateTime(txtFechaInicio.Text))
        {
            toolBar.MostrarMensajeError("Fecha de Fin de la Prórroga no es Válida");
            return;
        }
        //D:\Contratos\Icbf.SIA-Contratos\Icbf.SIA.Web\Page\Contratos\AmparosGarantias\Add.aspx.cs
        DateTime caTxtFechaFinalContrato = Convert.ToDateTime(TxtFechaFinalContrato.Text);
        DateTime caTxtFechaInicioContrato = Convert.ToDateTime(TxtFechaInicioContrato.Text);
        DateTime caFechaInicioEjecucion = Convert.ToDateTime(txtFechaInicio.Text);

        DateTime caFechaFinalizacionInicial = Convert.ToDateTime(txtFechaFin.Text);
        
        
        if (!DateTime.TryParse(txtFechaInicio.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha de Inicio de la Prórroga no es Válida");
            return;
        }

        if (!DateTime.TryParse(txtFechaFin.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha Final de la Prórroga no es Válida");
            return;
        }

        string calculo = string.Empty;

        ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion, caFechaFinalizacionInicial, out calculo);

        var itemsFecha = calculo.Split('|');

        txtDias.Text = itemsFecha[2];
        txtMeses.Text = itemsFecha[1];
        txtAños.Text = itemsFecha[0];

        calculo = string.Empty;

        ContratoService.ObtenerDiferenciaFechas(caTxtFechaInicioContrato, caFechaFinalizacionInicial, out calculo);

        itemsFecha = calculo.Split('|');

        txtDiasAcumulados.Text = itemsFecha[2];
        txtMesesAcumulado.Text = itemsFecha[1];
        txtAñosAcumulados.Text = itemsFecha[0];         
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion


}
