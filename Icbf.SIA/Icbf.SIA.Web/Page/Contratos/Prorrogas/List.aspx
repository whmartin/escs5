<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Prorrogas_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha Inicio Prórroga *
            </td>
            <td>
                Fecha Final Prórroga *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicio"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFin"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Plazo total con prórroga Días
            </td>
            <td>
                Plazo total con prórroga Meses
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtDias"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMeses"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Plazo total con prórroga Años
            </td>
            <td>
                Justificación de la novedad contractual *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAños"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Plazo total con prórroga Años *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtIdDetalleConsModContractual"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProrrogas" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdProrroga" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvProrrogas_PageIndexChanging" OnSelectedIndexChanged="gvProrrogas_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Inicio Prórroga" DataField="FechaInicio" />
                            <asp:BoundField HeaderText="Fecha Final Prórroga" DataField="FechaFin" />
                            <asp:BoundField HeaderText="Plazo total con prórroga Días" DataField="Dias" />
                            <asp:BoundField HeaderText="Plazo total con prórroga Meses" DataField="Meses" />
                            <asp:BoundField HeaderText="Plazo total con prórroga Años" DataField="Años" />
                            <asp:BoundField HeaderText="Justificación de la novedad contractual" DataField="Justificacion" />
                            <asp:BoundField HeaderText="Plazo total con prórroga Años" DataField="IdDetalleConsModContractual" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
