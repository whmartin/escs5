using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Prorrogas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Prorrogas";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            DateTime? vFechaInicio = null;
            DateTime? vFechaFin = null;
            int? vDias = null;
            int? vMeses = null;
            int? vAños = null;
            String vJustificacion = null;
            int? vIdDetalleConsModContractual = null;
            if (txtFechaInicio.Text!= "")
            {
                vFechaInicio = Convert.ToDateTime(txtFechaInicio.Text);
            }
            if (txtFechaFin.Text!= "")
            {
                vFechaFin = Convert.ToDateTime(txtFechaFin.Text);
            }
            if (txtDias.Text!= "")
            {
                vDias = Convert.ToInt32(txtDias.Text);
            }
            if (txtMeses.Text!= "")
            {
                vMeses = Convert.ToInt32(txtMeses.Text);
            }
            if (txtAños.Text!= "")
            {
                vAños = Convert.ToInt32(txtAños.Text);
            }
            if (txtJustificacion.Text!= "")
            {
                vJustificacion = Convert.ToString(txtJustificacion.Text);
            }
            if (txtIdDetalleConsModContractual.Text!= "")
            {
                vIdDetalleConsModContractual = Convert.ToInt32(txtIdDetalleConsModContractual.Text);
            }
            gvProrrogas.DataSource = vContratoService.ConsultarProrrogass( vFechaInicio, vFechaFin, vDias, vMeses, vAños, vJustificacion, vIdDetalleConsModContractual);
            gvProrrogas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvProrrogas.PageSize = PageSize();
            gvProrrogas.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Prorrogas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProrrogas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProrrogas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProrrogas.SelectedRow);
    }
    protected void gvProrrogas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProrrogas.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Prorrogas.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Prorrogas.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
