<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Prorrogas_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdProrroga" runat="server" />    
<asp:HiddenField ID="hfIDCosModContractual" runat="server" />
<asp:HiddenField ID="hfIDCosModContractualEstado" runat="server" />
<asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
<asp:HiddenField ID="hfIdDetalleConsModContractual" runat="server" />
<asp:HiddenField ID="hfIdTContrato" runat="server" />
<asp:HiddenField ID="hfEsSubscripcion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2" class="auto-style1">
                Número de Contrato
            </td>
            <td class="auto-style1" colspan="2">
                Regional
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtRegional"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Fecha Inicio Contrato/Convenio
            </td>
            <td colspan="2">
                Fecha Final de Terminación Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtFechaInicioContrato"  Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtFechaFinalContrato"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Objeto del Contrato
            </td>
            <td colspan="2">
                Alcance del Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtObjetoContrato"  TextMode="MultiLine" Height="73px" Width="90%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtAlcanceContrato" TextMode="MultiLine" Height="73px" Width="90%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Valor Inicial del Contrato/Convenio
            </td>
            <td colspan="2">
                Valor Final del Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtValorInicial"  Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="TxtValorFinal"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="2">
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                
                Plazo inicial del Contrato</td>
            <td colspan="2">
                Fecha Inicio Prórroga *
            </td>
        </tr>
        <tr class="rowA">
            <td>                
                <strong>Días</strong>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TxtDiasPlazoInicial"  Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false"></asp:TextBox>
                <asp:Image ID="imgCalendarioDesde" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Meses
            </td>
            <td>
                <asp:TextBox runat="server" ID="TxtMesesPlazoInicial"  Enabled="false"></asp:TextBox>
            </td>
            <td colspan="2">
                Fecha Final Prórroga *
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1">                
                <strong>Años</strong>
            </td>
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="TxtAñosPlazoInicial"  Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style1" colspan="2">
                <asp:TextBox runat="server" ID="txtFechaFin"  Enabled="false"></asp:TextBox>
                <asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="2">
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Plazo de la prórroga 
            </td>
            <td colspan="2">
                Plazo total del contrato con prórroga 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <strong>Días</strong>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDias"  Enabled="false"></asp:TextBox></td>
            <td>
                <strong>Días</strong>                
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiasAcumulados"  Enabled="false"></asp:TextBox></td>
            
        </tr>
        <tr class="rowB">
            <td>
                Meses
            <td>
                <asp:TextBox runat="server" ID="txtMeses"  Enabled="false"></asp:TextBox></td></td>
             <td> 
                Meses</td>
            <td> 
                <asp:TextBox runat="server" ID="txtMesesAcumulado"  Enabled="false"></asp:TextBox></td>
        </tr>
        <tr class="rowA">
            <td>
                <strong>Años</strong>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtAños"  Enabled="false"></asp:TextBox></td>
            <td>
                <strong>Años</strong></td>
            <td>
                <asp:TextBox runat="server" ID="txtAñosAcumulados"  Enabled="false"></asp:TextBox></td>
        </tr>
        <tr class="rowAB">
            <td colspan="2">
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="4">
                Justificación de la novedad contractual *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="4">
                <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Height="73px" Width="95%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="2">
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="4">
                Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="4">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Documentos&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
    </style>
</asp:Content>


