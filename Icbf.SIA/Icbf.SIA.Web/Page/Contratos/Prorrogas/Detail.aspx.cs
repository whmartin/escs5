using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using System.Net;
using Icbf.SIA.Service;

public partial class Page_Prorrogas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Prorrogas";
    string TIPO_ESTRUCTURA = "Prorrogas";
    ContratoService vContratoService = new ContratoService();
    private ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;            
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("Prorrogas", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        bool Nuevo = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                Nuevo = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                Nuevo = true;
            }
        }

        if (Nuevo)
        {
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetalleConsModContractual.Value.ToString());
            NavigateTo(SolutionPage.Add);
        }
        else
        {
            toolBar.MostrarMensajeError("No se puede Registrar una nueva prorroga, Por favor validar el estado!");
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        bool Editar = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                Editar = true;
            }
        }
        else
        {
            //if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            //{
            //    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            //    Editar = true;
            //}
            Editar = false;
        }

        if (Editar)
        {
            SetSessionParameter("Prorrogas.IdProrroga", hfIdProrroga.Value);
            NavigateTo(SolutionPage.Edit);
        }
        else
        {
            toolBar.MostrarMensajeError("No se puede Editar la prorroga, Por favor validar el estado!");
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        bool Eliminar = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
                Eliminar = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
                Eliminar = true;
            }
        }

        if (!Eliminar)
        {
            toolBar.MostrarMensajeError("No se puede Eliminar la prorroga, Por favor validar el estado!");
        }       
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());

            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    private void CargarDatos()
    {
        try
        {
            int vIdConsModContractual;

            if (! string.IsNullOrEmpty( GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConsModContractual);

                if (detalleSolicitud.Estado == "Registro" || detalleSolicitud.Estado == "Devuelta")
                    toolBar.MostrarBotonEditar(true);
                else
                    toolBar.MostrarBotonEditar(false);
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();

                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                {
                    hfEsSubscripcion.Value = "1";
                    RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                {
                    hfEsSubscripcion.Value = "2";
                    RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                }

                toolBar.MostrarBotonEditar(false);
                toolBar.OcultarBotonEliminar(true);
                toolBar.OcultarBotonNuevo(true);
            }

            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));

            int vIdProrroga = Convert.ToInt32(GetSessionParameter("Prorrogas.IdProrroga"));
            RemoveSessionParameter("Prorrogas.IdProrroga");

            if (GetSessionParameter("Prorrogas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Prorrogas.Guardado");

            hfIdTContrato.Value = vIdContrato.ToString();

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(vIdContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinalTerminacionContrato = Convert.ToDateTime(vContrato.FechaFinalTerminacionContrato);
            //DateTime caFechaFinalTerminacionContrato = Convert.ToDateTime(vContrato.FechaFinalizacionIniciaContrato);

            TxtContrato.Text = vContrato.NumeroContrato.ToString();
            TxtRegional.Text = vContrato.NombreRegional;
            TxtFechaInicioContrato.Text = caFechaInicioEjecucion.ToShortDateString();
            TxtFechaFinalContrato.Text = caFechaFinalTerminacionContrato.ToShortDateString();
            TxtObjetoContrato.Text = vContrato.ObjetoContrato;
            TxtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            TxtValorInicial.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            TxtValorFinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            #region CalculoDiferenciaFechasFinalTerminaInicioEjecucion




            int increment = 0;
            int day, month, year;
            if (caFechaInicioEjecucion.Day > caFechaFinalTerminacionContrato.Day)
            {
                increment = (DateTime.DaysInMonth(caFechaInicioEjecucion.Year, caFechaInicioEjecucion.Month));
            }

            if (increment == -1)
            {
                if (DateTime.IsLeapYear(caFechaInicioEjecucion.Year))
                {
                    increment = 29;
                }
                else
                {
                    increment = 28;
                }
            }

            if (increment != 0)
            {
                day = (caFechaFinalTerminacionContrato.Day + increment) - caFechaInicioEjecucion.Day;
                increment = 1;
            }
            else
            {
                day = caFechaFinalTerminacionContrato.Day - caFechaInicioEjecucion.Day + 1;
            }

            if ((caFechaInicioEjecucion.Month + increment) > caFechaFinalTerminacionContrato.Month)
            {
                month = (caFechaFinalTerminacionContrato.Month + 12) - (caFechaInicioEjecucion.Month + increment);
                increment = 1;
            }
            else
            {
                month = (caFechaFinalTerminacionContrato.Month) - (caFechaInicioEjecucion.Month + increment);
                increment = 0;
            }

            year = caFechaFinalTerminacionContrato.Year - (caFechaInicioEjecucion.Year + increment);

            // 2-09-2014 Ajuste para no robar un día, caso especial solicitado por ICBF
            if ((month == 0 && year == 0) && (caFechaFinalTerminacionContrato.Day != caFechaInicioEjecucion.Day))
            {
                day = day + 1;
            }

            #endregion

            TxtDiasPlazoInicial.Text = day.ToString();
            TxtMesesPlazoInicial.Text = month.ToString();
            TxtAñosPlazoInicial.Text = year.ToString();

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            txtJustificacion.Text = vConsModContractual.Justificacion;
            hfIDCosModContractualEstado.Value = vConsModContractual.IdConsModContractualesEstado.ToString();

            Prorrogas vProrrogas = new Prorrogas();
            vProrrogas = vContratoService.ConsultarProrrogas(vIdProrroga);

            DateTime caFechaInicio = Convert.ToDateTime(vProrrogas.FechaInicio);
            DateTime caFechaFin = Convert.ToDateTime(vProrrogas.FechaFin);

            hfIdProrroga.Value = vProrrogas.IdProrroga.ToString();
            hfIdDetalleConsModContractual.Value = vProrrogas.IdDetalleConsModContractual.ToString();
            txtFechaInicio.Text = caFechaInicio.ToShortDateString();
            txtFechaFin.Text = caFechaFin.ToShortDateString();
            txtDias.Text = vProrrogas.Dias.ToString();
            txtMeses.Text = vProrrogas.Meses.ToString();
            txtAños.Text = vProrrogas.Años.ToString();

            CalcularFechas(caFechaInicioEjecucion, caFechaFin);

            ObtenerAuditoria(PageName, hfIdProrroga.Value);

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vProrrogas.UsuarioCrea, vProrrogas.FechaCrea, vProrrogas.UsuarioModifica, vProrrogas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void CalcularFechas(DateTime caFechaInicioEjecucion, DateTime caFechaFinalizacionInicial)
    {

        try
        {
            int increment = 0;
            int day = 0;
            int month = 0;
            int year = 0;


            if (caFechaInicioEjecucion.Date < caFechaFinalizacionInicial.Date)
            {
                if (caFechaInicioEjecucion.Day > caFechaFinalizacionInicial.Day)
                {
                    increment = (DateTime.DaysInMonth(caFechaInicioEjecucion.Year, caFechaInicioEjecucion.Month));
                }

                if (increment == -1)
                {
                    if (DateTime.IsLeapYear(caFechaInicioEjecucion.Year))
                    {
                        increment = 29;
                    }
                    else
                    {
                        increment = 28;
                    }
                }

                if (increment != 0)
                {
                    day = (caFechaFinalizacionInicial.Day + increment) - caFechaInicioEjecucion.Day;
                    increment = 1;
                }
                else
                {
                    day = caFechaFinalizacionInicial.Day - caFechaInicioEjecucion.Day + 1;
                }

                if ((caFechaInicioEjecucion.Month + increment) > caFechaFinalizacionInicial.Month)
                {
                    month = (caFechaFinalizacionInicial.Month + 12) - (caFechaInicioEjecucion.Month + increment);
                    increment = 1;
                }
                else
                {
                    month = (caFechaFinalizacionInicial.Month) - (caFechaInicioEjecucion.Month + increment);
                    increment = 0;
                }

                year = caFechaFinalizacionInicial.Year - (caFechaInicioEjecucion.Year + increment);

                // 2-09-2014 Ajuste para no robar un día, caso especial solicitado por ICBF
                if ((month == 0 && year == 0) && (caFechaFinalizacionInicial.Day != caFechaInicioEjecucion.Day))
                {
                    day = day + 1;
                }


            }
            else
            {
                toolBar.MostrarMensajeError(
                            "La Fecha de Finalización no puede ser menor a la Fecha de Inicio");
            }



            txtDiasAcumulados.Text = day.ToString();
            txtMesesAcumulado.Text = month.ToString();
            txtAñosAcumulados.Text = year.ToString();


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
   
    private void EliminarRegistro()
    {
        try
        {
            int vIdProrroga = Convert.ToInt32(hfIdProrroga.Value);

            Prorrogas vProrrogas = new Prorrogas();
            vProrrogas = vContratoService.ConsultarProrrogas(vIdProrroga);
            InformacionAudioria(vProrrogas, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarProrrogas(vProrrogas);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Prorrogas.Eliminado", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdTContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
}
