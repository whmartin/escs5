using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_ModificacionObligacion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ModificacionObligacion";
    ContratoService vContratoService = new ContratoService();
    string TIPO_ESTRUCTURA = "ModificacionObligacion";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }
 
    protected void btnEditar_Click(object sender, EventArgs e)
    {

        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
        SetSessionParameter("ModificacionObligacion.IdModObligacion", hfIdModObligacion.Value);

        int vIdCosModContractual;

        if (!string.IsNullOrEmpty(hfIDCosModContractual.Value))
        {
            vIdCosModContractual = Convert.ToInt32(hfIDCosModContractual.Value);
            SetSessionParameter("ConsModContractual.IDCosModContractual", vIdCosModContractual);
        }
        else
        {
            vIdCosModContractual = Convert.ToInt32(hfIDCosModContractualGestion.Value);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", vIdCosModContractual);
        }

       

        NavigateTo(SolutionPage.Edit);
    }
   
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        int vIdCosModContractual;

        if (! string.IsNullOrEmpty(hfIDCosModContractual.Value))
        {
            vIdCosModContractual = Convert.ToInt32(hfIDCosModContractual.Value);
            SetSessionParameter("ConsModContractual.IDCosModContractual", vIdCosModContractual);
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
        }
        else
        {
            vIdCosModContractual = Convert.ToInt32(hfIDCosModContractualGestion.Value);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", vIdCosModContractual);
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }
    
    private void CargarDatos()
    {
        try
        {
            int vIdCosModContractual;

            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdCosModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdCosModContractual.ToString();

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdCosModContractual);

                if (detalleSolicitud.Estado == "Registro" || detalleSolicitud.Estado == "Devuelta")
                    toolBar.MostrarBotonEditar(true);
                else
                    toolBar.MostrarBotonEditar(false);
            }
            else
            {
                vIdCosModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIDCosModContractualGestion.Value = vIdCosModContractual.ToString();

                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                {
                    hfEsSubscripcion.Value = "1";
                    RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                {
                    hfEsSubscripcion.Value = "2";
                    RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                }

                toolBar.MostrarBotonEditar(false);
            }

            //cargar encabezado
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdContrato.ToString();

            int vIdModObligacion = Convert.ToInt32(GetSessionParameter("ModificacionObligacion.IdModObligacion"));
            hfIdModObligacion.Value = vIdModObligacion.ToString();

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato(vIdContrato);
            txtNumeroContrato.Text = vContrato.NumeroContrato.ToString();
            txtRegional.Text = vContrato.NombreRegional.ToString();            
            txtFechaInicioContratoConve.Text = (vContrato.FechaInicioEjecucion != null) ? DateTime.Parse(vContrato.FechaInicioEjecucion.ToString()).ToShortDateString() : string.Empty;
            txtFechaFinalContratoConve.Text = (vContrato.FechaFinalTerminacionContrato != null) ? DateTime.Parse(vContrato.FechaFinalTerminacionContrato.ToString()).ToShortDateString() : string.Empty;
            txtObjetoContrato.Text = vContrato.ObjetoContrato.ToString();
            txtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato.ToString();            
            txtValorInicialContConv.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);            
            txtValorFinalContConv.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);
            // termina carga encabezado

           if (GetSessionParameter("ModificacionObligacion.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
           RemoveSessionParameter("ModificacionObligacion.Guardado");

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();
            vModificacionObligacion = vContratoService.ConsultarModificacionObligacion(vIdModObligacion);
            hfIdModObligacion.Value = vModificacionObligacion.IdModObligacion.ToString();
            txtClausulaContrato.Text = vModificacionObligacion.ClausulaContrato;
            txtDescripcionModificacion.Text = vModificacionObligacion.DescripcionModificacion;
            ObtenerAuditoria(PageName, hfIdModObligacion.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModificacionObligacion.UsuarioCrea, vModificacionObligacion.FechaCrea, vModificacionObligacion.UsuarioModifica, vModificacionObligacion.FechaModifica);

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarRegistro()
    {
        try
        {
            int vIdModObligacion = Convert.ToInt32(hfIdModObligacion.Value);

            ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();
            vModificacionObligacion = vContratoService.ConsultarModificacionObligacion(vIdModObligacion);
            InformacionAudioria(vModificacionObligacion, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarModificacionObligacion(vModificacionObligacion);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("ModificacionObligacion.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;       
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);

            toolBar.EstablecerTitulos("ModificacionObligacion", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void CargarDatosIniciales()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
}
