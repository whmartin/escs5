using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_ModificacionObligacion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/ModificacionObligacion";
    string TIPO_ESTRUCTURA = "ModificacionObligacion";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();                
                CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        int vIdCosModContractual;

        if (hfIDCosModContractual.Value != null)
        {
            vIdCosModContractual = Convert.ToInt32(hfIDCosModContractual.Value);
            SetSessionParameter("ConsModContractual.IDCosModContractual", vIdCosModContractual);
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
        }
        else
        {
            vIdCosModContractual = Convert.ToInt32(hfIDCosModContractualGestion.Value);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", vIdCosModContractual);
            Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();

            vModificacionObligacion.IDDetalleConsModContractual = Convert.ToInt32(hfIDDetalleConsModContractual.Value);
            vModificacionObligacion.ClausulaContrato = Convert.ToString(txtClausulaContrato.Text);
            vModificacionObligacion.DescripcionModificacion = Convert.ToString(txtDescripcionModificacion.Text);

            if (Request.QueryString["oP"] == "E")
            {
            vModificacionObligacion.IdModObligacion = Convert.ToInt32(hfIdModObligacion.Value);
                vModificacionObligacion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vModificacionObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarModificacionObligacion(vModificacionObligacion);
            }
            else
            {
                vModificacionObligacion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vModificacionObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarModificacionObligacion(vModificacionObligacion);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                //SetSessionParameter("ModificacionObligacion.IdModObligacion", vModificacionObligacion.IdModObligacion);
                SetSessionParameter("ModificacionObligacion.Guardado", "1");

                int vIdCosModContractual;
                if ( ! string.IsNullOrEmpty(hfIDCosModContractual.Value))
                {
                    vIdCosModContractual = Convert.ToInt32(hfIDCosModContractual.Value);
                    SetSessionParameter("ConsModContractual.IDCosModContractual", vIdCosModContractual);
                    Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);                    
                }
                else
                {
                    vIdCosModContractual = Convert.ToInt32(hfIDCosModContractualGestion.Value);
                    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", vIdCosModContractual);
                    Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("ModificacionObligacion", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            //int vIdModObligacion = 4674687468;

            //cargar encabezado
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdContrato.ToString();

            int vIdCosModContractual;

            if (GetSessionParameter("ConsModContractual.IDCosModContractual").ToString() != "")
            {
                vIdCosModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdCosModContractual.ToString();
            }
            else
            {
                vIdCosModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIDCosModContractualGestion.Value = vIdCosModContractual.ToString();
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContrato (vIdContrato);
            txtNumeroContrato.Text = vContrato.NumeroContrato.ToString();
            txtRegional.Text = vContrato.NombreRegional.ToString();
            txtFechaInicioContratoConve.Text = (vContrato.FechaInicioEjecucion != null) ? DateTime.Parse(vContrato.FechaInicioEjecucion.ToString()).ToShortDateString() : string.Empty;
            txtFechaFinalContratoConve.Text = (vContrato.FechaFinalTerminacionContrato != null) ? DateTime.Parse(vContrato.FechaFinalTerminacionContrato.ToString()).ToShortDateString() : string.Empty;
            txtObjetoContrato.Text = vContrato.ObjetoContrato.ToString();
            txtAlcanceContrato.Text = vContrato.AlcanceObjetoContrato.ToString();         
            txtValorInicialContConv.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);           
            txtValorFinalContConv.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);
            // termina carga encabezado

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            if (Request.QueryString["oP"] == "E")
            { 
                int vIdModObligacion = Convert.ToInt32(GetSessionParameter("ModificacionObligacion.IdModObligacion"));
                RemoveSessionParameter("ModificacionObligacion.IdModObligacion");

                ModificacionObligacion vModificacionObligacion = new ModificacionObligacion();
                vModificacionObligacion = vContratoService.ConsultarModificacionObligacion(vIdModObligacion);
                hfIdModObligacion.Value = vModificacionObligacion.IdModObligacion.ToString();
                hfIDDetalleConsModContractual.Value = vModificacionObligacion.IDDetalleConsModContractual.ToString();
                txtClausulaContrato.Text = vModificacionObligacion.ClausulaContrato;
                txtDescripcionModificacion.Text = vModificacionObligacion.DescripcionModificacion;
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vModificacionObligacion.UsuarioCrea, vModificacionObligacion.FechaCrea, vModificacionObligacion.UsuarioModifica, vModificacionObligacion.FechaModifica);
                PanelArchivos.Visible = true;
                gvanexos.EmptyDataText = EmptyDataText();
                gvanexos.PageSize = PageSize();
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            else
            {
                int vIDDetalleConsModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                hfIDDetalleConsModContractual.Value = vIDDetalleConsModContractual.ToString();
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }


    #endregion
}
