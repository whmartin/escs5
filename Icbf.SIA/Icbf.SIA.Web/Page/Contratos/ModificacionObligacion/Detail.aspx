<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ModificacionObligacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdModObligacion" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIDCosModContractual" runat="server" />
    <asp:HiddenField ID="hfIDCosModContractualGestion" runat="server" />
    <asp:HiddenField runat="server" ID="hfEsSubscripcion" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="auto-style2">
                Número de Contrato</td>
            <td>
                Regional</td>
        </tr>
        <tr  class="rowA">
            <td class="auto-style2">
                <span>
                                            <asp:TextBox ID="txtNumeroContrato" runat="server" Enabled="false" ReadOnly="True" MaxLength="30"></asp:TextBox>
                                        </span>
            </td>
            <td>
                <span>
                                            <asp:TextBox ID="txtRegional" runat="server" Enabled="false" MaxLength="30" ReadOnly="True"></asp:TextBox>
                                        </span>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Fecha Inicio del Contrato / Convenio</td>
            <td>
                Fecha Final del Contrato / Convenio</td>
        </tr>
        <tr  class="rowA">
            <td class="auto-style2">
                <span>
                                            <asp:TextBox ID="txtFechaInicioContratoConve" runat="server" Enabled="false" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                        </span>
            </td>
            <td>
                <span>
                                            <asp:TextBox ID="txtFechaFinalContratoConve" runat="server" Enabled="false" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                        </span>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Objeto del Contrato / Convenio</td>
            <td>
                Alcance del Objeto del Contrato / Convenio</td>
        </tr>
        <tr  class="rowA">
            <td class="auto-style2">
                <span>
                                            <asp:TextBox ID="txtObjetoContrato" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true"
                                                MaxLength="500" onKeyDown="limitText(this,500);" onKeyUp="limitText(this,500);" Height="100px" ReadOnly="True"></asp:TextBox>
                                        </span>
            </td>
            <td>
                <span>
                                            <asp:TextBox ID="txtAlcanceContrato" runat="server" Width="80%" TextMode="MultiLine" Enabled="false"
                                                onchange="prePostbck(true)" AutoPostBack="true"
                                                MaxLength="4000" onKeyDown="limitText(this,4000);" onKeyUp="limitText(this,4000);" Height="100px" ReadOnly="True"></asp:TextBox>
                                        </span>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Valor Inicial del Contrato / Convenio</td>
            <td>
                Valor Final del Contrato / Convenio</td>
        </tr>
        <tr  class="rowA">
            <td class="auto-style2">
                <span>
                                            <asp:TextBox ID="txtValorInicialContConv" runat="server" Enabled="false" onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')"
                                                onblur="onBlurTxtValorInicialContConv()" onchange="prePostbck(true)" MaxLength="25"
                                                Width="300px" AutoPostBack="true" ReadOnly="True"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftValorInicialContConv" runat="server" TargetControlID="txtValorInicialContConv"
                                                FilterType="Custom,Numbers" ValidChars=".," />
                                        </span>
            </td>
            <td>
                <span>
                                            <asp:TextBox ID="txtValorFinalContConv" runat="server" Enabled="false" onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')"
                                                onblur="onBlurTxtValorInicialContConv()" onchange="prePostbck(true)" MaxLength="25"
                                                Width="300px" AutoPostBack="true" ReadOnly="True"></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="txtValorFinalContConv_FilteredTextBoxExtender" runat="server" TargetControlID="txtValorFinalContConv"
                                                FilterType="Custom,Numbers" ValidChars=".," />
                                        </span>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Supervisores&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td class="auto-style2" colspan="2">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style2">
                Cl&#225;usula y/u Obligación a modificar
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td class="auto-style1" colspan="2">
                <asp:TextBox runat="server" ID="txtClausulaContrato" Height="204px" TextMode="MultiLine" Width="704px" ReadOnly="True"  Enabled="false"></asp:TextBox>                
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Descripción de la modificación a realizar</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcionModificacion" Height="203px" TextMode="MultiLine" Width="704px" ReadOnly="True"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>

                    <table width="90%" align="center">
        <tr class="rowB">
            <td>
              Documentos
            </td>
            <td colspan="2">
                &nbsp;</td>            
        </tr>
            <tr class="rowAG">
                              <td>
                                  <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
 

</asp:Content>
