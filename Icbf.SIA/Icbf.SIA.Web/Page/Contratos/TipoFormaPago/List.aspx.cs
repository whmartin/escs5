using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de formas de pago
/// </summary>
public partial class Page_TipoFormaPago_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoFormaPago";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreTipoFormaPago = null;
            String vDescripcion = null;
            Boolean? vEstado = null;
            if (txtNombreTipoFormaPago.Text!= "")
            {
                vNombreTipoFormaPago = Convert.ToString(txtNombreTipoFormaPago.Text);
            }
            if (txtDescripcion.Text != "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }

            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }
            gvTipoFormaPago.DataSource = vContratoService.ConsultarTipoFormaPagos(vNombreTipoFormaPago,vDescripcion, vEstado);
            gvTipoFormaPago.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.LipiarMensajeError();
            gvTipoFormaPago.PageSize = PageSize();
            gvTipoFormaPago.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Forma Pago", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoFormaPago.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoFormaPago.IdTipoFormaPago", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoFormaPago_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoFormaPago.SelectedRow);
    }
    protected void gvTipoFormaPago_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoFormaPago.PageIndex = e.NewPageIndex;
        Buscar();
    }
    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoFormaPago.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoFormaPago.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            rblEstado.SelectedIndex = -1;
           
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void  gvTipoFormaPago_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNombreTipoFormaPago = null;
        String vDescripcion = null;
        Boolean? vEstado = null;
        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }
        if (txtNombreTipoFormaPago.Text != "")
        {
            vNombreTipoFormaPago = Convert.ToString(txtNombreTipoFormaPago.Text);
        }
        if (txtDescripcion.Text != "")
        {
            vDescripcion = Convert.ToString(txtDescripcion.Text);
        }
        var myGridResults = vContratoService.ConsultarTipoFormaPagos(vNombreTipoFormaPago,vDescripcion, vEstado);
        if (myGridResults != null)
        {
            var param = Expression.Parameter(typeof(TipoFormaPago), e.SortExpression);

            var prop = Expression.Property(param, e.SortExpression);

            var sortExpression = Expression.Lambda<Func<TipoFormaPago, object>>(Expression.Convert(prop, typeof(object)), param);

            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvTipoFormaPago.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                gvTipoFormaPago.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvTipoFormaPago.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }

 }
