<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoFormaPago_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">
                    Nombre de la Forma de Pago
                </td>
                <td style="width: 50%">
                    Descripci&#243;n
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombreTipoFormaPago" Height="25px" MaxLength="50"
                        Width="320px" onKeyDown="limitText(this,50);" onKeyUp="limitText(this,50);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreTipoFormaPago" runat="server" TargetControlID="txtNombreTipoFormaPago"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                </td>
                <td class="style1">
                    <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="128" TextMode="MultiLine"
                        Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado
                </td>
                <td>
                </td>
            </tr>
            <tr  class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoFormaPago" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTipoFormaPago" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvTipoFormaPago_PageIndexChanging" OnSelectedIndexChanged="gvTipoFormaPago_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvTipoFormaPago_Sorting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Nombre de la forma de pago" DataField="NombreTipoFormaPago" SortExpression="NombreTipoFormaPago" />--%>
                            <asp:TemplateField HeaderText="Nombre de la forma de pago" ItemStyle-HorizontalAlign="Center" SortExpression="NombreTipoFormaPago">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreTipoFormaPago")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Descripci&#243;n" DataField="Descripcion" SortExpression="Descripcion" />--%>
                            <asp:TemplateField HeaderText="Descripci&#243;n" ItemStyle-HorizontalAlign="Center" SortExpression="Descripcion">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Descripcion")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                            <%--<asp:CheckBoxField HeaderText="Estado" DataField="Estado" SortExpression="Estado" />--%>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
