using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de tipos de medida
/// </summary>
public partial class Page_UnidadMedida_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/UnidadMedida";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en una fuente de datos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            UnidadMedida vUnidadMedida = new UnidadMedida();
            vUnidadMedida.NumeroContrato = GetSessionParameter("GestionarClausulaContrato.NumeroContrato").ToString();
            vUnidadMedida.FechaInicioEjecuciónContrato = txtFechaInicioEjecucionContrato.Date;
            DateTime fecha = Convert.ToDateTime(GetSessionParameter("UnidadMedida.FechaInicioEjecucion").ToString());
            if (!vUnidadMedida.FechaInicioEjecuciónContrato.ToShortDateString().Equals(fecha.ToShortDateString()))
                throw new Exception("La Fecha de inicio de Ejecución del contrato debe ser: " + fecha.ToShortDateString());
            vUnidadMedida.FechaTerminacionInicialContrato = txtFechaTerminacionInicialContrato.Date;
            vUnidadMedida.FechaFinalTerminacionContrato = txtFechaFinalTerminacionContrato.Date;
            SetSessionParameter("GestionarClausulaContrato.NumeroContrato", txtNumeroContrato.Text);
            SetSessionParameter("GestionarClausulaContrato.RequiereActa", GetSessionParameter("GestionarClausulaContrato.RequiereActa").ToString());

            if (Request.QueryString["oP"] == "E")
            {
                vUnidadMedida.IdNumeroContrato = Convert.ToInt32(hfIdNumeroContrato.Value);
                vUnidadMedida.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vUnidadMedida, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarUnidadMedida(vUnidadMedida);
                imgBcodigoUsuario.Visible = true;
            }
            else
            {
                vUnidadMedida.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vUnidadMedida, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarUnidadMedida(vUnidadMedida);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("UnidadMedida.IdNumeroContrato", vUnidadMedida.IdNumeroContrato);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("UnidadMedida.Modificado", "1");
                }
                else
                {
                    SetSessionParameter("UnidadMedida.Guardado", "1");
                    
                }
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Unidad de Medida", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdNumeroContrato = Convert.ToInt32(GetSessionParameter("UnidadMedida.IdNumeroContrato"));
            RemoveSessionParameter("UnidadMedida.Id");

            UnidadMedida vUnidadMedida = new UnidadMedida();
            vUnidadMedida = vContratoService.ConsultarUnidadMedida(vIdNumeroContrato);
            hfIdNumeroContrato.Value = vUnidadMedida.IdNumeroContrato.ToString();

            txtNumeroContrato.Text = GetSessionParameter("GestionarClausulaContrato.NumeroContrato").ToString() != String.Empty ? GetSessionParameter("GestionarClausulaContrato.NumeroContrato").ToString() : String.Empty;

            txtFechaInicioEjecucionContrato.Date = vUnidadMedida.FechaInicioEjecuciónContrato;
            txtFechaTerminacionInicialContrato.Date = vUnidadMedida.FechaTerminacionInicialContrato;
            txtFechaFinalTerminacionContrato.Date = vUnidadMedida.FechaFinalTerminacionContrato;
            String answer = GetSessionParameter("GestionarClausulaContrato.RequiereActa").ToString();
            if(GetSessionParameter("GestionarClausulaContrato.RequiereActa").ToString().Equals("True"))
            {
                rblRequiereActa.Items.FindByValue("Activo").Selected = true;
            }
            else
            {
                rblRequiereActa.Items.FindByValue("Inactivo").Selected = true;
            }
            
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vUnidadMedida.UsuarioCrea, vUnidadMedida.FechaCrea, vUnidadMedida.UsuarioModifica, vUnidadMedida.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereActa, "Activo", "Inactivo");
            txtFechaInicioEjecucionContrato.HabilitarObligatoriedad(true);
            txtFechaTerminacionInicialContrato.HabilitarObligatoriedad(true);
            txtFechaFinalTerminacionContrato.HabilitarObligatoriedad(true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
