using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de unidad de medida
/// </summary>
public partial class Page_UnidadMedida_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/UnidadMedida";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("UnidadMedida.Guardado");
                RemoveSessionParameter("UnidadMedida.Modificado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("UnidadMedida.IdNumeroContrato", hfIdNumeroContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdNumeroContrato = Convert.ToInt32(GetSessionParameter("UnidadMedida.IdNumeroContrato"));
            RemoveSessionParameter("UnidadMedida.IdNumeroContrato");

            if (GetSessionParameter("UnidadMedida.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            if (GetSessionParameter("UnidadMedida.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
            RemoveSessionParameter("UnidadMedida");


            UnidadMedida vUnidadMedida = new UnidadMedida();
            vUnidadMedida = vContratoService.ConsultarUnidadMedida(vIdNumeroContrato);
            hfIdNumeroContrato.Value = vUnidadMedida.IdNumeroContrato.ToString();
            txtNumeroContrato.Text = vUnidadMedida.NumeroContrato.ToString(); 
            SetSessionParameter("GestionarClausulaContrato.NumeroContrato", txtNumeroContrato.Text);
            txtFechaInicioEjecuciónContrato.Date = vUnidadMedida.FechaInicioEjecuciónContrato;
            txtFechaInicioEjecuciónContrato.Date.ToShortDateString();
            txtFechaTerminacionInicialContrato.Date = vUnidadMedida.FechaTerminacionInicialContrato;
            txtFechaTerminacionInicialContrato.Date.ToShortDateString();
            txtFechaFinalTerminacionContrato.Date = vUnidadMedida.FechaFinalTerminacionContrato;
            txtFechaFinalTerminacionContrato.Date.ToShortDateString(); 

            int[] datos = DiferenciaFechas(vUnidadMedida.FechaTerminacionInicialContrato, vUnidadMedida.FechaInicioEjecuciónContrato);

            txtDiaPlazoInicialEjecucion.Text = datos[2].ToString();
            txtMesPlazoInicialEjecucion.Text = datos[1].ToString();
            txtAnioPlazoInicialEjecucion.Text = datos[0].ToString();

            datos = DiferenciaFechas(vUnidadMedida.FechaFinalTerminacionContrato, vUnidadMedida.FechaInicioEjecuciónContrato);

            txtDiaPlazoTotalEjecucion.Text = datos[2].ToString();
            txtMesPlazoTotalEjecucion.Text = datos[1].ToString();
            txtAnioPlazoTotalEjecucion.Text = datos[0].ToString();

            if (GetSessionParameter("GestionarClausulaContrato.RequiereActa").ToString().Equals("True"))
            {
                rblRequiereActa.Items.FindByValue("Activo").Selected = true;
            }
            else
            {
                rblRequiereActa.Items.FindByValue("Inactivo").Selected = true;
            }

            ObtenerAuditoria(PageName, hfIdNumeroContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vUnidadMedida.UsuarioCrea, vUnidadMedida.FechaCrea, vUnidadMedida.UsuarioModifica, vUnidadMedida.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdNumeroContrato = Convert.ToInt32(hfIdNumeroContrato.Value);

            UnidadMedida vUnidadMedida = new UnidadMedida();
            vUnidadMedida = vContratoService.ConsultarUnidadMedida(vIdNumeroContrato);
            InformacionAudioria(vUnidadMedida, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarUnidadMedida(vUnidadMedida);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("UnidadMedida.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Unidad de Medida", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereActa, "Activo", "Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private int[] DiferenciaFechas(DateTime newdt, DateTime olddt)
    {
        Int32 anios;
        Int32 meses;
        Int32 dias;
        int[] datos = new int[] { 0, 0, 0};

        anios = (newdt.Year - olddt.Year);
        meses = (newdt.Month - olddt.Month);
        dias = (newdt.Day - olddt.Day);

        if (meses < 0)
        {
            anios -= 1;
            meses += 12;
        }
        if (dias < 0)
        {
            meses -= 1;
            dias += DateTime.DaysInMonth(newdt.Year, newdt.Month);
        }

        if (anios < 0)
        {
            return datos = new int[] { -1, -1, -1 }; ;
        }
        if (anios > 0)
            datos[0] = Convert.ToInt32(anios);
        if (meses > 0)
            datos[1] = Convert.ToInt32(meses);
        if (dias > 0)
            datos[2] = Convert.ToInt32(dias);

        return datos;
    } 
}
