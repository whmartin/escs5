<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_UnidadMedida_Add" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdNumeroContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td width="50%">
                Número de Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroContrato" ControlToValidate="txtNumeroContrato"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Fecha de Inicio de Ejecución del Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" MaxLength="25"
                    onfocus="blur();" class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom, Numbers" ValidChars="0123456789" />
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
            </td>
            <td>
                <uc1:fecha ID="txtFechaInicioEjecucionContrato" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Terminación Inicial contrato *
            </td>
            <td>
                Fecha final de Terminación contrato *
            </td>
            <td colspan="2" width="34%">
                &nbsp;
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="txtFechaTerminacionInicialContrato" runat="server" />
            </td>
            <td>
                <uc1:fecha ID="txtFechaFinalTerminacionContrato" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblRequiereActa" RepeatDirection="Horizontal"
                    Enabled="false" Visible="False">
                    <asp:ListItem Value="Activo">Activo</asp:ListItem>
                    <asp:ListItem Value="Inactivo">Inactivo</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 9) {
                    $('#txtNumeroContrato').val(retLupa[3]);
                    $('#txtRequiereActa').val(retLupa[8]);
                    $('#txtFechaInicioEjecucionContrato2').val(retLupa[9]);
                    $('#txtTipoContrato').val(retLupa[6]);
                    if (retLupa[8] == "True") {
                        $('#<%=rblRequiereActa.ClientID %>').find("input[value='Activo']").attr("checked", "checked");
                    } else {
                        $('#<%=rblRequiereActa.ClientID %>').find("input[value='Inactivo']").attr("checked", "checked");
                    }
                }
            }
        }

        function ChangeValue(activo) {
            var obj = document.getElementsByName('<%= rblRequiereActa.ClientID %>');
            //alert  ('Activo = ' + activo + '.');
            if (activo == true) {
                obj[0].checked = true;
                obj[1].checked = false;
                //alert ('True Activo = ' + activo + ' obj[0].checked = ' + obj[0].checked + ' obj[1].checked = ' + obj[1].checked + '.');
            } else {
                obj[0].checked = false;
                obj[1].checked = true;
                //alert ('False Activo = ' + activo + ' obj[0].checked = ' + obj[0].checked + ' obj[1].checked = ' + obj[1].checked + '.');
            }

        }
    </script>
</asp:Content>
