using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega la consulta basada en filtros de unidades de medida
/// </summary>
public partial class Page_UnidadMedida_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/UnidadMedida";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdNumeroContrato = null;
            String vNumeroContrato = String.Empty;
            DateTime? vFechaInicioEjecuciónContrato = null;
            DateTime? vFechaTerminacionInicialContrato = null;
            DateTime? vFechaFinalTerminacionContrato = null;
            if (hfIdNumeroContrato.Text != "")
            {
                vNumeroContrato = hfIdNumeroContrato.Text;
            }
            if (txtFechaInicioEjecuciónContrato.Date.ToShortDateString() != "01/01/1900")
            {
                vFechaInicioEjecuciónContrato = txtFechaInicioEjecuciónContrato.Date;
            }
            if (txtFechaTerminacionInicialContrato.Date.ToShortDateString() != "01/01/1900")
            {
                vFechaTerminacionInicialContrato = txtFechaTerminacionInicialContrato.Date;
            }
            if (txtFechaFinalTerminacionContrato.Date.ToShortDateString() != "01/01/1900")
            {
                vFechaFinalTerminacionContrato = txtFechaFinalTerminacionContrato.Date;
            }
            gvUnidadMedida.DataSource = vContratoService.ConsultarUnidadMedidas( vIdNumeroContrato, vNumeroContrato, vFechaInicioEjecuciónContrato, vFechaTerminacionInicialContrato, vFechaFinalTerminacionContrato);
            gvUnidadMedida.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvUnidadMedida.PageSize = PageSize();
            gvUnidadMedida.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Unidad de Medida", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvUnidadMedida.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("UnidadMedida.IdNumeroContrato", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvUnidadMedida_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvUnidadMedida.SelectedRow);
    }
    protected void gvUnidadMedida_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUnidadMedida.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("UnidadMedida.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("UnidadMedida.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
