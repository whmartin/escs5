<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_UnidadMedida_Detail" %>
<%@ Register src="../../../General/General/Control/fecha.ascx" tagname="fecha" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdNumeroContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número de Contrato *
            </td>
            <td>
                Fecha de Inicio de Ejecución del Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato"  ClientIDMode="Static" 
                    Enabled="False"></asp:TextBox>
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" Visible="false"/>
            </td>
            <td>
                <uc1:fecha ID="txtFechaInicioEjecuciónContrato" runat="server" Enabled="False" 
                    Visible="True" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Terminación Inicial Contrato *
            </td>
            <td>
                Fecha Final de Terminación Contrato *
            </td>
            <td colspan="2">
                Requiere Acta
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha ID="txtFechaTerminacionInicialContrato" runat="server" 
                    Enabled="False" />
            </td>
            <td>
                <uc1:fecha ID="txtFechaFinalTerminacionContrato" runat="server" 
                    Enabled="False" />
            </td>
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblRequiereActa" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Value="Activo">Activo</asp:ListItem>
	                <asp:ListItem Value="Inactivo">Inactivo</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Plazo Inicial Ejecución
            </td>
            <td>
                
            </td>
            <td>
                Plazo Total Ejecución
            </td>
            <td>
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Día
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiaPlazoInicialEjecucion"  
                    ClientIDMode="Static" Enabled="False"></asp:TextBox>
            </td>
            <td>
                Día
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDiaPlazoTotalEjecucion"  
                    ClientIDMode="Static" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Mes
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMesPlazoInicialEjecucion"  
                    ClientIDMode="Static" Enabled="False"></asp:TextBox>
            </td>
            <td>
                Mes
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMesPlazoTotalEjecucion"  
                    ClientIDMode="Static" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Año
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtAnioPlazoInicialEjecucion"  
                    ClientIDMode="Static" Enabled="False"></asp:TextBox>
            </td>
            <td>
                Año
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtAnioPlazoTotalEjecucion"  
                    ClientIDMode="Static" Enabled="False"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
