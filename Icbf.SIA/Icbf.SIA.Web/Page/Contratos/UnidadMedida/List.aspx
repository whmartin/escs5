<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_UnidadMedida_List" %>

<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Número de Contrato
                </td>
                <td>
                    Fecha de Inicio de Ejecución del Contrato
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="hfIdNumeroContrato" ClientIDMode="Static" MaxLength="25"
                        onfocus="blur();" class="enable"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="hfIdNumeroContrato"
                        FilterType="Custom, Numbers" ValidChars="0123456789" />
                    <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
                </td>
                <td>
                    <uc1:fecha ID="txtFechaInicioEjecuciónContrato" runat="server" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Fecha Terminación Inicial Contrato
                </td>
                <td>
                    Fecha Final de Terminación Contrato
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <uc1:fecha ID="txtFechaTerminacionInicialContrato" runat="server" />
                </td>
                <td>
                    <uc1:fecha ID="txtFechaFinalTerminacionContrato" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvUnidadMedida" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdNumeroContrato" CellPadding="0"
                        Height="16px" OnPageIndexChanging="gvUnidadMedida_PageIndexChanging" OnSelectedIndexChanged="gvUnidadMedida_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número Contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="Fecha de inicio de Ejecución del contrato" DataField="FechaInicioEjecuciónContrato"
                                DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Fecha   Terminación  Inicial contrato" DataField="FechaTerminacionInicialContrato"
                                DataFormatString="{0:d}" />
                            <asp:BoundField HeaderText="Fecha  final de Terminación contrato" DataField="FechaFinalTerminacionContrato"
                                DataFormatString="{0:d}" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 3) {
                    $('#hfIdNumeroContrato').val(retLupa[3]);
                }
            }
        }
    </script>
</asp:Content>
