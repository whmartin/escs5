﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupaDetallePlanCompras.aspx.cs" Inherits="Page_Contratos_Lupas_PlanCompras_LupaDetallePlanCompras" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }

        function FormatoNumero(item, valor, decimales) {

            valor = valor.split('.').join('');
            valor = valor.replace(',', '.');
            item.value = tonumber(valor, 2, 3, '.', ',');
        }


         function tonumber (valor,n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = Number(valor).toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };



        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

    </script>

    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Número consecutivo plan de compras
                    </td>
                    <td>
                        <asp:TextBox ID="txtNumeroConsecutivoPlanCompras" Enabled="false" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                                <tr class="rowB">
                    <td>
                        Valor plan de compras PACCO</td>
                    <td>
                        <asp:TextBox ID="txtValorPlanComprasPACCO" runat="server" Enabled="false" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        Valor plan de compras SITCO&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtValorPlanCompras" runat="server" OnBlur="FormatoNumero(this,this.value,2)"  Width="250px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="ftValorPlanCompras" runat="server" FilterType="Custom,Numbers" TargetControlID="txtValorPlanCompras" ValidChars=".," />
                        <asp:RequiredFieldValidator ID="rfvRegistroContrato" runat="server" ControlToValidate="txtValorPlanCompras" Display="Dynamic" Enabled="true" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                </table>
            <table id="tblModificacion" runat="server" style="display:none" width="99%">
                <tr class="rowB">
                    <td><asp:Label ID="lblTipoModificacion" runat="server" /></td>
                    <td>
                        <asp:TextBox ID="txtValorModificacion"  onkeyup="FormatoNumero(this.value,2)" onchange="FormatoNumero(this.value,2)" runat="server"  Width="250px"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxtxtValorModificacion" runat="server" FilterType="Custom,Numbers" TargetControlID="txtValorModificacion" ValidChars=".," />
                        <asp:RequiredFieldValidator ID="rfvtxtValorModificacion" runat="server" ControlToValidate="txtValorModificacion" Display="Dynamic" Enabled="false" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                </table>
            <table width="99%">
                <tr class="rowB">
                    <td colspan="2">
                        Productos plan de compras
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView runat="server" ID="gvProductoContratoConvenio" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="iddetalleobjetocontractual"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvProductoContratoConvenio_Sorting"
                            OnPageIndexChanging="gvProductoContratoConvenio_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Código del producto" DataField="codigo_producto" SortExpression="codigo_producto" />
                                <asp:BoundField HeaderText="Nombre del producto" DataField="nombre_producto" SortExpression="nombre_producto" />
                                <asp:BoundField HeaderText="Tipo producto" DataField="iddetalleobjetocontractual" SortExpression="iddetalleobjetocontractual" Visible="false" />
                                <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" SortExpression="cantidad" />
                                <asp:BoundField HeaderText="Valor unitario" DataField="valor_unitario" SortExpression="valor_unitario"
                                    DataFormatString="{0:C}" />
                                <asp:BoundField HeaderText="Tiempo" DataField="tiempo" SortExpression="tiempo" />
                                <asp:BoundField HeaderText="Valor total" DataField="valor_total" SortExpression="valor_total"
                                    DataFormatString="{0:C}" />
                                <asp:BoundField HeaderText="Unidad sde tiempo" DataField="iddetalleobjetocontractual" SortExpression="iddetalleobjetocontractual" Visible="false" />
                                <asp:BoundField HeaderText="Unidad de medida" DataField="unidad_medida" SortExpression="unidad_medida" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        Rubros plan de compras
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView runat="server" ID="gvRubrosCDP" AutoGenerateColumns="False" AllowPaging="False"
                            GridLines="None" Width="100%" DataKeyNames="idpagosdetalle" CellPadding="0"
                            Height="16px" AllowSorting="True" OnSorting="gvRubrosCDP_Sorting" OnPageIndexChanging="gvRubrosCDP_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Código rubro" DataField="codigo_rubro" SortExpression="codigo_rubro" />
                                <asp:BoundField HeaderText="Descripción rubro" DataField="idpagosdetalle" SortExpression="idpagosdetalle" Visible="false" />
<%--                                <asp:BoundField HeaderText="Descripción rubro" DataField="NombreRubro" SortExpression="NombreRubro" />--%>
                                <asp:BoundField HeaderText="Valor rubro" DataField="total_rubro" SortExpression="total_rubro"
                                    DataFormatString="{0:C}" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
        <asp:HiddenField ID="hfContratoSV" runat="server" />
    <asp:HiddenField ID="hfPlanCompras" runat="server" />
    <asp:HiddenField ID="hfIdPlanCompras" runat="server" />
    <asp:HiddenField ID="hfEsEdicion" runat="server" />
    <asp:HiddenField ID="hfidContrato" runat="server" />
    <asp:HiddenField ID="hfValorOriginalPlanCompras" runat="server" />
    <asp:HiddenField ID="hfEsReduccion" runat="server" />
    <asp:HiddenField ID="hfIdReduccion" runat="server" />
    <asp:HiddenField ID="hfEsEsAdicion" runat="server" />
    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfIdAporte" runat="server" />
    <asp:HiddenField ID="hfValorActualPlanCompras" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfEsplanComprasAdicion" runat="server" />
    <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfIdVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfAnioVigencia" runat="server" />
    <asp:HiddenField ID="hfValorVigencia" runat="server" />
    <asp:HiddenField ID="hfIdRegional" runat="server" />
    <asp:HiddenField ID="hfObjeto" runat="server" />
    <asp:HiddenField ID="hfAlcance" runat="server" />
    <asp:HiddenField ID="hfEsPrecontractual" runat="server" />


</asp:Content>
