﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasDetallePlanCompras.aspx.cs" Inherits="Page_Contratos_Lupas_PlanCompras_LupasDetallePlanCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script language="javascript" type="text/javascript">
        function selecAllChecBox(gridView) {
            var chkSelec = $("#" + gridView + " input:checkbox")[0];
            $("#" + gridView + " tr td :checkbox").each(function () {
                if (chkSelec.checked == true) {
                    this.checked = chkSelec.checked;
                } else {
                    this.checked = chkSelec.checked;
                }

            });
        }


        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

    </script>
    <asp:HiddenField ID="hfContratoSV" runat="server" />
    <asp:HiddenField ID="hfPlanCompras" runat="server" />
    <asp:HiddenField ID="hfIdPlanCompras" runat="server" />
    <asp:HiddenField ID="hfEsEdicion" runat="server" />
    <asp:HiddenField ID="hfidContrato" runat="server" />
    <asp:HiddenField ID="hfValorOriginalPlanCompras" runat="server" />
    <asp:HiddenField ID="hfEsReduccion" runat="server" />
    <asp:HiddenField ID="hfIdReduccion" runat="server" />
    <asp:HiddenField ID="hfEsEsAdicion" runat="server" />
    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfIdAporte" runat="server" />
    <asp:HiddenField ID="hfValorActualPlanCompras" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfEsplanComprasAdicion" runat="server" />
    <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfIdVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfAnioVigencia" runat="server" />
    <asp:HiddenField ID="hfValorVigencia" runat="server" />
    <asp:HiddenField ID="hfIdRegional" runat="server" />


    <div align="center">
        <asp:Panel runat="server" ID="pnlProductosRubrosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Número consecutivo plan de compras
                    </td>
                    <td>
                        <asp:Label ID="lblValorPlanComras" Text="Valor del plan de compras contrato *" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvTipoBusqueda" runat="server" ControlToValidate="txtValorPlanCompras" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr class="rowA">
                    <td>
                        <asp:TextBox ID="txtNumeroConsecutivoPlanCompras" runat="server" Width="250px" ></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtValorPlanCompras" 
                                     runat="server" 
                                     onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')" onchange="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')"
                                     Width="250px" ></asp:TextBox>
                        <Ajax:FilteredTextBoxExtender ID="ftValorPlanCompras" runat="server" TargetControlID="txtValorPlanCompras"
                        FilterType="Custom,Numbers" ValidChars=".,"  /> 
                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        Productos plan de compras
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView runat="server" ID="gvProductoContratoConvenio" AutoGenerateColumns="False"
                            AllowPaging="False" GridLines="None" Width="100%" DataKeyNames="iddetalleobjetocontractual,consecutivo"
                            CellPadding="0" Height="16px" AllowSorting="True" OnSorting="gvProductoContratoConvenio_Sorting"
                            OnPageIndexChanging="gvProductoContratoConvenio_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Código del producto" DataField="codigo_producto" SortExpression="codigo_producto" />
                                <asp:BoundField HeaderText="Nombre del producto" DataField="nombre_producto" SortExpression="nombre_producto" />
                                <asp:BoundField HeaderText="Tipo producto" DataField="tipoProductoView" SortExpression="tipoProductoView" />
                                <asp:BoundField HeaderText="Cantidad/Cupos" DataField="cantidad" SortExpression="cantidad" />
                                <asp:BoundField HeaderText="Valor unitario" DataField="valor_unitario" SortExpression="valor_unitario"
                                    DataFormatString="{0:C}" />
                                <asp:BoundField HeaderText="Tiempo" DataField="tiempo" SortExpression="tiempo" />
                                <asp:BoundField HeaderText="Valor total" DataField="valor_total" SortExpression="valor_total"
                                    DataFormatString="{0:C}" />
                                <asp:BoundField HeaderText="Unidad sde tiempo" DataField="tipotiempoView" SortExpression="tipotiempoView" />
                                <asp:BoundField HeaderText="Unidad de medida" DataField="unidad_medida" SortExpression="unidad_medida" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr class="rowB">
                    <td>
                        <asp:Label ID="lblValorPlanComras0" runat="server" Text="Valor del plan de compras PACCO" />
                    </td>
                    <td>

                        <asp:TextBox ID="txtValorPlanComprasPACCO" Enabled="true" runat="server"  Width="250px"></asp:TextBox>
                        

                    </td>
                </tr>
                <tr class="rowB">
                    <td colspan="2">
                        Rubros plan de compras
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView runat="server" ID="gvRubrosCDP" AutoGenerateColumns="False" AllowPaging="False"
                            GridLines="None" Width="100%" DataKeyNames="idpagosdetalle,consecutivo" CellPadding="0"
                            Height="16px" AllowSorting="True" OnSorting="gvRubrosCDP_Sorting" OnPageIndexChanging="gvRubrosCDP_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Código rubro" DataField="codigo_rubro" SortExpression="codigo_rubro" />
                                <asp:BoundField HeaderText="Descripción rubro" DataField="NombreRubro" SortExpression="NombreRubro" />
                                <asp:BoundField HeaderText="Valor rubro" DataField="total_rubro" SortExpression="total_rubro"
                                    DataFormatString="{0:C}" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
