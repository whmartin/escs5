﻿using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using Icbf.Contrato.DataAccess;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using WsContratosPacco;

public partial class Page_Contratos_Lupas_PlanCompras_LupasDetallePlanCompras : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa _toolBar;
    readonly ContratoService _vContratoService = new ContratoService();
    public string PageName = "Contrato/PlanComprasContratos";
    public WSContratosPACCOSoap WsContratoPacco = new WSContratosPACCOSoapClient();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (!Page.IsPostBack)
        {            
            VerficarQueryStrings();

            txtNumeroConsecutivoPlanCompras.Text = hfPlanCompras.Value;

            var estadoConsecutivo =  Request.QueryString["EstadoConsecutivo"];
           
            if (estadoConsecutivo != "8" && !string.IsNullOrEmpty(estadoConsecutivo))
            {
                SetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivoPacco", "2");
                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                string returnValues = "<script language='javascript'> " +
                                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                    "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rvValidaEstado", returnValues);
            }
            else
            Buscar();
        }
    }

    protected void gvProductoContratoConvenio_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaProductos((GridView)sender, e.SortExpression, false);
    }

    protected void gvProductoContratoConvenio_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProductoContratoConvenio.PageIndex = e.NewPageIndex;
        CargarGrillaProductos((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvRubrosCDP_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaRubros((GridView)sender, e.SortExpression, false);
    }

    protected void gvRubrosCDP_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRubrosCDP.PageIndex = e.NewPageIndex;
        CargarGrillaRubros((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string template = "?idContrato={0}&idDetConsModContractual={1}&IdEsAdicion={2}&EsAdicion={3}&vesVigenciaFutura={4}&vidVigenciaFutura={5}&vAnioVigencia={6}&vValorVigenciaFutura{7}&vIdRegContrato={8}";
        template = string.Format(template, hfidContrato.Value, hfIdDetConsModContractual.Value, hfIdAdicion.Value,hfEsEsAdicion.Value, hfesVigenciaFutura.Value, hfIdVigenciaFutura.Value,hfAnioVigencia.Value,hfValorVigencia.Value,hfIdRegional.Value);
        NavigateTo("LupasPlanCompras.aspx"+template);
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    #endregion

    #region Funciones y Procedmientos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)Master;
            if (_toolBar != null)
            {
                _toolBar.eventoGuardar += btnGuardar_Click;
                _toolBar.eventoRetornar += btnRetornar_Click;

                _toolBar.EstablecerTitulos("Plan de Compras", SolutionPage.Add.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrillaProductos(gvProductoContratoConvenio, GridViewSortExpression, true);
            CargarGrillaRubros(gvRubrosCDP, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaProductos(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var vNumeroConsecutivo = Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text);
            var vVigencia = Convert.ToInt32(Request.QueryString["Vigencia"]);

            var myGridResults = WsContratoPacco.GetListaDetalleProductoPACCO(vVigencia, vNumeroConsecutivo).OrderBy(d => d.codigo_producto);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (myGridResults.Any())
            {
                pnlProductosRubrosCrontratos.Visible = true;
                var valorTotalOriginal = Math.Round(myGridResults.Sum(pplan => pplan.valor_total).Value,2);
                hfValorOriginalPlanCompras.Value = valorTotalOriginal.ToString();
                txtValorPlanComprasPACCO.Text = valorTotalOriginal.ToString("#,###0.00##;($ #,###0.00##)");

                if (hfEsEdicion.Value == "0")
                {
                    if (!string.IsNullOrEmpty(hfEsEsAdicion.Value) || !string.IsNullOrEmpty(hfEsReduccion.Value))
                        SetValorActualPlanCompras();
                    else
                        txtValorPlanCompras.Text = myGridResults.Sum(pplan => pplan.valor_total).Value.ToString("#,###0.00##;($ #,###0.00##)");
                }
                else
                    SetValorActualPlanCompras();
            }

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                {
                    var param = Expression.Parameter(typeof(GetDetalleProductosServicio_Result), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<GetDetalleProductosServicio_Result, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Verifica el valor actual.
    /// </summary>
    private void SetValorActualPlanCompras()
    {
        var NoPlanCompras = int.Parse(hfPlanCompras.Value);
        var idContrato = int.Parse(hfidContrato.Value);
        var planCompras = _vContratoService.ConsultarPlanDeComprasContratoss(idContrato, NoPlanCompras);
        var planComprasItem = planCompras.Any() ? planCompras.First() : null;
        decimal valorAportesConModificacion = 0;
       
        var aportesContrato = _vContratoService.ObtenerAportesContrato(true, idContrato, null, null);

        var aportesDinero = aportesContrato.Where(e => e.AporteEnDinero).ToList();

        int idAdicion = !string.IsNullOrEmpty(hfIdAdicion.Value) ? int.Parse(hfIdAdicion.Value) : 0;
        int idReudccion = !string.IsNullOrEmpty(hfIdReduccion.Value) ? int.Parse(hfIdReduccion.Value) : 0;

        foreach (var itemAporte in aportesDinero)
        {
            if ((!itemAporte.EsAdicion && !itemAporte.Esreduccion) || (itemAporte.EsAdicion && itemAporte.IdAdicion != idAdicion  )) 
                valorAportesConModificacion += itemAporte.ValorAporte;
            else if (itemAporte.Esreduccion && itemAporte.IdReduccion != idReudccion)
                valorAportesConModificacion -= itemAporte.ValorAporte;
        }

        decimal valorActualAporte;

        decimal valorActualPlanCompras =0;

        if (string.IsNullOrEmpty(hfEsEsAdicion.Value) && string.IsNullOrEmpty(hfEsReduccion.Value) )
        {
            if (planComprasItem != null && planComprasItem.ValorTotal.HasValue)
                valorActualPlanCompras = planComprasItem.ValorTotal.Value;
            else
            valorActualPlanCompras = decimal.Parse(hfValorOriginalPlanCompras.Value);            
        }
        else if (valorAportesConModificacion > 0)
        valorActualPlanCompras = valorAportesConModificacion;


        if ((planComprasItem != null && planComprasItem.EsAdicion) || (!string.IsNullOrEmpty(hfEsEsAdicion.Value) && hfEsEdicion.Value == "0"))
            hfEsplanComprasAdicion.Value = "1";

        if (!string.IsNullOrEmpty(hfEsEsAdicion.Value))
            lblValorPlanComras.Text = lblValorPlanComras.Text.Substring(0,lblValorPlanComras.Text.Length -1) + ",con adición *";

        if (!string.IsNullOrEmpty(hfEsReduccion.Value))
            lblValorPlanComras.Text = lblValorPlanComras.Text.Substring(0, lblValorPlanComras.Text.Length - 1) + ",con reducción *";

        hfValorActualPlanCompras.Value = valorActualPlanCompras.ToString();

        if (!string.IsNullOrEmpty(hfEsEsAdicion.Value) && idAdicion != 0)
        {
            valorActualAporte = aportesDinero.FirstOrDefault(e => e.IdAdicion == idAdicion).ValorAporte;
            valorActualPlanCompras += valorActualAporte;
        }
        else if (!string.IsNullOrEmpty(hfEsReduccion.Value) && idReudccion != 0)
        {
            valorActualAporte = aportesDinero.FirstOrDefault(e => e.IdReduccion == idReudccion).ValorAporte;
            valorActualPlanCompras -= valorActualAporte;
        }

        txtValorPlanCompras.Text = valorActualPlanCompras.ToString("#,###0.00##;($ #,###0.00##)");
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaRubros(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var vNumeroConsecutivo = Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text);
            var vVigencia = Convert.ToInt32(Request.QueryString["Vigencia"]);

            var myGridResults = WsContratoPacco.GetListaDetalleRubroPACCO(vVigencia, vNumeroConsecutivo).OrderBy(d => d.codigo_rubro);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (myGridResults.Any())
            {
                pnlProductosRubrosCrontratos.Visible = true;
            }

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }

                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(GetDetalleRubrosServicio_Result), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<GetDetalleRubrosServicio_Result, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Guardar()
    {
        _toolBar.LipiarMensajeError();

        decimal valorTotal;

        if (!decimal.TryParse(txtValorPlanCompras.Text, out valorTotal))
        {
            _toolBar.MostrarMensajeError("El Valor del contrato del plan de compras es incorrecto.");
            return;
        }

        if (string.IsNullOrEmpty(hfEsEsAdicion.Value) && string.IsNullOrEmpty(hfEsReduccion.Value)&& string.IsNullOrEmpty(hfesVigenciaFutura.Value))
            GuardarRegistroContrato(valorTotal);
        else if (!string.IsNullOrEmpty(hfEsEsAdicion.Value))
            GuardarAdicion(valorTotal);        
        else if (!string.IsNullOrEmpty(hfesVigenciaFutura.Value))
            GuardarPlanComprasNuevoVigencias(valorTotal);
        else if (!string.IsNullOrEmpty(hfEsEdicion.Value))
            GuardarReduccion(valorTotal);
    }

    private void GuardarRegistroContrato(decimal valorTotal)
    {
        decimal valorOriginal = decimal.Parse(hfValorOriginalPlanCompras.Value);

        if (valorTotal > valorOriginal)
        {
            _toolBar.MostrarMensajeError("El Valor del contrato no puede ser mayor al valor del plan de compras.");
            return;
        }

        if (hfEsEdicion.Value == "1")
        {
            var vPlanComprasProductos = new PlanComprasProductos
            {
                UsuarioModifica = GetSessionUser().NombreUsuario,
                IdPlanDeComprasContratos = int.Parse(hfIdPlanCompras.Value),
                ValorTotal = valorTotal
            };

            var vResultado = _vContratoService.InsertarDetallePlanComprasProductosRubrosConTotal(vPlanComprasProductos, true);

            if (vResultado == 0)
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 0)
            {
                CerrarVentana();
            }
            else
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        else
        {

            //if (hfEsEdicion.Value == "1")
            //{
            //    int idPlanCompras = int.Parse(hfIdPlanCompras.Value);
            //    int vresultado = _vContratoService.ActualizarPlanComprasTotal(idPlanCompras, valorTotal);

            //    if (vresultado == 1)
            //        CerrarVentana();
            //    else
            //        _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else
            //{
                _toolBar.LipiarMensajeError();
                if (txtNumeroConsecutivoPlanCompras.Text == "")
                {
                    _toolBar.MostrarMensajeError("Se debe seleccionar como mínimo un Consecutivo del Plan de Compras.");
                    return;
                }

                if (gvProductoContratoConvenio.Rows.Count <= 0)
                {
                    _toolBar.MostrarMensajeError("No Se permite seleccionar un Consecutivo Plan de Compras sin producto(s).");
                    return;
                }

                if (gvRubrosCDP.Rows.Count <= 0)
                {
                    _toolBar.MostrarMensajeError("No Se permite seleccionar un Consecutivo Plan de Compras sin Rubro(s).");
                    return;
                }

                var vTotalProductosPorConsecutivo = (from vListRows in gvProductoContratoConvenio.Rows.Cast<GridViewRow>()
                                                     let vTipoProd = vListRows.Cells[2].Text.ToUpper()
                                                     let vNumConse = Convert.ToString(gvProductoContratoConvenio.DataKeys[vListRows.RowIndex].Values[1])
                                                     where vNumConse == txtNumeroConsecutivoPlanCompras.Text
                                                     select new
                                                     {
                                                         vTotalProductos = Convert.ToDecimal(decimal.Parse(vListRows.Cells[6].Text, NumberStyles.Currency))
                                                     }).Sum(d => d.vTotalProductos);



                if (vTotalProductosPorConsecutivo > 0 && hfContratoSV.Value == "true")
                {
                    _toolBar.MostrarMensajeError("Se requiere asociar un plan de compras sin Valor, verifque por favor.");
                    return;
                }

                var vTotalRubrosPorConsecutivo = (from vListRows in gvRubrosCDP.Rows.Cast<GridViewRow>()
                                                  let vNumConse = Convert.ToString(gvRubrosCDP.DataKeys[vListRows.RowIndex].Values[1])
                                                  where vNumConse == txtNumeroConsecutivoPlanCompras.Text
                                                  select new
                                                  {
                                                      vTotalRubros = Convert.ToDecimal(decimal.Parse(vListRows.Cells[2].Text, NumberStyles.Currency))
                                                  }).Sum(d => d.vTotalRubros);


                if (vTotalRubrosPorConsecutivo > 0 && hfContratoSV.Value == "true")
                {
                    _toolBar.MostrarMensajeError("Se requiere asociar un plan de compras sin Valor, verifque por favor.");
                    return;
                }
                if (vTotalProductosPorConsecutivo != vTotalRubrosPorConsecutivo)
                {
                    _toolBar.MostrarMensajeError("La suma de los productos no es igual a la suma de los rubros por cada Consecutivo Plan de Compras, verifique por favor.");
                    return;
                }

                try
                {
                    int contProd, contRub;
                    contProd = contRub = 0;
                    var vDtProductos = new DataTable();
                    vDtProductos.Columns.Add(new DataColumn("CodigoProducto", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("CantidadCupos", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("IdDetalleObjeto", Type.GetType("System.String")));
                    vDtProductos.Columns.Add(new DataColumn("Tiempo", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("ValorUnitario", Type.GetType("System.Decimal")));
                    vDtProductos.Columns.Add(new DataColumn("UnidadMedida", Type.GetType("System.String")));
                    vDtProductos.Columns.Add(new DataColumn("UnidadTiempo", Type.GetType("System.String")));
                    vDtProductos.Columns.Add(new DataColumn("DescripcionProducto", Type.GetType("System.String")));

                    var vDtRubros = new DataTable();
                    vDtRubros.Columns.Add(new DataColumn("CodigoRubro", Type.GetType("System.String")));
                    vDtRubros.Columns.Add(new DataColumn("ValorRubroPresupuestal", Type.GetType("System.Decimal")));
                    vDtRubros.Columns.Add(new DataColumn("IdPagosDetalle", Type.GetType("System.String")));


                    var vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));

                    foreach (GridViewRow vResultRow in gvProductoContratoConvenio.Rows)
                    {
                        var filaDatatable = vDtProductos.NewRow();
                        filaDatatable[0] = vResultRow.Cells[0].Text;
                        filaDatatable[1] = vResultRow.Cells[3].Text;
                        filaDatatable[2] = gvProductoContratoConvenio.DataKeys[vResultRow.RowIndex]["iddetalleobjetocontractual"].ToString();
                        filaDatatable[3] = vResultRow.Cells[5].Text;
                        filaDatatable[4] = Convert.ToDecimal(decimal.Parse(vResultRow.Cells[4].Text, NumberStyles.Currency));
                        filaDatatable[5] = vResultRow.Cells[8].Text;
                        filaDatatable[6] = vResultRow.Cells[7].Text;
                        filaDatatable[7] = vResultRow.Cells[1].Text;
                        vDtProductos.Rows.Add(filaDatatable);
                        contProd++;
                    }

                    foreach (GridViewRow vResultRow in gvRubrosCDP.Rows)
                    {
                        var filaDatatable = vDtRubros.NewRow();
                        filaDatatable[0] = vResultRow.Cells[0].Text;
                        filaDatatable[1] = Convert.ToDecimal(decimal.Parse(vResultRow.Cells[2].Text, NumberStyles.Currency));
                        filaDatatable[2] = gvRubrosCDP.DataKeys[vResultRow.RowIndex]["idpagosdetalle"].ToString();
                        vDtRubros.Rows.Add(filaDatatable);
                        contRub++;
                    }


                    bool? esAdicion = false;
                    int? IdAdicion = null;

                    if (!string.IsNullOrEmpty(GetSessionParameter("Adiciones.EsAdiciones").ToString()))
                    {
                        if (Convert.ToBoolean(GetSessionParameter("Adiciones.EsAdiciones")))
                        {
                            esAdicion = true;
                            IdAdicion = Convert.ToInt32(GetSessionParameter("Adiciones.IDDetalleConsModContractual"));
                        }
                    }

                    var vPlanComprasProductos = new PlanComprasProductos
                    {
                        DtDatosProductos = vDtProductos,
                        DtDatosRubros = vDtRubros,
                        UsuarioCrea = GetSessionUser().NombreUsuario,
                        IdContrato = vIdContrato,
                        NumeroConsecutivoPlanCompras = Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text),
                        Vigencia = Convert.ToInt32(Request.QueryString["Vigencia"]),
                        IdUsuario = GetSessionUser().IdUsuario,
                        EsAdicion = esAdicion,
                        IdAdicion = IdAdicion,
                        ValorTotal = valorTotal
                    };

                    InformacionAudioria(vPlanComprasProductos, PageName, vSolutionPage);
                    var vResultado = _vContratoService.InsertarDetallePlanComprasProductosRubrosConTotal(vPlanComprasProductos, false);

                    if (vResultado == 0)
                    {
                        _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado >= (contProd + contRub + 2))
                    {
                        SetSessionParameter("DetallePlanComprasProductos.Guardado", "1");
                        CerrarVentana();
                    }
                    else
                    {
                        _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                    }
                }
                catch (UserInterfaceException ex)
                {
                    _toolBar.MostrarMensajeError(ex.InnerException.Message);
                }
                catch (Exception ex)
                {
                    _toolBar.MostrarMensajeError(ex.Message);
                }
            //}
        }
    }

    private void GuardarAdicion(decimal valorTotal)
    {
        if (hfEsplanComprasAdicion.Value == "1")
            GuardarPlanComprasNuevoAdicion(valorTotal);
        else
            ModificarPlanComprasAdicion(valorTotal);
    }

    private void GuardarPlanComprasNuevoAdicion(decimal valorTotal)
    {
        decimal valorOriginal = decimal.Parse(hfValorOriginalPlanCompras.Value);

        if (valorTotal > valorOriginal)
        {
            _toolBar.MostrarMensajeError("El Valor de la adición no puede ser mayor al valor del plan de compras.");
            return;
        }

        if (hfEsEdicion.Value == "1")
        {
            var vPlanComprasProductos = new PlanComprasProductos
            {
                UsuarioModifica = GetSessionUser().NombreUsuario,
                IdPlanDeComprasContratos = int.Parse(hfIdPlanCompras.Value),
                ValorTotal = valorTotal
            };

            var vResultado = _vContratoService.InsertarDetallePlanComprasProductosRubrosConTotalAdicion(vPlanComprasProductos, true);

            if (vResultado == 0)
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            else if (vResultado > 0)
                CerrarVentana();
            else
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
        }
        else
        {
            _toolBar.LipiarMensajeError();
            if (txtNumeroConsecutivoPlanCompras.Text == "")
            {
                _toolBar.MostrarMensajeError("Se debe seleccionar como mínimo un Consecutivo del Plan de Compras.");
                return;
            }

            if (gvProductoContratoConvenio.Rows.Count <= 0)
            {
                _toolBar.MostrarMensajeError("No Se permite seleccionar un Consecutivo Plan de Compras sin producto(s).");
                return;
            }

            if (gvRubrosCDP.Rows.Count <= 0)
            {
                _toolBar.MostrarMensajeError("No Se permite seleccionar un Consecutivo Plan de Compras sin Rubro(s).");
                return;
            }

            var vTotalProductosPorConsecutivo = (from vListRows in gvProductoContratoConvenio.Rows.Cast<GridViewRow>()
                                                 let vTipoProd = vListRows.Cells[2].Text.ToUpper()
                                                 let vNumConse = Convert.ToString(gvProductoContratoConvenio.DataKeys[vListRows.RowIndex].Values[1])
                                                 where vNumConse == txtNumeroConsecutivoPlanCompras.Text
                                                 select new
                                                 {
                                                     vTotalProductos = Convert.ToDecimal(decimal.Parse(vListRows.Cells[6].Text, NumberStyles.Currency))
                                                 }).Sum(d => d.vTotalProductos);



            if (vTotalProductosPorConsecutivo > 0 && hfContratoSV.Value == "true")
            {
                _toolBar.MostrarMensajeError("Se requiere asociar un plan de compras sin Valor, verifque por favor.");
                return;
            }

            var vTotalRubrosPorConsecutivo = (from vListRows in gvRubrosCDP.Rows.Cast<GridViewRow>()
                                              let vNumConse = Convert.ToString(gvRubrosCDP.DataKeys[vListRows.RowIndex].Values[1])
                                              where vNumConse == txtNumeroConsecutivoPlanCompras.Text
                                              select new
                                              {
                                                  vTotalRubros = Convert.ToDecimal(decimal.Parse(vListRows.Cells[2].Text, NumberStyles.Currency))
                                              }).Sum(d => d.vTotalRubros);


            if (vTotalRubrosPorConsecutivo > 0 && hfContratoSV.Value == "true")
            {
                _toolBar.MostrarMensajeError("Se requiere asociar un plan de compras sin Valor, verifque por favor.");
                return;
            }
            if (vTotalProductosPorConsecutivo != vTotalRubrosPorConsecutivo)
            {
                _toolBar.MostrarMensajeError("La suma de los productos no es igual a la suma de los rubros por cada Consecutivo Plan de Compras, verifique por favor.");
                return;
            }

            try
            {
                int contProd, contRub;
                contProd = contRub = 0;
                var vDtProductos = new DataTable();
                vDtProductos.Columns.Add(new DataColumn("CodigoProducto", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("CantidadCupos", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("IdDetalleObjeto", Type.GetType("System.String")));
                vDtProductos.Columns.Add(new DataColumn("Tiempo", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("ValorUnitario", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("UnidadMedida", Type.GetType("System.String")));
                vDtProductos.Columns.Add(new DataColumn("UnidadTiempo", Type.GetType("System.String")));
                vDtProductos.Columns.Add(new DataColumn("DescripcionProducto", Type.GetType("System.String")));

                var vDtRubros = new DataTable();
                vDtRubros.Columns.Add(new DataColumn("CodigoRubro", Type.GetType("System.String")));
                vDtRubros.Columns.Add(new DataColumn("ValorRubroPresupuestal", Type.GetType("System.Decimal")));
                vDtRubros.Columns.Add(new DataColumn("IdPagosDetalle", Type.GetType("System.String")));


                var vIdContrato = Convert.ToInt32(hfidContrato.Value);

                foreach (GridViewRow vResultRow in gvProductoContratoConvenio.Rows)
                {
                    var filaDatatable = vDtProductos.NewRow();
                    filaDatatable[0] = vResultRow.Cells[0].Text;
                    filaDatatable[1] = vResultRow.Cells[3].Text;
                    filaDatatable[2] = gvProductoContratoConvenio.DataKeys[vResultRow.RowIndex]["iddetalleobjetocontractual"].ToString();
                    filaDatatable[3] = vResultRow.Cells[5].Text;
                    filaDatatable[4] = Convert.ToDecimal(decimal.Parse(vResultRow.Cells[4].Text, NumberStyles.Currency));
                    filaDatatable[5] = vResultRow.Cells[8].Text;
                    filaDatatable[6] = vResultRow.Cells[7].Text;
                    filaDatatable[7] = vResultRow.Cells[1].Text;
                    vDtProductos.Rows.Add(filaDatatable);
                    contProd++;
                }

                foreach (GridViewRow vResultRow in gvRubrosCDP.Rows)
                {
                    var filaDatatable = vDtRubros.NewRow();
                    filaDatatable[0] = vResultRow.Cells[0].Text;
                    filaDatatable[1] = Convert.ToDecimal(decimal.Parse(vResultRow.Cells[2].Text, NumberStyles.Currency));
                    filaDatatable[2] = gvRubrosCDP.DataKeys[vResultRow.RowIndex]["idpagosdetalle"].ToString();
                    vDtRubros.Rows.Add(filaDatatable);
                    contRub++;
                }

                var vPlanComprasProductos = new PlanComprasProductos
                {
                    DtDatosProductos = vDtProductos,
                    DtDatosRubros = vDtRubros,
                    UsuarioCrea = GetSessionUser().NombreUsuario,
                    IdContrato = vIdContrato,
                    NumeroConsecutivoPlanCompras = Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text),
                    Vigencia = Convert.ToInt32(Request.QueryString["Vigencia"]),
                    IdUsuario = GetSessionUser().IdUsuario,
                    EsAdicion = true,
                    IdAdicion = !string.IsNullOrEmpty(hfIdAdicion.Value) ? int.Parse(hfIdAdicion.Value.ToString()) : 0,
                    IdDetConsModContractual = ! string.IsNullOrEmpty(hfIdDetConsModContractual.Value) ? int.Parse(hfIdDetConsModContractual.Value):0,
                    ValorTotal = valorTotal
                };

                InformacionAudioria(vPlanComprasProductos, PageName, vSolutionPage);
                var vResultado = _vContratoService.InsertarDetallePlanComprasProductosRubrosConTotalAdicion(vPlanComprasProductos, false);

                if (vResultado == 0)
                {
                    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == (contProd + contRub + 2))
                {
                    SetSessionParameter("DetallePlanComprasProductos.Guardado", "1");
                    CerrarVentana();
                }
                else
                {
                    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            catch (UserInterfaceException ex)
            {
                _toolBar.MostrarMensajeError(ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                _toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    private void GuardarPlanComprasNuevoVigencias(decimal valorTotal)
    {
        //decimal valorOriginal = decimal.Parse(hfValorOriginalPlanCompras.Value);
        decimal valorVigenciaFutura = decimal.Parse(hfValorVigencia.Value);

        //if (valorTotal > valorOriginal)
        //{
        //    _toolBar.MostrarMensajeError("El Valor del contrato no puede ser mayor al valor del plan de compras.");
        //    return;
        //}

        if (valorTotal < valorVigenciaFutura)
        {
            _toolBar.MostrarMensajeError("El Valor de la Vigencia Futura no puede ser mayor al valor del plan de compras.");
            return;
        }

        if (hfEsEdicion.Value == "1")
        {
            var vPlanComprasProductos = new PlanComprasProductos
            {
                UsuarioModifica = GetSessionUser().NombreUsuario,
                IdPlanDeComprasContratos = int.Parse(hfIdPlanCompras.Value),
                ValorTotal = valorTotal
            };

            var vResultado = _vContratoService.InsertarDetallePlanComprasProductosRubrosConTotal(vPlanComprasProductos, true);

            if (vResultado == 0)
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 0)
            {
                CerrarVentana();
            }
            else
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        else
        {
            _toolBar.LipiarMensajeError();
            if (txtNumeroConsecutivoPlanCompras.Text == "")
            {
                _toolBar.MostrarMensajeError("Se debe seleccionar como mínimo un Consecutivo del Plan de Compras.");
                return;
            }

            if (gvProductoContratoConvenio.Rows.Count <= 0)
            {
                _toolBar.MostrarMensajeError("No Se permite seleccionar un Consecutivo Plan de Compras sin producto(s).");
                return;
            }

            if (gvRubrosCDP.Rows.Count <= 0)
            {
                _toolBar.MostrarMensajeError("No Se permite seleccionar un Consecutivo Plan de Compras sin Rubro(s).");
                return;
            }

            var vTotalProductosPorConsecutivo = (from vListRows in gvProductoContratoConvenio.Rows.Cast<GridViewRow>()
                                                 let vTipoProd = vListRows.Cells[2].Text.ToUpper()
                                                 let vNumConse = Convert.ToString(gvProductoContratoConvenio.DataKeys[vListRows.RowIndex].Values[1])
                                                 where vNumConse == txtNumeroConsecutivoPlanCompras.Text
                                                 select new
                                                 {
                                                     vTotalProductos = Convert.ToDecimal(decimal.Parse(vListRows.Cells[6].Text, NumberStyles.Currency))
                                                 }).Sum(d => d.vTotalProductos);



            //if (vTotalProductosPorConsecutivo > 0 && hfContratoSV.Value == "true")
            //{
            //    _toolBar.MostrarMensajeError("Se requiere asociar un plan de compras sin Valor, verifque por favor.");
            //    return;
            //}

            var vTotalRubrosPorConsecutivo = (from vListRows in gvRubrosCDP.Rows.Cast<GridViewRow>()
                                              let vNumConse = Convert.ToString(gvRubrosCDP.DataKeys[vListRows.RowIndex].Values[1])
                                              where vNumConse == txtNumeroConsecutivoPlanCompras.Text
                                              select new
                                              {
                                                  vTotalRubros = Convert.ToDecimal(decimal.Parse(vListRows.Cells[2].Text, NumberStyles.Currency))
                                              }).Sum(d => d.vTotalRubros);


            //if (vTotalRubrosPorConsecutivo > 0 && hfContratoSV.Value == "true")
            //{
            //    _toolBar.MostrarMensajeError("Se requiere asociar un plan de compras sin Valor, verifque por favor.");
            //    return;
            //}
            if (vTotalProductosPorConsecutivo != vTotalRubrosPorConsecutivo)
            {
                _toolBar.MostrarMensajeError("La suma de los productos no es igual a la suma de los rubros por cada Consecutivo Plan de Compras, verifique por favor.");
                return;
            }

            try
            {
                int contProd, contRub;
                contProd = contRub = 0;
                var vDtProductos = new DataTable();
                vDtProductos.Columns.Add(new DataColumn("CodigoProducto", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("CantidadCupos", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("IdDetalleObjeto", Type.GetType("System.String")));
                vDtProductos.Columns.Add(new DataColumn("Tiempo", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("ValorUnitario", Type.GetType("System.Decimal")));
                vDtProductos.Columns.Add(new DataColumn("UnidadMedida", Type.GetType("System.String")));
                vDtProductos.Columns.Add(new DataColumn("UnidadTiempo", Type.GetType("System.String")));
                vDtProductos.Columns.Add(new DataColumn("DescripcionProducto", Type.GetType("System.String")));

                var vDtRubros = new DataTable();
                vDtRubros.Columns.Add(new DataColumn("CodigoRubro", Type.GetType("System.String")));
                vDtRubros.Columns.Add(new DataColumn("ValorRubroPresupuestal", Type.GetType("System.Decimal")));
                vDtRubros.Columns.Add(new DataColumn("IdPagosDetalle", Type.GetType("System.String")));


                var vIdContrato = Convert.ToInt32(hfidContrato.Value);

                foreach (GridViewRow vResultRow in gvProductoContratoConvenio.Rows)
                {
                    var filaDatatable = vDtProductos.NewRow();
                    filaDatatable[0] = vResultRow.Cells[0].Text;
                    filaDatatable[1] = vResultRow.Cells[3].Text;
                    filaDatatable[2] = gvProductoContratoConvenio.DataKeys[vResultRow.RowIndex]["iddetalleobjetocontractual"].ToString();
                    filaDatatable[3] = vResultRow.Cells[5].Text;
                    filaDatatable[4] = Convert.ToDecimal(decimal.Parse(vResultRow.Cells[4].Text, NumberStyles.Currency));
                    filaDatatable[5] = vResultRow.Cells[8].Text;
                    filaDatatable[6] = vResultRow.Cells[7].Text;
                    filaDatatable[7] = vResultRow.Cells[1].Text;

                    vDtProductos.Rows.Add(filaDatatable);
                    contProd++;
                }

                foreach (GridViewRow vResultRow in gvRubrosCDP.Rows)
                {
                    var filaDatatable = vDtRubros.NewRow();
                    filaDatatable[0] = vResultRow.Cells[0].Text;
                    filaDatatable[1] = Convert.ToDecimal(decimal.Parse(vResultRow.Cells[2].Text, NumberStyles.Currency));
                    filaDatatable[2] = gvRubrosCDP.DataKeys[vResultRow.RowIndex]["idpagosdetalle"].ToString();
                    vDtRubros.Rows.Add(filaDatatable);
                    contRub++;
                }

                var vPlanComprasProductos = new PlanComprasProductos
                {
                    DtDatosProductos = vDtProductos,
                    DtDatosRubros = vDtRubros,
                    UsuarioCrea = GetSessionUser().NombreUsuario,
                    IdContrato = vIdContrato,
                    NumeroConsecutivoPlanCompras = Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text),
                    Vigencia = Convert.ToInt32(Request.QueryString["Vigencia"]),
                    IdUsuario = GetSessionUser().IdUsuario,
                    IdVigenciaFutura = Convert.ToInt32(hfIdVigenciaFutura.Value),
                    esVigenciaFutura = true,
                    ValorTotal = valorTotal
                };

                InformacionAudioria(vPlanComprasProductos, PageName, vSolutionPage);
                var vResultado = _vContratoService.InsertarDetallePlanComprasProductosRubrosConTotalVigencias(vPlanComprasProductos, false);

                if (vResultado == 0)
                {
                    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == (contProd + contRub + 3))
                {
                    SetSessionParameter("DetallePlanComprasProductos.Guardado", "1");
                    CerrarVentana();
                }
                else
                {
                    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            catch (UserInterfaceException ex)
            {
                _toolBar.MostrarMensajeError(ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                _toolBar.MostrarMensajeError(ex.Message);
            }
            //}
        } 
    }

    private void ModificarPlanComprasAdicion(decimal valorTotal)
    {
        decimal valorActual = decimal.Parse(hfValorActualPlanCompras.Value);

        if (valorTotal <= valorActual)
        {
            _toolBar.MostrarMensajeError("El Valor de la adición no puede ser menor o igual al valor actual del plan de compras.");
            return;
        }

        if (string.IsNullOrEmpty(hfIdAdicion.Value))
        {
            var valorAporte = valorTotal - valorActual;
            int idDetCons = int.Parse(hfIdDetConsModContractual.Value);
            int idAporte = int.Parse(hfIdAporte.Value);
            int idplanCompras = int.Parse(hfIdPlanCompras.Value);
            var vResultado = _vContratoService.CrearAporteDineroAdicion(idDetCons, valorAporte, GetSessionUser().NombreUsuario, true, idAporte, idplanCompras);

            if (vResultado.Key == 0)
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado.Key > 0)
            {
                SetSessionParameter("Adiciones.IdAdiccion", vResultado.Value);
                CerrarVentana();
            }
            else
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        else
        {
            var valorAporte = valorTotal - valorActual;
            int idAdicion = int.Parse(hfIdAdicion.Value);
            int idplanCompras = int.Parse(hfIdPlanCompras.Value);
            int idAporte = int.Parse(hfIdAporte.Value);
            var vResultado = _vContratoService.ActualizarAporteDineroAdicion(idAdicion, valorAporte, GetSessionUser().NombreUsuario, true, idplanCompras, idAporte);

            if (vResultado == 0)
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 0)
            {

                CerrarVentana();
            }
            else
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
    }

    private void GuardarReduccion(decimal valorReduccion)
    {
        decimal valorActual = decimal.Parse(hfValorActualPlanCompras.Value);

        if (valorReduccion >= valorActual)
        {
            _toolBar.MostrarMensajeError("El Valor de la reducción no puede ser mayor o igual al valor actual del plan de compras.");
            return;
        }

        if (string.IsNullOrEmpty(hfIdReduccion.Value))
        {
            var valorAporte = valorActual - valorReduccion;
            int idDetCons = int.Parse(hfIdDetConsModContractual.Value);
            int idAporte = int.Parse(hfIdAporte.Value);
            int idplanCompras = int.Parse(hfIdPlanCompras.Value);
            var vResultado = _vContratoService.CrearAporteDineroAdicion(idDetCons, valorAporte, GetSessionUser().NombreUsuario,false, idAporte, idplanCompras);

            if (vResultado.Key == 0)
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado.Key > 0)
            {
                SetSessionParameter("Reducciones.IdReduccion", vResultado.Value);
                CerrarVentana();
            }
            else
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        else
        {
            var valorAporte = valorActual - valorReduccion;
            int idReduccion = int.Parse(hfIdReduccion.Value);
            int idplanCompras = int.Parse(hfIdPlanCompras.Value);
            int idAporte = int.Parse(hfIdAporte.Value);
            var vResultado = _vContratoService.ActualizarAporteDineroAdicion(idReduccion, valorAporte, GetSessionUser().NombreUsuario, false, idplanCompras, idAporte);

            if (vResultado == 0)
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado > 0)
            {

                CerrarVentana();
            }
            else
            {
                _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
    }

    private void CerrarVentana()
    {
        string dialog = "Page_Contratos_LupasV11_LupasPlanCompras";
        string returnValues = "   <script src='../../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                                "   <script src='../../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                                "   <script src='../../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                                "   <script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rvConsultaPlanCompra", returnValues);
    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ContratoSV"]))
            hfContratoSV.Value = Request.QueryString["ContratoSV"];
        
        if (!string.IsNullOrEmpty(Request.QueryString["esEdicion"]))
            hfEsEdicion.Value = Request.QueryString["esEdicion"];
        
        if (!string.IsNullOrEmpty(Request.QueryString["idPlanCompras"]))
            hfIdPlanCompras.Value = Request.QueryString["idPlanCompras"];
        
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            hfidContrato.Value = Request.QueryString["idContrato"]; 
        
        if (!string.IsNullOrEmpty(Request.QueryString["NumeroConsecutivoPlanCompras"]))
            hfPlanCompras.Value = Request.QueryString["NumeroConsecutivoPlanCompras"];
        
        if (!string.IsNullOrEmpty(Request.QueryString["esReduccion"]))
            hfEsReduccion.Value = Request.QueryString["esReduccion"];

        if (!string.IsNullOrEmpty(Request.QueryString["idDetConsModContractual"]))
            hfIdDetConsModContractual.Value = Request.QueryString["idDetConsModContractual"];

        if (!string.IsNullOrEmpty(Request.QueryString["idReduccion"]))
            hfIdReduccion.Value = Request.QueryString["idReduccion"];

        if (!string.IsNullOrEmpty(Request.QueryString["EsAdicion"]))
            hfEsEsAdicion.Value = Request.QueryString["EsAdicion"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdEsAdicion"]))
            hfIdAdicion.Value = Request.QueryString["IdEsAdicion"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdAporte"]))
            hfIdAporte.Value = Request.QueryString["IdAporte"];

        if (!string.IsNullOrEmpty(Request.QueryString["vesVigenciaFutura"]))
            hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vidVigenciaFutura"]))
            hfIdVigenciaFutura.Value = Request.QueryString["vidVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vAnioVigencia"]))
            hfAnioVigencia.Value = Request.QueryString["vAnioVigencia"];

        if (!string.IsNullOrEmpty(Request.QueryString["vValorVigenciaFutura"]))
             hfValorVigencia.Value = Request.QueryString["vValorVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vIdRegContrato"]))
            hfIdRegional.Value = Request.QueryString["vIdRegContrato"];
    }

    #endregion
 
}