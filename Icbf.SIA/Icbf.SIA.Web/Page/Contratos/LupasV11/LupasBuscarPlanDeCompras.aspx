<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasBuscarPlanDeCompras.aspx.cs" Inherits="Page_PlanComprasContratos_LupasBuscarPlanDeCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfContratoSV" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfEsReduccion" runat="server" />
    <asp:HiddenField ID="hfIdReduccion" runat="server" />
    <asp:HiddenField ID="hfEsEsAdicion" runat="server" />
    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfIdAporte" runat="server" />
    <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfidVigenciaFutura" runat="server" />
    <asp:HiddenField ID="hfValorVigencia" runat="server" />
    <asp:HiddenField ID="hfAnioVigencia" runat="server" />
    <asp:HiddenField ID="hfIdRegional" runat="server" />
    <asp:HiddenField ID="hfEsPrecontractual" runat="server" />


    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Número consecutivo Plan de Compras
                </td>
                <td class="Cell">
                    Vigencia *
                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtVigencia" ControlToValidate="txtVigencia"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Font-Bold="False"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumeroConsecutivoPlanCompras" MaxLength="25"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroConsecutivoPlanCompras" runat="server"
                        TargetControlID="txtNumeroConsecutivoPlanCompras" FilterType="Numbers" ValidChars="" />
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtVigencia" MaxLength="4" AutoPostBack="True" OnTextChanged="txtVigencia_TextChanged"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroConsecutivoPlanComprass" runat="server"
                        TargetControlID="txtVigencia" FilterType="Numbers" ValidChars="" />
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Regional *
                    <asp:RequiredFieldValidator runat="server" ID="rfvddlIdRegional" ControlToValidate="ddlIdRegional"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                        ForeColor="Red" Font-Bold="False" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    Dependencia
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdRegional" AutoPostBack="True" OnSelectedIndexChanged="ddlIdRegional_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdDependencia" Enabled="False" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlIdDependencia_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Usuario dueño del Plan Compras
                </td>
                <td class="Cell">
                    Valor
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlUsuarioDuenoPlanCompras" Enabled="False"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlUsuarioDuenoPlanCompras_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList ID="ddlValor" runat="server" Enabled="False" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlValor_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Estado Plan de Compras
                </td>
                <td class="Cell">
                   <%-- Alcance--%>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList ID="ddlIdEstadoPlanCompras" runat="server" Enabled="False" OnSelectedIndexChanged="ddlIdEstadoPlanCompras_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:TextBox ID="txtIdAlcance" runat="server" MaxLength="4000" TextMode="MultiLine"
                        Width="350px" Enabled="False" AutoPostBack="True" Visible="false" OnTextChanged="txtIdAlcance_OnTextChanged"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftIdAlcance" runat="server" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers"
                        TargetControlID="txtIdAlcance" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvPlanComprasContratos" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="" CellPadding="0"
                        Height="16px" OnSorting="gvPlanComprasContratos_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvPlanComprasContratos_PageIndexChanging" OnSelectedIndexChanging="gvPlanComprasContratos_SelectedIndexChanging">
                        <Columns>
                            <asp:BoundField HeaderText="Número consecutivo plan de compras" DataField="consecutivo"
                                SortExpression="consecutivo" ItemStyle-Width="180px" />
                            <asp:BoundField HeaderText="Vigencia" DataField="anop_vigencia" SortExpression="anop_vigencia"
                                ItemStyle-Width="180px" />
                            <asp:BoundField HeaderText="Regional" DataField="codigo_regional" SortExpression="codigo_regional"
                                ItemStyle-Width="180px" />
                            <asp:BoundField HeaderText="Dependencia" DataField="codigoarea" SortExpression="codigoarea"
                                ItemStyle-Width="180px" />
                            <asp:BoundField HeaderText="Usuario dueño del plan" DataField="Usuario" SortExpression="Usuario"
                                ItemStyle-Width="180px" />
                            <asp:TemplateField HeaderText="Alcance" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 380px;">
                                        <%# Eval("alcance")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Valor" DataField="valor_contrato" SortExpression="valor_contrato"
                                DataFormatString="{0:c}" ItemStyle-Width="180px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Estado plan de compras" DataField="estado" SortExpression="estado" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        $(function () {
            $("#<%= ddlUsuarioDuenoPlanCompras.ClientID%>").change(function () {
                if ($(this).get(0).options[$(this).get(0).selectedIndex] != "-1") {
                    $("#<%= ddlValor.ClientID%>").attr("disabled", false);
                } else {
                    $("#<%= ddlValor.ClientID%>").attr("disabled", true);
                }
            });

            $("#<%= ddlValor.ClientID%>").change(function () {
                if ($(this).get(0).options[$(this).get(0).selectedIndex] != "-1") {
                    $("#<%= ddlIdEstadoPlanCompras.ClientID%>").attr("disabled", false);
                } else {
                    $("#<%= ddlIdEstadoPlanCompras.ClientID%>").attr("disabled", true);
                }
            });
        });
    </script>
</asp:Content>
