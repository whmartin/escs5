﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using WsContratosPacco;
using System.Collections.Generic;

public partial class Page_Contratos_Lupas_PlanCompras_LupasPlanCompras : GeneralWeb
{
    #region Variables

    General_General_Master_Lupa _toolBar;
    readonly ContratoService _vContratoService = new ContratoService();
    public string PageName = "Contrato/PlanComprasContratos";
    public WSContratosPACCOSoap WsContratoPacco = new WSContratosPACCOSoapClient();
    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (!Page.IsPostBack)
        {
            VerficarQueryStrings();
            Buscar();
        }
    }

    protected void gvConsecutivoContratoConvenio_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void gvConsecutivoContratoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConsecutivoContratoConvenio.SelectedRow);
    }

    protected void gvConsecutivoContratoConvenio_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConsecutivoContratoConvenio.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvConsecutivoContratoConvenio_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (! string.IsNullOrEmpty(hfEsEsAdicion.Value))
            {
               bool esAdicion = bool.Parse(gvConsecutivoContratoConvenio.DataKeys[e.Row.RowIndex].Values[3].ToString());

               if (!esAdicion)
                   ((ImageButton)e.Row.FindControl("btnEliminarContrato")).Visible = false;                   
            }
            else if (! string.IsNullOrEmpty(hfEsReduccion.Value))
            {
                ((ImageButton)e.Row.FindControl("btnEliminarContrato")).Visible = false;
              
            }
            
        }
    }

    protected void btnEliminarContrato_OnClick(object sender, ImageClickEventArgs e)
    {
        var lnk = (ImageButton)(sender);
        var objDtGridRow = (GridViewRow)(lnk.NamingContainer);
        EliminarRegistro(objDtGridRow);
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        CerrarVentana();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        var vIdAdicion = hfIdAdicion.Value;
        var vIdReduccion = hfIdReduccion.Value;
        var vIdAporte = hfIdAporte.Value;
        var esadicion = hfEsEsAdicion.Value;
        var esReduccion = hfEsReduccion.Value;
        var idDetConsMod = hfIdDetConsModContractual.Value;
        var esVigenciaFutura = hfesVigenciaFutura.Value;
        var idVigenciaFutura = hfidVigenciaFutura.Value;
        var vValorVigenciaFutura = hfValorVigencia.Value;
        var vAnioVigencia = hfAnioVigencia.Value;
        var vIdRegContrato = hfIdRegional.Value;

        string template = "LupasBuscarPlanDeCompras.aspx?ContratoSV={0}&idContrato={1}" +
                                                      "&IdEsAdicion={2}&idReduccion={3}&idAporte={4}" +
                                                      "&EsAdicion={5}&esReduccion={6}&idDetConsModContractual={7}"
                                                      + "&vesVigenciaFutura={8}&vidVigenciaFutura={9}&vValorVigenciaFutura={10}&vAnioVigencia={11}"
                                                      + "&vIdRegContrato={12}";

        if(!string.IsNullOrEmpty(hfEsPrecontractual.Value))
            template += "&esPrecontractual=1";

        var url = string.Format
                            (
                            template,
                            hfContratoSV.Value,
                            hfIdContrato.Value,
                            vIdAdicion,
                            vIdReduccion,
                            vIdAporte,
                            esadicion,
                            esReduccion,
                            idDetConsMod,
                            esVigenciaFutura,
                            idVigenciaFutura,
                            vValorVigenciaFutura,
                            vAnioVigencia,
                            vIdRegContrato
                            );
        if (String.IsNullOrEmpty(esVigenciaFutura) && String.IsNullOrEmpty(idVigenciaFutura))
        {
            NavigateTo(url);
        }
        else if (!String.IsNullOrEmpty(esVigenciaFutura) && !String.IsNullOrEmpty(idVigenciaFutura))
        {
            NavigateTo(url);
        }
    }

    #endregion

    #region Funciones y Procedmientos

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)Master;

            if (_toolBar != null)
            {
                _toolBar.eventoNuevo += btnNuevo_Click;
                _toolBar.eventoRetornar += btnRetornar_Click;
                _toolBar.EstablecerTitulos("Plan de Compras", SolutionPage.Add.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            if (ReferenceEquals(GetSessionParameter("PlanComprasContratos.Eliminado"), ""))
            {
                _toolBar.LipiarMensajeError();
            }
            else
            {
                RemoveSessionParameter("PlanComprasContratos.Eliminado");
            }

            CargarGrilla(gvConsecutivoContratoConvenio, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            var rowIndex = pRow.RowIndex;

            var vNumeroConsecutivo = Convert.ToInt32(gvConsecutivoContratoConvenio.Rows[rowIndex].Cells[1].Text);
            var vVigencia = Convert.ToInt32(gvConsecutivoContratoConvenio.Rows[rowIndex].Cells[2].Text);
            var vIdPlanDeComprasContratos = Convert.ToInt32(gvConsecutivoContratoConvenio.DataKeys[rowIndex].Values[0]);
            var vIdContrato = Convert.ToInt32(gvConsecutivoContratoConvenio.DataKeys[rowIndex].Values[2]);
            var vIdAdicion = hfIdAdicion.Value;
            var vIdReduccion = hfIdReduccion.Value;
            var vIdAporte = hfIdAporte.Value;
            var esadicion = hfEsEsAdicion.Value;
            var esReduccion = hfEsReduccion.Value;
            var idDetConsMod = hfIdDetConsModContractual.Value;
            var esVigenciaFutura = hfesVigenciaFutura.Value;
            var idVigenciaFutura = hfidVigenciaFutura.Value;
            var vAnioVigencia = hfAnioVigencia.Value;
            var vValorVigenciaFutura = hfValorVigencia.Value;
            var vIdRegContrato = hfIdRegional.Value;
            bool esAdicion = bool.Parse(gvConsecutivoContratoConvenio.DataKeys[rowIndex].Values[3].ToString());
            string esPlanComprasNuevo = string.Empty;

            if (esAdicion)    
             esPlanComprasNuevo = "1";
           
            string template = "LupaDetallePlanCompras.aspx?NumeroConsecutivoPlanCompras={0}&esEdicion=1&idContrato={1}"+
                                            "&idPlanCompras={2}&Vigencia={3}&IdEsAdicion={4}&idReduccion={5}&idAporte={6}" +
                                            "&EsAdicion={7}&esReduccion={8}&idDetConsModContractual={9}&vesVigenciaFutura={10}"
                                            + "&vidVigenciaFutura={11}&vAnioVigencia={12}&vValorVigenciaFutura={13}&vIdRegContrato={14}&esPlanComprasAdicion={15}";

            string url = string.Format
                         (
                          template,
                          vNumeroConsecutivo,                         
                          vIdContrato,
                          vIdPlanDeComprasContratos,
                          vVigencia,
                          vIdAdicion,
                          vIdReduccion,
                          vIdAporte,
                          esadicion,
                          esReduccion,
                          idDetConsMod,
                          esVigenciaFutura,
                          idVigenciaFutura,
                          vAnioVigencia,
                          vValorVigenciaFutura,
                          vIdRegContrato,
                          esPlanComprasNuevo
                         );

            if(!string.IsNullOrEmpty(hfEsPrecontractual.Value))
                url += "&esPrecontractual=1";

            NavigateTo(url);


        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        //Lleno una lista con los datos que uso para llenar la grilla

        try
        {
            var vUsuario = GetSessionUser().IdUsuario;            

            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);           

            if (hfesVigenciaFutura.Value == "1")
            {
                var myGridResults = _vContratoService.ConsultarPlanDeComprasContratosVigencias(vIdContrato, null, true, Convert.ToInt32(hfAnioVigencia.Value));

                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(PlanDeComprasContratos), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression = Expression.Lambda<Func<PlanDeComprasContratos, object>>(Expression.Convert(prop, typeof(object)), param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults;
                }

                gridViewsender.DataBind();
            }
        
            else
            {
               var myGridResults2 = _vContratoService.ConsultarPlanComprasContratoss(null, vIdContrato, null, null);

                //////////////////////////////////////////////////////////////////////////////////
                //////Fin del código de llenado de datos para la grilla 
                //////////////////////////////////////////////////////////////////////////////////

                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults2 != null)
                    {
                        var param = Expression.Parameter(typeof(PlanComprasContratos), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression = Expression.Lambda<Func<PlanComprasContratos, object>>(Expression.Convert(prop, typeof(object)), param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults2.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults2.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults2.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults2.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults2;
                }

                gridViewsender.DataBind();
            }        


        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Guarda la información de un plan de compras.
    /// </summary>
    private void Guardar()
    {
        _toolBar.LipiarMensajeError();
        if (GetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo") == null ||
            ReferenceEquals(GetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo"), "") ||
            gvConsecutivoContratoConvenio.Rows.Count == 0)
        {
            _toolBar.MostrarMensajeError("Se debe seleccionar como mínimo un consecutivo del Plan de Compras .");
            return;
        }

        SetSessionParameter("DetallePlanComprasProductos.Guardado", "1");
        CerrarVentana();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pRow"></param>
    /// <param name="pElimina"></param>
    private void EliminarRegistro(GridViewRow pRow)
    {
        try
        {
            var vResultado = 0;
            int vIdProductoPlanCompraContrato;

            if(! string.IsNullOrEmpty(hfIdContrato.Value) && ! string.IsNullOrEmpty(Request.QueryString["esPrecontractual"]))
            {
                vIdProductoPlanCompraContrato = Convert.ToInt32(gvConsecutivoContratoConvenio.DataKeys[pRow.RowIndex].Values[0]);
                var vPlanComprasContratos = _vContratoService.ConsultarPlanComprasContratos(vIdProductoPlanCompraContrato);
                InformacionAudioria(vPlanComprasContratos, this.PageName, vSolutionPage);
                vResultado = _vContratoService.EliminarPlanComprasPreContractual(vPlanComprasContratos);
            }
            else
            {
                if(!string.IsNullOrEmpty(hfEsReduccion.Value))
                    vResultado = 2;
                else
                {
                    if(ReferenceEquals(GetSessionParameter("PlanComprasContratos.Eliminado"), ""))
                    {
                        vIdProductoPlanCompraContrato = Convert.ToInt32(gvConsecutivoContratoConvenio.DataKeys[pRow.RowIndex].Values[0]);
                        var vPlanComprasContratos = _vContratoService.ConsultarPlanComprasContratos(vIdProductoPlanCompraContrato);
                        InformacionAudioria(vPlanComprasContratos, this.PageName, vSolutionPage);
                        if(hfesVigenciaFutura.Value == "1")
                        {
                            vPlanComprasContratos.EsVigenciaFutura = true;
                            vPlanComprasContratos.IdVigenciaFutura = Convert.ToInt32(hfidVigenciaFutura.Value);
                            vPlanComprasContratos.UsuarioModifica = GetSessionUser().NombreUsuario;
                        }
                        vResultado = _vContratoService.EliminarPlanComprasContratos(vPlanComprasContratos);
                        SetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo", vPlanComprasContratos.NumeroConsecutivoPlanCompras);
                        SetSessionParameter("LupasRelacionarPlanCompras.Vigencia", vPlanComprasContratos.Vigencia);
                    }
                    else
                        vResultado = 1;
                }
            }

            if(vResultado == 0)
            {
                if(ReferenceEquals(GetSessionParameter("PlanComprasContratos.Eliminado"), ""))
                    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if(vResultado >= 1)
            {
                if(! string.IsNullOrEmpty(hfIdContrato.Value))
                {
                    SetSessionParameter("DetallePlanComprasProductos.Elimino", "1");
                    GetScriptCloseDialogCallback(string.Empty);
                }
                else
                Buscar();
            }
        }
        catch (UserInterfaceException ex)
        {
            if (ex.InnerException != null)
            {
                _toolBar.MostrarMensajeError(ex.InnerException.Message);
            }
            else
            {
                _toolBar.MostrarMensajeError(ex.Message);
            }
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Verifica las variables que vienen cargadas vía query string.
    /// </summary>
    private void VerficarQueryStrings()
    {
        if (GetSessionParameter("Contrato.ContratosAPP") != null)
            hfIdContrato.Value = GetSessionParameter("Contrato.ContratosAPP").ToString();
        
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
        {
            hfIdContrato.Value = Request.QueryString["idContrato"];
        }                                              
        if (!string.IsNullOrEmpty(Request.QueryString["idDetConsModContractual"]))
            hfIdDetConsModContractual.Value = Request.QueryString["idDetConsModContractual"];

        if (!string.IsNullOrEmpty(Request.QueryString["esReduccion"]))
        {
            hfEsReduccion.Value = Request.QueryString["esReduccion"];
            _toolBar.OcultarBotonNuevo(true);
        }
        if (!string.IsNullOrEmpty(Request.QueryString["idReduccion"]))
            hfIdReduccion.Value = Request.QueryString["idReduccion"];

        if (!string.IsNullOrEmpty(Request.QueryString["EsAdicion"]))
            hfEsEsAdicion.Value = Request.QueryString["EsAdicion"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdAdicion"]))
            hfIdAdicion.Value = Request.QueryString["IdAdicion"];

        if (!string.IsNullOrEmpty(Request.QueryString["vContratoSV"]))
            hfContratoSV.Value= Request.QueryString["vContratoSV"];

        if (!string.IsNullOrEmpty(Request.QueryString["IdAporte"]))
            hfIdAporte.Value = Request.QueryString["IdAporte"];

        if (!string.IsNullOrEmpty(Request.QueryString["vesVigenciaFutura"]))
            hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vidVigenciaFutura"]))
            hfidVigenciaFutura.Value = Request.QueryString["vidVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vAnioVigencia"]))
            hfAnioVigencia.Value = Request.QueryString["vAnioVigencia"];

        if (!string.IsNullOrEmpty(Request.QueryString["vValorVigenciaFutura"]))
            hfValorVigencia.Value = Request.QueryString["vValorVigenciaFutura"];

        if (!string.IsNullOrEmpty(Request.QueryString["vIdRegContrato"]))
            hfIdRegional.Value = Request.QueryString["vIdRegContrato"];

        if(!string.IsNullOrEmpty(Request.QueryString["esPrecontractual"]))
            hfEsPrecontractual.Value = Request.QueryString["esPrecontractual"];
    }

    /// <summary>
    ///  Cierra la ventana en modo PopUp.
    /// </summary>
    private void CerrarVentana()
    {
        //string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        //string returnValues =
        //           "<script language='javascript'> " +
        //           "var pObj = Array();" +
        //           "pObj[" + (0) + "] = '1,1';" +
        //                " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
        //                " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
        //                    "</script>";


        //ClientScript.RegisterStartupScript(Page.GetType(), "rvRelacionarPlan", returnValues);
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " ;window.parent.window_closeModalDialog('dialog" + dialog + "')" +
                            "</script>";


        ClientScript.RegisterStartupScript(Page.GetType(), "rvRetornarRelacionarPlan", returnValues);
    }

    #endregion   
}
