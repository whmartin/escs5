using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using WsContratosPacco;
using System.ServiceModel;

/// <summary>
/// Página de consulta a través de filtros para la entidad PlanComprasContratos
/// </summary>
public partial class Page_PlanComprasContratos_LupasBuscarPlanDeCompras : GeneralWeb
{
    #region Variables y Constantes

    private General_General_Master_Lupa _toolBar;
    private const string PageName = "Contrato/PlanComprasContratos";
    private ContratoService _vContratoService = new ContratoService();
    private SIAService _vSiaService = new SIAService();
    private readonly ManejoControlesContratos _vManejoControlesContratos = new ManejoControlesContratos();
    private readonly WSContratosPACCOSoap _wsContratosPacco = new WSContratosPACCOSoapClient();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(_toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                VerficarQueryStrings();
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName))
                {
                    Buscar();
                }
            }
            else
            {
                if (GetSessionParameter("DetallePlanComprasProductos.Guardado") != null
                    && ReferenceEquals(GetSessionParameter("DetallePlanComprasProductos.Guardado"), "1"))
                {
                    _toolBar.MostrarMensajeGuardado();
                    RemoveSessionParameter("DetallePlanComprasProductos.Guardado");
                }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        _toolBar.LipiarMensajeError();
        //SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        //string dialog = "Page_Contratos_Lupas_PlanCompras_LupasPlanCompras";
        //string returnValues = "   <script src='../../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
        //                        "   <script src='../../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
        //                        "   <script src='../../../../Scripts/Dialog.js' type='text/javascript'></script>" +
        //                        "   <script language='javascript'> " +
        //                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
        //                    "</script>";
        //ClientScript.RegisterStartupScript(Page.GetType(), "rvConsultaPlanCompra", returnValues);

        string template = "?idContrato={0}&idDetConsModContractual={1}&IdEsAdicion={2}&EsAdicion={3}&vesVigenciaFutura={4}&vidVigenciaFutura={5}&vValorVigenciaFutura={6}&vAnioVigencia={7}&vIdRegContrato={8}";
        template = string.Format(template, hfIdContrato.Value, hfIdDetConsModContractual.Value, hfEsEsAdicion.Value, hfIdAdicion.Value, hfesVigenciaFutura.Value, hfidVigenciaFutura.Value, hfValorVigencia.Value,hfAnioVigencia.Value,hfIdRegional.Value);

        if(!string.IsNullOrEmpty(hfEsPrecontractual.Value))
            template += "&esPrecontractual=1";

        NavigateTo("LupasPlanCompras.aspx" + template);       
    }

    protected void gvPlanComprasContratos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string vParametros = "NumeroConsecutivoPlanCompras=" + gvPlanComprasContratos.Rows[e.NewSelectedIndex].Cells[0].Text
                        + "&Vigencia=" + gvPlanComprasContratos.Rows[e.NewSelectedIndex].Cells[1].Text
                        + "&EstadoConsecutivo=" + gvPlanComprasContratos.Rows[e.NewSelectedIndex].Cells[7].Text
                        + "&ContratoSV=" + hfContratoSV.Value
                        + "&esEdicion=0"
                        + "&esPlanComprasAdicion=1"
                        + "&EsAdicion=" + hfEsEsAdicion.Value
                        + "&IdEsAdicion=" + hfIdAdicion.Value
                        + "&idDetConsModContractual="+ hfIdDetConsModContractual.Value
                        + "&IdContrato=" + hfIdContrato.Value
                        + "&vesVigenciaFutura=" + hfesVigenciaFutura.Value
                        + "&vidVigenciaFutura=" + hfidVigenciaFutura.Value
                        + "&vValorVigenciaFutura=" + hfValorVigencia.Value
                        + "&vAnioVigencia=" + hfAnioVigencia.Value
                        + "&vIdRegContrato=" + hfIdRegional.Value;

        if(!string.IsNullOrEmpty(hfEsPrecontractual.Value))
            vParametros += "&esPrecontractual=1";

        NavigateTo("LupaDetallePlanCompras.aspx?"+vParametros);


        //string returnValues = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
        //                        "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
        //                        "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
        //                        "   <script language='javascript'> " +
        //                        "   function rePlanCompras(url) { " +
        //                        "       window_showModalDialog('' + url + '', setReturnrePlanCompras, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');" +
        //                        "   }" +
        //                        "   function setReturnrePlanCompras(dialog) {" +
        //                        "       var pObj = window_returnModalDialog(dialog);" +
        //                        "       if (pObj != undefined && pObj != null) {" +
        //                        "           var retLupa = pObj.split(',');" +
        //                        "           if (retLupa.length > 1) { " +
        //                        "               location.reload();}}" +
        //                        "   }" +
        //                        "   rePlanCompras('../../../Page/Contratos/LupasFormaPagos/LupasDetallePlanCompras.aspx" + vParametros + "')" +
        //                        "   </script>";
        //ClientScript.RegisterStartupScript(Page.GetType(), "rvRetornarRelacionarPlan", returnValues);
    }

    protected void gvPlanComprasContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPlanComprasContratos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvPlanComprasContratos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void txtVigencia_TextChanged(object sender, EventArgs e)
    {
        CargarControlesPorVigencia();
    }

    protected void ddlIdRegional_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarDependenciaPorRegional();
    }

    protected void ddlIdDependencia_SelectedIndexChanged(object sender, EventArgs e)
    {
        CargarUsuariosPlanPorDependencia();
    }

    protected void ddlUsuarioDuenoPlanCompras_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlValor.Enabled = ddlUsuarioDuenoPlanCompras.SelectedValue != "-1";
        //rfvddlValor.Enabled = ddlUsuarioDuenoPlanCompras.SelectedValue != "-1";

        ddlIdEstadoPlanCompras.SelectedValue = "-1";
        ddlIdEstadoPlanCompras.Enabled = ddlUsuarioDuenoPlanCompras.SelectedValue != "-1";
        //rfvddlIdEstadoPlanCompras.Enabled = ddlUsuarioDuenoPlanCompras.SelectedValue != "-1";

        txtIdAlcance.Text = string.Empty;
        txtIdAlcance.Enabled = ddlUsuarioDuenoPlanCompras.SelectedValue != "-1";
    }

    protected void ddlValor_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        ddlIdEstadoPlanCompras.Enabled = ddlValor.SelectedValue != "-1";
        //rfvddlIdEstadoPlanCompras.Enabled = ddlValor.SelectedValue != "-1";
    }

    #endregion

    #region Funciones y Procedimientos

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            _toolBar.LipiarMensajeError();

            if ((GetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivo") != null || GetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivo").ToString() != string.Empty)
                && ReferenceEquals(GetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivo"), "1"))
            {
                _toolBar.MostrarMensajeError(
                    "El consecutivo plan de compras seleccionado tiene pendiente una Solicitud de modificación.");
                RemoveSessionParameter("LupasDetallePlanCompras.EstadoConsecutivo");
            }
            else if ((GetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivo") != null || GetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivo").ToString() != string.Empty)
                    && ReferenceEquals(GetSessionParameter("LupasDetallePlanCompras.EstadoConsecutivoPacco"), "2"))
            {
                _toolBar.MostrarMensajeError("El Consecutivo Plan de Compras debe estar en estado Plan Definitivo, para ser asociado un contrato.");
                RemoveSessionParameter("LupasDetallePlanCompras.EstadoConsecutivoPacco"); //
            }
            else if (ddlIdEstadoPlanCompras.SelectedIndex != 8 && ddlIdEstadoPlanCompras.SelectedIndex != 0)
            {
                _toolBar.MostrarMensajeError("El Consecutivo Plan de Compras debe estar en estado Plan Definitivo, para ser asociado un contrato.");
                gvPlanComprasContratos.Visible = false;
                return;
            }
            else if (hfesVigenciaFutura.Value == "1" && hfAnioVigencia.Value != txtVigencia.Text)
            {
                _toolBar.MostrarMensajeError("El Consecutivo Plan de Compras debe corresponder a la misma Vigencia relacionada en la Vigencia Futura.");
                gvPlanComprasContratos.Visible = false;
                return;
            }


            //if ((txtIdAlcance.Text != "") && ((txtIdAlcance.Text.Length < 15) || (txtIdAlcance.Text.Split(Convert.ToChar(" ")).Length < 2)))
            //{
            //    _toolBar.MostrarMensajeError("El alcance debe contener como mínimo dos palabras y debe ser mayor o igual a 15 caracteres.");
            //    return;
            //}

            int? vidRegional;

            if (!string.IsNullOrEmpty(hfIdRegional.Value))
                vidRegional = Convert.ToInt32(hfIdRegional.Value);
            else
                vidRegional = _vSiaService.ConsultarUsuario(GetSessionUser().IdUsuario).IdRegional;


            int? vIdUsuario = GetSessionUser().IdUsuario;
            //var vIdRegional = _vSiaService.ConsultarUsuario(GetSessionUser().IdUsuario).IdRegional; 
            var vCodRegional = _vSiaService.ConsultarRegional(vidRegional).CodigoRegional + "00";
            string vCodRegionalAux = string.Empty;

            if (vCodRegional == "0100")
                vCodRegionalAux = "0101";

            if (vidRegional != null)
            {
                if (ddlIdRegional.SelectedValue != vCodRegional)
                {
                    if (vCodRegionalAux == string.Empty || ddlIdRegional.SelectedValue != vCodRegionalAux)
                    {
                        _toolBar.MostrarMensajeError("Debe seleccionar un Consecutivo Plan de Compras de su misma Regional.");
                        return;
                    }
                }
            }
            else
            {
                vCodRegional = ddlIdRegional.SelectedValue;
            }

            if (!txtNumeroConsecutivoPlanCompras.Text.Equals(string.Empty))
            {
               
                var vListConcecutivo = _vContratoService.ConsultarPlanComprasContratoss(Convert.ToInt32(txtVigencia.Text),
                                                                                        null,
                                                                                        Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text),
                                                                                        vIdUsuario);
                if (vListConcecutivo.Any())
                {
                    
                    foreach (Icbf.Contrato.Entity.PlanComprasContratos PlanCompras in vListConcecutivo)
                    {
                        int vIdContrato;
                        vIdContrato = PlanCompras.IdContrato;

                        if(string.IsNullOrEmpty(hfEsPrecontractual.Value))
                        {
                            _toolBar.MostrarMensajeError("El n&uacute;mero Consecutivo Plan de Compras ya se encuentra relacionado al contrato con numero de IdContrato" + " " + vIdContrato.ToString() + ".");
                        }
                        else
                        {
                            _toolBar.MostrarMensajeError("El n&uacute;mero Consecutivo Plan de Compras ya se encuentra relacionado al N&uacute;mero de Solicitud" + " " + PlanCompras.IdSolicitudContrato + " y con n&uacute;mero de Id de contrato "+ vIdContrato);
                        }

                        return;
                    }


                    
                }
            }

            gvPlanComprasContratos.Visible = true;
            CargarGrilla(gvPlanComprasContratos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)this.Master;
            if (_toolBar != null)
            {
                _toolBar.eventoRetornar += btnRetornar_Click;
                _toolBar.eventoBuscar += btnBuscar_Click;

                gvPlanComprasContratos.PageSize = PageSize();
                gvPlanComprasContratos.EmptyDataText = EmptyDataText();

                _toolBar.EstablecerTitulos("Consulta Plan de Compras", SolutionPage.List.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    //private void SeleccionarRegistro(GridViewRow pRow)
    //{
    //    try
    //    {
    //        int rowIndex = pRow.RowIndex;
    //        string strValue = gvPlanComprasContratos.DataKeys[rowIndex].Value.ToString();
    //        //NavigateTo(SolutionPage.Detail);
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        _toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        _toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var vNumeroConsecutivoPlanCompras = 0;
            var vVigencia = 0;
            String vIdRegional = null;
            var vIdDependencia = 0;
            String vUsuarioDuenoPlanCompras = null;
            var vIdEstadoPlanCompras = 0;
            String vIdAlcance = null;
            Decimal vValorDesde = 0;
            Decimal vValorHasta = 0;
            if (txtNumeroConsecutivoPlanCompras.Text != "")
            {
                vNumeroConsecutivoPlanCompras = Convert.ToInt32(txtNumeroConsecutivoPlanCompras.Text);
            }
            if (txtVigencia.Text != "")
            {
                vVigencia = Convert.ToInt32(txtVigencia.Text);
            }
            if (ddlIdRegional.SelectedValue != "-1")
            {
                vIdRegional = ddlIdRegional.SelectedValue;
            }
            if (ddlIdDependencia.SelectedValue != "-1")
            {
                vIdDependencia = Convert.ToInt32(ddlIdDependencia.SelectedValue);
            }
            if (ddlUsuarioDuenoPlanCompras.SelectedValue != "-1")
            {
                vUsuarioDuenoPlanCompras = Convert.ToString(ddlUsuarioDuenoPlanCompras.SelectedValue);
            }
            if (ddlIdEstadoPlanCompras.SelectedValue != "-1")
            {
                vIdEstadoPlanCompras = Convert.ToInt32(ddlIdEstadoPlanCompras.SelectedValue);
            }
            if (txtIdAlcance.Text != "")
            {
                vIdAlcance = Convert.ToString(txtIdAlcance.Text);
            }
            if (ddlValor.SelectedValue != "-1")
            {
                vValorDesde = Convert.ToDecimal(decimal.Parse(ddlValor.SelectedItem.Text.Split(Convert.ToChar("-"))[0], NumberStyles.Currency));
                vValorHasta = Convert.ToDecimal(decimal.Parse(ddlValor.SelectedItem.Text.Split(Convert.ToChar("-"))[1], NumberStyles.Currency));
            }

            var myGridResults = _wsContratosPacco.GetListaConsecutivosEncabezadosPACCO(Convert.ToInt32(vVigencia),
                                                                                       vIdRegional,
                                                                                       vIdEstadoPlanCompras,
                                                                                       vIdDependencia,
                                                                                       vNumeroConsecutivoPlanCompras,
                                                                                       vUsuarioDuenoPlanCompras,
                                                                                       vValorDesde,
                                                                                       vValorHasta,
                                                                                       vIdAlcance);

            //var myGridResults = _wsContratosPacco.GetListaConsecutivosEncabezadosPACCO(2014, "0800", 0, 41, 0, null, 0, 0, null);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (myGridResults.Length >= Convert.ToInt32(ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla")))
            {
                _toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta.");
            }

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                {
                    var param = Expression.Parameter(typeof(GetObjetosContractualesServicio_Result), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression =
                        Expression.Lambda<Func<GetObjetosContractualesServicio_Result, object>>(
                            Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (CommunicationException ex)
        {
            //Se ha excedido la cuota de tamaño máximo de los mensajes entrantes (65536). Para aumentar la cuota, use la propiedad MaxReceivedMessageSize en el elemento de enlace correspondiente.
            _toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta.");
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            string codigoRegional = string.Empty;

            if(! string.IsNullOrEmpty(hfIdRegional.Value))
            {
                var regional = _vContratoService.ConsultarRegionals(null, null);

                if(regional != null)
                {
                    int idRegional = int.Parse(hfIdRegional.Value);
                    var itemRegional = regional.FirstOrDefault(e => e.IdRegional == idRegional);
                    codigoRegional = itemRegional.CodigoRegional;

                    if(codigoRegional == "01")
                        codigoRegional += "01";
                    else
                        codigoRegional += "00";
                }
            }

            _vManejoControlesContratos.LlenarRegionalesPlanCompras(ddlIdRegional, "NombreRegional", "CodRegional", true);

            if(codigoRegional != string.Empty)
            {
                ddlIdRegional.SelectedValue = codigoRegional;

            }

            _vManejoControlesContratos.LlenarValoresPlanCompras(ddlValor, null, "Desde", "IdValoresContrato", true);



            /*Coloque aqui el codigo de llenar el combo.*/
            ddlUsuarioDuenoPlanCompras.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlUsuarioDuenoPlanCompras.SelectedValue = "-1";

            ddlIdEstadoPlanCompras.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdEstadoPlanCompras.SelectedValue = "-1";

            ddlIdDependencia.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdDependencia.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarControlesPorVigencia()
    {
        _toolBar.LipiarMensajeError();
        try
        {
            if (txtVigencia.Text.Equals(string.Empty))
            {
                ddlIdDependencia.Items.Clear();
                ddlIdDependencia.Enabled = false;
                ddlIdDependencia.Items.Insert(0, new ListItem("Seleccione", "-1"));
                ddlIdDependencia.SelectedValue = "-1";

                ddlUsuarioDuenoPlanCompras.Items.Clear();
                ddlUsuarioDuenoPlanCompras.Enabled = false;
                ddlUsuarioDuenoPlanCompras.Items.Insert(0, new ListItem("Seleccione", "-1"));
                ddlUsuarioDuenoPlanCompras.SelectedValue = "-1";

                ddlIdEstadoPlanCompras.Items.Clear();
                ddlIdEstadoPlanCompras.Enabled = false;
                ddlIdEstadoPlanCompras.Items.Insert(0, new ListItem("Seleccione", "-1"));
                ddlIdEstadoPlanCompras.SelectedValue = "-1";
            }
            else
            { 
                // Validación hecha para que el año ingresado no sea mayor al año en curso mas 1

                int vAñoVigencia = System.DateTime.Today.Year;
                try
                {
                    if (Convert.ToInt32(txtVigencia.Text) > vAñoVigencia + 1)
                    {
                        txtVigencia.Text = string.Empty;
                        _toolBar.MostrarMensajeError("La información registrada en el campo de Vigencia no es valida.");
                        return;
                    }
                    else
                    {
                        _toolBar.LipiarMensajeError();
                    }
                }
                catch
                {
                    txtVigencia.Text = string.Empty;
                    _toolBar.MostrarMensajeError("La información registrada en el campo de Vigencia no es valida.");
                    return;
                }


                if (txtVigencia.Text == "") return;

                if (!ddlIdRegional.SelectedValue.Equals("-1"))
                    ddlIdDependencia.Enabled = true;
                _vManejoControlesContratos.LlenarDependenciaPlanCompras(ddlIdDependencia, Convert.ToInt32(txtVigencia.Text), "area", "CodigoArea", true);

                if (!ddlIdDependencia.SelectedValue.Equals("-1"))
                    ddlUsuarioDuenoPlanCompras.Enabled = true;
                _vManejoControlesContratos.LlenarUsuariosDuenoPlanCompras(ddlUsuarioDuenoPlanCompras, Convert.ToInt32(txtVigencia.Text), "usuario", "usuario", true);

                if (!ddlUsuarioDuenoPlanCompras.SelectedValue.Equals("-1"))
                    ddlIdEstadoPlanCompras.Enabled = true;
                _vManejoControlesContratos.LlenarEstadosPlanCompras(ddlIdEstadoPlanCompras, Convert.ToInt32(txtVigencia.Text), "EstadoView", "Estado", true);
            
            }

        } catch(Exception ex)        
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDependenciaPorRegional()
    {
        _toolBar.LipiarMensajeError();
        try
        {
            if ((ddlIdRegional.SelectedValue != "-1") && (txtVigencia.Text != ""))
            {
                _vManejoControlesContratos.LlenarDependenciaPlanCompras(ddlIdDependencia, Convert.ToInt32(txtVigencia.Text), "area", "CodigoArea", true);
                ddlIdDependencia.Enabled = true;
                //rfvddlIdDependencia.Enabled = true;
                deshabilitarcamposdependientes();
            }
            else
            {
                ddlIdDependencia.Items.Clear();
                ddlIdDependencia.Items.Insert(0, new ListItem("Seleccione", "-1"));
                ddlIdDependencia.SelectedValue = "-1";
                ddlIdDependencia.Enabled = false;
                //rfvddlIdDependencia.Enabled = false;
                deshabilitarcamposdependientes();
            }
        } catch(Exception ex)        
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Por Cesar Ortiz
    /// Deshabilita los campos dependientes
    /// </summary>
    private void deshabilitarcamposdependientes() 
    {
        ddlUsuarioDuenoPlanCompras.SelectedValue = "-1";
        ddlUsuarioDuenoPlanCompras.Enabled = false;
        //rfvddlUsuarioDuenoPlanCompras.Enabled = false;
        ddlValor.SelectedValue = "-1";
        ddlValor.Enabled = false;
        //rfvddlValor.Enabled = false;
        ddlIdEstadoPlanCompras.SelectedValue = "-1";
        ddlIdEstadoPlanCompras.Enabled = false;
        //rfvddlIdEstadoPlanCompras.Enabled = false;
        txtIdAlcance.Enabled = false;
    }

    private void CargarUsuariosPlanPorDependencia()
    {
        if ((ddlIdDependencia.SelectedValue != "-1") && (txtVigencia.Text != ""))
        {
            _vManejoControlesContratos.LlenarUsuariosDuenoPlanCompras(ddlUsuarioDuenoPlanCompras, Convert.ToInt32(txtVigencia.Text), "usuario", "usuario", true);
            ddlUsuarioDuenoPlanCompras.Enabled = true;
            //rfvddlUsuarioDuenoPlanCompras.Enabled = true;
            ddlValor.SelectedValue = "-1";
            ddlValor.SelectedValue = "-1";
            ddlIdEstadoPlanCompras.SelectedValue = "-1";
            ddlIdEstadoPlanCompras.Enabled = false;
            txtIdAlcance.Text = string.Empty;
            txtIdAlcance.Enabled = false;
            ddlValor.Enabled = false;
        }
        else
        {
            ddlUsuarioDuenoPlanCompras.Items.Clear();
            ddlUsuarioDuenoPlanCompras.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlUsuarioDuenoPlanCompras.SelectedValue = "-1";
            ddlUsuarioDuenoPlanCompras.Enabled = false;
            ddlValor.SelectedValue = "-1";
            ddlIdEstadoPlanCompras.SelectedValue = "-1";
            ddlIdEstadoPlanCompras.Enabled = false;
            txtIdAlcance.Text = string.Empty;
            txtIdAlcance.Enabled = false;
            ddlValor.Enabled = false;

            //rfvddlUsuarioDuenoPlanCompras.Enabled = false;
        }
    }

    protected void ddlIdEstadoPlanCompras_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlIdEstadoPlanCompras.SelectedIndex != 8 && ddlIdEstadoPlanCompras.SelectedIndex != 0)
        {
            _toolBar.MostrarMensajeError("El Consecutivo Plan de Compras debe estar en estado Plan Definitivo, para ser asociado un contrato.");
            gvPlanComprasContratos.Visible = false;
            txtIdAlcance.Enabled = false;
        }
        else 
        {
            _toolBar.LipiarMensajeError();
            txtIdAlcance.Enabled = true;
        }

        
    }

    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ContratoSV"]))
            hfContratoSV.Value = Request.QueryString["ContratoSV"];
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            hfIdContrato.Value = Request.QueryString["idContrato"];
        if (!string.IsNullOrEmpty(Request.QueryString["idDetConsModContractual"]))
            hfIdDetConsModContractual.Value = Request.QueryString["idDetConsModContractual"];
        if (!string.IsNullOrEmpty(Request.QueryString["esReduccion"]))
            hfEsReduccion.Value = Request.QueryString["esReduccion"];
        if (!string.IsNullOrEmpty(Request.QueryString["idReduccion"]))
            hfIdReduccion.Value = Request.QueryString["idReduccion"];
        if (!string.IsNullOrEmpty(Request.QueryString["EsAdicion"]))
            hfEsEsAdicion.Value = Request.QueryString["EsAdicion"];
        if (!string.IsNullOrEmpty(Request.QueryString["IdEsAdicion"]))
          hfIdAdicion.Value = Request.QueryString["IdEsAdicion"];
        if (!string.IsNullOrEmpty(Request.QueryString["vesVigenciaFutura"]))
            hfesVigenciaFutura.Value = Request.QueryString["vesVigenciaFutura"];
        if (!string.IsNullOrEmpty(Request.QueryString["vidVigenciaFutura"]))
            hfidVigenciaFutura.Value = Request.QueryString["vidVigenciaFutura"];
        if (!string.IsNullOrEmpty(Request.QueryString["vValorVigenciaFutura"]))
            hfValorVigencia.Value = Request.QueryString["vValorVigenciaFutura"];
        if (!string.IsNullOrEmpty(Request.QueryString["vAnioVigencia"]))
            hfAnioVigencia.Value = Request.QueryString["vAnioVigencia"];
        if (!string.IsNullOrEmpty(Request.QueryString["vIdRegContrato"]))
            hfIdRegional.Value = Request.QueryString["vIdRegContrato"];
        if(!string.IsNullOrEmpty(Request.QueryString["idRegional"]))
            hfIdRegional.Value = Request.QueryString["idRegional"];
        if(!string.IsNullOrEmpty(Request.QueryString["esPrecontractual"]))
            hfEsPrecontractual.Value = Request.QueryString["esPrecontractual"];
    }

    protected void txtIdAlcance_OnTextChanged(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(txtIdAlcance.Text))
        {
            _toolBar.MostrarMensajeError("El alcance debe contener como mínimo dos palabras y debe ser mayor o igual a 10 caracteres.");
            return;
        }
        else if (((txtIdAlcance.Text.Length <= 15) || (txtIdAlcance.Text.Split(Convert.ToChar(" ")).Length <= 2)))
        {
            _toolBar.MostrarMensajeError("El alcance debe contener como mínimo dos palabras y debe ser mayor o igual a 15 caracteres.");
            return;
        }
    }

    #endregion


}
