﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasPlanCompras.aspx.cs" Inherits="Page_Contratos_Lupas_PlanCompras_LupasPlanCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <div align="center">
       <asp:HiddenField ID="hfIdContrato" runat="server" />
       <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
       <asp:HiddenField ID="hfEsReduccion" runat="server" />
       <asp:HiddenField ID="hfIdReduccion" runat="server" />
        <asp:HiddenField ID="hfEsEsAdicion" runat="server" />
        <asp:HiddenField ID="hfIdAdicion" runat="server" />
        <asp:HiddenField ID="hfIdAporte" runat="server" />
        <asp:HiddenField ID="hfPostbck" runat="server" />
        <asp:HiddenField ID="hfContratoSV" runat="server" />
        <asp:HiddenField ID="hfesVigenciaFutura" runat="server" />
        <asp:HiddenField ID="hfidVigenciaFutura" runat="server" />
        <asp:HiddenField ID="hfAnioVigencia" runat="server" />
        <asp:HiddenField ID="hfValorVigencia" runat="server" />
        <asp:HiddenField ID="hfIdRegional" runat="server" />
        <asp:HiddenField ID="hfEsPrecontractual" runat="server" />


        <asp:Panel runat="server" ID="pnlProductosCrontratos">
            <table width="99%">
                <tr class="rowB">
                    <td>
                        Número consecutivo plan de compras</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvConsecutivoContratoConvenio" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IdPlanDeComprasContratos,CodigoRegional,IdContrato,EsAdicion" GridLines="None" Height="16px" OnPageIndexChanging="gvConsecutivoContratoConvenio_PageIndexChanging" OnSelectedIndexChanged="gvConsecutivoContratoConvenio_SelectedIndexChanged" OnSorting="gvConsecutivoContratoConvenio_Sorting" Width="100%" OnRowDataBound="gvConsecutivoContratoConvenio_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" Height="16px" ImageUrl="~/Image/btn/info.jpg" ToolTip="Cargar productos y rubros" Width="16px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NumeroConsecutivoPlanCompras" HeaderText="Número consecutivo plan de compras" SortExpression="NumeroConsecutivoPlanCompras" />
                                <asp:BoundField DataField="Vigencia" HeaderText="Vigencia" SortExpression="Vigencia" />
                                <%--<asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminarContrato" runat="server" CommandName="Eliminar" Height="16px" ImageUrl="~/Image/btn/delete.gif" OnClick="btnEliminarContrato_OnClick" OnClientClick="javascript:if (!confirm('¿Está seguro de que desea eliminar el registro?')) return false;" ToolTip="Eliminar" Width="16px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRubrosCrontratos" Visible="False">
            <table width="99%">
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <script type="text/javascript" language="javascript">
        function rePlanCompras(url) {
            window_showModalDialog(url, setReturnrePlanCompras, 'Width:1250px;Height:600px;resizable:yes;');
        }

        function rePlanComprasEdit() {
            window_showModalDialog('../../../Page/Contratos/LupasFormaPagos/LupasProductosPlanCompras.aspx', setReturnrePlanCompras, 'Width:1250px;Height:600px;resizable:yes;');
        }
        //function setReturnrePlanCompras(dialog) {
        //    var pObj = window_returnModalDialog(dialog);
        //    if (pObj != undefined && pObj != null) {

        //        var retLupa = pObj.split(",");
        //        if (retLupa.length > 3) {
        //            document.getElementById('cphCont_TextBox8').value = retLupa[3];
        //        }
        //    }
        //}

        //function GetPlanCompras() {
        //    muestraImagenLoading();
        //    //CU-40-CONT-SOLI
        //    window_showModalDialog('../../../Page/Contratos/LupasFormaPagos/LupasRelacionarPlanCompras.aspx', setReturnGetPlanCompras, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        //}
        function setReturnrePlanCompras(dialog) {
            prePostbck(false);
            __doPostBack('', '');
        }

        



        //$(function () {
        //    $("#btnNuevo, #btnDetalle ").removeAttr("href");
        //});
    </script>
</asp:Content>
