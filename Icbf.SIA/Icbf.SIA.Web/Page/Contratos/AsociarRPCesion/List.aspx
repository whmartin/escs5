<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_AsociarRpCesion_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&uacute;mero del Contrato / Convenio
            </td>
            <td>
                Vigencia Fiscal 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato"  ></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddlVigenciaFiscalinicial" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            
            <td>
                Regional 
            </td>
            <td>
                Categoria del Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:DropDownList runat="server" ID="ddlRegional"  ></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlIDCategoriaContrato" runat="server" AutoPostBack="True" onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            
            <td>
                Tipo de Contrato/Convenio</td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:DropDownList ID="ddlIDTipoContrato" runat="server">
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvConsModContractual" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDCosModContractual,IDContrato,IdCesion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvConsModContractual_PageIndexChanging" OnSelectedIndexChanged="gvConsModContractual_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No Contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="No Solicitud" DataField="IDCosModContractual" />
                            <asp:BoundField HeaderText="Tipo de Modificaci&oacute;n" DataField="TipoModificacion" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />     
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
