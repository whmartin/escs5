using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_AsociarRpCesion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/AsociarRpCesion";
    ContratoService vContratoService = new ContratoService();
    private const string CONTRATO_INTERADMINSITRATIVO = "CONVENIO INTERADMINISTRATIVO";

    private string nConsecutivosPlanCompras
    { set { hfConsecutivosPlanCompras.Value = value; } }
    private string TotalProductos
    {
        get
        {
            return hfTotalProductos.Value;
        }
        set
        {
            hfTotalProductos.Value = value;
        }
    }
    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();
            }
            else
            {
                string sControlName = Request.Params.Get("__EVENTTARGET");
                switch (sControlName)
                {
                    case "cphCont_txtRP":
                        txtRP_TextChanged(sender, e);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoAprobar += toolBar_eventoAprobar;
            toolBar.SetAprobarConfirmation("return Aprobacion()");
            toolBar.EstablecerTitulos("Asociar RP Cesiones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoAprobar(object sender, EventArgs e)
    {
        try
        {
            if (! string.IsNullOrEmpty(txtRP.Text))
            {
                int idCesion = int.Parse(hfIdCesion.Value);
                vContratoService.ActualizarCesionRP(idCesion);
                SetSessionParameter("AsociarRP.Cesion", hfIdContrato.Value);
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            IdContrato = GetSessionParameter("Contrato.IdContrato").ToString();
            RemoveSessionParameter("Contrato.IdContrato");

            int vIdConModContratual;

            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);

                toolBar.MostrarBotonNuevo(false);                     
                toolBar.MostrarBotonEditar(false);
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoAdiciones(Convert.ToInt32(IdContrato));
            txtContrato.Text = string.IsNullOrEmpty(vContrato.NumeroContrato) ? string.Empty : vContrato.NumeroContrato;
            txtRegional.Text = vContrato.NombreRegional ?? vContrato.NombreRegional;
            idRegional.Value = vContrato.IdRegionalContrato.ToString();
            idVigencia.Value = vContrato.IdVigenciaInicial.ToString();
            idVigenciaF.Value = vContrato.IdVigenciaFinal.ToString();
            txtFechaInicio.Text = Convert.ToDateTime(vContrato.FechaInicioEjecucion).ToString("dd/MM/yyyy");
            txtFechaFinal.Text =Convert.ToDateTime(vContrato.FechaFinalTerminacion).ToString("dd/MM/yyyy");
            txtobjetoContrato.Text = vContrato.ObjetoContrato;
            txtalcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            txtvalorini.Text = string.Format("{0:C}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:C}", vContrato.ValorFinalContrato);

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }
    
    private void CargarRegistro()
    {
        try
        {
            try
            {
                if (!string.IsNullOrEmpty(GetSessionParameter("Cesion.IdCesion").ToString()) || !string.IsNullOrEmpty(hfIdCesion.Value))
                {
                    var vIdContrato = Convert.ToInt32(hfIdContrato.Value);

                    var itemContrato = vContratoService.ConsultarContratoReduccion(vIdContrato);

                    if (!string.IsNullOrEmpty(GetSessionParameter("Cesion.IdCesion").ToString()))
                    {
                        int vIdCesion = Convert.ToInt32(GetSessionParameter("Cesion.IdCesion"));
                        RemoveSessionParameter("Cesion.IdCesion");
                        hfIdCesion.Value = vIdCesion.ToString();

                        var cesion = vContratoService.ConsultarCesiones(vIdCesion);
                        
                        toolBar.OcultarBotonGuardar(false);
                        toolBar.MostrarBotonNuevo(false);

                        lblRP.Visible = true;
                        txtRP.Visible = true;

                        txtvalorCesion.Text = string.Format("{0:$#,##0}", cesion.ValorCesion);
                        txtvalorEjecutado.Text = string.Format("{0:$#,##0}", cesion.ValorEjecutado);

                        var item = vContratoService.ConsultarRPContratosAsociados(vIdContrato, vIdCesion, false);

                        if (item != null)
                            txtRP.Text = item.NumeroRP.ToString();
                    }
                    else
                    {
                        toolBar.MostrarBotonNuevo(true);
                        toolBar.OcultarBotonGuardar(true);
                    }

                    CargarContratistasActuales();
                    CargarContratistasCesion();
                }
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            //throw new GenericException(ex);
        }
    }

    protected void txtRP_TextChanged(object sender, EventArgs e)
    {
        try
        {
            int idContrato = Convert.ToInt32(hfIdContrato.Value);
            int idCesion = Convert.ToInt32(hfIdCesion.Value);
            var item = vContratoService.ConsultarRPContratosAsociados(idContrato, idCesion,false);
            if (item != null)
                txtRP.Text = item.NumeroRP.ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarContratistasActuales()
    {
        int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
        List<Cesiones> contratistas = vContratoService.ConsultarContratisasActualesCesiones(vIdContrato, null);
        gvContratistasCargar.DataSource = contratistas;
        gvContratistasCargar.DataBind();
    }

    private void CargarContratistasCesion()
    {
        if (!string.IsNullOrEmpty(hfIdCesion.Value))
        {
            int vIdCesion = Convert.ToInt32(hfIdCesion.Value);
            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            List<Cesiones> contratistas = vContratoService.ConsultarContratisasCesion(vIdContrato, vIdCesion, null);
            gvContratistas.DataSource = contratistas;
            gvContratistas.DataBind();
        }
    }
}
