<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_AsociarRpCesion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="idRegional" runat="server" />
    <asp:HiddenField ID="idVigencia" runat="server" />
    <asp:HiddenField ID="idVigenciaF" runat="server" />

    <script type="text/javascript">


        function Aprobacion() {

            return confirm("Esta segúro que desea aprobar el RP?");
        }

    </script>
    <asp:HiddenField runat="server" ID="hfEsSubscripcion" />
   <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td style="width: 50%">
                Regional
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px"></asp:TextBox>
               
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjetoContrato" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcanceContrato"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorini"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
            <asp:Label ID="lblRP" Text="Registro Presupuestal" Visible="false" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox ID="txtRP" runat="server" Enabled="false" Visible="false" ViewStateMode="Enabled"
                    OnTextChanged="txtRP_TextChanged" AutoPostBack="true"></asp:TextBox>
                <asp:ImageButton ID="imgRP" runat="server"  CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                     OnClientClick="GetRP(); return false;" Style="cursor: hand"
                    ToolTip="Buscar" Width="20px" />
                </td>
            <td>
                &nbsp;</td>
        </tr>

             <tr class="rowB">
            <td class="style1">
                Valor Ejecutado&nbsp;
            </td>

            <td>
                Valor Cesión</td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorEjecutado" Enabled="false"
                     MaxLength="128" Width="320px" Height="22px"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorCesion" Enabled="false"   
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Supervisores&nbsp;</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </td>
        </tr>
    </table>
    <style type="text/css">
        table .grillaCentral tr.rowAG td, table .grillaCentral tr.rowBG td
        {
            text-align: center;
            padding: 5px;
            padding-right: 0px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function ValidaPlanCompras(source, args) {
            var hfConsecutivosPlanCompras = document.getElementById('<%= hfConsecutivosPlanCompras.ClientID %>');
            if (parseInt(hfConsecutivosPlanCompras.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function mensajeAlerta(control, texto) {
            alert(texto);
            var creference = document.getElementById(control);
            creference.focus();
        }

        function ValidaEliminacion() {
            return confirm('Esta seguro de que desea eliminar el registro?');
        }

        function PreGuardado() {
            ConfiguraValidadores(false);
            muestraImagenLoading();
        }


        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        function prePostbck(imagenLoading) {
            if (!window.Page_IsValid) {
                __doPostBack('<%= hfPostbck.ClientID %>', '');
            }

            if (imagenLoading == true)
                muestraImagenLoading();
        }

        function EjecutarJSFechaInicialSupervisores(control, fechaOriginal) {
            $("#lblError")[0].innerHTML = '';
            $("#lblError")[0].className = '';
            var fechaInicioEjecucion = document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value;
            var fechaInicioSupervisor = document.getElementById(control.id).value;

            if (fechaInicioEjecucion != '') {
                var splitfechainicio = fechaInicioEjecucion.split('/');
                var jfechaInicioEjecucion = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                splitfechainicio = fechaInicioSupervisor.split('/');
                var jfechaInicioSupervisor = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                if (jfechaInicioSupervisor < jfechaInicioEjecucion) {
                    document.getElementById(control.id).value = fechaOriginal;
                    alert('La Fecha de Inicio de Supervisor y/o Interventor debe ser mayor o igual a la Fecha de Ejecución de Contrato/Convenio');
                }
            }
        }

    </script>
    <script type="text/javascript" language="javascript">
        /*****************************************************************************
        Código para colocar los indicadores de miles  y decimales mientras se escribe
        Script creado por Tunait!
        Si quieres usar este script en tu sitio eres libre de hacerlo con la condición de que permanezcan intactas estas líneas, osea, los créditos.

        http://javascript.tunait.com
        tunait@yahoo.com  27/Julio/03
        ******************************************************************************/
        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()
        }

        function GetRP() {

            muestraImagenLoading();
            var idCesion = document.getElementById('<%= hfIdCesion.ClientID %>');
            var idRegional = document.getElementById('<%= idRegional.ClientID %>');
            var idVigenciaInicial = document.getElementById('<%= idVigencia.ClientID %>');
            var idVigenciaFinal = document.getElementById('<%= idVigenciaF.ClientID %>');

            var url = '../../../Page/Contratos/Lupas/LupaListRP.aspx?esCesion=' + idCesion.value + '&IdRegContrato=' + idRegional.value + '&IdVigenciaI=' + idVigenciaInicial.value + '&IdVigenciaF=' + idVigenciaFinal.value;

            window_showModalDialog(url, setReturnGetRP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetRP(dialog) {
            prePostbck(false);
            __doPostBack('<%= txtRP.ClientID %>', '');
        }

    </script>
    
    <table width="90%" align="center">
                <tr class="rowB">
            <td >
                Contratistas Actuales
            </td>
        </tr>
         <tr>
            <td>

                <span>
                                            <asp:GridView ID="gvContratistasCargar" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                 <%--   <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClickCargar"
                                                                CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Medio Pago" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </span>

            </td>
        </tr>
        <tr class="rowB">
            <td >
              <asp:Label ID="lblTipoCesion" runat="server" Text="Contratistas Cesionario" />
            </td>
        </tr>
        <tr >
            <td >
              
                                                            <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                    <%--<asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Medio Pago" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
            </td>
        </tr>

    </table>
    <asp:HiddenField ID="hfIDDetalleConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIDCosModContractual" runat="server" />
    <asp:HiddenField ID="hfPostbck" runat="server" />
    <asp:HiddenField ID="hfIdEstadoContrato" runat="server" />
    <asp:HiddenField ID="hfCodContratoAsociadoSel" runat="server" />
    <asp:HiddenField ID="hfIdConvenioContratoAsociado" runat="server" />
    <asp:HiddenField ID="hfValorInicialContConv" runat="server" />
    <asp:HiddenField ID="hfFechaFinalizacion" runat="server" />
    <asp:HiddenField ID="hfFechaInicioEjecucion" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
    <asp:HiddenField ID="hfIdCategoriaConvenio" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServApoyoGestion" runat="server" />
    <asp:HiddenField ID="hfIdTipoContConvPrestServProfesionales" runat="server" />
    <asp:HiddenField ID="hfIdTipoContAporte" runat="server" />
    <asp:HiddenField ID="hfIdMarcoInteradministrativo" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirecta" runat="server" />
    <asp:HiddenField ID="hfIdContratacionDirectaAporte" runat="server" />
    <asp:HiddenField ID="hfTotalProductos" runat="server" />
    <asp:HiddenField ID="hfTotalProductosCDP" runat="server" />
    <asp:HiddenField ID="hfAcordeonActivo" runat="server" />
    <asp:HiddenField ID="hfObjPlan" runat="server" />
    <asp:HiddenField ID="hfAlcPlan" runat="server" />
    <asp:HiddenField ID="hfRegional" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="hfIndice" runat="server" />
    <asp:HiddenField ID="hfIdCesion" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
                                                <asp:HiddenField ID="hfConsecutivosPlanCompras" runat="server" />
    <script type="text/javascript" language="javascript">

     

    </script>
</asp:Content>
