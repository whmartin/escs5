﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_Pruebas_Jorge_PruebaLupa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" runat="Server">
    <table style="width: 100%">
        <tr class="rowA">
            <td>
                <span>CU-47-CONT-SOLI</span>
                <asp:TextBox runat="server" ID="txtIdTerceroEntidad" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetTercero()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField ID="hfTerceroEntidad" runat="server" />
                <asp:HiddenField ID="hfIdTerceroEntidad" runat="server" />
                <script type="text/javascript" language="javascript">
                    function GetTercero() {
                        window_showModalDialog('../../../Page/Contratos/Lupas/LupaEmpleado.aspx', setReturnGetTercero, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnGetTercero(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {
                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_hfTerceroEntidad').value = retLupa[3];
                                document.getElementById('cphCont_txtIdTerceroEntidad').value = retLupa[1] + " - " + retLupa[2];
                                document.getElementById('cphCont_hfIdTerceroEntidad').value = retLupa[0];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-028-CONT-RELAC-LUGEJEC </span>
                <asp:TextBox runat="server" ID="TextBox1" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="LugarContrato()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <script type="text/javascript" language="javascript">
                    function LugarContrato() {
                        window_showModalDialog('../../../Page/Contratos/LugarEjecucionContrato/LupaConsultarLugarContrato.aspx', setReturnLugarContrato, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnLugarContrato(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_HiddenField1').value = retLupa[3];
                                document.getElementById('cphCont_TextBox1').value = retLupa[1] + " - " + retLupa[2];
                                document.getElementById('cphCont_HiddenField2').value = retLupa[0];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <apan>CU-031-CONT-  REG_INF_ CDP</apan>
                <asp:TextBox runat="server" ID="TextBox2" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image2" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="RegistroP()" ToolTip="Buscar" />
                <asp:ImageButton ID="ibtnGarantias" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    Enabled="True" OnClientClick="GetInfoGarantias(); return false;" Style="cursor: hand"
                    ToolTip="Ver Garantías" />
                <script type="text/javascript" language="javascript">
                    function GetInfoGarantias() {
                        window_showModalDialog('../../../Page/Contratos/Lupas/LupaRegInfoPresupuestal.aspx', setReturnGetInfoGarantias, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnGetInfoGarantias(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 1) {
                                document.getElementById('cphCont_TextBox2').value = retLupa[1];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-022-CONT-RELAC-CONTRATISTA</span>
                <asp:TextBox runat="server" ID="TextBox3" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image3" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="LProveedores()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function LProveedores() {
                        window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx', setResultLProveedores, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setResultLProveedores(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_HiddenField1').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <span>CU-021-CONT-APORTESCONTRATO-contratista</span>
                <asp:TextBox runat="server" ID="TextBox4" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image4" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="Aporte()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function Aporte() {
                        window_showModalDialog('../../../Page/Contratos/AporteContrato/Add.aspx?Aporte=Contratista', setResultAporte, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setResultAporte(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_HiddenField1').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-021-CONT-APORTESCONTRATO-ICBF</span>
                <asp:TextBox runat="server" ID="TextBox5" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image5" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="AporteICBF()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function AporteICBF() {
                        window_showModalDialog('../../../Page/Contratos/AporteContrato/Add.aspx?Aporte=ICBF', setResultAporteICBF, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setResultAporteICBF(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_TextBox5').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <span>CU-017-CONT-ASIG-INTER/SUP</span>
                <asp:TextBox runat="server" ID="TextBox6" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image6" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="SuperInter()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function SuperInter() {
                        window_showModalDialog('../../../Page/Contratos/ConsultarSuperInterContrato/List.aspx', setResultSuperInter, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setResultSuperInter(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_HiddenField1').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-51-CON-SOLMOD-PLANCOM</span>
                <asp:TextBox runat="server" ID="TextBox7" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image7" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="SolimodPlancomp()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function SolimodPlancomp() {
                        window_showModalDialog('../../../Page/Contratos/SolicitudModPlanCompras/Add.aspx', setReturnSolimodPlancomp, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnSolimodPlancomp(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_HiddenField1').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <span>CU-40-CONT-PLANCOMP-- Sin Finalizar</span>
                <asp:TextBox runat="server" ID="TextBox8" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image8" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="rePlanCompras()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function rePlanCompras() {
                        window_showModalDialog('../../../Page/Contratos/LupasFormaPagos/LupasRelacionarPlanCompras.aspx', serReturnrePlanCompras, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function serReturnrePlanCompras(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_TextBox8').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-019-CONT-ASIG-INTER/SUP-- DEsarrollando</span>
                <asp:TextBox runat="server" ID="TextBox9" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image9" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="RelaSuperInter()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function RelaSuperInter() {
                        window_showModalDialog('../../../Page/Contratos/SupervisorInterContrato/RelacionarSupervisorInterventor.aspx', serReturnRelaSuperInter, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function serReturnRelaSuperInter(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_TextBox9').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <span>CU-014-CONT-ASOC-AMP-GAR</span>
                <asp:TextBox runat="server" ID="TextBox10" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image10" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="AmparoGanra()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function AmparoGanra() {
                        window_showModalDialog('../../../Page/Contratos/amparosgarantias/add.aspx', serReturnAmparoGanra, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function serReturnAmparoGanra(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_TextBox8').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-43-CON- REG-CUENTA</span>
                <asp:TextBox runat="server" ID="TextBox11" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image11" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="FormaPago()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function FormaPago() {
                        window_showModalDialog('../../../Page/Contratos/LupaFormapago/List.aspx', setReturnFormaPago, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnFormaPago(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_TextBox11').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <span>CU-48-CON-GEST-GARAN</span>
                <asp:TextBox runat="server" ID="TextBox12" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image12" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GestionGarantia()" Style="cursor: hand" ToolTip="Buscar" />
                <script type="text/javascript" language="javascript">
                    function GestionGarantia() {
                        window_showModalDialog('../../../Page/Contratos/GestionarGarantia/Add.aspx', setReturnGestionGarantia, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnGestionGarantia(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_TextBox12').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
            <td>
                <span>CU-12-VIGENCIAS-FUTURAS</span>
                <asp:TextBox runat="server" ID="TextBox13" MaxLength="0" Enabled="False"></asp:TextBox>
                <asp:Image ID="Image13" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="VigenciasFuturas()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField ID="hfIdVigenciaFutura" runat="server" />
                <script type="text/javascript" language="javascript">
                    function VigenciasFuturas() {
                        window_showModalDialog('../../../Page/Contratos/VigenciasFuturas/Add.aspx', setReturnVigenciasFuturas, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    }
                    function setReturnVigenciasFuturas(dialog) {
                        var pObj = window_returnModalDialog(dialog);
                        if (pObj != undefined && pObj != null) {

                            var retLupa = pObj.split(",");
                            if (retLupa.length > 3) {
                                document.getElementById('cphCont_hfIdVigenciaFutura').value = retLupa[3];
                            }
                        }
                    }
                </script>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="hfPrueba" />
    <input type="text" id="iPrueba" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPrincipalButton" runat="Server">
</asp:Content>
