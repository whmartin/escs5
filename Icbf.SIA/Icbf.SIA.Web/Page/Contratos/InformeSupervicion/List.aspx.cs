﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Service;
using System.Data;

/// <summary>
/// Página de consulta a través de filtros para la entidad Contratos
/// </summary>
public partial class Page_Contratos_InformeSupervicion_InformeSupervicion : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/InformeSupervicion";
    ContratoService vContratoService = new ContratoService();
    SeguridadService vSeguridadFAC = new SeguridadService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (GetState(Page.Master, PageName)) { Buscar(); }
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            toolBar.LipiarMensajeError();
            CargarGrilla(gvContratos, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
           // toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            gvContratos.PageSize = PageSize();
            gvContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos(@"Informe de Supervici&#243;n del Contrato/Convenio", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvContratos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            DateTime? vFechaInicioDesde = null;
            DateTime? vFechaInicioHasta = null;
            string vNumContrato = null;
            int vVigenciaFiscalinicial;
            int vIDRegional;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            string vUsuarioCrea = null;
            string[] lRoles = Roles.GetRolesForUser(GetSessionUser().NombreUsuario);

            if (!vSeguridadFAC.ConsultarRolesNombreEsAdmin(lRoles[0], true))
                vUsuarioCrea = GetSessionUser().NombreUsuario;

            if (txtFechaRegistroSistemaDesde.Date.Year.ToString() != "1900")
                vFechaInicioDesde = Convert.ToDateTime(txtFechaRegistroSistemaDesde.Date);

            if (txtFechaRegistroSistemaHasta.Date.Year.ToString() != "1900")
                vFechaInicioHasta = Convert.ToDateTime(txtFechaRegistroSistemaHasta.Date);

            if (!string.IsNullOrEmpty(txtIdContrato.Text))
                vNumContrato = txtIdContrato.Text;

            vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);

            vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);

            if (ddlIDCategoriaContrato.SelectedValue != "-1")
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);

            if (ddlIDTipoContrato.SelectedValue != "-1")
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);

            //List<EstadoContrato> vEstadoContrato = new List<EstadoContrato>();

            //vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "SUS", null, true);
            //if (vEstadoContrato.Count == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //    return;
            //}

            var myGridResults = vContratoService.ConsultarContratosInformeObligaciones(vFechaInicioDesde, vFechaInicioHasta, vNumContrato, vVigenciaFiscalinicial, vIDRegional, vIDCategoriaContrato, vIDTipoContrato);

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Contrato), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Contrato, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
                else
                    gridViewsender.DataSource = myGridResults;
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Contratos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Contratos.Eliminado");
            CargarListaVigencia();
            CargarListaRegional();
            LlenarCategoriaContrato();
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
            }

        }

    }

    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            // Página 1 Descripción adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion

          
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }


    }

    protected void gvContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
       // SeleccionarRegistro(gvContrato.SelectedRow);
    }

}
