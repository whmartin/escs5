using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using System.Net;
using Icbf.SIA.Service;

public partial class Page_Cesiones_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/Cesiones";
    const string TIPO_ESTRUCTURA = "Cesion"; 

    ContratoService vContratoService = new ContratoService();
    private ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
                CargarContratistasActuales();
                CargarContratistasCesion();
                CargarValoresCesiones();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        bool Nuevo = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                Nuevo = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                Nuevo = true;
            }
        }

        if (Nuevo)
        {
            SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetalleConsModContractual.Value.ToString());
            NavigateTo(SolutionPage.Add);
        }
        else
        {
            toolBar.MostrarMensajeError("No se puede Registrar, Por favor validar el estado!");
        }
        //NavigateTo(SolutionPage.Add);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {

        #region  Borrador 
        //bool Editar = false;

        //if (hfIDCosModContractual.Value != "")
        //{
        //    if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
        //    {
        //        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
        //        Editar = true;
        //    }
        //}
        //else
        //{
        //    //if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
        //    //{
        //    //    SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
        //    //    Editar = true;
        //    //}
        //    Editar = false;
        //}

        //if (Editar)
        //{
        //    SetSessionParameter("Cesiones.IdCesion", hfIdCesion.Value);
        //    NavigateTo(SolutionPage.Edit);
        //}
        //else
        //{
        //    toolBar.MostrarMensajeError("No se puede Editar, Por favor validar el estado!");
        //}
        ////SetSessionParameter("Cesiones.IdCesion", hfIdCesion.Value);
        ////NavigateTo(SolutionPage.Edit);
        #endregion

        SetSessionParameter("Cesiones.IdCesion", hfIdCesion.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());

        if (hfIDCosModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Edit);
    }
    
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        bool Eliminar = false;

        if (hfIDCosModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
                Eliminar = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIDCosModContractualEstado.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
                Eliminar = true;
            }
        }

        if (!Eliminar)
        {
            toolBar.MostrarMensajeError("No se puede Eliminar, Por favor validar el estado!");
        }
        //EliminarRegistro();
    }
    
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
            
            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }
    
    private void CargarDatos()
    {
        try
        {
            int vIdConsModContractual;

            if (!string.IsNullOrEmpty( GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConsModContractual);

                if (detalleSolicitud.Estado == "Registro" || detalleSolicitud.Estado == "Devuelta")
                    toolBar.MostrarBotonEditar(true);
                else
                    toolBar.MostrarBotonEditar(false);

                if (detalleSolicitud.Estado == "Suscrita")
                    lblTipoCesion.Text = "Contratista Cedente";
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");

                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                {
                    hfEsSubscripcion.Value = "1";
                    RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                {
                    hfEsSubscripcion.Value = "2";
                    RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                }

                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();
                toolBar.MostrarBotonEditar(false);

                var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConsModContractual);
                if (detalleSolicitud.Estado == "Suscrita")
                    lblTipoCesion.Text = "Contratista Cedente";

                var detalleSolicitud1 = vContratoService.ConsultarSolitud(vIdConsModContractual);

                if (detalleSolicitud1.Estado == "Suscrita")
                    lblTipoCesion.Text = "Contratista Cedente";
            }

            int vIdCesion = Convert.ToInt32(GetSessionParameter("Cesiones.IdCesion"));
            RemoveSessionParameter("Cesiones.IdCesion");
            hfIdCesion.Value = vIdCesion.ToString();

            var cesion = vContratoService.ConsultarCesiones(vIdCesion);

           // txtvalorCesion.Text = string.Format("{0:$#,##0}",cesion.ValorCesion);
            //txtvalorEjecutado.Text = string.Format("{0:$#,##0}",cesion.ValorEjecutado);

            if (GetSessionParameter("Cesiones.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Cesiones.Guardado");

            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            hfIdContrato.Value = vIdContrato.ToString();
            RemoveSessionParameter("Contrato.IdContrato");  

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(vIdContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(vContrato.FechaFinalizacionIniciaContrato);

            IdContrato = vContrato.IdContrato.ToString();
            txtContrato.Text = vContrato.NumeroContrato.ToString();
            txtRegional.Text = vContrato.NombreRegional;
            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();
            txtobjetoContrato.Text = vContrato.ObjetoContrato;
            txtalcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            txtvalorini.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            txtJustificacion.Text = vConsModContractual.Justificacion;
            hfIDCosModContractualEstado.Value = vConsModContractual.IdConsModContractualesEstado.ToString();

            Cesiones vCesiones = new Cesiones();
            vCesiones = vContratoService.ConsultarCesiones(vIdCesion);
            hfIdCesion.Value = vCesiones.IdCesion.ToString();
            txtFechaCesion.Text = vCesiones.FechaCesion.ToShortDateString();

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void EliminarRegistro()
    {
        try
        {
            int vIdCesion = Convert.ToInt32(hfIdCesion.Value);

            Cesiones vCesiones = new Cesiones();
            vCesiones = vContratoService.ConsultarCesiones(vIdCesion);
            InformacionAudioria(vCesiones, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarCesiones(vCesiones);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Cesiones.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
  
    private void Iniciar()
    {
        try
        {


            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.EstablecerTitulos("Cesiones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
   
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            //ddlIDDetalleConsModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            //ddlIDDetalleConsModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }

    private string nContratistas
    { set { hfContratistas.Value = value; } }

    private void CargarContratistasActuales()
    {
        int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
        List<Cesiones> contratistas = vContratoService.ConsultarContratisasActualesCesiones(vIdContrato, null);
        gvContratistasCargar.DataSource = contratistas;
        gvContratistasCargar.DataBind();
        nContratistas = gvContratistasCargar.Rows.Count.ToString();
    }

    private void CargarContratistasCesion()
    {
        if (!string.IsNullOrEmpty(hfIdCesion.Value))
        {
            int vIdCesion = Convert.ToInt32(hfIdCesion.Value);
            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            List<Cesiones> contratistas = vContratoService.ConsultarContratisasCesion(vIdContrato, vIdCesion, null);
            gvContratistas.DataSource = contratistas;
            gvContratistas.DataBind();
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(IdContrato), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    private void CargarValoresCesiones()
    {
        try
        {
            int vIdContrato = 0;

            if (!int.TryParse(hfIdContrato.Value, out vIdContrato))
            {
                toolBar.MostrarMensajeError("La operación no se pudo realizar, consulte con soporte técnico");
                return;
            }

            var cesiones = vContratoService.ConsultarInfoCesiones(vIdContrato);

            var myGridResults = cesiones;
            gvCesiones.DataSource = myGridResults;
            gvCesiones.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
