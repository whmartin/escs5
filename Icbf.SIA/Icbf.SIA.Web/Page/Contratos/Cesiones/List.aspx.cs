using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Cesiones_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/Cesiones";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            String vFechaCesion = null;
            String vJustificacion = null;
            int? vEstado = null;
            int? vIDDetalleConsModContractual = null;
            if (txtFechaCesion.Text!= "")
            {
                vFechaCesion = Convert.ToString(txtFechaCesion.Text);
            }
            if (txtJustificacion.Text!= "")
            {
                vJustificacion = Convert.ToString(txtJustificacion.Text);
            }
            if (txtEstado.Text!= "")
            {
                vEstado = Convert.ToInt32(txtEstado.Text);
            }
            if (ddlIDDetalleConsModContractual.SelectedValue!= "-1")
            {
                vIDDetalleConsModContractual = Convert.ToInt32(ddlIDDetalleConsModContractual.SelectedValue);
            }
            gvCesiones.DataSource = vContratoService.ConsultarCesioness( vFechaCesion, vJustificacion, vEstado, vIDDetalleConsModContractual);
            gvCesiones.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvCesiones.PageSize = PageSize();
            gvCesiones.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Cesiones", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvCesiones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Cesiones.IdCesion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCesiones_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvCesiones.SelectedRow);
    }
    protected void gvCesiones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCesiones.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Cesiones.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Cesiones.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIDDetalleConsModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDDetalleConsModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
