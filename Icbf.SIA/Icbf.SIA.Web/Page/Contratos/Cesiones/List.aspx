<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Cesiones_List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha de la Cesión *
            </td>
            <td>
                Justificación de la Cesión *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaCesion"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacion"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>
                Detalle adicion *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEstado"  ></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIDDetalleConsModContractual"  ></asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvCesiones" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdCesion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvCesiones_PageIndexChanging" OnSelectedIndexChanged="gvCesiones_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha de la Cesión" DataField="FechaCesion" />
                            <asp:BoundField HeaderText="Justificación de la Cesión" DataField="Justificacion" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />
                            <asp:BoundField HeaderText="Detalle adicion" DataField="IDDetalleConsModContractual" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
