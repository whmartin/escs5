using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;
using Icbf.SIA.Service;

public partial class Page_Cesiones_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    private SIAService vSiaService = new SIAService();    

    string PageName = "Contrato/Cesiones";
    const string TIPO_ESTRUCTURA = "Cesion"; 
    int vIdContrato;
    decimal vIdIndice = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();
                CargarContratistasActuales();
                CargarContratistasCesion();
            }
            else
            {
                string sControlName = Request.Params.Get("__EVENTTARGET");

                switch (sControlName)
                {                   
                    case "cphCont_txtContratista":
                        txtContratistaTextChanged(sender, e);
                        toolBar.LipiarMensajeError();
                        break;                   
                    default: break;
                }
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIDDetCosModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if (hfIDCosModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual",hfIDCosModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIDCosModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
        }
        //NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            toolBar.LipiarMensajeError();

            if (string.IsNullOrEmpty(txtvalorEjecutado.Text))
            {
                toolBar.MostrarMensajeError("Debe ingresar,el valor de ejecutado hasta el momento de la cesi�n.");
                return;
            }

            decimal valorEjecutado;
            //decimal valorCesion;

            if ( !decimal.TryParse(txtvalorEjecutado.Text.Replace("$",""), out valorEjecutado))
            {
                toolBar.MostrarMensajeError("El valor tiene un formato incorrecto");
                return;
            }

            if(gvContratistas.Rows.Count == 0 && gvIntegrantesConsorcio_UnionTemp.Rows.Count == 0)
            {
                toolBar.MostrarMensajeError("Debe ingresar el contratista cesionario para poder Guardar");
                return;
            }

            #region Guardar
                int vResultado;
                Cesiones vCesiones = new Cesiones();
                if (Request.QueryString["oP"] == "E" || ! string.IsNullOrEmpty(hfIdCesion.Value))
                {
                    vCesiones.IdCesion = int.Parse(hfIdCesion.Value);
                    vCesiones.FechaCesion = DateTime.Now;
                    vCesiones.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vCesiones.ValorEjecutado = decimal.Parse(txtvalorEjecutado.Text.Replace("$", string.Empty));
                    if (decimal.Parse(hfValorCesion.Value) > 0)
                    {
                        vCesiones.ValorCesion = decimal.Parse(hfValorCesion.Value)  - vCesiones.ValorEjecutado;
                    }
                    else
                    {
                        vCesiones.ValorCesion = decimal.Parse(txtvalorfinal.Text.Replace("$", string.Empty)) - vCesiones.ValorEjecutado;
                    }
                
                InformacionAudioria(vCesiones, this.PageName, vSolutionPage);
                    vResultado = vContratoService.ModificarCesiones(vCesiones);
                }
                else
                {
                    vCesiones.Justificacion = Convert.ToString(txtJustificacion.Text);
                    vCesiones.FechaCesion = DateTime.Now;
                    vCesiones.IDDetalleConsModContractual = Convert.ToInt32(hfIDDetCosModContractual.Value);
                    vCesiones.ValorEjecutado = decimal.Parse(txtvalorEjecutado.Text.Replace("$", string.Empty));
                    if (decimal.Parse(hfValorCesion.Value) > 0)
                    {
                        vCesiones.ValorCesion = decimal.Parse(hfValorCesion.Value) - vCesiones.ValorEjecutado;
                    }
                    else
                    {
                        vCesiones.ValorCesion = decimal.Parse(txtvalorfinal.Text.Replace("$", string.Empty)) - vCesiones.ValorEjecutado;
                    }
                    vCesiones.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vCesiones, this.PageName, vSolutionPage);
                    vResultado = vContratoService.InsertarCesiones(vCesiones);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operaci�n no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    //SetSessionParameter("Cesiones.IdCesion", vCesiones.IdCesion);
                    if (hfIDCosModContractual.Value != "")
                    {
                        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value.ToString());
                    }
                    else
                    {
                        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                    }

                    SetSessionParameter("Cesiones.IdCesion",vCesiones.IdCesion);
                    SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
                    SetSessionParameter("ConsModContractual.IDCosModContractual", hfIDCosModContractual.Value);
                    SetSessionParameter("Cesiones.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operaci�n no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
            catch (UserInterfaceException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            #endregion
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.MostrarBotonNuevo(false);
            toolBar.OcultarBotonGuardar(true);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Cesiones", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            hfIdContrato.Value = vIdContrato.ToString();
            //TODO: Limpiar al gurdar y el nuevo consultar
            RemoveSessionParameter("Contrato.IdContrato");  

            int vIdConsModContractual;

            if (!string.IsNullOrEmpty( GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIDCosModContractual.Value = vIdConsModContractual.ToString();
            }
            else
            {
                vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConsModContractual.ToString();
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoModificacion(vIdContrato);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(vContrato.FechaInicioEjecucion);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(vContrato.FechaFinalizacionIniciaContrato);

            IdContrato = vContrato.IdContrato.ToString();
            txtContrato.Text = vContrato.NumeroContrato.ToString();
            txtRegional.Text = vContrato.NombreRegional;
            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();
            txtobjetoContrato.Text = vContrato.ObjetoContrato;
            txtalcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            txtvalorini.Text = string.Format("{0:$#,##0}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", vContrato.ValorFinalContrato);

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConsModContractual);

            decimal valoreCesion = 0;

            var cesioness = vContratoService.ConsultarInfoCesionesSuscritas(vIdContrato).OrderByDescending(x => x.IDCosModContractual).FirstOrDefault();

            if (cesioness != null)
                hfValorCesion.Value = (valoreCesion + cesioness.ValorCesion).ToString();
            else
                hfValorCesion.Value = valoreCesion.ToString();

            txtJustificacion.Text = vConsModContractual.Justificacion;

            Cesiones vCesiones = new Cesiones();

            if (Request.QueryString["oP"] == "E")
            {
                int vIdCesion = Convert.ToInt32(GetSessionParameter("Cesiones.IdCesion"));
                RemoveSessionParameter("Cesiones.IdCesion");
                
                vCesiones = vContratoService.ConsultarCesiones(vIdCesion);
                txtvalorCesion.Text = string.Format("{0:$#,##0}", vCesiones.ValorCesion);
                txtvalorEjecutado.Text = string.Format("{0:$#,##0}", vCesiones.ValorEjecutado);
                hfIdCesion.Value = vCesiones.IdCesion.ToString();
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCesiones.UsuarioCrea, vCesiones.FechaCrea, vCesiones.UsuarioModifica, vCesiones.FechaModifica);
            }
            else
            {
                int detcons = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                hfIDDetCosModContractual.Value = detcons.ToString();
            }


            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            gvContratistas.EmptyDataText = EmptyDataText();
            gvContratistasCargar.EmptyDataText = EmptyDataText();
            gvIntegrantesConsorcio_UnionTemp.EmptyDataText = EmptyDataText();
            gvIntegrantesConsorcio_UnionTempCargar.EmptyDataText = EmptyDataText();
            gvContratistas.PageSize = PageSize();
            gvContratistasCargar.PageSize = PageSize();
            gvIntegrantesConsorcio_UnionTemp.PageSize = PageSize();
            gvIntegrantesConsorcio_UnionTempCargar.PageSize = PageSize();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }

    private string nContratistas
    { set { hfContratistas.Value = value; } }

    protected void txtContratistaTextChanged(object sender, EventArgs e)
    {
       if( ! string.IsNullOrEmpty(GetSessionParameter("Cesiones.idCesion").ToString()))
       {
           hfIdCesion.Value = GetSessionParameter("Cesiones.idCesion").ToString();
           RemoveSessionParameter("Cesiones.idCesion");
       }

       if (! string.IsNullOrEmpty(txtvalorEjecutado.Text))
       {
           var valorContrato = decimal.Parse(txtvalorfinal.Text.Replace("$", string.Empty));
           var valorEjecutado = decimal.Parse(txtvalorEjecutado.Text.Replace("$", string.Empty));
           var valorCesion = valorContrato - valorEjecutado;
           txtvalorCesion.Text = string.Format("{0:$#,##0}", valorCesion);
       }

        CargarContratistasActuales();
        CargarContratistasCesion();
    }

    protected void btnOpcionGvContratistasClickCargar(object sender, EventArgs e)
    {
        //switch (((LinkButton)sender).CommandName)
        //{
        //    case "Eliminar":
        //        EliminarContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
        //        break;
        //    case "Detalle":
        //        int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);

        //        string idProveedor = gvContratistasCargar.DataKeys[rowIndex]["IdTercero"].ToString();
        //        SetSessionParameter("Contrato.IdProveedor", idProveedor);

        //        string idProveedoresContratos = gvContratistasCargar.DataKeys[rowIndex]["IdProveedoresContratos"].ToString();
        //        SetSessionParameter("FormaPago.IDProveedoresContratos", idProveedoresContratos);

        //        //SetSessionParameter("Contrato.ContratosAPP", IdContrato);

        //        ScriptManager.RegisterStartupScript(this, GetType(), "formaPago", "GetFormaPago()", true);
        //        break;
        //    default:
        //        break;
        //}
    }

    protected void btnOpcionGvContratistasClick(object sender, EventArgs e)
    {
        //switch (((LinkButton)sender).CommandName)
        //{
        //    case "Eliminar":
        //        EliminarContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
        //        break;
        //    case "Detalle":
        //        int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);

        //        string idProveedor = gvContratistas.DataKeys[rowIndex]["IdTercero"].ToString();
        //        SetSessionParameter("Contrato.IdProveedor", idProveedor);

        //        string idProveedoresContratos = gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"].ToString();
        //        SetSessionParameter("FormaPago.IDProveedoresContratos", idProveedoresContratos);

        //        //SetSessionParameter("Contrato.ContratosAPP", IdContrato);

        //        ScriptManager.RegisterStartupScript(this, GetType(), "formaPago", "GetFormaPago()", true);
        //        break;
        //    default:
        //        break;
        //}
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            vIdIndice = Convert.ToInt64(strIDCosModContractual);
            hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion

    #region Gesti�n Contratistas 

    protected void gvContratistasCargar_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int idProveedorPadre = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdProveedoresContratos").ToString());
            int idContrato = Convert.ToInt32(hfIdContrato.Value);
            int idCesion = 0;
            int idDetConsModContractual = 0;

            if (!string.IsNullOrEmpty(hfIdCesion.Value))
	            idCesion = int.Parse(hfIdCesion.Value);

            if (!string.IsNullOrEmpty(hfIDDetCosModContractual.Value))
                idDetConsModContractual = int.Parse(hfIDDetCosModContractual.Value);

            var items = vContratoService.ConsultarExisteCesionario(idContrato, idCesion, idProveedorPadre);

            string isValid = "1";
            if (items == null || items.Count() == 0)
                isValid = "0";

            string script = string.Format("GetContratistasNew('{0}','{1}','{2}','{3}'); return false;", idProveedorPadre, idCesion, isValid, idDetConsModContractual);
            button.Attributes.Add("onclick", script);
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        var index =  Convert.ToInt32(((LinkButton)sender).CommandArgument);
        Proveedores_Contratos vProveedor_Contrato = new Proveedores_Contratos();
        vProveedor_Contrato.IdProveedoresContratos = Convert.ToInt32(gvContratistas.DataKeys[index]["IdProveedoresContratos"]);
        vProveedor_Contrato.IdContrato = Convert.ToInt32(IdContrato);
        vContratoService.EliminarProveedores_Contratos(vProveedor_Contrato);
        CargarContratistasActuales();
        CargarContratistasCesion();
    }

    private void CargarContratistasActuales()
    {
        int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

        List<Cesiones> contratistas = vContratoService.ConsultarContratisasActualesCesiones(vIdContrato, null);
        gvContratistasCargar.DataSource = contratistas;
        gvContratistasCargar.DataBind();
        nContratistas = gvContratistasCargar.Rows.Count.ToString();

        List<Cesiones> integrantesConsorcioUnionTemp = vContratoService.ConsultarContratisasActualesCesiones(vIdContrato, 1);
        gvIntegrantesConsorcio_UnionTempCargar.DataSource = integrantesConsorcioUnionTemp;
        gvIntegrantesConsorcio_UnionTempCargar.DataBind();
    }

    private void CargarContratistasCesion()
    {
        if (!string.IsNullOrEmpty(hfIdCesion.Value))
        {
            int vIdCesion = Convert.ToInt32(hfIdCesion.Value);
            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            List<Cesiones> contratistas = vContratoService.ConsultarContratisasCesion(vIdContrato,vIdCesion, null);
            gvContratistas.DataSource = contratistas;
            gvContratistas.DataBind();
            nContratistas = gvContratistas.Rows.Count.ToString();

            List<Cesiones> integrantesConsorcioUnionTemp = vContratoService.ConsultarCesionessContrato(vIdContrato, vIdCesion, 1);
            gvIntegrantesConsorcio_UnionTemp.DataSource = integrantesConsorcioUnionTemp;
            gvIntegrantesConsorcio_UnionTemp.DataBind();
        }
    }

    #endregion
}
