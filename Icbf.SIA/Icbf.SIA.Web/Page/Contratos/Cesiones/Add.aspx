<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Cesiones_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

<style type="text/css">
        table .grillaCentral tr.rowAG td, table .grillaCentral tr.rowBG td
        {
            text-align: center;
            padding: 5px;
            padding-right: 0px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        /*****************************************************************************
        Código para colocar los indicadores de miles  y decimales mientras se escribe
        Script creado por Tunait!
        Si quieres usar este script en tu sitio eres libre de hacerlo con la condición de que permanezcan intactas estas líneas, osea, los créditos.

        http://javascript.tunait.com
        tunait@yahoo.com  27/Julio/03
        ******************************************************************************/
        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

        function ValidarCesion(n, currency) {

            var valorContrato = document.getElementById('<%= txtvalorfinal.ClientID %>').value;
            var valorCesiones = document.getElementById('<%= hfValorCesion.ClientID %>').value;



            valorContrato = valorContrato.split('.').join('');
            valorContrato = valorContrato.split('$').join('');
            valorCesiones = valorCesiones.split(',').join('.');
            valorCesiones = parseFloat(valorCesiones);
            var nsinFormato = n.split('.').join('');
            nsinFormato = nsinFormato.split('$').join('');

            var valorEjecutado = parseFloat(nsinFormato);


            if (valorCesiones > 0) {
                if (valorEjecutado < valorCesiones) {
                    var valorCesion = valorCesiones - valorEjecutado;

                    var valueWithFormat = currency + " " + valorEjecutado.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                    var valuecesionWithFormat = currency + " " + valorCesion.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                    valueWithFormat = valueWithFormat.split('.').join('*');
                    valueWithFormat = valueWithFormat.split(',').join('.');
                    valueWithFormat = valueWithFormat.split('*').join(',');

                    valuecesionWithFormat = valuecesionWithFormat.split('.').join('*');
                    valuecesionWithFormat = valuecesionWithFormat.split(',').join('.');
                    valuecesionWithFormat = valuecesionWithFormat.split('*').join(',');

                    document.getElementById('<%= txtvalorEjecutado.ClientID %>').value = valueWithFormat;
                    document.getElementById('<%= txtvalorCesion.ClientID %>').value = valuecesionWithFormat;
                }
                else {
                    document.getElementById('<%= txtvalorEjecutado.ClientID %>').value = '';
                    document.getElementById('<%= txtvalorCesion.ClientID %>').value = '';
                    alert('El valor ejecutado debe ser menor al valor final del contrato o a la última cesión Registrada.');
                }
            }
            else {
                if (valorEjecutado < valorContrato) {
                    var valorCesion = valorContrato - valorEjecutado;

                    var valueWithFormat = currency + " " + valorEjecutado.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                    var valuecesionWithFormat = currency + " " + valorCesion.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                    valueWithFormat = valueWithFormat.split('.').join('*');
                    valueWithFormat = valueWithFormat.split(',').join('.');
                    valueWithFormat = valueWithFormat.split('*').join(',');

                    valuecesionWithFormat = valuecesionWithFormat.split('.').join('*');
                    valuecesionWithFormat = valuecesionWithFormat.split(',').join('.');
                    valuecesionWithFormat = valuecesionWithFormat.split('*').join(',');

                    document.getElementById('<%= txtvalorEjecutado.ClientID %>').value = valueWithFormat;
                    document.getElementById('<%= txtvalorCesion.ClientID %>').value = valuecesionWithFormat;
                }
                else {
                    document.getElementById('<%= txtvalorEjecutado.ClientID %>').value = '';
                    document.getElementById('<%= txtvalorCesion.ClientID %>').value = '';
                    alert('El valor ejecutado debe ser menor al valor final del contrato.');
                }
            }
        }

        function CheckDecimal(inputtxt) 
        {
            alert(inputtxt);
            var decimal = /^[-+][0-9]+\.[0-9]+[eE][-+]?[0-9]+$/;
            if(inputtxt.match(decimal)) 
            { 
                return true;
            }
            else
            { 
                return false;
            }
        }

         function GetInfoCesiones() { 
             var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>');  
             var url = '../../../Page/Contratos/Lupas/LupaInfoCesiones.aspx?vIdContrato=' + vIdContrato.value;
             window_showModalDialog(url, '', 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
    }


    </script>

<asp:HiddenField ID="hfIdCesion" runat="server" />
<asp:HiddenField ID="hfIDCosModContractual" runat="server" />
<asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
<asp:HiddenField ID="hfIndice" runat="server" />
<asp:HiddenField ID="hfIdContrato" runat="server" />
<asp:HiddenField ID="hfPostbck" runat="server" />
<asp:HiddenField ID="hfIDDetCosModContractual" runat="server" />
<asp:HiddenField ID="hfValorCesion" runat="server" />

    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td style="width: 50%">
                Regional
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRegional" Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px"></asp:TextBox>
               
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjetoContrato"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcanceContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorini"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Valor Ejecutado&nbsp;
                <asp:RequiredFieldValidator runat="server" ID="RfvValorEjecutado" ControlToValidate="txtvalorEjecutado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnBuscar"
                 ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
            </td>

            <td>
                Valor Cesión</td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorEjecutado"   
                     MaxLength="128" Width="320px" Height="22px" onchange="ValidarCesion(this.value,'$');" ></asp:TextBox>
                                        <Ajax:FilteredTextBoxExtender ID="ftValorPlanCompras" runat="server" TargetControlID="txtvalorEjecutado"
                        FilterType="Custom,Numbers" ValidChars="$.,"  /> 
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorCesion" ReadOnly="true"   
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">            
            <td>
                Justificaci&oacute;n de la novedad contractual&nbsp; 
               <%-- <asp:RequiredFieldValidator runat="server" ID="rfvJustificacion" ControlToValidate="txtJustificacion" 
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr class="rowA">            
            <td>
                <asp:TextBox runat="server" ID="txtJustificacion" Enabled="False" 
                   MaxLength="200" Height="68px"  Width="95%" TextMode="MultiLine"    ></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="ftJustificacion" runat="server" TargetControlID="txtJustificacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />--%>
            </td>
              <td>
                  <strong>
                <asp:Label runat="server" ID="lblCont" Text="Cesiones del Contrato"></asp:Label>
                  </strong>
                <asp:ImageButton ID="ibtnCesiones" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    Enabled="True" OnClientClick="GetInfoCesiones(); return false;" Style="cursor: hand"
                    ToolTip="Ver Cesiones" />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Supervisores</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>
    </table>
    <table width="100%" align="center">
        <tr>
            <td>
                <Ajax:Accordion ID="Accordion1" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                    ContentCssClass="accordionContent"  runat="server"
                    Width="100%" Height="100%">
                    <Panes>                        
                        <Ajax:AccordionPane ID="AccordionPane1" runat="server">
                            <Header>
                                Contratista Cedente</Header>
                            <Content>
                                <table width="90%" align="center">                                    
                                    <tr class="rowB">
                                        <td>
                                            Contratistas relacionados
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvContratistasCargar" runat="server" OnRowDataBound="gvContratistasCargar_RowDataBound" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEditar"  runat="server" AutoPostBack="false"  >
                                                                <img alt="Editar" src="../../../Image/btn/edit.gif"   title="Editar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
<%--                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClickCargar"
                                                                CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Medio Pago" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Integrantes relacionados al consorcio o unión temporal
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvIntegrantesConsorcio_UnionTempCargar" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Constratista" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacionIntegrante" />
                                                    <asp:BoundField HeaderText="Información Integrante" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Porcentaje Participación" DataField="PorcentajeParticipacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>                        
                    </Panes>
                </Ajax:Accordion>
            </td>
        </tr>
    </table>
    <table width="100%" align="center">
        <tr>
            <td>
                <Ajax:Accordion ID="AccContratos" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                    ContentCssClass="accordionContent"  runat="server"
                    Width="100%" Height="100%">
                    <Panes>                        
                        <Ajax:AccordionPane ID="ApContratista" runat="server">
                            <Header>
                                Contratista Cesionario</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <%--<asp:CustomValidator Visible="false" ID="cvContratistas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaContratistas"></asp:CustomValidator>--%>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfContratistas" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtContratista" runat="server" Enabled="false" Visible="false" ViewStateMode="Enabled"
                                                OnTextChanged="txtContratistaTextChanged" AutoPostBack="true"></asp:TextBox>
                                            <%--<asp:ImageButton ID="imgContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetContratista(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />--%>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Contratistas relacionados
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="Eliminar" OnClick="btnEliminar_Click" >
                                                            <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Integrantes relacionados al consorcio o unión temporal
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvIntegrantesConsorcio_UnionTemp" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Constratista" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacionIntegrante" />
                                                    <asp:BoundField HeaderText="Información Integrante" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Porcentaje Participación" DataField="PorcentajeParticipacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>                        
                    </Panes>
                </Ajax:Accordion>
            </td>
        </tr>
    </table>

        <script type="text/javascript" language="javascript">

            function GetContratistasNew(idContratista, idCesion, puedeEditar, idDetConsmodcontractual) {

                if (puedeEditar == '0') {
                    muestraImagenLoading();
                    var contrato = document.getElementById('<%= hfIdContrato.ClientID %>');
                    window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx?idContrato=' + contrato.value +
                                                                                                            '&idContratista=' + idContratista +
                                                                                                            '&idCesion=' + idCesion +
                                                                                                            '&idDetConsmodcontractual=' + idDetConsmodcontractual, setReturnGetContratistasNew, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                }
                else {
                    alert('No puede editar el contratista, porque ya existe una edición anterior.')
                }
            }

            function setReturnGetContratistasNew() {
                prePostbck(false);
                __doPostBack('<%= txtContratista.ClientID %>', '');
            }
            

            function GetContratista() {
                muestraImagenLoading();
                //CU-022-CONT-RELAC-CONTRATISTA
                window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx', setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetContratista() {
                prePostbck(false);
                __doPostBack('<%= txtContratista.ClientID %>', '');
            }


            function limitText(limitField, limitNum) {
                if (limitField.value.length > limitNum) {
                    limitField.value = limitField.value.substring(0, limitNum);
                }
            }
            function helpOver(idImage) {
                document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }        

        function mensajeAlerta(control, texto) {
            alert(texto);
            var creference = document.getElementById(control);
            creference.focus();
        }

        function ValidaEliminacion() {
            return confirm('Esta seguro de que desea eliminar el registro?');
        }

        function PreGuardado() {
            ConfiguraValidadores(false);
            muestraImagenLoading();
        }

        function Aprobacion() {
            ConfiguraValidadores(true);
            window.Page_ClientValidate("btnAprobar");
            if (!window.Page_IsValid) {
                alert('Debe diligenciar los campos obligatorios');
                return false;
            } else {
                return confirm('Está seguro que desea Finalizar el registro del Contrato/Convenio?');
            }
        }        

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }        

        //if (imagenLoading == true)
        //    muestraImagenLoading();
        //}

        function EjecutarJSFechaInicialSupervisores(control, fechaOriginal) {
            $("#lblError")[0].innerHTML = '';
            $("#lblError")[0].className = '';
            var fechaInicioEjecucion = document.getElementById('cphCont_caFechaInicioEjecucion_txtFecha').value;
            var fechaInicioSupervisor = document.getElementById(control.id).value;

            if (fechaInicioEjecucion != '') {
                var splitfechainicio = fechaInicioEjecucion.split('/');
                var jfechaInicioEjecucion = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                splitfechainicio = fechaInicioSupervisor.split('/');
                var jfechaInicioSupervisor = new Date(splitfechainicio[2], parseInt(splitfechainicio[1]), splitfechainicio[0]);

                if (jfechaInicioSupervisor < jfechaInicioEjecucion) {
                    document.getElementById(control.id).value = fechaOriginal;
                    alert('La Fecha de Inicio de Supervisor y/o Interventor debe ser mayor o igual a la Fecha de Ejecución de Contrato/Convenio');
                }
            }
        }

            function prePostbck(imagenLoading) {
                if (!window.Page_IsValid) {
                    __doPostBack('<%= hfPostbck.ClientID %>', '');
                }

                if (imagenLoading == true)
                    muestraImagenLoading();
            }

            
    </script>
    <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td><asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
