<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_Add" %>
<%@ Register Src="../../../General/General/Control/fechaJS1.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript">


        function ConfirmarSuscripcion() {

            if (!confirm('Esta seguro de que desea suscribir el contrato?')) {
                return false;
            }
            else
                return true;
        }

    </script>

    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Nombre del Usuario
            </td>
            <td>
                Cargo del Usuario
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreUsuario" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCargoUsuario"  ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Regional del Usuario
            </td>
            <td>
                Fecha de Suscripci&oacute;n*
                <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFechaSuscripcion"
                 ErrorMessage="Campo Requerido" Enabled="True" ValidationGroup="btnGuardar" 
                 Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                
                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaSuscripcion" 
                ErrorMessage="Fecha inválida." ForeColor="Red"
    Operator="DataTypeCheck" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator> </td>

        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtRegional" ></asp:TextBox>
            </td>
            <td>
                <%--<uc1:fecha ID="txtFechaSuscripcion" runat="server" Width="80%" Enabled="True"  
                        Requerid="True"  />--%>
                        
                        
                              <asp:TextBox ID="txtFechaSuscripcion" runat="server" ></asp:TextBox>
                <asp:Image ID="imgCalendario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />&nbsp;
                <Ajax:MaskedEditExtender ID="meetxtFechaSuscripcion" runat="server" CultureAMPMPlaceholder="AM;PM"
    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaSuscripcion">
</Ajax:MaskedEditExtender>
<Ajax:CalendarExtender ID="cetxtFechaSuscripcion" runat="server" Format="dd/MM/yyyy" 
        PopupButtonID="imgCalendario" TargetControlID="txtFechaSuscripcion" CssClass="calendario"></Ajax:CalendarExtender>
                        

            </td>
        </tr>
        <tr class="rowB">
            <td>N&uacute;mero del Contrato/Convenio *</td>
            <td>Vinculo del SECOP </td>
        </tr>
        <tr class="rowA">
            <td><asp:TextBox runat="server" ID="txtNumeroContrato" ></asp:TextBox>&nbsp;
                
                <asp:RequiredFieldValidator ID="rfvNumeroContrato" runat="server" ControlToValidate="txtNumeroContrato"
                 ErrorMessage="Campo Requerido" Enabled="True" ValidationGroup="btnGuardar" 
                 Display="Dynamic" ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>
                
            </td>
            <td>  
                <asp:TextBox runat="server" ID="txtVinculoSecop" Width="500px"  ></asp:TextBox>                               
            </td>
        </tr>
        <%--<tr class="rowB">
            <td>
                Fecha Registro al Sistema
            </td>
            <td>
                Id Contrato/Convenio
            </td>
        </tr>--%>
        <%--<tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistroSistema" MaxLength="10" ></asp:TextBox>
                <img id="imghFechaRegistroSistema" alt="help" title="Es la Fecha de Registro al Sistema." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghFechaRegistroSistema')" onmouseout="helpOut('imghFechaRegistroSistema')"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
                <img id="imghIdContrato" alt="help" title="Es el Id del contrato." class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghIdContrato')" onmouseout="helpOut('imghIdContrato')"/>
            </td>
        </tr>--%>
        
        <tr class="rowB">
            <td>
                Anexar Documentos
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="false"
                    GridLines="None" Width="100%" DataKeyNames="IdArchivoContrato"
                    CellPadding="0" Height="16px" OnSelectedIndexChanged="gvDocumentos_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Documento Anexo">
                            <ItemTemplate>
                                <asp:Label ID="LbArchivo" runat="server" Text='<%#Eval("NombreArchivoOri") %>'></asp:Label>
                                <asp:Label ID="LbArchivoOri" runat="server" Text='<%#Eval("NombreArchivo") %>' Visible="false"></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField> 
                            <ItemTemplate>
                                <asp:ImageButton ID="btnAdjuntar" runat="server" CommandName="Attach" ImageUrl="~/Image/btn/attach.jpg"
                                    Height="16px" Width="16px" ToolTip="Adjuntar" Enabled="true" 
                                    Visible='<%# HttpUtility.HtmlDecode((string)Eval("NombreArchivo")) == "" %>' 
                                    onclick="btnAdjuntar_Click" />
                                <asp:FileUpload ID="fuArchivo" runat="server" Visible="false" />
                                <asp:ImageButton ID="btnSave" runat="server" CommandName="Save" ImageUrl="~/Image/btn/apply.png"
                                    Height="16px" Width="16px" ToolTip="Guardar" Visible="false" 
                                    onclick="btnSave_Click" />
                                <asp:HiddenField ID="hfIdArchivo" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" ImageUrl="~/Image/btn/list.png"
                                    Height="16px" Width="16px" ToolTip="Visualizar" Enabled="True" 
                                   onclick="btnMostrar_Click" Visible='<%# HttpUtility.HtmlDecode((string)Eval("NombreArchivo")) != "" %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Borrar" ImageUrl="~/Image/btn/delete.gif"
                                    Height="16px" Width="16px" ToolTip="Eliminar" 
                                    Visible='<%# HttpUtility.HtmlDecode((string)Eval("NombreArchivo")) != "" %>' Enabled="true"
                                    OnClientClick="javascript:if (!confirm('¿Está seguro que desea eliminar el documento?')) return false;" 
                                    onclick="btnEliminar_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="LightBlue" />
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>

        
    </table>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

//        $(document).ready(function() {
//            $('#cphCont_gvDocumentos_fuArchivo_2').change(function () {
//                {
//                    $('._4remov').remove();
//                    var aspFileUpload = document.getElementById("fuArchivo");
//                    //var errorLabel = document.getElementById("ErrorLabel");
//                    //var img = document.getElementById("imgUploadThumbnail");

//                    var fileName = $('#cphCont_gvDocumentos_fuArchivo_2').val();
//                    var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
//                    if (ext == "jpeg" || ext == "jpg" || ext == "png") {
//                        //img.src = fileName;
//                    } else {
//                        //img.src = "../Images/blank.gif";
//                        //errorLabel.innerHTML = "Invalid image file, must select a *.jpeg, *.jpg, or *.png file.";
//                        $("#secErrorMaster").append("<span class='lbE _4remov'>Archivo invalido. </span>");
//                    }
//                }
//            });
//        });
//        function ShowThumbnail() {

//            $('._4remov').remove();
//            var aspFileUpload = document.getElementById("fuArchivo");
//            //var errorLabel = document.getElementById("ErrorLabel");
//            //var img = document.getElementById("imgUploadThumbnail");

//            var fileName = $('#cphCont_gvDocumentos_fuArchivo_2').val();
//            var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
//            if (ext == "jpeg" || ext == "jpg" || ext == "png") {
//                //img.src = fileName;
//            } else {
//                //img.src = "../Images/blank.gif";
//                //errorLabel.innerHTML = "Invalid image file, must select a *.jpeg, *.jpg, or *.png file.";
//                $("#secErrorMaster").append("<span class='lbE _4remov'>Archivo invalido. </span>");
//            }
//        }

//        function CheckImageSize() {
//            var aspFileUpload = document.getElementById("FileUpload1");
//            var errorLabel = document.getElementById("ErrorLabel");
//            var img = document.getElementById("imgUploadThumbnail");

//            var fileName = aspFileUpload.value;
//            var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
//            if (!(ext == "jpeg" || ext == "jpg" || ext == "png")) {
//                errorLabel.innerHTML = "Invalid image file, must select a *.jpeg, *.jpg, or *.png file.";
//                return false;
//            }
//            if (img.fileSize == -1) {
//                errorLabel.innerHTML = "Couldn't load image file size.  Please try to save again.";
//                return false;
//            }
//            else if (img.fileSize <= 3145728) {
//                errorLabel.innerHTML = "";
//                return true;
//            }
//            else {
//                var fileSize = (img.fileSize / 1048576);
//                errorLabel.innerHTML = "File is too large, must select file under 3 Mb. File  Size: " + fileSize.toFixed(1) + " Mb";
//                return false;
//            }
//        }

    </script>
</asp:Content>
