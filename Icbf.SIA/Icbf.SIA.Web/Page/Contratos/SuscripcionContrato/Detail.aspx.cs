using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad Contratos
/// </summary>
public partial class Page_Contratos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SuscripcionContrato";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {

            //if (GetSessionParameter("Contratos.Guardado").ToString() == "1")
            //    toolBar.MostrarMensajeGuardado();
            //RemoveSessionParameter("Contratos.Guardado");


            //Contratos vContratos = new Contratos();
            //vContratos = vContratoService.ConsultarContratos();
            //txtFechaRegistroSistema.Text = vContratos.FechaRegistroSistema.ToString();
            //txtIdContrato.Text = vContratos.IdContrato.ToString();
            //ddlVigenciaFiscalinicial.SelectedValue = vContratos.VigenciaFiscalinicial.ToString();
            //ddlIDRegional.SelectedValue = vContratos.IDRegional.ToString();
            //ddlIDCategoriaContrato.SelectedValue = vContratos.IDCategoriaContrato.ToString();
            //ddlIDTipoContrato.SelectedValue = vContratos.IDTipoContrato.ToString();
            //txtFechaInicioEjecucion.Text = vContratos.FechaInicioEjecucion.ToString();
            //txtFechaFinalizaInicioContrato.Text = vContratos.FechaFinalizaInicioContrato.ToString();
            //ObtenerAuditoria(PageName, );
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContratos.UsuarioCrea, vContratos.FechaCrea, vContratos.UsuarioModifica, vContratos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {

            //Contratos vContratos = new Contratos();
            //vContratos = vContratoService.ConsultarContratos();
            //InformacionAudioria(vContratos, this.PageName, vSolutionPage);
            //int vResultado = vContratoService.EliminarContratos(vContratos);
            //if (vResultado == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
            //    SetSessionParameter("Contratos.Eliminado", "1");
            //    NavigateTo(SolutionPage.List);
            //}
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos(@"Suscripción Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
