<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Fecha Registro al Sistema
                <asp:CompareValidator runat="server" ID="CompareValidator2" ControlToValidate="txtFechaRegistroSistema$txtFecha" Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha Registro al Sistema Válida" ForeColor="red"  ValidationGroup="btnBuscar"></asp:CompareValidator>
            </td>
            <td class="Cell">
                Id Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistema" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                <%--<asp:TextBox runat="server" ID="txtFechaRegistroSistema" MaxLength="10" ></asp:TextBox>--%>
            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="9" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Vigencia Fiscal Inicial
            </td>
            <td class="Cell">
                Regional del Contrato</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDRegional"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Categoria del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Fecha de Inicio Ejecución
                <asp:CompareValidator runat="server" ID="cvFechaDesdeValida" ControlToValidate="txtFechaInicioEjecucion$txtFecha" Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha de Inicio Ejecución Válida" ForeColor="red"  ValidationGroup="btnBuscar"></asp:CompareValidator>
            </td>
            <td class="Cell">
                Fecha de Finalización Inicial Contrato/Convenio
                <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="txtFechaFinalizaInicioContrato$txtFecha" Type="Date" Operator="DataTypeCheck" ErrorMessage=" Ingrese Fecha de Finalización Inicial Contrato/Convenio Válida" ForeColor="red"  ValidationGroup="btnBuscar"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaInicioEjecucion" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                <%--<asp:TextBox runat="server" ID="txtFechaInicioEjecucion" MaxLength="10" ></asp:TextBox>--%>
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFechaFinalizaInicioContrato" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                <%--<asp:TextBox runat="server" ID="txtFechaFinalizaInicioContrato" MaxLength="10" ></asp:TextBox>--%>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato,FechaInicioEjecucion" CellPadding="0" Height="16px"
                        OnSorting="gvContratos_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContratos_PageIndexChanging" OnSelectedIndexChanged="gvContratos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Suscribir Contrato" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro al Sistema" DataField="FechaCrea"  SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Id Contrato/Convenio" DataField="IdContrato"  SortExpression="IdContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal Inicial" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional del Contrato" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Categoria del Contrato" DataField="NombreCategoriaContrato"  SortExpression="NombreCategoriaContrato"/>
                            <asp:BoundField HeaderText="Tipo de Contrato/Convenio" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                            <asp:BoundField HeaderText="Fecha de Inicio Ejecución" DataField="FechaInicioEjecucion"  SortExpression="FechaInicioEjecucion" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Fecha de Finalización Inicial Contrato/Convenio" DataField="FechaFinalizacionIniciaContrato"  SortExpression="FechaFinalizaInicioContrato" DataFormatString="{0:dd/MM/yyyy}"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
