<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Contratos_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Fecha Registro al Sistema
            </td>
            <td>
                Id Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaRegistroSistema"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia Fiscal Inicial
            </td>
            <td>
                Regional del Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlVigenciaFiscalinicial"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIDRegional"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Categoria del Contrato
            </td>
            <td>
                Tipo de Contrato
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato"  Enabled="false"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Fecha Inicio Ejecución
            </td>
            <td>
                Fecha Finalización Inicial Contrato/Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaInicioEjecucion"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinalizaInicioContrato"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
