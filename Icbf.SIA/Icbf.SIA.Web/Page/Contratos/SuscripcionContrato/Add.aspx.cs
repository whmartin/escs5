using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Icbf.RUBO.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Text;
using System.Text.RegularExpressions;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición para la entidad Contratos
/// </summary>
public partial class Page_Contratos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    SIAService vSiaService = new SIAService();
    string PageName = "Contratos/SuscripcionContrato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();

    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad Contratos
    /// </summary>
    private void Guardar()
    {
        try
        {
            DateTime vFechaInicioEjecucion;
            DateTime vFechaFinalizacionIniciaContrato;
            DateTime vFechaSuscripcion;
            string vNumeroContrato;
            int vNumeroContratoOut;

            string urlSECOP = System.Configuration.ConfigurationManager.AppSettings["urlTemplateSECOP"];

            if ( ! string.IsNullOrEmpty(txtVinculoSecop.Text) && !txtVinculoSecop.Text.StartsWith(urlSECOP, StringComparison.OrdinalIgnoreCase))
            {
                toolBar.MostrarMensajeError("El vinculo del SECOP, no cumple con el formato establecido.");
                return;                          
            }

            if (! int.TryParse(txtNumeroContrato.Text, out vNumeroContratoOut))
            {
                toolBar.MostrarMensajeError("el número del contrato es invalido, verifique por favor.");
                return;                
            }

            if (vNumeroContratoOut <= 0 || vNumeroContratoOut >= 99999)
            {
                toolBar.MostrarMensajeError("el número del contrato se encuenta por fuera de los rangos habilitados, verifique por favor.");
                return;
            }

            if (txtFechaSuscripcion.Text != string.Empty)
            {
                if (!DateTime.TryParse(txtFechaSuscripcion.Text.Trim(), out vFechaSuscripcion))
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                if (vFechaSuscripcion > DateTime.Now)
                {  
                    toolBar.MostrarMensajeError("La fecha de suscripción debe ser menor o igual a la fecha actual.");
                    return;
                }
            }
            else
            {
                toolBar.MostrarMensajeError("Debe Ingresar la fecha de suscripción, verifique por favor.");
                return;
            }


            if (GetSessionParameter("Contratos.FechaInicioEjecucion") != null)
            {
                if (!DateTime.TryParse(GetSessionParameter("Contratos.FechaInicioEjecucion").ToString(), out vFechaInicioEjecucion))
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }
                else
                {
                    if (vFechaInicioEjecucion > vFechaSuscripcion)
                    {
                        toolBar.MostrarMensajeError("La fecha de suscripción debe ser mayor o igual a la fecha inicio del contrato.");
                        return;
                    }
                }
            }
            
            if (GetSessionParameter("Contratos.FechaFinalizacionIniciaContrato") != null)
            {
                if (DateTime.TryParse(GetSessionParameter("Contratos.FechaFinalizacionIniciaContrato").ToString(), out vFechaFinalizacionIniciaContrato))
                {
                    if (vFechaFinalizacionIniciaContrato < vFechaSuscripcion)
                    {
                        toolBar.MostrarMensajeError("La fecha de suscripción debe ser menor o igual a la fecha de finalizacion inicial Contrato/Convenio del contrato.");
                        return;
                    }
                }
            }

            if (GetSessionParameter("Contratos.IdContatoRegistrado") != null)
            {
                int vidContrato;
                int vIdVigencia;
                int vIdRegional;
                string vConsecutivo;

                vidContrato = int.Parse(GetSessionParameter("Contratos.IdContatoRegistrado").ToString());

                var lContrato = vContratoService.ConsultarContratoss(null, null, vidContrato, null, null, null, null,
                                                                     null, null, null, null, null, null, null, null, null,null);

                 vIdVigencia = Convert.ToInt32(lContrato[0].IdVigenciaInicial);              
                 vIdRegional = Convert.ToInt32(lContrato[0].IdRegionalContrato);

                Vigencia  vVigencia = vSiaService.ConsultarVigencia(vIdVigencia);
                Regional  vRegional = vSiaService.ConsultarRegional(vIdRegional);

                vNumeroContrato = vNumeroContratoOut.ToString().PadLeft(5, '0');

                vConsecutivo = vRegional.CodigoRegional.Trim() + "" + vNumeroContrato.Trim() + "" + vVigencia.AcnoVigencia.ToString().Trim();

                var lNumeroContrato = vContratoService.ConsultarContratoss(
                                                                           null, null, null, vConsecutivo, null, null, null,
                                                                           null, null, null, null, null, null, null, null, null,null
                                                                          );
                if (lNumeroContrato.Count > 0)
                {
                    toolBar.MostrarMensajeError("El consecutivo ya se encuentra asociado al id contrato "+ lNumeroContrato[0].IdContrato  + ", verifique por favor.");
                    return;                    
                }

                List<EstadoContrato> vEstadoContrato = vContratoService.ConsultarEstadoContrato(null, "SUS", null, true);

                if (vEstadoContrato.Count == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                if (vEstadoContrato[0].IdEstadoContrato != lContrato[0].IdEstadoContrato)
                {

                    var resultContrato = vContratoService.ContratoObtener(vidContrato);

                DateTime? fechaFinalTerminacion = null;

                if (resultContrato.EsFechaFinalCalculada.HasValue && resultContrato.EsFechaFinalCalculada.Value)
                {
                    fechaFinalTerminacion = ContratoService.CalcularFechaFinalContrato
                        (
                         vFechaSuscripcion,
                         resultContrato.DiasFechaFinalCalculada.Value,
                         resultContrato.MesesFechaFinalCalculada.Value
                        );
                }

                int vResultado = vContratoService.SuscribirContrato(
                                                                    vidContrato, 
                                                                    GetSessionUser().NombreUsuario, 
                                                                    vFechaSuscripcion, 
                                                                    vEstadoContrato[0].IdEstadoContrato,
                                                                    vConsecutivo,
                                                                    fechaFinalTerminacion,
                                                                    txtVinculoSecop.Text
                                                                    );
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    return;
                }

                if (vResultado >= 1)
                {
                    if (lContrato.Count > 0)
                    {
                        toolBar.MostrarMensajeGuardado("La operación se completó satisfactoriamente.");
                        foreach (GridViewRow fila in gvDocumentos.Rows)
                        {
                            ImageButton ibtnAdjuntar = ((ImageButton) fila.Cells[1].FindControl("btnAdjuntar"));
                            ImageButton ibtnEliminar = ((ImageButton)fila.Cells[3].FindControl("btnEliminar"));
                            ibtnAdjuntar.Enabled = false;
                            ibtnEliminar.Enabled = false;
                        }

                        txtNumeroContrato.Enabled = false;
                        txtFechaSuscripcion.Enabled = false;
                        txtNumeroContrato.Text = vConsecutivo;
                        cetxtFechaSuscripcion.Enabled = false;
                    }
                }
                else
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
               }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message + ex.StackTrace);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.SetSaveConfirmation(" if(!ConfirmarSuscripcion()) return false;");
            toolBar.EstablecerTitulos(@"Suscripción Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {

            //Contratos vContratos = new Contratos();
            //vContratos = vContratoService.ConsultarContratos();
            //txtFechaRegistroSistema.Text = vContratos.FechaRegistroSistema.ToString();
            //txtIdContrato.Text = vContratos.IdContrato.ToString();
            //ddlVigenciaFiscalinicial.SelectedValue = vContratos.VigenciaFiscalinicial.ToString();
            //ddlIDRegional.SelectedValue = vContratos.IDRegional.ToString();
            //ddlIDCategoriaContrato.SelectedValue = vContratos.IDCategoriaContrato.ToString();
            //ddlIDTipoContrato.SelectedValue = vContratos.IDTipoContrato.ToString();
            //txtFechaInicioEjecucion.Text = vContratos.FechaInicioEjecucion.ToString();
            //txtFechaFinalizaInicioContrato.Text = vContratos.FechaFinalizaInicioContrato.ToString();
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vContratos.UsuarioCrea, vContratos.FechaCrea, vContratos.UsuarioModifica, vContratos.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            txtNombreUsuario.Text = GetSessionUser().NombreUsuario;
            List<Usuario> lUsuario = new List<Usuario>();

            lUsuario = vSiaService.ConsultarUsuarios(GetSessionUser().NumeroDocumento.Trim(), GetSessionUser().PrimerNombre.Trim(), GetSessionUser().PrimerApellido.Trim(), 1, true);

            if (lUsuario.Count > 0)
            {
                Regional usuarioRegional = vSiaService.ConsultarRegional(lUsuario[0].IdRegional);
                txtRegional.Text = usuarioRegional.NombreRegional;
            }
            List<ArchivoContrato> lArchivoContrato = new List<ArchivoContrato>();

            lArchivoContrato = vContratoService.ConsultarArchivo(Convert.ToInt32(GetSessionParameter("Contratos.IdContatoRegistrado")));
            lArchivoContrato.Add(new ArchivoContrato
            {
                IdArchivoContrato = 0,
                IdArchivo = 0,
                IdContrato = 0,
                NombreArchivo = "",
                NombreArchivoOri = ""
            });

            this.gvDocumentos.DataSource = lArchivoContrato;
            this.gvDocumentos.DataBind();

            txtCargoUsuario.Enabled = false;
            txtNombreUsuario.Enabled = false;
            txtRegional.Enabled = false;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocumentos_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, ImageClickEventArgs e)
    {

        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
                Save(dataIteme.RowIndex);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Save(int pRow)
    {
        int rowIndex = pRow; 

        int maxFileSizeDoc = 4;

        FileUpload fuArchivo = (FileUpload)gvDocumentos.Rows[rowIndex].FindControl("fuArchivo");

        if (fuArchivo.HasFile)
        {
            try
            {
                //Extraer la extensión del archivo
                FileInfo finfo = new FileInfo(fuArchivo.FileName);

                string fileExt = finfo.Extension.ToLower();


                if (fileExt == ".pdf" )
                {
                    //Si el achivo es un documento o una imagen que no exceda los 4 MegaBytes
                    if (fuArchivo.PostedFile.ContentLength / 1024 > maxFileSizeDoc * 1024)
                    {
                        throw new GenericException("Documento no v&aacute;lido por tamaño.");
                    }
                }
                else
                    throw new GenericException("Formato de archivo no valido.");
                

                string filename = Path.GetFileName(fuArchivo.FileName);

                //Creación del Nombre del archivo
                string NombreArchivo = filename.Substring(0, filename.IndexOf(".")); 
                NombreArchivo = vContratoService.QuitarAcentos(NombreArchivo);
                NombreArchivo = DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" +
                                DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" +
                                DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() +
                                "_DocSusContrato_" + NombreArchivo + filename.Substring(filename.LastIndexOf("."));
                //NombreArchivo = Guid.NewGuid().ToString() + NombreArchivo;
                string NombreArchivoFtp = ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                          ConfigurationManager.AppSettings["FtpPort"] + @"/";

                //Guardado del archivo en el servidor
                HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;
                Boolean ArchivoSubido = vSiaService.SubirArchivoFtp((Stream)vHttpPostedFile.InputStream,
                                                                     NombreArchivoFtp, NombreArchivo);


                if (ArchivoSubido)
                {
                    int vResultado;
                    //toolBar.MostrarMensajeError("El archivo se cargo correctamente");
                    toolBar.MostrarMensajeGuardado();

                    //Creación del registro de control del archivo subido

                    ArchivoContrato RegistroControl = new ArchivoContrato();
                    RegistroControl.IdUsuario = Convert.ToInt32(GetSessionUser().IdUsuario.ToString());
                    RegistroControl.FechaRegistro = DateTime.Now;
                    RegistroControl.ServidorFTP = NombreArchivoFtp;
                    RegistroControl.NombreArchivo = NombreArchivo;
                    RegistroControl.NombreArchivoOri = filename;
                    RegistroControl.NombreTabla = "Minuta";

                    List<FormatoArchivo> vFormatoArchivo = vSiaService.ConsultarFormatoArchivoTemporarExt("General.archivoPDF", "PDF");

                    RegistroControl.IdFormatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;
                    RegistroControl.Estado = "C";
                    RegistroControl.ResumenCarga = string.Empty;
                    RegistroControl.UsuarioCrea = GetSessionUser().NombreUsuario;
                    RegistroControl.FechaCrea = DateTime.Now;

                    InformacionAudioria(RegistroControl, PageName, vSolutionPage);
                    vResultado = vContratoService.InsertarArchivo(RegistroControl);
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        decimal vIdArchivo = RegistroControl.IdArchivo;
                        int idContrato =
                            Convert.ToInt32(GetSessionParameter("Contratos.IdContatoRegistrado"));
                        ArchivoContrato vArchivoContrato = new ArchivoContrato();


                        vArchivoContrato.IdArchivo = vIdArchivo;
                        vArchivoContrato.IdContrato = idContrato;
                        vArchivoContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                        vArchivoContrato.FechaCrea = DateTime.Now;
                        vArchivoContrato.TipoEstructura = "Minuta";

                        vResultado = vContratoService.InsertarArchivoContrato(vArchivoContrato);
                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                            return;

                        }
                        else if (vResultado == 1)
                        {
                            SetSessionParameter("Contratos.IdArchivoContrato", vArchivoContrato.IdArchivoContrato);
                            SetSessionParameter("Contratos.Guardado", "1");
                            //NavigateTo(SolutionPage.Detail);

                        }
                        else
                        {
                            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                            return;
                        }

                        List<ArchivoContrato> lArchivoContrato = new List<ArchivoContrato>();

                        lArchivoContrato = vContratoService.ConsultarArchivo(idContrato);
                        lArchivoContrato.Add(new ArchivoContrato
                        {
                            IdArchivoContrato = 0,
                            IdArchivo = 0,
                            IdContrato = 0,
                            NombreArchivo = "",
                            NombreArchivoOri = ""
                        });

                        this.gvDocumentos.DataSource = lArchivoContrato;
                        this.gvDocumentos.DataBind();

                        HiddenField hfIdArchivo = (HiddenField)gvDocumentos.Rows[rowIndex].FindControl("hfIdArchivo");
                        hfIdArchivo.Value = vArchivoContrato.IdArchivoContrato.ToString();

                        Label LbArchivo = (Label)gvDocumentos.Rows[rowIndex].FindControl("LbArchivo");

                        if (LbArchivo != null)
                        {
                            LbArchivo.Text = filename;
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
                return;
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Seleccione un archivo");
            return;
        }



        fuArchivo.Visible = false;
        ImageButton btnAdjuntar = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnAdjuntar");
        btnAdjuntar.Visible = false;
        btnAdjuntar.Enabled = false;
        ImageButton btnSave = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnSave");
        btnSave.Visible = false;

        ImageButton btnMostrar = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnMostrar");
        btnMostrar.Enabled = true;
        ImageButton btnEliminar = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnEliminar");
        btnEliminar.Enabled = true;
        //foreach (GridViewRow gvRow in gvDocumentos.Rows)
        //{
        //    ImageButton btnSeleccionar = (ImageButton)gvRow.FindControl("btnSeleccionar");
        //    btnSeleccionar.Enabled = true;
        //}
        toolBar.LipiarMensajeError();
    }

    protected void btnAdjuntar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
            {

                int rowIndex = dataIteme.RowIndex;

                FileUpload fuArchivo =
                    (FileUpload)gvDocumentos.Rows[rowIndex].Cells[(int)grilla.filearchivo].FindControl("fuArchivo");
                fuArchivo.Visible = true;
                ImageButton btnAdjuntar =
                    (ImageButton)gvDocumentos.Rows[rowIndex].Cells[(int)grilla.adjuntar].FindControl("btnAdjuntar");
                btnAdjuntar.Visible = false;
                ImageButton btnSave =
                    (ImageButton)gvDocumentos.Rows[rowIndex].Cells[(int)grilla.guardar].FindControl("btnSave");
                btnSave.Visible = true;
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private enum grilla
    {
        Archivo = 0,
        adjuntar = 1,
        filearchivo = 1,
        guardar = 1,
        mostrar = 2
    }

    protected void btnMostrar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {

            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;

            if (dataItem != null)
            {
                Show(dataItem.RowIndex);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Show(int pRow)
    {
        try
        {
            //gvDocumentos
            int rowIndex = pRow; //pRow.SelectedRow.RowIndex;
            string nombreArchivo = "";

            Label LbArchivo = (Label)gvDocumentos.Rows[rowIndex].FindControl("LbArchivoOri");
            if (LbArchivo != null)
            {
                nombreArchivo = LbArchivo.Text;
            }

            Response.Redirect("../DescargarArchivo/DescargarArchivo.aspx?fname=" + nombreArchivo);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnEliminar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
            {
                Delete(dataIteme.RowIndex);
                
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Delete(int pRow)
    {
        int rowIndex = pRow; // pRow.SelectedRow.RowIndex;
        int idArchivoContrato;


        HiddenField hfIdArchivo = (HiddenField)gvDocumentos.Rows[rowIndex].FindControl("hfIdArchivo");
        if (hfIdArchivo.Value != "")
        {
            //Guardado en esta sesión
            idArchivoContrato = Convert.ToInt32(hfIdArchivo.Value);
        }
        else
        {
            //Guardado previamente
            idArchivoContrato = Convert.ToInt32(gvDocumentos.DataKeys[rowIndex].Value.ToString());
        }

        int vIdContrato = Convert.ToInt32(GetSessionParameter("Contratos.IdContatoRegistrado"));

        ArchivoContrato vArchivoContrato = new ArchivoContrato();
        vArchivoContrato.IdArchivoContrato = idArchivoContrato;
        int vResultado = vContratoService.EliminarArchivoContrato(vArchivoContrato);

        if (vResultado == 0)
        {
            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
        }
        else if (vResultado == 1)
        {
            toolBar.MostrarMensajeGuardado("El documento ha sido eliminado correctamente.");
            //Response.Redirect(Request.RawUrl);
        }

        List<ArchivoContrato> lArchivoContrato = new List<ArchivoContrato>();

        lArchivoContrato = vContratoService.ConsultarArchivo(vIdContrato);
        lArchivoContrato.Add(new ArchivoContrato
        {
            IdArchivoContrato = 0,
            IdArchivo = 0,
            IdContrato = 0,
            NombreArchivo = ""

        });

        this.gvDocumentos.DataSource = lArchivoContrato;
        this.gvDocumentos.DataBind();

        ImageButton btnAdjuntar = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnAdjuntar");
        btnAdjuntar.Enabled = true;
        ImageButton btnMostrar = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnMostrar");
        btnMostrar.Enabled = false;
        ImageButton btnEliminar = (ImageButton)gvDocumentos.Rows[rowIndex].FindControl("btnEliminar");
        btnEliminar.Enabled = false;
        //toolBar.LipiarMensajeError();
    }

    /// <summary>
    /// Gonet
    /// Quita los acentos de una cadena de texto eliminando también espacios
    /// en blanco existentes en cualquier ubicación dentro de la cadena de
    /// texto dada
    /// 31-Dic-2013
    /// </summary>
    /// <param name="Texto">Cadena de texto a la que se van a quitar los acentos</param>
    /// <returns>Cadena texto sin acentos</returns>
    public string QuitarAcentos(string Texto)
    {
        Texto = Texto.Replace(" ", "");

        //quitar acentos 
        var textoNormalizado = Texto.Normalize(NormalizationForm.FormD);
        var reg = new Regex("[^a-zA-Z0-9 ]");
        string textoFinal = reg.Replace(textoNormalizado, "");

        return textoFinal;
    }


}
