<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SupervisionGruposApoyoContratos_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <asp:HiddenField ID="hfIdGrupo" runat="server" />
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Nombre del Grupo *
                    <asp:RequiredFieldValidator runat="server" ID="rfvGrupo" ControlToValidate="txtGrupo"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    Direcci&oacute;n*
                    <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccion" ControlToValidate="ddlDireccion"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvIdDireccion" ControlToValidate="ddlDireccion"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtGrupo" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftGrupo" runat="server" TargetControlID="txtGrupo"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;0123456789" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDireccion" Width="80%" Enabled="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Vigencia *
                    <asp:RequiredFieldValidator runat="server" ID="rfvVigencia" ControlToValidate="ddlVigencia"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvVigencia" ControlToValidate="ddlVigencia"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
                <td>
                    Regional
                    <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlVigencia" Width="80%">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdRegional" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSupervisionGruposApoyoContratos" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdGrupo,IdRegional" CellPadding="0"
                        Height="16px" OnPageIndexChanging="gvSupervisionGruposApoyoContratos_PageIndexChanging"
                        
                        OnSelectedIndexChanged="gvSupervisionGruposApoyoContratos_SelectedIndexChanged" 
                        onrowdatabound="gvSupervisionGruposApoyoContratos_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Contrato" DataField="NumeroContrato" />
                            <asp:BoundField HeaderText="Nombre Entidad" DataField="NombreEntidadContratista" ItemStyle-Width="300px" />
                            <asp:BoundField HeaderText="Supervisor" DataField="NombreSupervisor" />
                            <asp:BoundField HeaderText="Estado" DataField="NombreEstado" />
                            <asp:BoundField HeaderText="Regional" DataField="NombreDireccionICBF"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
