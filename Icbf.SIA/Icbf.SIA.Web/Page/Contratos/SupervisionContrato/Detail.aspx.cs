using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_SupervisionGruposApoyoContratos_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionContrato";
    SupervisionService vSupervisionService = new SupervisionService();
    List<Regional> lstRegionales;
    System.Drawing.Color vColorDesactivado = System.Drawing.Color.FromArgb(235, 235, 228);
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionGruposApoyoContratos.IdContratosGrupo", hfIdContratosGrupo.Value);
        SetSessionParameter("SupervisionGruposApoyoContratos.IdGrupo", hfIdGrupo.Value);
        SetSessionParameter("SupervisionGruposApoyoContratos.IdContrato", hfIdContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void CargarDatos()
    {
        try
        {

            ddlIdRegional.SelectedValue = GetSessionParameter("SupervisionGruposApoyoContratos.Regional").ToString();

            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            hfIdGrupo.Value = GruposSupervision[1];
            txtGrupo.Enabled = false;

            ddlDireccion.SelectedValue = GruposSupervision[2];

            //Se les asigna color grside fondo a los controles para que tengan apariencia inactiva.
            ddlDireccion.BackColor = vColorDesactivado;
            ddlVigencia.BackColor = vColorDesactivado;
            ddlIdRegional.BackColor = vColorDesactivado;
            lstMain.BackColor = vColorDesactivado;
            lstSelect.BackColor = vColorDesactivado;
            lstMain.Enabled = false;
            lstSelect.Enabled = false;

            lstSelect.DataSource = vSupervisionService.ConsultarSupervisionGruposApoyoContratoss(null, Convert.ToInt32(hfIdGrupo.Value), Convert.ToInt32(GruposSupervision[2]), Convert.ToInt32(ddlIdRegional.SelectedValue), Convert.ToInt32(ddlVigencia.SelectedValue), 1);
            lstSelect.DataValueField = "idContratosGrupo";
            lstSelect.DataTextField = "NumeroContrato";
            lstSelect.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarRegistro()
    {
        try
        {
            int vIdContratosGrupo = Convert.ToInt32(hfIdContratosGrupo.Value);
            int vIdGrupo = Convert.ToInt32(hfIdGrupo.Value);
            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();
            vSupervisionGruposApoyoContratos = vSupervisionService.ConsultarSupervisionGruposApoyoContratos(vIdContratosGrupo,vIdGrupo,vIdContrato);
            InformacionAudioria(vSupervisionGruposApoyoContratos, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionGruposApoyoContratos(vSupervisionGruposApoyoContratos);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionGruposApoyoContratos.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Contratos", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {

            Usuario usuario = new Usuario();

            //Se llena el combo de vigencias a partir del 2016, que es el año del presente desarrollo
            for (int i = 2016; i <= DateTime.Now.Year; i++)
            {
                ddlVigencia.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
            usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);
            lstRegionales = vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null);
            ManejoControlesSupervision.LlenarComboLista(ddlIdRegional, lstRegionales, "IdRegional", "NombreRegional");

            ManejoControlesSupervision objControlesSupervision = new ManejoControlesSupervision();
            objControlesSupervision.LlenarDireccion(ddlDireccion, "-1", true);
           

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
