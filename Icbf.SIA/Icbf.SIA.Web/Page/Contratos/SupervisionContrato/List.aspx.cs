using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

public partial class Page_SupervisionGruposApoyoContratos_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionContrato";
    SupervisionService vSupervisionService = new SupervisionService();
    List<Regional> lstRegionales;
    SIAService vRuboService = new SIAService();
    string vGRegional;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        //Se agrupan las celdas de la primera columna
        //int RowSpan = 2;

        //for (int i = gvSupervisionGruposApoyoContratos.Rows.Count - 2; i >= 0; i--)
        //{
        //    GridViewRow currRow = gvSupervisionGruposApoyoContratos.Rows[i];
        //    GridViewRow prevRow = gvSupervisionGruposApoyoContratos.Rows[i + 1];

        //        //if (currRow.Cells[gvSupervisionGruposApoyoContratos.Columns.Count - 1].Text == prevRow.Cells[gvSupervisionGruposApoyoContratos.Columns.Count - 1].Text)
        //        if (((SupervisionGruposApoyoContratos)currRow.DataItem).IdRegional == ((SupervisionGruposApoyoContratos)prevRow.DataItem).IdRegional)
        //        {
        //            currRow.Cells[0].RowSpan = RowSpan;
        //            prevRow.Cells[0].Visible = false;
        //            RowSpan += 1;
        //        }
        //        else
        //        { RowSpan = 2; }
        //}
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdGrupo = null;
            int? vIdRegional = null;
            int? vVigencia = null;

            if (hfIdGrupo.Value != "")
            {
                vIdGrupo = Convert.ToInt32(hfIdGrupo.Value);
            }
            if (ddlIdRegional.SelectedValue != "-1")
            {
                vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            }
            if (ddlVigencia.SelectedValue != "-1")
            {
                vVigencia = Convert.ToInt32(ddlVigencia.SelectedValue);
            }

            gvSupervisionGruposApoyoContratos.DataSource = vSupervisionService.ConsultarSupervisionGruposApoyoContratoss(null, vIdGrupo, null, vIdRegional, vVigencia, 1);
            gvSupervisionGruposApoyoContratos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionGruposApoyoContratos.PageSize = PageSize();
            gvSupervisionGruposApoyoContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Asociaci&oacute;n de Contratos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionGruposApoyoContratos.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionGruposApoyoContratos.Eliminado");

            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            hfIdGrupo.Value = GruposSupervision[1];
            txtGrupo.Enabled = false;

            Usuario usuario = new Usuario();

            ManejoControlesSupervision objControlesSupervision = new ManejoControlesSupervision();
            objControlesSupervision.LlenarDireccion(ddlDireccion, "-1", true);
            ddlDireccion.SelectedValue = GruposSupervision[2];

            //Se llena el combo de vigencias a partir del 2016, que es el a�o del presente desarrollo
            for (int i = 2016; i <= DateTime.Now.Year; i++)
            {
                ddlVigencia.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
            usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);
            lstRegionales = vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null);
            ManejoControlesSupervision.LlenarComboLista(ddlIdRegional, lstRegionales, "IdRegional", "NombreRegional");

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }


    #region "Gridview"

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionGruposApoyoContratos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionGruposApoyoContratos.IdGrupo", strValue);
            SetSessionParameter("SupervisionGruposApoyoContratos.Vigencia", ddlVigencia.SelectedValue);
            SetSessionParameter("SupervisionGruposApoyoContratos.Regional", gvSupervisionGruposApoyoContratos.DataKeys[pRow.RowIndex].Values[1].ToString());
            
            //gvSupervisionGruposApoyoContratos.Rows[rowIndex].Cells[5].Text
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionGruposApoyoContratos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionGruposApoyoContratos.SelectedRow);
    }

    protected void gvSupervisionGruposApoyoContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionGruposApoyoContratos.PageIndex = e.NewPageIndex;
        Buscar();
    }

    protected void gvSupervisionGruposApoyoContratos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //Se agrupan las celdas de la primera columna
        int RowSpan = 2;

        for (int i = gvSupervisionGruposApoyoContratos.Rows.Count - 2; i >= 0; i--)
        {
            GridViewRow currRow = gvSupervisionGruposApoyoContratos.Rows[i];
            GridViewRow prevRow = gvSupervisionGruposApoyoContratos.Rows[i + 1];

            if (currRow.Cells[gvSupervisionGruposApoyoContratos.Columns.Count - 1].Text == prevRow.Cells[gvSupervisionGruposApoyoContratos.Columns.Count - 1].Text)
            //if (((SupervisionGruposApoyoContratos)currRow.DataItem).IdRegional == ((SupervisionGruposApoyoContratos)prevRow.DataItem).IdRegional)
            {
                currRow.Cells[0].RowSpan = RowSpan;
                prevRow.Cells[0].Visible = false;
                RowSpan += 1;
            }
            else
            { RowSpan = 2; }
        }
    }

    #endregion



}
