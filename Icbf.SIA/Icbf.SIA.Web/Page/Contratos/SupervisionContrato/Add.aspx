<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SupervisionGruposApoyoContratos_Add" 
    enableEventValidation="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContratosGrupo" runat="server" />
    <asp:HiddenField ID="hfIdGrupo" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <script src="../../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/SupervisionGruposApoyoContratos.js" type="text/javascript"></script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td  style="width:50%">
                Nombre del Grupo *
                <asp:RequiredFieldValidator runat="server" ID="rfvGrupo" ControlToValidate="txtGrupo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Dirección*
                <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccion" ControlToValidate="ddlDireccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdDireccion" ControlToValidate="ddlDireccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtGrupo" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftGrupo" runat="server" TargetControlID="txtGrupo"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlDireccion" Width="80%" Enabled="False">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvVigencia" ControlToValidate="ddlVigencia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvVigencia" ControlToValidate="ddlVigencia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlVigencia" Width="80%" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlVigencia_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional" Width="80%" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlIdRegional_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <table width="100%" align="center">
                    <tr class="rowB">
                        <td style="width: 45%">
                            Contratos
                        </td>
                        <td style="width: 5%">
                        </td>
                        <td style="width: 50%">
                            Contratos Grupo de Apoyo a la Supervisión *
                        </td>
                    </tr>
                    <tr class="rowA">
                        <td>
                            <asp:ListBox ID="lstMain" runat="server" SelectionMode="Multiple" Width="90%" Height="300px" ClientIDMode="Static">
                            </asp:ListBox>
                        </td>
                        <td>
                            <input id="Agregar" type="button" value="->" onclick="moverOptions('lstMain','lstSelect','lstSelect');" />
                            <br />
                            <br />
                            <input id="Eliminar" type="button" value="<-" onclick="moverOptions('lstSelect','lstMain','lstSelect');" />
                        </td>
                        <td>
                            <asp:HiddenField ID="listPre" runat="server" ClientIDMode="Static" />
                            <asp:ListBox ID="lstSelect" runat="server" SelectionMode="Multiple" Width="80%" Height="300px" ClientIDMode="Static">
                            </asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
 