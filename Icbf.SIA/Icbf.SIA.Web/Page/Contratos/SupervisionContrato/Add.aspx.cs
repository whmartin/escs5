using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.Data;

public partial class Page_SupervisionGruposApoyoContratos_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    SIAService vRuboService = new SIAService();
    List<Regional> lstRegionales;
    Usuario usuario;
    System.Drawing.Color vColorDesactivado = System.Drawing.Color.FromArgb(235, 235, 228);

    string PageName = "Contratos/SupervisionContrato";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        System.Text.StringBuilder vConfirmacion = new System.Text.StringBuilder();
        vConfirmacion.Append("<table style=\"text-align: center; padding: 9px\">");
        vConfirmacion.Append("<tr>");
        vConfirmacion.Append("    <td colspan=\"2\">");
        vConfirmacion.Append("        Esta seguro de asociar el (los) contrato(s) al Grupo de Apoyo a la Supervisión?</br>");
        vConfirmacion.Append("        Después de guardada no toda la información puede ser modificada");
        vConfirmacion.Append("    </td>");
        vConfirmacion.Append("</tr>");
        vConfirmacion.Append("<tr>");
        vConfirmacion.Append("    <td colspan=\"2\">");
        vConfirmacion.Append("        <br />");
        vConfirmacion.Append("    </td>");
        vConfirmacion.Append("</tr>");
        vConfirmacion.Append("<tr>");
        vConfirmacion.Append("    <td>");
        vConfirmacion.Append("        <input id=\"btnAceptar\" type=\"button\" value=\"Aceptar\" onclick=\"postback=true; $('#btnGuardar')[0].click()\" />");
        vConfirmacion.Append("    </td>");
        vConfirmacion.Append("    <td>");
        vConfirmacion.Append("        <input id=\"btnCancelar\" type=\"button\" value=\"Cancelar\" onclick=\"cerrarModal();\" />");
        vConfirmacion.Append("    </td>");
        vConfirmacion.Append("</tr>");
        vConfirmacion.Append("</table>");


        System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
        //createDiv.ID = "createDiv";
        createDiv.InnerHtml = vConfirmacion.ToString();
        createDiv.ID = "createDiv";
        this.Controls.Add(createDiv);


        SolutionPage vSolutionPage = SolutionPage.Add;
        usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            //lstSelect.Items = Request.Form[lstSelect.UniqueID];

            string vListaIdsContratos = listPre.Value;
            string[] stringSeparators = new string[] { "-" };
            string[] vIdsContratos = vListaIdsContratos.Split(stringSeparators, StringSplitOptions.None);
            int vResultado = 0;


            SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();


            if (Request.QueryString["oP"] == "E")
            {
                vSupervisionGruposApoyoContratos.IdContratosGrupo = Convert.ToInt32(hfIdContratosGrupo.Value);
                vSupervisionGruposApoyoContratos.IdGrupo = Convert.ToInt32(hfIdGrupo.Value);
                vSupervisionGruposApoyoContratos.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSupervisionGruposApoyoContratos, this.PageName, vSolutionPage);
                vResultado = vSupervisionService.ModificarSupervisionGruposApoyoContratos(vSupervisionGruposApoyoContratos);
            }
            else
            {

                DataTable vDtbContratos = new DataTable();
                vDtbContratos.Columns.Add("IdContrato");
                vDtbContratos.Columns.Add("Vigencia");
                vDtbContratos.Columns.Add("NumeroContrato");
                vDtbContratos.Columns.Add("NumeroDocumentoEntidad");
                vDtbContratos.Columns.Add("NombreEntidadContratista");
                vDtbContratos.Columns.Add("DireccionEntidadContratista");
                vDtbContratos.Columns.Add("NumeroDocumentoRepLegal");
                vDtbContratos.Columns.Add("NombreRepLegal");
                vDtbContratos.Columns.Add("NumeroDocumentoSupervisor");
                vDtbContratos.Columns.Add("NombreSupervisor");

                //Se consulta el listado de contratos de la Base de datos en base a los parametros suministrados
                List<SupervisionGruposApoyoContratos> listaContratos = consultarContratos();
                //Se filtran los id seleccionados para obtener toda la información
                List<SupervisionGruposApoyoContratos> listaContratosSeleccioandos = obtenerContrato(vIdsContratos, listaContratos);

                //Se recorren los selccionados para asignarlos a la tabla que se enviara a la base de datos
                foreach (SupervisionGruposApoyoContratos vObjContrato in listaContratosSeleccioandos)
                {
                    DataRow vDrContrato = vDtbContratos.NewRow();
                    vDrContrato["IdContrato"] = vObjContrato.IdContrato;
                    vDrContrato["Vigencia"] = ddlVigencia.SelectedValue;
                    vDrContrato["NumeroContrato"] = vObjContrato.NumeroContrato;
                    vDrContrato["NumeroDocumentoEntidad"] = vObjContrato.NumeroDocumentoEntidad;
                    vDrContrato["NombreEntidadContratista"] = vObjContrato.NombreEntidadContratista;
                    vDrContrato["DireccionEntidadContratista"] = vObjContrato.DireccionEntidadContratista;
                    vDrContrato["NumeroDocumentoRepLegal"] = vObjContrato.NumeroDocumentoRepLegal;
                    vDrContrato["NombreRepLegal"] = vObjContrato.NombreRepLegal;
                    vDrContrato["NumeroDocumentoSupervisor"] = vObjContrato.NumeroDocumentoSupervisor;
                    vDrContrato["NombreSupervisor"] = vObjContrato.NombreSupervisor;
                    vDtbContratos.Rows.Add(vDrContrato);
                }

                //Insertar Contratos relacionados con el grupo de apoyo
                vSupervisionGruposApoyoContratos.IdGrupo = Convert.ToInt32(hfIdGrupo.Value);
                vSupervisionGruposApoyoContratos.IdDireccion = Convert.ToInt32(ddlDireccion.SelectedValue);
                vSupervisionGruposApoyoContratos.IdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
                vSupervisionGruposApoyoContratos.Vigencia = Convert.ToInt32(ddlVigencia.SelectedValue);
                vSupervisionGruposApoyoContratos.Estado = 1;

                vSupervisionGruposApoyoContratos.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSupervisionGruposApoyoContratos, this.PageName, vSolutionPage);
                vResultado = vSupervisionService.InsertarSupervisionGruposApoyoContratos(vSupervisionGruposApoyoContratos, vDtbContratos);
            }

            if (vSupervisionGruposApoyoContratos.MensajeError == "@OK")//Se valida si el mensaje de respuesta es @OK, si es así el proceso fue exitoso
            {
                toolBar.MostrarMensajeGuardado();
            }
            else if (vSupervisionGruposApoyoContratos.MensajeError.Contains("contrato") == true)//Si el mensaje tiene la palabra contrato, es porque se presento una clave duplicada de contrato y estado
            {
                toolBar.MostrarMensajeError(vSupervisionGruposApoyoContratos.MensajeError);
            }
            else
            { toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor."); }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Contratos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {

            //int vIdContratosGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoContratos.IdContratosGrupo"));
            //RemoveSessionParameter("SupervisionGruposApoyoContratos.Id");
            //int vIdGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoContratos.IdGrupo"));
            //RemoveSessionParameter("SupervisionGruposApoyoContratos.Id");
            //int vIdContrato = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyoContratos.IdContrato"));
            //RemoveSessionParameter("SupervisionGruposApoyoContratos.Id");

            //SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();
            //vSupervisionGruposApoyo = vSupervisionService.ConsultarSupervisionGruposApoyo(vIdGrupo);
            //ddlDireccion.SelectedValue = vSupervisionGruposApoyo.IdDireccion.ToString();

            //SupervisionGruposApoyoContratos vSupervisionGruposApoyoContratos = new SupervisionGruposApoyoContratos();
            //vSupervisionGruposApoyoContratos = vSupervisionService.ConsultarSupervisionGruposApoyoContratos(vIdContratosGrupo, vIdGrupo, vIdContrato);
            //hfIdContratosGrupo.Value = vSupervisionGruposApoyoContratos.IdContratosGrupo.ToString();
            //hfIdGrupo.Value = vSupervisionGruposApoyoContratos.IdGrupo.ToString();
            ////txtGrupo.Text = vSupervisionGruposApoyoContratos.Grupo;
            ////hfIdContrato.Value = vSupervisionGruposApoyoContratos.IdContrato.ToString();
            //ddlIdRegional.SelectedValue = vSupervisionGruposApoyoContratos.IdRegional.ToString();
            //txtGrupo.Text = vSupervisionGruposApoyoContratos.Nombre;
            ////rblEstado.SelectedValue = vSupervisionGruposApoyoContratos.Estado.ToString();
            //((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionGruposApoyoContratos.UsuarioCrea, vSupervisionGruposApoyoContratos.FechaCrea, vSupervisionGruposApoyoContratos.UsuarioModifica, vSupervisionGruposApoyoContratos.FechaModifica);

            ddlIdRegional.SelectedValue = GetSessionParameter("SupervisionGruposApoyoContratos.Regional").ToString();

            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            hfIdGrupo.Value = GruposSupervision[1];
            txtGrupo.Enabled = false;

            ddlDireccion.SelectedValue = GruposSupervision[2];

            //Se les asigna color grside fondo a los controles para que tengan apariencia inactiva.
            ddlDireccion.BackColor = vColorDesactivado;

            lstSelect.DataSource = vSupervisionService.ConsultarSupervisionGruposApoyoContratoss(null, Convert.ToInt32(hfIdGrupo.Value), Convert.ToInt32(GruposSupervision[2]), Convert.ToInt32(ddlIdRegional.SelectedValue), Convert.ToInt32(ddlVigencia.SelectedValue), 1);
            lstSelect.DataValueField = "idContratosGrupo";
            lstSelect.DataTextField = "NumeroContrato";
            lstSelect.DataBind();

            lstMain.DataSource = consultarContratos();
            lstMain.DataValueField = "idContrato";
            lstMain.DataTextField = "NumeroContrato";
            lstMain.DataBind();


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlDireccion, "-1", true);

            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
            lstRegionales = vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null);
            ManejoControlesSupervision.LlenarComboLista(ddlIdRegional, lstRegionales, "IdRegional", "NombreRegional");

            //Se llena el combo de vigencias a partir del 2016, que es el año del presente desarrollo
            for (int i = 2016; i <= DateTime.Now.Year; i++)
            {
                ddlVigencia.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            List<string> GruposSupervision = new List<string>();
            GruposSupervision = (List<string>)Session["GruposSupervision"];
            txtGrupo.Text = GruposSupervision[0];
            hfIdGrupo.Value = GruposSupervision[1];
            ddlDireccion.SelectedValue = GruposSupervision[2];
            txtGrupo.Enabled = false;

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlVigencia_SelectedIndexChanged(object sender, EventArgs e)
    {

        lstMain.DataSource = consultarContratos();
        lstMain.DataValueField = "idContrato";
        lstMain.DataTextField = "NumeroContrato";
        lstMain.DataBind();
    }

    protected void ddlIdRegional_SelectedIndexChanged(object sender, EventArgs e)
    {
        lstMain.DataSource = consultarContratos();
        lstMain.DataValueField = "idContrato";
        lstMain.DataTextField = "NumeroContrato";
        lstMain.DataBind();
    }

    //Función para consultar los contratos dependiendo de la dirección seleccionada
    private List<SupervisionGruposApoyoContratos> consultarContratos()
    {
        List<SupervisionGruposApoyoContratos> listaContratos = new List<SupervisionGruposApoyoContratos>();

        //Se verifica que la vigencia y la regional sean valores validos
        if ((ddlVigencia.SelectedValue != "-1") && (ddlIdRegional.SelectedValue != "-1"))
        {
            string vCodRegional = obtenerRegional(Convert.ToInt32(ddlIdRegional.SelectedValue));

            if (ddlDireccion.SelectedValue == "5") //Se vaslida si la dirección seleccionada es protección
            {
                listaContratos = vSupervisionService.ConsultarContratosSim(Convert.ToInt32(ddlVigencia.SelectedValue), vCodRegional);
            }
            else
            {
                listaContratos = vSupervisionService.ConsultarContratosCuentame(Convert.ToInt32(ddlVigencia.SelectedValue), vCodRegional, Convert.ToInt16(ddlDireccion.SelectedValue));
            }
        }

        return listaContratos;
    }

    //Función para obtener el código de la regional en base al id suministrado
    private string obtenerRegional(int pIdRegional)
    {

        Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
        lstRegionales = vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null);
        var vCodigoRegional = from resp in lstRegionales
                              where resp.IdRegional == pIdRegional
                              select resp.CodigoRegional;

        return vCodigoRegional.ToList()[0].ToString();

    }

    //Función para obtener los datos de un contrato en base a su id
    private List<SupervisionGruposApoyoContratos> obtenerContrato(string[] pIdsContratos, List<SupervisionGruposApoyoContratos> pListaContratos)
    {
        List<SupervisionGruposApoyoContratos> vObjListContratos = new List<SupervisionGruposApoyoContratos>();
        foreach (string vContrato in pIdsContratos)
        {

            SupervisionGruposApoyoContratos vobjCon = pListaContratos.Find(x => x.IdContrato == Convert.ToInt64(vContrato));

            var vDatosContrato = from resp in pListaContratos
                                 where resp.IdContrato == Convert.ToInt64(vContrato)
                                 select (SupervisionGruposApoyoContratos)resp;

            if (vDatosContrato.Count() > 0)//Se valida que la consulta retorne un registro antes de agregar a la lista.
            {
                vObjListContratos.Add(vobjCon);
            }
        }

        return vObjListContratos;
    }

}
