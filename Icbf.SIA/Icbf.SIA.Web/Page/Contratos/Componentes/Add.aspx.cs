using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;

public partial class Page_Adiciones_Add : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Componentes";
    ContratoService vContratoService = new ContratoService();
    
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado = 0;
            Componentes _Componente = new Componentes();

            _Componente.IdComponente = 0;
            _Componente.Nombre = txtNombreComponente.Text;
            _Componente.Estado = Convert.ToInt16(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
                //_Componente.IdTipoAmparo = Convert.ToInt32(hfIdTipoAmparo.Value);
                //_Componente.UsuarioModifica = GetSessionUser().NombreUsuario;
                //InformacionAudioria(vTipoAmparo, this.PageName, vSolutionPage);
                //vResultado = vContratoService.ConsultarComponentes(vTipoAmparo);
            }
            else
            {
                vResultado = vContratoService.InsertarComponente(_Componente);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Componente.IdComponente", _Componente.IdComponente);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("Componente.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    NavigateTo(SolutionPage.List);
                }

            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Amparo", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
             int IdComponente = Convert.ToInt32(GetSessionParameter("Componentes.IdComponente"));
            
            if (int.TryParse(GetSessionParameter("Componentes.IdComponente").ToString(), out IdComponente))
            {
                Componentes _Entidad = new Componentes();
                _Entidad = vContratoService.ConsultarComponentes(IdComponente);
                txtNombreComponente.Text = _Entidad.Nombre;
                rblEstado.SelectedValue = _Entidad.Estado.ToString();
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            ///*Coloque aqui el codigo de llenar el combo.*/
            //ContratoService objContratoServices = new ContratoService();
            //ddlIdTipoGarantia.DataSource = objContratoServices.ConsultarTipoGarantias(null, true);
            //ddlIdTipoGarantia.DataTextField = "NombreTipoGarantia";
            //ddlIdTipoGarantia.DataValueField = "IdTipoGarantia";
            //ddlIdTipoGarantia.DataBind();
            //ManejoControlesContratos.InicializarCombo(ddlIdTipoGarantia);
            ///*Coloque aqui el codigo de llenar el combo.*/
            //rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            //rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            //rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}