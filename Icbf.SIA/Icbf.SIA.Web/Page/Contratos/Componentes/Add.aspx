<%@ Page Title="Componentes" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Adiciones_Add" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" width="50%">
                Nombre del
                Componente *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreTipoAmparo" ControlToValidate="txtNombreComponente"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td width="50%">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreComponente" Height="22px" 
                    MaxLength="128" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreTipoAmparo" runat="server" TargetControlID="txtNombreComponente"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td valign="top">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                    </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 339px;
        }
    </style>
</asp:Content>

