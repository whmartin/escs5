using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Adiciones_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contrato/Componentes";
    ContratoService vContratoService = new ContratoService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
            if (!Page.IsPostBack)
            {
                CargarDatos();
            }
        //}
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Componentes.IdComponente", hfIdAdicion.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int IdComponente = Convert.ToInt32(GetSessionParameter("Componentes.IdComponente"));
            RemoveSessionParameter("Componentes.IdComponente");

            if (GetSessionParameter("Componentes.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Componentes");

            Componentes _Entidad = new Componentes();
            _Entidad = vContratoService.ConsultarComponentes(IdComponente);
            txbIdComponente.Text = _Entidad.IdComponente.ToString();
            txtNombre.Text = _Entidad.Nombre;
            rblEstado.SelectedValue = _Entidad.Estado.ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {   
            Componentes _Entidad = new Componentes();
            _Entidad.IdComponente = Convert.ToInt16(txbIdComponente.Text);
            int vResultado = vContratoService.EliminarComponente(_Entidad);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                //SetSessionParameter("Adiciones.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            } 
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Adiciones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }    
}
