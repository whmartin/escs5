<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Adiciones_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id Componente *
            </td>
            <td>
                Componente *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txbIdComponente"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombre"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                    </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
