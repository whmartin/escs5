using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Adiciones_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Componentes";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    private void Buscar()
    {
        try
        {
            int Estado = Convert.ToInt16(rblEstado.SelectedValue);
            string Componente = txtNombre.Text == String.Empty ? "%" : txtNombre.Text;

            gvComponentes.DataSource = vContratoService.ConsultarComponentes(Componente, Estado);
            gvComponentes.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvComponentes.PageSize = PageSize();
            gvComponentes.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Componentes", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvComponentes.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Componentes.IdComponente", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvComponentes_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvComponentes.SelectedRow);
    }
    protected void gvComponentes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvComponentes.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Adiciones.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Adiciones.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            
            //ddlIDDetalleConsModContractual.Items.Insert(0, new ListItem("Seleccione", "-1"));
            //ddlIDDetalleConsModContractual.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
