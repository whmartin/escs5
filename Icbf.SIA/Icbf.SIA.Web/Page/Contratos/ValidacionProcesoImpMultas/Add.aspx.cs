﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_Contratos_ValidacionProcesoImpMultas__Add : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ValidacionProcesoImpMultas";
    string TIPO_ESTRUCTURA = "ProcesoImposicionMultas";
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage) )
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
        SetSessionParameter("Contrato.IdImposicionMultas", hfIdImposicionMultas.Value);
        NavigateTo(SolutionPage.Detail);
    }

    /// <summary>
    /// 
    /// </summary>
    private void Guardar()
    {
        try
        {
            StringBuilder resultValidation = new StringBuilder();
            int vResultado = 0;
            int idProceso = Convert.ToInt32(hfIdImposicionMultas.Value);
            ImposicionMulta vImposicionMultas = vContratoService.ConsultarImposicionMultaId(idProceso).FirstOrDefault();
            vImposicionMultas.UsuarioModifica = GetSessionUser().NombreUsuario;


            if (!string.IsNullOrEmpty(txtFechaEnvioCitacion.Text))
            {
                DateTime fechaEnvioCitacion;
                if (DateTime.TryParse(txtFechaEnvioCitacion.Text, out fechaEnvioCitacion))
                {
                    var vIdConModContratual = int.Parse(hfIdConsModContractual.Value);
                    var vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);
                    var historico = vContratoService.ConsultarSolicitudesHistorico(vIdConModContratual);
                    var itemAprobado = historico.FirstOrDefault(e => e.Estado == "Aceptada");

                    if(vConsModContractual.FechaSolicitud <=  fechaEnvioCitacion)
                        vImposicionMultas.FechaEnvioProcesoSancionatorio = fechaEnvioCitacion;
                    else
                        resultValidation.AppendLine("La fecha de envio del proceso sancionatorio debe ser mayor o igual a la fecha de solicitud de la modificación.");
                    
                }
                else
                    resultValidation.AppendLine("La fecha de envio del proceso sancionatorio es incorrecta.");

                vImposicionMultas.NumeroRadicadoCitacion = txtNumeroRadicadoCitacion.Text;
            }
            else
                vImposicionMultas.FechaEnvioProcesoSancionatorio = null;

            if (!string.IsNullOrEmpty(txtFechaAudiencia.Text))
            {
                DateTime fechaCelebracionAudicencia;
                if (DateTime.TryParse(txtFechaAudiencia.Text, out fechaCelebracionAudicencia))
                    vImposicionMultas.FechaCelebracionAudiencia = fechaCelebracionAudicencia;
                else
                    resultValidation.AppendLine("La fecha de la celebración de la audiencia es incorrecta.");
            }
            else
                vImposicionMultas.FechaCelebracionAudiencia = null;

            vImposicionMultas.EstadoAudiencia = RbtnListEstados.SelectedValue.ToString();
            vImposicionMultas.MotivoEstado = txtMotivodelEstado.Text;
            vImposicionMultas.InformeEjecutivo = txtInformeEjecutivo.Text;
            vImposicionMultas.NumeroResolucion = txtNumeroResolucion.Text;

            if (!string.IsNullOrEmpty(txtFechaResolucion.Text))
            {
                DateTime fechaResolucion;
                if (DateTime.TryParse(txtFechaResolucion.Text, out fechaResolucion))
                {
                    vImposicionMultas.FechaResolucion = fechaResolucion;

                    if (vImposicionMultas.FechaCelebracionAudiencia != null)
                    {
                        if (vImposicionMultas.FechaResolucion < vImposicionMultas.FechaCelebracionAudiencia)
                            resultValidation.AppendLine("La fecha de resolución debe ser mayor o igual a la fecha de celebración de la audiencia.");
                    }
                    else
                        resultValidation.AppendLine("Existe Fecha de resolución sin fecha de celebración de audiencia.");
                }
                else
                    resultValidation.AppendLine("La fecha de la resolución es incorrecta.");
            }
            else
                vImposicionMultas.FechaResolucion = null;

            vImposicionMultas.Resuleve = txtResuelve.Text;
            vImposicionMultas.TipoResuelve = ddlTiposResuelve.SelectedValue == "-1" ? null : ddlTiposResuelve.SelectedValue;

            if (vImposicionMultas.TipoResuelve == "Sancion")
                vImposicionMultas.TipoSancion = RbtnListSancion.SelectedValue;
            else
                vImposicionMultas.TipoSancion = null;

            if (!string.IsNullOrEmpty(txtValorSancion.Text) && vImposicionMultas.TipoResuelve == "Sancion")
            {
                decimal valorSancion;
                if (decimal.TryParse(txtValorSancion.Text.Replace("$",string.Empty), out valorSancion))
                    vImposicionMultas.ValorSancion = valorSancion;
                else
                    resultValidation.AppendLine("El valor de la sanción es Incorrecto.");
            }
            else
                vImposicionMultas.ValorSancion = null;

            if (!string.IsNullOrEmpty(vImposicionMultas.TipoSancion))
                vImposicionMultas.InterponeRecurso = RbtnInterponeRecurso.SelectedValue == "1" ? true : false;
            else
                vImposicionMultas.InterponeRecurso = null;

            if (vImposicionMultas.InterponeRecurso == true)
                vImposicionMultas.DesicionRecurso = RbtnListDesicionRecurso.SelectedValue;
            else
                vImposicionMultas.DesicionRecurso = null;

            if (!string.IsNullOrEmpty(txtValorSancionModifica.Text) && vImposicionMultas.InterponeRecurso == true && vImposicionMultas.DesicionRecurso == "Modifica")
            {
                decimal valorSancionModifica;
                if (decimal.TryParse(txtValorSancionModifica.Text.Replace("$", string.Empty), out valorSancionModifica))
                    vImposicionMultas.ValorSancionModificacion = valorSancionModifica;
                else
                    resultValidation.AppendLine("El valor de la modificación de la sanción es Incorrecto.");
            }
            else
                vImposicionMultas.ValorSancionModificacion = null;


            if (!string.IsNullOrEmpty(txtFechaEjecutoriaResolucion.Text) &&  PanelFechas.Visible == true && txtFechaEjecutoriaResolucion.Visible == true)
            {
                DateTime fechaEjecResolucion;
                if (DateTime.TryParse(txtFechaEjecutoriaResolucion.Text, out fechaEjecResolucion))
                {
                    vImposicionMultas.FechaEjecutoriaResolucion = fechaEjecResolucion;

                    if (vImposicionMultas.FechaResolucion != null)
                    {
                        if(vImposicionMultas.FechaEjecutoriaResolucion < vImposicionMultas.FechaResolucion)
                            resultValidation.AppendLine("La Fecha Ejecutoría de la resolución debe ser mayor o igual de la fecha de resolución.");                            
                    }
                    else
                        resultValidation.AppendLine("Existe Fecha Ejecutoría de la resolución sin fecha de resolución.");
                }
                else
                    resultValidation.AppendLine("La fecha de la ejecutoría de la resolución es incorrecta.");
            }
            else
                vImposicionMultas.FechaEjecutoriaResolucion = null;

            if (!string.IsNullOrEmpty(txtFechaComunicadoProcuraduria.Text) && PanelFechas.Visible == true)
            {
                DateTime fechaComProcuraduria;
                if (DateTime.TryParse(txtFechaComunicadoProcuraduria.Text, out fechaComProcuraduria))
                {
                    vImposicionMultas.FechaComunicacionProcuraduria = fechaComProcuraduria;

                    if (vImposicionMultas.FechaResolucion != null)
                    {
                        if (vImposicionMultas.FechaComunicacionProcuraduria < vImposicionMultas.FechaResolucion)
                            resultValidation.AppendLine("La Fecha de comunicación debe ser mayor o igual de la fecha de resolución.");
                    }
                    else
                        resultValidation.AppendLine("Existe Fecha Comunicación a la procuraduría sin fecha de resolución.");
                }
                else
                    resultValidation.AppendLine("La fecha del comunicado a la procuraduría es incorrecta.");
            }
            else
                vImposicionMultas.FechaComunicacionProcuraduria = null;

            if (!string.IsNullOrEmpty(txtFechaPublicacionSecop.Text) && PanelFechas.Visible == true)
            {
                DateTime fechaSecop;
                if (DateTime.TryParse(txtFechaPublicacionSecop.Text, out fechaSecop))
                {
                    vImposicionMultas.FechaPublicacionSECOP = fechaSecop;

                    if (vImposicionMultas.FechaResolucion != null)
                    {
                        if (vImposicionMultas.FechaPublicacionSECOP < vImposicionMultas.FechaResolucion)
                            resultValidation.AppendLine("La Fecha de publicación en el SECOP debe ser mayor o igual de la fecha de resolución.");
                    }
                    else
                        resultValidation.AppendLine("Existe Fecha Publicación en el SECOP sin fecha de resolución.");
                }
                else
                    resultValidation.AppendLine("La fecha de publicación en el SECOP es incorrecta.");
            }

            if (!string.IsNullOrEmpty( txtFechaEnvioCamaraComercio.Text) && PanelFechas.Visible == true)
            {
                DateTime fechaCamaraComercio;
                if (DateTime.TryParse(txtFechaEnvioCamaraComercio.Text, out fechaCamaraComercio))
                {
                    vImposicionMultas.FechaEnvioCamaraComercio = fechaCamaraComercio;

                    if (vImposicionMultas.FechaResolucion != null)
                    {
                        if (vImposicionMultas.FechaEnvioCamaraComercio < vImposicionMultas.FechaResolucion)
                            resultValidation.AppendLine("La Fecha de envio a camara de comercio debe ser mayor o igual de la fecha de resolución.");
                    }
                    else
                        resultValidation.AppendLine("Existe Fecha envion a camara de comercio sin fecha de resolución.");
                }
                else
                    resultValidation.AppendLine("La fecha de envio a la camara comercio es incorrecta.");
            }

            if (resultValidation.Length != 0)
                MostrarMensaje(resultValidation.ToString());
            else
            {
                vResultado = vContratoService.ModificarImposicionMulta(vImposicionMultas);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado >= 1)
                {
                    if (hfIdConsModContractual.Value != "")
                    {
                        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
                    }
                    else
                    {
                        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                    }

                    SetSessionParameter("Contrato.IdImposicionMultas", vImposicionMultas.IdImposicionMulta);
                    SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
                    SetSessionParameter("ImposicionMultas.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="texto"></param>
    private void MostrarMensaje(string texto)
    {
        if (texto.Length > 0)
        {
            texto = texto.Replace("\n", "<br/>");
            toolBar.MostrarMensajeError(texto);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Proceso de Imposici&#243;n de multas", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.MostrarBotonNuevo(false);
            PanelArchivos.Visible = true;

            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();
            var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            int vIdConModContratual;

            vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractual.IDCosModContractual");
            hfIdConsModContractual.Value = vIdConModContratual.ToString();
            
            int vIdImposicionMultas = Convert.ToInt32(GetSessionParameter("Contrato.IdImposicionMultas"));
            RemoveSessionParameter("Contrato.IdImposicionMultas");
            hfIdImposicionMultas.Value = vIdImposicionMultas.ToString();
            ImposicionMulta vImposicionMulta = vContratoService.ConsultarImposicionMultaId(vIdImposicionMultas).First();
            txtConsideraciones.Text = vImposicionMulta.Razones;

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            var historico = vContratoService.ConsultarSolicitudesHistorico(vIdConModContratual);

            var itemAprobado = historico.FirstOrDefault(e => e.Estado == "Aceptada");

            //CalendarExtenderFechaEnvioCitacion.StartDate = itemAprobado.FechaCreacion;

            DateTime caFechaSolicitud = Convert.ToDateTime(vConsModContractual.FechaSolicitud);

            CalendarExtenderFechaEnvioCitacion.StartDate = caFechaSolicitud;

            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalizacionInicialContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdCntrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            CargarDatosInicalesProceso(vImposicionMulta);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Validaciones Manejo del Proceso

    /// <summary>
    /// 
    /// </summary>
    protected void ValidacionChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();

        StringBuilder result = new StringBuilder();
        ConsModContractual vConsModContractual = new ConsModContractual();
        vConsModContractual = vContratoService.ConsultarConsModContractual(int.Parse(hfIdConsModContractual.Value));

        if (!string.IsNullOrEmpty(txtFechaEnvioCitacion.Text) && !string.IsNullOrEmpty(txtNumeroRadicadoCitacion.Text))
        {
            DateTime fechaEnvioCitacion;

            if (!DateTime.TryParse(txtFechaEnvioCitacion.Text, out fechaEnvioCitacion) || Convert.ToDateTime(txtFechaEnvioCitacion.Text) < vConsModContractual.FechaSolicitud)
            {
                txtFechaEnvioCitacion.Text = string.Empty;
                result.AppendLine("El Valor de la Fecha de citación es Invalida");
                PanelAudiencia.Visible = false;
            }
            else
            {
                PanelAudiencia.Visible = true;
                CalendarExtenderFechaAudiencia.StartDate = Convert.ToDateTime(txtFechaEnvioCitacion.Text);
            }

        }
        else
            PanelAudiencia.Visible = false;

        MostrarMensaje(result.ToString());
    }

    protected void ValidacionChanged2(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        StringBuilder result = new StringBuilder();
        ConsModContractual vConsModContractual = new ConsModContractual();
        vConsModContractual = vContratoService.ConsultarConsModContractual(int.Parse(hfIdConsModContractual.Value));


        DateTime fechaEnvioCitacion;

        if (DateTime.TryParse(txtFechaEnvioCitacion.Text, out fechaEnvioCitacion) && Convert.ToDateTime(txtFechaEnvioCitacion.Text) >= vConsModContractual.FechaSolicitud)
        {
            PanelAudiencia.Visible = true;
            CalendarExtenderFechaAudiencia.StartDate = fechaEnvioCitacion;

            DateTime fechaAudiencia;

            if (!string.IsNullOrEmpty(txtFechaAudiencia.Text) && DateTime.TryParse(txtFechaAudiencia.Text, out fechaAudiencia) && Convert.ToDateTime(txtFechaEnvioCitacion.Text) <= Convert.ToDateTime(txtFechaAudiencia.Text))
            {
                lblInformeEjecutivo.Visible = true;
                txtInformeEjecutivo.Visible = true;

                if (RbtnListEstados.SelectedValue == "Terminado")
                {
                    lblMotivoEstado.Visible = false;
                    txtMotivodelEstado.Visible = false;
                }
                else if (RbtnListEstados.SelectedValue == "Reprogramado")
                {
                    lblMotivoEstado.Visible = true;
                    txtMotivodelEstado.Visible = true;
                }
                else
                {
                    lblMotivoEstado.Visible = false;
                    txtMotivodelEstado.Visible = false;
                }
            }
            else
            {
                txtFechaAudiencia.Text = string.Empty;
                lblInformeEjecutivo.Visible = false;
                txtInformeEjecutivo.Visible = false;
                result.AppendLine("El Valor de la Fecha de audiencía es Invalida");
            }
        }
        else
        {
            txtFechaEnvioCitacion.Text = string.Empty;
            result.AppendLine("El Valor de la Fecha de citación es Invalida");
            PanelAudiencia.Visible = false;
        }


        MostrarMensaje(result.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ValidacionChangedResolucion(object sender, EventArgs e)
    {
        StringBuilder result = new StringBuilder();

        DateTime fechaResolucion;

        if ( 
             !string.IsNullOrEmpty(txtFechaResolucion.Text) && 
             DateTime.TryParse(txtFechaResolucion.Text, out fechaResolucion) && 
             ! string.IsNullOrEmpty(txtNumeroResolucion.Text)
            )
        {
            ddlTiposResuelve.Visible = true;

            if (ddlTiposResuelve.SelectedValue == "Sancion")
                ValidacionSancion(fechaResolucion);
            else
                ValidacionArchivoProceso(fechaResolucion);
        }
        else
        {
            ddlTiposResuelve.Visible = false;
            PanelFechas.Visible = false;
            PanelRecurso.Visible = false;
            result.AppendLine("El número y/o la fecha de la resolución son incorrectos.");
        }

        MostrarMensaje(result.ToString());
    }

    /// <summary>
    /// Realiza la validación cuando es una sanción.
    /// </summary>
    protected void ValidacionSancion(DateTime fechaResolucion)
    {
        HabilitarSancion(true);

        if (RbtnListSancion.SelectedIndex >= 0)
        {
            HabilitarFechaEjecutoria(true);
            HabilitarInterponeRecurso(true);
            ActualizarFechaInicioFechasProceso(fechaResolucion);

            if (RbtnInterponeRecurso.SelectedValue == "1")
            {
                PanelRecurso.Visible = true;
                PanelFechas.Visible = true;

                if (RbtnListDesicionRecurso.SelectedValue == "Modifica")
                {
                    HabilitarValorModificaSancion(true);
                    lblValorSancionRevoca.Visible = false;;
                }
                else if (RbtnListDesicionRecurso.SelectedValue == "Revoca" || RbtnListDesicionRecurso.SelectedValue == "Confirma")
                {
                    HabilitarValorModificaSancion(false);

                    if (RbtnListDesicionRecurso.SelectedValue == "Revoca")
                        lblValorSancionRevoca.Visible = true;
                    else
                        lblValorSancionRevoca.Visible = false;
                }
                else
                    PanelFechas.Visible = false;
            }
            else if (RbtnInterponeRecurso.SelectedValue == "0")
            {
                PanelFechas.Visible = true;
                PanelRecurso.Visible = false;
            }
            else
            {
                PanelFechas.Visible = false;
                PanelRecurso.Visible = false;
            }
        }
        else
        {
            PanelFechas.Visible = false;
            HabilitarInterponeRecurso(false);
        }
    }

    /// <summary>
    /// Realiza la validación cuando es un archivo de proceso o no se selecciona ninguna opción.
    /// </summary>
    protected void ValidacionArchivoProceso(DateTime fechaResolucion)
    {
        if (ddlTiposResuelve.SelectedValue == "-1")
        {
            PanelFechas.Visible = false;
            txtResuelve.Visible = false;
        }
        else
        {
            PanelFechas.Visible = true;
            txtResuelve.Visible = true;
        }

        HabilitarInterponeRecurso(false);
        HabilitarSancion(false);
        HabilitarFechaEjecutoria(false);
                    
    }

    #endregion 

    #region Validaciones  Cargar datos

    /// <summary>
    /// 
    /// </summary>
    private  void CargarDatosInicalesProceso(ImposicionMulta item)
    {
        if(item.FechaEnvioProcesoSancionatorio.HasValue && ! string.IsNullOrEmpty(item.NumeroRadicadoCitacion))
        {
            var items = vContratoService.ConsultarHistoricoProcesoImpMultas(item.IdImposicionMulta);

            if (items != null && items.Count > 0)
                imgImpMultas.Visible = true;
            

            PanelAudiencia.Visible = true;
            CalendarExtenderFechaAudiencia.StartDate = item.FechaEnvioProcesoSancionatorio.Value;
            txtFechaEnvioCitacion.Text = item.FechaEnvioProcesoSancionatorio.Value.ToShortDateString();
            txtNumeroRadicadoCitacion.Text = item.NumeroRadicadoCitacion;

            if (item.FechaCelebracionAudiencia.HasValue)
            {
                txtFechaAudiencia.Text = item.FechaCelebracionAudiencia.Value.ToShortDateString();
                txtMotivodelEstado.Text = item.MotivoEstado;
                lblInformeEjecutivo.Visible = true;
                txtInformeEjecutivo.Visible = true;
                txtInformeEjecutivo.Text = item.InformeEjecutivo;

                if (!string.IsNullOrEmpty(item.EstadoAudiencia))
                    RbtnListEstados.SelectedValue = item.EstadoAudiencia;

                if (item.EstadoAudiencia == "Terminado")
                    CargarDatosInicialesTerminado(item);
                else if (item.EstadoAudiencia == "Reprogramado")
                {
                    lblMotivoEstado.Visible = true;
                    txtMotivodelEstado.Visible = true;
                    RequiredFieldValidatorMotivoEstado.Enabled = true;
                    RequiredFieldValidatorMotivoEstado.Visible = true;
                }
                else
                {
                    lblMotivoEstado.Visible = false;
                    txtMotivodelEstado.Visible = false;
                    RequiredFieldValidatorMotivoEstado.Enabled = false;
                    RequiredFieldValidatorMotivoEstado.Visible = false;
                }
            }
            else
            {
                lblInformeEjecutivo.Visible = false;
                txtInformeEjecutivo.Visible = false;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    private void CargarDatosInicialesTerminado(ImposicionMulta item)
    {
        HabilitarTerminado();

        txtFechaResolucion.Text = item.FechaResolucion.HasValue ? item.FechaResolucion.Value.ToShortDateString() : string.Empty;
        CalendarExtenderFechaResolucion.StartDate = item.FechaCelebracionAudiencia;                   
        txtNumeroResolucion.Text = item.NumeroResolucion;

        if (!string.IsNullOrEmpty(txtFechaResolucion.Text) && !string.IsNullOrEmpty(txtNumeroResolucion.Text))
        {
            ddlTiposResuelve.Visible = true;
            lblResuelve.Visible = true;

            txtResuelve.Text = item.Resuleve;
            ddlTiposResuelve.SelectedValue = item.TipoResuelve;

            if (ddlTiposResuelve.SelectedValue == "-1")
                txtResuelve.Visible = false;
            else
                txtResuelve.Visible = true;

            if (item.TipoResuelve == "Sancion")
                CargarDatosInicialesSancion(item);
            else
                CargarDatosInicialesArchivoProceso(item);
        }
        else
        {
            ddlTiposResuelve.Visible = false;
            txtResuelve.Visible = false;
            lblResuelve.Visible = false;
            HabilitarInterponeRecurso(false);
            HabilitarFechaEjecutoria(false);
            HabilitarSancion(false);
        }
    }

    /// <summary>
    /// Carga los datos iniciales de una sanción.
    /// </summary>
    /// <param name="item"></param>
    private void CargarDatosInicialesSancion(ImposicionMulta item)
    {
        HabilitarSancion(true);
        txtValorSancion.Text = item.ValorSancion.HasValue ? string.Format("{0:$#,##0}", item.ValorSancion.Value) : string.Empty;

        if (item.InterponeRecurso.HasValue)
            RbtnInterponeRecurso.SelectedValue = item.InterponeRecurso.Value ? "1" : "0";

        if (!string.IsNullOrEmpty(item.TipoSancion))
        {
            RbtnListSancion.SelectedValue = item.TipoSancion;
            HabilitarFechaEjecutoria(true);
            HabilitarInterponeRecurso(true);
            txtFechaComunicadoProcuraduria.Text = item.FechaComunicacionProcuraduria.HasValue ? item.FechaComunicacionProcuraduria.Value.ToShortDateString() : string.Empty;
            txtFechaEnvioCamaraComercio.Text = item.FechaEnvioCamaraComercio.HasValue ? item.FechaEnvioCamaraComercio.Value.ToShortDateString() : string.Empty;
            txtFechaPublicacionSecop.Text = item.FechaPublicacionSECOP.HasValue ? item.FechaPublicacionSECOP.Value.ToShortDateString() : string.Empty;
            txtFechaEjecutoriaResolucion.Text = item.FechaEjecutoriaResolucion.HasValue ? item.FechaEjecutoriaResolucion.Value.ToShortDateString() : string.Empty;

            if (item.InterponeRecurso.HasValue && item.InterponeRecurso.Value)
            {
                PanelRecurso.Visible = true;
                PanelFechas.Visible = true;

                RbtnListDesicionRecurso.SelectedValue = item.DesicionRecurso;
                ActualizarFechaInicioFechasProceso(item.FechaResolucion.Value);

                if (item.DesicionRecurso == "Modifica")
                {
                    HabilitarValorModificaSancion(true);
                    txtValorSancionModifica.Text = item.ValorSancionModificacion.HasValue ? string.Format("{0:$#,##0}", item.ValorSancionModificacion.Value) : string.Empty;
                    lblValorSancionRevoca.Visible = false;
                }
                else if (item.DesicionRecurso == "Revoca" || item.DesicionRecurso == "Confirma")
                {
                    HabilitarValorModificaSancion(false);

                    if (item.DesicionRecurso == "Revoca")
                        lblValorSancionRevoca.Visible = true;
                    else
                        lblValorSancionRevoca.Visible = false;
                }
                else
                    PanelFechas.Visible = false;
            }
            else
            {
                if (!item.InterponeRecurso.HasValue)
                    PanelFechas.Visible = false;
                else
                    PanelFechas.Visible = true;

                PanelRecurso.Visible = false;
            }
        }
        else
        {
            HabilitarInterponeRecurso(false);
            PanelFechas.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    private void CargarDatosInicialesArchivoProceso(ImposicionMulta item)
    {
        if (!string.IsNullOrEmpty(item.TipoResuelve) && item.TipoResuelve != "-1")
        {
            PanelFechas.Visible = true;
            txtResuelve.Visible = true;
            ActualizarFechaInicioFechasProceso(item.FechaResolucion.Value);
            txtFechaComunicadoProcuraduria.Text = item.FechaComunicacionProcuraduria.HasValue ? item.FechaComunicacionProcuraduria.Value.ToShortDateString() : string.Empty;
            txtFechaEnvioCamaraComercio.Text = item.FechaEnvioCamaraComercio.HasValue ? item.FechaEnvioCamaraComercio.Value.ToShortDateString() : string.Empty;
            txtFechaPublicacionSecop.Text = item.FechaPublicacionSECOP.HasValue ? item.FechaPublicacionSECOP.Value.ToShortDateString() : string.Empty;
        }
        else
        {
            PanelFechas.Visible = false;
            txtResuelve.Visible = false;
        }

        HabilitarInterponeRecurso(false);
        HabilitarSancion(false);
        HabilitarFechaEjecutoria(false);
    }

    #endregion

    #region Metodos Auxiliares de Manejo de Controles

    /// <summary>
    /// 
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarSancion(bool estado)
    {
        RbtnListSancion.Visible = estado;
        lblValorSancion.Visible = estado;
        txtValorSancion.Visible = estado;
        RequiredFieldValidatorValorSancion.Enabled = estado;
        RequiredFieldValidatorValorSancion.Visible = estado;
        if(estado == false)
        RbtnListSancion.SelectedIndex = -1;
    }

    /// <summary>
    /// Habilita o Deshabilita la información de la fecha ejecutoría.
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarFechaEjecutoria(bool estado)
    {
        lblFechaEjecutoriaResolucion.Visible = estado;
        txtFechaEjecutoriaResolucion.Visible = estado;
        imgFechaEjeResolucion.Visible = estado;
        RequiredFieldValidatorFechaEjecutoriaResolucion.Visible = estado;
        CompareValidatorFechaEjecutoriaResolucion.Visible = estado;
        RequiredFieldValidatorFechaEjecutoriaResolucion.Enabled = estado;
        CompareValidatorFechaEjecutoriaResolucion.Enabled = estado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarInterponeRecurso(bool estado)
    {
        RbtnInterponeRecurso.Visible = estado;
        lblInterponeRecurso.Visible = estado;
        if(estado == false)
        RbtnInterponeRecurso.SelectedValue = "0";
        lblInterponeRecurso.Visible = estado;
        PanelRecurso.Visible = estado;       
    }

    /// <summary>
    /// 
    /// </summary>
    protected void HabilitarTerminado()
    {
        txtFechaEnvioCitacion.Enabled = false;
        txtMotivodelEstado.Enabled = false;
        CalendarExtenderFechaEnvioCitacion.Enabled = false;
        CalendarExtenderFechaAudiencia.Enabled = false;
        txtFechaAudiencia.Enabled = false;
        txtNumeroRadicadoCitacion.Enabled = false;
        RbtnListEstados.Enabled = false;
        PanelResolucion.Visible = true;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarValorModificaSancion(bool estado)
    {
        txtValorSancionModifica.Visible = estado;
        lblValorSancionModifica.Visible = estado;
        RequiredFieldValidatorValorSancionModifica.Visible = estado;
        RequiredFieldValidatorValorSancionModifica.Enabled = estado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fecha"></param>
    protected void ActualizarFechaInicioFechasProceso(DateTime fecha)
    {
        CalendarExtenderFechaComunicadoProcuraduria.StartDate = fecha;
        CalendarExtenderFechaEjecutoriaResolucion.StartDate = fecha;
        CalendarExtenderFechaEnvioCamaraComercio.StartDate = fecha;
        CalendarExtenderFechaPublicacionSecop.StartDate = fecha;
    }


    #endregion

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        //toolBar.LipiarMensajeError();

        //int idContrato = Convert.ToInt32(hfIdContrato.Value);

        //FileUpload fuArchivo = FileUploadArchivoContrato;

        //if (fuArchivo.HasFile)
        //{
        //    try
        //    {
        //        ManejoControlesContratos controles = new ManejoControlesContratos();
        //        controles.CargarArchivoFTPContratos
        //            (
        //             TIPO_ESTRUCTURA,
        //             fuArchivo,
        //             idContrato,
        //             GetSessionUser().IdUsuario
        //            );

        //        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        //        gvanexos.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        toolBar.MostrarMensajeError(ex.Message);
        //    }
        //}
    }

    #endregion
    
}

