﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_Contratos_ValidacionProcesoImpMultas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ValidacionProcesoImpMultas";
    string TIPO_ESTRUCTURA = "ProcesoImposicionMultas";
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
       SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Contrato.IdImposicionMultas", hfIdImposicionMultas.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());
        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        NavigateTo(SolutionPage.Edit);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void toolBar_eventoAprobar(object sender, EventArgs e)
    {
        try
        {
            var impMulta = vContratoService.ConsultarImposicionMultaId(int.Parse(hfIdImposicionMultas.Value)).First();
            impMulta.Aprobado = true;
            vContratoService.ModificarImposicionMulta(impMulta);
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
            SetSessionParameter("Contrato.IdImposicionMultas", hfIdImposicionMultas.Value);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoAprobar += toolBar_eventoAprobar;
            toolBar.SetAprobarConfirmation("return Aprobacion()");
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Proceso de Imposici&#243;n de multas", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.MostrarBotonEditar(true);
            toolBar.MostrarBotonNuevo(false);

            if (GetSessionParameter("ImposicionMultas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("ImposicionMultas.Guardado");

            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();
            var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            int vIdConModContratual;

            vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractual.IDCosModContractual");
            hfIdConsModContractual.Value = vIdConModContratual.ToString();

            var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);

            int vIdImposicionMultas = Convert.ToInt32(GetSessionParameter("Contrato.IdImposicionMultas"));
            RemoveSessionParameter("Contrato.IdImposicionMultas");
            hfIdImposicionMultas.Value = vIdImposicionMultas.ToString();
            ImposicionMulta vImposicionMulta = vContratoService.ConsultarImposicionMultaId(vIdImposicionMultas).First();
            txtConsideraciones.Text = vImposicionMulta.Razones;

            if (vImposicionMulta.Aprobado == true)
                toolBar.MostrarBotonEditar(false);
            
            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalizacionInicialContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdCntrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdCntrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            CargarDatosInicalesProceso(vImposicionMulta);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Validaciones  Cargar datos

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatosInicalesProceso(ImposicionMulta item)
    {
        toolBar.MostrarBotonAprobar(false);

        if (item.FechaEnvioProcesoSancionatorio.HasValue && !string.IsNullOrEmpty(item.NumeroRadicadoCitacion))
        {
            var items = vContratoService.ConsultarHistoricoProcesoImpMultas(item.IdImposicionMulta);

            if (items != null && items.Count > 0)
                imgImpMultas.Visible = true;
            
            PanelAudiencia.Visible = true;
            txtFechaEnvioCitacion.Text = item.FechaEnvioProcesoSancionatorio.Value.ToShortDateString();
            txtNumeroRadicadoCitacion.Text = item.NumeroRadicadoCitacion;

            if (item.FechaCelebracionAudiencia.HasValue)
            {
                txtFechaAudiencia.Text = item.FechaCelebracionAudiencia.Value.ToShortDateString();
                txtMotivodelEstado.Text = item.MotivoEstado;
                lblInformeEjecutivo.Visible = true;
                txtInformeEjecutivo.Visible = true;
                txtInformeEjecutivo.Text = item.InformeEjecutivo;

                if (!string.IsNullOrEmpty(item.EstadoAudiencia))
                    RbtnListEstados.SelectedValue = item.EstadoAudiencia;

                if (item.EstadoAudiencia == "Terminado")
                    CargarDatosInicialesTerminado(item);
                else if (item.EstadoAudiencia == "Reprogramado")
                {
                    lblMotivoEstado.Visible = true;
                    txtMotivodelEstado.Visible = true;
                }
                else
                {
                    lblMotivoEstado.Visible = false;
                    txtMotivodelEstado.Visible = false;
                }
            }
            else
            {
                lblInformeEjecutivo.Visible = false;
                txtInformeEjecutivo.Visible = false;
            }
        }

        if (item.Aprobado == true)
            toolBar.MostrarBotonAprobar(false);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    private void CargarDatosInicialesTerminado(ImposicionMulta item)
    {
        HabilitarTerminado();

        txtFechaResolucion.Text = item.FechaResolucion.HasValue ? item.FechaResolucion.Value.ToShortDateString() : string.Empty;
        txtNumeroResolucion.Text = item.NumeroResolucion;

        if (!string.IsNullOrEmpty(txtFechaResolucion.Text) && !string.IsNullOrEmpty(txtNumeroResolucion.Text))
        {
            ddlTiposResuelve.Visible = true;
            lblResuelve.Visible = true;

            txtResuelve.Text = item.Resuleve;
            ddlTiposResuelve.SelectedValue = item.TipoResuelve;

            if (ddlTiposResuelve.SelectedValue == "-1")
                txtResuelve.Visible = false;
            else
                txtResuelve.Visible = true;

            if (item.TipoResuelve == "Sancion")
                CargarDatosInicialesSancion(item);
            else
                CargarDatosInicialesArchivoProceso(item);
        }
        else
        {
            ddlTiposResuelve.Visible = false;
            txtResuelve.Visible = false;
            lblResuelve.Visible = false;
            HabilitarInterponeRecurso(false);
            HabilitarFechaEjecutoria(false);
            HabilitarSancion(false);
        }
    }

    /// <summary>
    /// Carga los datos iniciales de una sanción.
    /// </summary>
    /// <param name="item"></param>
    private void CargarDatosInicialesSancion(ImposicionMulta item)
    {
        HabilitarSancion(true);
        txtValorSancion.Text = item.ValorSancion.HasValue ? string.Format("{0:$#,##0}", item.ValorSancion.Value) : string.Empty;

        if (item.InterponeRecurso.HasValue)
            RbtnInterponeRecurso.SelectedValue = item.InterponeRecurso.Value ? "1" : "0";

        if (!string.IsNullOrEmpty(item.TipoSancion))
        {
            RbtnListSancion.SelectedValue = item.TipoSancion;
            HabilitarFechaEjecutoria(true);
            HabilitarInterponeRecurso(true);
            txtFechaComunicadoProcuraduria.Text = item.FechaComunicacionProcuraduria.HasValue ? item.FechaComunicacionProcuraduria.Value.ToShortDateString() : string.Empty;
            txtFechaEnvioCamaraComercio.Text = item.FechaEnvioCamaraComercio.HasValue ? item.FechaEnvioCamaraComercio.Value.ToShortDateString() : string.Empty;
            txtFechaPublicacionSecop.Text = item.FechaPublicacionSECOP.HasValue ? item.FechaPublicacionSECOP.Value.ToShortDateString() : string.Empty;
            txtFechaEjecutoriaResolucion.Text = item.FechaEjecutoriaResolucion.HasValue ? item.FechaEjecutoriaResolucion.Value.ToShortDateString() : string.Empty;

            if (item.InterponeRecurso.HasValue && item.InterponeRecurso.Value)
            {
                PanelRecurso.Visible = true;
                PanelFechas.Visible = true;
                toolBar.MostrarBotonAprobar(true);
                RbtnListDesicionRecurso.SelectedValue = item.DesicionRecurso;

                if (item.DesicionRecurso == "Modifica")
                {
                    HabilitarValorModificaSancion(true);
                    txtValorSancionModifica.Text = item.ValorSancionModificacion.HasValue ? string.Format("{0:$#,##0}", item.ValorSancionModificacion.Value) : string.Empty;
                    lblValorSancionRevoca.Visible = false;
                }
                else if (item.DesicionRecurso == "Revoca" || item.DesicionRecurso == "Confirma")
                {
                    HabilitarValorModificaSancion(false);

                    if (item.DesicionRecurso == "Revoca")
                        lblValorSancionRevoca.Visible = true;
                    else
                        lblValorSancionRevoca.Visible = false;
                }
                else
                {
                    PanelFechas.Visible = false;
                    toolBar.MostrarBotonAprobar(false);
                }
            }
            else
            {
                if (!item.InterponeRecurso.HasValue)
                    PanelFechas.Visible = false;
                else
                {
                    PanelFechas.Visible = true;
                    toolBar.MostrarBotonAprobar(true);
                }

                PanelRecurso.Visible = false;
            }
        }
        else
        {
            HabilitarInterponeRecurso(false);
            PanelFechas.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    private void CargarDatosInicialesArchivoProceso(ImposicionMulta item)
    {
        toolBar.MostrarBotonAprobar(true);

        if (!string.IsNullOrEmpty(item.TipoResuelve) && item.TipoResuelve != "-1")
        {
            PanelFechas.Visible = true;
            txtResuelve.Visible = true;;
            txtFechaComunicadoProcuraduria.Text = item.FechaComunicacionProcuraduria.HasValue ? item.FechaComunicacionProcuraduria.Value.ToShortDateString() : string.Empty;
            txtFechaEnvioCamaraComercio.Text = item.FechaEnvioCamaraComercio.HasValue ? item.FechaEnvioCamaraComercio.Value.ToShortDateString() : string.Empty;
            txtFechaPublicacionSecop.Text = item.FechaPublicacionSECOP.HasValue ? item.FechaPublicacionSECOP.Value.ToShortDateString() : string.Empty;
        }
        else
        {
            PanelFechas.Visible = false;
            txtResuelve.Visible = false;
        }

        HabilitarInterponeRecurso(false);
        HabilitarSancion(false);
        HabilitarFechaEjecutoria(false);
    }

    #endregion

    #region Metodos Auxiliares de Manejo de Controles

    /// <summary>
    /// 
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarSancion(bool estado)
    {
        RbtnListSancion.Visible = estado;
        lblValorSancion.Visible = estado;
        txtValorSancion.Visible = estado;
        if (estado == false)
            RbtnListSancion.SelectedIndex = -1;
    }

    /// <summary>
    /// Habilita o Deshabilita la información de la fecha ejecutoría.
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarFechaEjecutoria(bool estado)
    {
        lblFechaEjecutoriaResolucion.Visible = estado;
        txtFechaEjecutoriaResolucion.Visible = estado;
        imgFechaEjeResolucion.Visible = estado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarInterponeRecurso(bool estado)
    {
        RbtnInterponeRecurso.Visible = estado;
        lblInterponeRecurso.Visible = estado;
        if (estado == false)
            RbtnInterponeRecurso.SelectedValue = "0";
        lblInterponeRecurso.Visible = estado;
        PanelRecurso.Visible = estado;
    }

    /// <summary>
    /// 
    /// </summary>
    protected void HabilitarTerminado()
    {
        txtFechaEnvioCitacion.Enabled = false;
        txtMotivodelEstado.Enabled = false;
        CalendarExtenderFechaEnvioCitacion.Enabled = false;
        txtFechaAudiencia.Enabled = false;
        txtNumeroRadicadoCitacion.Enabled = false;
        RbtnListEstados.Enabled = false;
        PanelResolucion.Visible = true;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="estado"></param>
    protected void HabilitarValorModificaSancion(bool estado)
    {
        txtValorSancionModifica.Visible = estado;
        lblValorSancionModifica.Visible = estado;
    }

    #endregion

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
}

