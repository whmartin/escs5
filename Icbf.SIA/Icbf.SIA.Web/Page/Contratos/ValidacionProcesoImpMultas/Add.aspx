﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_ValidacionProcesoImpMultas__Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
    <asp:HiddenField ID="hfIdConsModContractual" runat="server"  />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdImposicionMultas" runat="server" />


    
    <script type="text/javascript">

        function GetDetalleImpMultas() {

            var idImposicionMultas = document.getElementById('<%= hfIdImposicionMultas.ClientID %>');

           
            var url = '../../../Page/Contratos/LupasModificaciones/LupasProcesoImpMultas.aspx?idMulta=' + idImposicionMultas.value;

            window_showModalDialog(url, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');

            return false;
        }

    </script>


    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                N&uacute;mero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                  Raz&oacute;n del Presunto Incumplimiento por parte del Contratista      
            </td>
            <td class="style1" style="width: 50%">
                        
                &nbsp;</td>
        </tr>
          <tr class="rowA">
            <td colspan="3">
                <asp:TextBox runat="server" Enabled="false" ID="txtConsideraciones" TextMode="MultiLine" Height="73px" Width="95%" MaxLength="200" ></asp:TextBox>
            </td>
        </tr>       
        
        <tr class="rowB">
            <td colspan="3">
                Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="3">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>                
               <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha de envio Citación Proceso Sancionatorio
                <asp:RequiredFieldValidator ID="RvfFecha3" runat="server" ControlToValidate="txtFechaEnvioCitacion" 
                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>  
                <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaEnvioCitacion" runat="server" ID="CompareValidator6" />             
                   </td>
            <td style="width: 50%">
                <asp:Label ID="lblNumeroRadicadoCitacion" runat="server" Text="Número de Radicado de la citación" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorNumeroRadicadoCitacion" runat="server" ControlToValidate="txtNumeroRadicadoCitacion" 
                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator> 
                   </td>
        </tr>
               <tr class="rowA">
                    <td>
                        <asp:TextBox ID="txtFechaEnvioCitacion" AutoPostBack="true" OnTextChanged="ValidacionChanged" runat="server" Height="20px"></asp:TextBox>
                        <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY"
                        CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaEnvioCitacion">
                        </Ajax:MaskedEditExtender> 
                        <Ajax:CalendarExtender ID="CalendarExtenderFechaEnvioCitacion" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image6" TargetControlID="txtFechaEnvioCitacion">
                        </Ajax:CalendarExtender>
                        <asp:Image ID="Image6" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNumeroRadicadoCitacion"  AutoPostBack="true" OnTextChanged="ValidacionChanged"  ></asp:TextBox>
                    </td>
                </tr>

            <tr>
                <td colspan="2">
                    <asp:Panel ID="PanelAudiencia" Visible="false" runat="server">
                        <table width="100%" align="center">
                                <tr class="rowB">
                                            <td class="style1" style="width: 50%">
                                               <asp:Label Text="Fecha de Celebración de la Audiencia" runat="server" ID="lblFechaCelebracionAudiencia" />
                                                <asp:RequiredFieldValidator ID="RvfFecha2" runat="server" ControlToValidate="txtFechaAudiencia" 
                                                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                                                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>  
                                                <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaAudiencia" runat="server" ID="CompareValidator1" />             
                                           </td>
                                            <td class="style1" style="width: 50%">
                                                 <asp:Label Text="Informe Ejecutivo del Desarrollo de la Audiencia" Visible="false" runat="server" ID="lblInformeEjecutivo" />
                                            </td>
                                       </tr>
                                <tr class="rowA">
                                    <td class="style1">
                                        <asp:TextBox ID="txtFechaAudiencia"  AutoPostBack="true" OnTextChanged="ValidacionChanged2" runat="server"></asp:TextBox>
                                        <Ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY"
                                        CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaAudiencia">
                                        </Ajax:MaskedEditExtender> 
                                        <Ajax:CalendarExtender ID="CalendarExtenderFechaAudiencia" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaSolicitud" TargetControlID="txtFechaAudiencia">
                                        </Ajax:CalendarExtender>
<%--                                        <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaAudiencia">
                                        </Ajax:MaskedEditExtender> --%>
                                        <asp:Image ID="imgFechaSolicitud" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtInformeEjecutivo"  Visible="false"  TextMode="MultiLine"
                                             MaxLength="1000" Width="475px" Height="87px" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="style1">
                                       <asp:Label ID="lblEstadoAudiencia" runat="server" Text="Estado de la Audiencia" />  &nbsp;<asp:ImageButton ID="imgImpMultas"  runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif" OnClientClick="GetDetalleImpMultas(); return false;" Style="cursor: hand" ToolTip="Buscar" Visible="false" />
                                    </td>
                                    <td>
                                       <asp:Label ID="lblMotivoEstado" runat="server" Text="Motivo del Estado" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorMotivoEstado" runat="server" Enabled="false" Visible="false" ControlToValidate="txtMotivodelEstado" Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr >
                                    <td>
                                        <asp:RadioButtonList ID="RbtnListEstados" AutoPostBack="true" Width="300px" runat="server" OnSelectedIndexChanged="ValidacionChanged">
                                            <asp:ListItem  Text="Reprogramado" Value="Reprogramado" />
                                            <asp:ListItem Text="Terminado" Value="Terminado" />
                                       </asp:RadioButtonList> 
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMotivodelEstado"   TextMode="MultiLine"
                                             MaxLength="128" Width="475px" Height="87px" ></asp:TextBox>
                                    </td>
                                </tr>
                        </table>
                     </asp:Panel>
                </td>
            </tr>

                <tr>
                <td colspan="2">
                    <asp:Panel ID="PanelResolucion" Visible="false" runat="server">
                        <table width="100%" align="center">
        <tr class="rowB" >
            <td>
                <asp:Label ID="lblNúmeroResolucion" runat="server" Text="Número de Resolución" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNumeroResolucion" 
                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator> 
            </td>
            <td>
                <asp:Label ID="lblFechaResolucion" runat="server" Text="Fecha de Resolución" />         
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorFechaResolucion" runat="server" ControlToValidate="txtFechaResolucion" 
                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>  
                <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaResolucion" runat="server" ID="CompareValidatorFechaResolucion" />     
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:TextBox runat="server" ID="txtNumeroResolucion"  AutoPostBack="true" OnTextChanged="ValidacionChangedResolucion"  ></asp:TextBox>
               <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNumeroResolucion" FilterType="Numbers"  />
            </td>
            <td>
               <asp:TextBox ID="txtFechaResolucion"  AutoPostBack="true" OnTextChanged="ValidacionChangedResolucion"  runat="server"></asp:TextBox>
                <Ajax:CalendarExtender ID="CalendarExtenderFechaResolucion" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image1" TargetControlID="txtFechaResolucion">
                </Ajax:CalendarExtender>
                <asp:Image ID="Image1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
           
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:Label ID="lblResuelve" runat="server" Text="Resuelve" />  
            </td>
        </tr>
        <tr class="rowA">
            <td rowspan="6" style="width: 50%">
               <asp:TextBox runat="server" ID="txtResuelve"   TextMode="MultiLine"
             MaxLength="250" Width="475px" Height="87px" ></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddlTiposResuelve" AutoPostBack="true" OnSelectedIndexChanged="ValidacionChangedResolucion" runat="server">
                    <asp:ListItem Selected="True" Text="Seleccione" Value="-1" />
                    <asp:ListItem Text="Sanción" Value="Sancion" />
                    <asp:ListItem Text="Archivo del Procedimiento" Value="ArchivodelProcedimiento" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:RadioButtonList ID="RbtnListSancion" AutoPostBack="true" OnSelectedIndexChanged="ValidacionChangedResolucion" Visible="false" Width="300px" runat="server">
                    <asp:ListItem Text="Multa" Value="Multa" />
                    <asp:ListItem Text="Caducidad" Value="Caducidad" />
                    <asp:ListItem Text="Cláusula Penal Pecunuaría" Value="CláusulaPenalPecunuaría" />
               </asp:RadioButtonList> 
            </td>
        </tr>
        <tr class="rowB">
            <td>
                       <asp:Label ID="lblValorSancion" Visible="false" runat="server" Text="Valor Sanción" />  
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorValorSancion" runat="server" ControlToValidate="txtValorSancion" 
                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator> 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                                <asp:TextBox runat="server" Width="220px"  Visible="false"   ID="txtValorSancion" Height="20px" />
                <Ajax:FilteredTextBoxExtender ID="ftValorUnitario" runat="server" TargetControlID="txtValorSancion"
                FilterType="Numbers,Custom" ValidChars=".,$" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                       <asp:Label ID="lblInterponeRecurso" Visible="false" runat="server" Text="Interpone Recurso" />  
                                </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:RadioButtonList ID="RbtnInterponeRecurso" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ValidacionChangedResolucion" RepeatDirection="Horizontal"  runat="server">
                    <asp:ListItem Text="Si" Value="1" />
                    <asp:ListItem Text="No" Value="0" Selected="True" />
               </asp:RadioButtonList> 
            </td>
        </tr>
        </table>
                        </asp:Panel>
                    </td>
        </tr>
        <tr>
            <td colspan="2">
                    <asp:Panel runat="server" ID="PanelRecurso" Visible="false">
                     <table width="100%" align="center">
           <tr class="rowB">
                          <td class="style1" style="width: 50%">
                    <asp:Label ID="lblDesicionRecurso" runat="server" Text="Desición del Recurso" />  
             </td>
                         <td class="style1" style="width: 50%">
                             <asp:Label ID="lblValorSancionModifica" runat="server" Text="Valor Sanción Modificación" Visible="false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorValorSancionModifica" runat="server" ControlToValidate="txtValorSancionModifica" Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>
                          </td>
           </tr>
            <tr class="rowA">
                <td>

                 <asp:RadioButtonList ID="RbtnListDesicionRecurso" AutoPostBack="true" OnSelectedIndexChanged="ValidacionChangedResolucion"  Width="300px" runat="server">
                    <asp:ListItem Text="Modifica" Value="Modifica" />
                    <asp:ListItem Text="Revoca" Value="Revoca" />
                    <asp:ListItem Text="Confirma" Value="Confirma" />
               </asp:RadioButtonList> 

                </td>
                <td>
                    <asp:TextBox runat="server" Width="220px"  Visible="false"   ID="txtValorSancionModifica" Height="20px" />
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtValorSancionModifica"
                FilterType="Numbers,Custom" ValidChars=".,$" />
                    <strong> <asp:Label ID="lblValorSancionRevoca" Text="Archivo del Procedimiento" runat="server" Visible="false" /></strong>
                </td>
            </tr>
         </table>
                        </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="PanelFechas" Visible="false" runat="server">
                           <table width="100%" align="center">
                               <tr class="rowB">
                                               <td class="style1" style="width: 50%">
                <asp:Label ID="lblFechaEjecutoriaResolucion" runat="server" Text="Fecha de Ejecutoría de la Resolución" />         
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorFechaEjecutoriaResolucion" runat="server" ControlToValidate="txtFechaEjecutoriaResolucion" 
                    Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                     style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>  
                <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaEjecutoriaResolucion" runat="server" ID="CompareValidatorFechaEjecutoriaResolucion" />     
            </td>
                <td class="style1" style="width: 50%">
                    <asp:Label ID="lblFechaPublicacionSecop" runat="server" Text="Fecha de Publicación SECOP" />         
                </td>
        </tr>
                    <tr class="rowA">
                                <td>
                                    <asp:TextBox ID="txtFechaEjecutoriaResolucion"  runat="server"></asp:TextBox>
                                    <Ajax:CalendarExtender ID="CalendarExtenderFechaEjecutoriaResolucion" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image2" TargetControlID="txtFechaEjecutoriaResolucion">
                                    </Ajax:CalendarExtender>
                                    <asp:Image ID="imgFechaEjeResolucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Height="22px" />
                                </td>
                            <td>
                                    <asp:TextBox ID="txtFechaPublicacionSecop"  runat="server"></asp:TextBox>
                                    <Ajax:CalendarExtender ID="CalendarExtenderFechaPublicacionSecop" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image3" TargetControlID="txtFechaPublicacionSecop">
                                    </Ajax:CalendarExtender>
                                    <asp:Image ID="Image3" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                             </td>
                      </tr>

                      <tr class="rowB">
                              <td class="style1" style="width: 50%">
                                    <asp:Label ID="lblFechaEnvioCamaraComercio" runat="server" Text="Fecha de Envio a Camara de Comercio" />         
                             </td>
                              <td class="style1" style="width: 50%">
                                    <asp:Label ID="lblFechaComunicadoProcuraduria" runat="server" Text="Fecha de Comunicación Procuraduría" />         
                               </td>
                        </tr>
                               
                         <tr class="rowB">
                                 <td>
                                    <asp:TextBox ID="txtFechaEnvioCamaraComercio"  runat="server"></asp:TextBox>
                                    <Ajax:CalendarExtender ID="CalendarExtenderFechaEnvioCamaraComercio" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image4" TargetControlID="txtFechaEnvioCamaraComercio">
                                    </Ajax:CalendarExtender>
                                    <asp:Image ID="Image4" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                </td>
                            <td>
                                    <asp:TextBox ID="txtFechaComunicadoProcuraduria"  runat="server"></asp:TextBox>
                                    <Ajax:CalendarExtender ID="CalendarExtenderFechaComunicadoProcuraduria" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image5" TargetControlID="txtFechaComunicadoProcuraduria">
                                    </Ajax:CalendarExtender>
                                    <asp:Image ID="Image5" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                             </td>
                          </tr>


                               </table>
                </asp:Panel>
            </td>
        </tr>

        </table>


             <asp:Panel ID="PanelArchivos" runat="server" Visible="false" Width="90%">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Documentos</td>
            <td>
                &nbsp;</td>
        </tr>
<%--        <tr class="rowA">
            <td>
               <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
            </td>
            <td>
                </td>
        </tr>--%>
            <tr class="rowAG">
                    <td>
                        <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
<%--                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
                 </asp:Panel>
    



</asp:Content>
