<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_CargueValoresProgramados_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">


        <script type="text/javascript" language="javascript">


            function ValidarInfo() {
                Page_ClientValidate("btnBuscar");

                if (Page_IsValid) {
                    muestraImagenLoading();
                }
            }

            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            $(document).ready(function () {

                ocultaImagenLoading();
            });


    </script>

    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Regional
                    <asp:RequiredFieldValidator ID="rfvddlIdRegional" runat="server" ControlToValidate="ddlIdRegional" Display="Dynamic" ErrorMessage="*" Font-Bold="False" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    Vigencia&nbsp;
                    <asp:RequiredFieldValidator ID="rvddlVigencia" runat="server" ControlToValidate="ddlVigencia" Display="Dynamic" ErrorMessage="*" Font-Bold="False" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:DropDownList ID="ddlIdRegional" runat="server" >
                    </asp:DropDownList>
                </td>
                <td class="Cell">
                    <asp:DropDownList ID="ddlVigencia" runat="server" >
                                        </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Trimestre
                   <asp:RequiredFieldValidator ID="rfddlPeriodo" runat="server" ControlToValidate="ddlPeriodo" Display="Dynamic" ErrorMessage="*" Font-Bold="False" ForeColor="Red" InitialValue="-1" SetFocusOnError="true" ValidationGroup="btnBuscar"></asp:RequiredFieldValidator>
                </td>
                <td class="Cell">
                    &nbsp;</td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                <asp:DropDownList ID="ddlPeriodo" runat="server" >
                    <asp:ListItem Text="Seleccione" Value="-1" />
                    <asp:ListItem Text="PRIMERO" Value="1"/>
                    <asp:ListItem Text="SEGUNDO" Value="2"/>
                    <asp:ListItem Text="TERCERO" Value="3"/>
                    <asp:ListItem Text="CUARTO" Value="4"/>
                </asp:DropDownList>
                </td>
                <td class="Cell">
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

      
    </script>
</asp:Content>
