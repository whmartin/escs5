using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using WsContratosPacco;
using System.ServiceModel;

/// <summary>
/// Página de consulta a través de filtros para la entidad PlanComprasContratos
/// </summary>
public partial class Page_Contratos_CargueValoresProgramados_List : GeneralWeb
{
    #region Variables y Constantes

    private masterPrincipal _toolBar;
    private const string PageName = "Contratos/CargueValoresProgramados";
    private ContratoService _vContratoService = new ContratoService();
    private readonly ManejoControlesContratos _vManejoControlesContratos = new ManejoControlesContratos();

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(_toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                if (! string.IsNullOrEmpty(GetSessionParameter("CargueValoresProgramados.Actualizo").ToString()))
                {
                    _toolBar.MostrarMensajeGuardado("Se actualizo correctamente el período");
                    RemoveSessionParameter("CargueValoresProgramados.Actualizo");
                }

                CargarDatosIniciales();
            }
        }
    }

    protected void btnReporte_Click(object sender, EventArgs e)
    {
        _toolBar.LipiarMensajeError();
        CargarInformacion();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
    
    }

    #endregion

    #region Funciones y Procedimientos

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (masterPrincipal)this.Master;
            if (_toolBar != null)
            {
                _toolBar.eventoRetornar += btnRetornar_Click;
                _toolBar.eventoReporte += btnReporte_Click;
                _toolBar.SetValidationGroupBoton(masterPrincipal.Botones.Reporte, "btnBuscar");
                _toolBar.EstablecerTitulos("Actualización Valores Programados Contratos", SolutionPage.List.ToString());
                _toolBar.SetGenerarReporteConfrimation("ValidarInfo();");
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            _vManejoControlesContratos.LlenarRegionalesPlanCompras(ddlIdRegional, "NombreRegional", "CodRegional", true);

            int anioActual = DateTime.Now.Year;

            for (int i = 2017; i <= anioActual; i++)
                ddlVigencia.Items.Add(new ListItem() { Text= i.ToString(), Value =i.ToString() });
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarInformacion()
    {
        try
        {
            int idVigencia = int.Parse(ddlVigencia.SelectedValue.ToString());
            string codigoRegional = ddlIdRegional.SelectedValue.ToString();

            Icbf.Contrato.Entity.ReporteContraloraTrimestre trimeste = Icbf.Contrato.Entity.ReporteContraloraTrimestre.Primero;

            switch (ddlPeriodo.SelectedValue)
            {
                case "1":
                    trimeste = Icbf.Contrato.Entity.ReporteContraloraTrimestre.Primero;
                    break;
                case "2":
                    trimeste = Icbf.Contrato.Entity.ReporteContraloraTrimestre.Segundo;
                    break;
                case "3":
                    trimeste = Icbf.Contrato.Entity.ReporteContraloraTrimestre.Tercero;
                    break;
                case "4":
                    trimeste = Icbf.Contrato.Entity.ReporteContraloraTrimestre.Cuarto;
                    break;
                default:
                    break;
            }

           var result =  _vContratoService.ActualizarInformacionEjecucionContrato(idVigencia,codigoRegional,trimeste,GetSessionUser().NombreUsuario);

           if (result > 0)
           {
               SetSessionParameter("CargueValoresProgramados.Actualizo", "1");
               NavigateTo(SolutionPage.List);
           }
           else
               _toolBar.MostrarMensajeError("Ocurrio un error actualizando los valores o este criterío no contiene información, por favor intente de nuevo.");
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

}
