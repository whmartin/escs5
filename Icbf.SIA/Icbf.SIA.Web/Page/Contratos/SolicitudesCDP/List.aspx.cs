using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Entity;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Seguridad.Service;
using Icbf.SIA.Service;
using System.IO;
using Excel;
using System.Data;

/// <summary>
/// Página de consulta a través de filtros para la entidad Contratos
/// </summary>
public partial class Page_SolicitudesCDP_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesCDP";
    ContratoService vContratoService = new ContratoService();
    SeguridadService vSeguridadFAC = new SeguridadService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        Response.Redirect("../../General/Inicio/Default.aspx");
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonBuscar(true);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoGuardar += toolBar_eventoGuardar;
            gvTrazabilidad.PageSize = PageSize();
            gvTrazabilidad.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos(@"Cargue de Solicitudes de CDP", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoGuardar(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        toolBar.LimpiarOpcionesAdicionales();

        FileUpload fuArchivo = (FileUpload)FuploadCargarInfoCDP;

        if (fuArchivo.HasFile)
        {
            try
            {
                FileInfo finfo = new FileInfo(fuArchivo.FileName);

                string fileExt = finfo.Extension.ToLower();

                if (fileExt == ".xls" || fileExt == ".xlsx" )
                {
                    string filename = Path.GetFileName(fuArchivo.FileName);

                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(FuploadCargarInfoCDP.PostedFile.InputStream);
                    
                    excelReader.Read();

                    DataTable tablaCargue = new DataTable();
                    tablaCargue.Columns.Add("NumeroCDP",typeof(int));
                    tablaCargue.Columns.Add("FechaCreacion",typeof(string));
                    tablaCargue.Columns.Add("Dependencia",typeof(string));
                    tablaCargue.Columns.Add("DependenciaDescripcion",typeof(string));
                    tablaCargue.Columns.Add("Rubro",typeof(string));
                    tablaCargue.Columns.Add("ValorInicial",typeof(decimal));
                    tablaCargue.Columns.Add("Codigo",typeof(string));
                  
                    while (excelReader.Read())
                    {
                        DataRow row = tablaCargue.NewRow();
                        row["NumeroCDP"] = excelReader.GetInt32(0);
                        row["FechaCreacion"] = excelReader.GetString(2);
                        row["Dependencia"] = excelReader.GetString(5);
                        row["DependenciaDescripcion"] = excelReader.GetString(6);
                        row["Rubro"] = excelReader.GetString(7);
                        row["ValorInicial"] = decimal.Parse(excelReader.GetString(12).Replace(",",""));
                        row["Codigo"] = excelReader.GetString(0)+"-"+excelReader.GetString(5);
                        tablaCargue.Rows.Add(row);
                    }

                    SolicitudCDP item = new SolicitudCDP();
                    item.SolicitidesCDP = tablaCargue;

                    var resultCount =  vContratoService.InsertarLoteSolicitudesCDP(item, filename);

                    CargarGrilla(gvTrazabilidad, null, false);

                    toolBar.MostrarMensajeGuardado("Se Actualizarón las solicitudes de CDP correctamente");
                }
                else
                {
                    toolBar.MostrarMensajeError("El formato del archivo es Incorrecto");
                }
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
                return;
            }
        }
        else
        {
            toolBar.MostrarMensajeError("Debe Seleccionar un archivo");
            return;
        }
    }

    protected void gvContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTrazabilidad.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvContratos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //Lleno una lista con los datos que uso para llenar la grilla
        try
        { 
            var myGridResults = vContratoService.ObtenerTrazabilidad();
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(TrazabilidadSolicitudCDP), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<TrazabilidadSolicitudCDP, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarGrilla(gvTrazabilidad, null, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
