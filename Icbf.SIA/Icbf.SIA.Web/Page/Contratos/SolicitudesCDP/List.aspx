<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SolicitudesCDP_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Cargue de solicitudes de CDP</td>
            <td class="Cell">
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <%--<asp:TextBox runat="server" ID="txtFechaRegistroSistema" MaxLength="10" ></asp:TextBox>--%>
                <asp:FileUpload ID="FuploadCargarInfoCDP" runat="server" />
            </td>
            <td class="Cell">
                &nbsp;</td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTrazabilidad" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdCargue" CellPadding="0" Height="16px"
                        OnSorting="gvContratos_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContratos_PageIndexChanging">
                        <Columns>
                            <asp:BoundField HeaderText="Id" DataField="IdCargue"  SortExpression="IdCargue" />
                            <asp:BoundField HeaderText="Fecha Cargue" DataField="Fecha"  SortExpression="Fecha"/>
                            <asp:BoundField HeaderText="Archivo" DataField="NombreArchivo"  SortExpression="NombreArchivo"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
