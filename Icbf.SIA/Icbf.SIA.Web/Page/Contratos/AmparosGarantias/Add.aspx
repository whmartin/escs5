<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_AmparosGarantias_Add" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfFechaSuscripcion" runat="server" />
    <asp:HiddenField ID="hfFechaInicioEjecucion" runat="server" />
    <asp:HiddenField ID="hfFechaTerminacionContrato" runat="server" />
    <asp:HiddenField ID="hfIdTipoModificacion" runat="server" />
    <asp:HiddenField ID="hfValorCofinaciacion" runat="server" />
    <asp:HiddenField ID="hfValorContrato" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfPostbck" runat="server" />
    <script src="../../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function PreGuardado() {
            ConfiguraValidadores(true);
            window.Page_ClientValidate("btnGuardar");
        }

        function prePostbck(imagenLoading)
        {
            if (imagenLoading == true)
                muestraImagenLoading();
            else {
                ocultaImagenLoading();
            }
         }


        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }


        //function prePostbck() {
            
        //}

        function ConfiguraValidadores(habilitar) {
            ValidatorEnable($("#cphCont_rfvIdTipoAmparo")[0], habilitar);
            ValidatorEnable($("#cphCont_cvIdTipoAmparo")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvFechaDesde")[0], habilitar);
            ValidatorEnable($("#cphCont_cvFechaDesde")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvFechaVigenciaHasta")[0], habilitar);
            ValidatorEnable($("#cphCont_cvFechaVigenciaHasta")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvIDUnidadCalculo")[0], habilitar);
            ValidatorEnable($("#cphCont_cvIDUnidadCalculo")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvValorCalculoAsegurado")[0], habilitar);
        }
    </script>
    
    <script type="text/javascript" language="javascript">
        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false; 
            else
                return false;
        }

        function juegoValorICBF(control) {
            var chkValoreICBF = document.getElementById('<%= chkValorICBF.ClientID %>');
            var chkValorICBFAporte = document.getElementById('<%= chkValorICBFAporte.ClientID %>');
            var chkValorReg = document.getElementById('<%= chkValorReg.ClientID %>');
            var txtFechaInicia = document.getElementById('<%= txtFechaVigenciaDesde.ClientID %>');
            var txtFechaFin = document.getElementById('<%= txtFechaVigenciaHasta.ClientID %>');
            var txtValorAseg = document.getElementById('<%= txtValorAsegurado.ClientID %>');
            var txtValorCal = document.getElementById('<%=  txtValorCalculoAsegurado.ClientID %>');
            var ddlunidadCalculo = document.getElementById('<%= ddlIDUnidadCalculo.ClientID %>');
            var txtValorBase = document.getElementById('<%=  txtValorInicialContrato.ClientID %>');
            var valorCofinaciacion = document.getElementById('<%=  hfValorCofinaciacion.ClientID %>').value.replace(',', '.') * 1;
            var valorContrato = document.getElementById('<%=  hfValorContrato.ClientID %>').value.replace(',', '.') * 1;
            
            if (chkValoreICBF.checked || chkValorICBFAporte.checked || chkValorReg.checked) {               
                if (control.id == chkValorICBFAporte.id) {
                    if (confirm('El calculo del valor asegurado se realizara teniendo en cuenta la cofinanciación del Contratista'))
                    {
                        chkValorICBFAporte.checked = true;
                        chkValoreICBF.checked = false;                        
                        chkValorReg.checked = false;
                        txtFechaInicia.value = '';
                        txtFechaFin.value = '';
                        txtValorAseg.value = '';
                        txtValorCal.value = '';
                        txtValorBase.value = "$ " + valorContrato.toLocaleString("es-CO", { minimumFractionDigits: 2 });
                        $('#<%=  txtValorInicialContrato.ClientID %>').prop('disabled',true);
                        prePostbck(false);
                        //__doPostBack('<%= chkValorICBF.ClientID %>', '');
                    }
                    else {                        
                        chkValoreICBF.checked = true;
                        chkValorReg.checked = false;
                        chkValorICBFAporte.checked = false;
                        txtFechaInicia.value = '';
                        txtFechaFin.value = '';
                        txtValorAseg.value = '';
                        txtValorCal.value = '';
                        txtValorBase.value = "$ " + (valorContrato - valorCofinaciacion).toLocaleString("es-CO", { minimumFractionDigits: 2 });
                        $('#<%=  txtValorInicialContrato.ClientID %>').prop('disabled',true);
                        control.checked = false;
                        ocultaImagenLoading();
                    }
                    
                }
                else if (control.id == chkValoreICBF.id)
                {
                    if (chkValoreICBF.checked) {
                        if (confirm('El calculo del valor asegurado se realizara sin incluir la cofinanciación del Contratista')) {
                            chkValoreICBF.checked = true;
                            chkValorICBFAporte.checked = false;
                            chkValorReg.checked = false;
                            txtFechaInicia.value = '';
                            txtFechaFin.value = '';
                            txtValorAseg.value = '';
                            txtValorCal.value = '';
                            txtValorBase.value = "$ " + (valorContrato - valorCofinaciacion).toLocaleString("es-CO", { minimumFractionDigits: 2 });
                            $('#<%=  txtValorInicialContrato.ClientID %>').prop('disabled',true);
                            ddlunidadCalculo.selectedIndex = 0;
                           prePostbck(false);
                           // __doPostBack('<%= chkValorICBF.ClientID %>', '');
                        }
                        else {
                            chkValorICBFAporte.checked = true;
                            chkValoreICBF.checked = false;
                            chkValorReg.checked = false;
                            txtFechaInicia.value = '';
                            txtFechaFin.value = '';
                            txtValorAseg.value = '';
                            txtValorCal.value = '';
                            txtValorBase.value = "$ " + valorContrato.toLocaleString("es-CO", { minimumFractionDigits: 2 });
                            $('#<%=  txtValorInicialContrato.ClientID %>').prop('disabled',true);
                            control.checked = false;
                            ocultaImagenLoading();
                        }
                    } else {
                        prePostbck(false);
                        __doPostBack('<%= chkValorICBF.ClientID %>', '');
                    }
                }
                else if (control.id == chkValorReg.id)
                {
                    if (chkValorReg.checked) {
                        if (confirm('El calculo del valor base asegurado sera ingresado por el usuario')) {
                            chkValoreICBF.checked = false;
                            chkValorICBFAporte.checked = false;
                            chkValorReg.checked = true;
                            txtFechaInicia.value = '';
                            txtFechaFin.value = '';
                            txtValorAseg.value = '';
                            txtValorCal.value = '';
                            txtValorBase.value = '';
                            $('#<%=  txtValorInicialContrato.ClientID %>').prop('disabled',false);
                            ddlunidadCalculo.selectedIndex = 0;
                            prePostbck(false);
                          <%--  __doPostBack('<%= chkValorICBF.ClientID %>', '');
                             __doPostBack('<%= chkValorICBFAporte.ClientID %>', '');--%>
                        }
                        else {
                            chkValorICBFAporte.checked = true;
                            chkValoreICBF.checked = false;
                            chkValorReg.checked = false;
                            txtFechaInicia.value = '';
                            txtFechaFin.value = '';
                            txtValorAseg.value = '';
                            txtValorCal.value = '';
                            txtValorBase.value = "$ " + valorContrato.toLocaleString("es-CO", { minimumFractionDigits: 2 });
                            $('#<%=  txtValorInicialContrato.ClientID %>').prop('disabled',true);
                            control.checked = false;
                            ocultaImagenLoading();
                        }
                    } else {
                        prePostbck(false);
                        __doPostBack('<%= chkValorICBF.ClientID %>', '');
                        __doPostBack('<%= chkValorICBFAporte.ClientID %>', '');
                    }
                }
            }
           
        }


        function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

        }

           
//        $(function () {
//            $("#<%= txtValorCalculoAsegurado.ClientID%>").change(function () {
//                $(this).formatCurrency();
//            }).blur(function () {
//                $(this).formatCurrency();
        //            });
        //if (chkValorReg.checked)
        //    txtValorCal.attributes('disabled', 'disabled');
        //else
        //    txtValorCal.removeAttribute('disabled');
//        });
     </script>
<asp:HiddenField ID="hfIDAmparosGarantias" runat="server" />
    <asp:HiddenField ID="hfIdGarantia" runat="server" />
    <table width="90%" align="center">
    <tr class="rowB">
            <td>
                Número del Contrato/Convenio 
            </td>
            <td>
                Valor Base Calculo del Valor Asegurado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25" Width="80%" onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
               <asp:TextBox runat="server" ID="txtValorInicialContrato"
                   onkeyup="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')" onchange="puntitos(this,this.value.charAt(this.value.length-1),'decimales2')"
                   MaxLength="25" Width="80%"  Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom,Numbers" ValidChars=".,$ " />                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                 Tipo Amparo *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoAmparo" ControlToValidate="ddlIdTipoAmparo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoAmparo" ControlToValidate="ddlIdTipoAmparo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="True"></asp:CompareValidator>
            </td>
            <td>
                 Tipo de Calculo *                
            </td>
            
        </tr>
        <tr class="rowA">
            <td rowspan="4">
               <asp:DropDownList runat="server" ID="ddlIdTipoAmparo" AutoPostBack="True" OnSelectedIndexChanged="ddlIdTipoAmparo_OnSelectedIndexChanged"></asp:DropDownList>
            </td>            
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox ID="chkValorICBF" runat="server"  Enabled="true" onclick="juegoValorICBF(this)" />
                    Valor Aporte ICBF
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox ID="chkValorICBFAporte" runat="server"  Enabled="true" onclick="juegoValorICBF(this)" />
                Valor ICBF + Confinanciación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:CheckBox ID="chkValorReg" runat="server"  Enabled="false"  onclick="juegoValorICBF(this)" />
                Requiere Registrar Valor Base
            </td>            
        </tr>
        <tr class="rowB">
            <td>
               Fecha Vigencia Desde *
                <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" ControlToValidate="txtFechaVigenciaDesde" ErrorMessage="Campo Requerido" SetFocusOnError="True" 
                Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="cvFechaDesde" runat="server" ControlToValidate="txtFechaVigenciaDesde" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>

            <td>
                 Fecha Vigencia Hasta *
                  <asp:RequiredFieldValidator ID="rfvFechaVigenciaHasta" runat="server" ControlToValidate="txtFechaVigenciaHasta" ErrorMessage="Campo Requerido" SetFocusOnError="True" 
                  Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="cvFechaVigenciaHasta" runat="server" ControlToValidate="txtFechaVigenciaHasta" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>            
        </tr>
        <tr class="rowA">
            <td>
                <%--<uc1:fecha ID="txtFechaVigenciaDesded" runat="server"  Enabled="False" Requerid="True" Width="80%" on/>--%>
                <asp:TextBox runat="server" ID="txtFechaVigenciaDesde" OnTextChanged="txtFechaVigenciaDesde_OnTextChanged" AutoPostBack="True" Enabled="False"></asp:TextBox>
                    <asp:Image ID="imgCalendarioDesde" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
                    <Ajax:CalendarExtender ID="cetxtFechaDesde" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioDesde" TargetControlID="txtFechaVigenciaDesde"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVigenciaDesde">
                    </Ajax:MaskedEditExtender>
												
            </td>

            <td>
                  <%--<uc1:fecha ID="txtFechaVigenciaHasta" runat="server"  Enabled="False" Requerid="True" Width="80%"/>--%>
                  <asp:TextBox runat="server" ID="txtFechaVigenciaHasta" OnTextChanged="txtFechaVigenciaHasta_OnTextChanged" AutoPostBack="True" Enabled="False"></asp:TextBox>
                <asp:Image ID="imgCalendarioHasta" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
                <Ajax:CalendarExtender ID="ceFechaVigenciaHasta" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="imgCalendarioHasta" TargetControlID="txtFechaVigenciaHasta"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meFechaVigenciaHasta" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVigenciaHasta">
                </Ajax:MaskedEditExtender>
												
            </td>            
        </tr>
        <tr class="rowB">
            <td>
                Unidad de Cálculo *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDUnidadCalculo" ControlToValidate="ddlIDUnidadCalculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIDUnidadCalculo" ControlToValidate="ddlIDUnidadCalculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>          
            <td>
                Valor para Cálculo Asegurado *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorCalculoAsegurado" ControlToValidate="txtValorCalculoAsegurado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>           
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIDUnidadCalculo" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="ddlIDUnidadCalculo_OnSelectedIndexChanged"></asp:DropDownList>
                <%----%>
            </td>

            <td>
                 <asp:TextBox runat="server" ID="txtValorCalculoAsegurado" MaxLength="8" Width="80%" onkeypress="return EsEspacio(event,this)" Enabled="False" AutoPostBack="True" OnTextChanged="txtValorCalculoAsegurado_OnTextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorCalculoAsegurado" runat="server" TargetControlID="txtValorCalculoAsegurado"
                    FilterType="Custom,Numbers" ValidChars=".,$% " />
            </td>         
        </tr>
        <tr class="rowB">
            <td>
               Valor Asegurado 
               <%-- <asp:RequiredFieldValidator runat="server" ID="rfvValorAsegurado" ControlToValidate="txtValorAsegurado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" Enabled="False"
                 ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr class="rowA">
               <td>
               <asp:TextBox runat="server" ID="txtValorAsegurado" MaxLength="32" Width="80%" onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAsegurado" runat="server" TargetControlID="txtValorAsegurado"
                    FilterType="Custom,Numbers" ValidChars=".,$ " />
            </td>
        </tr>

    </table>
</asp:Content>
