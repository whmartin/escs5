﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_AmparosGarantiasCesion_Add" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfFechaSuscripcion" runat="server" />
    <asp:HiddenField ID="hfFechaInicioEjecucion" runat="server" />
    <asp:HiddenField ID="hfFechaTerminacionContrato" runat="server" />
    <asp:HiddenField ID="hfIdTipoModificacion" runat="server" />
    <asp:HiddenField ID="hfValorCofinaciacion" runat="server" />
    <asp:HiddenField ID="hfValorContrato" runat="server" />
    <asp:HiddenField ID="hfValorCesion" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />

    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function PreGuardado() {
            ConfiguraValidadores(true);
            window.Page_ClientValidate("btnGuardar");
        }

        function ConfiguraValidadores(habilitar) {
            ValidatorEnable($("#cphCont_rfvIdTipoAmparo")[0], habilitar);
            ValidatorEnable($("#cphCont_cvIdTipoAmparo")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvFechaDesde")[0], habilitar);
            ValidatorEnable($("#cphCont_cvFechaDesde")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvFechaVigenciaHasta")[0], habilitar);
            ValidatorEnable($("#cphCont_cvFechaVigenciaHasta")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvIDUnidadCalculo")[0], habilitar);
            ValidatorEnable($("#cphCont_cvIDUnidadCalculo")[0], habilitar);
            ValidatorEnable($("#cphCont_rfvValorCalculoAsegurado")[0], habilitar);
        }
    </script>
    
    <script type="text/javascript" language="javascript">
        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false; 
            else
                return false;
        }    

           
//        $(function () {
//            $("#<%= txtValorCalculoAsegurado.ClientID%>").change(function () {
//                $(this).formatCurrency();
//            }).blur(function () {
//                $(this).formatCurrency();
//            });
//        });
     </script>
<asp:HiddenField ID="hfIDAmparosGarantias" runat="server" />
    <asp:HiddenField ID="hfIdGarantia" runat="server" />
    <table width="90%" align="center">
    <tr class="rowB">
            <td>
                Número del Contrato/Convenio 
            </td>
            <td>
                Valor Base Calculo del Valor Asegurado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="25" Width="80%" onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td>
               <asp:TextBox runat="server" ID="txtValorInicialContrato" MaxLength="25" Width="80%"  Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorInicialContrato" runat="server" TargetControlID="txtValorInicialContrato"
                    FilterType="Custom,Numbers" ValidChars=".,$ " />                
            </td>
        </tr>
        <tr class="rowB">
            <td>
                  Tipo Amparo *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdTipoAmparo" ControlToValidate="ddlIdTipoAmparo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="True"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdTipoAmparo" ControlToValidate="ddlIdTipoAmparo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="True"></asp:CompareValidator>
            </td>
            <td>
                Fecha Vigencia Desde *
                <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" ControlToValidate="txtFechaVigenciaDesde" ErrorMessage="Campo Requerido" SetFocusOnError="True" 
                Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="cvFechaDesde" runat="server" ControlToValidate="txtFechaVigenciaDesde" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td rowspan="3">
                 <asp:DropDownList runat="server" ID="ddlIdTipoAmparo" AutoPostBack="True" OnSelectedIndexChanged="ddlIdTipoAmparo_OnSelectedIndexChanged"></asp:DropDownList>
            </td>
            <td>
                    <%--<uc1:fecha ID="txtFechaVigenciaDesded" runat="server"  Enabled="False" Requerid="True" Width="80%" on/>--%>
                <asp:TextBox runat="server" ID="txtFechaVigenciaDesde" OnTextChanged="txtFechaVigenciaDesde_OnTextChanged" AutoPostBack="True" Enabled="False"></asp:TextBox>
                    <asp:Image ID="imgCalendarioDesde" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
                    <Ajax:CalendarExtender ID="cetxtFechaDesde" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioDesde" TargetControlID="txtFechaVigenciaDesde"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVigenciaDesde">
                    </Ajax:MaskedEditExtender>          
            </td>
        </tr>
        <tr class="rowB">           
        </tr>
        <tr class="rowA">                        
        </tr>
        
        <tr class="rowB">
            <td>
               Fecha Vigencia Hasta *
                  <asp:RequiredFieldValidator ID="rfvFechaVigenciaHasta" runat="server" ControlToValidate="txtFechaVigenciaHasta" ErrorMessage="Campo Requerido" SetFocusOnError="True" 
                  Enabled="False" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:CompareValidator ID="cvFechaVigenciaHasta" runat="server" ControlToValidate="txtFechaVigenciaHasta" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>

            <td>
                  Unidad de Cálculo *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDUnidadCalculo" ControlToValidate="ddlIDUnidadCalculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIDUnidadCalculo" ControlToValidate="ddlIDUnidadCalculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" Enabled="False"></asp:CompareValidator>
            </td>            
        </tr>
        <tr class="rowA">
            <td>               
					 <%--<uc1:fecha ID="txtFechaVigenciaHasta" runat="server"  Enabled="False" Requerid="True" Width="80%"/>--%>
                  <asp:TextBox runat="server" ID="txtFechaVigenciaHasta" OnTextChanged="txtFechaVigenciaHasta_OnTextChanged" AutoPostBack="True" Enabled="False"></asp:TextBox>
                <asp:Image ID="imgCalendarioHasta" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" Visible="False"/>
                <Ajax:CalendarExtender ID="ceFechaVigenciaHasta" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="imgCalendarioHasta" TargetControlID="txtFechaVigenciaHasta"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meFechaVigenciaHasta" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVigenciaHasta">
                </Ajax:MaskedEditExtender>							
            </td>

            <td>
                <asp:DropDownList runat="server" ID="ddlIDUnidadCalculo" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="ddlIDUnidadCalculo_OnSelectedIndexChanged"></asp:DropDownList>												
            </td>            
        </tr>
        <tr class="rowB">
            <td>
               Valor para Cálculo Asegurado *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorCalculoAsegurado" ControlToValidate="txtValorCalculoAsegurado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>          
            <td>
                Valor Asegurado 
               <%-- <asp:RequiredFieldValidator runat="server" ID="rfvValorAsegurado" ControlToValidate="txtValorAsegurado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" Enabled="False"
                 ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </td>           
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtValorCalculoAsegurado" MaxLength="8" Width="80%" onkeypress="return EsEspacio(event,this)" Enabled="False" AutoPostBack="True" OnTextChanged="txtValorCalculoAsegurado_OnTextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorCalculoAsegurado" runat="server" TargetControlID="txtValorCalculoAsegurado"
                    FilterType="Custom,Numbers" ValidChars=".,$% " />                
            </td>

            <td>
                 <asp:TextBox runat="server" ID="txtValorAsegurado" MaxLength="32" Width="80%" onkeypress="return EsEspacio(event,this)" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorAsegurado" runat="server" TargetControlID="txtValorAsegurado"
                    FilterType="Custom,Numbers" ValidChars=".,$ " />
            </td>         
        </tr>       

    </table>
</asp:Content>

