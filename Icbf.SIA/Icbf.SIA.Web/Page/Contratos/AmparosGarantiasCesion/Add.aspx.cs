﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición para la entidad AmparosGarantias
/// </summary>
public partial class Page_Contratos_AmparosGarantiasCesion_Add : GeneralWeb
{
    #region Variables

    private General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    OferenteService vOferenteService = new OferenteService();
    string PageName = "Contrato/AmparosGarantias";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (!Page.IsPostBack)
        {
            CargarValoresContrato();
            CargarDatosIniciales();

        }
    }

    protected void ddlIdTipoAmparo_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (ddlIdTipoAmparo.SelectedValue != "-1")
        {
            txtFechaVigenciaDesde.Enabled = true;
            imgCalendarioDesde.Visible = true;
            rfvFechaDesde.Enabled = true;
            cvFechaDesde.Enabled = true;
        }
        else
        {
            txtFechaVigenciaDesde.Enabled = false;
            imgCalendarioDesde.Visible = false;
        }

        
    }

    protected void txtFechaVigenciaDesde_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        DateTime fechavalidacion = new DateTime(1900, 1, 1);
        DateTime fechaSuscripcion = Convert.ToDateTime(hfFechaSuscripcion.Value);


        if (!txtFechaVigenciaDesde.Text.StartsWith("_") && !string.IsNullOrEmpty(txtFechaVigenciaDesde.Text) && DateTime.TryParse(txtFechaVigenciaDesde.Text, out fechavalidacion))
        {
            //if (fechaInicioEjecucion.Date != fechavalidacion.Date)
            if (Convert.ToDateTime(txtFechaVigenciaDesde.Text).Date < fechaSuscripcion.Date)
            {
                toolBar.MostrarMensajeError("La Fecha Vigencia Desde debe ser igual o mayor a la fecha de suscripcion");
                txtFechaVigenciaHasta.Text = string.Empty;
                txtFechaVigenciaHasta.Enabled = false;
                imgCalendarioHasta.Visible = false;
                rfvFechaVigenciaHasta.Enabled = false;
                cvFechaVigenciaHasta.Enabled = false;
                return;
            }

            txtFechaVigenciaHasta.Enabled = true;
            imgCalendarioHasta.Visible = true;
            rfvFechaVigenciaHasta.Enabled = true;
            cvFechaVigenciaHasta.Enabled = true;
        }
        else
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Desde no es Válida");
            return;
        }

       

    }

    protected void txtFechaVigenciaHasta_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        DateTime fechavalidacion = new DateTime(1900, 1, 1);
        DateTime fechaTerminacionContrato = Convert.ToDateTime(hfFechaTerminacionContrato.Value).Date;

        if (!DateTime.TryParse(txtFechaVigenciaDesde.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Desde no es Válida");
            return;
        }

        if (!DateTime.TryParse(txtFechaVigenciaHasta.Text, out fechavalidacion))
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Hasta no es Válida");
            return;
        }

        if (string.IsNullOrEmpty(txtFechaVigenciaDesde.Text))
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Desde no es Válida");
            return;
        }

        if (string.IsNullOrEmpty(txtFechaVigenciaHasta.Text))
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Hasta no es Válida");
            return;
        }

        if (txtFechaVigenciaDesde.Text.StartsWith("_"))
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Desde no es Válida ");
            return;
        }

        if (txtFechaVigenciaHasta.Text.StartsWith("_"))
        {
            toolBar.MostrarMensajeError("Fecha Vigencia Hasta no es Válida");
            return;
        }

        if (Convert.ToDateTime(txtFechaVigenciaHasta.Text).Date < Convert.ToDateTime(txtFechaVigenciaDesde.Text).Date)
        {
            toolBar.MostrarMensajeError("La Fecha Vigencia Hasta no puede ser menor a la Fecha Vigencia Desde");
            return;
        }
        if (Convert.ToDateTime(txtFechaVigenciaHasta.Text).Date < fechaTerminacionContrato)
        {
            toolBar.MostrarMensajeError("La Fecha Vigencia Hasta no puede ser menor a la Fecha de Terminacion del contrato");
            return;
        }

        else
        {
            ddlIDUnidadCalculo.Enabled = true;
            rfvIDUnidadCalculo.Enabled = true;
            cvIDUnidadCalculo.Enabled = true;
            CargarIDUnidadCalculo();
        }

    }

    protected void ddlIDUnidadCalculo_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        if (ddlIDUnidadCalculo.SelectedValue != "-1")
        {
            txtValorCalculoAsegurado.Enabled = true;
            rfvValorCalculoAsegurado.Enabled = true;
            //rfvValorAsegurado.Enabled = true;
            txtValorCalculoAsegurado.Text = string.Empty;
            txtValorAsegurado.Text = string.Empty;
            txtValorCalculoAsegurado.Focus();
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void txtValorCalculoAsegurado_OnTextChanged(object sender, EventArgs e)
    {
        CalcularValores();
    }
    #endregion

    #region Métodos
    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad AmparosGarantias
    /// </summary>
    private void Guardar()
    {
        toolBar.LipiarMensajeError();
        try
        {
            if (txtValorAsegurado.Text.Equals(string.Empty))
            {
                toolBar.MostrarMensajeError("Debe diligenciar los campos obligatorios");
                return;
            }

            if (ddlIDUnidadCalculo.SelectedItem.Text.Equals("Porcentaje") && decimal.Parse(txtValorAsegurado.Text, NumberStyles.Currency) > decimal.Parse(txtValorInicialContrato.Text, NumberStyles.Currency))
            {
                toolBar.MostrarMensajeError("El Valor Asegurado debe ser menor o igual al Valor inicial del Contrato");
                return;
            }

            int vResultado;
            AmparosGarantias vAmparosGarantias = new AmparosGarantias();

            vAmparosGarantias.IDGarantia = int.Parse(hfIdGarantia.Value);

            if (txtFechaVigenciaDesde.Text.StartsWith("_") || txtFechaVigenciaHasta.Text.StartsWith("_"))
            {
                toolBar.MostrarMensajeError("Fecha(s) Inválidas");
                return;
            }

            DateTime fechaVigenciaDesde, fechaVigenciaHasta;
            if (!DateTime.TryParse(txtFechaVigenciaDesde.Text, out fechaVigenciaDesde) && !DateTime.TryParse(txtFechaVigenciaHasta.Text, out fechaVigenciaHasta))
            {
                toolBar.MostrarMensajeError("Fecha(s) Inválidas");
                return;
            }

            if (Convert.ToDateTime(txtFechaVigenciaDesde.Text).Date > Convert.ToDateTime(txtFechaVigenciaHasta.Text).Date)
            {
                toolBar.MostrarMensajeError("La fecha de vigencia desde no puede ser mayor a la fecha de vigencia hasta");
                return;
            }

            vAmparosGarantias.IdTipoAmparo = Convert.ToInt32(ddlIdTipoAmparo.SelectedValue);
            vAmparosGarantias.NombreTipoAmparo = Convert.ToString(ddlIdTipoAmparo.SelectedItem);
            vAmparosGarantias.FechaVigenciaDesde = Convert.ToDateTime(txtFechaVigenciaDesde.Text);
            vAmparosGarantias.FechaVigenciaHasta = Convert.ToDateTime(txtFechaVigenciaHasta.Text);
            vAmparosGarantias.IDUnidadCalculo = Convert.ToInt32(ddlIDUnidadCalculo.SelectedValue);
            vAmparosGarantias.NombreUnidadCalculo = Convert.ToString(ddlIDUnidadCalculo.SelectedItem);
            if (txtValorCalculoAsegurado.Text.Contains("%"))
                vAmparosGarantias.ValorCalculoAsegurado = Convert.ToDecimal(txtValorCalculoAsegurado.Text.Split(' ')[0]);
            else
                vAmparosGarantias.ValorCalculoAsegurado = decimal.Parse(txtValorCalculoAsegurado.Text, NumberStyles.Currency);
            vAmparosGarantias.ValorAsegurado = decimal.Parse(txtValorAsegurado.Text, NumberStyles.Currency);

            vAmparosGarantias.UsuarioCrea = GetSessionUser().NombreUsuario;
            InformacionAudioria(vAmparosGarantias, this.PageName, vSolutionPage);
            vResultado = vContratoService.InsertarAmparosGarantias(vAmparosGarantias);

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("AmparosGarantias.IDAmparosGarantias", vAmparosGarantias.IDAmparosGarantias);
                toolBar.MostrarMensajeGuardado();
                return;
            }
            RemoveSessionParameter("ContratoTerminacion");
            //else
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
        }
        //catch (FormatException ex)
        //{
        //    toolBar.MostrarMensajeError("Fecha(s) Inválidas");
        //}
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.EstablecerTitulos("Amparo Garantía", SolutionPage.Add.ToString());

            toolBar.SetBtnGuardarOnClientClick("return PreGuardado()");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnRetornar_Click(object sender, EventArgs e)
    {
        Retornar();
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            hfIdGarantia.Value = Request.QueryString["idGarantia"];
            hfIdTipoModificacion.Value = Request.QueryString["idTipoModificacion"];
            
            ftValorCalculoAsegurado.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftValorAsegurado.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ddlIDUnidadCalculo.Items.Insert(0, new ListItem("Seleccione", "-1"));
            CargarIdTipoAmparo();


            DateTime fechaSuscripcion = DateTime.MinValue;

            if (string.IsNullOrEmpty(hfIdTipoModificacion.Value) || hfIdTipoModificacion.Value == "0")
                fechaSuscripcion = Convert.ToDateTime(hfFechaSuscripcion.Value);
            else
            {
                SolModContractual mod = vContratoService.ObtenerModificacionSinModificacionGarantiaPorTipo(int.Parse(hfIdTipoModificacion.Value), int.Parse(hfIdContrato.Value));
                fechaSuscripcion = mod.FechaSuscripcion;
            }

            DateTime fechaTerminacionContrato = Convert.ToDateTime(hfFechaTerminacionContrato.Value).Date;
            cetxtFechaDesde.StartDate = fechaSuscripcion;
            ceFechaVigenciaHasta.StartDate = fechaTerminacionContrato;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarValoresContrato()
    {
        try
        {
            toolBar.LipiarMensajeError();

            hfValorCesion.Value = Request.QueryString["valorCesion"];

            int? idContrato = null;
            if (GetSessionParameter("Contrato.ContratosAPP") != null)
                idContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));

            hfIdContrato.Value = idContrato.ToString();
            Contrato vContrato = vContratoService.ConsultarContrato(idContrato.Value);
            txtNumeroContrato.Text = vContrato.NumeroContrato;

            hfFechaSuscripcion.Value = vContrato.FechaSuscripcion.ToString();
            hfFechaTerminacionContrato.Value = vContrato.FechaFinalTerminacionContrato.ToString();
            hfFechaInicioEjecucion.Value = vContrato.FechaInicioEjecucion.ToString();
            //List<AporteContrato> aportes = vContratoService.ConsultarCesiones(idContrato.Value);
            //hfValorCofinaciacion.Value = aportes.Sum(e => e.ValorAporte).ToString();
            hfValorContrato.Value = vContrato.ValorInicialContrato.ToString();
            
            if(!string.IsNullOrEmpty(hfValorCesion.Value.ToString()))
               txtValorInicialContrato.Text = Convert.ToDecimal(hfValorCesion.Value).ToString("$ #,###0.00##;($ #,###0.00##)");
           

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de datos de IdTipoAmparo
    /// </summary>
    private void CargarIdTipoAmparo()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarTiposAmparos(ddlIdTipoAmparo, "-1", true);
    }

    /// <summary>
    /// Método de carga de datos de IDUnidadCalculo
    /// </summary>
    private void CargarIDUnidadCalculo()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarUnidadCalculo(ddlIDUnidadCalculo, "-1", true);
    }


    /// <summary>
    /// Método que retorna los amparos a las garantias
    /// </summary>private void Retornar()
    private void Retornar()
    {
        try
        {
            string returnValues =
           "<script language='javascript'> " +
           "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(Convert.ToString("AmparosGarantias.IDAmparosGarantias")) + "';";

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CalcularValores()
    {
        toolBar.LipiarMensajeError();
        txtValorAsegurado.Text = "";

        if (ddlIDUnidadCalculo.SelectedItem.Text.Equals("Porcentaje"))
        {
            if (txtValorCalculoAsegurado.Text.Contains("$") || txtValorCalculoAsegurado.Text.Contains(".") || txtValorCalculoAsegurado.Text.Contains("%") || string.IsNullOrEmpty(txtValorCalculoAsegurado.Text))
            {
                toolBar.MostrarMensajeError("Debe colocar un valor numérico válido, sin caracteres especiales");
                txtValorCalculoAsegurado.Text = string.Empty;
                txtValorCalculoAsegurado.Focus();
                return;
            }

            if (decimal.Parse(txtValorCalculoAsegurado.Text) <= 0 || decimal.Parse(txtValorCalculoAsegurado.Text) > 100)
            {
                toolBar.MostrarMensajeError("El Valor para Calculo Asegurado debe ser mayor a 0 y menor o igual a 100");
                //ddlIDUnidadCalculo.Items.Clear();
                //CargarIDUnidadCalculo();
                txtValorCalculoAsegurado.Text = string.Empty;
                txtValorCalculoAsegurado.Focus();
                return;
            }
            else
            {

                toolBar.LipiarMensajeError();
                txtValorAsegurado.Text =
                    ((decimal.Parse(txtValorCalculoAsegurado.Text, NumberStyles.Currency) * decimal.Parse(txtValorInicialContrato.Text, NumberStyles.Currency)) / 100).ToString("$ #,###0.00##;($ #,###0.00##)");
                txtValorCalculoAsegurado.Text += " %";

            }
        }
        else
        {
            if (ddlIDUnidadCalculo.SelectedItem.Text.Equals("Salarios Mínimos"))
            {
                if (txtValorCalculoAsegurado.Text.Contains("$") || txtValorCalculoAsegurado.Text.Contains(".") || txtValorCalculoAsegurado.Text.Contains("%") || string.IsNullOrEmpty(txtValorCalculoAsegurado.Text))
                {
                    toolBar.MostrarMensajeError("Debe colocar un valor numérico válido, sin caracteres especiales");
                    //ddlIDUnidadCalculo.Items.Clear();
                    //CargarIDUnidadCalculo();
                    txtValorCalculoAsegurado.Text = string.Empty;
                    txtValorCalculoAsegurado.Focus();
                    return;
                }

                if (decimal.Parse(txtValorCalculoAsegurado.Text) <= 0)
                {
                    toolBar.MostrarMensajeError("El Valor para Calculo Asegurado debe ser mayor a 0");
                    return;
                }
                string descripcion = "minimo";
                int fechaVigencia = Convert.ToInt32(DateTime.Parse(txtFechaVigenciaDesde.Text).Year);
                decimal SalarioMinimo = Convert.ToDecimal(vOferenteService.ConsultarSalarioMinimo(fechaVigencia, descripcion).Valor);
                if (SalarioMinimo == 0)
                {
                    toolBar.MostrarMensajeError("No se encuentra el valor del salario configurado para el año " + Convert.ToDateTime(txtFechaVigenciaDesde.Text).Year);
                    return;
                }

                txtValorAsegurado.Text = (decimal.Parse(txtValorCalculoAsegurado.Text) * SalarioMinimo).ToString("$ #,###0.00##;($ #,###0.00##)");
                string valor = Convert.ToDecimal(txtValorCalculoAsegurado.Text).ToString("$ #,###0.00##;($ #,###0.00##)");
                txtValorCalculoAsegurado.Text = valor;
            }
        }
    }
    #endregion
}
