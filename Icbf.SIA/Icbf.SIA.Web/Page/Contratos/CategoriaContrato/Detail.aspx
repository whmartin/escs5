<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_CategoriaContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Categor&#237;a *
            </td>
            <td style="width: 50%">
                Descripci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td valign="top" class="style1">
                <asp:TextBox runat="server" ID="txtNombreCategoriaContrato"  Enabled="false" 
                    Width="320px" Height="22px" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false" Height="50px" 
                    MaxLength="100" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,100);" onKeyUp="limitText(this,100);"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 371px;
        }
    </style>
</asp:Content>

