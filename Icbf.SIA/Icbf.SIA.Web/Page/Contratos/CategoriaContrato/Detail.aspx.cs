using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de categoría contratos
/// </summary>
public partial class Page_CategoriaContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/CategoriaContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
           SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("CategoriaContrato.Guardado");
            }

        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("CategoriaContrato.IdCategoriaContrato", hfIdCategoriaContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdCategoriaContrato = Convert.ToInt32(GetSessionParameter("CategoriaContrato.IdCategoriaContrato"));
            RemoveSessionParameter("CategoriaContrato.IdCategoriaContrato");
            
            if (GetSessionParameter("CategoriaContrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("CategoriaContrato");

            if (GetSessionParameter("CategoriaContrato.Modificado").ToString() == "1")
                toolBar.MostrarMensajeModificado();
            RemoveSessionParameter("CategoriaContrato.Modificado");

            CategoriaContrato vCategoriaContrato = new CategoriaContrato();
            vCategoriaContrato = vContratoService.ConsultarCategoriaContrato(vIdCategoriaContrato);
            hfIdCategoriaContrato.Value = vCategoriaContrato.IdCategoriaContrato.ToString();
            txtNombreCategoriaContrato.Text = vCategoriaContrato.NombreCategoriaContrato;
            txtDescripcion.Text = vCategoriaContrato.Descripcion;
            rblEstado.SelectedValue = vCategoriaContrato.Estado==true?"1":"0";
            ObtenerAuditoria(PageName, hfIdCategoriaContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCategoriaContrato.UsuarioCrea, vCategoriaContrato.FechaCrea, vCategoriaContrato.UsuarioModifica, vCategoriaContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdCategoriaContrato = Convert.ToInt32(hfIdCategoriaContrato.Value);

            CategoriaContrato vCategoriaContrato = new CategoriaContrato();
            vCategoriaContrato = vContratoService.ConsultarCategoriaContrato(vIdCategoriaContrato);
            InformacionAudioria(vCategoriaContrato, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarCategoriaContrato(vCategoriaContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("CategoriaContrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Categor&#237;a Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
