<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_CategoriaContrato_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td colspan="2" style="width: 50%">
                    Nombre Categor&#237;a
                </td>
                <td colspan="2" style="width: 50%">
                    Descripci&#243;n
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" MaxLength="50" ID="txtNombreCategoriaContrato" Width="300px"
                        Height="22px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                        TargetControlID="txtNombreCategoriaContrato" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                        ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " />
                </td>
                <td colspan="2">
                    <asp:TextBox runat="server" MaxLength="100" ID="txtDescripcionCategoriaContrato"
                        Width="320px" Height="50px" TextMode="MultiLine" onKeyDown="limitText(this,100);"
                        onKeyUp="limitText(this,100);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftDescripcionCategoriaContrato" runat="server"
                        TargetControlID="txtDescripcionCategoriaContrato" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                        ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; " />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvCategoriaContrato" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdCategoriaContrato"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvCategoriaContrato_PageIndexChanging"
                        OnSelectedIndexChanged="gvCategoriaContrato_SelectedIndexChanged" OnSorting="gvCategoriaContrato_Sorting"
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Nombre Categor&#237;a" DataField="NombreCategoriaContrato" SortExpression="NombreCategoriaContrato" />--%>
                            <asp:TemplateField HeaderText="Nombre Categor&#237;a" ItemStyle-HorizontalAlign="Center"
                                SortExpression="NombreCategoriaContrato">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreCategoriaContrato")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="Descripcion" HeaderText="Descripci&#243;n" SortExpression="Descripcion"/>--%>
                            <asp:TemplateField HeaderText="Descripci&#243;n" ItemStyle-HorizontalAlign="Center" SortExpression="Descripcion">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Descripcion")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                            <%--<asp:CheckBoxField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />--%>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
