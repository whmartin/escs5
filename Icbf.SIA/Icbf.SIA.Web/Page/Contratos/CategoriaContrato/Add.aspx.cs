using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de categoría contratos
/// </summary>
public partial class Page_CategoriaContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/CategoriaContrato";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
   
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            CategoriaContrato vCategoriaContrato = new CategoriaContrato();

            vCategoriaContrato.NombreCategoriaContrato = Convert.ToString(txtNombreCategoriaContrato.Text);
            vCategoriaContrato.Descripcion = Convert.ToString(txtDescripcion.Text);
            vCategoriaContrato.Estado = rblEstado.SelectedValue=="1"?true:false;

            if (Request.QueryString["oP"] == "E")
            {
            vCategoriaContrato.IdCategoriaContrato = Convert.ToInt32(hfIdCategoriaContrato.Value);
                vCategoriaContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCategoriaContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarCategoriaContrato(vCategoriaContrato);
            }
            else
            {
                vCategoriaContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCategoriaContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarCategoriaContrato(vCategoriaContrato);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("CategoriaContrato.IdCategoriaContrato", vCategoriaContrato.IdCategoriaContrato);
                if (Request.QueryString["oP"] == "E")
            {
                    SetSessionParameter("CategoriaContrato.Modificado", "1");
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                    NavigateTo(SolutionPage.Detail);
            }
                else
            {
                    SetSessionParameter("CategoriaContrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
            }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
    
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
 
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Categor&#237;a Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos provenientes de la entidad a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdCategoriaContrato = Convert.ToInt32(GetSessionParameter("CategoriaContrato.IdCategoriaContrato"));
            int vIdCategoriaContrato = 0;

            if (int.TryParse(GetSessionParameter("CategoriaContrato.IdCategoriaContrato").ToString(), out vIdCategoriaContrato))
            {

                RemoveSessionParameter("CategoriaContrato.Id");

                CategoriaContrato vCategoriaContrato = new CategoriaContrato();
                vCategoriaContrato = vContratoService.ConsultarCategoriaContrato(vIdCategoriaContrato);
                hfIdCategoriaContrato.Value = vCategoriaContrato.IdCategoriaContrato.ToString();
                txtNombreCategoriaContrato.Text = vCategoriaContrato.NombreCategoriaContrato;
                txtDescripcion.Text = vCategoriaContrato.Descripcion;
                rblEstado.SelectedValue = vCategoriaContrato.Estado == true ? "1" : "0";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vCategoriaContrato.UsuarioCrea, vCategoriaContrato.FechaCrea, vCategoriaContrato.UsuarioModifica, vCategoriaContrato.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
