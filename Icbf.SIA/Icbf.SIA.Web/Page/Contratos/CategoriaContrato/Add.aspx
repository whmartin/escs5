<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_CategoriaContrato_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

<asp:HiddenField ID="hfIdCategoriaContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Categor&#237;a *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreCategoriaContrato" ControlToValidate="txtNombreCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 50%">
                Descripci&#243;n
            </td>
        </tr>
        <tr class="rowA">
            <td valign="top" class="style1">
                <asp:TextBox runat="server" MaxLength="50" ID="txtNombreCategoriaContrato" 
                    Height="22px" Width="320px"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreCategoriaContrato" runat="server" TargetControlID="txtNombreCategoriaContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="100" 
                    TextMode="MultiLine" Width="320px"
                     onKeyDown="limitText(this,100);" 
                     onKeyUp="limitText(this,100);"> 
                     </asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 401px;
        }
    </style>
</asp:Content>

