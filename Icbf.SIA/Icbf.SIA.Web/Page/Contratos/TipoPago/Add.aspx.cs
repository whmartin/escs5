﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de registro y edición para la entidad NumeroProcesos
/// </summary>
public partial class Page_Contratos_TipoPago_Add : GeneralWeb
{
    private masterPrincipal toolBar;
    private ContratoService vContratoService = new ContratoService();
    private string PageName = "Contratos/TipoPago";
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad NumeroProcesos
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoPago vTipoPago = new TipoPago();


            vTipoPago.NombreTipoPago = txtCodigo.Text.Trim();
            vTipoPago.Estado = Convert.ToBoolean(rblInactivo.SelectedValue);
            vTipoPago.Descripcion = txtDescripcion.Text.Trim();


            if (Request.QueryString["oP"] == "E")
            {

                vTipoPago.IdTipoPago = Convert.ToInt32(hfIdTipoPago.Value);
                vTipoPago.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoPago, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarTipoPago(vTipoPago);
            }
            else
            {
                vTipoPago.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoPago, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarTipoPago(vTipoPago);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoPago.IdTipoPago", vTipoPago.IdTipoPago);
                SetSessionParameter("TipoPago.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError(
                    "La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Tipos de Pago", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {

            int vIdTipoPago = 0;

            if (int.TryParse(GetSessionParameter("TipoPago.IdTipoPago").ToString(), out vIdTipoPago))
            {
                RemoveSessionParameter("TipoPago.IdTipoPago");

                TipoPago vTipoPago = new TipoPago();
                vTipoPago = vContratoService.ConsultarTipoPago(vIdTipoPago);
                hfIdTipoPago.Value = vTipoPago.IdTipoPago.ToString();
                txtCodigo.Text = vTipoPago.NombreTipoPago;
                txtDescripcion.Text = vTipoPago.Descripcion;
                rblInactivo.SelectedValue = vTipoPago.Estado.ToString();

            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
            rblInactivo.SelectedValue = "true";


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}
