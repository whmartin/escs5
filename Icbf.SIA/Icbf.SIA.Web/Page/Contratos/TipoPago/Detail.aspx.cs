﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;


/// <summary>
/// Página de visualización detallada para la entidad NumeroProcesos
/// </summary>
public partial class Page_Contratos_TipoPago_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoPago";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoPago.IdTipoPago", hfIdTipoPago.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoPago = Convert.ToInt32(GetSessionParameter("TipoPago.IdTipoPago"));
            RemoveSessionParameter("TipoPago.IdTipoPago");

            if (GetSessionParameter("TipoPago.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoPago.Guardado");

            TipoPago vTipoPago = new TipoPago();
            vTipoPago = vContratoService.ConsultarTipoPago(vIdTipoPago);
            hfIdTipoPago.Value = vTipoPago.IdTipoPago.ToString();
            txtCodigo.Text = vTipoPago.NombreTipoPago;
            txtDescripcion.Text = vTipoPago.Descripcion;
            rblInactivo.SelectedValue = vTipoPago.Estado.ToString().Trim().ToLower();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoPago.UsuarioCrea, vTipoPago.FechaCrea, vTipoPago.UsuarioModifica, vTipoPago.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoPago = Convert.ToInt32(hfIdTipoPago.Value);

            TipoPago vTipoPago = new TipoPago();
            vTipoPago = vContratoService.ConsultarTipoPago(vIdTipoPago);

            //if (vContratoService.ConsultarNumeroProcesosPorContrato(vNumeroProcesos.IdNumeroProceso))
            //{
            //    toolBar.MostrarMensajeError("El registro tiene elementos  que dependen de él, verifique por favor.");
            //    return;
            //}

            InformacionAudioria(vTipoPago, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarTipoPago(vTipoPago);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoPago.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Tipos de Pago", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            rblInactivo.Items.Insert(0, new ListItem("Activo", "true"));
            rblInactivo.Items.Insert(0, new ListItem("Inactivo", "false"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }



}
