using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_SubComponente_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SubComponente";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            String vNombreSubComponente = null;
            int? vIdComponente = null;
            int? vEstado = null;
            if (txtNombreSubComponente.Text!= "")
            {
                vNombreSubComponente = Convert.ToString(txtNombreSubComponente.Text);
            }
            if (ddlIdComponente.SelectedValue!= "-1")
            {
                vIdComponente = Convert.ToInt32(ddlIdComponente.SelectedValue);
            }
            if (rblEstado.SelectedValue!= "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            gvSubComponente.DataSource = vContratoService.ConsultarSubComponentes(vNombreSubComponente, vIdComponente, vEstado);
            gvSubComponente.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSubComponente.PageSize = PageSize();
            gvSubComponente.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("SubComponente", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSubComponente.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SubComponente.IdSubComponente", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSubComponente_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSubComponente.SelectedRow);
    }
    protected void gvSubComponente_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSubComponente.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SubComponente.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SubComponente.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdComponente.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
