using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_SubComponente_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/SubComponente";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SubComponente vSubComponente = new SubComponente();

            vSubComponente.NombreSubComponente = Convert.ToString(txtNombreSubComponente.Text);
            vSubComponente.IdComponente = Convert.ToInt32(ddlIdComponente.SelectedValue);
            vSubComponente.Estado = Convert.ToInt32(rblEstado.SelectedValue);

            if (Request.QueryString["oP"] == "E")
            {
            vSubComponente.IdSubComponente = Convert.ToInt32(hfIdSubComponente.Value);
                vSubComponente.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSubComponente, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarSubComponente(vSubComponente);
            }
            else
            {
                vSubComponente.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSubComponente, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarSubComponente(vSubComponente);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SubComponente.IdSubComponente", vSubComponente.IdSubComponente);
                SetSessionParameter("SubComponente.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("SubComponente", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdSubComponente = Convert.ToInt32(GetSessionParameter("SubComponente.IdSubComponente"));
            RemoveSessionParameter("SubComponente.Id");

            SubComponente vSubComponente = new SubComponente();
            vSubComponente = vContratoService.ConsultarSubComponente(vIdSubComponente);
            hfIdSubComponente.Value = vSubComponente.IdSubComponente.ToString();
            txtNombreSubComponente.Text = vSubComponente.NombreSubComponente;
            ddlIdComponente.SelectedValue = vSubComponente.IdComponente.ToString();
            rblEstado.SelectedValue = vSubComponente.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSubComponente.UsuarioCrea, vSubComponente.FechaCrea, vSubComponente.UsuarioModifica, vSubComponente.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdComponente.SelectedValue = "-1";
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
