using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_SubComponente_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SubComponente";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SubComponente.IdSubComponente", hfIdSubComponente.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdSubComponente = Convert.ToInt32(GetSessionParameter("SubComponente.IdSubComponente"));
            RemoveSessionParameter("SubComponente.IdSubComponente");

            if (GetSessionParameter("SubComponente.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SubComponente");


            SubComponente vSubComponente = new SubComponente();
            vSubComponente = vContratoService.ConsultarSubComponente(vIdSubComponente);
            hfIdSubComponente.Value = vSubComponente.IdSubComponente.ToString();
            txtNombreSubComponente.Text = vSubComponente.NombreSubComponente;
            ddlIdComponente.SelectedValue = vSubComponente.IdComponente.ToString();
            rblEstado.SelectedValue = vSubComponente.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdSubComponente.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSubComponente.UsuarioCrea, vSubComponente.FechaCrea, vSubComponente.UsuarioModifica, vSubComponente.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdSubComponente = Convert.ToInt32(hfIdSubComponente.Value);

            SubComponente vSubComponente = new SubComponente();
            vSubComponente = vContratoService.ConsultarSubComponente(vIdSubComponente);
            InformacionAudioria(vSubComponente, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarSubComponente(vSubComponente);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SubComponente.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("SubComponente", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo para llenar los DropDownList*/
            ddlIdComponente.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIdComponente.SelectedValue = "-1";
            rblEstado.Items.Insert(0, new ListItem("Seleccione", "-1"));
            rblEstado.SelectedValue = "-1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
