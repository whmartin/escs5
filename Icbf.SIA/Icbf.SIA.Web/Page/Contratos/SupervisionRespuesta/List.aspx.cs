using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionRespuestas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionRespuesta";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                BuscarLoad();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdPregunta = null;
            String vValor = null;
            Boolean? vVulneraDerecho = null;
            int? vEstado = null;
            if (hfIdPregunta.Value != "")
            {
                vIdPregunta = Convert.ToInt32(hfIdPregunta.Value);
            }
            if (txtValor.Text != "")
            {
                vValor = Convert.ToString(txtValor.Text);
            }
            if (rblVulneraDerecho.SelectedValue != "-1")
            {
                vVulneraDerecho = Convert.ToBoolean(Int16.Parse(rblVulneraDerecho.SelectedValue));
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            gvSupervisionRespuestas.DataSource = vSupervisionService.ConsultarSupervisionRespuestass(vIdPregunta, vValor, vVulneraDerecho, vEstado);
            gvSupervisionRespuestas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void BuscarLoad()
    {
        try
        {
            int? vIdPregunta = null;
            if (hfIdPregunta.Value != "")
            {
                vIdPregunta = Convert.ToInt32(hfIdPregunta.Value);
            }
            gvSupervisionRespuestas.DataSource = vSupervisionService.ConsultarSupervisionRespuestass(vIdPregunta, null, null, null);
            gvSupervisionRespuestas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionRespuestas.PageSize = PageSize();
            gvSupervisionRespuestas.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Respuestas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionRespuestas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionRespuestas.IdRespuesta", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionRespuestas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionRespuestas.SelectedRow);
    }
    protected void gvSupervisionRespuestas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionRespuestas.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionRespuestas.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionRespuestas.Eliminado");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblVulneraDerecho, "Si", "No", "1");

            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            hfIdPregunta.Value = Preguntasupervision[1];
            if (Preguntasupervision[2] == "1")
            {
                lblVulneraRespuesta.Visible = true;
                rblVulneraDerecho.Visible = true;
            }
            else
            {
                lblVulneraRespuesta.Visible = false;
                rblVulneraDerecho.Visible = false;
                rblVulneraDerecho.Enabled = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
