<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SupervisionRespuestas_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/SupervisionContratos-ConfirmaGuardar.js" type="text/javascript"></script>
    <asp:HiddenField ID="hfIdRespuesta" runat="server" />
    <asp:HiddenField ID="hfIdPregunta" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Pregunta *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdPregunta" ControlToValidate="txtIdPregunta"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Respuesta *
                <asp:RequiredFieldValidator runat="server" ID="rfvValor" ControlToValidate="txtValor"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdPregunta" Enabled="false" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtIdPregunta"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValor" Width="80%" 
                    OnTextChanged="txtValor_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtValor"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                No aplica *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:CheckBox ID="chkNoAplica" runat="server" OnCheckedChanged="chkNoAplica_CheckedChanged"
                    AutoPostBack="True" />
            </td>
        </tr>
        <tr class="rowB" runat="server" id="trVulneraRespuesta">
            <td>
                Valor vulnera la respuesta? *
                <asp:RequiredFieldValidator runat="server" ID="rfvVulneraDerecho" ControlToValidate="rblVulneraDerecho"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA" runat="server" id="trVulneraDerecho">
            <td>
                <asp:RadioButtonList runat="server" ID="rblVulneraDerecho" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <div id="dvBloqueo" style="position: absolute; z-index: 1; width: 100%; height: 100%;
        background-color: #000000; top: 0px; left: 0px; display: none; opacity: 0.25;
        filter: alpha(opacity=25);">
    </div>
    <div id="dvConfirmacion" class="modalDialog">
        <table style="text-align: center; padding: 9px">
            <tr>
                <td colspan="2">
                    ¿Está seguro de guardar la respuesta? Recuerde que después de guardada no puede
                    ser modificada toda la información
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnAceptar" type="button" value="Aceptar" onclick="postback=true; $('#btnGuardar')[0].click()" />
                </td>
                <td>
                    <input id="btnCancelar" type="button" value="Cancelar" onclick="ocultarDiv();" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
