<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SupervisionRespuestas_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdRespuesta" runat="server" />
    <asp:HiddenField ID="hfIdPregunta" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Pregunta *
            </td>
            <td>
                Respuesta *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdPregunta" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValor" Width="80%" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Estado *
            </td>
            <td>
                <asp:Label Text="Valor vulnera la respuesta? *" runat="server" ID="lblVulneraRespuesta"></asp:Label>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblVulneraDerecho" RepeatDirection="Horizontal"
                    Enabled="false">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB" runat="server" id="trLblNA">
            <td colspan="2">
                No aplica *
            </td>
        </tr>
        <tr class="rowA" runat="server" id="trNA">
            <td colspan="2">
                <asp:CheckBox ID="chkNoAplica" runat="server" />
            </td>
        </tr>
        
    </table>
</asp:Content>
