using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionRespuestas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionRespuesta";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionRespuestas.IdRespuesta", hfIdRespuesta.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdRespuesta = Convert.ToInt32(GetSessionParameter("SupervisionRespuestas.IdRespuesta"));
            RemoveSessionParameter("SupervisionRespuestas.IdRespuesta");

            if (GetSessionParameter("SupervisionRespuestas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionRespuestas.Guardado");
            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
        

            SupervisionRespuestas vSupervisionRespuestas = new SupervisionRespuestas();
            vSupervisionRespuestas = vSupervisionService.ConsultarSupervisionRespuestas(vIdRespuesta);
            hfIdRespuesta.Value = vSupervisionRespuestas.IdRespuesta.ToString();
            txtIdPregunta.Text = Preguntasupervision[0];
            rblVulneraDerecho.SelectedValue = vSupervisionRespuestas.VulneraDerecho.ToString();
            rblEstado.SelectedValue = vSupervisionRespuestas.Estado.ToString();
            if (vSupervisionRespuestas.Valor == "N/A")
            {
                trLblNA.Visible = true;
                trNA.Visible = true;
                chkNoAplica.Checked = true;
                txtValor.Enabled = false;
                txtValor.Text = "";
            }
            else
            {
                trLblNA.Visible = false;
                trNA.Visible = false;
                chkNoAplica.Enabled = false;
                txtValor.Text = vSupervisionRespuestas.Valor;
                //txtValor.Enabled = true;

            }

            ObtenerAuditoria(PageName, hfIdRespuesta.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionRespuestas.UsuarioCrea, vSupervisionRespuestas.FechaCrea, vSupervisionRespuestas.UsuarioModifica, vSupervisionRespuestas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdRespuesta = Convert.ToInt32(hfIdRespuesta.Value);

            SupervisionRespuestas vSupervisionRespuestas = new SupervisionRespuestas();
            vSupervisionRespuestas = vSupervisionService.ConsultarSupervisionRespuestas(vIdRespuesta);
            InformacionAudioria(vSupervisionRespuestas, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionRespuestas(vSupervisionRespuestas);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionRespuestas.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoCambiarOpcion += new ToolBarDelegate(ddlExtends_SelectedIndexChanged);

            toolBar.EstablecerTitulos("Respuestas", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.AgregarOpcionAdicional(new ListItem("Ir a", "0"));
            toolBar.AgregarOpcionAdicional(new ListItem("Criterios", "1"));
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblVulneraDerecho, "Si", "No", "1");
            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            hfIdPregunta.Value = Preguntasupervision[1];
            if (Preguntasupervision[2] == "1")
            {
                lblVulneraRespuesta.Visible = true;
                rblVulneraDerecho.Visible = true;
            }
            else
            {
                lblVulneraRespuesta.Visible = false;
                rblVulneraDerecho.Visible = false;
                rblVulneraDerecho.Enabled = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlExtends_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (((DropDownList)sender).SelectedValue)
        {
            case "1":
                {
                    NavigateTo("~/Page/Contratos/SupervisionCriterio/Add.aspx", true);

                    break;
                }
            default:
                break;
        }
    }
}
