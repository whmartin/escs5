using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionRespuestas_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionRespuesta";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    //protected void Page_LoadComplete(object sender, EventArgs e)
    //{

    //    //if (!Page.IsPostBack)
    //    //{
    //    //    List<string> Preguntasupervision = new List<string>();
    //    //    Preguntasupervision = (List<string>)Session["Preguntasupervision"];
    //    //    txtIdPregunta.Text = Preguntasupervision[0];
    //    //    hfIdPregunta.Value = Preguntasupervision[1];
    //    //}

    //}

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionRespuestas vSupervisionRespuestas = new SupervisionRespuestas();

            vSupervisionRespuestas.Valor = Convert.ToString(txtValor.Text);
            vSupervisionRespuestas.VulneraDerecho = Convert.ToBoolean(Int16.Parse(rblVulneraDerecho.SelectedValue));
            vSupervisionRespuestas.Estado = Convert.ToInt32(rblEstado.SelectedValue);
            vSupervisionRespuestas.IdPregunta = Convert.ToInt32(hfIdPregunta.Value);

            if (chkNoAplica.Checked)
            {
                vSupervisionRespuestas.Valor = "N/A";
            }
            //int vResultExis = vSupervisionService.ConsultarSupervisionRespuestas(vSupervisionRespuestas.Valor);
            int vResultExis = vSupervisionService.ConsultarSupervisionRespuestass(vSupervisionRespuestas.IdPregunta, vSupervisionRespuestas.Valor, null, 1).Count();
            if (Request.QueryString["oP"] == "E")
            {
                if (vResultExis == 0)
                {
                    vSupervisionRespuestas.IdRespuesta = Convert.ToInt32(hfIdRespuesta.Value);
                    vSupervisionRespuestas.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionRespuestas, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.ModificarSupervisionRespuestas(vSupervisionRespuestas);
                }
                else
                {
                    toolBar.MostrarMensajeError("La respuesta ingresada ya existe");
                    vResultado = 3;
                }
            }
            else
            {
                if (vResultExis == 0)
                {

                    vSupervisionRespuestas.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionRespuestas, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.InsertarSupervisionRespuestas(vSupervisionRespuestas);
                }
                else
                {
                    toolBar.MostrarMensajeError("La respuesta ingresada ya existe");
                    vResultado = 3;
                }
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SupervisionRespuestas.IdRespuesta", vSupervisionRespuestas.IdRespuesta);
                SetSessionParameter("SupervisionRespuestas.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado != 3)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Respuestas", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdRespuesta = Convert.ToInt32(GetSessionParameter("SupervisionRespuestas.IdRespuesta"));
            RemoveSessionParameter("SupervisionRespuestas.Id");

            SupervisionRespuestas vSupervisionRespuestas = new SupervisionRespuestas();
            vSupervisionRespuestas = vSupervisionService.ConsultarSupervisionRespuestas(vIdRespuesta);
            hfIdRespuesta.Value = vSupervisionRespuestas.IdRespuesta.ToString();
            txtIdPregunta.Text = vSupervisionRespuestas.IdPregunta.ToString();
            txtValor.Text = vSupervisionRespuestas.Valor;
            rblVulneraDerecho.SelectedValue = vSupervisionRespuestas.VulneraDerecho.ToString();

            if (vSupervisionRespuestas.Valor == "N/A")
            {
                chkNoAplica.Checked = true;
                txtValor.Enabled = false;
            }
            else
            {
                chkNoAplica.Enabled = false;
                txtValor.Text = vSupervisionRespuestas.Valor;
                txtValor.Enabled = true;

            }


            rblEstado.SelectedValue = vSupervisionRespuestas.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionRespuestas.UsuarioCrea, vSupervisionRespuestas.FechaCrea, vSupervisionRespuestas.UsuarioModifica, vSupervisionRespuestas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.AgregarOpcionAdicional(new ListItem("Ir a", "0"));
            toolBar.AgregarOpcionAdicional(new ListItem("Criterios", "1"));

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblVulneraDerecho, "Si", "No", "0");

            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            hfIdPregunta.Value = Preguntasupervision[1];
            if (Preguntasupervision[2] == "1")
            {
                trVulneraRespuesta.Visible = true;
                trVulneraDerecho.Visible = true;
            }
            else
            {
                trVulneraRespuesta.Visible = false;
                trVulneraDerecho.Visible = false;
                rblVulneraDerecho.Enabled = false;
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void chkNoAplica_CheckedChanged(object sender, EventArgs e)
    {
        if (chkNoAplica.Checked)
        {
            txtValor.Text = "";
            txtValor.Enabled = false;
            rfvValor.Enabled = false;
        }
        else
        {
            txtValor.Enabled = true;
            rfvValor.Enabled = true;
        }
    }


    protected void txtValor_TextChanged(object sender, EventArgs e)
    {
        if (txtValor.Text.Length > 0)
        {
            chkNoAplica.Enabled = false;
        }
        else
        {
            chkNoAplica.Enabled = true;
        }
    }
}
