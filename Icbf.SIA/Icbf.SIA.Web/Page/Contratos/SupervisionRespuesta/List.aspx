<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SupervisionRespuestas_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdPregunta" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Pregunta *
                </td>
                <td>
                    Respuesta *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtIdPregunta" Enabled="false" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtIdPregunta"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;0123456789" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtValor" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtValor"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/������������ .,@_():;0123456789" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado *
                </td>
                <td>
                    <asp:Label Text="Valor vulnera la respuesta? *" runat="server" ID="lblVulneraRespuesta"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblVulneraDerecho" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSupervisionRespuestas" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdRespuesta" CellPadding="0"
                        Height="16px" OnPageIndexChanging="gvSupervisionRespuestas_PageIndexChanging"
                        OnSelectedIndexChanged="gvSupervisionRespuestas_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Pregunta" DataField="IdPregunta" />--%>
                            <asp:BoundField HeaderText="Respuesta" DataField="Valor" />
                            <asp:BoundField HeaderText="Valor vulnera la respuesta?" DataField="VulneraDerechoStr" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoStr" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
