<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_ReduccionesValor_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdConsModContractual" runat="server" />
<asp:HiddenField ID="hfIdReducciones" runat="server" />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfAlcPlan" runat="server" />
    <asp:HiddenField ID="hfEstado" runat="server" />
    <asp:HiddenField runat="server" ID="hfEsSubscripcion" />    
     <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Numero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminaci&oacute;n de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="320px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%" colspan="2">
                Valor total Reduccion 
            </td>
            <td style="width: 50%">
                 Valor Final del Contrato\Convenio con la Reduccion
            </td>
        </tr>
         <tr class="rowA">
            <td class="style1" colspan="2">
                <asp:TextBox runat="server" ID="txtvalorReduccion"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
              <asp:TextBox runat="server" ID="txtValorFinalContratoReduccion"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px"></asp:TextBox>
            </td>
        </tr>         
        <tr class="rowAB">
            <td colspan="3">
            </td>
            <td>
            </td>
        </tr>    
         <tr class="rowB">
            <td colspan="3">
                Plazo total de Ejecuci&oacute;n
            </td>
            <td>
                
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <strong>D&iacute;as</strong>          

            </td>
            <td>
                &nbsp;<asp:TextBox runat="server" ID="txtDias"  Enabled="false"></asp:TextBox></td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <strong>Meses</strong> &nbsp;  </td>
            <td>
                &nbsp;<asp:TextBox runat="server" ID="txtMeses"  Enabled="false"></asp:TextBox></td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <strong>A&ntilde;os</strong> 

            </td>
            <td>
                &nbsp;<asp:TextBox runat="server" ID="txtanos"  Enabled="false"></asp:TextBox></td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="3">
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="4">
                Justificaci&oacute;n de la novedad contractual
                 
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="4">
                <asp:TextBox runat="server" ID="txtJustificacion" TextMode="MultiLine" Height="73px" Width="95%" MaxLength="200" Enabled="false" ></asp:TextBox>
            </td>
        </tr>
         
        <tr class="rowB">
            <td colspan="4">
                Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="4">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>
                 <tr class="rowB">
            <td>
             Valores Actuales de Contrato
                </td>
        </tr>

        <tr class="rowB">
            <td colspan="4">
                 
                <asp:GridView ID="gvAportesActuales" runat="server" AllowSorting="true"  AutoGenerateColumns="false" CellPadding="8" 
                              DataKeyNames ="IdAporteContrato,EsAdicion,AporteEnDinero,AportanteICBF" GridLines="None" Height="16px"  Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Procedencia" HeaderText="Procedencia" SortExpression="Procedencia" />
                        <asp:BoundField DataField="TipoAporte" HeaderText="Tipo de Aporte" SortExpression="TipoAporte" />
                        <asp:BoundField DataField="ValorAporte" DataFormatString="{0:c}" HeaderText="Valor Total Aporte" SortExpression="ValorAporte" />
                        <asp:BoundField DataField="TipoModificacion" HeaderText="Tipo de Modificaci&oacute;n" SortExpression="TipoModificacion" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                 
                </td>
        </tr>
        <tr class="rowB">
            <td>

                Valores de la reducci&oacute;n</td>
        </tr>
        <tr class="rowB">
            <td colspan="4">

                <asp:GridView ID="gvAportesAdicion" runat="server" AllowSorting="true" OnRowDataBound="gvAportesAdicion_RowDataBound"  AutoGenerateColumns="false" CellPadding="8" 
                              DataKeyNames ="IdAporteContrato,EsAdicion,AporteEnDinero,AportanteICBF" GridLines="None" Height="16px"  Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" Visible="false"  runat="server" AutoPostBack="false"  >
                                <img alt="Editar" src="../../../Image/btn/info.jpg"   title="Editar" />
                            </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Procedencia" HeaderText="Procedencia" SortExpression="Procedencia" />
                        <asp:BoundField DataField="TipoAporte" HeaderText="Tipo de Aporte" SortExpression="TipoAporte" />
                        <asp:BoundField DataField="ValorAporte" DataFormatString="{0:c}" HeaderText="Valor Total Aporte" SortExpression="ValorAporte" />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                 
            </td>
        </tr>

    </table>

        <asp:Panel runat="server" ID="Panel3">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Documentos&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>


</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 342px;
        }
    </style>
</asp:Content>

