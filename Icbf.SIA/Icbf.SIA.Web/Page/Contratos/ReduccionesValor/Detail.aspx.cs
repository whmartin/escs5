using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_ReduccionesValor_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/ReduccionesValor";
    string TIPO_ESTRUCTURA = "ReduccionValor";
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();

            }
            else
                RemoveSessionParameter("Reducciones.Guardado");
            
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Reducciones.IdReduccion", hfIdReducciones.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());
        
        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Edit);

       
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {

        bool Eliminar = false;

        if (hfIdConsModContractual.Value != "")
        {
            if (Convert.ToInt32(hfIdConsModContractualGestion.Value) == (int)ConsModContractualesEstado.ENVIADA || Convert.ToInt32(hfIdConsModContractualGestion.Value) == (int)ConsModContractualesEstado.REGISTRO || Convert.ToInt32(hfIdConsModContractualGestion.Value) == (int)ConsModContractualesEstado.DEVUELTA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
                NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
                Eliminar = true;
            }
        }
        else
        {
            if (Convert.ToInt32(hfIdConsModContractualGestion.Value) == (int)ConsModContractualesEstado.ENVIADA)
            {
                EliminarRegistro();
                SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
                Eliminar = true;
            }
        }

        if (!Eliminar)
        {
            toolBar.MostrarMensajeError("No se puede Eliminar, Por favor validar el estado!");
        }
        //EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx?oP=E", false);
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());

            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            toolBar.MostrarBotonNuevo(false);
            int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
             hfIdContrato.Value = vIdContrato.ToString();
             int vIdConModContratual;

             if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
             {
                 vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                 RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                 hfIdConsModContractual.Value = vIdConModContratual.ToString();

                 var detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);
                 hfEstado.Value = detalleSolicitud.Estado;


                if (hfEstado.Value == "Registro" || hfEstado.Value == "Devuelta")
                     toolBar.MostrarBotonEditar(true);
                 else
                     toolBar.MostrarBotonEditar(false);
             }
             else
             {
                 vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                 RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                 hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();

                 if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                 {
                     hfEsSubscripcion.Value = "1";
                     RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                 }
                 else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                 {
                     hfEsSubscripcion.Value = "2";
                     RemoveSessionParameter("ConsModContractualGestion.EsReparto");
                 }

                 toolBar.MostrarBotonEditar(false);
             }
                                                 
            if (GetSessionParameter("Reducciones.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Reducciones.Guardado");

            Reducciones vReducciones = new Reducciones();
            ReduccionContrato itemContrato = new ReduccionContrato();

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            txtJustificacion.Text = vConsModContractual.Justificacion;

            int vIdReduccion = Convert.ToInt32(GetSessionParameter("Reducciones.IdReduccion"));
            RemoveSessionParameter("Reducciones.IdReduccion");
            hfIdReducciones.Value = vIdReduccion.ToString();

            vReducciones = vContratoService.ConsultarReducciones(vIdReduccion);

            itemContrato = vContratoService.ConsultarContratoReduccion(vIdContrato);

            var aportesReducciones = vContratoService.ObtenerAportesContratoAdicionReduccion(vIdReduccion, false);
            var valorReducciones = aportesReducciones.Sum(e => e.ValorAporte);
            txtvalorReduccion.Text = string.Format("{0:C}", valorReducciones);

            gvAportesAdicion.DataSource = aportesReducciones;
            gvAportesAdicion.DataBind();
                        
            if (hfEstado.Value == "Suscrita")
            {
                txtValorFinalContratoReduccion.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);
            }
            else
            {
                decimal totalValorConReduccion = itemContrato.ValorFinal - valorReducciones;
                txtValorFinalContratoReduccion.Text = string.Format("{0:$#,##0}", totalValorConReduccion);
            }
            
                                                                                                       
            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalizacionInicialContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            string calculo = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion, caFechaFinalizacionInicial, out calculo);

            var itemsFecha = calculo.Split('|');
            txtDias.Text = itemsFecha[2];
            txtMeses.Text = itemsFecha[1];
            txtanos.Text = itemsFecha[0];

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            gvAportesActuales.EmptyDataText = EmptyDataText();
            gvAportesActuales.PageSize = PageSize();

            var aportesValoresActuales = vContratoService.ObtenerAportesContratoActuales(Convert.ToInt32(hfIdContrato.Value));
            gvAportesActuales.DataSource = aportesValoresActuales;
            gvAportesActuales.DataBind();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdContrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
            gvanexos.DataBind();       
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            //toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            //toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Solcitud de Reducci&#243;n de valor", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarDatos();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarRegistro()
    {
        try
        {
            int vIdReduccion = Convert.ToInt32(hfIdReducciones);

            Reducciones vReduccion = new Reducciones();
            vReduccion = vContratoService.ConsultarReducciones(vIdReduccion);
            InformacionAudioria(vReduccion, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarReducciones(vReduccion);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("Reducciones.Eliminado", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion

    protected void gvAportesAdicion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdAporteContrato = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdAporteContrato").ToString());
            bool AportanteICBF = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AportanteICBF").ToString());
            bool AporteEnDinero = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AporteEnDinero").ToString());
            bool EsAdicion = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "EsAdicion").ToString());


            if (AportanteICBF)
            {
                if (AporteEnDinero)
                {
                    string valor = string.Format("GetDetallePlanCompras(); return false;");
                    button.Attributes.Add("onclick", valor);
                    button.Visible = true;
                }
            }
        }
    }
}
