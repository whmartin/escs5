using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Data;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición de tipos de garantía
/// </summary>
public partial class Page_ReduccionesValor_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    SIAService vSiaService = new SIAService();

    int vIdContrato;
    decimal vIdIndice = 0;

    string PageName = "Contratos/ReduccionesValor";
    string TIPO_ESTRUCTURA = "ReduccionValor";

    #region ConstantesConfiguracionControles
    #endregion
    
    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        string controlFechaValidando = string.Empty;
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        try
        {
            toolBar.LipiarMensajeError();
            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {
                if (!Page.IsPostBack)
                {
                    CargarDatosIniciales();
                    CargarRegistro();
                }
                else
                {
                    #region AdministraPostBack
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch (sControlName)
                    {
                        case "ActualizarValores":
                            ActualizarAportes();
                            break;
                        default:
                            break;
                    }
                    #endregion                 
                }
            }
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inválido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if(hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx?oP=E", false);
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {

            #region
            //int vResultado;
            //Reducciones vReducciones = new Reducciones();
            //vReducciones.IDDetalleConsModContractual = Convert.ToInt32(hfIdDetConsModContractual.Value);

            //decimal valorreduccion = 0;
            //decimal valorcontrato = 0;

            //string valContrato = new string(txtvalorfinal.Text.Where(char.IsDigit).ToArray());
            //string valReduccion = new string(txtvalorReduccion.Text.Where(char.IsDigit).ToArray());

            //valorcontrato = Convert.ToDecimal(valContrato);
            //valorreduccion = Convert.ToDecimal(valReduccion);                        
            //vReducciones.FechaReduccion = DateTime.Now;
            ////vReducciones.Justificacion = txtJustificacion.Text;

            ////if(valorreduccion  < valorcontrato)
            ////{
            //    vReducciones.ValorReduccion = valorreduccion; 

            ////}
            ////else
            ////{
            ////    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, el valor de la reducción no puede ser mayor que el valor del contrato.");
            ////}

            

            ////if (Request.QueryString["oP"] == "E")
            ////{
            //    vReducciones.IdReduccion = Convert.ToInt32(hfIdReducciones.Value);
            //    vReducciones.UsuarioModifica = GetSessionUser().NombreUsuario;
            //    InformacionAudioria(vReducciones, this.PageName, vSolutionPage);
            //    vResultado = vContratoService.ModificarReducciones(vReducciones);
            //}
            //else
            //{
            //    vReducciones.UsuarioCrea = GetSessionUser().NombreUsuario;
            //    InformacionAudioria(vReducciones, this.PageName, vSolutionPage);
            //    vResultado = vContratoService.InsertarReducciones(vReducciones);
            //}        

            //if (vResultado == 0)
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            //}
            //else if (vResultado == 1)
            //{
            //    //SetSessionParameter("Reducciones.IdReduccion", vReducciones.IdReduccion);
            //    //SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());
            //    SetSessionParameter("DetConsModContractual.Guardado", "1");
            //    if (hfIdConsModContractual.Value != "")
            //    {
            //        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            //        Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
            //    }
            //    else
            //    {
            //        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            //        Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
            //    }
            //}
            //else 
            //{
            //    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            //}
            #endregion

            if (!string.IsNullOrEmpty(hfIdReducciones.Value.ToString()))
            {
                SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
                Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
            }
            else
            {
                toolBar.MostrarMensajeError("Debe Modificar Primero los Productos.");
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Solcitud de Reducci&#243;n de valor", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            if (!string.IsNullOrEmpty( GetSessionParameter("Reducciones.IdReduccion").ToString()) || !string.IsNullOrEmpty(hfIdReducciones.Value))
            {
                var itemContrato = vContratoService.ConsultarContratoReduccion(Convert.ToInt32(hfIdContrato.Value));

                int vIdReduccion = 0;

                if (!string.IsNullOrEmpty(GetSessionParameter("Reducciones.IdReduccion").ToString()))
                {
                    vIdReduccion = Convert.ToInt32(GetSessionParameter("Reducciones.IdReduccion"));
                    RemoveSessionParameter("Reducciones.IdReduccion");
                    hfIdReducciones.Value = vIdReduccion.ToString();
                }
                else
                    vIdReduccion = int.Parse(hfIdReducciones.Value);
                
                Reducciones vReducciones = new Reducciones();

                vReducciones = vContratoService.ConsultarReducciones(Convert.ToInt32(hfIdReducciones.Value));

                var aporteReducciones = vContratoService.ObtenerAportesContratoAdicionReduccion(vIdReduccion, false);
                var valorReducciones = aporteReducciones.Sum(e => e.ValorAporte);

                txtvalorReduccion.Text = string.Format("{0:C}", valorReducciones);
                gvAportesAdicion.DataSource = aporteReducciones;
                gvAportesAdicion.DataBind();

                decimal totalValorConReduccion = itemContrato.ValorFinal - valorReducciones;
                txtValorFinalContratoReduccion.Text = string.Format("{0:$#,##0}", totalValorConReduccion);
            }

            var aportesContrato = vContratoService.ObtenerAportesContrato(Convert.ToInt32(hfIdContrato.Value), false);
            List<AporteContrato> misAportes = new List<AporteContrato>();
            foreach (var item in aportesContrato)
            {
                if (!misAportes.Any(e => e.AportanteICBF == item.AportanteICBF && e.AporteEnDinero == item.AporteEnDinero))
                    misAportes.Add(item);
            }

            gvAportesICBF.DataSource = misAportes;
            gvAportesICBF.DataBind();


            var aportesValoresActuales = vContratoService.ObtenerAportesContratoActuales(Convert.ToInt32(hfIdContrato.Value));
            gvAportesActuales.DataSource = aportesValoresActuales;
            gvAportesActuales.DataBind();


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();
            var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            int vIdConModContratual;

            if (GetSessionParameter("ConsModContractual.IDCosModContractual") != null)
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();
            }
            else
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();
            }

            if (Request.QueryString["oP"] == "E")
            {                
                    int vIdReduccion = Convert.ToInt32(GetSessionParameter("Reducciones.IdReduccion"));
                    RemoveSessionParameter("Reducciones.IdReduccion");
                    hfIdReducciones.Value = vIdReduccion.ToString();
                    Reducciones vReducciones = new Reducciones();

                    vReducciones = vContratoService.ConsultarReducciones(vIdReduccion);
                    txtvalorReduccion.Text = string.Format("{0:$#,##0}", vReducciones.ValorReduccion);              
                    decimal totalValorConReduccion = itemContrato.ValorFinal - vReducciones.ValorReduccion;        
                    toolBar.MostrarBotonNuevo(false);
                    PanelArchivos.Visible = true;
            }
            else
            {
                int vIdDetConModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                hfIdDetConsModContractual.Value = vIdDetConModContractual.ToString();
            }

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            txtJustificacion.Text = vConsModContractual.Justificacion;

            //var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            DateTime FechaMinima = Convert.ToDateTime("01-01-1900").Date;

            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalizacionInicialContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            string calculo = string.Empty;

            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion, caFechaFinalizacionInicial, out calculo);

            var itemsFecha = calculo.Split('|');
            txtDias.Text = itemsFecha[2];
            txtMeses.Text = itemsFecha[1];
            txtAnos.Text = itemsFecha[0];

            //CalcularFechas(caFechaInicioEjecucion, caFechaFinalizacionInicial);           

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdCntrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
            gvanexos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion

    #region Manejo de Aportes

    protected void gvAportesICBF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdAporteContrato = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdAporteContrato").ToString());
            bool AportanteICBF = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AportanteICBF").ToString());
            bool AporteEnDinero = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AporteEnDinero").ToString());
            bool EsAdicion = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "EsAdicion").ToString());

            if (!EsAdicion)
            {
                if (AportanteICBF)
                {
                    if (AporteEnDinero)
                    {
                        string valor = string.Format("GetPlanComprasNuevoV11('{0}'); return false;", IdAporteContrato.ToString());
                        button.Attributes.Add("onclick", valor);
                    }
                    else
                    {
                        string valor = string.Format("GetAportes('{0}'); return false;", IdAporteContrato.ToString());
                        button.Attributes.Add("onclick", valor);
                    }
                }
                else
                {
                    string valor = string.Format("GetAportes('{0}'); return false;", IdAporteContrato.ToString());
                    button.Attributes.Add("onclick", valor);
                }
            }
            else
                button.Visible = false;
        }
    }

    private void ActualizarAportes()
    {
        if (!string.IsNullOrEmpty(GetSessionParameter("Reducciones.IdReduccion").ToString()))
        {
            hfIdReducciones.Value = GetSessionParameter("Reducciones.IdReduccion").ToString();
            RemoveSessionParameter("Reducciones.IdReduccion");
        }

        if (!string.IsNullOrEmpty(GetSessionParameter("Reducciones.Guardo").ToString()))
        {
            toolBar.MostrarMensajeGuardado("Se guardo el aporte correctamente");
            RemoveSessionParameter("Reducciones.Guardo");
        }
        if (!string.IsNullOrEmpty(hfIdReducciones.Value))
        {
            int vIdReduccion = int.Parse(hfIdReducciones.Value);
            var aportesAdiciones = vContratoService.ObtenerAportesContratoAdicionReduccion(vIdReduccion, false);
            var valorAdiciones = aportesAdiciones.Sum(e => e.ValorAporte);
            txtvalorReduccion.Text = string.Format("{0:C}", valorAdiciones);

            decimal totalValorConAdicion = decimal.Parse(txtvalorfinal.Text.Replace("$",string.Empty)) - valorAdiciones;
            txtValorFinalContratoReduccion.Text = string.Format("{0:C}", totalValorConAdicion);

            gvAportesAdicion.DataSource = aportesAdiciones;
            gvAportesAdicion.DataBind();
        }
    }

    #endregion

    #region Aportes Adiciones

    protected void gvAportesAdicion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdAporteContrato = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdAporteContrato").ToString());
            bool AportanteICBF = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AportanteICBF").ToString());
            bool AporteEnDinero = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AporteEnDinero").ToString());
            bool EsAdicion = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "EsAdicion").ToString());


            if (AportanteICBF)
            {
                if (AporteEnDinero)
                {
                    string valor = string.Format("GetDetallePlanCompras(); return false;");
                    button.Attributes.Add("onclick", valor);
                    button.Visible = true;
                }
            }
        }
    }

    #endregion
}
