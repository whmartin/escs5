<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Obligacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdObligacion" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Tipo de Obligaci&#243;n *
            </td>
            <td style="width: 50%">
                Tipo de Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:DropDownList runat="server"  ID="ddlIdTipoObligacion"  Enabled="false" 
                    Height="22px" Width="349px"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoContrato"  Enabled="false" 
                    Height="22px" Width="362px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Contenido de la Obligaci&#243;n
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtDescripcion"  Enabled="false" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 393px;
        }
    </style>
</asp:Content>

