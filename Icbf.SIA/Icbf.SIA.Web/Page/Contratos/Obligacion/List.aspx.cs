using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de obligaciones
/// </summary>
public partial class Page_Obligacion_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Obligacion";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdTipoObligacion = null;
            int? vIdTipoContrato = null;
            Boolean? vEstado = null;
            String vDescripcion = null;
            if (ddlIdTipoObligacion.SelectedValue != "-1")
            {
                vIdTipoObligacion = Convert.ToInt32(ddlIdTipoObligacion.SelectedValue);
            }
            if (ddlIdTipoContrato.SelectedValue != "-1")
            {
                vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            }
            if (txtDescripcion.Text != "")
            {
                vDescripcion = Convert.ToString(txtDescripcion.Text);
            }

            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }

            var result = from obligacion in vContratoService.ConsultarObligacions(vDescripcion, vIdTipoObligacion, vIdTipoContrato, vEstado)
                         join tipoObligacion in vContratoService.ConsultarTipoObligacions(null, null, true)
                         on obligacion.IdTipoObligacion equals tipoObligacion.IdTipoObligacion
                         join tipoContrato in vContratoService.ConsultarTipoContratos(null, null, true, null, null, null, null, null)
                         on obligacion.IdTipoContrato equals tipoContrato.IdTipoContrato
                         orderby tipoContrato.NombreTipoContrato, obligacion.Descripcion
                         select new
                         {
                             tipoContrato.NombreTipoContrato,
                             obligacion.Descripcion,
                             obligacion.IdObligacion,
                             tipoObligacion.NombreTipoObligacion,
                             obligacion.Estado
                         };

            List<datosGridView> listaObligacion = new List<datosGridView>();

            foreach (var datosResult in result)
            {
                datosGridView objDatosGridView = new datosGridView();
                objDatosGridView.IdObligacion = datosResult.IdObligacion;
                objDatosGridView.NombreTipoObligacion = datosResult.NombreTipoObligacion;
                objDatosGridView.NombreTipoContrato = datosResult.NombreTipoContrato;
                objDatosGridView.Descripcion = datosResult.Descripcion;


                if (datosResult.Estado)
                {
                    objDatosGridView.EstadoString = "Activo";
                }
                else
                {
                    objDatosGridView.EstadoString = "Inactivo";
                }
                listaObligacion.Add(objDatosGridView);
            }

            gvObligacion.DataSource = listaObligacion.ToList();
            gvObligacion.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            gvObligacion.PageSize = PageSize();
            gvObligacion.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Obligaci&#243;n", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvObligacion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Obligacion.IdObligacion", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvObligacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvObligacion.SelectedRow);
    }
    protected void gvObligacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvObligacion.PageIndex = e.NewPageIndex;
        Buscar();
    }


    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Obligacion.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("Obligacion.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService objContratoServices = new ContratoService();
            ddlIdTipoObligacion.DataSource = objContratoServices.ConsultarTipoObligacions(null, null, true);
            ddlIdTipoObligacion.DataTextField = "NombreTipoObligacion";
            ddlIdTipoObligacion.DataValueField = "IdTipoObligacion";
            ddlIdTipoObligacion.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoObligacion);
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlIdTipoContrato.DataSource = objContratoServices.ConsultarTipoContratos(null, null, true, null, null, null, null, null);
            ddlIdTipoContrato.DataTextField = "NombreTipoContrato";
            ddlIdTipoContrato.DataValueField = "IdTipoContrato";
            ddlIdTipoContrato.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoContrato);
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            rblEstado.SelectedIndex = -1;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvObligacion_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        int? vIdTipoObligacion = null;
        int? vIdTipoContrato = null;
        Boolean? vEstado = null;
        String vDescripcion = null;
        if (ddlIdTipoObligacion.SelectedValue != "-1")
        {
            vIdTipoObligacion = Convert.ToInt32(ddlIdTipoObligacion.SelectedValue);
        }
        if (ddlIdTipoContrato.SelectedValue != "-1")
        {
            vIdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
        }
        if (txtDescripcion.Text != "")
        {
            vDescripcion = Convert.ToString(txtDescripcion.Text);
        }
        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }
        var myGridResults = from obligacion in vContratoService.ConsultarObligacions(vDescripcion, vIdTipoObligacion, vIdTipoContrato, vEstado)
                            join tipoObligacion in vContratoService.ConsultarTipoObligacions(null, null, true)
                            on obligacion.IdTipoObligacion equals tipoObligacion.IdTipoObligacion
                            join tipoContrato in vContratoService.ConsultarTipoContratos(null, null, true, null, null, null, null, null)
                            on obligacion.IdTipoContrato equals tipoContrato.IdTipoContrato
                            select new
                            {
                                obligacion.IdObligacion,
                                tipoObligacion.NombreTipoObligacion,
                                tipoContrato.NombreTipoContrato,
                                obligacion.Descripcion,
                                obligacion.Estado
                            };

        List<datosGridView> listaObligacion = new List<datosGridView>();

        foreach (var datosResult in myGridResults)
        {
            datosGridView objDatosGridView = new datosGridView();
            objDatosGridView.IdObligacion = datosResult.IdObligacion;
            objDatosGridView.NombreTipoObligacion = datosResult.NombreTipoObligacion;
            objDatosGridView.NombreTipoContrato = datosResult.NombreTipoContrato;
            objDatosGridView.Descripcion = datosResult.Descripcion;


            if (datosResult.Estado)
            {
                objDatosGridView.EstadoString = "Activo";
            }
            else
            {
                objDatosGridView.EstadoString = "Inactivo";
            }
            listaObligacion.Add(objDatosGridView);
        }

        if (myGridResults != null)
        {
            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                switch (e.SortExpression)
                {
                    case ("NombreTipoObligacion"):
                        listaObligacion = listaObligacion.OrderBy(x => x.NombreTipoObligacion).ToList();
                        break;
                    case ("NombreTipoContrato"):
                        listaObligacion = listaObligacion.OrderBy(x => x.NombreTipoContrato).ToList();
                        break;
                    case ("Descripcion"):
                        listaObligacion = listaObligacion.OrderBy(x => x.Descripcion).ToList();
                        break;
                    case ("EstadoString"):
                        listaObligacion = listaObligacion.OrderBy(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvObligacion.DataSource = listaObligacion;
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                switch (e.SortExpression)
                {
                    case ("NombreTipoObligacion"):
                        listaObligacion = listaObligacion.OrderByDescending(x => x.NombreTipoObligacion).ToList();
                        break;
                    case ("NombreTipoContrato"):
                        listaObligacion = listaObligacion.OrderByDescending(x => x.NombreTipoContrato).ToList();
                        break;
                    case ("Descripcion"):
                        listaObligacion = listaObligacion.OrderByDescending(x => x.Descripcion).ToList();
                        break;
                    case ("EstadoString"): 
                        listaObligacion = listaObligacion.OrderByDescending(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvObligacion.DataSource = listaObligacion;
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvObligacion.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}

public class datosGridView
{
    public int IdObligacion { get; set; }
    public String NombreTipoObligacion { get; set; }
    public String NombreTipoContrato { get; set; }
    public String Descripcion { get; set; }
    public String EstadoString { get; set; }

    public datosGridView()
    {

    }
}
