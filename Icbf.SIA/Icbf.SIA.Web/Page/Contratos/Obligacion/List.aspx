<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Obligacion_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="style1" style="width: 50%">
                    Tipo de Obligaci&#243;n
                </td>
                <td style="width: 50%">
                    Tipo de Contrato
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:DropDownList runat="server" ID="ddlIdTipoObligacion" Height="22px" Width="303px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdTipoContrato" Height="22px" Width="339px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="style1">
                    Contenido de la Obligaci&#243;n
                </td>
                <td colspan="2">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" TextMode="MultiLine"
                        Width="320px" onKeyDown="limitText(this,128);" 
                        onKeyUp="limitText(this,128);"></asp:TextBox>
                    <%--<Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; @"
                        InvalidChars="&#63;" />--%>
                    <Ajax:FilteredTextBoxExtender ID="ftDescripcion" runat="server" TargetControlID="txtDescripcion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
                </td>
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvObligacion" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdObligacion" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvObligacion_PageIndexChanging" OnSelectedIndexChanged="gvObligacion_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="gvObligacion_Sorting">
                        <Columns>
                            <%--<asp:BoundField HeaderText="Tipo Contrato" DataField="NombreTipoContrato" SortExpression="NombreTipoContrato"/>--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" Height="16px" 
                                        ImageUrl="~/Image/btn/info.jpg" ToolTip="Detalle" Width="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo Contrato" 
                                ItemStyle-HorizontalAlign="Center" SortExpression="NombreTipoContrato">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("NombreTipoContrato")%>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contenido de la obligación" 
                                ItemStyle-HorizontalAlign="Center" SortExpression="Descripcion">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Descripcion")%>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="NombreTipoObligacion" 
                                HeaderText="Tipo De Obligación" SortExpression="NombreTipoObligacion" />
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 315px;
        }
    </style>
</asp:Content>
