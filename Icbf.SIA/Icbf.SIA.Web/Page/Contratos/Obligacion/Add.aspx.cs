using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de obligaciones
/// </summary>
public partial class Page_Obligacion_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/Obligacion";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            Obligacion vObligacion = new Obligacion();

            vObligacion.IdTipoObligacion = Convert.ToInt32(ddlIdTipoObligacion.SelectedValue);
            vObligacion.IdTipoContrato = Convert.ToInt32(ddlIdTipoContrato.SelectedValue);
            vObligacion.Descripcion = Convert.ToString(txtDescripcion.Text);
            vObligacion.Estado = rblEstado.SelectedValue == "1" ? true : false;

            if (Request.QueryString["oP"] ==  "E")
            {
            vObligacion.IdObligacion = Convert.ToInt32(hfIdObligacion.Value);
                vObligacion.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarObligacion(vObligacion);
            }
            else
            {
                vObligacion.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vObligacion, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarObligacion(vObligacion);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Obligacion.IdObligacion", vObligacion.IdObligacion);

                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("Obligacion.Modificado", "1");
                    toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("Obligacion.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
               
            }
            else if (vResultado == 4)
            {
                toolBar.MostrarMensajeError(
                    "Registro duplicado, verifique por favor.");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Obligaci&#243;n", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdObligacion = Convert.ToInt32(GetSessionParameter("Obligacion.IdObligacion"));

            int vIdObligacion = 0;

            if (int.TryParse(GetSessionParameter("Obligacion.IdObligacion").ToString(), out vIdObligacion))
            {
                RemoveSessionParameter("Obligacion.Id");

                Obligacion vObligacion = new Obligacion();
                vObligacion = vContratoService.ConsultarObligacion(vIdObligacion);
                hfIdObligacion.Value = vObligacion.IdObligacion.ToString();
                ddlIdTipoObligacion.SelectedValue = vObligacion.IdTipoObligacion.ToString();
                ddlIdTipoContrato.SelectedValue = vObligacion.IdTipoContrato.ToString();
                txtDescripcion.Text = vObligacion.Descripcion;
                rblEstado.SelectedValue = vObligacion.Estado == true ? "1" : "0";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vObligacion.UsuarioCrea, vObligacion.FechaCrea, vObligacion.UsuarioModifica, vObligacion.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ContratoService objContratoServices = new ContratoService();
            ddlIdTipoObligacion.DataSource = objContratoServices.ConsultarTipoObligacions(null, null, true);
            ddlIdTipoObligacion.DataTextField = "NombreTipoObligacion";
            ddlIdTipoObligacion.DataValueField = "IdTipoObligacion";
            ddlIdTipoObligacion.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoObligacion);
            
            ddlIdTipoContrato.DataSource = objContratoServices.ConsultarTipoContratos(null, null, true, null, null, null, null, null);
            ddlIdTipoContrato.DataTextField = "NombreTipoContrato";
            ddlIdTipoContrato.DataValueField = "IdTipoContrato";
            ddlIdTipoContrato.DataBind();
            ManejoControlesContratos.InicializarCombo(ddlIdTipoContrato);
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
