<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Precontractual_Lupas_LupaDocumentos" %>

<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfValor" runat="server" />
    <asp:HiddenField ID="HfAporte" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Documento
                a Cargar</td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
               <asp:FileUpload Width="100%" ID="FileUploadArchivoContrato" runat="server" /> 
            </td>
        </tr>
        </table>
       <table width="90%" align="center" id="pnlResultados" runat="server" style="display:none" >
<%--       <tr class="rowB">
            <td colspan="2">
                Descripción
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtDescripcion" TextMode="MultiLine" Width="500px" Height="50px" />
                </td>
            </tr>--%>
        <tr class="rowB">
            <td colspan="2">
                Resultados
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                    <asp:GridView runat="server" ID="gvCargueContratos" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" 
                        CellPadding="0" Height="16px"
                        
                        AllowSorting="True">
                        <Columns>
                            <asp:BoundField HeaderText="Total Registros" DataField="TotalRegistros"  />
                            <asp:BoundField HeaderText="Registros Cargados" DataField="RegistrosCargados"  />
                            <asp:BoundField HeaderText="Registros Errados" DataField="RegistrosErrados"  />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
            </td>
        </tr>
    <script type="text/javascript" language="javascript">
        
    </script>
    </table>
</asp:Content>
