using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.IO;

/// <summary>
/// Página de registro de los documentos de una solicitud de un contrato.
/// </summary>
public partial class Page_Precontractual_Lupas_LupaDocumentos : GeneralWeb
{
    masterPrincipal toolBar;

    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/CargueContratosMasivo";
    PreContractualService vPrecontractualService = new PreContractualService();

    public List<itemCargueError> ItemsError
    {
        get
        {
            List<itemCargueError> result = new List<itemCargueError>();

            if (ViewState["itemsError"]!= null)
                result = ViewState["itemsError"] as List<itemCargueError>;

            return result;
        }
        set
        {
            ViewState["itemsError"] = value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {        
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        Guardar();
    }
    
   /// <summary>
        /// Método de guardado (nuevo y edición) para la entidad AporteContrato
        /// </summary>
    private void Guardar()
    {
        try
        {
            if (FileUploadArchivoContrato.HasFile)
            {
               var result = vContratoService.ValidarArchivo(FileUploadArchivoContrato.FileContent, GetSessionUser().Usuario);

               if (result != null)
               {
                   List<CargueContratoMasivo> resultList = new List<CargueContratoMasivo>();
                   resultList.Add(result);
                   gvCargueContratos.DataSource = resultList;
                   gvCargueContratos.DataBind();
                   pnlResultados.Style.Add("display","");

                   if (result.ListaRegistroErrados.Count > 0)
                   {
                       toolBar.MostarBotonReporte(true);
                       toolBar.MostrarBotonAprobar(false);
                       ItemsError = result.ListaRegistroErrados;
                   }
               }
            }
            else
                toolBar.MostrarMensajeError("Debe Cargar el Archivo, relacionado.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
        /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            toolBar.MostarBotonReporte(false);
            toolBar.EstablecerTitulos("Cargue Masivo de Contratos", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnReporte_Click(object sender, EventArgs e)
    {
        try
        {
            var reportePath = Server.MapPath("~/General/General/Report/PlantillaValidacionCargueMasivo.xlsx");

            var result = vContratoService.GenerarCargueArchivoValidaciones(ItemsError, reportePath);

            if (result != string.Empty)
            {
                string archivoLocal = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["ReportLocalPath"], result);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(archivoLocal);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + result);
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(archivoLocal);
                Response.End();
                toolBar.MostrarMensajeGuardado("Se genero el reporte correctamente");
            }
            else
            {
                toolBar.MostrarMensajeError("Los parametros seleccionados no generarón información, Por favor verifique.");
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
   /// Método de carga de listas y valores por defecto 
   /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
