using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de Categor�a contrato
/// </summary>
public partial class Page_CargaMasivaContratos_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/CargueContratosMasivo";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }

        }
    }

    /// <summary>
    /// Manejador de eventos click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            gvCargueContratos.PageSize = PageSize();
            gvCargueContratos.EmptyDataText = EmptyDataText();
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Carga Masiva de Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvCargueContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCargueContratos.PageIndex = e.NewPageIndex;
        CargarCargues();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarCargues()
    {
        int? idRegional = null;

        if (GetSessionUser().IdTipoUsuario != 1)
            idRegional = GetSessionUser().IdRegional;

        var myGridResults = vContratoService.ConsultarCargueContratosArchivos(idRegional);
        gvCargueContratos.DataSource = myGridResults;
        gvCargueContratos.DataBind();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarCargues();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    public void Ordenar(GridViewSortEventArgs e)
    {
        int? idRegional = null;

        if (GetSessionUser().IdTipoUsuario != 1)
            idRegional = GetSessionUser().IdRegional;

        var myGridResults = vContratoService.ConsultarCargueContratosArchivos(idRegional);

        if (myGridResults != null)
        {
            var param = Expression.Parameter(typeof(CargueContratoMasivo), e.SortExpression);

            var prop = Expression.Property(param, e.SortExpression);

            var sortExpression = Expression.Lambda<Func<CargueContratoMasivo, object>>(Expression.Convert(prop, typeof(object)), param);

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                gvCargueContratos.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                gvCargueContratos.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvCargueContratos.DataBind();
        }
    }
}
