﻿using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Contratos_OficioLegalizacion_Iist : GeneralWeb
{

    masterPrincipal toolBar;
    string PageName = "Contratos/OficioLegalizacion";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += ToolBar_eventoBuscar;

            toolBar.EstablecerTitulos("Oficio de Legalización", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ToolBar_eventoBuscar(object sender, EventArgs e)
    {
        buscar();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {

            }
        }
    }

    protected void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {

            int rowIndex = pRow.RowIndex;
            string strValue = gvGarantias.DataKeys[rowIndex]["IDGarantia"].ToString();
            string valueIDContrato = gvGarantias.DataKeys[rowIndex]["IDContrato"].ToString();
            SetSessionParameter("RptCertificacionesGarantia.IdGarantia", strValue);
            SetSessionParameter("RptCertificacionesGarantia.IdContrato", valueIDContrato);
            //AGREGAR PARAM IDCONTRATO
            Response.Redirect("GenerarRptOficio.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvGarantias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvGarantias.SelectedRow);
    }
    protected void gvGarantias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvGarantias.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvGarantias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    private void buscar()
    {
        try
        {
            CargarGrilla(gvGarantias, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    //int? vIdContrato = null;

    //if (!string.IsNullOrEmpty(txtNumeroContrato.Text) && string.IsNullOrEmpty(txtIdContrato.Text))
    //{
    //    var Contratos = vContratoService.ConsultarContratosSimple(null, null, null, txtNumeroContrato.Text, null, null, null, null
    //        , null, null, null, null, null);
    //    if (Contratos.Count() > 0)
    //        vIdContrato = Contratos[0].IdContrato;
    //    else
    //        vIdContrato = Convert.ToInt32(txtNumeroContrato.Text);
    //}

    //if (string.IsNullOrEmpty(txtNumeroContrato.Text) && !string.IsNullOrEmpty(txtIdContrato.Text))
    //{
    //    vIdContrato = Convert.ToInt32(txtIdContrato.Text);
    //}

    //if (!string.IsNullOrEmpty(txtNumeroContrato.Text) && !string.IsNullOrEmpty(txtIdContrato.Text))
    //{
    //    vIdContrato = Convert.ToInt32(txtIdContrato.Text);
    //}

    //List<Garantia> itemsGarantia = vContratoService.ConsultarGarantias(null, null, null, null, null, null, null, null, vIdContrato, null, null, null, null, null,
    //    null, null, null, null, null, null, null, null, null, null, "SUS ", null);

    //decimal valor = 0;

    //IEnumerable<Garantia> itemsResult = null;

    //if (itemsGarantia != null && itemsGarantia.Count() > 0)
    //{

    //    var itemsValidos = itemsGarantia.Where(e => e.CodigoEstadoGarantia == "002" ||
    //                                        e.CodigoEstadoGarantia == "003");

    //    if (itemsValidos != null)
    //        itemsResult = itemsValidos.ToList();
    //}

    //gvGarantias.DataSource = itemsResult;
    //gvGarantias.DataBind();

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {

            int? vIdContrato = null;

            if (!string.IsNullOrEmpty(txtNumeroContrato.Text) && string.IsNullOrEmpty(txtIdContrato.Text))
            {
                var Contratos = vContratoService.ConsultarContratosSimple(null, null, null, txtNumeroContrato.Text, null, null, null, null
                    , null, null, null, null, null);
                if (Contratos.Count() > 0)
                    vIdContrato = Contratos[0].IdContrato;
                else
                    vIdContrato = Convert.ToInt32(txtNumeroContrato.Text);
            }

            if (string.IsNullOrEmpty(txtNumeroContrato.Text) && !string.IsNullOrEmpty(txtIdContrato.Text))
            {
                vIdContrato = Convert.ToInt32(txtIdContrato.Text);
            }

            if (!string.IsNullOrEmpty(txtNumeroContrato.Text) && !string.IsNullOrEmpty(txtIdContrato.Text))
            {
                vIdContrato = Convert.ToInt32(txtIdContrato.Text);
            }

            List<Garantia> myGridResults = vContratoService.ConsultarGarantias(null, null, null, null, null, null, null, null, vIdContrato, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, "SUS ;EJE ;CRP ", null);

            decimal valor = 0;
            //List<Garantia> myGridResults = null;

            //if (itemsGarantia.Count() == 0)
            //    myGridResults = itemsGarantia;
            //else
            //{
            //    List<Garantia> itemsResult = null;

            //    if (itemsGarantia != null && itemsGarantia.Count() > 0)
            //    {

            //        var itemsValidos = itemsGarantia.Where(e => e.CodigoEstadoGarantia == "002" ||
            //                                            e.CodigoEstadoGarantia == "003");

            //        if (itemsValidos != null)
            //            itemsResult = itemsValidos.ToList();
            //    }
            //    myGridResults = itemsResult;
            //}
           
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Garantia), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Garantia, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

}