﻿<%@ Page Language="C#" MasterPageFile="~/General/General/Master/main2.master" 
    AutoEventWireup="true" CodeFile="GenerarRptOficio.aspx.cs" Inherits="Page_Contratos_OficioLegalizacion_GenerarRptOficio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
     <asp:HiddenField ID="HfIdGarantia" runat="server" />
     <asp:HiddenField ID="HfIdContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <%--<td>Código del area</td>--%>
            <td class="style1" style="width: 50%">
                Código del area *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoArea" ControlToValidate="txtCodigoArea"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <%--<td>Vinculo SECOP</td>--%>
             <td class="style1" style="width: 50%">
                Vinculo SECOP *
                <asp:RequiredFieldValidator runat="server" ID="rfvVinculo" ControlToValidate="txtVinculo"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
             <td>
                <asp:TextBox runat="server" ID="txtCodigoArea" Width="200px" MaxLength="10"></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftCodigoArea" runat="server" TargetControlID="txtCodigoArea"
                                              FilterType="Numbers"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtVinculo" Width="200px" MaxLength="200"></asp:TextBox>
                 <Ajax:FilteredTextBoxExtender ID="ftVinculo" runat="server" TargetControlID="txtVinculo"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
        </tr>        
        <tr class="rowB">            
            <%--<td>Nombre Usuario Firma</td>--%>
            <td class="style1" style="width: 50%">
               Nombre Usuario Firma *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreUusario" ControlToValidate="txtNombreUsuario"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <%--<td>Cargo Usuario Firma</td>--%>
            <td class="style1" style="width: 50%">
               Cargo Usuario Firma *
                <asp:RequiredFieldValidator runat="server" ID="rfvCargoUsuario" ControlToValidate="txtCargoUsuario"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombreUsuario" Width="200px" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreUsuario" runat="server" TargetControlID="txtNombreUsuario"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCargoUsuario" Width="200px" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCargoUsuario" runat="server" TargetControlID="txtCargoUsuario"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
        </tr>
        <tr class="rowB">
            <%--<td>Aprobado por</td>--%>
            <td class="style1" style="width: 50%">
               Aprobado por *
                <asp:RequiredFieldValidator runat="server" ID="rfvAprobado" ControlToValidate="txtAprobado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnReporte"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtAprobado" Width="200px" MaxLength="50"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftAprobado" runat="server" TargetControlID="txtAprobado"
                                              FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ,.;:!=?¡¿#$%&/()=¿?+*-´'\ " />
            </td>
        </tr>
    </table>
</asp:Content>
