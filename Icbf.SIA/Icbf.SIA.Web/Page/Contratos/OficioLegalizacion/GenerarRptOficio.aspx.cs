﻿using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Contratos_OficioLegalizacion_GenerarRptOficio : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/OficioLegalizacion";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;

        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                string vIDCertificacion = GetSessionParameter("RptCertificacionesGarantia.IdGarantia").ToString();
                string vIDOficio = GetSessionParameter("RptCertificacionesGarantia.IdContrato").ToString();
                RemoveSessionParameter("RptCertificacionesGarantia.IdGarantia");
                RemoveSessionParameter("RptCertificacionesGarantia.IdContrato");
                HfIdGarantia.Value = vIDCertificacion;
                HfIdContrato.Value = vIDOficio;

            }
        }

        HabilitarCampoVinculo();
    }

    private void HabilitarCampoVinculo()
    {
        try
        {
            Contrato contrato = vContratoService.ConsultarContrato(Convert.ToInt32(HfIdContrato.Value));

            if (contrato.VinculoSECOP == null || contrato.VinculoSECOP == "")
            {
                txtVinculo.Enabled = true;
            }
            else
            {
                txtVinculo.Text = contrato.VinculoSECOP;
                txtVinculo.Enabled = true;                
            }
           

            //
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }
    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);       
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.eventoReporte += ToolBar_eventoReporte;
            toolBar.EstablecerTitulos("Oficio de legalizaci&oacute;n", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void ToolBar_eventoReporte(object sender, EventArgs e)
    {
        GenerarReporte();
    }

    private void GenerarReporte()
    {

        try
        {
            #region Creacion reporte


            //Crea un objeto de tipo reporte
            Report objReport;
            objReport = new Report("OficioLegalizacion", true, PageName, "Oficio Legalización");


            //Adiciona los parametros al objeto reporte
            objReport.AddParameter("IDGarantia", HfIdGarantia.Value);
            objReport.AddParameter("CodArea", txtCodigoArea.Text);
            objReport.AddParameter("VinculoSECOP", txtVinculo.Text);
            objReport.AddParameter("NombreFirma", txtNombreUsuario.Text.ToUpper());
            objReport.AddParameter("CargoFirma", txtCargoUsuario.Text.ToUpper());
            objReport.AddParameter("UsuarioAprobo", txtAprobado.Text.ToUpper());
            objReport.AddParameter("UsuarioGenero", GetSessionUser().NombreUsuario);

            //Crea un session con el objeto Reporte
            SetSessionParameter("Report", objReport);

            if (! string.IsNullOrEmpty(HfIdContrato.Value))
            {
                int idContrato = int.Parse(HfIdContrato.Value);
                vContratoService.ActualizarVinculoSECOP(idContrato, txtVinculo.Text);
            }

            //Abre la pagina de visor de reportes
            NavigateTo("~/General/General/Report/ReportViewer.aspx", false);
            #endregion

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }


    }
}