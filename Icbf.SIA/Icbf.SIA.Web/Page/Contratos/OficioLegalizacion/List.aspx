﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" 
    CodeFile="List.aspx.cs" 
    Inherits="Page_Contratos_OficioLegalizacion_Iist" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>N&uacute;mero del Contrato / Convenio
                </td>
                <td>Id Contrato/Convenio</td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroContrato"  MaxLength="20" Width="200px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="20" Width="200px"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" />
                </td>
            </tr>

        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <div style="overflow-x: auto; width: 100%">
                        <asp:GridView ID="gvGarantias" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CellPadding="0" DataKeyNames="IDGarantia,IDSucursalAseguradoraContrato,CodigoEstadoGarantia,IDContrato" 
                            GridLines="None" Height="16px" OnPageIndexChanging="gvGarantias_PageIndexChanging" OnSelectedIndexChanged="gvGarantias_SelectedIndexChanged"
                             Width="100%" AllowSorting="True" OnSorting="gvGarantias_Sorting" EmptyDataText="No se encontraron datos, por favor verifique.">
                            <Columns>
                                <asp:TemplateField HeaderText="Acciones">
                                    <HeaderTemplate>
                                        Opciones:
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImgRptGarantia" runat="server" CommandName="Select"
                                            Height="16px"
                                            Width="16px"
                                            ImageUrl="~/Image/btn/Report.png"
                                            ToolTip="Ver Reporte"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdContrato" HeaderText="Id Contrato" SortExpression="IdContrato" />
                                <asp:BoundField DataField="NumeroContrato" HeaderText="Número de Contrato" SortExpression="NumeroContrato" />
                                <asp:BoundField DataField="FechaSuscripcionContrato" HeaderText="Fecha de Suscripción" DataFormatString="{0:d}" SortExpression="FechaSuscripcionContrato" />
                                <asp:BoundField DataField="NombreSupervisor" HeaderText="Nombre del supervisor" SortExpression="NombreSupervisor" />
                                <asp:BoundField DataField="DescEstadoContrato" HeaderText="Estado" SortExpression="DescEstadoContrato" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
       
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>

