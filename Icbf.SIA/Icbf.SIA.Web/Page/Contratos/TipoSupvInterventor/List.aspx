<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoSupvInterventor_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td style="width: 50%">
                    Nombre de tipo supervisor/interventor
                </td>
                <td style="width: 50%">
                    Estado
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombre" Height="50px" MaxLength="128" TextMode="MultiLine"
                        Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; "
                        InvalidChars="&#63;" />
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvTipoSupvInterventor" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdTipoSupvInterventor"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvTipoSupvInterventor_PageIndexChanging"
                        OnSelectedIndexChanged="gvTipoSupvInterventor_SelectedIndexChanged" OnSorting="gvTipoSupvInterventor_Sorting"
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Supervisor/Interventor" DataField="Nombre" SortExpression="Nombre" />--%>
                            <asp:TemplateField HeaderText="Nombre De Tipo Supervisor/Interventor" ItemStyle-HorizontalAlign="Center" SortExpression="Nombre">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Nombre")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
