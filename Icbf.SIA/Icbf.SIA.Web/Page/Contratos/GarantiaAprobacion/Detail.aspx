<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_GarantiaAprobacion_Detail" %>
<%@ Register TagPrefix="uc2" TagName="IcbfDireccion" Src="~/General/General/Control/IcbfDireccion.ascx" %>
<%@ Register TagPrefix="uc1" TagName="fecha_1" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <script type="text/javascript">

        function Confirmacion() {

            return confirm("Esta segúro que desea realizar esta acción sobre la garantía?");
        }

        function Eliminacion() {
            return confirm("Esta segúro que  desea devolver la garantía?");
        }

</script>
    <asp:HiddenField ID="hfIDGarantia" runat="server" />
    <asp:HiddenField ID="hfEstadoGarantia" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdSucursal" runat="server" />
   
            <fieldset>
        <legend>Información General</legend>
 <table width="90%" align="center">
      <tr class="rowB">
            <td>
            <asp:RadioButtonList ID="RbtnListGestion" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RbtnListGestion_SelectedIndexChanged" runat="server" Width="330px">
                <asp:ListItem Selected="True" Text="Aprobar" Value="1" ></asp:ListItem>
                <asp:ListItem  Text="Devolver" Value="0" ></asp:ListItem>
            </asp:RadioButtonList>
            </td>
            <td colspan= "2">
                        <asp:ImageButton ID="btnAprobar" runat="server" OnClientClick="if ( ! Confirmacion()) return false;" ValidationGroup="Aprobar" ImageUrl="~/Image/btn/save.gif" ToolTip="Aprobar" OnClick="btnAprobar_Click" />
            </td>
   </tr>
      <tr class="rowA">
            <td colspan= "3">
                    <asp:MultiView ID="MultiViewGestion" ActiveViewIndex="0" runat="server">
                        <asp:View ID="ViewAprobacion" runat="server">
                                    <table width="100%">
                                <tr class="rowB">
                                    <td class="auto-style1">
                                        <asp:Label runat="server" ID="lblFechaAprobacion" Text="Fecha de Aprobación" />
                                        </td>
                                    <td>

                                        <asp:TextBox ID="TxtFechaAprobacion" runat="server"></asp:TextBox>
                                        <asp:Image ID="imgFechaAprobacion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                        <Ajax:CalendarExtender ID="CalendarExtenderFechaAprobacion" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaAprobacion" TargetControlID="TxtFechaAprobacion">
                                        </Ajax:CalendarExtender>
                                        <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="TxtFechaAprobacion">
                                        </Ajax:MaskedEditExtender>
                                        <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="TxtFechaAprobacion" Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" style="font-weight: 700" ValidationGroup="Aprobar"></asp:RequiredFieldValidator>

                                    </td>
                                </tr>
                                                   <%--<tr class="rowB">
                                    <td class="auto-style1">
                                        Fecha de Certificación del Pago a la Garantía
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TxtFechaCertificacion" runat="server"></asp:TextBox>
                                        <Ajax:CalendarExtender ID="CalendarExtenderFechaCertificacion" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaAprobacion0" TargetControlID="TxtFechaCertificacion">
                                        </Ajax:CalendarExtender>
                                        <Ajax:MaskedEditExtender ID="TxtFechaAprobacion0_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="TxtFechaCertificacion">
                                        </Ajax:MaskedEditExtender>
                                        <asp:Image ID="imgFechaAprobacion0" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                        <%--<asp:RequiredFieldValidator ID="rfvFecha0" runat="server" ControlToValidate="TxtFechaCertificacion" Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" style="font-weight: 700" ValidationGroup="Aprobar"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>--%>
                            </table>
                        </asp:View>
                        <asp:View ID="ViewDevolucion" runat="server">
                            <table width="100%">
                                <tr class="rowB">
                                    <td >
                                        Fecha Devolución Garantía</td>
                                    <td>

                                        <asp:TextBox ID="TxtFechaDevolucion" runat="server"></asp:TextBox>
                                        <Ajax:CalendarExtender ID="CalendarExtenderFehcaDevolucion" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaAprobacion1" TargetControlID="TxtFechaDevolucion">
                                        </Ajax:CalendarExtender>
                                        <Ajax:MaskedEditExtender ID="TxtFechaAprobacion1_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="TxtFechaDevolucion">
                                        </Ajax:MaskedEditExtender>
                                        <asp:Image ID="imgFechaAprobacion1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                        <asp:RequiredFieldValidator ID="rfvFecha1" runat="server" ControlToValidate="TxtFechaDevolucion" Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" style="font-weight: 700" ValidationGroup="Aprobar"></asp:RequiredFieldValidator>

                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="auto-style2">Motivo Devolución Garantía</td>
                                    <td>
                                        <asp:TextBox ID="txtObservacionesRechazo" runat="server" CssClass="TextBoxGrande" Height="80px" MaxLength="200" TextMode="MultiLine" Width="90%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
            </td>
   </tr>
      <tr class="rowA">
          <td colspan="4">
              <asp:MultiView ID="MultiViewModificacionGarantia" runat="server" ActiveViewIndex="-1">
                  <asp:View ID="ViewModificacionGarantia" runat="server">
                      <table width="100%">
                          <tr class="rowB">
                              <td>
                                  Número de Modificación de la Garantía
                              </td>
                              <td>
                                  Descripción de la Modificación de la Garantía
                              </td>
                          </tr>
                          <tr class="rowB">
                              <td>
                                  <asp:TextBox runat="server" Enabled="false" ID="txtNumeroModificacionGarantia" MaxLength="50" Width="80%"></asp:TextBox>
                              </td>
                              <td>
                                                  <asp:TextBox runat="server" Enabled="false" ID="txtDescripciónModificación" MaxLength="250" Width="80%"
                    Height="80px" TextMode="MultiLine" 
                    CssClass="TextBoxGrande"></asp:TextBox>
            
                              </td>
                          </tr>
                          <tr class="rowB">
                              <td>
                                  Fecha de la Modificación de la Garantía
                              </td>
                              <td>
                                  Tipo de Modificación Contractual 
                              </td>
                          </tr>
                          <tr class="rowA">
                              <td>
                                          <asp:TextBox runat="server" Enabled="false" ID="txtFechaModificacionGarantia"></asp:TextBox>
                <asp:Image ID="imgFechaModificacionGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand"  />
                              </td>
                              <td>
                                  <asp:Label ID="lblTipoModificacionGarantia" runat="server"></asp:Label>
                              </td>
                          </tr>
                      </table>
                  </asp:View>
              </asp:MultiView>
          </td>
      </tr>
      <tr class="rowB">
            <td colspan= "3">
               Número del Contrato/Convenio
            </td>
   </tr>
   <tr class="rowA">
            <td colspan = "3">
               <asp:TextBox runat="server" ID="txtNumeroContratoConvenio"  MaxLength="80" Width="34%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContratoConvenio" runat="server" TargetControlID="txtNumeroContratoConvenio"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
   </tr>
   <tr class="rowB">
		<td colspan="2">
		   <asp:Label ID="lblNumeroGarantia" runat="server" Text="Número Garantía" /> 
		</td>
		<td>
		   Tipo Garantía 
		</td>
    </tr>
    <tr class="rowA">
		<td colspan="2">
		    <asp:TextBox runat="server" ID="txtNumeroGarantia" Enabled="false" MaxLength="50"  Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroGarantia" runat="server" TargetControlID="txtNumeroGarantia"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-" />
		</td>
		<td style="width: 50%">
			<asp:DropDownList runat="server" ID="ddlIDTipoGarantia" Enabled="False"></asp:DropDownList>
		</td>
    </tr>
    </table>
  </fieldset>
	<fieldset>
        <legend>Información Aseguradora</legend>
        <table width="90%" align="center">
			<tr class="rowB">
            <td>
               Nit de la Aseguradora 
            </td>
            <td>
               Nombre de la Aseguradora
            </td>
        </tr>
        <tr class="rowA">
            <td>
                 <asp:TextBox runat="server" ID="txtNitAseguradora" MaxLength="80" Width="80%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNitAseguradora" runat="server" TargetControlID="txtNitAseguradora"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreAseguradora" MaxLength="80" Width="80%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteNombreSucursal" runat="server" TargetControlID="txtNombreAseguradora"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars="áéíóúÁÉÍÓÚñÑ "/>
                    <asp:HiddenField runat="server" ID="HdNombreAseguradora" ClientIDMode="Static" />
           </td>
        </tr>
		  <tr class="rowB">
            <td>
               Departamento Sucursal 
            </td>
            <td>
               Municipio Sucursal 
            </td>
        </tr>
        <tr class="rowA">
            <td style="width: 50%">
               <asp:DropDownList runat="server" ID="ddlDepartamentoSucursal" Enabled="false"></asp:DropDownList>
            </td>
            <td>
              <asp:DropDownList runat="server" ID="ddlMunicipioSucursal" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
               Nombre de la Sucursal
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                 <asp:TextBox runat="server" ID="txtNombreSucursal" MaxLength="256" Enabled="false" Width="40%" Height="80px" TextMode="MultiLine"  onKeyDown="limitText(this,256);" 
                onKeyUp="limitText(this,256);" CssClass="TextBoxGrande"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreAseguradora" runat="server" TargetControlID="txtNombreSucursal"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars=" "/>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
               Dirección Notificación
            </td>
         </tr>
        <tr class="rowA">
            <td colspan="2">
                <uc2:IcbfDireccion ID="txtDireccionNotificacion" runat="server" Requerido="False" Enabled="false" />
            </td>
         </tr>
          <tr class="rowB">
            <td>
               Correo Electrónico 
            </td>
            <td>
               Indicativo 
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:TextBox runat="server" ID="txtCorreoElectronico" MaxLength="128" Width="80%" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftecorreoelectronico" runat="server" TargetControlID="txtCorreoElectronico"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars="@.-_"/>
            </td>
            <td>
               <asp:TextBox runat="server" ID="txtIndicativo" MaxLength="3" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteIndicativo" runat="server" TargetControlID="txtIndicativo"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
        </tr>
          <tr class="rowB">
            <td>
               Teléfono 
            </td>
            <td>
               Extensión 
            </td>
        </tr>
        <tr class="rowA">
            <td>
               <asp:TextBox runat="server" ID="txtTelefono" MaxLength="7" Width="40%" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteTelefono" runat="server" TargetControlID="txtTelefono"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtExtension" MaxLength="5" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtExtension"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
        </tr>
         <tr class="rowB">
            <td>
               Celular
            </td>
            <td>
               Beneficiario y/o Asegurado y/o Afianzado
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCelular" MaxLength="10" Width="40%" Enabled="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteCelular" runat="server" TargetControlID="txtCelular"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>
            </td>
            <td>
              <asp:CheckBoxList runat="server" ID="chkBeneAsegAfianz" RepeatDirection="Horizontal"></asp:CheckBoxList>
            </td>
        </tr>
		 </table>
    </fieldset>
  
 <fieldset>
        <legend>Contratistas</legend>
        <table width="90%" align="center">
             <tr class="rowAG">
                <td>
                     <asp:GridView runat="server" ID="gvProveedores" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" DataKeyNames="IDContratista_Garantias" CellPadding="0" Height="16px"
                    EmptyDataText="No se encontraron datos, verifique por favor.">
                    <Columns>
                <%--<asp:TemplateField HeaderText="Seleccionar">
                    <ItemTemplate>
                       <asp:CheckBox runat="server" CommandName="Select" ToolTip="Seleccionar" ID="check_SeleccionarContratista"/>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" SortExpression="TipoPersonaNombre"  />
                <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                <asp:BoundField HeaderText="Contratista" DataField="Proveedor" SortExpression="Proveedor"  />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                </td>
            </tr>
         </table>
         <table width="90%" align="center">
      <tr class="rowB">
            <td colspan= "2">
               Descripción de los Beneficiarios y/o Asegurados y/o Afianzados
            </td>
       </tr>
       <tr class="rowA">
                <td colspan = "2" style="text-align:center; vertical-align: middle;">
                   <asp:TextBox runat="server" ID="txtDescBenAsegAfianz" MaxLength="200" Width="90%" Height="80px" TextMode="MultiLine" 
                onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);" CssClass="TextBoxGrande" Enabled="False"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteDescBenAsegAfianz" runat="server" TargetControlID="txtDescBenAsegAfianz"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars=" -.,;:"/>
                </td>
       </tr>
   </table>
    </fieldset>
   

          <fieldset>
        <legend>Amparos</legend>

   <asp:Panel runat="server" ID="pnlAmparos">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAmparosGarantias" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IDAmparosGarantias" CellPadding="0" Height="16px"
                         EmptyDataText="No se encontraron datos, verifique por favor.">
                        <Columns>
                           <asp:BoundField HeaderText="Tipo Amparo" DataField="NombreTipoAmparo"  SortExpression="NombreTipoAmparo"/>
                            <%--<asp:BoundField HeaderText="Fecha Vigencia Desde" DataField="FechaVigenciaDesde"  SortExpression="FechaVigenciaDesde"/>--%>
                             <asp:TemplateField HeaderText="Fecha Vigencia Desde" ItemStyle-HorizontalAlign="Center" SortExpression="FechaVigenciaDesde">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 80px;">
                                            <%#Convert.ToDateTime(Eval("FechaVigenciaDesde")).ToString("dd/MM/yyyy")%>
                                     </div>
                                 </ItemTemplate>
                              </asp:TemplateField>

                            <%--<asp:BoundField HeaderText="Fecha Vigencia Hasta" DataField="FechaVigenciaHasta"  SortExpression="FechaVigenciaHasta"/>--%>
                           
                            <asp:TemplateField HeaderText="Fecha Vigencia Hasta" ItemStyle-HorizontalAlign="Center" SortExpression="FechaVigenciaHasta">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 80px;">
                                            <%#Convert.ToDateTime(Eval("FechaVigenciaHasta")).ToString("dd/MM/yyyy")%>
                                     </div>
                                 </ItemTemplate>
                              </asp:TemplateField>

                            <asp:BoundField HeaderText="Valor para Cálculo Asegurado" DataField="ValorCalculoAsegurado"  SortExpression="ValorCalculoAsegurado"/>
                             <asp:BoundField HeaderText="Unidad de Cálculo" DataField="NombreUnidadCalculo"  SortExpression="NombreUnidadCalculo"/>
                            <%--<asp:BoundField HeaderText="Valor Asegurado" DataField="ValorAsegurado"  SortExpression="ValorAsegurado"/>--%>
                            <asp:TemplateField HeaderText="Valor Asegurado" ItemStyle-HorizontalAlign="Center" SortExpression="ValorAsegurado">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 150px;">
                                         <%# Convert.ToDecimal(Eval("ValorAsegurado")).ToString("$ #,###0.00##;($ #,###0.00##)")%>
                                     </div>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField>
                                <ItemTemplate>
                                   <%-- <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />--%>
                                    <%--<asp:ImageButton ID="btneliminar" runat="server" CommandName="Delete" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Eliminar" />--%>
                                    <%--<asp:ImageButton ID="btneliminarAmparo" runat="server" CommandName="Borrar" ImageUrl="~/Image/btn/delete.gif" 
                                    Height="16px" Width="16px" ToolTip="Eliminar" 
                                    Enabled="False" 
                                    OnClientClick="javascript:if (!confirm('¿Est&#225; seguro que desea eliminar el documento?')) return false;" 
                                    onclick="btnEliminarAmparo_Click" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </fieldset>

              <fieldset>
        <legend>Cubrimientos y Anexos </legend>
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
      <table width="90%" align="center">
			<tr class="rowB">
            <td>
               Fecha de Inicio Garantía 
			     
            </td>
            <td>
               Fecha de Expedición Garantía 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <uc1:fecha_1 ID="txtFechaInicioGarantia" runat="server"  Enabled="False" Requerid="False" />
            </td>
            <td>
                <%--<uc1:fecha ID="txtFechaExpedicionGarantia" runat="server"  Enabled="False" Requerid="False" />--%>
                <asp:TextBox runat="server" ID="txtFechaExpedicionGarantia" Enabled="False"></asp:TextBox>
                <asp:Image ID="imgFechaExpedicionGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                <Ajax:CalendarExtender ID="cetxtFechaDesde" runat="server" Enabled="false" Format="dd/MM/yyyy" 
                    PopupButtonID="imgFechaExpedicionGarantia" TargetControlID="txtFechaExpedicionGarantia"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="False" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaExpedicionGarantia">
                </Ajax:MaskedEditExtender>
            </td>
        </tr>
        <tr class="rowB">
            <td>
               Fecha de Vencimiento Inicial Garantía 
            </td>
            <td>
               Fecha de Vencimiento Final
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <%--<uc1:fecha ID="txtFechaVencimientoInicial" runat="server"  Enabled="False" Requerid="False"/>--%>
                <asp:TextBox runat="server" ID="txtFechaVencimientoInicial" Enabled="False"></asp:TextBox>
                <asp:Image ID="ImgFechaVencimientoInicial" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                <Ajax:CalendarExtender ID="ceFechaVencimientoInicial" Enabled="false" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="ImgFechaVencimientoInicial" TargetControlID="txtFechaVencimientoInicial"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meeFechaVencimientoInicial" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="False" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVencimientoInicial">
                </Ajax:MaskedEditExtender>
            </td>
            <td>
                <uc1:fecha_1 ID="txtFechaVencimientoFinal" runat="server"  Enabled="False" Requerid="False" />
            </td>
        </tr>
        <tr class="rowB">
            <%--<td>
               Fecha de Recibo Garantía 
            </td>--%>
            <td>
               Valor Garantía 
            </td>
        </tr>
        <tr class="rowA">
            <%--<td>
                <%--<uc1:fecha ID="txtFechaReciboGarantia" runat="server"  Enabled="False" Requerid="False" />
                <asp:TextBox runat="server" ID="txtFechaReciboGarantia" Enabled="False"></asp:TextBox>
                <asp:Image ID="ImgFechaReciboGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                <Ajax:CalendarExtender ID="ceFechaReciboGarantia" Enabled="false" runat="server" Format="dd/MM/yyyy" 
                    PopupButtonID="ImgFechaReciboGarantia" TargetControlID="txtFechaReciboGarantia"></Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meeFechaReciboGarantia" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="False" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaReciboGarantia">
                </Ajax:MaskedEditExtender>
            </td>--%>
            <td>
                 <asp:TextBox runat="server" ID="txtValorGarantia" MaxLength="50" Width="40%" Enabled="False"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="fteValorGarantia" runat="server" TargetControlID="txtValorGarantia"
                    FilterType="LowercaseLetters,UppercaseLetters,Numbers" />--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
              Anexos 
            </td>
            <td>
              Observaciones 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:RadioButtonList runat="server" ID="rblAnexos" RepeatDirection="Horizontal" Enabled="False"></asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionAnexos" MaxLength="250" Width="80%" Height="80px" TextMode="MultiLine" 
            onKeyDown="limitText(this,250);" onKeyUp="limitText(this,250);" CssClass="TextBoxGrande" Enabled="false"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtDescripcionAnexos"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters"/>--%>
            </td>
        </tr>
		</table>
         
     <table width="90%" align="center">
        <tr>
            <td colspan="2">
               Documento Anexo 
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="PnlArchivosAnexos" Visible="True">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="NombreArchivo,IdArchivo,IDArchivosGarantias"
                                CellPadding="0" Height="16px" 
                                onselectedindexchanged="gvDocumentos_SelectedIndexChanged" 
                                onrowcommand="gvDocumentos_RowCommand">
                     <Columns>
                        <%--<asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnSeleccionar" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                    Height="16px" Width="16px" ToolTip="Seleccionar" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:BoundField HeaderText="Documento Anexo" DataField="NombreArchivoOri" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" ImageUrl="~/Image/btn/list.png"
                                    Height="16px" Width="16px" ToolTip="Mostrar" Enabled="true" 
                                    onclick="btnMostrar_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="LightBlue" />
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
                  </fieldset>
     </asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 229px;
        }
        .auto-style2 {
            width: 230px;
        }
    </style>
    </asp:Content>

