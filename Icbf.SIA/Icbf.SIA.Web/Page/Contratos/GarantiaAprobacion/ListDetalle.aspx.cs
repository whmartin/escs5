﻿using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;

public partial class Page_Contratos_GarantiaAprobacion_ListDetalle : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Garantia";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.MostrarBotonEditar(false);
            toolBar.OcultarBotonGuardar(true);
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.EstablecerTitulos("Gesti&oacute;n de las garantías del contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoGuardar(object sender, EventArgs e)
    {

    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles Controles = new ManejoControles();
            int idContrato = int.Parse(GetSessionParameter("Garantia.IDContratoConsultado").ToString());
            RemoveSessionParameter("Garantia.IDContratoConsultado");
            hfIdContrato.Value = idContrato.ToString();
            List<Garantia> items = vContratoService.ConsultarInfoGarantias(idContrato);
            List<Garantia> itemsPendientes = new List<Garantia>();
            GridGarantias.EmptyDataText = "No hay Garantìas pendientes de revisiòn";
            GridGarantias.PageSize = PageSize();

            decimal valor = CargarItemsPendientes();

            txtValorTotalGarantias.Text = valor.ToString("$ #,###0.00##;($ #,###0.00##)");

            var contrato = vContratoService.ContratoObtener(idContrato);
            txtNumeroContrato.Text = contrato.NumeroContrato;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private decimal CargarItemsPendientes()
    {
        decimal valor = 0;

        int idContrato = int.Parse(hfIdContrato.Value);
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(idContrato);
        List<Garantia> itemsPendientes = new List<Garantia>();

        if (items != null && items.Count() > 0)
        {
           var itemsPendientesTemp = items.Where(e => e.CodigoEstadoGarantia == "006" || e.CodigoEstadoGarantia == "007");

           if (itemsPendientesTemp != null)
               itemsPendientes = itemsPendientesTemp.ToList();

            foreach (var item in items)
                if (!string.IsNullOrEmpty(item.ValorGarantia))
                {
                    item.ValorGarantia = item.ValorGarantia.Replace("$", string.Empty);
                    valor += decimal.Parse(item.ValorGarantia);
                }
        }

        GridGarantias.DataSource = itemsPendientes;
        GridGarantias.DataBind();

        return valor;
    }

    protected void GridGarantias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridGarantias.PageIndex = e.NewPageIndex;
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(int.Parse(hfIdContrato.Value));
        GridGarantias.DataSource = items;
        GridGarantias.DataBind();
    }

    protected void GridGarantias_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var idContrato = int.Parse(hfIdContrato.Value);
        int index = int.Parse(e.CommandArgument.ToString());
        var idGarantia = int.Parse(GridGarantias.DataKeys[index].Values[0].ToString());
        var estado = GridGarantias.DataKeys[index].Values[2].ToString();
        if (e.CommandName == "Select")
        {
            var idSucursal = GridGarantias.DataKeys[index].Values[1];
            SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
            SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", idSucursal);
            SetSessionParameter("Garantia.Estado", estado);
            SetSessionParameter("Garantia.IdGarantia", idGarantia); 
            NavigateTo(SolutionPage.Detail);
        }
        //else if (e.CommandName == "Aprobar")
        //{
        //    if (estado == "006")
        //        vContratoService.CambiarEstadoGarantia(idGarantia, true, true);
        //    else
        //        vContratoService.GenerarModificacionGarantia(idGarantia, Page.User.Identity.Name);

        //    CargarItemsPendientes();
        //}
        //else if (e.CommandName == "Eliminar")
        //{
        //    if (estado == "006")
        //        vContratoService.CambiarEstadoGarantia(idGarantia, false, true);
        //    else
        //        vContratoService.CambiarEstadoGarantia(idGarantia, false, false);

        //    CargarItemsPendientes();
        //}
    }
}