using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad Garantia
/// </summary>
public partial class Page_GarantiaAprobacion_Detail : GeneralWeb
{
    #region variables
    masterPrincipal toolBar;
    string PageName = "Contratos/Garantia";
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();
    OferenteService vOferenteService = new OferenteService();
    #endregion

    #region Eventos
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
        SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", hfIdSucursal.Value);
        SetSessionParameter("Garantia.IDGarantia", hfIDGarantia.Value);
        NavigateTo(SolutionPage.Edit);
    }
    
    protected void btnEliminarGarantia_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
        NavigateTo("ListDetalle.aspx");
    }

    protected void btnAprobar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (RbtnListGestion.SelectedValue == "1")
            {
                DateTime fechaAprobacion;

                if (DateTime.TryParse(TxtFechaAprobacion.Text,out fechaAprobacion) && Convert.ToDateTime( TxtFechaAprobacion.Text) < DateTime.Now)
                {
                    int idGarantia = int.Parse(hfIDGarantia.Value);

                    if (hfEstadoGarantia.Value == "006")
                    {
                        DateTime? fechaFinalizacion = null;

                        var miContrato = vContratoService.ContratoObtener(int.Parse(hfIdContrato.Value));
                    
                        if (miContrato.EsFechaFinalCalculada.HasValue && miContrato.EsFechaFinalCalculada.Value)
	                    {		 
                           DateTime fechaInicio = fechaAprobacion;

                           var garantia = vContratoService.ConsultarMaximaGarantia(idGarantia);

                           if (garantia != null && garantia.FechaAprobacionGarantia.HasValue && garantia.FechaAprobacionGarantia.Value > fechaInicio)
                               fechaInicio = garantia.FechaAprobacionGarantia.Value;

                            fechaFinalizacion = ContratoService.CalcularFechaFinalContrato
                                (
                                    fechaInicio,
                                    miContrato.DiasFechaFinalCalculada.Value,
                                    miContrato.MesesFechaFinalCalculada.Value
                                );
                          }

                       vContratoService.CambiarEstadoGarantia(idGarantia, true, true, fechaAprobacion, string.Empty, fechaFinalizacion);
                    }
                    else
                        vContratoService.GenerarModificacionGarantia(idGarantia, Page.User.Identity.Name, fechaAprobacion);

                    SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
                    NavigateTo("ListDetalle.aspx");
                }
                toolBar.MostrarMensajeError("La fecha de aprobación es invalida");
            }
            else
            {
                DateTime fechaRechazo;

                if (!string.IsNullOrEmpty(txtObservacionesRechazo.Text) && DateTime.TryParse(TxtFechaDevolucion.Text, out fechaRechazo))
                {
                    int idGarantia = int.Parse(hfIDGarantia.Value);
                    if (hfEstadoGarantia.Value == "006")
                        vContratoService.CambiarEstadoGarantia(idGarantia, false, true, fechaRechazo, txtObservacionesRechazo.Text, null);
                    else
                        vContratoService.CambiarEstadoGarantia(idGarantia, false, false, fechaRechazo, txtObservacionesRechazo.Text, null);

                    SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
                    NavigateTo("ListDetalle.aspx");
                }
                else
                    toolBar.MostrarMensajeError("Debe ingresar las observaciones relacionadas a la devolución y/o la fecha de devolución en un formato correcto.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return;
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
            return;
        }
    }

    #endregion

    #region Métodos


    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIDGarantia = Convert.ToInt32(GetSessionParameter("Garantia.IdGarantia"));
            RemoveSessionParameter("Garantia.IDGarantia");
            hfIDGarantia.Value = vIDGarantia.ToString();

            if (!string.IsNullOrEmpty(GetSessionParameter("Garantia.Estado").ToString()))
                hfEstadoGarantia.Value = GetSessionParameter("Garantia.Estado").ToString();
            
            int vIDSucursalAseguradoraContrato = Convert.ToInt32(GetSessionParameter("Garantia.IDSucursalAseguradoraContrato"));
            RemoveSessionParameter("Garantia.IDSucursalAseguradoraContrato");
            hfIdSucursal.Value = vIDSucursalAseguradoraContrato.ToString();

            if (GetSessionParameter("Garantia.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Garantia.Guardado");

            SucursalAseguradoraContrato vSucursalAseguradoraContrato = vContratoService.ConsultarSucursalAseguradoraContrato(vIDSucursalAseguradoraContrato);
            //int idtercero = vProveedorService.ConsultarEntidadProvOferente(vSucursalAseguradoraContrato.IDEntidadProvOferente).IdTercero;
            //Tercero vTercero = vOferenteService.ConsultarTercero(idtercero);            
            Tercero vTercero = vOferenteService.ConsultarTercero(vSucursalAseguradoraContrato.IdTercero);
            List<ContratistaGarantias> vContratistaGarantias = vContratoService.ConsultarContratistaGarantiass(vIDGarantia, null);
            List<ArchivosGarantias> vArchivosGarantias = vContratoService.ConsultarArchivosGarantiass(null, vIDGarantia);
            List<AmparosGarantias> vAmparosGarantias = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, vIDGarantia);

            ddlDepartamentoSucursal.SelectedValue = Convert.ToString(vSucursalAseguradoraContrato.IDDepartamento);
            ddlMunicipioSucursal.SelectedValue = Convert.ToString(vSucursalAseguradoraContrato.IDMunicipio);
            txtNombreSucursal.Text = vSucursalAseguradoraContrato.Nombre;
            txtDireccionNotificacion.Text = vSucursalAseguradoraContrato.DireccionNotificacion;
            txtCorreoElectronico.Text = vSucursalAseguradoraContrato.CorreoElectronico;
            txtIndicativo.Text = vSucursalAseguradoraContrato.Indicativo;
            txtTelefono.Text = vSucursalAseguradoraContrato.Telefono;
            txtExtension.Text = vSucursalAseguradoraContrato.Extension;
            txtCelular.Text = vSucursalAseguradoraContrato.Celular;
            txtNitAseguradora.Text = vTercero.NumeroIdentificacion;
            txtNombreAseguradora.Text = vTercero.Nombre_Razonsocial;

            Garantia vGarantia = vContratoService.ConsultarGarantia(vIDGarantia);

            if (vGarantia.CodigoEstadoGarantia == "007")
            {
                MultiViewModificacionGarantia.ActiveViewIndex = 0;
                lblNumeroGarantia.Text = "Número de Garatía Original";
                txtDescripciónModificación.Text = vGarantia.DescripcionModificacionGarantia;
                txtNumeroModificacionGarantia.Text = vGarantia.NumeroModificacion;
                txtFechaModificacionGarantia.Text = Convert.ToDateTime(vGarantia.FechaModificacionGarantia).ToString("dd/MM/yyyy");
                lblTipoModificacionGarantia.Text = vContratoService.TipoModificacionGarantiaTemporal(vIDGarantia);
            }
            else
                MultiViewModificacionGarantia.ActiveViewIndex = -1;

            txtNumeroGarantia.Text = vGarantia.NumeroGarantia;
            ddlIDTipoGarantia.SelectedValue = Convert.ToString(vGarantia.IDTipoGarantia);
            if (vGarantia.BeneficiarioICBF.Equals(true))
                chkBeneAsegAfianz.Items.FindByText("ICBF").Selected = true;
            else
                chkBeneAsegAfianz.Items.FindByText("ICBF").Selected = false;

            if (vGarantia.BeneficiarioOTROS.Equals(true))
                chkBeneAsegAfianz.Items.FindByText("Otros").Selected = true;
            else
                chkBeneAsegAfianz.Items.FindByText("Otros").Selected = false;

            txtDescBenAsegAfianz.Text = Convert.ToString(vGarantia.DescripcionBeneficiarios);

            txtFechaInicioGarantia.Date = Convert.ToDateTime(vGarantia.FechaInicioGarantia);
            txtFechaExpedicionGarantia.Text = Convert.ToDateTime(vGarantia.FechaExpedicionGarantia).ToString("dd/MM/yyyy");
            txtFechaVencimientoInicial.Text = Convert.ToDateTime(vGarantia.FechaVencimientoInicialGarantia).ToString("dd/MM/yyyy");
            txtFechaVencimientoFinal.Date = Convert.ToDateTime(vGarantia.FechaVencimientoFinalGarantia);
           // txtFechaReciboGarantia.Text = Convert.ToDateTime(vGarantia.FechaReciboGarantia).ToString("dd/MM/yyyy");

            CalendarExtenderFechaAprobacion.StartDate = vGarantia.FechaExpedicionGarantia;
            CalendarExtenderFechaAprobacion.EndDate = DateTime.Now;
            CalendarExtenderFehcaDevolucion.StartDate = vGarantia.FechaExpedicionGarantia;
            //CalendarExtenderFechaCertificacion.StartDate = vGarantia.FechaExpedicionGarantia;

            if (!string.IsNullOrEmpty(vGarantia.ValorGarantia))
                txtValorGarantia.Text = vGarantia.ValorGarantia;  //Convert.ToDecimal(vGarantia.ValorGarantia).ToString("$ #,###0.00##;($ #,###0.00##)"); 
            
            rblAnexos.SelectedValue = Convert.ToString(vGarantia.Anexos);
            txtDescripcionAnexos.Text = vGarantia.ObservacionesAnexos;

            gvAmparosGarantias.DataSource = vAmparosGarantias;
            gvAmparosGarantias.DataBind();

            gvProveedores.DataSource = vContratistaGarantias;
            gvProveedores.DataBind();

            if (vArchivosGarantias.Count > 0)
                rblAnexos.SelectedValue = "true";
            else
                rblAnexos.SelectedValue = "false";

            gvDocumentos.DataSource = vArchivosGarantias;
            gvDocumentos.DataBind();

           
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGarantia.UsuarioCrea, vGarantia.FechaCrea, vGarantia.UsuarioModifica, vGarantia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    /// <summary>
    /// Método de eliminación del registro seleccionado 
    /// </summary>
    private void EliminarRegistro()
    {
        try
        {
            toolBar.LipiarMensajeError();
            int vIDGarantia = Convert.ToInt32(GetSessionParameter("Garantia.IDGarantia"));

            Garantia vGarantia = new Garantia();
            vGarantia = vContratoService.ConsultarGarantia(vIDGarantia);
            if (vGarantia.IDGarantia.Equals(0))
            {
                toolBar.MostrarMensajeError("La garantía ya fue eliminada");
                return;
            }

            InformacionAudioria(vGarantia, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarGarantia(vGarantia);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                return;
            }
            else if (vResultado >= 1)
            {
                toolBar.MostrarMensajeGuardado("El registro ha sido eliminado correctamente.");
                SetSessionParameter("Garantia.Eliminado", "1");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;            
            toolBar.MostrarBotonEditar(false);
            toolBar.MostrarBotonNuevo(false);
            toolBar.OcultarBotonEliminar(true);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.EstablecerTitulos("Garantia", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de carga de listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ddlIDTipoGarantia.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlDepartamentoSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlMunicipioSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            chkBeneAsegAfianz.Items.Insert(0, new ListItem("ICBF", "true"));
            chkBeneAsegAfianz.Items.Insert(1, new ListItem("Otros", "false"));
            rblAnexos.Items.Insert(0, new ListItem("SI", "true"));
            rblAnexos.Items.Insert(1, new ListItem("NO", "false"));
            CargarIDTipoGarantia();
            CargarDepartamentos();
            CargarMunicipios();

            hfIdContrato.Value = Convert.ToString(GetSessionParameter("Garantia.IDContratoConsultado"));
            RemoveSessionParameter("Garantia.IDContratoConsultado");

            if (! string.IsNullOrEmpty(hfIdContrato.Value))
            txtNumeroContratoConvenio.Text = vContratoService.ConsultarContrato(Convert.ToInt32(hfIdContrato.Value)).NumeroContrato;

            List<ArchivosGarantias> lDocAnexosGarantias = new List<ArchivosGarantias>();
            lDocAnexosGarantias.Add(new ArchivosGarantias
            {
                IDArchivosGarantias = 0,
                IDArchivo = 0,
                NombreArchivo = "0",
                NombreArchivoOri = ""
            });

            this.gvDocumentos.DataSource = lDocAnexosGarantias;
            this.gvDocumentos.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region Grilla Contratistas

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistroContratista(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProveedores.DataKeys[rowIndex].Value.ToString();
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistroContratista(gvProveedores.SelectedRow);
    }

    protected void gvProveedores_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaContratistas((GridView)sender, e.SortExpression, false);
    }

    protected void gvProveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedores.PageIndex = e.NewPageIndex;
        CargarGrillaContratistas((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaContratistas(BaseDataBoundControl gridViewsender, string expresionOrdenamiento,
        bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        int? IdContrato = null;
        if (GetSessionParameter("Contrato.ContratosAPP") != null)
            IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var myGridResults = vContratoService.ConsultarContratistasContratos(IdContrato);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Proveedores_Contratos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression =
                        Expression.Lambda<Func<Proveedores_Contratos, object>>(
                            Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource =
                                myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource =
                                myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Grilla Amparos Garantias

    protected void gvAmparosGarantias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistroAmparos(gvAmparosGarantias.SelectedRow);
    }

    protected void gvAmparosGarantias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAmparosGarantias.PageIndex = e.NewPageIndex;
        CargarGrillaAmparos((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvAmparosGarantias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaAmparos((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaAmparos(BaseDataBoundControl gridViewsender, string expresionOrdenamiento,
        bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            toolBar.LipiarMensajeError();
            int? idGarantia = 0;

                idGarantia = Convert.ToInt32(hfIDGarantia.Value);

            var myGridResults = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, idGarantia);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(AmparosGarantias), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression =
                        Expression.Lambda<Func<AmparosGarantias, object>>(Expression.Convert(prop, typeof(object)),
                            param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource =
                                myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource =
                                myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistroAmparos(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvAmparosGarantias.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("AmparosGarantias.IDAmparosGarantias", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Grilla Documentos Anexos
    protected void btnMostrar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton)sender;
        try
        {
            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;

            string nombreArchivo = "";

            if (dataItem != null)
                nombreArchivo = HttpUtility.HtmlDecode(gvDocumentos.DataKeys[dataItem.RowIndex]["NombreArchivo"].ToString()).Trim();

            Response.Redirect("../DescargarArchivo/DescargarArchivo.aspx?fname=" + nombreArchivo);

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvDocumentos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int numIndex = gvDocumentos.SelectedRow.RowIndex;
        foreach (GridViewRow gvRow in gvDocumentos.Rows)
        {
            ImageButton btnMostrar = (ImageButton)gvRow.Cells[2].FindControl("btnMostrar");
            if (gvRow.RowIndex == numIndex)
            {
                btnMostrar.Enabled = true;
            }
            else
            {
                btnMostrar.Enabled = false;
            }
        }
    }

    protected void gvDocumentos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            //case "Show":
            //    Show(gvDocumentos.SelectedRow);
            //    break;
        }
    }
    #endregion

    /// <summary>
    /// Método de carga el Tipo Garantia
    /// </summary>
    
    private void CargarIDTipoGarantia()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarTipoGarantia(ddlIDTipoGarantia, "-1", true);
    }

    private void CargarDepartamentos()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarDepartamentos(ddlDepartamentoSucursal,"-1",true);
    }

    private void CargarMunicipios()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarMunicipios(ddlMunicipioSucursal, "-1", true,null);
    }

    private void CargarAmparosGarantias()
    {
        toolBar.LipiarMensajeError();
        CargarGrillaAmparos(gvAmparosGarantias, GridViewSortExpression, true);
        txtFechaInicioGarantia.Date = vContratoService.ConsultarFechaVigenciaAmparoRelacionado(Convert.ToInt32(hfIDGarantia.Value)).FechaVigenciaDesde;
        txtFechaVencimientoFinal.Date = vContratoService.ConsultarFechaVigenciaAmparoRelacionado(Convert.ToInt32(hfIDGarantia.Value)).FechaVigenciaHasta;
        txtFechaVencimientoInicial.Text = vContratoService.ConsultarFechaVigenciaAmparoRelacionado(Convert.ToInt32(hfIDGarantia.Value)).FechaVigenciaHasta.ToShortDateString();
        decimal acomuladovalores_asegurado =vContratoService.ConsultarAmparosGarantiass(null, null, null, null, Convert.ToInt32(hfIDGarantia.Value)).Sum(d => d.ValorAsegurado);
        txtValorGarantia.Text = acomuladovalores_asegurado.ToString("$ #,###0.00##;($ #,###0.00##)");
    }

    #endregion

    protected void RbtnListGestion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RbtnListGestion.SelectedValue =="1")
        {
            MultiViewGestion.ActiveViewIndex = 0;
        }
        else
        {
            MultiViewGestion.ActiveViewIndex = 1;
        }
    }
}
