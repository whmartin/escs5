<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_GarantiaAprobacion_List" %>
<%@ Register TagPrefix="uc1" TagName="fecha_1" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
       <tr class="rowB">
	        <td class="Cell">
	                Fecha Registro al Sistema Desde
                    <asp:RequiredFieldValidator ID="rfvFechaDesde" runat="server" ControlToValidate="txtFechaRegistroDesde" ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="True" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="cvFechaDesde" runat="server" ControlToValidate="txtFechaRegistroDesde" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator>
	        </td>
	        <td class="Cell">
		            Fecha Registro al Sistema Hasta
                    <asp:RequiredFieldValidator ID="rfvFechaHasta" runat="server" ControlToValidate="txtFechaRegistroHasta" ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="True" ValidationGroup="btnGuardar" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="cvFechahasta" runat="server" ControlToValidate="txtFechaRegistroHasta" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red"
                    Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator>
	        </td>
        </tr>
        <tr class="rowA">
	        <td class="Cell">
		            <%--<uc1:fecha_1 ID="txtFechaRegistroDesde" runat="server"  Enabled="True" Requerid="False" />--%>
                    <asp:TextBox runat="server" ID="txtFechaRegistroDesde"></asp:TextBox>
                    <asp:Image ID="imgCalendarioDesde" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                    <Ajax:CalendarExtender ID="cetxtFechaDesde" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioDesde" TargetControlID="txtFechaRegistroDesde"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaRegistroDesde">
                    </Ajax:MaskedEditExtender>
	        </td>
	        <td class="Cell">
	                <%--<uc1:fecha_1 ID="txtFechaRegistroHasta" runat="server"  Enabled="True" Requerid="False" />--%>
                    <asp:TextBox runat="server" ID="txtFechaRegistroHasta"></asp:TextBox>
                    <asp:Image ID="imgCalendarioHasta" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                    <Ajax:CalendarExtender ID="cetxtFechaHasta" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioHasta" TargetControlID="txtFechaRegistroHasta"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxFechaHasta" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaRegistroHasta">
                    </Ajax:MaskedEditExtender>

	        </td>
        </tr>
        <tr class="rowB">
	        <td class="Cell">
	               Número del Contrato/Convenio
	        </td>
	        <td class="Cell">
		            Vigencia Fiscal Inicial
	        </td>
        </tr>
        <tr class="rowA">
	        <td class="Cell">
		            <asp:TextBox runat="server" ID="txtNumeroContratoConvenio" MaxLength="50" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteNumeroContratoConvenio" runat="server" TargetControlID="txtNumeroContratoConvenio"
                    FilterType="LowercaseLetters,UppercaseLetters,Numbers" />
	        </td>
	        <td class="Cell">
	                <asp:TextBox runat="server" ID="txtVigenciaFiscalInicial" MaxLength="4" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteVigenciaFiscalInicial" runat="server" TargetControlID="txtVigenciaFiscalInicial"
                    FilterType="LowercaseLetters,UppercaseLetters,Numbers" />
	        </td>
        </tr>
        <tr class="rowB">
	        <td class="Cell">
	                Regional del Contrato/Convenio
	        </td>
	        <td class="Cell">
		            Categoría del contrato/Convenio
	        </td>
        </tr>
        <tr class="rowA">
	        <td class="Cell">
		            <asp:DropDownList runat="server" ID="ddlRegionalContrato"></asp:DropDownList>   
	        </td>
	        <td class="Cell">
	                <asp:DropDownList runat="server" ID="ddlCategoriaContrato" OnSelectedIndexChanged="ddlCategoriaContrato_OnSelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>  
	        </td>
        </tr>
        <tr class="rowB">
	        <td class="Cell" colspan="2">
	            Tipo de Contrato/Convenio
	        </td>
	       
        </tr>
        <tr class="rowA">
	        <td class="Cell" colspan="2">
		        <asp:DropDownList runat="server" ID="ddlTipoContrato" Width="40%"></asp:DropDownList>
	        </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContrato" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px"
                        OnSorting="gvContrato_Sorting" AllowSorting="True" 
                        OnPageIndexChanging="gvContrato_PageIndexChanging" OnSelectedIndexChanged="gvContrato_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Registro al Sistema" DataField="FechaCrea"  SortExpression="FechaCrea" DataFormatString="{0:dd/MM/yyyy}"/>
                            <asp:BoundField HeaderText="Número del Contrato/Convenio" DataField="NumeroContrato"  SortExpression="NumeroContrato"/>
                            <asp:BoundField HeaderText="Vigencia Fiscal Inicial" DataField="AcnoVigencia"  SortExpression="AcnoVigencia"/>
                            <asp:BoundField HeaderText="Regional del Contrato" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                            <asp:BoundField HeaderText="Categoría del Contrato" DataField="NombreCategoriaContrato"  SortExpression="NombreCategoriaContrato"/>
                            <asp:BoundField HeaderText="Tipo de Contrato/Convenio" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
