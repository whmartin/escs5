﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListDetalle.aspx.cs" Inherits="Page_Contratos_GarantiaAprobacion_ListDetalle" %>



<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" Runat="Server">
    <script type="text/javascript">


        function Confirmacion() {

            return confirm("Esta segúro que desea aprobar la garantía?");
        }

        function Eliminacion() {
            return confirm("Esta segúro que no desea aprobar la garantía?");
        }

</script>
    <asp:HiddenField ID="hfIdContrato" runat="server" />
  <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&uacute;mero del Contrato / Convenio
            </td>
            <td>
               Valor Total de las Garantías</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ReadOnly="true" ID="txtNumeroContrato" Width="200px"  ></asp:TextBox>
            </td>
            <td>
               <asp:TextBox runat="server" ReadOnly="true" ID="txtValorTotalGarantias" Width="200px"  ></asp:TextBox>
               </td>
        </tr>
                
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="GridGarantias" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IDGarantia,IDSucursalAseguradoraContrato,CodigoEstadoGarantia" GridLines="None" Height="16px" OnPageIndexChanging="GridGarantias_PageIndexChanging" OnRowCommand="GridGarantias_RowCommand"  Width="100%">
                        <Columns>
<%--                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="btnAdd" runat="server" CommandName="Add" Height="16px" ImageUrl="~/Image/btn/add.gif" ToolTip="Adicionar" Width="16px" />
                                </HeaderTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Acciones">
                                <HeaderTemplate>
                                    Opciones:
                                </HeaderTemplate>
                                <ItemTemplate>
                                   <asp:ImageButton ID="ImageButton2" runat="server" CommandName="Select" CommandArgument='<%# Container.DataItemIndex%>'  Height="16px" ImageUrl="~/Image/btn/info.jpg" ToolTip="Ver Detalle" Width="16px" />
<%--                                    <asp:ImageButton ID="btnInfo0" runat="server" OnClientClick="if ( ! Confirmacion()) return false;" CommandName="Aprobar" CommandArgument='<%# Container.DataItemIndex%>'  Height="16px" ImageUrl="~/Image/btn/apply.png" ToolTip="Ver Detalle" Width="16px" />
                                    <asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="if ( ! Eliminacion()) return false;" CommandName="Eliminar" CommandArgument='<%# Container.DataItemIndex%>'  Height="16px" ImageUrl="~/Image/btn/Cancel.png" ToolTip="Ver Detalle" Width="16px" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NumeroGarantia" HeaderText="Número de Garantía" />
                            <asp:BoundField DataField="NombreTipoGarantia" HeaderText="Tipo de Garantía" />
                            <asp:BoundField DataField="FechaInicioGarantiaView" HeaderText="Fecha Vigencia Desde" />
                            <asp:BoundField DataField="FechaVencimientoFinalGarantiaView" HeaderText="Fecha Vigencia Hasta" />
                            <%--<asp:BoundField DataField="ValorGarantia" HeaderText="Valor Asegurado" DataFormatString="{0:C}" />--%>
                            <asp:TemplateField HeaderText="Valor Asegurado" ItemStyle-HorizontalAlign="Center" SortExpression="ValorAsegurado">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 150px;">
                                         <%# Convert.ToDecimal(Eval("ValorGarantia")).ToString("$ #,###0.00##;($ #,###0.00##)")%>
                                     </div>
                                 </ItemTemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionEstadoGarantia" HeaderText="Estado" />
                            <asp:BoundField DataField="OrigenAprobación" HeaderText="Orígen" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>


