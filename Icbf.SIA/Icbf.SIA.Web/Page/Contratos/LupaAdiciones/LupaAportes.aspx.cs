﻿using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Page_Contratos_LupaAdiciones_LupaAportes : GeneralWeb
{
    General_General_Master_Lupa _toolBar;
    readonly ContratoService _vContratoService = new ContratoService();
    public string PageName = "Contratos/Adiciones";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _toolBar.LipiarMensajeError();
            if (ValidateAccess(_toolBar, this.PageName, vSolutionPage))
            {
                if (!Page.IsPostBack)
                    CargarRegistro();
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)this.Master;
            _toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            //_toolBar.eventoRetornar += new ToolBarDelegateLupa(btnBuscar_Click);
            _toolBar.OcultarBotonBuscar(true);
            _toolBar.EstablecerTitulos("Aportes", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnGuardar_Click(object sender, EventArgs e)
    {
        if (GuardoEnTabla())
        {
            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            string returnValues = "<script language='javascript'> " +
                                       " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                "</script>";
            ClientScript.RegisterStartupScript(Page.GetType(), "rvDetallePlan", returnValues); 
        }
    }

    private bool GuardoEnTabla()
    {
        bool isValid = false;

        try
        {
            if(hfEsReduccion.Value == "1")
            {
                isValid = GuardarReduccion();
                SetSessionParameter("Reducciones.Guardo", "1");
            }
            else
            {
                isValid = GuardarAdicion();                   
                SetSessionParameter("Adiciones.Guardo", "1");
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }

        return isValid;
    }

    private bool GuardarReduccion()
    {
        bool isValid = false;

        decimal valorAporte;

        if (decimal.TryParse(txtValorAporteInicial.Text.Replace("$", string.Empty), NumberStyles.Number, CultureInfo.CurrentCulture, out valorAporte))
        {
            if(valorAporte > 0 && valorAporte <= decimal.Parse(txtValorAporteActual.Text.Replace("$", string.Empty), NumberStyles.Number, CultureInfo.CurrentCulture))
            {

                int idReduccion = !string.IsNullOrEmpty(hfIdReduccion.Value) ? int.Parse(hfIdReduccion.Value) : 0;
                int idAportePadre = int.Parse(hfIdAporte.Value);
                string usuario = GetSessionUser().Usuario;

                int iddetCons = 0;

                if(idReduccion == 0)
                    iddetCons = int.Parse(hfIdDetConsModContractual.Value);

                if(hfEsCreacion.Value == "1")
                {
                    var result = _vContratoService.CrearAportesReduccion(iddetCons, idReduccion, idAportePadre, valorAporte, usuario);
                    SetSessionParameter("Reducciones.IdReduccion", result);
                }
                else
                {
                    _vContratoService.ActualizarAportesReduccion(idReduccion, valorAporte, idAportePadre, usuario);
                    SetSessionParameter("Reducciones.IdReduccion", idReduccion);
                }

                isValid = true;
            }
            else
                _toolBar.MostrarMensajeError("El aporte debe ser mayor a 0 y deber ser menor al valor del  aporte actual");
        }
        else
            _toolBar.MostrarMensajeError("El formato del valor es incorrecto");

        return isValid;
    }

    private bool GuardarAdicion()
    {
        bool isValid = false;

        decimal valorAporte;

        if (decimal.TryParse(txtValorAporteInicial.Text.Replace("$", string.Empty), NumberStyles.Number, CultureInfo.CurrentCulture, out valorAporte))
        {
            int idAdicion = !string.IsNullOrEmpty(hfIdAdicion.Value) ? int.Parse(hfIdAdicion.Value) : 0;
            int idAportePadre = int.Parse(hfIdAporte.Value);
            string usuario = GetSessionUser().Usuario;

            int iddetCons = 0;

            if (idAdicion == 0)
                iddetCons = int.Parse(hfIdDetConsModContractual.Value);

            if (hfEsCreacion.Value == "1")
                _vContratoService.CrearAportesAdicion(iddetCons, idAdicion, idAportePadre, valorAporte, usuario);
            else
                _vContratoService.actualizarAportesAdicion(idAdicion, valorAporte, idAportePadre, usuario);
            
            isValid = true;
        }
        else
            _toolBar.MostrarMensajeError("El formato del valor es incorrecto");
        
        return isValid;
    }

    private void CargarRegistro()
    {
        try
        {
            hfIdContrato.Value = Request.QueryString["idContrato"];
            hfIdAporte.Value = Request.QueryString["IdAporte"];
            hfIdDetConsModContractual.Value = Request.QueryString["idDetConsModContractual"];

            if (!string.IsNullOrEmpty(Request.QueryString["EsReduccion"]))
                hfEsReduccion.Value = "1";
        
            AporteContrato miAporte = null;

            var aporteInicial = _vContratoService.ObtenerAportesContrato(int.Parse(hfIdContrato.Value), false);

            if(aporteInicial != null)
            {
                int idAporte = int.Parse(hfIdAporte.Value);
                miAporte = aporteInicial.FirstOrDefault(e => e.IdAporteContrato == idAporte);
                txtValorAporteActual.Text = miAporte.ValorAporte.ToString("$ #,###0.00##;($ #,###0.00##)");
            }

            txtProcedenciaAporte.Text = miAporte.AportanteICBF ? "ICBF" : "Cofinanciación Contratista";
            txtTipoAporte.Text = miAporte.AporteEnDinero ? "Dinerio" : "Especie";

            hfProcedencia.Value = miAporte.Procedencia.ToString();
            hfTipoAporte.Value = miAporte.TipoAporte.ToString();

            if (hfEsReduccion.Value == "1")
            {
                hfIdReduccion.Value = Request.QueryString["IdReduccion"];

                if (string.IsNullOrEmpty(hfIdReduccion.Value))
                    hfEsCreacion.Value = "1";
                else
                {
                    int idReduccion = int.Parse(hfIdReduccion.Value);

                    var aportesreduccion = _vContratoService.ObtenerAportesContratoAdicionReduccion(idReduccion, false);

                    if (aportesreduccion != null)
                    {
                        var aporteRedu = aportesreduccion.FirstOrDefault
                            (
                              e => e.AportanteICBF == miAporte.AportanteICBF &&
                                   e.AporteEnDinero == miAporte.AporteEnDinero &&
                                   e.IdReduccion == idReduccion
                            );

                        if (aporteRedu != null)
                        {
                            hfEsCreacion.Value = "0";
                            txtValorAporteInicial.Text = aporteRedu.ValorAporte.ToString("$ #,###0.00##;($ #,###0.00##)");
                        }
                        else
                            hfEsCreacion.Value = "1";
                    }
                }
            }
            else
            {
                hfIdAdicion.Value = Request.QueryString["IdAdicion"];

                if (string.IsNullOrEmpty(hfIdAdicion.Value))
                    hfEsCreacion.Value = "1";
                else
                {
                    int idAdicion = int.Parse(hfIdAdicion.Value);

                    var aportesAdicion = _vContratoService.ObtenerAportesContratoAdicionReduccion(idAdicion, true);

                    if (aportesAdicion != null)
                    {
                        var aporteAdicion = aportesAdicion.FirstOrDefault
                            (
                              e => e.AportanteICBF == miAporte.AportanteICBF &&
                                   e.AporteEnDinero == miAporte.AporteEnDinero &&
                                   e.IdAdicion == idAdicion
                            );

                        if (aporteAdicion != null)
                        {
                            hfEsCreacion.Value = "0";
                            txtValorAporteInicial.Text = aporteAdicion.ValorAporte.ToString("$ #,###0.00##;($ #,###0.00##)");
                        }
                        else
                            hfEsCreacion.Value = "1";
                    }
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }

    }
}