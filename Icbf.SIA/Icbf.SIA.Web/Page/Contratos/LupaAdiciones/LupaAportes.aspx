﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaAportes.aspx.cs" Inherits="Page_Contratos_LupaAdiciones_LupaAportes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

    <script type="text/javascript">

        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }

    </script>

    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfIdReduccion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdAporte" runat="server" />
    <asp:HiddenField ID="hfEsCreacion" runat="server" />
    <asp:HiddenField ID="hfTipoAporte" runat="server"  />
    <asp:HiddenField ID="hfProcedencia" runat="server" />
    <asp:HiddenField ID="hfEsReduccion" runat="server" />

        <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Procedencia del Aporte
            </td>
            <td style="width: 50%">
                 Tipo de Aporte
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox Enabled="false" runat="server" Width="350px"  ID="txtProcedenciaAporte" />
            </td>
            <td>
                <asp:TextBox Enabled="false" runat="server" Width="350px"  ID="txtTipoAporte" />
            </td>
        </tr>

        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial del Aporte</td>
            <td style="width: 50%">
                Valor Aportar
                *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorAporte" ControlToValidate="txtValorAporteInicial"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox Enabled="false" runat="server" Width="350px"    ID="txtValorAporteActual" />
            </td>
            <td>
                <asp:TextBox runat="server" Width="350px"    ID="txtValorAporteInicial" />
                <Ajax:FilteredTextBoxExtender ID="ftValorUnitario" runat="server" TargetControlID="txtValorAporteInicial"
                FilterType="Numbers,Custom" ValidChars=".,$" />
            </td>
        </tr>
        
        
        </table>


</asp:Content>

