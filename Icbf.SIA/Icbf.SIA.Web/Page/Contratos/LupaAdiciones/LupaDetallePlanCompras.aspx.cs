﻿using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsContratosPacco;

public partial class Page_Contratos_LupaAdiciones_LupaDetallePlanCompras : GeneralWeb
{
    General_General_Master_Lupa _toolBar;
    readonly ContratoService _vContratoService = new ContratoService();
    public WSContratosPACCOSoap WsContratoPacco = new WSContratosPACCOSoapClient();
    public string PageName = "Contratos/Adiciones";

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _toolBar.LipiarMensajeError();
            if (ValidateAccess(_toolBar, this.PageName, vSolutionPage))
            {
                if (!Page.IsPostBack)
                    CargarRegistro();
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)this.Master;;
            _toolBar.OcultarBotonBuscar(true);
            _toolBar.OcultarBotonGuardar(true);
            _toolBar.OcultarBotonNuevo(true);
            _toolBar.EstablecerTitulos("Detalle Plan Compras Adición", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            hfIdAdicion.Value = Request.QueryString["IdEsAdicion"];
            hfIdContrato.Value = Request.QueryString["idContrato"];

            int vIdContrato = Convert.ToInt32(hfIdContrato.Value);

            var myGridResults = _vContratoService.ConsultarPlanComprasContratoss(null,
                                                                                 vIdContrato,
                                                                                 null,
                                                                                 null);

            gvConsecutivoContratoConvenio.DataSource = myGridResults;
            gvConsecutivoContratoConvenio.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void gvConsecutivoContratoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConsecutivoContratoConvenio.SelectedRow);
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            var rowIndex = pRow.RowIndex;

            var vNumeroConsecutivo = Convert.ToInt32(gvConsecutivoContratoConvenio.Rows[rowIndex].Cells[1].Text);
            var vVigencia = Convert.ToInt32(gvConsecutivoContratoConvenio.Rows[rowIndex].Cells[2].Text);

            hfNumConsecutivoPlanCompras.Value = vNumeroConsecutivo.ToString();
            hfVigencia.Value = vVigencia.ToString();

            CargarGrillaProductos(gvProductoContratoConvenio, GridViewSortExpression, true, true);
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaProductos(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion, bool mostrarMensaje)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var vNumeroConsecutivo = Convert.ToInt32(hfNumConsecutivoPlanCompras.Value);
            var vVigencia = Convert.ToInt32(hfVigencia.Value);

            var vMyGrid = _vContratoService.ConsultarPlanComprasProductoss(vNumeroConsecutivo,
                                                                            null, null, null, null,
                                                                            null, null, null, null, null);

            int idAdcion = int.Parse(hfIdAdicion.Value);

            if(vMyGrid != null)
            vMyGrid = vMyGrid.Where(e => e.IdAdicion == idAdcion).ToList();

            var vGetListaDetalleProducto = WsContratoPacco.GetListaDetalleProductoPACCO(vVigencia, vNumeroConsecutivo).OrderBy(d => d.codigo_producto);

            var myGridResults = (from vListProd in vGetListaDetalleProducto
                                 join vMyg in vMyGrid
                                     //on vListProd.consecutivo equals vMyg.NumeroConsecutivoPlanCompras
                                 on vListProd.iddetalleobjetocontractual.ToString() + vListProd.consecutivo.ToString() equals // TEMPORAL
                                 vMyg.IdDetalleObjeto + vMyg.NumeroConsecutivoPlanCompras.ToString()
                                 select new PlanComprasProductos
                                 {
                                     IdPlanDeComprasContratos = vMyg.IdPlanDeComprasContratos,
                                     IdProductoPlanCompraContrato = vMyg.IdProductoPlanCompraContrato,
                                     NumeroConsecutivoPlanCompras = vListProd.consecutivo,
                                     CodigoProducto = vListProd.codigo_producto,
                                     NombreProducto = vListProd.nombre_producto,
                                     TipoProducto = vListProd.tipoProductoView,
                                     CantidadCupos = (vMyg.CantidadCupos == null ? Convert.ToInt32(vListProd.cantidad) : Convert.ToInt32(vMyg.CantidadCupos)),//, // 
                                     ValorUnitario = (vMyg.ValorUnitario == null ? vListProd.valor_unitario : vMyg.ValorUnitario), //vMyg.ValorUnitario, //
                                     //ValorTotal = vListProd.valor_total,
                                     //ValorTotal = (vMyg.ValorTotal == null ? vListProd.valor_total : vMyg.ValorTotal), //vListProd.valor_total
                                     ValorTotal = vListProd.tipoProductoView.ToUpper() == "SERVICIO" ?
                                                       Convert.ToDecimal((vMyg.CantidadCupos == null ? Convert.ToInt32(vListProd.cantidad) : Convert.ToInt32(vMyg.CantidadCupos))) *
                                                       Convert.ToDecimal((vMyg.ValorUnitario == null ? vListProd.valor_unitario : vMyg.ValorUnitario)) *
                                                       Convert.ToDecimal((vMyg.Tiempo == null ? Convert.ToDecimal(vListProd.tiempo) : Convert.ToDecimal(vMyg.Tiempo))) :
                                                       Convert.ToDecimal(vMyg.CantidadCupos == null ? Convert.ToInt32(vListProd.cantidad) : Convert.ToInt32(vMyg.CantidadCupos)) *
                                                       Convert.ToDecimal((vMyg.ValorUnitario == null ? vListProd.valor_unitario : vMyg.ValorUnitario)),
                                     Tiempo = (vMyg.Tiempo == null ? Convert.ToDecimal(vListProd.tiempo) : Convert.ToDecimal(vMyg.Tiempo)), //, //
                                     UnidadTiempo = (vMyg.UnidadTiempo == null ? vListProd.tipotiempoView : vMyg.UnidadTiempo), //vMyg.UnidadTiempo, //
                                     UnidadMedida = (vMyg.IdUnidadTiempo == null ? vListProd.unidad_medida : vMyg.IdUnidadTiempo) //vMyg.IdUnidadTiempo //
                                 }).ToList();


            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                {
                    var param = Expression.Parameter(typeof(PlanComprasProductos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<PlanComprasProductos, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();

            if (myGridResults.Count == 0 && mostrarMensaje)
            {
                throw new Exception("El Consecutivo Plan de Compras seleccionado no tiene productos o rubros para relacionar");
            }
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.InnerException.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }
}