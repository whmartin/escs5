<%@ Page Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="LupasProductosPlanCompras.aspx.cs" Inherits="Page_PlanComprasProductos_LupasProductosPlanCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
       <asp:HiddenField ID="hfIdContrato" runat="server" />
       <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
       <asp:HiddenField ID="hfValorActualProducto" runat="server" />
       <asp:HiddenField ID="hfIdDetalleObjeto" runat="server" />
       <asp:HiddenField ID="hfCantidadCupos" runat="server" />
            <asp:HiddenField ID="hfValorActualModificacion" runat="server" />
    <asp:HiddenField ID="hfTipoProducto" runat="server" />
    <asp:HiddenField ID="hfValorContrato" runat="server" />
    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfTipoContratoConvenio" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Código del producto *
                <asp:RequiredFieldValidator runat="server" ID="rfvCodigoProducto" ControlToValidate="txtCodigoProducto"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">
                Nombre del producto *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreProducto" ControlToValidate="txtNombreProducto"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCodigoProducto" MaxLength="30" Width="250px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCodigoProducto" runat="server" TargetControlID="txtCodigoProducto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtNombreProducto" MaxLength="400" Width="250px"
                    Enabled="False" TextMode="MultiLine"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreProducto" runat="server" TargetControlID="txtNombreProducto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad de medida *
                <asp:RequiredFieldValidator runat="server" ID="rfvUnidadMedida" ControlToValidate="txtUnidadMedida"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">
                Tipo producto *
                <asp:RequiredFieldValidator runat="server" ID="rfvTipoProducto" ControlToValidate="txtTipoProducto"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtUnidadMedida" MaxLength="50" Width="250px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftUnidadMedida" runat="server" TargetControlID="txtUnidadMedida"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtTipoProducto" MaxLength="10" Width="250px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoProducto" runat="server" TargetControlID="txtTipoProducto"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Cantidad/Cupos *
                <asp:RequiredFieldValidator runat="server" ID="rfvCantidadCupos" ControlToValidate="txtCantidadCupos"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">
                &nbsp; Valor Total Actual&nbsp; Modificación
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtCantidadCupos" MaxLength="15" Width="250px" AutoPostBack="True"
                    OnTextChanged="txtCantidadCupos_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftCantidadCupos" runat="server" TargetControlID="txtCantidadCupos"
                    FilterType="Numbers,Custom" ValidChars="/áéíóúÁÉÍÓÚñÑ @_():;" />
            </td>
            <td colspan="2">
                <asp:Label ID="LblValorTotalModificacionActual" runat="server" Text=""></asp:Label>
                    
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tiempo *
                <asp:RequiredFieldValidator runat="server" ID="rfvTiempo" ControlToValidate="txtTiempo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Valor unitario *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorUnitario" ControlToValidate="txtValorUnitario"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTiempo" MaxLength="32" Width="250px" AutoPostBack="True"
                     OnTextChanged="txtTiempo_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTiempo" runat="server" TargetControlID="txtTiempo"
                    FilterType="Numbers,Custom" ValidChars="" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorUnitario" MaxLength="32" Width="250px" AutoPostBack="True"
                    OnTextChanged="txtValorUnitario_TextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorUnitario" runat="server" TargetControlID="txtValorUnitario"
                    FilterType="Numbers,Custom" ValidChars=".,$" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowB">
            <td>
                Unidad de tiempo *
                <asp:RequiredFieldValidator runat="server" ID="rfvUnidadTiempo" ControlToValidate="ddlUnidadTiempo"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" InitialValue="-1"></asp:RequiredFieldValidator>
            </td>
            <td colspan="2">
                Valor total *
                <asp:RequiredFieldValidator runat="server" ID="rfvValorTotal" ControlToValidate="txtValorTotal"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                                <asp:DropDownList runat="server" ID="ddlUnidadTiempo" Width="250px" />
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtValorTotal" MaxLength="32" Width="250px" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftValorTotal" runat="server" TargetControlID="txtValorTotal"
                    FilterType="Numbers,Custom" ValidChars=".,$" />
                    
            </td>
        </tr>
        <tr class="rowB">
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:Label ID="lblJusticiación" Text="Justificación adición superior al 50% *" runat="server" Visible="false" />
                <asp:CustomValidator runat="server" ID="CustomValidator" Visible="false" ControlToValidate="txtJustificacionAdicion" SetFocusOnError="true"
                 ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" ForeColor="Red" OnServerValidate="CustomValidator_ServerValidate" />    
                    
            </td>
        </tr>
        <tr class="rowA">
            <td>
                                &nbsp;</td>
            <td colspan="2">
            <asp:TextBox runat="server" ID="txtJustificacionAdicion" Width="320px" Height="65px" Visible="false"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftJustificacionAdicion" runat="server" TargetControlID="txtJustificacionAdicion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
            </td>
        </tr>
        </table>
                    <img src="../../../Image/main/loading.gif" name="imgLoading" id="imgLoading" class="loading"
                    alt="" />
    <script src="../../../Scripts/formatoNumeros.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
       
        function muestraImagenLoading() {

            var btnguardar = 'btnGuardar';
            document.getElementById(btnguardar).setAttribute("disabled", "true");
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {

            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
            var btnguardar = 'btnGuardar';
            document.getElementById(btnguardar).setAttribute("disabled", "false");
        }
        
        </script>





</asp:Content>
