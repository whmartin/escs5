using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using System.Reflection;
using Icbf.Contrato.Service;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;


/// <summary>
/// Página de consulta a través de filtros para la entidad RegistroInformacionPresupuestal
/// </summary>
public partial class Page_LupasAdiciones_RegistroInformacionPresupuestal_List : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/RegistroInformacionPresupuestal";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();
    public const int vNumeroRegistros = 501;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvRegistroInformacionPresupuestal, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.HabilitarBotonGuardar(false);
            gvRegistroInformacionPresupuestal.PageSize = PageSize() + 5;
            gvRegistroInformacionPresupuestal.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Registro Información Presupuestal", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";
        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRegistroInformacionPresupuestal.DataKeys[rowIndex].Value.ToString();
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRegistroInformacionPresupuestal_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRegistroInformacionPresupuestal.SelectedRow);
    }
    protected void gvRegistroInformacionPresupuestal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegistroInformacionPresupuestal.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvRegistroInformacionPresupuestal_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            int? vVigenciaFiscal = null;
            int? vRegionalICBF = null;
            string vNumeroCDP = null;
            int? vArea = null;
            Decimal? vValorTotalDesde = null;
            Decimal? vValorTotalHasta = null;
            DateTime? vFechaCDPDesde = null;
            DateTime? vFechaCDPHasta = null;
            string error = string.Empty;
            int vRegistros = 0;
            bool flag = false;

            toolBar.QuitarMensajeError();

            if (txtFechaCDPDesde.Date.Year.ToString() != "1900" && txtFechaCDPHasta.Date.Year.ToString() == "1900")
            {
                error += "Debe registrar una fecha desde y una fecha hasta.</br>";
                flag = true;
            }

            if (txtFechaCDPHasta.Date.Year.ToString() != "1900" && txtFechaCDPDesde.Date.Year.ToString() == "1900")
            {
                error += "Debe registrar una fecha desde y una fecha hasta.</br>";
                flag = true;
            }


            if (txtValorTotalDesde.Text == string.Empty && txtValorTotalHasta.Text != string.Empty)
            {
                error += "Debe registrar el valor total hasta y valor Total Desde.</br>";
                flag = true;
            }

            if (txtValorTotalDesde.Text != string.Empty && txtValorTotalHasta.Text == string.Empty)
            {
                error += "Debe registrar el valor total hasta y valor Total Desde.</br>";
                flag = true;
            }

            if (txtValorTotalDesde.Text != string.Empty && txtValorTotalHasta.Text != string.Empty)
            {
                string valorTotalDesde = txtValorTotalDesde.Text.Replace(@"$", "").Replace(",", "").Replace(".00", "");
                string valorTotalHasta = txtValorTotalHasta.Text.Replace(@"$", "").Replace(",", "").Replace(".00", "");
                decimal vDTotalDesde = 0;
                decimal vDTotalHasta = 0;

                if (!decimal.TryParse(valorTotalDesde, out vDTotalDesde))
                {
                    toolBar.MostrarMensajeError("Ingrese un valor válido.");
                    return;
                }

                if (!decimal.TryParse(valorTotalHasta, out vDTotalHasta))
                {
                    toolBar.MostrarMensajeError("Ingrese un valor válido.");
                    return;
                }
                vValorTotalDesde = vDTotalDesde;
                vValorTotalHasta = vDTotalHasta;

                if (vValorTotalHasta < vValorTotalDesde)
                {
                    error += "El valor total hasta debe ser mayor o igual que el valor total Desde.</br>";
                    flag = true;
                }
            }

            if (txtFechaCDPHasta.Date.Year.ToString() != "1900" && txtFechaCDPDesde.Date.Year.ToString() != "1900")
            {
                if (ddlVigenciaFiscal.SelectedValue != "-1")
                {
                    int vigencia = Convert.ToInt32(ddlVigenciaFiscal.SelectedItem.Text);
                    if (vigencia != Convert.ToInt32(txtFechaCDPDesde.Year.ToString()) && vigencia != Convert.ToInt32(txtFechaCDPHasta.Year.ToString()))
                    {
                        error += "La fecha debe corresponder a la vigencia registrada.</br>";
                        flag = true;
                    }
                }
            }

            if (ddlVigenciaFiscal.SelectedValue != "-1")
            {
                vVigenciaFiscal = Convert.ToInt32(ddlVigenciaFiscal.SelectedItem.Text);
            }
            if (ddlRegionalICBF.SelectedValue != "-1")
            {
                vRegionalICBF = Convert.ToInt32(ddlRegionalICBF.SelectedValue);
            }
            if (txtNumeroCDP.Text != "")
            {
                vNumeroCDP = txtNumeroCDP.Text;
            }
            if (ddlArea.SelectedValue != "-1")
            {
                vArea = Convert.ToInt32(ddlArea.SelectedValue);
            }
            //if (txtValorTotalDesde.Text!= "")
            //{
            //    vValorTotalDesde = Convert.ToDecimal(txtValorTotalDesde.Text);
            //}
            //if (txtValorTotalHasta.Text!= "")
            //{
            //    vValorTotalHasta = Convert.ToDecimal(txtValorTotalHasta.Text);
            //}

            if (txtFechaCDPDesde.Date.Year.ToString() != "1900")
            {
                vFechaCDPDesde = Convert.ToDateTime(txtFechaCDPDesde.Date);
            }
            if (txtFechaCDPHasta.Date.Year.ToString() != "1900")
            {
                vFechaCDPHasta = Convert.ToDateTime(txtFechaCDPHasta.Date);
            }

            if (flag)
            {
                if (!string.IsNullOrEmpty(error))
                {
                    toolBar.MostrarMensajeError(error);
                    gridViewsender.Visible = false;
                    return;

                }

            }
            if (ddlNumeroRegistros.SelectedValue != "-1")
            {
                if (!int.TryParse(ddlNumeroRegistros.SelectedValue, out vRegistros))
                {
                    toolBar.MostrarMensajeError("La operación no se pudo realizar, intentelo nuevamente.");
                    return;
                }

            }
            else
            {
                vRegistros = vNumeroRegistros;
            }
            var myGridResults = vContratoService.ConsultarRegistroInformacionPresupuestals(vVigenciaFiscal, vRegionalICBF, vNumeroCDP, vArea, vValorTotalDesde, vValorTotalHasta, vFechaCDPDesde, vFechaCDPHasta, vRegistros);

            int nRegistros = myGridResults.Count;
            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));


            lblResultadosCDP.Visible = true;

            if (nRegistros < NumRegConsultaGrilla)
            {
                lblResultadosCDP.Visible = true;
                gridViewsender.Visible = true;
                //////////////////////////////////////////////////////////////////////////////////
                //////Fin del código de llenado de datos para la grilla 
                //////////////////////////////////////////////////////////////////////////////////
                gridViewsender.DataSource = myGridResults;
                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(RegistroInformacionPresupuestal), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression = Expression.Lambda<Func<RegistroInformacionPresupuestal, object>>(Expression.Convert(prop, typeof(object)), param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults;
                }

                gridViewsender.DataBind();
            }
            else
            {
                toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
                toolBar.HabilitarBotonGuardar(false);
                gridViewsender.Visible = false;
                lblResultadosCDP.Visible = false;

            }



        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaVigencia();
            CargarListaRegional();
            CargarListaNumeroRegistros();
            //CargarListaArea();
            ddlArea.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {

        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscal, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");


    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {
            int? idRegional = usuario.IdRegional;
            if ((idRegional == null) && (Request.QueryString["IdRegContrato"] != null))
            {
                idRegional = Convert.ToInt32(Request.QueryString["IdRegContrato"]);
            }


            if (usuario.TipoUsuario != 1)
            {
                Regional vRegional = new Regional();
                vRegional = vRuboService.ConsultarRegional(idRegional);
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF, vRuboService.ConsultarRegionalPCIs(vRegional.CodigoRegional, null), "IdRegional", "CodigoNombreRegional");
                if (ddlRegionalICBF.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlRegionalICBF.SelectedValue = usuario.IdRegional.ToString();
                        ddlRegionalICBF.Enabled = false;
                    }
                    else
                    {
                        ddlRegionalICBF.SelectedValue = idRegional.ToString(); // EC-05/08/2014 incidente reportado por Sandra Mendez
                        ddlRegionalICBF.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ddlRegionalICBF, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "CodigoNombreRegional");
                ddlRegionalICBF.SelectedValue = idRegional.ToString();
                ddlRegionalICBF.Enabled = false;
            }

        }

    }

    public void CargarListaArea()
    {
        if (ddlRegionalICBF.SelectedValue != "-1")
        {
            ManejoControlesContratos.LlenarComboLista(ddlArea, vContratoService.ConsultarAreasInternas(Convert.ToInt32(ddlRegionalICBF.SelectedValue)), "Codigo", "CodigoDescripcion");
        }
        else
        {
            ddlArea.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlArea.SelectedValue = "-1";
        }
    }

    public void CargarListaNumeroRegistros()
    {

        ddlNumeroRegistros.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        ddlNumeroRegistros.Items.Insert(1, new ListItem("5", "5"));
        ddlNumeroRegistros.Items.Insert(2, new ListItem("10", "10"));
        ddlNumeroRegistros.Items.Insert(3, new ListItem("15", "15"));
        ddlNumeroRegistros.SelectedValue = "-1";
    }

    private void SeleccionarRegistros(string IdContratosCDP)
    {
        string[] strContratosSplit = IdContratosCDP.Split('|');

        try
        {
            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();" +
            "    pObj[0] = '1';";

            for (int c = 0; c < strContratosSplit.Count(); c++)
            {
                returnValues += "pObj[" + (c + 1) + "] = '" + HttpUtility.HtmlDecode(strContratosSplit[c]) + "';";
            }

            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void chbContiene_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox item = (CheckBox)sender;

        bool flag = false;
        foreach (GridViewRow fila in gvRegistroInformacionPresupuestal.Rows)
        {
            var check = (CheckBox)fila.Cells[0].FindControl("chbContiene");

            if (check.ClientID != item.ClientID)
                check.Checked = false;

            if (check.Checked)
                flag = true;
        }
        if (flag)
        {
            toolBar.HabilitarBotonGuardar(true);
        }
        else
        {
            toolBar.HabilitarBotonGuardar(false);
        }
    }
    
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    private void Guardar()
    {
        if (gvRegistroInformacionPresupuestal.Rows.Count > 0)
        {
            bool flag = false;
            string IdContratosCDP = string.Empty;
            int vIdEtlCDP = 0;

            int idAdicion = int.Parse(Request.QueryString["IdAdicion"]);
            int idContrato = int.Parse(Request.QueryString["IdContrato"]);

            var adicion = vContratoService.ConsultarAdiciones(idAdicion);

            var aportesContratos = vContratoService.ConsultarAporteContratos(true,null,null,null,true,idContrato,null,null,null,null);

            IEnumerable<AporteContrato> miAporte = null;

            if (aportesContratos != null)
                miAporte = aportesContratos.Where(e => e.IdAdicion == idAdicion && e.AportanteICBF == true && e.AporteEnDinero == true);

            if (gvRegistroInformacionPresupuestal.Rows.Count > 0)
            {
                List<ContratosCDP> lContratosCDP = new List<ContratosCDP>();
                int count = 0;
                foreach (GridViewRow fila in gvRegistroInformacionPresupuestal.Rows)
                {

                    if ((fila.Cells[0].FindControl("chbContiene") as CheckBox).Checked)
                    {
                        flag = true;
                        if (!int.TryParse(gvRegistroInformacionPresupuestal.Rows[fila.RowIndex].Cells[3].Text.Trim(), out vIdEtlCDP))
                        {
                            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                            return;
                        }

                        if (miAporte != null)
                        { 
                            decimal valorCDP;

                            if (decimal.TryParse(gvRegistroInformacionPresupuestal.Rows[fila.RowIndex].Cells[5].Text.Trim().Replace("$", string.Empty), out valorCDP))
                            {
                                if (miAporte.Sum(e1 => e1.ValorAporte) > valorCDP)
                                {
                                    toolBar.MostrarMensajeError("El valor del aporte es mayor al aporte del cdp.");
                                    return;
                                }
                            }
                            else
                            {
                                toolBar.MostrarMensajeError("El formato del valor del CDP es incorrecto.");
                                return;
                            }
                        }
                        else
                            {  
                                toolBar.MostrarMensajeError("No hay un modificación en dinero para asociar el RP");
                                return;
                            }

                        if (vContratoService.ConsultarContratosCDP(idContrato, vIdEtlCDP))
                        {
                            toolBar.MostrarMensajeError("El contrato " + idContrato + " ya esta asociado al número de CDP " + fila.Cells[3].Text);
                            return;
                        }

                        ContratosCDP contratosCDP = new ContratosCDP();
                        contratosCDP.IdContrato = Convert.ToInt32(idContrato);
                        contratosCDP.IdCDP = vIdEtlCDP;
                        contratosCDP.EsAdicion = true;
                        contratosCDP.IdAdicion = idAdicion;
                        contratosCDP.UsuarioCrea = GetSessionUser().NombreUsuario;

                        int vResultado = vContratoService.InsertarContratosCDP(contratosCDP);

                        if (vResultado == 0)
                        {
                            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                        }
                        else if (vResultado >= 1)
                        {
                            toolBar.MostrarMensajeGuardado();
                            SetSessionParameter("Contrato.ContratoCDP.IdContratoCDP", contratosCDP.IdContratosCDP);
                            if (count == 0)
                            {
                                IdContratosCDP = contratosCDP.IdContratosCDP.ToString().Trim();
                            }
                            else
                            {
                                IdContratosCDP += "|" + contratosCDP.IdContratosCDP.ToString().Trim();
                            }
                        }
                        else
                        {
                            toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                        }


                        lContratosCDP.Add(contratosCDP);
                        count++;
                    }
                }
                if (!flag)
                {
                    return;
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La grilla no arrojó registros asociados.");
            }

            SeleccionarRegistros(IdContratosCDP);
        }

    }

    protected void ddlRegionalICBF_SelectedIndexChanged(object sender, EventArgs e)
    {
        //CargarListaArea();
    }
}
