﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master" AutoEventWireup="true" CodeFile="LupaDetallePlanCompras.aspx.cs" Inherits="Page_Contratos_LupaAdiciones_LupaDetallePlanCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" Runat="Server">

    <script type="text/javascript">

        function EsEspacio(evt, control) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode != 32) {
                return true;
            } else if (charCode == 32 && control.value.length >= 1)
                return true
            else if (charCode == 32 && control.value.length == 0)
                return false;
            else
                return false;
        }

    </script>

    <asp:HiddenField ID="hfIdAdicion" runat="server" />
    <asp:HiddenField ID="hfNumConsecutivoPlanCompras" runat="server" />
    <asp:HiddenField ID="hfVigencia" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />

        <table>
        <tr class="rowB">
            <td>
                       Plan de Compras
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" ID="gvConsecutivoContratoConvenio" AutoGenerateColumns="False"
                            AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdPlanDeComprasContratos,CodigoRegional,IdContrato"
                            CellPadding="0" Height="16px" AllowSorting="True" 
                            OnSelectedIndexChanged="gvConsecutivoContratoConvenio_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Cargar productos y rubros" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Número consecutivo plan de compras" DataField="NumeroConsecutivoPlanCompras"
                                    SortExpression="NumeroConsecutivoPlanCompras"></asp:BoundField>
                                <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" SortExpression="Vigencia" />
                                <%--<asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
<%--                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminarContrato" runat="server" CommandName="Eliminar" ImageUrl="~/Image/btn/delete.gif"
                                            Height="16px" Width="16px" ToolTip="Eliminar" OnClick="btnEliminarContrato_OnClick"
                                            OnClientClick="javascript:if (!confirm('&#191;Est&#225; seguro de que desea eliminar el registro?')) return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
    <table>
        <tr class="rowB">
            <td>
                            Productos Plan de Compras
                </td>
        </tr>
    </table>
                        <asp:GridView runat="server" ID="gvProductoContratoConvenio" AutoGenerateColumns="False"
                            AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IDProductoPlanCompraContrato,NumeroConsecutivoPlanCompras"
                            CellPadding="0" Height="16px" AllowSorting="True" 
                            >
                            <Columns>
                                <asp:BoundField HeaderText="Número consecutivo plan de compras" DataField="NumeroConsecutivoPlanCompras"
                                    SortExpression="NumeroConsecutivoPlanCompras" ItemStyle-Width="50px" />
                                <asp:BoundField HeaderText="Código del producto" DataField="CodigoProducto" SortExpression="CodigoProducto" />
                                <asp:BoundField HeaderText="Nombre del producto" DataField="NombreProducto" SortExpression="NombreProducto" />
                                <asp:BoundField HeaderText="Tipo producto" DataField="TipoProducto" SortExpression="TipoProducto" />
                                <asp:BoundField HeaderText="Cantidad/Cupos" DataField="CantidadCupos" SortExpression="CantidadCupos" />
                                <asp:BoundField HeaderText="Valor unitario" DataField="ValorUnitario" SortExpression="ValorUnitario"
                                    DataFormatString="{0:C}" />
                                <asp:BoundField HeaderText="Tiempo" DataField="Tiempo" SortExpression="Tiempo" />
                                <asp:BoundField HeaderText="Valor total" DataField="ValorTotal" SortExpression="ValorTotal"
                                    DataFormatString="{0:C}" />
                                <asp:BoundField HeaderText="Unidad de tiempo" DataField="UnidadTiempo" SortExpression="UnidadTiempo" />
                                <asp:BoundField HeaderText="Unidad de medida" DataField="UnidadMedida" SortExpression="UnidadMedida" />
<%--                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDetalle" runat="server" CommandName="Select" ImageUrl="~/Image/btn/edit.gif"
                                            Height="16px" Width="16px" ToolTip="Editar productos Contrato/Convenios" />
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminarProducto" runat="server" CommandName="Eliminar" ImageUrl="~/Image/btn/delete.gif"
                                            Height="16px" Width="16px" ToolTip="Eliminar" OnClick="btnEliminarProducto_OnClick"
                                            OnClientClick="javascript:if (!confirm('&#191;Est&#225; seguro de que desea eliminar el registro?')) return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>

</asp:Content>

