using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using WsContratosPacco;

/// <summary>
/// Página de registro y edición para la entidad PlanComprasProductos
/// </summary>
public partial class Page_PlanComprasProductos_LupasProductosPlanCompras : GeneralWeb
{
    #region Variables y Constantes

    public General_General_Master_Lupa _toolBar;
    private readonly ContratoService _vContratoService = new ContratoService();
    private const string PageName = "Contrato/PlanComprasContratos";
    private const string TIPOCONTRATOCONVENIO = "CONVENIO INTERADMINISTRATIVO";
    private readonly ManejoControlesContratos _vManejoControlesContratos = new ManejoControlesContratos();
    private readonly WSContratosPACCOSoap _wsContratosPacco = new WSContratosPACCOSoapClient();

    public decimal ValorContrato
    {
        get { return decimal.Parse(hfValorContrato.Value); }
        set { hfValorContrato.Value = value.ToString(); }
    }

    public decimal ValorActualModificacion
    {
        get { return decimal.Parse(hfValorActualModificacion.Value); }
        set { hfValorActualModificacion.Value = value.ToString(); }
    }

    public string TipoProducto
    {
        get { return hfTipoProducto.Value; }
        set { hfTipoProducto.Value = value; }
    }
    
    public string TipoContratoConvenio
    {
        get { return hfTipoContratoConvenio.Value; }
        set { hfTipoContratoConvenio.Value = value; }
    }
 
    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(_toolBar, PageName, vSolutionPage))
        {
            _toolBar.QuitarMensajeError();
            if (!Page.IsPostBack)
            {
                VerficarQueryStrings();
                CargarDatosIniciales();
                txtCantidadCupos.Attributes.Add("onchange", "muestraImagenLoading();");
                txtTiempo.Attributes.Add("onchange", "muestraImagenLoading();");
                txtValorUnitario.Attributes.Add("onchange", "muestraImagenLoading();");
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
            GuardarProductoAdicion();           
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {

        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rvDetallePlan", returnValues); 
    }

    protected void txtCantidadCupos_TextChanged(object sender, EventArgs e)
    {
        CalcularValorTotal();
    }

    protected void txtValorUnitario_TextChanged(object sender, EventArgs e)
    {
        CalcularValorTotal();
    }

    protected void txtTiempo_TextChanged(object sender, EventArgs e)
    {
        CalcularValorTotal();
    }

    protected void CustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        decimal valorTotal = decimal.Parse(txtValorTotal.Text, NumberStyles.Currency);
        decimal valorTotalInicial = decimal.Parse(hfValorActualProducto.Value, NumberStyles.Currency);

        decimal diferencia = Math.Abs(valorTotal - valorTotalInicial);
        decimal valorMitadContrato = ValorContrato / 2;

        if (diferencia > valorMitadContrato && string.IsNullOrEmpty(txtJustificacionAdicion.Text) && TipoContratoConvenio == TIPOCONTRATOCONVENIO)
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    #endregion

    #region Funciones y Procedimientos

    /// <summary>
    /// Valida si se puede o no guardar la reducción y/o Adicción.
    /// </summary>
    /// <returns>
    ///  Bandera que indica si se puede o no guardar.
    /// </returns>
    private bool ValidarProductos()
    {
        bool isvalid = true;

        string vVigencia = Convert.ToString(Request.QueryString["Vigencia"]);
        var vNumeroCOnsecutivo = Convert.ToInt32(Request.QueryString["Consecutivo"]);
        var vIdContrato = Convert.ToInt32(hfIdContrato.Value);

        //var vWsContratoPacco = _wsContratosPacco.GetListaDetalleProductoPACCO(Convert.ToInt32(vVigencia), vNumeroCOnsecutivo);

        //var myGridResults = _vContratoService.ConsultarSolicitudModPlanComprass(null, vIdContrato, null, null);

        if (_vContratoService.ExisteEnviadoSolicitudModPlanCompras("001", vNumeroCOnsecutivo, vVigencia))
        {
            throw new Exception("El consecutivo seleccionado tiene una solicitud pendiente");
        }

        return isvalid;
    }

    /// <summary>
    /// 
    /// </summary>
    private void GuardarProductoAdicion()
    {
        try
        {
            decimal valorAdicionado = decimal.Parse(txtValorTotal.Text.Replace("$",string.Empty), NumberStyles.Number);

            if (ValorActualModificacion < valorAdicionado)
            {
                int idAdicion = 0;

                if (!string.IsNullOrEmpty(hfIdAdicion.Value))
                    idAdicion = int.Parse(hfIdAdicion.Value);
                else
                    idAdicion = GuardarAdicion();

                if (idAdicion != 0 && ValidarProductos())
                {
                    int idAporte = int.Parse(Request.QueryString["IdAporte"]);

                    var vProductosPlanCompras = new ProductoPlanComprasContratos
                    {
                        IDProductoPlanCompraContrato = Convert.ToInt32(Request.QueryString["ProductoPlanCompraContrato"]),
                        IDPlanDeComprasContratos = Convert.ToInt32(Request.QueryString["IdPlanCompras"]),
                        IDProducto = Convert.ToString(txtCodigoProducto.Text),
                        CantidadCupos = Convert.ToDecimal(txtCantidadCupos.Text),
                        ValorProducto = decimal.Parse(txtValorUnitario.Text, NumberStyles.Currency),
                        Tiempo = decimal.Parse(txtTiempo.Text, NumberStyles.Currency),
                        UnidadTiempo = ddlUnidadTiempo.SelectedItem.Text,
                        IdUnidadTiempo = Convert.ToInt32(ddlUnidadTiempo.SelectedValue),
                        IdDetalleObjeto = hfIdDetalleObjeto.Value,
                        UsuarioCrea = GetSessionUser().NombreUsuario,
                        IdAdicion = idAdicion,
                        IsAdicion = true,
                        ValorAdicion = Math.Abs(ValorActualModificacion - valorAdicionado)
                    };

                    var vResultado = 1;

                    if (_vContratoService.VerificarExistenciaModificacionProducto(
                                                                                 vProductosPlanCompras.IDPlanDeComprasContratos.Value,
                                                                                 vProductosPlanCompras.IDProducto,
                                                                                 true,
                                                                                 vProductosPlanCompras.IdAdicion.Value
                                                                                ))
                        vResultado = _vContratoService.ModificarProductoPlanComprasContratoAdiciones(vProductosPlanCompras, idAporte);
                    else
                        vResultado = _vContratoService.InsertarProductoPlanComprasContratoAdiciones(vProductosPlanCompras, idAporte);

                    if (vResultado == 0)
                        _toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                    else if (vResultado >= 1)
                    {
                        SetSessionParameter("Adiciones.Guardo", "1");
                        SetSessionParameter("Adiciones.IdAdiccion", idAdicion);
                        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                        string returnValues =
                           "<script language='javascript'> " +
                           "var pObj = Array();" +
                           "pObj[" + (0) + "] = '1,1';" +
                                " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                                " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                            "</script>";

                        ClientScript.RegisterStartupScript(Page.GetType(), "rvProductos", returnValues);
                    }
                    else
                        _toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó más registros de los esperados, verifique por favor.");

                }
            }
            else
                _toolBar.MostrarMensajeError("El valor que se adiciona debe ser mayor al valor actual del producto");
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.InnerException.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }

    }

    /// <summary>
    ///  Guarda una adicción en el sistema.
    /// </summary>
    /// <returns>
    ///  Id de la Adicción que se guarda en el sistema.
    /// </returns>
    private int GuardarAdicion()
    {
        int result = 0;

        try
        {
            var valorActual = decimal.Parse(hfValorActualProducto.Value, NumberStyles.Currency);
            var valorAdiccion = decimal.Parse(txtValorTotal.Text, NumberStyles.Currency);

           var contrato = _vContratoService.ConsultarContrato(int.Parse(hfIdContrato.Value));

            if (valorActual < valorAdiccion)
            {
                int vResultado;

                Adiciones vAdiciones = new Adiciones();

                int vIdDetConModContractual = Convert.ToInt32(hfIdDetConsModContractual.Value);
                vAdiciones.IDDetalleConsModContractual = vIdDetConModContractual;
                vAdiciones.FechaAdicion = DateTime.Now;
                vAdiciones.UsuarioCrea = GetSessionUser().NombreUsuario;
                vAdiciones.JustificacionAdicion = txtJustificacionAdicion.Text;
                vAdiciones.ValorAdicion = Math.Abs(valorAdiccion - valorActual);
                vResultado = _vContratoService.InsertarAdiciones(vAdiciones);

                if (vResultado == 1)
                    _toolBar.MostrarMensajeError("La operación se completo satisfactoriamente, verifique por favor.");
                else
                    _toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");

                return vAdiciones.IdAdicion;
            }
            else
                _toolBar.MostrarMensajeError("El valor de la adición es menor al valor actual, verifique por favor.");
        }
        catch (UserInterfaceException ex)
        {
            throw new Exception(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return result;
    }   

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            _toolBar = (General_General_Master_Lupa)this.Master;
            if (_toolBar != null)
            {
                _toolBar.eventoRetornar += btnRetornar_Click;
                _toolBar.eventoGuardar += btnGuardar_Click;

                _toolBar.EstablecerTitulos("Productos", SolutionPage.Add.ToString());
            }
        }
        catch (UserInterfaceException ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            if (_toolBar != null) _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ftCantidadCupos.ValidChars += CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftValorUnitario.ValidChars += CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftTiempo.ValidChars += CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();
            ftValorTotal.ValidChars += CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToString();

            ddlUnidadTiempo.Items.Add(new ListItem("Seleccione", "-1"));
            ddlUnidadTiempo.Items.Add(new ListItem("Dia", "1"));
            ddlUnidadTiempo.Items.Add(new ListItem("Meses", "2"));
            ddlUnidadTiempo.Items.Add(new ListItem("Años", "3"));

            var vVigencia = Convert.ToInt32(Request.QueryString["Vigencia"]);
            var vNumeroCOnsecutivo = Convert.ToInt32(Request.QueryString["Consecutivo"]);
            var idProductoPlanCompras = Convert.ToInt32(Request.QueryString["ProductoPlanCompraContrato"]);
            var idPlanCompras = Convert.ToInt32(Request.QueryString["IdPlanCompras"]);

            var productoBD = _vContratoService.ConsultarPlanComprasProductos(idProductoPlanCompras);
            
            decimal valorActual;

            var vWsContratoPacco = _wsContratosPacco.GetListaDetalleProductoPACCO(vVigencia, vNumeroCOnsecutivo);

            var productoWS = vWsContratoPacco.FirstOrDefault(e => e.codigo_producto == productoBD.CodigoProducto);

            var vPlanComprasProductos = new PlanComprasProductos();
                vPlanComprasProductos.NumeroConsecutivoPlanCompras = productoWS.consecutivo;
                vPlanComprasProductos.CodigoProducto = productoWS.codigo_producto;
                vPlanComprasProductos.NombreProducto = productoWS.nombre_producto;
                vPlanComprasProductos.IdDetalleObjeto = productoWS.iddetalleobjetocontractual.ToString();
                vPlanComprasProductos.TipoProducto = productoWS.tipoProductoView;
                vPlanComprasProductos.CantidadCupos = (productoBD.CantidadCupos == null ? productoWS.cantidad : productoBD.CantidadCupos);
                vPlanComprasProductos.ValorUnitario = (productoBD.ValorUnitario == null ? productoWS.valor_unitario : productoBD.ValorUnitario);

                decimal valorTotal = 0;
                decimal valorUnitario = 0;
                decimal cantidadCupos = 0;
                decimal tiempo = 0;

                TipoProducto = productoWS.tipoProductoView;   

                if (productoWS.tipoProductoView.ToUpper() == "SERVICIO")
                {
                    cantidadCupos = Convert.ToDecimal((productoBD.CantidadCupos == null ? Convert.ToInt32(productoWS.cantidad) : Convert.ToInt32(productoBD.CantidadCupos)));
                    valorUnitario = Convert.ToDecimal((productoBD.ValorUnitario == null ? productoWS.valor_unitario : productoBD.ValorUnitario));
                    tiempo = Convert.ToDecimal((productoBD.Tiempo == null ? Convert.ToDecimal(productoWS.tiempo) : Convert.ToDecimal(productoBD.Tiempo)));

                    valorTotal = cantidadCupos * valorUnitario * tiempo;
                }
                else
                {
                    valorTotal = Convert.ToDecimal(productoBD.CantidadCupos == null ? Convert.ToInt32(productoWS.cantidad) : Convert.ToInt32(productoBD.CantidadCupos)) *
                                 Convert.ToDecimal((productoBD.ValorUnitario == null ? productoWS.valor_unitario : productoBD.ValorUnitario));
                }

                vPlanComprasProductos.ValorTotal = valorTotal;
                vPlanComprasProductos.Tiempo = (productoBD.Tiempo == null ? productoWS.tiempo : productoBD.Tiempo);
                vPlanComprasProductos.UnidadTiempo = (productoBD.IdUnidadTiempo == null ? Convert.ToString(productoWS.tipotiempo) : productoBD.IdUnidadTiempo);
                vPlanComprasProductos.UnidadMedida = productoWS.unidad_medida;

            txtCodigoProducto.Text = vPlanComprasProductos.CodigoProducto;
            txtNombreProducto.Text = vPlanComprasProductos.NombreProducto;
            txtTipoProducto.Text = vPlanComprasProductos.TipoProducto;
            ddlUnidadTiempo.SelectedValue = Convert.ToString(vPlanComprasProductos.UnidadTiempo);
            txtUnidadMedida.Text = vPlanComprasProductos.UnidadMedida;

            hfIdDetalleObjeto.Value = vPlanComprasProductos.IdDetalleObjeto;
            hfCantidadCupos.Value = vPlanComprasProductos.CantidadCupos.ToString();

            var productoActual = _vContratoService.ConsultarPlanComprasProductoActual(idPlanCompras,productoBD.CodigoProducto);

            if (productoWS.tipoProductoView.Equals("SERVICIO", StringComparison.CurrentCultureIgnoreCase))
            {
                ValorActualModificacion = productoActual.CantidadCupos.Value * productoActual.ValorUnitario.Value * productoActual.Tiempo.Value;
                valorActual = productoBD.CantidadCupos.Value * productoBD.ValorUnitario.Value * productoBD.Tiempo.Value;
            }
            else
            {
                ValorActualModificacion = productoActual.CantidadCupos.Value * productoActual.ValorUnitario.Value;
                valorActual = productoBD.CantidadCupos.Value * productoBD.ValorUnitario.Value;
            }

            txtCantidadCupos.Text = productoActual.CantidadCupos.ToString();
            txtValorUnitario.Text = decimal.Parse(productoActual.ValorUnitario.ToString(), NumberStyles.Currency).ToString("$ #,###0.00##;($ #,###0.00##)");
            txtTiempo.Text = productoActual.Tiempo.ToString();
            txtValorTotal.Text = ValorActualModificacion.ToString("$ #,###0.00##;($ #,###0.00##)");
            LblValorTotalModificacionActual.Text = valorActual.ToString("$ #,###0.00##;($ #,###0.00##)");

            hfValorActualProducto.Value = ValorActualModificacion.ToString();
        }
        catch (UserInterfaceException ex)
        {
            _toolBar.MostrarMensajeError(ex.InnerException.Message);
        }
        catch (Exception ex)
        {
            _toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CalcularValorTotal()
    {
        if ((txtCantidadCupos.Text != "") && (txtTiempo.Text != "") && (txtValorUnitario.Text != ""))
        {
            var vCupos = Convert.ToDecimal(txtCantidadCupos.Text);
            var vTiempo = Convert.ToDecimal(txtTiempo.Text);
            var vValorUnitario = Convert.ToDecimal(decimal.Parse(txtValorUnitario.Text, NumberStyles.Currency));
            decimal vValotTotal;

            if (txtTipoProducto.Text.ToUpper() == "SERVICIO")
            {
                vValotTotal = vCupos * vTiempo * vValorUnitario;
            }
            else
            {
                vValotTotal = vCupos * vValorUnitario;

            }

            txtValorTotal.Text = decimal.Parse(Convert.ToString(vValotTotal), NumberStyles.Currency).ToString("$ #,###0.00##;($ #,###0.00##)");
        }

        ClientScript.RegisterStartupScript(Page.GetType(), "rvLoading", "ocultaImagenLoading();");
    }

    /// <summary>
    ///  Verifica los datos que vienen por query string.
    /// </summary>
    private void VerficarQueryStrings()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            hfIdContrato.Value = Request.QueryString["idContrato"];

        if (!string.IsNullOrEmpty(Request.QueryString["idDetConsModContractual"]))
            hfIdDetConsModContractual.Value = Request.QueryString["idDetConsModContractual"];

            lblJusticiación.Visible = true;
            txtJustificacionAdicion.Visible = true;
            CustomValidator.Visible = true;

           var contrato =   _vContratoService.ConsultarContrato(int.Parse(hfIdContrato.Value));
           ValorContrato =  contrato.ValorFinalContrato ?? contrato.ValorInicialContrato ?? 0;
           TipoContratoConvenio = contrato.NombreTipoContrato;

        if (!string.IsNullOrEmpty(Request.QueryString["IdAdicion"]))
            hfIdAdicion.Value = Request.QueryString["IdAdicion"];
    }

    #endregion
}
