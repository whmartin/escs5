using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Text;

public partial class Page_SolicitudesModificacion_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RepartoModificaciones";
    ContratoService vTipoSolicitudService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        var strValue = hfIdTContrato.Value.ToString();
        NavigateTo("List.aspx");
    }

    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        try
        {
            var idConsModContractual = int.Parse( hfIdConsModContrato.Value.ToString());

            var itemsMsjs = vTipoSolicitudService.ValidarCambiodeEstadoaEnviada(idConsModContractual);

            if (itemsMsjs == null || itemsMsjs.Count == 0)
            {
               var result =  vTipoSolicitudService.CambiarEstadoRegistroaEnviada(idConsModContractual);

               if (result > 0)
               {
                   var strValue = hfIdTContrato.Value.ToString();
                   SetSessionParameter("Contrato.IdContrato", strValue);
                   SetSessionParameter("SolModContratual.CambioEstado", "1");
                   NavigateTo("ListDetalle.aspx");
               }
            }
            else
            {
                StringBuilder texto = new StringBuilder();

                foreach (var item in itemsMsjs)
                    texto.AppendLine(item.Tipo + " - " + item.Descripcion);

                toolBar.MostrarMensajeError(texto.ToString().Replace("\n","<br/>"));
            } 
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            int vIdConsModContractual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
            RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");

            hfIdConsModContrato.Value = vIdConsModContractual.ToString();

            SolModContractual vSolContractual = new SolModContractual();
            vSolContractual = vTipoSolicitudService.ConsultarSolitud(vIdConsModContractual);

            hfIdTContrato.Value = vSolContractual.IdContrato.ToString();

            var detalleSolicitud = vTipoSolicitudService.ConsultarSolicitudesDetalle(vIdConsModContractual);

            var historicosSolicitud = vTipoSolicitudService.ConsultarSolicitudesHistorico(vIdConsModContractual);

            if (vSolContractual.Estado == "Devuelta" || vSolContractual.Estado == "Enviada")
            {
                toolBar.MostrarBotonAprobar(true);
                ddlUsuariosAsginados.Enabled = true;
            }
            else
            {
                toolBar.MostrarBotonAprobar(false);
                ddlUsuariosAsginados.Enabled = false;
                if (vSolContractual.Estado == "Aceptada" && vSolContractual.TipoModificacion == "Cambio de Supervisor")
                    MultiViewSupervisores.ActiveViewIndex = -1;
            }

            txtJustificacion.Text = vSolContractual.Justificacion;
            txtNumeroDocumento.Text = vSolContractual.NumeroDoc;
            txtFechaSolicitud.Text = vSolContractual.FechaCrea.ToShortDateString();
            txtIdContrato.Text = vSolContractual.NumeroContrato ?? String.Empty;
            txtNumeroSolicitud.Text = vSolContractual.IDCosModContractual.ToString();
            txtTiposModificaciones.Text = vSolContractual.TipoModificacion;

            lblEstado.Text = vSolContractual.Estado;

            gvDetalleSolicitud.DataSource = detalleSolicitud;
            gvDetalleSolicitud.EmptyDataText = EmptyDataText();
            gvDetalleSolicitud.PageSize = PageSize();
            gvDetalleSolicitud.DataBind();

            gvHistorico.DataSource = historicosSolicitud;
            gvHistorico.EmptyDataText = EmptyDataText();
            gvHistorico.PageSize = PageSize();
            gvHistorico.DataBind();

            foreach (var itemDetalle in detalleSolicitud)
            {
                switch (itemDetalle.TipoModificacion)
                {
                    case "Adición":
                        AccTiposModificacion.Panes[0].Visible = true;
                        gvAdicciones.DataSource = vTipoSolicitudService.ConsultarAdicioness(null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvAdicciones.EmptyDataText = EmptyDataText();
                        gvAdicciones.PageSize = PageSize();
                        gvAdicciones.DataBind();
                        break;
                    case "Prorroga":
                        AccTiposModificacion.Panes[1].Visible = true;
                        gvProrroga.DataSource = vTipoSolicitudService.ConsultarProrrogass(null, null, null, null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvProrroga.EmptyDataText = EmptyDataText();
                        gvProrroga.PageSize = PageSize();
                        gvProrroga.DataBind();
                        break;
                    case "Modificación":
                        AccTiposModificacion.Panes[2].Visible = true;
                        gvModificacionesContractuales.DataSource = vTipoSolicitudService.ConsultarModificacionObligacionXIDDetalleConsModContractual(itemDetalle.IDDetalleConsModContractual);
                        gvModificacionesContractuales.EmptyDataText = EmptyDataText();
                        gvModificacionesContractuales.PageSize = PageSize();
                        gvModificacionesContractuales.DataBind();
                        break;
                    case "Reducción de Valor":
                        AccTiposModificacion.Panes[3].Visible = true;
                        gvReduccionesValor.DataSource = vTipoSolicitudService.ConsultarReduccioness(null, null, itemDetalle.IDDetalleConsModContractual);
                        gvReduccionesValor.EmptyDataText = EmptyDataText();
                        gvReduccionesValor.PageSize = PageSize();
                        gvReduccionesValor.DataBind();
                        break;
                    case "Cesión":
                        AccTiposModificacion.Panes[4].Visible = true;
                        gvCesion.DataSource = vTipoSolicitudService.ConsultarCesioness(null, null, null, itemDetalle.IDDetalleConsModContractual);
                        gvCesion.EmptyDataText = EmptyDataText();
                        gvCesion.PageSize = PageSize();
                        gvCesion.DataBind();
                        break;
                    case "Suspensión":
                        AccTiposModificacion.Panes[5].Visible = true;
                        gvSuspencion.DataSource = vTipoSolicitudService.ConsultarSuspensionesPorDetalleModificacion(itemDetalle.IDDetalleConsModContractual);
                        gvSuspencion.EmptyDataText = EmptyDataText();
                        gvSuspencion.PageSize = PageSize();
                        gvSuspencion.DataBind();
                        break;
                    case "Liquidación de Contrato":
                        AccTiposModificacion.Panes[6].Visible = true;
                        gvLiquidacionContrato.DataSource = vTipoSolicitudService.ConsultarLiquidacionIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvLiquidacionContrato.EmptyDataText = EmptyDataText();
                        gvLiquidacionContrato.PageSize = PageSize();
                        gvLiquidacionContrato.DataBind();
                        break;
                    case "Imposición de Multas":
                        AccTiposModificacion.Panes[7].Visible = true;
                        gvImposicionMultas.DataSource = vTipoSolicitudService.ConsultarImposicionMultaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvImposicionMultas.EmptyDataText = EmptyDataText();
                        gvImposicionMultas.PageSize = PageSize();
                        gvImposicionMultas.DataBind();
                        break;
                    case "Terminación Anticipada":
                        AccTiposModificacion.Panes[8].Visible = true;
                        gvTerminacionAnticipada.DataSource = vTipoSolicitudService.ConsultarTerminacionAnticipadaIdDetalle(itemDetalle.IDDetalleConsModContractual);
                        gvTerminacionAnticipada.EmptyDataText = EmptyDataText();
                        gvTerminacionAnticipada.PageSize = PageSize();
                        gvTerminacionAnticipada.DataBind();
                        break;
                    case "Cambio de Supervisor":
                        AccTiposModificacion.Panes[9].Visible = true;
                        List<SupervisorInterContrato> supervisoresInterventores = vTipoSolicitudService.ObtenerSupervisoresInterventoresContrato(vSolContractual.IdContrato, null);
                        if (supervisoresInterventores != null)
                        {
                            var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                            gvSupervisoresActuales.DataSource = querySupervisores;
                            gvSupervisoresActuales.DataBind();
                        }
                        List<SupervisorInterContrato> supervisoresTemporales = vTipoSolicitudService.ObtenerSupervisoresTemporalesContrato(vSolContractual.IdContrato, null);
                        gvSupervInterv.DataSource = supervisoresTemporales;
                        gvSupervInterv.DataBind();

                    break;
                }
            }

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSolContractual.UsuarioCrea, vSolContractual.FechaCrea, vSolContractual.UsuarioModifica, vSolContractual.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonBuscar(true);
            toolBar.eventoAprobar += new ToolBarDelegate(btnAprobar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
           
            toolBar.EstablecerTitulos("Detalle de la Solicitud Modificación", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    protected void gvAdicciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvAdicciones.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvAdicciones.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Adiciones.IdAdiccion", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../Adiciones/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        //var btn = sender as ImageButton;

        //string comando = btn.CommandName;

        //if (comando == "Add")
        //{

        //}
        //else if (comando == "Select")
        //{

        //}
    }

    protected void gvProrroga_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvProrroga.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvProrroga.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Prorrogas.IdProrroga", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../Prorrogas/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvModificacionesContractuales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvModificacionesContractuales.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvModificacionesContractuales.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ModificacionObligacion.IdModObligacion", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../ModificacionObligacion/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvReduccionesValor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvReduccionesValor.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvReduccionesValor.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Reducciones.IdReduccion", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../ReduccionesValor/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvCesion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvCesion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvCesion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Cesiones.IdCesion", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../Cesiones/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSuspencion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvSuspencion.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvSuspencion.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Suspensiones.IdSuspension", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../Suspenciones/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvLiquidacionContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvLiquidacionContrato.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvLiquidacionContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdLiquidacionContrato", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../LiquidacionContrato/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvImpsicionMultas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvImposicionMultas.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvImposicionMultas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdImposicionMultas", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../ProcesoImposicionMulta/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTerminacionAnticipada_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvTerminacionAnticipada.SelectedRow;
            int rowIndex = row.RowIndex;
            string strValue = gvTerminacionAnticipada.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("Contrato.IdTerminacionAnticipada", strValue);
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../TerminacionAnticipada/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnInfo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            SetSessionParameter("Contrato.IdContrato", hfIdTContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContrato.Value.ToString());
            SetSessionParameter("ConsModContractualGestion.EsReparto", true);
            Response.Redirect("../CambioSupervision/Detail.aspx", false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
