using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_RepartoModificaciones_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RepartoModificaciones";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            //int? vIdContrato = null;
            String vNumeroContrato = null;
            int? vVigenciaFiscal = null;
            int? vRegional = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            bool isNull = true;

            if (txtNumeroContrato.Text != "")
            {
                vNumeroContrato = Convert.ToString(txtNumeroContrato.Text);
                isNull = false;
            }
            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
            {
                vVigenciaFiscal = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
                isNull = false;
            }
            if (ddlRegional.SelectedValue != "-1")
            {
                vRegional = Convert.ToInt32(ddlRegional.SelectedValue);
            }
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
            {
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
                isNull = false;
            }
            if (ddlIDTipoContrato.SelectedValue != "-1" && ddlIDTipoContrato.SelectedValue != "")
            {
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
                isNull = false;
            }

            var items = vContratoService.ConsultarConsModContractualPorSolicitudReparto(vNumeroContrato, vVigenciaFiscal, vRegional, vIDCategoriaContrato, vIDTipoContrato);

            if (isNull)
            {
                var subItems = items.Where(e => e.UsuarioAsignado != "" && e.UsuarioAsignado != null);
                items = subItems != null ? subItems.ToList() : null;
            }

            gvConsModContractual.DataSource = items;            
            gvConsModContractual.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvConsModContractual.PageSize = PageSize();
            gvConsModContractual.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Reparto de Solicitudes de Modificaciones Contractuales", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvConsModContractual.DataKeys[rowIndex].Values[0].ToString();
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", strIDCosModContractual);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvConsModContractual_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvConsModContractual.SelectedRow);
    }
    
    protected void gvConsModContractual_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConsModContractual.PageIndex = e.NewPageIndex;
        Buscar();
    }
  
    private void CargarDatosIniciales()
    {
        try
        {
            
            var regional = this.GetSessionUser().IdRegional;
             
            if (regional.HasValue)
             {
                 if (GetSessionParameter("ConsModContractualGestion.Eliminado").ToString() == "1")
                     toolBar.MostrarMensajeEliminado();
                 RemoveSessionParameter("ConsModContractualGestion.Eliminado");

                 ManejoControles Controles = new ManejoControles();
                 Controles.LlenarNombreRegional(ddlRegional, string.Empty, true);
                 ddlRegional.SelectedValue = regional.Value.ToString();
                 ddlRegional.Enabled = false;
                 LlenarCategoriaContrato();
                 CargarListaVigencia();

                 var items = vContratoService.ConsultarConsModContractualPorSolicitudReparto(null, null, regional, null, null);

                 var subItems = items.Where(e => e.UsuarioAsignado == "" || e.UsuarioAsignado == null);

                 if (subItems != null)
                 {
                     gvConsModContractual.DataSource = subItems.ToList();
                     gvConsModContractual.DataBind();
                 }
             }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void CargarListaVigencia()
    {
        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            // P�gina 1 Descripci�n adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }


    }
}
