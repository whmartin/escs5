<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SolicitudesModificacion_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
        <asp:HiddenField ID="hfIdTContrato" runat="server" />
        <asp:HiddenField ID="hfIdConsModContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Asignado&nbsp;</td>
            <td>
                &nbsp;</td>             
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList ID="ddlUsuariosAsginados" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>             
        </tr>
        <tr class="rowB">
            <td>
                N&uacute;mero de Contrato/Convenio
            </td>
            <td>
                N&uacute;mero Solicitud
            </td>             
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroSolicitud" Enabled="false"></asp:TextBox>
            </td>             
        </tr>
       <tr class="rowB">           
            <td>
                Fecha Solicitud
            </td>
            <td>
                N&uacute;mero de Documento
            </td>            
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaSolicitud" Enabled="false"></asp:TextBox>
            </td>   
             <td>
                <asp:TextBox runat="server" ID="txtNumeroDocumento" Enabled="False"></asp:TextBox>
            </td>                     
        </tr>
        <tr class="rowB">
            <td>
               Tipo de Modificaci&oacute;n
            </td>
            <td>
                Justificación de la modificación contractual</td>            
        </tr>        
        <tr class="rowA">
              <td>
                <asp:TextBox ID="txtTiposModificaciones" runat="server" Width="400px" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox TextMode="MultiLine" ID="txtJustificacion" runat="server" Height="80px" Width="400px" Enabled="False"></asp:TextBox>
            </td>         
        </tr>
        <tr class="rowB">           
              <td>
              Estado
            </td>         
        </tr>
        <tr class="rowA">
            
            <td>
                <asp:Label runat="server" ID="lblEstado" />
            <%--<asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="False">
            </asp:RadioButtonList>--%>
            </td>
        </tr>
        
    </table>

     <table width="90%" align="center">
          <tr class="rowA">
                <td>
                  <Ajax:TabContainer Width="100%" runat="server"  ActiveTabIndex="0">
        <Ajax:TabPanel ID="TabDetalle" runat="server">
            <HeaderTemplate>
                Detalle Modificaciones
            </HeaderTemplate>
            <ContentTemplate>
                    <table width="90%" align="center">
                        <tr class="rowAG">
                            <td>
                                <asp:GridView runat="server" ID="gvDetalleSolicitud" AutoGenerateColumns="False" AllowPaging="True"
                                    GridLines="None" Width="100%" DataKeyNames="IDDetalleConsModContractual" CellPadding="0" Height="16px"
                                    >
                                    <Columns>
                                        <asp:BoundField HeaderText="Tipo de Modificaci&oacute;n" DataField="TipoModificacion" />
                                        <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaString" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
            </ContentTemplate>
        </Ajax:TabPanel>
            <Ajax:TabPanel ID="TabHistorico" runat="server">
                    <HeaderTemplate>
                        Historico Solicitud
                    </HeaderTemplate>
                    <ContentTemplate>
                                            <table width="90%" align="center">
                        <tr class="rowAG">
                            <td>
                                    <asp:GridView runat="server" ID="gvHistorico" AutoGenerateColumns="False" AllowPaging="True"
                                    GridLines="None" Width="100%" DataKeyNames="IdConsModContractualEstado" CellPadding="0" Height="16px"
                                    >
                                    <Columns>
                                        <asp:BoundField HeaderText="Estado" DataField="Estado" />
                                        <asp:BoundField HeaderText="Fecha de Registro" DataField="FechaString" />
                                        <asp:BoundField HeaderText="Justificaci&oacute;n" DataField="Justificacion" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                                </td>
                            </tr>
                                                </table>
                    </ContentTemplate>
        </Ajax:TabPanel>
    </Ajax:TabContainer>
                </td>
            </tr>
        <tr class="rowB">
                 <td>
                  <Ajax:Accordion ID="AccTiposModificacion" HeaderCssClass="accordionHeader"  HeaderSelectedCssClass="accordionHeaderSelected"
                    ContentCssClass="accordionContent" runat="server"
                    Width="100%" Height="100%">
                    <Panes>

                        <Ajax:AccordionPane ID="ApAdiccion" Visible="false" runat="server">
                            <Header>
                                Adici&oacute;n
                            </Header>
                            <Content>
                                        <asp:Panel runat="server" ID="Panel1">
                                            <table width="90%" align="center">
                                                <tr class="rowAG">
                                                    <td>
                                                        <asp:GridView runat="server" ID="gvAdicciones" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" DataKeyNames="IdAdicion" OnSelectedIndexChanged="gvAdicciones_SelectedIndexChanged" CellPadding="0" Height="16px"
                                                            >
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                    <asp:BoundField DataFormatString="{0:c}" HeaderText="Valor de Adicci&oacute;n" DataField="ValorAdicion" />
<%--                                                                    <asp:BoundField HeaderText="Valor Total Contrato/Convenio" DataField="ValorContratoConvenio" />--%>
                                                                   <%-- <asp:BoundField HeaderText="Justificacion Adici&oacute;n" DataField="JustificacionAdicion" />--%>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                </Content>
                        </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="ApProrroga" Visible="false" runat="server">
                            <Header>
                                Prorroga
                            </Header>
                            <Content>
                                    <asp:Panel runat="server" ID="Panel2">
                                            <table width="90%" align="center">
                                                <tr class="rowAG">
                                                    <td>
                                                        <asp:GridView runat="server" ID="gvProrroga" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" DataKeyNames="IdProrroga" CellPadding="0" OnSelectedIndexChanged="gvProrroga_SelectedIndexChanged" Height="16px"
                                                            >
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha Inicial Prorroga" DataField="FechaInicioView" />
                                                                <asp:BoundField HeaderText="Fecha Final Prorroga" DataField="FechaFinView" />
                                                                <asp:BoundField HeaderText="Plazo Total Con D&iacute;as" DataField="Dias" />
                                                                <asp:BoundField HeaderText="Plazo Total Con Meses" DataField="Meses" />
                                                                <asp:BoundField HeaderText="Plazo Total Con A&ntilde;os" DataField="Anios" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                </Content>
                        </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="ApModificacionesContractuales" Visible="false" runat="server">
                            <Header>
                                Modificaciones Obligaciones / Clausulas
                            </Header>
                            <Content>
                                                                        <asp:Panel runat="server" ID="Panel3">
                                            <table width="90%" align="center">
                                                <tr class="rowAG">
                                                    <td>
                                                        <asp:GridView runat="server" ID="gvModificacionesContractuales" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" DataKeyNames="IdModObligacion" CellPadding="0" OnSelectedIndexChanged="gvModificacionesContractuales_SelectedIndexChanged" Height="16px"
                                                            >
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Modificaci&oacute;n" DataField="FechaCrea" />
                                                                <asp:BoundField HeaderText="Clausula y/o Obligaci&oacute;n a Modificar" DataField="ClausulaContrato" />
                                                                <asp:BoundField HeaderText="Descripci&oacute;n de la Modificaci&oacute;n" DataField="DescripcionModificacion" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                </Content>
                        </Ajax:AccordionPane>
                                                <Ajax:AccordionPane ID="ApReduccionesValor" Visible="false" runat="server">
                            <Header>
                                Reducciones de Valor
                            </Header>
                            <Content>
                                                                        <asp:Panel runat="server" ID="Panel4">
                                            <table width="90%" align="center">
                                                <tr class="rowAG">
                                                    <td>
                                                        <asp:GridView runat="server" ID="gvReduccionesValor" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" DataKeyNames="IdReduccion" OnSelectedIndexChanged="gvReduccionesValor_SelectedIndexChanged" CellPadding="0" Height="16px" >
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Valor Reducci&oacute;n" DataField="ValorReduccion" />
                                                                <asp:BoundField HeaderText="Fecha Reducci&oacute;n" DataField="FechaReduccion" />

<%--                                                                <asp:BoundField HeaderText="Valor Inicial Contrato/Convenio" DataField="ValorReduccion" />
                                                                <asp:BoundField HeaderText="Valor Contrato/Convenio con reducci&oacute;n" DataField="Fecha" />--%>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                </Content>
                        </Ajax:AccordionPane>
                           <Ajax:AccordionPane ID="ApCesion" Visible="false" runat="server">
                            <Header>
                                Cesi&oacute;n
                            </Header>
                            <Content>
                                                                        <asp:Panel runat="server" ID="Panel5">
                                            <table width="90%" align="center">
                                                <tr class="rowAG">
                                                    <td>
                                                        <asp:GridView runat="server" ID="gvCesion" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" DataKeyNames="IdCesion" OnSelectedIndexChanged="gvCesion_SelectedIndexChanged" CellPadding="0" Height="16px"
                                                            >
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="Fecha" DataField="FechaCesion" />
                                                                    <%--<asp:BoundField HeaderText="Justificaci&#243;n" DataField="Justificacion" />--%>
<%--                                                                <asp:BoundField HeaderText="Valor del Contrato/Convenio" DataField="TipoModificacion" />
                                                                <asp:BoundField HeaderText="Tipo de Documento cesionario" DataField="Fecha" />
                                                                <asp:BoundField HeaderText="N&uacute;mero de Documento cesionario" DataField="Fecha" />
                                                                <asp:BoundField HeaderText="Cesionario" DataField="Fecha" />--%>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApSuspencion" Visible="false" runat="server">
                            <Header>
                                Suspenci&oacute;n
                            </Header>
                            <Content>
                                   <asp:Panel runat="server" ID="Panel6">
                                            <table width="90%" align="center">
                                                <tr class="rowAG">
                                                    <td>
                                                        <asp:GridView runat="server" ID="gvSuspencion" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvSuspencion_SelectedIndexChanged" DataKeyNames="IdSuspension" CellPadding="0" Height="16px"
                                                            >
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Inicio de la suspensi&oacute;n" DataField="FechaInicioView" />
                                                                 <asp:BoundField HeaderText="Fecha de Fin de la suspensi&oacute;n" DataField="FechaFinView" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApLiquidacionContrato" Visible="false" runat="server">
                            <Header>
                                Liquidac&oacute;n de contrato
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel7">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                <asp:GridView runat="server" ID="gvLiquidacionContrato" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvLiquidacionContrato_SelectedIndexChanged" DataKeyNames="IdLiquidacionContrato" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Radicaci&oacute;n" DataField="FechaRadicacionView" />
                                                                 <asp:BoundField HeaderText="Dependencia" DataField="DependenciaSueprvisor" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApImposicionMultas" Visible="false" runat="server">
                            <Header>
                                Imposici&oacute;n de multas
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel8">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                 <asp:GridView runat="server" ID="gvImposicionMultas" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvImpsicionMultas_SelectedIndexChanged" DataKeyNames="IdImposicionMulta" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Raz&oacute;n" DataField="Razones" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApTerminacionAnticipada" Visible="false" runat="server">
                            <Header>
                                Terminaci&oacute;n anticipada
                            </Header>
                            <Content>
                               <asp:Panel runat="server" ID="Panel9">
                                    <table width="90%" align="center">
                                       <tr class="rowAG">
                                         <td>
                                                  <asp:GridView runat="server" ID="gvTerminacionAnticipada" AutoGenerateColumns="False" AllowPaging="True"
                                                            GridLines="None" Width="100%" OnSelectedIndexChanged="gvTerminacionAnticipada_SelectedIndexChanged" DataKeyNames="IdTerminacionAnticipada" CellPadding="0" Height="16px">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="Fecha de Radicaci&oacute;n" DataField="FechaRadicacionView" />
                                                                <asp:BoundField HeaderText="No de Radicaci&oacute;n" DataField="NumeroRadicacion" />
                                                                <asp:BoundField HeaderText="Fecha de Terminaci&oacute;n Anticipada" DataField="FechaTerminacionAnticipadaView" />
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="rowBG" />
                                                            <EmptyDataRowStyle CssClass="headerForm" />
                                                            <HeaderStyle CssClass="headerForm" />
                                                            <RowStyle CssClass="rowAG" />
                                                        </asp:GridView>
                                        </td>
                                       </tr>
                                    </table>
                               </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        <Ajax:AccordionPane ID="ApCambioSupervisor" Visible="false" runat="server">
                            <Header>
                                Cambio de Supervisor de Contrato
                            </Header>
                            <Content>
                                <asp:Panel runat="server" ID="Panel10">
                                    <table width="90%" align="center">
                                     <tr class="rowAG">
                                         <td>
                                          Detalle:
                                         </td>
                                         <td>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" OnClick="btnInfo_Click" ImageUrl="~/Image/btn/info.jpg" Height="16px" Width="16px" ToolTip="Detalle" />      
                                         </td>
                                     </tr>
                                      <table width="90%" align="center">
                                            <tr class="rowBG">
            <td colspan="4">
            <table align="center" width="90%">
                <tr class="rowB">
                    <td class="Cell">Supervisores relacionados Actuales </td>
                </tr>
                  <tr class="rowA">
                        <td class="Cell">
                            <div id="Div1" runat="server">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px" >
                                    <Columns>
                                                                                <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
            </table>
            </td>
        </tr>
        <tr class="rowAB">
            <td colspan="4">
                                <asp:MultiView ID="MultiViewSupervisores" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewSupervisores" runat="server">
             <table align="center" width="90%">
                    <tr class="rowB">
                        <td class="Cell">Supervisores relacionados Modificaci&oacute;n</td>
                        <td class="Cell">
                            &nbsp;</td>
                    </tr>
                    <tr class="rowA">
                        <td class="Cell" colspan="2">
                            <div id="dvSupervInterv" runat="server">
                                <asp:GridView ID="gvSupervInterv" runat="server" Width="100%" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px">
                                    <Columns>
                                                                                <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                        </asp:View>
                                    </asp:MultiView>
            </td>
        </tr> 
                                    </table>
                                </asp:Panel>
                            </Content>
                        </Ajax:AccordionPane>
                        </Panes>
                    </Ajax:Accordion>
                </td>
            </tr>
      </table>


</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    </asp:Content>


