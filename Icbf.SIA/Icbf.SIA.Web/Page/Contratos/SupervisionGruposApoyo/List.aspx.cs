using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

public partial class Page_SupervisionGruposApoyo_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionGruposApoyo";
    SupervisionService vSupervisionService = new SupervisionService();
    SIAService vRuboService = new SIAService();
    List<Regional> lstRegionales;
    List<SupervisionDireccion> lstDireccion;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdDireccion = null;
            int? vIdRegional = null;
            String vNombre = null;
            int? vEstado = null;
            if (ddlIdDireccion.SelectedValue!= "-1")
            {
                vIdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            }
            if (ddlIdRegional.SelectedValue!= "-1")
            {
                vIdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            }
            if (txtNombre.Text!= "")
            {
                vNombre = Convert.ToString(txtNombre.Text);
            }
            if (rblEstado.SelectedValue!= "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }

            Usuario usuario = new Usuario();
            usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);
            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
            lstRegionales = vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null);
            lstDireccion = vSupervisionService.ConsultarSupervisionDireccions(null, "", "", 1);
            gvSupervisionGruposApoyo.DataSource = vSupervisionService.ConsultarSupervisionGruposApoyos( vIdDireccion, vIdRegional, vNombre, vEstado);
            gvSupervisionGruposApoyo.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionGruposApoyo.PageSize = PageSize();
            gvSupervisionGruposApoyo.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Grupos de Apoyo a la Supervisi&#243;n", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionGruposApoyo.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionGruposApoyo.IdGrupo", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionGruposApoyo_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionGruposApoyo.SelectedRow);
    }
    protected void gvSupervisionGruposApoyo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionGruposApoyo.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            Usuario usuario = new Usuario();
            usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);

            if (GetSessionParameter("SupervisionGruposApoyo.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionGruposApoyo.Eliminado");
            ///*Coloque aqui el codigo de llenar el combo.*/
         

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            lstRegionales = vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null);
            ManejoControlesSupervision.LlenarComboLista(ddlIdRegional, lstRegionales, "IdRegional", "NombreRegional");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void gvSupervisionGruposApoyo_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //Se busca si la pregunta ya fue respondida
            var vRegistrosRespuestas = from resp in lstRegionales
                                       where resp.IdRegional ==  Convert.ToInt32(e.Row.Cells[2].Text)
                                       select resp;
            e.Row.Cells[2].Text = vRegistrosRespuestas.ToList()[0].NombreRegional;

            //Se busca la Direccion
            var vRegistrosDireccion = from resp in lstDireccion
                                      where resp.IdDireccionesICBF == Convert.ToInt16(e.Row.Cells[1].Text)
                                      select resp;
            e.Row.Cells[1].Text = vRegistrosDireccion.ToList()[0].NombreDireccion;

            switch (e.Row.Cells[4].Text)
            {
                case "1":
                    e.Row.Cells[4].Text = "Activo";
                    break;
                default:
                    e.Row.Cells[4].Text = "Desactivado";
                    break;
            }
                    
        }

    }

 
}
