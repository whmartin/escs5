using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

public partial class Page_SupervisionGruposApoyo_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionGruposApoyo";
    SupervisionService vSupervisionService = new SupervisionService();
    SIAService vRuboService = new SIAService();
    System.Drawing.Color vColorDesactivado = System.Drawing.Color.FromArgb(235, 235, 228);

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionGruposApoyo.IdGrupo", hfIdGrupo.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {

            ddlIdDireccion.BackColor = vColorDesactivado;
            ddlIdRegional.BackColor = vColorDesactivado;

            int vIdGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyo.IdGrupo"));
            RemoveSessionParameter("SupervisionGruposApoyo.IdGrupo");

            if (GetSessionParameter("SupervisionGruposApoyo.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionGruposApoyo.Guardado");


            SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();
            vSupervisionGruposApoyo = vSupervisionService.ConsultarSupervisionGruposApoyo(vIdGrupo);
            hfIdGrupo.Value = vSupervisionGruposApoyo.IdGrupo.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionGruposApoyo.IdDireccion.ToString();
            ddlIdRegional.SelectedValue = vSupervisionGruposApoyo.IdRegional.ToString();
            txtNombre.Text = vSupervisionGruposApoyo.Nombre;
            rblEstado.SelectedValue = vSupervisionGruposApoyo.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdGrupo.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionGruposApoyo.UsuarioCrea, vSupervisionGruposApoyo.FechaCrea, vSupervisionGruposApoyo.UsuarioModifica, vSupervisionGruposApoyo.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdGrupo = Convert.ToInt32(hfIdGrupo.Value);

            SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();
            vSupervisionGruposApoyo = vSupervisionService.ConsultarSupervisionGruposApoyo(vIdGrupo);
            InformacionAudioria(vSupervisionGruposApoyo, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionGruposApoyo(vSupervisionGruposApoyo);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionGruposApoyo.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoCambiarOpcion += new ToolBarDelegate(ddlExtends_SelectedIndexChanged);

            toolBar.EstablecerTitulos("Grupos de Apoyo a la Supervisi&#243;n", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            toolBar.AgregarOpcionAdicional(new ListItem("Ir a", "0"));
            toolBar.AgregarOpcionAdicional(new ListItem("Integrantes", "1"));
            toolBar.AgregarOpcionAdicional(new ListItem("Asociar Contratos", "2"));
            Usuario usuario = new Usuario();
            usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision.LlenarComboLista(ddlIdRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlExtends_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<string> GruposSupervision = new List<string>();
        GruposSupervision.Add(txtNombre.Text);
        GruposSupervision.Add(hfIdGrupo.Value);
        GruposSupervision.Add(ddlIdDireccion.SelectedValue);
        Session.Add("GruposSupervision", GruposSupervision);
        switch (((DropDownList)sender).SelectedValue)
        {
            case "1":
                {

                    NavigateTo("~/Page/Contratos/SupervisionIntegrantes/List.aspx", true);
                    break;
                }
            case "2":
                {
                    NavigateTo("~/Page/Contratos/SupervisionContrato/List.aspx", true);
                    break;
                }
            default:
                break;
        }
    }

}
