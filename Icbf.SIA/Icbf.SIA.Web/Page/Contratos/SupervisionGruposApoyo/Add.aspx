<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SupervisionGruposApoyo_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script src="../../../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/SupervisionContratos-ConfirmaGuardar.js" type="text/javascript"></script>
    <asp:HiddenField ID="hfIdGrupo" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td style="width:50%">
                Dirección *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdDireccion" ControlToValidate="ddlIdDireccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdDireccion" ControlToValidate="ddlIdDireccion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
            <td>
                Regional *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdRegional" ControlToValidate="ddlIdRegional"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccion" Width="80%">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdRegional" Width="80%">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Nombre del Grupo *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombre" ControlToValidate="txtNombre"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre"  Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <div id="dvBloqueo" style="position: absolute; z-index: 1; width: 100%; height: 100%;
        background-color: #000000; top: 0px; left: 0px; display: none; opacity: 0.25;
        filter: alpha(opacity=25);">
    </div>
    <div id="dvConfirmacion" class="modalDialog">
        <table style="text-align: center; padding: 9px">
            <tr>
                <td colspan="2">
                    ¿Está seguro de guardar el Grupo de Apoyo a la Supervisión? Recuerde que después
                    de guardado no puede ser modificada toda la información
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnAceptar" type="button" value="Aceptar" onclick="postback=true; $('#btnGuardar')[0].click()" />
                </td>
                <td>
                    <input id="btnCancelar" type="button" value="Cancelar" onclick="ocultarDiv();" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
