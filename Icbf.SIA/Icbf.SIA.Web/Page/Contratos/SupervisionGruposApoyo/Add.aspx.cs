using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

public partial class Page_SupervisionGruposApoyo_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionGruposApoyo";
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();

            vSupervisionGruposApoyo.IdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            vSupervisionGruposApoyo.IdRegional = Convert.ToInt32(ddlIdRegional.SelectedValue);
            vSupervisionGruposApoyo.Nombre = Convert.ToString(txtNombre.Text);
            vSupervisionGruposApoyo.Estado = Convert.ToInt32(rblEstado.SelectedValue);
            int vResultExis = vSupervisionService.ConsultarSupervisionGruposApoyo(vSupervisionGruposApoyo.Nombre);

            if (Request.QueryString["oP"] == "E")
            {
                if (vResultExis == 0)
                {
                    vSupervisionGruposApoyo.IdGrupo = Convert.ToInt32(hfIdGrupo.Value);
                    vSupervisionGruposApoyo.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionGruposApoyo, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.ModificarSupervisionGruposApoyo(vSupervisionGruposApoyo);
                }
                else
                {
                    toolBar.MostrarMensajeError("El Grupo de Apoyo a la Supervisión ingresado ya existe");
                    vResultado = 3;
                }
            }
            else
            {
                if (vResultExis == 0)
                {
                    vSupervisionGruposApoyo.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionGruposApoyo, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.InsertarSupervisionGruposApoyo(vSupervisionGruposApoyo);
                }
                else
                {
                    toolBar.MostrarMensajeError("El Grupo de Apoyo a la Supervisión ingresado ya existe");
                    vResultado = 3;
                }
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SupervisionGruposApoyo.IdGrupo", vSupervisionGruposApoyo.IdGrupo);
                SetSessionParameter("SupervisionGruposApoyo.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado !=3)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Grupos de Apoyo a la Supervisi&#243;n", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdGrupo = Convert.ToInt32(GetSessionParameter("SupervisionGruposApoyo.IdGrupo"));
            RemoveSessionParameter("SupervisionGruposApoyo.IdGrupo");

            SupervisionGruposApoyo vSupervisionGruposApoyo = new SupervisionGruposApoyo();
            vSupervisionGruposApoyo = vSupervisionService.ConsultarSupervisionGruposApoyo(vIdGrupo);
            hfIdGrupo.Value = vSupervisionGruposApoyo.IdGrupo.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionGruposApoyo.IdDireccion.ToString();
            ddlIdRegional.SelectedValue = vSupervisionGruposApoyo.IdRegional.ToString();
            txtNombre.Text = vSupervisionGruposApoyo.Nombre;
            rblEstado.SelectedValue = vSupervisionGruposApoyo.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionGruposApoyo.UsuarioCrea, vSupervisionGruposApoyo.FechaCrea, vSupervisionGruposApoyo.UsuarioModifica, vSupervisionGruposApoyo.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            Usuario usuario = new Usuario();
            usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

            Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);

            ManejoControlesSupervision.LlenarComboLista(ddlIdRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
