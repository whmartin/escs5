﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Contratos_SolicitudModPlanCompras_DetalleAdd : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/SolicitudModPlanCompras";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //vSolutionPage = SolutionPage.Detail;
        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {
            //CargarDatosIniciales();
            CargarDatos();
        }
        //}
    }


    //protected void btnRetornar_Click(object sender, EventArgs e)
    //{
    //    NavigateTo(SolutionPage.Add);
    //}
    /// <summary>
    /// Método de carga de datos del registro 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdSolicitudModPlanCompra = Convert.ToInt32(GetSessionParameter("SolicitudModPlanCompras.IdSolicitudModPlanCompra"));
            RemoveSessionParameter("SolicitudModPlanCompras.IdSolicitudModPlanCompra");

            if (GetSessionParameter("SolicitudModPlanCompras.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SolicitudModPlanCompras.Guardado");


            SolicitudModPlanCompras vSolicitudModPlanCompras = new SolicitudModPlanCompras();
            vSolicitudModPlanCompras = vContratoService.ConsultarSolicitudModPlanCompras(vIdSolicitudModPlanCompra);
            hfIdSolicitudModPlanCompra.Value = vSolicitudModPlanCompras.IdSolicitudModPlanCompra.ToString();
            //txtIdContrato.Text = vSolicitudModPlanCompras.IdContrato.ToString();
            //txtNumeroContrato.Text = vSolicitudModPlanCompras.NumeroContrato.ToString();
            //txtNumeroConsecPlanCompras.Text = vSolicitudModPlanCompras.NumeroConsecPlanDeCompras.ToString();
            //txtUsuarioSolicita.Text = vSolicitudModPlanCompras.UsuarioSolicitud;
            //txtVigencia.Text = vSolicitudModPlanCompras.Vigencia;
            //txtJustificacion.Text = vSolicitudModPlanCompras.Justificacion;
            txtNumeroSolicitud.Text = vSolicitudModPlanCompras.NumeroSolicitud.ToString();
            txtFechaHoraSolicitud.Text = vSolicitudModPlanCompras.FechaSolicitud.ToString("dd/MM/yyyy HH:MM:ss");
            txtEstado.Text = vSolicitudModPlanCompras.EstadoSolicitud;

            ObtenerAuditoria(PageName, hfIdSolicitudModPlanCompra.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSolicitudModPlanCompras.UsuarioCrea, vSolicitudModPlanCompras.FechaCrea, vSolicitudModPlanCompras.UsuarioModifica, vSolicitudModPlanCompras.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para establecer título e inicializar delegados de botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;

            //toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.EstablecerTitulos("Solicitud modificación Plan de compras");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}