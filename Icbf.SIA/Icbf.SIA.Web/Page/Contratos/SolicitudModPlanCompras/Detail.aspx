<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SolicitudModPlanCompras_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdSolicitudModPlanCompra" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id del Contrato
            </td>
            <td>
                Número Contrato / Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato" Width="85%"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" Width="85%"  Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowB">
            <td>
                Número consecutivo Plan de compras
            </td>
            <td>
               Usuario que solicita la modificación  
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroConsecPlanCompras" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtUsuarioSolicita" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowB">
            <td>
               Vigencia 
            </td>
            <td>
                Justificación Solicitud 
            </td>
        </tr>
        <tr class="rowA">
            <td>
              <asp:TextBox runat="server" ID="txtVigencia" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtJustificacion" Rows="5" MaxLength="112" Width="85%" TextMode="MultiLine"  Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowB">
            <td>
                Número solicitud
            </td>
            <td>
                 Fecha y hora de la solicitud
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroSolicitud" Width="85%"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaHoraSolicitud" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowB">
            <td>
                Estado
            </td>
            <td>
                 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEstado" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                
            </td>
        </tr>  
        <%--<tr class="rowB">
            <td>
                Justificacion
            </td>
            <td>
                Número Solicitud
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtJustificacion"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroSolicitud"  Enabled="false"></asp:TextBox>
            </td>
        </tr>   
        <tr class="rowB">
            <td>
                Fecha y hora de la solicitud
            </td>
            <td>
                Id EstadoSolicitud
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtFechaSolicitud"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Id UsuarioSolicitud
            </td>
            <td>
                Plan de compras
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdUsuarioSolicitud"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtIdPlanDeCompras"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Vigencia
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:DropDownList runat="server" ID="ddlIdVigencia"  Enabled="false"></asp:DropDownList>
            </td>
        </tr>--%>
    </table>
</asp:Content>
