using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de consulta a través de filtros para la entidad SolicitudModPlanCompras
/// </summary>
public partial class Page_SolicitudModPlanCompras_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudModPlanCompras";
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //if (GetState(Page.Master, PageName))
                //{
                //   Buscar();
                //}
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        SaveState(this.Master, PageName);
        Buscar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {

        string returnValues =
            "<script language='javascript'> " +
            "window.history.back()" +
            "</script>";

        ClientScript.RegisterClientScriptBlock(Page.GetType(), "rv", returnValues);
    }

    /// <summary>
    /// Método que realiza la búsqueda filtrada con múltiples criterios 
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvSolicitudModPlanCompras, GridViewSortExpression, true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que establece el título del módulo e inicializa los delegados de los botones 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            //toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSolicitudModPlanCompras.PageSize = PageSize();
            gvSolicitudModPlanCompras.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Solicitud Modificación Plan de Compras", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSolicitudModPlanCompras.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SolicitudModPlanCompras.IdSolicitudModPlanCompra", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSolicitudModPlanCompras_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSolicitudModPlanCompras.SelectedRow);
    }
    protected void gvSolicitudModPlanCompras_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSolicitudModPlanCompras.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }
    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void gvSolicitudModPlanCompras_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {

            int? vIdContrato = null;
            string vVigencia = null;
            int? vNumeroSolicitud = null;
            int? vNumeroConsecPlanCompras = null;
            if (txtIdContrato.Text != "")
            {
                vIdContrato = Convert.ToInt32(txtIdContrato.Text);
            }
            //if (hfIdPlanDeCompras.Value != "")
            //{
            //    vIdNumConsecPlanCompras = Convert.ToInt32(hfIdPlanDeCompras.Value);
            //}
            if (txtNumConsecPlanCompras.Text != "")
            {
                vNumeroConsecPlanCompras = Convert.ToInt32(txtNumConsecPlanCompras.Text);
            }
            if (txtNumeroSolicitud.Text != "")
            {
                vNumeroSolicitud = Convert.ToInt32(txtNumeroSolicitud.Text);
            }

            if (ddlIdVigencia.SelectedValue != "-1")
            {
                vVigencia = ddlIdVigencia.SelectedValue;
            }

            var myGridResults = vContratoService.ConsultarSolicitudModPlanComprass(vNumeroConsecPlanCompras, vIdContrato, vNumeroSolicitud, vVigencia);
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(SolicitudModPlanCompras), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<SolicitudModPlanCompras, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("Contrato.ContratosAPP") != null && GetSessionParameter("Contrato.ContratosAPP") != "")
            {
                int vIdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
                //RemoveSessionParameter("Contrato.ContratosAPP"); // Se comenta ya que hace que se pierda la sesión en el flujo de llamados desde las lupas de plan de compras CU_40 Contratos

                List<Contrato> vlistContrato = vContratoService.ConsultarContratosLupa(vIdContrato, null);
                if (vlistContrato.Count() > 0)
                {
                    txtIdContrato.Text = vIdContrato.ToString();

                }
            }
            CargarIdVigencia();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }



    protected void btnLimpiarNumConsecPlanCompras_Click(object sender, EventArgs e)
    {
        txtNumConsecPlanCompras.Text = string.Empty;
        //hfIdPlanDeCompras.Value = string.Empty;
    }
    protected void btnLimpiarIdContrato_Click(object sender, EventArgs e)
    {
        txtIdContrato.Text = string.Empty;
    }

    /// <summary>
    /// Método para cargar los datos de IdVigencia
    /// </summary>
    private void CargarIdVigencia()
    {
        var vResult = vSIAService.ConsultarVigencias(true);
        ManejoControlesContratos.LlenarComboLista(ddlIdVigencia, vResult, "AcnoVigencia", "AcnoVigencia");
    }
}
