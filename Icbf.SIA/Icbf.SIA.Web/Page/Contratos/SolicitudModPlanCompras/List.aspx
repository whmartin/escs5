<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SolicitudModPlanCompras_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="Cell">
                    Número consecutivo Plan de compras
                </td>
                <td class="Cell">
                    Vigencia
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumConsecPlanCompras" MaxLength="20" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:DropDownList runat="server" ID="ddlIdVigencia">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="Cell">
                    Id Contrato
                </td>
                <td class="Cell">
                    Número Solicitud
                </td>
            </tr>
            <tr class="rowA">
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="25" Width="80%"></asp:TextBox>
                </td>
                <td class="Cell">
                    <asp:TextBox runat="server" ID="txtNumeroSolicitud" MaxLength="10" Width="80%"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSolicitudModPlanCompras" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdSolicitudModPlanCompra"
                        CellPadding="0" Height="16px" OnSorting="gvSolicitudModPlanCompras_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvSolicitudModPlanCompras_PageIndexChanging" OnSelectedIndexChanged="gvSolicitudModPlanCompras_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Número Solicitud" DataField="IdSolicitudModPlanCompra"
                                SortExpression="IdSolicitudModPlanCompra" />
                            <asp:BoundField HeaderText="Fecha y hora solicitud" DataField="FechaSolicitud" SortExpression="FechaSolicitud"
                                DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" />
                            <asp:BoundField HeaderText="Id del Contrato" DataField="IdContrato" SortExpression="IdContrato" />
                            <asp:BoundField HeaderText="Número Consecutivo Plan de compras" DataField="NumeroConsecPlanDeCompras"
                                SortExpression="NumeroConsecPlanDeCompras" />
                            <asp:BoundField HeaderText="Usuario que solicita la modificación" DataField="UsuarioSolicitud"
                                SortExpression="UsuarioSolicitud" />
                            <asp:BoundField HeaderText="Vigencia" DataField="Vigencia" SortExpression="Vigencia" />
                            <asp:TemplateField HeaderText="Justificación de la solicitud" SortExpression="Justificacion">
                                <ItemTemplate>
                                    <%# Convert.ToString(Eval("Justificacion")).Length > 100 ? Convert.ToString(Eval("Justificacion")).Substring(0, 100) : Convert.ToString(Eval("Justificacion"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" DataField="EstadoSolicitud" SortExpression="EstadoSolicitud" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
</asp:Content>
