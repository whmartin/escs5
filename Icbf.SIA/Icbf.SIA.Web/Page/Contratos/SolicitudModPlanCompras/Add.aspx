<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/Lupa.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_SolicitudModPlanCompras_Add" %>

<%@ Register Src="../../../General/General/Control/fechaJS.ascx" TagName="fechaJS"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }
    </script>
    <asp:HiddenField ID="hfIdSolicitudModPlanCompra" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Id del Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtIdContrato"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Número Contrato / Convenio
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="25"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" />
                <%--<img id="imgh" alt="help" title="Justificaci&#243;n de la solicitud" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghJustificacion')" onmouseout="helpOut('imghJustificacion')"/>--%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContrato" MaxLength="10"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Numbers" ValidChars="" />
                <%--<img id="img2" alt="help" title="N&#250;mero de la solicitud" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghNumeroSolicitud')" onmouseout="helpOut('imghNumeroSolicitud')"/>--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número Consecutivo Plan de Compras *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroConsecutivoPlan" ControlToValidate="txtNumeroConsecutivoPlan"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Usuario que solicita la Modificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvusuarioSolicita" ControlToValidate="txtUsuarioSolicita"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroConsecutivoPlan" MaxLength="25" Width="85%"></asp:TextBox>
                <asp:HiddenField runat="server" ID="hfIdPlanDeCompras" />
                <Ajax:FilteredTextBoxExtender ID="fteNumeroConsecutivo" runat="server" TargetControlID="txtNumeroConsecutivoPlan"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" />
                <%--<img id="imgh" alt="help" title="Justificaci&#243;n de la solicitud" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghJustificacion')" onmouseout="helpOut('imghJustificacion')"/>--%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtUsuarioSolicita" MaxLength="200" Width="85%"></asp:TextBox>
                <asp:HiddenField runat="server" ID="hfIdUsuarioSolicita" />
                <Ajax:FilteredTextBoxExtender ID="fteUsuarioSolicita" runat="server" TargetControlID="txtUsuarioSolicita"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <%--<img id="img2" alt="help" title="N&#250;mero de la solicitud" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghNumeroSolicitud')" onmouseout="helpOut('imghNumeroSolicitud')"/>--%>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Vigencia *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdVigencia" ControlToValidate="TxtVigenciaPlanCompra"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <%--                <asp:DropDownList runat="server" ID="ddlIdVigencia"></asp:DropDownList>--%>
                <asp:TextBox runat="server" ID="TxtVigenciaPlanCompra"></asp:TextBox>
                <%--<img id="imghIdVigencia" alt="help" title="Vigencia" class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>"
                    onmouseover="helpOver('imghIdVigencia')" onmouseout="helpOut('imghIdVigencia')" />--%>
            </td>
            <td>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Justificación de la Solicitud *
                <asp:RequiredFieldValidator runat="server" ID="rfvJustificacion" ControlToValidate="txtJustificacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtJustificacion" ToolTip="Registre de forma clara lo que requiere modificar del consecutivo de Plan de Compras"
                    Rows="3" CssClass="TextBoxGrande" MaxLength="100" TextMode="MultiLine" onKeyDown="limitText(this,100);" Width="90%"
                    onKeyUp="limitText(this,112);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftJustificacion" runat="server" TargetControlID="txtJustificacion"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;" />
                <%--<img id="imghJustificacion" alt="Registre de forma clara lo que requiere modificar del consecutivo de Plan de Compras"
                    title="Registre de forma clara lo que requiere modificar del consecutivo de Plan de Compras"
                    class="help" src="<%=ResolveClientUrl("~/Image/btn/help_g.png")%>" onmouseover="helpOver('imghJustificacion')"
                    onmouseout="helpOut('imghJustificacion')" />--%>
            </td>
        </tr>
        <tr >
            <td colspan="2">
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número Solicitud
            </td>
            <td>
                Fecha y hora Solicitud
                <%--<asp:RequiredFieldValidator runat="server" ID="rfvHoraSolicitud" ControlToValidate="ddlHoraSolicitud"
                 SetFocusOnError="true" InitialValue="-1" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </td>
        </tr>
        <tr  class="rowA">
            <td>
                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaSolicitud" Width="200px"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtEstadopc" Text="" Enabled="False"></asp:TextBox>
                <asp:DropDownList runat="server" ID="ddlIdEstadoSolicitud" Visible="false">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
