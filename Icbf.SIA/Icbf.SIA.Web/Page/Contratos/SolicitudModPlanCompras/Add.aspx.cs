using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using Icbf.SIA.Entity;

/// <summary>
/// Página de registro y edición para la entidad SolicitudModPlanCompras
/// </summary>
public partial class Page_SolicitudModPlanCompras_Add : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();
    WsContratosPacco.WSContratosPACCOSoapClient vWsContratosPacco = new WsContratosPacco.WSContratosPACCOSoapClient();
    SIAService vSIAService = new SIAService();
    string PageName = "Contratos/SolicitudModPlanCompras";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //if (Request.QueryString["oP"] == "E")
                CargarRegistro();
            }
        }
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad SolicitudModPlanCompras
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado = -1;
            bool ExitoPACCO = false;
            SolicitudModPlanCompras vSolicitudModPlanCompras = new SolicitudModPlanCompras();



            //DateTime vFechaSolicitud = Convert.ToDateTime(txtFechaSolicitud.Date);
            //vFechaSolicitud = vFechaSolicitud.AddHours(Convert.ToInt32(ddlHoraSolicitud.SelectedValue));
            
            vSolicitudModPlanCompras.IdContrato = Convert.ToInt32(txtIdContrato.Text);
            vSolicitudModPlanCompras.NumeroConsecPlanDeCompras = Convert.ToInt32(txtNumeroConsecutivoPlan.Text);
            vSolicitudModPlanCompras.IdPlanDeCompras = Convert.ToInt32(hfIdPlanDeCompras.Value);
            vSolicitudModPlanCompras.IdUsuarioSolicitud = Convert.ToInt32(hfIdUsuarioSolicita.Value);
            vSolicitudModPlanCompras.UsuarioSolicitud = txtUsuarioSolicita.Text;
            //vSolicitudModPlanCompras.IdVigencia = Convert.ToInt32(ddlIdVigencia.SelectedValue);
            vSolicitudModPlanCompras.Vigencia = TxtVigenciaPlanCompra.Text; //ddlIdVigencia.SelectedItem.Text;

            vSolicitudModPlanCompras.Justificacion = Convert.ToString(txtJustificacion.Text);

            //VAlida con el numero consecutivo plan de compras una solicitud no se encuentre en estado Enviado
            if (vContratoService.ExisteEnviadoSolicitudModPlanCompras("001", vSolicitudModPlanCompras.NumeroConsecPlanDeCompras, vSolicitudModPlanCompras.Vigencia))
            {
                throw new Exception("El consecutivo seleccionado tiene una solicitud pendiente");

            }

            try
            {

                var estadompc = vWsContratosPacco.SolicitudModificacionServicio(vSolicitudModPlanCompras.NumeroConsecPlanDeCompras,
                     Convert.ToInt16(vSolicitudModPlanCompras.Vigencia), vSolicitudModPlanCompras.IdContrato,
                     GetSessionUser().CorreoElectronico, vSolicitudModPlanCompras.Justificacion);

                List<EstadoSolcitudModPlanCompras> vresult;

                //Esatdo devolucion del servicio de pacco
                switch (estadompc)
                {
                    case 1: //1. Aprobado
                        vresult = vContratoService.ConsultarEstadoSolcitudModPlanComprass("002", null, true); // Aprobado
                        break;
                    case 2://2. En proceso
                        vresult = vContratoService.ConsultarEstadoSolcitudModPlanComprass("001", null, true); // enviado
                        break;
                    case 3: //3. Rechazado
                        vresult = vContratoService.ConsultarEstadoSolcitudModPlanComprass("003", null, true); // rechazado
                        break;
                    default:
                        vresult = null;
                        break;
                }


                if (vresult == null || !vresult.Any())
                {
                    throw new Exception("No se ha configurado los estado del plan de compras, verifique por favor.");

                }

                vSolicitudModPlanCompras.IdEstadoSolicitud = Convert.ToString(vresult[0].IdEstadoSolicitud);

                ExitoPACCO = true;
            }
            catch (Exception ex)
            {
                //ExitoPACCO = false;

                toolBar.MostrarMensajeError("No se puede enviar la información a PACCO para continuar");
                return;
            }


            if (ExitoPACCO)
            {
                vSolicitudModPlanCompras.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vSolicitudModPlanCompras, this.PageName, vSolutionPage);

                vSolicitudModPlanCompras.UsuarioCrea = GetSessionUser().NombreUsuario;
                vResultado = vContratoService.InsertarSolicitudModPlanCompras(vSolicitudModPlanCompras);
            }

            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                txtFechaSolicitud.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                txtEstadopc.Text = "Enviado";
                txtJustificacion.Enabled=false;
                txtNumeroSolicitud.Text = vSolicitudModPlanCompras.IdSolicitudModPlanCompra.ToString();
                toolBar.eventoGuardar -= new ToolBarDelegateLupa(btnGuardar_Click);
            
                SetSessionParameter("SolicitudModPlanCompras.IdSolicitudModPlanCompra", vSolicitudModPlanCompras.IdSolicitudModPlanCompra);
                
                toolBar.MostrarMensajeGuardado();

                string returnValues =
               "<script language='javascript'> " +
               "var pObj = Array();";
                returnValues += "pObj[" + (0) + "] = '0';" +
                               "if (window.opener)" +
                               "{" +
                               "    window.opener.returnValue = pObj" +
                               "}" +
                               "window.returnValue = pObj;" +
                "</script>";

                ClientScript.RegisterClientScriptBlock(Page.GetType(), "rv", returnValues);

                //NavigateTo("DetalleAdd.aspx");
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, afectó más registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.EstablecerTitulos("Solicitud Modificación Plan de Compras", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            txtUsuarioSolicita.Text = GetSessionUser().PrimerNombre + ' ' +
                        GetSessionUser().SegundoNombre + ' ' + GetSessionUser().PrimerApellido + ' ' +
                        GetSessionUser().SegundoApellido;
            hfIdUsuarioSolicita.Value = GetSessionUser().IdUsuario.ToString(); ;
            txtUsuarioSolicita.Enabled = false;
            ddlIdEstadoSolicitud.Visible = false;
            ddlIdEstadoSolicitud.SelectedValue = "1";
            
            if (GetSessionParameter("Contrato.idplandecompras") != null && Convert.ToString(GetSessionParameter("Contrato.idplandecompras")) != "")
            {
                int vIdPlanCompras = Convert.ToInt32(GetSessionParameter("Contrato.idplandecompras"));
                //RemoveSessionParameter("  ");
                string vVigencias = GetSessionParameter("Contrato.VigenciaPlanCompra").ToString();
                //RemoveSessionParameter("Contrato.VigenciaPlanCompra");

                TxtVigenciaPlanCompra.Text = vVigencias;
                TxtVigenciaPlanCompra.Enabled = false;

                PlanDeCompras vPlanDeComprasContrato = vContratoService.ConsultarPlanDeCompras(vIdPlanCompras, vVigencias);
                if (vPlanDeComprasContrato != null)
                {
                    HabilitaControlContrato(false);
                    HabilitaControlNumConsecPlan(false);

                    if (vPlanDeComprasContrato.IdPlanDeCompras == 0)
                    {
                        toolBar.MostrarMensajeError("No Existe El Plan de Compra Enviado");
                        return;
                    }
                    hfIdPlanDeCompras.Value = vIdPlanCompras.ToString();
                    txtNumeroConsecutivoPlan.Text = vPlanDeComprasContrato.NumeroConsecutivo.ToString();


                    int vIdContrato = vPlanDeComprasContrato.IdContrato;
                    List<Contrato> vlistContrato = vContratoService.ConsultarContratosLupa(vIdContrato, null);
                    if (vlistContrato.Count() > 0)
                    {
                        long vNumeroContrato = Convert.ToInt64(vlistContrato.First<Contrato>().NumeroContrato);
                        txtIdContrato.Text = vIdContrato.ToString();
                        txtNumeroContrato.Text = vNumeroContrato == 0 ? "" : vNumeroContrato.ToString();
                        HabilitaControlContrato(false);
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("No existe contrato");
                        return;
                    }

                }
                else
                {
                    toolBar.MostrarMensajeError("No existe Número consecutivo Plan de Compras");
                }

            }
            else //La llamda es desde menú
            {
                HabilitaControlContrato(false);
                HabilitaControlNumConsecPlan(false);
                //HabilitaControlContrato(true);
                //HabilitaControlNumConsecPlan(true);
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //CargarVigencias();
            CargarEstadosSolicitud();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de datos de IdEstadoSolicitud
    /// </summary>
    //private void CargarVigencias()
    //{

    //    var vResult = vSIAService.ConsultarVigencias(true);
    //    ManejoControlesContratos.LlenarComboLista(ddlIdVigencia, vResult, "IdVigencia", "AcnoVigencia");
    //}

    private void CargarEstadosSolicitud()
    {
        var vresult = vContratoService.ConsultarEstadoSolcitudModPlanComprass(null, null, true);
        ManejoControlesContratos.LlenarComboLista(ddlIdEstadoSolicitud, vresult, "IdEstadoSolicitud", "Descripcion");
    }

    private void HabilitaControlContrato(bool pHabilita)
    {
        txtIdContrato.Enabled = pHabilita;
        txtNumeroContrato.Enabled = pHabilita;
    }

    private void HabilitaControlNumConsecPlan(bool pHabilita)
    {
        txtNumeroConsecutivoPlan.Enabled = pHabilita;
    }
}
