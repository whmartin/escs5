using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de visualización detallada para la entidad SolicitudModPlanCompras
/// </summary>
public partial class Page_SolicitudModPlanCompras_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudModPlanCompras";
    ContratoService vContratoService = new ContratoService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //vSolutionPage = SolutionPage.Detail;
        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
            if (!Page.IsPostBack)
            {
                //CargarDatosIniciales();
                CargarDatos();
            }
        //}
    }

   
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
        /// <summary>
        /// Método de carga de datos del registro 
        /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdSolicitudModPlanCompra = Convert.ToInt32(GetSessionParameter("SolicitudModPlanCompras.IdSolicitudModPlanCompra"));
            RemoveSessionParameter("SolicitudModPlanCompras.IdSolicitudModPlanCompra");

            SolicitudModPlanCompras vSolicitudModPlanCompras = new SolicitudModPlanCompras();
            vSolicitudModPlanCompras = vContratoService.ConsultarSolicitudModPlanCompras(vIdSolicitudModPlanCompra);
            hfIdSolicitudModPlanCompra.Value = vSolicitudModPlanCompras.IdSolicitudModPlanCompra.ToString();
            txtIdContrato.Text = vSolicitudModPlanCompras.IdContrato.ToString();
            txtNumeroContrato.Text = vSolicitudModPlanCompras.NumeroContrato;
            txtNumeroConsecPlanCompras.Text = vSolicitudModPlanCompras.NumeroConsecPlanDeCompras.ToString();
            txtUsuarioSolicita.Text = vSolicitudModPlanCompras.UsuarioSolicitud;
            txtVigencia.Text = vSolicitudModPlanCompras.Vigencia;
            txtJustificacion.Text = vSolicitudModPlanCompras.Justificacion;
            txtNumeroSolicitud.Text = vSolicitudModPlanCompras.IdSolicitudModPlanCompra.ToString();
            txtFechaHoraSolicitud.Text = vSolicitudModPlanCompras.FechaSolicitud.ToString("dd/MM/yyyy HH:MM:ss");
            txtEstado.Text = vSolicitudModPlanCompras.EstadoSolicitud;

            ObtenerAuditoria(PageName, hfIdSolicitudModPlanCompra.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSolicitudModPlanCompras.UsuarioCrea, vSolicitudModPlanCompras.FechaCrea, vSolicitudModPlanCompras.UsuarioModifica, vSolicitudModPlanCompras.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
        /// <summary>
        /// Método para establecer título e inicializar delegados de botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;

            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("solicitud modificación Pago de compras", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    

}
