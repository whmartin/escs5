﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DetalleAdd.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master" Inherits="Page_Contratos_SolicitudModPlanCompras_DetalleAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdSolicitudModPlanCompra" runat="server" />
    <table width="90%" align="center">
        
        <tr class="rowB">
            <td>
                Número de solicitud
            </td>
            <td>
                 Fecha y hora de la solicitud
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroSolicitud" Width="85%"  Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaHoraSolicitud" Width="85%" Enabled="false"></asp:TextBox>
            </td>
        </tr> 
        <tr class="rowB">
            <td>
                Estado
            </td>
            <td>
                 
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtEstado" Width="85%" Enabled="false"></asp:TextBox>
            </td>
            <td>
                
            </td>
        </tr>  
    </table>
</asp:Content>