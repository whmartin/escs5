using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;

/// <summary>
/// Página de registro y edición para la entidad Garantia
/// </summary>
public partial class Page_GestionarGarantia_Add : GeneralWeb
{
    #region Variables
    General_General_Master_Lupa toolBar;
    ContratoService vContratoService = new ContratoService();

    string PageName = "Contratos/VigenciasFuturas";
    #endregion

    #region Eventos
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        //if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        //{
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        //}
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        CerrarLupa();
    }

    #endregion

    #region Métodos

    private void CerrarLupa()
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    /// <summary>
    /// Método de guardado para la entidad Garantia
    /// </summary>
    private void Guardar()
    {
        try
        {
            #region Crecion objeto Vigencias futuras
            int vResultado = 0;

            
            VigenciaFuturas vVigenciaFuturas = new VigenciaFuturas();
            vVigenciaFuturas.NumeroRadicado = txtNumeroRadicadoVigenciaFutura.Text;
            vVigenciaFuturas.FechaExpedicion = txtFechaExpedicionVigenciaFutura.Date;
            vVigenciaFuturas.ValorVigenciaFutura = decimal.Parse(txtValorVigenciaFutura.Text);
            vVigenciaFuturas.AnioVigencia = Convert.ToInt32(ddlAnio.SelectedValue);
            vVigenciaFuturas.Estado = 1;
            
            #endregion


            #region Validaciones de negocio
            string strValidacion = ValidarReglasDeNegocio(vVigenciaFuturas);
            if (strValidacion != string.Empty)
            {
                toolBar.MostrarMensajeError(strValidacion);
                return;
            }
            #endregion


            #region Grabar vigencias futuras
            vVigenciaFuturas.IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
            vVigenciaFuturas.UsuarioCrea = GetSessionUser().NombreUsuario;
            vVigenciaFuturas.FechaCrea = DateTime.Now;
            InformacionAudioria(vVigenciaFuturas, this.PageName, vSolutionPage);
            vResultado = vContratoService.InsertarVigenciaFuturas(vVigenciaFuturas);
            vContratoService.ConsultarContrato(Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP")));
            #endregion


            #region Mostrar al usuario el resultado de la transaccion
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                CerrarLupa();
            }
            #endregion

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);

            toolBar.EstablecerTitulos("Vigencias Futuras", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
          
            int idContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
            SIAService vSIAService = new SIAService();
            Icbf.Contrato.Entity.Contrato vContrato = new Icbf.Contrato.Entity.Contrato();            
            vContrato = vContratoService.ConsultarContrato(idContrato);          
            
            ddlAnio.DataSource = vSIAService.ConsultarVigencias(true).Where(x=> x.AcnoVigencia > vContrato.FechaInicioEjecucion.Value.Year && x.AcnoVigencia <= vContrato.FechaFinalTerminacionContrato.Value.Year).OrderBy(x=> x.AcnoVigencia);
            ddlAnio.DataTextField = "AcnoVigencia";
            ddlAnio.DataValueField = "AcnoVigencia";            
            ddlAnio.DataBind();

            

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Validación de reglas de negocio
    /// </summary>
    /// <param name="vGarantia">Objeto garantía</param>
    /// <returns></returns>
    private string ValidarReglasDeNegocio(VigenciaFuturas vVigenciaFuturas)
    {
        int idContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
        Icbf.Contrato.Entity.Contrato vContrato = new Icbf.Contrato.Entity.Contrato();
        vContrato = vContratoService.ConsultarContrato(idContrato);

        //if (!(txtFechaExpedicionVigenciaFutura.Date == DateTime.Now.AddYears(-1).Date || txtFechaExpedicionVigenciaFutura.Date.Year == DateTime.Now.Date.Year))
        //{
        //    return "La fecha de expedición de la vigencia futura debe ser menor en un año o igual al año actual";
        //}

        //if (vContrato.FechaInicioEjecucion <= txtFechaExpedicionVigenciaFutura.Date )
        //{
        //    return "La fecha de expedición de la vigencia futura debe ser menor o igual a la fecha de Inicio de Ejecucion de Contrato";
        //}

        //PENDIENTE: validar regla de negocio
        
        //decimal? valorTotalCDP = 0;
        //List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDP(idContrato);
        //foreach (ContratosCDP cdp in cdps)
        //{
        //     valorTotalCDP += cdp.ValorRubro;
        //}

        if (vContratoService.ValidaValorInicialVsCDPplusVigenciasFuturas(idContrato, vVigenciaFuturas))//, valorTotalCDP))
        {
            return "El valor inicial del contrato/convenio debe ser menor o igual al valor de la informacion presupuestal CDPS y/o Vigencias Futuras";
        }

        return string.Empty;
    }
    #endregion
    protected void ddlAnioSelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
    }

}
