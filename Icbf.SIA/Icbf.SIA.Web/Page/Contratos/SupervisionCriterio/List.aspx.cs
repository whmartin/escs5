using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionCriterios_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionCriterio";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                BuscarLoad();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }



    private void BuscarLoad()
    {
        try
        {
            int? vIdPregunta = null;

            if (hfIdPregunta.Value != "")
            {
                vIdPregunta = Convert.ToInt32(hfIdPregunta.Value);
            }

            gvSupervisionCriterios.DataSource = vSupervisionService.ConsultarSupervisionCriterioss(vIdPregunta, null, null);
            gvSupervisionCriterios.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Buscar()
    {
        try
        {
            int? vIdPregunta = null;
            String vValor = null;
            int? vEstado = null;

            if (hfIdPregunta.Value != "")
            {
                vIdPregunta = Convert.ToInt16(hfIdPregunta.Value);
            }
                       
            if (txtValor.Text != "")
            {
                vValor = Convert.ToString(txtValor.Text);
            }
            if (rblEstado.SelectedValue != "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            gvSupervisionCriterios.DataSource = vSupervisionService.ConsultarSupervisionCriterioss(vIdPregunta, vValor, vEstado);
            gvSupervisionCriterios.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionCriterios.PageSize = PageSize();
            gvSupervisionCriterios.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Criterios", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionCriterios.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionCriterios.IdCriterio", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionCriterios_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionCriterios.SelectedRow);
    }
    protected void gvSupervisionCriterios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionCriterios.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionCriterios.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionCriterios.Eliminado");

            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            hfIdPregunta.Value = Preguntasupervision[1];
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionCriterios_RowDataBound(object sender, GridViewRowEventArgs e)
    {        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            switch (e.Row.Cells[2].Text)
            {
                case "1":
                    e.Row.Cells[2].Text = "Activo";
                    break;
                default:
                    e.Row.Cells[2].Text = "Desactivado";
                    break;
            }

        }

    }
}
