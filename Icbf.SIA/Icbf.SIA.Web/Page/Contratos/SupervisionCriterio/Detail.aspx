<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SupervisionCriterios_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdPregunta" runat="server" />
<asp:HiddenField ID="hfIdCriterio" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Pregunta *
            </td>
            <td>
                Criterio *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtIdPregunta"  Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValor"  Enabled="false" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="2">
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
