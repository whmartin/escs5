using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionCriterios_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionCriterio";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();

            vSupervisionCriterios.IdPregunta = Convert.ToInt32(hfIdPregunta.Value);
            vSupervisionCriterios.Valor = Convert.ToString(txtValor.Text);
            vSupervisionCriterios.Estado = Convert.ToInt32(rblEstado.SelectedValue);
            int vResultExis = vSupervisionService.ConsultarSupervisionCriterios(vSupervisionCriterios.Valor);

            if (Request.QueryString["oP"] == "E")
            {
                if (vResultExis == 0)
                {

                    vSupervisionCriterios.IdCriterio = Convert.ToInt32(hfIdCriterio.Value);
                    vSupervisionCriterios.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionCriterios, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.ModificarSupervisionCriterios(vSupervisionCriterios);
                }
                else
                {
                    toolBar.MostrarMensajeError("El criterio ingresado ya existe");
                    vResultado = 3;
                }
            }
            else
            {
                if (vResultExis == 0)
                {
                    vSupervisionCriterios.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionCriterios, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.InsertarSupervisionCriterios(vSupervisionCriterios);
                }
                else
                {
                    toolBar.MostrarMensajeError("El criterio ingresado ya existe");
                    vResultado = 3;
                }
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SupervisionCriterios.IdCriterio", vSupervisionCriterios.IdCriterio);
                SetSessionParameter("SupervisionCriterios.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado != 3)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Criterios", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdCriterio = Convert.ToInt32(GetSessionParameter("SupervisionCriterios.IdCriterio"));
            RemoveSessionParameter("SupervisionCriterios.Id");

            SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
            vSupervisionCriterios = vSupervisionService.ConsultarSupervisionCriterios(vIdCriterio);
            hfIdCriterio.Value = vSupervisionCriterios.IdCriterio.ToString();
            txtIdPregunta.Text = vSupervisionCriterios.IdPregunta.ToString();
            txtValor.Text = vSupervisionCriterios.Valor;
            rblEstado.SelectedValue = vSupervisionCriterios.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionCriterios.UsuarioCrea, vSupervisionCriterios.FechaCrea, vSupervisionCriterios.UsuarioModifica, vSupervisionCriterios.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");
            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            hfIdPregunta.Value = Preguntasupervision[1];
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
