using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionCriterios_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionCriterio";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionCriterios.IdCriterio", hfIdCriterio.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdCriterio = Convert.ToInt32(GetSessionParameter("SupervisionCriterios.IdCriterio"));
            RemoveSessionParameter("SupervisionCriterios.IdCriterio");

            if (GetSessionParameter("SupervisionCriterios.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionCriterios.Guardado");

            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
            vSupervisionCriterios = vSupervisionService.ConsultarSupervisionCriterios(vIdCriterio);
            hfIdCriterio.Value = vSupervisionCriterios.IdCriterio.ToString();
            txtValor.Text = vSupervisionCriterios.Valor;
            rblEstado.SelectedValue = vSupervisionCriterios.Estado.ToString();
            ObtenerAuditoria(PageName, hfIdCriterio.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionCriterios.UsuarioCrea, vSupervisionCriterios.FechaCrea, vSupervisionCriterios.UsuarioModifica, vSupervisionCriterios.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdCriterio = Convert.ToInt32(hfIdCriterio.Value);

            SupervisionCriterios vSupervisionCriterios = new SupervisionCriterios();
            vSupervisionCriterios = vSupervisionService.ConsultarSupervisionCriterios(vIdCriterio);
            InformacionAudioria(vSupervisionCriterios, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionCriterios(vSupervisionCriterios);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionCriterios.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Criterios", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo","1");
            List<string> Preguntasupervision = new List<string>();
            Preguntasupervision = (List<string>)Session["Preguntasupervision"];
            txtIdPregunta.Text = Preguntasupervision[0];
            hfIdPregunta.Value = Preguntasupervision[1];
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
