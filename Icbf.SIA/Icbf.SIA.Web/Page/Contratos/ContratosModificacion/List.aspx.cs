using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using Icbf.Contrato.Entity.PreContractual;
using System.Linq.Expressions;

public partial class Page_Contratos_ContratosModificacion_List : GeneralWeb
{
    masterPrincipal toolBar;
     
    string PageName = "Contratos/ContratosModificacion";

    SIAService vRuboService = new SIAService();

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    /// <summary>
    /// Guarda la direcci�n de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void gvContratos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvContratos.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    protected void gvContratos_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContratos.SelectedRow);
    }

    protected void gvContratos_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIDCategoriaContrato.SelectedValue != "-1")
        {
            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;
        }
        else
        {
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
        }

    }

    private void Buscar()
    {
        try
        {
            CargarGrilla(gvContratos, GridViewSortExpression, false);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            gvContratos.PageSize = PageSize();
            gvContratos.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Modificaci&oacute;n de Contratos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        string controlFechaValidando = string.Empty;
        try
        {
            DateTime? vFechaRegistroSistemaDesde = null;
            DateTime? vFechaRegistroSistemaHasta = null;
            int? vIdContrato = null;
            int? vVigenciaFiscalinicial = null;
            int? vIDRegional = null;
            int? vIDModalidadSeleccion = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            int? vIDEstadoContrato = null;
            string vNumeroContrato = null;

            controlFechaValidando = "Fecha Registro al Sistema Desde";
            if (txtFechaRegistroSistemaDesde.Date.Year.ToString() != "1900")
            {
                vFechaRegistroSistemaDesde = Convert.ToDateTime(txtFechaRegistroSistemaDesde.Date);
            }

            controlFechaValidando = string.Empty;

            controlFechaValidando = "Fecha Registro al Sistema Hasta";
            if (txtFechaRegistroSistemaHasta.Date.Year.ToString() != "1900")
            {
                vFechaRegistroSistemaHasta = Convert.ToDateTime(txtFechaRegistroSistemaHasta.Date);
            }
            controlFechaValidando = string.Empty;
            if (vFechaRegistroSistemaHasta != null)
            {
                TimeSpan ts = new TimeSpan(0, 23, 59, 59);
                vFechaRegistroSistemaHasta = Convert.ToDateTime(vFechaRegistroSistemaHasta) + ts;
            }

            if (txtIdContrato.Text != "")
                vIdContrato = Convert.ToInt32(txtIdContrato.Text);

            if (!string.IsNullOrEmpty(txtNumeroContrato.Text))
                vNumeroContrato = txtNumeroContrato.Text;
            
            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
                vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            
            if (ddlIDRegional.SelectedValue != "-1")
                vIDRegional = Convert.ToInt32(ddlIDRegional.SelectedValue);
            
            if (ddlIDModalidadSeleccion.SelectedValue != "-1")
                vIDModalidadSeleccion = Convert.ToInt32(ddlIDModalidadSeleccion.SelectedValue);
            
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
            
            if (ddlIDTipoContrato.SelectedValue != "-1" && ddlIDTipoContrato.SelectedValue != string.Empty)
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);

            if (ddlEstadoContrato.SelectedValue != "-1")
                vIDEstadoContrato = Convert.ToInt32(ddlEstadoContrato.SelectedValue);

            string usuarioArea = null;

            if (GetSessionUser().IdTipoUsuario != 1)
                usuarioArea = GetSessionUser().NombreUsuario;

            List<Contrato> myGridResults = vContratoService.ConsultarContratosModificacion(vFechaRegistroSistemaDesde, vFechaRegistroSistemaHasta, vIdContrato, vVigenciaFiscalinicial, vIDRegional, vIDModalidadSeleccion, vIDCategoriaContrato, vIDTipoContrato, vIDEstadoContrato, vNumeroContrato);

                int nRegistros = myGridResults.Count;
                int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

                if (nRegistros < NumRegConsultaGrilla)
                {
                    if (expresionOrdenamiento != null)
                    {
                        if (string.IsNullOrEmpty(GridViewSortExpression))
                            GridViewSortDirection = SortDirection.Ascending;
                        else if (GridViewSortExpression != expresionOrdenamiento)
                            GridViewSortDirection = SortDirection.Descending;

                        if (myGridResults != null)
                        {
                            var param = Expression.Parameter(typeof(SolicitudContrato), expresionOrdenamiento);

                            var prop = Expression.Property(param, expresionOrdenamiento);

                            var sortExpression = Expression.Lambda<Func<Contrato, object>>(Expression.Convert(prop, typeof(object)), param);

                            if (GridViewSortDirection == SortDirection.Ascending)
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Descending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                if (cambioPaginacion == false)
                                {
                                    GridViewSortDirection = SortDirection.Ascending;
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                                }
                                else
                                    gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }

                            GridViewSortExpression = expresionOrdenamiento;
                        }
                    }
                    else
                        gridViewsender.DataSource = myGridResults;
                    
                    gridViewsender.DataBind();
                }
                else
                    toolBar.MostrarMensajeError("Esta consulta es demasiado grande, ingrese un criterio de consulta");
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inv�lido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaVigencia();
            CargarListaRegional();
            CargarModalidadSeleccion();
            CargarCategoriaContrato();
            CargarEstados();

            if (!string.IsNullOrEmpty(GetSessionParameter("ModificacionContrato.Modifico").ToString()))
            {
                toolBar.MostrarMensajeGuardado(GetSessionParameter("ModificacionContrato.Modifico").ToString());
                RemoveSessionParameter("ModificacionContrato.Modifico");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaVigencia()
    {
        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {
            if (usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ddlIDRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ddlIDRegional.SelectedValue = usuario.IdRegional.ToString();
                        ddlIDRegional.Enabled = false;
                    }
                    else
                    {
                        ddlIDRegional.SelectedValue = "-1";
                        ddlIDRegional.Enabled = false;
                    }
                }
            }
            else
                ManejoControlesContratos.LlenarComboLista(ddlIDRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
        }
    }

    public void CargarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void CargarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void CargarEstados()
    {
       ddlEstadoContrato.Items.Clear();
       var vLEstados = vContratoService.ConsultarEstadoContrato(null, null, null, true);
       foreach (EstadoContrato tD in vLEstados)
       {
           if (
                tD.CodEstado.Contains("CRP") || 
                tD.CodEstado.Contains("SUS") || 
                tD.CodEstado.Contains("EJE") || 
                tD.CodEstado.Contains("TER") || 
                tD.CodEstado.Contains("LIQB") || 
                tD.CodEstado.Contains("ANTB") || 
                tD.CodEstado.Contains("LIQU") || 
                tD.CodEstado.Contains("ANTU") 
               )
           ddlEstadoContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdEstadoContrato.ToString()));
       }
        
        ddlEstadoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContratos.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ModificacionContrato.IdContrato", strValue);
            NavigateTo(SolutionPage.Add);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
