<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Contratos_ContratosModificacion_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
 <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
                <tr class="rowB">
            <td class="Cell">
                Fecha Inicio Desde
            </td>
            <td class="Cell">
                Fecha Finalizaci&oacute;n hasta
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaDesde" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
            <td class="Cell">
                <uc1:fecha ID="txtFechaRegistroSistemaHasta" runat="server" Width="80%" Enabled="True" 
                        Requerid="False" />
                
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Id Contrato</td>
            <td class="Cell">
                N&uacute;mero de Contrato</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtIdContrato" MaxLength="30" ></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftIdContrato" runat="server" TargetControlID="txtIdContrato"
                    FilterType="Numbers" ValidChars="" />
            </td>
            <td class="Cell">
                                <asp:TextBox ID="txtNumeroContrato" runat="server" MaxLength="30"></asp:TextBox>
                                <Ajax:FilteredTextBoxExtender ID="fttxtNumeroContrato" runat="server" FilterType="Numbers" TargetControlID="txtNumeroContrato" ValidChars="" />
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Vigencia Fiscal Inicial</td>
            <td class="Cell">
                Modalidad de Selecci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList ID="ddlVigenciaFiscalinicial" runat="server">
                </asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDModalidadSeleccion"  ></asp:DropDownList>
            </td>
        </tr> 
        <tr class="rowB">
            <td class="Cell">
                Categoria del Contrato/Convenio</td>
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDCategoriaContrato" 
                    onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged" 
                    AutoPostBack="True"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlIDTipoContrato"  ></asp:DropDownList>
            </td>
        </tr>     
          <tr class="rowB">
            <td class="Cell">
                Estado de Contrato</td>
            <td class="Cell">
                
                Regional Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList runat="server" ID="ddlEstadoContrato" 
                    AutoPostBack="True"  ></asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlIDRegional" runat="server">
                </asp:DropDownList>
            </td>
        </tr>     

    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvContratos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdContrato" CellPadding="0" Height="16px"
                        OnPageIndexChanging="gvContratos_PageIndexChanging" OnSorting="gvContratos_Sorting" OnSelectedIndexChanged="gvContratos_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Ver Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>  
                                <asp:BoundField HeaderText="Fecha Inicio" DataField="FechaInicioEjecucion"  SortExpression="FechaInicioEjecucion" DataFormatString="{0:dd/MM/yyyy}"/>
                                <asp:BoundField HeaderText="Id Contrato" DataField="IdContrato"  SortExpression="IdContrato"/>
                                <asp:BoundField HeaderText="Consecutivo de Contrato" DataField="Consecutivo"  SortExpression="Consecutivo"/>
                                <asp:BoundField HeaderText="Regional" DataField="NombreRegional"  SortExpression="NombreRegional"/>
                                <asp:BoundField HeaderText="Modalidad de Selecci&oacute;n" DataField="NombreModalidadSeleccion"  SortExpression="NombreModalidadSeleccion"/>
                                <asp:BoundField HeaderText="Categoria del Contrato/Convenio" DataField="NombreCategoriaContrato"  SortExpression="NombreCategoriaContrato"/>
                                <asp:BoundField HeaderText="Tipo de Contrato/Convenio" DataField="NombreTipoContrato"  SortExpression="NombreTipoContrato"/>
                                <asp:BoundField HeaderText="Estado" DataField="NombreEstadoContrato"  SortExpression="NombreEstadoContrato"/>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
