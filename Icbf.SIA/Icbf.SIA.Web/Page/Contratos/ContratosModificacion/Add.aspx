﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_ContratosModificacion_Add" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fechaJScontratos.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
  
        <script type="text/javascript" language="javascript">


            function ValidarFormato(n, currency) {

                var nsinFormato = n.split('.').join('');
                nsinFormato = nsinFormato.split('$').join('');

                var valorEjecutado = parseFloat(nsinFormato);

                var valueWithFormat = currency + " " + valorEjecutado.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                valueWithFormat = valueWithFormat.split('.').join('*');
                valueWithFormat = valueWithFormat.split(',').join('.');
                valueWithFormat = valueWithFormat.split('*').join(',');

                document.getElementById('<%= txtValorInicialAportesICBF.ClientID %>').value = valueWithFormat;
            }


            function muestraImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "visible";
            }

            function ocultaImagenLoading() {
                var imgLoading = document.getElementById("imgLoading");
                imgLoading.style.visibility = "";
            }

            function prePostbck(imagenLoading) {
                if (imagenLoading == true)
                    muestraImagenLoading();
                else
                    ocultaImagenLoading();
            }

            function GetCalculoFechaFinal() {
                muestraImagenLoading();

                var anio = '<%= caFechaInicioEjecucion.Date.Year  %>';
                var mes = '<%= caFechaInicioEjecucion.Date.Month  %>';
                var dia = '<%= caFechaInicioEjecucion.Date.Day  %>';

                var diasCalculados = '<%= txtDiasPiEj.Text  %>';
                var mesesCalculados = '<%= txtMesesPiEj.Text  %>';

                window_showModalDialog('../../../Page/Contratos/Lupas/LupaCalcularFecha.aspx?anio=' + anio + '&mes=' + mes + '&dia=' + dia + '&diasCalculados=' + diasCalculados + '&mesesCalculados=' + mesesCalculados, setReturnCalculoFechaFinal, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnCalculoFechaFinal() {
              prePostbck(false);
                __doPostBack('<%= caFechaFinalizacionInicial.ClientID %>', '');
            }

            function EjecutarJS(control) {
                switch (control.id) {
                    case "cphCont_caFechaFinalizacionInicial_txtFecha":
               prePostbck(false);
                        __doPostBack('<%= caFechaFinalizacionInicial.ClientID %>', '');
                        break;
                    case "cphCont_caFechaInicioEjecucion_txtFecha":
                        prePostbck(false);
                        __doPostBack('<%= caFechaInicioEjecucion.ClientID %>', '');
                        break;
                    default:
                        break;
                }
            }

            $(document).ready(function () {

                setTimeout(function () {
                    $('#cphCont_caFechaInicioEjecucion_txtFecha').trigger('mouseover');}, 10);

                $('#cphCont_caFechaInicioEjecucion_txtFecha').mouseover(function () {

                    setTimeout(function () {
                        $('cphCont_caFechaInicioEjecucion_txtFecha').show();
                    }, 500);
                });

                $('#<%=ddlVigenciaFiscalIni.ClientID %>').change(function () {

                    var inicial =  $(this).val();
                    var final =  $('#<%=ddlVigenciaFiscalFin.ClientID %>').val();

                    if (final != '' && final != null) {

                        var finalInt = parseInt(final);
                        var inicialInt = parseInt(inicial);

                        if(finalInt < inicialInt)
                        {
                            $('#<%=ddlVigenciaFiscalFin.ClientID %>').val(inicial);
                        }
                    }
                });

                $('#<%=ddlVigenciaFiscalFin.ClientID %>').change(function () {
                    
                    var final = $(this).val();
                    var inicial = $('#<%=ddlVigenciaFiscalIni.ClientID %>').val();

                    if (inicial != '' && inicial != null) {

                        var finalInt = parseInt(final);
                        var inicialInt = parseInt(inicial);

                        if (finalInt < inicialInt) {
                            $('#<%=ddlVigenciaFiscalFin.ClientID %>').val(inicial);
                        }
                    }
                    else {
                        $('#<%=ddlVigenciaFiscalIni.ClientID %>').val(inicial);
                    }

                });

                $('#<%=rblManejaVigenciasFuturas.ClientID %>  input').change(function () {

                    if ($(this).val() == 'Si') {
                        $("[id*=imgVigFuturas]").show();
                        //__doPostBack('ObtenerVigenciasFuturas', '');
                    }
                    else {
                        $("[id*=imgVigFuturas]").hide();
                        var VigenciasFututasExiste = document.getElementById('<%= hfVigenciasFuturas.ClientID %>').value;

                        if (VigenciasFututasExiste != null && VigenciasFututasExiste != '') {

                            if (confirm('Esta seguro que desea eliminar, la información de las vigencias futuras ?')) {
                                __doPostBack('EliminarVigenciasFuturas', '');
                            }
                            else {
                                $('#<%=rblManejaVigenciasFuturas.ClientID %>').find("input[value='Si']").prop("checked", true);
                            }
                        }
                        else {
                            __doPostBack('ObtenerVigenciasFuturas', '');
                        }
                    }
                });

                $("#<%=chkCalculaFecha.ClientID%>").on('click', function () {
                    var $cb = $(this);
                    if ($cb.is(':checked')) {
                        $("[id*=ImgBtnFechaFinal]").show();
                    }
                    else {
                        $("[id*=ImgBtnFechaFinal]").hide();
                    }
                });


                $('#<%=rblManejaCofinanciacion.ClientID %> input').change(function () {

                    if ($(this).val() == 'Si') {
                        $("[id*=imgValorApoContrat]").show();
                    }
                    else {
                        var aportesCofinanciacionExiste = document.getElementById('<%= hfCofinanciacion.ClientID %>').value;

                        if (aportesEspecieExiste != null && aportesEspecieExiste != '') {

                            if (confirm('Esta seguro que desea eliminar, la información de los aportes en confinaciación ?')) {
                                $("[id*=imgValorApoContrat]").hide();
                                __doPostBack('EliminarAportesCofinanciacion', '');
                            }
                            else {
                                $('#<%=rblManejaCofinanciacion.ClientID %>').find("input[value='Si']").prop("checked", true);
                            }
                        }
                        else 
                            $("[id*=imgValorApoContrat]").hide();
                    }
                });

               $('#<%= rblRequiereActaInicio.ClientID %> input').change(function () {

                   if ($(this).val() == 'Si') {
                       $("[id*=DivDateFechaActaInicio]").show();
                       $("[id*=DivLblFechaActaInicio]").show();

                       var existeFechaActaInicio = document.getElementById('<%= hfValorFechaActaIncion.ClientID  %>').value;

                       if (existeFechaActaInicio != null && existeFechaActaInicio != '') {
                           document.getElementById('<%= txtFechaActaInicio.ClientID  %>').value = existeFechaActaInicio;
                       }
                    }
                    else {
                       $("[id*=DivDateFechaActaInicio]").hide();
                       $("[id*=DivLblFechaActaInicio]").hide();
                       document.getElementById('<%= txtFechaActaInicio.ClientID  %>').value = '';
                    }
                });


                $('#<%=rblManejaAportesEspecie.ClientID%> input').change(function () {

                    if ($(this).val() == 'Si') {
                        $("[id*=imgValorApoICBF]").show();
                    }
                    else {
                        var aportesEspecieExiste = document.getElementById('<%= hfAportes.ClientID %>').value;

                        if (aportesEspecieExiste != null && aportesEspecieExiste != '') {

                            if (confirm('Esta seguro que desea eliminar, la información de los aportes en especie ?')) {
                                $("[id*=imgValorApoICBF]").hide();
                                __doPostBack('EliminarAportesEspecieICBF', '');
                            }
                            else {
                                $('#<%=rblManejaAportesEspecie.ClientID %>').find("input[value='Si']").prop("checked", true);
                            }
                        }
                        else 
                             $("[id*=imgValorApoICBF]").hide();
                    }
                });

            });


            function ValidaEliminacion(tipo) {

                if(tipo == 'Aportes')
                    return confirm('Esta seguro de que desea eliminar el aporte?');
                else if (tipo == 'LugarEjecuccion')
                    return confirm('Esta seguro de que desea eliminar el lugar de ejecucción?');
                else if (tipo == 'CDP')
                    return confirm('Esta seguro de que desea eliminar el CDP?');
                else if (tipo == 'RP')
                    return confirm('Esta seguro de que desea eliminar el RP?');
                else if (tipo == 'VigenciasFuturas')
                    return confirm('Esta seguro de que desea eliminar la Vigencia Futura?');
                else if (tipo == 'Documento')
                    return confirm('Esta seguro de que desea eliminar el Documento?');
                else if (tipo == 'Contratista')
                    return confirm('Esta seguro de que desea eliminar el contratista?');
                else if (tipo == 'CodigoSECOP') 
                    return confirm('Esta seguro de que desea eliminar el código SECOP?');
                else if (tipo == 'Supervisor')
                    return confirm('Esta seguro de que desea eliminar el Supervisor?');

                

            }


            function GetSuperInterv() {
                muestraImagenLoading();
                var contrato = document.getElementById('<%= hfIdContrato.ClientID %>');
                window_showModalDialog('../../../Page/Contratos/SupervisorInterContrato/RelacionarSupervisorInterventor.aspx?idContrato='+contrato.value, setReturnGetSuperInterv, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
         }
            
        function setReturnGetSuperInterv() {
            prePostbck(false);
            __doPostBack('ObtenerSupervisorInterventor', '');
        }


            function GetEmpleado(valor) {
                muestraImagenLoading();
                var valorContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                window_showModalDialog('../../../Page/Precontractual/Lupas/LupaEmpleado.aspx?op=' + valor + '&idContrato=' + valorContrato, setReturnGetEmpleado, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetEmpleado(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    if (retLupa.length > 12)
                    {
                        if (retLupa[13] == 'Sol') {
                            document.getElementById('<%= hfCodRegionalContConv.ClientID %>').value = retLupa[12];

                            document.getElementById('<%= txtNombreSolicitante.ClientID  %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                            document.getElementById('<%= txtDependenciaSolicitante.ClientID %>').value = retLupa[8];
                            document.getElementById('<%= txtCargoSolicitante.ClientID %>').value = retLupa[9];
                            document.getElementById('<%= txtRegionalContratoConvenio.ClientID %>').value = retLupa[7];
                        }
                        else {
                            document.getElementById('<%= txtNombreOrdenadorGasto.ClientID  %>').value = retLupa[3] + ' ' + retLupa[4] + ' ' + retLupa[5] + ' ' + retLupa[6];
                            document.getElementById('<%= txtRegionalOrdenadorGasto.ClientID %>').value = retLupa[7];
                            document.getElementById('<%= txtNumeroIdentOrdenadoGasto.ClientID %>').value = retLupa[1];
                            document.getElementById('<%= txtCargoOrdenadoGasto.ClientID %>').value = retLupa[9];
                        }
                        prePostbck(false);
                    }
                    else
                    {
                        ocultaImagenLoading();
                    }
                } else {
                    ocultaImagenLoading();
                }
            }

            function GetLugarEjecucion() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                window_showModalDialog('../../../Page/PreContractual/Lupas/LupaConsultarLugarContrato.aspx?idContrato=' + vIdContrato, setRetrunGetLugarEjecucion, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');

            }



            function GetPlanCompras() {
                muestraImagenLoading();

                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var vIdRegional = document.getElementById('<%= hfCodRegionalContConv.ClientID %>').value;
                var vIdPlanCompras = document.getElementById('<%= hfPlanCompras.ClientID %>').value;
                var vAnio = '<%= ddlVigenciaFiscalIni.SelectedItem.Text %>';

                var url = '';
                if(vIdPlanCompras == '')
                    url = '../../../Page/Precontractual/LupasPlanCompras/LupasBuscarPlanDeCompras.aspx?idContrato=' + vIdContrato +
                                                                                                     '&idRegional=' + vIdRegional +
                                                                                                     '&anio=' + vAnio;
                else
                    url = '../../../Page/Precontractual/LupasPlanCompras/LupasPlanCompras.aspx?idContrato=' + vIdContrato +
                                                                                             '&idRegional=' + vIdRegional +
                                                                                             '&idPlanCompras=' + vIdPlanCompras;

                window_showModalDialog(url, setReturnGetPlanCompras, 'dialogWidth:800px;dialogHeight:600px;resizable:yes;');
            }


            function GetValorAportes(tipo,tipoAporte) {

                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                muestraImagenLoading();
                var url = '../../../Page/Contratos/Lupas/LupaAportes.aspx?Aportante='+tipo+'&TipoAporte='+tipoAporte+'&idContrato='+vIdContrato;
                window_showModalDialog(url, setReturnGetValorAportes, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetValorAportes() {
               // prePostbck(false);
                __doPostBack('CargarAportes', '');
            }


            function GetLugarEjecucion() {
                muestraImagenLoading();
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                    window_showModalDialog('../../../Page/PreContractual/Lupas/LupaConsultarLugarContrato.aspx?idContrato=' + vIdContrato, setRetrunGetLugarEjecucion, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                
            }

            function setRetrunGetLugarEjecucion(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {
                    var retLupa = pObj.split(",");
                    var chkNivelNacional = document.getElementById('<%= chkNivelNacional.ClientID %>');
                    prePostbck(false);
                    if (retLupa[0] == 'SI')
                        chkNivelNacional.checked = true;
                    else
                        chkNivelNacional.checked = false;
                    __doPostBack('ObtenerLugarEjecucion', retLupa[0]);
                }
            }

            function GetCDP() {
                muestraImagenLoading();
                var vIdRegional = document.getElementById('<%= hfRegionalContConv.ClientID  %>').value;
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var VigenciaInicial = document.getElementById('<%= ddlVigenciaFiscalIni.ClientID %>').value;
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaRegInfoPresupuestalEnLinea.aspx?IdRegContrato=' + vIdRegional + '&idContrato=' + vIdContrato + '&idVigencia=' + VigenciaInicial, setReturnGetCDP, 'dialogWidth:1050px;dialogHeight:600px;resizable:yes;');
            }
            function setReturnGetCDP(dialog) {
                    prePostbck(false);
                    __doPostBack('ObtenerCDP', '');
            }

            function GetRP() {
                muestraImagenLoading();
                var vIdRegional = document.getElementById('<%= hfRegionalContConv.ClientID  %>').value;
                var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                var VigenciaInicial = document.getElementById('<%= ddlVigenciaFiscalIni.ClientID %>').value;
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaRP.aspx?idRegContrato=' + vIdRegional + '&idContrato=' + vIdContrato + '&idVigenciaInicial=' + VigenciaInicial, setReturnGetCDP, 'dialogWidth:1050px;dialogHeight:600px;resizable:yes;');
            }
            function setReturnGetCDP(dialog) {
                prePostbck(false);
                __doPostBack('ObtenerCDP', '');
            }


            
            function GetVigFuturas() {

                    var vIdContrato = document.getElementById('<%= hfIdContrato.ClientID %>').value;
                    var VigenciaInicial = document.getElementById('<%= ddlVigenciaFiscalIni.ClientID %>').value;
                    var VigenciaFinal = document.getElementById('<%= ddlVigenciaFiscalFin.ClientID %>').value;
                    var VigenciaInicialA = document.getElementById('<%= hfVigenciaInicial.ClientID %>').value;
                    var VigenciaFinalA = document.getElementById('<%= hfVigenciaFinal.ClientID %>').value;

                if (VigenciaFinal != null && VigenciaInicial != null && VigenciaFinal !='' && VigenciaInicial !='') {

                    if (VigenciaFinal == VigenciaFinalA && VigenciaInicial == VigenciaInicialA) {

                        var anioInicial = '<%= ddlVigenciaFiscalIni.SelectedItem.Text %>';
                        var anioFinal = '<%= ddlVigenciaFiscalFin.SelectedItem.Text %>';
                            muestraImagenLoading();
                            window_showModalDialog('../../../Page/Contratos/Lupas/LupaVigenciasFuturas.aspx?idContrato=' + vIdContrato + '&idVigenciaInicial=' + anioInicial + '&idVigenciaFinal=' + anioFinal, setReturnGetVigFuturas, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                    } else
                        alert('No puede asociar una vigencia futura, las vigencias fueron modificadas y no han sido actualizadas. Guarde cambios por favor.');
                }
                else
                    alert('La Vigencia Incial y/o Vigencia Final No estan asociadas.');
            }
            
            function setReturnGetVigFuturas() {
                prePostbck(false);
                __doPostBack('ObtenerVigenciasFuturas', '');
            }


       <%--     function GetSuperInterv(idSupervisorActual, idRol) {

                muestraImagenLoading();
                var contrato = document.getElementById('<%= hfIdContrato.ClientID %>');
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaRoles.aspx?idContrato=' + contrato.value +'&idSupervisor=' + idSupervisorActual +
                                                                                                                    '&idRol=' + idRol, setReturnGetSupervisor, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetSupervisor() {
                prePostbck(false);
                __doPostBack('ObtenerSupervisorInterventor', '');
            }--%>


            function GetNumeroProceso() {
                muestraImagenLoading();
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaNumeroProceso.aspx', setReturnGetNumeroProceso, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }

            function setReturnGetNumeroProceso(dialog) {
                var pObj = window_returnModalDialog(dialog);
                if (pObj != undefined && pObj != null) {

                    var retLupa = pObj.split(",");
                    if (retLupa.length > 1) {
                        document.getElementById('<%= hfIdNumeroProceso.ClientID %>').value = retLupa[0];
                        document.getElementById('<%= txtFechaProceso.ClientID %>').value = retLupa[2];
                        document.getElementById('<%= txtNumeroProceso.ClientID %>').value = retLupa[1];

                        prePostbck(false);
                        <%--                        __doPostBack('ObtenerNumeroProceso');--%>
                    } else {
                        ocultaImagenLoading();
                    }
                } else {
                    ocultaImagenLoading();
                }
            }


                function ValidaContratistas(source, args) {
                    var hfContratistas = document.getElementById('<%= hfContratistas.ClientID %>');
                    if (parseInt(hfContratistas.value) > 0) {
                        args.IsValid = true;
                    } else {
                        args.IsValid = false;
                    }
                }

                function GetContratista() {
                    muestraImagenLoading();
                    var contrato = document.getElementById('<%= hfIdContrato.ClientID %>');
                    window_showModalDialog('../../../Page/Contratos/ContratistaProveedor/LupaProveedores.aspx?idContrato='+contrato.value, setReturnGetContratista, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
                }

                function setReturnGetContratista() {
                    prePostbck(false);
                    __doPostBack('ObtenerContratistas', '');
                }

        function GetCodigosSECOP() {
        muestraImagenLoading();
        var idContrato = document.getElementById('<%= hfIdContrato.ClientID %>');
        window_showModalDialog('../../../Page/Contratos/Lupas/LupaCodigoSecop.aspx?idContrato='+idContrato.value, setReturnGetCodigosSECOP, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');         
        }

        function setReturnGetCodigosSECOP() {
            prePostbck(false);
            __doPostBack('GetCodigosSECOP', '');
        }

       function ValidaCodigoSECOP(source, args) {
            var hfCodigoSECOP = document.getElementById('<%= hfCodigoSECOP.ClientID %>');              
            if (parseInt(hfCodigoSECOP.value) > 0) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
       }


            function GetGarantias(idGarantia, numeroGarantia, fechaGarantia) {
                muestraImagenLoading();
                var fechaInicioEjecucion = '<%= caFechaInicioEjecucion.Date.ToShortDateString() %>'; 
                window_showModalDialog('../../../Page/Contratos/Lupas/LupaGarantia.aspx?idGarantia=' + idGarantia + '&numeroGarantia=' + numeroGarantia + '&fechaGarantia=' + fechaGarantia + '&fechaInicio=' + fechaInicioEjecucion, setReturnGetGarantias, 'dialogWidth:500px;dialogHeight:300px;resizable:no;');
            }

            function setReturnGetGarantias() {
                prePostbck(false);
                __doPostBack('GetGarantias', '');
            }

        </script>

            <Ajax:Accordion ID="AccSolicitudContrato" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
            ContentCssClass="accordionContent" OnItemCommand="AccSolicitudContrato_ItemCommand"  runat="server"
            Width="98%" Height="100%">
                <Panes>
                 <Ajax:AccordionPane ID="ApDatosGeneralContrato" runat="server">
                         <Header>
                             Datos Generales
                        </Header>
                        <Content>
                              <table width="100%" id="pnInfoSolicitud" runat="server" align="center" style="display:none">
                                <tr class="rowB">
                                    <td>
                                        Número de Contrato
                                    </td>
                                    <td>
                                        Fecha de Registro en el Sistema                                                 
                                    </td>
                                </tr>
                                <tr class="rowA">
                                       <td class="Cell">
                                            <asp:TextBox ID="txtNumeroContrato" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtFechaRegistroSsistema" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                              </table>
                              <table width="100%" id="pnInfoCategoriaContrato" align="center">
                                <tr class="rowB">
                                    <td class="Cell">
                                        Id Contrato
                                    </td>
                                    <td class="Cell">
                                            Modalidad de Selección *
                                            <asp:CompareValidator runat="server" ID="cvModalidadSeleccion" ControlToValidate="ddlModalidadSeleccion"
                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                            ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                <td class="Cell">
                                         <asp:TextBox ID="txtIdContrato" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                    </td>
                                    <td class="Cell">
                                        <asp:DropDownList ID="ddlModalidadSeleccion" OnSelectedIndexChanged="ddlModalidadSeleccion_SelectedIndexChanged" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                    Categoría Contrato/Convenio *
                                    <asp:CompareValidator runat="server" ID="cvCategoriaContrato" ControlToValidate="ddlCategoriaContrato"
                                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>                                                 
                                </td>
                                    <td class="Cell">
                                        Tipo de Contrato/Convenio *
                                        <asp:CompareValidator runat="server" ID="cvTipoContratoConvenio" ControlToValidate="ddlTipoContratoConvenio"
                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr class="rowA">
                                        <td class="Cell">
                                        <asp:DropDownList ID="ddlCategoriaContrato" runat="server" onchange="prePostbck(true)"
                                        OnSelectedIndexChanged="ddlCategoriaContrato_SelectedIndexChanged"   AutoPostBack="true">
                                        </asp:DropDownList>
                                        </td>
                                        <td class="Cell">
                                        <asp:DropDownList ID="ddlTipoContratoConvenio" runat="server"  onchange="prePostbck(true)"
                                            OnSelectedIndexChanged="ddlTipoContratoConvenio_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="rowB">
                                    <td class="Cell">
                                            Régimen de Contratación *
                                            <asp:CompareValidator runat="server" ID="cvRegimenContratacion" ControlToValidate="ddlRegimenContratacion"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                                Enabled="false" ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>                                            
                                </td>
                                    <td class="Cell">

                                    </td>
                                </tr>
                                <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlRegimenContratacion" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Cell">

                                    </td>
                                </tr>
                               </table>
                              <table  width="100%" id="PnPrestacionServicios"  runat="server" style="display:none" align="center">
                                  <tr class="rowB">
                                                        <td class="Cell">
                                                            Modalidad Académica *
                                                            <asp:CompareValidator runat="server" ID="cvModalidadAcademica" ControlToValidate="ddlModalidadAcademica"
                                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                                            ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic" ></asp:CompareValidator>
                                                        </td>
                                                        <td class="Cell">
                                                            Nombre de la Profesión *
                                                        <asp:CompareValidator runat="server" ID="cvNombreProfesion" ControlToValidate="ddlNombreProfesion"
                                                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                                                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                  <tr class="rowA" >
                                                        <td class="Cell">
                                                            <asp:DropDownList ID="ddlModalidadAcademica" AutoPostBack="true" OnSelectedIndexChanged="ddlModalidadAcademica_SelectedIndexChanged" runat="server" Enabled="true" />
                                                        </td>
                                                        <td class="Cell">
                                                            <asp:DropDownList ID="ddlNombreProfesion" runat="server" />
                                                        </td>
                                                    </tr>
                                </table>
                              <table width="100%" id="PnNumeroProceso" runat="server" style="display:none" align="center">
                                                   <tr class="rowB">
                                                        <td class="Cell">
                                                            Número de Proceso *
                                                            <asp:RequiredFieldValidator runat="server" ID="rfvNumeroProceso" ControlToValidate="txtNumeroProceso"
                                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnAprobar"
                                                            ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="Cell">
                                                            Fecha de  Proceso
                                                        </td>
                                                    </tr>
                                                   <tr class="rowA" >
                                                        <td class="Cell">
                                                            <asp:HiddenField ID="hfNumeroProceso" runat="server" />
                                                            <asp:HiddenField ID="hfIdNumeroProceso" runat="server" />
                                                            <asp:HiddenField ID="hfFechaAdjudicacionProceso" runat="server" />
                                                            <asp:TextBox ID="txtNumeroProceso" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                                            <asp:ImageButton ID="imgNumeroProceso" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                                OnClientClick="GetNumeroProceso(); return false;" Style="cursor: hand" ToolTip="Buscar" />
                                                        </td>
                                                        <td class="Cell">
                                                            <asp:TextBox ID="txtFechaProceso" runat="server" Enabled="false" Width="100px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                              </table>
                              <table width="100%" id="PnInfoTipoContrato" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Requiere Garantía *
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="rblRequiereGarantia"
                                            SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                            ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                        <td class="Cell">
                                            Requiere Acta de Inicio *
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                             <asp:RadioButtonList runat="server" ID="rblRequiereGarantia" Enabled="false" RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>  
                                        </td>
                                        <td class="Cell">
                                             <asp:RadioButtonList runat="server" ID="rblRequiereActaInicio"  Enabled="false"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Valor Inicial Contrato *
                                        </td>
                                        <td class="Cell">
                                            Valor Final Contrato
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox runat="server" ID="txtvalorInicial" Enabled="false"  MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox runat="server" ID="txtValorFinal" Enabled="false" MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Valor Inicial Aportes ICBF *
                                        </td>
                                        <td class="Cell">
                                            Maneja Vigencias Futuras
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                        <asp:TextBox runat="server" ID="txtValorInicialAportesICBF"  Enabled="false"
                                           MaxLength="128" Width="320px" Height="22px" onchange="ValidarFormato(this.value,'$');" ></asp:TextBox>
                                        <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtValorInicialAportesICBF" FilterType="Custom,Numbers" ValidChars="$.,"  />   
                                        </td>
                                        <td class="Cell">
                                            <asp:RadioButtonList runat="server" ID="rblManejaVigenciasFuturas" Enabled="false"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Maneja Aportes Especie ICBF *
                                        </td>
                                        <td class="Cell">
                                            Maneja Cofinanciación *
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td>
                                             <asp:RadioButtonList runat="server" ID="rblManejaAportesEspecie" Enabled="false"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>  
                                        </td>
                                        <td class="Cell">
                                             <asp:RadioButtonList runat="server" ID="rblManejaCofinanciacion" Enabled="false"  RepeatDirection="Horizontal">
                                                 <asp:ListItem Text="Si" Value="Si" />
                                                 <asp:ListItem Text="No" Value="No" />
                                             </asp:RadioButtonList>
                                        </td>
                                    </tr>

                                                                      <tr class="rowB">
                                        <td class="Cell">
                                            <label id="lblFechaInicioEjecucion" 
                                                   runat="server" 
                                                   title="Favor registre la posible fecha de suscripción del contrato,  ya que esta fecha es dinámica y una vez se aprueben las garantías o se registe la fecha de firma del acta de inicio se actualizara con la fecha correspondiente" >
                                                Fecha de Inicio de ejecución del Contrato/Convenio * 
                                            </label>
                                        </td>
                                        <td class="Cell">
                                            Plazo en Ejecucción (MM/dd)
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                                        <uc1:fecha ID="caFechaInicioEjecucion" runat="server" Enabled="false"  Requerid="true" />
                                        </td>
                                        <td class="Cell">
                                                        <asp:CheckBox runat="server" ID="chkCalculaFecha" Text=" "  Enabled="false" /> 
                                                        <asp:ImageButton ID="ImgBtnFechaFinal" runat="server" CssClass="bN"  ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetCalculoFechaFinal(); return false;" Style="cursor: hand; display:none" ToolTip="Buscar"/>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Fecha de Finalización del Contrato/Convenio *
                                        </td>
                                        <td colspan="2">
                                            Plazo  Inicial de Ejecución
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <uc1:fecha ID="caFechaFinalizacionInicial" runat="server" Enabled="false" Requerid="true" />                                            
                                        </td>
                                        <td>
                                            <table>

                                                                                    <tr class="rowB">
                                        <td>
                                            Días
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDiasPiEj" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Meses
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMesesPiEj" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Años
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAcnosPiEj" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>

                                            </table>

                                        </td>
                                    </tr>
                                   <tr class="rowB">
                                        <td>
                                            Fecha de Suscripción *
                                        </td>
                                        <td >
                                            <div id="DivLblFechaActaInicio" runat="server" style="display:none">
                                                <asp:Label ID="lblFechaActaInicio" Text="Fecha Acta Inicio" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                                <asp:TextBox ID="txtFechaSuscripcion" runat="server"></asp:TextBox>
                                                <asp:Image ID="imgFechaSolicitud" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                                <Ajax:CalendarExtender ID="CalendarExtenderFechaSuscripcion" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaSolicitud" TargetControlID="txtFechaSuscripcion">
                                                </Ajax:CalendarExtender>
                                                <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaSuscripcion">
                                                </Ajax:MaskedEditExtender> 
                                                <asp:RequiredFieldValidator ID="RvfFecha2" runat="server" ControlToValidate="txtFechaSuscripcion" 
                                                Display="Dynamic" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True"
                                                style="font-weight: 700" ValidationGroup="btnGuardar"></asp:RequiredFieldValidator>  
                                                <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaSuscripcion" runat="server" />             
                                        </td>
                                        <td>
                                            <div id="DivDateFechaActaInicio" runat="server" style="display:none">
                                               <asp:TextBox ID="txtFechaActaInicio" Enabled="false" runat="server"></asp:TextBox>
                                                <asp:Image ID="imgFechaActaInicio" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand" />
                                                <Ajax:CalendarExtender ID="CalendarExtenderFechaActaInicio" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgFechaActaInicio" TargetControlID="txtFechaActaInicio">
                                                </Ajax:CalendarExtender>
                                                <Ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaActaInicio">
                                                </Ajax:MaskedEditExtender> 
                                                <asp:CompareValidator ErrorMessage="Fecha no válida" ForeColor="Red" Type="Date" Operator="DataTypeCheck"  ControlToValidate="txtFechaActaInicio" runat="server" />
                                            </div>
                                        </td>
                                     </tr>

                                    <tr class="rowB">
                                        <td>
                                            Vigencia Fiscal Inicial del Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvVigenciaFiscalIni" ControlToValidate="ddlVigenciaFiscalIni"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar" 
                                                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                        <td colspan="2">
                                            Vigencia Fiscal Final del Contrato/Convenio *
                                            <asp:CompareValidator runat="server" ID="cvVigenciaFiscalFin" ControlToValidate="ddlVigenciaFiscalFin"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnAprobar"
                                                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:DropDownList ID="ddlVigenciaFiscalIni"   Enabled="false" runat="server"  />
                                        </td>
                                        <td class="Cell" colspan="2">
                                            <asp:DropDownList ID="ddlVigenciaFiscalFin" Enabled="false"  runat="server"  />
                                        </td>
                                    </tr>
                                </table>
                              <table width="100%"  id="pnInfoSolcitantes" runat="server" align="center" style="display:none">
                                    <tr class="rowB">
                                        <td>
                                            Nombre del Solicitante *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombreSolicitante" ControlToValidate="txtNombreSolicitante"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                                ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Regional del Contrato/Convenio *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvRegionalContConv" ControlToValidate="txtRegionalContratoConvenio"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                                                Enabled="false" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtNombreSolicitante" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled" ></asp:TextBox>
                                            <asp:ImageButton ID="imgNombreSolicitante" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetEmpleado('Sol'); return false;" Style="cursor: hand" ToolTip="Buscar"
                                                 />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtRegionalContratoConvenio" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Dependencia Solicitante
                                        </td>
                                        <td>
                                            Cargo el Solicitante
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtDependenciaSolicitante" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtCargoSolicitante" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Nombre del Ordenador del Gasto *
                                            <asp:RequiredFieldValidator runat="server" ID="rfvNombreOrdenadorGasto" ControlToValidate="txtNombreOrdenadorGasto"
                                                SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar" Enabled="false"
                                                ForeColor="Red" ></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Regional Ordenador del Gasto
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtNombreOrdenadorGasto" runat="server" Enabled="false" Width="80%"
                                                ViewStateMode="Enabled" ></asp:TextBox>
                                            <asp:ImageButton ID="imgNombreOrdenadorGasto" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                OnClientClick="GetEmpleado('OrdG'); return false;" Style="cursor: hand" ToolTip="Buscar" />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtRegionalOrdenadorGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Número Identificación Ordenador del Gasto
                                        </td>
                                        <td>
                                            Cargo Ordenador del Gasto
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtNumeroIdentOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtCargoOrdenadoGasto" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>

                              </table>
                        </Content>
                    </Ajax:AccordionPane>

                 <Ajax:AccordionPane ID="AccordionPanelAportes" runat="server">
                            <Header>
                                Detalle Valor Inicial del Contrato</Header>
                            <Content>
                                <table width="100%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Valor ICBF 
                                            <asp:CustomValidator ID="cvAportes" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaAportes"></asp:CustomValidator>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Valor Cofinanciaci&#243;n Contratista
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoICBF" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                               ></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoICBF" runat="server"  CssClass="bN"  ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetValorAportes('ICBF','Especie'); return false;" Style="cursor:inherit; display:none"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtValorApoContrat" runat="server" Enabled="false" Width="80%" ViewStateMode="Enabled"
                                               ></asp:TextBox>
                                            <asp:ImageButton ID="imgValorApoContrat" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetValorAportes('Contratista','Mixto'); return false;" Style="cursor: hand; display:none"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="3">
                                            Aportes ICBF
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvAportesICBF" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvAportesICBF_RowDataBound" DataKeyNames="IdAporteContrato,TipoAporte,TipoAportante"
                                                AllowSorting="true"  GridLines="None" Width="100%"
                                                CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Aportante" DataField="TipoAportante" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Tipo de Aporte" DataField="TipoAporte" SortExpression="TipoAporte" />
                                                    <asp:BoundField HeaderText="Valor Aporte" DataField="ValorAporte" SortExpression="ValorAporte"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Descripci&oacute;n Cofinanciaci&oacute;n Especie" DataField="DescripcionAporte"
                                                        SortExpression="DescripcionAporte" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarAporteClick"
                                                                OnClientClick="return ValidaEliminacion('Aportes');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>



                 <Ajax:AccordionPane ID="ApCDP" runat="server">
                            <Header>
                                CDP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbCDP" runat="server" Text="CDP"></asp:Label>
                                            <asp:CustomValidator ID="cvCDP" runat="server" ErrorMessage="Campo Requerido" Enabled="false"
                                                ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaCDP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCDP" runat="server" />
                                            <asp:TextBox ID="txtCDP" runat="server" Enabled="false" ViewStateMode="Enabled" />
                                            <asp:ImageButton ID="imgCDP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetCDP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvCDP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdContratosCDP,CodigoRubro,ValorCDP,ValorRubro"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarCDPClick" OnClientClick="return ValidaEliminacion('CDP');"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                    <asp:BoundField HeaderText="Fecha CDP" DataField="FechaCDP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor CDP" DataField="ValorCDP"  DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                     <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRubrosCDP" runat="server" AutoGenerateColumns="false" DataKeyNames=""
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                        <asp:BoundField HeaderText="Número CDP" DataField="NumeroCDP" />
                                                        <asp:BoundField HeaderText="Dependencia Afectación Gastos" DataField="DependenciaAfectacionGastos"/>
                                                        <asp:BoundField HeaderText="Tipo Fuente Financiamiento" DataField="TipoFuenteFinanciamiento"/>
                                                        <asp:BoundField HeaderText="Rubro Presupuestal" DataField="RubroPresupuestal" />
                                                        <asp:BoundField HeaderText="Recurso Presupuestal" DataField="RecursoPresupuestal" />
                                                        <asp:BoundField HeaderText="Tipo Situación Fondos" DataField="TipoSituacionFondos"/>
                                                        <asp:BoundField HeaderText="Valor Rubro" DataField="ValorRubro"  DataFormatString="{0:C}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                 <Ajax:AccordionPane ID="ApRP" runat="server">
                            <Header>
                                RP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="LabelRP" runat="server" Text="RP"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfRP" runat="server" />
                                            <asp:TextBox ID="txtValorRP" runat="server" Enabled="false" ViewStateMode="Enabled" />
                                            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetRP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvRP" runat="server" AutoGenerateColumns="false" DataKeyNames="IdRPContrato"
                                                AllowPaging="True"  GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarRPClick"  OnClientClick="return ValidaEliminacion('RP');"
                                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Regional ICBF" DataField="NombreRegional" />
                                                    <asp:BoundField HeaderText="Número RP" DataField="NumeroRP" />
                                                    <asp:BoundField HeaderText="Fecha RP" DataField="FechaRP" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor Inicial" DataField="ValorInicialRP" DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Valor Actual" DataField="ValorActualRP"  DataFormatString="{0:c}" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                   <Ajax:AccordionPane ID="ApSupervisores" runat="server">
     <Header> Supervisor </Header>
            <Content>
                       <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Supervisor y/o Interventor relacionados *
                                            <asp:CustomValidator ID="cvSupervInterv" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaSupervInterv"></asp:CustomValidator>
                                        </td>                                        
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfSupervInterv" runat="server" />
                                            <asp:TextBox ID="txtSuperInterv" runat="server" Enabled="false" ViewStateMode="Enabled"></asp:TextBox>
                                            <asp:ImageButton ID="imgSuperInterv" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetSuperInterv(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>                                       
                                    </tr>                                   
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Supervisor y/o Interventor relacionados
                                        </td>                                        
                                    </tr>                                    
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <div id="dvSupervInterv" runat="server">
                                                <asp:GridView ID="gvSupervInterv"  OnRowDataBound="gvSupervInterv_RowDataBound"  runat="server" AutoGenerateColumns="false" DataKeyNames="IDSupervisorIntervContrato,FechaInicio"
                                                     GridLines="None" CellPadding="8"
                                                    Height="16px">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Supervisor y/o Interventor" DataField="EtQSupervisorInterventor" />
                                                        <asp:BoundField HeaderText="Tipo Supervisor y/o Interventor" DataField="EtQInternoExterno" />
                                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                <asp:TextBox ID="txtFechaInicio" runat="server" Visible="false" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'
                                                                    Width="73px" MaxLength="10"></asp:TextBox>
                                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio"
                                                                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                                                                    SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar" Display="Dynamic"></asp:CompareValidator>
                                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM"
                                                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                                                                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                                                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                                </Ajax:MaskedEditExtender>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Nombres" DataField="NombreCompletoSuperInterventor" />
                                                        <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                        <asp:BoundField HeaderText="Número Identificación" DataField="Identificacion" />
                                                        <asp:BoundField HeaderText="Rol Supervisor" DataField="NombreRol" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarGvSupervIntervClick"
                                                                    OnClientClick="return ValidaEliminacion('Supervisor');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Director Interventoria
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td colspan="3">
                                            <asp:GridView ID="gvDirectoresInterv" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="80%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="Identificacion" />
                                                    <asp:BoundField HeaderText="Información Director Interventoría" DataField="SupervisorInterventor.NombreCompleto" />
                                                    <asp:BoundField HeaderText="Teléfono" DataField="SupervisorInterventor.Telefono" />
                                                    <asp:BoundField HeaderText="Celular" DataField="SupervisorInterventor.Celular" />
                                                    <asp:BoundField HeaderText="Correo Electrónico" DataField="SupervisorInterventor.CorreoElectronico" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
            </Content>
     </Ajax:AccordionPane>
                   <Ajax:AccordionPane ID="ApVigenciasFuturas" Visible="false" runat="server">
                            <Header>
                                Vigencias Futuras</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="lbVigenciasFuturas" runat="server" Text="Vigencias Futuras"></asp:Label>
                                            <asp:CustomValidator ID="cvVigenciasFuturas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="False" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaVigenciasFuturas"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                            <asp:TextBox ID="txtVigFuturas" runat="server" Enabled="false" ViewStateMode="Enabled" />
                                            <asp:ImageButton ID="imgVigFuturas" runat="server"  CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetVigFuturas(); return false;" Style="cursor: hand; display:none"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvVigFuturas" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="IDVigenciaFuturas" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Número del Radicado Vigencia Futura" DataField="NumeroRadicado" />
                                                    <asp:BoundField HeaderText="Fecha de Expedición Vigencia Futura" DataField="FechaExpedicion"
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField HeaderText="Valor Vigencia Futura" DataField="ValorVigenciaFutura"
                                                        DataFormatString="{0:c}" />
                                                    <asp:BoundField HeaderText="Año Vigencia Futura" DataField="AnioVigencia" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarGvVigFuturasClick"
                                                                OnClientClick="return ValidaEliminacion('VigenciasFuturas');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                   <Ajax:AccordionPane ID="AccordionPanelLugarEjecucion" runat="server">
                            <Header>
                                Lugar de ejecución</Header>
                            <Content>
                                <table width="100%" align="center">
                                    <tr class="rowB">
                                        <td>
                                            Lugar de ejecución *
                                            <asp:CustomValidator ID="cvLugarEjecucion" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="false" ForeColor="Red" ValidationGroup="btnAprobar" ClientValidationFunction="ValidaLugarEjecucion"></asp:CustomValidator>
                                        </td>
                                        <td>
                                            Datos Adicionales lugar de ejecución
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:TextBox ID="txtLugarEjecucion" runat="server" Enabled="false" ViewStateMode="Enabled"></asp:TextBox>
                                            <asp:ImageButton ID="imgLugarEjecucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetLugarEjecucion('LugarEjeccucion'); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                        <td class="Cell">
                                            <asp:TextBox ID="txtDatosAdicionalesLugarEjecucion" runat="server" Width="80%" TextMode="MultiLine" Enabled="false" ></asp:TextBox>
                                            <Ajax:FilteredTextBoxExtender ID="ftDatosAdicionalesLugarEjecucion" runat="server"
                                                TargetControlID="txtDatosAdicionalesLugarEjecucion" FilterType="Custom,LowercaseLetters,UppercaseLetters"
                                                ValidChars="áéíóúÁÉÍÓÚñÑ -" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkNivelNacional" runat="server" Enabled="false" AutoPostBack="true"
                                                OnCheckedChanged="chkNivelNacionalCheckedChanged" />
                                            Nivel Nacional
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell" colspan="2">
                                            <div id="dvLugaresEjecucion" runat="server">
                                                <asp:GridView ID="gvLugaresEjecucion" runat="server" DataKeyNames="IdLugarEjecucionContratos"
                                                    AutoGenerateColumns="false" GridLines="None" Width="98%" CellPadding="8" Height="16px">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Departamento" DataField="Departamento" />
                                                        <asp:BoundField HeaderText="Municipio" DataField="Municipio" />
                                                        <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminarLugEjecucionClick"
                                                                    OnClientClick="return ValidaEliminacion('LugarEjecuccion');" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                    <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="rowBG" />
                                                    <EmptyDataRowStyle CssClass="headerForm" />
                                                    <HeaderStyle CssClass="headerForm" />
                                                    <RowStyle CssClass="rowAG" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                 </Ajax:AccordionPane>
                   <Ajax:AccordionPane ID="ApContratista" runat="server">
                            <Header>
                                Contratista</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Contratista *
                                            <asp:CustomValidator ID="cvContratistas" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnGuardar" ClientValidationFunction="ValidaContratistas"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfContratistas" runat="server" />
                                            <asp:ImageButton ID="imgContratista" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                 OnClientClick="GetContratista(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td>
                                            Contratistas relacionados
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvContratistas" runat="server" AutoGenerateColumns="false" DataKeyNames="IdProveedoresContratos,IdTercero"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Tipo Identificación Representante Legal" DataField="TipoDocumentoRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Número Identificación Representante Legal" DataField="NumeroIDRepresentanteLegal" />
                                                    <asp:BoundField HeaderText="Representante" DataField="RepresentanteLegal" />
                                                    <asp:TemplateField HeaderText="Opciones" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                OnClientClick="return ValidaEliminacion('Contratista');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
<%--                                                            <asp:LinkButton ID="btnDetalle" runat="server" OnClick="btnOpcionGvContratistasClick"
                                                                CommandName="Detalle" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Detalle" src="../../../Image/btn/ico_Pesos.jpg" title="Medio Pago" />
                                                            </asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr class="rowB">
                                        <td class="Cell">
                                            Integrantes relacionados al consorcio o unión temporal
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:GridView ID="gvIntegrantesConsorcio_UnionTemp" runat="server" AutoGenerateColumns="false"
                                                GridLines="None" Width="100%" CellPadding="8" Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Identificación Constratista" DataField="NumeroIdentificacion" />
                                                    <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" />
                                                    <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" />
                                                    <asp:BoundField HeaderText="Número Identificación" DataField="NumeroIdentificacionIntegrante" />
                                                    <asp:BoundField HeaderText="Información Integrante" DataField="Proveedor" />
                                                    <asp:BoundField HeaderText="Porcentaje Participación" DataField="PorcentajeParticipacion" />
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                           <Ajax:AccordionPane ID="ApCodigosSECOP" runat="server">
                            <Header>
                               Código SECOP</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                            <asp:Label ID="Label1" runat="server"  Text="Códigos SECOP"></asp:Label>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Campo Requerido"
                                                Enabled="true" ForeColor="Red" ValidationGroup="btnGuardar" ClientValidationFunction="ValidaCodigoSECOP"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr class="rowA">
                                        <td class="Cell">
                                            <asp:HiddenField ID="hfCodigoSECOP" runat="server" Value="0" />
                                            <asp:TextBox ID="txtCodigosSECOP" runat="server" Enabled="false" ViewStateMode="Enabled"
                                                  AutoPostBack="true"></asp:TextBox>
                                            <asp:ImageButton ID="imgbtnCodigosSECOP" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                                                Enabled="false" OnClientClick="GetCodigosSECOP(); return false;" Style="cursor: hand"
                                                ToolTip="Buscar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvCodigosSECOP" runat="server" AutoGenerateColumns="false" CssClass="grillaCentral"
                                                DataKeyNames="key" GridLines="None" Width="100%" CellPadding="8"
                                                Height="16px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Código SECOP" DataField="Value" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEliminarCodigoSECOP" runat="server" OnClick="btnEliminarCodigoSECOP_Click"
                                                                OnClientClick="return ValidaEliminacion('CodigoSECOP');" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">
                                                                <img alt="Eliminar" src="../../../Image/btn/delete.gif" title="Eliminar" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <RowStyle CssClass="rowAG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>
                         <Ajax:AccordionPane ID="ApGarantias" runat="server">
                            <Header>
                               Garantías</Header>
                            <Content>
                                <table width="90%" align="center">
                                    <tr class="rowB">
                                        <td class="Cell">
                                 <asp:GridView ID="GridGarantias" runat="server" OnRowDataBound="GridGarantias_RowDataBound" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IDGarantia,NumeroGarantia,FechaAprobacionGarantia" GridLines="None" Height="16px" Width="100%">
                        <Columns>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditar"  runat="server" AutoPostBack="false"  >
                                    <img alt="Editar" src="../../../Image/btn/edit.gif"   title="Editar" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:BoundField DataField="NumeroGarantia" HeaderText="Número de Garantía" />
                            <asp:BoundField DataField="NombreTipoGarantia" HeaderText="Tipo de Garantía" />
                            <asp:BoundField DataField="FechaAprobacionGarantiaView" HeaderText="Fecha Aprobación" />
                            <asp:TemplateField HeaderText="Valor Asegurado" ItemStyle-HorizontalAlign="Center" SortExpression="ValorAsegurado">
                                 <ItemTemplate>
                                     <div style="word-wrap: break-word; width: 150px;">
                                         <%# Convert.ToDecimal(Eval("ValorGarantia")).ToString("$ #,###0.00##;($ #,###0.00##)")%>
                                     </div>
                                 </ItemTemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionEstadoGarantia" HeaderText="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </Ajax:AccordionPane>

                </Panes>
             </Ajax:Accordion>
    
                <asp:HiddenField ID="hfIdContrato" runat="server" />
                <asp:HiddenField ID="hfIdRegionalUsuarioCreacion" runat="server" />
                <asp:HiddenField ID="hfEsPrestacionServicios" runat="server" />
                <asp:HiddenField ID="hfRegionalContConv" runat="server" />
                <asp:HiddenField ID="hfCodRegionalContConv" runat="server" />
                <asp:HiddenField ID="hfPlanCompras" runat="server" />
                <asp:HiddenField ID="hfAportes" runat="server" />
                <asp:HiddenField ID="hfCofinanciacion" runat="server" />
                <asp:HiddenField ID="hfVigenciasFuturas" runat="server" />
                <asp:HiddenField ID="hfLugarEjecucion" runat="server" />

                <asp:HiddenField ID="hfEsCambioVigencia" runat="server" />
                <asp:HiddenField ID="hfVigenciaInicial" runat="server" />
                <asp:HiddenField ID="hfVigenciaFinal" runat="server" />
                <asp:HiddenField ID="hfEsCambioFecha" runat="server" />
                <asp:HiddenField ID="hfEstadoContrato" runat="server" />
                <asp:HiddenField ID="hfCodigoModalidad" runat="server" />
                <asp:HiddenField ID="hfValorFechaActaIncion" runat="server" />
                <asp:HiddenField ID="hfTieneCamBioSupervisor" runat="server" />
</asp:Content>
