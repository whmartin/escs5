﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;
using Icbf.Contrato.Entity.PreContractual;
using System.Text;

/// <summary>
/// Página que despliega el detalle del registro de una solicitud de contrato
/// </summary>
public partial class Page_Contratos_ContratosModificacion_Add : GeneralWeb
{
    #region Constantes

    masterPrincipal toolBar;

    string PageName = "Contratos/ContratosModificacion";


    //Contrato Asociado
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    //Categoria contrato
    private const string CodCategoriaContrato = "CTR"; //"Contrato";
    private const string CodCategoriaConvenio = "CVN"; //"Convenio";

    //Tipos contrato
    private const string CotTipoContConvPrestServApoyoGestion = "PREST_SERV_APO_GEST"; //"Prestación de Servicios de Apoyo a la gestión";
    private const string CodTipoContConvPrestServProfesionales = "PREST_SERV_PROF"; //"Prestación de Servicios Profesionales";
    private const string CodTipoContAporte = "APORTE"; //"Aporte";
    private const string CodMarcoInteradministrativo = "MC_INT_ADMIN"; //"Marco Interadministrativo";

    //Modalidad selección
    private const string CodContratacionDirecta = "CONT_DIR"; //"Contratación Directa";
    private const string CodContratacionDirectaAporte = "CDA"; //"Contratación Directa Aporte";
    #endregion

    #region Fachadas

    ContratoService vContratoService = new ContratoService();

    PreContractualService vPrecontractualService = new PreContractualService();

    SIAService vSIAService = new SIAService();

    #endregion

    #region Negocio y Acceso a Datos

    /// <summary>
    /// 
    /// </summary>
    private void Guardar()
    {
        try
        {
            int result = 0;

            var resultVal = ValidarActualizaciones();

            if(resultVal.Length == 0)
            {
                ContratoModificacionEditar modContrato = new ContratoModificacionEditar();
                modContrato.IdContrato = int.Parse(hfIdContrato.Value);
                modContrato.IdModalidadSeleccion = int.Parse(ddlModalidadSeleccion.SelectedValue);
                modContrato.IdCategoriaContrato = int.Parse(ddlCategoriaContrato.SelectedValue);
                modContrato.IdTipoContrato = int.Parse(ddlTipoContratoConvenio.SelectedValue);
                modContrato.ValorAporteDinero = decimal.Parse(txtValorInicialAportesICBF.Text.Replace("$", string.Empty));
                modContrato.CodigoEstadoContrato = hfEstadoContrato.Value.ToString().Trim().TrimEnd();
                modContrato.FechaSuscripcion = DateTime.Parse(txtFechaSuscripcion.Text);
                modContrato.RequiereGarantia = rblRequiereGarantia.SelectedValue == "Si" ? true : false;
                modContrato.RequiereActaInicio = rblRequiereActaInicio.SelectedValue == "Si" ? true : false;
                modContrato.EsFechaCalculada = chkCalculaFecha.Checked;
                modContrato.FechaFinalEjecuccion = caFechaFinalizacionInicial.Date;
                modContrato.FechaInicioEjecuccion = caFechaInicioEjecucion.Date;
                modContrato.IdVigenciaInicial = int.Parse(ddlVigenciaFiscalIni.SelectedValue);
                modContrato.IdVigenciaFinal = int.Parse(ddlVigenciaFiscalFin.SelectedValue);
                modContrato.ManejaCofinanciacion = rblManejaCofinanciacion.SelectedValue == "Si" ? true : false;
                modContrato.ManejaRecursosEspecieICBF = rblManejaAportesEspecie.SelectedValue == "Si" ? true : false;
                modContrato.ManejaVigenciasFuturas = rblManejaVigenciasFuturas.SelectedValue == "Si" ? true : false;
                modContrato.IdRegimenContratacion = int.Parse(ddlRegimenContratacion.SelectedValue);

                if(modContrato.EsFechaCalculada)
                {
                    modContrato.DiasCalculados = int.Parse(txtDiasPiEj.Text);
                    modContrato.MesesCalculados = int.Parse(txtMesesPiEj.Text);
                }

                if(hfEsPrestacionServicios.Value == "1")
                {
                    modContrato.IdModalidadAcademica = ddlModalidadAcademica.SelectedValue;
                    modContrato.IdProfesion = ddlNombreProfesion.SelectedValue;
                }

                if(!string.IsNullOrEmpty(hfIdNumeroProceso.Value))
                    modContrato.IdNumeroProceso = int.Parse(hfIdNumeroProceso.Value);

                if(!string.IsNullOrEmpty(txtFechaProceso.Text))
                    modContrato.FechaProceso = DateTime.Parse(txtFechaProceso.Text);

                if(rblRequiereActaInicio.SelectedValue == "Si" && !string.IsNullOrEmpty(txtFechaActaInicio.Text))
                {
                    DateTime fechaActaInicio;
                    if(DateTime.TryParse(txtFechaActaInicio.Text, out fechaActaInicio))
                        modContrato.FechaActaInicio = fechaActaInicio;
                }

                modContrato.UsuarioModifica = GetSessionUser().NombreUsuario;

                result = vContratoService.ModificarContratosModificacion(modContrato);

                if(result > 0)
                {
                    SetSessionParameter("ModificacionContrato.Modifico", string.Format("Se realizo la actualización del contrato No: {0}", txtNumeroContrato.Text));
                    NavigateTo(SolutionPage.List);
                }
            }
            else
                toolBar.MostrarMensajeError(resultVal.Replace("\n", "<br/>"));
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            if(Request.QueryString["oP"] == "E")
                toolBar.SetSaveConfirmation("return Confirmar();");
            toolBar.EstablecerTitulos("Registro de Solicitudes de Contrato", SolutionPage.Add.ToString());
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarCategoriaContrato();
            CargarTipodeContrato();
            CargarModalidadSeleccion();
            CargarRegimenContratacion();
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalIni, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalFin, vSIAService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
            caFechaInicioEjecucion.ToolTip = "Favor registre la posible fecha de suscripción del contrato,  ya que esta fecha es dinámica y una vez se aprueben las garantías o se registe la fecha de firma del acta de inicio se actualizara con la fecha correspondiente";
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            pnInfoSolicitud.Style.Add("display", "");
            pnInfoSolcitantes.Style.Add("display", "");

            AccordionPanelAportes.Visible = true;
            string idContrato = GetSessionParameter("ModificacionContrato.IdContrato").ToString();

            ContratoModificacionDetalle itemSolicitud = vContratoService.ConsultarContratosModificacionPorId(int.Parse(idContrato));
            hfIdContrato.Value = itemSolicitud.IdContrato.ToString();
            txtFechaRegistroSsistema.Text = itemSolicitud.FechaCrea.ToShortDateString();
            txtNumeroContrato.Text = itemSolicitud.NumeroContrato;
            txtIdContrato.Text = itemSolicitud.IdContrato.ToString();
            ddlModalidadSeleccion.SelectedValue = itemSolicitud.IdModalidadSeleccion.ToString();
            ddlCategoriaContrato.SelectedValue = itemSolicitud.IdCategoriaContrato.ToString();
            hfRegionalContConv.Value = itemSolicitud.IdRegional.ToString();
            CargarTipodeContrato();
            ddlTipoContratoConvenio.SelectedValue = itemSolicitud.IdTipoContrato.ToString();

            ddlRegimenContratacion.SelectedValue = itemSolicitud.IdRegimenContratacion.ToString();

            if(itemSolicitud.CodigoTipoContrato == CotTipoContConvPrestServApoyoGestion || itemSolicitud.CodigoTipoContrato == CodTipoContConvPrestServProfesionales)
            {
                ActualizarTipoContratoConvenio(true);

                cargarModalidadAcademica();

                if(!string.IsNullOrEmpty(itemSolicitud.IdModalidadAcademica))
                {
                    ddlModalidadAcademica.SelectedValue = itemSolicitud.IdModalidadAcademica.ToString();
                    cargarProfesion();
                }

                if(!string.IsNullOrEmpty(itemSolicitud.IdProfesion))
                    ddlNombreProfesion.SelectedValue = itemSolicitud.IdProfesion.ToString();
            }




            txtNumeroProceso.Text = itemSolicitud.NumeroProceso;

            if(itemSolicitud.IdNumeroProceso.HasValue)
                hfIdNumeroProceso.Value = itemSolicitud.IdNumeroProceso.Value.ToString();

            if(itemSolicitud.FechaProceso.HasValue)
                txtFechaProceso.Text = itemSolicitud.FechaProceso.Value.ToShortDateString();

            if(itemSolicitud.ManejaCofinanciacion)
                imgValorApoContrat.Style.Add("display", "");

            if(itemSolicitud.ManejaRecursosEspecieICBF)
                imgValorApoICBF.Style.Add("display", "");

            if(itemSolicitud.ManejaVigenciasFuturas)
                imgVigFuturas.Style.Add("display", "");

            if(itemSolicitud.RequiereActaInicio)
            {
                DivDateFechaActaInicio.Style.Add("display", "");
                DivLblFechaActaInicio.Style.Add("display", "");
                CalendarExtenderFechaActaInicio.StartDate = itemSolicitud.FechaInicioContrato;
                CalendarExtenderFechaActaInicio.EndDate = DateTime.Now;

                if(itemSolicitud.FechaActaInicio.HasValue)
                {
                    txtFechaActaInicio.Text = itemSolicitud.FechaActaInicio.Value.ToShortDateString();
                }
            }

            hfTieneCamBioSupervisor.Value = itemSolicitud.TieneCambioSupervisor.ToString();

            if(itemSolicitud.TieneCambioSupervisor > 0)
                imgSuperInterv.Enabled = false;
            else
                imgSuperInterv.Enabled = true;

            if(itemSolicitud.ValorContrato.HasValue)
                txtvalorInicial.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContrato);

            if(itemSolicitud.ValorContratoFinal.HasValue)
                txtValorFinal.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContratoFinal);

            if(itemSolicitud.ValorAporteDinero.HasValue)
                txtValorInicialAportesICBF.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorAporteDinero);

            rblRequiereActaInicio.SelectedValue = itemSolicitud.RequiereActaInicio == true ? "Si" : "No";
            rblRequiereGarantia.SelectedValue = itemSolicitud.RequiereGarantia == true ? "Si" : "No";
            rblManejaCofinanciacion.SelectedValue = itemSolicitud.ManejaCofinanciacion == true ? "Si" : "No";
            rblManejaAportesEspecie.SelectedValue = itemSolicitud.ManejaRecursosEspecieICBF == true ? "Si" : "No";
            rblManejaVigenciasFuturas.SelectedValue = itemSolicitud.ManejaVigenciasFuturas == true ? "Si" : "No";

            if(itemSolicitud.FechaInicioContrato.HasValue)
                caFechaInicioEjecucion.Date = itemSolicitud.FechaInicioContrato.Value;

            if(itemSolicitud.FechaFinalizaContrato.HasValue)
                caFechaFinalizacionInicial.Date = itemSolicitud.FechaFinalizaContrato.Value;

            if(itemSolicitud.EsFechaFinalCalculada.HasValue)
                chkCalculaFecha.Checked = itemSolicitud.EsFechaFinalCalculada.Value;

            CalendarExtenderFechaSuscripcion.EndDate = caFechaInicioEjecucion.Date;

            if(itemSolicitud.FechaSuscripcionContrato.HasValue)
                txtFechaSuscripcion.Text = itemSolicitud.FechaSuscripcionContrato.Value.ToShortDateString();

            string diferenciaFechas = string.Empty;
            ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion.Date, caFechaFinalizacionInicial.Date, out diferenciaFechas);

            var items = diferenciaFechas.Split('|');

            txtDiasPiEj.Text = items[2];
            txtMesesPiEj.Text = items[1];
            txtAcnosPiEj.Text = items[0];

            ddlVigenciaFiscalIni.SelectedValue = itemSolicitud.IdVigenciaInicial.ToString();
            hfVigenciaInicial.Value = itemSolicitud.IdVigenciaInicial.ToString();
            ddlVigenciaFiscalFin.SelectedValue = itemSolicitud.IdVigenciaFinal.ToString();
            hfVigenciaFinal.Value = itemSolicitud.IdVigenciaFinal.ToString();

            chkCalculaFecha.Checked = itemSolicitud.EsFechaFinalCalculada.HasValue ? itemSolicitud.EsFechaFinalCalculada.Value : false;

            if(itemSolicitud.EsFechaFinalCalculada.HasValue && itemSolicitud.EsFechaFinalCalculada.Value)
                caFechaFinalizacionInicial.Enabled = false;
            else
                caFechaFinalizacionInicial.Enabled = true;

            if(itemSolicitud.IdPlanCompras.HasValue)
                hfPlanCompras.Value = itemSolicitud.IdPlanCompras.Value.ToString();

            gvAportesICBF.PageSize = PageSize();
            gvAportesICBF.EmptyDataText = EmptyDataText();

            hfEstadoContrato.Value = itemSolicitud.CodigoEstadoContrato.ToString();

            CargarAportes();
            CargarRP();
            CargarCDP();
            CargarNumeroProceso();
            CargarVigenciasFuturas();
            CargarLugarEjecucion();
            CargarContratistas();
            CargarSupervisores();
            CargarInfoEmpleados(itemSolicitud);
            CargarCodigosSECOP();
            CargarGarantias();

            ValidarPosiblesModificaciones(itemSolicitud);
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void ValidarPosiblesModificaciones(ContratoModificacionDetalle item)
    {
        if(item.TieneCesiones > 0)
            ApContratista.Visible = false;

        if(item.CodigoEstadoContrato.Contains("CRP"))
        {
            rblRequiereGarantia.Enabled = true;
            rblRequiereActaInicio.Enabled = true;
            txtFechaActaInicio.Enabled = true;
            HabilitarFechas(true, item.EsFechaFinalCalculada);
            rblManejaAportesEspecie.Enabled = true;
            rblManejaCofinanciacion.Enabled = true;
            ApVigenciasFuturas.Visible = true;
            rblManejaVigenciasFuturas.Enabled = true;
        }
        else if(item.CodigoEstadoContrato.Contains("SUS"))
        {
            rblRequiereGarantia.Enabled = true;
            rblRequiereActaInicio.Enabled = true;
            txtFechaActaInicio.Enabled = true;
            txtValorInicialAportesICBF.Enabled = true;
            HabilitarFechas(true, item.EsFechaFinalCalculada);
            rblManejaCofinanciacion.Enabled = true;
            rblManejaAportesEspecie.Enabled = true;
            ApRP.Visible = false;
            ApVigenciasFuturas.Visible = true;
            rblManejaVigenciasFuturas.Enabled = true;
        }
        else if
            (
             item.CodigoEstadoContrato.Contains("EJE") ||
             item.CodigoEstadoContrato.Contains("TER") ||
             item.CodigoEstadoContrato.Contains("LIQB") ||
             item.CodigoEstadoContrato.Contains("LIQU") ||
             item.CodigoEstadoContrato.Contains("ANTB") ||
             item.CodigoEstadoContrato.Contains("ANTU")
            )
        {
            if(item.CodigoEstadoContrato.Contains("EJE"))
            {
                rblRequiereActaInicio.Enabled = true;
                txtFechaActaInicio.Enabled = true;
            }

            if(item.TieneProrrogas == 0)
                HabilitarFechas(true, item.EsFechaFinalCalculada);

            if(item.TieneAdiciones == 0 && item.TieneReducciones == 0)
            {
                rblManejaCofinanciacion.Enabled = true;
                rblManejaAportesEspecie.Enabled = true;
            }
            else
            {
                imgValorApoContrat.Style.Add("display", "none");
                imgValorApoICBF.Style.Add("display", "none");
            }

            ApRP.Visible = true;
            ApVigenciasFuturas.Visible = true;
            rblManejaVigenciasFuturas.Enabled = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="valid"></param>
    private void HabilitarFechas(bool valid, bool? calculaFecha)
    {
        caFechaInicioEjecucion.Enabled = valid;
        chkCalculaFecha.Enabled = valid;

        if(calculaFecha.HasValue && !calculaFecha.Value)
        {
            caFechaFinalizacionInicial.Enabled = valid;
            ImgBtnFechaFinal.Style.Add("display", "none");
        }
        else if(calculaFecha.HasValue)
        {
            caFechaFinalizacionInicial.Enabled = false;
            ImgBtnFechaFinal.Style.Add("display", "");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string ValidarActualizaciones()
    {
        StringBuilder isValid = new StringBuilder();

        Dictionary<string, string> resultVal = new Dictionary<string, string>();

        DateTime fechaInicio = caFechaInicioEjecucion.Date;
        DateTime fechaFin = caFechaFinalizacionInicial.Date;
        DateTime fechasuscripcion, fechaActaInicio;

        if(!DateTime.TryParse(txtFechaSuscripcion.Text, out fechasuscripcion))
            isValid.AppendLine("FechaSuscripción - Formato de Fecha Invalido.");

        else if(fechasuscripcion > fechaInicio)
            isValid.AppendLine("FechaSuscripción - Valor de la fecha de suscripción debe ser menor o igual a la fecha de inicio.");

        if(fechaFin.Date < fechaInicio.Date)
            isValid.AppendLine("FechaFinalización - Valor de la fecha de finalización mayor a la Fecha Inicial.");

        if(rblManejaVigenciasFuturas.SelectedValue == "Si" && fechaFin.Date.Year <= fechaInicio.Date.Year)
            isValid.AppendLine("VigenciaFinal - Valor de la Vigencia final menor a la vigencía actual.");

        if(rblRequiereActaInicio.SelectedValue == "Si" && !string.IsNullOrEmpty(txtFechaActaInicio.Text))
        {
            if(DateTime.TryParse(txtFechaActaInicio.Text, out fechaActaInicio))
            {
                if(fechaActaInicio.Date < fechaInicio.Date)
                    isValid.AppendLine("FechaActaInicio - Valor de la fecha de acta Inicio menor que la fecha de inicio de ejecución.");
            }
            else
                isValid.AppendLine("FechaActaInicio - Formato de Fecha Invalido.");
        }

        if(hfEstadoContrato.Value.Contains("CRP"))
        {
        }
        else if(hfEstadoContrato.Value.Contains("SUS"))
        {
            decimal valorPlanCompras;

            if(decimal.TryParse(txtValorInicialAportesICBF.Text.Replace("$", string.Empty), out valorPlanCompras))
            {
                var planCompras = vContratoService.ConsultarPlanComprasContratoss(null, int.Parse(hfIdContrato.Value), null, null);

                WsContratosPacco.WSContratosPACCOSoapClient cliente = new WsContratosPacco.WSContratosPACCOSoapClient();
                var miProductos = cliente.GetListaDetalleProductoPACCO(planCompras[0].Vigencia, planCompras[0].NumeroConsecutivoPlanCompras);
                var valorTotal = miProductos.Sum(e => e.valor_total).Value;

                var residuo = valorPlanCompras - valorTotal;

                if(residuo > 1)
                    isValid.AppendLine("ValorAportesICBF - Valor de aportes ICBF mayor al valor del plan de compras.");
            }
            else
                isValid.AppendLine("ValorAportesICBF - Valor incorrecto de aportes ICBF.");
        }
        else if(hfEstadoContrato.Value.Contains("EJE"))
        {

        }

        return isValid.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarCategoriaContrato()
    {
        ddlCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, true);
        foreach(CategoriaContrato tD in vLContratos)
        {
            ddlCategoriaContrato.Items.Add(new ListItem(tD.NombreCategoriaContrato, tD.IdCategoriaContrato.ToString()));
        }
        ddlCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarModalidadSeleccion()
    {
        ddlModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, true);
        foreach(ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarTipodeContrato()
    {
        string idCategoriaSeleccionada = ddlCategoriaContrato.SelectedValue;
        PnPrestacionServicios.Style.Add("display", "none");

        if(idCategoriaSeleccionada != "-1")
        {
            ddlTipoContratoConvenio.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), true, null, null, null, null, null);
            foreach(TipoContrato tD in vLTiposContratos)
            {
                ddlTipoContratoConvenio.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlTipoContratoConvenio.Items.Clear();
            ddlTipoContratoConvenio.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlTipoContratoConvenio.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void VerificarTipoContratoConvenio()
    {
        string idTipoContratoConvenio = ddlTipoContratoConvenio.SelectedValue;
        string idCategoriaContrato = ddlCategoriaContrato.SelectedValue;

        if(idTipoContratoConvenio != "-1" && idCategoriaContrato != "-1")
        {
            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato.IdTipoContrato = Convert.ToInt32(idTipoContratoConvenio);
            vTipoContrato.IdCategoriaContrato = Convert.ToInt32(idCategoriaContrato);
            vTipoContrato = vContratoService.IdentificadorCodigoTipoContrato(vTipoContrato);

            if(vTipoContrato.CodigoTipoContrato == CodTipoContConvPrestServProfesionales || vTipoContrato.CodigoTipoContrato == CotTipoContConvPrestServApoyoGestion)
            {
                ActualizarTipoContratoConvenio(true);
                cargarModalidadAcademica();
                cargarProfesion();
            }
            else
                ActualizarTipoContratoConvenio(false);
        }
        else
            ActualizarTipoContratoConvenio(false);
    }

    /// <summary>
    /// 
    /// </summary>
    private void ActualizarTipoContratoConvenio(bool valido)
    {
        if(valido)
        {
            PnPrestacionServicios.Style.Add("display", "");
            hfEsPrestacionServicios.Value = "1";
        }
        else
        {
            PnPrestacionServicios.Style.Add("display", "none");
            hfEsPrestacionServicios.Value = "0";
        }
        cvModalidadAcademica.Enabled = valido;
        cvNombreProfesion.Enabled = valido;
    }

    /// <summary>
    /// 
    /// </summary>
    private void cargarModalidadAcademica()
    {
        ddlModalidadAcademica.Items.Clear();
        List<ModalidadAcademicaKactus> vLModalidadesAcademicas = vContratoService.ConsultarModalidadesAcademicasKactus(null, null, null);
        foreach(ModalidadAcademicaKactus tD in vLModalidadesAcademicas)
            ddlModalidadAcademica.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));

        ddlModalidadAcademica.Items.Insert(0, new ListItem("Seleccionar", "-1"));

        ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarRegimenContratacion()
    {
        ddlRegimenContratacion.Items.Clear();
        List<RegimenContratacion> vLRegimensContratacion = vContratoService.ConsultarRegimenContratacions(null, null, true);
        foreach(RegimenContratacion tD in vLRegimensContratacion)
        {
            ddlRegimenContratacion.Items.Add(new ListItem(tD.NombreRegimenContratacion, tD.IdRegimenContratacion.ToString()));
        }
        ddlRegimenContratacion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void cargarProfesion()
    {
        var idModalidadAcademicaSeleccionada = ddlModalidadAcademica.SelectedValue;

        if(idModalidadAcademicaSeleccionada != "-1")
        {
            ddlNombreProfesion.Items.Clear();
            List<ProfesionKactus> vLTiposContratos = vContratoService.ConsultarProfesionesKactus(idModalidadAcademicaSeleccionada, null, null, null);
            foreach(ProfesionKactus tD in vLTiposContratos)
                ddlNombreProfesion.Items.Add(new ListItem(tD.Descripcion, tD.Codigo.ToString()));

            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        else
        {
            ddlNombreProfesion.Items.Clear();
            ddlNombreProfesion.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlNombreProfesion.SelectedValue = "-1";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarInfoEmpleados(ContratoModificacionDetalle item)
    {
        if(item.IdentificacionSolicitante != null)
        {
            hfCodRegionalContConv.Value = item.IdRegionalSolicitante.ToString();

            var solicitante = vContratoService.ConsultarEmpleado(item.IdentificacionSolicitante.ToString(), item.IdRegionalSolicitante.ToString(), item.IdDependenciaSolicitante.ToString(), item.IdCargoSolicitante.ToString());

            txtNombreSolicitante.Text = solicitante.NombreEmpleado;
            txtRegionalContratoConvenio.Text = solicitante.Regional;
            txtDependenciaSolicitante.Text = solicitante.Dependencia;
            txtCargoSolicitante.Text = solicitante.Cargo;
        }

        if(item.IdentificacionOrdenadorGasto != null)
        {
            var ordenador = vContratoService.ConsultarEmpleado(item.IdentificacionOrdenadorGasto.ToString(), item.IdRegionalOrdenadorGasto.ToString(), item.IdDependenciaOrdenadorGasto.ToString(), item.IdCargoOrdenadorGasto.ToString());

            txtNombreOrdenadorGasto.Text = ordenador.NombreEmpleado;
            txtRegionalOrdenadorGasto.Text = ordenador.Regional;
            txtNumeroIdentOrdenadoGasto.Text = ordenador.Dependencia;
            txtCargoOrdenadoGasto.Text = ordenador.Cargo;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarAportes(bool requiereActualizacion = false)
    {
        var misaportes = vContratoService.ObtenerAportesContrato(int.Parse(hfIdContrato.Value), false);
        gvAportesICBF.DataSource = misaportes;
        gvAportesICBF.DataBind();

        var AportesICBF = misaportes.Where(e => e.AportanteICBF == true);
        var AportesCofi = misaportes.Where(e => e.AportanteICBF == false);

        if(AportesICBF != null)
        {
            var valor = AportesICBF.Sum(e => e.ValorAporte);
            txtValorApoICBF.Text = string.Format("{0:$#,##0}", valor);

            var valorEspecie = AportesICBF.FirstOrDefault(e => e.AporteEnDinero == false);

            hfAportes.Value = valorEspecie != null ? "1" : string.Empty;
        }
        else
            hfAportes.Value = string.Empty;

        if(AportesCofi != null)
        {
            var valor = AportesCofi.Sum(e => e.ValorAporte);
            txtValorApoContrat.Text = string.Format("{0:$#,##0}", valor);

            hfCofinanciacion.Value = valor > 0 ? "1" : string.Empty;
        }
        else
            hfCofinanciacion.Value = string.Empty;

        if(requiereActualizacion)
        {
            var itemSolicitud = vContratoService.ConsultarContratosModificacionPorId(int.Parse(hfIdContrato.Value));

            if(itemSolicitud.ValorContrato.HasValue)
                txtvalorInicial.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContrato);

            if(itemSolicitud.ValorContratoFinal.HasValue)
                txtValorFinal.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContratoFinal);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarAporteCofinanciacion()
    {
        int idContrato = int.Parse(hfIdContrato.Value);
        var result = vContratoService.EliminarAportesCofinanciacionContrato(idContrato);

        if(result > 0)
        {
            CargarAportes(true);
            imgValorApoContrat.Style.Add("display", "none");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarAporteEspecieICBF()
    {
        int idContrato = int.Parse(hfIdContrato.Value);
        var result = vContratoService.EliminarAportesEspecieICBFContrato(idContrato);

        if(result > 0)
        {
            CargarAportes(true);
            imgValorApoICBF.Style.Add("display", "none");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarCDP()
    {
        gvCDP.EmptyDataText = EmptyDataText();
        gvCDP.PageSize = PageSize();
        gvRubrosCDP.EmptyDataText = EmptyDataText();
        gvRubrosCDP.PageSize = PageSize();

        List<ContratosCDP> cdps = vContratoService.ConsultarContratosCDPLinea(Convert.ToInt32(hfIdContrato.Value));
        gvCDP.DataSource = cdps;
        gvCDP.DataBind();

        List<ContratosCDP> rubrosCdp = vContratoService.ConsultarContratosCDPRubroLinea(Convert.ToInt32(hfIdContrato.Value));
        gvRubrosCDP.DataSource = rubrosCdp;
        gvRubrosCDP.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarRP()
    {
        gvRP.EmptyDataText = EmptyDataText();
        gvRP.PageSize = PageSize();

        List<RPContrato> rps = vContratoService.ConsultarRPContratosAsociados(Convert.ToInt32(hfIdContrato.Value));
        gvRP.DataSource = rps;
        gvRP.DataBind();
    }

    private void CargarSupervisores()
    {
        List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
        gvSupervInterv.DataSource = supervisoresInterventores;
        gvSupervInterv.DataBind();

        List<SupervisorInterContrato> directoresInterventoria = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), true);
        gvDirectoresInterv.DataSource = directoresInterventoria;
        gvDirectoresInterv.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarVigenciasFuturas(bool requiereActualizacion = false)
    {
        gvVigFuturas.EmptyDataText = EmptyDataText();
        gvVigFuturas.PageSize = PageSize();

        int idContrato = int.Parse(hfIdContrato.Value);
        var itemsVigencia = vContratoService.ConsultarVigenciaFuturass(null, null, idContrato, null);
        gvVigFuturas.DataSource = itemsVigencia;
        gvVigFuturas.DataBind();

        if(itemsVigencia.Count > 0)
            hfVigenciasFuturas.Value = "1";

        if(rblManejaVigenciasFuturas.SelectedValue == "Si")
            imgVigFuturas.Style.Add("display", "");
        else
            imgVigFuturas.Style.Add("display", "none");

        if(requiereActualizacion)
        {
            var itemSolicitud = vContratoService.ConsultarContratosModificacionPorId(int.Parse(hfIdContrato.Value));

            if(itemSolicitud.ValorContrato.HasValue)
                txtvalorInicial.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContrato);

            if(itemSolicitud.ValorContratoFinal.HasValue)
                txtValorFinal.Text = string.Format("{0:$#,##0}", itemSolicitud.ValorContratoFinal);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dateTime"></param>
    /// <returns></returns>
    private string ValidacionesFechaFinalizacionInicialContConv(DateTime dateTime)
    {
        return string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarInfoFechaFinalizacion()
    {
        string fechaFinalizacionInicial = CargarInfoCallbackFechaFinalizacion();

        if(
             !fechaFinalizacionInicial.Equals(string.Empty)
             && Convert.ToDateTime(caFechaFinalizacionInicial.Date) != Convert.ToDateTime("01/01/1900")
            )
        {
            string textValFechaFinalIniciContConv = ValidacionesFechaFinalizacionInicialContConv(caFechaFinalizacionInicial.Date);

            if(textValFechaFinalIniciContConv == string.Empty)
            {
                if(!chkCalculaFecha.Checked)
                {
                    string diferenciaFechas = string.Empty;
                    ContratoService.ObtenerDiferenciaFechas(caFechaInicioEjecucion.Date, caFechaFinalizacionInicial.Date, out diferenciaFechas);

                    var items = diferenciaFechas.Split('|');

                    txtDiasPiEj.Text = items[2];
                    txtMesesPiEj.Text = items[1];
                    txtAcnosPiEj.Text = items[0];
                }
                else
                {
                    txtAcnosPiEj.Text = "0";
                    if(!string.IsNullOrEmpty(GetSessionParameter("CalculoFecha.Dias").ToString()))
                    {
                        txtDiasPiEj.Text = GetSessionParameter("CalculoFecha.Dias").ToString();
                        RemoveSessionParameter("CalculoFecha.Dias");
                    }
                    if(!string.IsNullOrEmpty(GetSessionParameter("CalculoFecha.Meses").ToString()))
                    {
                        txtMesesPiEj.Text = GetSessionParameter("CalculoFecha.Meses").ToString();
                        RemoveSessionParameter("CalculoFecha.Meses");
                    }
                }

                ddlVigenciaFiscalFin.SelectedValue = null;
                if(ddlVigenciaFiscalFin.Items.FindByText(caFechaFinalizacionInicial.Date.Year.ToString()) != null)
                    ddlVigenciaFiscalFin.Items.FindByText(caFechaFinalizacionInicial.Date.Year.ToString()).Selected = true;
            }
            else
            {
                txtDiasPiEj.Text = string.Empty;
                txtMesesPiEj.Text = string.Empty;
                txtAcnosPiEj.Text = string.Empty;
                caFechaFinalizacionInicial.InitNull = true;
                ScriptManager.RegisterStartupScript(this, GetType(), "valFechaFinIniContConv", "alert('" + textValFechaFinalIniciContConv + "')", true);
            }
        }
        else
        {
            txtDiasPiEj.Text = string.Empty;
            txtMesesPiEj.Text = string.Empty;
            txtAcnosPiEj.Text = string.Empty;
            caFechaFinalizacionInicial.InitNull = true;
        }

        if(!string.IsNullOrEmpty(hfIdContrato.Value))
            hfEsCambioFecha.Value = "1";

        toolBar.LipiarMensajeError();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string CargarInfoCallbackFechaFinalizacion()
    {
        string fechaFinalizacionInicial = string.Empty;
        if(chkCalculaFecha.Checked)
        {
            if(GetSessionParameter("CalculoFecha.FechaFinal") != null)
            {
                DateTime fechafinalizacionInicialD = DateTime.Parse(GetSessionParameter("CalculoFecha.FechaFinal").ToString());
                caFechaFinalizacionInicial.Date = fechafinalizacionInicialD;
                RemoveSessionParameter("CalculoFecha.FechaFinal");
                fechaFinalizacionInicial = caFechaFinalizacionInicial.Date.ToString();
            }
            else if(caFechaFinalizacionInicial.Date != DateTime.MinValue)
                fechaFinalizacionInicial = caFechaFinalizacionInicial.Date.ToString();
        }
        else
            fechaFinalizacionInicial = caFechaFinalizacionInicial.Date.ToString();

        return fechaFinalizacionInicial;
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarInfoFechaInicioEjecucion()
    {
        string fechaInicioEjecucionSeleccionada = caFechaInicioEjecucion.Date.ToString();

        if(fechaInicioEjecucionSeleccionada.Equals(string.Empty) || Convert.ToDateTime(fechaInicioEjecucionSeleccionada) == Convert.ToDateTime("01/01/1900"))
        {
            chkCalculaFecha.Enabled = false;
            ImgBtnFechaFinal.Visible = false;
            caFechaFinalizacionInicial.Enabled = false;
            caFechaFinalizacionInicial.InitNull = true;
            txtAcnosPiEj.Text = string.Empty;
            txtMesesPiEj.Text = string.Empty;
            txtDiasPiEj.Text = string.Empty;
            ddlVigenciaFiscalIni.SelectedValue = null;
            ddlVigenciaFiscalFin.SelectedValue = null;
        }
        else
        {
            if(chkCalculaFecha.Checked)
            {
                ImgBtnFechaFinal.Visible = true;
                caFechaFinalizacionInicial.Enabled = false;
                caFechaFinalizacionInicial.InitNull = false;

                DateTime fechaFinal = caFechaInicioEjecucion.Date;

                if(!string.IsNullOrEmpty(txtMesesPiEj.Text))
                    fechaFinal = fechaFinal.AddMonths(int.Parse(txtMesesPiEj.Text));
                else
                    txtMesesPiEj.Text = string.Empty;

                if(!string.IsNullOrEmpty(txtDiasPiEj.Text))
                {
                    fechaFinal = fechaFinal.AddDays(int.Parse(txtDiasPiEj.Text));
                    fechaFinal = fechaFinal.AddDays(-1);
                }
                else
                    txtDiasPiEj.Text = string.Empty;

                caFechaFinalizacionInicial.Date = fechaFinal;
            }
            else
            {
                if(caFechaInicioEjecucion.Date < caFechaFinalizacionInicial.Date)
                {
                    caFechaFinalizacionInicial.Enabled = true;
                    CargarInfoFechaFinalizacion();
                }
                else
                {
                    caFechaFinalizacionInicial.Enabled = true;
                    caFechaFinalizacionInicial.InitNull = true;
                    txtAcnosPiEj.Text = string.Empty;
                    txtMesesPiEj.Text = string.Empty;
                    txtDiasPiEj.Text = string.Empty;
                    ddlVigenciaFiscalFin.SelectedValue = null;
                    toolBar.MostrarMensajeError("La Fecha de Inicio debe ser menor a la fecha de finalización del contrato.");
                }
            }

            ddlVigenciaFiscalIni.SelectedValue = null;
            if(ddlVigenciaFiscalIni.Items.FindByText(caFechaInicioEjecucion.Date.Year.ToString()) != null)
                ddlVigenciaFiscalIni.Items.FindByText(caFechaInicioEjecucion.Date.Year.ToString()).Selected = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarNumeroProceso()
    {
        ModalidadSeleccion vModalidadSeleccion = new ModalidadSeleccion();
        vModalidadSeleccion.IdModalidad = Convert.ToInt32(ddlModalidadSeleccion.SelectedValue);
        hfCodigoModalidad.Value = vContratoService.IdentificadorCodigoModalidadSeleccion(vModalidadSeleccion).CodigoModalidad;

        if(hfCodigoModalidad.Value == CodContratacionDirecta || hfCodigoModalidad.Value == CodContratacionDirectaAporte)
        {
            PnNumeroProceso.Style.Add("display", "none");
            hfIdNumeroProceso.Value = string.Empty;
            hfFechaAdjudicacionProceso.Value = string.Empty;
        }
        else
            PnNumeroProceso.Style.Add("display", "");
    }

    /// <summary>
    /// 
    /// </summary>
    private void EliminarVigenciasFuturas()
    {
        var result = vContratoService.EliminarVigenciaFuturasModificacionContratoTodas(int.Parse(hfIdContrato.Value));

        if(result > 0)
            CargarVigenciasFuturas(true);
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarLugarEjecucion()
    {
        gvLugaresEjecucion.EmptyDataText = EmptyDataText();
        gvLugaresEjecucion.PageSize = PageSize();
        int idContrato = int.Parse(hfIdContrato.Value);
        var lugarEjecucion = vContratoService.ConsultarLugaresEjecucionContrato(idContrato);
        gvLugaresEjecucion.DataSource = lugarEjecucion;
        gvLugaresEjecucion.DataBind();

        if(lugarEjecucion != null && lugarEjecucion.Count > 0)
            txtDatosAdicionalesLugarEjecucion.Text = lugarEjecucion.First().DatosAdicionales;

        if(lugarEjecucion.Count() > 2)
        {
            dvLugaresEjecucion.Style.Add("height", "100px");
            dvLugaresEjecucion.Style.Add("overflow-x", "hidden");
            dvLugaresEjecucion.Style.Add("overflow-y", "scroll");
        }
        else
        {
            dvLugaresEjecucion.Style.Remove("height");
            dvLugaresEjecucion.Style.Remove("overflow-x");
            dvLugaresEjecucion.Style.Remove("overflow-y");
        }

        var args = Request.Params.Get("__EVENTARGUMENT");

        if(args == "SI")
            dvLugaresEjecucion.Style.Add("display", "none");
        else
            dvLugaresEjecucion.Style.Add("display", "");

        chkNivelNacional.Checked = args == "SI" ? true : false;
    }

    private void EliminarContratista(int rowIndex)
    {
        Proveedores_Contratos vProveedor_Contrato = new Proveedores_Contratos();
        vProveedor_Contrato.IdProveedoresContratos = Convert.ToInt32(gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"]);
        vProveedor_Contrato.IdContrato = Convert.ToInt32(hfIdContrato.Value);
        vContratoService.EliminarProveedores_Contratos(vProveedor_Contrato);
        CargarContratistas();
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarContratistas()
    {
        List<Proveedores_Contratos> contratistas = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
        gvContratistas.DataSource = contratistas;
        gvContratistas.DataBind();

        List<Proveedores_Contratos> integrantesConsorcioUnionTemp = vContratoService.ObtenerProveedoresContrato(Convert.ToInt32(hfIdContrato.Value), 1);
        gvIntegrantesConsorcio_UnionTemp.DataSource = integrantesConsorcioUnionTemp;
        gvIntegrantesConsorcio_UnionTemp.DataBind();

        if(contratistas.Count() > 0)
            hfContratistas.Value = "1";
        else
            hfContratistas.Value = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarCodigosSECOP()
    {
        imgbtnCodigosSECOP.Enabled = true;
        int idContrato = int.Parse(hfIdContrato.Value);
        gvCodigosSECOP.EmptyDataText = EmptyDataText();
        gvCodigosSECOP.PageSize = PageSize();
        gvCodigosSECOP.DataSource = vContratoService.ObtenerCodigosSECOPContrato(idContrato);
        gvCodigosSECOP.DataBind();
        if(gvCodigosSECOP.Rows != null && gvCodigosSECOP.Rows.Count > 0)
            hfCodigoSECOP.Value = "1";
        else
            hfCodigoSECOP.Value = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    public void CargarGarantias()
    {
        int idContrato = int.Parse(hfIdContrato.Value);
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(idContrato);
        GridGarantias.DataSource = items;
        GridGarantias.DataBind();
    }

    #endregion 

    #region Eventos

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if(Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if(ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if(!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                try
                {
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch(sControlName)
                    {
                        case "CargarAportes":
                        CargarAportes(true);
                        break;
                        case "EliminarAportesEspecieICBF":
                        EliminarAporteEspecieICBF();
                        break;
                        case "EliminarAportesCofinanciacion":
                        EliminarAporteCofinanciacion();
                        break;
                        case "EliminarVigenciasFuturas":
                        EliminarVigenciasFuturas();
                        break;
                        case "ObtenerCDP":
                        CargarCDP();
                        break;
                        case "ObtenerRP":
                        CargarRP();
                        break;
                        case "ObtenerSupervisorInterventor":
                        CargarSupervisores();
                        break;
                        case "cphCont_caFechaFinalizacionInicial":
                        CargarInfoFechaFinalizacion();
                        break;
                        case "cphCont_caFechaInicioEjecucion":
                        CargarInfoFechaInicioEjecucion();
                        break;
                        case "CargarNumeroProceso":
                        CargarNumeroProceso();
                        break;
                        case "ObtenerVigenciasFuturas":
                        CargarVigenciasFuturas(true);
                        break;
                        case "ObtenerLugarEjecucion":
                        CargarLugarEjecucion();
                        break;
                        case "ObtenerContratistas":
                        CargarContratistas();
                        break;
                        case "GetCodigosSECOP":
                        CargarCodigosSECOP();
                        break;
                        case "CargarSupervisores":
                        CargarSupervisores();
                        break;
                        case "GetGarantias":
                        CargarGarantias();
                        break;
                        default: break;
                    }
                }
                catch(UserInterfaceException ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
                catch(Exception ex)
                {
                    toolBar.MostrarMensajeError(ex.Message);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void AccSolicitudContrato_ItemCommand(object sender, CommandEventArgs e)
    {

    }

    protected void ddlCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CargarTipodeContrato();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlTipoContratoConvenio_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            VerificarTipoContratoConvenio();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlModalidadAcademica_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            cargarProfesion();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlModalidadSeleccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CargarNumeroProceso();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarAporteClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            AporteContrato vAporteContrato = new AporteContrato();
            vAporteContrato.IdAporteContrato = Convert.ToInt32(gvAportesICBF.DataKeys[rowIndex]["IdAporteContrato"]);
            var result = vContratoService.EliminarAporteContrato(vAporteContrato);

            if(result > 0)
            {
                CargarAportes(true);
            }
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvAportesICBF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            string aportante = gvAportesICBF.DataKeys[e.Row.RowIndex].Values[2].ToString();
            string tipoAporte = gvAportesICBF.DataKeys[e.Row.RowIndex].Values[1].ToString();

            if(aportante == "ICBF" && tipoAporte == "DINERO")
                ((LinkButton)e.Row.FindControl("btnEliminar")).Visible = false;
        }
    }

    protected void btnEliminarCDPClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            ContratosCDP vContratoCPD = new ContratosCDP();
            vContratoCPD.IdCDP = Convert.ToInt32(gvCDP.DataKeys[rowIndex]["IdContratosCDP"]);
            int result = vContratoService.EliminarContratosCDP(vContratoCPD);

            if(result > 0)
                CargarCDP();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarRPClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            RPContrato vContratoRP = new RPContrato();
            vContratoRP.IdRPContrato = Convert.ToInt32(gvRP.DataKeys[rowIndex]["IdRPContrato"]);
            int result = vContratoService.EliminarRPContratos(vContratoRP, 0);

            if(result > 0)
                CargarRP();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarGvVigFuturasClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            var IDVigenciaFuturas = Convert.ToInt32(gvVigFuturas.DataKeys[rowIndex]["IDVigenciaFuturas"]);
            int result = vContratoService.EliminarVigenciaFuturasModificacionContrato(IDVigenciaFuturas);

            if(result > 0)
                CargarVigenciasFuturas(true);
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlVigenciaSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void gvSupervisoresActuales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            string id = DataBinder.Eval(e.Row.DataItem, "IDSupervisorIntervContrato").ToString();
            string idrol = "1";

            string script = string.Format("GetSuperInterv('{0}','{1}'); return false;", id, idrol);
            button.Attributes.Add("onclick", script);
        }
    }

    protected void chkNivelNacionalCheckedChanged(object sender, EventArgs e)
    {
        if(chkNivelNacional.Checked)
        {
            imgLugarEjecucion.Enabled = false;
            txtDatosAdicionalesLugarEjecucion.Enabled = true;
            dvLugaresEjecucion.Style.Add("display", "none");
        }
        else
        {
            imgLugarEjecucion.Enabled = true;
            txtDatosAdicionalesLugarEjecucion.Enabled = false;
            txtDatosAdicionalesLugarEjecucion.Text = string.Empty;
            dvLugaresEjecucion.Style.Add("display", "");
        }
    }

    protected void btnEliminarLugEjecucionClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            LugarEjecucionContrato vLugarEjecucionContrato = new LugarEjecucionContrato();
            vLugarEjecucionContrato.IdLugarEjecucionContratos = Convert.ToInt32(gvLugaresEjecucion.DataKeys[rowIndex]["IdLugarEjecucionContratos"]);
            var result = vContratoService.EliminarLugarEjecucionContrato(vLugarEjecucionContrato);

            if(result > 0)
                CargarLugarEjecucion();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnOpcionGvContratistasClick(object sender, EventArgs e)
    {
        switch(((LinkButton)sender).CommandName)
        {
            case "Eliminar":
            EliminarContratista(Convert.ToInt32(((LinkButton)sender).CommandArgument));
            break;
            case "Detalle":
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);

            string idProveedor = gvContratistas.DataKeys[rowIndex]["IdTercero"].ToString();
            SetSessionParameter("Contrato.IdProveedor", idProveedor);

            string idProveedoresContratos = gvContratistas.DataKeys[rowIndex]["IdProveedoresContratos"].ToString();
            SetSessionParameter("FormaPago.IDProveedoresContratos", idProveedoresContratos);

            //SetSessionParameter("Contrato.ContratosAPP", IdContrato);
            string returnValue = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                               "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                               "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                               "   <script language='javascript'> " +
                               "        GetFormaPago();" +
                               "   </script>";


            ClientScript.RegisterStartupScript(Page.GetType(), "formaPago", returnValue);


            //ScriptManager.RegisterStartupScript(this, GetType(), "formaPago", "GetFormaPago()", true);
            break;
            default:
            break;
        }
    }

    protected void btnEliminarGvSupervIntervClick(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            SupervisorInterContrato vSupervisorInterContrato = new SupervisorInterContrato();
            vSupervisorInterContrato.IDSupervisorIntervContrato = Convert.ToInt32(gvSupervInterv.DataKeys[rowIndex]["IDSupervisorIntervContrato"]);
            vContratoService.EliminarSupervisorInterContrato(vSupervisorInterContrato);
            CargarSupervisores();
        }
        catch(UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void btnEliminarCodigoSECOP_Click(object sender, EventArgs e)
    {
        try
        {
            int idContrato = int.Parse(hfIdContrato.Value);
            int rowIndex = Convert.ToInt32(((LinkButton)sender).CommandArgument);
            int idCodigoSECOP = Convert.ToInt32(gvCodigosSECOP.DataKeys[rowIndex]["key"]);
            vContratoService.EliminarCodigoSECOPContrato(idContrato, idCodigoSECOP);
            CargarCodigosSECOP();
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void GridGarantias_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdGarantia = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IDGarantia").ToString());
            string NumeroGarantia = DataBinder.Eval(e.Row.DataItem, "NumeroGarantia").ToString();
            string FechaGarantia = DataBinder.Eval(e.Row.DataItem, "FechaAprobacionGarantia").ToString();

            string valor = string.Format("GetGarantias('{0}','{1}','{2}'); return false;", IdGarantia.ToString(), NumeroGarantia, FechaGarantia);
            button.Attributes.Add("onclick", valor);
        }
    }

    protected void gvSupervInterv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {

             if(
                 ! string.IsNullOrEmpty(hfTieneCamBioSupervisor.Value) && int.Parse(hfTieneCamBioSupervisor.Value) > 0
               )
             {
                LinkButton button = (LinkButton)e.Row.FindControl("btnEliminar");
                button.Visible = false;
             }
        }
    }
    #endregion                                                                          

}

