using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Proveedor.Service;
using Icbf.Oferente.Entity;

/// <summary>
/// Página que despliega el detalle del registro del consorcio relacionado
/// </summary>
public partial class Page_RelacionarContratistas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RelacionarContratistas";
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("RelacionarContratistas.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("RelacionarContratistas.IdContratistaContrato", hfIdContratistaContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdContratistaContrato = Convert.ToInt32(GetSessionParameter("RelacionarContratistas.IdContratistaContrato"));
            RemoveSessionParameter("RelacionarContratistas.IdContratistaContrato");
            string vUsuario = GetSessionUser().NombreUsuario;

            if (GetSessionParameter("RelacionarContratistas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            if (GetSessionParameter("RelacionarContratistas.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La información ha sido modificada exitosamente");
            RemoveSessionParameter("RelacionarContratistas");


            RelacionarContratistas vRelacionarContratistas = new RelacionarContratistas();
            vRelacionarContratistas = vContratoService.ConsultarRelacionarContratistas(vIdContratistaContrato);
            hfIdContratistaContrato.Value = vIdContratistaContrato.ToString();
            Tercero vTercero = vProveedorService.ConsultarTerceros(null, null, null, vRelacionarContratistas.NumeroIdentificacion.ToString(), vUsuario).First();
            if (vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal > 0)
            {
                Tercero vTerceroRepresentante = vProveedorService.ConsultarTerceros(null, null, null, vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal.ToString(), vUsuario).First();
                this.txtNumeroIdentificacionRL.Text = vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal.ToString();
                this.txtPrimerNombreRepresentanteLegal.Text = vTerceroRepresentante.PrimerNombre;
                this.txtSegundoNombreRepresentanteLegal.Text = vTerceroRepresentante.SegundoNombre;
                this.txtPrimerApellidoRepresentanteLegal.Text = vTerceroRepresentante.PrimerApellido;
                this.txtSegundoApellidoRepresentanteLegal.Text = vTerceroRepresentante.SegundoApellido;
            }
            Contrato vContrato = vContratoService.ConsultarContrato(vRelacionarContratistas.IdContrato);
            this.txtNumeroContrato.Text = vContrato.NumeroContrato;
            this.txtTipoPersona.Text = vTercero.NombreTipoPersona;
            this.txtRazonSocial.Text = vTercero.RazonSocial;
            this.txtPrimerNombre.Text = vTercero.PrimerNombre;
            this.txtSegundoNombre.Text = vTercero.SegundoNombre;
            this.txtPrimerApellido.Text = vTercero.PrimerApellido;
            this.txtSegundoApellido.Text = vTercero.SegundoApellido;
            this.txtNumeroIdentificacion.Text = vRelacionarContratistas.NumeroIdentificacion.ToString();

            if (txtTipoPersona.Text.Equals("JURIDICA"))
            {
                panelRepresentanteLegalContratista.Visible = true;
            }
            else
                panelRepresentanteLegalContratista.Visible = false;
           
            txtPorcentajeParticipacion.Text = vRelacionarContratistas.PorcentajeParticipacion.ToString();
            rblEstadoIntegrante.SelectedValue = vRelacionarContratistas.EstadoIntegrante.ToString();
            ObtenerAuditoria(PageName, hfIdContratistaContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRelacionarContratistas.UsuarioCrea, vRelacionarContratistas.FechaCrea, vRelacionarContratistas.UsuarioModifica, vRelacionarContratistas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdContratistaContrato = Convert.ToInt32(hfIdContratistaContrato.Value);

            RelacionarContratistas vRelacionarContratistas = new RelacionarContratistas();
            vRelacionarContratistas = vContratoService.ConsultarRelacionarContratistas(vIdContratistaContrato);
            InformacionAudioria(vRelacionarContratistas, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarRelacionarContratistas(vRelacionarContratistas);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("RelacionarContratistas.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);

            toolBar.EstablecerTitulos("Relacionar Consorcio", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstadoIntegrante, "Activo", "Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
