<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_RelacionarContratistas_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdContratistaContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Número Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdContrato" ControlToValidate="txtNumeroContrato"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Número de identificación *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroIdentificacion" ControlToValidate="txtNumeroIdentificacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td width="50%">
                <asp:TextBox runat="server" ID="txtNumeroContrato" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContrato" runat="server" TargetControlID="txtNumeroContrato"
                    FilterType="Numbers" ValidChars="0123456789" />
                <asp:Image ID="imgBcodigoUsuario" runat="server" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    OnClick="GetContrato()" Style="cursor: hand" ToolTip="Buscar" />
                <asp:HiddenField ID="hfIdContrato" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hfNumeroContrato" runat="server" ClientIDMode="Static" />
            </td>
            <td width="50%">
                <asp:TextBox runat="server" ID="txtNumeroIdentificacion" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <asp:Image Width="20px" Height="20px" ID="imgBuscarPersona" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    Style="cursor: hand" ToolTip="Buscar" onClick="GetPersona()" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo Persona *
            </td>
            <td>
                Razón Social *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtTipoPersona" ClientIDMode="Static" MaxLength="10"
                    onfocus="blur();" class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftTipoPersona" runat="server" TargetControlID="txtTipoPersona"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtRazonSocial" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FTRazonSocial" runat="server" TargetControlID="txtRazonSocial"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <asp:HiddenField ID="hfClaseEntidad" runat="server" ClientIDMode="Static" />
        </tr>
        <tr class="rowB">
            <td>
                Primer Nombre
            </td>
            <td>
                Segundo Nombre
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerNombre" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FTPrimerNombre" runat="server" TargetControlID="txtPrimerNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoNombre" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FtSegundoNombre" runat="server" TargetControlID="txtSegundoNombre"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Primer Apellido
            </td>
            <td>
                Segundo Apellido
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPrimerApellido" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPrimerApellido" runat="server" TargetControlID="txtPrimerApellido"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSegundoApellido" ClientIDMode="Static" onfocus="blur();"
                    class="enable"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="FtSegundoApellido" runat="server" TargetControlID="txtSegundoApellido"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ @" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Porcentaje de Participación *
                <asp:RequiredFieldValidator runat="server" ID="rfvPorcentajeParticipacion" ControlToValidate="txtPorcentajeParticipacion"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Estado Integrante *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstadoIntegrante" ControlToValidate="rblEstadoIntegrante"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtPorcentajeParticipacion" MaxLength="4"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftPorcentajeParticipacion" runat="server" TargetControlID="txtPorcentajeParticipacion"
                    FilterType="Numbers" ValidChars="0123456789" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstadoIntegrante" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <asp:Panel ID="panelRepresentanteLegalContratista" Style="display: none;" ClientIDMode="Static"
        runat="server">
        <asp:Label ID="Label1" runat="server" Text="INFORMACION REPRESENTANTE LEGAL" Font-Bold="true"
            Font-Size="Small"></asp:Label>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Número de Identificación Representante Legal *
                    <asp:Label ID="lblErrorRepresentanteLegal" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNumeroIdentificacionRL" ClientIDMode="Static"
                        onfocus="blur();" class="enable"></asp:TextBox>
                    <asp:Image Width="20px" Height="20px" ID="Image1" runat="server" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        Style="cursor: hand" ToolTip="Buscar" onClick="GetRepresentanteLegal()" />
                </td>
                <td>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Nombre
                </td>
                <td>
                    Segundo Nombre
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerNombreRepresentanteLegal" ClientIDMode="Static"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoNombreRepresentanteLegal" ClientIDMode="Static"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Primer Apellido
                </td>
                <td>
                    Segundo Apellido
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtPrimerApellidoRepresentanteLegal" ClientIDMode="Static"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtSegundoApellidoRepresentanteLegal" ClientIDMode="Static"
                        onfocus="blur();" class="enable"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" language="javascript">
        function GetContrato() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaContratos.aspx', setReturnGetContrato, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetContrato(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 3) {
                    $('#txtNumeroContrato').val(retLupa[3]);
                    $('#hfIdContrato').val(retLupa[0]);
                }
            }
        }

        function GetPersona() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaTercero.aspx', setReturnGetPersona, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }

        function setReturnGetPersona(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 10) {
                    if (retLupa[1] == "JURIDICA") {
                        document.getElementById('panelRepresentanteLegalContratista').style.display = 'block';
                    }
                    else {
                        document.getElementById('panelRepresentanteLegalContratista').style.display = 'none';
                    }
                    $('#txtTipoPersona').val(retLupa[1]);
                    $('#txtNumeroIdentificacion').val(retLupa[3]);
                    $('#txtRazonSocial').val(retLupa[4]);
                    $('#txtPrimerNombre').val(retLupa[7]);
                    $('#txtSegundoNombre').val(retLupa[8]);
                    $('#txtPrimerApellido').val(retLupa[9]);
                    $('#txtSegundoApellido').val(retLupa[10]);
                }
            }
        }

        function GetRepresentanteLegal() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaTercero.aspx', setReturnGetRepresentanteLegal, 'dialogWidth:750px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetRepresentanteLegal(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                var retLupa = pObj.split(",");
                if (retLupa.length > 10) {
                    $('#txtNumeroIdentificacionRL').val(retLupa[3]);
                    $('#txtPrimerNombreRepresentanteLegal').val(retLupa[7]);
                    $('#txtSegundoNombreRepresentanteLegal').val(retLupa[8]);
                    $('#txtPrimerApellidoRepresentanteLegal').val(retLupa[9]);
                    $('#txtSegundoApellidoRepresentanteLegal').val(retLupa[10]);
                }
            }
        }

        function mostrarPanel() {
            document.getElementById('panelRepresentanteLegalContratista').style.display = 'block';
        }

        function ocultarPanel() {
            document.getElementById('panelRepresentanteLegalContratista').style.display = 'none';
        }
    </script>
</asp:Content>
