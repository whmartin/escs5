using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de relaci�n de consorcios
/// </summary>
public partial class Page_RelacionarContratistas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/RelacionarContratistas";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            int? vIdContratistaContrato = null;
            int? vIdContrato = null;
            long vNumeroIdentificacion = 0;
            String vClaseEntidad = null;
            int? vPorcentajeParticipacion = null;
            Boolean vEstadoIntegrante = false;
            long vNumeroIdentificacionRepresentanteLegal = 0;
            
            if (hfIdContrato.Value!= "")
            {
                vIdContrato = Convert.ToInt32(hfIdContrato.Value);
            }
            if (txtNumeroIdentificacion.Text!= "")
            {
                vNumeroIdentificacion = Convert.ToInt64(txtNumeroIdentificacion.Text);
            }
            if (txtPorcentajeParticipacion.Text != "")
            {
                vPorcentajeParticipacion = Convert.ToInt32(txtPorcentajeParticipacion.Text);
            }
            if (rblEstadoIntegrante.SelectedValue != "")
            {
                vEstadoIntegrante = Convert.ToBoolean(rblEstadoIntegrante.SelectedValue);
            }
            if (txtNumeroIdentificacionRL.Text!= "")
            {
                vNumeroIdentificacionRepresentanteLegal = Convert.ToInt64(txtNumeroIdentificacionRL.Text);
            }
            gvRelacionarContratistas.DataSource = vContratoService.ConsultarRelacionarContratistass( vIdContratistaContrato, vIdContrato, vNumeroIdentificacion, vClaseEntidad, vPorcentajeParticipacion, vEstadoIntegrante, vNumeroIdentificacionRepresentanteLegal);
            gvRelacionarContratistas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvRelacionarContratistas.PageSize = PageSize();
            gvRelacionarContratistas.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Relacionar Consorcio", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvRelacionarContratistas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("RelacionarContratistas.IdContratistaContrato", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvRelacionarContratistas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvRelacionarContratistas.SelectedRow);
    }
    protected void gvRelacionarContratistas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRelacionarContratistas.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstadoIntegrante, "Activo", "Inactivo");
            if (GetSessionParameter("RelacionarContratistas.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("RelacionarContratistas.Eliminado");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
