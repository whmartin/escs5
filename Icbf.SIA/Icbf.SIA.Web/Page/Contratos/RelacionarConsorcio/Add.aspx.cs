using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Oferente.Entity;
using Icbf.Proveedor.Service;

/// <summary>
/// Página de registro y edición de relación de consorcio
/// </summary>
public partial class Page_RelacionarContratistas_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();
    string PageName = "Contratos/RelacionarContratistas";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }

        if(hfIdContrato.Value.Equals(String.Empty) && !GetSessionParameter("RelacionarContratistas.IdContrato").ToString().Equals(String.Empty))
        {
            hfIdContrato.Value = GetSessionParameter("RelacionarContratistas.IdContrato").ToString();
        }
    }

    /// <summary>
    /// Manejador de eventos click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            this.lblErrorRepresentanteLegal.Text = string.Empty;
            this.lblErrorRepresentanteLegal.Visible = false;
            int vResultado;
            RelacionarContratistas vRelacionarContratistas = new RelacionarContratistas();

            vRelacionarContratistas.IdContrato = Convert.ToInt32(hfIdContrato.Value);
            vRelacionarContratistas.IdContrato = Convert.ToInt32(hfIdContrato.Value);
            vRelacionarContratistas.NumeroIdentificacion = Convert.ToInt64(txtNumeroIdentificacion.Text);
            vRelacionarContratistas.ClaseEntidad = "2- Consorcio o Union Temporal";
            vRelacionarContratistas.PorcentajeParticipacion = Convert.ToInt32(txtPorcentajeParticipacion.Text);
            vRelacionarContratistas.EstadoIntegrante = Convert.ToBoolean(rblEstadoIntegrante.SelectedValue);
            vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal = txtNumeroIdentificacionRL.Text.Equals(String.Empty) ? 0 : Convert.ToInt64(txtNumeroIdentificacionRL.Text);
            
            if (txtTipoPersona.Text.Equals("JURIDICA"))
            {
                if (this.txtNumeroIdentificacionRL.Text.Trim() == string.Empty)
                {
                    this.lblErrorRepresentanteLegal.Text = "Campo Requerido";
                    this.lblErrorRepresentanteLegal.Visible = true;
                    this.panelRepresentanteLegalContratista.Style.Clear();
                    this.panelRepresentanteLegalContratista.Style.Add("display", "block");
                    return;
                }
            }


            if (Request.QueryString["oP"] == "E")
            {
            vRelacionarContratistas.IdContratistaContrato = Convert.ToInt32(hfIdContratistaContrato.Value);
                vRelacionarContratistas.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRelacionarContratistas, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarRelacionarContratistas(vRelacionarContratistas);
            }
            else
            {
                vRelacionarContratistas.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vRelacionarContratistas, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarRelacionarContratistas(vRelacionarContratistas);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("RelacionarContratistas.IdContratistaContrato", vRelacionarContratistas.IdContratistaContrato);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("RelacionarContratistas.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("RelacionarContratistas.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }                
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Relacionar Consorcio", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            int vIdContratistaContrato = Convert.ToInt32(GetSessionParameter("RelacionarContratistas.IdContratistaContrato"));
            RemoveSessionParameter("RelacionarContratistas.Id");

            RelacionarContratistas vRelacionarContratistas = new RelacionarContratistas();
            vRelacionarContratistas = vContratoService.ConsultarRelacionarContratistas(vIdContratistaContrato);
            hfIdContratistaContrato.Value = vRelacionarContratistas.IdContratistaContrato.ToString();
            hfIdContrato.Value = vRelacionarContratistas.IdContrato.ToString();
            txtPorcentajeParticipacion.Text = vRelacionarContratistas.PorcentajeParticipacion.ToString();
            rblEstadoIntegrante.SelectedValue = vRelacionarContratistas.EstadoIntegrante.ToString();

            Tercero vTercero = vProveedorService.ConsultarTerceros(null, null, null, vRelacionarContratistas.NumeroIdentificacion.ToString(), GetSessionUser().NombreUsuario).First();
            if (vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal > 0)
            {
                Tercero vTerceroRepresentante = vProveedorService.ConsultarTerceros(null, null, null, vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal.ToString(), GetSessionUser().NombreUsuario).First();
                this.txtNumeroIdentificacionRL.Text = vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal.ToString();
                this.txtPrimerNombreRepresentanteLegal.Text = vTerceroRepresentante.PrimerNombre;
                this.txtSegundoNombreRepresentanteLegal.Text = vTerceroRepresentante.SegundoNombre;
                this.txtPrimerApellidoRepresentanteLegal.Text = vTerceroRepresentante.PrimerApellido;
                this.txtSegundoApellidoRepresentanteLegal.Text = vTerceroRepresentante.SegundoApellido;
            }
            Contrato vContrato = vContratoService.ConsultarContrato(vRelacionarContratistas.IdContrato);
            this.txtNumeroContrato.Text = vContrato.NumeroContrato;
            this.txtTipoPersona.Text = vTercero.NombreTipoPersona;
            this.txtRazonSocial.Text = vTercero.RazonSocial;
            this.txtPrimerNombre.Text = vTercero.PrimerNombre;
            this.txtSegundoNombre.Text = vTercero.SegundoNombre;
            this.txtPrimerApellido.Text = vTercero.PrimerApellido;
            this.txtSegundoApellido.Text = vTercero.SegundoApellido;
            this.txtNumeroIdentificacion.Text = vRelacionarContratistas.NumeroIdentificacion.ToString();
            if (txtTipoPersona.Text.Equals("JURIDICA"))
            {
                //panelRepresentanteLegalContratista.Visible = true;
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "mostrarPanel();", true);
            }else
            {
                //panelRepresentanteLegalContratista.Visible = true;
                ClientScript.RegisterStartupScript(this.GetType(), "myScript", "ocultarPanel();", true);
            }

            txtNumeroIdentificacionRL.Text = vRelacionarContratistas.NumeroIdentificacionRepresentanteLegal.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vRelacionarContratistas.UsuarioCrea, vRelacionarContratistas.FechaCrea, vRelacionarContratistas.UsuarioModifica, vRelacionarContratistas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstadoIntegrante, "Activo", "Inactivo");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void rblTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void txtTipoPersona_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(txtTipoPersona.Text).Equals("JURIDICA"))
        {
            panelRepresentanteLegalContratista.Visible = true;
        }
        else
        {
            panelRepresentanteLegalContratista.Visible = false;
        }
    }
}
