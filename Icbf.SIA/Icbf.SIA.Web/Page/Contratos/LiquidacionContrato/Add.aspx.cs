﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_Contratos_LiquidacionContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesModificacion";
    string PageName1 = "Contratos/ConsModContractual";
    string TIPO_ESTRUCTURA = "LiquidacionContrato";
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage) ||  ValidateAccess(toolBar,PageName1,vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void Guardar()
    {
        try
        {
            if (ValidarFechas())
            {
                int vResultado = 0;
                LiquidacionContrato vLiquidacionContrato = new LiquidacionContrato();

                decimal valorEjecutado;
                decimal valorFinal;

                if (!decimal.TryParse(txtValorEjecutado.Text.Replace("$", ""), out valorEjecutado))
                {
                    toolBar.MostrarMensajeError("El valor del campo Valor Ejecutado tiene un formato incorrecto");
                    return;
                }

                if (!decimal.TryParse(txtvalorfinal.Text.Replace("$", ""), out valorFinal))
                {
                    toolBar.MostrarMensajeError("El valor del campo Valor Ejecutado tiene un formato incorrecto");
                    return;
                }

                List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
                if (supervisoresInterventores != null)
                {
                    var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).FirstOrDefault();
                    vLiquidacionContrato.NombreSupervisor = querySupervisores.SupervisorInterventor.NombreCompleto;
                    vLiquidacionContrato.CargoSupervisor = querySupervisores.SupervisorInterventor.Cargo;
                    vLiquidacionContrato.DependenciaSueprvisor = querySupervisores.SupervisorInterventor.Dependencia;
                }

                vLiquidacionContrato.TipodeLiquidacion = ddlTipoTerminacion.SelectedItem.Text;
                vLiquidacionContrato.FechaRaquidacion = DateTime.Parse(txtFechaRadicacion.Text);


                if (valorEjecutado > 0)
                {
                    if( string.IsNullOrEmpty(txtJustificacionLib.Text) && (valorFinal != valorEjecutado) )
                    {
                        toolBar.MostrarMensajeError("Ingrese la Justifiación de la Liberación");                        
                        return;
                    }

                    vLiquidacionContrato.ValorEjecutado = decimal.Parse(txtValorEjecutado.Text.Replace("$", string.Empty));
                    vLiquidacionContrato.ValorLiberado = decimal.Parse(txtvalorfinal.Text.Replace("$", string.Empty)) - vLiquidacionContrato.ValorEjecutado;
                    vLiquidacionContrato.JustificacionLiberacion = txtJustificacionLib.Text;
                }

                if (Request.QueryString["oP"] == "E")
                {

                    vLiquidacionContrato.IdLiquidacionContrato = Convert.ToInt32(hfIdLiquidacionContrato.Value);
                    vLiquidacionContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultado = vContratoService.ModificarLiquidacionContrato(vLiquidacionContrato);
                }
                else
                { 
                    vLiquidacionContrato.IdDetConsModContractual = Convert.ToInt32(hfIdDetConsModContractual.Value);
                    vLiquidacionContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultado = vContratoService.InsertarLiquidacionContrato(vLiquidacionContrato);
                }
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    if (hfIdConsModContractual.Value != "")
                    {
                        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
                    }
                    else
                    {
                        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                    }

                    SetSessionParameter("Contrato.IdLiquidacionContrato", vLiquidacionContrato.IdLiquidacionContrato);
                    SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
                    SetSessionParameter("LiquidacionContrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private bool ValidarFechas()
    {
        bool isValid = true;

        try
        {
            DateTime fechaRadicacionLiquidacion;

            if (!DateTime.TryParse(txtFechaRadicacion.Text, out fechaRadicacionLiquidacion))
            {
                toolBar.MostrarMensajeError("El formato de la fecha de radicación es invalido.");
                isValid = false;
            }
            else
            {
                var contrato = vContratoService.ContratoObtener(int.Parse(hfIdContrato.Value));

                if (contrato.FechaFinalTerminacionContrato.Value > fechaRadicacionLiquidacion)
                {
                    toolBar.MostrarMensajeError("La fecha de radicación es menor a la fecha de finalización del contrato.");
                    isValid = false;
                }
            }
        }
        catch (Exception ex)
        {
               
        }

        return isValid;
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Liquidaci&#243;n de Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();
            var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            int vIdConModContratual;

            if (GetSessionParameter("ConsModContractual.IDCosModContractual") != null)
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();
            }
            else
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();
            }

            if (Request.QueryString["oP"] == "E")
            {
                int vIdLiquidacion = Convert.ToInt32(GetSessionParameter("Contrato.IdLiquidacionContrato"));
                RemoveSessionParameter("Contrato.IdLiquidacionContrato");
                hfIdLiquidacionContrato.Value = vIdLiquidacion.ToString();
                LiquidacionContrato vLiquidacionContrato = vContratoService.ConsultarLiquidacionId(vIdLiquidacion).First();
                txtFechaRadicacion.Text = vLiquidacionContrato.FechaRadicacionView;
                txtValorEjecutado.Text = string.Format("{0:$#,##0}", vLiquidacionContrato.ValorEjecutado);
                txtValorLiberado.Text = string.Format("{0:$#,##0}", vLiquidacionContrato.ValorLiberado);
                txtJustificacionLib.Text = vLiquidacionContrato.JustificacionLiberacion;
                toolBar.MostrarBotonNuevo(false);
                PanelArchivos.Visible = true;
                gvanexos.EmptyDataText = EmptyDataText();
                gvanexos.PageSize = PageSize();
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            else
            {
                int vIdDetConModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                hfIdDetConsModContractual.Value = vIdDetConModContractual.ToString();
            }

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalTerminacionContrato);

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();
            

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            cetxtfechaRadicacion.StartDate = caFechaFinalizacionInicial.Date;


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion
    
}

