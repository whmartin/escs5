﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_LiquidacionContrato_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

 <script type="text/javascript" language="javascript">

    function puntitos(donde, caracter, campo) {
            var decimales = false
            dec = 2;
            if (dec != 0) {
                decimales = true
            }

            pat = /[\*,\+,\(,\),\?,\\,\$,\[,\],\^]/
            valor = donde.value
            largo = valor.length
            crtr = true
            if (isNaN(caracter) || pat.test(caracter) == true) {
                if (pat.test(caracter) == true)
                { caracter = "\\" + caracter }
                carcter = new RegExp(caracter, "g")
                valor = valor.replace(carcter, "")
                donde.value = valor
                crtr = false
            }
            else {
                var nums = new Array()
                cont = 0
                for (m = 0; m < largo; m++) {
                    if (valor.charAt(m) == "." || valor.charAt(m) == " " || valor.charAt(m) == ",")
                    { continue; }
                    else {
                        nums[cont] = valor.charAt(m)
                        cont++
                    }

                }
            }

            if (decimales == true) {
                ctdd = eval(1 + dec);
                nmrs = 1
            }
            else {
                ctdd = 1; nmrs = 3
            }

            var cad1 = "", cad2 = "", cad3 = "", tres = 0
            if (largo > nmrs && crtr == true) {
                for (k = nums.length - ctdd; k >= 0; k--) {
                    cad1 = nums[k]
                    cad2 = cad1 + cad2
                    tres++
                    if ((tres % 3) == 0) {
                        if (k != 0) {
                            cad2 = "." + cad2
                        }
                    }
                }

                for (dd = dec; dd > 0; dd--) {
                    cad3 += nums[nums.length - dd]
                }
                if (decimales == true && (cad2.length + cad3.length) > dec) {
                    cad2 += "," + cad3
                } else {
                    cad2 += cad3
                }
                donde.value = cad2
            }
            donde.focus()

    }

      function ValidarValor(n, currency) {

            var valorContrato = document.getElementById('<%= txtvalorfinal.ClientID %>').value;

            valorContrato = valorContrato.split('.').join('');
            valorContrato = valorContrato.split('$').join('');

            var nsinFormato = n.split('.').join('');
                nsinFormato = nsinFormato.split('$').join('');

            var valorEjecutado = parseFloat(nsinFormato);

            if (valorEjecutado <= valorContrato) {

                var valorLiberacion = valorContrato - valorEjecutado;

                var valueWithFormat = currency + " " + valorEjecutado.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
                var valuecesionWithFormat = currency + " " + valorLiberacion.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");

                valueWithFormat = valueWithFormat.split('.').join('*');
                valueWithFormat = valueWithFormat.split(',').join('.');
                valueWithFormat = valueWithFormat.split('*').join(',');

                valuecesionWithFormat = valuecesionWithFormat.split('.').join('*');
                valuecesionWithFormat = valuecesionWithFormat.split(',').join('.');
                valuecesionWithFormat = valuecesionWithFormat.split('*').join(',');

                document.getElementById('<%= txtValorEjecutado.ClientID %>').value = valueWithFormat;
                document.getElementById('<%= txtValorLiberado.ClientID %>').value = valuecesionWithFormat;
            }
            else {
                document.getElementById('<%= txtValorEjecutado.ClientID %>').value = '';
                document.getElementById('<%= txtValorLiberado.ClientID %>').value = '';
                alert('El valor ejecutado debe ser menor al valor final del contrato.');
            }
      }
     function limitText(limitField, limitNum) {
         if (limitField.value.length > limitNum) {
             limitField.value = limitField.value.substring(0, limitNum);
         }
     }
     </script>

    <asp:HiddenField ID="hfIdConsModContractual" runat="server"  />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdLiquidacionContrato" runat="server" />

    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha de Radicaci&oacute;n 
                <asp:RequiredFieldValidator runat="server" ID="rfvvalorReduccion" ControlToValidate="txtFechaRadicacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>                
            </td>
            <td class="style1" style="width: 50%">
                Tipo de Liquidación   
                                <asp:RegularExpressionValidator runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" style="font-weight: 700" Display="Dynamic" ValidationExpression="^[0-9]" ValidationGroup="btnGuardar" ControlToValidate="ddlTipoTerminacion" ID="RegularExpressionValidator1" />
            </td>
        </tr>
         <tr class="rowA">
            <td class="auto-style1">
                <%--<asp:TextBox runat="server" ID="txtvalorReduccion"    
                     MaxLength="128" Width="320px" Height="22px" OnTextChanged="txtvalorReduccion_TextChanged" AutoPostBack="true"></asp:TextBox>--%>
                <asp:TextBox runat="server" ID="txtFechaRadicacion" MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
                <asp:Image ID="imgFechaRadicacion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"  />
                <Ajax:CalendarExtender ID="cetxtfechaRadicacion" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFechaRadicacion" TargetControlID="txtFechaRadicacion">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaRadicacion">
                </Ajax:MaskedEditExtender>
            </td>
            <td class="auto-style2"> 
                <asp:DropDownList runat="server" ID="ddlTipoTerminacion">
                    <asp:ListItem Text="Seleccione" Value="-1" />
                    <asp:ListItem Text="Bilateral" Value="1" />
                    <asp:ListItem Text="Unilateral" Value="2" />
                </asp:DropDownList>
             </td>
        </tr>    
        
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Ejecutado
            </td>
            <td style="width: 50%">
                Saldo a Liberar
            </td>
        </tr>          
         <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtValorEjecutado"  
                 MaxLength="128" Width="320px" Height="22px" onchange="ValidarValor(this.value,'$');" ></asp:TextBox>
                                        <Ajax:FilteredTextBoxExtender ID="ftValorEjecutado" runat="server" TargetControlID="txtvalorEjecutado"
                        FilterType="Custom,Numbers" ValidChars="$.,"  /> 
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtValorLiberado"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Justificación de la Liberación
            </td>
            <td style="width: 50%">              
            </td>
        </tr>  
        <tr class="rowA">
            <td class="style1">
              <asp:TextBox runat="server" ID="txtJustificacionLib" TextMode="MultiLine" Width="400px" Height="42px" MaxLength="250" 
                    onKeyDown="limitText(this,250);" Rows="3" Style="resize: none" onKeyUp="limitText(this,250);"></asp:TextBox>
            </td>
            <td>
               
            </td>
        </tr>  
        <tr class="rowB">
            <td>
                Supervisores
                &nbsp;&nbsp;&nbsp;</td>
            <td>
                &nbsp;</td>
            
        </tr>
        <tr class="rowA">
            <td colspan="2">
                
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificación" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="Número Identificación" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>  
                              
        </tr>       

    </table>
        <asp:Panel ID="PanelArchivos" runat="server" Visible="false" Width="90%">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUploadArchivoContrato" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>            
        </asp:Panel>

</asp:Content>
