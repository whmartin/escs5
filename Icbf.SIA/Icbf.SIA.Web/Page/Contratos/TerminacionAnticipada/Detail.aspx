﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_Contratos_TerminacionAnticipada_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
     <asp:HiddenField ID="hfIdConsModContractual" runat="server"  />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdTerminacionAnticipada" runat="server" />
    <asp:HiddenField runat="server" ID="hfEsSubscripcion" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha de Radicaci&oacute;n 
                <%--<asp:RequiredFieldValidator runat="server" ID="rfvvalorReduccion" ControlToValidate="txtvalorReduccion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>--%>                
            </td>
            <td style="width: 50%">
                 N&uacute;mero de Radicaci&oacute;n
            </td>
        </tr>
         <tr class="rowA">
            <td class="auto-style1">
                <%--<asp:TextBox runat="server" ID="txtvalorReduccion"    
                     MaxLength="128" Width="320px" Height="22px" OnTextChanged="txtvalorReduccion_TextChanged" AutoPostBack="true"></asp:TextBox>--%>
                <asp:TextBox runat="server" ID="txtFechaRadicacion"    
                     MaxLength="128" Width="320px" Height="22px" Enabled="false"></asp:TextBox>
            </td>
            <td class="auto-style2">
              <asp:TextBox runat="server" ID="txtNuemroRadicacion"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>    
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Tipo de Terminaci&oacute;n
                                
            </td>
            <td style="width: 50%">
                 Fecha Terminaci&oacute;n Anticipada
            </td>
        </tr>
         <tr class="rowA">
            <td class="auto-style1">
                <%--<asp:TextBox runat="server" ID="txtvalorReduccion"    
                     MaxLength="128" Width="320px" Height="22px" OnTextChanged="txtvalorReduccion_TextChanged" AutoPostBack="true"></asp:TextBox>--%>
                <asp:DropDownList runat="server" ID="ddlTipoTerminacion" Enabled="false">
                    <asp:ListItem Text="Seleccione" Value="-1" />
                    <asp:ListItem Text="Anticipada Mutuo Acuerdo" Value="1" />
                    <asp:ListItem Text="Anticipada Unilateral" Value="2" />
                </asp:DropDownList>
                
            </td>
            <td class="auto-style2">
              <asp:TextBox runat="server" ID="TxtFechaTerminacion"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
                
            </td>
        </tr>    
      
        <tr class="rowB">
            <td colspan="3">
                Consideraciones
                
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtConsideraciones" TextMode="MultiLine" Height="73px" Width="95%" MaxLength="200" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="4">
                Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="4">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>
        
    </table>
          <table width="90%" align="center">
        <tr class="rowB">
            <td>
              Documentos
            </td>
            <td colspan="2">
                &nbsp;</td>            
        </tr>
            <tr class="rowAG">
                              <td>
                                  <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" 
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>

</asp:Content>