﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Service;
using System.IO;
using System.Net;

/// <summary>
/// Página que despliega el detalle del registro de tipo garantía
/// </summary>
public partial class Page_Contratos_TerminacionAnticipada_Add : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SolicitudesModificacion";
    string PageName1 = "Contratos/ConsModContractual";
    string TIPO_ESTRUCTURA = "TerminacionAnticipada";
    ContratoService vContratoService = new ContratoService();
    ContratoService vTipoSolicitudService = new ContratoService();
    private SIAService vSiaService = new SIAService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage) || ValidateAccess(toolBar, PageName1, vSolutionPage))
        {
            if (!Page.IsPostBack)
                CargarDatosIniciales();
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual",hfIdConsModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            NavigateTo("~/Page/Contratos/ConsModContractual/Detail.aspx");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado = 0;
            TerminacionAnticipada vTerminacionAnticipada = new TerminacionAnticipada();
            vTerminacionAnticipada.FechaTerminacionAnticipada = DateTime.Parse(txtFechaTerminacion.Text);
            vTerminacionAnticipada.FechaRadicacion = DateTime.Parse(TxtFechaRadicacion.Text);
            vTerminacionAnticipada.Consideraciones = txtConsideraciones.Text;
            vTerminacionAnticipada.NumeroRadicacion = txtNuemroRadicacion.Text;
            vTerminacionAnticipada.TipoTerminacion = ddlTipoTerminacion.SelectedItem.Text;
            var itemContrato = vContratoService.ConsultarContratoReduccion(int.Parse(hfIdContrato.Value));
            
            if(ValidarFechas())
            {
                if (Request.QueryString["oP"] == "E")
                {
                    vTerminacionAnticipada.IdTerminacionAnticipada = Convert.ToInt32(hfIdTerminacionAnticipada.Value);
                    vTerminacionAnticipada.UsuarioModifica = GetSessionUser().NombreUsuario;
                    vResultado = vContratoService.ModificarTerminacionAnticipada(vTerminacionAnticipada);
                }
                else
                {
                    vTerminacionAnticipada.IDDetalleConsModeContractual = Convert.ToInt32(hfIdDetConsModContractual.Value);
                    vTerminacionAnticipada.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultado = vContratoService.InsertarTerminacionAnticipada(vTerminacionAnticipada);
                }

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    if (hfIdConsModContractual.Value != "")
                    {
                        SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
                    }
                    else
                    {
                        SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
                    }

                    SetSessionParameter("Contrato.IdTerminacionAnticipada", vTerminacionAnticipada.IdTerminacionAnticipada);
                    SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);
                    SetSessionParameter("TerminacionAnticipada.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);

                }
                else
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
                }
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Terminaci&#243;n de Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            int vIdCntrato = Convert.ToInt32(GetSessionParameter("Contrato.IdContrato"));
            RemoveSessionParameter("Contrato.IdContrato");
            hfIdContrato.Value = vIdCntrato.ToString();
            var itemContrato = vContratoService.ConsultarContratoReduccion(vIdCntrato);
            int vIdConModContratual;

            if (GetSessionParameter("ConsModContractual.IDCosModContractual") != null)
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();
            }
            else
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();
            }

            if (Request.QueryString["oP"] == "E")
            {
                int vIdTerminacion = Convert.ToInt32(GetSessionParameter("Contrato.IdTerminacionAnticipada"));
                RemoveSessionParameter("Contrato.IdTerminacionAnticipada");
                hfIdTerminacionAnticipada.Value = vIdTerminacion.ToString();
                TerminacionAnticipada vTerminacionAnticipada = vContratoService.ConsultarTerminacionAnticipadaId(vIdTerminacion).First();
                TxtFechaRadicacion.Text = vTerminacionAnticipada.FechaRadicacionView;
                txtFechaTerminacion.Text = vTerminacionAnticipada.FechaTerminacionAnticipadaView;
                txtConsideraciones.Text = vTerminacionAnticipada.Consideraciones;
                txtNuemroRadicacion.Text = vTerminacionAnticipada.NumeroRadicacion;
                ddlTipoTerminacion.SelectedValue = ddlTipoTerminacion.Items.FindByText(vTerminacionAnticipada.TipoTerminacion).Value;
                toolBar.MostrarBotonNuevo(false);
                PanelArchivos.Visible = true;

                var tiposDocumentos = vContratoService.ConsultarVariosDocumentosLiquidacion(null, null);
                ddlDocumentosTerminacion.DataSource = tiposDocumentos;
                ddlDocumentosTerminacion.DataTextField = "NombreDocumento";
                ddlDocumentosTerminacion.DataValueField = "IdDocumentoLiquidacion";
                ddlDocumentosTerminacion.DataBind();

                gvanexos.EmptyDataText = EmptyDataText();
                gvanexos.PageSize = PageSize();
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            else
            {
                int vIdDetConModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                hfIdDetConsModContractual.Value = vIdDetConModContractual.ToString();
            }

            ConsModContractual vConsModContractual = new ConsModContractual();
            vConsModContractual = vContratoService.ConsultarConsModContractual(vIdConModContratual);

            txtContrato.Text = itemContrato.NumeroContrato;
            txtRegional.Text = itemContrato.NombreRegional;

            txtobjeto.Text = itemContrato.ObjetoContrato;
            txtalcance.Text = itemContrato.AlcanceObjeto;
            txtvalorinicial.Text = string.Format("{0:$#,##0}", itemContrato.ValorInicial);
            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);

            DateTime caFechaInicioEjecucion = Convert.ToDateTime(itemContrato.FechaInicioContrato);
            DateTime caFechaFinalizacionInicial = Convert.ToDateTime(itemContrato.FechaFinalTerminacionContrato);

            CalendarExtenderFechaRadicacion.StartDate = caFechaInicioEjecucion;
            cetxtFehcaTerminacion.StartDate = caFechaInicioEjecucion;

            CalendarExtenderFechaRadicacion.EndDate = caFechaFinalizacionInicial;
            cetxtFehcaTerminacion.EndDate = caFechaFinalizacionInicial;

            txtFechaInicio.Text = caFechaInicioEjecucion.ToShortDateString();
            txtFechaFinal.Text = caFechaFinalizacionInicial.ToShortDateString();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(vIdCntrato, null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private bool ValidarFechas()
    {
        bool isValid = true;    
        
        try
        {
            var itemContrato = vContratoService.ConsultarContratoReduccion(int.Parse(hfIdContrato.Value));
            
            DateTime FechaTerminacionAnticipada;
            DateTime FechaRadicacion;

            if (!DateTime.TryParse(txtFechaTerminacion.Text, out FechaTerminacionAnticipada))
            {
                toolBar.MostrarMensajeError("El formato de la fecha de terminación es invalido");
                isValid = false;
            }
            else if (!DateTime.TryParse(TxtFechaRadicacion.Text, out FechaRadicacion))
            {
                toolBar.MostrarMensajeError("El formato de la fecha de radicación es invalido");
                isValid = false;
            }
            else if (FechaTerminacionAnticipada < FechaRadicacion)
            { 
                toolBar.MostrarMensajeError("La Fecha de Terminaciòn Anticipada debe ser mayor que la decha de radicaciòn. ");
                isValid = false;
            }
            else if (FechaRadicacion < itemContrato.FechaInicioContrato)
            {
                toolBar.MostrarMensajeError("La Fecha de radicaciòn debe ser mayor a la fecha de inicio de contrato. ");
                isValid = false;
            }
            else if (FechaRadicacion > itemContrato.FechaFinalTerminacionContrato)
            {
                toolBar.MostrarMensajeError("La Fecha de radicaciòn debe ser menor a la fecha de Terminaciòn de contrato. ");
                isValid = false;
            }
            else if (FechaTerminacionAnticipada < itemContrato.FechaInicioContrato)
            {
                toolBar.MostrarMensajeError("La Fecha de terminaciòn anticipada debe ser mayor a la fecha de inicio de contrato. ");
                isValid = false;
            }
            else if (FechaTerminacionAnticipada > itemContrato.FechaFinalTerminacionContrato)
            {
                isValid = false;
                toolBar.MostrarMensajeError("La Fecha de terminaciòn anticipada debe ser menor a la fecha de Terminaciòn de contrato. ");
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

        return isValid;
    }

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUpload1;

        if (fuArchivo.HasFile && ddlDocumentosTerminacion.SelectedItem != null)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario,
                     ddlDocumentosTerminacion.SelectedItem.Text
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        else
            toolBar.MostrarMensajeError("Debe seleccionar el documento y/o el tipo de documento que desea subir");
    }

    #endregion
}

