﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_Contratos_TerminacionAnticipada_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

    <asp:HiddenField ID="hfIdConsModContractual" runat="server"  />
    <asp:HiddenField ID="hfIdConsModContractualGestion" runat="server" />
    <asp:HiddenField ID="hfIdDetConsModContractual" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdTerminacionAnticipada" runat="server" />

    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Numero Contrato / Convenio 
            </td>
            <td class="style1" style="width: 50%">
                Regional 
            </td>
            <td style="width: 50%">
               
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtContrato"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
            <td class="style1">
                <asp:TextBox runat="server" ID="txtRegional"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="21px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha Inicio de Contrato / Convenio 
            </td>
            <td style="width: 50%">
               Fecha Final de Terminacion de Contrato / Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtFechaInicio"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaFinal"  Enabled="false" 
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Objeto del  Contrato 
            </td>
            <td style="width: 50%">
               Alcance del Contrato 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtobjeto" TextMode="MultiLine"  Enabled="false" 
                     MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtalcance"  TextMode="MultiLine" Enabled="false" 
                    MaxLength="128" Width="400px" Height="50px" ></asp:TextBox>
            </td>
        </tr>
         <tr class="rowB">
            <td class="style1" style="width: 50%">
                Valor Inicial  del  Contrato/ Convenio 
            </td>
            <td style="width: 50%">
                Valor Final  del  Contrato/ Convenio 
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtvalorinicial"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtvalorfinal"  Enabled="false" 
                     MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Fecha de Radicaci&oacute;n 
                </td>
            <td style="width: 50%">
                 N&uacute;mero de Radicaci&oacute;n
                 <asp:RequiredFieldValidator runat="server" ID="rrfvConsideraciones0" ControlToValidate="txtNuemroRadicacion" 
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" ></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr class="rowA">
            <td class="auto-style1">
                <asp:TextBox runat="server" ID="TxtFechaRadicacion"  Enabled="true" AutoPostBack="True"></asp:TextBox>
                <asp:Image ID="ImageFechaRadicacion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="CalendarExtenderFechaRadicacion" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="ImageFechaTerminacion" TargetControlID="TxtFechaRadicacion"></Ajax:CalendarExtender>
                <asp:RequiredFieldValidator runat="server" ID="rfFechaRadicacion" ControlToValidate="TxtFechaRadicacion"
                 SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>
                    <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="TxtFechaRadicacion">
                    </Ajax:MaskedEditExtender>
            </td>
            <td class="auto-style2">
              <asp:TextBox runat="server" ID="txtNuemroRadicacion"   
                    MaxLength="128" Width="320px" Height="22px" ></asp:TextBox>
            </td>
        </tr>    
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Tipo de Terminaci&oacute;n   
                                <asp:RegularExpressionValidator runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" style="font-weight: 700" Display="Dynamic" ValidationExpression="^[0-9]" ValidationGroup="btnGuardar" ControlToValidate="ddlTipoTerminacion" />
            </td>
            <td style="width: 50%">
                
                 Fecha Terminaci&oacute;n Anticipada

            </td>
        </tr>
         <tr class="rowA">
            <td class="auto-style1">
                <asp:DropDownList runat="server" ID="ddlTipoTerminacion">
                    <asp:ListItem Text="Seleccione" Value="-1" />
                    <asp:ListItem Text="Anticipada Mutuo Acuerdo" Value="1" />
                    <asp:ListItem Text="Anticipada Unilateral" Value="2" />
                </asp:DropDownList>
            </td>
            <td class="auto-style2">
               <asp:TextBox runat="server" ID="txtFechaTerminacion"  AutoPostBack="True"></asp:TextBox>
                <asp:Image ID="imgCalendarioFin1" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif" Style="cursor: hand"/>
                    <Ajax:CalendarExtender ID="cetxtFehcaTerminacion" runat="server" Format="dd/MM/yyyy" 
                        PopupButtonID="imgCalendarioFin1" TargetControlID="txtFechaTerminacion"></Ajax:CalendarExtender>
                    <Ajax:MaskedEditExtender ID="meetxtFechaFin" runat="server" CultureAMPMPlaceholder="AM;PM"
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                        CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaTerminacion">
                    </Ajax:MaskedEditExtender>
                <asp:RequiredFieldValidator runat="server" ID="rfFechaRadicacion0" ControlToValidate="txtFechaTerminacion"
                 SetFocusOnError="true" ErrorMessage="*" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" style="font-weight: 700"></asp:RequiredFieldValidator>
            </td>
        </tr>    
      
        <tr class="rowB">
            <td colspan="3">
                Consideraciones
                 <asp:RequiredFieldValidator runat="server" ID="rrfvConsideraciones" ControlToValidate="txtConsideraciones" 
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red" ></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtConsideraciones" TextMode="MultiLine" Height="73px" Width="95%" MaxLength="200" ValidationGroup="btnGuardar" ></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td colspan="3">
                Supervisor
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="3">
                                <asp:GridView ID="gvSupervisoresActuales" Width="100%" runat="server" AutoGenerateColumns="false" CellPadding="8" DataKeyNames="IDSupervisorIntervContrato,FechaInicio" GridLines="None" Height="16px"  >
                                    <Columns>
                                        <asp:BoundField DataField="NombreCompletoSuperInterventor" HeaderText="Supervisor" />
<%--                                        <asp:BoundField DataField="EtQInternoExterno" HeaderText="Tipo Supervisor y/o Interventor" />--%>
                                        <asp:TemplateField HeaderText="Fecha de Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lbFechaInicio" runat="server" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
<%--                                                <asp:TextBox ID="txtFechaInicio0" runat="server" MaxLength="10" Text='<%# Bind("FechaInicio", "{0:dd/MM/yyyy}") %>' Visible="false" Width="73px"></asp:TextBox>
                                                <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFechaInicio$txtFechaInicio" Display="Dynamic" ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="btnAprobar"></asp:CompareValidator>
                                                <Ajax:MaskedEditExtender ID="meetxtFecha" runat="server" CultureAMPMPlaceholder="AM;PM" CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/" CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                                                </Ajax:MaskedEditExtender>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TipoIdentificacion" HeaderText="Tipo Identificaci&oacute;n" />
                                        <asp:BoundField DataField="Identificacion" HeaderText="N&uacute;mero Identificaci&oacute;n" />
                                        <asp:BoundField HeaderText="Cargo Supervisor" DataField="SupervisorInterventor.Cargo" />
                                        <asp:BoundField HeaderText="Dependencia Supervisor" DataField="SupervisorInterventor.Dependencia" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="rowBG" />
                                    <EmptyDataRowStyle CssClass="headerForm" />
                                    <HeaderStyle CssClass="headerForm" />
                                    <RowStyle CssClass="rowAG" />
                                </asp:GridView>
            </td>
        </tr>

    </table>

    <asp:Panel runat="server" Visible="false" ID="PanelArchivos">
        <table width="90%" align="center">
            <tr class="rowB">
            <td>
                Archivo a Cargar</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Agregar"   OnClick="CargarArchivoFTP" />
                <asp:DropDownList ID="ddlDocumentosTerminacion" runat="server" Width="282px">
                </asp:DropDownList>
            </td>
            <td>
                </td>
        </tr>
            <tr class="rowAG">
                <td>
                                            <asp:GridView ID="gvanexos" runat="server" DataKeyNames="IdArchivo" AutoGenerateColumns="false" GridLines="None"
                                                Width="100%" CellPadding="8" Height="16px" OnSelectedIndexChanged="gvanexos_SelectedIndexChanged"
                                               OnPageIndexChanging="gvanexos_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField>
                                                    <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetails" ImageUrl="~/Image/btn/info.jpg" runat="server" Target="_blank" NavigateUrl='<%# Eval("NombreArchivo", "~/Page/Contratos/DescargarArchivo/DescargarArchivo.aspx?fname={0}") %>'>Archivo</asp:HyperLink>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>  
                                                    <asp:BoundField HeaderText="IdArchivo" DataField="IdArchivo" Visible ="False" />
                                                    <asp:BoundField HeaderText="Nombre del Documento" DataField="NombreArchivoOri" />
                                                    <asp:BoundField HeaderText="Nombre del archivo" DataField="NombreArchivo" />                                                                                                        
                                                   <asp:TemplateField>
                                                      <ItemTemplate>
                                                       <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/Cancel.png"
                                                         Height ="16px" Width="16px" ToolTip="Eliminar Anexo" />
                                                      </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="rowBG" />
                                                <EmptyDataRowStyle CssClass="headerForm" />
                                                <HeaderStyle CssClass="headerForm" />
                                                <RowStyle CssClass="rowAG" />
                  </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style1 {
            width: 636px;
        }
        .auto-style2 {
            width: 629px;
        }
        .auto-style3 {
            width: 249px;
        }
        .auto-style4 {
            width: 239px;
        }
    </style>
</asp:Content>
