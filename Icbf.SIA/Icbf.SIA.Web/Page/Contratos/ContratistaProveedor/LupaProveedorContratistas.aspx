﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LupaProveedorContratistas.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master"
 Inherits="Page_Contratos_Contratos_LupaProveedorContratistas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

<asp:Panel runat="server" ID="Panel1">
    <asp:Panel runat="server" ID="pnlLista">
                <table width="90%" align="center">
                    <tr class="rowAG">
                        <td> 
                            <asp:GridView runat="server" ID="gvContratistas" AutoGenerateColumns="False" AllowPaging="True"
                                GridLines="None" Width="100%" DataKeyNames="IdEntidad" CellPadding="0" Height="16px"
                                OnSorting="gvContratistas_Sorting" AllowSorting="True" 
                                OnPageIndexChanging="gvContratistas_PageIndexChanging" OnSelectedIndexChanged="gvContratistas_SelectedIndexChanged">
                                <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Seleccionar" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="NombreTipoPersona" SortExpression="NombreTipoPersona"  />
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="CodDocumento" SortExpression="CodDocumento" />
                            <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                            <%--<asp:BoundField HeaderText="Proveedor" DataField="Proveedor" SortExpression="Proveedor"  />--%>
                            <asp:TemplateField HeaderText="Información contratista" ItemStyle-HorizontalAlign="Center" SortExpression="Proveedor">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 300px;">
                                        <%#Eval("Razonsocial")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Identificación Representante legal" DataField="NumeroidentificacionRepLegal" SortExpression="NumeroidentificacionRepLegal"  />
                            <asp:BoundField HeaderText="Representante" DataField="RazonsocialRepLegal" SortExpression="RazonsocialRepLegal"  />
                                </Columns>
                                <AlternatingRowStyle CssClass="rowBG" />
                                <EmptyDataRowStyle CssClass="headerForm" />
                                <HeaderStyle CssClass="headerForm" />
                                <RowStyle CssClass="rowAG" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
</asp:Panel>

</asp:Content>
