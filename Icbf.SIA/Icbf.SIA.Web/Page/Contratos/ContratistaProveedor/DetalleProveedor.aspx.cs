﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
//using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Entity;
using Icbf.Proveedor.Service;

public partial class Page_Contratos_Contratos_DetalleProveedor : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    
    string PageName = "Contratos/Contratos";

    ContratoService vContratoService = new ContratoService();
    
    EntidadProvOferente vEntidadProvOferente = new EntidadProvOferente();
    
    ProveedorService vProveedorService = new ProveedorService();
    
    OferenteService vOferenteService = new OferenteService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //SolutionPage vSolutionPage = SolutionPage.List;
        //if (ValidateAccess(toolBar, PageName, vSolutionPage))
        //{
        if (!Page.IsPostBack)
        {

            CargarRegistro();
        }
        //}
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        // Condición para redireccionar a la pagina Lupa info Contratista o a Lupa proveedores
        bool LLamadoPaginaInfoContratista = false;
        if (GetSessionParameter("Contratista.Retornar") != null)
        {
            if (bool.TryParse(GetSessionParameter("Contratista.Retornar").ToString(), out LLamadoPaginaInfoContratista))
            {
                if (LLamadoPaginaInfoContratista)
                {
                    string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                    string returnValues =
                            "<script language='javascript'> " +
                            "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

                    this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
                    return;                   

                }                
            }
        }
            if (
                   !string.IsNullOrEmpty(Request.QueryString["idContrato"]) &&
                   !string.IsNullOrEmpty(Request.QueryString["idContratista"]) &&
                   !string.IsNullOrEmpty(Request.QueryString["idCesion"]) &&
                   !string.IsNullOrEmpty(Request.QueryString["idDetConsmodcontractual"])
                  )
            {
                string idContrato = Request.QueryString["idContrato"];
                string idContratista = Request.QueryString["idContratista"];
                string idCesion = Request.QueryString["idCesion"];
                string iddetConsmodContractual = Request.QueryString["idDetConsmodcontractual"];

                var url = string.Format(
                                        "LupaProveedores.aspx?idContrato={0}&idContratista={1}&idCesion={2}&idDetConsmodcontractual={3}",
                                        idContrato,
                                        idContratista,
                                        idCesion,
                                        iddetConsmodContractual
                                       );
                NavigateTo(url);
            }
            else
                NavigateTo("LupaProveedores.aspx");
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        
        Guardar();
    }

    private void Guardar()
    {
        try
        {
            if (GuardarEnTabla())
            {
                string returnValues =
                "<script language='javascript'> " +
                "var pObj = Array();";

                returnValues += "pObj[" + (0) + "] = '" + hfIdTreceroProveedor.Value + "';";

                string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
                returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                               " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                                "</script>";

                ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
                toolBar.OcultarBotonGuardar(true);
                SetSessionParameter("Prooveedor.btnretornar","1");
                toolBar.eventoRetornar += toolBar_eventoRetornar;
            }
            else
            {
                if(!string.IsNullOrEmpty(GetSessionParameter("Proveedor.Duplicado").ToString()))
                    RemoveSessionParameter("Proveedor.Duplicado");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues =
                "<script language='javascript'> " +
                "   window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                "</script>";

        this.ClientScript.RegisterStartupScript(this.GetType(), "Closed", returnValues);
        return;      
    }

    private bool GuardarEnTabla()
    {
        bool vSeGuardo = false;
        try
        {
            int vResultado = 0;

            if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]) && !string.IsNullOrEmpty(Request.QueryString["idContratista"]) && hfIdTreceroProveedor.Value != string.Empty)
            {
                int idContrato = int.Parse(Request.QueryString["idContrato"]);
                int idContratista = int.Parse(Request.QueryString["idContratista"]);
                int idCesion = int.Parse(Request.QueryString["idCesion"]);

                var existe = ExisteContratoEnProveedor(idContrato, Convert.ToInt32(hfIdTreceroProveedor.Value));

                if (existe.Key > 0)
                {
                    toolBar.MostrarMensajeError(existe.Value);
                    return false;
                }
                else if(existe.Key == -1)
                {
                    SetSessionParameter("Proveedor.Duplicado",existe.Value);
                }

                bool esNuevo = false;

                if (idCesion == 0)
                {
                    idCesion = GuardarCesion();
                    esNuevo = true;
                }

                Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                vProveedores_Contratos.IdContrato = idContrato;
                vProveedores_Contratos.IdProveedores = Convert.ToInt32(hfIdTreceroProveedor.Value);
                vProveedores_Contratos.UsuarioCrea = GetSessionUser().NombreUsuario;              
                vProveedores_Contratos.EsCesion = true;
                vProveedores_Contratos.idCesion = idCesion;
                vProveedores_Contratos.idProveedorPadre = idContratista;

                InformacionAudioria(vProveedores_Contratos, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarProveedores_Contratos(vProveedores_Contratos);

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    vSeGuardo = true;

                    if(esNuevo)
                        SetSessionParameter("Cesiones.idCesion", idCesion);
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación afectó más registros de lo esperado");
                }
            }
            else if ( (!string.IsNullOrEmpty(GetSessionParameter("Contrato.ContratosAPP").ToString()) || !string.IsNullOrEmpty(Request.QueryString["idContrato"]) ) && hfIdTreceroProveedor.Value != string.Empty)
            {
                int IdContrato = 0;
                if (GetSessionParameter("Contrato.ContratosAPP").ToString() != string.Empty)
                {
                    IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));
                }
                else
                    IdContrato = Convert.ToInt32(Request.QueryString["idContrato"]);

                var existe = ExisteContratoEnProveedor(IdContrato, Convert.ToInt32(hfIdTreceroProveedor.Value));

                if (existe.Key > 0)
                {
                    toolBar.MostrarMensajeError(existe.Value);
                    return false;
                }
                else if(existe.Key == -1)
                {
                    SetSessionParameter("Proveedor.Duplicado", existe.Value);
                }

                Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
                vProveedores_Contratos.IdContrato = IdContrato;
                vProveedores_Contratos.IdProveedores = Convert.ToInt32( hfIdTreceroProveedor.Value);
                vProveedores_Contratos.UsuarioCrea = GetSessionUser().NombreUsuario;

                InformacionAudioria(vProveedores_Contratos, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarProveedores_Contratos(vProveedores_Contratos);
                
                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completó satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    vSeGuardo = true;
                }
                else
                {
                    toolBar.MostrarMensajeError("La operación afectó más registros de lo esperado");
                }
            }
            else
            {
                throw new Exception("El valor del contrato es nulo o el valor del proveedor no existe");
            }

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        return vSeGuardo;
    }

    private int GuardarCesion()
    {
        try
        {
            int vResultado;
            Cesiones vCesiones = new Cesiones();

            vCesiones.FechaCesion = Convert.ToDateTime(DateTime.Now);
            vCesiones.Justificacion = "";
            vCesiones.IDDetalleConsModContractual = Convert.ToInt32(Request.QueryString["idDetConsmodcontractual"]);

            //if (Request.QueryString["oP"] == "E")
            //{
            //    vCesiones.IdCesion = idCesion;
            //    vCesiones.UsuarioModifica = GetSessionUser().NombreUsuario;
            //    InformacionAudioria(vCesiones, this.PageName, vSolutionPage);
            //    vResultado = vContratoService.ModificarCesiones(vCesiones);
            //}
            //else
            //{
                vCesiones.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vCesiones, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarCesiones(vCesiones);
            //}
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("Cesiones.IdCesion", vCesiones.IdCesion);
                //SetSessionParameter("Cesiones.Guardado", "1");
                //NavigateTo(SolutionPage.Detail);
               
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
            return vCesiones.IdCesion;
        }
        catch (UserInterfaceException ex)
        {
            throw new Exception(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            if (string.IsNullOrEmpty("Prooveedor.btnretornar"))
                toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            else
                RemoveSessionParameter("Prooveedor.btnretornar");

            toolBar.eventoGuardar += new ToolBarDelegateLupa(btnGuardar_Click);

            // Condición para redireccionar a la pagina Lupa info Contratista o a Lupa proveedores
            bool LLamadoPaginaInfoContratista = false;
            if (GetSessionParameter("Contratista.Retornar") != null)
            {
                if (bool.TryParse(GetSessionParameter("Contratista.Retornar").ToString(), out LLamadoPaginaInfoContratista))
                {
                    if (LLamadoPaginaInfoContratista)
                    {
                        toolBar.OcultarBotonGuardar(true);
                       
                    }

                }
            }
            RemoveSessionParameter("Contratista.Retornar");
            toolBar.EstablecerTitulos("Detalle del proveedor");
            CargarDatosTipoPersona();
        }
        catch (UserInterfaceException ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
        catch (Exception ex)
        {
            //toolBar.MostrarMensajeError(ex.Message);
            throw ex;
        }
    }

    private void habilitarcamposnarutal(bool estado)
    {
        PnlReprLegal.Visible = !estado;
        txtRazonSocial.Visible = !estado;
        lblNumIdentificacion.Visible = !estado;
        txtPrimerNombre.Visible = estado;
        lblPrimerNombre.Visible = estado;
        txtSegundoNombre.Visible = estado;
        lblSegundoNombre.Visible = estado;
        txtPrimerApellido.Visible = estado;
        lblPrimerApellido.Visible = estado;
        txtSegundoApellido.Visible = estado;
        lblSegundoApellido.Visible = estado;
    }

    private void CargarRegistro()
    {
        try
        {
            if (GetSessionParameter("EntidadProvOferentes.IdEntidad") == null)
            {
                toolBar.MostrarMensajeError("Seleccione un proveedor");
                return;
            }
            //CargarDatos 
            var identida = GetSessionParameter("EntidadProvOferentes.IdEntidad");
            RemoveSessionParameter("EntidadProvOferentes.IdEntidad");
            if (identida == null)
                throw new Exception("El valor de la entidad es nula, verifique la selección del proveedor");
            int vIdEntidad = Convert.ToInt32(identida);

            vEntidadProvOferente = vProveedorService.ConsultarEntidadProvOferente(vIdEntidad);
            if (vEntidadProvOferente != null)
            {
                vEntidadProvOferente.TerceroProveedor = vOferenteService.ConsultarTercero(vEntidadProvOferente.IdTercero);
                vEntidadProvOferente.InfoAdminEntidadProv = new InfoAdminEntidad();
                vEntidadProvOferente.InfoAdminEntidadProv = vProveedorService.ConsultarInfoAdminEntidadIdEntidad(vEntidadProvOferente.IdEntidad);

                //Proveedor
                if (vEntidadProvOferente.TerceroProveedor != null)
                {
                    if (vEntidadProvOferente.TerceroProveedor.IdTipoPersona != null)
                    {
                        ddlTipoPersona.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdTipoPersona.ToString();
                        txtTipoPersona.Text = ddlTipoPersona.SelectedItem.Text;

                        switch (ddlTipoPersona.SelectedItem.Text)
                        {
                            case "UNIÓN TEMPORAL":
                                habilitarcamposnarutal(false);
                                break;
                            case "JURIDICA":
                                habilitarcamposnarutal(false);
                                break;
                            case "CONSORCIO":
                                habilitarcamposnarutal(false);
                                break;
                            case "NATURAL":
                                habilitarcamposnarutal(true);
                                break;
                        }
                    }
                    else
                    { throw new Exception("El proveedor presenta inconsistencia, por favor comuníquese con el administrador."); }

                    CargarDatosTipoIdentificacion();
                    ddlTipoIdentificacion.SelectedValue = vEntidadProvOferente.TerceroProveedor.IdDListaTipoDocumento.ToString();
                    txtTipoIdentificacion.Text = ddlTipoIdentificacion.SelectedItem.Text;
                    hfIdTreceroProveedor.Value = vEntidadProvOferente.IdEntidad.ToString(); //vEntidadProvOferente.TerceroProveedor.IdTercero.ToString();
                    if (vEntidadProvOferente.TerceroProveedor.NumeroIdentificacion != null)
                    {
                        txtNúmeroIdentificacion.Text = vEntidadProvOferente.TerceroProveedor.NumeroIdentificacion.ToString();
                    }
                    if (vEntidadProvOferente.TerceroProveedor.RazonSocial != null)
                    {
                        txtRazonSocial.Text = vEntidadProvOferente.TerceroProveedor.RazonSocial;
                    }
                    if (vEntidadProvOferente.TerceroProveedor.PrimerNombre != null)
                    {
                        txtPrimerNombre.Text = vEntidadProvOferente.TerceroProveedor.PrimerNombre;
                    }
                    if (vEntidadProvOferente.TerceroProveedor.SegundoNombre != null)
                    {
                        txtSegundoNombre.Text = vEntidadProvOferente.TerceroProveedor.SegundoNombre;
                    }
                    if (vEntidadProvOferente.TerceroProveedor.PrimerApellido != null)
                    {
                        txtPrimerApellido.Text = vEntidadProvOferente.TerceroProveedor.PrimerApellido;
                    }
                    if (vEntidadProvOferente.TerceroProveedor.SegundoApellido != null)
                    {
                        txtSegundoApellido.Text = vEntidadProvOferente.TerceroProveedor.SegundoApellido;
                    }
                    
                }
                else
                { throw new Exception("El proveedor presenta inconsistencia, por favor comuníquese con el administrador."); }

                //Representante legal
                vEntidadProvOferente.RepresentanteLegal = new Icbf.Oferente.Entity.Tercero();
                if (vEntidadProvOferente.InfoAdminEntidadProv != null)
                {
                    if (vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal != null)
                    {
                        vEntidadProvOferente.RepresentanteLegal = vOferenteService.ConsultarTercero((int)vEntidadProvOferente.InfoAdminEntidadProv.IdRepLegal);

                        CargarDatosTipoIdentificacionRepr();
                        if (vEntidadProvOferente.RepresentanteLegal != null)
                        {
                            ddlTipoIdentificacionRepr.SelectedValue = vEntidadProvOferente.RepresentanteLegal.IdDListaTipoDocumento.ToString();
                            txtTipoIdentificacionRepr.Text = ddlTipoIdentificacionRepr.SelectedItem.Text;
                            if (vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion != null)
                            {
                                txtNumIdentificacionRepr.Text = vEntidadProvOferente.RepresentanteLegal.NumeroIdentificacion;
                            }
                            if (vEntidadProvOferente.RepresentanteLegal.PrimerNombre != null)
                            {
                                TxtPrimerNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerNombre;
                            }
                            if (vEntidadProvOferente.RepresentanteLegal.SegundoNombre != null)
                            {
                                TxtSegundoNombreRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoNombre;
                            }
                            if (vEntidadProvOferente.RepresentanteLegal.PrimerApellido != null)
                            {
                                TxtPrimerApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.PrimerApellido;
                            }
                            if (vEntidadProvOferente.RepresentanteLegal.SegundoApellido != null)
                            {
                                TxtSegundoApellidoRepr.Text = vEntidadProvOferente.RepresentanteLegal.SegundoApellido;
                            }
                        }
                        else
                        { throw new Exception("El proveedor presenta inconsistencia, por favor comuníquese con el administrador."); }
                    }
                }
            }
            else
            { throw new Exception("El proveedor presenta inconsistencia, por favor comuníquese con el administrador."); }
        }
        catch (UserInterfaceException ex)
        {
            if (ex.Message.IndexOf("Object reference not set to an instance of an object") >= 0)
            {
                toolBar.MostrarMensajeError("El proveedor presenta inconsistencia, por favor comuníquese con el administrador.");
            }
            else
            { toolBar.MostrarMensajeError(ex.Message); }
        }
        catch (Exception ex)
        {
            if (ex.Message.IndexOf("Object reference not set to an instance of an object") >= 0)
            {
                toolBar.MostrarMensajeError("El proveedor presenta inconsistencia, por favor comuníquese con el administrador.");
            }
            else
            { toolBar.MostrarMensajeError(ex.Message); }
        }
    }

    private void CargarDatosTipoPersona()
    {
        ManejoControles vManejoControles = new ManejoControles();
        vManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
    }

    private void CargarDatosTipoIdentificacion()
    {
        ManejoControles vManejoControles = new ManejoControles();
        if (ddlTipoPersona.SelectedValue == "1") //NATURAL
        {
            vManejoControles.LlenarTipoDocumentoRL(ddlTipoIdentificacion, "-1", true);
        }
        else
        {
            vManejoControles.LlenarTipoDocumentoN(ddlTipoIdentificacion, "-1", true);
        }
    }

    private void CargarDatosTipoIdentificacionRepr()
    {
        ManejoControles vManejoControles = new ManejoControles();
        
        vManejoControles.LlenarTipoDocumentoRL(ddlTipoIdentificacionRepr, "-1", true);
        
    }

    private KeyValuePair<int,string> ExisteContratoEnProveedor(int pIdContrato, int pIdProveedor)
    {
        Proveedores_Contratos vProveedores_Contratos = new Proveedores_Contratos();
        vProveedores_Contratos.IdContrato = pIdContrato;
        vProveedores_Contratos.IdProveedores = pIdProveedor;
        return vContratoService.ExisteProveedores_Contratos(vProveedores_Contratos);
    }
}