﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using AjaxControlToolkit;
using System.Linq.Expressions;
using Icbf.Proveedor.Service;

public partial class Page_Contratos_Contratos_LupaProveedores : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/Contratos";
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CargarDatosIniciales();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
        string returnValues = "<script language='javascript'> " +
                                   " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

        ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        Buscar();
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvProveedores, GridViewSortExpression, true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            toolBar.eventoRetornar += new ToolBarDelegateLupa(btnRetornar_Click);
            toolBar.eventoBuscar += new ToolBarDelegateLupa(btnBuscar_Click);

            toolBar.EstablecerTitulos("Lista proveedores");

            gvProveedores.EmptyDataText = "No se encontraron datos, verifique por favor";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            
            ManejoControles vManejoControles = new ManejoControles();
            vManejoControles.LlenarTipoPersonaParameter(ddlTipoPersona, "-1", true);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        ManejoControles vManejoControles = new ManejoControles();
        if (ddlTipoPersona.SelectedValue == "1") //NATURAL
        {
            vManejoControles.LlenarTipoDocumentoRL(ddlIdentificacion, "-1", true);
        }
        else
        {
            vManejoControles.LlenarTipoDocumentoN(ddlIdentificacion, "-1", true);
        }
    }
    protected void gvProveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProveedores.SelectedRow);
    }
    protected void gvProveedores_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvProveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedores.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            String vTipoPersona = null;
            String vTipoidentificacion = null;
            String vIdentificacion = null;
            String vProveedor = null;
            String vEstado = null;
            String vUsuarioCrea = null;
            if (ddlTipoPersona.SelectedValue != "-1")
            {
                vTipoPersona = Convert.ToString(ddlTipoPersona.SelectedValue);
            }
            if (ddlIdentificacion.SelectedValue != "-1" && ddlIdentificacion.SelectedValue != "")
            {
                vTipoidentificacion = Convert.ToString(ddlIdentificacion.SelectedValue);
            }
            if (txtNumeroDocumento.Text != "")
            {
                vIdentificacion = Convert.ToString(txtNumeroDocumento.Text.Trim());
            }
            if (txtProveedor.Text != "")
            {
                vProveedor = Convert.ToString(txtProveedor.Text.Trim());
            }
            
            vEstado = string.Empty;
            
            var myGridResults = vProveedorService.ConsultarEntidadProvOferentes(vTipoPersona, vTipoidentificacion, vIdentificacion, vProveedor, vEstado, vUsuarioCrea);

            int NumRegConsultaGrilla = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("NumRegConsultaGrilla"));

            if (myGridResults.Count() >= NumRegConsultaGrilla)
            {
                toolBar.MostrarMensajeError("La consulta arroja demasiados resultados, por favor refine su consulta");
                return;
            }

            
            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Icbf.Proveedor.Entity.EntidadProvOferente), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Icbf.Proveedor.Entity.EntidadProvOferente, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProveedores.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("EntidadProvOferentes.IdEntidad", strValue);
            if(
               !string.IsNullOrEmpty(Request.QueryString["idContrato"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idContratista"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idCesion"]) &&
               !string.IsNullOrEmpty(Request.QueryString["idDetConsmodcontractual"]) 
              )
            {
                string idContrato = Request.QueryString["idContrato"];
                string idContratista = Request.QueryString["idContratista"];
                string idCesion = Request.QueryString["idCesion"];
                string iddetConsmodContractual = Request.QueryString["idDetConsmodcontractual"];

                var url = string.Format(
                                        "DetalleProveedor.aspx?idContrato={0}&idContratista={1}&idCesion={2}&idDetConsmodcontractual={3}", 
                                        idContrato, 
                                        idContratista, 
                                        idCesion,
                                        iddetConsmodContractual
                                       );
                NavigateTo(url);
            }
            else if (! string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            {
                string idContrato = Request.QueryString["idContrato"];
                var url = string.Format("DetalleProveedor.aspx?idContrato={0}",idContrato);
                NavigateTo(url);
            }
            else
                NavigateTo("DetalleProveedor.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}