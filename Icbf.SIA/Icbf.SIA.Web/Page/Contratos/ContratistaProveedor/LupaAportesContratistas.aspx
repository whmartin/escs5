﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LupaAportesContratistas.aspx.cs" MasterPageFile="~/General/General/Master/Lupa.master" 
Inherits="Page_Contratos_Contratos_LupaAportesContratistas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">

<asp:Panel runat="server" ID="Panel1">
<asp:Panel runat="server" ID="pnlLista">
    <table width="90%" align="center">
        <tr class="rowAG">
            <td>
                <asp:GridView runat="server" ID="gvProveedores" AutoGenerateColumns="False" AllowPaging="True"
                    GridLines="None" Width="100%" DataKeyNames="IdEntidad,IdTercero" CellPadding="0" Height="16px"
                    OnSorting="gvProveedores_Sorting" AllowSorting="True" 
                    OnPageIndexChanging="gvProveedores_PageIndexChanging" OnSelectedIndexChanged="gvProveedores_SelectedIndexChanged">
                    <Columns>
                <asp:TemplateField HeaderText="Seleccionar">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                            Height="16px" Width="16px" ToolTip="Seleccionar" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" SortExpression="TipoPersonaNombre"  />
                <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion" SortExpression="NumeroIdentificacion"  />
                <asp:BoundField HeaderText="Información Contratista" DataField="Proveedor" SortExpression="Proveedor"  />
                    </Columns>
                    <AlternatingRowStyle CssClass="rowBG" />
                    <EmptyDataRowStyle CssClass="headerForm" />
                    <HeaderStyle CssClass="headerForm" />
                    <RowStyle CssClass="rowAG" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>

</asp:Panel>
</asp:Content>
