﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using AjaxControlToolkit;
using System.Linq.Expressions;
using Icbf.Proveedor.Service;

public partial class Page_Contratos_Contratos_LupaAportesContratistas : GeneralWeb
{
    General_General_Master_Lupa toolBar;
    string PageName = "Contratos/Contratos";
    ContratoService vContratoService = new ContratoService();
    ProveedorService vProveedorService = new ProveedorService();

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string)ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Buscar();
        }
       
    }

    /// <summary>
    /// Realiza la bùsqueda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            CargarGrilla(gvProveedores, GridViewSortExpression, true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master y manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (General_General_Master_Lupa)this.Master;
            //toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            //toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Contratistas Contrato");

            gvProveedores.EmptyDataText = "No se encontraron datos, verifique por favor";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    ///// <summary>
    ///// Inicializa valores de controles
    ///// </summary>
    //private void CargarDatosIniciales()
    //{
        
    //}
   
    protected void gvProveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvProveedores.SelectedRow);
    }
    protected void gvProveedores_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrilla((GridView)sender, e.SortExpression, false);
    }
    protected void gvProveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedores.PageIndex = e.NewPageIndex;
        CargarGrilla((GridView)sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrilla(BaseDataBoundControl gridViewsender, string expresionOrdenamiento, bool cambioPaginacion)
    {
        if (! string.IsNullOrEmpty( GetSessionParameter("Contrato.ContratosAPP").ToString())  && ! string.IsNullOrEmpty(Request.QueryString["idContrato"]))
        {
            toolBar.MostrarMensajeError("Se ha perdido la sesión, por favor ingrese de nuevo");
            return;
        }
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        int? IdContrato = null;
        if (! string.IsNullOrEmpty(GetSessionParameter("Contrato.ContratosAPP").ToString()))
            IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP").ToString());
        else if (!string.IsNullOrEmpty(Request.QueryString["idContrato"]))
            IdContrato = int.Parse(Request.QueryString["idContrato"]);

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var myGridResults = vContratoService.ConsultarContratistasContratos(IdContrato);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof(Proveedores_Contratos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression = Expression.Lambda<Func<Proveedores_Contratos, object>>(Expression.Convert(prop, typeof(object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {

            string returnValues =
            "<script language='javascript'> " +
            "var pObj = Array();";

            returnValues += "pObj[" + (0) + "] = '" + HttpUtility.HtmlDecode(gvProveedores.DataKeys[gvProveedores.SelectedRow.RowIndex].Values["IdEntidad"].ToString()) + "';";

            for (int c = 2; c <= 4; c++)
            {
                if (gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[c].Text != "&nbsp;")
                {

                    returnValues += "pObj[" + (c) + "] = '" + HttpUtility.HtmlDecode(gvProveedores.Rows[gvProveedores.SelectedIndex].Cells[c].Text) + "';";
                }
                else
                    returnValues += "pObj[" + (c) + "] = '';";
            }


            string dialog = Request.AppRelativeCurrentExecutionFilePath.Substring(2, Request.AppRelativeCurrentExecutionFilePath.IndexOf(".") - 2).Replace('/', '_');
            returnValues += " parent.document.getElementById('hdLupa" + dialog + "').value = pObj;" +
                           " window.parent.window_closeModalDialog('dialog" + dialog + "');" +
                            "</script>";

            ClientScript.RegisterStartupScript(Page.GetType(), "rv", returnValues);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}