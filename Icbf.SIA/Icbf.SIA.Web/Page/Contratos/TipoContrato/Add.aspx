<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_TipoContrato_Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
<asp:HiddenField ID="hfIdTipoContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Tipo de Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvNombreTipoContrato" ControlToValidate="txtNombreTipoContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 50%">
                Categor&#237;a del Contrato *
                <asp:RequiredFieldValidator runat="server" ID="rfvIdCategoriaContrato" ControlToValidate="ddlIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIdCategoriaContrato" ControlToValidate="ddlIdCategoriaContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreTipoContrato" Height="50px" 
                    MaxLength="128" TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNombreTipoContrato" runat="server" TargetControlID="txtNombreTipoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato" Height="25px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Requiere Acta de Inicio *
                <asp:RequiredFieldValidator runat="server" ID="rfvActaInicio" ControlToValidate="rblActaInicio"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Requiere Aportes o Cofinanciaci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvAporteCofinaciacion" ControlToValidate="rblAporteCofinaciacion"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:RadioButtonList runat="server" ID="rblActaInicio" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblAporteCofinaciacion" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Recursos Financieros *
                <asp:RequiredFieldValidator runat="server" ID="rfvRecursoFinanciero" ControlToValidate="rblRecursoFinanciero"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Régimen de Contrataci&#243;n *
                <asp:RequiredFieldValidator runat="server" ID="rfvRegimenContrato" ControlToValidate="ddlRegimenContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvRegimenContrato" ControlToValidate="ddlRegimenContrato"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                 ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:RadioButtonList runat="server" ID="rblRecursoFinanciero" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlRegimenContrato" Height="25px" 
                    Width="300px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Descripci&#243;n
            </td>
            <td>
                Estado *
                <asp:RequiredFieldValidator runat="server" ID="rfvEstado" ControlToValidate="rblEstado"
                 SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                 ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtDescripcionTipoContrato" Height="50px" 
                    MaxLength="100" TextMode="MultiLine" Width="320px" 
                    onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftDescripcionTipoContrato" runat="server" TargetControlID="txtDescripcionTipoContrato"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="áéíóúÁÉÍÓÚñÑ " />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 358px;
        }
    </style>
</asp:Content>

