<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_TipoContrato_Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
<asp:HiddenField ID="hfIdTipoContrato" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="style1" style="width: 50%">
                Nombre Tipo de Contrato *
            </td>
            <td style="width: 50%">
                Categor&#237;a del Contrato *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtNombreTipoContrato"  Enabled="false" 
                    Height="50px" MaxLength="128" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td valign="top">
                <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato"  Enabled="false" 
                    Height="25px" Width="300px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Requiere Acta de Inicio *
            </td>
            <td>
                Requiere Aportes o Cofinanciaci&#243;n*
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:RadioButtonList runat="server" ID="rblActaInicio" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblAporteCofinaciacion" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Recursos Financieros *
            </td>
            <td>
                Régimen de Contrataci&#243;n *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:RadioButtonList runat="server" ID="rblRecursoFinanciero" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlRegimenContrato"  Enabled="false" 
                    Height="25px" Width="300px"></asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="style1">
                Descripci&#243;n
            </td>
            <td>
                Estado *
            </td>
        </tr>
        <tr class="rowA">
            <td class="style1">
                <asp:TextBox runat="server" ID="txtDescripcionTipoContrato"  Enabled="false" 
                    Height="50px" MaxLength="128" TextMode="MultiLine" Width="320px"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal"  Enabled="false"></asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" 
    contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 348px;
        }
    </style>
</asp:Content>

