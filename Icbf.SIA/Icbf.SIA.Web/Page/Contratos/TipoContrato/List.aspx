<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_TipoContrato_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                //limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td class="style1" style="width: 50%">
                    <asp:label runat="server" ID="lblNombreTipoContrato" Text="Nombre Tipo de Contrato"></asp:label> 
                </td>
                <td style="width: 50%">
                    Categor&#237;a del Contrato
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:TextBox runat="server" ID="txtNombreTipoContrato" Height="50px" MaxLength="128"
                        TextMode="MultiLine" Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreTipoContrato" runat="server" TargetControlID="txtNombreTipoContrato"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; "
                        InvalidChars="&#63;" />
                </td>
                <td valign="top">
                    <asp:DropDownList runat="server" ID="ddlIdCategoriaContrato" Height="25px" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB"><td class="style1">
                    Estado
                </td></tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
            </table>
            <table width="90%" align="center" runat="server" Visible="False">
            <tr class="rowB">
                <td class="style1">
                    Requiere Acta de Inicio
                </td>
                <td class="style1">
                    Requiere Aportes o Cofinanciaci&#243;n
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:RadioButtonList runat="server" ID="rblRequiereActa" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td class="style1">
                    <asp:RadioButtonList runat="server" ID="rblRequiereAportes" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td class="style1">
                    Recursos Financieros
                </td>
                <td class="style1">
                    R&#233;gimen de Contrataci&#243;n
                </td>
            </tr>
            <tr class="rowA">
                <td class="style1">
                    <asp:RadioButtonList runat="server" ID="rblRecursosFinancieros" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td valign="top">
                    <asp:DropDownList runat="server" ID="ddlRegimen" Height="25px" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                
                <td class="style1">
                    Descripci&#243;n
                </td>
                
            </tr>
            <tr class="rowA">
                
                <td class="style1">
                    <asp:TextBox runat="server" ID="txtDescripcion" Height="50px" MaxLength="128" TextMode="MultiLine"
                        Width="320px" onKeyDown="limitText(this,128);" onKeyUp="limitText(this,128);"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftescripcion" runat="server" TargetControlID="txtDescripcion"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="&#225;&#233;&#237;&#243;&#250;&#193;&#201;&#205;&#211;&#218;&#241;&#209; "
                        InvalidChars="&#63;" />
                </td>
                
            </tr>
        </table>
    </asp:Panel>
    <div id="Layer1" style="width: 95%; overflow-x: scroll;">
        <asp:Panel runat="server" ID="pnlLista">
            <table width="100%" align="center">
                <tr class="rowAG">
                    <td>
                        <asp:GridView runat="server" ID="gvTipoContrato" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" Width="100%" DataKeyNames="IdTipoContrato" CellPadding="0" Height="16px"
                            OnPageIndexChanging="gvTipoContrato_PageIndexChanging" OnSelectedIndexChanged="gvTipoContrato_SelectedIndexChanged"
                            AllowSorting="True" OnSorting="gvTipoContrato_Sorting">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                            Height="16px" Width="16px" ToolTip="Detalle" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderText="Tipo Contrato" DataField="NombreTipoContrato" SortExpression="NombreTipoContrato" />--%>
                                <asp:TemplateField HeaderText="Nombre Tipo de Contrato" ItemStyle-HorizontalAlign="Center" SortExpression="NombreTipoContrato">
                                    <ItemTemplate>
                                        <div style="word-wrap: break-word; width: 150px;">
                                            <%#Eval("NombreTipoContrato")%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Categor&#237;a del Contrato" DataField="NombreCategoriaContrato"
                                    SortExpression="NombreCategoriaContrato" />
                                <%--<asp:BoundField HeaderText="Requiere Acta de Inicio" DataField="ActaInicioString"
                                    SortExpression="ActaInicioString" />--%>
                                <%--<asp:BoundField HeaderText="Requiere Aportes o Cofinanciaci&#243;n" DataField="AporteCofinanciacionString"
                                    SortExpression="AporteCofinanciacionString" />--%>
                                <%--<asp:BoundField HeaderText="Recursos Financieros" DataField="RecursoFinancieroString"
                                    SortExpression="RecursoFinancieroString" />--%>
                                <%--<asp:CheckBoxField HeaderText="Acta de Inicio" DataField="ActaInicio" />
                            <asp:CheckBoxField HeaderText="Aportes" DataField="AporteCofinaciacion" />
                            <asp:CheckBoxField HeaderText="Recursos" DataField="RecursoFinanciero" />--%>
                                <%--<asp:BoundField HeaderText="R&#233;gimen de Financiaci&#243;n" DataField="NombreRegimenContratacion" SortExpression="NombreRegimenContratacion" />--%>
                              <%--<asp:TemplateField HeaderText="R&#233;gimen de Contrataci&#243;n" ItemStyle-HorizontalAlign="Center"  SortExpression="NombreRegimenContratacion">
                                    <ItemTemplate>
                                        <div style="word-wrap: break-word; width: 150px;">
                                            <%#Eval("NombreRegimenContratacion")%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <%--<asp:BoundField HeaderText="Descripci&#243;n" DataField="DescripcionTipoContrato" SortExpression="DescripcionTipoContrato" />--%>
                                <asp:TemplateField HeaderText="Descripci&#243;n" ItemStyle-HorizontalAlign="Center" SortExpression="DescripcionTipoContrato">
                                    <ItemTemplate>
                                        <div style="word-wrap: break-word; width: 150px;">
                                            <%#Eval("DescripcionTipoContrato")%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Estado" DataField="EstadoString" SortExpression="EstadoString" />
                            </Columns>
                            <AlternatingRowStyle CssClass="rowBG" />
                            <EmptyDataRowStyle CssClass="headerForm" />
                            <HeaderStyle CssClass="headerForm" />
                            <RowStyle CssClass="rowAG" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <style type="text/css">
        .style1
        {
            width: 426px;
        }
    </style>
</asp:Content>
