using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página de registro y edición de tipos de contrato
/// </summary>
public partial class Page_TipoContrato_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    string PageName = "Contratos/TipoContrato";

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn guardar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Almacena Informaciòn del formulario en tabla
    /// </summary>
    private void Guardar()
    {
        try
        {
            int vResultado;
            TipoContrato vTipoContrato = new TipoContrato();

            vTipoContrato.NombreTipoContrato = Convert.ToString(txtNombreTipoContrato.Text);
            vTipoContrato.IdCategoriaContrato = Convert.ToInt32(ddlIdCategoriaContrato.SelectedValue);
            vTipoContrato.ActaInicio = rblActaInicio.SelectedValue == "1" ? true : false;
            vTipoContrato.AporteCofinaciacion = rblAporteCofinaciacion.SelectedValue == "1" ? true : false;
            vTipoContrato.RecursoFinanciero = rblRecursoFinanciero.SelectedValue=="1"?true:false;
            vTipoContrato.RegimenContrato = Convert.ToInt32(ddlRegimenContrato.SelectedValue);
            vTipoContrato.DescripcionTipoContrato = Convert.ToString(txtDescripcionTipoContrato.Text);
            vTipoContrato.Estado = rblEstado.SelectedValue == "1" ? true : false;

            if (Request.QueryString["oP"] == "E")
            {
            vTipoContrato.IdTipoContrato = Convert.ToInt32(hfIdTipoContrato.Value);
                vTipoContrato.UsuarioModifica = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.ModificarTipoContrato(vTipoContrato);
            }
            else
            {
                vTipoContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                InformacionAudioria(vTipoContrato, this.PageName, vSolutionPage);
                vResultado = vContratoService.InsertarTipoContrato(vTipoContrato);
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("TipoContrato.IdTipoContrato", vTipoContrato.IdTipoContrato);
                if (Request.QueryString["oP"] == "E")
                {
                    SetSessionParameter("TipoContrato.Modificado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
                else
                {
                    SetSessionParameter("TipoContrato.Guardado", "1");
                    NavigateTo(SolutionPage.Detail);
                }
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Contrato", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarRegistro()
    {
        try
        {
            //int vIdTipoContrato = Convert.ToInt32(GetSessionParameter("TipoContrato.IdTipoContrato"));
            int vIdTipoContrato = 0;

            if (int.TryParse(GetSessionParameter("TipoContrato.IdTipoContrato").ToString(), out vIdTipoContrato))
            {
                RemoveSessionParameter("TipoContrato.Id");

                TipoContrato vTipoContrato = new TipoContrato();
                vTipoContrato = vContratoService.ConsultarTipoContrato(vIdTipoContrato);
                hfIdTipoContrato.Value = vTipoContrato.IdTipoContrato.ToString();
                txtNombreTipoContrato.Text = vTipoContrato.NombreTipoContrato;
                ddlIdCategoriaContrato.SelectedValue = vTipoContrato.IdCategoriaContrato.ToString();
                rblActaInicio.SelectedValue = vTipoContrato.ActaInicio == true ? "1" : "0";
                rblAporteCofinaciacion.SelectedValue = vTipoContrato.AporteCofinaciacion == true ? "1" : "0";
                rblRecursoFinanciero.SelectedValue = vTipoContrato.RecursoFinanciero == true ? "1" : "0";
                ddlRegimenContrato.SelectedValue = vTipoContrato.RegimenContrato.ToString();
                txtDescripcionTipoContrato.Text = vTipoContrato.DescripcionTipoContrato;
                rblEstado.SelectedValue = vTipoContrato.Estado == true ? "1" : "0";
                ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoContrato.UsuarioCrea, vTipoContrato.FechaCrea, vTipoContrato.UsuarioModifica, vTipoContrato.FechaModifica);
            }
            else
            {
                NavigateTo(SolutionPage.List);
            }
            
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {

            // CARGA COMBO 14/08/2013
            ContratoService objContratoService = new ContratoService();
            ManejoControlesContratos.LlenarComboLista(ddlIdCategoriaContrato, objContratoService.ConsultarCategoriaContratos(null, null, true), "IdCategoriaContrato", "NombreCategoriaContrato");



            /*Coloque aqui el codigo de llenar el combo.*/
            rblActaInicio.Items.Insert(0, new ListItem("Si", "1"));
            rblActaInicio.Items.Insert(1, new ListItem("No", "0"));
            /*Coloque aqui el codigo de llenar el combo.*/
            rblAporteCofinaciacion.Items.Insert(0, new ListItem("Si", "1"));
            rblAporteCofinaciacion.Items.Insert(1, new ListItem("No", "0"));
            /*Coloque aqui el codigo de llenar el combo.*/
            rblRecursoFinanciero.Items.Insert(0, new ListItem("Si", "1"));
            rblRecursoFinanciero.Items.Insert(1, new ListItem("No", "0"));
            

            // CARGA COMBO 14/08/2013
            ManejoControlesContratos.LlenarComboLista(ddlRegimenContrato, objContratoService.ConsultarRegimenContratacions(null, null, true), "IdRegimenContratacion", "NombreRegimenContratacion");


            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
            rblEstado.SelectedValue = "1";
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
