using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

/// <summary>
/// Página que despliega el detalle del registro de tipo contrato
/// </summary>
public partial class Page_TipoContrato_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar pàgina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
            else
            {
                RemoveSessionParameter("TipoContrato.Guardado");
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el botòn Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Manejador de eventos click para el botòn Editar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("TipoContrato.IdTipoContrato", hfIdTipoContrato.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }

    /// <summary>
    /// Manejador de evento click para el botòn Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    /// <summary>
    /// Carga datos a los controles del formulario
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            int vIdTipoContrato = Convert.ToInt32(GetSessionParameter("TipoContrato.IdTipoContrato"));
            RemoveSessionParameter("TipoContrato.IdTipoContrato");

            if (GetSessionParameter("TipoContrato.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("TipoContrato");

            if (GetSessionParameter("TipoContrato.Modificado").ToString() == "1")
                toolBar.MostrarMensajeGuardado("La informaci&#243;n ha sido modificada exitosamente");
            RemoveSessionParameter("TipoContrato.Modificado");

            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato = vContratoService.ConsultarTipoContrato(vIdTipoContrato);
            hfIdTipoContrato.Value = vTipoContrato.IdTipoContrato.ToString();
            txtNombreTipoContrato.Text = vTipoContrato.NombreTipoContrato;
            ddlIdCategoriaContrato.SelectedValue = vTipoContrato.IdCategoriaContrato.ToString();
            rblActaInicio.SelectedValue = vTipoContrato.ActaInicio == true ? "1" : "0";
            rblAporteCofinaciacion.SelectedValue = vTipoContrato.AporteCofinaciacion == true ? "1" : "0";
            rblRecursoFinanciero.SelectedValue = vTipoContrato.RecursoFinanciero == true ? "1" : "0";
            ddlRegimenContrato.SelectedValue = vTipoContrato.RegimenContrato.ToString();
            txtDescripcionTipoContrato.Text = vTipoContrato.DescripcionTipoContrato;
            rblEstado.SelectedValue = vTipoContrato.Estado == true ? "1" : "0";
            ObtenerAuditoria(PageName, hfIdTipoContrato.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vTipoContrato.UsuarioCrea, vTipoContrato.FechaCrea, vTipoContrato.UsuarioModifica, vTipoContrato.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdTipoContrato = Convert.ToInt32(hfIdTipoContrato.Value);

            TipoContrato vTipoContrato = new TipoContrato();
            vTipoContrato = vContratoService.ConsultarTipoContrato(vIdTipoContrato);
            InformacionAudioria(vTipoContrato, this.PageName, vSolutionPage);
            int vResultado = vContratoService.EliminarTipoContrato(vTipoContrato);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("TipoContrato.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece tìtulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.LipiarMensajeError();
            toolBar.EstablecerTitulos("Tipo Contrato", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            /*Coloque aqui el codigo de llenar el combo.*/
            ContratoService objContratoService = new ContratoService();
            ddlIdCategoriaContrato.DataSource = objContratoService.ConsultarCategoriaContratos(null,null,true);
            ddlIdCategoriaContrato.DataTextField = "NombreCategoriaContrato";
            ddlIdCategoriaContrato.DataValueField = "IdCategoriaContrato";
            ddlIdCategoriaContrato.DataBind();
            /*Coloque aqui el codigo de llenar el combo.*/
            rblActaInicio.Items.Insert(0, new ListItem("Si", "1"));
            rblActaInicio.Items.Insert(1, new ListItem("No", "0"));
            /*Coloque aqui el codigo de llenar el combo.*/
            rblAporteCofinaciacion.Items.Insert(0, new ListItem("Si", "1"));
            rblAporteCofinaciacion.Items.Insert(1, new ListItem("No", "0"));
            /*Coloque aqui el codigo de llenar el combo.*/
            rblRecursoFinanciero.Items.Insert(0, new ListItem("Si", "1"));
            rblRecursoFinanciero.Items.Insert(1, new ListItem("No", "0"));
            /*Coloque aqui el codigo de llenar el combo.*/
            ddlRegimenContrato.DataSource = objContratoService.ConsultarRegimenContratacions(null,null,true);
            ddlRegimenContrato.DataTextField = "NombreRegimenContratacion";
            ddlRegimenContrato.DataValueField = "IdRegimenContratacion";
            ddlRegimenContrato.DataBind();
            /*Coloque aqui el codigo de llenar el combo.*/
            rblEstado.Items.Insert(0, new ListItem("Activo", "1"));
            rblEstado.Items.Insert(1, new ListItem("Inactivo", "0"));
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
}
