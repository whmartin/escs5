using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Linq.Expressions;

/// <summary>
/// P�gina que despliega la consulta basada en filtros de tipos contrato
/// </summary>
public partial class Page_TipoContrato_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/TipoContrato";
    ContratoService vContratoService = new ContratoService();

    /// <summary>
    /// Manejador del evento PreInit de la p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    /// <summary>
    /// Manejador del evento Cargar p�gina
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
            else
            {
                toolBar.LipiarMensajeError();
            }
        }
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Buscar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    /// <summary>
    /// Manejador de evento click para el bot�n Nuevo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    /// <summary>
    /// Realiza la b�squeda con los filtros proporcionados y carga resultado en grilla
    /// </summary>
    private void Buscar()
    {
        try
        {
            String vNombreTipoContrato = null;
            int? vIdCategoriaContrato = null;
            Boolean? vEstado = null;
            Boolean? vActaInicio = null;
            Boolean? vAporteCofinanciacion = null;
            Boolean? vRecursosFinancieros = null;
            int? vRegimenContrato = null;
            String vDescripcionTipoContrato = null;

            if (txtNombreTipoContrato.Text != "")
            {
                vNombreTipoContrato = Convert.ToString(txtNombreTipoContrato.Text);
            }
            if (ddlIdCategoriaContrato.SelectedValue != "-1")
            {
                vIdCategoriaContrato = Convert.ToInt32(ddlIdCategoriaContrato.SelectedValue);
            }

            if (rblEstado.SelectedValue == "")
            {
                vEstado = null;
            }
            else
            {
                vEstado = rblEstado.SelectedValue == "True" ? true : false;
            }

            

            if (rblRequiereActa.SelectedValue == "")
            {
                vActaInicio = null;
            }
            else
            {
                vActaInicio = rblRequiereActa.SelectedValue == "True" ? true : false;
            }
            

            if (rblRequiereAportes.SelectedValue == "")
            {
                vAporteCofinanciacion = null;
            }
            else
            {
                vAporteCofinanciacion = rblRequiereAportes.SelectedValue == "True" ? true : false;
            }
            

            if (rblRecursosFinancieros.SelectedValue == "")
            {
                vRecursosFinancieros = null;
            }
            else
            {
                vRecursosFinancieros = rblRecursosFinancieros.SelectedValue == "True" ? true : false;
            }


            if (ddlRegimen.SelectedValue != "-1")
            {
                vRegimenContrato = Convert.ToInt32(ddlRegimen.SelectedValue);
            }

            if (txtDescripcion.Text != "")
            {
                vDescripcionTipoContrato = Convert.ToString(txtDescripcion.Text);
            }
            var result = from tipoCOntrato in vContratoService.ConsultarTipoContratos(vNombreTipoContrato, vIdCategoriaContrato, vEstado, vActaInicio, vAporteCofinanciacion, vRecursosFinancieros, vRegimenContrato, vDescripcionTipoContrato)
                         join categoriaContrato in vContratoService.ConsultarCategoriaContratos(null, null, true)
                         on tipoCOntrato.IdCategoriaContrato equals categoriaContrato.IdCategoriaContrato
                         join regimenContratacion in vContratoService.ConsultarRegimenContratacions(null, null, true)
                         on tipoCOntrato.RegimenContrato equals regimenContratacion.IdRegimenContratacion
                         select new
                         {
                             tipoCOntrato.IdTipoContrato,
                             tipoCOntrato.NombreTipoContrato,
                             tipoCOntrato.DescripcionTipoContrato,
                             tipoCOntrato.ActaInicio,
                             tipoCOntrato.AporteCofinaciacion,
                             tipoCOntrato.RecursoFinanciero,
                             tipoCOntrato.Estado,
                             categoriaContrato.NombreCategoriaContrato,
                             regimenContratacion.NombreRegimenContratacion
                         };

            List<datosGridView> listaTipoContrato = new List<datosGridView>();

            foreach (var datosResult in result)
            {
                datosGridView objDatosGridView = new datosGridView();
                objDatosGridView.IdTipoContrato = datosResult.IdTipoContrato;
                objDatosGridView.NombreTipoContrato = datosResult.NombreTipoContrato;
                objDatosGridView.DescripcionTipoContrato = datosResult.DescripcionTipoContrato;
                objDatosGridView.ActaInicio = datosResult.ActaInicio;
                objDatosGridView.AporteCofinaciacion = datosResult.AporteCofinaciacion;
                objDatosGridView.RecursoFinanciero = datosResult.RecursoFinanciero;
                objDatosGridView.NombreCategoriaContrato = datosResult.NombreCategoriaContrato;
                objDatosGridView.NombreRegimenContratacion = datosResult.NombreRegimenContratacion;

                if (datosResult.ActaInicio)
                {
                    objDatosGridView.ActaInicioString = "Si";
                }
                else
                {
                    objDatosGridView.ActaInicioString = "No";
                }

                if (datosResult.AporteCofinaciacion)
                {
                    objDatosGridView.AporteCofinanciacionString = "Si";
                }
                else
                {
                    objDatosGridView.AporteCofinanciacionString = "No";
                }

                if (datosResult.RecursoFinanciero)
                {
                    objDatosGridView.RecursoFinancieroString = "Si";
                }
                else
                {
                    objDatosGridView.RecursoFinancieroString = "No";
                }

                if (datosResult.Estado)
                {
                    objDatosGridView.EstadoString = "Activo";
                }
                else
                {
                    objDatosGridView.EstadoString = "Inactivo";
                }
                listaTipoContrato.Add(objDatosGridView);
            }

            gvTipoContrato.DataSource = listaTipoContrato.ToList();
            gvTipoContrato.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Inicializa instancia master, crea manejadores de eventos, y establece t�tulo
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);

            toolBar.LipiarMensajeError();
            gvTipoContrato.PageSize = PageSize();
            gvTipoContrato.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Tipo Contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Selecciona registro de la grilla
    /// </summary>
    /// <param name="pRow"></param>
    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvTipoContrato.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("TipoContrato.IdTipoContrato", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvTipoContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvTipoContrato.SelectedRow);
    }
    protected void gvTipoContrato_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTipoContrato.PageIndex = e.NewPageIndex;
        Buscar();
    }

    /// <summary>
    /// Inicializa valores de controles
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoContrato.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoContrato.Eliminado");
            /*Coloque aqui el codigo de llenar el combo.*/

            ContratoService objContratoService = new ContratoService();
            ManejoControlesContratos.LlenarComboLista(ddlIdCategoriaContrato, objContratoService.ConsultarCategoriaContratos(null, null, true), "IdCategoriaContrato", "NombreCategoriaContrato");
            /*Coloque aqui el codigo de llenar el combo.*/
            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo");
            rblEstado.SelectedIndex = -1;

            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereActa, "Si", "No");
            rblRequiereActa.SelectedIndex = -1;

            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRequiereAportes, "Si", "No");
            rblRequiereAportes.SelectedIndex = -1;

            ManejoControlesContratos.ValoresTrueFalseRadioButtonList(rblRecursosFinancieros, "Si", "No");
            rblRecursosFinancieros.SelectedIndex = -1;

            ManejoControlesContratos.LlenarComboLista(ddlRegimen, objContratoService.ConsultarRegimenContratacions(null, null, true), "IdRegimenContratacion", "NombreRegimenContratacion");

            ///*Coloque aqui el codigo de llenar el combo.*/
            //var resultRegimenContratacion = from regimenContratacion in objContratoService.ConsultarRegimenContratacions(null, null, true)
            //                                where regimenContratacion.Estado == true
            //                                select new { regimenContratacion.IdRegimenContratacion, regimenContratacion.NombreRegimenContratacion };
            //ddlRegimen.DataSource = resultRegimenContratacion.ToList();
            //ddlRegimen.DataTextField = "NombreRegimenContratacion";
            //ddlRegimen.DataValueField = "IdRegimenContratacion";
            //ddlRegimen.DataBind();

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }
    protected void gvTipoContrato_Sorting(object sender, GridViewSortEventArgs e)
    {
        Ordenar(e);
    }
    public void Ordenar(GridViewSortEventArgs e)
    {
        String vNombreTipoContrato = null;
        int? vIdCategoriaContrato = null;
        Boolean? vEstado = null;
        Boolean? vActaInicio = null;
        Boolean? vAporteCofinanciacion = null;
        Boolean? vRecursosFinancieros = null;
        int? vRegimenContrato = null;
        String vDescripcionTipoContrato = null;

        if (txtNombreTipoContrato.Text != "")
        {
            vNombreTipoContrato = Convert.ToString(txtNombreTipoContrato.Text);
        }
        if (ddlIdCategoriaContrato.SelectedValue != "-1")
        {
            vIdCategoriaContrato = Convert.ToInt32(ddlIdCategoriaContrato.SelectedValue);
        }

        if (rblEstado.SelectedValue == "")
        {
            vEstado = null;
        }
        else
        {
            vEstado = rblEstado.SelectedValue == "True" ? true : false;
        }


        var myGridResults = from tipoCOntrato in vContratoService.ConsultarTipoContratos(vNombreTipoContrato, vIdCategoriaContrato, vEstado, vActaInicio, vAporteCofinanciacion, vRecursosFinancieros, vRegimenContrato, vDescripcionTipoContrato)
                            join categoriaContrato in vContratoService.ConsultarCategoriaContratos(null, null, true)
                            on tipoCOntrato.IdCategoriaContrato equals categoriaContrato.IdCategoriaContrato
                            join regimenContratacion in vContratoService.ConsultarRegimenContratacions(null, null, true)
                            on tipoCOntrato.RegimenContrato equals regimenContratacion.IdRegimenContratacion
                            select new
                            {
                                tipoCOntrato.IdTipoContrato,
                                tipoCOntrato.NombreTipoContrato,
                                tipoCOntrato.DescripcionTipoContrato,
                                tipoCOntrato.ActaInicio,
                                tipoCOntrato.AporteCofinaciacion,
                                tipoCOntrato.RecursoFinanciero,
                                tipoCOntrato.Estado,
                                categoriaContrato.NombreCategoriaContrato,
                                regimenContratacion.NombreRegimenContratacion
                            };

        List<datosGridView> listaTipoContrato = new List<datosGridView>();

        foreach (var datosResult in myGridResults)
        {
            datosGridView objDatosGridView = new datosGridView();
            objDatosGridView.IdTipoContrato = datosResult.IdTipoContrato;
            objDatosGridView.NombreTipoContrato = datosResult.NombreTipoContrato;
            objDatosGridView.DescripcionTipoContrato = datosResult.DescripcionTipoContrato;
            objDatosGridView.ActaInicio = datosResult.ActaInicio;
            objDatosGridView.AporteCofinaciacion = datosResult.AporteCofinaciacion;
            objDatosGridView.RecursoFinanciero = datosResult.RecursoFinanciero;
            objDatosGridView.NombreCategoriaContrato = datosResult.NombreCategoriaContrato;
            objDatosGridView.NombreRegimenContratacion = datosResult.NombreRegimenContratacion;

            if (datosResult.ActaInicio)
            {
                objDatosGridView.ActaInicioString = "Si";
            }
            else
            {
                objDatosGridView.ActaInicioString = "No";
            }

            if (datosResult.AporteCofinaciacion)
            {
                objDatosGridView.AporteCofinanciacionString = "Si";
            }
            else
            {
                objDatosGridView.AporteCofinanciacionString = "No";
            }

            if (datosResult.RecursoFinanciero)
            {
                objDatosGridView.RecursoFinancieroString = "Si";
            }
            else
            {
                objDatosGridView.RecursoFinancieroString = "No";
            }

            if (datosResult.Estado)
            {
                objDatosGridView.EstadoString = "Activo";
            }
            else
            {
                objDatosGridView.EstadoString = "Inactivo";
            }
            listaTipoContrato.Add(objDatosGridView);
        }

        if (myGridResults != null)
        {
            //Dependiendo del modo de ordenamiento . . .
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                switch (e.SortExpression)
                {
                    case ("NombreTipoContrato"):
                        listaTipoContrato = listaTipoContrato.OrderBy(x => x.NombreTipoContrato).ToList();
                        break;
                    case ("DescripcionTipoContrato"):
                        listaTipoContrato = listaTipoContrato.OrderBy(x => x.DescripcionTipoContrato).ToList();
                        break;
                    case ("NombreCategoriaContrato"):
                        listaTipoContrato = listaTipoContrato.OrderBy(x => x.NombreCategoriaContrato).ToList();
                        break;
                    case ("NombreRegimenContratacion"):
                        listaTipoContrato = listaTipoContrato.OrderBy(x => x.NombreRegimenContratacion).ToList();
                        break;
                    case ("EstadoString"):
                        listaTipoContrato = listaTipoContrato.OrderBy(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvTipoContrato.DataSource = listaTipoContrato;
                GridViewSortDirection = SortDirection.Descending;
            }
            else
            {
                switch (e.SortExpression)
                {
                    case ("NombreTipoContrato"):
                        listaTipoContrato = listaTipoContrato.OrderByDescending(x => x.NombreTipoContrato).ToList();
                        break;
                    case ("DescripcionTipoContrato"):
                        listaTipoContrato = listaTipoContrato.OrderByDescending(x => x.DescripcionTipoContrato).ToList();
                        break;
                    case ("NombreCategoriaContrato"):
                        listaTipoContrato = listaTipoContrato.OrderByDescending(x => x.NombreCategoriaContrato).ToList();
                        break;
                    case ("NombreRegimenContratacion"):
                        listaTipoContrato = listaTipoContrato.OrderByDescending(x => x.NombreRegimenContratacion).ToList();
                        break;
                    case ("EstadoString"):
                        listaTipoContrato = listaTipoContrato.OrderByDescending(x => x.EstadoString).ToList();
                        break;
                    default:
                        break;
                }
                gvTipoContrato.DataSource = listaTipoContrato;
                GridViewSortDirection = SortDirection.Ascending;
            }

            gvTipoContrato.DataBind();
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        NavigateTo("~/Page/Contratos/TablaParametrica/List.aspx");
    }
}

public class datosGridView
{
    public int IdTipoContrato { get; set; }
    public String NombreTipoContrato { get; set; }
    public String DescripcionTipoContrato { get; set; }
    public bool ActaInicio { get; set; }
    public String ActaInicioString { get; set; }
    public bool AporteCofinaciacion { get; set; }
    public String AporteCofinanciacionString { get; set; }
    public bool RecursoFinanciero { get; set; }
    public String RecursoFinancieroString { get; set; }
    public String EstadoString { get; set; }
    public String NombreCategoriaContrato { get; set; }
    public String NombreRegimenContratacion { get; set; }

    public datosGridView()
    {

    }
}
