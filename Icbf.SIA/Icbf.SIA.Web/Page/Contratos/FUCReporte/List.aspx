<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_FUCReporte_List" %>
<%@ Register Src="../../../General/General/Control/fecha.ascx" TagName="fecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    
    <script type="text/javascript" language="javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function muestraImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "visible";
        }

        function ocultaImagenLoading() {
            var imgLoading = document.getElementById("imgLoading");
            imgLoading.style.visibility = "";
        }

        $(document).ready(function () {

            ocultaImagenLoading();
        });


        $(function () {
            $('[id*=ListRegional]').multiselect({
                includeSelectAllOption: true, maxHeight: 200, nonSelectedText: "Seleccione", numberDisplayed: 1,
                nSelectedText: 'Seleccionado(s)',
            });
        });

    </script>
    <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td class="Cell">
                Tipo de Busqueda * <asp:RequiredFieldValidator ID="rfvTipoBusqueda" runat="server" ControlToValidate="RbtListTipoBusqueda" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">
                Vigencia Fiscal Inicial * <asp:RequiredFieldValidator ID="rfvVigencia" runat="server" ControlToValidate="ddlVigenciaFiscalinicial"  Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red" SetFocusOnError="True" ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
        </tr>
                <tr class="rowA">
            <td class="Cell">
                <asp:RadioButtonList ID="RbtListTipoBusqueda" AutoPostBack="true" OnSelectedIndexChanged="RbtListTipoBusqueda_SelectedIndexChanged" RepeatDirection="Horizontal"  runat="server">
                    <asp:ListItem Text="Actual" Value="1" />
                    <asp:ListItem Text="Historico" Value="0" />
                </asp:RadioButtonList>
                    </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlVigenciaFiscalinicial" runat="server" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlVigenciaFiscalinicial_SelectedIndexChanged">
                </asp:DropDownList>
                    </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">Fecha Inicio Desde *<asp:RequiredFieldValidator ID="rfvFechaInicio0" runat="server" ControlToValidate="txtFechaInicio" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red"  ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
            <td class="Cell">Fecha Inicio hasta *<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFechaFinalizacion" Display="Dynamic" Enabled="true" ErrorMessage="Campo Requerido" ForeColor="Red"  ValidationGroup="btnReporte"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td class="Cell">

                                          <asp:TextBox runat="server" ID="txtFechaInicio" Enabled="false" Height="19px" ></asp:TextBox>
                <asp:Image ID="imgFechaInicio" runat="server"  CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand"  />
                <Ajax:CalendarExtender ID="cetxtfechainicio" Enabled="false" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFechaInicio" TargetControlID="txtFechaInicio">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicio">
                </Ajax:MaskedEditExtender>
                &nbsp;<asp:CompareValidator ID="cvFechaInicio" runat="server" ControlToValidate="txtFechaInicio"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="false" Type="Date" ValidationGroup="btnReporte" Display="Dynamic"></asp:CompareValidator>

            </td>
            <td class="Cell">
                <asp:TextBox runat="server" ID="txtFechaFinalizacion" Enabled="false"  Height="19px"  ></asp:TextBox>
                <asp:Image ID="imgFechaFin" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"  />
                <Ajax:CalendarExtender ID="cetxtfechaFinal" Enabled="false" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFechaFin" TargetControlID="txtFechaFinalizacion">
                </Ajax:CalendarExtender>
                 <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaFinalizacion">
                </Ajax:MaskedEditExtender>
                &nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFechaFinalizacion"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="false" Type="Date" ValidationGroup="btnReporte" Display="Dynamic"></asp:CompareValidator>   
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Regional Contrato/Convenio *</td>
            <td class="Cell">
                Tipo de Persona</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:ListBox ID="ListRegional" Enabled="false" AutoPostBack="true" EnableViewState="true" SelectionMode="Multiple"  OnSelectedIndexChanged="ListRegional_SelectedIndexChanged1" runat="server">
                </asp:ListBox>
            </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlIDTipoPersona" Enabled="false" runat="server" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Modalidad de Selección
            </td>
            <td class="Cell">
                Categoria del Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList ID="ddlIDModalidadSeleccion" Enabled="false" runat="server">
                </asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlIDCategoriaContrato" Enabled="false" runat="server" AutoPostBack="True" onselectedindexchanged="ddlIDCategoriaContrato_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="rowB">
            <td class="Cell">
                Tipo de Contrato/Convenio</td>
            <td class="Cell">
                Estado del Contrato/Convenio</td>
        </tr>
        <tr class="rowA">
            <td class="Cell">
                <asp:DropDownList ID="ddlIDTipoContrato" Enabled="false" runat="server">
                </asp:DropDownList>
            </td>
            <td class="Cell">
                <asp:DropDownList ID="ddlIDEstadoContraro" Enabled="false" runat="server" >
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    </asp:Content>

