using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;

/// <summary>
/// Página de consulta a través de filtros para la entidad Contratos
/// </summary>
public partial class Page_FUCReporte_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/FUCReporte";
    SIAService vRuboService = new SIAService();
    ContratoService vContratoService = new ContratoService();
  
    private const string CodConvenioMarco = "CM";
    private const string CodContratoConvenioAdhesion = "CCA";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        ddlIDCategoriaContrato.SelectedIndex = 0;
        ddlIDEstadoContraro.SelectedIndex = 0;
        ddlIDModalidadSeleccion.SelectedIndex = 0;
        ListRegional.SelectedIndex = 0;
        ddlIDTipoContrato.SelectedIndex = 0;
        ddlVigenciaFiscalinicial.SelectedIndex = 0;
        ddlIDTipoContrato.Enabled = false;
        ddlIDTipoPersona.SelectedIndex = 0;
        toolBar.LipiarMensajeError();
        HabilitarFiltrosAdcionales(false);
        cetxtfechainicio.Enabled = false;
        txtFechaFinalizacion.Enabled = false;
        ListRegional.Enabled = false;
        cetxtfechaFinal.Enabled = false;
        txtFechaInicio.Enabled = false;
        ListRegional.Enabled = false;
        ddlVigenciaFiscalinicial.Enabled = false;
        RbtListTipoBusqueda.SelectedIndex = -1;
    }

    /// <summary>
        /// Método que establece el título del módulo e inicializa los delegados de los botones 
        /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoReporte += new ToolBarDelegate(btnReporte_Click);
            toolBar.eventoLimpiar += new ToolBarDelegate(btnLimpiar_Click);
            toolBar.MostrarBotonEditar(false);
            toolBar.MostrarBotonNuevo(false);
            toolBar.OcultarBotonBuscar(true);
            toolBar.EstablecerTitulos("Reporte de Gestión de Contratos", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void btnReporte_Click(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        GenerarReporte(true);
    }

    private void GenerarReporte(bool pPrevisualizar = false)
    {
        try
        {
            int vVigenciaFiscalinicial;
            DateTime vFechaRegistroSistemaDesde;
            DateTime vFechaRegistroSistemaHasta;
            string vIDRegional;
            int? vIDModalidadSeleccion = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;
            int? vIDEstadoContraro = null;
            int? vIDTipoPersona = null;



            Report objReport;

            if (pPrevisualizar)
                objReport = new Report("ReporteFUC", false, PageName, "Reporte FUC");
            else
                objReport = new Report("ReporteFUC", true, PageName, "Reporte FUC", true, "ReporteFUC");

            

            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
            {
                vVigenciaFiscalinicial = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
                objReport.AddParameter("VigenciaFiscalInicial", vVigenciaFiscalinicial.ToString());
            }
            else
            {
                toolBar.MostrarMensajeError("Debe ingresar la vigencía fiscal inicial.");
                return;
            }

            if (DateTime.TryParse(txtFechaInicio.Text, out vFechaRegistroSistemaDesde) && DateTime.TryParse(txtFechaFinalizacion.Text, out vFechaRegistroSistemaHasta))
            {
                if (vFechaRegistroSistemaDesde > vFechaRegistroSistemaHasta)
                {
                    toolBar.MostrarMensajeError("la Fecha desde, debe ser mayor o igual a la fecha hasta.");
                    return;                    
                }

                objReport.AddParameter("FechaDesde", vFechaRegistroSistemaDesde.ToShortDateString());
                objReport.AddParameter("FechaHasta", vFechaRegistroSistemaHasta.ToShortDateString());
            }
            else
            {
                toolBar.MostrarMensajeError("El formato de la fecha de inicio y/o fecha de fin es incorrecto");
                return;
            }

            string regional = string.Empty;

            foreach (ListItem item in ListRegional.Items)
            {
                if(item.Selected)
                    regional += item.Value + ",";
            }

            if (!string.IsNullOrEmpty(regional))
            {
                objReport.AddParameter("Regional", regional.ToString());
            }
            else
            {
                toolBar.MostrarMensajeError("Debe Seleccionar la regional del Reporte");
                return;
            }

            if (ddlIDModalidadSeleccion.SelectedValue != "-1")
            {
                vIDModalidadSeleccion = Convert.ToInt32(ddlIDModalidadSeleccion.SelectedValue);
                objReport.AddParameter("ModalidadSeleccion", vIDModalidadSeleccion.ToString());
            }
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
            {
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
                objReport.AddParameter("CategoriaContrato", vIDCategoriaContrato.ToString());
            }
            if (ddlIDTipoContrato.SelectedValue != "-1")
            {
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
                objReport.AddParameter("TipoContrato", vIDTipoContrato.ToString());
            }
            if (ddlIDEstadoContraro.SelectedValue != "-1")
            {
                vIDEstadoContraro = Convert.ToInt32(ddlIDEstadoContraro.SelectedValue);
                objReport.AddParameter("EstadoContrato", vIDEstadoContraro.ToString());
            }

            if (ddlIDTipoPersona.SelectedValue != "-1")
            {
                vIDTipoPersona = Convert.ToInt32(ddlIDTipoPersona.SelectedValue);
                objReport.AddParameter("TipoPersona", vIDTipoPersona.ToString());                
            }

            SetSessionParameter("Report", objReport);

            string url = string.Empty;

            if (RbtListTipoBusqueda.SelectedValue == "1")
                url = "~/General/General/Report/ReportViewer.aspx?esFuc=1";
            else
                url = "~/General/General/Report/ReportViewer.aspx?esFuc=1&esHistorico=1";

            
            NavigateTo(url, false);
        }
        catch(Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para cargar listas desplegables y valores por defecto
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            CargarListaRegional();
            LlenarModalidadSeleccion();
            CargarListaEstadoContrato();
            LlenarCategoriaContrato();
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";

            ManejoControles vManejoControles = new ManejoControles();
            ddlIDTipoPersona.SelectedIndex = 0;
            vManejoControles.LlenarTipoPersonaParameter(ddlIDTipoPersona, "-1", true);

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    public void CargarListaEstadoContrato()
    {
        ManejoControlesContratos.LlenarComboLista(ddlIDEstadoContraro, vContratoService.ConsultarEstadoContrato(null,null,null,true), "IdEstadoContrato", "Descripcion");
    }

    public void CargarListaRegional()
    {
        Usuario usuario = new Usuario();
        usuario = vRuboService.ConsultarUsuario(GetSessionUser().IdUsuario);

        if (usuario != null)
        {

            if (usuario.TipoUsuario != 1)
            {
                Regional usuarioRegional = vRuboService.ConsultarRegional(usuario.IdRegional);
                ManejoControlesContratos.LlenarComboLista(ListRegional, vRuboService.ConsultarRegionalPCIs(usuarioRegional.CodigoRegional, null), "IdRegional", "NombreRegional");
                if (ListRegional.Items.Count > 0)
                {
                    if (usuario.IdRegional != null)
                    {
                        ListRegional.SelectedValue = usuario.IdRegional.ToString();
                        ListRegional.Enabled = false;
                    }
                    else
                    {
                        ListRegional.SelectedValue = "-1";
                        ListRegional.Enabled = false;
                    }

                }

            }
            else
            {
                ManejoControlesContratos.LlenarComboLista(ListRegional, vRuboService.ConsultarRegionalPCIs(null, null), "IdRegional", "NombreRegional");
            }

        }

    }

    public void LlenarModalidadSeleccion()
    {
        ddlIDModalidadSeleccion.Items.Clear();
        List<ModalidadSeleccion> vLModalidadesSeleccion = vContratoService.ConsultarModalidadSeleccions(null, null, null);
        foreach (ModalidadSeleccion tD in vLModalidadesSeleccion)
        {
            ddlIDModalidadSeleccion.Items.Add(new ListItem(tD.Nombre, tD.IdModalidad.ToString()));
        }
        ddlIDModalidadSeleccion.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }

    protected void ddlIDTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ManejoControles vManejoControles = new ManejoControles();
        //if (ddlIDTipoPersona.SelectedValue == "1") //NATURAL
        //{
        //    vManejoControles.LlenarTipoDocumentoRL(ddlIDTipoIdentificacion, "-1", true);
        //}
        //else
        //{
        //    vManejoControles.LlenarTipoDocumentoN(ddlIDTipoIdentificacion, "-1", true);
        //}
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            // Página 1 Descripción adicional Tipo Contrato Convenio
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion

            //switch (CodContratoAsociadoSeleccionado) // Se asigna previamente en SeleccionadoContratoAsociado 
            //{
            //    case CodContratoConvenioAdhesion:
            //        ddlIDTipoContrato.Enabled = false;
            //        ddlIDTipoContrato.SelectedValue = IdTipoContAporte; // Página 1 Descripción adicional Tipo Contrato Convenio
            //        break;
            //    default:
            //        ddlIDTipoContrato.Enabled = true; //Página 11 Paso 9 
            //        break;
            //}
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }

        
    }

    protected void ddlVigenciaFiscalinicial_SelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        toolBar.SetGenerarReporteConfrimation(string.Empty);

        txtFechaInicio.Text = string.Empty;
        txtFechaFinalizacion.Text = string.Empty;

        if (ddlVigenciaFiscalinicial.SelectedIndex > 0)
        {
            int anio = int.Parse(ddlVigenciaFiscalinicial.SelectedItem.Text);

            cetxtfechainicio.StartDate = new DateTime(anio, 1, 1);
            cetxtfechainicio.EndDate = new DateTime(anio, 12, 31);
            cetxtfechainicio.Enabled = true;
            txtFechaFinalizacion.Enabled = true;
            ListRegional.Enabled = true;

            cetxtfechaFinal.StartDate = new DateTime(anio, 1, 1);
            cetxtfechaFinal.EndDate = new DateTime(anio, 12, 31);
            cetxtfechaFinal.Enabled = true;
            txtFechaInicio.Enabled = true;
            ListRegional.Enabled = true;
        }
        else
        {
            cetxtfechainicio.Enabled = false;
            txtFechaFinalizacion.Enabled = false;
            ListRegional.Enabled = false;
            cetxtfechaFinal.Enabled = false;
            txtFechaInicio.Enabled = false;
            ListRegional.Enabled = false;
        }
    }

    protected void RbtListTipoBusqueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        toolBar.SetGenerarReporteConfrimation(string.Empty);

        toolBar.SetGenerarReporteConfrimation(string.Empty);

        ddlVigenciaFiscalinicial.Enabled = true;

        var vigencia = vRuboService.ConsultarVigencias(true);

        rfvVigencia.InitialValue = "-1";

        if (RbtListTipoBusqueda.SelectedValue == "1")
        {
            var miVigencia = vigencia.Where(e1 => e1.AcnoVigencia >= 2016 );
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial,miVigencia, "IdVigencia", "AcnoVigencia");
        }
        else
        {
            var miVigencia = vigencia.Where(e1 => e1.AcnoVigencia <= 2016 && e1.AcnoVigencia >= 2014 );
            ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, miVigencia, "IdVigencia", "AcnoVigencia");
        }
    }

    private void HabilitarFiltrosAdcionales(bool isValid)
    {
        ddlIDTipoPersona.Enabled = isValid;
        ddlIDModalidadSeleccion.Enabled = isValid;
        ddlIDCategoriaContrato.Enabled = isValid;
        ddlIDEstadoContraro.Enabled = isValid;
    }

    protected void ListRegional_SelectedIndexChanged1(object sender, EventArgs e)
    {
        toolBar.SetGenerarReporteConfrimation("muestraImagenLoading();");

        toolBar.LipiarMensajeError();

        bool isvalid = false;

        foreach (ListItem item in ListRegional.Items)
        {
            if(item.Selected)
            {
                isvalid = true;
                break;
            }
        }

        if (!isvalid)
            HabilitarFiltrosAdcionales(false);
        else
            HabilitarFiltrosAdcionales(true);
    }
}
