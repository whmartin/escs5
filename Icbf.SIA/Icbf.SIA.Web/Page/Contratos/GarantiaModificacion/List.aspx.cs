﻿using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.SIA.Service;

public partial class Page_Contratos_GarantiaModificacion_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GarantiaModificacion";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    private void Buscar()
    {
        try
        {
            //int? vIdContrato = null;
            String vNumeroContrato = null;
            int? vVigenciaFiscal = null;
            int? vRegional = null;
            int? vIDCategoriaContrato = null;
            int? vIDTipoContrato = null;


            if (txtNumeroContrato.Text != "")
            {
                vNumeroContrato = Convert.ToString(txtNumeroContrato.Text);
            }
            if (ddlIDCategoriaContrato.SelectedValue != "-1")
            {
                vIDCategoriaContrato = Convert.ToInt32(ddlIDCategoriaContrato.SelectedValue);
            }
            if (ddlIDTipoContrato.SelectedValue != "-1" && ddlIDTipoContrato.SelectedValue != "")
            {
                vIDTipoContrato = Convert.ToInt32(ddlIDTipoContrato.SelectedValue);
            }
            if (ddlRegional.SelectedValue != "-1")
            {
                vRegional = Convert.ToInt32(ddlRegional.SelectedValue);
            }
            if (ddlVigenciaFiscalinicial.SelectedValue != "-1")
            {
                vVigenciaFiscal = Convert.ToInt32(ddlVigenciaFiscalinicial.SelectedValue);
            }

            gvContraros.DataSource = vContratoService.ConsultarContratosModificacionGarantia(vNumeroContrato, vVigenciaFiscal, vRegional, vIDCategoriaContrato, vIDTipoContrato);
            gvContraros.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.MostrarBotonNuevo(false);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            
            gvContraros.PageSize = PageSize();
            gvContraros.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gesti&oacute;n de Modificaciones a las garantías", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvContraros.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("ModificacionGarantia.IdContrato", strValue);
            NavigateTo("ListDetalle.aspx");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("TipoSolicitud.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("TipoSolicitud.Eliminado");

            ManejoControles Controles = new ManejoControles();
            Controles.LlenarNombreRegional(ddlRegional, string.Empty, true);
            LlenarCategoriaContrato();
            CargarListaVigencia();
            gvContraros.PageSize = PageSize();
            gvContraros.EmptyDataText = EmptyDataText();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void ddlIDCategoriaContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionadoCategoriaContrato(ddlIDCategoriaContrato.SelectedValue);
    }

    private void SeleccionadoCategoriaContrato(string idCategoriaSeleccionada)
    {
        if (idCategoriaSeleccionada != "-1")
        {
            #region LlenarComboTipoContratoConvenio

            ddlIDTipoContrato.Items.Clear();
            List<TipoContrato> vLTiposContratos = vContratoService.ConsultarTipoContratos(null, Convert.ToInt32(idCategoriaSeleccionada), null, null, null, null, null, null);
            foreach (TipoContrato tD in vLTiposContratos)
            {
                ddlIDTipoContrato.Items.Add(new ListItem(tD.NombreTipoContrato, tD.IdTipoContrato.ToString()));
            }
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
            ddlIDTipoContrato.Enabled = true;

            #endregion
        }
        else
        {
            #region LimpiaControlesDependientesCategoriaConvenio
            ddlIDTipoContrato.Enabled = false;
            ddlIDTipoContrato.Items.Clear();
            ddlIDTipoContrato.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlIDTipoContrato.SelectedValue = "-1";
            #endregion
        }


    }

    protected void gvContraros_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
       gvContraros.PageIndex = e.NewPageIndex;
       Buscar();
    }
    
    protected void gvContraros_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvContraros.SelectedRow);
    }

    public void CargarListaVigencia()
    {
        ManejoControlesContratos.LlenarComboLista(ddlVigenciaFiscalinicial, vRuboService.ConsultarVigencias(true), "IdVigencia", "AcnoVigencia");
    }

    public void LlenarCategoriaContrato()
    {

        ddlIDCategoriaContrato.Items.Clear();
        List<CategoriaContrato> vLContratos = vContratoService.ConsultarCategoriaContratos(null, null, null);
        foreach (CategoriaContrato tD in vLContratos)
        {
            ddlIDCategoriaContrato.Items.Add(new ListItem(tD.Descripcion, tD.IdCategoriaContrato.ToString()));
        }
        ddlIDCategoriaContrato.Items.Insert(0, new ListItem("Seleccionar", "-1"));
    }
}