﻿using Icbf.Contrato.Service;
using Icbf.SIA.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Utilities.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Entity;

public partial class Page_Contratos_GarantiaModificacion_ListDetalle : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/GarantiaModificacion";
    ContratoService vContratoService = new ContratoService();
    SIAService vRuboService = new SIAService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.MostrarBotonNuevo(true);
            toolBar.MostrarBotonEditar(false);
            toolBar.eventoRetornar += toolBar_eventoRetornar;
            toolBar.eventoNuevo += toolBar_eventoNuevo;
            
            //gvContraros.PageSize = PageSize();
            //gvContraros.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Gesti&oacute;n de Modificaciones a las garantías contrato", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    void toolBar_eventoNuevo(object sender, EventArgs e)
    {
        try
        {
            var idContrato = int.Parse(hfIdContrato.Value);

            int idTipoModificacion = int.Parse(ddlTiposModificacion.SelectedValue.ToString());

            if (vContratoService.PuedeCrearGarantiasNuevasModificacion(idTipoModificacion))
            {
                SetSessionParameter("Garantia.IDContratoConsultado", idContrato);
                SetSessionParameter("Garantia.IdTipoModificacion", idTipoModificacion);
                NavigateTo(SolutionPage.Add);
            }
            else
                toolBar.MostrarMensajeError("La modificación seleccionada, no puede crear nuevas garantias.");
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    void toolBar_eventoRetornar(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
            }
        }
    }

    private void CargarDatosIniciales()
    {
        try
        {
            ManejoControles Controles = new ManejoControles();
            int idContrato = int.Parse(GetSessionParameter("ModificacionGarantia.IdContrato").ToString());
            RemoveSessionParameter("ModificacionGarantia.IdContrato");
            hfIdContrato.Value = idContrato.ToString();
            GridGarantias.EmptyDataText = EmptyDataText();
            GridGarantias.PageSize = PageSize();

            CargarGarantias();

            var contrato = vContratoService.ContratoObtener(idContrato);
            txtNumeroContrato.Text = contrato.NumeroContrato;
            List<TipoModificacion> tiposModificacion = vContratoService.ObtenerTiposModificacionModGarantia(idContrato);

            if (tiposModificacion.Count > 0)
            {
                ddlTiposModificacion.DataSource = tiposModificacion;
                ddlTiposModificacion.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarGarantias()
    {
        var idContrato = int.Parse(hfIdContrato.Value);
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(idContrato);
        if (items != null && items.Count() > 0)
        {
            var itemsPendientesTemp = items.Where(e => e.CodigoEstadoGarantia == "001" || e.CodigoEstadoGarantia == "004" || e.CodigoEstadoGarantia == "005");

            if (itemsPendientesTemp != null && itemsPendientesTemp.Count() > 0)
            {
                var itemsPendientes = itemsPendientesTemp.ToList();
                GridGarantias.DataSource = itemsPendientes;
                GridGarantias.DataBind();
            }
            else
                toolBar.MostrarMensajeError("No existen modificaciones de garantìas a gestionar.");
        }
    }

    protected void GridGarantias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridGarantias.PageIndex = e.NewPageIndex;
        List<Garantia> items = vContratoService.ConsultarInfoGarantias(int.Parse(hfIdContrato.Value));
        GridGarantias.DataSource = items;
        GridGarantias.DataBind();
    }

    protected void GridGarantias_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            var idContrato = int.Parse(hfIdContrato.Value);
            int index = int.Parse(e.CommandArgument.ToString());
            var idGarantia = int.Parse(GridGarantias.DataKeys[index].Values[0].ToString());
            var idSucursal = GridGarantias.DataKeys[index].Values[1].ToString();
            var codigoGarantia = GridGarantias.DataKeys[index].Values[2].ToString();

        if (e.CommandName == "Select")
        {
            SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
            SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", idSucursal);
            SetSessionParameter("Garantia.IdGarantia", idGarantia); 
            NavigateTo(SolutionPage.Detail);
        }
        else if (e.CommandName == "Editar")
        {
            if ( 
                 codigoGarantia != "001" || 
                (ddlTiposModificacion.Items != null && ddlTiposModificacion.Items.Count > 0 && !string.IsNullOrEmpty(ddlTiposModificacion.SelectedValue))
               )
            {
                    bool esGarantiaNueva = bool.Parse(GridGarantias.DataKeys[index].Values[3].ToString());

                    if (esGarantiaNueva)
                    {
                        var garantia = vContratoService.ConsultarGarantia(idGarantia); 
                        SetSessionParameter("Garantia.IDContratoConsultado", idContrato);
                        SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", idSucursal);
                        SetSessionParameter("Garantia.IdGarantia", idGarantia);
                        SetSessionParameter("Garantia.IdTipoModificacion", garantia.IdTipoModificacion);
  
                       NavigateTo(SolutionPage.Edit);    
                    }
                    else if (vContratoService.ConsultarGarantiaTemporal(idGarantia) == null)
                    {
                        int idTipoModificacion = int.Parse(ddlTiposModificacion.SelectedValue.ToString());

                        if (vContratoService.EsGarantiaEditada(idTipoModificacion, idContrato))
                        {
                            vContratoService.GenerarGarantiaTemporal(idGarantia, idTipoModificacion);
                            SetSessionParameter("Garantia.IDContratoConsultado", idContrato);
                            SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", idSucursal);
                            SetSessionParameter("Garantia.IdGarantia", idGarantia);
                            SetSessionParameter("Garantia.Editada", true);
                            NavigateTo(SolutionPage.Edit);
                        }
                        else
                            toolBar.MostrarMensajeError("El tipo de Modificación tiene asociada una generación de una nueva garantía.");
                    }
                    else
                    {
                        SetSessionParameter("Garantia.IDContratoConsultado", idContrato);
                        SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", idSucursal);
                        SetSessionParameter("Garantia.IdGarantia", idGarantia);
                        SetSessionParameter("Garantia.Editada", true);
                        NavigateTo(SolutionPage.Edit);
                    }
            }
            else
                toolBar.MostrarMensajeError("No existen modificaciones contractuales que modifiquen la Garantía");
        }
        else if (e.CommandName == "Aprobar")
        {
            var estado = GridGarantias.DataKeys[index].Values[2];
            bool esGarantiaNueva = bool.Parse(GridGarantias.DataKeys[index].Values[3].ToString());

            Garantia vGarantia = vContratoService.ConsultarGarantia(idGarantia);

            if (esGarantiaNueva || (!string.IsNullOrEmpty(vGarantia.NumeroModificacion) && vGarantia.FechaModificacionGarantia.HasValue))
            {
                vContratoService.EnviarAprobacionGarantia(idGarantia, estado.ToString());
                CargarGarantias();
            }
            else
                toolBar.MostrarMensajeError("Debe ingresar el número y la fecha de la modificación de la garantía");
          }
       }
        catch(Exception ex) 
        { 
          toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void GridGarantias_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton button = (ImageButton)e.Row.FindControl("btnEdit");
            ImageButton buttonAprobar = (ImageButton)e.Row.FindControl("btnAprobar");
            ImageButton buttonInfo = (ImageButton)e.Row.FindControl("btnInfo0");
            var estado = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CodigoEstadoGarantia"));

            if (estado == "001")
            {
                buttonAprobar.Visible = false;
                string script = "Verificar()";
                button.Attributes.Add("onclick", script);
            }
        }
    }
}