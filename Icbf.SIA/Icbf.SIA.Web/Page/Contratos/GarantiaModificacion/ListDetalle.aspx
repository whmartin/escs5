﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master" AutoEventWireup="true" CodeFile="ListDetalle.aspx.cs" Inherits="Page_Contratos_GarantiaModificacion_ListDetalle" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphCont" Runat="Server">
    
      <script type="text/javascript">


        function Confirmacion() {
            return confirm("Esta segúro que desea envíar a revisión la modificación de la garantía?");
        }

        function Verificar() {
            var valor = document.getElementById('<%= ddlTiposModificacion.ClientID %>'); 
            var itemValue = valor.options[document.getElementById("<%=ddlTiposModificacion.ClientID%>").selectedIndex].text
            return confirm("Esta segúro que desea asocíar la garantía, al tipo de  modificación " + itemValue + " ?");
        }

</script>
    <asp:HiddenField ID="hfIdContrato" runat="server" />
  <asp:Panel runat="server" ID="pnlConsulta">
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                N&uacute;mero del Contrato / Convenio
            </td>
            <td>
                Tipos de Modificaci&oacute;n
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ReadOnly="true" ID="txtNumeroContrato"  ></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddlTiposModificacion" runat="server" DataTextField="Descripcion" DataValueField="IdTipoModificacionBasica" >
                   
                </asp:DropDownList>
            </td>
        </tr>
                
    </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView ID="GridGarantias" OnRowDataBound="GridGarantias_RowDataBound"     runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="IDGarantia,IDSucursalAseguradoraContrato,CodigoEstadoGarantia,EsGarantiaModificada" GridLines="None" Height="16px" OnPageIndexChanging="GridGarantias_PageIndexChanging" OnRowCommand="GridGarantias_RowCommand"  Width="100%">
                        <Columns>
<%--                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:ImageButton ID="btnAdd" runat="server" CommandName="Add" Height="16px" ImageUrl="~/Image/btn/add.gif" ToolTip="Adicionar" Width="16px" />
                                </HeaderTemplate>

                            </asp:TemplateField>--%>
                            <asp:BoundField DataField="NumeroGarantia" HeaderText="Número de Garantía" />
                            <asp:BoundField DataField="NombreTipoGarantia" HeaderText="Tipo de Garantía" />
                            <asp:BoundField DataField="FechaInicioGarantiaView" HeaderText="Fecha Vigencia Desde" />
                            <asp:BoundField DataField="FechaVencimientoFinalGarantiaView" HeaderText="Fecha Vigencia Hasta" />
                            <asp:BoundField DataField="ValorGarantia" HeaderText="Valor Asegurado" DataFormatString="{0:C}" />
                                                        <asp:BoundField DataField="DescripcionEstadoGarantia" HeaderText="Estado" />
                            <asp:TemplateField HeaderText="Acciones">
                                <HeaderTemplate>
                                    Opciones:
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo0" runat="server" CommandName="Select" CommandArgument='<%# Container.DataItemIndex%>'  Height="16px" ImageUrl="~/Image/btn/list.png" ToolTip="Ver Detalle" Width="16px" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnEdit" runat="server" CommandName="Editar" OnClientClick="if (!Verificar()) return false;" CommandArgument='<%# Container.DataItemIndex%>'  Height="16px" ImageUrl="~/Image/btn/edit.gif" ToolTip="Editar" Width="16px" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnAprobar" runat="server" CommandName="Aprobar" OnClientClick="if (!Confirmacion()) return false;" CommandArgument='<%# Container.DataItemIndex%>'  Height="16px" ImageUrl="~/Image/btn/apply.png" ToolTip="Aprobar" Width="16px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>


