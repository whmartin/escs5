<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="Page_GarantiaModificacion_Add" %>

<%@ Register TagPrefix="uc2" TagName="IcbfDireccion" Src="~/General/General/Control/IcbfDireccion.ascx" %>
<%@ Register TagPrefix="uc1" TagName="fecha" Src="~/General/General/Control/fecha.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIDGarantia" runat="server" />
    <asp:HiddenField ID="hfEstadoGarantia" runat="server" />
    <asp:HiddenField ID="hfIdContrato" runat="server" />
    <asp:HiddenField ID="hfIdSucursal" runat="server" />
    <asp:HiddenField ID="hfGarantiaEditada" runat="server" />
    <asp:HiddenField ID="hfIdTipoModificacion" runat="server" /> 
    <asp:HiddenField ID="hfCesion" runat="server" />
    <asp:HiddenField ID="hfValorCesion" runat="server" />
     <asp:HiddenField ID="hfFechaSuscripcionModificacion" runat="server" />
        <fieldset>
        <legend>Información General</legend>
    <table width="90%" align="center">
        <tr class="rowA">
            <td colspan="2">
            <asp:MultiView ID="MultiViewDevolucion" runat="server" ActiveViewIndex="-1">
                <asp:View runat="server"  ID="ViewDevolucion">
                        <table width="100%" align="center">
                                <tr class="rowB">
                                    <td>
                                    Fecha de Devolución
                                    </td>
                                    <td >
                                    Motivo de Devolución
                                    </td>
                                </tr>
                                <tr class="rowA">
                                    <td>
                                        <asp:TextBox runat="server" Enabled="false" ID="txtFechaDevolucion"></asp:TextBox>
                                        <asp:Image ID="imgFechaDevolucion" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                                        Style="cursor: hand" /> 
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" Enabled="false" ID="txtMotivoDevoluicion" MaxLength="250" Width="80%"
                                        Height="80px" TextMode="MultiLine" 
                                        CssClass="TextBoxGrande"></asp:TextBox>
                                    </td>
                                </tr>
                         </table>
                </asp:View>
            </asp:MultiView>
                </td>
        </tr>
        <tr class="rowA" align="center">
            <td colspan="2">
                        <asp:MultiView ID="MultiViewEsGarantiaEditada" runat="server">
            <asp:View ID="ViewGarantiaEditada" runat="server">
                <Table width="100%" align="center">
                            <tr class="rowB">
            <td class="auto-style3">
                Número Modificación de la Garantía *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroModificacionGarantia" ControlToValidate="txtNumeroModificacionGarantia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td rowspan="2">
                Descripción de la Modificación de la Garantía *
                </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style3">
                <asp:TextBox runat="server" ID="txtNumeroModificacionGarantia" MaxLength="50" Width="80%"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="txtNumeroGarantia0_FilteredTextBoxExtender" runat="server" TargetControlID="txtNumeroModificacionGarantia"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-" />
            </td>
        </tr>
        <tr class="rowA">
            <td class="auto-style3">
                <strong>Fecha de la Modificación de la Garantía 
                <asp:RequiredFieldValidator  ID="rfvFechaModificacionGarantia" runat="server" ControlToValidate="txtFechaModificacionGarantia"
                    ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="true" ValidationGroup="btnGuardar"
                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvFechaModificacionGarantia" runat="server" ControlToValidate="txtFechaModificacionGarantia"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator>
                    </strong> 
                </td>
            <td rowspan="2">
                <asp:TextBox runat="server" ID="txtDescripciónModificación" MaxLength="250" Width="80%"
                    Height="80px" TextMode="MultiLine" onKeyDown="limitText(this,250);" onKeyUp="limitText(this,250);"
                    CssClass="TextBoxGrande"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td class="auto-style3">
                <asp:TextBox runat="server" ID="txtFechaModificacionGarantia"></asp:TextBox>
                <Ajax:CalendarExtender ID="txtFechaModificacionGarantia_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFechaModificacionGarantia" TargetControlID="txtFechaModificacionGarantia">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="txtFechaModificacion_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaModificacionGarantia">
                </Ajax:MaskedEditExtender>
                <asp:Image ID="imgFechaModificacionGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand"  />
            </td>
        </tr>
                </Table>
                </asp:View>
            </asp:MultiView>
            </td>
        </tr>

        <tr class="rowB">
            <td>
                Número del Contrato/Convenio
            </td>
            <td>
                &nbsp;Tipo de Modificación</td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroContratoConvenio" MaxLength="80" Width="34%"
                    Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroContratoConvenio" runat="server" TargetControlID="txtNumeroContratoConvenio"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters" />
            </td>
            <td>
                <asp:Label ID="lblTipoModificacion" runat="server" />
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Número Garantía *
                <asp:RequiredFieldValidator runat="server" ID="rfvNumeroGarantia" ControlToValidate="txtNumeroGarantia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Tipo Garantía *
                <asp:RequiredFieldValidator runat="server" ID="rfvIDTipoGarantia" ControlToValidate="ddlIDTipoGarantia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ID="cvIDTipoGarantia" ControlToValidate="ddlIDTipoGarantia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                    ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNumeroGarantia" MaxLength="50" Width="80%" AutoPostBack="True"
                    OnTextChanged="txtNumeroGarantia_OnTextChanged"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="ftNumeroGarantia" runat="server" TargetControlID="txtNumeroGarantia"
                    FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" ValidChars="-" />
            </td>
            <td style="width: 50%">
                <asp:DropDownList runat="server" ID="ddlIDTipoGarantia" Enabled="False" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlIDTipoGarantia_OnSelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
               </fieldset>
    <fieldset>
        <legend>Información Aseguradora</legend>
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Nit de la Aseguradora *
                    <asp:Image ID="imgAseguradora" runat="server" Visible="False" CssClass="bN" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                        OnClick="GetAseguradora()" Style="cursor: hand" ToolTip="Buscar" />
                    <asp:HiddenField runat="server" ID="hdNitAseguradora" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdDepartamentos_Sucursales" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdtercero" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hdIdEntidad" ClientIDMode="Static" />
                    <asp:Button runat="server" ID="btnCargaDatos" Style="display: none" OnClick="btnCargarDeptosSucursales_Click" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtNitAseguradora" ControlToValidate="txtNitAseguradora"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    Nombre de la Aseguradora
                    <asp:RequiredFieldValidator runat="server" ID="rfvtxtNombreAseguradora" ControlToValidate="txtNombreAseguradora"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNitAseguradora" MaxLength="80" Width="80%" Enabled="False"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNitAseguradora" runat="server" TargetControlID="txtNitAseguradora"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNombreAseguradora" MaxLength="80" Width="80%"
                        Enabled="False"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteNombreSucursal" runat="server" TargetControlID="txtNombreAseguradora"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars="áéíóúÁÉÍÓÚñÑ. " />
                    <asp:HiddenField runat="server" ID="HdNombreAseguradora" ClientIDMode="Static" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Departamento Sucursal&nbsp;
                    <%--<asp:RequiredFieldValidator runat="server" ID="rfvDepartamentoSucursal" ControlToValidate="ddlDepartamentoSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvDepartamentoSucursal" ControlToValidate="ddlDepartamentoSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
                </td>
                <td>
                    Municipio Sucursal&nbsp; <%--<asp:RequiredFieldValidator runat="server" ID="rfvMunicipioSucursal" ControlToValidate="ddlMunicipioSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvMunicipioSucursal" ControlToValidate="ddlMunicipioSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>--%>
                </td>
            </tr>
            <tr class="rowA">
                <td style="width: 50%">
                    <asp:DropDownList runat="server" ID="ddlDepartamentoSucursal" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlDepartamentoSucursal_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlMunicipioSucursal" AutoPostBack="True" OnSelectedIndexChanged="ddlMunicipioSucursal_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Nombre de la Sucursal
                   <%-- <asp:RequiredFieldValidator runat="server" ID="rfvddlNombreSucursal" ControlToValidate="ddlNombreSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido 1" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator runat="server" ID="cvddlNombreSucursal" ControlToValidate="ddlNombreSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" ValidationGroup="btnGuardar"
                        ForeColor="Red" Operator="NotEqual" ValueToCompare="-1" Display="Dynamic"></asp:CompareValidator>
                    <asp:RequiredFieldValidator runat="server" ID="rvtxtNombreSucursal" ControlToValidate="txtNombreSucursal"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <asp:TextBox runat="server" ID="txtNombreSucursal" MaxLength="256" Width="40%" Height="80px"
                        TextMode="MultiLine" onKeyDown="limitText(this,256);" onKeyUp="limitText(this,256);"
                        CssClass="TextBoxGrande"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombreAseguradora" runat="server" TargetControlID="txtNombreSucursal"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars=" " />
                    <asp:DropDownList runat="server" ID="ddlNombreSucursal" Width="37%" Visible="False"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlNombreSucursal_OnSelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:HiddenField runat="server" ID="hfdireccion" ClientIDMode="Static" />
                </td>
            </tr>
            <tr class="rowB">
                <td colspan="2">
                    Dirección Notificación
                </td>
            </tr>
            <tr class="rowA">
                <td colspan="2">
                    <uc2:IcbfDireccion ID="txtDireccionNotificacion" runat="server" Requerido="False" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Correo Electrónico 
                   <%-- <asp:RequiredFieldValidator runat="server" ID="rfvCorreo" ControlToValidate="txtCorreoElectronico"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="reCorreoElectronico" ErrorMessage="Formato de correo electrónico no válido"
                        ControlToValidate="txtCorreoElectronico" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        Enabled="true" ForeColor="Red" Display="Dynamic" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$">
                    </asp:RegularExpressionValidator>--%>
                </td>
                <td>
                    Indicativo 
                   <%-- <asp:RequiredFieldValidator runat="server" ID="rfvIndicativo" ControlToValidate="txtIndicativo"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtCorreoElectronico" MaxLength="128" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftecorreoelectronico" runat="server" TargetControlID="txtCorreoElectronico"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars="@.-_" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIndicativo" MaxLength="3"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteIndicativo" runat="server" TargetControlID="txtIndicativo"
                        FilterType="Numbers,LowercaseLetters,UppercaseLetters" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Teléfono 
                    <%--<asp:RegularExpressionValidator runat="server" ID="reTelefono" ErrorMessage="El teléfono no tiene una longitud válida"
                        ControlToValidate="txtTelefono" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" ValidationExpression="^([0-9]{7})$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTelefono" ControlToValidate="txtTelefono"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    Extensión 
                    <%--<asp:RequiredFieldValidator runat="server" ID="rfvExtension" ControlToValidate="txtExtension"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtTelefono" MaxLength="7" Width="40%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteTelefono" runat="server" TargetControlID="txtTelefono"
                        FilterType="Numbers" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtExtension" MaxLength="10"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtExtension"
                        FilterType="Numbers" />
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Celular 
                    <%--<asp:RegularExpressionValidator runat="server" ID="revTI" ErrorMessage="El teléfono no tiene una longitud válida"
                        ControlToValidate="txtCelular" SetFocusOnError="true" ValidationGroup="btnGuardar"
                        ForeColor="Red" Display="Dynamic" ValidationExpression="^([0-9]{10})$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator runat="server" ID="rfvCelular" ControlToValidate="txtCelular"
                        SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                        ForeColor="Red"></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    Beneficiario y/o Asegurado y/o Afianzado *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtCelular" MaxLength="10" Width="40%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="fteCelular" runat="server" TargetControlID="txtCelular"
                        FilterType="Numbers" />
                </td>
                <td>
                    <asp:CheckBoxList runat="server" ID="chkBeneAsegAfianz" RepeatDirection="Horizontal"
                        AutoPostBack="True" OnSelectedIndexChanged="chkBeneAsegAfianz_OnSelectedIndexChanged"
                        ValidationGroup="btnGuardar">
                        <asp:ListItem Text="ICBF" Value="true"></asp:ListItem>
                        <asp:ListItem Text="Otros" Value="false"></asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>Contratistas</legend>
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvProveedores" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="IdTercero,IdEntidad" CellPadding="0"
                        Height="16px" OnSorting="gvProveedores_Sorting" AllowSorting="True" OnPageIndexChanging="gvProveedores_PageIndexChanging"
                        OnSelectedIndexChanged="gvProveedores_SelectedIndexChanged" EmptyDataText="No se encontraron datos, verifique por favor.">
                        <Columns>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" CommandName="Select" ToolTip="Seleccionar" ID="check_SeleccionarContratista" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Tipo Persona" DataField="TipoPersonaNombre" SortExpression="TipoPersonaNombre" />
                            <asp:BoundField HeaderText="Tipo Identificación" DataField="TipoIdentificacion" SortExpression="TipoIdentificacion" />
                            <asp:BoundField HeaderText="Número de Identificación" DataField="NumeroIdentificacion"
                                SortExpression="NumeroIdentificacion" />
                            <asp:BoundField HeaderText="Contratista" DataField="Proveedor" SortExpression="Proveedor" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

    <table width="90%" align="center">
        <tr class="rowB">
            <td colspan="2">
                Descripción de los Beneficiarios y/o Asegurados y/o Afianzados *
                <asp:RequiredFieldValidator runat="server" ID="rfvDescBenAsegAfianz" ControlToValidate="txtDescBenAsegAfianz"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td colspan="2" style="text-align: center; vertical-align: middle;">
                <asp:TextBox runat="server" ID="txtDescBenAsegAfianz" MaxLength="200" Width="90%"
                    Height="80px" TextMode="MultiLine" onKeyDown="limitText(this,200);" onKeyUp="limitText(this,200);"
                    CssClass="TextBoxGrande" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteDescBenAsegAfianz" runat="server" TargetControlID="txtDescBenAsegAfianz"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters,Custom" ValidChars=" -.,;:" />
            </td>
        </tr>
    </table>

            </fieldset>

    <fieldset>
        <legend>Amparos de la Garantía</legend>
    <table width="90%" align="center">
        <tr>
            <td colspan="2">
                <asp:Button runat="server" ID="btnCargarAmparos" Style="display: none" OnClick="btnCargarAmparosGarantias_Click" />
                <asp:ImageButton ID="imgAmparos" runat="server" CommandName="Amparos" ImageUrl="~/Image/btn/icoPagBuscar.gif"
                    Height="16px" Width="16px" ToolTip="Amparos Garantías" OnClick="btnGuardarGarantiaPreliminar_Click"
                     />
            </td>
        </tr>
    </table>

    <asp:Panel runat="server" ID="pnlAmparos">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvAmparosGarantias" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IDAmparosGarantias"
                        CellPadding="0" Height="16px" OnSorting="gvAmparosGarantias_Sorting" AllowSorting="True"
                        OnPageIndexChanging="gvAmparosGarantias_PageIndexChanging" OnSelectedIndexChanged="gvAmparosGarantias_SelectedIndexChanged"
                        EmptyDataText="No se encontraron datos, verifique por favor.">
                        <Columns>
                            <asp:BoundField HeaderText="Tipo Amparo" DataField="NombreTipoAmparo" SortExpression="NombreTipoAmparo" />
                            <asp:TemplateField HeaderText="Fecha Vigencia Desde" ItemStyle-HorizontalAlign="Center"
                                SortExpression="FechaVigenciaDesde">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 80px;">
                                        <%#Convert.ToDateTime(Eval("FechaVigenciaDesde")).ToString("dd/MM/yyyy")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha Vigencia Hasta" ItemStyle-HorizontalAlign="Center"
                                SortExpression="FechaVigenciaHasta">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 80px;">
                                        <%#Convert.ToDateTime(Eval("FechaVigenciaHasta")).ToString("dd/MM/yyyy")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Valor para Cálculo Asegurado" DataField="ValorCalculoAsegurado"
                                SortExpression="ValorCalculoAsegurado" />
                            <asp:BoundField HeaderText="Unidad de Cálculo" DataField="NombreUnidadCalculo" SortExpression="NombreUnidadCalculo" />
                            <asp:TemplateField HeaderText="Valor Asegurado" ItemStyle-HorizontalAlign="Center"
                                SortExpression="ValorAsegurado">
                                <ItemTemplate>
                                    <div style="word-wrap: break-word; width: 210px;">
                                        <%# Convert.ToDecimal(Eval("ValorAsegurado")).ToString("$ #,###0.00##;($ #,###0.00##)")%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btneliminarAmparo" runat="server" CommandName="Borrar" ImageUrl="~/Image/btn/delete.gif"
                                        Height="16px" Width="16px" ToolTip="Eliminar" Enabled="true" OnClientClick="javascript:if (!confirm('¿Est&#225; seguro que desea eliminar el registro?')) return false;"
                                        OnClick="btnEliminarAmparo_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
                </fieldset>


        <fieldset>
        <legend>Cubrimientos y Anexos de la Garantía</legend>
    <table width="90%" align="center">
        <tr class="rowB">
             <td>
                Fecha de Expedición Garantía *
                <br />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaExpedicionGarantia" OnTextChanged="txtFechaExpedicionGarantia_OnTextChanged"
                    AutoPostBack="True" ></asp:TextBox>
                <asp:Image ID="imgFechaExpedicionGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand"  />
                <Ajax:CalendarExtender ID="cetxtfechaexpediciongarantia" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFechaExpedicionGarantia" TargetControlID="txtFechaExpedicionGarantia">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meetxtFechaDesde" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaExpedicionGarantia">
                </Ajax:MaskedEditExtender>
                <asp:RequiredFieldValidator ID="rfvFechaExpedicion" runat="server" ControlToValidate="txtFechaExpedicionGarantia"
                    ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="true" ValidationGroup="btnGuardar"
                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvFechaExpedicion" runat="server" ControlToValidate="txtFechaExpedicionGarantia"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator>
            </td>
          
           
        </tr>
        <tr class="rowA">
              <td>
                Fecha de Inicio Garantía
            </td>
                        <td>
                <asp:TextBox runat="server" ID="txtFechaInicioGarantia" 
                    AutoPostBack="True" Enabled="false"></asp:TextBox>
                <asp:Image ID="ImgFechaInicioGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand" Visible="true" />
<%--                <Ajax:CalendarExtender ID="CalendarExtenderFechaInicioGarantia" Enabled="false" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFechaExpedicionGarantia" TargetControlID="txtFechaInicioGarantia">
                </Ajax:CalendarExtender>--%>
                <Ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaInicioGarantia">
                </Ajax:MaskedEditExtender>
                  <asp:RequiredFieldValidator ID="RfvFechaInicioGarantia" runat="server" ControlToValidate="txtFechaInicioGarantia"
                    ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnGuardar"
                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CvFechaIncioGarantia" runat="server" ControlToValidate="txtFechaInicioGarantia"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar"  Display="Dynamic"></asp:CompareValidator>
                &nbsp;</td>
        </tr>
<%--        <tr class="rowB">
            <td>
                Fecha de Vencimiento Inicial Garantía *
                <br />
            </td>
                        <td>
                <asp:TextBox runat="server" ID="txtFechaVencimientoInicial" OnTextChanged="txtFechaVencimientoInicial_OnTextChanged"
                    AutoPostBack="True" Enabled="False"></asp:TextBox>
                <asp:Image ID="ImgFechaVencimientoInicial" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand" Visible="False" />
                <Ajax:CalendarExtender ID="ceFechaVencimientoInicial" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="ImgFechaVencimientoInicial" Enabled="false" TargetControlID="txtFechaVencimientoInicial">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meeFechaVencimientoInicial" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVencimientoInicial">
                </Ajax:MaskedEditExtender>
                <asp:RequiredFieldValidator ID="rfvFechaVencimientoInicial" runat="server" ControlToValidate="txtFechaVencimientoInicial"
                    ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnGuardar"
                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvFechaVencimientoInicial" runat="server" ControlToValidate="txtFechaVencimientoInicial"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator>
            </td>
       
        </tr>--%>
        <tr class="rowA">
              <td>
                  Fecha de Vencimiento Inicial Garantía
            </td>
                        <td>
                <asp:TextBox runat="server" ID="txtFechaVencimientoInicial" 
                    AutoPostBack="false" Enabled="False"></asp:TextBox>
                <Ajax:MaskedEditExtender ID="txtFechaVencimientoFinal0_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVencimientoInicial">
                </Ajax:MaskedEditExtender>
                <asp:Image ID="ImgFechaVencimientoFinal0" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand" Visible="true" />
              </td>
        </tr>
        <tr class="rowB">
                 <td>
                     Fecha de Vencimiento Garantía
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaVencimientoFinal" 
                    AutoPostBack="True" Enabled="False"></asp:TextBox>
                <asp:Image ID="ImgFechaVencimientoFinal" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand" Visible="true" />
<%--                <Ajax:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="ImgFechaVencimientoFinal" TargetControlID="txtFechaVencimientoFinal">
                </Ajax:CalendarExtender>--%>
                <Ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVencimientoFinal">
                </Ajax:MaskedEditExtender>
            </td>
        </tr>
       <%-- <tr class="rowA">
          <td>
                Fecha de Recibo Garantía *
                </td>
            <td>
                <asp:TextBox runat="server" ID="txtFechaReciboGarantia" OnTextChanged="txtFechaReciboGarantia_OnTextChanged"
                    AutoPostBack="True" Enabled="False"></asp:TextBox>
                <asp:Image ID="ImgFechaReciboGarantia" runat="server" CssClass="bN" ImageUrl="~/Image/btn/Calendar.gif"
                    Style="cursor: hand" Visible="False" />
                <Ajax:CalendarExtender ID="ceFechaReciboGarantia" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="ImgFechaReciboGarantia" Enabled="false" TargetControlID="txtFechaReciboGarantia">
                </Ajax:CalendarExtender>
                <Ajax:MaskedEditExtender ID="meeFechaReciboGarantia" runat="server" CultureAMPMPlaceholder="AM;PM"
                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="DMY" CultureDatePlaceholder="/"
                    CultureDecimalPlaceholder="." CultureThousandsPlaceholder="," CultureTimePlaceholder=":"
                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaReciboGarantia">
                </Ajax:MaskedEditExtender>
                
                <asp:CompareValidator ID="cvFechaReciboGarantia" runat="server" ControlToValidate="txtFechaReciboGarantia"
                    ErrorMessage="El formato Correcto es (dd/mm/aaaa)" ForeColor="Red" Operator="DataTypeCheck"
                    SetFocusOnError="True" Type="Date" ValidationGroup="btnGuardar" Display="Dynamic"></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="rfvFechaReciboGarantia" runat="server" ControlToValidate="txtFechaReciboGarantia"
                    ErrorMessage="Campo Requerido" SetFocusOnError="True" Enabled="False" ValidationGroup="btnGuardar"
                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                
            </td>
        </tr>--%>
        <tr class="rowB">
            <td>
                Valor Garantía *
                </td>
             <td>
                <asp:TextBox runat="server" ID="txtValorGarantia" MaxLength="50" Width="40%" Enabled="False"></asp:TextBox>
                <Ajax:FilteredTextBoxExtender ID="fteValorGarantia" runat="server" TargetControlID="txtValorGarantia"
                    FilterType="Numbers,Custom" ValidChars=".,$ " />
                <asp:RequiredFieldValidator runat="server" ID="rfvValorGarantia" ControlToValidate="txtValorGarantia"
                    SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic" ValidationGroup="btnGuardar"
                    ForeColor="Red" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="rowA">
            <td>
                Anexos *
                <asp:RequiredFieldValidator runat="server" ID="rfvrblAnexos" ControlToValidate="rblAnexos"
                    Enabled="False" SetFocusOnError="true" ErrorMessage="Campo Requerido" Display="Dynamic"
                    ValidationGroup="btnGuardar" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td>
                Observaciones
            </td>
        </tr>
        <tr class="rowB">
            <td>
                <asp:RadioButtonList runat="server" ID="rblAnexos" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblAnexos_OnSelectedIndexChanged"
                    AutoPostBack="True" Enabled="False">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDescripcionAnexos" MaxLength="250" Width="80%"
                    Height="80px" TextMode="MultiLine" onKeyDown="limitText(this,250);" onKeyUp="limitText(this,250);"
                    CssClass="TextBoxGrande"></asp:TextBox>
                <%--<Ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtDescripcionAnexos"
                    FilterType="Numbers,LowercaseLetters,UppercaseLetters" />--%>
            </td>
        </tr>
    </table>

    <table width="90%" align="center">
        <tr>
            <td colspan="2">
                Documento Anexo
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="PnlArchivosAnexos" Visible="False">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvDocumentos" AutoGenerateColumns="False" AllowPaging="True"
                        GridLines="None" Width="100%" DataKeyNames="NombreArchivo,IdArchivo,IDArchivosGarantias"
                        CellPadding="0" Height="16px" OnPageIndexChanging="gvDocumentos_PageIndexChanging"
                        OnSelectedIndexChanged="gvDocumentos_SelectedIndexChanged" OnRowCommand="gvDocumentos_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderText="Adjuntar documentos" DataField="NombreArchivoOri"></asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnAdjuntar" runat="server" CommandName="Attach" ImageUrl="~/Image/btn/attach.jpg"
                                        Height="16px" Width="16px" ToolTip="Adjuntar" Enabled='<%# (string) Eval("NombreArchivo") == "0" %>'
                                        Visible='<%# (string) Eval("NombreArchivo") == "0" %>' OnClick="btnAdjuntar_Click" />
                                    <asp:FileUpload ID="fuArchivo" runat="server" Visible="false" Width="400px" />
                                    <asp:ImageButton ID="btnSave" runat="server" CommandName="Save" ImageUrl="~/Image/btn/apply.png"
                                        Height="16px" Width="16px" ToolTip="Adjuntar" Visible="false" OnClick="btnSave_Click1" />
                                    <asp:ImageButton ID="btnMostrar" runat="server" CommandName="Show" ImageUrl="~/Image/btn/list.png"
                                        Height="16px" Width="16px" ToolTip="Visualizar Documento" Enabled="true" Visible='<%# (string) Eval("NombreArchivo") != "0" %>'
                                        OnClick="btnMostrar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEliminar" runat="server" CommandName="Borrar" ImageUrl="~/Image/btn/delete.gif"
                                        Visible='<%# (string) Eval("NombreArchivo") != "0" %>' Height="16px" Width="16px"
                                        ToolTip="Eliminar" Enabled="true" OnClientClick="javascript:if (!confirm('¿Est&#225; seguro que desea eliminar el documento?')) return false;"
                                        OnClick="btnEliminar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="LightBlue" />
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

                </fieldset>
    <script type="text/javascript" language="javascript">
        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
        function helpOver(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_a.png")%>';
        }
        function helpOut(idImage) {
            document.getElementById(idImage).src = '<%=ResolveClientUrl("~/Image/btn/help_g.png")%>';
        }

        function GetActualizarAmparos() {

            var idGarantia = document.getElementById('<%= hfIDGarantia.ClientID %>'); 
            var idTipoModificacion = document.getElementById('<%= hfIdTipoModificacion.ClientID %>');
            
            var EsCesion = document.getElementById('<%= hfCesion.ClientID %>');   
            var valorCesion = document.getElementById('<%= hfValorCesion.ClientID %>');
            
            if (EsCesion.value == "" || EsCesion.value == null)
            {
                 window_showModalDialog('../../../Page/Contratos/AmparosGarantias/Add.aspx?idGarantia=' + idGarantia.value + '&idTipoModificacion=' + idTipoModificacion.value, setReturnGetActualizarAmparos, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }
            else
            {
                window_showModalDialog('../../../Page/Contratos/AmparosGarantiasCesion/Add.aspx?idGarantia=' + idGarantia.value + '&idTipoModificacion=' +  idTipoModificacion.value + '&valorCesion=' + valorCesion.value , setReturnGetActualizarAmparos, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
            }


        }
        function setReturnGetActualizarAmparos(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {
                $("#<%= btnCargarAmparos.ClientID%>").click();
            }
        }

        function GetAseguradora() {
            window_showModalDialog('../../../Page/Contratos/Lupas/LupaAseguradora.aspx', setReturnGetAseguradora, 'dialogWidth:1250px;dialogHeight:600px;resizable:yes;');
        }
        function setReturnGetAseguradora(dialog) {
            var pObj = window_returnModalDialog(dialog);
            if (pObj != undefined && pObj != null) {

                var retLupa = pObj.split(",");
                if (retLupa.length > 5) {
                    $('#<%=hdtercero.ClientID %>').val(retLupa[0]);
                    $('#<%=hdIdEntidad.ClientID %>').val(retLupa[1]);
                    $('#<%=hdNitAseguradora.ClientID %>').val(retLupa[3]);
                    $('#<%=txtNitAseguradora.ClientID %>').val(retLupa[3]);
                    $('#<%=HdNombreAseguradora.ClientID %>').val(retLupa[4]);
                    $('#<%=txtNombreAseguradora.ClientID %>').val(retLupa[4]);
                    $('#<%=hdDepartamentos_Sucursales.ClientID %>').val(retLupa[5]);
                    $("#<%= btnCargaDatos.ClientID%>").click();
                }
            }
        }
    </script>
    </fieldset></asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContentPlaceHolder">
    <style type="text/css">
        .auto-style3 {
            width: 515px;
        }
    </style>
</asp:Content>

