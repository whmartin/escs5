using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Oferente.Entity;
using Icbf.Oferente.Service;
using Icbf.Proveedor.Entity;
using Icbf.RUBO.Entity;
using Icbf.SIA.Service;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Proveedor.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Org.BouncyCastle.Crypto.Signers;

/// <summary>
/// Página de registro y edición para la entidad Garantia
/// </summary>
public partial class Page_GarantiaModificacion_Add : GeneralWeb
{
    #region Variables

    private masterPrincipal toolBar;
    private ContratoService vContratoService = new ContratoService();
    private ProveedorService vProveedorService = new ProveedorService();
    private OferenteService vOferenteService = new OferenteService();
    private SIAService vSiaService = new SIAService();
    private List<int> vListContratistas = new List<int>();
    private string PageName = "Contratos/Garantia";
    private enum grilla
    {
        nombreOriginal = 0,
        archivo = 1,
        eliminar = 2,
        mostrar = 1
    }

    #endregion

    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        
            if (!Page.IsPostBack)
            {
                int contratoseleccionado = Convert.ToInt32(GetSessionParameter("Garantia.IDContratoConsultado"));
                hfIdContrato.Value = contratoseleccionado.ToString();
                SetSessionParameter("Contrato.ContratosAPP", contratoseleccionado);
                CargarDatosIniciales();

                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("ModificacionGarantia.IdContrato", hfIdContrato.Value);
        NavigateTo("ListDetalle.aspx");
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar(1);
    }

   protected void rblAnexos_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblAnexos.SelectedItem.Text == "SI")
            PnlArchivosAnexos.Visible = true;
        else
            PnlArchivosAnexos.Visible = false;
    }

    protected void txtNumeroGarantia_OnTextChanged(object sender, EventArgs e)
    {
        toolBar.LipiarMensajeError();
        var idcontratoGarantia = existeGarantiaContrato(txtNumeroGarantia.Text);
        if (ExisteNumeroGarantia(txtNumeroGarantia.Text))
        {
            toolBar.MostrarMensajeError("El Número de Garantía ya se encuentra registrado en otra Garantía asociada al contrato con numero de IdContrato" + " " + idcontratoGarantia.ToString() + ".");
            return;
        }
        else
        {
            ddlIDTipoGarantia.Enabled = true;
            CargarIDTipoGarantia();
        }

    }

    protected void ddlIDTipoGarantia_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIDTipoGarantia.SelectedValue != "-1")
            imgAseguradora.Visible = true;
        else
            imgAseguradora.Visible = false;

    }

    protected void btnCargarDeptosSucursales_Click(object sender, EventArgs e)
    {
        LlenarDepartamentos();
    }

    protected void ddlDepartamentoSucursal_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LlenarMunicipios(Convert.ToInt32(ddlDepartamentoSucursal.SelectedValue));
    }

    protected void ddlMunicipioSucursal_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LlenarNombreSucursal(Convert.ToInt32(ddlDepartamentoSucursal.SelectedValue),
            Convert.ToInt32(ddlMunicipioSucursal.SelectedValue));
    }

    protected void chkBeneAsegAfianz_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        int i = 0;
        txtDescBenAsegAfianz.Text = string.Empty;
        txtDescBenAsegAfianz.Enabled = true;
        imgAmparos.Visible = false;
        foreach (ListItem item in chkBeneAsegAfianz.Items)
        {
            if (item.Selected)
            {
                imgAmparos.Visible = true;
                if (item.Text.Equals("ICBF"))
                {
                    txtDescBenAsegAfianz.Text =
                        "INSTITUTO COLOMBIANO DE BIENESTAR FAMILIAR CECILIA DE LA FUENTE DE LLERAS - ICBF, NIT: 899999239-2";
                    txtDescBenAsegAfianz.Enabled = false;
                    i = 1;
                    continue;
                }
                if (item.Text.Equals("Otros") && i == 1)
                {
                    txtDescBenAsegAfianz.Enabled = true;
                    continue;
                }
                if (item.Text.Equals("Otros") && i == 0)
                {
                    txtDescBenAsegAfianz.Text = string.Empty;
                    txtDescBenAsegAfianz.Enabled = true;
                }
            }
        }
   }

    protected void ddlNombreSucursal_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlNombreSucursal.SelectedValue != "-1")
        {
            //rfvddlNombreSucursal.Enabled = false;
            //cvddlNombreSucursal.Enabled = false;
            //rvtxtNombreSucursal.Enabled = false;
            LlenarDatosSucursal(Convert.ToInt32(ddlNombreSucursal.SelectedValue));
        }
        else
        {
            //rfvddlNombreSucursal.Enabled = true;
            //cvddlNombreSucursal.Enabled = true;
            //rvtxtNombreSucursal.Enabled = true;
        }
            
    }

    protected void btnCargarAmparosGarantias_Click(object sender, EventArgs e)
    {
        CargarRegistroSimple();
        CargarAmparosGarantias();
    }

    protected void btnGuardarGarantiaPreliminar_Click(object sender, EventArgs e)
    {
        //imgAseguradora.Enabled = false;
        toolBar.LipiarMensajeError();
        if (ValidacionAseguradora().Equals(1))
        {
            toolBar.MostrarMensajeError("Debe diligenciar previamente, toda la información de la aseguradora");
            return;
        }
        else
        {
            if (!ExisteNumeroGarantia(txtNumeroGarantia.Text))
                Guardar(0);  
        }


        string returnValue = "   <script src='../../../Scripts/jquery-1.10.2.js' type='text/javascript'></script>" +
                                       "   <script src='../../../Scripts/jquery-ui.js' type='text/javascript'></script>" +
                                       "   <script src='../../../Scripts/Dialog.js' type='text/javascript'></script>" +
                                       "   <script language='javascript'> " +
                                       "        GetActualizarAmparos();" +
                                       "   </script>";


        ClientScript.RegisterStartupScript(Page.GetType(), "invocarfuncion", returnValue);


//        string script = @"<script type='text/javascript'>
//                        GetActualizarAmparos();
//                  </script>";

//        ScriptManager.RegisterStartupScript(this, typeof(Page), "invocarfuncion", script, false);
    }

    protected void txtFechaExpedicionGarantia_OnTextChanged(object sender, EventArgs e)
    {
        try
        {
            toolBar.LipiarMensajeError();

            if (ValidacionFecha(txtFechaExpedicionGarantia.Text) == 0)
            {
                DateTime fechaExpedicionOut = Convert.ToDateTime(txtFechaExpedicionGarantia.Text);

                // Consultar 
                //txtFechaInicioGarantia.Text = string.Empty;
                //txtFechaInicioGarantia.Enabled = true;
                //CalendarExtenderFechaInicioGarantia.Enabled = true;
                //ImgFechaInicioGarantia.Visible = true;
                //RfvFechaInicioGarantia.Enabled = true;
                //CalendarExtenderFechaInicioGarantia.StartDate = fechaExpedicionOut;

                //txtFechaVencimientoInicial.Text = string.Empty;
                //txtFechaVencimientoInicial.Enabled = false;
                //ImgFechaVencimientoInicial.Visible = false;
                //ceFechaVencimientoInicial.Enabled = false;
                //rfvFechaVencimientoInicial.Enabled = false;

                //txtFechaReciboGarantia.Text = string.Empty;
                //txtFechaReciboGarantia.Enabled = true;
                //rfvFechaReciboGarantia.Enabled = true;
                //ImgFechaReciboGarantia.Visible = true;
                //ceFechaReciboGarantia.Enabled = true;
                //ceFechaReciboGarantia.StartDate = fechaExpedicionOut;

                decimal acomuladovalores_asegurado = vContratoService.ConsultarAmparosGarantiass(null,
                                                                                                   null,
                                                                                                   null,
                                                                                                   null,
                                                                                                   Convert.ToInt32(hfIDGarantia.Value))
                                                                                                   .Sum(d => d.ValorAsegurado);

                txtValorGarantia.Text = acomuladovalores_asegurado.ToString("$ #,###0.00##;($ #,###0.00##)");
                rblAnexos.Enabled = true;
                rfvrblAnexos.Enabled = true;
                if (String.IsNullOrEmpty(txtValorGarantia.Text))
                    rfvValorGarantia.Enabled = true;


            }
            else
                toolBar.MostrarMensajeError("La fecha de expedición de la garantía es inválida");
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("Fecha(s) Inválidas");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    //protected void txtFechaReciboGarantia_OnTextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        toolBar.LipiarMensajeError();

    //        if (ValidacionFecha(txtFechaReciboGarantia.Text) == 0)
    //        {
    //            DateTime fechaReciboOut = Convert.ToDateTime(txtFechaReciboGarantia.Text);

    //            decimal acomuladovalores_asegurado = vContratoService.ConsultarAmparosGarantiass(null,
    //                                                                                           null,
    //                                                                                           null,
    //                                                                                           null,
    //                                                                                           Convert.ToInt32(hfIDGarantia.Value))
    //                                                                                           .Sum(d => d.ValorAsegurado);

    //            txtValorGarantia.Text = acomuladovalores_asegurado.ToString("$ #,###0.00##;($ #,###0.00##)");
    //            rblAnexos.Enabled = true;
    //            rfvrblAnexos.Enabled = true;
    //            if (String.IsNullOrEmpty(txtValorGarantia.Text))
    //                rfvValorGarantia.Enabled = true;
    //        }
    //        else
    //            toolBar.MostrarMensajeError("La fecha de recibo  de la garantía es inválida");
    //    }
    //    catch (FormatException ex)
    //    {
    //        toolBar.MostrarMensajeError("Fecha(s) Inválidas");
    //    }
    //    catch (UserInterfaceException ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //    catch (Exception ex)
    //    {
    //        toolBar.MostrarMensajeError(ex.Message);
    //    }
    //}

    private int ValidacionFecha(String Fecha)
    {
        int temp = 0;
        DateTime fechavalidacion = new DateTime(1900, 1, 1);

        if (string.IsNullOrEmpty(Fecha))
        {
            temp = 1;
            return temp;
        }

        if (Fecha.StartsWith("_"))
        {
            temp = 1;
            return temp;
        }

        if (!DateTime.TryParse(Fecha, out fechavalidacion))
        {
            temp = 1;
            return temp;
        }

        fechavalidacion = new DateTime(1900, 1, 1);
        if (Convert.ToDateTime(Fecha).Date.Equals(fechavalidacion.Date))
        {
            temp = 1;
            return temp;
        }       

        return temp;
    }

    private int ValidacionAseguradora()
    {
        int temp = 0;
        
        if (string.IsNullOrEmpty(txtNitAseguradora.Text) || string.IsNullOrEmpty(txtNombreAseguradora.Text))
        {
            temp = 1;
            return temp;
        }
        return temp;
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método de guardado (nuevo y edición) para la entidad Garantia
    /// </summary>
    private void Guardar(int llamado)
    {
        try
        {
            if (hfGarantiaEditada.Value == "1")
            {
                DateTime fechaModificacionGarantia;

                if (DateTime.TryParse(txtFechaModificacionGarantia.Text,out fechaModificacionGarantia))
                {
                    int idgarantia = int.Parse(hfIDGarantia.Value);
                    var garantia = vContratoService.ConsultarGarantia(idgarantia);
                    garantia.DescripcionModificacionGarantia = txtDescripciónModificación.Text;
                    garantia.FechaModificacionGarantia = DateTime.Parse(txtFechaModificacionGarantia.Text);
                    garantia.NumeroModificacion = txtNumeroModificacionGarantia.Text;
                   int vResultado = vContratoService.ModificarGarantia(garantia);

                   if (vResultado == 1)
                   {
                       SetSessionParameter("Garantia.IdGarantia", garantia.IDGarantia);
                       SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
                       SetSessionParameter("Garantia.Guardado", "1");
                       SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", hfIdSucursal.Value);
                       NavigateTo(SolutionPage.Detail);
                   }
                   else
                       toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else
                    toolBar.MostrarMensajeError("El formato de la fecha de modificación de la garantía es incorrecto.");
            }
            else
            {
                int vResultado;
                Garantia vGarantia = new Garantia();

                //********************************************************************************************************************************
                //Insercion de la Sucursal Aseguradora Contrato
                if (llamado.Equals(0))
                {
                    SucursalAseguradoraContrato vSucursalAseguradoraContrato = new SucursalAseguradoraContrato();
                    vSucursalAseguradoraContrato.IDDepartamento = Convert.ToInt32(ddlDepartamentoSucursal.SelectedValue);
                    vSucursalAseguradoraContrato.IDMunicipio = Convert.ToInt32(ddlMunicipioSucursal.SelectedValue);
                    if (txtNombreSucursal.Text != string.Empty)
                        vSucursalAseguradoraContrato.Nombre = txtNombreSucursal.Text;
                    else if (!string.IsNullOrEmpty(ddlNombreSucursal.SelectedValue))
                        vSucursalAseguradoraContrato.Nombre = ddlNombreSucursal.SelectedItem.Text;

                    vSucursalAseguradoraContrato.DireccionNotificacion = txtDireccionNotificacion.Text;
                    vSucursalAseguradoraContrato.IDZona = txtDireccionNotificacion.TipoZona.Equals("U") ? 1 : 2;
                    vSucursalAseguradoraContrato.CorreoElectronico = txtCorreoElectronico.Text;
                    vSucursalAseguradoraContrato.Indicativo = txtIndicativo.Text;
                    vSucursalAseguradoraContrato.Telefono = txtTelefono.Text;
                    vSucursalAseguradoraContrato.Extension = txtExtension.Text;
                    vSucursalAseguradoraContrato.Celular = txtCelular.Text;
                    //vSucursalAseguradoraContrato.IDEntidadProvOferente = Convert.ToInt32(hdIdEntidad.Value);
                    vSucursalAseguradoraContrato.IdTercero = Convert.ToInt32(hdtercero.Value);
                    
                    vSucursalAseguradoraContrato.CodigoSucursal = Convert.ToInt32(hdtercero.Value);
                    vSucursalAseguradoraContrato.UsuarioCrea = GetSessionUser().NombreUsuario;
                    vResultado = vContratoService.InsertarSucursalAseguradoraContrato(vSucursalAseguradoraContrato);
                    if (vResultado == 0)
                    {
                        toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                    }
                    else if (vResultado == 1)
                    {
                        hfIdSucursal.Value = vSucursalAseguradoraContrato.IDSucursalAseguradoraContrato.ToString();
                        SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", vSucursalAseguradoraContrato.IDSucursalAseguradoraContrato);
                    }
                }
                else
                {
                    SetSessionParameter("Garantia.IDSucursalAseguradoraContrato", hfIdSucursal.Value);
                }

                //********************************************************************************************************************************
                //Insercion de la Garantía

                vGarantia.NumeroGarantia = txtNumeroGarantia.Text;
                vGarantia.IDTipoGarantia = Convert.ToInt32(ddlIDTipoGarantia.SelectedValue);
                int validarcheck = 0;
                foreach (ListItem item in chkBeneAsegAfianz.Items)
                {
                    if (item.Text.Equals("ICBF"))
                    {
                        if (item.Selected)
                        {
                            validarcheck = 1;
                            vGarantia.BeneficiarioICBF = Convert.ToBoolean(true);
                        }
                        else
                            vGarantia.BeneficiarioICBF = Convert.ToBoolean(false);
                    }
                    if (item.Text.Equals("Otros"))
                    {
                        if (item.Selected)
                        {
                            validarcheck = 1;
                            vGarantia.BeneficiarioOTROS = Convert.ToBoolean(true);
                        }
                        else
                            vGarantia.BeneficiarioOTROS = Convert.ToBoolean(false);
                    }
                }

                if (validarcheck.Equals(0))
                {
                    toolBar.MostrarMensajeError("Debe seleccionar al menos un item de Beneficiario y/o Asegurado y/o Afianzado");
                    return;
                }

                vGarantia.DescripcionBeneficiarios = Convert.ToString(txtDescBenAsegAfianz.Text);
                vGarantia.IdContrato = Convert.ToInt32(hfIdContrato.Value);
                if (string.IsNullOrEmpty(hfIdSucursal.Value))
                {
                    toolBar.MostrarMensajeError("Debe relacionar al menos un amparo a la garantía");
                    return;
                }
                else
                    vGarantia.IDSucursalAseguradoraContrato = Convert.ToInt32(hfIdSucursal.Value);

                //vGarantia.EntidadProvOferenteAseguradora = Convert.ToInt32(hdIdEntidad.Value);
                vGarantia.IdTercero = Convert.ToInt32(hdtercero.Value);

                vGarantia.IDEstadosGarantias = vContratoService.ConsultarEstadosGarantiass("004", null)[0].IDEstadosGarantias;

                DateTime validacion = new DateTime(1900, 1, 1);

                if (llamado.Equals(0))
                {
                    vGarantia.FechaInicioGarantia = null;
                    vGarantia.FechaExpedicionGarantia = null;
                    vGarantia.FechaVencimientoInicialGarantia = null;
                    vGarantia.FechaVencimientoFinalGarantia = null;
                    vGarantia.FechaReciboGarantia = null;
                    vGarantia.EsGarantiaModificada = true;
                    vGarantia.IdTipoModificacion = int.Parse(hfIdTipoModificacion.Value);
                }
                else
                {
                    if (ValidacionFecha(txtFechaInicioGarantia.Text) == 1)
                    {
                        toolBar.MostrarMensajeError("La fecha de inicio de la garantía es inválida");
                        return;
                    }
                    else
                        vGarantia.FechaInicioGarantia = Convert.ToDateTime(txtFechaInicioGarantia.Text);

                    if (ValidacionFecha(txtFechaExpedicionGarantia.Text).Equals(1))
                    {
                        toolBar.MostrarMensajeError("La fecha de expedición de la garantía es inválida");
                        return;
                    }
                    else
                        vGarantia.FechaExpedicionGarantia = Convert.ToDateTime(txtFechaExpedicionGarantia.Text);

                    //if (ValidacionFecha(txtFechaVencimientoInicial.Text).Equals(1))
                    //{
                    //    toolBar.MostrarMensajeError("La fecha de vencimiento inicial de la garantía es inválida");
                    //    return;
                    //}
                    //else
                    //    vGarantia.FechaVencimientoInicialGarantia = Convert.ToDateTime(txtFechaVencimientoInicial.Text);

                    if (ValidacionFecha(txtFechaVencimientoFinal.Text) == 1)
                    {
                        toolBar.MostrarMensajeError("La fecha de vencimiento final de la garantía es inválida");
                        return;
                    }
                    else
                    {
                        var fechaVencimiento = Convert.ToDateTime(txtFechaVencimientoFinal.Text);

                        vGarantia.FechaVencimientoInicialGarantia = fechaVencimiento;
                        vGarantia.EsGarantiaModificada = true;
                        vGarantia.IdTipoModificacion = int.Parse(hfIdTipoModificacion.Value);
                        
                        vGarantia.FechaVencimientoFinalGarantia = fechaVencimiento;
                    }

                    //if (ValidacionFecha(txtFechaReciboGarantia.Text).Equals(1))
                    //{
                    //    toolBar.MostrarMensajeError("La fecha de recibo de la garantía es inválida");
                    //    return;
                    //}
                    //else
                    //    vGarantia.FechaReciboGarantia = Convert.ToDateTime(txtFechaReciboGarantia.Text);
                }


                if (llamado.Equals(1))
                {
                    if (txtValorGarantia.Text != "")
                    {
                        decimal numgarantia = decimal.Parse(txtValorGarantia.Text, NumberStyles.Currency);
                        vGarantia.ValorGarantia = Convert.ToString(numgarantia);
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("Faltan datos por diligenciar");
                        return;
                    }

                }
                else
                    vGarantia.ValorGarantia = string.Empty;

                if (rblAnexos.SelectedValue != "")
                    vGarantia.Anexos = Convert.ToBoolean(rblAnexos.SelectedValue);
                else
                    vGarantia.Anexos = null;

                vGarantia.ObservacionesAnexos = txtDescripcionAnexos.Text;

                if (llamado.Equals(1))
                {
                    if (txtFechaExpedicionGarantia.Text != "" && Convert.ToDateTime(txtFechaInicioGarantia.Text) != validacion)
                    {
                        //if (Convert.ToDateTime(txtFechaExpedicionGarantia.Text) > Convert.ToDateTime(txtFechaInicioGarantia.Text))
                        //{
                        //    toolBar.MostrarMensajeError("La Fecha de Expedición Garantía debe ser menor o igual a la Fecha de Inicio Garantía");
                        //    return;
                        //}
                        if (Convert.ToDateTime(txtFechaExpedicionGarantia.Text) < Convert.ToDateTime(hfFechaSuscripcionModificacion.Value))
                        {
                            toolBar.MostrarMensajeError("La Fecha de Expedición Garantía debe ser mayor o igual a la Fecha de suscripción");
                            return;
                        }
                    }

                    vGarantia.IDGarantia = Convert.ToInt32(hfIDGarantia.Value);
                    vGarantia.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vGarantia, this.PageName, vSolutionPage);
                    //vGarantia.DescripcionModificacionGarantia = txtDescripciónModificación.Text;
                    //vGarantia.FechaModificacionGarantia = DateTime.Parse(txtFechaModificacionGarantia.Text);
                    //vGarantia.NumeroModificacion = txtNumeroModificacionGarantia.Text;
                    vResultado = vContratoService.ModificarGarantia(vGarantia);

                    //*****************************************************************************
                    //validación de existencias de amparos
                    if (gvAmparosGarantias.Rows.Count == 0)
                    {
                        toolBar.MostrarMensajeError("No se ha relacionado Amparo a la garantía");
                        return;
                    }

                    //Se insertan los contratistas asociados
                    int idGarantia = Convert.ToInt32(hfIDGarantia.Value);
                    vContratoService.EliminarContratistaGarantias(idGarantia);
                    int cont = 0;
                    foreach (GridViewRow row in gvProveedores.Rows)
                    {
                        CheckBox check_SeleccionarContratista = (CheckBox)row.Cells[0].FindControl("check_SeleccionarContratista");

                        if (check_SeleccionarContratista.Checked)
                        {
                            cont = 1;
                            ContratistaGarantias inContratistaGarantias = new ContratistaGarantias();
                            inContratistaGarantias.IDGarantia = idGarantia;
                            inContratistaGarantias.IDEntidadProvOferente = Convert.ToInt32(gvProveedores.DataKeys[row.RowIndex].Values["IdEntidad"].ToString());
                            inContratistaGarantias.UsuarioCrea = GetSessionUser().NombreUsuario;
                            vResultado = vContratoService.InsertarContratistaGarantias(inContratistaGarantias);

                            if (vResultado == 0)
                            {
                                toolBar.MostrarMensajeError("La inserción de los contratistas no se completo satisfactoriamente, verifique por favor.");
                            }
                        }
                    }

                    if (cont == 0)
                    {
                        toolBar.MostrarMensajeError("No se ha relacionado ningún Contratista");
                        return;
                    }

                    //Se insertan los archivos anexos a la garantia
                    if (rblAnexos.SelectedItem != null)
                    {
                        if (rblAnexos.SelectedItem.Text == "SI")
                        {
                            if (gvDocumentos.Rows.Count >= 1 )
                            {
                                var primero = gvDocumentos.DataKeys[0]["IDArchivosGarantias"].ToString();

                                var esValido = true;

                                if (gvDocumentos.Rows.Count == 1)
                                    esValido = !string.IsNullOrEmpty(primero) && primero != "0";

                                if (esValido)
                                {
                                    foreach (GridViewRow row in gvDocumentos.Rows)
                                    {
                                        if (gvDocumentos.DataKeys[row.RowIndex]["IDArchivosGarantias"].ToString() != string.Empty &&
                                            gvDocumentos.DataKeys[row.RowIndex]["IDArchivosGarantias"].ToString() == "0")
                                        {
                                            ArchivosGarantias inDocAnexosGarantias = new ArchivosGarantias();
                                            inDocAnexosGarantias.IDGarantia = Convert.ToInt32(hfIDGarantia.Value);

                                            if (gvDocumentos.DataKeys[row.RowIndex]["IdArchivo"].ToString() != null)
                                            {
                                                if (gvDocumentos.DataKeys[row.RowIndex]["IdArchivo"].ToString() != "0")
                                                {
                                                    inDocAnexosGarantias.IDArchivo =
                                                        Convert.ToInt32(gvDocumentos.DataKeys[row.RowIndex]["IdArchivo"].ToString());
                                                    inDocAnexosGarantias.UsuarioCrea = GetSessionUser().NombreUsuario;
                                                    vResultado = vContratoService.InsertarArchivosGarantias(inDocAnexosGarantias);
                                                    if (vResultado == 0)
                                                    {
                                                        toolBar.MostrarMensajeError("La inserción de los documentos anexos no se completo satisfactoriamente, verifique por favor.");
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    toolBar.MostrarMensajeError("Debe adjuntar un documento");
                                    return;
                                }
                            }
                            else
                            {
                                toolBar.MostrarMensajeError("Debe adjuntar un documento");
                                return;
                            }
                        }
                    }
                    else
                    {
                        toolBar.MostrarMensajeError("Falta diligenciar campos obligatorios");
                        return;
                    }

                }
                else
                {
                    vGarantia.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vGarantia, this.PageName, vSolutionPage);
                    vResultado = vContratoService.InsertarGarantia(vGarantia);
                    hfIDGarantia.Value = vGarantia.IDGarantia.ToString();
                }

                if (vResultado == 0)
                {
                    toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
                }
                else if (vResultado == 1)
                {
                    if (llamado != 0)
                    {
                        SetSessionParameter("Garantia.IdGarantia", vGarantia.IDGarantia);
                        SetSessionParameter("Garantia.IDContratoConsultado", hfIdContrato.Value);
                        SetSessionParameter("Garantia.Guardado", "1");
                        NavigateTo(SolutionPage.Detail);                        
                    }
                }
            }
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("Fecha(s) Inválidas");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal) this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnRetornar_Click);
            toolBar.EstablecerTitulos("Garantía", SolutionPage.Add.ToString());
          
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
        try
        {

            int vIDGarantia = int.Parse(hfIDGarantia.Value.ToString());

            int vIDSucursalAseguradoraContrato = Convert.ToInt32(GetSessionParameter("Garantia.IDSucursalAseguradoraContrato"));
            RemoveSessionParameter("Garantia.IDSucursalAseguradoraContrato");
            hfIdSucursal.Value = vIDSucursalAseguradoraContrato.ToString();

            if (!string.IsNullOrEmpty(GetSessionParameter("Garantia.Editada").ToString()))
            {
                hfGarantiaEditada.Value = "1";
                RemoveSessionParameter("Garantia.Editada");
                DeshabilitarControles();
                MultiViewEsGarantiaEditada.ActiveViewIndex = 0;
            }

            if (GetSessionParameter("Garantia.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("Garantia.Guardado");

            SucursalAseguradoraContrato vSucursalAseguradoraContrato = vContratoService.ConsultarSucursalAseguradoraContrato(vIDSucursalAseguradoraContrato);
            //int idtercero = vProveedorService.ConsultarEntidadProvOferente(vSucursalAseguradoraContrato.IDEntidadProvOferente).IdTercero;
            //Tercero vTercero = vOferenteService.ConsultarTercero(idtercero);            
            Tercero vTercero = vOferenteService.ConsultarTercero(vSucursalAseguradoraContrato.IdTercero);
            List<ContratistaGarantias> vContratistaGarantias = vContratoService.ConsultarContratistaGarantiass(vIDGarantia, null);
            List<ArchivosGarantias> vArchivosGarantias = vContratoService.ConsultarArchivosGarantiass(null, vIDGarantia);
            List<AmparosGarantias> vAmparosGarantias = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, vIDGarantia);

            ddlDepartamentoSucursal.SelectedValue = Convert.ToString(vSucursalAseguradoraContrato.IDDepartamento);
            ddlMunicipioSucursal.SelectedValue = Convert.ToString(vSucursalAseguradoraContrato.IDMunicipio);
            txtNombreSucursal.Text = vSucursalAseguradoraContrato.Nombre;
            txtDireccionNotificacion.Text = vSucursalAseguradoraContrato.DireccionNotificacion;
            txtCorreoElectronico.Text = vSucursalAseguradoraContrato.CorreoElectronico;
            txtIndicativo.Text = vSucursalAseguradoraContrato.Indicativo;
            txtTelefono.Text = vSucursalAseguradoraContrato.Telefono;
            txtExtension.Text = vSucursalAseguradoraContrato.Extension;
            txtCelular.Text = vSucursalAseguradoraContrato.Celular;
            txtNitAseguradora.Text = vTercero.NumeroIdentificacion;
            txtNombreAseguradora.Text = vTercero.Nombre_Razonsocial;

            Garantia vGarantia = vContratoService.ConsultarGarantia(vIDGarantia);
            //hdIdEntidad.Value = vGarantia.EntidadProvOferenteAseguradora.ToString();
            hdtercero.Value = vGarantia.IdTercero.ToString();

            if (vGarantia.CodigoEstadoGarantia == "005")
            {
                txtFechaDevolucion.Text = Convert.ToDateTime(vGarantia.FechaDevolucion).ToString("dd/MM/yyyy");
                txtMotivoDevoluicion.Text = vGarantia.MotivoDevolucion;
                MultiViewDevolucion.ActiveViewIndex = 0;
            }

            var modificacionContractual = vContratoService.ConsultarModificacionContractualGarantia(vGarantia.IdContrato, vIDGarantia);

            txtFechaModificacionGarantia_CalendarExtender.StartDate = modificacionContractual.FechaSuscripcion;
            txtFechaModificacionGarantia_CalendarExtender.EndDate = DateTime.Now;

            txtNumeroModificacionGarantia.Text = vGarantia.NumeroModificacion;
            txtDescripciónModificación.Text = vGarantia.DescripcionModificacionGarantia;

            if (vGarantia.FechaModificacionGarantia.HasValue)
                txtFechaModificacionGarantia.Text = Convert.ToDateTime(vGarantia.FechaModificacionGarantia).ToString("dd/MM/yyyy");
           

            txtNumeroGarantia.Text = vGarantia.NumeroGarantia;
            ddlIDTipoGarantia.SelectedValue = Convert.ToString(vGarantia.IDTipoGarantia);
            if (vGarantia.BeneficiarioICBF.Equals(true))
                chkBeneAsegAfianz.Items.FindByText("ICBF").Selected = true;
            else
                chkBeneAsegAfianz.Items.FindByText("ICBF").Selected = false;

            if (vGarantia.BeneficiarioOTROS.Equals(true))
                chkBeneAsegAfianz.Items.FindByText("Otros").Selected = true;
            else
                chkBeneAsegAfianz.Items.FindByText("Otros").Selected = false;

            txtDescBenAsegAfianz.Text = Convert.ToString(vGarantia.DescripcionBeneficiarios);

            txtFechaInicioGarantia.Text = vGarantia.FechaInicioGarantia.HasValue ?vGarantia.FechaInicioGarantia.Value.ToShortDateString() : string.Empty;
            txtFechaExpedicionGarantia.Text = vGarantia.FechaExpedicionGarantia.HasValue ? Convert.ToDateTime(vGarantia.FechaExpedicionGarantia).ToString("dd/MM/yyyy"): string.Empty;
            txtFechaVencimientoInicial.Text = vGarantia.FechaVencimientoInicialGarantia.HasValue ? vGarantia.FechaVencimientoInicialGarantia.Value.ToShortDateString() : string.Empty;
            txtFechaVencimientoFinal.Text = vGarantia.FechaVencimientoFinalGarantia.HasValue ? vGarantia.FechaVencimientoFinalGarantia.Value.ToShortDateString() : string.Empty;
            //txtFechaReciboGarantia.Text =  vGarantia.FechaReciboGarantia.HasValue ? Convert.ToDateTime(vGarantia.FechaReciboGarantia).ToString("dd/MM/yyyy") : string.Empty;
            if (!string.IsNullOrEmpty(vGarantia.ValorGarantia))
                txtValorGarantia.Text = vGarantia.ValorGarantia; //Convert.ToDecimal(vGarantia.ValorGarantia).ToString("$ #,###0.00##;($ #,###0.00##)");
            rblAnexos.SelectedValue = Convert.ToString(vGarantia.Anexos);
            txtDescripcionAnexos.Text = vGarantia.ObservacionesAnexos;

            gvAmparosGarantias.DataSource = vAmparosGarantias;
            gvAmparosGarantias.DataBind();

            if (vContratistaGarantias !=  null && vContratistaGarantias.Count > 0)
            {
                foreach (GridViewRow row in gvProveedores.Rows)
                {
                    var idEntidad =  int.Parse(gvProveedores.DataKeys[row.RowIndex].Values["IdEntidad"].ToString());

                    if (vContratistaGarantias.Exists(e => e.IDEntidadProvOferente == idEntidad))
                        ((CheckBox)row.Cells[0].FindControl("check_SeleccionarContratista")).Checked = true;   
                }
            }

            if (vArchivosGarantias.Count > 0)
            {
                rblAnexos.SelectedValue = "true";
                PnlArchivosAnexos.Visible = true;
            }
            else
            {
                vArchivosGarantias.Add(new ArchivosGarantias
                {
                    IDArchivosGarantias = 0,
                    IDArchivo = 0,
                    NombreArchivo = "0",
                    NombreArchivoOri = ""
                });
                rblAnexos.SelectedValue = "false";
            }

            rblAnexos.Enabled = true;

            gvDocumentos.DataSource = vArchivosGarantias;
            gvDocumentos.DataBind();

            CargarAmparosGarantias();

            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGarantia.UsuarioCrea, vGarantia.FechaCrea, vGarantia.UsuarioModifica, vGarantia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void DeshabilitarControles()
    {
        txtNumeroGarantia.Enabled = false;
        ddlDepartamentoSucursal.Enabled = false;
        ddlMunicipioSucursal.Enabled = false;
        txtNombreSucursal.Enabled = false;
        txtDireccionNotificacion.Enabled = false;
        txtCorreoElectronico.Enabled = false;
        txtIndicativo.Enabled = false;
        txtTelefono.Enabled = false;
        txtCelular.Enabled = false;
        chkBeneAsegAfianz.Enabled = false;
        gvProveedores.Columns[0].Visible = false;
        imgAmparos.Visible = false;
        gvAmparosGarantias.Columns[6].Visible = false;
        txtFechaExpedicionGarantia.Enabled = false;
        txtDescripcionAnexos.Enabled = false;
        txtDescripcionAnexos.Enabled = false;
        txtExtension.Enabled = false;
        txtDireccionNotificacion.HabilitarLimpiar(false);
    }

    /// <summary>
    /// 
    /// </summary>
    private void CargarRegistroSimple()
    {
        try
        {

            int vIDGarantia = Convert.ToInt32(hfIDGarantia.Value);

            int vIDSucursalAseguradoraContrato = Convert.ToInt32(hfIdSucursal.Value);

            SucursalAseguradoraContrato vSucursalAseguradoraContrato = vContratoService.ConsultarSucursalAseguradoraContrato(vIDSucursalAseguradoraContrato);
            //int idtercero = vProveedorService.ConsultarEntidadProvOferente(vSucursalAseguradoraContrato.IDEntidadProvOferente).IdTercero;
            //Tercero vTercero = vOferenteService.ConsultarTercero(idtercero);            
            Tercero vTercero = vOferenteService.ConsultarTercero(vSucursalAseguradoraContrato.IdTercero);
            List<ContratistaGarantias> vContratistaGarantias = vContratoService.ConsultarContratistaGarantiass(vIDGarantia, null);
            List<ArchivosGarantias> vArchivosGarantias = vContratoService.ConsultarArchivosGarantiass(null, vIDGarantia);

            ddlDepartamentoSucursal.SelectedValue = Convert.ToString(vSucursalAseguradoraContrato.IDDepartamento);
            ddlMunicipioSucursal.SelectedValue = Convert.ToString(vSucursalAseguradoraContrato.IDMunicipio);
            txtNombreSucursal.Text = vSucursalAseguradoraContrato.Nombre;
            txtDireccionNotificacion.Text = vSucursalAseguradoraContrato.DireccionNotificacion;
            txtCorreoElectronico.Text = vSucursalAseguradoraContrato.CorreoElectronico;
            txtIndicativo.Text = vSucursalAseguradoraContrato.Indicativo;
            txtTelefono.Text = vSucursalAseguradoraContrato.Telefono;
            txtExtension.Text = vSucursalAseguradoraContrato.Extension;
            txtCelular.Text = vSucursalAseguradoraContrato.Celular;
            txtNitAseguradora.Text = vTercero.NumeroIdentificacion;
            txtNombreAseguradora.Text = vTercero.Nombre_Razonsocial;

            Garantia vGarantia = vContratoService.ConsultarGarantia(vIDGarantia);
            //            hdIdEntidad.Value = vGarantia.EntidadProvOferenteAseguradora.ToString();
            hdtercero.Value = vGarantia.IdTercero.ToString();

            txtNumeroGarantia.Text = vGarantia.NumeroGarantia;
            ddlIDTipoGarantia.SelectedValue = Convert.ToString(vGarantia.IDTipoGarantia);
            if (vGarantia.BeneficiarioICBF.Equals(true))
                chkBeneAsegAfianz.Items.FindByText("ICBF").Selected = true;
            else
                chkBeneAsegAfianz.Items.FindByText("ICBF").Selected = false;

            if (vGarantia.BeneficiarioOTROS.Equals(true))
                chkBeneAsegAfianz.Items.FindByText("Otros").Selected = true;
            else
                chkBeneAsegAfianz.Items.FindByText("Otros").Selected = false;

            txtDescBenAsegAfianz.Text = Convert.ToString(vGarantia.DescripcionBeneficiarios);

            txtFechaInicioGarantia.Text = vGarantia.FechaInicioGarantia.HasValue ? vGarantia.FechaInicioGarantia.Value.ToShortDateString() : string.Empty;
            txtFechaExpedicionGarantia.Text = vGarantia.FechaExpedicionGarantia.HasValue ? Convert.ToDateTime(vGarantia.FechaExpedicionGarantia).ToString("dd/MM/yyyy") : string.Empty;
            txtFechaVencimientoFinal.Text = vGarantia.FechaVencimientoFinalGarantia.HasValue ? vGarantia.FechaVencimientoFinalGarantia.Value.ToShortDateString() : string.Empty;
            //txtFechaReciboGarantia.Text = vGarantia.FechaReciboGarantia.HasValue ? Convert.ToDateTime(vGarantia.FechaReciboGarantia).ToString("dd/MM/yyyy") : string.Empty;
            rblAnexos.SelectedValue = Convert.ToString(vGarantia.Anexos);
            txtDescripcionAnexos.Text = vGarantia.ObservacionesAnexos;

            if (vContratistaGarantias != null && vContratistaGarantias.Count > 0)
            {
                foreach (GridViewRow row in gvProveedores.Rows)
                {
                    var idEntidad = int.Parse(gvProveedores.DataKeys[row.RowIndex].Values["IdEntidad"].ToString());

                    if (vContratistaGarantias.Exists(e => e.IDEntidadProvOferente == idEntidad))
                        ((CheckBox)row.Cells[0].FindControl("check_SeleccionarContratista")).Checked = true;
                }
            }

            if (vArchivosGarantias.Count > 0)
            {
                rblAnexos.SelectedValue = "true";
                PnlArchivosAnexos.Visible = true;
            }
            else
            {
                vArchivosGarantias.Add(new ArchivosGarantias
                {
                    IDArchivosGarantias = 0,
                    IDArchivo = 0,
                    NombreArchivo = "0",
                    NombreArchivoOri = ""
                });
                rblAnexos.SelectedValue = "false";
            }

            rblAnexos.Enabled = true;

            gvDocumentos.DataSource = vArchivosGarantias;
            gvDocumentos.DataBind();

            CargarAmparosGarantias();


            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vGarantia.UsuarioCrea, vGarantia.FechaCrea, vGarantia.UsuarioModifica, vGarantia.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            if (!string.IsNullOrEmpty(GetSessionParameter("Garantia.IdTipoModificacion").ToString()))
            {
                hfIdTipoModificacion.Value = GetSessionParameter("Garantia.IdTipoModificacion").ToString();
                RemoveSessionParameter("Garantia.IdTipoModificacion");
                var tipoModificacion = vContratoService.consultartipomodificacion(int.Parse(hfIdTipoModificacion.Value));
                lblTipoModificacion.Text = tipoModificacion.Descripcion;
                

            }
            else
              hfIdTipoModificacion.Value = "0";
               
            

            if (!string.IsNullOrEmpty(GetSessionParameter("Garantia.IdGarantia").ToString()))
            {
                int vIDGarantia = Convert.ToInt32(GetSessionParameter("Garantia.IdGarantia"));
                RemoveSessionParameter("Garantia.IdGarantia");
                hfIDGarantia.Value = vIDGarantia.ToString();

                var Item = vContratoService.TipoModificacionGarantiaTemporal(vIDGarantia);

                if(! string.IsNullOrEmpty(Item))
                    lblTipoModificacion.Text = Item;
            }

            if(lblTipoModificacion.Text == "Cesión")
            {
                hfCesion.Value = hfIdTipoModificacion.Value;
                var cesion = vContratoService.ConsultarCesionesContrato(Convert.ToInt32(hfIdContrato.Value));
                hfValorCesion.Value = cesion.ValorCesion.ToString();
                
            }
            else
             hfCesion.Value = null;            


            toolBar.LipiarMensajeError();
            hfGarantiaEditada.Value = "0";
            toolBar.OcultarBotonGuardar(true);
            int idContrato = Convert.ToInt32(hfIdContrato.Value);
            Icbf.Contrato.Entity.Contrato vContrato = vContratoService.ConsultarContrato(idContrato);

            ddlIDTipoGarantia.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlDepartamentoSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            ddlMunicipioSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            //chkBeneAsegAfianz.Items.Insert(0, new ListItem("ICBF", "false"));
            //chkBeneAsegAfianz.Items.Insert(1, new ListItem("Otros", "false"));
            rblAnexos.Items.Insert(0, new ListItem("SI", "true"));
            rblAnexos.Items.Insert(1, new ListItem("NO", "false"));
            CargarGrillaContratistas(gvProveedores, GridViewSortExpression, true);
            CargarGrillaAmparos(gvAmparosGarantias, GridViewSortExpression, true);
            if (!string.IsNullOrEmpty(hfIdContrato.Value))
               txtNumeroContratoConvenio.Text = vContratoService.ConsultarContrato(idContrato).NumeroContrato;
               
                //Si existe varias garantias asociadas a un contrato se hace la sumatoria total del valorgarantia.
                       
            List<ArchivosGarantias> lDocAnexosGarantias = new List<ArchivosGarantias>();
            lDocAnexosGarantias.Add(new ArchivosGarantias
            {
                IDArchivosGarantias = 0,
                IDArchivo = 0,
                NombreArchivo = "0",
                NombreArchivoOri = ""
            });

            this.gvDocumentos.DataSource = lDocAnexosGarantias;
            this.gvDocumentos.DataBind();

            if (Request.QueryString["oP"] == "E")
            {
                CargarIDTipoGarantia();
                CargarDepartamentos();
                CargarMunicipios();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de que verifica si existe el número de la garantía 
    /// </summary>
    private bool ExisteNumeroGarantia(String pNumeroGarantia)
    {
        return vContratoService.ExisteNumeroGarantia(pNumeroGarantia);
    }

    /// <summary>
    /// Método de carga el Tipo Garantia
    /// </summary>
    private void CargarIDTipoGarantia()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarTipoGarantia(ddlIDTipoGarantia, "-1", true);
    }

    private void CargarDepartamentos()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarDepartamentos(ddlDepartamentoSucursal, "-1", true);
    }

    private void CargarMunicipios()
    {
        ManejoControlesContratos Controles = new ManejoControlesContratos();
        Controles.LlenarMunicipios(ddlMunicipioSucursal, "-1", true, null);
    }

    private void LlenarDepartamentos()
    {
        LimpiarDatosSucursales();
        if (hdDepartamentos_Sucursales.Value != "-1")
        {
            ddlDepartamentoSucursal.Items.Clear();
            List<String> DeptosSucursal = hdDepartamentos_Sucursales.Value.Split(';').Distinct().ToList();
            DeptosSucursal.RemoveAt(DeptosSucursal.Count - 1);
            for (int i = 0; i < DeptosSucursal.Count - 1; i++)
            {
                Departamento dept = new Departamento();
                dept.IdDepartamento = Convert.ToInt32(DeptosSucursal[i]);
                dept.NombreDepartamento = DeptosSucursal[i + 1];
                ddlDepartamentoSucursal.Items.Insert(0,
                    new ListItem(dept.NombreDepartamento, dept.IdDepartamento.ToString()));
                i = i + 1;

            }
            ddlDepartamentoSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        else
        {
            ManejoControlesContratos Controles = new ManejoControlesContratos();
            Controles.LlenarDepartamentos(ddlDepartamentoSucursal, "-1", true);
        }

        ddlMunicipioSucursal.Items.Clear();
        ddlMunicipioSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
        txtNitAseguradora.Text = hdNitAseguradora.Value;
        txtNombreAseguradora.Text = HdNombreAseguradora.Value;
    }

    private void LlenarMunicipios(int Departamento)
    {
        if (hdDepartamentos_Sucursales.Value != "-1")
        {
            ddlMunicipioSucursal.Items.Clear();
            List<Icbf.Proveedor.Entity.EntidadProvOferente> vEntidadProvOferentes =
                vProveedorService.ConsultarSucursales_EntidadProvOferentes_Municipio(Convert.ToInt32(hdtercero.Value),
                    Departamento, null);
            foreach (Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente in vEntidadProvOferentes)
            {
                ddlMunicipioSucursal.Items.Insert(0,
                    new ListItem(vEntidadProvOferente.NombreMunicipio, vEntidadProvOferente.IdMunicipio.ToString()));
            }
            //pDropDownList.DataBind();
            ddlMunicipioSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
        }
        else
        {
            ManejoControlesContratos Controles = new ManejoControlesContratos();
            Controles.LlenarMunicipios(ddlMunicipioSucursal, "-1", true, Departamento);
        }
    }

    private void LlenarNombreSucursal(int Departamento, int Municipio)
    {
        if (Departamento != -1 && Municipio != -1)
        {
            List<Icbf.Proveedor.Entity.EntidadProvOferente> vEntidadProvOferentes =
                vProveedorService.ConsultarSucursales_EntidadProvOferentes(Convert.ToInt32(hdtercero.Value),
                    Departamento, Municipio);
            if (vEntidadProvOferentes.Count > 0)
            {
                txtNombreSucursal.Text = string.Empty;
                txtNombreSucursal.Visible = false;
                ddlNombreSucursal.Items.Clear();
                ddlNombreSucursal.Visible = true;

                foreach (Icbf.Proveedor.Entity.EntidadProvOferente vEntidadProvOferente in vEntidadProvOferentes)
                {
                    ddlNombreSucursal.Items.Insert(0,
                        new ListItem(vEntidadProvOferente.NombreSucursal, vEntidadProvOferente.IdSucursal.ToString()));
                }
                //pDropDownList.DataBind();
                ddlNombreSucursal.Items.Insert(0, new ListItem("Seleccione", "-1"));
            }
            else
            {
                ddlNombreSucursal.Items.Clear();
                ddlNombreSucursal.Visible = false;
                txtNombreSucursal.Text = string.Empty;
                txtNombreSucursal.Visible = true;
            }
        }
        else
        {
            ddlNombreSucursal.Items.Clear();
            txtNombreSucursal.Text = string.Empty;
        }
    }

    private void LlenarDatosSucursal(int pIdSucursal)
    {
        Sucursal vSucursal = vProveedorService.ConsultarSucursal(pIdSucursal);
        hfdireccion.Value = vSucursal.Direccion;
        txtDireccionNotificacion.TipoZona = vSucursal.IdZona.Equals(1) ? "U" : "R";
        txtDireccionNotificacion.Text = hfdireccion.Value;
        txtCorreoElectronico.Text = vSucursal.Correo;
        txtIndicativo.Text = Convert.ToString(vSucursal.Indicativo);
        txtTelefono.Text = Convert.ToString(vSucursal.Telefono);
        txtExtension.Text = Convert.ToString(vSucursal.Extension);
        txtCelular.Text = Convert.ToString(vSucursal.Celular);
    }

    private void LimpiarDatosSucursales()
    {
        txtDireccionNotificacion.Text = string.Empty;
        //txtDireccionNotificacion.TipoZona = string.Empty;
        txtCorreoElectronico.Text = string.Empty;
        txtIndicativo.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtExtension.Text = string.Empty;
        txtCelular.Text = string.Empty;
    }

    public int existeGarantiaContrato(String pNumeroGarantia)
    {
        return vContratoService.existeGarantiaContrato(pNumeroGarantia);
    }
    private void CargarAmparosGarantias()
    {
        toolBar.LipiarMensajeError();

        if (!string.IsNullOrEmpty(hfIDGarantia.Value))
        {        
            CargarGrillaAmparos(gvAmparosGarantias, GridViewSortExpression, true);

            int idGarantia = Convert.ToInt32(hfIDGarantia.Value);
            var myGridResults = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, idGarantia);

            if (myGridResults != null && myGridResults.Count > 0)
            {
                txtFechaInicioGarantia.Text = myGridResults.OrderBy(e => e.FechaVigenciaDesde).First().FechaVigenciaDesde.ToShortDateString();
                txtFechaVencimientoFinal.Text = myGridResults.OrderByDescending(e => e.FechaVigenciaHasta).First().FechaVigenciaHasta.ToShortDateString();
                cetxtfechaexpediciongarantia.EndDate = Convert.ToDateTime(txtFechaInicioGarantia.Text);

                if (! string.IsNullOrEmpty(hfIdTipoModificacion.Value))
                {
                    SolModContractual mod = vContratoService.ObtenerModificacionSinModificacionGarantiaPorTipo(int.Parse(hfIdTipoModificacion.Value),int.Parse(hfIdContrato.Value));
                    cetxtfechaexpediciongarantia.StartDate = mod.FechaSuscripcion;
                    hfFechaSuscripcionModificacion.Value = mod.FechaSuscripcion.ToString();
                }

                if (hfGarantiaEditada.Value == "1")
                {
                    txtFechaExpedicionGarantia.Enabled = false;
                    cetxtfechaexpediciongarantia.Enabled = false;
                }
                else
                {
                    txtFechaExpedicionGarantia.Enabled = true;
                    imgFechaExpedicionGarantia.Visible = true;
                    rfvFechaExpedicion.Enabled = true;
                    cetxtfechaexpediciongarantia.EndDate = Convert.ToDateTime(txtFechaInicioGarantia.Text);
                }
            }

            decimal acomuladovalores_asegurado = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, Convert.ToInt32(hfIDGarantia.Value)).Sum(d => d.ValorAsegurado);

            txtValorGarantia.Text = acomuladovalores_asegurado.ToString("$ #,###0.00##;($ #,###0.00##)");

            var garantia = vContratoService.ConsultarGarantia(idGarantia);
            garantia.ValorGarantia = Convert.ToDecimal(acomuladovalores_asegurado).ToString("$ #,###0.00##;($ #,###0.00##)");
            vContratoService.ModificarGarantia(garantia);
        }
 
    }

    #endregion

    #region Grilla Contratistas

    /// <summary>
    /// Guarda la dirección de ordenamiento del gridview
    /// </summary>
    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection) ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }

    /// <summary>
    /// Guarda el criterio de ordenamiento de la grilla
    /// </summary>
    public string GridViewSortExpression
    {
        get { return (string) ViewState["sortExpression"]; }
        set { ViewState["sortExpression"] = value; }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistroContratista(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvProveedores.DataKeys[rowIndex].Value.ToString();
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvProveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistroContratista(gvProveedores.SelectedRow);
    }

    protected void gvProveedores_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaContratistas((GridView) sender, e.SortExpression, false);
    }

    protected void gvProveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProveedores.PageIndex = e.NewPageIndex;
        CargarGrillaContratistas((GridView) sender, GridViewSortExpression, true);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaContratistas(BaseDataBoundControl gridViewsender, string expresionOrdenamiento,
        bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////
        int? IdContrato = null;
        if (GetSessionParameter("Contrato.ContratosAPP") != null && Convert.ToString(GetSessionParameter("Contrato.ContratosAPP")) != "")
            IdContrato = Convert.ToInt32(GetSessionParameter("Contrato.ContratosAPP"));

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            var myGridResults = vContratoService.ConsultarContratistasContratos(IdContrato);

            //////////////////////////////////////////////////////////////////////////////////
            //////Fin del código de llenado de datos para la grilla 
            //////////////////////////////////////////////////////////////////////////////////

            if (expresionOrdenamiento != null)
            {
                //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                if (string.IsNullOrEmpty(GridViewSortExpression))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
                else if (GridViewSortExpression != expresionOrdenamiento)
                {
                    GridViewSortDirection = SortDirection.Descending;
                }
                if (myGridResults != null)
                {
                    var param = Expression.Parameter(typeof (Proveedores_Contratos), expresionOrdenamiento);

                    //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                    var prop = Expression.Property(param, expresionOrdenamiento);

                    //Creo en tiempo de ejecución la expresión lambda
                    var sortExpression =
                        Expression.Lambda<Func<Proveedores_Contratos, object>>(
                            Expression.Convert(prop, typeof (object)), param);

                    //Dependiendo del modo de ordenamiento . . .
                    if (GridViewSortDirection == SortDirection.Ascending)
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Descending;
                            gridViewsender.DataSource =
                                myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                    }
                    else
                    {

                        //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                        if (cambioPaginacion == false)
                        {
                            GridViewSortDirection = SortDirection.Ascending;
                            gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                        }
                        else
                        {
                            gridViewsender.DataSource =
                                myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                        }
                    }

                    GridViewSortExpression = expresionOrdenamiento;
                }
            }
            else
            {
                gridViewsender.DataSource = myGridResults;
            }

            gridViewsender.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Grilla Amparos Garantias

    protected void gvAmparosGarantias_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistroAmparos(gvAmparosGarantias.SelectedRow);
    }

    protected void gvAmparosGarantias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAmparosGarantias.PageIndex = e.NewPageIndex;
        CargarGrillaAmparos((GridView) sender, GridViewSortExpression, true);
    }

    protected void gvAmparosGarantias_Sorting(object sender, GridViewSortEventArgs e)
    {
        CargarGrillaAmparos((GridView) sender, e.SortExpression, false);
    }

    /// <summary>
    /// Cargar una grilla con ordenamiento
    /// </summary>
    /// <param name="gridViewsender">Grilla a ordenar</param>
    /// <param name="expresionOrdenamiento">Columna que sirve de criterio para ordenar</param>
    /// <param name="cambioPaginacion">indica si el metodo se envia desde el evento PageIndexChanging de la grilla</param>
    private void CargarGrillaAmparos(BaseDataBoundControl gridViewsender, string expresionOrdenamiento,bool cambioPaginacion)
    {
        //////////////////////////////////////////////////////////////////////////////////
        //////Aqui va el código de llenado de datos para la grilla 
        //////////////////////////////////////////////////////////////////////////////////

        //Lleno una lista con los datos que uso para llenar la grilla
        try
        {
            if (!string.IsNullOrEmpty(hfIDGarantia.Value))
            {
                toolBar.LipiarMensajeError();
                int? idGarantia = 0;

                idGarantia = Convert.ToInt32(hfIDGarantia.Value);

                var myGridResults = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, idGarantia);
                //////////////////////////////////////////////////////////////////////////////////
                //////Fin del código de llenado de datos para la grilla 
                //////////////////////////////////////////////////////////////////////////////////

                if (expresionOrdenamiento != null)
                {
                    //Si la expresión de ordenamiento (columna) cambió, entonces la direccion de ordenamiento es ascendente
                    if (string.IsNullOrEmpty(GridViewSortExpression))
                    {
                        GridViewSortDirection = SortDirection.Ascending;
                    }
                    else if (GridViewSortExpression != expresionOrdenamiento)
                    {
                        GridViewSortDirection = SortDirection.Descending;
                    }
                    if (myGridResults != null)
                    {
                        var param = Expression.Parameter(typeof(AmparosGarantias), expresionOrdenamiento);

                        //La propiedad de mi lista, esto es "Entidad.CualquierCampo"
                        var prop = Expression.Property(param, expresionOrdenamiento);

                        //Creo en tiempo de ejecución la expresión lambda
                        var sortExpression =
                            Expression.Lambda<Func<AmparosGarantias, object>>(Expression.Convert(prop, typeof(object)),
                                param);

                        //Dependiendo del modo de ordenamiento . . .
                        if (GridViewSortDirection == SortDirection.Ascending)
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Descending;
                                gridViewsender.DataSource =
                                    myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                        }
                        else
                        {

                            //Si no viene del evento de paginacion (busqueda normal), entonces cambio la direccion de ordenamiento
                            if (cambioPaginacion == false)
                            {
                                GridViewSortDirection = SortDirection.Ascending;
                                gridViewsender.DataSource = myGridResults.AsQueryable().OrderBy(sortExpression).ToList();
                            }
                            else
                            {
                                gridViewsender.DataSource =
                                    myGridResults.AsQueryable().OrderByDescending(sortExpression).ToList();
                            }
                        }

                        GridViewSortExpression = expresionOrdenamiento;
                    }
                }
                else
                {
                    gridViewsender.DataSource = myGridResults;
                }

                gridViewsender.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método para redirigir a la página detalle del registro seleccionado 
    /// </summary>
    private void SeleccionarRegistroAmparos(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvAmparosGarantias.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("AmparosGarantias.IDAmparosGarantias", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region Grilla Archivos Garantias

    protected void gvDocumentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocumentos.PageIndex = e.NewPageIndex;


        //if (ddlIdRegional.SelectedValue == "1")
        //{
        //    CargarGrillasDocumento(true);
        //}
        //else
        //{
        //    CargarGrillasDocumento(false);
        //}
    }

    protected void gvDocumentos_SelectedIndexChanged(object sender, EventArgs e)
    {
        int numIndex = gvDocumentos.SelectedRow.RowIndex;
        foreach (GridViewRow gvRow in gvDocumentos.Rows)
        {
            ImageButton btnMostrar = (ImageButton) gvRow.Cells[(int) grilla.mostrar].FindControl("btnMostrar");
            ImageButton btnAdjuntar = (ImageButton) gvRow.Cells[(int) grilla.archivo].FindControl("btnAdjuntar");
            ImageButton btnEliminar = (ImageButton) gvRow.Cells[(int) grilla.eliminar].FindControl("btnEliminar");
            if (gvRow.RowIndex == numIndex)
            {
                if (HttpUtility.HtmlDecode(gvDocumentos.DataKeys[gvRow.RowIndex]["NombreArchivo"].ToString()).Trim() ==
                    "0")
                {
                    btnAdjuntar.Enabled = true;
                }
                else
                {
                    btnMostrar.Enabled = true;
                    btnEliminar.Enabled = true;
                }
            }
            else
            {
                btnMostrar.Enabled = false;
                btnAdjuntar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }
    }

    protected void gvDocumentos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //switch (e.CommandName)
        //{
        //    //case "Show":
        //    //    Show(gvDocumentos.SelectedRow, true);
        //    //    break;
        //    //case "Attach":
        //    //    Attach(gvDocumentos.SelectedRow, true);
        //    //    break;
        //    case "Save":
        //        Save(gvDocumentos.SelectedRow);
        //        break;
        //    //case "Cancelar":
        //    //    Cancel(gvDocumentos.SelectedRow, true);
        //    //    break;
        //    //case "Borrar":
        //    //    Delete(gvDocumentos.SelectedRow, true);
        //    //    break;
        //}
    }

    protected void btnAdjuntar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton) sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
            {

                int rowIndex = dataIteme.RowIndex;

                FileUpload fuArchivo =
                    (FileUpload) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.archivo].FindControl("fuArchivo");
                fuArchivo.Visible = true;
                ImageButton btnAdjuntar =
                    (ImageButton) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.archivo].FindControl("btnAdjuntar");
                btnAdjuntar.Visible = false;
                ImageButton btnSave =
                    (ImageButton) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.archivo].FindControl("btnSave");
                btnSave.Visible = true;
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }

    }

    protected void btnSave_Click1(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton) sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
                Save(dataIteme.RowIndex);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Save(int pRow)
    {
        int rowIndex = pRow;

        String NombreArchivoOri;
        string NombreArchivo;
        decimal idarchivo = 0;

        FileUpload fuArchivo =
            (FileUpload) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.archivo].FindControl("fuArchivo");

        if (fuArchivo.HasFile)
        {
            try
            {
                string filename = Path.GetFileName(fuArchivo.FileName);

                if (fuArchivo.PostedFile.ContentLength/1024 > 4096)
                {
                    throw new GenericException("Sólo se permiten archivos inferiores a 4MB");
                }

                NombreArchivoOri = filename;

                NombreArchivo = filename.Substring(0, filename.IndexOf("."));
                //NombreArchivo = vOACService.QuitarEspacios(NombreArchivo);
                NombreArchivo = vContratoService.QuitarAcentos(NombreArchivo);

                string tipoArchivo = "";
                if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() !=
                    "PDF")
                {
                    throw new GenericException("Solo se permiten archivos PDF");
                }

                if (filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal) + 1).ToUpper() ==
                    "PDF")
                {
                    tipoArchivo = "General.archivoPDF";
                }

                List<Icbf.SIA.Entity.FormatoArchivo> vFormatoArchivo =
                    vSiaService.ConsultarFormatoArchivoTemporarExt(tipoArchivo, null);

                if (vFormatoArchivo.Count == 0)
                {
                    if (vFormatoArchivo.Count > 0)
                    {
                        toolBar.MostrarMensajeError(
                            "No se ha parametrizado La extensión del archivo a cargar o La extensión esta duplicada");
                        return;
                    }

                }

                int idformatoArchivo = vFormatoArchivo[0].IdFormatoArchivo;

                NombreArchivo = Guid.NewGuid().ToString() + "_" + DateTime.Now.Year.ToString() + "_" +
                                DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + "_" +
                                DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" +
                                DateTime.Now.Second.ToString() + "_OfOAC_" + NombreArchivo +
                                filename.Substring(filename.LastIndexOf("."));
                string NombreArchivoFtp = System.Configuration.ConfigurationManager.AppSettings["FtpPath"] + ":" +
                                          System.Configuration.ConfigurationManager.AppSettings["FtpPort"] + @"/";

                //Guardado del archivo en el servidor
                HttpPostedFile vHttpPostedFile = fuArchivo.PostedFile;

                Boolean ArchivoSubido = vSiaService.SubirArchivoFtp((Stream) vHttpPostedFile.InputStream,
                    NombreArchivoFtp, NombreArchivo);
                if (ArchivoSubido)
                {

                    //Creación del registro de control del archivo subido
                    ArchivoContrato RegistroControl = new ArchivoContrato();
                    RegistroControl.IdUsuario = Convert.ToInt32(GetSessionUser().IdUsuario.ToString());
                    RegistroControl.FechaRegistro = DateTime.Now;
                    RegistroControl.ServidorFTP = NombreArchivoFtp;
                    RegistroControl.NombreArchivo = NombreArchivo;
                    RegistroControl.NombreArchivoOri = NombreArchivoOri;
                    RegistroControl.IdFormatoArchivo = idformatoArchivo; //idFormato; 
                    RegistroControl.Estado = "C";
                    RegistroControl.ResumenCarga = string.Empty;
                    RegistroControl.UsuarioCrea = GetSessionUser().NombreUsuario;
                    RegistroControl.FechaCrea = DateTime.Now;
                    InformacionAudioria(RegistroControl, this.PageName, vSolutionPage);
                    vContratoService.InsertarArchivo(RegistroControl);


                    idarchivo = RegistroControl.IdArchivo;

                }

            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
                return;
            }
        }
        else
        {

            return;
        }
        fuArchivo.Visible = false;
        ImageButton btnAdjuntar =
            (ImageButton) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.archivo].FindControl("btnAdjuntar");
        btnAdjuntar.Visible = true;
        btnAdjuntar.Enabled = false;
        ImageButton btnSave =
            (ImageButton) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.archivo].FindControl("btnSave");
        btnSave.Visible = false;
        ImageButton btnMostrar =
            (ImageButton) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.mostrar].FindControl("btnMostrar");
        btnMostrar.Enabled = true;
        ImageButton btnEliminar =
            (ImageButton) gvDocumentos.Rows[rowIndex].Cells[(int) grilla.eliminar].FindControl("btnEliminar");
        btnEliminar.Enabled = true;

        //CargarGrillasDocumento();
        int cfila = gvDocumentos.Rows.Count;

        System.Data.DataTable tablaDocumentos = (DataTable) ViewState["DocumentosAnexosGarantias"];

        if (tablaDocumentos == null)
        {
            CargarGrillasDocumento();
            tablaDocumentos = (DataTable) ViewState["DocumentosAnexosGarantias"];
        }


        TableRow fila = new TableRow();
        tablaDocumentos.Rows.Add(fila);

        tablaDocumentos.Rows[cfila][0] = "0";
        tablaDocumentos.Rows[cfila][1] = idarchivo;
        tablaDocumentos.Rows[cfila][2] = NombreArchivo;
        tablaDocumentos.Rows[cfila][3] = NombreArchivoOri;

        ViewState["DocumentosAnexosGarantias"] = tablaDocumentos;

        gvDocumentos.DataSource = tablaDocumentos;
        gvDocumentos.DataBind();

    }

    private void CargarGrillasDocumento()
    {
        int contadorFilas = 0;
        DataTable tablaDocumentos = new DataTable();
        tablaDocumentos.Columns.Add("IDArchivosGarantias");
        tablaDocumentos.Columns.Add("IdArchivo");
        tablaDocumentos.Columns.Add("NombreArchivo");
        tablaDocumentos.Columns.Add("NombreArchivoOri");


        foreach (GridViewRow gvRow in gvDocumentos.Rows)
        {
            TableRow fila = new TableRow();
            tablaDocumentos.Rows.Add(fila);

            tablaDocumentos.Rows[contadorFilas][0] =
                gvDocumentos.DataKeys[gvRow.RowIndex]["IDArchivosGarantias"].ToString();
            //gvRow.Cells[(int)Keygrilla.IdDocSoporteReclamo].Text;
            tablaDocumentos.Rows[contadorFilas][1] = gvDocumentos.DataKeys[gvRow.RowIndex]["IdArchivo"].ToString();
            //gvRow.Cells[(int)Keygrilla.IdArchivo].Text;
            tablaDocumentos.Rows[contadorFilas][2] = gvDocumentos.DataKeys[gvRow.RowIndex]["NombreArchivo"].ToString();
            //gvRow.Cells[(int)Keygrilla.NombreArchivo].Text;

            tablaDocumentos.Rows[contadorFilas][3] =
                HttpUtility.HtmlDecode(gvRow.Cells[(int) grilla.nombreOriginal].Text);
            contadorFilas += 1;
        }

        ViewState["DocumentosAnexosGarantias"] = tablaDocumentos;

        gvDocumentos.DataSource = tablaDocumentos;
        gvDocumentos.DataBind();
    }

    protected void btnMostrar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton) sender;
        try
        {
            GridViewRow dataItem = imgSelecItem.NamingContainer as GridViewRow;


            gvDocumentos.DataSource = ViewState["DocumentosAnexosGarantias"];
            gvDocumentos.DataBind();

            DataTable tablaDocumentos = (DataTable) ViewState["DocumentosAnexosGarantias"];
            if (dataItem != null)
            {
                string nombreArchivo =
                    HttpUtility.HtmlDecode(tablaDocumentos.Rows[dataItem.RowIndex]["NombreArchivo"].ToString()).Trim();


                if (nombreArchivo != "0")
                    Response.Redirect("../DescargarArchivo/DescargarArchivo.aspx?fname=" + nombreArchivo);
            }
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton) sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
            {

                ArchivosGarantias delArchivosGarantias = new ArchivosGarantias();
                if (gvDocumentos.DataKeys[dataIteme.RowIndex]["IDArchivosGarantias"] != null &&
                    gvDocumentos.DataKeys[dataIteme.RowIndex]["IDArchivosGarantias"].ToString() != "") 
                {
                    delArchivosGarantias.IDArchivosGarantias =
                        Convert.ToInt32(gvDocumentos.DataKeys[dataIteme.RowIndex]["IDArchivosGarantias"].ToString());
                    vContratoService.EliminarArchivosGarantias(delArchivosGarantias);
                }

                DataTable tablaDocumentos = (DataTable) ViewState["DocumentosAnexosGarantias"];
                tablaDocumentos.Rows[dataIteme.RowIndex].Delete();

                ViewState["DocumentosAnexosGarantias"] = tablaDocumentos;
                gvDocumentos.DataSource = tablaDocumentos;
                gvDocumentos.DataBind();

                toolBar.MostrarMensajeGuardado("El documento ha sido eliminado correctamente");
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnEliminarAmparo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgSelecItem = (ImageButton) sender;
        try
        {
            GridViewRow dataIteme = imgSelecItem.NamingContainer as GridViewRow;

            if (dataIteme != null)
            {
                AmparosGarantias delAmparosGarantias = new AmparosGarantias();
                if (gvAmparosGarantias.DataKeys[dataIteme.RowIndex]["IDAmparosGarantias"] != null &&
                    gvAmparosGarantias.DataKeys[dataIteme.RowIndex]["IDAmparosGarantias"].ToString() != "")
                {
                    delAmparosGarantias.IDAmparosGarantias =
                        Convert.ToInt32(gvAmparosGarantias.DataKeys[dataIteme.RowIndex]["IDAmparosGarantias"].ToString());
                    vContratoService.EliminarAmparosGarantias(delAmparosGarantias);
                }

                CargarGrillaAmparos(gvAmparosGarantias, GridViewSortExpression, true);

                int idGarantia = Convert.ToInt32(hfIDGarantia.Value);
                var myGridResults = vContratoService.ConsultarAmparosGarantiass(null, null, null, null, idGarantia);

                if (myGridResults != null && myGridResults.Count > 0)
                {
                    txtFechaInicioGarantia.Text = myGridResults.OrderBy(e1 => e1.FechaVigenciaDesde).First().FechaVigenciaDesde.ToShortDateString();
                    txtFechaVencimientoFinal.Text = myGridResults.OrderByDescending(e1 => e1.FechaVigenciaHasta).First().FechaVigenciaHasta.ToShortDateString();
                }

                decimal acomuladovalores_asegurado = vContratoService.ConsultarAmparosGarantiass(null, null, null, null,idGarantia).Sum(d => d.ValorAsegurado);

                txtValorGarantia.Text = acomuladovalores_asegurado.ToString("$ #,###0.00##;($ #,###0.00##)");

                toolBar.MostrarMensajeGuardado("El registro ha sido eliminado correctamente");
                
                return;
            }

        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion    

}


