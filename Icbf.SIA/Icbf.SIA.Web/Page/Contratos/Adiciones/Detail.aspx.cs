using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Contrato.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;

public partial class Page_Adiciones_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/Adiciones";
    string TIPO_ESTRUCTURA = "Adicion";
    private const string CONVENIO_INTERADMINSITRATIVO = "CONVENIO INTERADMINISTRATIVO";
    private const string CONTRATO_INTERADMINSITRATIVO = "CONTRATO INTERADMINISTRATIVO";

    ContratoService vContratoService = new ContratoService();

    private string nConsecutivosPlanCompras
    { set { hfConsecutivosPlanCompras.Value = value; } }
    //private string nProductos
    //{ set { hfProductos.Value = value; } }
    private string TotalProductos
    {
        get
        {
            return hfTotalProductos.Value;
        }
        set
        {
            hfTotalProductos.Value = value;
        }
    }
    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarRegistro();
            }
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("Contrato.IdContrato", IdContrato);
        SetSessionParameter("Adiciones.IdAdiccion", hfIdAdicion.Value);

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
        
        NavigateTo(SolutionPage.Edit);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            Response.Redirect("~/Page/Contratos/SolicitudesModificacion/Detail.aspx", false);
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value.ToString());

            if (hfEsSubscripcion.Value == "1")
                Response.Redirect("~/Page/Contratos/SubscripcionModificacion/Detail.aspx", false);
            else if (hfEsSubscripcion.Value == "2")
                Response.Redirect("~/Page/Contratos/RepartoModificaciones/Detail.aspx", false);
            else
                Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }
    
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.OcultarBotonNuevo(true);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.EstablecerTitulos("Adiciones", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    
    private void CargarDatosIniciales()
    {
        try
        {
            IdContrato = GetSessionParameter("Contrato.IdContrato").ToString();
            RemoveSessionParameter("Contrato.IdContrato");

            int vIdConModContratual;
            SolModContractual detalleSolicitud = null;


            if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractual.IDCosModContractual").ToString()))
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);
                hfEstado.Value = detalleSolicitud.Estado;

                if (hfEstado.Value == "Registro" || hfEstado.Value == "Devuelta")
                    toolBar.MostrarBotonEditar(true);
                else            
                    toolBar.MostrarBotonEditar(false);
            
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();
            }
            else
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);
                hfEstado.Value = detalleSolicitud.Estado;

                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");

                if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsSubscripcion").ToString()))
                {
                    hfEsSubscripcion.Value = "1";
                    RemoveSessionParameter("ConsModContractualGestion.EsSubscripcion");
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("ConsModContractualGestion.EsReparto").ToString()))
                {
                    hfEsSubscripcion.Value = "2";
                    RemoveSessionParameter("ConsModContractualGestion.EsReparto");                   
                }

                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();
                toolBar.MostrarBotonEditar(false);
            }

            hfIDCosModContractual.Value = vIdConModContratual.ToString();

            if(detalleSolicitud == null)
               detalleSolicitud = vContratoService.ConsultarSolitud(vIdConModContratual);

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoAdiciones(Convert.ToInt32(IdContrato));
            txtContrato.Text = string.IsNullOrEmpty(vContrato.NumeroContrato) ? string.Empty : vContrato.NumeroContrato;
            txtRegional.Text = vContrato.NombreRegional ?? vContrato.NombreRegional;
            txtFechaInicio.Text = Convert.ToDateTime(vContrato.FechaInicioEjecucion).ToString("dd/MM/yyyy");
            txtFechaFinal.Text =Convert.ToDateTime(vContrato.FechaFinalTerminacion).ToString("dd/MM/yyyy");
            txtobjetoContrato.Text = vContrato.ObjetoContrato;
            txtalcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            txtvalorini.Text = string.Format("{0:C}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:C}", vContrato.ValorFinalContrato);


                var misCDPS = vContratoService.ConsultarContratosCDP(Convert.ToInt32(IdContrato));
                if (misCDPS != null)
                {

                    string cdps = string.Empty;

                    foreach (var itemCDP in misCDPS)
                        cdps += itemCDP.NumeroCDP+",";

                    cdps = cdps.Substring(0, cdps.Length - 1);
                    txtCDP.Text = cdps;

                    decimal? valorActualCDPS = misCDPS.Sum(val => val.ValorActualCDP);

                    if (valorActualCDPS.HasValue)
                        txtValorCDP.Text = string.Format("{0:C}", valorActualCDPS.Value);
                }

                var misRPS = vContratoService.ConsultarRPContratosAsociados(Convert.ToInt32(IdContrato), null);
                if (misRPS != null)
                {
                    string rps = string.Empty;

                    foreach (var itemRP in misRPS)
                        rps += itemRP.NumeroRP+",";

                    rps = rps.Substring(0, rps.Length - 1);
                    txtRP.Text = rps;

                    decimal? valorActualRPS = misRPS.Sum(val => val.ValorActualRP);

                    if (valorActualRPS.HasValue)
                        txtValorRP.Text = string.Format("{0:C}", valorActualRPS.Value);                    
                  }
            
            gvanexos.EmptyDataText = EmptyDataText();
            gvanexos.PageSize = PageSize();
            gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(IdContrato), TIPO_ESTRUCTURA);
            gvanexos.DataBind();

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            gvAportesActuales.EmptyDataText = EmptyDataText();
            gvAportesActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            var aportesValoresActuales = vContratoService.ObtenerAportesContratoActuales(Convert.ToInt32(hfIdContrato.Value));
            gvAportesActuales.DataSource = aportesValoresActuales;
            gvAportesActuales.DataBind();

            //DdlConsecutivoPlanCompras.Items.Insert(0, new ListItem("Seleccionar", "-1"));
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }
    
    private void CargarRegistro()
    {
        try
        {
            try
            {
                if (!string.IsNullOrEmpty(GetSessionParameter("Adiciones.IdAdiccion").ToString()) || !string.IsNullOrEmpty(hfIdAdicion.Value))
                {
                    var itemContrato = vContratoService.ConsultarContratoReduccion(Convert.ToInt32(hfIdContrato.Value));

                    if (!string.IsNullOrEmpty(GetSessionParameter("Adiciones.IdAdiccion").ToString()))
                    {
                        int vIdAdicion = Convert.ToInt32(GetSessionParameter("Adiciones.IdAdiccion"));
                        RemoveSessionParameter("Adiciones.IdAdiccion");
                        hfIdAdicion.Value = vIdAdicion.ToString();

                        Adiciones vAdiciones = new Adiciones();
                        vAdiciones = vContratoService.ConsultarAdiciones(Convert.ToInt32(hfIdAdicion.Value));

                        var aportesAdiciones = vContratoService.ObtenerAportesContratoAdicionReduccion(vIdAdicion,true);
                        var valorAdiciones = aportesAdiciones.Sum(e => e.ValorAporte);
                        txtValorAdicion.Text = string.Format("{0:C}", valorAdiciones);

                        gvAportesAdicion.DataSource = aportesAdiciones;
                        gvAportesAdicion.DataBind();

                        txtValorAdicion.Text = string.Format("{0:$#,##0}", valorAdiciones);

                        if (aportesAdiciones != null && aportesAdiciones.Any(e => e.AportanteICBF == true && e.AporteEnDinero == true))
                        {
                            txtCDP.Visible = true;
                            lblCDPAdicion.Visible = true;
                            if (vAdiciones.IdCDP != 0)
                                txtCDP.Text = vAdiciones.IdCDP.ToString();
                        }
                                                
                        if (hfEstado.Value == "Suscrita")
                        {
                            txtvalorfinal.Text = string.Format("{0:$#,##0}", itemContrato.ValorFinal);
                        }
                        else
                        {
                            decimal totalValorConAdicion = valorAdiciones + itemContrato.ValorFinal;
                            txtvalorfinal.Text = string.Format("{0:$#,##0}", totalValorConAdicion);
                        }
                      
                        toolBar.OcultarBotonGuardar(false);
                        toolBar.MostrarBotonNuevo(false);

                        if (hfEstado.Value != "Registro" && hfEstado.Value != "Devuelta")
                        {
                            if(vAdiciones.ValorCDP > 0)
                            txtValorCDP.Text = string.Format("{0:$#,##0}", vAdiciones.ValorCDP);
                            if(vAdiciones.ValorRP > 0)
                            txtValorRP.Text = string.Format("{0:$#,##0}", vAdiciones.ValorRP);
                        }

                        //lblRP.Visible = true;
                        //txtRP.Visible = true;
                        //var item = vContratoService.ConsultarRPContratosAsociados(Convert.ToInt32(hfIdContrato.Value), vIdAdicion);
                        //if (item != null)
                        //    txtRP.Text = item.IdRP.ToString();

                        var vContratoEx = vContratoService.ConsultarContrato(Convert.ToInt32(IdContrato));

                        if (vContratoEx.NombreTipoContrato == CONTRATO_INTERADMINSITRATIVO || vContratoEx.NombreTipoContrato == CONVENIO_INTERADMINSITRATIVO)
                        {
                            txtJustificacionAdicion.Text = vAdiciones.JustificacionAdicion;
                            txtJustificacionAdicion.Visible = true;
                            lblJustificacion.Visible = true;
                        }
                        else
                        {
                            txtJustificacionAdicion.Visible = false;
                            lblJustificacion.Visible = false;
                        }
                    }
                    else
                    {
                        toolBar.MostrarBotonNuevo(true);
                        toolBar.OcultarBotonGuardar(true);
                    }
                }
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            //throw new GenericException(ex);
        }
    }

    #region  Cargar Documentos

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(int.Parse(hfIdContrato.Value), TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    #endregion
    protected void gvAportesAdicion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdAporteContrato = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdAporteContrato").ToString());
            bool AportanteICBF = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AportanteICBF").ToString());
            bool AporteEnDinero = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AporteEnDinero").ToString());
            bool EsAdicion = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "EsAdicion").ToString());


            if (AportanteICBF)
            {
                if (AporteEnDinero)
                {
                    string valor = string.Format("GetDetallePlanCompras(); return false;");
                    button.Attributes.Add("onclick", valor);
                    button.Visible = true;
                }
            }
        }
    }
}
