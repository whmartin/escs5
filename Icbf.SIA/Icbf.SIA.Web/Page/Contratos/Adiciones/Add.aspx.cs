using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Contrato.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Contrato.Entity;
using System.Data;
using System.Globalization;
using Icbf.SIA.Entity;
using Icbf.SIA.Service;
using System.Web.Security;
using Icbf.Seguridad.Service;

public partial class Page_Adiciones_Add : GeneralWeb
{
    masterPrincipal toolBar;
    ContratoService vContratoService = new ContratoService();
    SIAService vSIAService = new SIAService();
    SeguridadService vSeguridadFAC = new SeguridadService();
    Contrato vContrato = new Contrato();
    string PageName = "Contratos/Adiciones";
    string TIPO_ESTRUCTURA = "Adicion";

    private const string CONVENIO_INTERADMINSITRATIVO = "CONVENIO INTERADMINISTRATIVO";
    private const string CONTRATO_INTERADMINSITRATIVO = "CONTRATO INTERADMINISTRATIVO";

    #region IdentificadoresFijos
    private string IdContrato
    {
        get
        {
            return hfIdContrato.Value;
        }
        set
        {
            hfIdContrato.Value = value;
        }
    }

    private string IdCategoriaConvenio
    {
        get
        {
            return hfIdCategoriaConvenio.Value;
        }
        set
        {
            hfIdCategoriaConvenio.Value = value;
        }
    }

    private string IdTipoContConvPrestServApoyoGestion
    {
        get
        {
            return hfIdTipoContConvPrestServApoyoGestion.Value;
        }
        set
        {
            hfIdTipoContConvPrestServApoyoGestion.Value = value;
        }
    }
    private string IdTipoContConvPrestServProfesionales
    {
        get
        {
            return hfIdTipoContConvPrestServProfesionales.Value;
        }
        set
        {
            hfIdTipoContConvPrestServProfesionales.Value = value;
        }
    }

    private string IdMarcoInteradministrativo
    {
        get
        {
            return hfIdMarcoInteradministrativo.Value;
        }
        set
        {
            hfIdMarcoInteradministrativo.Value = value;
        }
    }

    private string IdContratacionDirecta
    {
        get
        {
            return hfIdContratacionDirecta.Value;
        }
        set
        {
            hfIdContratacionDirecta.Value = value;
        }
    }
    private string IdContratacionDirectaAporte
    {
        get
        {
            return hfIdContratacionDirectaAporte.Value;
        }
        set
        {
            hfIdContratacionDirectaAporte.Value = value;
        }
    }
    #endregion

    #region variables
    private string CodContratoAsociadoSeleccionado
    {
        get
        {
            return hfCodContratoAsociadoSel.Value;
        }
        set
        {
            hfCodContratoAsociadoSel.Value = value;
        }
    }
    private string TotalProductos
    {
        get
        {
            return hfTotalProductos.Value;
        }
        set
        {
            hfTotalProductos.Value = value;
        }
    }
    private string TotalProductosCDP
    {
        get
        {
            return hfTotalProductosCDP.Value;
        }
        set
        {
            hfTotalProductosCDP.Value = value;
        }
    }
    
    private string IdContratoAsociado
    {
        get
        {
            return hfIdConvenioContratoAsociado.Value;
        }
        set
        {
            //hfIdConvenioContratoAsociado.Value = value;
        }
    }
    
    private string ValorInicial
    {
        get
        {
            return hfValorInicialContConv.Value;
        }
        set
        {
            hfValorInicialContConv.Value = value;
        }
    }
    
    private string FechaInicio
    {
        get
        {
            return hfFechaInicioEjecucion.Value;
        }
        set
        {
            hfFechaInicioEjecucion.Value = value;
        }
    }
    
    private string FechaFinalizacion
    {
        get
        {
            return hfFechaFinalizacion.Value;
        }
        set
        {
            hfFechaFinalizacion.Value = value;
        }
    }
    
    private string AcordeonActivo
    {
        get
        {
            if (hfAcordeonActivo.Value.Equals(string.Empty))
                hfAcordeonActivo.Value = "1";
            return hfAcordeonActivo.Value;
        }
        set
        {
            hfAcordeonActivo.Value = value;
        }
    }
    
    private string ObjetoPlanCompras
    {
        get
        {
            return hfObjPlan.Value;
        }
        set
        {
            hfObjPlan.Value = value;
        }
    }
  
    private string AlcancePlanCompras
    {
        get
        {
            return hfAlcPlan.Value;
        }
        set
        {
            hfAlcPlan.Value = value;
        }
    }

    //private string nConsecutivosPlanCompras
    //{ set { hfConsecutivosPlanCompras.Value = value; } }
   
    //private string nProductos
    //{ set { hfProductos.Value = value; } }

    public decimal ValorContrato
    {
        get { return decimal.Parse(hfValorContrato.Value); }
        set { hfValorContrato.Value = value.ToString(); }
    }
    
    #endregion

    /// <summary>
    /// Método donde se establece el título del módulo y se inicializan los delegados de los botones a mostrar 
    /// </summary>
    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            //toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoRetornar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.OcultarBotonBuscar(true);
            toolBar.EstablecerTitulos("Adiciones", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    /// <summary>
    /// Método de carga de listas y valores por defecto 
    /// </summary>
    private void CargarDatosIniciales()
    {
        try
        {
            //cargar encabezado
            
            IdContrato = GetSessionParameter("Contrato.IdContrato").ToString();
            RemoveSessionParameter("Contrato.IdContrato");

            int vIdConModContratual;

            if (GetSessionParameter("ConsModContractual.IDCosModContractual") != null)
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractual.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractual.IDCosModContractual");
                hfIdConsModContractual.Value = vIdConModContratual.ToString();
            }
            else
            {
                vIdConModContratual = Convert.ToInt32(GetSessionParameter("ConsModContractualGestion.IDCosModContractual"));
                RemoveSessionParameter("ConsModContractualGestion.IDCosModContractual");
                hfIdConsModContractualGestion.Value = vIdConModContratual.ToString();
            }

            Contrato vContrato = new Contrato();
            vContrato = vContratoService.ConsultarContratoAdiciones(Convert.ToInt32(IdContrato));
            hfRegional.Value = vContrato.IdRegionalContrato.ToString();
            txtContrato.Text =   string.IsNullOrEmpty(vContrato.NumeroContrato) ? string.Empty: vContrato.NumeroContrato;
            txtRegional.Text = vContrato.NombreRegional ?? vContrato.NombreRegional ;
            txtFechaInicio.Text = Convert.ToDateTime(vContrato.FechaInicioEjecucion).ToString("dd/MM/yyyy");
            txtFechaFinal.Text = Convert.ToDateTime(vContrato.FechaFinalTerminacion).ToString("dd/MM/yyyy");
            txtobjetoContrato.Text = vContrato.ObjetoContrato;
            txtalcanceContrato.Text = vContrato.AlcanceObjetoContrato;
            txtvalorini.Text = string.Format("{0:C}", vContrato.ValorInicialContrato);
            txtvalorfinal.Text = string.Format("{0:C}",  vContrato.ValorFinalContrato);

            var misCDPS = vContratoService.ConsultarContratosCDPLinea(Convert.ToInt32(IdContrato));
            if (misCDPS != null)
	        {
                decimal ? valorActualCDPS = misCDPS.Sum(val => val.ValorActualCDP);

                string cdps = string.Empty;

                foreach (var itemCDP in misCDPS)
                    cdps += itemCDP.NumeroCDP+",";

                if (valorActualCDPS.HasValue)
                  txtValorCDP.Text = string.Format("{0:C}", valorActualCDPS.Value);

                if (!string.IsNullOrEmpty(cdps))
                {
                    cdps = cdps.Substring(0, cdps.Length - 1);
                    txtCDP.Text = cdps;
                }
	        }

            var misRPS = vContratoService.ConsultarRPContratosAsociados(Convert.ToInt32(IdContrato),null);
            if (misRPS != null)
            {
                decimal? valorActualRPS = misRPS.Sum(val => val.ValorActualRP);

                string rps = string.Empty;

                foreach (var itemRP in misRPS)
                    rps += itemRP.NumeroRP + ",";

                if (valorActualRPS.HasValue)
                txtValorRP.Text = string.Format("{0:C}", valorActualRPS.Value);

                rps = rps.Substring(0, rps.Length - 1);
                txtRP.Text = rps;                
            }

            ValorContrato = vContrato.ValorFinalContrato ?? vContrato.ValorInicialContrato ?? 0;

            gvSupervisoresActuales.EmptyDataText = EmptyDataText();
            gvSupervisoresActuales.PageSize = PageSize();

            gvAportesActuales.EmptyDataText = EmptyDataText();
            gvAportesActuales.PageSize = PageSize();

            List<SupervisorInterContrato> supervisoresInterventores = vContratoService.ObtenerSupervisoresInterventoresContrato(Convert.ToInt32(hfIdContrato.Value), null);
            if (supervisoresInterventores != null)
            {
                var querySupervisores = supervisoresInterventores.Where(e1 => e1.IDTipoSuperInter == 1).ToList();
                gvSupervisoresActuales.DataSource = querySupervisores;
                gvSupervisoresActuales.DataBind();
            }

            var aportesContrato = vContratoService.ObtenerAportesContrato(Convert.ToInt32(hfIdContrato.Value),false);

            List<AporteContrato> misAportes = new List<AporteContrato>();

            foreach (var item in aportesContrato)
            {
                if (!misAportes.Any(e => e.AportanteICBF == item.AportanteICBF && e.AporteEnDinero == item.AporteEnDinero))
                    misAportes.Add(item);
            }

            gvAportesICBF.DataSource = misAportes;
            gvAportesICBF.DataBind();

            var aportesValoresActuales = vContratoService.ObtenerAportesContratoActuales(Convert.ToInt32(hfIdContrato.Value));
            gvAportesActuales.DataSource = aportesValoresActuales;
            gvAportesActuales.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            throw;
        }
        catch (Exception ex)
        {
            throw new GenericException(ex);
        }
    }

    private void SeleccionarPanel(string commandNamePanel)
    {
        switch (commandNamePanel)
        {
            case "DatosGeneralContrato":
                break;
            case "PlanComprasProductos":
                break;
            case "VigenciaValorContrato":
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Método que carga los datos del registro a editar
    /// </summary>
    private void CargarRegistro()
    {
            try
            {
                gvanexos.EmptyDataText = EmptyDataText();
                gvanexos.PageSize = PageSize();

                gvAportesAdicion.EmptyDataText = EmptyDataText();
                gvAportesAdicion.PageSize = PageSize();
                                                                                        
                if (!string.IsNullOrEmpty(GetSessionParameter("Adiciones.IdAdiccion").ToString()))
                {
                    int vIdAdicion = Convert.ToInt32(GetSessionParameter("Adiciones.IdAdiccion"));
                    RemoveSessionParameter("Adiciones.IdAdiccion");
                    hfIdAdicion.Value = vIdAdicion.ToString();
                    PanelArchivos.Visible = true;

                    gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(Convert.ToInt32(hfIdContrato.Value), TIPO_ESTRUCTURA);
                    gvanexos.DataBind();

                    var aportesAdiciones = vContratoService.ObtenerAportesContratoAdicionReduccion(vIdAdicion,true);
                    var valorAdiciones = aportesAdiciones.Sum(e => e.ValorAporte);
                    txtValorAdicion.Text = string.Format("{0:C}", valorAdiciones);

                    decimal totalValorConAdicion = valorAdiciones + ValorContrato;
                    txtvalorfinal.Text = string.Format("{0:C}", totalValorConAdicion);

                    Adiciones vAdiciones = null;

                    toolBar.OcultarBotonGuardar(false);
                    gvAportesAdicion.DataSource = aportesAdiciones;
                    gvAportesAdicion.DataBind();

                    toolBar.MostrarBotonNuevo(false);

                    var vContratoEx = vContratoService.ConsultarContrato(Convert.ToInt32(IdContrato));

                    if (vContratoEx.NombreTipoContrato.ToUpper() == CONTRATO_INTERADMINSITRATIVO ||
                        vContratoEx.NombreTipoContrato.ToUpper() == CONVENIO_INTERADMINSITRATIVO)
                    {
                        if(vAdiciones == null)
                           vAdiciones = vContratoService.ConsultarAdiciones(vIdAdicion);

                        txtJustificacionAdicion.Text = vAdiciones.JustificacionAdicion;
                        rfvJustificacionAdicion.Enabled = true;
                        rfvJustificacionAdicion.Visible = true;
                        txtJustificacionAdicion.Enabled = true;
                        txtJustificacionAdicion.Visible = true;
                        lblJustificacion.Visible = true;
                        toolBar.OcultarBotonGuardar(true);
                    }
                    else
                    {
                        rfvJustificacionAdicion.Enabled = false;
                        rfvJustificacionAdicion.Visible = false;
                        txtJustificacionAdicion.Enabled = false;
                        txtJustificacionAdicion.Visible = false;
                        lblJustificacion.Visible = false;
                    }
                }
                else if (!string.IsNullOrEmpty(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual").ToString()))
                {
                    int vIDDetalleConsModContractual = Convert.ToInt32(GetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual"));
                    RemoveSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual");
                    hfIDDetalleConsModContractual.Value = vIDDetalleConsModContractual.ToString();
                    toolBar.MostrarBotonNuevo(true);
                    toolBar.OcultarBotonGuardar(false);

                    gvanexos.DataSource = new List<ArchivoContrato>();
                    gvanexos.DataBind();

                    gvAportesAdicion.DataSource = new List<AporteContrato>();
                    gvAportesAdicion.DataBind();
                }
            }
            catch (UserInterfaceException ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
    }
 
    #region Eventos

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string controlFechaValidando = string.Empty;
        //Response.HeaderEncoding = System.Text.Encoding.UTF32;

        vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;
        try
        {
            toolBar.LipiarMensajeError();
            if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
            {
                if (!Page.IsPostBack)
                {
                    CargarDatosIniciales();
                    CargarRegistro();
                }
                else
                {
                    #region AdministraPostBack
                    string sControlName = Request.Params.Get("__EVENTTARGET");
                    switch (sControlName)
                    {
                        case "cphCont_txtPlanCompras":
                            txtPlanComprasTextChanged(sender, e);
                            CargarRegistro();
                            break;
                        case "cphCont_txtRP":
                            txtRP_TextChanged(sender, e);
                            break;
                        case "ActualizarValores":
                            ActualizarAportes();
                            break;
                        case "cphCont_txtCDP":
                            txtCDP_TextChanged(sender, e);
                            break;
                        default:
                            break;
                    }
                    #endregion

                }
            }
        }
        catch (FormatException ex)
        {
            toolBar.MostrarMensajeError("El formato del campo " + controlFechaValidando + " es inválido.");
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {

                var adiccion = vContratoService.ConsultarAdiciones(int.Parse(hfIdAdicion.Value));
                adiccion.JustificacionAdicion = txtJustificacionAdicion.Text;
                vContratoService.ModificarAdiciones(adiccion);                
            

        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        if (hfIdConsModContractual.Value != "")
        {
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value.ToString());
            NavigateTo("~/Page/Contratos/SolicitudesModificacion/Detail.aspx");
        }
        else
        {
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value.ToString());
            Response.Redirect("~/Page/Contratos/ConsModContractual/Detail.aspx", false);
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        SetSessionParameter("DetalleConsModContractual.IDDetalleConsModContractual", hfIdDetConsModContractual.Value);
        SetSessionParameter("Contrato.IdContrato", hfIdContrato.Value);

        if (hfIdConsModContractual.Value != "")
            SetSessionParameter("ConsModContractual.IDCosModContractual", hfIdConsModContractual.Value);
        else
            SetSessionParameter("ConsModContractualGestion.IDCosModContractual", hfIdConsModContractualGestion.Value);

        NavigateTo(SolutionPage.Add);
    }

    private bool ValidacionSumatoriaProductosVsRubros()
    {
        return true;
        //foreach (GridViewRow gvr in gvConsecutivos.Rows)
        //{
        //    string numeroConsecutivo = gvConsecutivos.DataKeys[gvr.RowIndex]["IDPlanDeCompras"].ToString();

        //    var vTotalProductosPorConsecutivo = (from vListRows in gvProductos.Rows.Cast<GridViewRow>()
        //                                         let vTipoProd = vListRows.Cells[3].Text
        //                                         where vListRows.Cells[0].Text == numeroConsecutivo//GetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo").ToString()
        //                                         select new
        //                                         {
        //                                             vTotalProductos = vTipoProd == "SERVICIO" ?
        //                                                               Convert.ToDecimal(vListRows.Cells[4].Text) *
        //                                                               Convert.ToDecimal(decimal.Parse(vListRows.Cells[5].Text, NumberStyles.Currency)) *
        //                                                               Convert.ToDecimal(vListRows.Cells[7].Text) :
        //                                                               Convert.ToDecimal(vListRows.Cells[4].Text) *
        //                                                               Convert.ToDecimal(decimal.Parse(vListRows.Cells[5].Text, NumberStyles.Currency))
        //                                         }).Sum(d => d.vTotalProductos);

        //    var vTotalRubrosPorConsecutivo = (from vListRows in gvRubrosPlanCompras.Rows.Cast<GridViewRow>()
        //                                      where vListRows.Cells[0].Text == numeroConsecutivo//GetSessionParameter("LupasRelacionarPlanCompras.NumeroConsecutivo").ToString()
        //                                      select new
        //                                      {
        //                                          vTotalRubros = Convert.ToDecimal(decimal.Parse(vListRows.Cells[4].Text, NumberStyles.Currency))
        //                                      }).Sum(d => d.vTotalRubros);

        //    if (vTotalProductosPorConsecutivo != vTotalRubrosPorConsecutivo)
        //    {
        //        return false;
        //    }
        //}

        //return true;
    }

    protected void AccContratos_ItemCommand(object sender, CommandEventArgs e)
    {
        SeleccionarPanel(e.CommandName);
    }

    protected void ddlRegionalUsuarioContratoSelectedIndexChanged(object sender, EventArgs e)
    {
        //SeleccionaRegional(ddlRegionalUsuarioContrato.SelectedValue);
    }

    protected void txtPlanComprasTextChanged(object sender, EventArgs e)
    {
        //SeleccionarPlanCompras("1"); //txtPlanCompras.Text);
    }

    protected void DdlConsecutivoPlanComprasSelectedIndexChanged(object sender, EventArgs e)
    {
        //SeleccionaConsecutivoPlanDeCompras(DdlConsecutivoPlanCompras.SelectedValue);
    }

    protected void txtRP_TextChanged(object sender, EventArgs e)
    {
        try
        {
            //int idContrato = Convert.ToInt32(hfIdContrato.Value);
            //int idAdicion = Convert.ToInt32(hfIdAdicion.Value);
            //var item = vContratoService.ConsultarRPContratosAsociados(idContrato, idAdicion);
            //if (item != null)
            //    txtRP.Text = item.IdRP.ToString();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion

    #region  Cargar Documentos

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            //vIdIndice = Convert.ToInt64(strIDCosModContractual);
            //hfIndice.Value = strIDCosModContractual;
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void EliminarAnexo(GridViewRow pRow)
    {
        try
        {
            int vResultado = 0;
            decimal indice = 0;
            int rowIndex = pRow.RowIndex;
            string strIDCosModContractual = gvanexos.DataKeys[rowIndex].Values[0].ToString();
            indice = Convert.ToInt64(strIDCosModContractual);

            if (indice != 0)
            {
                vResultado = vContratoService.EliminarDocumentoAnexoContrato(indice);
                int vIdContrato = Convert.ToInt32(hfIdContrato.Value);
                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(vIdContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvanexos_PageIndexChanging(object sender, EventArgs e)
    {
        int idContrato = Convert.ToInt32(hfIdContrato.Value);
        gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
        gvanexos.DataBind();
    }

    protected void gvanexos_SelectedIndexChanged(object sender, EventArgs e)
    {
        EliminarAnexo(gvanexos.SelectedRow);
    }

    protected void CargarArchivoFTP(object sender, ImageClickEventArgs e)
    {
        toolBar.LipiarMensajeError();

        int idContrato = Convert.ToInt32(hfIdContrato.Value);

        FileUpload fuArchivo = FileUploadArchivoContrato;

        if (fuArchivo.HasFile)
        {
            try
            {
                ManejoControlesContratos controles = new ManejoControlesContratos();
                controles.CargarArchivoFTPContratos
                    (
                     TIPO_ESTRUCTURA,
                     fuArchivo,
                     idContrato,
                     GetSessionUser().IdUsuario
                    );

                gvanexos.DataSource = vContratoService.ConsultarArchivoTipoEstructurayContrato(idContrato, TIPO_ESTRUCTURA);
                gvanexos.DataBind();
            }
            catch (Exception ex)
            {
                toolBar.MostrarMensajeError(ex.Message);
            }
        }
    }

    #endregion

    #region Manejo de Aportes

    protected void gvAportesICBF_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdAporteContrato = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdAporteContrato").ToString());
            bool AportanteICBF = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AportanteICBF").ToString());
            bool AporteEnDinero = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AporteEnDinero").ToString());
            bool EsAdicion = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "EsAdicion").ToString());

            if (!EsAdicion)
            {
                if (AportanteICBF)
                {
                    if (AporteEnDinero)
                    {
                        string valor = string.Format("GetPlanComprasNuevoV11('{0}'); return false;", IdAporteContrato.ToString());
                        button.Attributes.Add("onclick", valor);
                    }
                    else
                    {
                        string valor = string.Format("GetAportes('{0}'); return false;", IdAporteContrato.ToString());
                        button.Attributes.Add("onclick", valor);
                    }
                }
                else
                {
                    string valor = string.Format("GetAportes('{0}'); return false;", IdAporteContrato.ToString());
                    button.Attributes.Add("onclick", valor);
                }
            }
            else
                button.Visible = false;
        }
    }

    private void ActualizarAportes()
    {
        if (!string.IsNullOrEmpty(GetSessionParameter("Adiciones.IdAdiccion").ToString()))
        {
            hfIdAdicion.Value = GetSessionParameter("Adiciones.IdAdiccion").ToString();
            RemoveSessionParameter("Adiciones.IdAdiccion");
        }

        if (!string.IsNullOrEmpty(GetSessionParameter("Adiciones.Guardo").ToString()))
        {
            toolBar.MostrarMensajeGuardado("Se guardo el aporte correctamente");
            RemoveSessionParameter("Adiciones.Guardo");
        }
        if (! string.IsNullOrEmpty(hfIdAdicion.Value))
        {
            int vIdAdicion = int.Parse(hfIdAdicion.Value);
            var aportesAdiciones = vContratoService.ObtenerAportesContratoAdicionReduccion(vIdAdicion,true);
            var valorAdiciones = aportesAdiciones.Sum(e => e.ValorAporte);
            txtValorAdicion.Text = string.Format("{0:C}", valorAdiciones);

            decimal totalValorConAdicion = valorAdiciones + ValorContrato;
            txtvalorfinal.Text = string.Format("{0:C}", totalValorConAdicion);

            gvAportesAdicion.DataSource = aportesAdiciones;
            gvAportesAdicion.DataBind();

            var vContratoEx = vContratoService.ConsultarContrato(Convert.ToInt32(IdContrato));

            if(vContratoEx.NombreTipoContrato.ToUpper() == CONTRATO_INTERADMINSITRATIVO ||
                vContratoEx.NombreTipoContrato.ToUpper() == CONVENIO_INTERADMINSITRATIVO)
            {
                rfvJustificacionAdicion.Enabled = true;
                rfvJustificacionAdicion.Visible = true;
                txtJustificacionAdicion.Enabled = true;
                txtJustificacionAdicion.Visible = true;
                lblJustificacion.Visible = true;
                toolBar.OcultarBotonGuardar(true);
            }
        }
    }

    #endregion

    #region Aportes Adiciones

    protected void gvAportesAdicion_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton button = (LinkButton)e.Row.FindControl("btnEditar");
            int IdAporteContrato = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IdAporteContrato").ToString());
            bool AportanteICBF = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AportanteICBF").ToString());
            bool AporteEnDinero = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "AporteEnDinero").ToString());
            bool EsAdicion = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "EsAdicion").ToString());


            if (AportanteICBF)
            {
                if (AporteEnDinero)
                {
                    string valor = string.Format("GetDetallePlanCompras(); return false;");
                    button.Attributes.Add("onclick", valor);
                    button.Visible = true;
                }
            }
        }
    }

    #endregion

    #region CDP

    protected void txtCDP_TextChanged(object sender, EventArgs e)
    {
        try
        {
            //int vIdAdicion;

            //if (int.TryParse(hfIdAdicion.Value, out vIdAdicion))
            //{
            //    var vAdiciones = vContratoService.ConsultarAdiciones(vIdAdicion);
            //    lblCDPAdicion.Visible = true;
            //    txtCDP.Visible = true;
            //    imgCDP.Visible = true;
            //    if (vAdiciones.IdCDP != 0)
            //        txtCDP.Text = vAdiciones.IdCDP.ToString();
            //}
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    #endregion
}
