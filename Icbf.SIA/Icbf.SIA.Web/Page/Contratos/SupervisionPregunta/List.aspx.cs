using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionPreguntas_List : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionPregunta";
    SupervisionService vSupervisionService = new SupervisionService();
    List<SupervisionDireccion> lstDireccion;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.List;
        if (ValidateAccess(toolBar, PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                //Session.Clear["Preguntasupervision"];
            }
        }
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        Buscar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    private void Buscar()
    {
        try
        {
            int? vIdDireccion = null;
            String vNombre = null;
            String vDescripcion = null;
            int? vIdTipoRespuesta = null;
            int? vAplicable = null;
            int? vResponsable = null;
            int? vEstado = null;
            if (ddlIdDireccion.SelectedValue!= "-1")
            {
                vIdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            }
            if (txtNombre.Text!= "")
            {
                vNombre = Convert.ToString(txtNombre.Text);
            }
            if (ddlIdTipoRespuesta.SelectedValue!= "-1")
            {
                vIdTipoRespuesta = Convert.ToInt32(ddlIdTipoRespuesta.SelectedValue);
            }
            if (rblAplicable.SelectedValue!= "-1")
            {
                vAplicable = Convert.ToInt32(rblAplicable.SelectedValue);
            }
            if (ddlResponsable.SelectedValue!= "-1")
            {
                vResponsable = Convert.ToInt32(ddlResponsable.SelectedValue);
            }
            if (rblEstado.SelectedValue!= "-1")
            {
                vEstado = Convert.ToInt32(rblEstado.SelectedValue);
            }
            lstDireccion = vSupervisionService.ConsultarSupervisionDireccions(null, "", "", 1);
            gvSupervisionPreguntas.DataSource = vSupervisionService.ConsultarSupervisionPreguntass( null, vIdDireccion, vNombre, vDescripcion, vIdTipoRespuesta, vAplicable, vResponsable, vEstado, "");
            gvSupervisionPreguntas.DataBind();
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            gvSupervisionPreguntas.PageSize = PageSize();
            gvSupervisionPreguntas.EmptyDataText = EmptyDataText();

            toolBar.EstablecerTitulos("Preguntas", SolutionPage.List.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void SeleccionarRegistro(GridViewRow pRow)
    {
        try
        {
            int rowIndex = pRow.RowIndex;
            string strValue = gvSupervisionPreguntas.DataKeys[rowIndex].Value.ToString();
            SetSessionParameter("SupervisionPreguntas.IdPregunta", strValue);
            NavigateTo(SolutionPage.Detail);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    protected void gvSupervisionPreguntas_SelectedIndexChanged(object sender, EventArgs e)
    {
        SeleccionarRegistro(gvSupervisionPreguntas.SelectedRow);
    }
    protected void gvSupervisionPreguntas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSupervisionPreguntas.PageIndex = e.NewPageIndex;
        Buscar();
    }
    private void CargarDatosIniciales()
    {
        try
        {
            if (GetSessionParameter("SupervisionPreguntas.Eliminado").ToString() == "1")
                toolBar.MostrarMensajeEliminado();
            RemoveSessionParameter("SupervisionPreguntas.Eliminado");


            ManejoControlesSupervision.LlenarAplicable(rblAplicable, "1");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo","1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlearTipoRespuesta(ddlIdTipoRespuesta, "-1", true);
            Controles.LlenarResponsable(ddlResponsable, "-1", true,null);
            Controles.LlenarDireccion(ddlIdDireccion, "-1",true );


			
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlIdDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdDireccion.SelectedValue == "1")
        {
            ddlResponsable.Enabled = true;
            ddlResponsable.Visible = true;
            lblResponsable.Visible = true;
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarResponsable(ddlResponsable, "-1", true, Int16.Parse(ddlIdDireccion.SelectedValue));
        }
        else
        {
            ddlResponsable.Enabled = false;
            ddlResponsable.Visible = false;
            lblResponsable.Visible = false;


        }

    }

    protected void gvSupervisionPreguntas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow            )
        {
            //Se busca el nombre de las direcciones
            var vRegistrosDireccion = from resp in lstDireccion
                                      where resp.IdDireccionesICBF == Convert.ToInt16(e.Row.Cells[1].Text)
                                      select resp;
            e.Row.Cells[1].Text = vRegistrosDireccion.ToList()[0].NombreDireccion;


            switch (e.Row.Cells[5].Text)
            {
                case "0":
                    e.Row.Cells[5].Text = "UDS";
                    break;
                default:
                    e.Row.Cells[5].Text = "EC";
                    break;
            }

            //Se busca el estado
            switch (e.Row.Cells[7].Text)
            {
                case "1":
                    e.Row.Cells[7].Text = "Activo";
                    break;
                default:
                    e.Row.Cells[7].Text = "Desactivado";
                    break;
            }


           
        }
    }
}
