using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Supervision.Service;
using Icbf.Utilities.Presentation;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionPreguntas_Detail : GeneralWeb
{
    masterPrincipal toolBar;
    string PageName = "Contratos/SupervisionPregunta";
    SupervisionService vSupervisionService = new SupervisionService();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Detail;
        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                CargarDatos();
            }
        }
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        SetSessionParameter("SupervisionPreguntas.IdPregunta", hfIdPregunta.Value);
        NavigateTo(SolutionPage.Edit);
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        EliminarRegistro();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }
    private void CargarDatos()
    {
        try
        {
            int vIdPregunta = Convert.ToInt32(GetSessionParameter("SupervisionPreguntas.IdPregunta"));
            RemoveSessionParameter("SupervisionPreguntas.IdPregunta");

            if (GetSessionParameter("SupervisionPreguntas.Guardado").ToString() == "1")
                toolBar.MostrarMensajeGuardado();
            RemoveSessionParameter("SupervisionPreguntas.Guardado");


            SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
            vSupervisionPreguntas = vSupervisionService.ConsultarSupervisionPreguntas(vIdPregunta);
            hfIdPregunta.Value = vSupervisionPreguntas.IdPregunta.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionPreguntas.IdDireccion.ToString();
            txtNombre.Text = vSupervisionPreguntas.Nombre;
            txtDescripcion.Text = vSupervisionPreguntas.Descripcion;
            ddlIdTipoRespuesta.SelectedValue = vSupervisionPreguntas.IdTipoRespuesta.ToString();
            rblAplicable.SelectedValue = vSupervisionPreguntas.Aplicable.ToString();
            rblEstado.SelectedValue = vSupervisionPreguntas.Estado.ToString();

            if (ddlIdDireccion.SelectedValue == "1")
            {
                ddlResponsable.Visible = true;
                lblResponsable.Visible = true;
                ManejoControlesSupervision Controles = new ManejoControlesSupervision();
                Controles.LlenarResponsable(ddlResponsable, "-1", true, Int16.Parse(ddlIdDireccion.SelectedValue));
                ddlResponsable.SelectedValue = vSupervisionPreguntas.Responsable.ToString();
                
            }
            else
            {
                ddlResponsable.Enabled = false;
                ddlResponsable.Visible = false;
                lblResponsable.Visible = false;
            }


            ObtenerAuditoria(PageName, hfIdPregunta.Value);
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionPreguntas.UsuarioCrea, vSupervisionPreguntas.FechaCrea, vSupervisionPreguntas.UsuarioModifica, vSupervisionPreguntas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void EliminarRegistro()
    {
        try
        {
            int vIdPregunta = Convert.ToInt32(hfIdPregunta.Value);

            SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
            vSupervisionPreguntas = vSupervisionService.ConsultarSupervisionPreguntas(vIdPregunta);
            InformacionAudioria(vSupervisionPreguntas, this.PageName, vSolutionPage);
            int vResultado = vSupervisionService.EliminarSupervisionPreguntas(vSupervisionPreguntas);
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                toolBar.MostrarMensajeError("La operación se completo satisfactoriamente.");
                SetSessionParameter("SupervisionPreguntas.Eliminado", "1");
                NavigateTo(SolutionPage.List);
            }
            else
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void Iniciar()
    {
        try
        {


            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);
            toolBar.eventoEditar += new ToolBarDelegate(btnEditar_Click);
            toolBar.eventoEliminar += new ToolBarDelegate(btnEliminar_Click);
            toolBar.eventoCambiarOpcion += new ToolBarDelegate(ddlExtends_SelectedIndexChanged);
            toolBar.EstablecerTitulos("Preguntas", SolutionPage.Detail.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            toolBar.AgregarOpcionAdicional(new ListItem("Ir a", "0"));
            toolBar.AgregarOpcionAdicional(new ListItem("Respuestas", "1"));
            toolBar.AgregarOpcionAdicional(new ListItem("Criterios", "2"));


            ManejoControlesSupervision.LlenarAplicable(rblAplicable, "1");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo","1");
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlearTipoRespuesta(ddlIdTipoRespuesta, "-1", true);
            Controles.LlenarResponsable(ddlResponsable, "-1", true, null);
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);


        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlIdDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdDireccion.SelectedValue == "1")
        {
            ddlResponsable.Enabled = true;
            ddlResponsable.Visible = true;
            lblResponsable.Visible = true;
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarResponsable(ddlResponsable, "-1", true, Int16.Parse(ddlIdDireccion.SelectedValue));
        }
        else
        {
            ddlResponsable.Enabled = false;
            ddlResponsable.Visible = false;
            lblResponsable.Visible = false;


        }
    }

    protected void ddlExtends_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<string> Preguntasupervision = new List<string>();
        Preguntasupervision.Add(txtNombre.Text);
        Preguntasupervision.Add(hfIdPregunta.Value);
        Preguntasupervision.Add(ddlIdDireccion.SelectedValue);
        Session.Add("Preguntasupervision", Preguntasupervision);
        switch (((DropDownList)sender).SelectedValue)
        {
            case "1":
                {
                    
                    NavigateTo("~/Page/Contratos/SupervisionRespuesta/Add.aspx", true);

                    break;
                }
            case "2":
                {
                    NavigateTo("~/Page/Contratos/SupervisionCriterio/Add.aspx", true);

                    break;
                }
            default:
                break;
        }
    }
}
