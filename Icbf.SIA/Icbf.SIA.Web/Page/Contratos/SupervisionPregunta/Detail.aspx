<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="Detail.aspx.cs" Inherits="Page_SupervisionPreguntas_Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:HiddenField ID="hfIdPregunta" runat="server" />
    <table width="90%" align="center">
        <tr class="rowB">
            <td>
                Dirección *
            </td>
            <td>
                Aplicable a *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdDireccion" Enabled="false" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlIdDireccion_SelectedIndexChanged" Width="80%">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblAplicable" RepeatDirection="Horizontal"
                    Enabled="false">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Pregunta *
            </td>
            <td>
                Descripción de la pregunta *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:TextBox runat="server" ID="txtNombre" Enabled="false" Width="80%"></asp:TextBox>
            </td>
            <td rowspan="3">
                <asp:TextBox runat="server" ID="txtDescripcion" Enabled="false" Width="80%" TextMode="MultiLine"
                    Height="68px"></asp:TextBox>
            </td>
        </tr>
        <tr class="rowB">
            <td>
                Tipo de respuesta *
            </td>
        </tr>
        <tr class="rowA">
            <td>
                <asp:DropDownList runat="server" ID="ddlIdTipoRespuesta" Enabled="false">
                </asp:DropDownList>
            </td>
            <tr class="rowB">
                <td>
                    Estado *
                </td>
                <td>
                    <asp:Label ID="lblResponsable" runat="server" Text="Responsable *"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal" Enabled="false">
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlResponsable" Enabled="false" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
        </tr>
    </table>
</asp:Content>
