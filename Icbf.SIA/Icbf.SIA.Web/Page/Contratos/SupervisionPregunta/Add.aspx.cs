using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Icbf.Utilities.Presentation;
using Icbf.Supervision.Service;
using Icbf.Utilities.Exceptions;
using Icbf.Supervision.Entity;

public partial class Page_SupervisionPreguntas_Add : GeneralWeb
{
    masterPrincipal toolBar;
    SupervisionService vSupervisionService = new SupervisionService();
    string PageName = "Contratos/SupervisionPregunta";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Iniciar();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolutionPage vSolutionPage = SolutionPage.Add;
        if (Request.QueryString["oP"] == "E")
            vSolutionPage = SolutionPage.Edit;

        if (ValidateAccess(toolBar, this.PageName, vSolutionPage))
        {
            if (!Page.IsPostBack)
            {
                CargarDatosIniciales();
                if (Request.QueryString["oP"] == "E")
                    CargarRegistro();
            }
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.Add);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        NavigateTo(SolutionPage.List);
    }

    private void Guardar()
    {
        try
        {
            int vResultado;
            SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();

            vSupervisionPreguntas.IdDireccion = Convert.ToInt32(ddlIdDireccion.SelectedValue);
            vSupervisionPreguntas.Nombre = Convert.ToString(txtNombre.Text);
            vSupervisionPreguntas.Descripcion = Convert.ToString(txtDescripcion.Text);
            vSupervisionPreguntas.IdTipoRespuesta = Convert.ToInt32(ddlIdTipoRespuesta.SelectedValue);
            vSupervisionPreguntas.Aplicable = Convert.ToInt32(rblAplicable.SelectedValue);
            if (ddlResponsable.SelectedValue != "-1" && ddlResponsable.SelectedValue!="")
            {
                vSupervisionPreguntas.Responsable = Convert.ToInt32(ddlResponsable.SelectedValue);
            }
            vSupervisionPreguntas.Estado = Convert.ToInt32(rblEstado.SelectedValue);
            int vResultExis = vSupervisionService.ConsultarSupervisionPreguntas(vSupervisionPreguntas.Nombre, vSupervisionPreguntas.Aplicable);


            if (Request.QueryString["oP"] == "E")
            {
                    vSupervisionPreguntas.IdPregunta = Convert.ToInt32(hfIdPregunta.Value);
                    vSupervisionPreguntas.UsuarioModifica = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionPreguntas, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.ModificarSupervisionPreguntas(vSupervisionPreguntas);
            }
            else
            {

                if (vResultExis == 0)
                {
                    vSupervisionPreguntas.UsuarioCrea = GetSessionUser().NombreUsuario;
                    InformacionAudioria(vSupervisionPreguntas, this.PageName, vSolutionPage);
                    vResultado = vSupervisionService.InsertarSupervisionPreguntas(vSupervisionPreguntas);
                }
                else
                {
                    toolBar.MostrarMensajeError("La pregunta ingresada ya existe");
                    vResultado = 3;
                }
            }
            if (vResultado == 0)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, verifique por favor.");
            }
            else if (vResultado == 1)
            {
                SetSessionParameter("SupervisionPreguntas.IdPregunta", vSupervisionPreguntas.IdPregunta);
                SetSessionParameter("SupervisionPreguntas.Guardado", "1");
                NavigateTo(SolutionPage.Detail);
            }
            else if (vResultado != 3)
            {
                toolBar.MostrarMensajeError("La operación no se completo satisfactoriamente, afecto mas registros de los esperados, verifique por favor.");
            }
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void Iniciar()
    {
        try
        {
            toolBar = (masterPrincipal)this.Master;
            toolBar.eventoGuardar += new ToolBarDelegate(btnGuardar_Click);
            toolBar.eventoNuevo += new ToolBarDelegate(btnNuevo_Click);
            toolBar.eventoBuscar += new ToolBarDelegate(btnBuscar_Click);

            toolBar.EstablecerTitulos("Preguntas", SolutionPage.Add.ToString());
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }

    private void CargarRegistro()
    {
        try
        {
            int vIdPregunta = Convert.ToInt32(GetSessionParameter("SupervisionPreguntas.IdPregunta"));
            RemoveSessionParameter("SupervisionPreguntas.IdPregunta");

            SupervisionPreguntas vSupervisionPreguntas = new SupervisionPreguntas();
            vSupervisionPreguntas = vSupervisionService.ConsultarSupervisionPreguntas(vIdPregunta);
            hfIdPregunta.Value = vSupervisionPreguntas.IdPregunta.ToString();
            ddlIdDireccion.SelectedValue = vSupervisionPreguntas.IdDireccion.ToString();
            txtNombre.Text = vSupervisionPreguntas.Nombre;
            txtDescripcion.Text = vSupervisionPreguntas.Descripcion;
            ddlIdTipoRespuesta.SelectedValue = vSupervisionPreguntas.IdTipoRespuesta.ToString();
            rblAplicable.SelectedValue = vSupervisionPreguntas.Aplicable.ToString();
            if (ddlIdDireccion.SelectedValue == "1")
            {
                ddlResponsable.Enabled = true;
                ddlResponsable.Visible = true;
                lblResponsable.Visible = true;
                ManejoControlesSupervision Controles = new ManejoControlesSupervision();
                Controles.LlenarResponsable(ddlResponsable, "-1", true, Int16.Parse(ddlIdDireccion.SelectedValue));
                ddlResponsable.SelectedValue = vSupervisionPreguntas.Responsable.ToString();

            }
            else
            {
                ddlResponsable.Enabled = false;
                ddlResponsable.Visible = false;
                lblResponsable.Visible = false;


            }
            rblEstado.SelectedValue = vSupervisionPreguntas.Estado.ToString();
            ((Label)toolBar.FindControl("lblAuditoria")).Text = GetAuditLabel(vSupervisionPreguntas.UsuarioCrea, vSupervisionPreguntas.FechaCrea, vSupervisionPreguntas.UsuarioModifica, vSupervisionPreguntas.FechaModifica);
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    private void CargarDatosIniciales()
    {
        try
        {

            ManejoControlesSupervision.LlenarAplicable(rblAplicable, "1");
            ManejoControlesSupervision.ValoresTrueFalseRadioButtonList(rblEstado, "Activo", "Inactivo", "1");

            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlearTipoRespuesta(ddlIdTipoRespuesta, "-1", true);
            Controles.LlenarDireccion(ddlIdDireccion, "-1", true);

            //if (ddlIdDireccion.SelectedValue == "1")
            //{
            //    ddlResponsable.Enabled = true;
            //    ddlResponsable.Visible = true;
            //    lblResponsable.Visible = true;
            //    Controles.LlenarResponsable(ddlResponsable, "-1", true, Int16.Parse(ddlIdDireccion.SelectedValue));
            //}
            //else
            //{
            //    ddlResponsable.Enabled = false;
            //    ddlResponsable.Visible = false;
            //    lblResponsable.Visible = false;
            //}
        }
        catch (UserInterfaceException ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
        catch (Exception ex)
        {
            toolBar.MostrarMensajeError(ex.Message);
        }
    }
    protected void ddlIdDireccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIdDireccion.SelectedValue == "1")
        {
            ddlResponsable.Enabled = true;
            ddlResponsable.Visible = true;
            lblResponsable.Visible = true;
            ManejoControlesSupervision Controles = new ManejoControlesSupervision();
            Controles.LlenarResponsable(ddlResponsable, "-1", true, Int16.Parse(ddlIdDireccion.SelectedValue));
        }
        else
        {
            ddlResponsable.Enabled = false;
            ddlResponsable.Visible = false;
            lblResponsable.Visible = false;


        }
    }
}
