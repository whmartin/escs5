<%@ Page Title="" Language="C#" MasterPageFile="~/General/General/Master/main2.master"
    AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_SupervisionPreguntas_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCont" runat="Server">
    <asp:Panel runat="server" ID="pnlConsulta">
        <table width="90%" align="center">
            <tr class="rowB">
                <td>
                    Dirección *
                </td>
                <td>
                    Aplicable a *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdDireccion" AutoPostBack="True" OnSelectedIndexChanged="ddlIdDireccion_SelectedIndexChanged"
                        Width="80%">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblAplicable" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Pregunta *
                </td>
                <td>
                    Tipo de respuesta *
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:TextBox runat="server" ID="txtNombre" Width="80%"></asp:TextBox>
                    <Ajax:FilteredTextBoxExtender ID="ftNombre" runat="server" TargetControlID="txtNombre"
                        FilterType="Custom,LowercaseLetters,UppercaseLetters" ValidChars="/áéíóúÁÉÍÓÚñÑ .,@_():;0123456789" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlIdTipoRespuesta" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="rowB">
                <td>
                    Estado *
                </td>
                <td>
                    <asp:Label ID="lblResponsable" runat="server" Text="Responsable *"></asp:Label>
                </td>
            </tr>
            <tr class="rowA">
                <td>
                    <asp:RadioButtonList runat="server" ID="rblEstado" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlResponsable" Width="80%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLista">
        <table width="90%" align="center">
            <tr class="rowAG">
                <td>
                    <asp:GridView runat="server" ID="gvSupervisionPreguntas" AutoGenerateColumns="False"
                        AllowPaging="True" GridLines="None" Width="100%" DataKeyNames="IdPregunta" CellPadding="0"
                        Height="16px" OnPageIndexChanging="gvSupervisionPreguntas_PageIndexChanging"
                        OnSelectedIndexChanged="gvSupervisionPreguntas_SelectedIndexChanged"
                        OnRowDataBound="gvSupervisionPreguntas_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnInfo" runat="server" CommandName="Select" ImageUrl="~/Image/btn/info.jpg"
                                        Height="16px" Width="16px" ToolTip="Detalle" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Direccion" DataField="IdDireccion" />
                            <asp:BoundField HeaderText="Pregunta" DataField="Nombre" />
                            <asp:BoundField HeaderText="Descripción de la pregunta" DataField="Descripcion" />
                            <asp:BoundField HeaderText="Tipo de respuesta" DataField="NombreTipoRespuesta" />
                            <asp:BoundField HeaderText="Aplicable a" DataField="Aplicable" />
                            <asp:BoundField HeaderText="Responsable" DataField="NombreResponsable" />
                            <asp:BoundField HeaderText="Estado" DataField="Estado" />
                        </Columns>
                        <AlternatingRowStyle CssClass="rowBG" />
                        <EmptyDataRowStyle CssClass="headerForm" />
                        <HeaderStyle CssClass="headerForm" />
                        <RowStyle CssClass="rowAG" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
